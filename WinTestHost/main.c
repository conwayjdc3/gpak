// File Name: main.c
//
// Description:
//   This file contains the main Trilogy host test application.
//

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN	// prevent include of winsock.h
#endif

//#define _CRT_SECURE_NO_WARNINGS

#include <Windows.h>
#include <CommCtrl.h>
#include <tchar.h>
#include "resource.h"
#include <stdio.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include "stdint.h"
typedef int Bool;
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "GpakCustWin32.h"
#include "GpakHpi.h"
#include "GpakApi.h"
#include "GpakEnum.h"
#include "ED137BGpakApi.h"

#pragma comment(lib, "ComCtl32.lib")
HWND hDlg;

#define PackCore(CoreId,ChanId)   ((ADT_UInt16) ((CoreId)<<8) | ((ChanId) & 0x00ff))
#define CORE_ID(chan) ((chan >> 8) & 0xFF)
#define CHAN_ID(chan) (chan & 0xFF)

#define FAX_TRANSPORT faxRtp

// Dynamic payload type assignments
#define TELE_DYNTYPE      98
#define OPUS_DYNTYPE      106
#define L16_8K_DYNTYPE    107
#define L16_16K_DYNTYPE   108
#define ILBC_20MS_DYNTYPE 109
#define ILBC_30MS_DYNTYPE 109
#define G722_2_DYNTYPE    111  
#define G726_16_DYNTYPE   112 
#define G726_24_DYNTYPE   113 
#define G726_32_DYNTYPE   114 
#define G726_40_DYNTYPE   115 
#define ISAC_DYNTYPE      116
#define G722_1_DYNTYPE    117  

#define  DTMF_MODE_INBAND    1  // pass through unaltered...no suppression, no reporting
#define  DTMF_MODE_RELAY     2  // rfc 2833 tone relay
#define  DTMF_MODE_OUTOFBAND 3  // out of band... detect, suppress tone, notify host
const char *dtmf_mode_str[] = {"NONE", "INBAND", "RELAY", "OUTBAND"};
const char *fax_mode_str[]  = {"DISABLE", "FAXONLY", "FAXVOICE"};
char *cidmode_string[] = {
"disabled",
"Rx On Hook",
"Rx Off Hook",
"Tx On Hook",
"Tx Off Hook"
};

// G.PAK host custom APIs.                  
extern void initGpakStructures(void);
extern int gpakServerInit(int ipVer);
extern int gpakServerConnect(
	ADT_UInt32 DspId,
	char *gpakServer,
	void *remoteIP
	);
extern void gpakServerDisconnect(ADT_UInt32 DspId);
void getver();

// custom command interface for client testing 
#define PERIODIC_TIMER       // enables timer function when defined

#define CUSTOM_PERIODIC_SECS 8
#define CUSTOM_COMMANDS
#ifdef CUSTOM_COMMANDS
extern void (*procCustCommand) (char *pUserCmd);
extern void initCustomCommand(int minChanCore, int maxChanCore);
extern void setCustMenuState(int onOff);
extern int  getCustMenuState();
extern int getCustPeriodicActive();
extern void custPeriodicFunc(unsigned int count);

#endif

int tcpIpVersion = 4;
int rtpIpVersion = 4;

GpakSerialPort_t APort = McBSPPort0;
GpakSerialPort_t BPort = McBSPPort0;

#define DEBIAN_GW_DEVICE 0
#define ALLWORX_DEVICE   1
#define STMF4_DEVICE     2
#define DESKTOP_BROADCOM 3

#define REMOTE_DEVICE    DESKTOP_BROADCOM

#if (REMOTE_DEVICE == ALLWORX_DEVICE)
    // Allworx test phone 
    // MAC: 00.0a.dd.80.00.29
    // IP == 192.168.0.250
#define DEFAULT_DEST_IP 0xC0A800FA
#elif (REMOTE_DEVICE == DEBIAN_GW_DEVICE)
    // Debian VEGW 
    // MAC: 08.00.27.06.3c.3d
    // IP == 192.168.0.33
#define DEFAULT_DEST_IP 0xC0A80021
#elif (REMOTE_DEVICE == STMF4_DEVICE)
	// Stmicro F4 
    // MAC: 02.00.00.00.00.00
    // IP == 192.168.0.11
#define DEFAULT_DEST_IP 0xC0A8000B
#elif (REMOTE_DEVICE == DESKTOP_BROADCOM)
    // MAC: 00:10:18:ea:50:cb
    // IP == 192.168.0.30
#define DEFAULT_DEST_IP 0xC0A8001E
#endif

#define TXMCAST 1
#define RXMCAST 2
int initMac = 0;

// Application related variables.
static HINSTANCE thisAppInstance;	// this app's instance

// Network related variables.
WSADATA wsaData;				// Windows Sockets data
static Bool networkReady;		// flag: network interface is ready
//#define GPAKSERVER "Gpak.00.08.ff.67.48.01" //"TrilogyWinTestHost0424"		// must be 22 chars
//#define GPAKSERVER "Gpak.00.08.ee.05.7e.2e" // LCDK C6748		// must be 22 chars
//#define GPAKSERVER "Gpak.00.18.30.0a.0e.44" // Steve's 6657 evm
#define GPAKSERVER "Gpak.00.18.30.0a.17.6d" // Jim's 6657 evm

char GpakServer[23];
static ADT_UInt32 dspIpAddress=0;	// DSP's IP address
static IP6N dspIpAddress6;
DSP_Address PlayRecAddress = 0; //x98000000;

// Jim's desktop ADT_LAN_172 link-local IPv6 == fe80::b55c:ef7e:cb55:4630
    // MAC: 00:10:18:ea:50:cb

static ADT_UInt8 Defaut_Dest_IpV6[16] = {0xfe,0x80,0x00,0x00,
                                         0x00,0x00,0x00,0x00,
							             0xb5,0x5c,0xef,0x7e,
							             0xcb,0x55,0x46,0x30}; 

 // IANA IPv6: variable scope,  'any private experiment' ipv6 mcast address. Assigned scope of link-local (i.e. 'ff02').
static ADT_UInt8 BaseMCastIPv6[16]    = {0xff,0x02,0x00,0x00,
                                         0x00,0x00,0x00,0x00,
							             0x00,0x00,0x00,0x00,
							             0x00,0x00,0x01,0x14};
static BaseMCastIP = 0xE000001A;   // 224.0.0.26 --> IANA IPv4 mcast unassigned

#define USER_OUT_BUFLEN 64000
// User input and output related variables.
static char userInBufr[128];	// user input buffer
static char userOutBufr[USER_OUT_BUFLEN];	// user output buffer
static UINT userInLength;		// user input length
static UINT userOutLength;		// user output length
static UINT userOutIndex;		// user output index
static UINT eventCounts = 0;

// Serial port output related variables.
static char uartPrintBufr[1024];	// serial port output print buffer
static char ipAddrsStringBufr[48];	// IP address string buffer
static char ipAddrsStringBufr1[48];	// IP address string buffer

// Definition of channel algorithm Ids.
typedef enum
{
	algoAec = 0,				// AEC
	algoLec = 1,				// LEC
	algoAgc = 2,				// AGC
	algoRtcp= 3,				// RTCP
	algoVadCng= 4,				// VADCNG
	algoDtx= 5, 				// DTX
    algoData=6                  // DATA mode
} chanAlgoId_t;

GpakCompandModes compandMode[3] = {cmpNone16, cmpNone16, cmpNone16};
char *cmpModeString[3] = {"U-LAW", "A-LAW", "L16"};
typedef enum
{
    detect_none  = 0,
    detect_dtmf  = 1,
    detect_cprg  = 2,
    detect_mfr2f = 3,
    detect_mfr2r = 4,
    detect_arb   = 5,
    detect_mfr1  = 6,
    detect_fax   = 7
} chanDetectId_t;
char *detectorNames[]  = {
"NONE",
"DTMF",
"CPRG",
"MFR2F",
"MFR2R",
"ARB",
"MFR1",
"FAX"
};
GpakToneTypes toneTypeMap[] = {0, (DTMF_tone|Tone_Squelch), CallProg_tone, MFR2Fwd_tone, MFR2Back_tone, Arbit_tone, MFR1_tone, FAX_tone};   
char *sideString[] = {"A-Side","B-Side"};

typedef struct stressStats_t {
    uint32_t    chCfgPass; 
    uint32_t    chCfgFail; 
    uint32_t    rtpCfgPass;
    uint32_t    rtpCfgFail;
    uint16_t    pktTxCnt;
    uint16_t    pktRxCnt;
} stressStats_t;


// Channel configuration related variables.
#define NUM_TEST_CHANS MAX_CHANNELS		// number of test channels
#define NUM_CONFS      2

ADT_UInt16 chanCore[NUM_TEST_CHANS];            // core assigned to this channel
ADT_UInt16 confCore[NUM_CONFS];                 // core assigned to this conference
GpakFrameHalfMS confFrameSize[NUM_CONFS];	// channel Frame Size
stressStats_t stressStats[NUM_TEST_CHANS];
int slotCross = 0;
int confReady[NUM_CONFS];
uint16_t chanMultiCast[NUM_TEST_CHANS];			// channel Multi-cast enabled
uint16_t portCrossover[NUM_TEST_CHANS];			// channel Crossover enabled
GpakCodecs chanCodecType[NUM_TEST_CHANS];		// channel Codec Type
GpakFrameHalfMS chanFrameSize[NUM_TEST_CHANS];	// channel Frame Size
uint16_t chanInSlotA[NUM_TEST_CHANS];			// channel side A input slot
uint16_t chanInSlotB[NUM_TEST_CHANS];			// channel side B input slot
uint16_t chanOutSlotA[NUM_TEST_CHANS];			// channel side A output slot
uint16_t chanOutSlotB[NUM_TEST_CHANS];			// channel side B output slot
int16_t chanInGainA[NUM_TEST_CHANS];			// channel side A input gain
int16_t chanInGainB[NUM_TEST_CHANS];			// channel side B input gain
int16_t chanOutGainA[NUM_TEST_CHANS];			// channel side A output gain
int16_t chanOutGainB[NUM_TEST_CHANS];			// channel side B output gain
GpakActivation chanAecEnable[NUM_TEST_CHANS];	// channel AEC Enable
GpakActivation chanLecEnable[NUM_TEST_CHANS];	// channel LEC Enable
GpakActivation chanAgcEnable[NUM_TEST_CHANS];	// channel AGC Enable
uint32_t chanDestRtpIp[NUM_TEST_CHANS];		// channel Destination RTP IP Addrs
uint16_t chanDestRtpPort[NUM_TEST_CHANS];	// channel Destination RTP Port
uint16_t chanSourceRtpPort[NUM_TEST_CHANS];	// channel Destination RTP Port
uint16_t chanJitrBufrSize[NUM_TEST_CHANS];	// channel Jitter Buffer Size (msec)
chanDetectId_t chanDetectA[NUM_TEST_CHANS]; // A-side tone detectors
chanDetectId_t chanDetectB[NUM_TEST_CHANS]; // B-side tone detectors
uint16_t chanCryptoSuite[NUM_TEST_CHANS];
ADT_UInt8 VlanIdx[NUM_TEST_CHANS];
GpakFaxMode_t chanFaxMode[NUM_TEST_CHANS];
int chanRtcpEnable[NUM_TEST_CHANS];
ADT_UInt8 rtcpByeData[256];
ADT_UInt8 rtcpAppData[256];
ADT_UInt8 rtcpSdesData[MAX_SDES_ITEMS][256];
GpakActivation chanVadCngEnable[NUM_TEST_CHANS];
GpakActivation chanDtxEnable[NUM_TEST_CHANS];
int dtmfMode[NUM_TEST_CHANS];
GpakActivation chanDataEnable[NUM_TEST_CHANS];
uint16_t chanCIDMode[NUM_TEST_CHANS]; // 0 disabled, 1=RxOnHook, 2=RxOffHook, 3=TxOnHook, 4=TxOffHook  

ADT_UInt8  chanIpVersion[NUM_TEST_CHANS];       // Channel IP verison Ipv4 or Ipv6
GpakIPv6_t chanDestRtpV6Ip[NUM_TEST_CHANS];		// channel Destination RTP IP Addrs
int num_Of_NewKeys= 0;

// Channel status related variables.
uint32_t rtpTxPktCnt[NUM_TEST_CHANS];	// RTP Tx packet count
uint32_t rtpRxPktCnt[NUM_TEST_CHANS];	// RTP Rx packet count

// AEC and LEC related variables.
#define MAX_AEC_PARM_ID 28				// maximum AEC parameter Id
#define MAX_LEC_PARM_ID 17				// maximum LEC parameter Id
#define MAX_PTT_PARM_ID 10				// maximum PTT parameter Id
#define MAX_RTP_TX_TO_PARM_ID 2		    // maximum RTP TX Timeout Value Id 
static GpakAECParms_t hostAecParms;		// host's copy of AEC parameters
static GpakECParams_t hostLecParms;		// host's copy of LEC parameters
static GpakSystemParms_t tempSysParms;	// temporary system parameters



// DSP general tone(s) to generate.
static GpakToneGenParms_t DSPtones[] = {
	{ToneGenStart, ADevice, TgContinuous, {1004,    0, 0, 0}, {  0,   0, 0, 0}, {   0,    0, 0, 0}, {   0,    0, 0, 0} },  /* test tone */
	{ToneGenStart, ADevice, TgContinuous, { 350,  440, 0, 0}, {-12, -12, 0, 0}, {   0,    0, 0, 0}, {   0,    0, 0, 0} },  /* dial tone */
	{ToneGenStart, ADevice,    TgCadence, { 480,  620, 0, 0}, {-12, -12, 0, 0}, { 500,  500, 0, 0}, { 500,  500, 0, 0} },  /* busy tone */
	{ToneGenStart, ADevice,    TgCadence, { 480,  620, 0, 0}, {-12, -12, 0, 0}, { 250,  250, 0, 0}, { 250,  250, 0, 0} },  /* reorder tone */
	{ToneGenStart, ADevice,    TgCadence, { 440,  480, 0, 0}, {-12, -12, 0, 0}, {1000, 1000, 0, 0}, {3000, 3000, 0, 0} },  /* ring-back tone */
	{ToneGenStart, ADevice,      TgBurst, { 800,    0, 0, 0}, {-12,   0, 0, 0}, { 500,    0, 0, 0}, {   0,    0, 0, 0} },  /* bong tone */
	{ToneGenStart, ADevice,      TgBurst, { 440,  620, 0, 0}, {-18, -18, 0, 0}, {1000,    0, 0, 0}, {   0,    0, 0, 0} },  /* preemption tone */

// cornet arb starts at index==7
	{ToneGenStart, ADevice, TgBurst, { 300,  700, 0, 0}, {-12, -12, 0, 0}, {1000, 0, 0, 0}, {   0,    0, 0, 0} },
	{ToneGenStart, ADevice, TgBurst, { 480,  0,   0, 0}, {-12, -12, 0, 0}, {1000, 0, 0, 0}, {   0,    0, 0, 0} },
	{ToneGenStart, ADevice, TgBurst, { 1400, 0,   0, 0}, {-12, -12, 0, 0}, {1000, 0, 0, 0}, {   0,    0, 0, 0} },
	{ToneGenStart, ADevice, TgBurst, { 2048, 0,   0, 0}, {-12, -12, 0, 0}, {1000, 0, 0, 0}, {   0,    0, 0, 0} },
	{ToneGenStart, ADevice, TgBurst, { 2400, 0,   0, 0}, {-12, -12, 0, 0}, {1000, 0, 0, 0}, {   0,    0, 0, 0} },
	{ToneGenStart, ADevice, TgBurst, { 2600, 0,   0, 0}, {-12, -12, 0, 0}, {1000, 0, 0, 0}, {   0,    0, 0, 0} },
	{ToneGenStart, ADevice, TgBurst, { 2800, 0,   0, 0}, {-12, -12, 0, 0}, {1000, 0, 0, 0}, {   0,    0, 0, 0} },

};
static GpakToneGenParms_t DSPtonesHires[] = {
	{ToneGenStart, ADevice, TgContinuous, {1004*10,    0, 0, 0}, {  0,   0, 0, 0}, {   0,    0, 0, 0}, {   0,    0, 0, 0} },  /* test tone */
	{ToneGenStart, ADevice, TgContinuous, { 350*10,  440*10, 0, 0}, {-12*10, -12*10, 0, 0}, {   0,    0, 0, 0}, {   0,    0, 0, 0} },  /* dial tone */
	{ToneGenStart, ADevice,    TgCadence, { 480*10,  620*10, 0, 0}, {-12*10, -12*10, 0, 0}, { 500,  500, 0, 0}, { 500,  500, 0, 0} },  /* busy tone */
	{ToneGenStart, ADevice,    TgCadence, { 480*10,  620*10, 0, 0}, {-12*10, -12*10, 0, 0}, { 250,  250, 0, 0}, { 250,  250, 0, 0} },  /* reorder tone */
	{ToneGenStart, ADevice,    TgCadence, { 440*10,  480*10, 0, 0}, {-12*10, -12*10, 0, 0}, {1000, 1000, 0, 0}, {3000, 3000, 0, 0} },  /* ring-back tone */
	{ToneGenStart, ADevice,      TgBurst, { 800*10,    0, 0, 0}, {-12*10,   0, 0, 0}, { 500,    0, 0, 0}, {   0,    0, 0, 0} },  /* bong tone */
	{ToneGenStart, ADevice,      TgBurst, { 440*10,  620*10, 0, 0}, {-18*10, -18*10, 0, 0}, {1000,    0, 0, 0}, {   0,    0, 0, 0} },  /* preemption tone */

// cornet arb starts at index==7
	{ToneGenStart, ADevice, TgBurst, { 300*10,  700*10, 0, 0}, {-12*10, -12*10, 0, 0}, {1000, 0, 0, 0}, {   0,    0, 0, 0} },
	{ToneGenStart, ADevice, TgBurst, { 480*10,  0,   0, 0}, {-12*10, -12*10, 0, 0}, {1000, 0, 0, 0}, {   0,    0, 0, 0} },
	{ToneGenStart, ADevice, TgBurst, { 1400*10, 0,   0, 0}, {-12*10, -12*10, 0, 0}, {1000, 0, 0, 0}, {   0,    0, 0, 0} },
	{ToneGenStart, ADevice, TgBurst, { 2048*10, 0,   0, 0}, {-12*10, -12*10, 0, 0}, {1000, 0, 0, 0}, {   0,    0, 0, 0} },
	{ToneGenStart, ADevice, TgBurst, { 2400*10, 0,   0, 0}, {-12*10, -12*10, 0, 0}, {1000, 0, 0, 0}, {   0,    0, 0, 0} },
	{ToneGenStart, ADevice, TgBurst, { 2600*10, 0,   0, 0}, {-12*10, -12*10, 0, 0}, {1000, 0, 0, 0}, {   0,    0, 0, 0} },
	{ToneGenStart, ADevice, TgBurst, { 2800*10, 0,   0, 0}, {-12*10, -12*10, 0, 0}, {1000, 0, 0, 0}, {   0,    0, 0, 0} },

};

 // RTCP canned-SDES configuration data.
#define NUM_SDES_ITEMS 4  // send 4 sdes items: cname, email, phone, name
ADT_UInt8 sdes_text[NUM_SDES_ITEMS][256];
typedef struct sdesUserInfo_t {
    const char * cname;
    const char * name;
    const char * email;
    const char * phone;
} sdesUserInfo_t;
sdesUserInfo_t userSdesInfo = {
    "cname_usr_", "name_usr_", "email_usr_", "phone_usr_"
};

ADT_UInt32 numToneGenParms = sizeof(DSPtones)/sizeof(GpakToneGenParms_t);
// Local function prototypes.

// Main dialog box procedure.
static INT_PTR CALLBACK mainDialogProc(
	HWND hDlg,
	UINT uMsg,
	WPARAM wParam,
	LPARAM lParam
	);

// G.PAK interface thread.
static void gpakThread(void);

// Perform 'printf' to the serial port.
int uartPrintf(char *format, ...);

// Case insensitive compare strings of maximum length.
static int strncmpi(
	char *pString1,				// pointer to string 1
	char *pString2,				// pointer to string 2
	uint32_t length				// max number of characters to compare
	);
static void getEvent (void);
// Process an event from the DSP.
static void procDspEvent(
	ADT_UInt16 channelId,				// channel Id
	GpakAsyncEventCode_t eventCode,		// event code
	GpakAsyncEventData_t *pEventData	// pointer to event data
	);

// Print (display) a warning event.
static void printWarning(
	GpakAsyncEventData_t *pEventData,	// pointer to event data
	ADT_UInt16 warningValue				// warning value
	);

// Process a user command.
static void procUserCommand(
	char *pUserCmd				// pointer to user command string
	);

// Parse user input for a command.
static Bool parseCommand(
	char *pUserInput,			// pointer to user input string
	char *pCmd,					// pointer to command name string to match
	int32_t *pSuffix1,			// pointer to 1st suffix value variable
	int32_t *pSuffix2,			// pointer to 2nd suffix value variable
	char **ppArgument			// pointer to argument pointer variable
	);

// Display command help information.
static void displayHelp(void);

// Process a Codec Type command.
static void procCodecType(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg,				// pointer to command argument string
    int interleave
	);

// Determine the name of a Codec Type.
static char *codecTypeName(
	GpakCodecs codecType		// codec type
	);

// Process a Frame Size command.
static void procFrameSize(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	);

// Process a Frame Size interleaved command.
static void procFrameSizeInterleaved(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	);

// Determine the name of a Frame Size.
static char *frameSizeName(
	GpakFrameHalfMS frameSize	// frame size
	);

// Process a TDM Slot command.
static void procTdmSlot(
	Bool input,					// flag: input slot
	GpakDeviceSide_t sideId,	// side of channel Id
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	);

// Process a Channel I/O Gain command.
static void procChannelIoGain(
	Bool input,					// flag: input gain
	GpakDeviceSide_t sideId,	// side of channel Id
    GpakAlgCtrl_t controlCode,  // non-zer == real-time adjust control code
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	);


// Process a Channel Algorithm Enable command.
static void procChannelAlgoEnable(
	chanAlgoId_t algoId,		// algorithm Id
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	);

// Enables a Channels AEC.
static void procChannelEnableAEC(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	);

// Bybass a Channels AEC.
static void procChannelBypassAEC(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	);

// Reset a Channels AEC.
static void procChannelResetAEC(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	);
static void procChannelRTPControl(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	);
// Determine the name of an Algorithm State.
static char *algoStateName(
	GpakActivation algoState	// algorithm state
	);

// Parse a boolean value string.
static Bool parseBooleanValue(
	char *pString,				// pointer to string
	Bool *pValue				// pointer to variable for storing parsed value
	);

// Process an RTP IP Address command.
static void procRtpIpAddress(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	);

// Convert an IP address to a string.
static char *ipAddressToString(
	ADT_UInt32 ipAddress		// IP address
	);
static char *ipV6AddressToString(
	ADT_UInt8 *ipAddress		// IP address
	);

// Process an RTP Port command.
static void procRtpPort(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	);

// Process an RTP Source Port command.
static void procSRtpPort(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	);

// Process a Jitter Buffer Size command.
static void procJitrBufrSize(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	);

// Configure a PCM to Packet channel on the DSP.
void setupPcmToPktChannel(
	ADT_UInt16 channelId		// channel Id
	);

static void setupPcmConfChannel(
	ADT_UInt16 channelId,		// channel Id
    ADT_UInt16 confID
	);

static void setupPktConfChannel(
	ADT_UInt16 channelId,		// channel Id
    ADT_UInt16 confID
	);

static void setupCompositeConfChannel(
	ADT_UInt16 channelId,		// channel Id
    ADT_UInt16 confID
	);

static void setupPcmHalfConnect(
	ADT_UInt16 channelId		// channel Id
	);

// Configure a PCM to PCM channel on the DSP.
static void setupPcmToPcmChannel(
	ADT_UInt16 channelId		// channel Id
	);

void setupCustChannel(
	ADT_UInt16 channelId		// channel Id
	);

// Get status for a channel on the DSP.
void getChannelStatus(
	ADT_UInt16 channelId,		// channel Id
	Bool rtpRxTxOnly			// flag: only display RTP Rx and Tx pkt counts
	);

// Teardown a channel on the DSP.
void teardownChannel(
	ADT_UInt16 channelId		// channel Id
	);

// Start generating a tone for a channel on the DSP.
static void startToneGen(
	ADT_UInt16 channelId,		// channel Id
	GpakDeviceSide_t sideId,	// side of channel Id 
	ADT_UInt16 toneIdx    		// tone index
	);

// Stop generating a tone for a channel on the DSP.
static void stopToneGen(
	ADT_UInt16 channelId,		// channel Id
	GpakDeviceSide_t sideId		// side of channel Id 
	);

// Get the DSP's System Configuration.
static void getSysConfig(void);

// Get the DSP's System Parameters.
static void getSysParms(void);

static void writeSysVad(int32_t noise, int32_t hangMs, int32_t winMs, int32_t report);

// Read the DSP's IP Stack configuration.
static void readIpStackConfig(void);

// Print (display) an IP address.
static void printIpAddress(
	char *pNameText,			// pointer to name string
	ADT_UInt32 ipAddress		// IP address
	);

// Get TDM statistics from the DSP.
static void getTdmStats(void);

// Get CPU usage statistics from the DSP.
static void getCpuUsage( );

// Reset the DSP's Statistics.
static void resetDspStats(
	Bool resetCpuStats,			// flag: reset CPU statistics
	Bool resetFramingStats		// flag: reset Framing statistics
	);

// Read the DSP's AEC Parameters.
static Bool readAecParms(void);

// Write the DSP's AEC Parameters.
static Bool writeAecParms(void);

// Process an AEC Parameter command.
static void procAecParm(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	);

// Determine the name of an AEC Parameter.
static char *aecParmName(
	int32_t aecParmId			// AEC parameter Id
	);

// Display AEC Status for a channel on the DSP.
static void displayAecStatus(
	ADT_UInt16 channelId		// channel Id
	);

// Read the DSP's LEC Parameters.
static Bool readLecParms(void);

// Write the DSP's LEC Parameters.
static Bool writeLecParms(void);

// Process MemoryTest command.
static void procMemoryTest();

int procRtpRxTimeoutParams(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string);
	);

int procPTTConfigParms(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	);

int procPTTUpdateParms(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	);

int procPFParms(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	);

int procPFUpdateParms(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	);

int procNPFParms(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	);

int procGAParms(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	);
// Process SideTone command.
static void procSideToneParms(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	);

// Process an LEC Parameter command.
static void procLecParm(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	);

// convert chanDetectId to gpak tonetype
static GpakToneTypes getToneType(
    ADT_UInt16 channelId, 
    GpakDeviceSide_t side
    );

static Bool parseDetectorValue(
	char *pString,				// pointer to string
	chanDetectId_t *pValue	    // pointer to variable for storing parsed value
	);

static void getDtmfDetectFlags(
    ADT_UInt16 channelId, 
    GpakToneTypes *ADet, 
    GpakToneTypes *BDet
    );

static void procDtmfMode(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	);

static void procFaxMode(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	);

static void procChannelDetect(
	GpakDeviceSide_t sideId,		// side of channel Id 
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	);

static void procVLANSetParms(
	char *pCmdArg				// pointer to command argument string
	);

static void procVLANGetParms(
	char *pCmdArg				// pointer to command argument string
	);
static void procVLANChanEnabParms(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	);

// Determine the name of an LEC Parameter.
static char *lecParmName(
	int32_t lecParmId			// LEC parameter Id
	);

// Display LEC Status for a channel on the DSP.
static void displayLecStatus(
	ADT_UInt16 channelId,		// channel Id
	GpakDeviceSide_t chanSide	// side of channel
	);

static void tdmFixedValControl(
    ADT_UInt16 channelId, 
    GpakActivation state,  
    ADT_UInt16 value);
    
static void procCIDMode(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	);
    

// config TDM port
static void dspMcBspTDMInit (void);
static void dspMcAspTDMInit (void);
static void dspTsipTDMInit (void);
static void configArbitraryDetector();
static void  startAudioRecording(ADT_UInt16 channelId, GpakDeviceSide_t DeviceSide);
static void  audioRecordingControl(ADT_UInt16 channelId, GpakDeviceSide_t DeviceSide, char code);

static void threadInit ();
static void evtThread (void * args);

static void procMCast(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	);
static void procCrossover(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	);

static void openConf(
    ADT_UInt16 suffix1
    );

static void procCoreNum(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg,              // pointer to command argument string
    char mode				    // mode: c==constant, i==interleaved, b==block 
	);


static void procRtcpBye(ADT_UInt16 channelId);
static int  formatCustChannelConfig (ADT_UInt16 *Msg, void *chanCfg, ADT_UInt16 ChannelId);
static void parseCustChannelStatus (ADT_UInt16 *Msg, void *pChanStat);
static ADT_UInt16 sendDtmfDialString(ADT_UInt16 channelId, GpakDeviceSide_t sideId, char *str);

void stressStatInit(ADT_UInt16 chID) {
    memset(&stressStats[chID],0, sizeof(stressStats_t));
}

int stressStatReport(ADT_UInt16 chID) {

    if ((stressStats[chID].chCfgFail  != 0) ||
        (stressStats[chID].rtpCfgFail != 0) ||
        (stressStats[chID].pktTxCnt   == 0) ||
        (stressStats[chID].pktRxCnt   == 0)) 
    {

        uartPrintf("Stress failure chan%u: chFail %u, rtpFail %u, txCnt %u, rxCnt %u\n", 
            chID,
            stressStats[chID].chCfgFail,
            stressStats[chID].rtpCfgFail,
            stressStats[chID].pktTxCnt,
            stressStats[chID].pktRxCnt); 
        return 1;
    }
    return 0;
}

static int isDspIpAddressNonZero() {
ADT_UInt32 ip = 0;
    if (tcpIpVersion == 4) {
        return (dspIpAddress != 0);
    } else {
        ip = dspIpAddress6.u.addr32[0];
        ip |= dspIpAddress6.u.addr32[1];
        ip |= dspIpAddress6.u.addr32[2];
        ip |= dspIpAddress6.u.addr32[3];
        return (ip != 0);
    }
}


// Remote device's IP address on out-bound packets (for multicast transmits)
ADT_Int32  dstTxIPFromChan (ADT_UInt16 chID) {
   if(chanMultiCast[chID] & TXMCAST)
      return (BaseMCastIP);
   return 0;
}

ADT_Int32  dstTxIPv6FromChan (ADT_UInt16 chID, ADT_UInt8*TxDestIP) {
   if(chanMultiCast[chID] & TXMCAST)
      memcpy(TxDestIP, BaseMCastIPv6, sizeof(GpakIPv6_t));
    else
      memset(TxDestIP, 0, sizeof(GpakIPv6_t));
   return 0;
}
// Destination IP of in-bound packets (For multicast receives)
ADT_Int32 lclDstIPFromChan (ADT_UInt16 chID) {
   if(chanMultiCast[chID] & RXMCAST)
      return (BaseMCastIP);

   return 0;
}
void lclDstIPv6FromChan (ADT_UInt16 chID, ADT_UInt8*InDestIP) {
	if(chanMultiCast[chID] & RXMCAST)
		memcpy(InDestIP, BaseMCastIPv6, sizeof(GpakIPv6_t));
    else
        memset(InDestIP, 0, sizeof(GpakIPv6_t));
}

// SSRC for packets originating at DSP
#define BaseSSRC 1

// SSRC for packets originating at DSP
ADT_UInt32 ssrcFromChan (ADT_UInt16 chID) {
	return (BaseSSRC + chID);
}

// SSRC for packets originating at remote soruce
ADT_UInt32 dstSSRCFromChan (ADT_UInt16 chID) {
ADT_UInt16 dstChID;
	if(portCrossover[chID]) 
		dstChID = (chID ^ 1);
	else
		dstChID = (chID);

	return (BaseSSRC + dstChID);		
}

// Local device's UDP port
ADT_UInt16 srcPortFromChan (ADT_UInt16 chID) {
    //return 6334 + (chID * 2);
    return chanSourceRtpPort[chID];
}

// Remote device's UDP port
ADT_UInt16 dstPortFromChan (ADT_UInt16 chID) {
	if (portCrossover[chID])  
		return srcPortFromChan ((ADT_UInt16) (chID^1));

	return chanDestRtpPort[chID];
}

static void setupTxMCASTPcmToPktChannel(
	ADT_UInt16 channelId		// channel Id
	);
static void setupRxMCASTPcmToPktChannel(
	ADT_UInt16 channelId		// channel Id
	);
static void sendCIDMsg (ADT_UInt16 chanId);
static void gpakSendSrtpKey(ADT_UInt32 DspId, ADT_UInt16 chId, GpakAsyncEventData_t *eventData) ;

volatile ADT_UInt16 breakChannel = 46;
volatile int pause=0;

// Advanced SRTP test
// Definition of Key schemes.
//SRTP Key stuff 
#define NUM_KEY_ON_RING 7
typedef enum SrtpKeyScheme_t {
    KEY_PSK = 0,                // Pre-shared Master key
    KEY_MKI = 1,                // Master Key Identifier
    KEY_FT =  2                 // From To
} SrtpKeyScheme_t;

// Definition of Encrytion types.
typedef enum SrtpEncryptType_t {
    ENCRYPT_NONE = 0,           // None
    ENCRYPT_CM = 1,             // Counter Mode
    ENCRYPT_F8 = 2,             // F8 Mode
    ENCRYPT_CBC = 3             // CBC
} SrtpEncryptType_t;

// Definition of Authentication types.
typedef enum SrtpAuthType_t {
    AUTH_NONE = 0,              // None
    AUTH_HMAC_SHA = 1,          // HMAC-SHA
    AUTH_MD5 = 2                // MD5
} SrtpAuthType_t;
typedef struct optionParams {
   SrtpKeyScheme_t   KeyScheme;
   SrtpEncryptType_t EncryptType;
   SrtpAuthType_t    AuthType;
   ADT_UInt16        Kdr;
   ADT_UInt16        MkiI8;
   ADT_UInt8         AuthI8;
} optionParams;

int SRTP_Key_Option=0;  // 0-11 specifies crypto suite 1-12
optionParams optTests [] = {
   {  KEY_PSK,    ENCRYPT_CM,     AUTH_HMAC_SHA,   25,  0, 10 },   // SUITE 1
   {  KEY_PSK,    ENCRYPT_CM,     AUTH_HMAC_SHA,   25,  0, 4 },    // SUITE 2
   {  KEY_PSK,    ENCRYPT_CM,     AUTH_HMAC_SHA,    8,  0, 10 },   // SUITE 3
   {  KEY_PSK,    ENCRYPT_CM,     AUTH_MD5,        25,  0, 10 },   // SUITE 4

   {  KEY_PSK,    ENCRYPT_F8,     AUTH_HMAC_SHA,   25,  0, 10 },   // SUITE 5
   {  KEY_PSK,    ENCRYPT_F8,     AUTH_HMAC_SHA,    8,  0, 10 },   // SUITE 6
   {  KEY_PSK,    ENCRYPT_F8,     AUTH_MD5,        25,  0, 10 },   // SUITE 7

   {  KEY_MKI,    ENCRYPT_CM,     AUTH_HMAC_SHA,   25,  2, 10 },   // SUITE 8
   {  KEY_MKI,    ENCRYPT_CM,     AUTH_HMAC_SHA,    8,  2, 10 },   // SUITE 9
   {  KEY_MKI,    ENCRYPT_CM,     AUTH_HMAC_SHA,    8,  2, 10 },   // SUITE 10
   {  KEY_MKI,    ENCRYPT_CM,     AUTH_HMAC_SHA,   25,  3, 10 },   // SUITE 11
   {  KEY_MKI,    ENCRYPT_CM,     AUTH_HMAC_SHA,    8,  4, 10 },   // SUITE 12

   
   {  KEY_PSK,    ENCRYPT_NONE }   // Terminator
};
const char *crypto_suites[13] = {
"SRTP DISABLED",
"1)  PSK  CM  HMAC_SHA  KDR25  MKILEN0  AUTHLEN10", 
"2)  PSK  CM  HMAC_SHA  KDR25  MKILEN0  AUTHLEN4",  
"3)  PSK  CM  HMAC_SHA  KDR8   MKILEN0  AUTHLEN10", 
"4)  PSK  CM  MD5       KDR25  MKILEN0  AUTHLEN10", 
"5)  PSK  F8  HMAC_SHA  KDR25  MKILEN0  AUTHLEN10", 
"6)  PSK  F8  HMAC_SHA  KDR8   MKILEN0  AUTHLEN10", 
"7)  PSK  F8  MD5       KDR25  MKILEN0  AUTHLEN10", 
"8)  MKI  CM  HMAC_SHA  KDR25  MKILEN2  AUTHLEN10", 
"9)  MKI  CM  HMAC_SHA  KDR8   MKILEN2  AUTHLEN10", 
"10) MKI  CM  HMAC_SHA  KDR8   MKILEN2  AUTHLEN10", 
"11) MKI  CM  HMAC_SHA  KDR25  MKILEN3  AUTHLEN10", 
"12) MKI  CM  HMAC_SHA  KDR8   MKILEN4  AUTHLEN10", 
};


/* MKI only ket stuff */
typedef struct keySchedule {
   ADT_UInt32 roc;
   ADT_UInt16 seq;
   ADT_UInt32 mkiLife;
   ADT_UInt32 mkiValue;
   ADT_UInt8 key [16];
} keySchedule;

// Schedule is for key such that    roc + seq <= key <= roc + seq + key_life - 1
	/* Master
		HEX(e1, f9), HEX(7a, 0d), HEX(3e, 01), HEX(8b, e0),
		HEX(d6, 4f), HEX(a3, 2c), HEX(06, de), HEX(41, 39)
		Salt
		HEX(0e, c6), HEX(75, ad), HEX(49, 8a), HEX(fe, eb),
		HEX(b6, 96), HEX(0b, 3a), HEX(ab, e6)

	*/
keySchedule keySched [NUM_KEY_ON_RING] = { 
  { 0x00000000, 0x0000, 0x100,       0x12345678,  { 0x12, 0x34, 0x23, 0x45, 0x34, 0x56, 0x45, 0x67, 0x56, 0x78, 0x67, 0x89, 0x78, 0x9A, 0x89, 0xAB } },
  { 0x00000000, 0x0000, 0x100,       0x12345679,  { 0x45, 0x67, 0x56, 0x78, 0x67, 0x89, 0x78, 0x9A, 0x89, 0xAB, 0x9A, 0xBC, 0xAB, 0xCD, 0xDE, 0xF0 } },
  { 0x00000001, 0x0000, 0x100,       0x1234567a,  { 0x23, 0x45, 0x34, 0x56, 0x45, 0x67, 0x56, 0x78, 0x67, 0x89, 0x78, 0x9A, 0x89, 0xAB, 0x9A, 0xBC } },
  { 0x00000001, 0x0000, 0x100,       0x1234567b,  { 0x23, 0x45, 0x34, 0x56, 0x45, 0x67, 0x56, 0x78, 0x67, 0x89, 0x78, 0x9A, 0x89, 0xAB, 0x9A, 0xBC } },
  { 0x00000001, 0x0010, 0x200,       0x1234567c,  { 0x34, 0x56, 0x45, 0x67, 0x56, 0x78, 0x67, 0x89, 0x78, 0x9A, 0x89, 0xAB, 0x9A, 0xBC, 0xAB, 0xCD } },
  { 0x00000001, 0x0020, 0x200,       0x1234567d,  { 0x45, 0x67, 0x56, 0x78, 0x67, 0x89, 0x78, 0x9A, 0x89, 0xAB, 0x9A, 0xBC, 0xAB, 0xCD, 0xDE, 0xF0 } },
  { 0xffffffff, 0xffff, 0x800,       0x1234567e,  { 0xdd, 0xdd, 0x45, 0x67, 0x56, 0x78, 0x67, 0x89, 0x78, 0x9A, 0x89, 0xAB, 0x9A, 0xBC, 0xAB, 0xCD } }
};


typedef struct keyRingChannel_t {
	int option[MAX_CHANNELS]; // record of the srtp types
	int keyIndex[MAX_CHANNELS];
} keyRingChannel_t;

keyRingChannel_t SRTP_keyRingEncode;
keyRingChannel_t SRTP_keyRingDecode;
init_keyRing = 0;

static void procSRTPCryptoSuite(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	)
{
	int32_t i;					// loop index / counter
    int32_t crypto = 0;

	// Validate the channel Id.
	if (cmdSuffix1 < 0)
	{
		cmdSuffix1 = 0;
		cmdSuffix2 = NUM_TEST_CHANS - 1;
	}
	else if ((cmdSuffix1 >= NUM_TEST_CHANS) || (cmdSuffix2 >= NUM_TEST_CHANS))
	{
		uartPrintf("Channel exceeds max of %u!\n", NUM_TEST_CHANS - 1);
		return;
	}

	// If the argument is null, display the current value(s).
	if (pCmdArg == NULL)
	{
		if (cmdSuffix1 < cmdSuffix2)
		{
			uartPrintf("SRTP CRYPTO SUITE: \n");
			for (i = cmdSuffix1; i <= cmdSuffix2; i++)
			{
                crypto = chanCryptoSuite[i];
				uartPrintf(" [%u] = %s \n", i, crypto_suites[crypto]);
			}
		}
		else
		{
            crypto = chanCryptoSuite[cmdSuffix1];
			uartPrintf(" [%u] = %s \n", cmdSuffix1, crypto_suites[crypto]);
		}
		return;
	}
	if ((sscanf(pCmdArg, "%u", &crypto) != 1) || (crypto > 12))
	{
		uartPrintf("Invalid Crypto Suite: %s\n", pCmdArg);
		return;
	}

	// Set the channel(s) current value.
	if (cmdSuffix1 < cmdSuffix2)
	{
		for (i = cmdSuffix1; i <= cmdSuffix2; i++)
		{
            chanCryptoSuite[i] = crypto;
		}
		uartPrintf("Chan %d-%d CryptoSuite: %s\n",
				   cmdSuffix1, cmdSuffix2, crypto_suites[crypto]);
	}
	else
	{
        chanCryptoSuite[cmdSuffix1] = crypto;
		uartPrintf("Chan %d CryptoSuite: %s\n",
				   cmdSuffix1, crypto_suites[crypto]);
	}

	return;
}

void srtpGetConfig (int ChanId, int opt, GpakSRTPCfg_t    *srtpCfgPtr)
{
   keySchedule *keyElement;
   optionParams *optTest;
   ADT_UInt32  mask = 0, mki = 0, *pMkiValue, MkiValue=0;
   ADT_UInt8  i, *mkey, *msalt, *lkey, *lsalt;
	int keyIndex = 0;
	
	if(!init_keyRing) {
		memset(&SRTP_keyRingEncode, 0, sizeof(keyRingChannel_t));
		memset(&SRTP_keyRingDecode, 0, sizeof(keyRingChannel_t));
		init_keyRing = 1;
		num_Of_NewKeys= 0;
	}
	optTest = &optTests[opt];


   // Initialize Encode SRTP processing for channels
   keyElement = keySched;
   keyIndex = 0;
   srtpCfgPtr->Encode.EncryptionScheme = optTest->EncryptType;
   srtpCfgPtr->Encode.AuthenticationScheme = optTest->AuthType;
   srtpCfgPtr->Encode.AuthenticationI8 = optTest->AuthI8;
   srtpCfgPtr->Encode.MKI_I8 = optTest->MkiI8;
   srtpCfgPtr->Encode.KDR = optTest->Kdr;
   srtpCfgPtr->Encode.KeySizeU8 = 16;

	if(optTest->KeyScheme == KEY_MKI) {
      switch (optTest->MkiI8) {
      case 2: mask = 0xffff;     break;
      case 3: mask = 0xffffff;   break;
      case 4: mask = 0xffffffff; break;
      }
      MkiValue = keyElement->mkiValue & mask;
      srtpCfgPtr->Encode.MKI[0] = (ADT_UInt8)((MkiValue>>24) & 0xff);
      srtpCfgPtr->Encode.MKI[1] = (ADT_UInt8)((MkiValue>>16) & 0xff);
      srtpCfgPtr->Encode.MKI[2] = (ADT_UInt8)((MkiValue>>8) & 0xff);
      srtpCfgPtr->Encode.MKI[3] = (ADT_UInt8)((MkiValue) & 0xff);
      srtpCfgPtr->Encode.MasterKeyLife[0] = 0;
      srtpCfgPtr->Encode.MasterKeyLife[1] = (keyElement->mkiLife >> 16) & 0xffff;
      srtpCfgPtr->Encode.MasterKeyLife[2] = keyElement->mkiLife & 0xffff;

	}

   mkey  = &srtpCfgPtr->Encode.MasterKey[0];
   msalt = &srtpCfgPtr->Encode.MasterSalt[0];
   lkey  = &keyElement->key[0];
   lsalt = &keyElement->key[2];

   for (i=0; i<14; i++) {
      *mkey++  = *lkey++;
      *msalt++ = *lsalt++;
   }
   *mkey++ = *lkey++;
   *mkey++ = *lkey++;

   SRTP_keyRingEncode.option[ChanId] = opt;
   SRTP_keyRingEncode.keyIndex[ChanId] = keyIndex;
   

   srtpCfgPtr->Decode.EncryptionScheme = optTest->EncryptType;
   srtpCfgPtr->Decode.AuthenticationScheme = optTest->AuthType;
   srtpCfgPtr->Decode.AuthenticationI8 =  optTest->AuthI8;
   srtpCfgPtr->Decode.MKI_I8 = optTest->MkiI8;
   srtpCfgPtr->Decode.KDR = optTest->Kdr;
   srtpCfgPtr->Decode.KeySizeU8 = 16;
	if(optTest->KeyScheme == KEY_MKI) {
      switch (optTest->MkiI8) {
      case 2: mask = 0xffff;     break;
      case 3: mask = 0xffffff;   break;
      case 4: mask = 0xffffffff; break;
      }
	  MkiValue = keyElement->mkiValue & mask;
	  srtpCfgPtr->Decode.MKI[0] = (ADT_UInt8)((MkiValue>>24) & 0xff);
	  srtpCfgPtr->Decode.MKI[1] = (ADT_UInt8)((MkiValue>>16) & 0xff);
	  srtpCfgPtr->Decode.MKI[2] = (ADT_UInt8)((MkiValue>>8) & 0xff);
	  srtpCfgPtr->Decode.MKI[3] = (ADT_UInt8)((MkiValue) & 0xff);
      srtpCfgPtr->Decode.MasterKeyLife[0] = 0;
      srtpCfgPtr->Decode.MasterKeyLife[1] = (keyElement->mkiLife >> 16) & 0xffff;
      srtpCfgPtr->Decode.MasterKeyLife[2] = keyElement->mkiLife & 0xffff;
	}

    mkey  = &srtpCfgPtr->Decode.MasterKey[0];
    msalt = &srtpCfgPtr->Decode.MasterSalt[0];
    lkey  = &keyElement->key[0];
    lsalt = &keyElement->key[2];

    for (i=0; i<14; i++) {
      *mkey++  = *lkey++;
      *msalt++ = *lsalt++;
    }
    *mkey++ = *lkey++;
    *mkey++ = *lkey++;

    srtpCfgPtr->Decode.ROC = keyElement->roc;
	srtpCfgPtr->FrameHalfMS = chanFrameSize[ChanId];
	SRTP_keyRingDecode.option[ChanId] = opt;
	SRTP_keyRingDecode.keyIndex[ChanId] = keyIndex;
}
// Callback handler when SRTP needs a new key.
static void gpakSendSrtpKey(ADT_UInt32 DspId, int channelId, GpakAsyncEventData_t *eventData) {
	GpakApiStatus_t srtpRet;
	GpakSrtpStatus_t    *dspStatus;
	GpakSRTPKey_t srtpKeyInfo;
    keySchedule *keyElement;
    optionParams *optTest;
    ADT_UInt32  mask = 0, mki = 0, *pMkiValue, MkiValue;
    ADT_UInt8  i, *mkey, *msalt, *lkey, *lsalt;
	int keyIndex;
	int opt;

	// get the record out of the storage
	if(eventData->Direction == DSPToNet) {
		opt = SRTP_keyRingEncode.option[channelId];
	}else {
		opt = SRTP_keyRingDecode.option[channelId];
	}

	optTest = &optTests[opt];

   // Initialize SRTP processing for channels

	srtpKeyInfo.Direction = eventData->Direction;
	srtpKeyInfo.MKI_I8 = eventData->aux.srtpEvent.MkiI8;
    srtpKeyInfo.KDR = optTest->Kdr;

	if(optTest->KeyScheme == KEY_MKI) {
      switch (optTest->MkiI8) {
      case 2: mask = 0xffff;     break;
      case 3: mask = 0xffffff;   break;
      case 4: mask = 0xffffffff; break;
      }
      // Find key that matches mki value
		if(eventData->Direction == DSPToNet) {
			// Serach the keyring for the current matached encoder key, then use the next key if there is any
			keyIndex = 0; //SRTP_keyRingEncode.keyIndex[chId];
			keyElement = &keySched[0];
			while (((keyElement->mkiValue & mask) != eventData->aux.srtpEvent.MkiValue) && (eventData->aux.srtpEvent.MkiValue != 0)) {
				if(keyIndex < NUM_KEY_ON_RING-1)  {
					// hasn't reached to the end of the key ring
					keyElement ++;
					keyIndex ++;
				}
				if(keyIndex == NUM_KEY_ON_RING-1)
					break;
			}
		   //if (eventData->aux.srtpEvent.seq) 
			if(keyIndex < NUM_KEY_ON_RING-1) {
				// hasn't reached to the end of the key ring
				keyElement++;
				keyIndex ++;
			}
			else {
				keyElement =&keySched[0];
				keyIndex =0;
			}
			SRTP_keyRingEncode.keyIndex[channelId] = keyIndex;
		} else {
			if(eventData->aux.srtpEvent.MkiValue == 0) {
				keyIndex = SRTP_keyRingDecode.keyIndex[channelId];
				keyElement = &keySched[keyIndex];
				// Lookahead for the decryption
				if(keyIndex < NUM_KEY_ON_RING-1) {
					// hasn't reached to the end of the key ring
					keyElement++;
					keyIndex ++;
				}
				else {
					keyElement =&keySched[0];
					keyIndex =0;
				}
			
			} else {
				keyIndex = 0;
				keyElement = &keySched[0];
				while ( (keyElement->mkiValue & mask) != eventData->aux.srtpEvent.MkiValue) {
					if(keyIndex < NUM_KEY_ON_RING-1)  {
						// hasn't reached to the end of the key ring
						keyElement ++;
						keyIndex ++;
					}
					if(keyIndex == NUM_KEY_ON_RING-1)
						break;
				}
				//if (eventData->aux.srtpEvent.seq) 
				if(keyIndex > NUM_KEY_ON_RING-1) {
					keyElement = &keySched[0];
					keyIndex =0;
					printf("Can't find the key\n");
					return;
				}
			}
			SRTP_keyRingDecode.keyIndex[channelId] = keyIndex;
		}

		//*pMkiValue = keyElement->mkiValue & mask;
		MkiValue = keyElement->mkiValue & mask;
		srtpKeyInfo.MKI[0] = (ADT_UInt8)((MkiValue>>24) & 0xff);
		srtpKeyInfo.MKI[1] = (ADT_UInt8)((MkiValue>>16) & 0xff);
		srtpKeyInfo.MKI[2] = (ADT_UInt8)((MkiValue>>8) & 0xff);
		srtpKeyInfo.MKI[3] = (ADT_UInt8)((MkiValue) & 0xff);


      srtpKeyInfo.MasterKeyLife[0] = 0;
      srtpKeyInfo.MasterKeyLife[1] = (keyElement->mkiLife >> 16) & 0xffff;
      srtpKeyInfo.MasterKeyLife[2] = keyElement->mkiLife & 0xffff;
	}


   mkey  = &srtpKeyInfo.MasterKey[0];
   msalt = &srtpKeyInfo.MasterSalt[0];
   lkey  = &keyElement->key[0];
   lsalt = &keyElement->key[2];
   

   for (i=0; i<14; i++) {
      *mkey++  = *lkey++;
      *msalt++ = *lsalt++;
   }
   *mkey++ = *lkey++;
   *mkey++ = *lkey++;

   
	srtpRet = gpakSRTPNewKey (DspId, PackCore(chanCore[channelId],channelId), &srtpKeyInfo, &dspStatus);

}

void getCustomSRTPParams(GpakSRTPCfg_t *ps) {
//ADT_UInt8  MasterKey[32]  = {0x34, 0x12, 0x45, 0x23, 0x56, 0x34, 0x67, 0x45, 0x78, 0x56, 0x89, 0x67, 0x9a, 0x78, 0xab, 0x89, 0x00, 0x00, 0x00 };
//ADT_UInt8  MasterSalt[14] = {0x45, 0x23, 0x56, 0x34, 0x67, 0x45, 0x78, 0x56, 0x89, 0x67, 0x9a, 0x78, 0xab, 0x89};

ADT_UInt8  MasterKey[32]  = {0x54, 0xb0, 0xce, 0x35, 0xbb, 0x1b, 0x1a, 0x5d, 0x8f, 0xba, 0xac, 0xbe, 0xd0, 0xf5, 0x7b, 0xc3};
ADT_UInt8 MasterSalt[14]  = {0xfb, 0x86, 0x7e, 0x94, 0x8f, 0x28, 0x57, 0xaf, 0x0e, 0x9c, 0x46, 0xb3, 0x13, 0x77};
    memset(ps, 0, sizeof(GpakSRTPCfg_t));
    ps->FrameHalfMS = Frame_20ms;
    ps->Encode.EncryptionScheme = GPAK_ENCRYPT_CM;
    ps->Encode.AuthenticationScheme = GPAK_AUTH_HMAC_SHA;
    ps->Encode.AuthenticationI8 = 10;
    memcpy(ps->Encode.MasterKey, MasterKey, 16);
    memcpy(ps->Encode.MasterSalt, MasterSalt,14);
    // NULL ps->Encode.MasterKeyLife[3];
    // NULL ps->Encode.MKI[4];
    // NUL ps->Encode.MKI_I8;
    ps->Encode.KDR = 25; 
    ps->Encode.KeySizeU8 = 16;

    ps->Decode.EncryptionScheme = GPAK_ENCRYPT_CM;
    ps->Decode.AuthenticationScheme = GPAK_AUTH_HMAC_SHA;
    ps->Decode.AuthenticationI8 = 10;
    memcpy(ps->Decode.MasterKey, MasterKey, 16);
    memcpy(ps->Decode.MasterSalt, MasterSalt,14);
    // NULL ps->Encode.MasterKeyLife[3];
    // NULL ps->Encode.MKI[4];
    // NUL ps->Encode.MKI_I8;
    ps->Decode.KDR = 25; 
    // NULL ps->Decode.ROC;
    ps->Decode.KeySizeU8 = 16;
}


#define SCAN_IS_SPACE(c) ((c)==' ' || (c)=='\t')
#define SCAN_IS_EOL(c) ((c)=='\r' || (c)=='\n')

void readIpParamsFromFile() {
char params[128], temp_ipv4[32], temp_ipv6[65], temp_destipv6[65];
char token[16];
char *str;
FILE *fp;
IP6N destAddr;
int i, n, found=0;


    fp = fopen("gpak_ip.txt","r");
    if (fp == NULL) return;
    memset(temp_ipv4, 0, sizeof(temp_ipv4));
    memset(temp_ipv6, 0, sizeof(temp_ipv6));
    memset(temp_destipv6, 0, sizeof(temp_ipv6));

    while (1)
    {
        str = fgets(params, sizeof(params), fp);
        if (str == NULL) break;
        while (SCAN_IS_SPACE(*str)) {
	        ++str;
        }
        if (*str == '#') {
           // skip line started with comment character '#'
           continue;
           //while (!SCAN_IS_EOL(*str)) {
           //     str++;
           //}   
        }

        memset(token, 0, sizeof(token));
        i = 0;
        while((*str != '=') && !SCAN_IS_EOL(*str)) {
            if (SCAN_IS_SPACE(*str))
                str++;
            else
                token[i++] = *str++;
        }
        if (*str == '=') 
        {
            token[i] = 0;
            str++;
            while (SCAN_IS_SPACE(*str)) {
	            ++str;
            }
            if (strcmp(token, "tcpver") == 0) {
                if (*str == '4')      tcpIpVersion = 4;
                else if (*str == '6') tcpIpVersion = 6; 
                found |= 1;
            }
            
            else if (strcmp(token, "rtpver") == 0) {
                if (*str == '4')      rtpIpVersion = 4;
                else if (*str == '6') rtpIpVersion = 6; 
                found |= 2;
            }

            else if (strcmp(token, "dspsrv") == 0) {
                n = strlen(str);
                if (n > 22) str[22] = 0;
                SetDlgItemText(hDlg, IDC_EDIT_MACADDRESS, str);
                found |= 4;
            }

            else if (strcmp(token, "dspip4") == 0) {
                n = strlen(str);
                if (n <= sizeof(temp_ipv4)) {
                    for (i=0; i<n; i++) {
                        if (!SCAN_IS_EOL(str[i]))
                            temp_ipv4[i] = str[i];
                    }
                    found |= 8;
                }
            }

            else if (strcmp(token, "dspip6") == 0) {
                n = strlen(str);
                if (n <= sizeof(temp_ipv6)) {
                    for (i=0; i<n; i++) {
                        if (!SCAN_IS_EOL(str[i]))
                            temp_ipv6[i] = str[i];
                    }
                    found |= 16;
                }                  
            }

            else if (strcmp(token, "destip6") == 0) {
                n = strlen(str);
                if (n <= sizeof(temp_destipv6)) {
                    for (i=0; i<n; i++) {
                        if (!SCAN_IS_EOL(str[i]))
                            temp_destipv6[i] = str[i];
                    }
                    if (IPv6StringToIPAddress (temp_destipv6, &destAddr) == 0) {
                        memcpy(&Defaut_Dest_IpV6, &destAddr.u.addr8, 16);
                    }
                }                  
            }
        }            
    }
    if ((found & 4) == 0)
        SetDlgItemText(hDlg, IDC_EDIT_MACADDRESS, GPAKSERVER);

    if ((found & 16) && (tcpIpVersion == 6))   
        SetDlgItemText(hDlg, IDC_EDIT_IPADDRS, temp_ipv6);
    else if ((found & 8) && (tcpIpVersion == 4))    
        SetDlgItemText(hDlg, IDC_EDIT_IPADDRS, temp_ipv4);

    if (fp)
        fclose(fp);
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// WinMain - Main Windows function.
//
// FUNCTION
//  This function is the main Windows function for the application.
//
// RETURNS
//  nothing
//
int WINAPI _tWinMain(
	HINSTANCE hInst,
	HINSTANCE h0,
	LPTSTR lpCmdLine,
	int nCmdShow
	)
{
	MSG msg;
	BOOL ret;
	int i;						// loop index / counter

	// Save the app's instance Id.
	thisAppInstance = hInst;

	// Initialize the common controls.
	InitCommonControls();

	// Create the main dialog procedure.
	hDlg = CreateDialogParam(hInst, MAKEINTRESOURCE(IDD_DIALOG1), 0,
							 mainDialogProc, 0);
	ShowWindow(hDlg, nCmdShow);

    readIpParamsFromFile();					

#ifdef CUSTOM_COMMANDS
    initCustomCommand(MIN_CHAN_CORE, MAX_CHAN_CORE);
#endif

	// Start a timer for periodic display.
#ifdef PERIODIC_TIMER
	SetTimer(hDlg, 999, 1000, NULL);
#endif

	// Prepare G.PAK host custom API structures.
	initGpakStructures();

	// Initialize Windows Sockets.
	if (WSAStartup(MAKEWORD(2,2), &wsaData) != 0)
	{
		return -1;
	}

	// Initialize network related variables.
	networkReady = FALSE;
    memset(&dspIpAddress6, 0, sizeof(IP6N));
    dspIpAddress = 0;

	// Initialize user input and output related variables.
	userInLength = 0;
	userOutLength = 0;

	// Initialize channel configuration related variables.
	for (i = 0; i < NUM_TEST_CHANS; i++)
	{
		chanMultiCast[i] = 0;
		portCrossover[i] = 0;
		chanCodecType[i] = PCMU_64;
		chanFrameSize[i] = Frame_20ms;
        chanCore[i]      = MIN_CHAN_CORE;

        if ((NUM_TDM_SLOTS == 2) || (NUM_TDM_SLOTS >= 128)) {
            chanInSlotA[i]  = (uint16_t) ((i) % NUM_TDM_SLOTS);
			chanOutSlotA[i] = (uint16_t) ((i) % NUM_TDM_SLOTS);
			chanInSlotB[i]  = (uint16_t) ((i+1) % NUM_TDM_SLOTS);;
			chanOutSlotB[i] = (uint16_t) ((i+1) % NUM_TDM_SLOTS);;
        } else {
		    if(i < 16) {
			    chanInSlotA[i] = (uint16_t) ((2*i) % NUM_TDM_SLOTS);
			    chanInSlotB[i] = (uint16_t) ((2*i + 1) % NUM_TDM_SLOTS); 
			    chanOutSlotA[i] = (uint16_t) (2*i % NUM_TDM_SLOTS);
			    chanOutSlotB[i] = (uint16_t) ((2*i + 1) % NUM_TDM_SLOTS);
    		} else {
        		chanInSlotA[i] = (uint16_t) ((2*(i-16)+1) % NUM_TDM_SLOTS);
    			chanInSlotB[i] = (uint16_t) ((2*(i-16)) % NUM_TDM_SLOTS); 
    			chanOutSlotA[i] = (uint16_t) ((2*(i-16)+1) % NUM_TDM_SLOTS);
    			chanOutSlotB[i] = (uint16_t) ((2*(i-16) ) % NUM_TDM_SLOTS);
    		}
        }
		chanInGainA[i] = 0;
		chanInGainB[i] = 0;
		chanOutGainA[i] = 0;
		chanOutGainB[i] = 0;
		chanAecEnable[i] = Disabled;
		chanLecEnable[i] = Disabled;
		chanAgcEnable[i] = Disabled;
		chanDestRtpIp[i] = DEFAULT_DEST_IP;
		memcpy(&chanDestRtpV6Ip[i].IpV6Address, &Defaut_Dest_IpV6,  sizeof(GpakIPv6_t));
		chanDestRtpPort[i] = 6334 + (i * 2);
        chanSourceRtpPort[i] = 6334 + (i * 2);
		chanJitrBufrSize[i] = 80;  // 80 ms
		rtpTxPktCnt[i] = 0;
		rtpRxPktCnt[i] = 0;
        chanDetectA[i] = 0; // A-side tone detectors
        chanDetectB[i] = 0; // B-side tone detectors
        chanCryptoSuite[i] = 0;
        VlanIdx[i] = VLAN_NULL_IDX;

        chanRtcpEnable[i] = Disabled;
        chanVadCngEnable[i] = Disabled;
        chanDtxEnable[i] = Disabled;
        chanDataEnable[i] = Disabled;
        chanIpVersion[i] = rtpIpVersion;
        dtmfMode[i] = 0;
        chanFaxMode[i] = disabled;
        chanCIDMode[i] = 0;
        stressStatInit(i);
	}
    for (i=0; i<NUM_CONFS; i++) {
        confReady[i] = 0;
        confCore[i]  = MIN_CHAN_CORE;   // all conferences default to core 0
        confFrameSize[i] = Frame_20ms;
    }
    customChannelConfig = formatCustChannelConfig;
    customChannelStatusParse = parseCustChannelStatus;

	// App's message loop.
//	while (1) {

	while ((ret = GetMessage(&msg, 0, 0, 0)) != 0)
	{
		if (ret == -1)
		{
			return -1;
		}
        //getEvent();
		if (!IsDialogMessage(hDlg, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
//	}
	return 0;
}

void resetChanParms() {
int i;
    for (i=0; i<NUM_TEST_CHANS; i++) {
        chanCryptoSuite[i] = 0;
		chanLecEnable[i] = Disabled;
        chanRtcpEnable[i] = Disabled;
        chanVadCngEnable[i] = Disabled;
        chanDtxEnable[i] = Disabled;
        chanDataEnable[i] = Disabled;
        dtmfMode[i] = 0;
        chanFaxMode[i] = disabled;
		chanCodecType[i] = PCMU_64;
		chanFrameSize[i] = Frame_20ms;
    }
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// mainDialogProc - Main dialog box procedure.
//
// FUNCTION
//  This function is the app's main dialog box procedure.
//
// RETURNS
//  True if message was processed or False if not processed.
//
static int exitFlag = FALSE;
unsigned int timer_count=0;

static INT_PTR CALLBACK mainDialogProc(
	HWND hDlg,
	UINT uMsg,
	WPARAM wParam,
	LPARAM lParam
	)
{
	UINT textLength;			// text length
	char textBufr[128];			// text buffer
	unsigned int arg1;			// argument 1
	unsigned int arg2;			// argument 2
	unsigned int arg3;			// argument 3
	unsigned int arg4;			// argument 4

	switch (uMsg)
	{

	// Periodic timer.
	case WM_TIMER:
#ifdef PERIODIC_TIMER
        timer_count++;
        if ((timer_count % CUSTOM_PERIODIC_SECS) != 0) break;
#ifdef CUSTOM_COMMANDS
        if (getCustPeriodicActive()) {
		    userOutBufr[0] = '\0';
            userOutIndex = 0;
            custPeriodicFunc(timer_count);
            if (userOutIndex) { 
		        userOutBufr[userOutIndex] = '\0';
		        userOutLength = userOutIndex;
			    SetDlgItemText(hDlg, IDC_EDIT_OUTPUT, userOutBufr);
			    userOutLength = 0;
            }
        }
#endif
#endif
		break;

	// Control notifications.
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{

		// Execute button.
		case IDC_BUTTON_EXEC:

			// Get the DSP's IP address.
            memset(textBufr, 0, sizeof(textBufr));
			textLength = GetDlgItemText(hDlg, IDC_EDIT_IPADDRS, textBufr,
										sizeof(textBufr));
			if (textLength != 0)
			{
                if (tcpIpVersion == 4) 
                {
				    if ((sscanf(textBufr, "%u.%u.%u.%u",
							&arg1, &arg2, &arg3, &arg4) == 4) &&
					    (arg1 <= 255) && (arg2 <= 255) && (arg3 <= 255) &&
					    (arg4 <= 255))
				    {
					    dspIpAddress =
						    (arg1 << 24) | (arg2 << 16) | (arg3 << 8) | arg4;
				    }
				    else
				    {
					    dspIpAddress = 0;
				    }   
                } 
                else 
                {
                    
                    if (IPv6StringToIPAddress (textBufr, &dspIpAddress6)) {
                        memset(&dspIpAddress6, 0, sizeof(IP6N));
                    }
                }
			}

			// Get the DSP's MAC address.
			textLength = GetDlgItemText(hDlg, IDC_EDIT_MACADDRESS, textBufr,
										sizeof(textBufr));
			if ( (textLength == 22) &&
                 (textBufr[0] = 'G') && 
                 (textBufr[1] = 'p') &&
                 (textBufr[2] = 'a') &&
                 (textBufr[3] = 'k') &&
                 (textBufr[4] = '.') &&
                 (textBufr[7] = '.') &&
                 (textBufr[10] = '.') &&
                 (textBufr[13] = '.') &&
                 (textBufr[16] = '.') &&
                 (textBufr[19] = '.') )
            {
                memcpy(GpakServer, textBufr, 22);
			} else {
			    SendDlgItemMessage(hDlg, IDC_EDIT_OUTPUT, WM_CLEAR, 0, 0);
			    SetDlgItemText(hDlg, IDC_EDIT_OUTPUT, "Invalid GPAKSERVER address");
                return TRUE;
            }

			if (userInLength == 0)
			{
				if (userOutLength != 0)
				{
//					SendDlgItemMessage(hDlg, IDC_EDIT_OUTPUT, WM_CLEAR, 0, 0);
					SetDlgItemText(hDlg, IDC_EDIT_OUTPUT, userOutBufr);
					userOutLength = 0;
				}
				userInLength = GetDlgItemText(hDlg, IDC_EDIT_CMD, userInBufr,
											  sizeof(userInBufr));
				if (userInLength != 0)
				{
					gpakThread();
					if (userOutLength != 0)
					{
//						SendDlgItemMessage(hDlg, IDC_EDIT_OUTPUT, WM_CLEAR, 0,
//										   0);
						SetDlgItemText(hDlg, IDC_EDIT_OUTPUT, userOutBufr);
						userOutLength = 0;
					}
				}
			}
			return TRUE;

		// Exit button.
		case IDCANCEL:
			SendMessage(hDlg, WM_CLOSE, 0, 0);
			return TRUE;
		}
		break;

	case WM_CLOSE:

		// Verify program exit.
		if (MessageBox(hDlg, "Are you sure?", "Exit",
					   MB_ICONQUESTION | MB_YESNO) == IDYES)
		{
			exitFlag = TRUE;
			DestroyWindow(hDlg);
		}
		return TRUE;

	case WM_DESTROY:

		// Disconnect communication with the DSP if needed.
		if (networkReady)
		{
			gpakServerDisconnect(0);
		}

		PostQuitMessage(0);
		return TRUE;
	}

	return FALSE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// gpakThread - G.PAK interface thread.
//
// FUNCTION
//  This function is the G.PAK interface thread.
//
// RETURNS
//  nothing
//
static void gpakThread(void)
{

	if (userInLength != 0)
	{
		userOutLength = 0;
		userOutBufr[0] = '\0';
		userOutIndex = 0;
        eventCounts = 0;
#ifdef CUSTOM_COMMANDS
        if (getCustMenuState() != 0)
            procCustCommand(userInBufr);
        else 
#endif
		    procUserCommand(userInBufr);
		userInLength = 0;
		userOutBufr[userOutIndex] = '\0';
		userOutLength = userOutIndex;
	}

	return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// uartPrintf - Perform 'printf' to the serial port.
//
// FUNCTION
//  This function performs a 'printf' to the serial port.
//
// RETURNS
//  The number of characters output.
//
int uartPrintf(char *format, ...)
{
	va_list arguments;			// list of arguments
	int numChars;				// number of characters
	char *pSource;				// pointer to source string
	char *pDest;				// pointer to destination string
	char c;						// temporary character

	// Verify the format argument exists.
	if (format == NULL)
	{
		return 0;
	}

	// Format the output to a temporary buffer.
	va_start(arguments, format);
	numChars = vsprintf(uartPrintBufr, format, arguments);
	va_end(arguments);

    if (userOutIndex >= USER_OUT_BUFLEN) {
        userOutBufr[USER_OUT_BUFLEN-9] = '\n';
        userOutBufr[USER_OUT_BUFLEN-8] = '\r';
        userOutBufr[USER_OUT_BUFLEN-7] = 'O';
        userOutBufr[USER_OUT_BUFLEN-6] = 'V';
        userOutBufr[USER_OUT_BUFLEN-5] = 'F';
        userOutBufr[USER_OUT_BUFLEN-4] = 'L';
        userOutBufr[USER_OUT_BUFLEN-3] = '\n';
        userOutBufr[USER_OUT_BUFLEN-2] = '\r';
        userOutBufr[USER_OUT_BUFLEN-1] = 0;
        return numChars;    
    }
	// Output the buffer to the serial port.
	if (numChars > 0)
	{
		pSource = uartPrintBufr;
		pDest = &(userOutBufr[userOutIndex]);
		while ((c = *pSource++) != '\0')
		{
            if (userOutIndex >= USER_OUT_BUFLEN) {
                userOutBufr[USER_OUT_BUFLEN-9] = '\n';
                userOutBufr[USER_OUT_BUFLEN-8] = '\r';
                userOutBufr[USER_OUT_BUFLEN-7] = 'O';
                userOutBufr[USER_OUT_BUFLEN-6] = 'V';
                userOutBufr[USER_OUT_BUFLEN-5] = 'F';
                userOutBufr[USER_OUT_BUFLEN-4] = 'L';
                userOutBufr[USER_OUT_BUFLEN-3] = '\n';
                userOutBufr[USER_OUT_BUFLEN-2] = '\r';
                userOutBufr[USER_OUT_BUFLEN-1] = 0;
                break;    
            }

			if (c == '\n')
			{
				*pDest++ = '\r';
				userOutIndex++;
			}
			*pDest++ = c;
			userOutIndex++;
		}
	}

	return numChars;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// strncmpi - Case insensitive compare strings of maximum length.
//
// FUNCTION
//  This function performs a case insensitive compare of two strings of maximum
//  length.
//
// RETURNS
//  Zero if the strings are equal or non zero if not equal.
//
static int strncmpi(
	char *pString1,				// pointer to string 1
	char *pString2,				// pointer to string 2
	uint32_t length				// max number of characters to compare
	)
{

	while (length-- > 0)
	{
		if (toupper(*pString1++) != toupper(*pString2++))
		{
			return 1;
		}
	}
	return 0;
}
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// printWarning - Print (display) a warning event.
//
// FUNCTION
//  This function prints a warning event.
//
// RETURNS
//  nothing
//
static void printWarning(
	GpakAsyncEventData_t *pEventData,	// pointer to event data
	ADT_UInt16 warningValue				// warning value
	)
{
	static int txSlipCnt = 0;		// Tx Slip count
	static int rxSlipCnt = 0;		// Rx Slip count
	static int frameWarnCnt = 0;	// Frame Slip count

	switch (pEventData->ToneDevice)
	{
	case WarnTxDmaSlip:       
		if ((txSlipCnt++ % 50) == 0)
		{
			uartPrintf("%u Tx TDM slips on Port %u\n",
					   txSlipCnt, warningValue % 100);
		}
		break;
	case WarnRxDmaSlip:
		if ((rxSlipCnt++ % 50) == 0)
		{
			uartPrintf("%u Rx TDM slips on Port %u\n",
					   rxSlipCnt, warningValue % 100);
		}
		break;
	case WarnFrameSyncError:
		if ((frameWarnCnt++ % 50) == 0)
		{
			uartPrintf("TDM frame sync error on Port %u\n",
					   warningValue);
		}
		break;
	case WarnFrameTiming:
		uartPrintf("%u ms Framing task MIPS overload\n", warningValue & 0xff);
		break;
	case WarnFrameBuffers:
		uartPrintf("%u ms Framing task circular buffer reset\n",
				   warningValue & 0xff);
		break;
	case WarnPortRestart:
		uartPrintf("TDM port restart\n");
		break;
	case WarnTDMFailure:
		uartPrintf("TDM port %u failure\n", warningValue);
		break;
	case WarnApplicationError:
		uartPrintf("App Error: %s\n", pEventData->aux.customPayload);
		break;
	default:
		uartPrintf("Code %u\n", pEventData->ToneDevice);
		break;
	}

	return;
}

const char *dtmfString[16] = {
    "DTMF 1",
    "DTMF 2",
    "DTMF 3",
    "DTMF A",
    "DTMF 4",
    "DTMF 5",
    "DTMF 6",
    "DTMF B",
    "DTMF 7",
    "DTMF 8",
    "DTMF 9",
    "DTMF C",
    "DTMF *",
    "DTMF 0",
    "DTMF #",
    "DTMF D"};

typedef enum CallerIdMsgTypes_e {
   CID_sdmf = 0x04,         // Single Data Message Format(SDMF)
   CID_sdmf_wait_ind = 0x06,// SDMF MWI
   CID_sdmf_desk_info = 0x0B,// SDMF (Reserved for Message Desk Information)
   CID_mdmf = 0x80,         // Multiple data message format
   CID_mdmf_test = 0x81,     // MDMF (test sequence)
   CID_msg_wait_ind = 0x82,  // MDMF Message Waiting indicator(MWI)
   CID_msg_advice = 0x86    // MDMF Advise
} CallerIdMsgTypes_e;
static void CIDNotify (ADT_UInt8 *data) { 
	//ADT_UInt8 *data;
	//data = cid->Data;
	int fieldI8, msgI8;
	unsigned int type=0, param=0;

	type  = *data++;
	msgI8 = *data++;
	//uartPrintf("CIDNotify gateway chan: %d, pkt chan id: %d",cid->nCID, cid->ChanId);
	uartPrintf(" MessageType:%d MessageLength=%d\n", type, msgI8);
	if (type == CID_sdmf) {
		uartPrintf(" pre-defined SDMF\n");
		fieldI8 = msgI8;
	} else if (type == CID_mdmf) {
		param    = *data++;
		fieldI8 = *data++;
		uartPrintf(" pre-defined MDMF\n");
	} else if (type == CID_msg_wait_ind) {
		uartPrintf(" pre-defined MWI\n");
		param    = *data++;
		fieldI8 = *data++;
    } else if (type & 0x80) { // Cover all MDMF 0x80/81/82/86
        uartPrintf(" user-defined MDMF\n");
        param    = *data++;
        fieldI8 = *data++;
	} else {
		uartPrintf(" User Defined\n");
		fieldI8 = msgI8;
	}

	do {
		if(type & 0x80) // MDMF msg type
			uartPrintf(" ParamType: %d ParamLength: %d\n", param, fieldI8);
		msgI8 -= (fieldI8 + 2);
		while (fieldI8--) {
			if (isprint (*data)) 
				uartPrintf ("%c", *data++);
			else                 
				uartPrintf ("[%x]", *data++);
		}
		uartPrintf (" ");
		param    = *data++;
		fieldI8 = *data++;
	} while (0 < msgI8);
	uartPrintf("\n");
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// procDspEvent - Process an event from the DSP.
//
// FUNCTION
//  This function processes an event from the DSP.
//
// RETURNS
//  nothing
//
static void procDspEvent(
	ADT_UInt16 channelId,				// channel Id
	GpakAsyncEventCode_t eventCode,		// event code
	GpakAsyncEventData_t *pEventData	// pointer to event data
	)
{

#if 1
    int i;
	char side = 'A';					// channel's side Id
    scustomED137B_PTT_Event_t *PTT_ptr;
	scustomED137B_PersistentFeatureEvent_t *PF_ptr;

	// Obtain exclusive use of the serial port.
	//uartLock();

	// Display a warning.
	if (eventCode == EventWarning)
	{
		uartPrintf("Warn: ");
		printWarning(pEventData, channelId);
	}

	// Display the event.
	else
	{
       if (pEventData->ToneDevice != 0)		
	        side = 'B';

		uartPrintf("Event: ");
		switch (eventCode)
		{
		case EventDspReady:
			uartPrintf("Core %u Ready\n", channelId);
			break;
		case EventToneDetect:
			if ((pEventData->aux.toneEvent.ToneCode >= 0) && (pEventData->aux.toneEvent.ToneCode <= 15))
				uartPrintf("Chan %u Tone %s detected for %d ms on side %c\n",
					   channelId, dtmfString[pEventData->aux.toneEvent.ToneCode],
					   pEventData->aux.toneEvent.ToneDuration, side);
			else {
				uartPrintf("Chan %u Tone %u detected for %d ms on side %c\n",
					   channelId, pEventData->aux.toneEvent.ToneCode,
					   pEventData->aux.toneEvent.ToneDuration, side);
               if (pEventData->aux.toneEvent.ToneCode == 100) uartPrintf ("\n");
			}
			break;
        case EventPlaybackComplete:     uartPrintf ("\nPlayback complete channel %d side %c\n", channelId, side);  break;
        case EventRecordingStopped:     uartPrintf ("\nRecording stopped channel %d side %c\n",channelId, side);  break;
        case EventRecordingBufferFull:  uartPrintf ("\nRecording buffer full channel %d side %c\n",channelId, side);  break;
		case EventVadStateVoice:
			uartPrintf("Chan %u Voice detected side %c\n", channelId, side);
			break;
		case EventVadStateSilence:
			uartPrintf("Chan %u Silence detected side %c\n", channelId, side);
			break;
		case EventNeedSRTPMasterKey:
			uartPrintf("Chan %u SRTP Master Key needed\n", channelId);
 			
 			if(pEventData->aux.srtpEvent.MkiValue){
				uartPrintf ("SRTP ch %d dir %d Roc=%ld, seq=%d, MkiValue=%ld, MkiI8=%d \n",
													channelId,
													pEventData->Direction,
													pEventData->aux.srtpEvent.roc, 
													pEventData->aux.srtpEvent.seq, 
													pEventData->aux.srtpEvent.MkiValue, 
													pEventData->aux.srtpEvent.MkiI8 ); 
			}
			
			num_Of_NewKeys++;
			gpakSendSrtpKey(0, channelId, pEventData);

			if(num_Of_NewKeys %100 == 0) {
				uartPrintf ("SRTP send new key %d times \n", num_Of_NewKeys);
				//printf (prompt);
			}
			return;
			break;
		case EventTxCidMessageComplete: // = 10,     // Cid Tx message complete event
 			uartPrintf("Chan %u TxCid MessageComplete\n", channelId);
			break;
		case EventRxCidCarrierLock: // = 11,         // Cid Rx carrier lock event
 			uartPrintf("Chan %u RxCid EventRxCidCarrierLock\n", channelId);
			break;
		case  EventRxCidMessageComplete: 
 			uartPrintf("Chan %u RxCid MessageComplete\n", channelId);
            //cidInfo->Data = EvtData.aux.rxCidEvent.Payload;
            CIDNotify (pEventData->aux.rxCidEvent.Payload);
            break;
		case EventRtpRx_Timeout:
            uartPrintf("Chan %u RTP TX Timeout \n", channelId);
			break;
		case EventCoreHealth:
            uartPrintf("Core %u Health Event \n", channelId);
			break;
		case customED137B_PTT_Event:
			PTT_ptr = pEventData;
			uartPrintf("Chan %u code %d\n", channelId, eventCode);
			uartPrintf(" ds %x\n" ,PTT_ptr->Devside);
			uartPrintf(" pttx %x\n" ,PTT_ptr->PTTx);
			break;
        case customED137B_PersistentFeatureEvent:
			PF_ptr = pEventData;
			uartPrintf("Chan %u code %d\n", channelId, eventCode);
			uartPrintf(" ds %x\n" ,PF_ptr->Devside);
			uartPrintf(" pf Type   %x\n" ,PF_ptr->TYPE);
			uartPrintf(" pf Value  %x\n" ,PF_ptr->PF);
			uartPrintf(" PT        %d\n" ,PF_ptr->RTP_PAYLOAD_TYPE);
			break;
        default:
			uartPrintf("Chan %u code %d\n", channelId, eventCode);
		}
	}

	// Release exclusive use of the serial port.
	//uartUnlock();

#endif

	return;
}

static void getEvent (void) {
   ADT_Int16  chId;
   gpakReadEventFIFOMessageStat_t evtStat;
   GpakAsyncEventCode_t           EvtCode;
   GpakAsyncEventData_t           EvtData;

#ifndef SKIP_EVENTS
   while (1) {
      evtStat = gpakReadEventFIFOMessage (0, &chId, &EvtCode, &EvtData);
      if (evtStat != RefEventAvail) break;
      procDspEvent (chId, EvtCode, &EvtData);
   }

   uartPrintf ("Polling thread ended\n");
#endif

}

#define NUM_WAVE_FILES 4
ADT_UInt16 playbackId = 0;
typedef struct waveInfo_t {
    const char *file;
    int        playbackId;
    int        loaded;
} waveInfo_t;

waveInfo_t waveInfo[NUM_WAVE_FILES] = {
    {"..\\VectGel\\dtmfstring_PCM.wav",         -1, 0},
    {"..\\VectGel\\leaves_noise_8k_mono.wav",   -1, 0},
    {"..\\VectGel\\f2_f1_m1_8k_10sec_ULAW.wav", -1, 0},
    {"..\\VectGel\\Atoms_8k_10sec_ALAW.wav",    -1, 0}};


static void loadWav(const char *typeString) {
FILE *wavfp;
GpakApiStatus_t status;
WaveReadStatus_t waveFileStatus;
GPAK_LoadWavefileParmsStat_t dspStatus;
int32_t index;

    if (typeString == NULL) {
        for (index=0; index<NUM_WAVE_FILES; index++) {
            uartPrintf("%d)\t%s, playbackId %d, loaded %d\n",index, waveInfo[index].file, waveInfo[index].playbackId, waveInfo[index].loaded);
        }
        return;
    }
    if ((sscanf(typeString, "%u", &index) != 1) || (index >= NUM_WAVE_FILES)) {
        uartPrintf("Error: unsupported file\n");
        for (index=0; index<NUM_WAVE_FILES; index++) {
            uartPrintf("%d)\t%s, playbackId %d, loaded %d\n",index, waveInfo[index].file, waveInfo[index].playbackId, waveInfo[index].loaded);
        }
        return;
    }

    if (waveInfo[index].loaded) {
        uartPrintf ("Wavefile %s already loaded, playbackId = %d\n", waveInfo[index].file, waveInfo[index].playbackId);
        return;
    }

    wavfp = fopen(waveInfo[index].file,"rb");
    if (wavfp == NULL) {
        uartPrintf ("Error opening %s for download\n", waveInfo[index].file);
        return;
    }

    status = gpakLoadWavefile (0, wavfp, playbackId, &waveFileStatus, &dspStatus); 
    if ((status != GpakApiSuccess) || (waveFileStatus != WAVREAD_SUCCESS) || 
           (dspStatus != Lw_Success )) 
    {
        uartPrintf ("Error loading %s: %d, %d, %d \n",waveInfo[index].file, status, waveFileStatus, dspStatus);
    } else {
        waveInfo[index].loaded = 1;
        waveInfo[index].playbackId = playbackId;
        uartPrintf("Wavefile %s, download Success.\n PlaybackId = %d\n",waveInfo[index].file, waveInfo[index].playbackId);
        playbackId++;
    }
    fclose(wavfp);

    return;
}
void playWav(ADT_UInt16 channelId,  ADT_UInt16 playbackID, WavePlaybackCmd command, GpakDeviceSide_t direction) {
GpakApiStatus_t status; 
GPAK_PlayWavefileStat_t dspStatus; 
const char *dirString, *cmdString;
int i, found=0;

    for (i=0; i<NUM_WAVE_FILES; i++) {
        if (waveInfo[i].playbackId == playbackID) {
            found = 1;
            break;
        }        
    }
    if (!found) {
        uartPrintf ("Error: invalid playbackID\n");
        return;
    }

    if (direction == DSPToNet) dirString = "ToNet";
    else  dirString = "ToTDM";

    if (command == WavePlaybackStop) cmdString = "PLAYSTOP";   
    else if (command == WavePlaybackOnce) cmdString = "PLAYONCE"; 
    else cmdString = "PLAYLOOP";

    status = gpakPlayWavefile (0, PackCore(chanCore[channelId],channelId), playbackID, command, direction, &dspStatus);
    if ((status != GpakApiSuccess) || (dspStatus != Pw_Success))
        uartPrintf("Error: channelId %d, playbackId %d, %s, %s: %d %d\n", channelId, playbackID, cmdString, dirString, status, dspStatus);
    else { 
        uartPrintf("Success: channelId %d, playbackId %d, %s, %s\n", channelId, playbackID, cmdString, dirString);
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// procUserCommand - Process a user command.
//
// FUNCTION
//  This function processes a user command.
//
// RETURNS
//  nothing
//
static void procUserCommand(
	char *pUserCmd				// pointer to user command string
	)
{
	int32_t suffix1;			// 1st command suffix value
	int32_t suffix2;			// 2nd command suffix value
	char *pArgument;			// pointer to command argument
	uint32_t i;					// loop index / counter
	int32_t temp32;				// temporary 32 bit signed value
    void *ipAddr;
    int stat;

	// Attempt to initialize network communication with the DSP if needed.
	if (!networkReady)
	{
		if (isDspIpAddressNonZero())
		{
			if (gpakServerInit(tcpIpVersion) == 0)
			{
                if (tcpIpVersion == 4)
                    ipAddr = (void *)&dspIpAddress;
                else   
                    ipAddr = (void *)&dspIpAddress6;

				if (gpakServerConnect(0, GpakServer, ipAddr))
				{
					networkReady = TRUE;
					threadInit ();
				}
				else
				{
					uartPrintf("* gpakServerConnect failed!\n");
				}
			}
			else
			{
				uartPrintf("* gpakServerInit failed!\n");
			}
		}
		else
		{
			uartPrintf("* Enter DSP's IP address!\n");
		}
	}

	// Check for Help command.
	if ((strcmpi(pUserCmd, "HELP") == 0) || (strcmpi(pUserCmd, "?") == 0))
	{
		displayHelp();
		return;
	}

#ifdef CUSTOM_COMMANDS
	if (strcmpi(pUserCmd, "CM") == 0)
	{
        setCustMenuState(1);
		return;
	}
#endif

	// Check for Codec Type command.
	if (parseCommand(pUserCmd, "CODEC", &suffix1, &suffix2, &pArgument))
	{
		procCodecType(suffix1, suffix2, pArgument,0);
		return;
	}
	// Check for Codec Type command.
	if (parseCommand(pUserCmd, "CODECI", &suffix1, &suffix2, &pArgument))
	{
		procCodecType(suffix1, suffix2, pArgument,1);
		return;
	}
    // channel assign to specified Core
	if (parseCommand(pUserCmd, "CORE", &suffix1, &suffix2, &pArgument))
	{
		procCoreNum(suffix1, suffix2, pArgument,'c');
		return;
	}

    // channel assign to N-cores by interleave
	if (parseCommand(pUserCmd, "COREI", &suffix1, &suffix2, &pArgument))
	{
		procCoreNum(suffix1, suffix2, pArgument,'i');
		return;
	}

    // channel assign to N-cores by block group
	if (parseCommand(pUserCmd, "COREB", &suffix1, &suffix2, &pArgument))
	{
		procCoreNum(suffix1, suffix2, pArgument,'b');
		return;
	}


	// Check for Frame Size command.
	if (parseCommand(pUserCmd, "FRAME", &suffix1, &suffix2, &pArgument))
	{
		procFrameSize(suffix1, suffix2, pArgument);
		return;
	}

	// frame interleave: assign assign channels to frame rates suffix1...suffix2
	if (parseCommand(pUserCmd, "FRAMEI", &suffix1, &suffix2, &pArgument))
	{
		procFrameSizeInterleaved(suffix1, suffix2, pArgument);
		return;
	}

	// Check for Multi-cast Enable command.
	if (parseCommand(pUserCmd, "MCAST", &suffix1, &suffix2, &pArgument))
	{
		procMCast(suffix1, suffix2, pArgument);
		return;
	}
	// Check for Crossover Enable command.
	if (parseCommand(pUserCmd, "PASS", &suffix1, &suffix2, &pArgument))
	{
		procCrossover(suffix1, suffix2, pArgument);
		return;
	}
	// Check for A Side Input Slot command.
	if (parseCommand(pUserCmd, "AINSLOT", &suffix1, &suffix2, &pArgument))
	{
		procTdmSlot(TRUE, ADevice, suffix1, suffix2, pArgument);
		return;
	}

	// Check for B Side Input Slot command.
	if (parseCommand(pUserCmd, "BINSLOT", &suffix1, &suffix2, &pArgument))
	{
		procTdmSlot(TRUE, BDevice, suffix1, suffix2, pArgument);
		return;
	}

	// Check for A Side Output Slot command.
	if (parseCommand(pUserCmd, "AOUTSLOT", &suffix1, &suffix2, &pArgument))
	{
		procTdmSlot(FALSE, ADevice, suffix1, suffix2, pArgument);
		return;
	}

	// Check for B Side Output Slot command.
	if (parseCommand(pUserCmd, "BOUTSLOT", &suffix1, &suffix2, &pArgument))
	{
		procTdmSlot(FALSE, BDevice, suffix1, suffix2, pArgument);
		return;
	}

	if (parseCommand(pUserCmd, "ASLOTCROSS", &suffix1, &suffix2, &pArgument))
	{
		if (pArgument == NULL) {
			uartPrintf("Slot Cross set to %u\n", slotCross);
            return;
        }
        if ((sscanf(pArgument, "%u", &temp32) == 1)) {
            if (temp32) slotCross = 1;
            else        slotCross = 0;
        }
        uartPrintf("Slot Cross set to %u\n", slotCross);
	}
	// Check for A Side Input Gain command.
	if (parseCommand(pUserCmd, "AINGAIN", &suffix1, &suffix2, &pArgument))
	{
		procChannelIoGain(TRUE, ADevice, 0, suffix1, suffix2, pArgument);
		return;
	}

	// Check for B Side Input Gain command.
	if (parseCommand(pUserCmd, "BINGAIN", &suffix1, &suffix2, &pArgument))
	{
		procChannelIoGain(TRUE, BDevice, 0, suffix1, suffix2, pArgument);
		return;
	}

	// Check for A Side Output Gain command.
	if (parseCommand(pUserCmd, "AOUTGAIN", &suffix1, &suffix2, &pArgument))
	{
		procChannelIoGain(FALSE, ADevice, 0, suffix1, suffix2, pArgument);
		return;
	}

	// Check for B Side Output Gain command.
	if (parseCommand(pUserCmd, "BOUTGAIN", &suffix1, &suffix2, &pArgument))
	{
		procChannelIoGain(FALSE, BDevice, 0, suffix1, suffix2, pArgument);
		return;
	}
	// Check for real-time A Side Input Gain command.
	if (parseCommand(pUserCmd, "RTAINGAIN", &suffix1, &suffix2, &pArgument))
	{
		procChannelIoGain(TRUE, ADevice, UpdateTdmInGain, suffix1, suffix2, pArgument);
		return;
	}

	// Check for real-time B Side Input Gain command.
	if (parseCommand(pUserCmd, "RTBINGAIN", &suffix1, &suffix2, &pArgument))
	{
		procChannelIoGain(TRUE, BDevice, UpdateNetInGain, suffix1, suffix2, pArgument);
		return;
	}

	// Check for real-time A Side Output Gain command.
	if (parseCommand(pUserCmd, "RTAOUTGAIN", &suffix1, &suffix2, &pArgument))
	{
		procChannelIoGain(FALSE, ADevice, UpdateTdmOutGain, suffix1, suffix2, pArgument);
		return;
	}

	// Check for real-time B Side Output Gain command.
	if (parseCommand(pUserCmd, "RTBOUTGAIN", &suffix1, &suffix2, &pArgument))
	{
		procChannelIoGain(FALSE, BDevice, UpdateNetOutGain, suffix1, suffix2, pArgument);
		return;
	}	
#ifdef USE_AEC
	// Check for Reset AEC command.
	if (parseCommand(pUserCmd, "RESAEC", &suffix1, &suffix2, &pArgument))
	{
		procChannelResetAEC(suffix1, suffix2, pArgument);
		return;
	}
	// Check for Bypass AEC command.
	if (parseCommand(pUserCmd, "BYPAEC", &suffix1, &suffix2, &pArgument))
	{
		procChannelBypassAEC(suffix1, suffix2, pArgument);
		return;
	}
	// Check for Enable AEC command.
	if (parseCommand(pUserCmd, "ENABAEC", &suffix1, &suffix2, &pArgument))
	{
		procChannelEnableAEC(suffix1, suffix2, pArgument);
		return;
	}
	// Check for AEC Enable command.
	if (parseCommand(pUserCmd, "AECENAB", &suffix1, &suffix2, &pArgument))
	{
		procChannelAlgoEnable(algoAec, suffix1, suffix2, pArgument);
		return;
	}
#endif
	// Check for RTP control command.
	if (parseCommand(pUserCmd, "RTPCTRL", &suffix1, &suffix2, &pArgument))
	{
		procChannelRTPControl(suffix1, suffix2, pArgument);
		return;
	}

	// Check for LEC Enable command.
	if (parseCommand(pUserCmd, "LECENAB", &suffix1, &suffix2, &pArgument))
	{
		procChannelAlgoEnable(algoLec, suffix1, suffix2, pArgument);
		return;
	}

	// Check for AGC Enable command.
	if (parseCommand(pUserCmd, "AGCENAB", &suffix1, &suffix2, &pArgument))
	{
		procChannelAlgoEnable(algoAgc, suffix1, suffix2, pArgument);
		return;
	}

	// Check for VADCNG Enable command.
	if (parseCommand(pUserCmd, "VADCNGENAB", &suffix1, &suffix2, &pArgument))
	{
		procChannelAlgoEnable(algoVadCng, suffix1, suffix2, pArgument);
		return;
	}

	// Check for DTX Enable command.
	if (parseCommand(pUserCmd, "DTXENAB", &suffix1, &suffix2, &pArgument))
	{
		procChannelAlgoEnable(algoDtx, suffix1, suffix2, pArgument);
		return;
	}

	// Check for Data mdoe Enable command.
	if (parseCommand(pUserCmd, "DATAENAB", &suffix1, &suffix2, &pArgument))
	{
		procChannelAlgoEnable(algoData, suffix1, suffix2, pArgument);
		return;
	}

	// Check for RTP IP Address command.
	if (parseCommand(pUserCmd, "RTPIP", &suffix1, &suffix2, &pArgument))
	{
		procRtpIpAddress(suffix1, suffix2, pArgument);
		return;
	}

	// Check for RTP Port command.
	if (parseCommand(pUserCmd, "RTPPORT", &suffix1, &suffix2, &pArgument))
	{
		procRtpPort(suffix1, suffix2, pArgument);
		return;
	}
	// Check for Source RTP Port command.
	if (parseCommand(pUserCmd, "SRTPPORT", &suffix1, &suffix2, &pArgument))
	{
		procSRtpPort(suffix1, suffix2, pArgument);
		return;
	}

	// Check for Jitter Buffer Size command.
	if (parseCommand(pUserCmd, "JBSIZE", &suffix1, &suffix2, &pArgument))
	{
		procJitrBufrSize(suffix1, suffix2, pArgument);
		return;
	}

	// Check for PCM to Packet Channel Setup command.
	if (parseCommand(pUserCmd, "SUPKT", &suffix1, &suffix2, &pArgument))
	{
		if (pArgument != NULL)
		{
			uartPrintf("Invalid setup command!\n");
		}
		else
		{
			if (suffix1 < 0)
			{
				suffix1 = 0;
				suffix2 = NUM_TEST_CHANS - 1;
			}
			else if ((suffix1 >= NUM_TEST_CHANS) || (suffix2 >= NUM_TEST_CHANS))
			{
				uartPrintf("Channel exceeds max of %u!\n", NUM_TEST_CHANS - 1);
				return;
			}
			if (suffix1 < suffix2)
			{
				for (i = suffix1; i <= suffix2; i++)
				{
					setupPcmToPktChannel((ADT_UInt16) i);
				}
			}
			else
			{
				setupPcmToPktChannel((ADT_UInt16) suffix1);
			}
		}
		return;
	}
	// Check for custom Channel Setup command.
	if (parseCommand(pUserCmd, "SUCUST", &suffix1, &suffix2, &pArgument))
	{
		if (pArgument != NULL)
		{
			uartPrintf("Invalid setup command!\n");
		}
		else
		{
			if (suffix1 < 0)
			{
				suffix1 = 0;
				suffix2 = NUM_TEST_CHANS - 1;
			}
			if (suffix1 < suffix2)
			{
				for (i = suffix1; i <= suffix2; i++)
				{
					setupCustChannel((ADT_UInt16) i);
				}
			}
			else
			{
				setupCustChannel((ADT_UInt16) suffix1);
			}
		}
		return;
	}
	// Check for Packet Conference Channel Setup command.
	if (parseCommand(pUserCmd, "CNFPKT", &suffix1, &suffix2, &pArgument))
	{
		if (pArgument != NULL)
		{
			uartPrintf("Invalid setup command!\n");
		}
		else
		{
	        setupPktConfChannel((ADT_UInt16) suffix1,  (ADT_UInt16) suffix2);
		}
		return;
	}

	// Check for Pcm Conference Channel Setup command.
	if (parseCommand(pUserCmd, "CNFPCM", &suffix1, &suffix2, &pArgument))
	{
		if (pArgument != NULL)
		{
			uartPrintf("Invalid setup command!\n");
		}
		else
		{
		    setupPcmConfChannel((ADT_UInt16) suffix1, (ADT_UInt16) suffix2);
		}
		return;
	}

	// Check for Conference Composite Channel Setup command.
	if (parseCommand(pUserCmd, "CNFCMP", &suffix1, &suffix2, &pArgument))
	{
		if (pArgument != NULL)
		{
			uartPrintf("Invalid setup command!\n");
		}
		else
		{
			setupCompositeConfChannel((ADT_UInt16) suffix1, (ADT_UInt16)suffix2);
		}
		return;
	}

	// Check for TX MCAST PCM to Packet Channel Setup command.
	if (parseCommand(pUserCmd, "SUTX", &suffix1, &suffix2, &pArgument))
	{
		if (pArgument != NULL)
		{
			uartPrintf("Invalid setup command!\n");
		}
		else
		{
			if (suffix1 < 0)
			{
				suffix1 = 0;
				suffix2 = NUM_TEST_CHANS - 1;
			}
			if (suffix1 < suffix2)
			{
				for (i = suffix1; i <= suffix2; i++)
				{
					setupTxMCASTPcmToPktChannel((ADT_UInt16) i);
				}
			}
			else
			{
				setupTxMCASTPcmToPktChannel((ADT_UInt16) suffix1);
			}
		}
		return;
	}
	// Check for RX MCAST PCM to Packet Channel Setup command.
	if (parseCommand(pUserCmd, "SURX", &suffix1, &suffix2, &pArgument))
	{
		if (pArgument != NULL)
		{
			uartPrintf("Invalid setup command!\n");
		}
		else
		{
			if (suffix1 < 0)
			{
				suffix1 = 0;
				suffix2 = NUM_TEST_CHANS - 1;
			}
			if (suffix1 < suffix2)
			{
				for (i = suffix1; i <= suffix2; i++)
				{
					setupRxMCASTPcmToPktChannel((ADT_UInt16) i);
				}
			}
			else
			{
				setupRxMCASTPcmToPktChannel((ADT_UInt16) suffix1);
			}
		}
		return;
	}

	// Check for PCM to PCM Channel Setup command.
	if (parseCommand(pUserCmd, "SUPCM", &suffix1, &suffix2, &pArgument))
	{
		if (pArgument != NULL)
		{
			uartPrintf("Invalid setup command!\n");
		}
		else
		{
			if (suffix1 < 0)
			{
				suffix1 = 0;
				suffix2 = NUM_TEST_CHANS - 1;
			}
			if (suffix1 < suffix2)
			{
				for (i = suffix1; i <= suffix2; i++)
				{
					setupPcmToPcmChannel((ADT_UInt16) i);
				}
			}
			else
			{
				setupPcmToPcmChannel((ADT_UInt16) suffix1);
			}
		}
		return;
	}

	// Check for Channel Status command.
	if (parseCommand(pUserCmd, "CS", &suffix1, &suffix2, &pArgument))
	{
		if (pArgument != NULL)
		{
			uartPrintf("Invalid status command!\n");
		}
		else
		{
			if (suffix1 < 0)
			{
				suffix1 = 0;
				suffix2 = NUM_TEST_CHANS - 1;
			}
			if (suffix1 < suffix2)
			{
				for (i = suffix1; i <= suffix2; i++)
				{
					getChannelStatus((ADT_UInt16) i, FALSE);
				}
			}
			else
			{
				getChannelStatus((ADT_UInt16) suffix1, FALSE);
			}
		}
		return;
	}

	// Check for Channel RTP Packet Statistics command.
	if (parseCommand(pUserCmd, "PKTS", &suffix1, &suffix2, &pArgument))
	{
		if (pArgument != NULL)
		{
			uartPrintf("Invalid status command!\n");
		}
		else
		{
			if (suffix1 < 0)
			{
				suffix1 = 0;
				suffix2 = NUM_TEST_CHANS - 1;
			}
			if (suffix1 < suffix2)
			{
				for (i = suffix1; i <= suffix2; i++)
				{
					getChannelStatus((ADT_UInt16) i, TRUE);
				}
			}
			else
			{
				getChannelStatus((ADT_UInt16) suffix1, TRUE);
			}
		}
		return;
	}

	// Check for Channel Teardown command.
	if (parseCommand(pUserCmd, "TD", &suffix1, &suffix2, &pArgument))
	{
		if (pArgument != NULL)
		{
			uartPrintf("Invalid status command!\n");
		}
		else
		{
			if (suffix1 < 0)
			{
				suffix1 = 0;
				suffix2 = NUM_TEST_CHANS - 1;
			}
			if (suffix1 < suffix2)
			{
				for (i = suffix1; i <= suffix2; i++)
				{
					teardownChannel((ADT_UInt16) i);
				}
			}
			else
			{
				teardownChannel((ADT_UInt16) suffix1);
			}
		}
		return;
	}

	// Check for Start Tone Generation command.
	if (parseCommand(pUserCmd, "TONEGENA", &suffix1, &suffix2, &pArgument))
	{
		temp32 = 0;
		if (pArgument != NULL)
		{
			if ((sscanf(pArgument, "%u", &temp32) != 1) || (temp32 >= numToneGenParms))
			{
				uartPrintf("Invalid tonegen argument (0-%d)!\n",numToneGenParms-1);
				return; 
			}
		}
		if (suffix1 < 0)
		{
			suffix1 = 0;
			suffix2 = NUM_TEST_CHANS - 1;
		}
		if (suffix1 < suffix2)
		{
			for (i = suffix1; i <= suffix2; i++)
			{
				startToneGen((ADT_UInt16) i, ADevice, temp32);
			}
		}
		else
		{
			startToneGen((ADT_UInt16) suffix1, ADevice, temp32);
		}
		return;
	}

	if (parseCommand(pUserCmd, "TONEGENB", &suffix1, &suffix2, &pArgument))
	{
		temp32 = 0;
		if (pArgument != NULL)
		{
			if ((sscanf(pArgument, "%u", &temp32) != 1) || (temp32 >= numToneGenParms))
			{
				uartPrintf("Invalid tonegen argument (0-%d)!\n",numToneGenParms-1);
				return; 
			}
		}
		if (suffix1 < 0)
		{
			suffix1 = 0;
			suffix2 = NUM_TEST_CHANS - 1;
		}
		if (suffix1 < suffix2)
		{
			for (i = suffix1; i <= suffix2; i++)
			{
				startToneGen((ADT_UInt16) i, BDevice, temp32);
			}
		}
		else
		{
			startToneGen((ADT_UInt16) suffix1, BDevice, temp32);
		}
		return;
	}

	if (parseCommand(pUserCmd, "DTMFDIALA", &suffix1, &suffix2, &pArgument))
	{
        ADT_UInt16 rv;

		if (pArgument == NULL)
		{
	        uartPrintf("Invalid dtmf dialstring!\n");
		    return; 
        }

		if (suffix1 < 0)
		{
			suffix1 = 0;
			suffix2 = NUM_TEST_CHANS - 1;
		}
		if (suffix1 < suffix2)
		{
			for (i = suffix1; i <= suffix2; i++)
			{
				rv = sendDtmfDialString((ADT_UInt16) i, ADevice, pArgument);
                if (!rv) break;
			}
		}
		else
		{
		    rv = sendDtmfDialString((ADT_UInt16) suffix1, ADevice, pArgument);
		}
		return;
	}

	if (parseCommand(pUserCmd, "DTMFDIALB", &suffix1, &suffix2, &pArgument))
	{
        ADT_UInt16 rv;

		if (pArgument == NULL)
		{
	        uartPrintf("Invalid dtmf dialstring!\n");
		    return; 
        }

		if (suffix1 < 0)
		{
			suffix1 = 0;
			suffix2 = NUM_TEST_CHANS - 1;
		}
		if (suffix1 < suffix2)
		{
			for (i = suffix1; i <= suffix2; i++)
			{
				rv = sendDtmfDialString((ADT_UInt16) i, BDevice, pArgument);
                if (!rv) break;
			}
		}
		else
		{
		    rv = sendDtmfDialString((ADT_UInt16) suffix1, BDevice, pArgument);
		}
		return;
	}


	// Check for Stop Tone Generation command.
	if (strncmpi(pUserCmd, "TONESTOPA", 9) == 0)
	{
		if ((sscanf(&(pUserCmd[9]), "%u", &temp32) == 1) && (temp32 <= 65535))
		{
			stopToneGen((ADT_UInt16) temp32, ADevice);
			return;
		}
	}

	// Check for Stop Tone Generation command.
	if (strncmpi(pUserCmd, "TONESTOPB", 9) == 0)
	{
		if ((sscanf(&(pUserCmd[9]), "%u", &temp32) == 1) && (temp32 <= 65535))
		{
			stopToneGen((ADT_UInt16) temp32, BDevice);
			return;
		}
	}

	// Check for audio control A-side command.
	if (strncmpi(pUserCmd, "AUDA", 4) == 0)
	{
		if ((sscanf(&(pUserCmd[5]), "%u", &temp32) == 1) && (temp32 <= 65535))
		{
            audioRecordingControl((ADT_UInt16) temp32, ADevice, pUserCmd[4]);
			return;
		} 
        else {
             uartPrintf("AUDA Missing parameters\n\rUsage:  AUDA<R,P,C,S><chan> - A-side audio control: Record,Play,ContinuousPlay,Stop on chan\n");
        }
	}

	// Check for audio control B-side command.
	if (strncmpi(pUserCmd, "AUDB", 4) == 0)
	{
		if ((sscanf(&(pUserCmd[5]), "%u", &temp32) == 1) && (temp32 <= 65535))
		{
            audioRecordingControl((ADT_UInt16) temp32, BDevice, pUserCmd[4]);
			return;
		}
        else {
             uartPrintf("AUDB Missing parameters\n\rUsage:  AUDA<R,P,C,S><chan> - A-side audio control: Record,Play,ContinuousPlay,Stop on chan\n");
        }
	}

	// Check for Set VLAN Tags Parameter command.
	if (strncmpi(pUserCmd, "VLANSTAG", 8) == 0)
	{
		procVLANSetParms(&pUserCmd[9]);
		return;
	}

	// Check for Get VLAN Tags Parameter command.
	if (strncmpi(pUserCmd, "VLANGTAG", 8) == 0)
	{
		procVLANGetParms(&pUserCmd[9]);
		return;
	}

	// Check for VLAN chanEnable command.
	if (parseCommand(pUserCmd, "VLANIDX", &suffix1, &suffix2, &pArgument))
	{
		procVLANChanEnabParms(suffix1, suffix2, pArgument);
		return;
	}

	// Check for SYSVAD command.
	if (strncmpi(pUserCmd, "SYSVAD", 6) == 0)
	{
        int32_t noise, hangMs, winMs, report;
		if (sscanf(&(pUserCmd[6]), "%u %u %u %u", &noise, &hangMs, &winMs, &report) == 4)
		{
            writeSysVad(noise, hangMs, winMs, report);
			return;
		}
	}


	// Check for Display DSP's System Configuration command.
	if (strcmpi(pUserCmd, "SYSCFG") == 0)
	{
		getSysConfig();
		return;
	}

	// Check for Display DSP's System Parameters command.
	if (strcmpi(pUserCmd, "SYSPARMS") == 0)
	{
		getSysParms(1);
		return;
	}

	// Check for Display Network Config command.
	if (strcmpi(pUserCmd, "NET") == 0)
	{
		readIpStackConfig();
		return;
	}

	// Check for Display TDM Statistics command.
	if (strcmpi(pUserCmd, "TDM") == 0)
	{
		getTdmStats();
		return;
	}

	// Check for Display CPU Usage command.
	if (strcmpi(pUserCmd, "CPU") == 0)
	{
		getCpuUsage();
		return;
	}

	// Configure Arbitrary tone detector command.
	if (strcmpi(pUserCmd, "ARB") == 0)
	{
		 configArbitraryDetector();
		return;
	}

	// Check for Reset CPU Statistics command.
	if (strcmpi(pUserCmd, "RSTCPU") == 0)
	{
		resetDspStats(TRUE, FALSE);
		return;
	}

	// Check for Reset Framing Statistics command.
	if (strcmpi(pUserCmd, "RSTFRAME") == 0)
	{
		resetDspStats(FALSE, TRUE);
		return;
	}

	// Check for Channel AEC Status command.
	if (parseCommand(pUserCmd, "AECSTAT", &suffix1, &suffix2, &pArgument))
	{
		if (pArgument != NULL)
		{
			uartPrintf("Invalid status command!\n");
		}
		else
		{
			if (suffix1 < 0)
			{
				suffix1 = 0;
				suffix2 = NUM_TEST_CHANS - 1;
			}
			if (suffix1 < suffix2)
			{
				for (i = suffix1; i <= suffix2; i++)
				{
					displayAecStatus((ADT_UInt16) i);
				}
			}
			else
			{
				displayAecStatus((ADT_UInt16) suffix1);
			}
		}
		return;
	}

	// Check for AEC Parameter command.
	if (parseCommand(pUserCmd, "AEC", &suffix1, &suffix2, &pArgument))
	{
		procAecParm(suffix1, suffix2, pArgument);
		return;
	}

	// Check for Channel LEC Status command.
	if (parseCommand(pUserCmd, "LECSTAT", &suffix1, &suffix2, &pArgument))
	{
		if (pArgument != NULL)
		{
			uartPrintf("Invalid status command!\n");
		}
		else
		{
			if (suffix1 < 0)
			{
				suffix1 = 0;
				suffix2 = NUM_TEST_CHANS - 1;
			}
			if (suffix1 < suffix2)
			{
				for (i = suffix1; i <= suffix2; i++)
				{
					displayLecStatus((ADT_UInt16) i, ADevice);
				}
			}
			else
			{
				displayLecStatus((ADT_UInt16) suffix1, ADevice);
			}
		}
		return;
	}

	// Check for LEC Parameter command.
	if (parseCommand(pUserCmd, "LEC", &suffix1, &suffix2, &pArgument))
	{
		procLecParm(suffix1, suffix2, pArgument);
		return;
	}

    // check for A-side detector command
	if (parseCommand(pUserCmd, "DETA", &suffix1, &suffix2, &pArgument))
	{
		procChannelDetect(ADevice, suffix1, suffix2, pArgument);
		return;
	}

    // check for B-side detector command
	if (parseCommand(pUserCmd, "DETB", &suffix1, &suffix2, &pArgument))
	{
		procChannelDetect(BDevice, suffix1, suffix2, pArgument);
		return;
	}

	if (parseCommand(pUserCmd, "DTMFMODE", &suffix1, &suffix2, &pArgument))
	{
		procDtmfMode(suffix1, suffix2, pArgument);
		return;
	}

	if (parseCommand(pUserCmd, "FAXMODE", &suffix1, &suffix2, &pArgument))
	{
		procFaxMode(suffix1, suffix2, pArgument);
		return;
	}

	if (parseCommand(pUserCmd, "CIDMODE", &suffix1, &suffix2, &pArgument))
	{
		procCIDMode(suffix1, suffix2, pArgument);
		return;
	}
    
	// Config TDM McBsp Port command.
	if (strcmpi(pUserCmd, "PORT") == 0)
	{
#ifdef PORT_CFG_MCBSP_64
		dspMcBspTDMInit();
#endif
#ifdef PORT_CFG_MCASP_64
    	dspMcAspTDMInit();
#endif
#ifdef PORT_CFG_TSIP
    	dspTsipTDMInit();
#endif
		return;
	}

	// Check for command to set the A-side tdm
	if (parseCommand(pUserCmd, "APORT", &suffix1, &suffix2, &pArgument))
	{
		if (suffix1 == 0)
            APort = McBSPPort0;
        else if (suffix1 == 1)
            APort = McBSPPort1;
        else
            uartPrintf("APort=%d\n",APort);
		return;
	}

	// Check for command to set the B-side tdm
	if (parseCommand(pUserCmd, "BPORT", &suffix1, &suffix2, &pArgument))
	{
		if (suffix1 == 0)
            BPort = McBSPPort0;
        else  if (suffix1 == 1)
            BPort = McBSPPort1;
        else
            uartPrintf("BPort=%d\n",APort);
		return;
	}

	// Check for command to set the port comnpanding
	if (parseCommand(pUserCmd, "CMP", &suffix1, &suffix2, &pArgument))
	{
        char mode;
        if (pArgument != NULL)
            mode = toupper(pArgument[0]);

		if (suffix1 == 0) {
            if (mode == 'U')
                compandMode[0] = 0;
            else if (mode == 'A')
                compandMode[0] = 1;
            else if (mode == 'L')
                compandMode[0] = 2;
            else
                uartPrintf("Companding Mode value must be: U,A,L\n");
        } else if (suffix1 == 1) {
            if (mode == 'U')
                compandMode[1] = 0;
            else if (mode == 'A')
                compandMode[1] = 1;
            else if (mode == 'L')
                compandMode[1] = 2;
            else
                uartPrintf("Companding Mode value must be: U,A,L\n");
        } else if (suffix1 == 2) {
            if (mode == 'U')
                compandMode[2] = 0;
            else if (mode == 'A')
                compandMode[2] = 1;
            else if (mode == 'L')
                compandMode[2] = 2;
            else
                uartPrintf("Companding Mode value must be: U,A,L\n");
        } else {
            uartPrintf("Companding Mode[0]=%s\n",cmpModeString[compandMode[0]]);
            uartPrintf("Companding Mode[1]=%s\n",cmpModeString[compandMode[1]]);
            uartPrintf("Companding Mode[2]=%s\n",cmpModeString[compandMode[2]]);
        }
		return;
	}


    // manually check for event
	if (strcmpi(pUserCmd, "EVT") == 0)
	{
		getEvent();
		return;
	}
	// Check for MEMT Parameter command.
	if (parseCommand(pUserCmd, "MEMT", &suffix1, &suffix2, &pArgument))
	{
		procMemoryTest();
		return;
	}
	// Check for Sidetone Parameter command.
	if (parseCommand(pUserCmd, "STN", &suffix1, &suffix2, &pArgument))
	{
		procSideToneParms(suffix1, suffix2, pArgument);
		return;
	}
	// Config Conference
	if (parseCommand(pUserCmd, "CONF", &suffix1, &suffix2, &pArgument))
	{
		if (pArgument != NULL)
		{
			uartPrintf("Invalid setup command!\n");
		}
		else
		{
            openConf((ADT_UInt16) suffix1);
		}
		return;
	}

    // load a wavefile
	if (parseCommand(pUserCmd, "WAVLOAD", &suffix1, &suffix2, &pArgument))
    {
	    loadWav(pArgument);
        return;
    }

	if (parseCommand(pUserCmd, "WAVSTOP", &suffix1, &suffix2, &pArgument))
    {
		GpakDeviceSide_t direction = NetToDSP;
		if (pArgument != NULL)
		{
             if (strcmpi(pArgument, "NET") == 0)
                direction = DSPToNet;
		}
		if (suffix1 < 0)
		{
			uartPrintf("Invalid Channel Id\n");
		}

		if (suffix2 < 0)
		{
			uartPrintf("Invalid Wavefile Id\n");
		}
        playWav((ADT_UInt16)suffix1,  (ADT_UInt16)suffix2, WavePlaybackStop, direction);
        return;
    }

	if (parseCommand(pUserCmd, "WAVPLAY", &suffix1, &suffix2, &pArgument))
    {
		GpakDeviceSide_t direction = NetToDSP;
		if (pArgument != NULL)
		{
            if (strcmpi(pArgument, "NET") == 0)
                direction = DSPToNet;
		}
		if (suffix1 < 0)
		{
			uartPrintf("Invalid Channel Id\n");
		}

		if (suffix2 < 0)
		{
			uartPrintf("Invalid Wavefile Id\n");
		}
        playWav((ADT_UInt16)suffix1,  (ADT_UInt16)suffix2, WavePlaybackOnce, direction);
        return;
    }

	if (parseCommand(pUserCmd, "WAVLOOP", &suffix1, &suffix2, &pArgument))
    {
		GpakDeviceSide_t direction = NetToDSP;
		if (pArgument != NULL)
		{
            if (strcmpi(pArgument, "NET") == 0)
                direction = DSPToNet;
		}
		if (suffix1 < 0)
		{
			uartPrintf("Invalid Channel Id\n");
		}

		if (suffix2 < 0)
		{
			uartPrintf("Invalid Wavefile Id\n");
		}
        playWav((ADT_UInt16)suffix1,  (ADT_UInt16)suffix2, WavePlaybackLoop, direction);
        return;
    }

	// Check for CID command.
	if (parseCommand(pUserCmd, "TXCID", &suffix1, &suffix2, &pArgument))
	{
		if (pArgument != NULL)
		{
			uartPrintf("Invalid status command!\n");
		}
		else
		{
			if (suffix1 < 0)
			{
				suffix1 = 0;
				suffix2 = NUM_TEST_CHANS - 1;
			}
			if (suffix1 < suffix2)
			{
				for (i = suffix1; i <= suffix2; i++)
				{
					sendCIDMsg((ADT_UInt16) i);
				}
			}
			else
			{
				sendCIDMsg((ADT_UInt16) suffix1);
			}
		}
		return;
	}

	// Check for SRTP command.
	if (parseCommand(pUserCmd, "SRTPENAB", &suffix1, &suffix2, &pArgument))
	{
		procSRTPCryptoSuite(suffix1, suffix2, pArgument);
		return;
	}

	// Check for RTCP command.
	if (parseCommand(pUserCmd, "RTCPENAB", &suffix1, &suffix2, &pArgument))
	{
		procChannelAlgoEnable(algoRtcp, suffix1, suffix2, pArgument);
		return;
	}
	if (parseCommand(pUserCmd, "RTPTO", &suffix1, &suffix2, &pArgument))
	{
		procRtpRxTimeoutParams(suffix1, suffix2, pArgument);
		return;
	}
	// Check for PTT Parameter command.
	if (parseCommand(pUserCmd, "PTTCFG", &suffix1, &suffix2, &pArgument))
	{
		procPTTConfigParms(suffix1, suffix2, pArgument);
		return;
	}

	if (parseCommand(pUserCmd, "PTTUPT", &suffix1, &suffix2, &pArgument))
	{
		procPTTUpdateParms(suffix1, suffix2, pArgument);
		return;
	}
	if (parseCommand(pUserCmd, "PTTPF", &suffix1, &suffix2, &pArgument))
	{
		procPFParms(suffix1, suffix2, pArgument);
		return;
	}
	if (parseCommand(pUserCmd, "PTTPFU", &suffix1, &suffix2, &pArgument))
	{
		procPFUpdateParms(suffix1, suffix2, pArgument);
		return;
	}
	if (parseCommand(pUserCmd, "PTTNPF", &suffix1, &suffix2, &pArgument))
	{
		procNPFParms(suffix1, suffix2, pArgument);
		return;
	}
	if (parseCommand(pUserCmd, "PTTGA", &suffix1, &suffix2, &pArgument))
	{
		procGAParms(suffix1, suffix2, pArgument);
		return;
	}

	// Check for RTCP Bye Type command.
	if (parseCommand(pUserCmd, "RTCPBYE", &suffix1, &suffix2, &pArgument))
	{
		if (pArgument != NULL)
		{
			uartPrintf("Invalid status command!\n");
		}
		else
		{
			if (suffix1 < 0)
			{
				suffix1 = 0;
			}
		    procRtcpBye((ADT_UInt16)suffix1);
		}
		return;
	}

	// tell DSP to send version info
	if (strcmpi(pUserCmd, "GETVER") == 0)  
    {
        getver();
        return;
    }

	if (parseCommand(pUserCmd, "TDMFIXON", &suffix1, &suffix2, &pArgument))
    {
		if (pArgument == NULL)
		{
			uartPrintf("Invalid TDMFix command!\n");
		}
		else
		{
			if (suffix1 < 0)
			{
				suffix1 = 0;
			}
		    if ((sscanf(pUserCmd, "%u", &temp32) != 1))
                temp32 = 0x87;

		    tdmFixedValControl((ADT_UInt16)suffix1, Enabled, (ADT_UInt16)temp32);
		}
		return;
    }

	if (parseCommand(pUserCmd, "TDMFIXOFF", &suffix1, &suffix2, &pArgument))
    {
	     if (suffix1 < 0)
	     {
		    suffix1 = 0;
		 }
		 tdmFixedValControl((ADT_UInt16)suffix1, Disabled, (ADT_UInt16)0);
		return;
    }

	return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// parseCommand - Parse user input for a command.
//
// FUNCTION
//  This function parses the user input for a command according to general
//  syntax as shown below.
//
//    cmd          - command without numeric suffix and without argument
//    cmd#         - command with numeric suffix and without argument
//    cmd#-#       - command with numeric range suffix and without argument
//    cmd=value    - command without numeric suffix with argument
//    cmd#=value   - command with numeric suffix with argument
//    cmd#-#=value - command with numeric range suffix with argument
//
//  Note: The equal sign may be preceeded and followed by spaces.
//
// RETURNS
//  TRUE if the command is valid or FALSE if invalid.
//  *pSuffix1 = the 1st command suffix value (-1 if no suffix)
//  *pSuffix2 = the 2nd command suffix value (same as 1st suffix if missing)
//  *ppArgument = pointer to argument (NULL if no argument)
//
static Bool parseCommand(
	char *pUserInput,			// pointer to user input string
	char *pCmd,					// pointer to command name string to match
	int32_t *pSuffix1,			// pointer to 1st suffix value variable
	int32_t *pSuffix2,			// pointer to 2nd suffix value variable
	char **ppArgument			// pointer to argument pointer variable
	)
{
	uint32_t temp32;			// temporary 32 bit unsigned value

	// Initialize return values to null.
	*pSuffix1 = -1;
	*pSuffix2 = -1;
	*ppArgument = NULL;

	// Compare the command name.
	while (*pCmd != NULL)
	{
		if (toupper(*pCmd++) != toupper(*pUserInput++))
		{
			return FALSE;
		}
	}

	// Check for the optional numeric suffix.
	if ((*pUserInput >= '0') && (*pUserInput <= '9'))
	{
		temp32 = (uint32_t) (*pUserInput++ - '0');
		while ((*pUserInput >= '0') && (*pUserInput <= '9'))
		{
			temp32 = (temp32 * 10) + ((uint32_t) (*pUserInput++ - '0'));
			if (temp32 >= 0x80000000)
			{
				return FALSE;
			}
		}
		*pSuffix1 = (int32_t) temp32;
		*pSuffix2 = *pSuffix1;

		// Check for optional suffix range.
		if ((pUserInput[0] == '-') &&
			(pUserInput[1] >= '0') && (pUserInput[1] <= '9'))
		{
			pUserInput++;
			temp32 = 0;
			while ((*pUserInput >= '0') && (*pUserInput <= '9'))
			{
				temp32 = (temp32 * 10) + ((uint32_t) (*pUserInput++ - '0'));
				if (temp32 >= 0x80000000)
				{
					return FALSE;
				}
			}
			*pSuffix2 = (int32_t) temp32;
		}
	}

	// Skip space characters.
	while (*pUserInput == ' ')
	{
		pUserInput++;
	}

	// Check for the optional equal sign.
	if (*pUserInput == '=')
	{

		// Skip trailing space characters.
		pUserInput++;
		while (*pUserInput == ' ')
		{
			pUserInput++;
		}
		*ppArgument = pUserInput;
	}
	else if (*pUserInput != '\0')
	{
		return FALSE;
	}

	// Return with an indication the command is valid.
	return TRUE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// procCoreNum - Process a CoreNum command.
//
// FUNCTION
//  This function processes a CoreNum command.
//
// RETURNS
//  nothing
//
static void procCoreNum(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg,				// pointer to command argument string
    char mode				    // mode: c==constant, i==interleaved, b==block 
	)
{
	int32_t i,j;					// loop index / counter
    uint32_t  core, chansPerCore;
	int32_t temp32;				// temporary 32 bit signed value
    int stat;

    // Constant mode: assign specified channels to the same core
    if (mode == 'c') {
	    // Validate the channel Id.
    	if (cmdSuffix1 < 0)
    	{
    		cmdSuffix1 = 0;
    		cmdSuffix2 = NUM_TEST_CHANS - 1;
    	}
    	else if ((cmdSuffix1 >= NUM_TEST_CHANS) || (cmdSuffix2 >= NUM_TEST_CHANS))
    	{
    		uartPrintf("Channel exceeds max of %u!\n", NUM_TEST_CHANS - 1);
    		return;
    	}
    
    	// If the argument is null, display the current value(s).
    	if (pCmdArg == NULL)
    	{
    		if (cmdSuffix1 < cmdSuffix2)
    		{
    			uartPrintf("Channel Core Assignments:\n");
    			for (i = cmdSuffix1; i <= cmdSuffix2; i++)
    			{
    				uartPrintf(" [%u] = %d\n", i, chanCore[i]);
    			}
    		}
    		else
    		{
    			uartPrintf("Chan %d Core Assignment = %d\n",
    					   cmdSuffix1, chanCore[cmdSuffix1]);
    		}
    		return;
    	}
    
    	if ((sscanf(pCmdArg, "%u", &core) != 1) || (core >= MAX_CHAN_CORE))
    	{
    		uartPrintf("Invalid core setting: %s\n", pCmdArg);
    		return;
    	}
    
    
    
    	// Set the channel(s) current value.
    	if (cmdSuffix1 < cmdSuffix2)
    	{
    		for (i = cmdSuffix1; i <= cmdSuffix2; i++)
    		{
    			chanCore[i] = core;
    		}
    		uartPrintf("Chan %d-%d assigned to core to %d\n",
    				   cmdSuffix1, cmdSuffix2, core);
    	}
    	else
    	{
    		chanCore[cmdSuffix1] = (ADT_UInt16)core;
    		uartPrintf("Chan %d assigned to core %d\n",
    				   cmdSuffix1, core);
    	}
	    return;
    } 

    // channel assign to N-cores by interleave
    // example for N=4
    // core0 = chans 0, 4, 8,  12...156 
    // core1 = chans 1, 5, 9,  13...157
    // core2 = chans 2, 6, 10, 14...158
    // core3 = chans 3, 7, 11, 15...159
    if (mode == 'i') {
		if (pCmdArg == NULL)
		{
			uartPrintf("Invalid coreI setting!\n");
            return;
		}
        if (cmdSuffix1 < (int32_t)MIN_CHAN_CORE)
            cmdSuffix1 = MIN_CHAN_CORE;

        // interleave channels across this many cores
        stat = sscanf_s(pCmdArg, "%u", &temp32);
        if ((stat != 1) || (temp32 > (int32_t)MAX_CHAN_CORE)) {
			uartPrintf("Invalid number of cores!\n");
            return;
        }
        
        core = cmdSuffix1;
        for (i=0; i<NUM_TEST_CHANS; i++) {
            chanCore[i] = core++;
            if (core >= (uint32_t)temp32)
                core = cmdSuffix1;
        }
  	    uartPrintf("Channel Core Interleave mode success:\n");         
		return;
    }

    // channel assign to N-cores by block group
    // example for N=4
    // core0 = chans 0  - 39 
    // core1 = chans 40 - 79
    // core2 = chans 80 - 119
    // core3 = chans 120 - 139
    if (mode == 'b') {
		if (pCmdArg == NULL)
		{
			uartPrintf("Invalid coreB setting!\n");
            return;
		}
        if (cmdSuffix1 < (int32_t)MIN_CHAN_CORE)
            cmdSuffix1 = MIN_CHAN_CORE;

        // interleave channels across this many cores
        stat = sscanf_s(pCmdArg, "%u", &temp32);
        if ((stat != 1) || (temp32 > (int32_t)MAX_CHAN_CORE)) {
			uartPrintf("Invalid number of cores!\n");
            return;
        }
        
        core = cmdSuffix1;
        chansPerCore = NUM_TEST_CHANS/(temp32-core);
        j = chansPerCore;
        for (i=0; i<NUM_TEST_CHANS; i++) {
            if (i == j) {
                core++;
                j += chansPerCore;
            }
            if (core == temp32) 
                core = cmdSuffix1;
            chanCore[i] = core;
        }
        uartPrintf("Channel Core Block mode success:\n");       
		return;
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// procCodecType - Process a Codec Type command.
//
// FUNCTION
//  This function processes a Codec Type command.
//
// RETURNS
//  nothing
//
static GpakCodecs interleaved_codecs[3] = {PCMU_64, G729, G726_32};

static void procCodecType(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg,				// pointer to command argument string
    int interleave
	)
{
	int32_t i;					// loop index / counter
	GpakCodecs codecType;		// codec type

	// Validate the channel Id.
	if (cmdSuffix1 < 0)
	{
		cmdSuffix1 = 0;
		cmdSuffix2 = NUM_TEST_CHANS - 1;
	}
	else if ((cmdSuffix1 >= NUM_TEST_CHANS) || (cmdSuffix2 >= NUM_TEST_CHANS))
	{
		uartPrintf("Channel exceeds max of %u!\n", NUM_TEST_CHANS - 1);
		return;
	}
    if (interleave) {
        int codec=0;

	    if (cmdSuffix1 < cmdSuffix2)
	    {
		    for (i = cmdSuffix1; i <= cmdSuffix2; i++)
		    {
                chanCodecType[i] = interleaved_codecs[codec++];
                if (codec > 2) codec = 0;   
		    }
	    }
        uartPrintf("codecs interleaved to channels%d-%d\n",cmdSuffix1,cmdSuffix2);
        return;
    }

	// If the argument is null, display the current value(s).
	if (pCmdArg == NULL)
	{
		if (cmdSuffix1 < cmdSuffix2)
		{
			uartPrintf("Codec Types:\n");
			for (i = cmdSuffix1; i <= cmdSuffix2; i++)
			{
				uartPrintf(" [%u] = %s\n", i, codecTypeName(chanCodecType[i]));
			}
		}
		else
		{
			uartPrintf("Chan %d codec type = %s\n",
					   cmdSuffix1, codecTypeName(chanCodecType[cmdSuffix1]));
		}
		return;
	}
	// Determine the specified codec type.
	if (strcmpi(pCmdArg, codecTypeName(PCMU_64)) == 0)
	{
		codecType = PCMU_64;
	}
	else if (strcmpi(pCmdArg, codecTypeName(PCMA_64)) == 0)
	{
		codecType = PCMA_64;
	}
	else if (strcmpi(pCmdArg, codecTypeName(G722)) == 0)
	{
		codecType = G722;
	}
	else if (strcmpi(pCmdArg, codecTypeName(G723_53)) == 0)
	{
		codecType = G723_53;
	}
	else if (strcmpi(pCmdArg, codecTypeName(G723_63)) == 0)
	{
		codecType = G723_63;
	}
	else if (strcmpi(pCmdArg, codecTypeName(G723_53A)) == 0)
	{
		codecType = G723_53A;
	}
	else if (strcmpi(pCmdArg, codecTypeName(G723_63A)) == 0)
	{
		codecType = G723_63A;
	}
	else if (strcmpi(pCmdArg, codecTypeName(G726_40)) == 0)
	{
		codecType = G726_40;
	}
	else if (strcmpi(pCmdArg, codecTypeName(G726_32)) == 0)
	{
		codecType = G726_32;
	}
	else if (strcmpi(pCmdArg, codecTypeName(G726_24)) == 0)
	{
		codecType = G726_24;
	}
	else if (strcmpi(pCmdArg, codecTypeName(G726_16)) == 0)
	{
		codecType = G726_16;
	}
	else if (strcmpi(pCmdArg, codecTypeName(G729)) == 0)
	{
		codecType = G729;
	}
	else if (strcmpi(pCmdArg, codecTypeName(G729AB)) == 0)
	{
		codecType = G729AB;
	}
	else if (strcmpi(pCmdArg, codecTypeName(AMR_475)) == 0)
	{
		codecType = AMR_475;
	}
	else if (strcmpi(pCmdArg, codecTypeName(AMR_515)) == 0)
	{
		codecType = AMR_515;
	}
	else if (strcmpi(pCmdArg, codecTypeName(AMR_590)) == 0)
	{
		codecType = AMR_590;
	}
	else if (strcmpi(pCmdArg, codecTypeName(AMR_670)) == 0)
	{
		codecType = AMR_670;
	}
	else if (strcmpi(pCmdArg, codecTypeName(AMR_740)) == 0)
	{
		codecType = AMR_740;
	}
	else if (strcmpi(pCmdArg, codecTypeName(AMR_795)) == 0)
	{
		codecType = AMR_795;
	}
	else if (strcmpi(pCmdArg, codecTypeName(AMR_1020)) == 0)
	{
		codecType = AMR_1020;
	}
	else if (strcmpi(pCmdArg, codecTypeName(AMR_1220)) == 0)
	{
		codecType = AMR_1220;
	}
	else if (strcmpi(pCmdArg, codecTypeName(L8)) == 0)
	{
		codecType = L8;
	}
	else if (strcmpi(pCmdArg, codecTypeName(L16)) == 0)
	{
		codecType = L16;
	}
	else
	{
		uartPrintf("Invalid Codec Type: %s\n", pCmdArg);
		return;
	}

	// Set the channel(s) current value.
	if (cmdSuffix1 < cmdSuffix2)
	{
		for (i = cmdSuffix1; i <= cmdSuffix2; i++)
		{
			chanCodecType[i] = codecType;
		}
		uartPrintf("Chan %d-%d codec types set to %s\n",
				   cmdSuffix1, cmdSuffix2, codecTypeName(codecType));
	}
	else
	{
		chanCodecType[cmdSuffix1] = codecType;
		uartPrintf("Chan %d codec type set to %s\n",
				   cmdSuffix1, codecTypeName(codecType));
	}

	return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// codecTypeName - Determine the name of a Codec Type.
//
// FUNCTION
//  This function determines the name of a Codec Type.
//
// RETURNS
//  Pointer to text string for specified Codec Type.
//
static char *codecTypeName(
	GpakCodecs codecType		// codec type
	)
{

	switch (codecType)
	{
	case PCMU_64:
		return "PCMU";
	case PCMA_64:
		return "PCMA";
	case G722:
		return "G722";
	case G723_53:
		return "G723_53";
	case G723_63:
		return "G723_63";
	case G723_53A:
		return "G723_53A";
	case G723_63A:
		return "G723_63A";
	case G726_40:
		return "G726_40";
	case G726_32:
		return "G726_32";
	case G726_24:
		return "G726_24";
	case G726_16:
		return "G726_16";
	case G729:
		return "G729";
	case G729AB:
		return "G729AB";
	case AMR_475:
        return "AMR_475";
	case AMR_515:
        return "AMR_515";
	case AMR_590:
        return "AMR_590";
	case AMR_670:
        return "AMR_670";
	case AMR_740:
        return "AMR_740";
	case AMR_795:
        return "AMR_795";
	case AMR_1020:
        return "AMR_1020";
	case AMR_1220:
        return "AMR_1220";
	case NullCodec:
		return "NullCodec";
	case L16:
		return "L16";
	case L8:
		return "L8";
	}

	return "?";
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// procFrameSize - Process a Frame Size command.
//
// FUNCTION
//  This function processes a Frame Size command.
//
// RETURNS
//  nothing
//
static void procFrameSize(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	)
{
	int32_t i;					// loop index / counter
	GpakFrameHalfMS frameSize;	// frame size

	// Validate the channel Id.
	if (cmdSuffix1 < 0)
	{
		cmdSuffix1 = 0;
		cmdSuffix2 = NUM_TEST_CHANS - 1;
	}
	else if ((cmdSuffix1 >= NUM_TEST_CHANS) || (cmdSuffix2 >= NUM_TEST_CHANS))
	{
		uartPrintf("Channel exceeds max of %u!\n", NUM_TEST_CHANS - 1);
		return;
	}

	// If the argument is null, display the current value(s).
	if (pCmdArg == NULL)
	{
		if (cmdSuffix1 < cmdSuffix2)
		{
			uartPrintf("Frame Sizes:\n");
			for (i = cmdSuffix1; i <= cmdSuffix2; i++)
			{
				uartPrintf(" [%u] = %s\n", i, frameSizeName(chanFrameSize[i]));
			}
		}
		else
		{
			uartPrintf("Chan %d frame size = %s\n",
					   cmdSuffix1, frameSizeName(chanFrameSize[cmdSuffix1]));
		}
		return;
	}

	// Determine the specified frame size.
	if (strcmpi(pCmdArg, frameSizeName(Frame_1ms)) == 0)
	{
		frameSize = Frame_1ms;
	}
	else if (strcmpi(pCmdArg, frameSizeName(Frame_2_5ms)) == 0)
	{
		frameSize = Frame_2_5ms;
	}
	else if (strcmpi(pCmdArg, frameSizeName(Frame_5ms)) == 0)
	{
		frameSize = Frame_5ms;
	}
	else if (strcmpi(pCmdArg, frameSizeName(Frame_10ms)) == 0)
	{
		frameSize = Frame_10ms;
	}
	else if (strcmpi(pCmdArg, frameSizeName(Frame_20ms)) == 0)
	{
		frameSize = Frame_20ms;
	}
	else if (strcmpi(pCmdArg, frameSizeName(Frame_22_5ms)) == 0)
	{
		frameSize = Frame_22_5ms;
	}
	else if (strcmpi(pCmdArg, frameSizeName(Frame_30ms)) == 0)
	{
		frameSize = Frame_30ms;
	}
	else if (strcmpi(pCmdArg, frameSizeName((GpakFrameHalfMS)40*2)) == 0)
	{
		frameSize = (GpakFrameHalfMS)40*2;
	}
	else if (strcmpi(pCmdArg, frameSizeName((GpakFrameHalfMS)50*2)) == 0)
	{
		frameSize = (GpakFrameHalfMS)50*2;
	}
	else if (strcmpi(pCmdArg, frameSizeName((GpakFrameHalfMS)60*2)) == 0)
	{
		frameSize = (GpakFrameHalfMS)60*2;
	}
	else
	{
		uartPrintf("Invalid Frame Size: %s\n", pCmdArg);
		return;
	}

	// Set the channel(s) current value.
	if (cmdSuffix1 < cmdSuffix2)
	{
		for (i = cmdSuffix1; i <= cmdSuffix2; i++)
		{
			chanFrameSize[i] = frameSize;
		}
		uartPrintf("Chan %d-%d frame sizes set to %s\n",
				   cmdSuffix1, cmdSuffix2, frameSizeName(frameSize));
	}
	else
	{
		chanFrameSize[cmdSuffix1] = frameSize;
		uartPrintf("Chan %d frame size set to %s\n",
				   cmdSuffix1, frameSizeName(frameSize));
	}

	return;
}

static void procFrameSizeInterleaved(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	)
{
	int32_t i, numChans;					// loop index / counter
	int32_t fs;	 // frame size

    numChans = NUM_TEST_CHANS;
	// Validate the channel Id.
	if (cmdSuffix1 < 0)
	{
		cmdSuffix1 = 10;
		cmdSuffix2 = 60;
	}
	else {
        switch(cmdSuffix1) {
            case 10: 
            case 20: 
            case 30: 
            case 40: 
            case 50: 
            case 60:
            break;

            default: 
		    uartPrintf("low framesize not supported\n");
            return;
        }
        switch(cmdSuffix2) {
            case 10: 
            case 20: 
            case 30: 
            case 40: 
            case 50: 
            case 60:
            break;

            default: 
		    uartPrintf("upper framesize not supported\n");
            return;
        }
        if (cmdSuffix1 > cmdSuffix2) {
            i = cmdSuffix1;
            cmdSuffix1 = cmdSuffix2;
            cmdSuffix2 = i;
        }
    }
    fs = cmdSuffix1;
    for (i = 0; i < numChans; i++){
        chanFrameSize[i] = (GpakFrameHalfMS)fs*2;
        fs += 10;
        if (fs > cmdSuffix2) {
            fs = cmdSuffix1;
		}
    }
    uartPrintf("channels 0-%d assigned to frames %d-%d\n",numChans,cmdSuffix1, cmdSuffix2);
	return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// frameSizeName - Determine the name of a Frame Size.
//
// FUNCTION
//  This function determines the name of a Frame Size.
//
// RETURNS
//  Pointer to text string for specified Frame Size.
//
static char *frameSizeName(
	GpakFrameHalfMS frameSize	// frame size
	)
{

	switch (frameSize)
	{
	case Frame_1ms:
		return "1 msec";
	case Frame_2_5ms:
		return "2.5 msec";
	case Frame_5ms:
		return "5 msec";
	case Frame_10ms:
		return "10 msec";
	case Frame_20ms:
		return "20 msec";
	case Frame_22_5ms:
		return "22.5 msec";
	case Frame_30ms:
		return "30 msec";
    case (40*2):
		return "40 msec";
    case (50*2):
		return "50 msec";
    case (60*2):
		return "60 msec";
	}

	return "?";
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// procMCast - Process a multi-cast command.
//
// FUNCTION
//  This function processes a multi-cast command.
//
// RETURNS
//  nothing
//
static void procMCast(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	)
{
	uint16_t *pValue;		// pointer to selected value
	char *pName;			// pointer to name
	int32_t i;			    // loop index / counter
	uint32_t mcast;	        // mcast value

	// Validate the channel Id.
	if (cmdSuffix1 < 0)
	{
		cmdSuffix1 = 0;
		cmdSuffix2 = NUM_TEST_CHANS - 1;
	}
	else if ((cmdSuffix1 >= NUM_TEST_CHANS) || (cmdSuffix2 >= NUM_TEST_CHANS))
	{
		uartPrintf("Channel exceeds max of %u!\n", NUM_TEST_CHANS - 1);
		return;
	}

	// Determine the specific channel slot number being used.
	pValue = chanMultiCast;
	pName = "Muli-cast enable (1==TX, 2==RX): ";


	// If the argument is null, display the current value(s).
	if (pCmdArg == NULL)
	{
		if (cmdSuffix1 < cmdSuffix2)
		{
			uartPrintf("%s:\n", pName);
			for (i = cmdSuffix1; i <= cmdSuffix2; i++)
			{
				uartPrintf(" [%u] = %u\n", i, pValue[i]);
			}
		}
		else
		{
			uartPrintf("Chan %d %s = %u\n",
					   cmdSuffix1, pName, pValue[cmdSuffix1]);
		}
		return;
	}

	// Determine the multicast setting.
	if ((sscanf(pCmdArg, "%u", &mcast) != 1) || (mcast > 3))
	{
		uartPrintf("Invalid multicast setting: %s\n", pCmdArg);
		return;
	}

	// Update the channel(s) current multicast value.
	if (cmdSuffix1 < cmdSuffix2)
	{
		for (i = cmdSuffix1; i <= cmdSuffix2; i++)
		{
			pValue[i] = (uint16_t) mcast;
		}
		uartPrintf("Chan %d-%d %s set to %u\n",
				   cmdSuffix1, cmdSuffix2, pName, mcast);
	}
	else
	{
		pValue[cmdSuffix1] = (uint16_t) mcast;
		uartPrintf("Chan %d %s set to %u\n", cmdSuffix1, pName, mcast);
	}

	return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// procMCast - Process a multi-cast command.
//
// FUNCTION
//  This function processes a multi-cast command.
//
// RETURNS
//  nothing
//
static void procCrossover(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	)
{
	uint16_t *pSlotValue;		// pointer to selected slot value
	char *pSlotName;			// pointer to slot name
	int32_t i;					// loop index / counter
	uint32_t slotNum;			// slot number

	// Validate the channel Id.
	if (cmdSuffix1 < 0)
	{
		cmdSuffix1 = 0;
		cmdSuffix2 = NUM_TEST_CHANS - 1;
	}
	else if ((cmdSuffix1 >= NUM_TEST_CHANS) || (cmdSuffix2 >= NUM_TEST_CHANS))
	{
		uartPrintf("Channel exceeds max of %u!\n", NUM_TEST_CHANS - 1);
		return;
	}

	// Determine the specific channel slot number being used.
	pSlotValue = portCrossover;
	pSlotName = "Crossover enable:";


	// If the argument is null, display the current value(s).
	if (pCmdArg == NULL)
	{
		if (cmdSuffix1 < cmdSuffix2)
		{
			uartPrintf("%ss:\n", pSlotName);
			for (i = cmdSuffix1; i <= cmdSuffix2; i++)
			{
				uartPrintf(" [%u] = %u\n", i, pSlotValue[i]);
			}
		}
		else
		{
			uartPrintf("Chan %d %s = %u\n",
					   cmdSuffix1, pSlotName, pSlotValue[cmdSuffix1]);
		}
		return;
	}

	// Determine the crossover.
	if ((sscanf(pCmdArg, "%u", &slotNum) != 1) || (slotNum < 0) || (slotNum > 1))
	{
		uartPrintf("Invalid Slot Number: %s\n", pCmdArg);
		return;
	}

	// Set the channel(s) current value.
	if (cmdSuffix1 < cmdSuffix2)
	{
		for (i = cmdSuffix1; i <= cmdSuffix2; i++)
		{
			pSlotValue[i] = (uint16_t) slotNum;
		}
		uartPrintf("Chan %d-%d %ss set to %u\n",
				   cmdSuffix1, cmdSuffix2, pSlotName, slotNum);
	}
	else
	{
		pSlotValue[cmdSuffix1] = (uint16_t) slotNum;
		uartPrintf("Chan %d %s set to %u\n", cmdSuffix1, pSlotName, slotNum);
	}

	return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// procTdmSlot - Process a TDM Slot command.
//
// FUNCTION
//  This function processes a TDM Slot command.
//
// RETURNS
//  nothing
//
static void procTdmSlot(
	Bool input,					// flag: input slot
	GpakDeviceSide_t sideId,	// side of channel Id
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	)
{
	uint16_t *pSlotValue;		// pointer to selected slot value
	char *pSlotName;			// pointer to slot name
	int32_t i;					// loop index / counter
	uint32_t slotNum;			// slot number

	// Validate the channel Id.
	if (cmdSuffix1 < 0)
	{
		cmdSuffix1 = 0;
		cmdSuffix2 = NUM_TEST_CHANS - 1;
	}
	else if ((cmdSuffix1 >= NUM_TEST_CHANS) || (cmdSuffix2 >= NUM_TEST_CHANS))
	{
		uartPrintf("Channel exceeds max of %u!\n", NUM_TEST_CHANS - 1);
		return;
	}

	// Determine the specific channel slot number being used.
	if (input)
	{
		if (sideId == ADevice)
		{
			pSlotValue = chanInSlotA;
			pSlotName = "A Side Input Slot";
		}
		else
		{
			pSlotValue = chanInSlotB;
			pSlotName = "B Side Input Slot";
		}
	}
	else
	{
		if (sideId == ADevice)
		{
			pSlotValue = chanOutSlotA;
			pSlotName = "A Side Output Slot";
		}
		else
		{
			pSlotValue = chanOutSlotB;
			pSlotName = "B Side Output Slot";
		}
	}

	// If the argument is null, display the current value(s).
	if (pCmdArg == NULL)
	{
		if (cmdSuffix1 < cmdSuffix2)
		{
			uartPrintf("%ss:\n", pSlotName);
			for (i = cmdSuffix1; i <= cmdSuffix2; i++)
			{
				uartPrintf(" [%u] = %u\n", i, pSlotValue[i]);
			}
		}
		else
		{
			uartPrintf("Chan %d %s = %u\n",
					   cmdSuffix1, pSlotName, pSlotValue[cmdSuffix1]);
		}
		return;
	}

	// Determine the slot number.
	if ((sscanf(pCmdArg, "%u", &slotNum) != 1) || (slotNum > (uint32_t)NUM_TDM_SLOTS))
	{
		uartPrintf("Invalid Slot Number: %s\n", pCmdArg);
		return;
	}

	// Set the channel(s) current value.
	if (cmdSuffix1 < cmdSuffix2)
	{
		for (i = cmdSuffix1; i <= cmdSuffix2; i++)
		{
			pSlotValue[i] = (uint16_t) slotNum;
		}
		uartPrintf("Chan %d-%d %ss set to %u\n",
				   cmdSuffix1, cmdSuffix2, pSlotName, slotNum);
	}
	else
	{
		pSlotValue[cmdSuffix1] = (uint16_t) slotNum;
		uartPrintf("Chan %d %s set to %u\n", cmdSuffix1, pSlotName, slotNum);
	}

	return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// procChannelIoGain - Process a Channel I/O Gain command.
//
// FUNCTION
//  This function processes a Channel I/O Gain command.
//
// RETURNS
//  nothing
//
static void procChannelIoGain(
	Bool input,					// flag: input gain
	GpakDeviceSide_t sideId,	// side of channel Id
    GpakAlgCtrl_t controlCode,  // non-zero: real-time adjust control code
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	)
{
	int16_t *pGainValue;		// pointer to selected gain value
	char *pGainName;			// pointer to gain name
	int32_t i;					// loop index / counter
	int gain;					// entered gain value
	GPAK_AlgControlStat_t algStatus;
    ADT_UInt16            channelId;
    ADT_Int16             GaindB;
    GpakApiStatus_t       status;
    ADT_UInt32 DspId = 0;

	// Validate the channel Id.
	if (cmdSuffix1 < 0)
	{
		cmdSuffix1 = 0;
		cmdSuffix2 = NUM_TEST_CHANS - 1;
	}
	else if ((cmdSuffix1 >= NUM_TEST_CHANS) || (cmdSuffix2 >= NUM_TEST_CHANS))
	{
		uartPrintf("Channel exceeds max of %u!\n", NUM_TEST_CHANS - 1);
		return;
	}

	// Determine the specific channel gain being used.
	if (input)
	{
		if (sideId == ADevice)
		{
			pGainValue = chanInGainA;
			pGainName = "A Side Input Gain";
		}
		else
		{
			pGainValue = chanInGainB;
			pGainName = "B Side Input Gain";
		}
	}
	else
	{
		if (sideId == ADevice)
		{
			pGainValue = chanOutGainA;
			pGainName = "A Side Output Gain";
		}
		else
		{
			pGainValue = chanOutGainB;
			pGainName = "B Side Output Gain";
		}
	}

	// If the argument is null, display the current value(s).
	if (pCmdArg == NULL)
	{
		if (cmdSuffix1 < cmdSuffix2)
		{
			uartPrintf("%ss:\n", pGainName);
			for (i = cmdSuffix1; i <= cmdSuffix2; i++)
			{
				uartPrintf(" [%u] = %d\n", i, pGainValue[i]);
			}
		}
		else
		{
			uartPrintf("Chan %d %s = %d\n",
					   cmdSuffix1, pGainName, pGainValue[cmdSuffix1]);
		}
		return;
	}

	// Determine the gain value.
	if ((sscanf(pCmdArg, "%d", &gain) == 1)) {
#if 0 // DSP will range-check
        if (gain != -32768) { // if not 'mute' special value of 0x8000, then range check
            if ((gain < -40)|| (gain > 40)) {  
                uartPrintf("Invalid Gain: %s\n", pCmdArg);
		        return;
            }
        }
#endif
	} else {
		uartPrintf("Invalid Gain: %s\n", pCmdArg);
		return;
	}

	// Set the channel(s) current value.
	if (cmdSuffix1 < cmdSuffix2)
	{
		for (i = cmdSuffix1; i <= cmdSuffix2; i++)
		{
			pGainValue[i] = (int16_t) gain;
		}
		uartPrintf("Chan %d-%d %ss set to %d\n",
				   cmdSuffix1, cmdSuffix2, pGainName, gain);
	}
	else
	{
		pGainValue[cmdSuffix1] = (int16_t) gain;
		uartPrintf("Chan %d %s set to %d\n", cmdSuffix1, pGainName, gain);
	}

    if (controlCode != 0) {
	    channelId = cmdSuffix1;
        GaindB = pGainValue[cmdSuffix1];
	    status =  gpakAlgControl (DspId, PackCore(chanCore[channelId],channelId), controlCode,0,0,sideId,0,
                              GaindB, NullCodec, &algStatus); 
	    if (status == GpakApiSuccess)
	    {
		    uartPrintf("Chan %d algControl success %d\n", channelId, GaindB);
	    }
	    else
	    {
		    uartPrintf("Gain Update failed on Chan %d, error %d\n", channelId, status);
	    }
    }
	return;
}
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// procChannelRTPControl - Dynamic RTP Tx/RX control.
//
// FUNCTION
//  This function processes a Channel Algorithm RTP Control
//
// RETURNS
//  nothing
//
static void procChannelRTPControl(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	)
{
	GpakActivation *pEnable;	// pointer to selected algorithm enable
	char *pAlgoName;			// pointer to algorithm name
	int32_t i;					// loop index / counter
	Bool parsedState;			// flag: parsed state
	GpakApiStatus_t  status;		

	ADT_UInt32 DspId = 0;
	ADT_UInt16 channelId;
    GpakAlgCtrl_t ControlCode;
	GpakToneTypes DeactTonetype = 0;
    GpakToneTypes ActTonetype = 0;
	GpakDeviceSide_t AorBSide = ADevice;
	ADT_UInt16 NLPsetting = 0;
    ADT_Int16 GaindB = 0;
	GpakCodecs CodecType = PCMU_64;
	GPAK_AlgControlStat_t pStatus;

	// Validate the channel Id.
	if ((cmdSuffix1 < 0) || (cmdSuffix1 >= NUM_TEST_CHANS))
	{
		uartPrintf("invalid channel ID: %d\n",cmdSuffix1);
        return;
	}

    if (pCmdArg == NULL) {
		uartPrintf("missing parameter: RX0 | RX1 | TX0 | TX1 \n");
        return;
    }

	if (strcmpi(pCmdArg, "RX1") == 0)
	    ControlCode = RTPReceiveEnable;
	else if (strcmpi(pCmdArg, "RX0") == 0)
	    ControlCode = RTPReceiveDisable;
	else if (strcmpi(pCmdArg, "TX1") == 0)
	    ControlCode = RTPTransmitEnable;
	else if (strcmpi(pCmdArg, "TX0") == 0)
	    ControlCode = RTPTransmitDisable;
    else {
		uartPrintf("incorrect parameter: RX0 | RX1 | TX0 | TX1 \n");
        return;
    }

	channelId = cmdSuffix1;
	status =  gpakAlgControl (DspId, PackCore(chanCore[channelId],channelId),
                                               ControlCode, DeactTonetype,
                                               ActTonetype, AorBSide, NLPsetting,
                                               GaindB,  CodecType, &pStatus);
	// Set the channel(s) current value.
	if (status == GpakApiSuccess)
	{
		uartPrintf("Chan %d RTP control success: %s\n", channelId, pCmdArg);
	}
	else
	{
		uartPrintf("RTP control failed on Chan %d, error %d\n", channelId, status);
	}

	return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// procChannelResetAEC - Reset a Channels AEC.
//
// FUNCTION
//  This function processes a Channel Algorithm Control Reset AEC command.
//
// RETURNS
//  nothing
//
static void procChannelResetAEC(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	)
{
	GpakActivation *pEnable;	// pointer to selected algorithm enable
	char *pAlgoName;			// pointer to algorithm name
	int32_t i;					// loop index / counter
	Bool parsedState;			// flag: parsed state
	GpakApiStatus_t  status;		

	ADT_UInt32 DspId = 0;
	ADT_UInt16 channelId;
    GpakAlgCtrl_t ControlCode;
	GpakToneTypes DeactTonetype = 0;
    GpakToneTypes ActTonetype = 0;
	GpakDeviceSide_t AorBSide = ADevice;
	ADT_UInt16 NLPsetting = 0;
    ADT_Int16 GaindB = 0;
	GpakCodecs CodecType = PCMU_64;
	GPAK_AlgControlStat_t pStatus;

	// Validate the channel Id.
	if (cmdSuffix1 < 0)
	{
		cmdSuffix1 = 0;
		cmdSuffix2 = NUM_TEST_CHANS - 1;
	}
	else if (cmdSuffix1 >= NUM_TEST_CHANS)
	{
		uartPrintf("Channel exceeds max of %u!\n", NUM_TEST_CHANS - 1);
		return;
	}
	ControlCode = ResetEcan;
	channelId = cmdSuffix1;
	status =  gpakAlgControl (DspId, PackCore(chanCore[channelId],channelId),
                                               ControlCode, DeactTonetype,
                                               ActTonetype, AorBSide, NLPsetting,
                                               GaindB,  CodecType, &pStatus);
	// Set the channel(s) current value.
	if (status == GpakApiSuccess)
	{
		uartPrintf("Chan %d AEC Reset\n", channelId);
	}
	else
	{
		uartPrintf("AEC Reset failed on Chan %d, error %d\n", channelId, status);
	}

	return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// procChannelBypasssAEC - Bypass a Channels AEC.
//
// FUNCTION
//  This function processes a Channel Algorithm Enable command.
//
// RETURNS
//  nothing
//
static void procChannelBypassAEC(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	)
{
	GpakActivation *pEnable;	// pointer to selected algorithm enable
	char *pAlgoName;			// pointer to algorithm name
	int32_t i;					// loop index / counter
	Bool parsedState;			// flag: parsed state
	GpakApiStatus_t  status;		

	ADT_UInt32 DspId = 0;
	ADT_UInt16 channelId;
    GpakAlgCtrl_t ControlCode;
	GpakToneTypes DeactTonetype = 0;
    GpakToneTypes ActTonetype = 0;
	GpakDeviceSide_t AorBSide = 0;
	ADT_UInt16 NLPsetting = 0;
    ADT_Int16 GaindB = 0;
	GpakCodecs CodecType = PCMU_64;
	GPAK_AlgControlStat_t pStatus;

	// Validate the channel Id.
	if (cmdSuffix1 < 0)
	{
		cmdSuffix1 = 0;
		cmdSuffix2 = NUM_TEST_CHANS - 1;
	}
	else if (cmdSuffix1 >= NUM_TEST_CHANS)
	{
		uartPrintf("Channel exceeds max of %u!\n", NUM_TEST_CHANS - 1);
		return;
	}
	ControlCode = BypassEcan;
	channelId = cmdSuffix1;
	status =  gpakAlgControl (DspId, PackCore(chanCore[channelId],channelId),
                                               ControlCode, DeactTonetype,
                                               ActTonetype, AorBSide, NLPsetting,
                                               GaindB,  CodecType, &pStatus);
	// Set the channel(s) current value.
	if (status == GpakApiSuccess)
	{
		uartPrintf("Chan %d AEC Bypass\n", channelId);
	}
	else
	{
		uartPrintf("AEC Bypass failed on Chan %d, error %d\n", channelId, status);
	}

	return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// procChannelEnableAEC - Enable a Channels AEC.
//
// FUNCTION
//  This function processes a Channel Algorithm Enable command.
//
// RETURNS
//  nothing
//
static void procChannelEnableAEC(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	)
{
	Bool parsedState;			// flag: parsed state
	GpakApiStatus_t  status;		

	ADT_UInt32 DspId = 0;
	ADT_UInt16 channelId;
    GpakAlgCtrl_t ControlCode;
	GpakToneTypes DeactTonetype = 0;
    GpakToneTypes ActTonetype = 0;
	GpakDeviceSide_t AorBSide = 0;
	ADT_UInt16 NLPsetting = 0;
    ADT_Int16 GaindB = 0;
	GpakCodecs CodecType = PCMU_64;
	GPAK_AlgControlStat_t pStatus;

	// Validate the channel Id.
	if (cmdSuffix1 < 0)
	{
		cmdSuffix1 = 0;
		cmdSuffix2 = NUM_TEST_CHANS - 1;
	}
	else if (cmdSuffix1 >= NUM_TEST_CHANS)
	{
		uartPrintf("Channel exceeds max of %u!\n", NUM_TEST_CHANS - 1);
		return;
	}
	ControlCode = EnableEcan;
	channelId = cmdSuffix1;
	status =  gpakAlgControl (DspId, PackCore(chanCore[channelId],channelId),
                                               ControlCode, DeactTonetype,
                                               ActTonetype, AorBSide, NLPsetting,
                                               GaindB,  CodecType, &pStatus);
	// Set the channel(s) current value.
	if (status == GpakApiSuccess)
	{
		uartPrintf("Chan %d AEC Enabled\n", channelId);
	}
	else
	{
		uartPrintf("AEC Enabled failed on Chan %d, error %d\n", channelId, status);
	}

	return;
}
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// procChannelAlgoEnable - Process a Channel Algorithm Enable command.
//
// FUNCTION
//  This function processes a Channel Algorithm Enable command.
//
// RETURNS
//  nothing
//
static void procChannelAlgoEnable(
	chanAlgoId_t algoId,		// algorithm Id
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	)
{
	GpakActivation *pEnable;	// pointer to selected algorithm enable
	char *pAlgoName;			// pointer to algorithm name
	int32_t i;					// loop index / counter
	Bool parsedState;			// flag: parsed state
	GpakActivation state;		// selected enable state

	// Validate the channel Id.
	if (cmdSuffix1 < 0)
	{
		cmdSuffix1 = 0;
		cmdSuffix2 = NUM_TEST_CHANS - 1;
	}
	else if ((cmdSuffix1 >= NUM_TEST_CHANS) || (cmdSuffix2 >= NUM_TEST_CHANS))
	{
		uartPrintf("Channel exceeds max of %u!\n", NUM_TEST_CHANS - 1);
		return;
	}

	// Determine the specific algorithm enable being used.
	if (algoId == algoAec)
	{
		pEnable = chanAecEnable;
		pAlgoName = "AEC";
	}
	else if (algoId == algoLec)
	{
		pEnable = chanLecEnable;
		pAlgoName = "LEC";
	}
	else if (algoId == algoAgc)
	{
		pEnable = chanAgcEnable;
		pAlgoName = "AGC";
	}
    else if (algoId == algoRtcp)
	{
		pEnable = chanRtcpEnable;
		pAlgoName = "RTCP";
    } 
    else if (algoId == algoVadCng)
	{
		pEnable = chanVadCngEnable;
		pAlgoName = "VADCNG";
    }
    else if (algoId == algoDtx)
	{
		pEnable = chanDtxEnable;
		pAlgoName = "DTX";
    }
    else if (algoId == algoData)
	{
		pEnable = chanDataEnable;
		pAlgoName = "DATA";
	} else {
		uartPrintf("algo not accepted\n");
		return;
	}
	// If the argument is null, display the current value(s).
	if (pCmdArg == NULL)
	{
		if (cmdSuffix1 < cmdSuffix2)
		{
			uartPrintf("%s Enables:\n", pAlgoName);
			for (i = cmdSuffix1; i <= cmdSuffix2; i++)
			{
				uartPrintf(" [%u] = %s\n", i, algoStateName(pEnable[i]));
			}
		}
		else
		{
			uartPrintf("Chan %d %s Enable = %s\n",
					   cmdSuffix1, pAlgoName,
					   algoStateName(pEnable[cmdSuffix1]));
		}
		return;
	}

	// Determine the algorithm enable state selection.
	if (!parseBooleanValue(pCmdArg, &parsedState))
	{
		uartPrintf("Invalid State: %s\n", pCmdArg);
		return;
	}
	if (parsedState)
	{
		state = Enabled;
	}
	else
	{
		state = Disabled;
	}

	// Set the channel(s) current value.
	if (cmdSuffix1 < cmdSuffix2)
	{
		for (i = cmdSuffix1; i <= cmdSuffix2; i++)
		{
			pEnable[i] = state;
		}
		uartPrintf("Chan %d-%d %s Enables set to %s\n",
				   cmdSuffix1, cmdSuffix2, pAlgoName, algoStateName(state));
	}
	else
	{
		pEnable[cmdSuffix1] = state;
		uartPrintf("Chan %d %s Enable set to %s\n",
				   cmdSuffix1, pAlgoName, algoStateName(state));
	}

	return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// algoStateName - Determine the name of an Algorithm State.
//
// FUNCTION
//  This function determines the name of an Algorithm State.
//
// RETURNS
//  Pointer to text string for specified Algorithm State.
//
static char *algoStateName(
	GpakActivation algoState	// algorithm state
	)
{

	if (algoState == Enabled)
	{
		return "Enabled";
	}
	else
	{
		return "Disabled";
	}
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// parseBooleanValue - Parse a boolean value string.
//
// FUNCTION
//  This function parses a boolean value string.
//
// RETURNS
//  True if a valid boolean value or False if invalid.
//
static Bool parseBooleanValue(
	char *pString,				// pointer to string
	Bool *pValue				// pointer to variable for storing parsed value
	)
{

	// Check for asserted values.
	if ((strcmpi(pString, "YES") == 0) ||
		(strcmpi(pString, "TRUE") == 0) ||
		(strcmpi(pString, "ENABLED") == 0) ||
		(strcmpi(pString, "ON") == 0) ||
		(strcmpi(pString, "START") == 0) ||
		(strcmpi(pString, "1") == 0))
	{
		*pValue = TRUE;
	}

	// Check for non asserted values.
	else if ((strcmpi(pString, "NO") == 0) ||
			 (strcmpi(pString, "FALSE") == 0) ||
			 (strcmpi(pString, "DISABLED") == 0) ||
			 (strcmpi(pString, "OFF") == 0) ||
			 (strcmpi(pString, "STOP") == 0) ||
			 (strcmpi(pString, "0") == 0))
	{
		*pValue = FALSE;
	}

	else
	{
		*pValue = FALSE;
		return FALSE;
	}

	return TRUE;
}

GpakToneTypes getToneType(ADT_UInt16 channelId, GpakDeviceSide_t side) {
chanDetectId_t detectId;
    if (side == ADevice)
        detectId = chanDetectA[channelId];
    else 
        detectId = chanDetectB[channelId];

    return toneTypeMap[detectId];
}

static Bool parseDetectorValue(
	char *pString,				// pointer to string
	chanDetectId_t *pValue	    // pointer to variable for storing parsed value
	)
{

	// Check for asserted values.
	if ((strcmpi(pString, "NONE") == 0) ||
	    (strcmpi(pString, "DISABLED") == 0) ||
		(strcmpi(pString, "OFF") == 0) ||
		(strcmpi(pString, "STOP") == 0) ||
		(strcmpi(pString, "0") == 0))
    {
        *pValue = detect_none;
        return TRUE;
    }
	if(strcmpi(pString, "DTMF") == 0) {
        *pValue = detect_dtmf;
        return TRUE;
    }
	if (strcmpi(pString, "CPRG") == 0) {
        *pValue = detect_cprg;
        return TRUE;
    }
	if (strcmpi(pString, "MFR2F") == 0) {
        *pValue = detect_mfr2f;
        return TRUE;
    }
	if (strcmpi(pString, "MFR2R") == 0) {
        *pValue = detect_mfr2r;
        return TRUE;
    }
	if (strcmpi(pString, "ARB") == 0) {
        *pValue = detect_arb;
        return TRUE;
    }
	if (strcmpi(pString, "MFR1") == 0) {
        *pValue = detect_mfr1;
        return TRUE;
    }
	if (strcmpi(pString, "FAX") == 0) {
        *pValue = detect_fax;
        return TRUE;
    }

	else
	{
		*pValue = FALSE;
		return FALSE;
	}

	return TRUE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// procChannelDetectA- Process a Channel Detector A
//
// FUNCTION
//  This function processes a Channel Detector command.
//
// RETURNS
//  nothing
//
static void procChannelDetect(
	GpakDeviceSide_t sideId,		// side of channel Id 
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	)
{
	GpakActivation *pEnable;	// pointer to selected algorithm enable
	char *pAlgoName;			// pointer to algorithm name
	int32_t i;					// loop index / counter
	Bool parsedState;			// flag: parsed state
	GpakActivation state;		// selected enable state
    chanDetectId_t detectId;
    chanDetectId_t *pDet;
    char *pSide;

    if (sideId == ADevice) {
        pDet = chanDetectA;
        pSide = sideString[0];
    } else {
        pDet = chanDetectB;
        pSide = sideString[1];
    }

	// Validate the channel Id.
	if (cmdSuffix1 < 0)
	{
		cmdSuffix1 = 0;
		cmdSuffix2 = NUM_TEST_CHANS - 1;
	}
	else if ((cmdSuffix1 >= NUM_TEST_CHANS) || (cmdSuffix2 >= NUM_TEST_CHANS))
	{
		uartPrintf("Channel exceeds max of %u!\n", NUM_TEST_CHANS - 1);
		return;
	}

	// If the argument is null, display the current value(s).
	if (pCmdArg == NULL)
	{
		if (cmdSuffix1 < cmdSuffix2)
		{
			uartPrintf("%s Detector: \n", pSide);
			for (i = cmdSuffix1; i <= cmdSuffix2; i++)
			{
				uartPrintf(" [%u] = %s \n", i, detectorNames[pDet[i]] );
			}
		}
		else
		{
            uartPrintf(" [%u] = %s \n", cmdSuffix1, detectorNames[pDet[cmdSuffix1]] );
		}
		return;
	}

    if (parseDetectorValue(pCmdArg, &detectId) == FALSE) {
		uartPrintf("Invalid Detector: %s\n", pCmdArg);
        return;
    }

	// Set the channel(s) current value.
	if (cmdSuffix1 < cmdSuffix2)
	{
		for (i = cmdSuffix1; i <= cmdSuffix2; i++)
		{
            pDet[i] = detectId;
		}
		uartPrintf("Chan %d-%d %s Detector set to %s\n",
				   cmdSuffix1, cmdSuffix2, pSide, detectorNames[pDet[cmdSuffix1]] );
	}
	else
	{
        pDet[cmdSuffix1] = detectId;
		uartPrintf("Chan %d %s Detector set to %s\n",
				   cmdSuffix1, pSide, detectorNames[pDet[cmdSuffix1]] );
	}

	return;
}

static void procDtmfMode(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	)
{
	int32_t i;					// loop index / counter
    int32_t mode = 0;

	// Validate the channel Id.
	if (cmdSuffix1 < 0)
	{
		cmdSuffix1 = 0;
		cmdSuffix2 = NUM_TEST_CHANS - 1;
	}
	else if ((cmdSuffix1 >= NUM_TEST_CHANS) || (cmdSuffix2 >= NUM_TEST_CHANS))
	{
		uartPrintf("Channel exceeds max of %u!\n", NUM_TEST_CHANS - 1);
		return;
	}

	// If the argument is null, display the current value(s).
	if (pCmdArg == NULL)
	{
		if (cmdSuffix1 < cmdSuffix2)
		{
			uartPrintf("DTMF MODE: \n");
			for (i = cmdSuffix1; i <= cmdSuffix2; i++)
			{
                mode = dtmfMode[i] & 3;
				uartPrintf(" [%u] = %s \n", i, dtmf_mode_str[mode]);
			}
		}
		else
		{
            mode = dtmfMode[cmdSuffix1] & 3;
	        uartPrintf(" [%u] = %s \n", cmdSuffix1, dtmf_mode_str[mode]);
		}
		return;
	}
	if ((sscanf(pCmdArg, "%u", &mode) != 1) || (mode > 3))
	{
		uartPrintf("Invalid Mode: %s\n", pCmdArg);
		return;
	}

	// Set the channel(s) current value.
	if (cmdSuffix1 < cmdSuffix2)
	{
		for (i = cmdSuffix1; i <= cmdSuffix2; i++)
		{
            dtmfMode[i] = mode;
		}
		uartPrintf("Chan %d-%d DTMF Mode set to %s\n",
				   cmdSuffix1, cmdSuffix2, dtmf_mode_str[mode]);
	}
	else
	{
        dtmfMode[cmdSuffix1] = mode;
		uartPrintf("Chan %d DTMF Mode set to %s\n",
				   cmdSuffix1, dtmf_mode_str[mode]);
	}

	return;
}

static void getDtmfDetectFlags(ADT_UInt16 channelId, GpakToneTypes *ADet, GpakToneTypes *BDet) {
    if (dtmfMode[channelId] == DTMF_MODE_RELAY) {
        // rfc 2833 tone relay and regeneration
	    *ADet |= (DTMF_tone  | Tone_Relay);        
	    *BDet |= (Tone_Regen | Notify_Host);
    }
    else if (dtmfMode[channelId] == DTMF_MODE_OUTOFBAND) {
        // out of band... detect, suppress tone, notify host
	    *ADet |= (DTMF_tone | Tone_Squelch | Notify_Host);
    } 
    else if (dtmfMode[channelId] == DTMF_MODE_INBAND) {
        // pass through unaltered...no suppression, notify host
	    *ADet |= DTMF_tone | Notify_Host;
    }
}
static void procFaxMode(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	)
{
	int32_t i;					// loop index / counter
    int32_t mode = 0;

	// Validate the channel Id.
	if (cmdSuffix1 < 0)
	{
		cmdSuffix1 = 0;
		cmdSuffix2 = NUM_TEST_CHANS - 1;
	}
	else if ((cmdSuffix1 >= NUM_TEST_CHANS) || (cmdSuffix2 >= NUM_TEST_CHANS))
	{
		uartPrintf("Channel exceeds max of %u!\n", NUM_TEST_CHANS - 1);
		return;
	}

	// If the argument is null, display the current value(s).
	if (pCmdArg == NULL)
	{
		if (cmdSuffix1 < cmdSuffix2)
		{
			uartPrintf("FAX MODE: \n");
			for (i = cmdSuffix1; i <= cmdSuffix2; i++)
			{
                mode = chanFaxMode[i] & 3;
				uartPrintf(" [%u] = %s \n", i, fax_mode_str[mode]);
			}
		}
		else
		{
            mode = chanFaxMode[cmdSuffix1] & 3;
	        uartPrintf(" [%u] = %s \n", cmdSuffix1, fax_mode_str[mode]);
		}
		return;
	}

    if ((pCmdArg[0] == 'd') || (pCmdArg[0] == 'D'))
        mode =  (int32_t)disabled;
    else if ((pCmdArg[0] == 'f') || (pCmdArg[0] == 'F'))
        mode = (int32_t)faxOnly;
    else if ((pCmdArg[0] == 'v') || (pCmdArg[0] == 'V'))
        mode = (int32_t)faxVoice;
    else 
		uartPrintf("Invalid Fax Mode: %s... must be D,F,V (disabled, fax-only, fax-voice)\n", pCmdArg);

	// Set the channel(s) current value.
	if (cmdSuffix1 < cmdSuffix2)
	{
		for (i = cmdSuffix1; i <= cmdSuffix2; i++)
		{
            chanFaxMode[i] = (GpakFaxMode_t)mode;
		}
		uartPrintf("Chan %d-%d FAX Mode set to %s\n",
				   cmdSuffix1, cmdSuffix2, fax_mode_str[mode]);
	}
	else
	{
        chanFaxMode[cmdSuffix1] = mode;
		uartPrintf("Chan %d FAX Mode set to %s\n",
				   cmdSuffix1, fax_mode_str[mode]);
	}
	return;
}
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// procCIDMode - Process a CID Mode command.
//
// FUNCTION
//  This function processes an RTP Port command.
//
// RETURNS
//  nothing
//
static void procCIDMode(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	)
{
	int32_t i;					// loop index / counter
	uint32_t cidMode;

	// Validate the channel Id.
	if (cmdSuffix1 < 0)
	{
		cmdSuffix1 = 0;
		cmdSuffix2 = NUM_TEST_CHANS - 1;
	}
	else if ((cmdSuffix1 >= NUM_TEST_CHANS) || (cmdSuffix2 >= NUM_TEST_CHANS))
	{
		uartPrintf("Channel exceeds max of %u!\n", NUM_TEST_CHANS - 1);
		return;
	}

	// If the argument is null, display the current value(s).
	if (pCmdArg == NULL)
	{
		if (cmdSuffix1 < cmdSuffix2)
		{
			for (i = cmdSuffix1; i <= cmdSuffix2; i++)
			{
				uartPrintf(" [%u] = %s\n", i, cidmode_string[chanCIDMode[i]]);
			}
		}
		else
		{
			uartPrintf("Chan %d CID mode = %s\n",
					   cmdSuffix1, cidmode_string[chanCIDMode[cmdSuffix1]]);
		}
		return;
	}

    cidMode = 1000;
    if (pCmdArg[0] == 'D') {
        cidMode = 0;
    } 
    else if ((pCmdArg[0] == 'R') || (pCmdArg[0] == 'r')) {
        if (pCmdArg[1] == '1')
            cidMode = 1;
        else if (pCmdArg[1] == '2') 
            cidMode = 2;
    } 
    else if ((pCmdArg[0] == 'T') || (pCmdArg[0] == 't')) {
        if (pCmdArg[1] == '1')
            cidMode = 3;
        else if (pCmdArg[1] == '2') 
            cidMode = 4;
    }

	if (cidMode > 4)
	{
		uartPrintf("Invalid CID MODE: %s\n", pCmdArg);
		return;
	}

	// Set the channel(s) current value.
	if (cmdSuffix1 < cmdSuffix2)
	{
		for (i = cmdSuffix1; i <= cmdSuffix2; i++)
		{
			chanCIDMode[i] = cidMode;
		}
		uartPrintf("Chan %d-%d CIDMode set to %s\n",
				   cmdSuffix1, cmdSuffix2, cidmode_string[cidMode]);
	}
	else
	{
		chanCIDMode[cmdSuffix1] = cidMode;
		uartPrintf("Chan %d CIDMode set to %s\n",
				   cmdSuffix1, cidmode_string[cidMode]);
	}

	return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// procRtpIpAddress - Process an RTP IP Address command.
//
// FUNCTION
//  This function processes an RTP IP Address command.
//
// RETURNS
//  nothing
//
static void procRtpIpAddress(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	)
{
	int32_t i;					// loop index / counter
	uint32_t arg1;				// argument 1
	uint32_t arg2;				// argument 2
	uint32_t arg3;				// argument 3
	uint32_t arg4;				// argument 4
	uint32_t arg5;				// argument 5
	uint32_t arg6;				// argument 6
	uint32_t arg7;				// argument 7
	uint32_t arg8;				// argument 8
	uint32_t ipAddress;			// IP address
	uint16_t ipAddresstest;			// IP address
	uint8_t  ipV6Address[16];	// IPv6 address
    IP6N destAddr;

	// Validate the channel Id.
	if (cmdSuffix1 < 0)
	{
		cmdSuffix1 = 0;
		cmdSuffix2 = NUM_TEST_CHANS - 1;
	}
	else if ((cmdSuffix1 >= NUM_TEST_CHANS) || (cmdSuffix2 >= NUM_TEST_CHANS))
	{
		uartPrintf("Channel exceeds max of %u!\n", NUM_TEST_CHANS - 1);
		return;
	}

	// If the argument is null, display the current value(s).
	if (pCmdArg == NULL)
	{
		if (cmdSuffix1 < cmdSuffix2)
		{
			uartPrintf("Destination RTP IP Addresses:\n");
			for (i = cmdSuffix1; i <= cmdSuffix2; i++)
			{
                if (chanIpVersion[i] == 4) {
				    uartPrintf(" [%u] = %s\n",
						   i, ipAddressToString(chanDestRtpIp[i]));
                } else {
		            uartPrintf(" [%u] = %s\n",
				           i, ipV6AddressToString(chanDestRtpV6Ip[i].IpV6Address));
                }
			}
		}
		else
		{
            if (chanIpVersion[cmdSuffix1] == 4) {
			    uartPrintf("Chan %d destination RTP IP address = %s\n",
					   cmdSuffix1,
					   ipAddressToString(chanDestRtpIp[cmdSuffix1]));
            } else {
			    uartPrintf("Chan %d destination RTP IP address = %s\n",
					   cmdSuffix1,
			           ipV6AddressToString(chanDestRtpV6Ip[cmdSuffix1].IpV6Address));
            }
		}
		return;
	}
    if (IPv6StringToIPAddress (pCmdArg, &destAddr) == 0) {
		if (cmdSuffix1 < cmdSuffix2)
		{
			for (i = cmdSuffix1; i <= cmdSuffix2; i++)
			{
				chanIpVersion[i] = 6;
			}
		}
		else
		{
			chanIpVersion[cmdSuffix1] = 6;
		}
    }
	else if ((sscanf(pCmdArg, "%u.%u.%u.%u", &arg1, &arg2, &arg3, &arg4) == 4) &&
		(arg1 <= 255) && (arg2 <= 255) && (arg3 <= 255) && (arg4 <= 255))
	{
		ipAddress = (arg1 << 24) | (arg2 << 16) | (arg3 << 8) | arg4;
		if (cmdSuffix1 < cmdSuffix2)
		{
			for (i = cmdSuffix1; i <= cmdSuffix2; i++)
			{
				chanIpVersion[i] = 4;
			}
		}
		else
		{
			chanIpVersion[cmdSuffix1] = 4;
		}
	}
	else
	{
		uartPrintf("Invalid IP Address: %s\n", pCmdArg);
		return;
	}

	// Set the channel(s) current value.
	if (cmdSuffix1 < cmdSuffix2)
	{
		for (i = cmdSuffix1; i <= cmdSuffix2; i++)
		{
			if(chanIpVersion[i] == 4)
			{			
			chanDestRtpIp[i] = ipAddress;
			}
			else if(chanIpVersion[i] == 6)
			{
				memcpy(&chanDestRtpV6Ip[i].IpV6Address, &destAddr.u.addr8,  sizeof(GpakIPv6_t));
			}
		}
		if(chanIpVersion[cmdSuffix1] == 4)
		{	
		uartPrintf("Chan %d-%d destination RTP IP Addresses set to %s\n",
				   cmdSuffix1, cmdSuffix2, ipAddressToString(ipAddress));
		}
		else if(chanIpVersion[cmdSuffix1] == 6)
		{
			uartPrintf("Chan %d-%d destination RTP IP Addresses set to %s\n",
					   cmdSuffix1, cmdSuffix2, ipV6AddressToString(&chanDestRtpV6Ip[cmdSuffix1].IpV6Address)); //ipV6Address[0]
		}
	}
	else
	{
		if(chanIpVersion[cmdSuffix1] == 4)
	    {
		chanDestRtpIp[cmdSuffix1] = ipAddress;
		uartPrintf("Chan %d destination RTP IP address set to %s\n",
				   cmdSuffix1, ipAddressToString(ipAddress));
	}
		else if(chanIpVersion[cmdSuffix1] == 6)
		{
            memcpy(&chanDestRtpV6Ip[cmdSuffix1].IpV6Address, &destAddr.u.addr8,  sizeof(GpakIPv6_t));
		    uartPrintf("Chan %d destination RTP IP address set to %s\n",
				      cmdSuffix1, ipV6AddressToString(&chanDestRtpV6Ip[cmdSuffix1].IpV6Address));
		}
	}
	return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ipAddressToString - Convert an IP address to a string.
//
// FUNCTION
//  This function converts an IP address to a string.
//
// RETURNS
//  Pointer to string.
//
static char *ipAddressToString(
	ADT_UInt32 ipAddress		// IP address
	)
{

	sprintf(ipAddrsStringBufr, "%u.%u.%u.%u",
			(ipAddress >> 24) & 0xFF, (ipAddress >> 16) & 0xFF,
			(ipAddress >> 8) & 0xFF, ipAddress  & 0xFF);
	return ipAddrsStringBufr;
}
static char *ipAddressToString1(
	ADT_UInt32 ipAddress		// IP address
	)
{
	sprintf(ipAddrsStringBufr1, "%u.%u.%u.%u",
			(ipAddress >> 24) & 0xFF, (ipAddress >> 16) & 0xFF,
			(ipAddress >> 8) & 0xFF, ipAddress  & 0xFF);
	return ipAddrsStringBufr1;
}

static char *ipV6AddressToString(
	ADT_UInt8  *ipAddress		// IP address
	)
{
    InetNtopA(AF_INET6, ipAddress, ipAddrsStringBufr, 48);
	return ipAddrsStringBufr;
}
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// procRtpPort - Process an RTP Port command.
//
// FUNCTION
//  This function processes an RTP Port command.
//
// RETURNS
//  nothing
//
static void procRtpPort(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	)
{
	int32_t i;					// loop index / counter
	uint32_t portNum;			// port number

	// Validate the channel Id.
	if (cmdSuffix1 < 0)
	{
		cmdSuffix1 = 0;
		cmdSuffix2 = NUM_TEST_CHANS - 1;
	}
	else if ((cmdSuffix1 >= NUM_TEST_CHANS) || (cmdSuffix2 >= NUM_TEST_CHANS))
	{
		uartPrintf("Channel exceeds max of %u!\n", NUM_TEST_CHANS - 1);
		return;
	}

	// If the argument is null, display the current value(s).
	if (pCmdArg == NULL)
	{
		if (cmdSuffix1 < cmdSuffix2)
		{
			uartPrintf("Destination RTP Ports:\n");
			for (i = cmdSuffix1; i <= cmdSuffix2; i++)
			{
				uartPrintf(" [%u] = %u\n", i, chanDestRtpPort[i]);
			}
		}
		else
		{
			uartPrintf("Chan %d destination RTP port = %u\n",
					   cmdSuffix1, chanDestRtpPort[cmdSuffix1]);
		}
		return;
	}

	// Determine the port number.
	if ((sscanf(pCmdArg, "%u", &portNum) != 1) || (portNum > 65535))
	{
		uartPrintf("Invalid Port Number: %s\n", pCmdArg);
		return;
	}

	// Set the channel(s) current value.
	if (cmdSuffix1 < cmdSuffix2)
	{
		for (i = cmdSuffix1; i <= cmdSuffix2; i++)
		{
			chanDestRtpPort[i] = portNum;
		}
		uartPrintf("Chan %d-%d destination RTP ports set to %u\n",
				   cmdSuffix1, cmdSuffix2, portNum);
	}
	else
	{
		chanDestRtpPort[cmdSuffix1] = portNum;
		uartPrintf("Chan %d destination RTP port set to %u\n",
				   cmdSuffix1, portNum);
	}

	return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// procRtpPort - Process an RTP Port command.
//
// FUNCTION
//  This function processes an RTP Port command.
//
// RETURNS
//  nothing
//
static void procSRtpPort(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	)
{
	int32_t i;					// loop index / counter
	uint32_t portNum;			// port number

	// Validate the channel Id.
	if (cmdSuffix1 < 0)
	{
		cmdSuffix1 = 0;
		cmdSuffix2 = NUM_TEST_CHANS - 1;
	}
	else if ((cmdSuffix1 >= NUM_TEST_CHANS) || (cmdSuffix2 >= NUM_TEST_CHANS))
	{
		uartPrintf("Channel exceeds max of %u!\n", NUM_TEST_CHANS - 1);
		return;
	}

	// If the argument is null, display the current value(s).
	if (pCmdArg == NULL)
	{
		if (cmdSuffix1 < cmdSuffix2)
		{
			uartPrintf("Source RTP Ports:\n");
			for (i = cmdSuffix1; i <= cmdSuffix2; i++)
			{
				uartPrintf(" [%u] = %u\n", i, chanSourceRtpPort[i]);
			}
		}
		else
		{
			uartPrintf("Chan %d source RTP port = %u\n",
					   cmdSuffix1, chanSourceRtpPort[cmdSuffix1]);
		}
		return;
	}

	// Determine the port number.
	if ((sscanf(pCmdArg, "%u", &portNum) != 1) || (portNum > 65535))
	{
		uartPrintf("Invalid Port Number: %s\n", pCmdArg);
		return;
	}

	// Set the channel(s) current value.
	if (cmdSuffix1 < cmdSuffix2)
	{
		for (i = cmdSuffix1; i <= cmdSuffix2; i++)
		{
			chanSourceRtpPort[i] = portNum;
		}
		uartPrintf("Chan %d-%d source RTP ports set to %u\n",
				   cmdSuffix1, cmdSuffix2, portNum);
	}
	else
	{
		chanSourceRtpPort[cmdSuffix1] = portNum;
		uartPrintf("Chan %d source RTP port set to %u\n",
				   cmdSuffix1, portNum);
	}

	return;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// procJitrBufrSize - Process a Jitter Buffer Size command.
//
// FUNCTION
//  This function processes a Jitter Buffer Size command.
//
// RETURNS
//  nothing
//
static void procJitrBufrSize(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	)
{
	int32_t i;					// loop index / counter
	uint32_t jbSize;			// jitter buffer size

	// Validate the channel Id.
	if (cmdSuffix1 < 0)
	{
		cmdSuffix1 = 0;
		cmdSuffix2 = NUM_TEST_CHANS - 1;
	}
	else if ((cmdSuffix1 >= NUM_TEST_CHANS) || (cmdSuffix2 >= NUM_TEST_CHANS))
	{
		uartPrintf("Channel exceeds max of %u!\n", NUM_TEST_CHANS - 1);
		return;
	}

	// If the argument is null, display the current value(s).
	if (pCmdArg == NULL)
	{
		if (cmdSuffix1 < cmdSuffix2)
		{
			uartPrintf("Jitter Buffer Sizes:\n");
			for (i = cmdSuffix1; i <= cmdSuffix2; i++)
			{
				uartPrintf(" [%u] = %u msec\n", i, chanJitrBufrSize[i]);
			}
		}
		else
		{
			uartPrintf("Chan %d jitter buffer size = %u msec\n",
					   cmdSuffix1, chanJitrBufrSize[cmdSuffix1]);
		}
		return;
	}

	// Determine the jitter buffer size.
	if ((sscanf(pCmdArg, "%u", &jbSize) != 1) || (jbSize > 65535))
	{
		uartPrintf("Invalid Jitter Buffer Size: %s\n", pCmdArg);
		return;
	}

	// Set the channel(s) current value.
	if (cmdSuffix1 < cmdSuffix2)
	{
		for (i = cmdSuffix1; i <= cmdSuffix2; i++)
		{
			chanJitrBufrSize[i] = jbSize;
		}
		uartPrintf("Chan %d-%d jitter buffer sizes set to %u msec\n",
				   cmdSuffix1, cmdSuffix2, jbSize);
	}
	else
	{
		chanJitrBufrSize[cmdSuffix1] = jbSize;
		uartPrintf("Chan %d jitter buffer size set to %u msec\n",
				   cmdSuffix1, jbSize);
	}

	return;
}

static void setupPcmConfChannel(
	ADT_UInt16 channelId,		// channel Id
    ADT_UInt16 confID
	)
{
	GpakApiStatus_t gpakApiStatus;			// host API status
	GpakChannelConfig_t chanConfig;			// channel configuration info
	GPAK_ChannelConfigStat_t dspChanStatus;	// DSP's channel config status

	// Configure the channel.
    chanConfig.ConferPcm.ConferenceId = confID;       /* Conference Identifier */
    chanConfig.ConferPcm.PcmInPort = APort;
	chanConfig.ConferPcm.PcmInSlot = chanInSlotA[channelId];
	chanConfig.ConferPcm.PcmInPin = 0;
	chanConfig.ConferPcm.PcmOutPort = APort;
	chanConfig.ConferPcm.PcmOutSlot = chanOutSlotA[channelId];
	chanConfig.ConferPcm.PcmOutPin = 0;
    chanConfig.ConferPcm.EcanEnable = chanLecEnable[channelId];
    chanConfig.ConferPcm.AECEcanEnable = chanAecEnable[channelId];
    chanConfig.ConferPcm.AgcInEnable = Disabled;  
    chanConfig.ConferPcm.AgcOutEnable = Disabled; 
    chanConfig.ConferPcm.ToneTypes = Null_tone;    
    chanConfig.ConferPcm.ToneGenGainG1 = 0;
    chanConfig.ConferPcm.OutputGainG2 = 0; 
    chanConfig.ConferPcm.InputGainG3 = 0;  

	gpakApiStatus =
		gpakConfigureChannel(0, PackCore(chanCore[channelId],channelId), conferencePcm, &chanConfig,
							 &dspChanStatus);
	if (gpakApiStatus != GpakApiSuccess)
	{
		uartPrintf("* Failed to config chan %u (API status = %d, DSP status = %d)\n",
				   channelId, gpakApiStatus, dspChanStatus);
		return;
	}

	uartPrintf("Channel %u setup: ConfID = %u\n",
			   channelId, confID);
	uartPrintf(" In Slot = %u, Out Slot = %u\n",
			   chanConfig.ConferPcm.PcmInSlot, chanConfig.ConferPcm.PcmOutSlot);
	uartPrintf(" LEC = %s, AGCIN = %s, AGCOUT = %s\n",
			   algoStateName(chanConfig.ConferPcm.EcanEnable),
			   algoStateName(chanConfig.ConferPcm.AgcInEnable),
			   algoStateName(chanConfig.ConferPcm.AgcOutEnable));
	uartPrintf(" In Gain = %d, Out Gain = %d\n",
			   chanConfig.ConferPcm.InputGainG3, chanConfig.ConferPcm.OutputGainG2);

	return;


}

static void initRtpPayloadType(ADT_UInt16 channelId, GpakRTPCfg_t *rtpConfig) {
	switch (chanCodecType[channelId])
	{
	case PCMU_64:
	default:
		rtpConfig->VoicePyldType = 0;
		rtpConfig->CNGPyldType = 13;
		break;
	case PCMA_64:
		rtpConfig->VoicePyldType = 8;
		rtpConfig->CNGPyldType = 13;
		break;
	case G722:
		rtpConfig->VoicePyldType = 9;
		rtpConfig->CNGPyldType = 255;
		break;
	case G723_53:
	case G723_63:
	case G723_53A:
	case G723_63A:
		rtpConfig->VoicePyldType = 4;
		rtpConfig->CNGPyldType = 255;
		break;
	case G726_40:
		rtpConfig->VoicePyldType = G726_40_DYNTYPE;
		rtpConfig->CNGPyldType = 13;
		break;
	case G726_32:
		rtpConfig->VoicePyldType = G726_32_DYNTYPE;
		rtpConfig->CNGPyldType = 13;
		break;
	case G726_24:
		rtpConfig->VoicePyldType = G726_24_DYNTYPE;
		rtpConfig->CNGPyldType = 13;
		break;
	case G726_16:
		rtpConfig->VoicePyldType = G726_16_DYNTYPE;
		rtpConfig->CNGPyldType = 13;
		break;
	case G729:
	case G729AB:
		rtpConfig->VoicePyldType = 18;
		rtpConfig->CNGPyldType = 255;
		break;
	case AMR_475:
	case AMR_515:
	case AMR_590:
	case AMR_670:
	case AMR_740:
	case AMR_795:
	case AMR_1020:
	case AMR_1220:
		rtpConfig->VoicePyldType = G722_2_DYNTYPE;
		rtpConfig->CNGPyldType = 255;
        break;
    case L16:
	    rtpConfig->VoicePyldType = L16_8K_DYNTYPE;
	    rtpConfig->CNGPyldType = 97;
        break;
	}
}	

static void setupPktConfChannel(
	ADT_UInt16 channelId,		// channel Id
    ADT_UInt16 confID
	)
{
	GpakRTPCfg_t rtpConfig;					// DSP channel's RTP configuration
	GpakApiStatus_t gpakApiStatus;			// host API status
	GPAK_RTPConfigStat_t dspRtpStatus;		// DSP's RTP status
	GpakChannelConfig_t chanConfig;			// channel configuration info
	GPAK_ChannelConfigStat_t dspChanStatus;	// DSP's channel config status

	// Configure the channel's RTP.
	memset(&rtpConfig, 0, sizeof(rtpConfig));
	rtpConfig.JitterMode = 0;
	rtpConfig.DelayTargetMS = chanJitrBufrSize[channelId];
	rtpConfig.DelayTargetMinMS = 10;
	rtpConfig.DelayTargetMaxMS = 120;
	rtpConfig.TonePyldType = 98;
	rtpConfig.T38PyldType = 99;
	rtpConfig.CNGPyldType = 97;
	rtpConfig.StartSequence = 1;
//	rtpConfig.SrcPort = srcPortFromChan(channelId);
	rtpConfig.SrcPort = chanDestRtpPort[channelId];
	rtpConfig.InDestIP = lclDstIPFromChan(channelId);
	rtpConfig.SSRC = ssrcFromChan(channelId);
	rtpConfig.VlanIdx = VlanIdx[channelId];
	rtpConfig.DSCP = 0;
	rtpConfig.DestMAC[0] = 0;
	rtpConfig.DestMAC[1] = 0;
	rtpConfig.DestMAC[2] = 0;
	rtpConfig.DestMAC[3] = 0;
	rtpConfig.DestMAC[4] = 0;
	rtpConfig.DestMAC[5] = 0;
    initRtpPayloadType(channelId, &rtpConfig);
    if (chanMultiCast[channelId] & TXMCAST) {
	    rtpConfig.DestIP = 0;
        if ((chanMultiCast[channelId] & RXMCAST) == 0)
	        rtpConfig.InDestIP = 0;
    } else {
	rtpConfig.DestIP = chanDestRtpIp[channelId];
    }
	rtpConfig.DestTxIP = dstTxIPFromChan (channelId);  // Multicast //0;//chanDestRtpIp[channelId];
//	rtpConfig.DestPort = rtpConfig.SrcPort; //chanDestRtpPort[channelId]; // 
	rtpConfig.DestPort = chanDestRtpPort[channelId]; // 

	rtpConfig.DestSSRC = 0; // jdc ssrcFromChan(channelId); //dstSSRCFromChan(channelId);
	rtpConfig.SamplingHz = 8000;
	gpakApiStatus =
		gpakSendRTPMsg(0, PackCore(chanCore[channelId],channelId), &rtpConfig, &dspRtpStatus);
	if (gpakApiStatus != GpakApiSuccess)
	{
		uartPrintf("* Failed to configure RTP (API status = %d, DSP status = %d)\n",
				   gpakApiStatus, dspRtpStatus);
		return;
	}

	// Configure the channel.
    chanConfig.ConferPkt.ConferenceId = confID;       /* Conference Identifier */
    if (chanMultiCast[channelId] == 0) {
	chanConfig.ConferPkt.PktInCoding = chanCodecType[channelId];
	chanConfig.ConferPkt.PktOutCoding = chanCodecType[channelId];
    }  else {
        chanConfig.ConferPkt.PktInCoding   = NullCodec;
	    chanConfig.ConferPkt.PktOutCoding  = NullCodec;
        if (chanMultiCast[channelId] & TXMCAST)
	        chanConfig.ConferPkt.PktOutCoding = chanCodecType[channelId];
        if (chanMultiCast[channelId] & RXMCAST)
	        chanConfig.ConferPkt.PktInCoding = chanCodecType[channelId];
    }
	chanConfig.ConferPkt.PktFrameHalfMS = chanFrameSize[channelId];
    chanConfig.ConferPkt.EcanEnable = chanLecEnable[channelId];
    chanConfig.ConferPkt.VadEnable = chanVadCngEnable[channelId];    
    chanConfig.ConferPkt.AgcInEnable = Disabled;  
    chanConfig.ConferPkt.AgcOutEnable = Disabled; 
    chanConfig.ConferPkt.ToneTypes = Null_tone;    
    chanConfig.ConferPkt.ToneGenGainG1 = 0;
    chanConfig.ConferPkt.OutputGainG2 = 0; 
    chanConfig.ConferPkt.InputGainG3 = 0;  
    chanConfig.ConferPkt.DtxEnable = chanDtxEnable[channelId];    

	gpakApiStatus =
		gpakConfigureChannel(0, PackCore(chanCore[channelId],channelId), conferencePacket, &chanConfig,
							 &dspChanStatus);
	if (gpakApiStatus != GpakApiSuccess)
	{
		uartPrintf("* Failed to config chan %u (API status = %d, DSP status = %d)\n",
				   channelId, gpakApiStatus, dspChanStatus);
		return;
	}
	if (channelId < NUM_TEST_CHANS)
	{
		rtpTxPktCnt[channelId] = 0;
		rtpRxPktCnt[channelId] = 0;
	}

	uartPrintf("Channel %u setup: ConfID = %u, Codec = %s, Frame = %s\n",
			   channelId, confID, codecTypeName(chanConfig.ConferPkt.PktInCoding),
			   frameSizeName(chanConfig.ConferPkt.PktFrameHalfMS));
	uartPrintf(" LEC = %s, AGCIN = %s, AGCOUT = %s\n",
			   algoStateName(chanConfig.ConferPkt.EcanEnable),
			   algoStateName(chanConfig.ConferPkt.AgcInEnable),
			   algoStateName(chanConfig.ConferPkt.AgcOutEnable));
	uartPrintf(" In Gain = %d, Out Gain = %d\n",
			   chanConfig.ConferPkt.InputGainG3, chanConfig.ConferPkt.OutputGainG2);
	uartPrintf(" JB size = %u, RTP DestIP:DestPort = %s:%u\n",
			   rtpConfig.DelayTargetMS, ipAddressToString(rtpConfig.DestIP),
			   rtpConfig.DestPort);
	uartPrintf(" RTP DestTx:DestPort = %s:%u\n",
			   ipAddressToString(rtpConfig.DestTxIP),
			   rtpConfig.DestPort);
	uartPrintf(" RTP InDestIP:SrcPort = %s:%u\n",
			   ipAddressToString(rtpConfig.InDestIP),
			   rtpConfig.SrcPort);
	return;


}

static void setupCompositeConfChannel(
	ADT_UInt16 channelId,		// channel Id
    ADT_UInt16 confID
	)
{
	GpakRTPCfg_t rtpConfig;					// DSP channel's RTP configuration
	GpakApiStatus_t gpakApiStatus;			// host API status
	GPAK_RTPConfigStat_t dspRtpStatus;		// DSP's RTP status
	GpakChannelConfig_t chanConfig;			// channel configuration info
	GPAK_ChannelConfigStat_t dspChanStatus;	// DSP's channel config status

	// Configure the channel's RTP.
	memset(&rtpConfig, 0, sizeof(rtpConfig));
	rtpConfig.JitterMode = 0;
	rtpConfig.DelayTargetMS = chanJitrBufrSize[channelId];
	rtpConfig.DelayTargetMinMS = 10;
	rtpConfig.DelayTargetMaxMS = 120;
	rtpConfig.TonePyldType = 98;
	rtpConfig.T38PyldType = 99;
	rtpConfig.CNGPyldType = 97;
	rtpConfig.StartSequence = 1;
//	rtpConfig.SrcPort = srcPortFromChan(channelId);
	rtpConfig.SrcPort = chanDestRtpPort[channelId];
	rtpConfig.InDestIP = lclDstIPFromChan(channelId);
	rtpConfig.SSRC = ssrcFromChan(channelId);
	rtpConfig.VlanIdx = VlanIdx[channelId];
	rtpConfig.DSCP = 0;
	rtpConfig.DestMAC[0] = 0;
	rtpConfig.DestMAC[1] = 0;
	rtpConfig.DestMAC[2] = 0;
	rtpConfig.DestMAC[3] = 0;
	rtpConfig.DestMAC[4] = 0;
	rtpConfig.DestMAC[5] = 0;
    initRtpPayloadType(channelId, &rtpConfig);
    if (chanMultiCast[channelId] & TXMCAST) {
	    rtpConfig.DestIP = 0;
        if ((chanMultiCast[channelId] & RXMCAST) == 0)
	        rtpConfig.InDestIP = 0;
    } else {
	rtpConfig.DestIP = chanDestRtpIp[channelId];
    }
	rtpConfig.DestTxIP = dstTxIPFromChan (channelId);  // Multicast //0;//chanDestRtpIp[channelId];
//	rtpConfig.DestPort = rtpConfig.SrcPort; //chanDestRtpPort[channelId]; // 
	rtpConfig.DestPort = chanDestRtpPort[channelId]; // 
	rtpConfig.DestSSRC = 0; // jdc ssrcFromChan(channelId); //dstSSRCFromChan(channelId);
	rtpConfig.SamplingHz = 8000;
	gpakApiStatus =
		gpakSendRTPMsg(0, PackCore(chanCore[channelId],channelId), &rtpConfig, &dspRtpStatus);
	if (gpakApiStatus != GpakApiSuccess)
	{
		uartPrintf("* Failed to configure RTP (API status = %d, DSP status = %d)\n",
				   gpakApiStatus, dspRtpStatus);
		return;
	}

	// Configure the channel.
    chanConfig.ConferComp.ConferenceId = confID;       /* Conference Identifier */
	chanConfig.ConferComp.PcmOutPort = SerialPortNull; // APort;
	chanConfig.ConferComp.PcmOutSlot = chanOutSlotA[channelId];
	chanConfig.ConferComp.PcmOutPin = 0;
	chanConfig.ConferComp.PktOutCoding = chanCodecType[channelId];
    chanConfig.ConferComp.VadEnable = chanVadCngEnable[channelId];    
    chanConfig.ConferComp.DtxEnable = chanDtxEnable[channelId];    

	gpakApiStatus =
		gpakConfigureChannel(0, PackCore(chanCore[channelId],channelId), conferenceComposite, &chanConfig,
							 &dspChanStatus);
	if (gpakApiStatus != GpakApiSuccess)
	{
		uartPrintf("* Failed to config chan %u (API status = %d, DSP status = %d)\n",
				   channelId, gpakApiStatus, dspChanStatus);
		return;
	}
	if (channelId < NUM_TEST_CHANS)
	{
		rtpTxPktCnt[channelId] = 0;
		rtpRxPktCnt[channelId] = 0;
	}

	uartPrintf("Channel %u setup: ConfID = %u\n",
			   channelId, confID);
	uartPrintf(" Out Slot = %u\n",
			   chanConfig.ConferComp.PcmOutSlot);
	uartPrintf(" JB size = %u, RTP Dest = %s:%u\n",
			   rtpConfig.DelayTargetMS, ipAddressToString(rtpConfig.DestIP),
			   rtpConfig.DestPort);
	uartPrintf(" RTP DestTx:DestPort = %s:%u\n",
			   ipAddressToString(rtpConfig.DestTxIP),
			   rtpConfig.DestPort);
	uartPrintf(" RTP InDestIP:SrcPort = %s:%u\n",
			   ipAddressToString(rtpConfig.InDestIP),
			   rtpConfig.SrcPort);

	return;

}

void init_rtp_mac(GpakRTPCfg_t *rtpConfig) {

    if (initMac) {
#if (REMOTE_DEVICE == ALLWORX_DEVICE)
        // Allworx test phone MAC: 00.0a.dd.80.00.29
    	rtpConfig->DestMAC[0] = 0x00;
    	rtpConfig->DestMAC[1] = 0x0a;
    	rtpConfig->DestMAC[2] = 0xdd;
    	rtpConfig->DestMAC[3] = 0x80;
    	rtpConfig->DestMAC[4] = 0x00;
    	rtpConfig->DestMAC[5] = 0x29;
#elif (REMOTE_DEVICE == DEBIAN_GW_DEVICE)
        // Debian VEGW MAC: 08.00.27.06.3c.3d
	    rtpConfig->DestMAC[0] = 0x08;
	    rtpConfig->DestMAC[1] = 0x00;
	    rtpConfig->DestMAC[2] = 0x27;
	    rtpConfig->DestMAC[3] = 0x06;
	    rtpConfig->DestMAC[4] = 0x3c;
	    rtpConfig->DestMAC[5] = 0x3d;
#elif (REMOTE_DEVICE == STMF4_DEVICE)
        // STMicro MAC: 02.00.00.00.00.00
        rtpConfig->DestMAC[0] = 0x02;
    	rtpConfig->DestMAC[1] = 0x00;
    	rtpConfig->DestMAC[2] = 0x00;
    	rtpConfig->DestMAC[3] = 0x00;
    	rtpConfig->DestMAC[4] = 0x00;
    	rtpConfig->DestMAC[5] = 0x00;
#elif (REMOTE_DEVICE == DESKTOP_BROADCOM)
        // desktop MAC: 00:10:18:ea:50:cb
    	rtpConfig->DestMAC[0] = 0x00;
    	rtpConfig->DestMAC[1] = 0x10;
    	rtpConfig->DestMAC[2] = 0x18;
    	rtpConfig->DestMAC[3] = 0xea;
    	rtpConfig->DestMAC[4] = 0x50;
    	rtpConfig->DestMAC[5] = 0xcb;
#endif
    }
}
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// setupPcmToPktChannel - Configure a PCM to Packet channel on the DSP.
//
// FUNCTION
//  This function configures a PCM to Packet channel on the DSP.
//
// RETURNS
//  nothing
//
void setupPcmToPktChannel(
	ADT_UInt16 channelId		// channel Id
	)
{
	GpakRTPCfg_t rtpConfig;					// DSP channel's RTP configuration
	GpakRTPv6Cfg_t rtpV6Config;				// DSP channel's RTP IpV6 configuration
	GpakApiStatus_t gpakApiStatus;			// host API status
	GPAK_RTPConfigStat_t dspRtpStatus;		// DSP's RTP status
	GpakChannelConfig_t chanConfig;			// channel configuration info
	GPAK_ChannelConfigStat_t dspChanStatus;	// DSP's channel config status
    GpakToneTypes ADet, BDet;
	uint8_t  ipV6Address[16];	           // IPv6 address
    int i;

	// Configure the channel's RTP.
	if(chanIpVersion[channelId] == 4)
	{
	    memset(&rtpConfig, 0, sizeof(rtpConfig));
	    rtpConfig.JitterMode = 0;
	    rtpConfig.SamplingHz = 8000;
	    rtpConfig.DelayTargetMS    = chanJitrBufrSize[channelId];
	    rtpConfig.DelayTargetMinMS = 10;
	    rtpConfig.DelayTargetMaxMS = 120;
	    rtpConfig.TonePyldType = 98;
	    rtpConfig.T38PyldType = 99;
        initRtpPayloadType(channelId, &rtpConfig);
	    rtpConfig.VlanIdx = VlanIdx[channelId];
	    rtpConfig.DSCP = 0;
        init_rtp_mac(&rtpConfig);
	    rtpConfig.DestIP = chanDestRtpIp[channelId];
	    rtpConfig.StartSequence = 1;
	    rtpConfig.InDestIP = lclDstIPFromChan(channelId);
	    rtpConfig.DestTxIP = dstTxIPFromChan (channelId); 
	    rtpConfig.DestPort = dstPortFromChan(channelId); 
        rtpConfig.SrcPort = srcPortFromChan(channelId);
	    rtpConfig.SSRC = ssrcFromChan(channelId);
	    rtpConfig.DestSSRC = 0;
	} 
    else // ipv6
	{
		memset(&rtpV6Config, 0, sizeof(rtpV6Config));
		rtpV6Config.JitterMode = 0;
		rtpV6Config.SamplingHz = 8000;
		rtpV6Config.DelayTargetMS = chanJitrBufrSize[channelId];
		rtpV6Config.DelayTargetMinMS = 10;
		rtpV6Config.DelayTargetMaxMS = 120;
		rtpV6Config.TonePyldType = 98;
		rtpV6Config.T38PyldType = 99;
		initRtpPayloadType(channelId, &rtpV6Config);
	    rtpV6Config.VlanIdx = VlanIdx[channelId];
		rtpV6Config.DSCP = 0;
        init_rtp_mac(&rtpV6Config);
		rtpV6Config.StartSequence = 1;
        if (chanMultiCast[channelId] == 0)
		    memcpy(&rtpV6Config.DestIP, &chanDestRtpV6Ip[channelId].IpV6Address, sizeof(GpakIPv6_t));
		lclDstIPv6FromChan(channelId, &rtpV6Config.InDestIP);
        dstTxIPv6FromChan (channelId, &rtpV6Config.DestTxIP);
		rtpV6Config.DestPort = dstPortFromChan(channelId); 
		rtpV6Config.SrcPort = 6334;
		rtpV6Config.SSRC = ssrcFromChan(channelId);
		rtpV6Config.DestSSRC = 0;
	}

    if (chanRtcpEnable[channelId]) {
        GpakRTCPCfg_t rtcp_parms; 
        memset(&rtcp_parms, 0, sizeof(GpakRTCPCfg_t));
		if (chanIpVersion[channelId] == 4) 
        {
            rtcp_parms.localRtcpPort = rtpConfig.SrcPort+1;
            rtcp_parms.remoteRtcpPort = rtpConfig.DestPort+1;
		} else 
        { // ipv6
            rtcp_parms.localRtcpPort = rtpV6Config.SrcPort+1;
            rtcp_parms.remoteRtcpPort = rtpV6Config.DestPort+1;
        }
        rtcp_parms.transportOverheadBytes = 0;
        rtcp_parms.timeoutMultiplier = 0;
        rtcp_parms.minRTCPPktPeriodMs = 5000; 
        rtcp_parms.rtcpBandwidthBytesPerSec = 5000;
        rtcp_parms.sdes.itemCnt = NUM_SDES_ITEMS;       // send 4 sdes items: cname, email, phone, name
        rtcp_parms.sdes.pktsPerCycle = 5;  // 5 transmits slots per sde cycle: (0,1,2,3,4) 
        
        rtcp_parms.sdes.item [0].type = RTCP_SDES_CNAME;
        rtcp_parms.sdes.item [0].firstPktInCycle = 0; // cname is ALWAYS sent on every slot regardless of firstPktInCycle and txInterval settings
        rtcp_parms.sdes.item [0].txInterval = 0;      
        rtcp_parms.sdes.item [0].dataBytes = sprintf((void *)sdes_text[0], "%s%03d",userSdesInfo.cname,channelId);
        rtcp_parms.sdes.item [0].textData = sdes_text[0];
        
        rtcp_parms.sdes.item [1].type = RTCP_SDES_EMAIL;
        rtcp_parms.sdes.item [1].firstPktInCycle = 1; // send email on slots 1, 4
        rtcp_parms.sdes.item [1].txInterval = 3;
        rtcp_parms.sdes.item [1].dataBytes = sprintf((void *)sdes_text[1], "%s%03d",userSdesInfo.email,channelId);
        rtcp_parms.sdes.item [1].textData = sdes_text[1];
        
        rtcp_parms.sdes.item [2].type = RTCP_SDES_PHONE;
        rtcp_parms.sdes.item [2].firstPktInCycle = 2; // send phone on slots 2, 4
        rtcp_parms.sdes.item [2].txInterval = 2;
        rtcp_parms.sdes.item [2].dataBytes = sprintf((void *)sdes_text[2], "%s%03d",userSdesInfo.phone,channelId);
        rtcp_parms.sdes.item [2].textData = sdes_text[2];
        
        rtcp_parms.sdes.item [3].type = RTCP_SDES_NAME;
        rtcp_parms.sdes.item [3].firstPktInCycle = 3; // send name on slot 3
        rtcp_parms.sdes.item [3].txInterval = 5;
        rtcp_parms.sdes.item [3].dataBytes = sprintf((void *)sdes_text[3], "%s%03d",userSdesInfo.name,channelId);
        rtcp_parms.sdes.item [3].textData = sdes_text[3];
        
		if (chanIpVersion[channelId] == 4)
	    {
	        gpakApiStatus =
		        gpakSendRTP_RTCPMsg(0, PackCore(chanCore[channelId],channelId), &rtpConfig, &rtcp_parms, &dspRtpStatus);
	        if (gpakApiStatus != GpakApiSuccess)
	        {
		        uartPrintf("* Failed to configure RTP_RTCP (API status = %d, DSP status = %d)\n",
				   gpakApiStatus, dspRtpStatus);
		        return;
	        }
		}
		else // ipv6
		{
			gpakApiStatus =
				gpakSendRTPv6_RTCPMsg(0, PackCore(chanCore[channelId],channelId), &rtpV6Config, &rtcp_parms, &dspRtpStatus);
			if (gpakApiStatus != GpakApiSuccess)
			{
				uartPrintf("* Failed to configure RTP_RTCP (API status = %d, DSP status = %d)\n",
					   gpakApiStatus, dspRtpStatus);
				return;
			}
		}
        
    } else {
        if (chanIpVersion[channelId] == 4)
		{
	    gpakApiStatus =
		    gpakSendRTPMsg(0, PackCore(chanCore[channelId],channelId), &rtpConfig, &dspRtpStatus);
	    if (gpakApiStatus != GpakApiSuccess)
	    {
		    uartPrintf("* Failed to configure RTP (API status = %d, DSP status = %d)\n",
				   gpakApiStatus, dspRtpStatus);
		    return;
			}
		}
		else if (chanIpVersion[channelId] == 6)
		{
			gpakApiStatus =
				gpakSendRTPv6Msg(0, PackCore(chanCore[channelId],channelId), &rtpV6Config, &dspRtpStatus);
			if (gpakApiStatus != GpakApiSuccess)
			{
				uartPrintf("* Failed to configure RTP (API status = %d, DSP status = %d)\n",
					   gpakApiStatus, dspRtpStatus);
				return;
			}
	    }
    } 
	gpakHostDelay ();
    if(chanCryptoSuite[channelId] != 0) {

		// SRTP configuration variables
		GpakSRTPCfg_t    SRTPCfg;
		GpakApiStatus_t  srtpCfgStat = 0;
		GpakSrtpStatus_t     srtpDspStat = 0;
		srtpGetConfig(channelId, chanCryptoSuite[channelId]-1, &SRTPCfg);
        //getCustomSRTPParams(&SRTPCfg);

		srtpCfgStat = gpakConfigSRTP (0, PackCore(chanCore[channelId],channelId), &SRTPCfg, &srtpDspStat);
		if (srtpCfgStat != GpakApiSuccess || srtpDspStat != GPAK_SRTP_SUCCESS) {
			uartPrintf ("SRTP channel %d setup failure %d:%d:%d\n", channelId, srtpCfgStat, srtpDspStat, DSPError [0]);
			//appErr (__LINE__);
		}
	}
	gpakHostDelay ();

	// Configure the channel.
    chanConfig.PcmPkt.PcmInPort = McBSPPort0;
	chanConfig.PcmPkt.PcmInSlot = chanInSlotA[channelId];
	chanConfig.PcmPkt.PcmInPin = 0;
	chanConfig.PcmPkt.PcmOutPort = chanConfig.PcmPkt.PcmInPort;
	chanConfig.PcmPkt.PcmOutSlot = chanOutSlotA[channelId];

    if (slotCross) {
        if (channelId & 1)
	        chanConfig.PcmPkt.PcmInSlot = chanInSlotA[channelId-1];
        else
	        chanConfig.PcmPkt.PcmInSlot = chanInSlotA[channelId+1];
    }
	chanConfig.PcmPkt.PcmOutPin = 0;
	chanConfig.PcmPkt.PktInCoding = chanCodecType[channelId];
    if ((chanMultiCast[channelId] & RXMCAST) && (~chanMultiCast[channelId] & TXMCAST))
	    chanConfig.PcmPkt.PktOutCoding = NullCodec;
    else
	    chanConfig.PcmPkt.PktOutCoding = chanCodecType[channelId];
	chanConfig.PcmPkt.PktInFrameHalfMS = chanFrameSize[channelId];
	chanConfig.PcmPkt.PktOutFrameHalfMS = chanFrameSize[channelId];
	chanConfig.PcmPkt.PcmEcanEnable = chanLecEnable[channelId];
	chanConfig.PcmPkt.PktEcanEnable = Disabled;
	chanConfig.PcmPkt.AECEcanEnable = Disabled;
	chanConfig.PcmPkt.VadEnable = chanVadCngEnable[channelId];
	chanConfig.PcmPkt.AgcEnable = chanAgcEnable[channelId];

    if ((chanCIDMode[channelId] == 1) || (chanCIDMode[channelId] == 2)) 
	    chanConfig.PcmPkt.CIdMode = CidReceive;
    else if ((chanCIDMode[channelId] == 3) || (chanCIDMode[channelId] == 4)) 
	    chanConfig.PcmPkt.CIdMode = CidTransmit;
    else
		chanConfig.PcmPkt.CIdMode = CidDisabled;
	chanConfig.PcmPkt.FaxMode = chanFaxMode[channelId];
	chanConfig.PcmPkt.FaxTransport = FAX_TRANSPORT;
	chanConfig.PcmPkt.Coding = (GpakChannelTypes)chanDataEnable[channelId];
	chanConfig.PcmPkt.ToneGenGainG1 = 0;
	chanConfig.PcmPkt.OutputGainG2 = chanOutGainA[channelId];
	chanConfig.PcmPkt.InputGainG3 = chanInGainA[channelId];
	chanConfig.PcmPkt.DtxEnable = chanDtxEnable[channelId];
	chanConfig.PcmPkt.AgcEnablePkt2Pcm = 0;
	chanConfig.PcmPkt.VadEnablePkt = 0;

    ADet = getToneType(channelId, ADevice);
    BDet = getToneType(channelId, BDevice);
    getDtmfDetectFlags(channelId, &ADet, &BDet);
	chanConfig.PcmPkt.ToneTypes = Tone_Generate | ADet;
	chanConfig.PcmPkt.ToneTypesPkt = Tone_Generate | BDet;
	gpakApiStatus =
		gpakConfigureChannel(0, PackCore(chanCore[channelId],channelId), pcmToPacket, &chanConfig,
							 &dspChanStatus);
	if (gpakApiStatus != GpakApiSuccess)
	{
		uartPrintf("* Failed to config chan %u (API status = %d, DSP status = %d)\n",
				   channelId, gpakApiStatus, dspChanStatus);
		return;
	}
    stressStats[channelId].chCfgPass++;
	if (channelId < NUM_TEST_CHANS)
	{
		rtpTxPktCnt[channelId] = 0;
		rtpRxPktCnt[channelId] = 0;
	}

	uartPrintf("Channel:Core %u:%u setup: Codec = %s, Frame = %s\n",
			   channelId, chanCore[channelId], codecTypeName(chanConfig.PcmPkt.PktInCoding),
			   frameSizeName(chanConfig.PcmPkt.PktInFrameHalfMS));
	uartPrintf(" In Slot = %u, Out Slot = %u VadA = %s, VadB = %s\n",
			   chanConfig.PcmPkt.PcmInSlot, chanConfig.PcmPkt.PcmOutSlot,
			   algoStateName(chanConfig.PcmPkt.VadEnable),
			   algoStateName(chanConfig.PcmPkt.VadEnablePkt));
	uartPrintf(" AEC = %s, LEC = %s, AGCA = %s, AGCB = %s\n",
			   algoStateName(chanConfig.PcmPkt.AECEcanEnable),
			   algoStateName(chanConfig.PcmPkt.PcmEcanEnable),
			   algoStateName(chanConfig.PcmPkt.AgcEnable),
			   algoStateName(chanConfig.PcmPkt.AgcEnablePkt2Pcm));
	uartPrintf(" In Gain = %d, Out Gain = %d\n",
			   chanConfig.PcmPkt.InputGainG3, chanConfig.PcmPkt.OutputGainG2);
	if (chanIpVersion[channelId] == 4)
    {
        if (chanMultiCast[channelId] == 0) {
	        uartPrintf(" JB size = %u, RTP Dest = %s:%u\n",
			   rtpConfig.DelayTargetMS, ipAddressToString(rtpConfig.DestIP),
			   rtpConfig.DestPort);
        } else {
	        uartPrintf(" JB size = %u, RTP Dest = %s:%u\n",
			   rtpConfig.DelayTargetMS, ipAddressToString(BaseMCastIP),
			   rtpConfig.DestPort);
        }
	}
    else if (chanIpVersion[channelId] == 6)
    {
        if (chanMultiCast[channelId] == 0)
		    memcpy(&ipV6Address,&chanDestRtpV6Ip[channelId].IpV6Address, sizeof(GpakIPv6_t));
        else
		    memcpy(&ipV6Address,&BaseMCastIPv6, sizeof(GpakIPv6_t));
		uartPrintf(" JB size = %u, RTP Dest = %s:%u\n",
				   rtpV6Config.DelayTargetMS, ipV6AddressToString(ipV6Address),
				   rtpV6Config.DestPort);

	}

    for (i=0; i<10; i++)
	    gpakHostDelay ();

	return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// setupCustChannel - Configure a custom channel on the DSP
//                  
//
// FUNCTION
//  This function configures a custom channel on the DSP. .... For test puposes only! 
//  Configures custom channel using pcmPacket structure
//
// RETURNS
//  nothing
//
static int  formatCustChannelConfig (ADT_UInt16 *Msg, void *chanCfg, ADT_UInt16 ChannelId) {
    ADT_UInt32 MsgLenI16;                     /* message length */
    int        Voice;
    GpakChannelConfig_t *pChan = (GpakChannelConfig_t *)chanCfg;

    if (pChan->PcmPkt.Coding == GpakVoiceChannel) Voice = 1;
    else                                          Voice = 0;
    Msg[2] = PackPortandSlot (pChan->PcmPkt.PcmInPort,    pChan->PcmPkt.PcmInSlot);
    Msg[3] = PackPortandSlot (pChan->PcmPkt.PcmOutPort,   pChan->PcmPkt.PcmOutSlot);
    Msg[4] = PackBytes (pChan->PcmPkt.PktInCoding,  pChan->PcmPkt.PktInFrameHalfMS);
    Msg[5] = PackBytes (pChan->PcmPkt.PktOutCoding, pChan->PcmPkt.PktOutFrameHalfMS);
    Msg[6] = (ADT_UInt16) ( ( pChan->PcmPkt.PcmEcanEnable         << 15) |
                          ((pChan->PcmPkt.PktEcanEnable &    1) << 14) |
                          ((pChan->PcmPkt.VadEnable     &    1) << 13) |
                          ((pChan->PcmPkt.AgcEnable     &    1) << 12) |
                          ((pChan->PcmPkt.AECEcanEnable &    1) << 11) |
                          ((Voice                       &    1) << 10) |
                          ((pChan->PcmPkt.DtxEnable     &    1) << 9) 
#ifdef DSP_66x
                          |
                          ((pChan->PcmPkt.AgcEnablePkt2Pcm & 1) << 8)
                          |
                          ((pChan->PcmPkt.VadEnablePkt & 1) << 7)
#endif
							  );
    Msg[7] = 0;  // PinIn/PinOut Deprecated
    Msg[8] = (ADT_UInt16) ( ((pChan->PcmPkt.FaxMode     &    3) << 14) | 
                          ((pChan->PcmPkt.FaxTransport  &    3) << 12) |
                          ((pChan->PcmPkt.ToneTypes     & 0x0FFF)) );
    MsgLenI16 = 9;

    // 4_2 ---------------------------------------
    Msg[9] = (ADT_UInt16) (pChan->PcmPkt.CIdMode & 3);
    Msg[10] = pChan->PcmPkt.ToneGenGainG1;
    Msg[11] = pChan->PcmPkt.OutputGainG2;
    Msg[12] = pChan->PcmPkt.InputGainG3;
    MsgLenI16 += 4;
#ifdef DSP_66x
    Msg[13] = (ADT_UInt16) (((pChan->PcmPkt.ToneTypesPkt & 0x0FFF)) );
    MsgLenI16 += 1;
#endif

    return MsgLenI16; 
}


static void parseCustChannelStatus (ADT_UInt16 *Msg, void *pChanStat) {
  GpakChannelStatus_t *pChanStatus = (GpakChannelStatus_t *)pChanStat;
#define PCMPkt   pChanStatus->ChannelConfig.PcmPkt
       PCMPkt.PcmInPort =      (GpakSerialPort_t) UnpackPort (Msg[7]);
       PCMPkt.PcmInSlot =                         UnpackSlot (Msg[7]);
       PCMPkt.PcmOutPort =     (GpakSerialPort_t) UnpackPort (Msg[8]);
       PCMPkt.PcmOutSlot =                        UnpackSlot (Msg[8]);
       PCMPkt.PktInCoding       = (GpakCodecs)         Byte1 (Msg[9]);
       PCMPkt.PktInFrameHalfMS  = (GpakFrameHalfMS)    Byte0 (Msg[9]);
       PCMPkt.PktOutCoding      = (GpakCodecs)         Byte1 (Msg[10]);
       PCMPkt.PktOutFrameHalfMS = (GpakFrameHalfMS)    Byte0 (Msg[10]);

       if (Msg[11] & 0x8000)  PCMPkt.PcmEcanEnable = Enabled;
       else                   PCMPkt.PcmEcanEnable = Disabled;

       if (Msg[11] & 0x4000)  PCMPkt.PktEcanEnable = Enabled;
       else                   PCMPkt.PktEcanEnable = Disabled;

       if (Msg[11] & 0x2000)  PCMPkt.VadEnable = Enabled;
       else                   PCMPkt.VadEnable = Disabled;

       if (Msg[11] & 0x1000)  PCMPkt.AgcEnable = Enabled;
       else                   PCMPkt.AgcEnable = Disabled;

       if (Msg[11] & 0x0800)  PCMPkt.AECEcanEnable = Enabled;
       else                   PCMPkt.AECEcanEnable = Disabled;

       if (Msg[11] & 0x0400)  PCMPkt.Coding = GpakVoiceChannel;
       else                   PCMPkt.Coding = GpakDataChannel;

       if (Msg[11] & 0x0200)  PCMPkt.DtxEnable = Enabled;
       else                   PCMPkt.DtxEnable = Disabled;
 


       PCMPkt.PcmInPin =  0;   // Deprecated
       PCMPkt.PcmOutPin = 0;  // Deprecated

       PCMPkt.FaxMode =       (GpakFaxMode_t) ((Msg[13] >> 14) & 0x0003);
       PCMPkt.FaxTransport =  (GpakFaxTransport_t) ((Msg[13] >> 12) & 0x0003);
       PCMPkt.ToneTypes =     (GpakToneTypes) (Msg[13] & 0x0FFF);

        // 4_2 ----------------------------------------------------
        PCMPkt.CIdMode       =   (GpakCidMode_t) (Msg[14] & 0x0003);
        PCMPkt.ToneGenGainG1 =   Msg[15];
        PCMPkt.OutputGainG2  =   Msg[16];
        PCMPkt.InputGainG3   =   Msg[17];
        pChanStatus->NumPktsToDsp    = Msg[18];
        pChanStatus->NumPktsFromDsp  = Msg[19];
        // ---------------------------------------------------------
}


void setupCustChannel(
	ADT_UInt16 channelId		// channel Id
	)
{
	GpakRTPCfg_t rtpConfig;					// DSP channel's RTP configuration
	GpakRTPv6Cfg_t rtpV6Config;				// DSP channel's RTP IpV6 configuration
	GpakApiStatus_t gpakApiStatus;			// host API status
	GPAK_RTPConfigStat_t dspRtpStatus;		// DSP's RTP status
	GpakChannelConfig_t chanConfig;			// channel configuration info
	GPAK_ChannelConfigStat_t dspChanStatus;	// DSP's channel config status
    GpakToneTypes ADet, BDet;
	uint8_t  ipV6Address[16];	           // IPv6 address

    if (channelId == breakChannel) {
        pause++;
    }

	// Configure the channel's RTP.
	if(chanIpVersion[channelId] == 4)
	{
	    memset(&rtpConfig, 0, sizeof(rtpConfig));
	    rtpConfig.JitterMode = 0;
	    rtpConfig.SamplingHz = 8000;
	    rtpConfig.DelayTargetMS = chanJitrBufrSize[channelId];
	    rtpConfig.DelayTargetMinMS = 10;
	    rtpConfig.DelayTargetMaxMS = 120;
	    rtpConfig.TonePyldType = 98;
	    rtpConfig.T38PyldType = 99;
        initRtpPayloadType(channelId, &rtpConfig);
	    rtpConfig.VlanIdx = VlanIdx[channelId];
	    rtpConfig.DSCP = 0;
        init_rtp_mac(&rtpConfig);
	    rtpConfig.DestIP = chanDestRtpIp[channelId];
	    rtpConfig.StartSequence = 1;
	    rtpConfig.InDestIP = lclDstIPFromChan(channelId);
	    rtpConfig.DestTxIP = dstTxIPFromChan (channelId); 
	    rtpConfig.DestPort = dstPortFromChan(channelId); 
        rtpConfig.SrcPort =  srcPortFromChan(channelId);
	    rtpConfig.SSRC = ssrcFromChan(channelId);
	    rtpConfig.DestSSRC = 0;
	} 
    else // ipv6
	{
		memset(&rtpV6Config, 0, sizeof(rtpV6Config));
		rtpV6Config.JitterMode = 0;
		rtpV6Config.SamplingHz = 8000;
		rtpV6Config.DelayTargetMS = chanJitrBufrSize[channelId];
		rtpV6Config.DelayTargetMinMS = 10;
		rtpV6Config.DelayTargetMaxMS = 120;
		rtpV6Config.TonePyldType = 98;
		rtpV6Config.T38PyldType = 99;
		initRtpPayloadType(channelId, &rtpV6Config);
	    rtpV6Config.VlanIdx = VlanIdx[channelId];
		rtpV6Config.DSCP = 0;
        init_rtp_mac(&rtpV6Config);
		rtpV6Config.StartSequence = 1;
        if (chanMultiCast[channelId] == 0)
		    memcpy(&rtpV6Config.DestIP, &chanDestRtpV6Ip[channelId].IpV6Address, sizeof(GpakIPv6_t));
		lclDstIPv6FromChan(channelId, &rtpV6Config.InDestIP);
        dstTxIPv6FromChan (channelId, &rtpV6Config.DestTxIP);
		rtpV6Config.DestPort = dstPortFromChan(channelId); 
		rtpV6Config.SrcPort = 6334;
		rtpV6Config.SSRC = ssrcFromChan(channelId);
		rtpV6Config.DestSSRC = 0;
	}

    if (chanRtcpEnable[channelId]) {
        GpakRTCPCfg_t rtcp_parms; 
        memset(&rtcp_parms, 0, sizeof(GpakRTCPCfg_t));
		if (chanIpVersion[channelId] == 4) 
        {
            rtcp_parms.localRtcpPort = rtpConfig.SrcPort+1;
            rtcp_parms.remoteRtcpPort = rtpConfig.DestPort+1;
		} else 
        { // ipv6
            rtcp_parms.localRtcpPort = rtpV6Config.SrcPort+1;
            rtcp_parms.remoteRtcpPort = rtpV6Config.DestPort+1;
        }
        rtcp_parms.transportOverheadBytes = 0;
        rtcp_parms.timeoutMultiplier = 0;
        rtcp_parms.minRTCPPktPeriodMs = 5000; 
        rtcp_parms.rtcpBandwidthBytesPerSec = 5000;
        rtcp_parms.sdes.itemCnt = NUM_SDES_ITEMS;       // send 4 sdes items: cname, email, phone, name
        rtcp_parms.sdes.pktsPerCycle = 5;  // 5 transmits slots per sde cycle: (0,1,2,3,4) 
        
        rtcp_parms.sdes.item [0].type = RTCP_SDES_CNAME;
        rtcp_parms.sdes.item [0].firstPktInCycle = 0; // cname is ALWAYS sent on every slot regardless of firstPktInCycle and txInterval settings
        rtcp_parms.sdes.item [0].txInterval = 0;      
        rtcp_parms.sdes.item [0].dataBytes = sprintf((void *)sdes_text[0], "%s%03d",userSdesInfo.cname,channelId);
        rtcp_parms.sdes.item [0].textData = sdes_text[0];
        
        rtcp_parms.sdes.item [1].type = RTCP_SDES_EMAIL;
        rtcp_parms.sdes.item [1].firstPktInCycle = 1; // send email on slots 1, 4
        rtcp_parms.sdes.item [1].txInterval = 3;
        rtcp_parms.sdes.item [1].dataBytes = sprintf((void *)sdes_text[1], "%s%03d",userSdesInfo.email,channelId);
        rtcp_parms.sdes.item [1].textData = sdes_text[1];
        
        rtcp_parms.sdes.item [2].type = RTCP_SDES_PHONE;
        rtcp_parms.sdes.item [2].firstPktInCycle = 2; // send phone on slots 2, 4
        rtcp_parms.sdes.item [2].txInterval = 2;
        rtcp_parms.sdes.item [2].dataBytes = sprintf((void *)sdes_text[2], "%s%03d",userSdesInfo.phone,channelId);
        rtcp_parms.sdes.item [2].textData = sdes_text[2];
        
        rtcp_parms.sdes.item [3].type = RTCP_SDES_NAME;
        rtcp_parms.sdes.item [3].firstPktInCycle = 3; // send name on slot 3
        rtcp_parms.sdes.item [3].txInterval = 5;
        rtcp_parms.sdes.item [3].dataBytes = sprintf((void *)sdes_text[3], "%s%03d",userSdesInfo.name,channelId);
        rtcp_parms.sdes.item [3].textData = sdes_text[3];
        
		if (chanIpVersion[channelId] == 4)
	    {
	        gpakApiStatus =
		        gpakSendRTP_RTCPMsg(0, PackCore(chanCore[channelId],channelId), &rtpConfig, &rtcp_parms, &dspRtpStatus);
	        if (gpakApiStatus != GpakApiSuccess)
	        {
		        uartPrintf("* Failed to configure RTP_RTCP (API status = %d, DSP status = %d)\n",
				   gpakApiStatus, dspRtpStatus);
		        return;
	        }
		}
		else // ipv6
		{
			gpakApiStatus =
				gpakSendRTPv6_RTCPMsg(0, PackCore(chanCore[channelId],channelId), &rtpV6Config, &rtcp_parms, &dspRtpStatus);
			if (gpakApiStatus != GpakApiSuccess)
			{
				uartPrintf("* Failed to configure RTP_RTCP (API status = %d, DSP status = %d)\n",
					   gpakApiStatus, dspRtpStatus);
				return;
			}
		}
        
    } else {
        if (chanIpVersion[channelId] == 4)
		{
	        gpakApiStatus =
		        gpakSendRTPMsg(0, PackCore(chanCore[channelId],channelId), &rtpConfig, &dspRtpStatus);
	        if (gpakApiStatus != GpakApiSuccess)
	        {
                stressStats[channelId].rtpCfgFail++;
		        uartPrintf("* Failed to configure RTP (API status = %d, DSP status = %d)\n",
				   gpakApiStatus, dspRtpStatus);
		        return;
			}
            stressStats[channelId].rtpCfgPass++;
		}
		else if (chanIpVersion[channelId] == 6)
		{
			gpakApiStatus =
				gpakSendRTPv6Msg(0, PackCore(chanCore[channelId],channelId), &rtpV6Config, &dspRtpStatus);
			if (gpakApiStatus != GpakApiSuccess)
			{
				uartPrintf("* Failed to configure RTP (API status = %d, DSP status = %d)\n",
					   gpakApiStatus, dspRtpStatus);
				return;
			}
	    }
    } 
	gpakHostDelay ();
	if(chanCryptoSuite[channelId] != 0) {
		// SRTP configuration variables
		GpakSRTPCfg_t    SRTPCfg;
		GpakApiStatus_t  srtpCfgStat = 0;
		GpakSrtpStatus_t     srtpDspStat = 0;
	    srtpGetConfig(channelId, chanCryptoSuite[channelId]-1, &SRTPCfg);

		srtpCfgStat = gpakConfigSRTP (0, PackCore(chanCore[channelId],channelId), &SRTPCfg, &srtpDspStat);
		if (srtpCfgStat != GpakApiSuccess || srtpDspStat != GPAK_SRTP_SUCCESS) {
			uartPrintf ("SRTP channel %d setup failure %d:%d:%d\n", channelId, srtpCfgStat, srtpDspStat, DSPError [0]);
		}
	}
	gpakHostDelay ();

	// Configure the channel.
    chanConfig.PcmPkt.PcmInPort = SerialPortNull;
	chanConfig.PcmPkt.PcmInSlot = channelId;
	chanConfig.PcmPkt.PcmInPin = 0;
	chanConfig.PcmPkt.PcmOutPort = SerialPortNull;
	chanConfig.PcmPkt.PcmOutSlot = channelId;
	chanConfig.PcmPkt.PcmOutPin = 0;
	chanConfig.PcmPkt.PktInCoding = chanCodecType[channelId];
    if ((chanMultiCast[channelId] & RXMCAST) && (~chanMultiCast[channelId] & TXMCAST))
	    chanConfig.PcmPkt.PktOutCoding = NullCodec;
    else
	    chanConfig.PcmPkt.PktOutCoding = chanCodecType[channelId];
	chanConfig.PcmPkt.PktInFrameHalfMS = chanFrameSize[channelId];
	chanConfig.PcmPkt.PktOutFrameHalfMS = chanFrameSize[channelId];
	chanConfig.PcmPkt.PcmEcanEnable = chanLecEnable[channelId];
	chanConfig.PcmPkt.PktEcanEnable = Disabled;
	chanConfig.PcmPkt.AECEcanEnable = Disabled;
	chanConfig.PcmPkt.VadEnable = chanVadCngEnable[channelId];
	chanConfig.PcmPkt.AgcEnable = chanAgcEnable[channelId];

	chanConfig.PcmPkt.CIdMode = CidDisabled;
	chanConfig.PcmPkt.FaxMode = chanFaxMode[channelId];
	chanConfig.PcmPkt.FaxTransport = FAX_TRANSPORT;
	chanConfig.PcmPkt.Coding = (GpakChannelTypes)chanDataEnable[channelId];
	chanConfig.PcmPkt.ToneGenGainG1 = 0;
	chanConfig.PcmPkt.OutputGainG2 = chanOutGainA[channelId];
	chanConfig.PcmPkt.InputGainG3 = chanInGainA[channelId];
	chanConfig.PcmPkt.DtxEnable   = chanDtxEnable[channelId];
	chanConfig.PcmPkt.AgcEnablePkt2Pcm = 0;
	chanConfig.PcmPkt.VadEnablePkt = 0;

    ADet = getToneType(channelId, ADevice);
    BDet = getToneType(channelId, BDevice);
    getDtmfDetectFlags(channelId, &ADet, &BDet);
	chanConfig.PcmPkt.ToneTypes = Tone_Generate | ADet;
	chanConfig.PcmPkt.ToneTypesPkt = Tone_Generate | BDet;
	gpakApiStatus =
		gpakConfigureChannel(0, PackCore(chanCore[channelId],channelId), customChannel, &chanConfig,
							 &dspChanStatus);
	if (gpakApiStatus != GpakApiSuccess)
	{
        stressStats[channelId].chCfgFail++;
		uartPrintf("* Failed to config chan %u (API status = %d, DSP status = %d)\n",
				   channelId, gpakApiStatus, dspChanStatus);
		return;
	}
    stressStats[channelId].chCfgPass++;

	if (channelId < NUM_TEST_CHANS)
	{
		rtpTxPktCnt[channelId] = 0;
		rtpRxPktCnt[channelId] = 0;
	}
	uartPrintf("Channel:Core %u:%u setup: Codec = %s, Frame = %s\n",
			   channelId, chanCore[channelId], codecTypeName(chanConfig.PcmPkt.PktInCoding),
			   frameSizeName(chanConfig.PcmPkt.PktInFrameHalfMS));
	uartPrintf(" In Slot = %u, Out Slot = %u VadA = %s, VadB = %s\n",
			   chanConfig.PcmPkt.PcmInSlot, chanConfig.PcmPkt.PcmOutSlot,
			   algoStateName(chanConfig.PcmPkt.VadEnable),
			   algoStateName(chanConfig.PcmPkt.VadEnablePkt));
	uartPrintf(" AEC = %s, LEC = %s, AGCA = %s, AGCB = %s\n",
			   algoStateName(chanConfig.PcmPkt.AECEcanEnable),
			   algoStateName(chanConfig.PcmPkt.PcmEcanEnable),
			   algoStateName(chanConfig.PcmPkt.AgcEnable),
			   algoStateName(chanConfig.PcmPkt.AgcEnablePkt2Pcm));
	uartPrintf(" In Gain = %d, Out Gain = %d\n",
			   chanConfig.PcmPkt.InputGainG3, chanConfig.PcmPkt.OutputGainG2);
	if (chanIpVersion[channelId] == 4)
    {
        if (chanMultiCast[channelId] == 0) {
	        uartPrintf(" JB size = %u, RTP Dest = %s:%u\n",
			   rtpConfig.DelayTargetMS, ipAddressToString(rtpConfig.DestIP),
			   rtpConfig.DestPort);
        } else {
	        uartPrintf(" JB size = %u, RTP Dest = %s:%u\n",
			   rtpConfig.DelayTargetMS, ipAddressToString(BaseMCastIP),
			   rtpConfig.DestPort);
        }
	}
    else if (chanIpVersion[channelId] == 6)
    {
        if (chanMultiCast[channelId] == 0)
		    memcpy(&ipV6Address,&chanDestRtpV6Ip[channelId].IpV6Address, sizeof(GpakIPv6_t));
        else
		    memcpy(&ipV6Address,&BaseMCastIPv6, sizeof(GpakIPv6_t));
		uartPrintf(" JB size = %u, RTP Dest = %s:%u\n",
				   rtpV6Config.DelayTargetMS, ipV6AddressToString(ipV6Address),
				   rtpV6Config.DestPort);

	}
	return;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// setupTxMCASTPcmToPktChannel - Configure a PCM to Packet channel on the DSP.
//
// FUNCTION
//  This function configures a PCM to Packet channel on the DSP.
//
// RETURNS
//  nothing
//
static void setupTxMCASTPcmToPktChannel(
	ADT_UInt16 channelId		// channel Id
	)
{
	GpakRTPCfg_t rtpConfig;					// DSP channel's RTP configuration
	GpakApiStatus_t gpakApiStatus;			// host API status
	GPAK_RTPConfigStat_t dspRtpStatus;		// DSP's RTP status
	GpakChannelConfig_t chanConfig;			// channel configuration info
	GPAK_ChannelConfigStat_t dspChanStatus;	// DSP's channel config status
    GpakToneTypes ADet, BDet;

	// Configure the channel's RTP.
	memset(&rtpConfig, 0, sizeof(rtpConfig));
	rtpConfig.JitterMode = 0;
	rtpConfig.SamplingHz = 8000;
	rtpConfig.DelayTargetMS = chanJitrBufrSize[channelId];
	rtpConfig.DelayTargetMinMS = 10;
	rtpConfig.DelayTargetMaxMS = 120;
	rtpConfig.TonePyldType = 98;
	rtpConfig.T38PyldType = 99;
	rtpConfig.CNGPyldType = 97;
	rtpConfig.StartSequence = 1;
	rtpConfig.SSRC = 0; // ssrcFromChan(channelId);
    initRtpPayloadType(channelId, &rtpConfig);
	rtpConfig.VlanIdx = VlanIdx[channelId];
	rtpConfig.DSCP = 0;
	rtpConfig.DestMAC[0] = 0;
	rtpConfig.DestMAC[1] = 0;
	rtpConfig.DestMAC[2] = 0;
	rtpConfig.DestMAC[3] = 0;
	rtpConfig.DestMAC[4] = 0;
	rtpConfig.DestMAC[5] = 0;
	if (channelId < NUM_TEST_CHANS)
	{
        initRtpPayloadType(channelId, &rtpConfig);
    	rtpConfig.InDestIP = 0;
		rtpConfig.DestIP = 0; //chanDestRtpIp[channelId];

        // multicast transmit IP destination address (mcast receivers will listen on)
        rtpConfig.DestTxIP = BaseMCastIP;

        // multicast transmit UDP Src port
        rtpConfig.SrcPort = chanSourceRtpPort[channelId];
		rtpConfig.DestPort = chanDestRtpPort[channelId];		
		rtpConfig.DelayTargetMS = chanJitrBufrSize[channelId];
	}
	else
	{
		rtpConfig.VoicePyldType = 0;
		rtpConfig.CNGPyldType = 13;
		rtpConfig.DestIP = 0xAC1000A5;		// 172.16.0.165
		rtpConfig.DestTxIP = 0xAC1000A5;	// 172.16.0.165
		rtpConfig.DestPort = 50000 + channelId;
		rtpConfig.DelayTargetMS = chanFrameSize[channelId];
	}
	rtpConfig.DestSSRC = 0; //dstSSRCFromChan(channelId);
	rtpConfig.SamplingHz = 8000;
	gpakApiStatus =
		gpakSendRTPMsg(0, PackCore(chanCore[channelId],channelId), &rtpConfig, &dspRtpStatus);
	if (gpakApiStatus != GpakApiSuccess)
	{
		uartPrintf("* Failed to configure RTP (API status = %d, DSP status = %d)\n",
				   gpakApiStatus, dspRtpStatus);
		return;
	}

	gpakHostDelay ();
	if(chanCryptoSuite[channelId] != 0) {
		GpakSRTPCfg_t    SRTPCfg;
		GpakApiStatus_t  srtpCfgStat = 0;
		GpakSrtpStatus_t     srtpDspStat = 0;
		{
	        srtpGetConfig(channelId, chanCryptoSuite[channelId]-1, &SRTPCfg);
			srtpCfgStat = gpakConfigSRTP (0, PackCore(chanCore[channelId],channelId), &SRTPCfg, &srtpDspStat);
			if (srtpCfgStat != GpakApiSuccess || srtpDspStat != GPAK_SRTP_SUCCESS) {
				uartPrintf ("SRTP channel %d setup failure %d:%d:%d\n", channelId, srtpCfgStat, srtpDspStat, DSPError [0]);
			}
		}
	}
	gpakHostDelay ();
	// Configure the channel.
    chanConfig.PcmPkt.PcmInPort = McBSPPort0;
	chanConfig.PcmPkt.PcmInSlot = channelId;
	chanConfig.PcmPkt.PcmInPin = 0;
	chanConfig.PcmPkt.PcmOutPort = chanConfig.PcmPkt.PcmInPort;
	chanConfig.PcmPkt.PcmOutSlot = channelId;
	chanConfig.PcmPkt.PcmOutPin = 0;
		chanConfig.PcmPkt.PktInCoding = chanCodecType[channelId];
		chanConfig.PcmPkt.PktOutCoding = chanCodecType[channelId];
		chanConfig.PcmPkt.PktInFrameHalfMS = chanFrameSize[channelId];
		chanConfig.PcmPkt.PktOutFrameHalfMS = chanFrameSize[channelId];
		chanConfig.PcmPkt.PcmEcanEnable = Disabled;
	chanConfig.PcmPkt.PktEcanEnable = Disabled;
	chanConfig.PcmPkt.AECEcanEnable = Disabled;
	chanConfig.PcmPkt.VadEnable = chanVadCngEnable[channelId];
	chanConfig.PcmPkt.AgcEnable = chanAgcEnable[channelId];
    //ADet = getToneType(channelId, ADevice);
	chanConfig.PcmPkt.CIdMode = CidDisabled;
	chanConfig.PcmPkt.FaxMode = chanFaxMode[channelId];
	chanConfig.PcmPkt.FaxTransport = FAX_TRANSPORT;
	chanConfig.PcmPkt.Coding = (GpakChannelTypes)chanDataEnable[channelId];
	chanConfig.PcmPkt.ToneGenGainG1 = 0;
	chanConfig.PcmPkt.OutputGainG2 = chanOutGainA[channelId];
	chanConfig.PcmPkt.InputGainG3 = chanInGainA[channelId];
	chanConfig.PcmPkt.DtxEnable = chanDtxEnable[channelId];
	chanConfig.PcmPkt.AgcEnablePkt2Pcm = 0;
	chanConfig.PcmPkt.VadEnablePkt = 0; //Enabled;
	ADet = 0; //DTMF_tone; //ToneDetect;
	BDet = DTMF_tone; //0; //ToneDetect;
	chanConfig.PcmPkt.ToneTypes = Tone_Generate | ADet | Notify_Host;
	chanConfig.PcmPkt.ToneTypesPkt = Tone_Generate | BDet | Notify_Host;;
	gpakApiStatus =
		gpakConfigureChannel(0, PackCore(chanCore[channelId],channelId), pcmToPacket, &chanConfig,
							 &dspChanStatus);
	if (gpakApiStatus != GpakApiSuccess)
	{
		uartPrintf("* Failed to config chan %u (API status = %d, DSP status = %d)\n",
				   channelId, gpakApiStatus, dspChanStatus);
		return;
	}
	if (channelId < NUM_TEST_CHANS)
	{
		rtpTxPktCnt[channelId] = 0;
		rtpRxPktCnt[channelId] = 0;
	}

	uartPrintf("Channel %u setup: Codec = %s, Frame = %s\n",
			   channelId, codecTypeName(chanConfig.PcmPkt.PktInCoding),
			   frameSizeName(chanConfig.PcmPkt.PktInFrameHalfMS));
	uartPrintf(" In Slot = %u, Out Slot = %u VadA = %s, VadB = %s\n",
			   chanConfig.PcmPkt.PcmInSlot, chanConfig.PcmPkt.PcmOutSlot,
			   algoStateName(chanConfig.PcmPkt.VadEnable),
			   algoStateName(chanConfig.PcmPkt.VadEnablePkt));
	uartPrintf(" AEC = %s, LEC = %s, AGCA = %s, AGCB = %s\n",
			   algoStateName(chanConfig.PcmPkt.AECEcanEnable),
			   algoStateName(chanConfig.PcmPkt.PcmEcanEnable),
			   algoStateName(chanConfig.PcmPkt.AgcEnable),
			   algoStateName(chanConfig.PcmPkt.AgcEnablePkt2Pcm));
	uartPrintf(" In Gain = %d, Out Gain = %d\n",
			   chanConfig.PcmPkt.InputGainG3, chanConfig.PcmPkt.OutputGainG2);
	uartPrintf(" JB size = %u, RTP Dest:Destport = %s:%u,  RTP DestTx:Destport = %s:%u\n",
			   rtpConfig.DelayTargetMS, ipAddressToString(rtpConfig.DestIP), rtpConfig.DestPort, ipAddressToString1(rtpConfig.DestTxIP),
			   rtpConfig.DestPort);

	return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// setupRxMCASTPcmToPktChannel - Configure a PCM to Packet channel on the DSP.
//
// FUNCTION
//  This function configures a PCM to Packet channel on the DSP.
//
// RETURNS
//  nothing
//
static void setupRxMCASTPcmToPktChannel(
	ADT_UInt16 channelId		// channel Id
	)
{
	GpakRTPCfg_t rtpConfig;					// DSP channel's RTP configuration
	GpakApiStatus_t gpakApiStatus;			// host API status
	GPAK_RTPConfigStat_t dspRtpStatus;		// DSP's RTP status
	GpakChannelConfig_t chanConfig;			// channel configuration info
	GPAK_ChannelConfigStat_t dspChanStatus;	// DSP's channel config status
    GpakToneTypes ADet, BDet;

	// Configure the channel's RTP.
	memset(&rtpConfig, 0, sizeof(rtpConfig));
	rtpConfig.JitterMode = 0;
	rtpConfig.SamplingHz = 8000;
	rtpConfig.DelayTargetMS = chanJitrBufrSize[channelId];
	rtpConfig.DelayTargetMinMS = 10;
	rtpConfig.DelayTargetMaxMS = 120;
	rtpConfig.TonePyldType = 98;
	rtpConfig.T38PyldType = 99;
	rtpConfig.CNGPyldType = 97;
	rtpConfig.StartSequence = 1;
	rtpConfig.SSRC = ssrcFromChan(channelId); //channelId;
	rtpConfig.VlanIdx = VlanIdx[channelId];
	rtpConfig.DSCP = 0;
	rtpConfig.DestMAC[0] = 0;
	rtpConfig.DestMAC[1] = 0;
	rtpConfig.DestMAC[2] = 0;
	rtpConfig.DestMAC[3] = 0;
	rtpConfig.DestMAC[4] = 0;
	rtpConfig.DestMAC[5] = 0;
	if (channelId < NUM_TEST_CHANS)
	{
        initRtpPayloadType(channelId, &rtpConfig);
       	
        // Destination IP of in-bound packets (For multicast receives)
	    rtpConfig.InDestIP = BaseMCastIP;
	    rtpConfig.DestIP = 0; 
        rtpConfig.DestTxIP = 0;
        rtpConfig.SrcPort = chanSourceRtpPort[channelId];
		rtpConfig.DestPort = chanDestRtpPort[channelId];
		rtpConfig.DelayTargetMS = chanJitrBufrSize[channelId];
	}
	else
	{
		rtpConfig.VoicePyldType = 0;
		rtpConfig.CNGPyldType = 13;
		rtpConfig.DestIP = 0xAC1000A5;		// 172.16.0.165
		rtpConfig.DestTxIP = 0xAC1000A5;	// 172.16.0.165
		rtpConfig.DestPort = 50000 + channelId;
		rtpConfig.DelayTargetMS = chanFrameSize[channelId];
	}
	rtpConfig.DestSSRC = dstSSRCFromChan(channelId);
	rtpConfig.SamplingHz = 8000;
	gpakApiStatus =
		gpakSendRTPMsg(0, PackCore(chanCore[channelId],channelId), &rtpConfig, &dspRtpStatus);
	if (gpakApiStatus != GpakApiSuccess)
	{
		uartPrintf("* Failed to configure RTP (API status = %d, DSP status = %d)\n",
				   gpakApiStatus, dspRtpStatus);
		return;
	}

    chanConfig.PcmPkt.PcmInPort = SerialPortNull;
	chanConfig.PcmPkt.PcmInSlot =  chanInSlotA[channelId];
	chanConfig.PcmPkt.PcmInPin = 0;
	chanConfig.PcmPkt.PcmOutPort = APort; //chanConfig.PcmPkt.PcmInPort;
	chanConfig.PcmPkt.PcmOutSlot = chanOutSlotA[channelId];
	chanConfig.PcmPkt.PcmOutPin = 0;
	if (channelId < NUM_TEST_CHANS)
	{
		chanConfig.PcmPkt.PktInCoding = chanCodecType[channelId];
		chanConfig.PcmPkt.PktOutCoding = NullCodec; //chanCodecType[channelId];
		chanConfig.PcmPkt.PktInFrameHalfMS = chanFrameSize[channelId];
		chanConfig.PcmPkt.PktOutFrameHalfMS = chanFrameSize[channelId];
	}
	else
	{
		chanConfig.PcmPkt.PktInCoding = PCMU_64;
		chanConfig.PcmPkt.PktOutCoding = PCMU_64;
		chanConfig.PcmPkt.PktInFrameHalfMS = Frame_10ms;
		chanConfig.PcmPkt.PktOutFrameHalfMS = Frame_10ms;
	}
	if (chanAecEnable[channelId] == Enabled)
	{
		chanConfig.PcmPkt.PcmEcanEnable = Disabled;
	}
	else
	{
		chanConfig.PcmPkt.PcmEcanEnable = chanLecEnable[channelId];
	}
	chanConfig.PcmPkt.PktEcanEnable = Disabled;
	chanConfig.PcmPkt.AECEcanEnable = chanAecEnable[channelId];
	chanConfig.PcmPkt.VadEnable = chanVadCngEnable[channelId];
	chanConfig.PcmPkt.AgcEnable = chanAgcEnable[channelId];
	chanConfig.PcmPkt.ToneTypes = Tone_Generate | DTMF_tone| Notify_Host;// | Arbit_tone 
	chanConfig.PcmPkt.CIdMode = CidDisabled;
	chanConfig.PcmPkt.FaxMode = chanFaxMode[channelId];
	chanConfig.PcmPkt.FaxTransport = FAX_TRANSPORT;
	chanConfig.PcmPkt.Coding = (GpakChannelTypes)chanDataEnable[channelId];
	chanConfig.PcmPkt.ToneGenGainG1 = 0;
	chanConfig.PcmPkt.OutputGainG2 = chanOutGainA[channelId];
	chanConfig.PcmPkt.InputGainG3 = chanInGainA[channelId];
	gpakApiStatus =
		gpakConfigureChannel(0, PackCore(chanCore[channelId],channelId), pcmToPacket, &chanConfig,
							 &dspChanStatus);
	if (gpakApiStatus != GpakApiSuccess)
	{
		uartPrintf("* Failed to config chan %u (API status = %d, DSP status = %d)\n",
				   channelId, gpakApiStatus, dspChanStatus);
		return;
	}
	if (channelId < NUM_TEST_CHANS)
	{
		rtpTxPktCnt[channelId] = 0;
		rtpRxPktCnt[channelId] = 0;
	}

	uartPrintf("Channel %u setup: Codec = %s, Frame = %s\n",
			   channelId, codecTypeName(chanConfig.PcmPkt.PktInCoding),
			   frameSizeName(chanConfig.PcmPkt.PktInFrameHalfMS));
	uartPrintf(" In Slot = %u, Out Slot = %u VadA = %s, VadB = %s\n",
			   chanConfig.PcmPkt.PcmInSlot, chanConfig.PcmPkt.PcmOutSlot,
			   algoStateName(chanConfig.PcmPkt.VadEnable),
			   algoStateName(chanConfig.PcmPkt.VadEnablePkt));
	uartPrintf(" AEC = %s, LEC = %s, AGCA = %s, AGCB = %s\n",
			   algoStateName(chanConfig.PcmPkt.AECEcanEnable),
			   algoStateName(chanConfig.PcmPkt.PcmEcanEnable),
			   algoStateName(chanConfig.PcmPkt.AgcEnable),
			   algoStateName(chanConfig.PcmPkt.AgcEnablePkt2Pcm));
	uartPrintf(" In Gain = %d, Out Gain = %d\n",
			   chanConfig.PcmPkt.InputGainG3, chanConfig.PcmPkt.OutputGainG2);
	uartPrintf(" JB size = %u, RTP InDest:SrcPort = %s:%u,  RTP DestTx:DestPort = %s:%u\n",
			   rtpConfig.DelayTargetMS, ipAddressToString(rtpConfig.InDestIP),rtpConfig.SrcPort,  ipAddressToString1(rtpConfig.DestTxIP),
			   rtpConfig.DestPort);

	return;
}
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// setupPcmToPcmChannel - Configure a PCM to PCM channel on the DSP.
//
// FUNCTION
//  This function configures a PCM to PCM channel on the DSP.
//
// RETURNS
//  nothing
//
static void setupPcmToPcmChannel(
	ADT_UInt16 channelId		// channel Id
	)
{
	GpakApiStatus_t gpakApiStatus;			// host API status
	GpakChannelConfig_t chanConfig;			// channel configuration info
	GPAK_ChannelConfigStat_t dspChanStatus;	// DSP's channel config status

	// Configure the channel.
#if 0
	if (channelId >= 16)
	{
		chanConfig.PcmPcm.InPortA = APort;
		chanConfig.PcmPcm.InPortB = BPort;
	}
	else
#endif
	{
		chanConfig.PcmPcm.InPortA = APort;
		chanConfig.PcmPcm.InPortB = BPort;
	}
#if 1
	chanConfig.PcmPcm.InSlotA = 2; //RC HW chanInSlotA[channelId];
	chanConfig.PcmPcm.InPinA = 0;
	chanConfig.PcmPcm.OutPortA = chanConfig.PcmPcm.InPortA;
	chanConfig.PcmPcm.OutSlotA = 2; //RC HW chanOutSlotA[channelId];
	chanConfig.PcmPcm.OutPinA = 0;
	chanConfig.PcmPcm.InSlotB = 3; //RC HW chanInSlotB[channelId];
	chanConfig.PcmPcm.InPinB = 0;
	chanConfig.PcmPcm.OutPortB = chanConfig.PcmPcm.InPortB;
	chanConfig.PcmPcm.OutSlotB = 3; //RC HW chanOutSlotB[channelId];
	chanConfig.PcmPcm.OutPinB = 0;
	chanConfig.PcmPcm.EcanEnableA = chanLecEnable[channelId];
	chanConfig.PcmPcm.EcanEnableB = chanLecEnable[channelId];
	chanConfig.PcmPcm.AECEcanEnableA = chanAecEnable[channelId];
	chanConfig.PcmPcm.AECEcanEnableB = Disabled;
	chanConfig.PcmPcm.NoiseSuppressA = Disabled;
	chanConfig.PcmPcm.NoiseSuppressB = Disabled;
	chanConfig.PcmPcm.AgcEnableA = chanAgcEnable[channelId];
	chanConfig.PcmPcm.AgcEnableB = chanAgcEnable[channelId];
	chanConfig.PcmPcm.ToneTypesA = Tone_Generate | DTMF_tone | Notify_Host;// | Arbit_tone
	chanConfig.PcmPcm.ToneTypesB = Tone_Generate | DTMF_tone | Notify_Host;// | Arbit_tone
	chanConfig.PcmPcm.CIdModeA = CidDisabled;
	chanConfig.PcmPcm.CIdModeB = CidDisabled;
	chanConfig.PcmPcm.Coding = (GpakChannelTypes)chanDataEnable[channelId];
	chanConfig.PcmPcm.ToneGenGainG1A = 0;
	chanConfig.PcmPcm.OutputGainG2A = chanOutGainA[channelId];
	chanConfig.PcmPcm.InputGainG3A = chanInGainA[channelId];
	chanConfig.PcmPcm.ToneGenGainG1B = 0;
	chanConfig.PcmPcm.OutputGainG2B = chanOutGainB[channelId];
	chanConfig.PcmPcm.InputGainG3B = chanInGainB[channelId];
	if (channelId < NUM_TEST_CHANS)
	{
		chanConfig.PcmPcm.FrameHalfMS = chanFrameSize[channelId];
	}
	else
	{
		chanConfig.PcmPcm.FrameHalfMS = Frame_10ms;
	}
	gpakApiStatus =
		gpakConfigureChannel(0, PackCore(chanCore[channelId],channelId), pcmToPcm, &chanConfig,
							 &dspChanStatus);
	if (gpakApiStatus != GpakApiSuccess)
	{
		uartPrintf("* Failed to config chan %u (API status = %d, DSP status = %d)\n",
				   channelId, gpakApiStatus, dspChanStatus);
		return;
	}

	uartPrintf("Channel %u setup for PCM to PCM: Frame = %s\n",
			   channelId, frameSizeName(chanConfig.PcmPcm.FrameHalfMS));
	uartPrintf(" In Slot A = %u, Out Slot A = %u\n",
			   chanConfig.PcmPcm.InSlotA, chanConfig.PcmPcm.OutSlotA);
	uartPrintf(" In Slot B = %u, Out Slot B = %u\n",
			   chanConfig.PcmPcm.InSlotB, chanConfig.PcmPcm.OutSlotB);
	uartPrintf(" AEC = %s, LEC = %s, AGC = %s\n",
			   algoStateName(chanConfig.PcmPcm.AECEcanEnableA),
			   algoStateName(chanConfig.PcmPcm.EcanEnableA),
			   algoStateName(chanConfig.PcmPcm.AgcEnableA));
	uartPrintf(" In Gain A = %d, Out Gain A = %d\n",
			   chanConfig.PcmPcm.InputGainG3A, chanConfig.PcmPcm.OutputGainG2A);
	uartPrintf(" In Gain B = %d, Out Gain B = %d\n",
			   chanConfig.PcmPcm.InputGainG3B, chanConfig.PcmPcm.OutputGainG2B);
#endif

}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// getChannelStatus - Get status for a channel on the DSP.
//
// FUNCTION
//  This function gets status for a channel on the DSP.
//
// RETURNS
//  nothing
//
void getChannelStatus(
	ADT_UInt16 channelId,		// channel Id
	Bool rtpRxTxOnly			// flag: only display RTP Rx and Tx pkt counts
	)
{
	GpakApiStatus_t gpakApiStatus;			// host API status
	GpakChannelStatus_t dspChanInfo;		// DSP's channel status info
	GPAK_ChannelStatusStat_t dspChanStatus;	// DSP's channel status status
	GPAK_RTP_STATS dspRtpStats;				// DSP channel's RTP statistics
	GPAK_RTP_Stat_Rply_t dspRtpStatus;		// DSP's RTP statistics status
    GPAK_RTCP_STATS dspRTCPStats;
    int i;

	// Get the channel's status info.
	gpakApiStatus =
		gpakGetChannelStatus(0, PackCore(chanCore[channelId],channelId), &dspChanInfo, &dspChanStatus);
	if (gpakApiStatus != GpakApiSuccess)
	{
		uartPrintf("* Failed to get channel %u status (API status = %d, DSP status = %d)\n",
				   channelId, gpakApiStatus, dspChanStatus);
		return;
	}

	// Display the channel's status info.
	if (dspChanInfo.ChannelType == inactive)
	{
		uartPrintf("Channel %u is inactive.\n", channelId);
		return;
	}
	if (!rtpRxTxOnly)
	{
		uartPrintf("Channel %u:Core%u status:\n", CHAN_ID(dspChanInfo.ChannelId), CORE_ID(dspChanInfo.ChannelId));
		uartPrintf(" Type = ");
		switch (dspChanInfo.ChannelType)
		{
		case pcmToPacket:
			uartPrintf("PCM to Pkt");
			break;
		case pcmToPcm:
			uartPrintf("PCM to PCM");
			break;
		case packetToPacket:
			uartPrintf("Pkt to Pkt");
			break;
		default:
			uartPrintf("%d", dspChanInfo.ChannelType);
			break;
		}
		uartPrintf("\n");
		uartPrintf(" NumPktsToDsp = %u\n", dspChanInfo.NumPktsToDsp);
		uartPrintf(" NumPktsFromDsp = %u\n", dspChanInfo.NumPktsFromDsp);
		if (dspChanInfo.ChannelType == packetToPacket)
		{
			uartPrintf(" NumPktsToDspB = %u\n", dspChanInfo.NumPktsToDspB);
			uartPrintf(" NumPktsFromDspB = %u\n", dspChanInfo.NumPktsFromDspB);
		}
		uartPrintf(" BadPktHeaderCnt = %u\n", dspChanInfo.BadPktHeaderCnt);
		uartPrintf(" PktOverflowCnt = %u\n", dspChanInfo.PktOverflowCnt);
		uartPrintf(" PktUnderflowCnt = %u\n", dspChanInfo.PktUnderflowCnt);
		uartPrintf(" FrameCount = %u\n", dspChanInfo.StatusFlags);
	}

	// Get the RTP statistics.
	if ((dspChanInfo.ChannelType != pcmToPacket) &&
		(dspChanInfo.ChannelType != packetToPacket) &&
        (dspChanInfo.ChannelType != customChannel))
	{
		if (rtpRxTxOnly)
		{
			uartPrintf("Channel %u is not a packet type channel.\n", channelId);
		}
		return;
	}


    if (chanRtcpEnable[channelId] == Enabled) {
        dspRTCPStats.appReport.block =   rtcpAppData;
        dspRTCPStats.byeReport.reason =   rtcpByeData;
        for (i=0; i<MAX_SDES_ITEMS; i++) {
            dspRTCPStats.sdesReport.item[i].data = &rtcpSdesData[i];
        }
        memset(rtcpAppData, 0, sizeof(rtcpAppData));
        memset(rtcpByeData, 0, sizeof(rtcpByeData));
        memset(rtcpSdesData, 0, sizeof(rtcpSdesData));
	    gpakApiStatus =
		    gpakGetRTCPStatistics(0, PackCore(chanCore[channelId],channelId), &dspRtpStats, &dspRTCPStats, &dspRtpStatus);
    	if (gpakApiStatus != GpakApiSuccess)
    	{
    		uartPrintf("* Failed to get channel %u RTCP stats (API status = %d, DSP status = %d)\n",
    				   channelId, gpakApiStatus, dspRtpStatus);
    		return;
    	}

		uartPrintf(" RTP:\n");
		uartPrintf("  PktsSent = %u\n", dspRtpStats.PktsSent);
		uartPrintf("  PktsRcvd = %u\n", dspRtpStats.PktsRcvd);
		uartPrintf("  PktsPlayed = %u\n", dspRtpStats.PktsPlayed);
		uartPrintf("  PktsLate = %u\n", dspRtpStats.PktsLate);
		uartPrintf("  PktsOutOfRange = %u\n", dspRtpStats.PktsOutOfRange);
		uartPrintf("  BufferOverflowCnt = %u\n", dspRtpStats.BufferOverflowCnt);
		uartPrintf("  BufferUnderflowCnt = %u\n",
				   dspRtpStats.BufferUnderflowCnt);
		uartPrintf("  MinDelayMS = %u\n", dspRtpStats.MinDelayMS);
		uartPrintf("  MaxDelayMS = %u\n", dspRtpStats.MaxDelayMS);
		uartPrintf("  CurrentDelayMS = %u\n", dspRtpStats.CurrentDelayMS);
		uartPrintf("  LossPercentage= %f\n",    dspRtpStats.LossPercentage);
		uartPrintf("  JitterMinMS= %f\n",    dspRtpStats.JitterMinMS);
		uartPrintf("  JitterCurrentMS= %f\n",    dspRtpStats.JitterCurrentMS);
		uartPrintf("  JitterMaxMS= %f\n",    dspRtpStats.JitterMaxMS);
   
        if (dspRTCPStats.validFlags == 0) return;

        if (dspRTCPStats.validFlags & RR_VALID) {
		    uartPrintf(" RTCP: RR_VALID\n");
		    uartPrintf(" roundTripTimeMs = %f\n",    dspRTCPStats.roundTripTimeMs);
		    uartPrintf(" RR_JitterEstimate = %u\n",  dspRTCPStats.rr_JitterEstimate);
		    uartPrintf(" RR_cumPktsLost = %u\n",     dspRTCPStats.rr_cumPktsLost);
		    uartPrintf(" RR_fractionPktsLost = %f\n",dspRTCPStats.rr_fractionPktsLost);
        }

        if (dspRTCPStats.validFlags & SR_VALID) {
		    uartPrintf(" RTCP: SR_VALID\n");
		    uartPrintf(" SR_PktsSent = %u\n",        dspRTCPStats.sr_PktsSent);
		    uartPrintf(" SR_BytesSent = %u\n",       dspRTCPStats.sr_BytesSent);
        }

        if (dspRTCPStats.validFlags & SDES_VALID) {
		    uartPrintf(" RTCP: SDES_VALID\n");
            for (i=0; i<dspRTCPStats.sdesReport.itemCnt; i++) {
                uartPrintf(" SDES Item %d: type %d, %s\n", i, 
                    dspRTCPStats.sdesReport.item[i].type, dspRTCPStats.sdesReport.item[i].data);
            } 

        }
        if (dspRTCPStats.validFlags & BYE_VALID) {
		    uartPrintf(" RTCP: BYE_VALID\n");
            uartPrintf(" BYE reason: %s\n", dspRTCPStats.byeReport.reason);
        }

        if (dspRTCPStats.validFlags & APP_VALID) {
		    uartPrintf(" RTCP: APP_VALID\n");
            uartPrintf(" APP block: %s\n", dspRTCPStats.appReport.block);
        }

        return;
    }

	gpakApiStatus =
		gpakGetRTPStatistics(0, PackCore(chanCore[channelId],channelId), &dspRtpStats, &dspRtpStatus);
	if (gpakApiStatus != GpakApiSuccess)
	{
		uartPrintf("* Failed to get channel %u RTP stats (API status = %d, DSP status = %d)\n",
				   channelId, gpakApiStatus, dspRtpStatus);
		return;
	}

	// Display the channel's RTP statistics.
	if (rtpRxTxOnly)
	{
		uartPrintf("Channel %u RTP Packet Stats:\n", channelId);
		if (channelId < NUM_TEST_CHANS)
		{
			uartPrintf(" Tx = %u (+%u)\n", dspRtpStats.PktsSent,
					   dspRtpStats.PktsSent - rtpTxPktCnt[channelId]);
			uartPrintf(" Rx = %u (+%u)\n", dspRtpStats.PktsRcvd,
					   dspRtpStats.PktsRcvd - rtpRxPktCnt[channelId]);
			rtpTxPktCnt[channelId] = dspRtpStats.PktsSent;
			rtpRxPktCnt[channelId] = dspRtpStats.PktsRcvd;
            stressStats[channelId].pktTxCnt = rtpTxPktCnt[channelId];
            stressStats[channelId].pktRxCnt = rtpRxPktCnt[channelId];
		}
		else
		{
			uartPrintf(" Tx = %u\n", dspRtpStats.PktsSent);
			uartPrintf(" Rx = %u\n", dspRtpStats.PktsRcvd);
		}
	}
	else
	{
		uartPrintf(" RTP:\n");
		uartPrintf("  PktsSent = %u\n", dspRtpStats.PktsSent);
		uartPrintf("  PktsRcvd = %u\n", dspRtpStats.PktsRcvd);
		uartPrintf("  PktsPlayed = %u\n", dspRtpStats.PktsPlayed);
		uartPrintf("  PktsLate = %u\n", dspRtpStats.PktsLate);
		uartPrintf("  PktsOutOfRange = %u\n", dspRtpStats.PktsOutOfRange);
		uartPrintf("  BufferOverflowCnt = %u\n", dspRtpStats.BufferOverflowCnt);
		uartPrintf("  BufferUnderflowCnt = %u\n",
				   dspRtpStats.BufferUnderflowCnt);
		uartPrintf("  CurrentDelayMS = %u\n", dspRtpStats.CurrentDelayMS);
	}

	return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// teardownChannel - Teardown a channel on the DSP.
//
// FUNCTION
//  This function tears down a channel on the DSP.
//
// RETURNS
//  nothing
//
void teardownChannel(
	ADT_UInt16 channelId		// channel Id
	)
{
	GpakApiStatus_t gpakApiStatus;				// host API status
	GPAK_TearDownChanStat_t dspTearDownStatus;	// DSP's tear down status

	// Tear down the channel.
	gpakApiStatus = gpakTearDownChannel(0, PackCore(chanCore[channelId],channelId), &dspTearDownStatus);
	if (gpakApiStatus != GpakApiSuccess)
	{
		if ((gpakApiStatus == GpakApiParmError) &&
			(dspTearDownStatus == Td_ChannelNotActive))
		{
			uartPrintf("Channel %u isn't active\n", channelId);
		}
		else
		{
			uartPrintf("* Failed to tear down channel %u (API status = %d, DSP status = %d)\n",
					   channelId, gpakApiStatus, dspTearDownStatus);
		}
		return;
	}

	uartPrintf("Channel %u torn down OK\n", channelId);

	return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// startToneGen - Start generating a tone for a channel on the DSP.
//
// FUNCTION
//  This function starts generating a tone for a channel on the DSP.
//
// RETURNS
//  nothing
//
static void startToneGen(
	ADT_UInt16 channelId,		// channel Id
	GpakDeviceSide_t sideId,	// side of channel Id 
	ADT_UInt16 toneIdx  		// tone index
	)
{
	GpakToneGenParms_t toneGenParms;	// DSP's tone gen parameters
	gpakGenToneStatus_t apiStatus;		// API status
	GPAK_ToneGenStat_t dspStatus;		// DSP status
	char side;							// side of channel indicator

	//memcpy(&toneGenParms, &DSPtones[toneIdx], sizeof(toneGenParms));
	memcpy(&toneGenParms, &DSPtonesHires[toneIdx], sizeof(toneGenParms));
	toneGenParms.Device = sideId;
	toneGenParms.ToneCmd = ToneGenStart;
	apiStatus = gpakToneGenerateHighRes (0, PackCore(chanCore[channelId],channelId), &toneGenParms, &dspStatus); 

	if (sideId == ADevice)
	{
		side = 'A';
	}
	else
	{
		side = 'B';
	}
	if ((apiStatus != GpakApiSuccess) || (dspStatus != Tg_Success))
	{
		uartPrintf("* Failed to start tone on channel %u side %c (API status = %d, DSP status = %d)\n",
				   channelId, side, apiStatus, dspStatus);
		return;
	}
	uartPrintf("Started %u-tone on channel %u side %c\n",
			   toneIdx, channelId, side);

	return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// stopToneGen - Stop generating a tone for a channel on the DSP.
//
// FUNCTION
//  This function stops generating a tone for a channel on the DSP.
//
// RETURNS
//  nothing
//
static void stopToneGen(
	ADT_UInt16 channelId,		// channel Id
	GpakDeviceSide_t sideId		// side of channel Id 
	)
{
	GpakToneGenParms_t toneGenParms;	// DSP's tone gen parameters
	gpakGenToneStatus_t apiStatus;		// API status
	GPAK_ToneGenStat_t dspStatus;		// DSP status
	char side;							// side of channel indicator

	memset(&toneGenParms, 0, sizeof(toneGenParms));
	toneGenParms.Device = sideId;
	toneGenParms.ToneCmd = ToneGenStop;
	apiStatus = gpakToneGenerate(0, PackCore(chanCore[channelId],channelId), &toneGenParms, &dspStatus);
	if (sideId == ADevice)
	{
		side = 'A';
	}
	else
	{
		side = 'B';
	}
	if ((apiStatus != GpakApiSuccess) || (dspStatus != Tg_Success))
	{
		uartPrintf("* Failed to stop tone on channel %u side %c (API status = %d, DSP status = %d)\n",
				   channelId, side, apiStatus, dspStatus);
		return;
	}
	uartPrintf("Stopped tone on channel %u side %c\n", channelId, side);

	return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// getSysConfig - Get the DSP's System Configuration.
//
// FUNCTION
//  This function gets the DSP's System Configuration.
//
// RETURNS
//  nothing
//
static void getSysConfig(void)
{
	GpakApiStatus_t gpakApiStatus;	// host API status
	GpakSystemConfig_t dspSysCfg;	// DSP's System Configuration

	// Read the DSP's System Configuration.
	gpakApiStatus = gpakGetSystemConfig(0, &dspSysCfg);
	if (gpakApiStatus != GpakApiSuccess)
	{
		uartPrintf("* Failed to get system config (API status = %d)\n",
				   gpakApiStatus);
		return;
	}

	// Display the DSP System Configuration.
	uartPrintf("DSP System Configuration:\n");
	uartPrintf(" GpakVersionId = %08X\n", dspSysCfg.GpakVersionId);
	uartPrintf(" MaxChannels = %u\n", dspSysCfg.MaxChannels);
	uartPrintf(" ActiveChannels = %u\n", dspSysCfg.ActiveChannels);
	uartPrintf(" CfgEnableFlags = %08X\n", dspSysCfg.CfgEnableFlags);
	uartPrintf(" PacketProfile = %d\n", dspSysCfg.PacketProfile);
	uartPrintf(" RtpTonePktType = %d\n", dspSysCfg.RtpTonePktType);
	uartPrintf(" CfgFramesChans = %04X\n", dspSysCfg.CfgFramesChans);
	uartPrintf(" Port1NumSlots = %u\n", dspSysCfg.Port1NumSlots);
	uartPrintf(" Port2NumSlots = %u\n", dspSysCfg.Port2NumSlots);
	uartPrintf(" Port3NumSlots = %u\n", dspSysCfg.Port3NumSlots);
	uartPrintf(" Port1SupSlots = %u\n", dspSysCfg.Port1SupSlots);
	uartPrintf(" Port2SupSlots = %u\n", dspSysCfg.Port2SupSlots);
	uartPrintf(" Port3SupSlots = %u\n", dspSysCfg.Port3SupSlots);
	uartPrintf(" NumPcmEcans = %u\n", dspSysCfg.NumPcmEcans);
	uartPrintf(" PcmMaxTailLen = %u\n", dspSysCfg.PcmMaxTailLen);
	uartPrintf(" PcmMaxFirSegs = %u\n", dspSysCfg.PcmMaxFirSegs);
	uartPrintf(" PcmMaxFirSegLen = %u\n", dspSysCfg.PcmMaxFirSegLen);
	uartPrintf(" NumPktEcans = %u\n", dspSysCfg.NumPktEcans);
	uartPrintf(" PktMaxTailLen = %u\n", dspSysCfg.PktMaxTailLen);
	uartPrintf(" PktMaxFirSegs = %u\n", dspSysCfg.PktMaxFirSegs);
	uartPrintf(" PktMaxFirSegLen = %u\n", dspSysCfg.PktMaxFirSegLen);
	uartPrintf(" MaxConferences = %u\n", dspSysCfg.MaxConferences);
	uartPrintf(" MaxNumConfChans = %u\n", dspSysCfg.MaxNumConfChans);
	uartPrintf(" SelfTestStatus = %u\n", dspSysCfg.SelfTestStatus);
	uartPrintf(" MaxToneDetTypes = %u\n", dspSysCfg.MaxToneDetTypes);
	uartPrintf(" AECInstances = %u\n", dspSysCfg.AECInstances);
	uartPrintf(" AECTailLen = %u\n", dspSysCfg.AECTailLen);

	return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// getSysParms - Get the DSP's System Parameters.
//
// FUNCTION
//  This function gets the DSP's System Parameters.
//
// RETURNS
//  nothing
//
static void getSysParms(void)
{
	GpakApiStatus_t gpakApiStatus;	// host API status

	// Read the DSP's System Parameters.
	gpakApiStatus = gpakReadSystemParms(0, &tempSysParms);
	if (gpakApiStatus != GpakApiSuccess)
	{
		uartPrintf("* Failed to get system parms (API status = %d)\n",
				   gpakApiStatus);
		return;
	}


	// Display the DSP System Parameters.
	uartPrintf("DSP System Parameters:\n");
	uartPrintf(" AGC_A.TargetPower = %d\n", tempSysParms.AGC_A.TargetPower);
	uartPrintf(" AGC_A.LossLimit = %d\n", tempSysParms.AGC_A.LossLimit);
	uartPrintf(" AGC_A.GainLimit = %d\n", tempSysParms.AGC_A.GainLimit);
	uartPrintf(" AGC_A.LowSignal = %d\n", tempSysParms.AGC_A.LowSignal);
	uartPrintf(" AGC_B.TargetPower = %d\n", tempSysParms.AGC_B.TargetPower);
	uartPrintf(" AGC_B.LossLimit = %d\n", tempSysParms.AGC_B.LossLimit);
	uartPrintf(" AGC_B.GainLimit = %d\n", tempSysParms.AGC_B.GainLimit);
	uartPrintf(" AGC_B.LowSignal = %d\n", tempSysParms.AGC_B.LowSignal);
	uartPrintf(" VadNoiseFloor = %d\n", tempSysParms.VadNoiseFloor);
	uartPrintf(" VadHangTime = %d\n", tempSysParms.VadHangTime);
	uartPrintf(" VadWindowSize = %d\n", tempSysParms.VadWindowSize);
	uartPrintf(" VadReport = %d\n", tempSysParms.VadReport);

	return;
}

static void writeSysVad(int32_t noise, int32_t hangMs, int32_t winMs, int32_t report) {
	GpakApiStatus_t gpakApiStatus;			// host API status
	GPAK_SysParmsStat_t sysParmsStatus;		// System parms write status
    GpakSystemParms_t sysParms;	// temporary system parameters

    sysParms.VadNoiseFloor = noise;
    sysParms.VadHangTime = hangMs;
    sysParms.VadWindowSize = winMs;
    sysParms.VadReport = report;

	gpakApiStatus = gpakWriteSystemParms(0, &sysParms, 0, 1, 0, 0, 0, 0,
										 &sysParmsStatus);
	if (gpakApiStatus != GpakApiSuccess)
	{
		uartPrintf("* Failed to write VAD parms (API status = %d, DSP status = %d)\n",
				   gpakApiStatus, sysParmsStatus);
		return FALSE;
	}

	// Read the DSP's System Parameters.
	gpakApiStatus = gpakReadSystemParms(0, &tempSysParms);
	if (gpakApiStatus != GpakApiSuccess)
	{
		uartPrintf("* Failed to get system parms (API status = %d)\n",
				   gpakApiStatus);
		return;
	}


	// Display the DSP System Parameters.
	uartPrintf("DSP VAD Parameters:\n");
	uartPrintf(" VadNoiseFloor = %d\n", tempSysParms.VadNoiseFloor);
	uartPrintf(" VadHangTime = %d\n", tempSysParms.VadHangTime);
	uartPrintf(" VadWindowSize = %d\n", tempSysParms.VadWindowSize);
	uartPrintf(" VadReport = %d\n", tempSysParms.VadReport);

	return TRUE;

}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// readIpStackConfig - Read the DSP's IP Stack configuration.
//
// FUNCTION
//  This function reads the DSP's IP Stack configuration.
//
// RETURNS
//  nothing
//
static void readIpStackConfig(void)
{
	GpakIPCfg_t ipStackCfg;			// IP stack config
	GpakApiStatus_t gpakApiStatus;	// G.PAK API status

	gpakApiStatus = gpakReadIPStackCfg(0, &ipStackCfg);
	if (gpakApiStatus != GpakApiSuccess)
	{
		uartPrintf("* Failed to read IP Stack config (%d)\n", gpakApiStatus);
		return;
	}
	uartPrintf("IP Stack Config:\n");
	uartPrintf(" RTPSrcIP = %u\n", ipStackCfg.RTPSrcIP);
	printIpAddress(" RTPSrcIP", ipStackCfg.RTPSrcIP);
	uartPrintf(" TxMac = %02X-%02X-%02X-%02X-%02X-%02X\n",
			   ipStackCfg.TxMac[0], ipStackCfg.TxMac[1], ipStackCfg.TxMac[2],
			   ipStackCfg.TxMac[3], ipStackCfg.TxMac[4], ipStackCfg.TxMac[5]);
	uartPrintf(" RxMac = %02X-%02X-%02X-%02X-%02X-%02X\n",
			   ipStackCfg.RxMac[0], ipStackCfg.RxMac[1], ipStackCfg.RxMac[2],
			   ipStackCfg.RxMac[3], ipStackCfg.RxMac[4], ipStackCfg.RxMac[5]);
	printIpAddress(" IPAddress", ipStackCfg.IPAddress);
	printIpAddress(" IPGateway", ipStackCfg.IPGateway);
	printIpAddress(" IPMask", ipStackCfg.IPMask);
	uartPrintf(" RTPPort = %u\n", ipStackCfg.RTPPort);
	uartPrintf(" MsgPort = %u\n", ipStackCfg.MsgPort);
	uartPrintf(" EvtPort = %u\n", ipStackCfg.EvtPort);
	printIpAddress(" DHCPIP", ipStackCfg.DHCPIP);

	return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// printIpAddress - Print (display) an IP address.
//
// FUNCTION
//  This function prints an IP address following prefix name.
//
// RETURNS
//  nothing
//
static void printIpAddress(
	char *pNameText,			// pointer to name string
	ADT_UInt32 ipAddress		// IP address
	)
{

	uartPrintf("%s = ", pNameText);
	uartPrintf("%s\n", ipAddressToString(ipAddress));

	return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// getTdmStats - Get TDM statistics from the DSP.
//
// FUNCTION
//  This function gets TDM statistics from the DSP.
//
// RETURNS
//  nothing
//
static void getTdmStats(void)
{
	GpakApiStatus_t gpakApiStatus;	// host API status
	gpakMcBspStats_t dspTdmInfo;	// DSP's TDM statistics info

	// Get the DSP's TDM statistics info.
	gpakApiStatus = gpakReadMcBspStats(0, &dspTdmInfo);
	if (gpakApiStatus != GpakApiSuccess)
	{
		uartPrintf("* Failed to get TDM statistics (API status = %d)\n",
				   gpakApiStatus);
		return;
	}

	// Display the TDM statistics.
	uartPrintf("TDM Port 1 statistics:\n");
	uartPrintf(" Status = %u\n", dspTdmInfo.Port1Status);
	uartPrintf(" RxIntCount = %u\n", dspTdmInfo.Port1RxIntCount);
	uartPrintf(" RxSlips = %u\n", dspTdmInfo.Port1RxSlips);
	uartPrintf(" RxDmaErrors = %u\n", dspTdmInfo.Port1RxDmaErrors);
	uartPrintf(" TxIntCount = %u\n", dspTdmInfo.Port1TxIntCount);
	uartPrintf(" TxSlips = %u\n", dspTdmInfo.Port1TxSlips);
	uartPrintf(" TxDmaErrors = %u\n", dspTdmInfo.Port1TxDmaErrors);
	uartPrintf(" FrameSyncErrors = %u\n", dspTdmInfo.Port1FrameSyncErrors);
	uartPrintf(" RestartCount = %u\n", dspTdmInfo.Port1RestartCount);

	return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// getCpuUsage - Get CPU usage statistics from the DSP.
//
// FUNCTION
//  This function gets CPU usage statistics from the DSP.
//
// RETURNS
//  nothing
//
static void getCpuUsage( )
{
	GpakApiStatus_t gpakApiStatus;	// host API status
	ADT_UInt32 avgUsage[CORES_PER_DSP][MAX_NUM_GPAK_FRAMES];	// average usage
	ADT_UInt32 peakUsage[CORES_PER_DSP][MAX_NUM_GPAK_FRAMES];	// peak usage
	uint32_t i;					// loop index / counter
	ADT_UInt32 average;			// average value
	ADT_UInt32 peak;			// peak value
	ADT_UInt32 numCores, numFrames;
    uint32_t j;

    numCores  = CORES_PER_DSP; 
    numFrames = MAX_NUM_GPAK_FRAMES;
	// Get the DSP's CPU usage statistics.
	gpakApiStatus = gpakReadCoreUsage(0, &numCores, &numFrames, avgUsage, peakUsage);
	if (gpakApiStatus != GpakApiSuccess)
	{
		uartPrintf("* Failed to get CPU usage (API status = %d)\n",
				   gpakApiStatus);
		return;
	}

	// Display the CPU usage statistics.
    for (i=0; i<numCores; i++) 
    {
	    uartPrintf("Core %d CPU Usage statistics (Avg / Peak):\n",i);
	    for (j=0; j<MAX_NUM_GPAK_FRAMES; j++)
	    {
            if (j >= numFrames) break;
		    average = avgUsage[i][j];
		    peak = peakUsage[i][j];
		    uartPrintf(" [%u] = %3u.%1u / %3u.%1u\n",
				   j, average / 10, average % 10, peak / 10, peak % 10);
        }
	}

	return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// resetDspStats - Reset the DSP's Statistics.
//
// FUNCTION
//  This function resets the DSP's Statistics.
//
// RETURNS
//  nothing
//
static void resetDspStats(
	Bool resetCpuStats,			// flag: reset CPU statistics
	Bool resetFramingStats		// flag: reset Framing statistics
	)
{
	resetSsMask statsResetSelect;	// statistics reset select flags
	GpakApiStatus_t gpakApiStatus;	// host API status
	char *pText;					// pointer to text string

	// Reset the selected DSP's Statistics.
	memset(&statsResetSelect, 0, sizeof(statsResetSelect));
	if (resetCpuStats)
	{
		statsResetSelect.CpuUsageStats = 1;
	}
	if (resetFramingStats)
	{
		statsResetSelect.FramingStats = 1;
	}
	gpakApiStatus = gpakResetSystemStats(0, statsResetSelect);
	if (gpakApiStatus != GpakApiSuccess)
	{
		uartPrintf("* Failed to reset DSP statistics (API status = %d)\n",
				   gpakApiStatus);
	}
	else
	{
		if (resetCpuStats && resetFramingStats)
		{
			pText = "CPU and Framing";
		}
		else if (resetCpuStats)
		{
			pText = "CPU";
		}
		else if (resetFramingStats)
		{
			pText = "Framing";
		}
		else
		{
			pText = "?";
		}
		uartPrintf("Reset DSP %s statistics.\n", pText);
	}

    if (resetCpuStats)
        getCpuUsage( );
	return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// readAecParms - Read the DSP's AEC Parameters.
//
// FUNCTION
//  This function reads the DSP's AEC Parameters.
//
// RETURNS
//  True if successful or False if an error occurred.
//
static Bool readAecParms(void)
{
	GpakApiStatus_t gpakApiStatus;	// host API status

	// Read the DSP's AEC Parameters.
	gpakApiStatus = gpakReadAECParms(0, &hostAecParms);
	if (gpakApiStatus != GpakApiSuccess)
	{
		uartPrintf("* Failed to get AEC parms (API status = %d)\n",
				   gpakApiStatus);
		return FALSE;
	}

	return TRUE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// writeNONPersistentFeatureParms - Write the DSP's NON-Persistent Feature Parameters.
//
// FUNCTION
//  This function writes the DSP's NON-Persistent Feature Parameters.
//
// RETURNS
//  True if successful or False if an error occurred.
//
static Bool writeNONPersistentFeatureParms(ADT_UInt16 ChannelId, ADT_UInt32 Num_Features)
{
	GpakApiStatus_t gpakApiStatus;											// host API status
	GpakED137B_NONPersistentFeatureParms ED137B_NONPersistentFeatureParms[MAX_NONPF];
	GPAK_ED137B_NONPersistentFeatureStat_t NONPersistentFeatureParmsStatus;	// Persistent Feature parms write status
    ADT_UInt16 NPF_Count;

	for(NPF_Count = 0; NPF_Count < Num_Features; NPF_Count++)
	{ 
	    ED137B_NONPersistentFeatureParms[NPF_Count].NONPersistentFeature_Type = 1;																							
	    ED137B_NONPersistentFeatureParms[NPF_Count].NONPersistentFeature_Length = 16;		   												
	    ED137B_NONPersistentFeatureParms[NPF_Count].NONPersistentFeature_Value[0] = 0;	
	    ED137B_NONPersistentFeatureParms[NPF_Count].NONPersistentFeature_Value[1] = 1;
	    ED137B_NONPersistentFeatureParms[NPF_Count].NONPersistentFeature_Value[2] = 2;
	    ED137B_NONPersistentFeatureParms[NPF_Count].NONPersistentFeature_Value[3] = 3;	
	    ED137B_NONPersistentFeatureParms[NPF_Count].NONPersistentFeature_Value[4] = 4;
	    ED137B_NONPersistentFeatureParms[NPF_Count].NONPersistentFeature_Value[5] = 5;
	    ED137B_NONPersistentFeatureParms[NPF_Count].NONPersistentFeature_Value[6] = 6;	
	    ED137B_NONPersistentFeatureParms[NPF_Count].NONPersistentFeature_Value[7] = 7;
	    ED137B_NONPersistentFeatureParms[NPF_Count].NONPersistentFeature_Value[8] = 8;
	    ED137B_NONPersistentFeatureParms[NPF_Count].NONPersistentFeature_Value[9] = 9;	
	    ED137B_NONPersistentFeatureParms[NPF_Count].NONPersistentFeature_Value[10] = 10;
	    ED137B_NONPersistentFeatureParms[NPF_Count].NONPersistentFeature_Value[11] = 11;
	    ED137B_NONPersistentFeatureParms[NPF_Count].NONPersistentFeature_Value[12] = 12;
	    ED137B_NONPersistentFeatureParms[NPF_Count].NONPersistentFeature_Value[13] = 13;
	    ED137B_NONPersistentFeatureParms[NPF_Count].NONPersistentFeature_Value[14] = 14;
	    ED137B_NONPersistentFeatureParms[NPF_Count].NONPersistentFeature_Value[15] = 15;
	}
	// Write the DSP's Persistent Feature Parameters.
	gpakApiStatus = gpakNONPersistentFeature(0, ChannelId, Num_Features, &ED137B_NONPersistentFeatureParms, &NONPersistentFeatureParmsStatus);

	if (gpakApiStatus != GpakApiSuccess)
	{
		uartPrintf("* Failed to write Persistent Feature parms (API status = %d, DSP status = %d)\n",
				   gpakApiStatus, NONPersistentFeatureParmsStatus);
		return FALSE;
	}
	uartPrintf("* Write Persistent Feature parms Success (API status = %d, DSP status = %d)\n",
				   gpakApiStatus, NONPersistentFeatureParmsStatus);
	return TRUE;
}

static Bool writeGateAudioFeatureParms(ADT_UInt16 ChannelId, ADT_UInt32 Features)
{
	GpakApiStatus_t gpakApiStatus;											// host API status
	GpakED137B_GateAudioFeatureParms ED137B_GateAudioFeatureParms;
	GpakED137B_GateAudioFeatureStat_t ED137B_GateAudioFeatureParmsStatus;	    // Gate Audio Feature parms write status

	ED137B_GateAudioFeatureParms.Enable_Mask = Features;
	// Write the DSP's Persistent Feature Parameters.
	gpakApiStatus = gpakGateAudioFeature(0, ChannelId, ED137B_GateAudioFeatureParms.Enable_Mask, &ED137B_GateAudioFeatureParmsStatus);

	if (gpakApiStatus != GpakApiSuccess)
	{
		uartPrintf("* Failed to write Gate Audio Feature parms (API status = %d, DSP status = %d)\n",
				   gpakApiStatus, ED137B_GateAudioFeatureParmsStatus);
		return FALSE;
	}
	uartPrintf("* Write Gate Audio Feature parms Success (API status = %d, DSP status = %d)\n",
				   gpakApiStatus, ED137B_GateAudioFeatureParmsStatus);
	if(ED137B_GateAudioFeatureParms.Enable_Mask)
	{
		uartPrintf("* Gate Audio Feature: Enabled\n");
	}
	else
	{
		uartPrintf("* Gate Audio Feature: Disabled\n");
	}

	return TRUE;
}
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// PersistentFeatureUpdateParams - Update the DSP's Persistent Feature Parameters.
//
// FUNCTION
//  This function Updates the DSP's Persistent Feature Parameters.
//
// RETURNS
//  True if successful or False if an error occurred.
//
static Bool PersistentFeatureUpdateParams(ADT_UInt16 ChannelId, ADT_UInt16 PF_Type, ADT_UInt32 PF_Value)
{
	GpakApiStatus_t gpakApiStatus;													// host API status
	GpakED137B_PersistentFeatureUpdateParms ED137B_PersistentFeatureUpdateParms;
	GPAK_ED137B_PersistentFeatureUpdateStat_t PersistentFeatureUpdateParmsStatus;	// Persistent Feature parms update status

	ED137B_PersistentFeatureUpdateParms.PersistentFeature_Type = PF_Type;	/* Feature Type Definition														*/
																			/* 0x00-0xFF																	*/
	ED137B_PersistentFeatureUpdateParms.PersistentFeature_Value = PF_Value;	/* The initial value used for persistent additional feature						*/								
																			/* 0x00-0xFF																	*/

	// Update the DSP's Persistent Feature Parameters.
	gpakApiStatus = gpakPersistentFeatureUpdate(0, ChannelId, &ED137B_PersistentFeatureUpdateParms, &PersistentFeatureUpdateParmsStatus);

	if (gpakApiStatus != GpakApiSuccess)
	{
		uartPrintf("* Failed to update Persistent Feature parms (API status = %d, DSP status = %d)\n",
				   gpakApiStatus, PersistentFeatureUpdateParmsStatus);
		return FALSE;
	}
	uartPrintf("* Update Persistent Feature Success (API status = %d, DSP status = %d)\n",
				   gpakApiStatus, PersistentFeatureUpdateParmsStatus);
	return TRUE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// writePersistentFeatureParms - Write the DSP's Persistent Feature Parameters.
//
// FUNCTION
//  This function writes the DSP's Persistent Feature Parameters.
//
// RETURNS
//  True if successful or False if an error occurred.
//
static Bool writePersistentFeatureParms(ADT_UInt16 ChannelId, ADT_UInt16 PF_Type, ADT_UInt32 PF_Mask, ADT_UInt32 PF_Value)
{
	GpakApiStatus_t gpakApiStatus;			// host API status
	GpakED137B_PersistentFeatureParms ED137B_PersistentFeatureParms;
	GPAK_ED137B_PersistentFeatureStat_t PersistentFeatureParmsStatus;		// Persistent Feature parms write status

	ED137B_PersistentFeatureParms.PersistentFeature_Type = PF_Type;		/* Feature Type Definition														*/
																		/* 0x00-0xFF																	*/
	ED137B_PersistentFeatureParms.PersistentFeature_Mask = PF_Mask;		/* Used to signal which bit changes reflect event generation                    */
																		/* 0x00-0xFF																	*/
	ED137B_PersistentFeatureParms.PersistentFeature_Value = PF_Value;	/* The initial value used for persistent additional feature						*/								
																		/* 0x00-0xFF																	*/

	// Write the DSP's Persistent Feature Parameters.
	gpakApiStatus = gpakPersistentFeature(0, ChannelId, &ED137B_PersistentFeatureParms, &PersistentFeatureParmsStatus);

	if (gpakApiStatus != GpakApiSuccess)
	{
		uartPrintf("* Failed to write Persistent Feature parms (API status = %d, DSP status = %d)\n",
				   gpakApiStatus, PersistentFeatureParmsStatus);
		return FALSE;
	}
	uartPrintf("* Persistent Feature Success (API status = %d, DSP status = %d)\n",
				   gpakApiStatus, PersistentFeatureParmsStatus);

	return TRUE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// PTT_UpdateParms - Write the DSP's PTT Parameters.
//
// FUNCTION
//  This function writes the PTT Update Parameters.
//
// RETURNS
//  True if successful or False if an error occurred.
//
static Bool PTT_UpdateParms(ADT_UInt16 ChannelId, ADT_UInt16 PTT_value, ADT_UInt16 SQU, ADT_UInt16 PTT_ID_value, ADT_UInt16 PM_value, ADT_UInt16 PTTS_value)
{
	GpakApiStatus_t gpakApiStatus;			// host API status
	GpakED137B_PTTupdateParms ED137UpdatePTTParms;			
	GPAK_ED137B_PTTupdateStat_t UpdatePTTParmsStatus;		// PTT parms write status

	ED137UpdatePTTParms.PTT_Type = PTT_value;/* 0x00 PTT OFF										    				*/
										/* 0x01 Normal PTT ON															*/
										/* 0x02 Coupling PTT ON															*/
										/* 0x03 Priority PTT ON															*/
										/* 0x04 Emergency PTT ON														*/
										/* 0x05 Reserved																*/
										/* 0x06 Reserved																*/
										/* 0x07 Reserved																*/
	ED137UpdatePTTParms.SQU = SQU;      /* Squelch                                                                      */
	ED137UpdatePTTParms.PTT_ID	= PTT_ID_value;/* Set by the GRS Receiver in SDP transaction.  Ranges from 1 to the maximum */
										/* number of allowed SIP sessions. CTI will extract the SDP value and set this  */
										/* field.																		*/
	ED137UpdatePTTParms.PM		= PM_value;  /* Used for signaling PTT_ON to non-transmitting (not selected) transmitters in*/
										/* a coverage group.															*/
										/* 0-1																			*/
	ED137UpdatePTTParms.PTTS	= PTTS_value;/* PTT Summation																*/
										/* 0-1																			*/
		

	// Update the DSP's PTT Parameters.
	gpakApiStatus = gpakPTT_Update(0, ChannelId, &ED137UpdatePTTParms, &UpdatePTTParmsStatus);

	if (gpakApiStatus != GpakApiSuccess)
	{
		uartPrintf("* Failed to Update PTT parms (API status = %d, DSP status = %d)\n",
				   gpakApiStatus, UpdatePTTParmsStatus);
		return FALSE;
	}

	uartPrintf("* Update PTT parms Success(API status = %d, DSP status = %d)\n",
				   gpakApiStatus, UpdatePTTParmsStatus);

	return TRUE;
}
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// writePTTParms - Write the DSP's PTT Parameters.
//
// FUNCTION
//  This function writes the DSP's PTT Parameters.
//
// RETURNS
//  True if successful or False if an error occurred.
//
static Bool writePTTParms(ADT_UInt16 ChannelId)
{
	GpakApiStatus_t gpakApiStatus;			// host API status
	GpakED137BcfgParms ED137BcfgParms;
	GPAK_ED137BcfgStat_t PTTParmsStatus;		// PTT parms write status


	ED137BcfgParms.rtp_ext_en				= 0x01;			/* header extension enabled			*/											
    ED137BcfgParms.device					= 0x01;			/* VCS								*/
	ED137BcfgParms.txrxmode					= 0x02;			/* Connection Type		TxRx		*/
	ED137BcfgParms.follow_squ_with_vad      = 0x00;         /* Follow squelch with VAD feature  */
	ED137BcfgParms.ptt_rep					= 0x04;			/* PTT Repetition					*/
	ED137BcfgParms.R2SKeepAlive				= 200;			/* R2S Period						*/
	ED137BcfgParms.R2SKeepAliveMultiplier   = 0x00; 		/* R2S Multiplier					*/
	ED137BcfgParms.Ptt_val					= 0x2041;		/* PTT Value						*/									
	ED137BcfgParms.mask						= 0xFFFF;		/* PTT Mask					        */		

	// Write the DSP's PTT Parameters.
	gpakApiStatus = gpakConfigureED137B(0, ChannelId, &ED137BcfgParms, &PTTParmsStatus);

	if (gpakApiStatus != GpakApiSuccess)
	{
		uartPrintf("* Failed to write PTT parms (API status = %d, DSP status = %d)\n",
				   gpakApiStatus, PTTParmsStatus);
		return FALSE;
	}
    uartPrintf("* Config PTT Status (ChannelId = %d, API status = %d, DSP status = %d)\r\n",
				   ChannelId, gpakApiStatus, PTTParmsStatus);
	uartPrintf("* Config PTT parms (\r\n rtp_ext_en = %d\r\n device = %d\r\n txrxmode = %d\r\n Follow with SQ = %d\r\n ptt_rep = %d\r\n R2SKeepAlive = %d\r\n R2SKeepAliveMultiplier = %d\r\n Ptt_val = 0x%X\r\n mask = 0x%X\r\n)\r\n",
				   ED137BcfgParms.rtp_ext_en, 
				   ED137BcfgParms.device, 
				   ED137BcfgParms.txrxmode,
				   ED137BcfgParms.follow_squ_with_vad,
				   ED137BcfgParms.ptt_rep,
				   ED137BcfgParms.R2SKeepAlive,
				   ED137BcfgParms.R2SKeepAliveMultiplier,
				   ED137BcfgParms.Ptt_val,
				   ED137BcfgParms.mask);
	return TRUE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// writeAecParms - Write the DSP's AEC Parameters.
//
// FUNCTION
//  This function writes the DSP's AEC Parameters.
//
// RETURNS
//  True if successful or False if an error occurred.
//
static Bool writeAecParms(void)
{
	GpakApiStatus_t gpakApiStatus;			// host API status
	GPAK_AECParmsStat_t aecParmsStatus;		// AEC parms write status

	// Write the DSP's AEC Parameters.
	gpakApiStatus = gpakWriteAECParms(0, &hostAecParms, &aecParmsStatus);
	if (gpakApiStatus != GpakApiSuccess)
	{
		uartPrintf("* Failed to write AEC parms (API status = %d, DSP status = %d)\n",
				   gpakApiStatus, aecParmsStatus);
		return FALSE;
	}

	return TRUE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// procAecParm - Process an AEC Parameter command.
//
// FUNCTION
//  This function processes an AEC Parameter command.
//
// RETURNS
//  nothing
//
static void procAecParm(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	)
{
	int parmValue;				// parameter value

	// Validate the AEC parameter selection.
	if ((cmdSuffix1 != cmdSuffix2) || (cmdSuffix1 > MAX_AEC_PARM_ID))
	{
		uartPrintf("Invalid AEC parameter selection!\n");
		return;
	}

	// If the argument is null, display the current values.
	if (pCmdArg == NULL)
	{
		if (readAecParms())
		{
			uartPrintf("AEC Parameters:\n");
#if (AEC_LIB_VERSION >= 0x0430)
			uartPrintf(" 0 %s = %d\n", aecParmName(0),
					   hostAecParms.antiHowlEnable);
#endif
			uartPrintf(" 1 %s = %d\n", aecParmName(1),
					   hostAecParms.activeTailLengthMSec);
			uartPrintf(" 2 %s = %d\n", aecParmName(2),
					   hostAecParms.totalTailLengthMSec);
			uartPrintf(" 3 %s = %d\n", aecParmName(3),
					   hostAecParms.txNLPAggressiveness);
			uartPrintf(" 4 %s = %d\n", aecParmName(4),
					   hostAecParms.maxTxLossSTdB);
			uartPrintf(" 5 %s = %d\n", aecParmName(5),
					   hostAecParms.maxTxLossDTdB);
			uartPrintf(" 6 %s = %d\n", aecParmName(6),
					   hostAecParms.maxRxLossdB);
			uartPrintf(" 7 %s = %d\n", aecParmName(7),
					   hostAecParms.initialRxOutAttendB);
			uartPrintf(" 8 %s = %d\n", aecParmName(8),
					   hostAecParms.targetResidualLeveldBm);
			uartPrintf(" 9 %s = %d\n", aecParmName(9),
					   hostAecParms.maxRxNoiseLeveldBm);
			uartPrintf("10 %s = %d\n", aecParmName(10),
					   hostAecParms.worstExpectedERLdB);
			uartPrintf("11 %s = %d\n", aecParmName(11),
					   hostAecParms.rxSaturateLeveldBm);
			uartPrintf("12 %s = %d\n", aecParmName(12),
					   hostAecParms.noiseReduction1Setting);
			uartPrintf("13 %s = %d\n", aecParmName(13),
					   hostAecParms.noiseReduction2Setting);
			uartPrintf("14 %s = %d\n", aecParmName(14),
					   hostAecParms.cngEnable);
			uartPrintf("15 %s = %d\n", aecParmName(15),
					   hostAecParms.fixedGaindB10);
			uartPrintf("16 %s = %d\n", aecParmName(16),
					   hostAecParms.txAGCEnable);
			uartPrintf("17 %s = %d\n", aecParmName(17),
					   hostAecParms.txAGCMaxGaindB);
			uartPrintf("18 %s = %d\n", aecParmName(18),
					   hostAecParms.txAGCMaxLossdB);
			uartPrintf("19 %s = %d\n", aecParmName(19),
					   hostAecParms.txAGCTargetLeveldBm);
			uartPrintf("20 %s = %d\n", aecParmName(20),
					   hostAecParms.txAGCLowSigThreshdBm);
			uartPrintf("21 %s = %d\n", aecParmName(21),
					   hostAecParms.rxAGCEnable);
			uartPrintf("22 %s = %d\n", aecParmName(22),
					   hostAecParms.rxAGCMaxGaindB);
			uartPrintf("23 %s = %d\n", aecParmName(23),
					   hostAecParms.rxAGCMaxLossdB);
			uartPrintf("24 %s = %d\n", aecParmName(24),
					   hostAecParms.rxAGCTargetLeveldBm);
			uartPrintf("25 %s = %d\n", aecParmName(25),
					   hostAecParms.rxAGCLowSigThreshdBm);
			uartPrintf("26 %s = %d\n", aecParmName(26),
					   hostAecParms.rxBypassEnable);
			uartPrintf("27 %s = %d\n", aecParmName(27),
					   hostAecParms.maxTrainingTimeMSec);
			uartPrintf("28 %s = %d\n", aecParmName(28),
					   hostAecParms.trainingRxNoiseLeveldBm);
		}
		return;
	}

	// Validate the AEC parameter value.
	if ((sscanf(pCmdArg, "%d", &parmValue) != 1) ||
		(parmValue < -32768) || (parmValue > 32767))
	{
		uartPrintf("Invalid AEC parameter value: %s\n", pCmdArg);
		return;
	}

	// Set the value of the selected AEC parameter.
	if (readAecParms())
	{
		switch (cmdSuffix1)
		{
		case 0:
#if (AEC_LIB_VERSION >= 0x0430)
			if (parmValue == 0)
			{
				hostAecParms.antiHowlEnable = Disabled;
			}
			else
			{
				hostAecParms.antiHowlEnable = Enabled;
			}
			parmValue = hostAecParms.antiHowlEnable;
#else
			return;
#endif
			break;
		case 1:
			hostAecParms.activeTailLengthMSec = (ADT_Int16) parmValue;
			break;
		case 2:
			hostAecParms.totalTailLengthMSec = (ADT_Int16) parmValue;
			break;
		case 3:
			hostAecParms.txNLPAggressiveness = (ADT_Int16) parmValue;
			break;
		case 4:
			hostAecParms.maxTxLossSTdB = (ADT_Int16) parmValue;
			break;
		case 5:
			hostAecParms.maxTxLossDTdB = (ADT_Int16) parmValue;
			break;
		case 6:
			hostAecParms.maxRxLossdB = (ADT_Int16) parmValue;
			break;
		case 7:
			hostAecParms.initialRxOutAttendB = (ADT_Int16) parmValue;
			break;
		case 8:
			hostAecParms.targetResidualLeveldBm = (ADT_Int16) parmValue;
			break;
		case 9:
			hostAecParms.maxRxNoiseLeveldBm = (ADT_Int16) parmValue;
			break;
		case 10:
			hostAecParms.worstExpectedERLdB = (ADT_Int16) parmValue;
			break;
		case 11:
			hostAecParms.rxSaturateLeveldBm = (ADT_Int16) parmValue;
			break;
		case 12:
			hostAecParms.noiseReduction1Setting = (ADT_Int16) parmValue;
			break;
		case 13:
			hostAecParms.noiseReduction2Setting = (ADT_Int16) parmValue;
			break;
		case 14:
			if (parmValue == 0)
			{
				hostAecParms.cngEnable = Disabled;
			}
			else
			{
				hostAecParms.cngEnable = Enabled;
			}
			parmValue = hostAecParms.cngEnable;
			break;
		case 15:
			hostAecParms.fixedGaindB10 = (ADT_Int16) parmValue;
			break;
		case 16:
			if (parmValue == 0)
			{
				hostAecParms.txAGCEnable = Disabled;
			}
			else
			{
				hostAecParms.txAGCEnable = Enabled;
			}
			parmValue = hostAecParms.txAGCEnable;
			break;
		case 17:
			hostAecParms.txAGCMaxGaindB = (ADT_Int16) parmValue;
			break;
		case 18:
			hostAecParms.txAGCMaxLossdB = (ADT_Int16) parmValue;
			break;
		case 19:
			hostAecParms.txAGCTargetLeveldBm = (ADT_Int16) parmValue;
			break;
		case 20:
			hostAecParms.txAGCLowSigThreshdBm = (ADT_Int16) parmValue;
			break;
		case 21:
			if (parmValue == 0)
			{
				hostAecParms.rxAGCEnable = Disabled;
			}
			else
			{
				hostAecParms.rxAGCEnable = Enabled;
			}
			parmValue = hostAecParms.rxAGCEnable;
			break;
		case 22:
			hostAecParms.rxAGCMaxGaindB = (ADT_Int16) parmValue;
			break;
		case 23:
			hostAecParms.rxAGCMaxLossdB = (ADT_Int16) parmValue;
			break;
		case 24:
			hostAecParms.rxAGCTargetLeveldBm = (ADT_Int16) parmValue;
			break;
		case 25:
			hostAecParms.rxAGCLowSigThreshdBm = (ADT_Int16) parmValue;
			break;
		case 26:
			if (parmValue == 0)
			{
				hostAecParms.rxBypassEnable = Disabled;
			}
			else
			{
				hostAecParms.rxBypassEnable = Enabled;
			}
			parmValue = hostAecParms.rxBypassEnable;
			break;
		case 27:
			hostAecParms.maxTrainingTimeMSec = (ADT_Int16) parmValue;
			break;
		case 28:
			hostAecParms.trainingRxNoiseLeveldBm = (ADT_Int16) parmValue;
			break;
		}
		uartPrintf("%s set to %d\n", aecParmName(cmdSuffix1), parmValue);
		writeAecParms();
	}

	return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// aecParmName - Determine the name of an AEC Parameter.
//
// FUNCTION
//  This function determines the name of an AEC Parameter.
//
// RETURNS
//  Pointer to text string for specified AEC Parameter Id.
//
static char *aecParmName(
	int32_t aecParmId			// AEC parameter Id
	)
{

	switch (aecParmId)
	{
	case 0:
		return "antiHowlEnable";
	case 1:
		return "activeTailLengthMSec";
	case 2:
		return "totalTailLengthMSec";
	case 3:
		return "txNLPAggressiveness";
	case 4:
		return "maxTxLossSTdB";
	case 5:
		return "maxTxLossDTdB";
	case 6:
		return "maxRxLossdB";
	case 7:
		return "initialRxOutAttendB";
	case 8:
		return "targetResidualLeveldBm";
	case 9:
		return "maxRxNoiseLeveldBm";
	case 10:
		return "worstExpectedERLdB";
	case 11:
		return "rxSaturateLeveldBm";
	case 12:
		return "noiseReduction1Setting";
	case 13:
		return "noiseReduction2Setting";
	case 14:
		return "cngEnable";
	case 15:
		return "fixedGaindB10";
	case 16:
		return "txAGCEnable";
	case 17:
		return "txAGCMaxGaindB";
	case 18:
		return "txAGCMaxLossdB";
	case 19:
		return "txAGCTargetLeveldBm";
	case 20:
		return "txAGCLowSigThreshdBm";
	case 21:
		return "rxAGCEnable";
	case 22:
		return "rxAGCMaxGaindB";
	case 23:
		return "rxAGCMaxLossdB";
	case 24:
		return "rxAGCTargetLeveldBm";
	case 25:
		return "rxAGCLowSigThreshdBm";
	case 26:
		return "rxBypassEnable";
	case 27:
		return "maxTrainingTimeMSec";
	case 28:
		return "trainingRxNoiseLeveldBm";
	}

	return "?";
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// displayAecStatus - Display AEC Status for a channel on the DSP.
//
// FUNCTION
//  This function displays AEC Status for a channel on the DSP.
//
// RETURNS
//  nothing
//
static void displayAecStatus(
	ADT_UInt16 channelId		// channel Id
	)
{
	GpakApiStatus_t gpakApiStatus;		// host API status
	gpakAecStatus_t aecStatus;			// DSP's AEC status info
	GPAK_AecChanReadStat_t dspStatus;	// DSP's status

	// Get the channel's AEC status info.
	gpakApiStatus = gpakReadChanAecStatus(0, channelId, &aecStatus, &dspStatus);
	if (gpakApiStatus != GpakApiSuccess)
	{
		uartPrintf("* Failed to get channel %u AEC status (API status = %d, DSP status = %d)\n",
				   channelId, gpakApiStatus, dspStatus);
		return;
	}

	// Display the channel's AEC status info.
	uartPrintf("Channel %u AEC status:\n", channelId);
	uartPrintf(" txInPowerdBm10 = %d\n", aecStatus.txInPowerdBm10);
	uartPrintf(" txOutPowerdBm10 = %d\n", aecStatus.txOutPowerdBm10);
	uartPrintf(" rxInPowerdBm10 = %d\n", aecStatus.rxInPowerdBm10);
#if 0	// unused, not set by AEC
	uartPrintf(" rxOutPowerdBm10 = %d\n", aecStatus.rxOutPowerdBm10);
#endif
	uartPrintf(" residualPowerdBm10 = %d\n", aecStatus.residualPowerdBm10);
	uartPrintf(" erlDirectdB10 = %d\n", aecStatus.erlDirectdB10);
	uartPrintf(" erlIndirectdB10 = %d\n", aecStatus.erlIndirectdB10);
	uartPrintf(" erldB10BestEstimate = %d\n", aecStatus.erldB10BestEstimate);
	uartPrintf(" worstPerBinERLdB10BestEstimate = %d\n",
			   aecStatus.worstPerBinERLdB10BestEstimate);
	uartPrintf(" erledB10 = %d\n", aecStatus.erledB10);
	uartPrintf(" shortTermERLEdB10 = %d\n", aecStatus.shortTermERLEdB10);
#if (AEC_LIB_VERSION >= 0x0430)
	uartPrintf(" instantaneousERLEdB100 = %d\n",
			   aecStatus.instantaneousERLEdB100);
	uartPrintf(" dynamicNLPAggressivenessAdjustdB10 = %d\n",
			   aecStatus.dynamicNLPAggressivenessAdjustdB10);
#endif
#if 0	// unused, not set by AEC
	uartPrintf(" shadowERLEdB10 = %d\n", aecStatus.shadowERLEdB10);
#endif
	uartPrintf(" rxVADState = %d\n", aecStatus.rxVADState);
	uartPrintf(" txVADState = %d\n", aecStatus.txVADState);
	uartPrintf(" rxVADStateLatched = %d\n", aecStatus.rxVADStateLatched);
	uartPrintf(" currentBulkDelaySamples = %d\n",
			   aecStatus.currentBulkDelaySamples);
	uartPrintf(" txAttenuationdB10 = %d\n", aecStatus.txAttenuationdB10);
	uartPrintf(" rxAttenuationdB10 = %d\n", aecStatus.rxAttenuationdB10);
	uartPrintf(" rxOutAttenuationdB10 = %d\n", aecStatus.rxOutAttenuationdB10);
	uartPrintf(" nlpThresholddB10 = %d\n", aecStatus.nlpThresholddB10);
	uartPrintf(" nlpSaturateFlag = %d\n", aecStatus.nlpSaturateFlag);
	uartPrintf(" aecState = %d\n", aecStatus.aecState);
	uartPrintf(" sbcngResidualPowerdBm10 = %d\n",
			   aecStatus.sbcngResidualPowerdBm10);
	uartPrintf(" sbcngCNGPowerdBm10 = %d\n", aecStatus.sbcngCNGPowerdBm10);
	uartPrintf(" rxOutAttendB10 = %d\n", aecStatus.rxOutAttendB10);
	uartPrintf(" sbMaxAttendB10 = %d\n", aecStatus.sbMaxAttendB10);
	uartPrintf(" sbMaxClipLeveldBm10 = %d\n", aecStatus.sbMaxClipLeveldBm10);

	return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// readLecParms - Read the DSP's LEC Parameters.
//
// FUNCTION
//  This function reads the DSP's LEC Parameters.
//
// RETURNS
//  True if successful or False if an error occurred.
//
static Bool readLecParms(void)
{
	GpakApiStatus_t gpakApiStatus;	// host API status

	// Read the DSP's System Parameters.
	gpakApiStatus = gpakReadSystemParms(0, &tempSysParms);
	if (gpakApiStatus != GpakApiSuccess)
	{
		uartPrintf("* Failed to get system parms (API status = %d)\n",
				   gpakApiStatus);
		return FALSE;
	}
	hostLecParms = tempSysParms.PcmEc;

	return TRUE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// writeLecParms - Write the DSP's LEC Parameters.
//
// FUNCTION
//  This function writes the DSP's LEC Parameters.
//
// RETURNS
//  True if successful or False if an error occurred.
//
static Bool writeLecParms(void)
{
	GpakApiStatus_t gpakApiStatus;			// host API status
	GPAK_SysParmsStat_t sysParmsStatus;		// System parms write status

	// Write the DSP's LEC Parameters.
	tempSysParms.PcmEc = hostLecParms;
	gpakApiStatus = gpakWriteSystemParms(0, &tempSysParms, 0, 0, 1, 0, 0, 0,
										 &sysParmsStatus);
	if (gpakApiStatus != GpakApiSuccess)
	{
		uartPrintf("* Failed to write LEC parms (API status = %d, DSP status = %d)\n",
				   gpakApiStatus, sysParmsStatus);
		return FALSE;
	}

	return TRUE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// procSideToneParms - Process MEMT Parameter command.
//
// FUNCTION
//  This function processes an MEMT Parameter command.
//
// RETURNS
//  nothing
//

static void procMemoryTest( )
{
memoryTestResults        results;
GpakApiStatus_t          apiStatus;
GPAK_MemoryTestResults_t dspStatus;
	
    apiStatus = gpakReadMemoryTestStatus (0, &results, &dspStatus);  
    if ((apiStatus != GpakApiSuccess) || (dspStatus != MT_Success)) 
	{
		uartPrintf("Memory Test Failed: %d, %d\n", apiStatus, dspStatus);
		uartPrintf("Pattern %x, Address %x\n", results.TestPattern, results.TestAddress);
	}
	else
	{
		uartPrintf("Memory Test Passed\n");
	}
}

static void procVLANSetParms(
	char *pCmdArg				// pointer to command argument string
	)
{
	ADT_UInt32 idx;				// argument 1
	ADT_UInt32 id;				// argument 2
	ADT_UInt32 pri;				// argument 3
    GpakApiStatus_t status;
    GPAK_VlanStat_t dspStat;

	if (pCmdArg == NULL) {
		uartPrintf("Invalid num VLAN params: IDX, ID, PRI\n");
        return;
    }
	if (sscanf(pCmdArg, "%u %u %u", &idx, &id, &pri) != 3) 
	{
		uartPrintf("Invalid num VLAN params (IDX, ID, PRI = %s\n", pCmdArg);
		return;
	}
    status = gpakSetVlanTag (0, (ADT_UInt16)idx, (ADT_UInt16)id, (ADT_UInt8)pri, &dspStat);
	if (status != GpakApiSuccess) {
		uartPrintf("SetVlanTag API error (API status = %d, DSP status = %d)\n",
				   status, dspStat);
		return;
    } else {
       uartPrintf("Vlan set tag success for IDX %u: ID=%u, PRI=%u\n", idx, id, pri);
    }
}

static void procVLANGetParms(
	char *pCmdArg				// pointer to command argument string
	)
{
	ADT_UInt32 idx;				// argument 1
	ADT_UInt16 id;				// argument 2
	ADT_UInt8 pri;				// argument 3
    GpakApiStatus_t status;
    GPAK_VlanStat_t dspStat;

	if (pCmdArg == NULL) {
		uartPrintf("Invalid num VLAN params: IDX\n");
        return;
    }
	if (sscanf(pCmdArg, "%u", &idx) != 1) 
	{
		uartPrintf("Invalid num VLAN params (IDX = %s\n", pCmdArg);
		return;
	}

    status = gpakGetVlanTag (0, (ADT_UInt16)idx, &id, &pri, &dspStat);
	if (status != GpakApiSuccess) {
		uartPrintf("GetVlanTag API error (API status = %d, DSP status = %d)\n",
				   status, dspStat);
		return;
    }
    uartPrintf("GetVlanTag Success: IDX %d, ID %d, PRI %d\n",idx, id, pri);
}

static void procVLANChanEnabParms(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	)
{
	ADT_UInt32 idx, i;				// argument 1

	// Validate the channel Id.
	if (cmdSuffix1 < 0)
	{
		cmdSuffix1 = 0;
		cmdSuffix2 = NUM_TEST_CHANS - 1;
	}
	else if ((cmdSuffix1 >= NUM_TEST_CHANS) || (cmdSuffix2 >= NUM_TEST_CHANS))
	{
		uartPrintf("Channel exceeds max of %u!\n", NUM_TEST_CHANS - 1);
		return;
	}

	// If the argument is null, display the current value(s).
	if (pCmdArg == NULL)
	{
		if (cmdSuffix1 < cmdSuffix2)
		{
			uartPrintf("VlanIdx:\n");
			for (i = cmdSuffix1; i <= cmdSuffix2; i++)
			{
				uartPrintf(" [%u] = %d\n",
						   i, VlanIdx[i]);
			}
		}
		else
		{
			uartPrintf("Chan %d VlanIdx = %d\n",
					   cmdSuffix1,
					   VlanIdx[cmdSuffix1]);
		}
		return;
	}

	if (sscanf(pCmdArg, "%u", &idx) != 1) 
	{
		uartPrintf("Invalid num VLAN params (IDX = %s\n", pCmdArg);
		return;
	}
    if (idx != 255) {
        if (idx > 9) {
		    uartPrintf("Invalid Vlan Index (IDX = %s\n", pCmdArg);
		    return;
        }
    }

	if (cmdSuffix1 < cmdSuffix2)
	{
		for (i = cmdSuffix1; i <= cmdSuffix2; i++)
		{
            VlanIdx[i] = idx;
		}
	}
	else
	{
        VlanIdx[cmdSuffix1] = idx;
	}
}

void getver() {
gpakTestStatus_t testStat;
gpakVersionString_t verString;
 
    testStat = gpakReadVersionString (0, &verString);
    if (testStat ==  GpakApiSuccess) { 
         uartPrintf (" %s\n", verString.buf);
    } else {
        uartPrintf("gpakReadVersionString error\n");
    }

}

int procRtpRxTimeoutParams(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string);
	)
{
	uint32_t arg1;				// argument 1
	uint32_t arg2;				// argument 2
    int rslt;
	GpakApiStatus_t gpakApiStatus;		// host API status
	GPAK_RTP_Stat_Rply_t dspStatus;
	if ((cmdSuffix1 != cmdSuffix2) || (cmdSuffix1 > MAX_RTP_TX_TO_PARM_ID))
	{
		uartPrintf("Invalid RTP TX Timeout parameter!\n");
		return -1;
	}
	if (cmdSuffix1 == -1) 
	{
		uartPrintf("Help Syntax RTPTO:\n");
		uartPrintf("RTPTO Channel Number(valid Channel Number)\n");
		uartPrintf("RTPTO Timeout Value(0-65535)\n");
		uartPrintf("example: RTPTO 0 5000\n");
		return -1;
	}
	if (pCmdArg == NULL)
	{
		uartPrintf("Invalid RTP TX Timeout parameter value\n");
		return -1;
	}
	rslt = sscanf(pCmdArg, "%u", &arg1);
	if( rslt == 1 )
	{
	    gpakApiStatus = gpakWriteRtpRxTimeoutMsg(0, cmdSuffix1, arg1,  &dspStatus);
		if(gpakApiStatus == RtpSuccess)
		{
            uartPrintf("RTP TX Timeout set to %d\n", arg1);
		}
	}
	else
	{
		uartPrintf("Invalid number of RTP TX Timeout parameters\n");
	}
	return 0;
}
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// procPTTParm - Process an ED137B Push to Talk Parameter command.
//
// FUNCTION
//  This function processes an PTT Parameter command.
//
// RETURNS
//  nothing
//
int procPTTConfigParms(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	)
{
	int rslt;
	if ((cmdSuffix1 != cmdSuffix2) || (cmdSuffix1 > MAX_PTT_PARM_ID))
	{
		uartPrintf("Invalid PTT parameter selection!\n");
		return -1;
	}
	if (cmdSuffix1 == -1) 
	{
		uartPrintf("Help Syntax PTTCFG:\n");
		uartPrintf("PTTCFG Channel Number(valid Channel Number)\n");
		uartPrintf("example: PTTCFG 0\n");
		return -1;
	}
	writePTTParms(cmdSuffix1);
	return 0;
}
int procPTTUpdateParms(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	)
{
	GpakED137B_PTTupdateParms ED137UpdatePTTParms;	
	uint32_t arg1;				// argument 1
	uint32_t arg2;				// argument 2
	uint32_t arg3;				// argument 3
	uint32_t arg4;				// argument 4
	uint32_t arg5;				// argument 5
	int rslt;
	if ((cmdSuffix1 != cmdSuffix2) || (cmdSuffix1 > MAX_PTT_PARM_ID))
	{
		uartPrintf("Invalid PTT parameter selection!\n");
		return -1;
	}
    if (cmdSuffix1 == -1) 
	{
		uartPrintf("Help Syntax PTTUPT:\n");
		uartPrintf("PTTUPT Channel Number(valid Channel Number) = PTT_VALUE PTT_ID PM PTTS\n");
		uartPrintf("example: PTTUPT 0 = 12 1 4 0\n");
		return -1;
	}
	if (pCmdArg == NULL)
	{
		uartPrintf("Invalid  ED137B Push to Talk parameter value");
		return -1;
	}
	ED137UpdatePTTParms.SQU 	= 0x00; /* Squelch                                                                      */
	ED137UpdatePTTParms.PTT_ID	= 0x1A; /* Set by the GRS Receiver in SDP transaction.  Ranges from 1 to the maximum    */
	ED137UpdatePTTParms.PM		= 0x00;	/* Used for signaling PTT_ON to non-transmitting (not selected) transmitters in */
	ED137UpdatePTTParms.PTTS	= 0x00;	/* PTT Summation						   	     							    */
	rslt = sscanf(pCmdArg, "%u %u %u %u", &arg1, &arg2, &arg3, &arg4, &arg5);
	switch( rslt )
	{
		case 1:
		{
			PTT_UpdateParms(cmdSuffix1, arg1, ED137UpdatePTTParms.SQU, ED137UpdatePTTParms.PTT_ID, ED137UpdatePTTParms.PM, ED137UpdatePTTParms.PTTS);
			break;
		}
		case 2:
		{
			PTT_UpdateParms(cmdSuffix1, arg1, arg2, ED137UpdatePTTParms.PTT_ID, ED137UpdatePTTParms.PM, ED137UpdatePTTParms.PTTS);
			break;
		}
		case 3:
		{
			PTT_UpdateParms(cmdSuffix1, arg1, arg2, arg3, ED137UpdatePTTParms.PM, ED137UpdatePTTParms.PTTS);
			break;
		}
		case 4:
		{
			PTT_UpdateParms(cmdSuffix1, arg1, arg2, arg3, arg4, ED137UpdatePTTParms.PTTS);
			break;
		}
		case 5:
		{
			PTT_UpdateParms(cmdSuffix1, arg1, arg2, arg3, arg4, arg5);
			break;
		}
		default:
		{
			uartPrintf("Invalid  ED137B Push to Talk Update parameter value");
			break;
		}
	}
	return 0;
}
int procPFParms(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	)
{	
	uint32_t arg1;				// argument 1
	uint32_t arg2;				// argument 2
	uint32_t arg3;				// argument 3
	int rslt;

	// Validate the ED137B Push to Talk parameter selection.
	if ((cmdSuffix1 != cmdSuffix2) || (cmdSuffix1 > MAX_PTT_PARM_ID))
	{
		uartPrintf("Invalid PTT parameter selection!\n");
		return -1;
	}

	// If the argument is null, display the current values.
	if (pCmdArg == NULL)
	{
		uartPrintf("Invalid  ED137B Push to Talk parameter value");
		return -1;
	}

    rslt = sscanf(pCmdArg, "%u %u %u %u", &arg1, &arg2, &arg3);
	switch(rslt)
	{
		case 3:
		{
			writePersistentFeatureParms(cmdSuffix1, arg1, arg2, arg3);
			break;
		}
		default:
		{
			uartPrintf("Invalid  ED137B Push to Talk PF parameter value");
			break;
		}
	}
																	
	return 0;
}

int procPFUpdateParms(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	)
		{
	uint32_t arg1;				// argument 1
	uint32_t arg2;				// argument 2
	int rslt;
	if ((cmdSuffix1 != cmdSuffix2) || (cmdSuffix1 > MAX_PTT_PARM_ID))
			{
		uartPrintf("Invalid PTT parameter selection!\n");
		return -1;
			}

	if (pCmdArg == NULL)
	{
		uartPrintf("Invalid  ED137B Push to Talk parameter value");
		return -1;
		}
    rslt = sscanf(pCmdArg, "%u %u %u %u", &arg1, &arg2);
	switch( rslt )
		{
		case 2:
			{
			PersistentFeatureUpdateParams(cmdSuffix1, arg1, arg2);
			break;
		}
		default:
		{
			uartPrintf("Invalid  ED137B Push to Talk PF parameter value");
			break;
		}
	}

	return 0;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// procNPFParms - Process an ED137B Push to Talk Non-Protected Feature Parameter command.
//
int procNPFParms(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	)
		{
	uint32_t arg1;				// argument 1
	int rslt;

	// Validate the ED137B Push to Talk parameter selection.
	if ((cmdSuffix1 != cmdSuffix2) || (cmdSuffix1 > MAX_PTT_PARM_ID))
		{
		uartPrintf("Invalid PTT parameter selection!\n");
		return -1;
		}

	if (pCmdArg == NULL)
		{
		uartPrintf("Invalid  ED137B Push to Talk parameter value");
		return -1;
		}
    rslt = sscanf(pCmdArg, "%u %u %u %u", &arg1);
	switch( rslt )
		{
		case 1:
		{
			writeNONPersistentFeatureParms(cmdSuffix1, arg1);
			break;
		}
		default:
		{
			uartPrintf("Invalid  ED137B Push to Talk NPF parameter value");
			break;
		}
	}


	return 0;
}

int procGAParms(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	)
		{
	uint32_t arg1;				// argument 1
	int rslt;

	// Validate the ED137B Push to Talk parameter selection.
	if ((cmdSuffix1 != cmdSuffix2) || (cmdSuffix1 > MAX_PTT_PARM_ID))
		{
		uartPrintf("Invalid PTT parameter selection!\n");
		return;
		}

	if (pCmdArg == NULL)
		{
		uartPrintf("Invalid  ED137B Push to Talk parameter value");
		return;
		}
    rslt = sscanf(pCmdArg, "%u %u %u %u", &arg1);
	switch( rslt )
		{
		case 1:
		{
			writeGateAudioFeatureParms(cmdSuffix1, arg1);
			break;
		}
		default:
		{
			uartPrintf("Invalid  ED137B Gate Audio parameter value");
			break;
		}
	}


	return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// procSideToneParms - Process an STN Parameter command.
//
// FUNCTION
//  This function processes an STD Parameter command.
//
// RETURNS
//  nothing
//

static void procSideToneParms(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	)
{
	setSideTone dspSideTone;
    GpakApiStatus_t gpakApiStatus;			// host API status
	GpakChannelStatus_t dspChanInfo;		// DSP's channel status info
	GPAK_ChannelStatusStat_t dspChanStatus;	// DSP's channel status status
	int parmValue;				// parameter value

	ADT_UInt32 arg1;				// argument 1
	ADT_UInt32 arg2;				// argument 2
	ADT_Int32  arg3;				// argument 3

	// Validate the AEC parameter selection.
	if ((cmdSuffix1 != cmdSuffix2) || (cmdSuffix1 > 10))
	{
		uartPrintf("Invalid STN parameter selection!\n");
		return;
	}

	// If the argument is null, display the current values.
	if (pCmdArg == NULL)
	{
		return;
	}

	// Validate the Side Tone parameter values.
	if (!((sscanf(pCmdArg, "%u %u %i", &arg1, &arg2, &arg3) == 3) &&
		(arg1 <= 4) && (arg2 <= 3) && (arg3 <= 100) && (arg3 >= -400)))
	{
		uartPrintf("Invalid STN parameter value Enable/Disable: %s\n", pCmdArg);
		return;
	}
	dspSideTone.ChannelId = arg1;
	dspSideTone.Enable = arg2;
	dspSideTone.Gain = arg3;
	dspSideTone.MsgLen = 6;
    if (dspSideTone.ChannelId >= NUM_TEST_CHANS) {
		uartPrintf("Invalid ChannelID %u\n", dspSideTone.ChannelId);
        return;
    }
        
	// Get the channel's status info.
	gpakApiStatus =
		gpakGetChannelStatus(0, PackCore(chanCore[dspSideTone.ChannelId],dspSideTone.ChannelId), &dspChanInfo, &dspChanStatus);

	if (gpakApiStatus != GpakApiSuccess)
	{
		uartPrintf("* Failed to get channel %u status (API status = %d, DSP status = %d)\n",
				   dspSideTone.ChannelId, gpakApiStatus, dspChanStatus);
		return;
	}

	// Display the channel's status info.
	if (dspChanInfo.ChannelType == inactive)
	{
		uartPrintf("Channel %u is inactive.\n", dspSideTone.ChannelId);
		return;
	}

	uartPrintf("Channel %u status:\n", dspChanInfo.ChannelId);
	uartPrintf(" Type = ");
	switch (dspChanInfo.ChannelType)
	{
	case pcmToPacket:
		uartPrintf("PCM to Pkt ");
		break;
	case pcmToPcm:
		uartPrintf("PCM to PCM ");
		break;
	case packetToPacket:
		uartPrintf("Pkt to Pkt ");
		break;
	case conferencePcm:
		uartPrintf("PCM to Conf ");
		break;	
	default:
		uartPrintf("%d", dspChanInfo.ChannelType);
		break;
	}
	if (gpakConfigureSideTone(0,dspSideTone) == GpakApiSuccess)
	{
		uartPrintf("Side Tone Configured Enabled = %d, Gain = %d\n",dspSideTone.Enable, dspSideTone.Gain);
	}
}
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// procLecParm - Process an LEC Parameter command.
//
// FUNCTION
//  This function processes an LEC Parameter command.
//
// RETURNS
//  nothing
//
static void procLecParm(
	int32_t cmdSuffix1,			// 1st command suffix value
	int32_t cmdSuffix2,			// 2nd command suffix value
	char *pCmdArg				// pointer to command argument string
	)
{
	int parmValue;				// parameter value

	// Validate the LEC parameter selection.
	if ((cmdSuffix1 != cmdSuffix2) || (cmdSuffix1 > MAX_LEC_PARM_ID))
	{
		uartPrintf("Invalid LEC parameter selection!\n");
		return;
	}

	// If the argument is null, display the current values.
	if (pCmdArg == NULL)
	{
		if (readLecParms())
		{
			uartPrintf("LEC Parameters:\n");
			uartPrintf(" 0 %s = %d\n", lecParmName(0), hostLecParms.TailLength);
			uartPrintf(" 1 %s = %d\n", lecParmName(1), hostLecParms.NlpType);
			uartPrintf(" 2 %s = %d\n", lecParmName(2),
					   hostLecParms.AdaptEnable);
			uartPrintf(" 3 %s = %d\n", lecParmName(3),
					   hostLecParms.G165DetEnable);
			uartPrintf(" 4 %s = %d\n", lecParmName(4),
					   hostLecParms.DblTalkThresh);
			uartPrintf(" 5 %s = %d\n", lecParmName(5),
					   hostLecParms.NlpThreshold);
			uartPrintf(" 6 %s = %d\n", lecParmName(6),
					   hostLecParms.NlpUpperLimitThreshConv);
			uartPrintf(" 7 %s = %d\n", lecParmName(7),
					   hostLecParms.NlpUpperLimitThreshPreconv);
			uartPrintf(" 8 %s = %d\n", lecParmName(8), hostLecParms.NlpMaxSupp);
			uartPrintf(" 9 %s = %d\n", lecParmName(9),
					   hostLecParms.CngThreshold);
			uartPrintf("10 %s = %d\n", lecParmName(10),
					   hostLecParms.AdaptLimit);
			uartPrintf("11 %s = %d\n", lecParmName(11),
					   hostLecParms.CrossCorrLimit);
			uartPrintf("12 %s = %d\n", lecParmName(12),
					   hostLecParms.NumFirSegments);
			uartPrintf("13 %s = %d\n", lecParmName(13),
					   hostLecParms.FirSegmentLen);
			uartPrintf("14 %s = %d\n", lecParmName(14),
					   hostLecParms.FirTapCheckPeriod);
			uartPrintf("15 %s = %d\n", lecParmName(15),
					   hostLecParms.MaxDoubleTalkThres);
			uartPrintf("16 %s = %d\n", lecParmName(16),
					   hostLecParms.MixedFourWireMode);
			uartPrintf("17 %s = %d\n", lecParmName(17),
					   hostLecParms.ReconvergenceCheckEnable);
		}
		return;
	}

	// Validate the LEC parameter value.
	if ((sscanf(pCmdArg, "%d", &parmValue) != 1) ||
		(parmValue < -32768) || (parmValue > 32767))
	{
		uartPrintf("Invalid LEC parameter value: %s\n", pCmdArg);
		return;
	}

	// Set the value of the selected LEC parameter.
	if (readLecParms())
	{
		switch (cmdSuffix1)
		{
		case 0:
			hostLecParms.TailLength = (ADT_Int16) parmValue;
			break;
		case 1:
			hostLecParms.NlpType = (ADT_Int16) parmValue;
			break;
		case 2:
			hostLecParms.AdaptEnable = (ADT_Int16) parmValue;
			break;
		case 3:
			hostLecParms.G165DetEnable = (ADT_Int16) parmValue;
			break;
		case 4:
			hostLecParms.DblTalkThresh = (ADT_Int16) parmValue;
			break;
		case 5:
			hostLecParms.NlpThreshold = (ADT_Int16) parmValue;
			break;
		case 6:
			hostLecParms.NlpUpperLimitThreshConv = (ADT_Int16) parmValue;
			break;
		case 7:
			hostLecParms.NlpUpperLimitThreshPreconv = (ADT_Int16) parmValue;
			break;
		case 8:
			hostLecParms.NlpMaxSupp = (ADT_Int16) parmValue;
			break;
		case 9:
			hostLecParms.CngThreshold = (ADT_Int16) parmValue;
			break;
		case 10:
			hostLecParms.AdaptLimit = (ADT_Int16) parmValue;
			break;
		case 11:
			hostLecParms.CrossCorrLimit = (ADT_Int16) parmValue;
			break;
		case 12:
			hostLecParms.NumFirSegments = (ADT_Int16) parmValue;
			break;
		case 13:
			hostLecParms.FirSegmentLen = (ADT_Int16) parmValue;
			break;
		case 14:
			hostLecParms.FirTapCheckPeriod = (ADT_Int16) parmValue;
			break;
		case 15:
			hostLecParms.MaxDoubleTalkThres = (ADT_Int16) parmValue;
			break;
		case 16:
			hostLecParms.MixedFourWireMode = (ADT_Int16) parmValue;
			break;
		case 17:
			hostLecParms.ReconvergenceCheckEnable = (ADT_Int16) parmValue;
			break;
		}
		uartPrintf("%s set to %d\n", lecParmName(cmdSuffix1), parmValue);
		writeLecParms();
	}

	return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// lecParmName - Determine the name of an LEC Parameter.
//
// FUNCTION
//  This function determines the name of an LEC Parameter.
//
// RETURNS
//  Pointer to text string for specified LEC Parameter Id.
//
static char *lecParmName(
	int32_t lecParmId			// LEC parameter Id
	)
{

	switch (lecParmId)
	{
	case 0:
		return "TailLength";
	case 1:
		return "NlpType";
	case 2:
		return "AdaptEnable";
	case 3:
		return "G165DetEnable";
	case 4:
		return "DblTalkThresh";
	case 5:
		return "NlpThreshold";
	case 6:
		return "NlpUpperLimitThreshConv";
	case 7:
		return "NlpUpperLimitThreshPreconv";
	case 8:
		return "NlpMaxSupp";
	case 9:
		return "CngThreshold";
	case 10:
		return "AdaptLimit";
	case 11:
		return "CrossCorrLimit";
	case 12:
		return "NumFirSegments";
	case 13:
		return "FirSegmentLen";
	case 14:
		return "FirTapCheckPeriod";
	case 15:
		return "MaxDoubleTalkThres";
	case 16:
		return "MixedFourWireMode";
	case 17:
		return "ReconvergenceCheckEnable";
	}

	return "?";
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// displayLecStatus - Display LEC Status for a channel on the DSP.
//
// FUNCTION
//  This function displays LEC Status for a channel on the DSP.
//
// RETURNS
//  nothing
//
static void displayLecStatus(
	ADT_UInt16 channelId,		// channel Id
	GpakDeviceSide_t chanSide	// side of channel
	)
{
	GpakApiStatus_t gpakApiStatus;		// host API status
	gpakLecStatus_t lecStatus;			// DSP's LEC status info
	GPAK_LecChanReadStat_t dspStatus;	// DSP's status

	// Get the channel's LEC status info.
	gpakApiStatus =
		gpakReadChanLecStatus(0, channelId, chanSide, &lecStatus, &dspStatus);
	if (gpakApiStatus != GpakApiSuccess)
	{
		uartPrintf("* Failed to get channel %u LEC status (API status = %d, DSP status = %d)\n",
				   channelId, gpakApiStatus, dspStatus);
		return;
	}

	// Display the channel's LEC status info.
	uartPrintf("Channel %u LEC status:\n", channelId);
	uartPrintf(" AdaptReport = %d\n", lecStatus.AdaptReport);
	uartPrintf(" CrossCorrReport = %d\n", lecStatus.CrossCorrReport);
	uartPrintf(" G165Status = 0x%04X\n", lecStatus.G165Status);
	uartPrintf(" ConvergenceStat = %d\n", lecStatus.ConvergenceStat);
	uartPrintf(" NLPFlag = %d\n", lecStatus.NLPFlag);
	uartPrintf(" DoubleTalkFlag = %d\n", lecStatus.DoubleTalkFlag);
	uartPrintf(" SmartPacketMode = %d\n", lecStatus.SmartPacketMode);
	uartPrintf(" ERL1 = %d\n", lecStatus.ERL1);
	uartPrintf(" ERL2 = %d\n", lecStatus.ERL2);
	uartPrintf(" ERLE = %d\n", lecStatus.ERLE);
	uartPrintf(" StatusFlags = 0x%04X\n", lecStatus.StatusFlags);
	uartPrintf(" EventFlags = 0x%04X\n", lecStatus.EventFlags);

	return;
}


#ifdef PORT_CFG_MCBSP_64

// C6657 EVM setup: McBsp 0,1 master
#define SRG_div_1Ghz 81     //  (166.7/2.048 = 81.4)
//#define SRG_div_850Mhz 69  //  ((850/6)/2.048
#define SRG_div_850Mhz 68  //  ((850/6)/2.048)
int bsp_clk_div = SRG_div_850Mhz;

static void dspMcBspTDMInit (void) {
GpakPortConfig_t cfg;
gpakConfigPortStatus_t PrtCfgStat = 0;
GPAK_PortConfigStat_t  PrtDspStat = 0;

    memset(&cfg, 0, sizeof(cfg));
    cfg.SlotsSelect1        = SlotCfgAllActive;
    cfg.Port1Enable         = Enabled;
    cfg.Compand1            = compandMode[0];
    if (compandMode[0] == cmpNone16) 
        cfg.Port1SlotMask0_31   = 0x0000FFFF;
    else
        cfg.Port1SlotMask0_31   = 0xFFFFFFFF;

    cfg.Port1SlotMask32_63  = 0;
    cfg.Port1SlotMask64_95  = 0;
    cfg.Port1SlotMask96_127 = 0;

    cfg.TxDataDelay1        = 0;
    cfg.RxDataDelay1        = 0;
    cfg.ClkDiv1             = bsp_clk_div;
    cfg.FramePeriod1        = (32*8) - 1; // number of clock cycles per frame (32 slots, 8-bits/slot)
    cfg.FrameWidth1         = 1 - 1; // framesync pulse width is 1 clock-period wide 
    cfg.SampRateGen1        = Enabled; 

    cfg.SlotsSelect2        = SlotCfgAllActive;
    cfg.Port2Enable         = Enabled;
    cfg.Compand2            = compandMode[1];
    if (compandMode[1] == cmpNone16) 
        cfg.Port2SlotMask0_31   = 0x0000FFFF;
    else
        cfg.Port2SlotMask0_31   = 0xFFFFFFFF;
    cfg.Port2SlotMask32_63  = 0;
    cfg.Port2SlotMask64_95  = 0;
    cfg.Port2SlotMask96_127 = 0;
    cfg.TxDataDelay2        = 0;
    cfg.RxDataDelay2        = 0;
    cfg.ClkDiv2             = bsp_clk_div;
    cfg.FramePeriod2        = (32*8) - 1; // number of clock cycles per frame (32 slots, 8-bits/slot)
    cfg.FrameWidth2         = 1 - 1; // framesync pulse width is 1 clock-period wide
    cfg.SampRateGen2        = Enabled;
   
   PrtCfgStat = gpakConfigurePorts(0, &cfg, &PrtDspStat);
   if (PrtCfgStat != CfsSuccess || PrtDspStat != Pc_Success) {
      uartPrintf ("McBSP setup failure %d:%d:%d\n", PrtCfgStat, PrtDspStat, DSPError [0]);
   } else {
      uartPrintf("Configured DSP's serial ports. McBSP setup OK\n");
   }

   return;
}

#endif

#ifdef PORT_CFG_MCASP_64
static void dspMcAspTDMInit (void) {
	GpakApiStatus_t gpakApiStatus;			// G.PAK API status
	GpakPortConfig_t gpakPortConfig;		// G.PAK serial port config info
	GPAK_PortConfigStat_t portCfgDspStatus;	// G.PAK Configure Ports DSP status

	memset(&gpakPortConfig, 0, sizeof(gpakPortConfig));
	gpakPortConfig.PortID = APort;
	gpakPortConfig.SlotsPerFrame = NUM_TDM_SLOTS;
    gpakPortConfig.ActiveSlotMap = 0x00000003;
	gpakPortConfig.BitsPerSample = 16;
	gpakPortConfig.BitsPerSlot = 16;
	gpakPortConfig.InputPinMap = 1 << 14;
	gpakPortConfig.OutputPinMap = 1 << 13; 
	gpakPortConfig.HiResClockDivisor = 1;
	gpakPortConfig.ClockDivisor = 1;
	gpakPortConfig.DataDelay = 1;
	gpakPortConfig.FsWord = ADT_FALSE;
	gpakPortConfig.FsFalling = ADT_FALSE;
	gpakPortConfig.ClkRxRising = ADT_TRUE;
	gpakPortConfig.HiResClockInvert = ADT_FALSE;
	gpakPortConfig.GenClocks = ADT_FALSE; 
	gpakPortConfig.SharedClk = ADT_TRUE;
	gpakApiStatus = gpakConfigurePorts(0, &gpakPortConfig, &portCfgDspStatus);
		
	if (gpakApiStatus == GpakApiSuccess)
	{
        uartPrintf("Configured DSP's serial ports. McASP setup OK\n");
	}
	else
	{
		uartPrintf("* Failed to configure serial ports (API status = %d, DSP status = %d)\n",
				   gpakApiStatus, portCfgDspStatus);
	}
}
#endif

#ifdef PORT_CFG_TSIP
#define MAX_SLOTS 1024
void PopulateSlotMask (ADT_UInt32 *slotMask, int slotCnt, int slotsPerSample) {
   ADT_UInt32 mask;
   int  slotsPerMask, bitsPerSlot;

   // Space out slots across the TDM bus.
   if (slotsPerSample == 8) {
      slotsPerMask = 4;
      mask = 0x01010101;
   } else if (slotsPerSample == 4) {
      slotsPerMask = 8;
      mask = 0x11111111;
   } else if (slotsPerSample == 2) {
      slotsPerMask = 16;
      mask = 0x55555555;
   } else {
      slotsPerMask = 32;
      mask = 0xffffffff;
      slotsPerSample = 1;
   }
   bitsPerSlot = 32 / slotsPerMask;

   // Populate slot mask array
   while (slotsPerMask <= slotCnt) {
      *slotMask++ = mask;
      slotCnt -= slotsPerMask;
   }
   *slotMask = (1 << (slotCnt * bitsPerSlot)) - 1;
   *slotMask &= mask;
}
void PopulateCompandingMask (ADT_UInt32 *slotMask, int slotCnt, int slotsPerSample, int compand_a) {
   ADT_UInt32 mask;
   int slotsPerMask;

   if (compand_a) mask = (ADT_UInt32) -1;
   else           mask =  0;
   slotsPerMask = 32 * 8;

   slotCnt *= slotsPerSample;

   // Populate slot mask array
   while (slotsPerMask <= slotCnt) {
      *slotMask++ = mask;
      slotCnt -= slotsPerMask;
   }
   *slotMask = (1 << (slotCnt / 8)) - 1;
   *slotMask &= mask;
}
static void dspTsipTDMInit (void) {

// TSIP configuration variables
   GpakPortConfig_t       PortConfig;
   gpakConfigPortStatus_t PrtCfgStat = (gpakConfigPortStatus_t) 0;
   GPAK_PortConfigStat_t  PrtDspStat = (GPAK_PortConfigStat_t) 0;
   int portBits = 1;
   int slotCnt = NUM_TDM_SLOTS;
   int slotsPerSample = 1;
   ADT_UInt32 DspId = 0;

   memset (&PortConfig, 0, sizeof (PortConfig));

   PortConfig.TsipEnable1 = Disabled;
   PortConfig.TsipEnable2 = Disabled;
   PortConfig.TsipEnable3 = Disabled;
   if (portBits & 1) PortConfig.TsipEnable1 = Enabled;
   if (portBits & 2) PortConfig.TsipEnable2 = Enabled;
   if (portBits & 4) PortConfig.TsipEnable3 = Enabled;

   PopulateSlotMask (PortConfig.slotMask1, slotCnt, slotsPerSample);
   PortConfig.singleClk1       = Enabled;
   PortConfig.txFsPolarity1    = fsActLow;
   PortConfig.txDataClkEdge1   = clkRising;
   PortConfig.txFsClkEdge1     = clkRising;
   PortConfig.txClkMode1       = clkSingleRate;
   PortConfig.txDataRate1      = rate_8Mbps;
   PortConfig.txClkFsSrc1      = clkFsA;
   PortConfig.txDatDly1        = 1023;
   PortConfig.txDriveState1    = highz;
   PortConfig.txOutputDly1     = Disabled;
   PortConfig.rxFsPolarity1    = fsActLow;
   PortConfig.rxDataClkEdge1   = clkFalling;
   PortConfig.rxFsClkEdge1     = clkRising;
   PortConfig.rxClkMode1       = clkSingleRate;
   PortConfig.rxDataRate1      = rate_8Mbps;
   PortConfig.rxClkFsSrc1      = clkFsA;
   PortConfig.rxDatDly1        = 1023;
   PortConfig.loopBack1        = internalLoopback;
   PopulateCompandingMask (PortConfig.slotCompand1, slotCnt, slotsPerSample, 0);
   
   PopulateSlotMask (PortConfig.slotMask2, slotCnt, slotsPerSample);
   PortConfig.singleClk2       = Enabled;
   PortConfig.txFsPolarity2    = fsActLow;
   PortConfig.txDataClkEdge2   = clkRising;
   PortConfig.txFsClkEdge2     = clkRising;
   PortConfig.txClkMode2       = clkSingleRate;
   PortConfig.txDataRate2      = rate_8Mbps;
   PortConfig.txClkFsSrc2      = clkFsA;
   PortConfig.txDatDly2        = 1023;
   PortConfig.txDriveState2    = highz;
   PortConfig.txOutputDly2     = Disabled;
   PortConfig.rxFsPolarity2    = fsActLow;
   PortConfig.rxDataClkEdge2   = clkFalling;
   PortConfig.rxFsClkEdge2     = clkRising;
   PortConfig.rxClkMode2       = clkSingleRate;
   PortConfig.rxDataRate2      = rate_8Mbps;
   PortConfig.rxClkFsSrc2      = clkFsA;
   PortConfig.rxDatDly2        = 1023;
   PortConfig.loopBack2        = internalLoopback;
   PopulateCompandingMask (PortConfig.slotCompand2, slotCnt, slotsPerSample, 0);
   
   PopulateSlotMask (PortConfig.slotMask3, slotCnt, slotsPerSample);
   PortConfig.singleClk3       = Enabled;
   PortConfig.txFsPolarity3    = fsActLow;
   PortConfig.txDataClkEdge3   = clkRising;
   PortConfig.txFsClkEdge3     = clkRising;
   PortConfig.txClkMode3       = clkSingleRate;
   PortConfig.txDataRate3      = rate_8Mbps;
   PortConfig.txClkFsSrc3      = clkFsA;
   PortConfig.txDatDly3        = 1023;
   PortConfig.txDriveState3    = highz;
   PortConfig.txOutputDly3     = Disabled;
   PortConfig.rxFsPolarity3    = fsActLow;
   PortConfig.rxDataClkEdge3   = clkFalling;
   PortConfig.rxFsClkEdge3     = clkRising;
   PortConfig.rxClkMode3       = clkSingleRate;
   PortConfig.rxDataRate3      = rate_8Mbps;
   PortConfig.rxClkFsSrc3      = clkFsA;
   PortConfig.rxDatDly3        = 1023;
   PortConfig.loopBack3        = internalLoopback;
   PopulateCompandingMask (PortConfig.slotCompand3, slotCnt, slotsPerSample, 0);

   PrtCfgStat = gpakConfigurePorts (DspId, &PortConfig, &PrtDspStat);
   if (PrtCfgStat != GpakApiSuccess || PrtDspStat != Pc_Success) {
      uartPrintf ("\n TSIP setup failure %d:%d\n", PrtCfgStat, PrtDspStat);
   } else {
      uartPrintf (" \n TSIP configuration success\n");
   }
#if 0
GpakPortConfig_t cfg;
gpakConfigPortStatus_t PrtCfgStat = 0;
GPAK_PortConfigStat_t  PrtDspStat = 0;

    memset(&cfg, 0, sizeof(cfg));
    cfg.SlotsSelect1        = SlotCfgAllActive;
    cfg.Port1Enable         = Enabled;
    cfg.Compand1            = compandMode[0];
    if (compandMode[0] == cmpNone16) 
        cfg.Port1SlotMask0_31   = 0x0000FFFF;
    else
        cfg.Port1SlotMask0_31   = 0xFFFFFFFF;

    cfg.Port1SlotMask32_63  = 0;
    cfg.Port1SlotMask64_95  = 0;
    cfg.Port1SlotMask96_127 = 0;

    cfg.TxDataDelay1        = 0;
    cfg.RxDataDelay1        = 0;
    cfg.ClkDiv1             = bsp_clk_div;
    cfg.FramePeriod1        = (32*8) - 1; // number of clock cycles per frame (32 slots, 8-bits/slot)
    cfg.FrameWidth1         = 1 - 1; // framesync pulse width is 1 clock-period wide 
    cfg.SampRateGen1        = Enabled; 

    cfg.SlotsSelect2        = SlotCfgAllActive;
    cfg.Port2Enable         = Enabled;
    cfg.Compand2            = compandMode[1];
    if (compandMode[1] == cmpNone16) 
        cfg.Port2SlotMask0_31   = 0x0000FFFF;
    else
        cfg.Port2SlotMask0_31   = 0xFFFFFFFF;
    cfg.Port2SlotMask32_63  = 0;
    cfg.Port2SlotMask64_95  = 0;
    cfg.Port2SlotMask96_127 = 0;
    cfg.TxDataDelay2        = 0;
    cfg.RxDataDelay2        = 0;
    cfg.ClkDiv2             = bsp_clk_div;
    cfg.FramePeriod2        = (32*8) - 1; // number of clock cycles per frame (32 slots, 8-bits/slot)
    cfg.FrameWidth2         = 1 - 1; // framesync pulse width is 1 clock-period wide
    cfg.SampRateGen2        = Enabled;
   
   PrtCfgStat = gpakConfigurePorts(0, &cfg, &PrtDspStat);
   if (PrtCfgStat != CfsSuccess || PrtDspStat != Pc_Success) {
      uartPrintf ("McBSP setup failure %d:%d:%d\n", PrtCfgStat, PrtDspStat, DSPError [0]);
   } else {
      uartPrintf("Configured DSP's serial ports. McBSP setup OK\n");
   }
#endif   
   return;
}

#endif

static void openConf(ADT_UInt16 confID) {

GpakApiStatus_t status;
GPAK_ConferConfigStat_t dsp_status;

    if (confID >= NUM_CONFS) {
        uartPrintf("Invalid Conf ID\n");
        return;
    }

    status = gpakConfigureConference (0, PackCore(confCore[confID], confID), confFrameSize[confID], Null_tone, &dsp_status);
    if (status == GpakApiSuccess) {
        uartPrintf("ConfigureConference %d success\n", confID);
        confReady[confID] = 1;
    } else {
        uartPrintf("ConfigureConference failure: status=%d, dsp_status=%d\n", status, dsp_status);
    }
}

// Defined arbitrary detector settings for CORNET C665x build:
// preempt: 300, 700
// hold:    480
// recorder warning: 1400
// keyclick: 2048
// f2400: 2400
// f2600: 2600
// f2800: 2800
#define NUM_ARB_FREQS   8
#define NUM_ARB_TONES   7

#define ARB_INDEX_BASE    101
#define ARB_INDEX_300_700 (ARB_INDEX_BASE + 0)
#define ARB_INDEX_480     (ARB_INDEX_BASE + 1)
#define ARB_INDEX_1400    (ARB_INDEX_BASE + 2)
#define ARB_INDEX_2048    (ARB_INDEX_BASE + 3)
#define ARB_INDEX_2400    (ARB_INDEX_BASE + 4)
#define ARB_INDEX_2600    (ARB_INDEX_BASE + 5)
#define ARB_INDEX_2800    (ARB_INDEX_BASE + 6)

static void configArbitraryDetector() {
GpakArbTdParams_t           params;
GpakArbTdToneIdx_t          tones[NUM_ARB_TONES];
ADT_UInt16                  freqs[NUM_ARB_FREQS];
GpakApiStatus_t             apiStatus;
GPAK_ConfigArbToneStat_t    dspStatus;

    params.numDistinctFreqs = NUM_ARB_FREQS;   // distinct freqs need to be detected
    params.numTones = NUM_ARB_TONES;           // number of tones needs to be detected
    params.minPower = 0;           // miminum power of tone in dB, 0 == default level
    params.maxFreqDeviation = 0;   // frequency in spec range, , 0 == means default

    freqs[0] = 300;
    freqs[1] = 480;
    freqs[2] = 700;
    freqs[3] = 1400;
    freqs[4] = 2048;
    freqs[5] = 2400;
    freqs[6] = 2600;
    freqs[7] = 2800;

    memset(&tones, 0, sizeof(tones));
    tones[0].f1 = 300;
    tones[0].f2 = 700;
    tones[0].index = ARB_INDEX_300_700;

    tones[1].f1 = 480;
    tones[1].index = ARB_INDEX_480;

    tones[2].f1 = 1400;
    tones[2].index = ARB_INDEX_1400;

    tones[3].f1 = 2048;
    tones[3].index = ARB_INDEX_2048;

    tones[4].f1 = 2400;
    tones[4].index = ARB_INDEX_2400;

    tones[5].f1 = 2600;
    tones[5].index = ARB_INDEX_2600;

    tones[6].f1 = 2800;
    tones[6].index = ARB_INDEX_2800;

    apiStatus = gpakConfigArbToneDetect (0, 0, &params, tones, freqs, &dspStatus);
    if (apiStatus == GpakApiSuccess) {
        uartPrintf("ConfigureArbDetect success\n");
    } else {
        uartPrintf("ConfigureArbDetect failure: apiStatus=%d, dspStatus=%d\n", apiStatus, dspStatus);
    }
}
static void readPlaybackAddress (ADT_Int32 DspId) {
   GpakTestData_t  TestParm;
   GPAK_TestStat_t TestStatus;
   GpakApiStatus_t ApiStatus;

   ADT_UInt16 RespData[10];

   // Divide play record buffer segment into five recording buffers 
   ApiStatus = gpakTestMode (DspId, ReadPlayRecAddr, &TestParm, &RespData[0], &TestStatus);
   if (ApiStatus != GpakApiSuccess) {
      uartPrintf ("\n\rReadPlayRecAddr failure %d:%d:%d\t", ApiStatus, TestStatus, DSPError [DspId]);
      PlayRecAddress = (DSP_Address) -1;
   } else {
      PlayRecAddress = (RespData[0] << 16) | RespData[1];
      uartPrintf ("\n\rPlayRecAddr= %x\t", PlayRecAddress);
   }
}
	
static void  startAudioRecording(ADT_UInt16 channelId, GpakDeviceSide_t DeviceSide) {
GpakApiStatus_t apiStatus; 
GPAK_PlayRecordStat_t dspStatus;

    if (PlayRecAddress == 0)
        readPlaybackAddress (0);
    apiStatus = gpakSendPlayRecordMsg (0, channelId, DeviceSide, StartRecording, PlayRecAddress, 80000ul, RecordingL16, tdsNoToneDetected, &dspStatus);
    if (apiStatus == GpakApiSuccess) {
        uartPrintf("StartAudioRecording success\n");
    } else {
        uartPrintf("StartAudioRecording failure: apiStatus=%d, dspStatus=%d\n", apiStatus, dspStatus);
    }
}

static void  audioRecordingControl(ADT_UInt16 channelId, GpakDeviceSide_t DeviceSide, char code) {
GpakApiStatus_t apiStatus; 
GPAK_PlayRecordStat_t dspStatus;
GpakPlayRecordCmd_t Cmd;
ADT_UInt32 BufferLengthI8;

     if ((code == 'R') || (code == 'r')) 
        Cmd = StartRecording;
     else if ((code == 'P')  || (code == 'p')) 
        Cmd = StartPlayBack;
     else if ((code == 'C')  || (code == 'c')) 
        Cmd = StartContinuousPlayBack;
     else if ((code == 'S')  || (code == 's')) 
        Cmd = StopPlayBack;
     else {
        uartPrintf(" Invalid recording code %c \n",code);
        return;
     }

     if (PlayRecAddress == 0)
        readPlaybackAddress (0);
    BufferLengthI8 = 160000ul; // 10 seconds at 8K sampling

    apiStatus = gpakSendPlayRecordMsg (0, channelId, DeviceSide, Cmd, PlayRecAddress, BufferLengthI8, RecordingL16, tdsNoToneDetected, &dspStatus);
    if (apiStatus == GpakApiSuccess) {
        uartPrintf("StartAudioRecording success\n");
    } else {
        uartPrintf("StartAudioRecording failure: apiStatus=%d, dspStatus=%d\n", apiStatus, dspStatus);
    }


}
static void tdmFixedValControl(ADT_UInt16 channelId, GpakActivation state,  ADT_UInt16 value) {
   GpakTestData_t  TestParm;
   GPAK_TestStat_t TestStatus;
   GpakApiStatus_t ApiStatus;
    ADT_UInt32 DspId = 0;

    ADT_UInt16 RespData[10];

    TestParm.u.fixedVal.ChannelId = channelId;
    TestParm.u.fixedVal.DeviceSide = BDevice;
    TestParm.u.fixedVal.Value = value;
    TestParm.u.fixedVal.State = state; 
    ApiStatus = gpakTestMode (DspId, TdmFixedValue, &TestParm, &RespData[0], &TestStatus);
    if (ApiStatus != GpakApiSuccess) {
      uartPrintf ("\n\rtdmFixedVaControl failure %d:%d:%d\t", ApiStatus, TestStatus, DSPError [DspId]);
    }
}
	

static void procRtcpBye(ADT_UInt16 channelId) {
GpakApiStatus_t status; 
GPAK_RTCPBye_t dspStatus;
ADT_UInt8 bye_message[256];
ADT_UInt8 bye_messageI8;

    bye_messageI8 = sprintf((void *)bye_message, "%s%03d","bye_user_",channelId);
    status = gpakSendRTCPByeMsg (0, PackCore(chanCore[channelId],channelId), bye_message, bye_messageI8, &dspStatus);
    if (status == GpakApiSuccess) {
        uartPrintf("Bye Message success\n");
    } else {
        uartPrintf("Bye Message failure: status=%d, dspStatus=%d\n", status, dspStatus);
    }
}


// ========================================
//{  Host threads
//
//  NOTE: User interface thread is handled by main.
//        Events are handled by evtThread.
//        No RTP thread is required since DSP places packets on network.
static unsigned int threadHandles [2];
static HANDLE evtSemaphore = NULL;

void (*rtpProcess)(ADT_UInt32 DSP) = NULL;

static void evtThread (void * args) {
   ADT_UInt32 Dsp = 0;
   ADT_Int16  chanId;
   GpakApiStatus_t      fifoStat;
   GpakAsyncEventCode_t eventCode;
   GpakAsyncEventData_t eventData;

   if (SetPriorityClass (GetCurrentProcess (), NORMAL_PRIORITY_CLASS) == 0) {
      uartPrintf ("Priority change failure %d\n", GetLastError ());
      //appErr (__LINE__);
   }

   // Poll DSP 0 for events.
   while (!exitFlag) {

      Sleep (10);
#ifndef SKIP_EVENTS
      // Transfer events from DSP to host.
      do {
         fifoStat = gpakReadEventFIFOMessage (Dsp, &chanId, &eventCode, &eventData);
		 if (fifoStat == RefNoEventAvail) {
             //if (userOutLength != 0)
		     //{
		        //SetDlgItemText(hDlg, IDC_EDIT_OUTPUT, userOutBufr);
		    	//userOutLength = 0;
		     //}
			 break;
		 }
         else if (fifoStat != RefEventAvail) {
            uartPrintf ("readEvent error %d\n", fifoStat);
            break;
         }

		 if (userOutLength != 0)
		 {
		 	SetDlgItemText(hDlg, IDC_EDIT_OUTPUT, userOutBufr);
		 	userOutLength = 0;
		 }
	 	 //if (userInLength != 0)
      	 {
           if (eventCounts >= 40) {
                // jdc ... reset display buffer after X-num event messages
	            userOutLength = 0;
		        userOutBufr[0] = '\0';
                userOutIndex = 0;
                eventCounts = 0;
           }
           eventCounts++;
           procDspEvent (chanId, eventCode, &eventData);
		   userInLength = 0;
		   userOutBufr[userOutIndex] = '\0';
		   userOutLength = userOutIndex;
		   SetDlgItemText(hDlg, IDC_EDIT_OUTPUT, userOutBufr);
	     }

      } while (fifoStat == RefEventAvail);
#endif
   }
   uartPrintf ("Polling thread ended\n");
}

static void threadInit () {

   // Create Semaphore to signal RTP thread on interrupt
   evtSemaphore = CreateSemaphore (NULL, 0, 1, (LPCWSTR) "Evt available");

   // Start up DSP transfer threads
   threadHandles[0] = _beginthread (evtThread, 0, NULL);
   if (threadHandles[0] != -1) return;

   uartPrintf ("Thread startup error %d\n", errno);
   //appErr (__LINE__);
}

static void setCIDParams (ADT_UInt32 DspId, GpakCIDTypes CIDType) {
   GpakSystemParms_t  parms;
   GpakApiStatus_t    ApiStat;
   GPAK_SysParmsStat_t dspStatus;

   memset (&parms, 0, sizeof (parms));
   if (CIDType == CID_OFF_HOOK) {
      parms.Cid.numSeizureBytes = 0;
      parms.Cid.numMarkBytes    = 10;
      parms.Cid.numPostambleBytes  = 0;
   } else {
      parms.Cid.numSeizureBytes = 38;
      parms.Cid.numMarkBytes    = 23;
      parms.Cid.numPostambleBytes  = 5;
   }
   parms.Cid.fskType            = fskBell202;
   parms.Cid.fskLevel           = -4;
                                              //  AGC   VAD       PCM-EC     PKT-EC     Cnfr      CID
   ApiStat = gpakWriteSystemParms (DspId, &parms, 0, ADT_FALSE, ADT_FALSE, ADT_FALSE,  ADT_FALSE, ADT_TRUE, &dspStatus);
   if (ApiStat!= GpakApiSuccess) {
      uartPrintf ("Write system configuration failure %d:%d:%d\n", ApiStat, dspStatus, DSPError [DspId]);
   }
}
static void sendCIDMsg (ADT_UInt16 channelId ) {
   GpakApiStatus_t ApiStat;
   GPAK_TxCIDPayloadStat_t DspStat;
   GPAK_AlgControlStat_t   AlgStat;
   char  cidMsg[50];
	ADT_UInt32 DspId = 0; 

	GpakCIDTypes CIDType;
    if (chanCIDMode[channelId] == 3)
        CIDType = CID_ON_HOOK;
    else if (chanCIDMode[channelId] == 4)
        CIDType = CID_OFF_HOOK;
    else {
        uartPrintf ("CIDTx channel %d disabled\n",channelId);
        return;
    }
if(CIDType == CID_ON_HOOK) 
{
   // Caller ID type 1 tests
   setCIDParams (DspId, CIDType);

   Sleep (1000);
   //if (TDM_loopback != CROSSOVER) {
   //   uartPrintf ("TDM loopback not enabled\n");
   //   return;
   //}

   //chanId = ADT_getint ("\nCaller ID channel id: ");
   //uartPrintf ("\n\r------------------------------------------------\r\nType 1. Single field test.\r\n");
   // Single message format  mmddhhmmtttttttttt  (month day hours minutes and telephone)
   sprintf (cidMsg, "%s", "032114051115551234"); 
#if 0
   AlgStat = Ac_Success;
   ApiStat = gpakAlgControl (DspId, PackCore(chanCore[channelId],channelId), CID1RXRestart, Null_tone, Null_tone, ADevice, 0, 0, &AlgStat);
   if (ApiStat != GpakApiSuccess || AlgStat != Ac_Success) {
      uartPrintf ("CIDTx channel %d setup failure %d:%d:%d\n", channelId, ApiStat, DspStat, DSPError [DspId]);
   }
   Sleep (1000);
#endif

   DspStat =  Scp_Success;
   ApiStat = gpakSendCIDPayloadToDsp (DspId, PackCore(chanCore[channelId],channelId), CID_ON_HOOK, CID_SDMF, (ADT_UInt8 *) cidMsg, (ADT_UInt16) strlen (cidMsg), ADevice, &DspStat);
   if (ApiStat != GpakApiSuccess || DspStat != Scp_Success) {
      uartPrintf ("CIDTx channel %d setup failure %d:%d:%d\n", channelId, ApiStat, DspStat, DSPError [DspId]);
   }
   //if (processTestKeys (0, NULL) == ESC) return;
   //Sleep (5000);

   //uartPrintf ("\n\r------------------------------------------------\r\nType 1. Multiple field test.\r\n");
   // Multiple message format   _01_mmddhhmm_02_tttttttttt_07_name
   sprintf (cidMsg, "%c%c%s%c%c%s%c%c%s", 1, 8,"03211405", 2, 10, "1115551234", 7, 9, "DOE JOE 1");
   DspStat =  Scp_Success;
   ApiStat = gpakSendCIDPayloadToDsp (DspId, PackCore(chanCore[channelId],channelId), CID_ON_HOOK, CID_MDMF, (ADT_UInt8 *) cidMsg, (ADT_UInt16) strlen (cidMsg), ADevice, &DspStat);
   if (ApiStat != GpakApiSuccess || DspStat != Scp_Success) {
      uartPrintf ("CIDTx channel %d setup failure %d:%d:%d\n", channelId, ApiStat, DspStat, DSPError [DspId]);
   }
   //if (processTestKeys (0, NULL) == ESC) return;
   Sleep (1000);
} else {

   //------------------------------------------------------------------------------------
   // Caller ID type 2 tests
   uartPrintf ("\n\r------------------------------------------------\r\nType 2. Single field test.\r\n");
   setCIDParams (DspId, CID_OFF_HOOK);

   Sleep (1000);
#if 0
   AlgStat = Ac_Success;
   ADT_UInt16 ChannelIdxor = (channelId ^ 1); 
   ApiStat = gpakAlgControl (DspId, PackCore(chanCore[channelIdxor],channelIdxor), CID2RXRestart, Null_tone, Null_tone, ADevice, 0, 0, &AlgStat);
   if (ApiStat != GpakApiSuccess || AlgStat != Ac_Success) {
      uartPrintf ("AlgCtrl channel %d setup failure %d:%d:%d\n", channelIdxor, ApiStat, AlgStat, DSPError [DspId]);
   }
#endif
   Sleep (1000);
   // Single message format  mmddhhmmtttttttttt  (month day hours minutes and telephone)
   sprintf (cidMsg, "%s", "032114052225551234"); 
   DspStat =  Scp_Success;
   ApiStat = gpakSendCIDPayloadToDsp (DspId, PackCore(chanCore[channelId],channelId), CID_OFF_HOOK, CID_SDMF, (ADT_UInt8 *) cidMsg, (ADT_UInt16) strlen (cidMsg), ADevice, &DspStat);
   if (ApiStat != GpakApiSuccess || DspStat != Scp_Success) {
      uartPrintf ("CIDTx channel %d setup failure %d:%d:%d\n", channelId, ApiStat, DspStat, DSPError [DspId]);
   }

   //if (processTestKeys (0, NULL) == ESC) return;
   Sleep (100);

   // Multiple message format   _01_mmddhhmm_02_tttttttttt_07_name
   uartPrintf ("\n\r------------------------------------------------\r\nType 2. Multiple field test\r\n");
   sprintf (cidMsg, "%c%c%s%c%c%s%c%c%s", 1, 8,"03211405", 2, 10, "2225551234", 7, 9, "DOE JOE 2");
   DspStat =  Scp_Success;
   ApiStat = gpakSendCIDPayloadToDsp (DspId, PackCore(chanCore[channelId],channelId), CID_OFF_HOOK, CID_MDMF, (ADT_UInt8 *) cidMsg, (ADT_UInt16) strlen (cidMsg), ADevice, &DspStat);
   if (ApiStat != GpakApiSuccess || DspStat != Scp_Success) {
      uartPrintf ("CIDTx channel %d setup failure %d:%d:%d\n", channelId, ApiStat, DspStat, DSPError [DspId]);
   }

   //processTestKeys (0, NULL);
   Sleep (100);
}

}
static ADT_UInt16 sendDtmfDialString(ADT_UInt16 channelId, GpakDeviceSide_t sideId, char *str) {
GpakDtmfParms_t dtmfParms;
GpakApiStatus_t ApiStat;
GPAK_DtmfDialStat_t dspStatus;
GpakToneCodes_t *toneCodes = dtmfParms.DtmfDigits;
ADT_UInt16 n;
ADT_UInt32 DspId = 0;


    toneCodes = dtmfParms.DtmfDigits;
    n =0;
    while (*str !='\0') { 
	    // Skip space characters.
        while (SCAN_IS_SPACE(*str)) {
	        ++str;
        }
        if (*str == '1') {
            toneCodes[n++] = tdsDtmfDigit1;
        }
        else 
        if (*str == '2') {
            toneCodes[n++] = tdsDtmfDigit2;
        }
        else 
        if (*str == '3') {
            toneCodes[n++] = tdsDtmfDigit3;
        }
        else 
        if ((*str == 'A') || (*str == 'a')) {
            toneCodes[n++] = tdsDtmfDigitA;
        }
        else 
        if (*str == '4') {
            toneCodes[n++] = tdsDtmfDigit4;
        }
        else 
        if (*str == '5') {
            toneCodes[n++] = tdsDtmfDigit5;
        }
        else 
        if (*str == '6') {
            toneCodes[n++] = tdsDtmfDigit6;
        }
        else 
        if ((*str == 'B') || (*str == 'b')) {
            toneCodes[n++] = tdsDtmfDigitB;
        }
        else 
        if (*str == '7') {
            toneCodes[n++] = tdsDtmfDigit7;
        }
        else 
        if (*str == '8') {
            toneCodes[n++] = tdsDtmfDigit8;
        }
        else 
        if (*str == '9') {
            toneCodes[n++] = tdsDtmfDigit9;
        }
        else 
        if ((*str == 'C') || (*str == 'c')) {
            toneCodes[n++] = tdsDtmfDigitC;
        }
        else 
        if (*str == '*') {
            toneCodes[n++] = tdsDtmfDigitSt;
        }
        else 
        if (*str == '0') {
            toneCodes[n++] = tdsDtmfDigit0;
        }
        else 
        if (*str == '#') {
            toneCodes[n++] = tdsDtmfDigitPnd;
        }
        else 
        if ((*str == 'D') || (*str == 'd')) {
            toneCodes[n++] = tdsDtmfDigitD;
        }
        else {
	        uartPrintf("invalid dtmf digit!\n");
            return 0;
        }
        if ((n == 0) || (n>16))
		{
	        uartPrintf("dialstring length error!... must be 1-16 chars!\n");
		    return 0; 
        }
        str++;
    }

    dtmfParms.NumDigits = n;
    dtmfParms.Device = sideId;
    dtmfParms.OnTimeMs = 100;
    dtmfParms.InterdigitTimeMs = 100;
    dtmfParms.Level[0] = -10;
    dtmfParms.Level[1] = -13;

    ApiStat = gpakDtmfDial (DspId, PackCore(chanCore[channelId],channelId), &dtmfParms, &dspStatus);

    if (ApiStat!= GpakApiSuccess) {
      uartPrintf ("DtmdDialError %d:%d:%d\n", ApiStat, dspStatus, DSPError [DspId]);
      n = 0;
    }


    return n;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// displayHelp - Display command help information.
//
// FUNCTION
//  This function displays command help information.
//
// RETURNS
//  nothing
//
static void displayHelp(void)
{

	uartPrintf("Commands:\n");
	uartPrintf(" HELP or ? - Display this list of commands\n");
	uartPrintf(" Per channel settings, syntax: cmd[#[-#]][=#]\n");
	uartPrintf("  example: CODEC displays all channels Codec selection\n");
	uartPrintf("           CODEC=PCMU sets all channels for Mu-Law\n");
	uartPrintf("           CODEC0=PCMU sets channel 0 for MuLaw\n");
	uartPrintf("           CODEC0-2=PCMU sets channels 0 to 2 for MuLaw\n");
#ifdef CUSTOM_COMMANDS
	uartPrintf("  CM - switch to custom menu if enabled\n");
#endif

	uartPrintf("  CORE -  Channel Core assignment: single core\n");
	uartPrintf("  COREI - Channel Core assignment: interleave among N cores\n");
	uartPrintf("  COREB - Channel Core assignment: block mode among N cores\n");
	uartPrintf("  CODEC - Codec Type\n");
	uartPrintf("  CODECI - interleaved Codec: {PCMU,G729,G726_32} \n");
	uartPrintf("  FRAME - Frame Size\n");
	uartPrintf("  FRAMEI#-# interleaved #-#==startframe-endframe (10,20,30,40,50,60)\n");
	uartPrintf("  AINSLOT - A Side Input Slot\n");
	uartPrintf("  BINSLOT - B Side Input Slot\n");
	uartPrintf("  AOUTSLOT - A Side Output Slot\n");
	uartPrintf("  BOUTSLOT - B Side Output Slot\n");
	uartPrintf("  ASLOTCROSS - Even chan#: outslot=chanId, inslot=chanId+1\n");
	uartPrintf("             - Odd  chan#: outslot=chanId, inslot=chanId-1\n");
	uartPrintf("  AINGAIN - A Side Input Gain\n");
	uartPrintf("  BINGAIN - B Side Input Gain\n");
	uartPrintf("  AOUTGAIN - A Side Output Gain\n");
	uartPrintf("  BOUTGAIN - B Side Output Gain\n");
#ifdef USE_AEC
	uartPrintf("  AECENAB - AEC Enable\n");
	uartPrintf("  AECCAP - AEC Capture\n");
	uartPrintf("  AECSTAT - Display Channel AEC Status\n");
	uartPrintf("  AEC - AEC Parameter (16 bit signed integer)\n");
#endif
	uartPrintf("  LECENAB - LEC Enable\n");
	uartPrintf("  LECSTAT - Display Channel LEC Status\n");
	uartPrintf("  LEC - LEC Parameter (16 bit signed integer)\n");
	uartPrintf("  AGCENAB - AGC Enable\n");
	uartPrintf("  VADCNGENAB - VADCNG Enable\n");
	uartPrintf("  DTXENAB - DTX Enable\n");
	uartPrintf("  DATAENAB - Data Mode Enable\n");
	uartPrintf("  SRTPENAB - SRTP Crypto Suite: 0:Disable, 1-12:Enable\n");
	uartPrintf("  RTCPENAB - RTCP Enable\n");
	uartPrintf("  RTPCTRL - RTP Control: <RX0, RX1, TX0, TX1>\n");
	uartPrintf("  RTPIP - RTP IP Address\n");
	uartPrintf("  RTPPORT - RTP Port, SRTPPORT - Source RTP Port\n");
	uartPrintf("  JBSIZE - Jitter Buffer Size\n");
	uartPrintf("  SUPKT - Setup PCM to Packet Channel\n");
	uartPrintf("  SUPCM - Setup PCM to PCM Channel\n");
	uartPrintf("  CNFPKT - Setup Conference Packet Channel (chid-cnfid)\n");
	uartPrintf("  CNFPCM - Setup Conference PCM Channel (chid-cnfid)\n");
	uartPrintf("  CNFCMP - Setup Conference Composite Channel (chid-cnfid)\n");
	uartPrintf("  TD - Channel Teardown\n");
	uartPrintf("  CS - Channel Status\n");
	uartPrintf("  PKTS - Channel RTP Packet Statistics\n");
	uartPrintf("  TONEGENA  - Start A-side Tone Generation\n");
	uartPrintf("  TONEGENB  - Start B-side Tone Generation\n");
	uartPrintf("  TONESTOPA - Stop A-side Tone Generation\n");
	uartPrintf("  TONESTOPB - Stop B-side Tone Generation\n");
    uartPrintf("  DETA  - Set A-Side DTMF detect: DTMF,CPRG,MFR2F,MFR2R,ARB,MFR1,FAX\n");
    uartPrintf("  DETB  - Set B-Side DTMF detect: DTMF,CPRG,MFR2F,MFR2R,ARB,MFR1,FAX\n");
	uartPrintf("  DTMFMODE - set DTMFMODE 0==NONE, 1==INBAND,2==RELAY,3==OUTBAND\n");
	uartPrintf("  DTMFDIALA - <generate dtmf digitstring to A-side 0-9,a,b,c,d,*,#> \n");
	uartPrintf("  DTMFDIALB - <generate dtmf digitstring to B-side 0-9,a,b,c,d,*,#> \n");
	uartPrintf("  FAXMODE - set FaxMODE D==disabled, F==fax-only, V==fax-voice\n");
	uartPrintf("  CIDMODE - D==disabled, R1=RxOnHook, T1=TxOnHook, R2=RxOffHook, T2=TxOffHook\n"); 
    uartPrintf("  ARB - configure arbitrary toned detector\n");
    uartPrintf("  AUDA<R,P,C,S><chan> - A-side audio control: Record,Play,ContinuousPlay,Stop on chan\n");
    uartPrintf("  AUDB<R,P,C,S><chan> - B-side audio control: Record,Play,ContinuousPlay,Stop on chan\n");
    uartPrintf("  VLANSTAG - Set Vlan Tag ID:PRI field for index: <Idx=0-9> <Id=0-4095> <Pri=0-7>\n");
    uartPrintf("  VLANGTAG - Get Vlan Tag ID:PRI field for index: <Idx=0-9>\n");
    uartPrintf("  VLANIDX - Assign VLAN IDX to channel\n");
    uartPrintf("  SYSVAD  - noisefloor hangtimeMs windowSizeMs ReportEnable\n");
	uartPrintf("  SYSCFG - Display System Configuration\n");
	uartPrintf("  SYSPARMS - Display System Parameters\n");
	uartPrintf("  NET - Display Network Configuration\n");
	uartPrintf("  TDM - Display TDM Statistics\n");
	uartPrintf("  CPU - Display CPU Usage\n");
	uartPrintf("  RSTCPU - Reset CPU Statistics\n");
	uartPrintf("  RSTFRAME - Reset Framing Statistics\n");
#if 0
	uartPrintf("  STN - Side Tone\n");
	uartPrintf("           STN= Chan# Enable/Disab100 (.1dB)\n");
	uartPrintf("  MEMT - Read Memory Test Status\n");
#endif

#ifdef PORT_CFG_MCBSP_64	
	uartPrintf("  PORT - Config McBsp Port\n");
#endif
#ifdef PORT_CFG_MCASP_64
	uartPrintf("  PORT - Config McAsp Port\n");
#endif	
#ifdef PORT_CFG_TSIP
	uartPrintf("  PORT - Config Tsip Port\n");
#endif
	uartPrintf("  TDMFIXON - <value> .. turn on channel tdmout fixed value\n");
	uartPrintf("  TDMFIXOFF - turn off channel tdmout fixed value\n");

	uartPrintf("  PTTCFG - configure ED137 PTT parameters\n");
	uartPrintf("  PTTUPT - Update    ED137 PTT parameters\n");
	uartPrintf("  PTTPF  - configure ED137 Persistent Feature parameters\n");
	uartPrintf("  PTTPFU - update ED137 Persistent Feature parameters\n");
	uartPrintf("  PTTNPF - configure ED137 Non-Persistent Feature parameters\n");
	uartPrintf("  RTCPBYE - send RTCP bye message to channel\n");
	uartPrintf("  WAVLOAD=<file index> ... load a wavefile\n");
	uartPrintf("  WAVPLAY<chanId>-<waveId>=<NET> ... wavefile play once. Default to PCM.\n");
	uartPrintf("  WAVLOOP<chanId>-<waveId>=<NET> ... wavefile play loop. Default to PCM.\n");
	uartPrintf("  WAVSTOP<chanId>-<waveId>=<NET> ... wavefile play stopped. Default to PCM.\n");
	uartPrintf("  GETVER ... read DSP version string\n");
	return;
}

