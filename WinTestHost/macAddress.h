#pragma once


// macAddress dialog

class macAddress : public CDialog
{
	DECLARE_DYNAMIC(macAddress)

public:
	macAddress(CWnd* pParent = NULL);   // standard constructor
	virtual ~macAddress();

// Dialog Data
	enum { IDD = IDD_DIALOG1 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
};
