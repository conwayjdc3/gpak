/*
 * Copyright (c) 20019, Adaptive Digital Technologies, Inc.
 *
 * File Name: GpakApi.h
 *
 * Description:
 *   This file contains the function prototypes and data types for the user
 *   API functions that communicate with DSPs executing G.PAK software. The
 *   file is used by application software in the host processor connected to
 *   G.PAK DSPs via a Host Port Interface.
 *
 * Version: 2.0
 *
 * Revision History:
 *   10/17/01 - Initial release.
 *   07/03/02 - Conferencing and test mode updates.
 *   06/15/04 - Tone type updates.
 *   1/2007   - Combined C54 and C64
 */

#ifndef _GPAKED137BAPI_H  /* prevent multiple inclusion */
#define _GPAKED137BAPI_H

#include "adt_typedef_user.h"
#include "GpakEnum.h"
#include "GpakErrs.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Voice Activity Detection (VAD) */
#define EventVadStateVoice    8             // Vad state Voice event
#define EventVadStateSilence  9             // Vad state Silence event

    /* ED137B Events */
#define customED137B_PTT_Event                   202            /* Set when a change in PTT Value is detected                  */
#define customED137B_PersistentFeatureEvent      203            /* Set when a change in Peristent Feature Value is detected    */
#define customED137B_NONPersistentFeatureEvent   204            /* Set when a Non-Peristent Feature Value is received          */
#define customED137B_Timeout_Event               205            /* Set when a change in Peristent Feature Value is detected    */

    /* Need to update ServiceGpakInterface in GpakMsg.c and add defines to GpaHpi.h */
    
#define MSG_CONFIGURE_ED137B_PARMS                                  248      /* Write ED137B Configuration Parameters */
#define MSG_CONFIGURE_ED137B_PARMS_REPLY                            249

#define MSG_UPDATE_ED137B_PTT_PARMS                                 246      /* Update ED137B PPT Parameters */
#define MSG_UPDATE_ED137B_PTT_PARMS_REPLY                           247

#define MSG_CONFIGURE_ED137B_PERSISTENT_FEATURES_PARMS              244      /* Write ED137B Persistent Feature Parameters */
#define MSG_CONFIGURE_ED137B_PERSISTENT_FEATURES_PARMS_REPLY        245

#define MSG_ED137B_PERSISTENT_FEATURES_UPDATE_PARMS                 242      /* Update ED137B Persistent Feature Parameters */
#define MSG_CONFIGURE_ED137B_PERSISTENT_FEATURES_UPDATE_PARMS_REPLY 243

#define MSG_CONFIGURE_ED137B_NON_PERSISTENT_FEATURES_PARMS          240      /* Write ED137B NON Persistent Feature Parameters */
#define MSG_CONFIGURE_ED137B_NON_PERSISTENT_FEATURES_PARMS_REPLY    241

#define MAX_NONPF_EVENT 256                                                 /* Max Number NON Persistent Features in one packet */
#define MAX_NONPF       16 

typedef struct scustomED137B_PTT_Event {                              /*  For PPT Event detection                                        */
            ADT_UInt32 Devside;
            ADT_UInt32 PTTx;                                          /*  RTP extension                                                  */
} scustomED137B_PTT_Event_t;

typedef struct scustomED137B_PersistentFeatureEvent {                 /*  For Persistent Event detection                                 */
            ADT_UInt32 Devside;
			ADT_UInt16 Length;
			ADT_UInt16 TYPE;                                          /*  Persistent Feature Type                                        */
			ADT_UInt16 PF;                                            /*  Persistent Feature Value                                       */
 			ADT_UInt16 RTP_PAYLOAD_TYPE;                              /*  Persistent Feature Payload Type                                */
} scustomED137B_PersistentFeatureEvent_t;

struct sED137B_NONPersistentFeature {                                 /* For Non-Persistent Feature                                      */
            ADT_UInt16 NONPF_Lentgh;                                  /*  Length of NON-Persistent Features reported                     */
            ADT_UInt8  NONPersistentFeature_Value[MAX_NONPF];         /*  Non-Persistent Feature                                         */
} sED137B_NONPersistentFeature;

struct scustomED137B_NONPersistentFeatureEvent {                      /* For Non-Persistent Event detection                              */
            ADT_UInt16 NUM_NONPF;                                     /*  Number of NON-Persistent Features reported in this event       */
            ADT_UInt8  NONPersistentFeature[MAX_NONPF_EVENT];         /*  Non-Persistent Feature                                         */
} scustomED137B_NONPersistentFeatureEvent;

struct scustomED137B_Timeout_Event {                                 /*  For Stay-Alive Error Reporting                                  */
            ADT_UInt16 StayAliveTO;                                  /*  Time Out value to be assigned                                   */
} scustomED137B_Timeout_Event;

// gpakED137B Configuration Parameters reply status codes.
typedef enum GPAK_ED137BcfgStat_t {
    ED137Bcfg_Success = 0,                        // ED137B Configuration Parameters written successfully

    /* ED137B Configuration parameters errors. */
    ED137Bcfg_BadParam       
} GPAK_ED137BcfgStat_t;

// gpakED137B PTT Update Parameters reply status codes.
typedef enum GPAK_ED137B_PTTupdateStat_t {
    ED137B_PTTupdate_Success = 0,                // PTT Update Parameters written successfully

    /* PTT Update parameters errors. */
    ED137B_PTTupdate_BadParam       
} GPAK_ED137B_PTTupdateStat_t;

// gpakED137B Configure Persistent Feature Parameters reply status codes.
typedef enum GPAK_ED137B_PersistentFeatureStat_t {
    ED137B_PersistentFeature_Success = 0,        // Persistent Feature Parameters written successfully

    /* Persitent Feature parameters errors. */
    ED137B_PersistentFeature_BadParam       
} GPAK_ED137B_PersistentFeatureStat_t;

// gpakED137B Update Persistent Feature Parameters reply status codes.
typedef enum GPAK_ED137B_PersistentFeatureUpdateStat_t {
    ED137B_PersistentFeatureUpdate_Success = 0,    // Persistent Feature Parameters updated successfully

    /* Persitent Feature Update parameters errors. */
    ED137B_PersistentFeatureUpdate_BadParam       
} GPAK_ED137B_PersistentFeatureUpdateStat_t;

// gpakED137B Configure NON Persistent Feature Parameters reply status codes.
typedef enum GPAK_ED137B_NONPersistentFeatureStat_t {
    ED137B_NONPersistentFeature_Success = 0,        // NON Persistent Feature Parameters written successfully

    /* NON Persitent Feature parameters errors. */
    ED137B_NONPersistentFeature_BadParam       
} GPAK_ED137B_NONPersistentFeatureStat_t;

// gpakED137B Gate Audio Feature Parameters reply status codes.
typedef enum GpakED137B_GateAudioFeatureStat_t {
    ED137B_GateAudioFeature_Success = 0,        // NON Persistent Feature Parameters written successfully

    /* Gate Audio Feature parameters errors. */
    ED137B_GateAudioFeature_BadParam       
} GpakED137B_GateAudioFeatureStat_t;

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakED137B - ED137B Configuration settings
*/

typedef struct GpakED137BcfgParms {
    ADT_UInt16 rtp_ext_en;              /* Specifies normal RTP packet or a packet with an ED-137B header extension.      */
                                        /* 0=disable, 1=enable                                                            */
    ADT_UInt16 device;                  /* Device Type                                                                    */
                                        /* Specifies a VCS or GRS device. This dictates the direction of the parameters   */
                                        /* and the R2S keep-alive implementation.                                         */
                                        /* VCS=1, GRS=2                                                                   */
    ADT_UInt16 txrxmode;                /* Connection Type                                                                */
                                        /* This dictates if the GRS is a transceiver or separate transmitter/receiver.    */
    ADT_UInt16 follow_squ_with_vad;     /* Set the Squelch bit on RTP/Voice packets being generated by the DSP,         */
	                                    /* clear the Squelch bit on silence packets.                                    */
	ADT_UInt16 ptt_rep;                 /* PTT Repetition                                                                 */
                                        /* Number of audio packets sent with RTPTx HE indicating that PTT is OFF or       */
                                        /* RTPRx HE indicating that SQUELCH is OFF (value 0->only 1 PTT OFF or only 1     */
                                        /* SQUELCH OFF message). See ED-137B section 3.6.1.8.                             */
                                        /* 0-3                                                                            */
    ADT_UInt16 R2SKeepAlive;            /* R2S Period                                                                     */
                                        /* Maximum time between each R2S-Keep Alive packet. integer, in milliseconds.     */
                                        /* See ED-137B section 3.6.1.9.                                                   */
                                        /* 20-1000                                                                        */
    ADT_UInt16 R2SKeepAliveMultiplier;  /* R2S Multiplier                                                                 */
                                        /* Number of Keep Alive packets in error or not received by endpoint before Time  */
                                        /* Out of the session. See ED-137B section 3.6.1.10.                              */
                                        /* 2-50                                                                           */
    ADT_UInt16 Ptt_val;                 /* PTT Value                                                                      */
                                        /* Initial 16-bit entry for RTP extension                                         */
                                        /* 0000-FFFF                                                                      */
    ADT_UInt16 mask;                    /* PTT Rx Mask                                                                    */
                                        /* Mask to indicate which change of events to report                              */
                                        /* 0000-FFFF                                                                      */                                                        
} GpakED137BcfgParms;

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakED137B - ED137B Update PTT Configuration settings
*/

typedef struct GpakED137B_PTTupdateParms {                                                                      
    ADT_UInt16 PTT_Type;                /* 0x00 PTT OFF                                                                   */
                                        /* 0x01 Normal PTT ON                                                             */
                                        /* 0x02 Coupling PTT ON                                                           */
                                        /* 0x03 Priority PTT ON                                                           */
                                        /* 0x04 Emergency PTT ON                                                          */
                                        /* 0x05 Reserved                                                                  */
                                        /* 0x06 Reserved                                                                  */
                                        /* 0x07 Reserved                                                                  */
	ADT_UInt16 SQU;                     /* Squelch                                                                        */
                                        /* 0-1                                                                            */
    ADT_UInt16 PTT_ID;                  /* Set by the GRS Receiver in SDP transaction.  Ranges from 1 to the maximum      */
                                        /* number of allowed SIP sessions. CTI will extract the SDP value and set this    */
                                        /* field.                                                                         */
    ADT_UInt16 PM;                      /* Used for signaling PTT_ON to non-transmitting (not selected) transmitters in   */
                                        /* a coverage group.                                                              */
                                        /* 0-1                                                                            */
    ADT_UInt16 PTTS;                    /* PTT Summation                                                                  */
                                        /* 0-1                                                                            */

} GpakED137B_PTTupdateParms;

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakED137B - ED137B Configure Persistent Features settings
*/

typedef struct GpakED137B_PersistentFeatureParms {
    ADT_UInt16 PersistentFeature_Type;  /* Used to signal which bit changes reflect event generation                    */
                                           /* 0x00-0xFF                                                                    */
    ADT_UInt16    PersistentFeature_Mask;  /* Used to signal which bit changes reflect event generation                    */
                                           /* 0x00-0xFF                                                                    */
    ADT_UInt16 PersistentFeature_Value;    /* The initial value used for persistent additional feature                     */                                
                                           /* 0x00-0xFF                                                                    */

} GpakED137B_PersistentFeatureParms;

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakED137B - ED137B Update Persistent Features settings
*/

typedef struct GpakED137B_PersistentFeatureUpdateParms {
       ADT_UInt16 PersistentFeature_Type;  /* Used to signal which bit changes reflect event generation                    */
                                           /* 0x00-0xFF                                                                    */
    ADT_UInt16 PersistentFeature_Value;    /* The initial value used for persistent additional feature                     */                                
                                           /* 0x00-0xFF                                                                    */
} GpakED137B_PersistentFeatureUpdateParms;

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakED137B - ED137B Configure NON Persistent Features settings
*/
typedef struct GpakED137B_NONPersistentFeatureParms {
       ADT_UInt16 NONPersistentFeature_Type;            /* Additional Features Type Definition                            */
                                                        /* 0x00-0x0F                                                      */
    ADT_UInt16 NONPersistentFeature_Length;             /* Length = Number of Octets                                      */
                                                        /* 1-15                                                           */                                                            
    ADT_UInt32  NONPersistentFeature_Value[32];         /* The completed value to be added to an active RTP packet stream */
														/*	This will include the type, lenght and all value octets       */                                
                                        
} GpakED137B_NONPersistentFeatureParms;

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakED137B - ED137B Gate Audio Features settings
*/
typedef struct GpakED137B_GateAudioFeatureParms {
       ADT_UInt16 Enable_Mask;                        /* Enabled Features                                                */                          
                                        
} GpakED137B_GateAudioFeatureParms;
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakConfigureED137B
 *
 * FUNCTION
 *  This function configures a DSP's ED137B Support.  Each channel shall be 
 *  independently configured for ED-137B support. The fields below depict the 
 *  initial channel configurations. If the first field rtp_ext_en is set to 
 *  zero then the channel does not utilize the ED-137B header extension.
 *
 * Inputs
 *      DspId            - Dsp identifier 
 *      ADT_UInt16        - Channel Id
 *      ED137B            - Configuration params 
 *
 * Outputs
 *      Status            - DSP return status
 *
 * RETURNS
 *     Status code indicating success or a specific error.
 *
 */

GpakApiStatus_t gpakConfigureED137B (ADT_UInt32 DspId, ADT_UInt16 ChannelId, GpakED137BcfgParms *ED137BcfgParms,
                                                GPAK_ED137BcfgStat_t *Status);

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakPTT_Update
 *
 * FUNCTION
 *  This function supports dynamic ED137B updates of the PTT-Type field 
 *  used to key and un-key the GRS. The PTT-ID field will be unaltered 
 *  and repeated with the value set when the channel was initialized.
 *
 * Inputs
 *      DspId            - Dsp identifier 
 *      ADT_UInt16        - Channel Id
 *      ED137B             PTT Update params 
 *
 * Outputs
 *      Status  - DSP return status
 *
 * RETURNS
 *      Status code indicating success or a specific error.
 *
 */


GpakApiStatus_t gpakPTT_Update (ADT_UInt32 DspId, ADT_UInt16 ChannelId, GpakED137B_PTTupdateParms *ED137B_PTTupdateParms,
                                            GPAK_ED137B_PTTupdateStat_t *Status);

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakPersistentFeature
 *
 * FUNCTION
 * This function configures the Persistent Feature parameters.  The persistence 
 * feature shall be included in every RTP packet. This feature is used to
 * dynamically change the Value field. The Rx mask is used to trigger events
 * when unmasked bits change state. 
 *
 * Inputs
 *      DspId            - Dsp identifier 
 *      ADT_UInt16        - Channel Id
 *      ED137B            - Persistent Feature params 
 *
 * Outputs
 *      Status  - DSP return status
 *
 * RETURNS
 *      Status code indicating success or a specific error.
 *
 */


GpakApiStatus_t gpakPersistentFeature (ADT_UInt32 DspId, ADT_UInt16 ChannelId, GpakED137B_PersistentFeatureParms *ED137B_PersistentFeatureParms,
                                            GPAK_ED137B_PersistentFeatureStat_t *Status);


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakPersistentFeatureUpdate
 *
 * FUNCTION
 * This function allows dynamic updates of the value field of the persistent packet 
 * identified by the DSP channel and Type field (B-F).
 *
 * Inputs
 *      DspId            - Dsp identifier
 *      ADT_UInt16        - Channel Id
 *      ED137B            - Persistent Feature Type and Value params 
 *
 * Outputs
 *      Status  - DSP return status
 *
 * RETURNS
 *      Status code indicating success or a specific error.
 *
 */

GpakApiStatus_t gpakPersistentFeatureUpdate (ADT_UInt32 DspId, ADT_UInt16 ChannelId, GpakED137B_PersistentFeatureUpdateParms *ED137B_PersistentFeatureUpdateParms,
                                            GPAK_ED137B_PersistentFeatureUpdateStat_t *Status);

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakNONPersistentFeature
 *
 * FUNCTION
 * This function configures the NON Persistent Feature parameters.The non-persistent
 * feature type can be appended to any active ED-137B channel. 
 * These packets are only sent once but can include one or more feature types. 
 * Note that the total additional features total length must be reflected in the 
 * ED-137B Header Extension length field as specified in the ED-137B specification
 *
 * Inputs
 *      DspId            - Dsp identifier
 *      ADT_UInt16        - Channel Id
 *      ED137B            - NON Persistent Feature params 
 *
 * Outputs
 *      Status  - DSP return status
 *
 * RETURNS
 *      Status code indicating success or a specific error.
 *
 */

GpakApiStatus_t gpakNONPersistentFeature (ADT_UInt32 DspId, ADT_UInt16 ChannelId, ADT_UInt32 Num_Features, GpakED137B_NONPersistentFeatureParms *ED137B_NONPersistentFeatureParms,
                                            GPAK_ED137B_NONPersistentFeatureStat_t *Status);


#endif  /* prevent multiple inclusion */

#ifdef __cplusplus
}
#endif
