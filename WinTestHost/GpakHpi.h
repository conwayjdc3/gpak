/*
 * Copyright (c) 2001, Adaptive Digital Technologies, Inc.
 *
 * File Name: GpakHpi.h
 *
 * Description:
 *   This file contains common definitions related to the G.PAK interface
 *   between a host processor and a DSP processor via the Host Port Interface.
 *
 * Version: 1.0
 *
 * Revision History:
 *   10/17/01 - Initial release.
 *
 */

#ifndef _GPAKHPI_H  /* prevent multiple inclusion */
#define _GPAKHPI_H

#include "adt_typedef.h"

/* Definition of G.PAK Command/Reply message type codes. */
#define MSG_NULL_REPLY 0            /* Null Reply (unsupported Command) */

#define MSG_SYS_CONFIG_RQST 1             /* System Configuration Request */
#define MSG_SYS_CONFIG_REPLY 2

#define MSG_READ_SYS_PARMS 3              /* Read System Parameters */
#define MSG_READ_SYS_PARMS_REPLY 4  

#define MSG_WRITE_SYS_PARMS 5             /* Write System Parameters */
#define MSG_WRITE_SYS_PARMS_REPLY 6
 
#define MSG_CONFIGURE_PORTS    7          /* Configure Serial Ports */
#define MSG_CONFIG_PORTS_REPLY 8

#define MSG_CONFIGURE_CHANNEL  9          /* Configure Channel */
#define MSG_CONFIG_CHAN_REPLY 10    

#define MSG_TEAR_DOWN_CHANNEL 11          /* Tear Down Channel */
#define MSG_TEAR_DOWN_REPLY   12      

#define MSG_CHAN_STATUS_RQST  13          /* Channel Status Request */
#define MSG_CHAN_STATUS_REPLY 14

#define MSG_CONFIG_CONFERENCE 15          /* Configure Conference */
#define MSG_CONFIG_CONF_REPLY 16    

#define MSG_TEST_MODE  17                 /* Configure/Perform Test Mode */
#define MSG_TEST_REPLY 18

#define MSG_WRITE_NOISE_PARAMS       19   /* Write Noise Suppression Parameters */
#define MSG_WRITE_NOISE_PARAMS_REPLY 20 

#define MSG_READ_NOISE_PARAMS       21    /* Read Noise Suppression Parameters */
#define MSG_READ_NOISE_PARAMS_REPLY 22

#define MSG_READ_AEC_PARMS        23      /* Read AEC Parameters */
#define MSG_READ_AEC_PARMS_REPLY  24  

#define MSG_WRITE_AEC_PARMS       25      /* Write AEC Parameters */
#define MSG_WRITE_AEC_PARMS_REPLY 26 

#define MSG_TONEGEN       27              /* generate a tone */
#define MSG_TONEGEN_REPLY 28

#define MSG_PLAY_RECORD          29       /* voice playback and record */
#define MSG_PLAY_RECORD_RESPONSE 30

#define MSG_RTP_CONFIG           31       /* RTP configuration paraemters */
#define MSG_RTP_CONFIG_REPLY     32

#define MSG_READ_ECAN_STATE        33     /* Read Ecan State */
#define MSG_READ_ECAN_STATE_REPLY  34     

#define MSG_WRITE_ECAN_STATE       35     /* Write Ecan State */
#define MSG_WRITE_ECAN_STATE_REPLY 36     

#define MSG_ALG_CONTROL            37     /* Algorithm Control */
#define MSG_ALG_CONTROL_REPLY      38     

#define MSG_WRITE_TXCID_DATA        39     /* Send CID TX MSG data */
#define MSG_WRITE_TXCID_DATA_REPLY  40     

#define MSG_CONFTONEGEN            41     /* Generate a conference tone */
#define MSG_CONFTONEGEN_REPLY      42     

#define MSG_READ_AGC_POWER         43     /* Read AGC Power */
#define MSG_READ_AGC_POWER_REPLY   44     

#define MSG_RESET_CHAN_STATS       45     /* Reset Channel Statistics */
#define MSG_RESET_CHAN_STATS_REPLY 46     

#define MSG_RESET_SYS_STATS        47     /* Reset System Statistics */
#define MSG_RESET_SYS_STATS_REPLY  48     

#define MSG_READ_MCBSP_STATS       49     /* Read McBSP Statistics */
#define MSG_READ_MCBSP_STATS_REPLY 50     

#define MSG_CONFIG_ARB_DETECTOR       51  /* Configure ARbitrary Detector */
#define MSG_CONFIG_ARB_DETECTOR_REPLY 52    

#define MSG_ACTIVE_ARB_DETECTOR       53  /* Configure ARbitrary Detector */
#define MSG_ACTIVE_ARB_DETECTOR_REPLY 54    

#define MSG_READ_CPU_USAGE            55
#define MSG_READ_CPU_USAGE_REPLY      56

#define MSG_RTP_STATUS_RQST           57
#define MSG_RTP_STATUS_REPLY          58

#define MSG_TONEPKTGEN                59 /* generate a tone pkt*/
#define MSG_TONEPKTGEN_REPLY          60

#define MSG_INRTPTONEPKTGEN           61 /* insert a rtp tone evt pkt*/
#define MSG_INRTPTONEPKTGEN_REPLY     62

#define MSG_SRTP_CONFIG               63     /* SRTP configuration */
#define MSG_SRTP_CONFIG_REPLY         64
#define MSG_SRTPNEWKEY_CONFIG         65     /* SRTP new key */
#define MSG_SRTPNEWKEY_CONFIG_REPLY   66

#define MSG_DTMFDIAL           67    /* dtmf dialing */
#define MSG_DTMFDIAL_REPLY     68

#define MSG_WRITE_DTMFCFG_PARMS       69   /* dtmf configuration */
#define MSG_WRITE_DTMFCFG_PARMS_REPLY 70

#define MSG_WRITE_SPEEX_PARAMS        71
#define MSG_WRITE_SPEEX_PARAMS_REPLY  72

#define MSG_SET_VLAN_TAG              73
#define MSG_SET_VLAN_TAG_REPLY        74

#define MSG_GET_VLAN_TAG              75
#define MSG_GET_VLAN_TAG_REPLY        76

#define MSG_READ_AEC_STATUS           77      // Read AEC Status
#define MSG_READ_AEC_STATUS_REPLY     78

#define MSG_READ_LEC_STATUS           79      // Read LEC Status
#define MSG_READ_LEC_STATUS_REPLY     80  

#define MSG_SAVE_AEC_STATE            81      // Save AEC State
#define MSG_SAVE_AEC_STATE_REPLY      82

#define MSG_RESTORE_AEC_STATE         83      // Restore AEC State
#define MSG_RESTORE_AEC_STATE_REPLY   84

#define MSG_AEC_GAIN_CHANGE           85      // Inform AEC of Gain Change
#define MSG_AEC_GAIN_CHANGE_REPLY     86

#define MSG_READ_MEMTEST_STATUS       87      // read memory test results 
#define MSG_READ_MEMTEST_STATUS_REPLY 88

#define MSG_NEW_WAVEFILE              89      // open a new wavefile
#define MSG_NEW_WAVEFILE_REPLY        90

#define MSG_LOAD_WAVEFILE             91      // load the new wavefile
#define MSG_LOAD_WAVEFILE_REPLY       92

#define MSG_PLAY_WAVEFILE             93      // play a wavefile
#define MSG_PLAY_WAVEFILE_REPLY       94

#define MSG_RTCP_BYE                  95     // send RTCP bye message
#define MSG_RTCP_BYE_REPLY            96

#define MSG_WRITE_RTP_TIMEOUT         97     // write RTP TX Timeout Value
#define MSG_WRITE_RTP_TIMEOUT_REPLY   98

#define MSG_RTP_V6_CONFIG             99       /* RTP IPv6 configuration parameters */
#define MSG_RTP_V6_CONFIG_REPLY       100

#define MSG_CONFIGURE_ED137B_GATE_AUDIO_FEATURE_PARMS               254     /* Enable ED137B Dynamic Feature Parameters */
#define MSG_CONFIGURE_ED137B_GATE_AUDIO_FEATURE_PARMS_REPLY         255

#define MSG_CONFIGURE_SIDETONE       252      // configure sidetone 
#define MSG_CONFIGURE_SIDETONE_REPLY 253

#define MSG_TIME_OUT_REPLY     250   /* Message timed out */

#define MSG_CONFIGURE_ED137B_PARMS                                  248      /* Write ED137B Configuration Parameters */
#define MSG_CONFIGURE_ED137B_PARMS_REPLY                            249

#define MSG_UPDATE_ED137B_PTT_PARMS                                 246      /* Update ED137B PPT Parameters */
#define MSG_UPDATE_ED137B_PTT_PARMS_REPLY                           247

#define MSG_CONFIGURE_ED137B_PERSISTENT_FEATURES_PARMS              244      /* Write ED137B Persistent Feature Parameters */
#define MSG_CONFIGURE_ED137B_PERSISTENT_FEATURES_PARMS_REPLY        245

#define MSG_ED137B_PERSISTENT_FEATURES_UPDATE_PARMS                 242      /* Update ED137B Persistent Feature Parameters */
#define MSG_CONFIGURE_ED137B_PERSISTENT_FEATURES_UPDATE_PARMS_REPLY 243

#define MSG_CONFIGURE_ED137B_NON_PERSISTENT_FEATURES_PARMS          240      /* Write ED137B NON Persistent Feature Parameters */
#define MSG_CONFIGURE_ED137B_NON_PERSISTENT_FEATURES_PARMS_REPLY    241

// C674 interrupt masks for events and frame completions
#define EVENT_INTERRUPT_MASK_674 1
#define FRAME_INTERRUPT_MASK_674 2

typedef struct HostVAD_Params_t {
	short LowThreshdB;	// Threshold below which we declare noise
	short HighThreshdB;	// Threshold above which we declare speech
	short HangMSec;
	short FrameSize;
	short WindowSize;
} HostVAD_Params_t;

typedef struct HostNC_Params_t {
	short MaxAttendB;
} HostNC_Params_t;

typedef struct HostNCAN_Params_t {
	HostNC_Params_t  NC_Params;
	HostVAD_Params_t VAD_Params;
} HostNCAN_Params_t;


// Allow for 13 bit slot ID and 3 bit port ID in following bit format: ssss sppp ssss ssss
#define PackPortandSlot(port,slot) ((ADT_UInt16) (((slot << 3) & 0xf800) | ((port << 8) & 0x0700) | (slot & 0xff)))
#define UnpackSlot(value)  (((value & 0xf800) >> 3) | (value & 0xff))
#define UnpackPort(value)   (((value & 0x0700) == 0x0700) ? 0xff : ((value & 0x0700) >> 8))

#endif  /* prevent multiple inclusion */
