// File Name: main.c
//
// Description:
//   This file contains the main Trilogy host test application.
//

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN	// prevent include of winsock.h
#endif

//#define _CRT_SECURE_NO_WARNINGS

#include <Windows.h>
#include <CommCtrl.h>
#include <tchar.h>
#include "resource.h"
#include <stdio.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include "stdint.h"
typedef int Bool;
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "GpakCustWin32.h"
#include "GpakHpi.h"
#include "GpakApi.h"
#include "GpakEnum.h"
#pragma comment(lib, "ComCtl32.lib")

#define NUM_TEST_CHANS MAX_CHANNELS		// number of test channels
#define PackCore(CoreId,ChanId)   ((ADT_UInt16) ((CoreId)<<8) | ((ChanId) & 0x00ff))

void (*procCustCommand) (char *pUserCmd) = 0;

#define TEST_MODE_NONE   0
#define TEST_MODE_STRESS 1 // continuous setup/teardown

#define PERIODIC_ACTIVE  1
#define PERIODIC_PAUSED  2


typedef struct custState_t {
    int active;
    int perActive;
    int testMode;
    int firstChan;
    int lastChan;
    int chansActive;
    int testCount;
    int minChanCore;
    int maxChanCore;
} custState_t;
custState_t cust = {0,0,0};
uint16_t chanCoreMod[NUM_TEST_CHANS];

extern ADT_UInt16 chanCore[];
extern GpakCodecs chanCodecType[];
extern GpakFrameHalfMS chanFrameSize[];
extern GpakActivation chanVadCngEnable[];
extern GpakActivation chanDtxEnable[];
extern GpakActivation chanLecEnable[];
extern uint16_t chanCryptoSuite[];
extern int chanRtcpEnable[];
extern int dtmfMode[];



extern int uartPrintf(char *format, ...);
extern void setupPcmToPktChannel(
	ADT_UInt16 channelId		// channel Id
	);
extern void setupCustChannel(
	ADT_UInt16 channelId		// channel Id
	);
extern void teardownChannel(
	ADT_UInt16 channelId		// channel Id
	);
extern void stressStatInit(ADT_UInt16 chID);
extern void getChannelStatus(
	ADT_UInt16 channelId,		// channel Id
	Bool rtpRxTxOnly			// flag: only display RTP Rx and Tx pkt counts
	);
extern int stressStatReport(ADT_UInt16 chID);
extern void resetChanParms();

static void startTest(int16_t testID, int32_t perStart, int32_t perStop);

static void displayHelp(void);
static void procAvayaCommand(
	char *pUserCmd				// pointer to user command string
	);
static Bool parseCommand(
	char *pUserInput,			// pointer to user input string
	char *pCmd,					// pointer to command name string to match
	int32_t *pSuffix1,			// pointer to 1st suffix value variable
	int32_t *pSuffix2,			// pointer to 2nd suffix value variable
	char **ppArgument			// pointer to argument pointer variable
	);
 
void initCustomCommand(int minChanCore, int maxChanCore) {
    memset(&cust, 0, sizeof(cust));
    procCustCommand = &procAvayaCommand;
    memset(chanCoreMod, 0, sizeof(chanCoreMod));
    cust.minChanCore = minChanCore;
    cust.maxChanCore = maxChanCore;
}
void setCustMenuState(int onOff) {
    cust.active = onOff;
    if (onOff) {
        uartPrintf("Custom Menu Active\n");
		displayHelp();
    }
    else {
        uartPrintf("Main Menu Active\n");
    }

}

int getCustPeriodicActive() {
    return (cust.perActive == PERIODIC_ACTIVE);

}
static void sutd () {
	int i;
    if (cust.chansActive) {
	    for (i = cust.firstChan; i <= cust.lastChan; i++)
	    {
            getChannelStatus(i,TRUE);
	        teardownChannel((ADT_UInt16) i);
            cust.chansActive--;
	    }
        
    } else {
	    for (i = cust.firstChan; i <= cust.lastChan; i++)
	    {
            // update core assignment for channels with modulus rotation enabled
            if (chanCoreMod[i] != 0) {
                chanCore[i]++;
                if (chanCore[i] >= chanCoreMod[i])
                    chanCore[i] = cust.minChanCore;
            }
		    // setupCustChannel((ADT_UInt16) i);
		    setupPcmToPktChannel((ADT_UInt16) i);
            cust.chansActive++;
	    }
        cust.testCount++;
    }
   uartPrintf("test %u, %u channels active \n", cust.testCount, cust.chansActive);
}

// called periodically from windows timer
void custPeriodicFunc(unsigned int count) {

    if (cust.perActive != PERIODIC_ACTIVE) return;

    switch (cust.testMode) {
        case TEST_MODE_NONE:
        default:
        break;

        case TEST_MODE_STRESS:
        cust.perActive = PERIODIC_PAUSED;
        sutd ();
        cust.perActive = PERIODIC_ACTIVE;
        break;
    }
}

int getCustMenuState() { 
    return (cust.active && procCustCommand); 
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// strncmpi - Case insensitive compare strings of maximum length.
//
// FUNCTION
//  This function performs a case insensitive compare of two strings of maximum
//  length.
//
// RETURNS
//  Zero if the strings are equal or non zero if not equal.
//
static int strncmpi(
	char *pString1,				// pointer to string 1
	char *pString2,				// pointer to string 2
	uint32_t length				// max number of characters to compare
	)
{

	while (length-- > 0)
	{
		if (toupper(*pString1++) != toupper(*pString2++))
		{
			return 1;
		}
	}
	return 0;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// procUserCommand - Process a user command.
//
// FUNCTION
//  This function processes a user command.
//
// RETURNS
//  nothing
//
static void procAvayaCommand(
	char *pUserCmd				// pointer to user command string
	)
{
	int32_t suffix1;			// 1st command suffix value
	int32_t suffix2;			// 2nd command suffix value
	char *pArgument;			// pointer to command argument
	uint32_t i,j, core, chansPerCore;
    uint16_t coreMod;
	int32_t temp32, ii;
    int stat;

	// Check for Help command.
	if ((_strcmpi(pUserCmd, "HELP") == 0) || (_strcmpi(pUserCmd, "?") == 0))
	{
		displayHelp();
		return;
	}

    // retturn to main menu
	if (_strcmpi(pUserCmd, "MM") == 0)
	{
        setCustMenuState(0);
		return;
	}

	// Check for custom Channel Setup command.
	if (parseCommand(pUserCmd, "STRESS", &suffix1, &suffix2, &pArgument))
	{
		if (pArgument != NULL)
		{
            cust.perActive = 0;
            stat = sscanf_s(pArgument, "%u", &temp32);
            if ((stat == 1) && (temp32!=0)) {
                cust.perActive = PERIODIC_PAUSED;
            }            
		}

		if (suffix1 < 0)
		{
			suffix1 = 0;
			suffix2 = NUM_TEST_CHANS - 1;
		}
        cust.firstChan = suffix1;
        cust.lastChan = suffix2;
        cust.chansActive = 0;
        cust.testCount = 0;

		if (suffix1 < suffix2)
		{
            cust.firstChan = suffix1;
            cust.lastChan = suffix2;
		}
		else
		{
            cust.firstChan = suffix1;
            cust.lastChan = suffix1;
		}

        for (ii=cust.firstChan; ii<=cust.lastChan; ii++)
            stressStatInit(ii);

        sutd ();
        if (cust.perActive == PERIODIC_PAUSED)
            cust.perActive = PERIODIC_ACTIVE;
        cust.testMode = TEST_MODE_STRESS;
		return;
	}

    // Check for periodic enable
	if (parseCommand(pUserCmd, "PER", &suffix1, &suffix2, &pArgument))
	{
		if (pArgument == NULL)
		{
			uartPrintf("periodic function active = %d\n", cust.perActive);
            return;
		}

        stat = sscanf_s(pArgument, "%u", &temp32);
        if (stat != 1) {
			uartPrintf("periodic function active = %d\n", cust.perActive);
            return;
        }

        cust.perActive = (temp32 != 0);
        return;
    }

    
	if (parseCommand(pUserCmd, "SUTD", &suffix1, &suffix2, &pArgument))
    {
        sutd ();
		return;
    }

	if (parseCommand(pUserCmd, "STAT", &suffix1, &suffix2, &pArgument))
    {
        int fail = 0;
        for (ii=cust.firstChan; ii<=cust.lastChan; ii++)
            fail += stressStatReport(ii);

        if (!fail) {
        	uartPrintf("stress test success: %u iterations for channels %u-%u\n", cust.testCount, cust.firstChan,cust.lastChan);
        }
		return;
    }
    // enable/disable rotatig core assignment modulus N
	if (parseCommand(pUserCmd, "ROT", &suffix1, &suffix2, &pArgument))
	{        
		if (pArgument != NULL)
		{
            stat = sscanf_s(pArgument, "%u", &temp32);
            coreMod = (uint16_t)temp32;
            if ((stat == 1) && (coreMod > (uint16_t)cust.maxChanCore))  {
                uartPrintf("invalid core Modulus\n");
                return;  
            }            
		}                
                 
		if (suffix1 < 0) {
			suffix1 = 0;
			suffix2 = NUM_TEST_CHANS - 1;            
		}

		if (suffix1 > suffix2) {    
            suffix2 = suffix1;         
		}

        if (pArgument == NULL) {
             for (i=0; i<NUM_TEST_CHANS; i++)
                uartPrintf("chanCoreMod[%d] = %d\n", i, chanCoreMod[i]);
        } else {
            for (ii=suffix1; ii<=suffix2; ii++)
                chanCoreMod[ii] = coreMod;
        }
    
		return;
	}

	if (parseCommand(pUserCmd, "TEST", &suffix1, &suffix2, &pArgument))
	{
		if (pArgument == NULL)
		{
			uartPrintf("Invalid test command!\n");
            return;
		}

        stat = sscanf_s(pArgument, "%u", &temp32);
        if (stat != 1) {
			uartPrintf("Invalid testID!\n");
            return;
        }

		if (suffix2 < 0) {
	        suffix2 = suffix1;
		}
        startTest((int16_t)temp32, suffix1, suffix2);        
		return;
	}

	// Check for Channel Teardown command.
	if (parseCommand(pUserCmd, "TD", &suffix1, &suffix2, &pArgument))
	{
		if (pArgument != NULL)
		{
			uartPrintf("Invalid status command!\n");
		}
		else
		{
            cust.perActive = 0;

			if (suffix1 < 0)
			{
				suffix1 = 0;
				suffix2 = NUM_TEST_CHANS - 1;
			}
			if (suffix1 < suffix2)
			{
				for (i = suffix1; i <= suffix2; i++)
				{
					teardownChannel((ADT_UInt16) i);
				}
			}
			else
			{
				teardownChannel((ADT_UInt16) suffix1);
			}
		}
		return;
	}

	if (parseCommand(pUserCmd, "CS", &suffix1, &suffix2, &pArgument))
	{
		if (pArgument != NULL)
		{
			uartPrintf("Invalid status command!\n");
		}
		else
		{
			if (suffix1 < 0)
			{
				suffix1 = 0;
				suffix2 = NUM_TEST_CHANS - 1;
			}
			if (suffix1 < suffix2)
			{
				for (i = suffix1; i <= suffix2; i++)
				{
					getChannelStatus((ADT_UInt16) i, FALSE);
				}
			}
			else
			{
				getChannelStatus((ADT_UInt16) suffix1, FALSE);
			}
		}
		return;
	}

	// Indicate an invalid or unknown command.
	uartPrintf("Invalid command!\n");

	return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// parseCommand - Parse user input for a command.
//
// FUNCTION
//  This function parses the user input for a command according to general
//  syntax as shown below.
//
//    cmd          - command without numeric suffix and without argument
//    cmd#         - command with numeric suffix and without argument
//    cmd#-#       - command with numeric range suffix and without argument
//    cmd=value    - command without numeric suffix with argument
//    cmd#=value   - command with numeric suffix with argument
//    cmd#-#=value - command with numeric range suffix with argument
//
//  Note: The equal sign may be preceeded and followed by spaces.
//
// RETURNS
//  TRUE if the command is valid or FALSE if invalid.
//  *pSuffix1 = the 1st command suffix value (-1 if no suffix)
//  *pSuffix2 = the 2nd command suffix value (same as 1st suffix if missing)
//  *ppArgument = pointer to argument (NULL if no argument)
//
static Bool parseCommand(
	char *pUserInput,			// pointer to user input string
	char *pCmd,					// pointer to command name string to match
	int32_t *pSuffix1,			// pointer to 1st suffix value variable
	int32_t *pSuffix2,			// pointer to 2nd suffix value variable
	char **ppArgument			// pointer to argument pointer variable
	)
{
	uint32_t temp32;			// temporary 32 bit unsigned value

	// Initialize return values to null.
	*pSuffix1 = -1;
	*pSuffix2 = -1;
	*ppArgument = NULL;

	// Compare the command name.
	while (*pCmd != (char)NULL)
	{
		if (toupper(*pCmd++) != toupper(*pUserInput++))
		{
			return FALSE;
		}
	}

	// Check for the optional numeric suffix.
	if ((*pUserInput >= '0') && (*pUserInput <= '9'))
	{
		temp32 = (uint32_t) (*pUserInput++ - '0');
		while ((*pUserInput >= '0') && (*pUserInput <= '9'))
		{
			temp32 = (temp32 * 10) + ((uint32_t) (*pUserInput++ - '0'));
			if (temp32 >= 0x80000000)
			{
				return FALSE;
			}
		}
		*pSuffix1 = (int32_t) temp32;
		*pSuffix2 = *pSuffix1;

		// Check for optional suffix range.
		if ((pUserInput[0] == '-') &&
			(pUserInput[1] >= '0') && (pUserInput[1] <= '9'))
		{
			pUserInput++;
			temp32 = 0;
			while ((*pUserInput >= '0') && (*pUserInput <= '9'))
			{
				temp32 = (temp32 * 10) + ((uint32_t) (*pUserInput++ - '0'));
				if (temp32 >= 0x80000000)
				{
					return FALSE;
				}
			}
			*pSuffix2 = (int32_t) temp32;
		}
	}

	// Skip space characters.
	while (*pUserInput == ' ')
	{
		pUserInput++;
	}

	// Check for the optional equal sign.
	if (*pUserInput == '=')
	{

		// Skip trailing space characters.
		pUserInput++;
		while (*pUserInput == ' ')
		{
			pUserInput++;
		}
		*ppArgument = pUserInput;
	}
	else if (*pUserInput != '\0')
	{
		return FALSE;
	}

	// Return with an indication the command is valid.
	return TRUE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// displayHelp - Display command help information.
//
// FUNCTION
//  This function displays command help information.
//
// RETURNS
//  nothing
//
static void displayHelp(void)
{

	uartPrintf("Custom Commands:\n");
	uartPrintf(" HELP or ? - Display this list of commands\n");
    uartPrintf(" MM - return to MainMenu\n");
    uartPrintf(" STRESS#-#=<per0,1> - stress test for #channels,0/1 enables periodic teardown,setup\n");
    uartPrintf(" PER=<0,1> - enable/disable periodic\n");
    uartPrintf(" SUTD      - setup/teardown #channels specified by stress command\n");
    uartPrintf(" STAT      - check stress stats for failures\n");
    uartPrintf(" ROT#-#=<N> - rotate core modulus N for #channels each time channel is setup\n");
    uartPrintf(" TEST#-#=<ID>  - run test ID... channels#-# are periodic\n");
    uartPrintf(" TD#-#      - turn off periodic and teardown\n");
    uartPrintf(" CS#-#      - channel status\n");
    uartPrintf(" MF#        - multi-frame test #==num chans per frame\n");
	return;
}

typedef struct range_t {
    int16_t start;      // first chan to enable (-1 == none enabled)
    int16_t stop;       // last chan to enable
} range_t;

typedef struct codec_t {
    GpakCodecs      enc;
    GpakFrameHalfMS fs;
} codec_t;


typedef struct testParms_t {
    int16_t     id;     // -1 == null test
    codec_t     codec;
    range_t     perOff;   // #channels with periodic off... they stay on entire duration
    range_t     vadcng;
    range_t     dtx;
    range_t     relay;
    range_t     lec;
    range_t     crypto;
    range_t     rtcp;
} testParms_t;

testParms_t testParms[] = {
// ID    CODEC     FS             PEROff   VADCNG      DTX      RELAY       LEC         CRYPTO    RTCP
{  0,    {PCMU_64, Frame_20ms},   {0,0},   {-1,0},     {-1,0},  {-1,0},     {-1,0},     {-1,0},   {-1,0}},
{  1,    {PCMU_64, Frame_20ms},   {0,0},   {0,159},    {-1,0},  {0,159},    {1,159},    {-1,0},   {-1,0}},
{  2,    {PCMU_64, Frame_20ms},   {0,0},   {0,159},    {-1,0},  {0,159},    {1,159},    {0, 1},   {-1,0}},
{  3,    {G729,    Frame_20ms},   {0,0},   {-1,0},     {-1,0},  {0,159},    {1,159},    {-1,0},   {-1,0}},
{  4,    {G729,    Frame_20ms},   {0,0},   {-1,0},     {-1,0},  {0,159},    {1,159},    {0, 1},   {-1,0}},
{  5,    {G729AB,  Frame_20ms},   {0,0},   {-1,0},     {-1,0},  {0,159},    {1,159},    {0, 1},   {-1,0}},

// Null test 
{  -1}};  
#define CLAMP(a,b) if (a >= (int16_t)b) a = (int16_t)(b-1);

static void startTest(int16_t testID, int32_t perStart, int32_t perStop) {
testParms_t local_test;
testParms_t *test = &local_test;
int found = 0;
int16_t i;

    i = 0;
    while (testParms[i].id >= 0) 
    {
        if (testParms[i].id == testID) {
            found = 1;
            memcpy(test, &testParms[i], sizeof(testParms_t));
            break;
        }
        i++;
    }

    if (!found) {
        uartPrintf("Invalid testID\n");
        return;
    }

    resetChanParms();

	for (i=0; i<NUM_TEST_CHANS; i++) {
		chanCodecType[i] = test->codec.enc;
		chanFrameSize[i] = test->codec.fs;
    }

    if (test->vadcng.start != -1) {
        CLAMP(test->vadcng.stop,NUM_TEST_CHANS);
        for (i=test->vadcng.start; i<=test->vadcng.stop; i++) {
            chanVadCngEnable[i] = Enabled;
        }
    }    

    if (test->dtx.start != -1) {
        CLAMP(test->dtx.stop,NUM_TEST_CHANS);
        for (i=test->dtx.start; i<=test->dtx.stop; i++) {
            chanDtxEnable[i] = Enabled;
        }
    }    

    if (test->relay.start != -1) {
        CLAMP(test->relay.stop,NUM_TEST_CHANS);
        for (i=test->relay.start; i<=test->relay.stop; i++) {
            dtmfMode[i] = 2;
        }
    }    

    if (test->lec.start != -1) {
        CLAMP(test->lec.stop,NUM_TEST_CHANS);
        for (i=test->lec.start; i<=test->lec.stop; i++) {
            chanLecEnable[i] = Enabled;
        }
    }    

    if (test->crypto.start != -1) {
        CLAMP(test->crypto.stop,NUM_TEST_CHANS);
        for (i=test->crypto.start; i<=test->crypto.stop; i++) {
            chanCryptoSuite[i] = 1;
        }
    }   

    if (test->rtcp.start != -1) {
        CLAMP(test->rtcp.stop,NUM_TEST_CHANS);
        for (i=test->rtcp.start; i<=test->rtcp.stop; i++) {
            chanRtcpEnable[i] = Enabled;
        }
    }

    // setup tests with periodic start-stop disabled
    cust.perActive = 0;
    if (test->perOff.start != -1) {
        CLAMP(test->perOff.stop,NUM_TEST_CHANS);
        for (i=test->perOff.start; i<=test->perOff.stop; i++) {
		    //setupCustChannel((ADT_UInt16) i);
		    setupPcmToPktChannel((ADT_UInt16) i);
        }
    } 

    // setup tests with periodic start-stop enabled
    if (perStart != -1) {
        cust.perActive = PERIODIC_PAUSED;
        cust.chansActive = 0;
        cust.testCount = 0;
        cust.firstChan = (int16_t)perStart;
        cust.lastChan  = (int16_t)perStop;
        for (i=cust.firstChan; i<=cust.lastChan; i++) {
            stressStatInit(i);
        }
        sutd ();
        if (cust.perActive == PERIODIC_PAUSED)
            cust.perActive = PERIODIC_ACTIVE;
        cust.testMode = TEST_MODE_STRESS;
    }

}


