/*
 * Copyright (c) 2009, Adaptive Digital Technologies, Inc.
 *
 * File Name: GpakCustWin32.c
 *
 * Description:
 *   This file contains host system dependent functions to support generic
 *   G.PAK API functions on the TI EVM6424 DSP. The file is integrated into 
 *   the host processor  connected to G.PAK DSPs via a Host Port Interface.
 *
 *   Note: This file needs to be modified by the G.PAK system integrator.
 *
 * Version: 1.0
 *
 * Revision History:
 *   10/17/08 - Initial release.
 *
 */

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN	// prevent include of winsock.h
#endif

#include <Windows.h>
#include <CommCtrl.h>
#include <tchar.h>
#include "resource.h"
#include <stdio.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include "stdint.h"
typedef int Bool;
#include "GpakCustWin32.h"
#include "GpakApi.h"

extern int uartPrintf(char *format, ...);
//#define ADT_printf uartPrintf
#define ADT_printf

unsigned int HostBigEndian = 0;

//=============================================================================
//
//  Support for GPAK APIs
//
//==============================================================================

unsigned int MaxCoresPerDSP   = CORES_PER_DSP;
unsigned int MaxDsps          = MAX_NUM_DSPS;
unsigned int MaxChannelAlloc  = MAX_CHANNELS;
unsigned int MaxWaitLoops     = MAX_WAIT_LOOPS;
unsigned int DownloadI8       = DOWNLOAD_BLOCK_I8;

unsigned int ReadErrCnt  = 0;
unsigned int WriteErrCnt = 0;

/* DSP Related arrays */
DSP_Address  ifBlkAddress = DSP_IFBLK_ADDRESS;                // Interface block address

GPAK_DspCommStat_t DSPError[MAX_NUM_DSPS];

DSP_Word    MaxChannels[MAX_NUM_DSPS];  /* max num channels */
DSP_Word    MaxCmdMsgLen[MAX_NUM_DSPS];  /* max Cmd msg length (octets) */

DSP_Address pDspIfBlk[MAX_NUM_DSPS];          /* DSP address of interface blocks */
DSP_Address pEventFifoAddress[MAX_NUM_DSPS];  /* Event circular buffers */


DSP_Address pPktInBufr [MAX_NUM_DSPS][MAX_CHANNELS];     /* Pkt In circular buffers */
DSP_Address pPktOutBufr[MAX_NUM_DSPS][MAX_CHANNELS];     /* Pkt Out circular buffers */

/* Download buffers */
ADT_UInt32  DlByteBufr_[(DOWNLOAD_BLOCK_I8 + sizeof(ADT_UInt8))/sizeof(ADT_UInt8)];      /* Dowload byte buf */
DSP_Word   DlWordBufr [(DOWNLOAD_BLOCK_I8 + sizeof(DSP_Word))/sizeof(DSP_Word)];      /* Dowload word buffer */

#ifdef NETWORK_MESSAGING  // Network variables
int ipver;
void *invalid_socket = (void *) INVALID_SOCKET;   // System definition of invalid socket handle
ADT_UInt32 LocalIP;                    // IP Address of host
ADT_UInt32 RemoteIP [MAX_NUM_DSPS];   // IP Addresses of DSPs
IP6N LocalIP6;                // IPv6 Address of host
IP6N RemoteIP6 [MAX_NUM_DSPS];  // IPv6 Addresses of DSPs
void *msgSkt[MAX_NUM_DSPS];           // DSPs TCP Messaging sockets
void *evtSkt[MAX_NUM_DSPS];           // DSPs TCP event sockets
char gpakVerifyTag[MAX_NUM_DSPS][GPAK_TAG_I8];   // DSPs handshaking verification tags
#ifdef UDP_MESSAGING
char gpakDisconnectTag[MAX_NUM_DSPS][GPAK_TAG_I8];   // DSPs handshaking verification tags
#endif
unsigned short msg_tcp_port = (unsigned short) 56765;
unsigned short evt_tcp_port = (unsigned short) 56766;

ADT_UInt32 drain[400];
extern void IPv6IPAddressToString (IP6N address, char *strIPAddress);

#endif


DSP_Address *getPktInBufrAddr  (int DspID, int channel) {
   return &pPktInBufr[DspID][channel];
}
DSP_Address *getPktOutBufrAddr (int DspID, int channel) {
   return &pPktOutBufr[DspID][channel];
}

void initGpakStructures () {
#ifdef NETWORK_MESSAGING
   int i;
   memset (gpakVerifyTag, 0, sizeof (gpakVerifyTag));
   for (i=0; i<MAX_NUM_DSPS; i++) {
      msgSkt[i] = evtSkt[i] = (void *) INVALID_SOCKET;
   }
   ipver = 4;
#endif
   memset (DSPError,     0, sizeof (DSPError));
   memset (MaxChannels,  0, sizeof (MaxChannels));
   memset (MaxCmdMsgLen, 0, sizeof (MaxCmdMsgLen));
   memset (pDspIfBlk,    0, sizeof (pDspIfBlk));
   memset (pPktInBufr,   0, sizeof (pPktInBufr));
   memset (pPktOutBufr,  0, sizeof (pPktOutBufr));
   memset (pEventFifoAddress, 0, sizeof (pEventFifoAddress));

}
#define MSG_BUFFER_I16 (MSG_BUFFER_SIZE * 2)
#define HI_16(i32) ((ADT_UInt16) (((i32) >> 16) & 0xffff))
#define LO_16(i32) ((ADT_UInt16)  ((i32) & 0xffff))

void gpakReadDspMemory32 (ADT_UInt32 DspId, DSP_Address DspAddress, ADT_UInt32 I32Cnt, ADT_UInt32 *Buff) {
   gpakTestStatus_t apiStat;
   GPAK_TestStat_t  tstStat;
   ADT_UInt16       tstParam[5];
   ADT_UInt16*      I16Buff;
   ADT_UInt32       I16Cnt, I16Rply;

   I16Buff = (ADT_UInt16*) Buff;
   I16Cnt  = I32Cnt * 2;

   tstParam [0] = 1;  // Message ID
   tstParam [1] = 5;  // CMD Size

   while (I16Cnt) {
      I16Rply = I16Cnt;
      if (MSG_BUFFER_I16 - 4 < I16Rply) I16Rply = MSG_BUFFER_I16 - 4;
      tstParam [2] = (ADT_UInt16) I16Rply;             // REPLY SIZE
      tstParam [3] = HI_16 (DspAddress);
      tstParam [4] = LO_16 (DspAddress);

      apiStat = gpakTestMode (DspId, CustomVarLen, (GpakTestData_t*) &tstParam, I16Buff, &tstStat);

      DspAddress += (I16Rply * 2);
      I16Buff    += I16Rply;
      I16Cnt     -= I16Rply;

      if (apiStat == GpakApiSuccess) continue;
      ADT_printf ("Read32 failure\n");
   }
   return;
}

void gpakWriteDspMemory32 (ADT_UInt32 DspId, DSP_Address DspAddress, ADT_UInt32 I32Cnt, ADT_UInt32 *Buff) {
   ADT_printf ("Write32 unimplemented %x [%d]\n", DspAddress, I32Cnt);
   return;
}

void gpakReadDspNoSwap32 (ADT_UInt32 DspId, DSP_Address DspAddress, ADT_UInt32 I32Cnt, ADT_UInt32 *Buff) {
    gpakReadDspMemory32 (DspId, DspAddress, I32Cnt, Buff);
}

void gpakWriteDspNoSwap32 (ADT_UInt32 DspId, DSP_Address DspAddress, ADT_UInt32 I32Cnt,  ADT_UInt32 *Buff) {
    gpakWriteDspMemory32 (DspId, DspAddress, I32Cnt,  Buff);
}
 
void gpakReadDspMemory16  (ADT_UInt32 DspId, DSP_Address DspAddress, ADT_UInt32 I16Cnt, ADT_UInt16 *Buff) {
   ADT_printf ("Read16 unimplemented %x [%d]\n", DspAddress, I16Cnt);
   return ;
}
void gpakWriteDspMemory16 (ADT_UInt32 DspId, DSP_Address DspAddress, ADT_UInt32 I16Cnt,  ADT_UInt16 *Buff) {
   ADT_printf ("Write16 unimplemented  %x [%d]\n",  DspAddress, I16Cnt);
   return;
}

void gpakReadDspMemory8  (ADT_UInt32 DspId, DSP_Address DspAddress, ADT_UInt32 I8Cnt, ADT_UInt8 *Buff) {
   ADT_printf ("Read8 unimplemented %x [%d]\n", DspAddress, I8Cnt);
   return ;

}
void gpakWriteDspMemory8 (ADT_UInt32 DspId, DSP_Address DspAddress, ADT_UInt32 I8Cnt,  ADT_UInt8 *Buff) {
   ADT_printf ("Write8 unimplemented  %x [%d]\n",  DspAddress, I8Cnt);
   return;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakHostDelay - Delay for a fixed time interval.
 *
 * FUNCTION
 *  Delays for a fixed time interval before returning. The time
 *  interval is the Host Port Interface sampling period when polling a DSP for
 *  replies to command messages.
 *
 * RETURNS
 *  nothing
 *
 */
void gpakHostDelay (void) {

/* This function needs to be implemented by the G.PAK system integrator. */
   Sleep (10);
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakLockAccess - Lock access to the specified DSP.
 *
 * FUNCTION
 *  Aquires exclusive access to the specified DSP.
 *
 * RETURNS
 *  nothing
 *
 */
void gpakLockAccess (ADT_UInt32 DspId) {

/* This function needs to be implemented by the G.PAK system integrator. */

}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakUnlockAccess - Unlock access to the specified DSP.
 *
 * FUNCTION
 *  Releases exclusive access to the specified DSP.
 *
 * RETURNS
 *  nothing
 *
 */
void gpakUnlockAccess (ADT_UInt32 DspId) {
   
/* This function needs to be implemented by the G.PAK system integrator. */

}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadFile - Read a block of bytes from a G.PAK Download file.
 *
 * FUNCTION
 *  Reads a contiguous block of bytes from a G.PAK Download file
 *  starting at the current file position.
 *
 * Inputs
 *   FileId -  Download file identifier
 *   pBuffer - Byte storage buffer
 *   ByteCnt     - Number of bytes to transfer
 *
 * RETURNS
 *  The number of bytes read from the file.
 *   -1 indicates an error occurred.
 *    0 indicates all bytes have been read (end of file)
 *
 */
int gpakReadFile (GPAK_FILE_ID FileId, ADT_UInt8 *pBuffer, ADT_UInt32 ByteCnt) {

    return fread (pBuffer, sizeof(char), ByteCnt, (FILE *) FileId);
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakDspStartup - Startup the G.PAK DSP.
 *
 * FUNCTION
 *  This function loads and starts execution of the G.PAK DSP and prepares for
 *  Host-DSP communication.
 *
 * RETURNS
 *  An indication of success or failure.
 *   -1 indicates an error occurred
 *    0 indicates success
 *
 */
int gpakDspStartup (ADT_UInt32 DspId, char *pCoffFileName) {

   return 0;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakDspShutdown - Shutdown the G.PAK DSP.
 *
 * FUNCTION
 *  This function stops execution of the G.PAK DSP and terminates Host-DSP
 *  communication.
 *
 * RETURNS
 *  An indication of success or failure.
 *   -1 indicates an error occurred
 *    0 indicates success
 *
 */
int gpakDspShutdown (ADT_UInt32 DspId) {

   return 0;
}
//  Customer supplied routines for TCP/IP communications with the DSP
//{
// Initialize the network stack
//
// Output parameters:
//      LocalIP -  Local IP address used to communicate with DSPs in network byte order
//
// Returns:
//      0.             Success
//     Negative value. Error value.
//}
int gpakNetworkStackInit (ADT_UInt32 *LocalIP) {

   return 0;
}

//{
// Open a TCP socket
//
//  Input Parameters:
//     DspIP    - Dsp's IP address
//     tcpPort  - Dsp's TCP port address
//
//  Returns:
//     NULL - open failure
//     Non-null - Socket handle cast as void pointer
//
//}
void *gpakOpenTCPSocket (ADT_UInt32 DspIP, ADT_UInt16 tcpPort) {
   SOCKET skt;
   struct sockaddr_in RemoteAddr;
   int rslt;

   //-------------------------------------------------
   //  Open TCP socket for msg stream
   skt = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
   if (skt == INVALID_SOCKET) {
      ADT_printf ("Unable to open TCP socket DSP %x:%d. Error %d\n", 
               DspIP, tcpPort, WSAGetLastError());
      return NULL;
   }

   //-------------------------------------------------
   // Issue connect to DSP's messaging socket
   RemoteAddr.sin_family = AF_INET;
   RemoteAddr.sin_addr.s_addr  = htonl (DspIP);
   RemoteAddr.sin_port   = htons (tcpPort);
   rslt = connect (skt, (struct sockaddr *) &RemoteAddr, sizeof (RemoteAddr));
   if (rslt != 0) {
      rslt = WSAGetLastError();
      if (rslt == WSAEISCONN) return NULL;  // Already connected
      ADT_printf ("G.PAK server connect error %d, %08x:%d\n", WSAGetLastError(), DspIP, tcpPort);
      return NULL;
   }
   return (void *) skt;
}
void *gpakOpenTCPSocket6 (IP6N DspIP, ADT_UInt16 tcpPort) {
   SOCKET skt;
   struct sockaddr_in6 RemoteAddr;
   int rslt;
   char ipString[46];
   //-------------------------------------------------
   //  Open TCP socket for msg stream
   skt = socket (AF_INET6, SOCK_STREAM, IPPROTO_TCP);
   if (skt == INVALID_SOCKET) {
      ADT_printf ("Unable to open TCP socket DSP %x:%d. Error %d\n", 
               DspIP, tcpPort, WSAGetLastError());
      return NULL;
   }

   //-------------------------------------------------
   // Issue connect to DSP's messaging socket
   memset(&RemoteAddr,0, sizeof(RemoteAddr));
   RemoteAddr.sin6_family = AF_INET6;
   memcpy(&RemoteAddr.sin6_addr.u.Byte, DspIP.u.addr8, 16);
   RemoteAddr.sin6_port   = htons (tcpPort);
   rslt = connect (skt, (struct sockaddr *) &RemoteAddr, sizeof (RemoteAddr));
   if (rslt != 0) {
      rslt = WSAGetLastError();
      if (rslt == WSAEISCONN) return NULL;  // Already connected
        IPv6IPAddressToString (DspIP, ipString);

      ADT_printf ("G.PAK server connect error %d, %s:%d\n", WSAGetLastError(), ipString, tcpPort);
      return NULL;
   }
   return (void *) skt;
}

//{
// Open a UDP socket
//
//  Input Parameters:
//     DspIP    - Dsp's IP address
//     udpPort  - Dsp's UDP port address
//
//  Returns:
//     NULL - open failure
//     Non-null - Socket handle cast as void pointer
//
//}
void *gpakOpenUDPSocket (ADT_UInt32 DspIP, ADT_UInt16 udpPort) {
   SOCKET skt;
   struct sockaddr_in LocalAddr;
   int rslt;

   //-------------------------------------------------
   //  Open UDP socket for msg stream
   skt = socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP);
   if (skt == INVALID_SOCKET) {
      ADT_printf ("Unable to open UDP socket DSP %x:%d. Error %d\n", 
               DspIP, udpPort, WSAGetLastError());
      return NULL;
   }

   LocalAddr.sin_family = AF_INET;
   LocalAddr.sin_addr.s_addr  = htonl (LocalIP);
   LocalAddr.sin_port         = htons (udpPort);
   rslt = bind (skt, (struct sockaddr *) &LocalAddr, sizeof (LocalAddr));
   if (rslt != 0) {
      rslt = WSAGetLastError();
      ADT_printf ("G.PAK server bind error %d, %08x:%d\n", WSAGetLastError(), DspIP, udpPort);
      return NULL;
   }
   ADT_printf ("UDP message IPv4 socket bound to: %08x:%d\n", LocalIP, udpPort);

   return (void *) skt;
}

void *gpakOpenUDPSocket6 (IP6N DspIP, ADT_UInt16 udpPort) {
   SOCKET skt;
   struct sockaddr_in6 LocalAddr;
   int rslt;
   char ipString[46];
   //-------------------------------------------------
   //  Open UDP socket for msg stream
   skt = socket (AF_INET6, SOCK_DGRAM, IPPROTO_UDP);
   if (skt == INVALID_SOCKET) {
      ADT_printf ("Unable to open IPv6 UDPP socket DSP %x:%d. Error %d\n", 
               DspIP, udpPort, WSAGetLastError());
      return NULL;
   }

   memset(&LocalAddr, 0, sizeof(LocalAddr));
   LocalAddr.sin6_family = PF_INET6;
   LocalAddr.sin6_port   = htons (udpPort);
   rslt = bind (skt, (struct sockaddr *)&LocalAddr, sizeof (LocalAddr));
   if (rslt != 0) {
      rslt = WSAGetLastError();
      IPv6IPAddressToString (DspIP, ipString);
      ADT_printf ("G.PAK server bind error %d, %s:%d\n", WSAGetLastError(), ipString, udpPort);
      return NULL;
   }
   ADT_printf ("G.PAK message IPv6 socket bound to: %s:%d\n", ipString, udpPort);
   return (void *) skt;
}

//{
// Close a TCP socket
//
//  Input Parameters:
//     skt   - Socket handle cast as void pointer
//}
void gpakCloseTCPSocket (void *skt) {

	// Verify socket is valid.
	if (((SOCKET)skt == INVALID_SOCKET) || (skt == NULL))
	{
		return;
	}

	shutdown ((SOCKET) skt, SD_BOTH);
	closesocket ((SOCKET) skt);
}

int gpakUnblockTCPSocket (void *skt) {
   unsigned long unblock;
   int rslt;

	// Verify socket is valid.
	if (((SOCKET)skt == INVALID_SOCKET) || (skt == NULL))
	{
		return -1;
	}

   unblock = TRUE;

   rslt = ioctlsocket ((SOCKET) skt, FIONBIO, &unblock);
   if (rslt == 0) return 0;
   
   return -1 * WSAGetLastError();
}

//{
// Send command message to DSP via TCP socket.
//
//  Input Parameters:
//     skt   - Socket handle cast as void pointer
//     msgBuf   - Pointer to message buffer to be transmitted.  
//     msgBufI8 - Number of bytes to transmit
//
//  Returns:
//     Positive value. Number of bytes transmitted
//     Negative value. Error value.
//
//  NOTE: This routine should return only after message has been 
//        successfully sent or an error has been detected on the socket
//}
int gpakSendDspMsg (ADT_UInt32 DspId, void *skt, void *msgBuf, int msgBufI8) {
   int rslt;

	// Verify socket is valid.
#ifdef UDP_MESSAGING
    struct sockaddr_in RemoteAddr;

	if (((SOCKET)skt == INVALID_SOCKET) || (skt == NULL))
	{
		return -1;
	}

    memset(&RemoteAddr, 0, sizeof(RemoteAddr));
    RemoteAddr.sin_family = AF_INET;
    RemoteAddr.sin_port   = htons(msg_tcp_port); 
    RemoteAddr.sin_addr.s_addr = htonl(RemoteIP[DspId]);
    rslt = sendto ((SOCKET)skt, (char *)msgBuf, msgBufI8, 0, (struct sockaddr*) &RemoteAddr, sizeof (RemoteAddr));
    if (rslt < 0) rslt = -1 * WSAGetLastError();
    return rslt;
#else
	if (((SOCKET)skt == INVALID_SOCKET) || (skt == NULL))
	{
		return -1;
	}

   rslt = send ((SOCKET) skt, (char *) msgBuf, msgBufI8, 0);
   if (rslt < 0) rslt = -1 * WSAGetLastError();
   return rslt;
#endif
}

// Send UDP Event pseudo 'connect or disconnect' message
int gpakSendDspConnectEvtUdp (ADT_UInt32 DspId, void *skt, void *msgBuf, int msgBufI8) {
   int rslt;

	// Verify socket is valid.
    struct sockaddr_in RemoteAddr;

	if (((SOCKET)skt == INVALID_SOCKET) || (skt == NULL))
	{
		return -1;
	}

    memset(&RemoteAddr, 0, sizeof(RemoteAddr));
    RemoteAddr.sin_family = AF_INET;
    RemoteAddr.sin_port   = htons(evt_tcp_port); 
    RemoteAddr.sin_addr.s_addr = htonl(RemoteIP[DspId]);
    rslt = sendto ((SOCKET)skt, (char *)msgBuf, msgBufI8, 0, (struct sockaddr*) &RemoteAddr, sizeof (RemoteAddr));
    if (rslt < 0) rslt = -1 * WSAGetLastError();
    return rslt;
}

//{
// Receive command reply from DSP via TCP socket
//  Input Parameters:
//     skt   - Socket handle cast as void pointerID.
//     msgBuf   - Pointer to message buffer to be transmitted.  
//     msgBufI8 - Number of bytes to transmit
//
//  Returns:
//     0.              No reply
//     Positive value. Number of bytes received.
//     Negative value. Error value.
//
//}
int gpakRecvDspMsg (ADT_UInt32 DspId, void *skt, void *msgBuf, int msgBufI8) {

   int rcvI8;

#ifdef UDP_MESSAGING
    struct sockaddr_in RecvAddr;
    int param;
	fd_set read_fds;
    int maxfd, activity;
    struct timeval select_timeout;
    int replyCount=1000;

	// Verify socket is valid.
	if (((SOCKET)skt == INVALID_SOCKET) || (skt == NULL))
	{
		return -1;
	}

    select_timeout.tv_sec = 0;
    select_timeout.tv_usec = 100;
    while (replyCount) {
        replyCount--;

        FD_ZERO(&read_fds);
        maxfd = -1;
        FD_SET((SOCKET)skt, &read_fds);
        if ((int)skt > maxfd)    
            maxfd = (int)skt;

        // wait for activity on a socket
        activity = select(maxfd+1, &read_fds, NULL, NULL, &select_timeout);
        if (activity == -1) {
            printf("select error\n");
            continue;
        } 
        else if (activity > 0) 
        {
            if (FD_ISSET((SOCKET)skt, &read_fds) == 0)
                continue;
            memset(&RecvAddr, 0, sizeof(RecvAddr));
            RecvAddr.sin_family = AF_INET;
            param = sizeof(RecvAddr);
            rcvI8 = recvfrom ((SOCKET)skt, (char *)msgBuf, msgBufI8, 0, (struct sockaddr*) &RecvAddr, &param);
            if (0 <= rcvI8) return rcvI8;

            if (WSAGetLastError() == WSAEWOULDBLOCK) return 0;
            return -1 * WSAGetLastError();
        }
    }
    printf("No UDP receive reply\n");
    return 0;
#else
	if (((SOCKET)skt == INVALID_SOCKET) || (skt == NULL))
	{
		return -1;
	}

   rcvI8 = recv ((SOCKET) skt, msgBuf, msgBufI8, 0);
   if (0 <= rcvI8) return rcvI8;

   if (WSAGetLastError() == WSAEWOULDBLOCK) return 0;
   return -1 * WSAGetLastError();
#endif
}

