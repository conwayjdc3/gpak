/*
 * Copyright (c) 2001 - 2007, Adaptive Digital Technologies, Inc.
 *
 * File Name: GpakApi.h
 *
 * Description:
 *   This file contains the function prototypes and data types for the user
 *   API functions that communicate with DSPs executing G.PAK software. The
 *   file is used by application software in the host processor connected to
 *   G.PAK DSPs via a Host Port Interface.
 *
 * Version: 2.0
 *
 * Revision History:
 *   10/17/01 - Initial release.
 *   07/03/02 - Conferencing and test mode updates.
 *   06/15/04 - Tone type updates.
 *   1/2007   - Combined C54 and C64
 */

#ifndef _GPAKAPI_H  /* prevent multiple inclusion */
#define _GPAKAPI_H

#include "adt_typedef_user.h"
#include "GpakEnum.h"
#include "GpakErrs.h"

#ifdef __cplusplus
extern "C" {
#endif

#define API_VERSION_ID      0x0609u     // G.PAK API Version Id
#define MIN_DSP_VERSION_ID  0x0402u     // the minimum G.PAK DSP Version Id that is fully compatible with the current API code
#define AEC_LIB_VERSION     0x0430		// AEC library version

#define MAX_CIDPAYLOAD_I8 256           // Defined by MAX_MESSAGE_BYTES of ADT_CIDUser.h 
#define GPAK_TAG_I8    32               // For network messaging

#ifndef MAX_NUM_GPAK_FRAMES
#define MAX_NUM_GPAK_FRAMES 12           // maximum number of gpak frame rates (includes extra for custom)
#endif


typedef void *GPAK_FILE_ID;  /* G.PAK Download file identifier */


extern GPAK_DspCommStat_t DSPError[];
extern unsigned int MaxCoresPerDSP;


int VerifyReply (ADT_UInt16 *pMsgBufr, ADT_UInt32 CheckMode, ADT_UInt16 CheckValue);

//}===============================================================
//  Predefined host<->DSP interface addresses and offsets
//{===============================================================
//
// Address at location defined by DSP_IFBLK_ADDRESS points to interface structure 
//  The host must first read the address of the interface structure from the
//  well defined location specified by DSP_IFBL_ADDRESS.  Access to individual
//  interface structure elements can then be made by adding the approriate offsets
//  to this address

//  C54xx processors (C5502, C5510, ...) should define DSP_TYPE as 55
//  C64xx processors (C6412, C6416. DM642, ...) should define DSP_TYPE as 64
//  C64+ processors (C6424, C6437, C6452, C6472) should define DSP_TYPE as 6424, 6437, 6452, or 647 respectively
//
#ifndef DSP_TYPE
   #error DSP_TYPE must be defined as 54, 55, 64, 6424, 6437, 6452, 647, 814 or 3530
#endif

#define CORES_PER_DSP 1   // Define default
#if (DSP_TYPE == 64 || DSP_TYPE == 6424  || DSP_TYPE == 6437 || DSP_TYPE == 6452 || \
     DSP_TYPE == 3530 || DSP_TYPE == 647 || DSP_TYPE == 674 || DSP_TYPE == 814 || DSP_TYPE == 6654 || DSP_TYPE == 6674 || DSP_TYPE == 6678) 

   #if (DSP_TYPE != 674)
      #define PORT_CFG_MCBSP_64
   #endif

   // Identification of host interface block address
   #if (DSP_TYPE == 64)
       #define DSP_IFBLK_ADDRESS 0x0200
   #elif (DSP_TYPE == 6424 || DSP_TYPE == 6437)
       #define DSP_IFBLK_ADDRESS 0x10800000
   #elif (DSP_TYPE == 6452)
       #define DSP_IFBLK_ADDRESS 0x00A00000
   #elif (DSP_TYPE == 647)
       #define DSP_IFBLK_ADDRESS   0xe4000000
       #define DSP_IPCFG_ADDRESS   0xe4000004
       #define DSP_TSIPCFG_ADDRESS 0xe4000100
       #undef  CORES_PER_DSP
       #define CORES_PER_DSP 6
       #undef PORT_CFG_MCBSP_64
       #define PORT_CFG_TSIP
    #elif (DSP_TYPE == 674)
       #define DSP_IFBLK_ADDRESS   0x80000000
       #define DSP_IPCFG_ADDRESS   0x80000004
       #define DSP_TDMCFG_ADDRESS 0x80000100
       #ifndef PORT_CFG_MCASP_64
          #define PORT_CFG_MCBSP_64
       #endif
 
    #elif (DSP_TYPE == 814)
       #define DSP_IFBLK_ADDRESS 0x10f00000
      #undef PORT_CFG_MCBSP_64
    #elif (DSP_TYPE == 3530)
       #define DSP_IFBLK_ADDRESS 0x10f04000
    #elif (DSP_TYPE == 6654)
       #define DSP_IFBLK_ADDRESS   0x90000000
       #define DSP_IPCFG_ADDRESS   0x90000004
       #define DSP_TDMCFG_ADDRESS  0x90000100
       #define DSP_66x
    #elif (DSP_TYPE == 6674)
       #define DSP_IFBLK_ADDRESS   0x90000000
       #define DSP_IPCFG_ADDRESS   0x90000004
       #define DSP_TSIPCFG_ADDRESS 0x90000100
       #undef  CORES_PER_DSP
       #define CORES_PER_DSP 4
       #undef PORT_CFG_MCBSP_64
       #define PORT_CFG_TSIP
       #define DSP_66x
    #elif (DSP_TYPE == 6678)
       #define DSP_IFBLK_ADDRESS   0x90000000
       #define DSP_IPCFG_ADDRESS   0x90000004
       #define DSP_TSIPCFG_ADDRESS 0x90000100
       #undef  CORES_PER_DSP
       #define CORES_PER_DSP 8
       #undef PORT_CFG_MCBSP_64
       #define PORT_CFG_TSIP
       #define DSP_66x
   #endif

   #define DSP_WORD_I8 4        // DSP word size in bytes
   #define DSP_ADDR_SIZE_I8 4   // DSP pointer size in bytes
   #define DSP_DLADDR_SIZE_I8 4 // DSP download address in bytes

   // C64 addressing consists of 1 bytes per MAU.  Offsets are in bytes, 4 bytes per transfer
   #define CircAddress(base,wds)  (base + (wds)*2)      
   #define CircTxSize(wds)        ((((wds) + 1) / 2))  // Round up to determine transfer units required
   #define CircAvail(wds)          (wds)               // Round down to determine transfer units available
   #define BytesToCircUnits(bytes) ((((bytes) + 3) / 4) * 2) // Round bytes up to 32-bit transfers
   #define BytesToTxUnits(bytes)   (((bytes) + 3) / 4) // Round bytes up to 32-bit transfers
   #define BytesToMAUs(bytes)      (bytes)

   #define CMD_MSG_PNTR_OFFSET     0   /* Command Msg Pointer */
   #define REPLY_MSG_PNTR_OFFSET   4   /* Reply Msg Pointer */
   #define EVENT_MSG_PNTR_OFFSET   8   /* Event Msg Pointer */
   #define PKT_BUFR_MEM_OFFSET    12   /* Packet Buffer memory */
   #define DSP_STATUS_OFFSET      16   /* DSP Status */
   #define VERSION_ID_OFFSET      20   /* G.PAK Dsp Version Id */
   #define MAX_CMD_MSG_LEN_OFFSET 24   /* Max Cmd Msg Length */
   #define CMD_MSG_LEN_OFFSET     28   /* Command Msg Length */
   #define REPLY_MSG_LEN_OFFSET   32   /* Reply Msg Length */
   #define EVENT_MSG_LEN_OFFSET   36   /* Event Msg Length */
   #define NUM_CHANNELS_OFFSET    40   /* Num Built Channels */
   #define CPU_USAGE_OFFSET       44   /* CPU Usage statistics */
   #define RTP_CLOCK_OFFSET       72   /* RTP Clock */
   #define API_VERSION_ID_OFFSET  76   /* API version Id */
   #define CPU_PKUSAGE_OFFSET     80   /* CPU Peak Usage statistics */

   #define DSP_INIT_STATUS  0x55555555UL   /* DSP Initialized status value */
   #define HOST_INIT_STATUS 0xAAAAAAAAUL   /* Host Initialized status value */

   typedef ADT_UInt32 DSP_Word;            // Size of DSP word
   typedef ADT_UInt32 DSP_Address;         // Size of DSP address pointer
   #define MAX_DSP_ADDRESS 0xF0000000l

   #define DL_HDR_SIZE 7

   /* Circular packet buffer information structure offsets. */
   #define CB_BUFR_PUT_INDEX       8   /* addressing offset for circular buffer */
   #define CB_BUFR_TAKE_INDEX     12   /* addressing offset for circular buffer */
   #define CB_INFO_STRUCT_LEN 5       /* circbuffer info struct 4 words  */


#ifdef DEBUG_XFER
   #define gpakReadDsp(d,a,s,b)  {                                          \
      DEBUG_XFER (" Read[%d]: %8x:%3d  ", d, a, s);                         \
      gpakReadDspMemory32  (d, (DSP_Address) (a), s, b);                    \
      DEBUG_XFER ("%8x  %8x  %8x\n", (b)[0], (b)[1], (b)[2]);               \
   }
   #define gpakReadDsp32(d,a,s,b)  {                                        \
      DEBUG_XFER (" Read[%d]: %8x:%3d  ", d, a, s);                         \
      gpakReadDspMemory32  (d, (DSP_Address) (a), s, b);                    \
      DEBUG_XFER ("%8x  %8x  %8x\n", (b)[0], (b)[1], (b)[2]);               \
   }
   #define gpakReadDspNoSwap(d,a,s,b)  {                                    \
      DEBUG_XFER (" Read[%d]: %8x:%3d  ", d, a, s);                         \
      gpakReadDspNoSwap32  (d, (DSP_Address) (a), s, b);                    \
      DEBUG_XFER ("%8x  %8x  %8x\n", (b)[0], (b)[1], (b)[2]);               \
   }
 
   #define gpakWriteDsp(d,a,s,b) {                                            \
      DEBUG_XFER ("Write[%d]: %8x:%3d  %8x  %8x  %8x\n", d, a, s, (b)[0], (b)[1], (b)[2]); \
      gpakWriteDspMemory32 (d, (DSP_Address) (a), s, b);                     \
   }
   #define gpakWriteDspNoSwap(d,a,s,b) {                                            \
      DEBUG_XFER ("Write[%d]: %8x:%3d  %8x  %8x  %8x\n", d, a, s, (b)[0], (b)[1], (b)[2]); \
      gpakWriteDspNoSwap32 (d, (DSP_Address) (a), s, b);                     \
   }
#else
   #define gpakReadDsp(d,a,s,b)        gpakReadDspMemory32  (d, (DSP_Address) (a), s, b)
   #define gpakReadDsp32(d,a,s,b)      gpakReadDspMemory32  (d, (DSP_Address) (a), s, b)
   #define gpakWriteDsp(d,a,s,b)       gpakWriteDspMemory32 (d, (DSP_Address) (a), s, b)
   #define gpakReadDspNoSwap(d,a,s,b)  gpakReadDspNoSwap32  (d, (DSP_Address) (a), s, b)
   #define gpakWriteDspNoSwap(d,a,s,b) gpakWriteDspNoSwap32 (d, (DSP_Address) (a), s, b)
#endif


   
#elif (DSP_TYPE == 54)

   #define DSP_IFBLK_ADDRESS 0x0100 

   #define PORT_CFG_MCBSP_54

   #define DSP_WORD_I8 2        // DSP word size in bytes
   #define DSP_ADDR_SIZE_I8 2   // DSP pointer size in bytes
   #define DSP_DLADDR_SIZE_I8 3 // DSP download address in bytes

   // C54 addressing consists of 2 bytes per MAU. Offsets are in 16-bit words
   #define CircAddress(base,wds)   (base + wds)
   #define CircTxSize(wds)         (wds)
   #define CircAvail(wds)          (wds)
   #define BytesToCircUnits(bytes) ((bytes + 1) / 2)  // Round bytes up to 16-bit transfers
   #define BytesToTxUnits(bytes)   ((bytes + 1) / 2)  // Round bytes up to 16-bit transfers
   #define BytesToMAUs(bytes)      ((bytes + 1) / 2)


   #define CMD_MSG_PNTR_OFFSET     0   /* Command Msg Pointer */
   #define REPLY_MSG_PNTR_OFFSET   1   /* Reply Msg Pointer */
   #define EVENT_MSG_PNTR_OFFSET   2   /* Event Msg Pointer */
   #define PKT_BUFR_MEM_OFFSET     3   /* Packet Buffer memory */
   #define DSP_STATUS_OFFSET       4   /* DSP Status */
   #define VERSION_ID_OFFSET       5   /* G.PAK Version Id */
   #define MAX_CMD_MSG_LEN_OFFSET  6   /* Max Cmd Msg Length */
   #define CMD_MSG_LEN_OFFSET      7   /* Command Msg Length */
   #define REPLY_MSG_LEN_OFFSET    8   /* Reply Msg Length */
   #define EVENT_MSG_LEN_OFFSET    9   /* Event Msg Length */
   #define NUM_CHANNELS_OFFSET    10   /* Num Built Channels */
   #define CPU_USAGE_OFFSET       11   /* CPU Usage statistics */
   #define RTP_CLOCK_OFFSET       18   /* RTP Clock */
   #define API_VERSION_ID_OFFSET  19   /* API version Id */
   #define CPU_PKUSAGE_OFFSET     20   /* CPU Peak Usage statistics */

   #define DSP_INIT_STATUS  0x5555U    /* DSP Initialized status value */
   #define HOST_INIT_STATUS 0xAAAAU    /* Host Initialized status value */

   typedef ADT_UInt16 DSP_Word;        // Size of DSP word
   typedef ADT_UInt16 DSP_Address;     // Size of DSP address pointer
   #define MAX_DSP_ADDRESS 0xFF00

   #define DL_HDR_SIZE 6


   /* Circular packet buffer information structure offsets. */
   #define CB_BUFR_PUT_INDEX       2   /* addressing offset for circular buffer */
   #define CB_BUFR_TAKE_INDEX      3   /* addressing offset for circular buffer */
   #define CB_INFO_STRUCT_LEN 5       /* circbuffer info struct 4 words  */

   #define gpakReadDsp(d,a,s,b)        gpakReadDspMemory16  (d, (DSP_Address) (a), s, b)
   #define gpakWriteDsp(d,a,s,b)       gpakWriteDspMemory16 (d, (DSP_Address) (a), s, b)
   #define gpakReadDspNoSwap(d,a,s,b)  gpakReadDspNoSwap16  (d, (DSP_Address) (a), s, b)
   #define gpakWriteDspNoSwap(d,a,s,b) gpakWriteDspNoSwap16 (d, (DSP_Address) (a), s, b)
   #define gpakReadDsp32(d,a,s,b)      gpakReadDspMemory32  (d, (DSP_Address) (a), s, b)


#elif (DSP_TYPE == 55) 

   #define DSP_IFBLK_ADDRESS 0x0100 

   #define PORT_CFG_MCBSP_54

   #define DSP_WORD_I8 2        // DSP word size in bytes
   #define DSP_ADDR_SIZE_I8 4   // DSP pointer size in bytes
   #define DSP_DLADDR_SIZE_I8 3 // DSP download address in bytes

   // C55 data addressing consists of 2 bytes per MAU. Offsets are in 16-bit words
   #define CircAddress(base,wds)   (base + wds)
   #define CircTxSize(wds)         (wds)
   #define CircAvail(wds)          (wds)
   #define BytesToCircUnits(bytes) ((bytes + 1) / 2)  // Round bytes up to 16-bit transfers
   #define BytesToTxUnits(bytes)   ((bytes + 1) / 2)  // Round bytes up to 16-bit transfers
   #define BytesToMAUs(bytes)      ((bytes + 1) / 2)

   #define CMD_MSG_PNTR_OFFSET     0   /* Command Msg Pointer */
   #define REPLY_MSG_PNTR_OFFSET   2   /* Reply Msg Pointer */
   #define EVENT_MSG_PNTR_OFFSET   4   /* Event Msg Pointer */
   #define PKT_BUFR_MEM_OFFSET     6   /* Packet Buffer memory */
   #define DSP_STATUS_OFFSET       8   /* DSP Status */
   #define VERSION_ID_OFFSET       9   /* G.PAK Version Id */
   #define MAX_CMD_MSG_LEN_OFFSET 10   /* Max Cmd Msg Length */
   #define CMD_MSG_LEN_OFFSET     11   /* Command Msg Length */
   #define REPLY_MSG_LEN_OFFSET   12   /* Reply Msg Length */
   #define EVENT_MSG_LEN_OFFSET   13   /* Event Msg Length */
   #define NUM_CHANNELS_OFFSET    14   /* Num Built Channels */
   #define CPU_USAGE_OFFSET       15   /* CPU Usage statistics */
   #define RTP_CLOCK_OFFSET       22   /* RTP Clock */
   #define API_VERSION_ID_OFFSET  24   /* API version Id */
   #define CPU_PKUSAGE_OFFSET     25   /* CPU Peak Usage statistics */

   #define DSP_INIT_STATUS  0x5555U    /* DSP Initialized status value */
   #define HOST_INIT_STATUS 0xAAAAU    /* Host Initialized status value */

   typedef ADT_UInt16 DSP_Word;        // Size of DSP word
   typedef ADT_UInt32 DSP_Address;     // Size of DSP address pointer
   #define MAX_DSP_ADDRESS 0x7FFF00

   #define DL_HDR_SIZE 6

   /* Circular packet buffer information structure offsets. */
   #define CB_BUFR_PUT_INDEX       3   /* addressing offset for circular buffer */
   #define CB_BUFR_TAKE_INDEX      4   /* addressing offset for circular buffer */
   #define CB_INFO_STRUCT_LEN 6       /* circbuffer info struct 6 words  */

   #define gpakReadDsp(d,a,s,b)        gpakReadDspMemory16  (d, (DSP_Address) (a), s, b)
   #define gpakWriteDsp(d,a,s,b)       gpakWriteDspMemory16 (d, (DSP_Address) (a), s, b)
   #define gpakReadDspNoSwap(d,a,s,b)  gpakReadDspNoSwap16  (d, (DSP_Address) (a), s, b)
   #define gpakWriteDspNoSwap(d,a,s,b) gpakWriteDspNoSwap16 (d, (DSP_Address) (a), s, b)
   #define gpakReadDsp32(d,a,s,b)      gpakReadDspMemory32  (d, (DSP_Address) (a), s, b)


#else

   #error DSP_TYPE must be defined as 54, 55, 64, 6424, 6437, 6452, 647, 814, 6654, 6674 or 3530

#endif


#if (DSP_ADDR_SIZE_I8 == 4)
   #define gpakReadDspAddr(d,a,s,b)        gpakReadDspMemory32(d, (DSP_Address) (a), s, b)
#elif (DSP_ADDR_SIZE_I8 == 2)
   #define gpakReadDspAddr(d,a,s,b)        gpakReadDspMemory16(d, (DSP_Address) (a), s, b)
#else
   #error DSP_ADDR_SIZE_I8 is not defined as 4, or 2
#endif


extern unsigned int MaxDsps;
extern unsigned int MaxChannelAlloc;
extern unsigned int MaxWaitLoops;
extern unsigned int DownloadI8;
extern unsigned int HostBigEndian;

extern DSP_Word    MaxChannels[];  /* max num channels */
extern DSP_Word    MaxCmdMsgLen[];  /* max Cmd msg length (octets) */

extern DSP_Address pDspIfBlk[];          /* DSP address of interface blocks */
extern DSP_Address pEventFifoAddress[];  /* Event circular buffers */
extern DSP_Address ifBlkAddress;

/* Download buffers */
extern ADT_UInt32 DlByteBufr_[];     /* Dowload byte buf */
extern DSP_Word   DlWordBufr[];      /* Dowload word buffer */




// Echo Canceller state variables.

/* Echo Canceller definitions used to size the GpakEcanState_t buffers */
/* Do Not modify these values: they are sized for DSP worst-case Ecan allocation */
//#define VARIANT_A         /* let this be defined for short-tail 'variant A' */
#define MAX_EC_TAP_LEN 1024  /* max tap length in sample units */
#define MAX_EC_FIR_SEGS 3    /* max number of FIR segments */
#define MAX_EC_SEG_LEN 64    /* mac FIR segment length in sample units */

#ifndef VARIANT_A
#define EC_STATE_DATA_LEN ((MAX_EC_FIR_SEGS * (2 + MAX_EC_SEG_LEN)) + (MAX_EC_TAP_LEN / 4))
#else
#define EC_STATE_DATA_LEN (MAX_EC_TAP_LEN)
#endif
/* definition of an Echo canceller State information structure */
typedef struct GpakEcanState_t {
    ADT_Int16 Size;                         // structure size
    ADT_Int16 TapLength;                    // Num Taps (tail length)
    ADT_Int16 CoefQ;                        // CoefficientQ
    ADT_Int16 BGCoefQ;                      // Background CoefficientQ
    ADT_Int16 NumFirSegments;               // Num FIR Segments
    ADT_Int16 FirSegmentLen;                // FIR Segment Length
    ADT_Int16 Data[EC_STATE_DATA_LEN];      // state data
} GpakEcanState_t;
#define ECAN_STATE_LEN_I16 (EC_STATE_DATA_LEN + 6)


/* Miscellaneous definitions. */
#define MAX_MSGLEN_I32       (5 + (ECAN_STATE_LEN_I16+1)/2)
#define MSG_BUFFER_SIZE      MAX_MSGLEN_I32         /* size of Host msg buffers (32-bit words) */
#define MSG_BUFFER_ELEMENTS  (MAX_MSGLEN_I32 * 2)   // max number of I16 elements in Msg buffer

#define EVENT_BUFFER_SIZE 144   /* size of DSP Word buffer (16-bit words) holds 16 words plus a 256 byte CID payload */

#define PackBytes(hi,lo) ((ADT_UInt16) (((hi) << 8) | ((lo) & 0xff)))
#define Byte1(a) (((a) >> 8) & 0xff)
#define Byte0(a) ((a) & 0xff)

//}===============================================================
//  Common type definitions and structures
//{===============================================================
typedef enum GpakApiStatus_t {
   GpakApiSuccess = 0,   GpakApiParmError,          GpakApiInvalidChannel,   
   GpakApiInvalidDsp,    GpakApiCommFailure,        GpakApiBufferTooSmall,
   GpakApiNoPayload,     GpakApiBufferFull,         GpakApiReadError,
   GpakApiInvalidFile,   GpakApiUnknownReturnData,  GpakApiNoQue,
   GpakApiInvalidPaylen, GpakApiNotConfigured
} GpakApiStatus_t;


#if 1  // Legacy definitions
typedef GpakApiStatus_t gpakIPStack_t;
#define IPSuccess            GpakApiSuccess
#define IPInvalidDSP         GpakApiInvalidDsp
#define IPDspCommFailure     GpakApiCommFailure

typedef GpakApiStatus_t gpakDownloadStatus_t;
#define GdlSuccess       GpakApiSuccess
#define GdlFileReadError GpakApiReadError
#define GdlInvalidFile   GpakApiInvalidFile
#define GdlInvalidDsp    GpakApiInvalidDsp

typedef GpakApiStatus_t gpakWriteSysParmsStatus_t;
#define WspSuccess         GpakApiSuccess
#define WspParmError       GpakApiParmError
#define WspInvalidDsp      GpakApiInvalidDsp
#define WspDspCommFailure  GpakApiCommFailure

typedef GpakApiStatus_t gpakConfigPortStatus_t;
#define CpsSuccess         GpakApiSuccess
#define CpsParmError       GpakApiParmError
#define CpsInvalidDsp      GpakApiInvalidDsp
#define CpsDspCommFailure  GpakApiCommFailure

typedef GpakApiStatus_t gpakAECParmsStatus_t;
#define AECSuccess         GpakApiSuccess
#define AECInvalidDsp      GpakApiInvalidDsp
#define AECDspCommFailure  GpakApiCommFailure
#define AECParmError       GpakApiParmError
#define AECNotConfigured   GpakApiNotConfigured

typedef GpakApiStatus_t gpakConfigArbToneStatus_t;
#define CatSuccess         GpakApiSuccess
#define CatParmError       GpakApiParmError
#define CatInvalidDsp      GpakApiInvalidDsp
#define CatDspCommFailure  GpakApiCommFailure

typedef GpakApiStatus_t gpakConfigConferStatus_t;
#define CfsSuccess         GpakApiSuccess
#define CfsParmError       GpakApiParmError
#define CfsInvalidDsp      GpakApiInvalidDsp
#define CfsDspCommFailure  GpakApiCommFailure

typedef GpakApiStatus_t gpakGetSysCfgStatus_t;
#define GscSuccess         GpakApiSuccess
#define GscInvalidDsp      GpakApiInvalidDsp
#define GscDspCommFailure  GpakApiCommFailure

typedef GpakApiStatus_t gpakReadSysParmsStatus_t;
#define RspSuccess         GpakApiSuccess
#define RspInvalidDsp      GpakApiInvalidDsp
#define RspDspCommFailure  GpakApiCommFailure

typedef GpakApiStatus_t gpakReadEventFIFOMessageStat_t;
#define RefEventAvail      GpakApiSuccess
#define RefNoEventAvail    GpakApiNoPayload
#define RefInvalidDsp      GpakApiInvalidDsp
#define RefDspCommFailure  GpakApiCommFailure
#define RefInvalidEvent    GpakApiUnknownReturnData
#define RefNoEventQue      GpakApiNoQue

typedef GpakApiStatus_t gpakCpuUsageStatus_t;
#define GcuSuccess         GpakApiSuccess
#define GcuInvalidDsp      GpakApiInvalidDsp
#define GcuDspCommFailure  GpakApiCommFailure

typedef GpakApiStatus_t gpakReadMcBspStats_t;
#define RmsSuccess           GpakApiSuccess
#define RmsInvalidDsp        GpakApiInvalidDsp
#define RmsDspCommFailure    GpakApiCommFailure

typedef GpakApiStatus_t gpakResetSystemStat_t;
#define RssSuccess           GpakApiSuccess
#define RssInvalidDsp        GpakApiInvalidDsp
#define RssDspCommFailure    GpakApiCommFailure

typedef GpakApiStatus_t gpakConfigChanStatus_t;
#define CcsSuccess         GpakApiSuccess
#define CcsParmError       GpakApiParmError
#define CcsInvalidChannel  GpakApiInvalidChannel
#define CcsInvalidDsp      GpakApiInvalidDsp
#define CcsDspCommFailure  GpakApiCommFailure

typedef GpakApiStatus_t gpakRtpStatus_t;
#define RtpSuccess         GpakApiSuccess
#define RtpInvalidDsp      GpakApiInvalidDsp
#define RtpDspCommFailure  GpakApiCommFailure
#define RtpInvalidChannel  GpakApiInvalidChannel
#define RtpParmError       GpakApiParmError

typedef GpakApiStatus_t gpakTearDownStatus_t;
#define TdsSuccess         GpakApiSuccess
#define TdsError           GpakApiParmError
#define TdsInvalidChannel  GpakApiInvalidChannel
#define TdsInvalidDsp      GpakApiInvalidDsp
#define TdsDspCommFailure  GpakApiCommFailure

typedef GpakApiStatus_t gpakGetChanStatStatus_t;
#define CssSuccess         GpakApiSuccess
#define CssError           GpakApiParmError
#define CssInvalidChannel  GpakApiInvalidChannel
#define CssInvalidDsp      GpakApiInvalidDsp
#define CssDspCommFailure  GpakApiCommFailure

typedef GpakApiStatus_t gpakResetChannelStat_t;
#define RcsSuccess           GpakApiSuccess
#define RcsInvalidChannel    GpakApiInvalidChannel
#define RcsInvalidDsp        GpakApiInvalidDsp
#define RcsDspCommFailure    GpakApiCommFailure

typedef GpakApiStatus_t gpakSendCIDPayloadToDspStat_t;
#define ScpSuccess           GpakApiSuccess
#define ScpInvalidChannel    GpakApiInvalidChannel
#define ScpInvalidDsp        GpakApiInvalidDsp
#define ScpInvalidPaylen     GpakApiInvalidPaylen
#define ScpDspCommFailure    GpakApiCommFailure
#define ScpParmError         GpakApiParmError

typedef GpakApiStatus_t gpakGenToneStatus_t;
#define TgcSuccess         GpakApiSuccess
#define TgcParmError       GpakApiParmError
#define TgcInvalidDsp      GpakApiInvalidDsp
#define TgcDspCommFailure  GpakApiCommFailure
#define TgcInvalidChannel  GpakApiInvalidChannel

typedef GpakApiStatus_t gpakPlayRecordStatus_t;
#define PrsSuccess         GpakApiSuccess
#define PrsInvalidDsp      GpakApiInvalidDsp
#define PrsDspCommFailure  GpakApiCommFailure
#define PrsInvalidChannel  GpakApiInvalidChannel
#define PrsBufferTooSmall  GpakApiBufferTooSmall
#define PrsParmError       GpakApiParmError

typedef GpakApiStatus_t gpakAlgControlStat_t;
#define AcSuccess            GpakApiSuccess
#define AcInvalidChannel     GpakApiInvalidChannel
#define AcInvalidDsp         GpakApiInvalidDsp
#define AcParmError          GpakApiParmError
#define AcDspCommFailure     GpakApiCommFailure

typedef GpakApiStatus_t gpakGetEcanStateStat_t;
#define GesSuccess           GpakApiSuccess
#define GesInvalidChannel    GpakApiInvalidChannel
#define GesInvalidDsp        GpakApiInvalidDsp
#define GesDspCommFailure    GpakApiCommFailure

typedef GpakApiStatus_t gpakSetEcanStateStat_t;
#define SesSuccess           GpakApiSuccess
#define SesInvalidChannel    GpakApiInvalidChannel
#define SesInvalidDsp        GpakApiInvalidDsp
#define SesParmError         GpakApiParmError
#define SesDspCommFailure    GpakApiCommFailure

typedef GpakApiStatus_t gpakGetAecStatusStat_t;
#define GaecsSuccess         GpakApiSuccess
#define GaecsInvalidChannel  GpakApiInvalidChannel
#define GaecsInvalidDsp      GpakApiInvalidDsp
#define GaecsParmError       GpakApiParmError
#define GaecsDspCommFailure  GpakApiCommFailure

typedef GpakApiStatus_t gpakGetLecStatusStat_t;
#define GlecsSuccess         GpakApiSuccess
#define GlecsInvalidChannel  GpakApiInvalidChannel
#define GlecsInvalidDsp      GpakApiInvalidDsp
#define GlecsParmError       GpakApiParmError
#define GlecsDspCommFailure  GpakApiCommFailure

typedef GpakApiStatus_t gpakAGCReadInputPowerStat_t;
#define AprSuccess           GpakApiSuccess
#define AprInvalidChannel    GpakApiInvalidChannel
#define AprInvalidDsp        GpakApiInvalidDsp
#define AprDspCommFailure    GpakApiCommFailure
#define AprParmError         GpakApiParmError

typedef GpakApiStatus_t gpakActiveArbToneStatus_t;
#define AatSuccess         GpakApiSuccess
#define AatParmError       GpakApiParmError
#define AatInvalidDsp      GpakApiInvalidDsp
#define AatDspCommFailure  GpakApiCommFailure

typedef GpakApiStatus_t gpakSendPayloadStatus_t;
#define SpsSuccess         GpakApiSuccess
#define SpsBufferFull      GpakApiBufferFull
#define SpsInvalidChannel  GpakApiInvalidChannel
#define SpsInvalidDsp      GpakApiInvalidDsp
#define SpsDspCommFailure  GpakApiCommFailure

typedef GpakApiStatus_t gpakGetPayloadStatus_t;
#define GpsSuccess         GpakApiSuccess
#define GpsNoPayload       GpakApiNoPayload
#define GpsInvalidChannel  GpakApiInvalidChannel
#define GpsBufferTooSmall  GpakApiBufferTooSmall
#define GpsInvalidDsp      GpakApiInvalidDsp
#define GpsDspCommFailure  GpakApiCommFailure

typedef GpakApiStatus_t gpakTestStatus_t;
#define CtmSuccess         GpakApiSuccess
#define CtmParmError       GpakApiParmError
#define CtmInvalidDsp      GpakApiInvalidDsp
#define CtmDspCommFailure  GpakApiCommFailure
#define CtmCustomMsgError  GpakApiBufferTooSmall


#endif
    
typedef enum  GpakChannelTypes {
   GpakVoiceChannel = 0,
   GpakDataChannel
} GpakChannelTypes;

typedef struct circBufr_t {
   DSP_Address BufrBase;
   DSP_Word BufrSize;
   DSP_Word PutIndex;
   DSP_Word TakeIndex;
   DSP_Word Reserved;
} CircBufr_t;

typedef struct pktHdr {
   ADT_UInt16 OctetsInPayload;
   ADT_UInt16 ChannelId;
   ADT_UInt16 PayloadClass;
   ADT_UInt16 PayloadType;
   ADT_UInt32 TimeStamp;
} PktHdr_t;

typedef struct pkt {
   PktHdr_t   Hdr;
   ADT_UInt8  PayloadData[240];
} Packet_t;

//}===============================================================
//    Host download of DSP image
//{===============================================================
/*
 * gpakDownloadDsp - Download a DSP's Program and initialized Data memory.
 *
 * FUNCTION
 *  This function reads a DSP's Program and Data memory image from the
 *  specified file and writes the image to the DSP's memory.
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
extern GpakApiStatus_t gpakDownloadDsp ( 
    ADT_UInt32 DspId,        /* DSP Identifier (0 to MaxDSPCores-1) */
    void *FileId     /* G.PAK Download File Identifier */
    );


//}===============================================================
//    System status and configuration
//{===============================================================
typedef struct GpakIPCfg_t {
   ADT_UInt32 RTPSrcIP;     // IP address to substitute as source IP on outbound RTP packets
   ADT_UInt8  TxMac[6];     // MAC address to substitute as MAC source on outbound RTP packets

   ADT_UInt8  RxMac[6];     // MAC address to use for DSP's IP stack

   ADT_UInt32 IPAddress;    // Static IP address of DSP.  If 0, the DSP will obtain IP address via DHCP
   ADT_UInt32 IPGateway;    // IP address of gateway. Used only if static IP address is selected.
   ADT_UInt32 IPMask;       // IP sub-net.  Used only if static IP address is selected.

   ADT_UInt16 RTPPort;      // RTP server port.
   ADT_UInt16 MsgPort;      // DSP's TCP messaging port.
   ADT_UInt16 EvtPort;      // DSP's TCP event port.
   ADT_UInt16 PortRange;    // Gpak RTP range (default 4000) // Filler for alignment

   ADT_UInt32 DHCPIP;       // IP address to substitute as source IP on outbound RTP packets

} GpakIPCfg_t;

typedef struct GpakSystemConfig_t {
    ADT_UInt32 GpakVersionId;    /* G.PAK Version Identification */
    ADT_UInt16 MaxChannels;      /* maximum number of channels */
    ADT_UInt16 ActiveChannels;   /* number of configured channels */
    ADT_UInt32 CfgEnableFlags;   /* Configuration Enable Flags */
    GpakProfiles PacketProfile;  /* Packet Profile type */
    GpakRTPPkts  RtpTonePktType; /* RTP Tone Packet type */
    ADT_UInt16 CfgFramesChans;   /* cfgd frame sizes and chan types */


    ADT_UInt16 Port1NumSlots;   /* Port 1's time slots on stream */
    ADT_UInt16 Port2NumSlots;   /* Port 2's time slots on stream */
    ADT_UInt16 Port3NumSlots;   /* Port 3's time slots on stream */
    ADT_UInt16 Port1SupSlots;   /* Port 1's num supported time slots */
    ADT_UInt16 Port2SupSlots;   /* Port 2's num supported time slots */
    ADT_UInt16 Port3SupSlots;   /* Port 3's num supported time slots */

	// PCM Echo canceller
    ADT_UInt16 NumPcmEcans;     /* Number of PCM Echo Cancellers */
    ADT_UInt16 PcmMaxTailLen;   /* Tail length (ms) */
    ADT_UInt16 PcmMaxFirSegs;   /* max FIR segments */
    ADT_UInt16 PcmMaxFirSegLen; /* max FIR segment len (ms) */

    // PKT Echo canceller
    ADT_UInt16 NumPktEcans;     /* Number of Packet Echo Cancellers */
    ADT_UInt16 PktMaxTailLen;   /* Tail length (ms) */
    ADT_UInt16 PktMaxFirSegs;   /* max FIR segments */
    ADT_UInt16 PktMaxFirSegLen; /* max FIR segment len (ms) */

    ADT_UInt16 MaxConferences;  /* maximum number of conferences */
    ADT_UInt16 MaxNumConfChans; /* max number channels per conference */
    ADT_UInt16 SelfTestStatus;  /* Self Test status flags */
    ADT_UInt16 MaxToneDetTypes; /* Max concurrent tone detect types */
    ADT_UInt16 AECInstances;    // Max acoustic echo canceller instances
    ADT_UInt16 AECTailLen;      // Max tail length for acoustic echo canceller
} GpakSystemConfig_t;

typedef struct GpakECParams_t {
    /* PCM and Packet Echo Canceller parameters. */
    ADT_Int16 TailLength;      // Tail length (ms) 
    ADT_Int16 NlpType;         // NLP Type 
    ADT_Int16 AdaptEnable;     // Adapt Enable flag 
    ADT_Int16 G165DetEnable;   // G165 Detect Enable flag 
    ADT_Int16 DblTalkThresh;   // Double Talk threshold      (  0..18 dB) 
    ADT_Int16 NlpThreshold;    // NLP threshold              (  0..90 dB) 
    ADT_Int16 NlpUpperLimitThreshConv;  // NLP converge      (  0..90 dB)
    ADT_Int16 NlpUpperLimitThreshPreconv; // NLP preconverge (  0..90 dB)
    ADT_Int16 NlpMaxSupp;      // Max suppression            (  0..90 dB)
    ADT_Int16 CngThreshold;    // CNG Noise threshold        (-96..0 dBm)
    ADT_Int16 AdaptLimit;      // Adaption frequency          (% of framesize)
    ADT_Int16 CrossCorrLimit;  // Cross correlation frequency (% of framesize)
    ADT_Int16 NumFirSegments;  // Num FIR Segments           (1..3)
    ADT_Int16 FirSegmentLen;   // FIR Segment Length         (ms)
    ADT_Int16 FirTapCheckPeriod; // Frequency to check tails (2..1000 ms)
    ADT_Int16 MaxDoubleTalkThres; // Maximum double-talk threshold (  0..50 dB) 
    ADT_Int16 MixedFourWireMode;	// Enable 4-wire (echo-free) lines flag
    ADT_Int16 ReconvergenceCheckEnable; // Enable reconvergence checking flag
} GpakECParams_t;

typedef struct GpakAGCParms_t {
    ADT_Int16 TargetPower;       // Target Power          (-30..0  dBm) 
    ADT_Int16 LossLimit;         // Maximum Attenuation   (-23..0  dB) 
    ADT_Int16 GainLimit;         // Maximum Positive Gain (  0..23 dB)
    ADT_Int16 LowSignal;         // Low Signal leven      (-65..-20 dBm)
} GpakAGCParms_t;

typedef struct GpakCIDParms_t {
    ADT_Int16   type2CID;           // 1==type2 CID, 0==type1 CID
    ADT_Int16   numSeizureBytes;    // (Tx only) num seizure bytes to prepend
    ADT_Int16   numMarkBytes;       // (Tx only) num mark bytes to prepend
    ADT_Int16   numPostambleBytes;  // (Tx only) num postamble bytes to append
    GpakFskType fskType;            // (Tx and Rx) v23 or bell 202 
    ADT_Int16   fskLevel;           // (Tx only) Tx fsk level (0...-19 dBm)
    ADT_UInt16  msgType;            // (Tx only) message type field (valid if format==1)
} GpakCIDParms_t;

typedef struct GpakSystemParms_t {

    /* AGC parameters- A and B sides */
    GpakAGCParms_t AGC_A;
    GpakAGCParms_t AGC_B;

    /* VAD parameters. */
    ADT_Int16 VadNoiseFloor;        /* Noise Floor Threshold (-96..0 dBm) */
    ADT_Int16 VadHangTime;          /* Speech Hangover Time  (0..4000 msec) */
    ADT_Int16 VadWindowSize;        /* Analysis Window Size  (1..30 msec) */

    /* PCM Echo Canceller parameters. */
    GpakECParams_t PcmEc;

    /* Packet Echo Canceller parameters. */
    GpakECParams_t PktEc;

    /* Conference parameters. */
    ADT_Int16 ConfNumDominant;      /* Number of Dominant Conference Members */
    ADT_Int16 MaxConfNoiseSuppress; /* maximum Conference Noise Suppression */

    /* Caller ID parameters */
    GpakCIDParms_t Cid;
    ADT_UInt16  VadReport;          /* 1 == vad state reporting enabled */
} GpakSystemParms_t;

typedef struct GpakDtmfDetParms_t {
    int UpdateMinSignalLevel:1;        // Update the minimum signal level criteria with the MinDetectSignalLevDbm value
    int UpdateMaxFreqDev:1;            // Update the maximum frequency deviation criteria with the MaxFreqDevPercent_x10 value
    int UpdateMaxTwist:1;              // Update the maximum twist level criteria with the MaxFwdTwist and MaxRevTwist values
    int UpdateToneQuality:1;           // Update the maximum tone quality criteria with the ToneQuality value
    int UpdateMaxWaivers:1;            // Update the maximum number of waivers with the MaxWaivers value 
    int UpdateWaiverEnable:1;          // Update the waived criteria with the WaiveEnable values

    ADT_Int16   MinDetectSignalLevDbm;  // minimum detect signal level: negative value in dBm. Default is -25 dBm
    ADT_UInt16  MaxFreqDevPercent_x10;  // maximum percent frequency deviation multiplied by 10. (e.g., to program a deviation to 1.9%, set the parameter to 19) Default is 17 (i.e., 1.7 %)
    ADT_Int16   MaxFwdTwistDb;          // maximum forward twist in dB. Valid range is 4 to 8. Default is 8 dB
    ADT_Int16   MaxRevTwistDb;          // maximum reverse twist in dB. Valid range is 4 to 8. Default is 4 dB
    ADT_UInt16  ToneQuality;            // quality of tones that can be detected.  0=highest, 5=lowest.  The lowest quality is more likely to report false tones.
    ADT_UInt16  MaxWaivers;             // maximum number of detection criteria that can be waivered when detecting tones.

    struct WaivedCriteriaBits {    // When enabled (1), the nominal criteria may be waived by the criteria's waiver settings
       int Power:1;           // Signal power level
       int FrequencyDev:1;    // Frequency deviation
       int Twist:1;           // Twist levels
       int SNR:1;             // Signal to noise ratio
       int ToneGap:1;         // Gap between two different DTMF tones
    } WaiverEnable; 
} GpakDtmfDetParms_t;


typedef struct GpakAsyncEventData_t {
   GpakDeviceSide_t   Direction;   // Device detecting tone
   union aux {
      struct toneEvent {   //  For EventToneDetect
        GpakToneCodes_t    ToneCode;       // detected tone code
        ADT_UInt16         ToneDuration;   // tone duration
      } toneEvent;

      struct recordEvent {    //  For EventRecordingStopped and EventRecordingBufferFull
        ADT_UInt32         RecordLength;  // Actual length (I16) of recording (EventRecordingStoprred or Full)
                                          // Length of recording (I16) buffer (other events)
        ADT_UInt32         RecordAddr;    // Address of recording buffer
      } recordEvent;
      
      struct captureData {
         ADT_UInt32  AAddr;
         ADT_UInt32  ALenI32;       

         ADT_UInt32  BAddr;
         ADT_UInt32  BLenI32;       

         ADT_UInt32  CAddr;
         ADT_UInt32  CLenI32;       
         ADT_UInt32  DAddr;
         ADT_UInt32  DLenI32;       
         
      } captureData;

      struct rxCidEvent {
         ADT_UInt16   Length;                     // length (octets) of Rx CID payload
         ADT_UInt8    Payload[MAX_CIDPAYLOAD_I8]; // Rx CID payload data
      } rxCidEvent;

      struct srtpEvent {   //  For EventToneDetect
            ADT_UInt32 roc;            // Identifies roc for next rtp packet
            ADT_UInt32 MkiValue;       // 
            ADT_UInt16 seq;            // Identifies seq for next rtp packet
            ADT_UInt16 MkiI8;          //
      } srtpEvent;
      ADT_UInt16 CID2StateCode[3];        // Type 2 CID state code
      ADT_UInt8  customPayload [1];
   } aux;
} GpakAsyncEventData_t;
#define ToneDevice Direction   // Legacy code

typedef struct GpakTestData_t {
   union {
        ADT_UInt16 TestParm;  // single parameter

        /* source a fixed pattern onto a channel's tdm timeslot */
        /* Used when Test Mode is TdmFixedValue */
        struct fixedVal {
            ADT_UInt16       ChannelId;     /* Channel Id */
            GpakDeviceSide_t DeviceSide;    /* Device side */
            ADT_UInt16       Value;         /* 16-bit value  */
            GpakActivation   State; 		  /* activation state */
        } fixedVal;

        /* Put a Serial port into loopback mode */
        /* Used when Test Mode is TdmLoopback */
        struct tdmLoop {
            GpakSerialPort_t PcmOutPort;    /* PCM Output Serial Port Id */
            GpakActivation   State; 	    /* activation state */
        } tdmLoop;        
  } u;
} GpakTestData_t;

/*
 * gpakGetSystemConfig - Read a DSP's System Configuration.
 *
 * FUNCTION
 *  This function reads a DSP's System Configuration information.
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */

extern GpakApiStatus_t gpakGetSystemConfig (
    ADT_UInt32 DspId,            /* DSP Identifier (0 to MaxDSPCores-1) */
    GpakSystemConfig_t *sysCfg  /* pointer to System Config info var */
    );

extern void gpakParseSystemConfig (ADT_UInt16 *Msg, GpakSystemConfig_t *sysCfg);

/*
 * gpakReadSystemParms - Read a DSP's System Parameters.
 *
 * FUNCTION
 *  This function reads a DSP's System Parameters information.
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */

extern GpakApiStatus_t gpakReadSystemParms (
    ADT_UInt32 DspId,                /* DSP Identifier (0 to MaxDSPCores-1) */
    GpakSystemParms_t *pSysParms    /* pointer to System Parms info var */
    );
extern void gpakParseSystemParms (ADT_Int16 *pMsgBuffer, GpakSystemParms_t *pSysParms);


/*
 * gpakWriteSystemParms - Write a DSP's System Parameters.
 *
 * FUNCTION
 *  This function writes a DSP's System Parameters information.
 *
 *    Note:
 *      Writing of all AGC related parameters occurs only if the AgcUpdate
 *      parameter is non zero.
 *      Writing of all VAD related parameters occurs only if the VadUpdate
 *      parameter is non zero.
 *      Writing of all PCM Echo Canceller related parameters occurs only if the
 *      PcmEcUpdate parameter is non zero.
 *      Writing of all Packet Echo Canceller related parameters occurs only if
 *      the PktEcUpdate parameter is non zero.
 *      Writing of all Conference related parameters occurs only if the
 *      ConfUpdate parameter is non zero.
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */

extern GpakApiStatus_t gpakWriteSystemParms (
    ADT_UInt32 DspId,                /* DSP Identifier (0 to MaxDSPCores-1) */
    GpakSystemParms_t *pSysParms,   /* pointer to System Parms info var */
    ADT_UInt16 AgcUpdate,           /* flag indicating AGC parms update: */
              /*  0 == no update, 1 == A-device update, 2 == B-device update, 3 == update A and B */

    ADT_UInt16 VadUpdate,           /* flag indicating VAD parms update */
    ADT_UInt16 PcmEcUpdate,         /* flag indicating PCM Ecan parms update */
    ADT_UInt16 PktEcUpdate,         /* flag indicating Pkt Ecan parms update */
    ADT_UInt16 ConfUpdate,          /* flag indicating Conference prms update */
    ADT_UInt16 CIDUpdate,           /* flag indicating CID parms update */
    GPAK_SysParmsStat_t *pStatus    /* pointer to Write System Parms Status */
    );
    
extern ADT_UInt32 gpakFormatSystemParms (ADT_Int16 *Msg, GpakSystemParms_t *sysPrms,
    ADT_UInt16 AgcUpdate,   ADT_UInt16 VadUpdate, ADT_UInt16 PcmEcUpdate,
    ADT_UInt16 PktEcUpdate, ADT_UInt16 ConfUpdate, ADT_UInt16 CIDUpdate);


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakDtmfDetConfig - Write a DSP's DTMF detector configuration parameters.
 *
 * FUNCTION
 *  Writes selective DTMF Detector Parameters to a DSP. Calling this API can 
 *  over-ride the DTMF detector's default global settings. Changing these settings
 *  will affect all dtmf detect instances, current and future. It's recommended
 *  to call this API while no detectors are running.
 *
 * Inputs
 *  DspId               - Dsp identifier 
 *  dtmfPrms            - Dtmf parameters data structure pointer
 *  CriteriaSet         - DTMF_Nominal   Nominal criteria.  Standard criteria used for tone detection.
                          DTMF_Waivers   Less stringent criteria that may be used to waive nominal criteria.                         
 *
 * Outputs
 *   pStatus     - dtmf parms status from DSP
 * 
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
extern GpakApiStatus_t gpakDtmfDetConfig (ADT_UInt32 DspId, GpakDtmfDetParms_t *dtmfPrms,
    GpakDTMFCriteria_t CriteriaSet, GPAK_DtmfDetConfigStat_t *pStatus);



/* 
 *  gpakReadEventFIFOMessage 
 *
 * FUNCTION
 *  This function reads the G.PAK event buffer.  It is called by the host
 *  to read the next unread event within the FIFO queue.  This routine is 
 *  normally trigger by a  host interrupt, but may be called periodically to 
 *  poll the DSP for events.
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */

extern GpakApiStatus_t gpakReadEventFIFOMessage (
      ADT_UInt32 DspId,                      /* DSP Identifier (0 to MaxDSPCores-1) */
      ADT_Int16 *pChannelId,                /* pointer to channel Id */
      GpakAsyncEventCode_t *pEventCode,     /* pointer to event type */
      GpakAsyncEventData_t *pEventData);    /* pointer to event structure */


/* 
 * gpakReadCoreUsage - Read CPU usage statistics from a DSP.
 *
 * FUNCTION
 *  This function reads the CPU usage statistics from a DSP's memory. The
 *  average CPU usage in units of .1 percent are obtained for each of the frame
 *  rates.
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
extern GpakApiStatus_t gpakReadCoreUsage (
    ADT_UInt32 DspId,         /* DSP Identifier (0 to MaxDSPCores-1) */
    ADT_UInt32 CoresPerDsp,   /* Number of cores for DSP ... DSP returns the number in report */
    ADT_UInt32 FramesPerCore, /* Number of frames per core ... DSP returns the number in report*/
    ADT_UInt32 AvgCpuUsage [CORES_PER_DSP][MAX_NUM_GPAK_FRAMES],  /* pointer to Average frame usage array */
    ADT_UInt32 PeakCpuUsage[CORES_PER_DSP][MAX_NUM_GPAK_FRAMES]  /* pointer to Peak frame usage array */
    );

extern ADT_Int32 gpakReadRTPClock (ADT_UInt32 DspId);

extern GpakApiStatus_t gpakCfgIPStack (ADT_Int32 DspId, GpakIPCfg_t *IPConfig);
extern GpakApiStatus_t gpakReadIPStackCfg (ADT_Int32 DspId, GpakIPCfg_t *IPConfig);

extern GpakApiStatus_t gpakSetVlanTag (ADT_UInt32 DspId, ADT_UInt16 VlanIdx, ADT_UInt16 VlanID, 
                                       ADT_UInt8 priority, GPAK_VlanStat_t *dspStat);
extern GpakApiStatus_t gpakGetVlanTag (ADT_UInt32 DspId, ADT_UInt16 VlanIdx, ADT_UInt16 *VlanID,
                                       ADT_UInt8 *priority, GPAK_VlanStat_t *dspStat);

extern GpakApiStatus_t gpakConfigSpeex (ADT_UInt32 DspId, ADT_UInt8 quality, ADT_UInt8 complexity, ADT_Bool variableRate,
                                        ADT_Bool perceptualEnhancement, GPAK_SpeexParamsStat_t *dspStatus);

//}===============================================================
//    Serial port configuration
//{===============================================================


#ifdef PORT_CFG_MCBSP_54
typedef struct GpakPortConfig_t {
   GpakSlotCfg_t MultiMode[3];   // Multi channel mode

   ADT_Int16 LowBlk[3];          // Index of channel in low block
   ADT_Int16 LowMask[3];         // Mask of channels in low block
   ADT_Int16 HighBlk[3];         // Index of channel in low block
   ADT_Int16 HighMask[3];        // Mask of channels in low block

   GpakCompandModes Compand[3];  // Companding mode

   // Tx/Rx clock and frame sync polarities and source
   ADT_Bool  RxClockRise [3];
   ADT_Bool  RxClockInternal [3];

   ADT_Bool  TxClockFall [3];
   ADT_Bool  TxClockInternal [3];

   ADT_Bool  RxSyncLow [3];
   ADT_Bool  RxSyncInternal [3];

   ADT_Bool  TxSyncLow [3];
   ADT_Bool  TxSyncInternal [3];

   // Tx and Rx data delays
   ADT_Int16 TxDelay [3];
   ADT_Int16 RxDelay [3];

   ADT_Int16 ClkDiv [3];
   ADT_Int16 PulseWidth [3];


} GpakPortConfig_t;
#endif

#ifdef PORT_CFG_MCBSP_55
typedef struct GpakPortConfig_t {
   GpakActivation Port1Enable;
   ADT_Int32 Port1SlotMask0_31;
   ADT_Int32 Port1SlotMask32_63;
   ADT_Int32 Port1SlotMask64_95;
   ADT_Int32 Port1SlotMask96_127;

   GpakActivation Port2Enable;
   ADT_Int32 Port2SlotMask0_31;
   ADT_Int32 Port2SlotMask32_63;
   ADT_Int32 Port2SlotMask64_95;
   ADT_Int32 Port2SlotMask96_127;
   
   GpakActivation Port3Enable;
   ADT_Int32 Port3SlotMask0_31;
   ADT_Int32 Port3SlotMask32_63;
   ADT_Int32 Port3SlotMask64_95;
   ADT_Int32 Port3SlotMask96_127;

   ADT_Int16 TxDataDelay1, TxDataDelay2, TxDataDelay3;
   ADT_Int16 RxDataDelay1, RxDataDelay2, RxDataDelay3;
   ADT_Int16 ClkDiv1,      ClkDiv2,      ClkDiv3;
   ADT_Int16 FramePeriod1, FramePeriod2, FramePeriod3;
   ADT_Int16 FrameWidth1,  FrameWidth2,  FrameWidth3;
   GpakCompandModes Compand1,     Compand2,  Compand3;
   GpakActivation   SampRateGen1, SampRateGen2, SampRateGen3; 
   
   ADT_UInt16 txClockPolarity1, txClockPolarity2, txClockPolarity3;
   ADT_UInt16 rxClockPolarity1, rxClockPolarity2, rxClockPolarity3;

} GpakPortConfig_t;
#endif

#ifdef PORT_CFG_MCBSP_64
typedef struct GpakPortConfig_t {
   GpakSlotCfg_t SlotsSelect1;         /* port 1 Slot selection */
   GpakActivation Port1Enable;
   ADT_Int32 Port1SlotMask0_31;
   ADT_Int32 Port1SlotMask32_63;
   ADT_Int32 Port1SlotMask64_95;
   ADT_Int32 Port1SlotMask96_127;

   GpakSlotCfg_t SlotsSelect2;         /* port 2 Slot selection */
   GpakActivation Port2Enable;
   ADT_Int32 Port2SlotMask0_31;
   ADT_Int32 Port2SlotMask32_63;
   ADT_Int32 Port2SlotMask64_95;
   ADT_Int32 Port2SlotMask96_127;
   
   GpakSlotCfg_t SlotsSelect3;         /* port 3 Slot selection */
   GpakActivation Port3Enable;
   ADT_Int32 Port3SlotMask0_31;
   ADT_Int32 Port3SlotMask32_63;
   ADT_Int32 Port3SlotMask64_95;
   ADT_Int32 Port3SlotMask96_127;

   ADT_Int16 TxDataDelay1, TxDataDelay2, TxDataDelay3;
   ADT_Int16 RxDataDelay1, RxDataDelay2, RxDataDelay3;
   ADT_Int16 ClkDiv1,      ClkDiv2,      ClkDiv3;
   ADT_Int16 FramePeriod1, FramePeriod2, FramePeriod3;
   ADT_Int16 FrameWidth1,  FrameWidth2,  FrameWidth3;
   GpakCompandModes Compand1,     Compand2,  Compand3;
   GpakActivation   SampRateGen1, SampRateGen2, SampRateGen3; 

   GpakActivation AudioPort1Enable;
   ADT_Int32 AudioPort1TxSerMask;  
   ADT_Int32 AudioPort1RxSerMask;  
   ADT_Int32 AudioPort1SlotMask;   

   GpakActivation AudioPort2Enable;
   ADT_Int32 AudioPort2TxSerMask;  
   ADT_Int32 AudioPort2RxSerMask;  
   ADT_Int32 AudioPort2SlotMask;   


} GpakPortConfig_t;

typedef struct GpakMcBspPortConfig_t {
   GpakSlotCfg_t SlotsSelect1;         /* port 1 Slot selection */
   GpakActivation Port1Enable;
   ADT_Int32 Port1SlotMask0_31;
   ADT_Int32 Port1SlotMask32_63;
   ADT_Int32 Port1SlotMask64_95;
   ADT_Int32 Port1SlotMask96_127;

   GpakSlotCfg_t SlotsSelect2;         /* port 2 Slot selection */
   GpakActivation Port2Enable;
   ADT_Int32 Port2SlotMask0_31;
   ADT_Int32 Port2SlotMask32_63;
   ADT_Int32 Port2SlotMask64_95;
   ADT_Int32 Port2SlotMask96_127;
   
   GpakSlotCfg_t SlotsSelect3;         /* port 3 Slot selection */
   GpakActivation Port3Enable;
   ADT_Int32 Port3SlotMask0_31;
   ADT_Int32 Port3SlotMask32_63;
   ADT_Int32 Port3SlotMask64_95;
   ADT_Int32 Port3SlotMask96_127;

   ADT_Int16 TxDataDelay1, TxDataDelay2, TxDataDelay3;
   ADT_Int16 RxDataDelay1, RxDataDelay2, RxDataDelay3;
   ADT_Int16 ClkDiv1,      ClkDiv2,      ClkDiv3;
   ADT_Int16 FramePeriod1, FramePeriod2, FramePeriod3;
   ADT_Int16 FrameWidth1,  FrameWidth2,  FrameWidth3;
   GpakCompandModes Compand1,     Compand2,  Compand3;
   GpakActivation   SampRateGen1, SampRateGen2, SampRateGen3; 

   ADT_UInt16 txFrameSyncPolarity1, txFrameSyncPolarity2, txFrameSyncPolarity3;
   ADT_UInt16 rxFrameSyncPolarity1, rxFrameSyncPolarity2, rxFrameSyncPolarity3;
   ADT_UInt16 txClockPolarity1, txClockPolarity2, txClockPolarity3;
   ADT_UInt16 rxClockPolarity1, rxClockPolarity2, rxClockPolarity3;

}GpakMcBspPortConfig_t;

#endif

#ifdef PORT_CFG_TSIP
// Each slotMask entry is a 32-bit mask that corresponds to 32 consective time slots on a given TSIP link.
// The number of links (serial port pins) connected to the TSIP is configurable.
//
// One bit for each corresponding time slot that the DSP must process must be enabled in order for the DSP 
// to transfer data on that wire.
//
// The following table can be used to determine the starting offset into the slotMask array that corresponds to
// the first 32 slots (0-31) on that link.
//
//                      -----------  Link -----------
//  DataRate (Mbps)     0   1   2   3   4   5   6   7
//        8             0   4   8  12  16  20  24  28
//       16             0   8  16  24   x   x   x   x
//       32             0  16   x   x   x   x   x   x

//
// Thus for an 8 Mbps clocking rate: link 3's slot map starts at &slotMaskx[12]
// for a 32 Mbps clocking rate: link 1's slot map starts at &slotMaskx[16]
//
// The timeslot companding is selected in 8-slot chunks via the slotCompand[]
// array. 
//      A 0-value specifies U-law companding, a 1-value specifies A-law 
//
//  slotCompand[0].Bit[0] sets companding for slots 0..7
//  slotCompand[0].Bit[1] sets companding for slots 8..15    ...
//  slotCompand[1].Bit[0] sets companding for slots 256..263 ...
//  slotCompand[3].Bit[0] sets companding for slots 768..775 ...
//
typedef struct GpakPortConfig_t {

    // Tsip 1 configuration
    GpakActivation  TsipEnable1;     // Port Enable/Disable Flag
    ADT_UInt32      slotMask1[32]; // timeslot enable/disable bit masks...see above
    GpakActivation  singleClk1;      // when Enabled: frame sync and serial clock signals are shared by the transmit and receive interfaces.
                                     // when Disabled: one framesync and serial clock for transmit, and a second framesync and clock for receive
    fsPolarity      txFsPolarity1;  // tx framesync polarity 
    clkEdge         txDataClkEdge1; // clock edge used to clock tx data
    clkEdge         txFsClkEdge1;   // clock edge used to sample tx framesync
    clkMode         txClkMode1;     // tx clocking mode               (specifies tx and rx when redundant mode is active)
    dataRate        txDataRate1;    // tx data rate                   (specifies tx and rx when redundant mode is active)
    clkFsSrc        txClkFsSrc1;    // tx clock and frame sync source (specifies tx and rx when redundant mode is active)
    ADT_Int16       txDatDly1;      // Transmit Data Delay: 
                                    // TXDLY = txDatDly + 0.5 When clock polarity for data and frame sync differs
                                    // TXDLY = txDatDly + 1 When clock polarity for data and frame sync is identical
                                    // The transmit data delay (TXDLY) determines the delay,
                                    // in sample clock periods, from the clock edge sampling the frame
                                    // synchronization signal to the clock edge that starts driving the first data bit of the first timeslot. A
                                    // txDatDly value of 0 represents a delay of one clock period from the clock edge sampling the frame
                                    // synchronization signal to the clock edge that starts driving the first data bit of the first timeslot when the
                                    // same clock polarity is used for both events. If opposite clock edges are used for these two events, an
                                    // txDatDly value of 0 represents a delay of half a clock period from the clock edge sampling the frame
                                    // synchronization signal to the clock edge that starts driving the first data bit.
    txDisState      txDriveState1;  // tx data line output drive state for disabled timeslots
    GpakActivation  txOutputDly1;   // when Enabled: the tx output of an enabled timeslot (that follows a high-z timeslot) is delayed by 0.5 serial clock period
                                    // when Disabled: output delay is disabled
    fsPolarity      rxFsPolarity1;  // rx framesync polarity 
    clkEdge         rxDataClkEdge1; // clock edge used to clock rx data
    clkEdge         rxFsClkEdge1;   // clock edge used to sample rx framesync
    clkMode         rxClkMode1;     // rx clocking mode (single or double) (only valid when redundant mode is inactive)
    dataRate        rxDataRate1;    // rx data rate                        (only valid when redundant mode is inactive)
    clkFsSrc        rxClkFsSrc1;    // tx clock and frame sync source      (only valid when redundant mode is inactive)
    ADT_Int16       rxDatDly1;      // Receive Data Delay: 
                                    // RXDLY = rxDatDly + 1.5 When clock polarity for data and frame sync differs
                                    // RXDLY = rxDatDly + 2 When clock polarity for data and frame sync is identical
                                    // The receive data delay (RXDLY) determines the delay, in sample clock periods, from the
                                    // clock edge sampling the frame synchronization signal to the clock edge that samples the first data bit of
                                    // the first timeslot. A rxDatDly  value of 0 represents a delay of two clock periods from the clock edge
                                    // sampling the frame synchronization signal to the clock edge that samples the first data bit of the first
                                    // timeslot when the same clock polarity is used for both events. If opposite clock edges are used for these
                                    // two events, a rxDatDly value of 0 represents a delay of one and a half clock periods from the clock
                                    // edge sampling the frame synchronization signal to the clock edge that samples the first data bit.
    loopbackMode    loopBack1;      // Loopback  mode
    ADT_UInt32      slotCompand1[4]; // Timeslot Companding mask: 0==U-law, 1==A-law    See above.

    // Tsip 2 configuration
    GpakActivation  TsipEnable2;     
    ADT_UInt32      slotMask2[32];   
    GpakActivation  singleClk2;      
    fsPolarity      txFsPolarity2;  
    clkEdge         txDataClkEdge2; 
    clkEdge         txFsClkEdge2;   
    clkMode         txClkMode2;     
    dataRate        txDataRate2;    
    clkFsSrc        txClkFsSrc2;    
    ADT_Int16       txDatDly2;      
    txDisState      txDriveState2;  
    GpakActivation  txOutputDly2;   
    fsPolarity      rxFsPolarity2;  
    clkEdge         rxDataClkEdge2; 
    clkEdge         rxFsClkEdge2;   
    clkMode         rxClkMode2;     
    dataRate        rxDataRate2;    
    clkFsSrc        rxClkFsSrc2;    
    ADT_Int16       rxDatDly2;      
    loopbackMode    loopBack2;
    ADT_UInt32      slotCompand2[4]; // Timeslot Companding mask: 0==U-law, 1==A-law    See above.

    // Tsip 3 configuration
    GpakActivation  TsipEnable3;    
    ADT_UInt32      slotMask3[32];  
    GpakActivation  singleClk3;     
    fsPolarity      txFsPolarity3;  
    clkEdge         txDataClkEdge3; 
    clkEdge         txFsClkEdge3;   
    clkMode         txClkMode3;     
    dataRate        txDataRate3;    
    clkFsSrc        txClkFsSrc3;    
    ADT_Int16       txDatDly3;      
    txDisState      txDriveState3;  
    GpakActivation  txOutputDly3;   
    fsPolarity      rxFsPolarity3;  
    clkEdge         rxDataClkEdge3; 
    clkEdge         rxFsClkEdge3;   
    clkMode         rxClkMode3;     
    dataRate        rxDataRate3;    
    clkFsSrc        rxClkFsSrc3;    
    ADT_Int16       rxDatDly3;      
    loopbackMode    loopBack3;
    ADT_UInt32      slotCompand3[4]; // Timeslot Companding mask: 0==U-law, 1==A-law    See above.
} GpakPortConfig_t;

#endif

#ifdef PORT_CFG_MCASP_64
typedef struct GpakPortConfig_t {
   ADT_UInt16 PortID;          // 0 - McASP0, 1 - McASP1, ...
   ADT_UInt16 SlotsPerFrame;   // 0 - Port inactive; 1-32 slot count
   ADT_UInt16 BitsPerSample;   // 8 or 16
   ADT_UInt16 BitsPerSlot;     // 8, 16, or 24

   ADT_UInt16 InputPinMap;    // bit set to 1 indicates pin is input
   ADT_UInt16 OutputPinMap;   // bit set to 1 indicates pin is output
   ADT_UInt32 ActiveSlotMap;  // bit set to 1 indicates slot is processed by McASP

   ADT_Bool   GenClocks;         // 1 - McASP supplies clock and frame sync signals
   ADT_Bool   HiResClockInvert;  // 1 - Invert high resolution clock when generating bit clock
   ADT_UInt16 HiResClockDivisor; // 1-4096
   ADT_UInt16 ClockDivisor;      // 1-32
   ADT_UInt16 DataDelay;         // 0-2
   ADT_Bool   ClkRxRising;       // 1 - clock in receive on rising clock edge
   ADT_Bool   SharedClk;         // 1 - Tx and Rx clock share same pin

   ADT_Bool   FsWord;            // 1 - frame sync lasts 1 timeslot. 0 - frame sync lasts 1 bit.
   ADT_Bool   FsFalling;         // 1 - frame sync starts on falling edge.  0 - rising edge
} GpakPortConfig_t;
#endif

/*
 * gpakConfigurePorts - Configure a DSP's serial ports.
 *
 * FUNCTION
 *  This function configures a DSP's serial ports.
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
extern GpakApiStatus_t gpakSetTSIPMode (
   ADT_Int32 DspId, 
   ADT_UInt16 sampleSizeI8,     // Number of bytes per sample size (1 or 2)
   ADT_UInt16 samplingRatekHz,  // Virtual sampling rate in kHz (8 or 16).
   ADT_UInt16 compandedData     // Data on the TDM bus is companded
   );
extern GpakApiStatus_t gpakSetTDMMode (
   ADT_Int32 DspId, 
   ADT_UInt16 samplingRatekHz  // sampling rate in kHz (8 or 16).
   );
extern GpakApiStatus_t gpakConfigurePorts (
    ADT_UInt32 DspId,         /* DSP Id (0 to MaxDSPCores-1) */
    GpakPortConfig_t *PortCfg,      /* pointer to Port Config info */
    GPAK_PortConfigStat_t *Status   /* pointer to Port Config Status */
    );
extern ADT_UInt32 gpakFormatPortConfig (ADT_UInt16 *Msg, GpakPortConfig_t *PortCfg);


//}===============================================================
//    Channel setup and teardown
//{===============================================================
//
//-----  Channel setup structures
//
//  PCM to PCM channel config
typedef struct PcmPcmCfg_t {
   GpakSerialPort_t    InPortA;            /* Input A  Serial Port Id */
   ADT_UInt16          InSlotA;            /*          Time Slot      */
   ADT_UInt16          InPinA;             /*          Serial Pin     */
   GpakSerialPort_t    OutPortA;           /* Output A Serial Port Id */
   ADT_UInt16          OutSlotA;           /*          Time Slot */
   ADT_UInt16          OutPinA;            /*          Serial Pin  */
   GpakSerialPort_t    InPortB;            /* Input B  Serial Port Id */
   ADT_UInt16          InSlotB;            /*          Time Slot */
   ADT_UInt16          InPinB;             /*          Serial Pin  */
   GpakSerialPort_t    OutPortB;           /* Output B Serial Port Id */
   ADT_UInt16          OutSlotB;           /*          Time Slot */
   ADT_UInt16          OutPinB;            /*          Serial Pin  */
   GpakActivation      EcanEnableA;        /* Input A Echo Cancel enable */
   GpakActivation      EcanEnableB;        /* Input B Echo Cancel enable */
   GpakActivation      AECEcanEnableA;     /* Acoustic Echo Cancel enable */
   GpakActivation      AECEcanEnableB;     /* Acoustic Echo Cancel enable */
   GpakFrameHalfMS     FrameHalfMS;        /* Frame Size */
   GpakActivation      NoiseSuppressA;     /* Output A Noise suppression */
   GpakActivation      NoiseSuppressB;     /* Output B Noise suppression */

   GpakActivation      AgcEnableA;          /* Output A AGC enable */
   GpakActivation      AgcEnableB;          /*          AGC enable */
   GpakToneTypes       ToneTypesA;          /* Input A Tone Detect Types */
   GpakToneTypes       ToneTypesB;          /*         Tone Detect Types */
   GpakCidMode_t       CIdModeA;            /* Caller Id mode */
   GpakCidMode_t       CIdModeB;            /* Caller Id mode */
   GpakChannelTypes    Coding;              /* VoiceChannel or DataChannel */
   ADT_Int16           ToneGenGainG1A;      /* Gain Control Block 1A */
   ADT_Int16           OutputGainG2A;       /* Gain Control Block 2A */
   ADT_Int16           InputGainG3A;        /* Gain Control Block 3A */
   ADT_Int16           ToneGenGainG1B;      /* Gain Control Block 1B */
   ADT_Int16           OutputGainG2B;       /* Gain Control Block 2B */
   ADT_Int16           InputGainG3B;        /* Gain Control Block 3B */
} PcmPcmCfg_t;

//  PCM to PKT channel config
typedef struct PcmPktCfg_t {
   GpakSerialPort_t    PcmInPort;          /* PCM Input  Serial Port Id */
   ADT_UInt16          PcmInSlot;          /*            Time Slot */
   ADT_UInt16          PcmInPin;           /*            Serial Pin  */
   GpakSerialPort_t    PcmOutPort;         /* PCM Output Serial Port Id */
   ADT_UInt16          PcmOutSlot;         /*            Time Slot */
   ADT_UInt16          PcmOutPin;          /*            Serial Pin  */
   GpakCodecs          PktInCoding;        /* Packet In  Coding */
   GpakFrameHalfMS     PktInFrameHalfMS;   /*            Frame Size */
   GpakCodecs          PktOutCoding;       /* Packet Out Coding */
   GpakFrameHalfMS     PktOutFrameHalfMS;  /*            Frame Size */
   GpakActivation      PcmEcanEnable;      /* PCM Echo Cancel enable */
   GpakActivation      PktEcanEnable;      /* Packet Echo Cancel enable */
   GpakActivation      AECEcanEnable;      /* Acoustic Echo Cancel enable */
   GpakActivation      VadEnable;          /* VAD enable */
   GpakActivation      AgcEnable;          /* AGC enable on pcmToPkt*/
   GpakToneTypes       ToneTypes;          /* Tone Detect Types on pcm Tx path*/
   GpakCidMode_t       CIdMode;            /* Caller Id mode */
   GpakFaxMode_t       FaxMode;            /* T38 Fax Mode */
   GpakFaxTransport_t  FaxTransport;       /* T.38 Fax Transport Mode */
   GpakChannelTypes    Coding;             /* VoiceChannel or DataChannel */
   ADT_Int16           ToneGenGainG1;      /* Gain Control Block 1B */
   ADT_Int16           OutputGainG2;       /* Gain Control Block 2B */
   ADT_Int16           InputGainG3;        /* Gain Control Block 3B */
   GpakActivation      DtxEnable;          /* Enables packet Dtx */
#ifdef DSP_66x
   GpakActivation      AgcEnablePkt2Pcm;   /* AGC enable on pkt rx path*/
   GpakActivation      VadEnablePkt;       /* Vad enable on pkt rx path*/
   GpakToneTypes       ToneTypesPkt;       /* Tone Detect Types on pkt rx path*/
#endif
} PcmPktCfg_t;

// PKT to PKT channel config
typedef struct PktPktCfg_t {
   GpakCodecs          PktInCodingA;       /* Packet In  A Coding */
   GpakFrameHalfMS     PktInFrameHalfMSA;  /*            A Frame Size */
   GpakCodecs          PktOutCodingA;      /* Packet Out A Coding */
   GpakFrameHalfMS     PktOutFrameHalfMSA; /*            A Frame Size */
   GpakActivation      EcanEnableA;        /* Pkt A Echo Cancel enable */
   GpakActivation      VadEnableA;         /*       VAD enable */
   GpakToneTypes       ToneTypesA;         /*       Tone Detect Types */
   GpakActivation      AgcEnableA;         /*       AGC Enable */
   ADT_Int16           OutputGainG2A;      /* Gain Control Block 2A */
   ADT_Int16           InputGainG3A;       /* Gain Control Block 3A */
   GpakActivation      DtxEnableA;         /* Enables A packet Dtx */

   ADT_UInt16          ChannelIdB;         /* Channel Id B */
   GpakCodecs          PktInCodingB;       /* Packet In B Coding */
   GpakFrameHalfMS     PktInFrameHalfMSB;  /*             Frame Size */
   GpakCodecs          PktOutCodingB;      /* Packet Out B Coding */
   GpakFrameHalfMS     PktOutFrameHalfMSB; /*              Frame Size */
   GpakActivation      EcanEnableB;        /* Pkt B Echo Cancel enable */
   GpakActivation      VadEnableB;         /*       VAD enable */
   GpakToneTypes       ToneTypesB;         /*       Tone Detect Types */
   GpakActivation      AgcEnableB;         /*       AGC Enable */
   ADT_Int16           OutputGainG2B;      /* Gain Control Block 2B */
   ADT_Int16           InputGainG3B;       /* Gain Control Block 3B */
   GpakActivation      DtxEnableB;         /* Enables B packet Dtx */
} PktPktCfg_t;

// Ciruit data channel config
typedef struct CircuitCfg_t {
   GpakSerialPort_t    PcmInPort;          /* PCM Input Serial Port Id */
   ADT_UInt16  PcmInSlot;                  /*           Time Slot */
   ADT_UInt16  PcmInPin;                   /*           Serializer  */
   GpakSerialPort_t    PcmOutPort;         /* PCM Output Serial Port Id */
   ADT_UInt16  PcmOutSlot;                 /*            Time Slot */
   ADT_UInt16  PcmOutPin;                  /*            Serializer */
   ADT_UInt16  MuxFactor;                  /* Circuit Mux Factor */
} CircuitCfg_t;

// Conference to PCM channel config
typedef struct CnfPcmCfg_t {
   ADT_UInt16          ConferenceId;       /* Conference Identifier */
   GpakSerialPort_t    PcmInPort;          /* PCM Input  Serial Port Id */
   ADT_UInt16          PcmInSlot;          /*            Time Slot */
   ADT_UInt16          PcmInPin;           /*            Pin  */

   GpakSerialPort_t    PcmOutPort;         /* PCM Output Serial Port Id */
   ADT_UInt16          PcmOutSlot;         /*            Time Slot */
   ADT_UInt16          PcmOutPin;          /*            Pin  */

   GpakActivation      EcanEnable;         /* Echo Cancel enable */
   GpakActivation      AECEcanEnable;      /* Acoustic Echo Cancel enable */
   GpakActivation      AgcInEnable;        /* Input AGC enable */
   GpakActivation      AgcOutEnable;       /* Output AGC enable */

   GpakToneTypes       ToneTypes;          /* Tone Detect Types */
   ADT_Int16           ToneGenGainG1;      /* Gain Control Block 1 */
   ADT_Int16           OutputGainG2;       /* Gain Control Block 2 */
   ADT_Int16           InputGainG3;        /* Gain Control Block 3 */

} CnfPcmCfg_t;

// Conference to PKT channel config
typedef struct CnfPktCfg_t {
   ADT_UInt16          ConferenceId;       /* Conference Identifier */
   GpakCodecs          PktInCoding;        /* Packet In Coding */
   GpakCodecs          PktOutCoding;       /* Packet Out Coding */
   GpakActivation      EcanEnable;         /* Echo Cancel enable */
   GpakActivation      VadEnable;          /* VAD enable */
   GpakActivation      AgcInEnable;        /* Input AGC enable */
   GpakActivation      AgcOutEnable;       /* Output AGC enable */
   GpakToneTypes       ToneTypes;          /* Tone Detect Types */
   ADT_Int16           ToneGenGainG1;      /* Gain Control Block 1 */
   ADT_Int16           OutputGainG2;       /* Gain Control Block 2 */
   ADT_Int16           InputGainG3;        /* Gain Control Block 3 */
   GpakFrameHalfMS     PktFrameHalfMS;     /* Pkt Frame Size */
   GpakActivation      DtxEnable;          /* Enables packet Dtx */

} CnfPktCfg_t;

// Composite conference channel config
typedef struct CnfCmpCfg_t {
   ADT_UInt16          ConferenceId;       /* Conference Identifier */
   GpakSerialPort_t    PcmOutPort;         /* PCM Output Serial Port Id */
   ADT_UInt16          PcmOutSlot;         /*            Time Slot */
   ADT_UInt16          PcmOutPin;          /*            Pin  */

   GpakCodecs          PktOutCoding;       /* Packet Out Coding */
   GpakActivation      VadEnable;          /* VAD enable */
   GpakActivation      DtxEnable;          /* Enables packet Dtx */
} CnfCmpCfg_t;

// Loopback Coder channel config
typedef struct LbCdrCgf_t {
   GpakCodecs          PktInCoding;        /* Packet In Coding */
   GpakCodecs          PktOutCoding;       /* Packet Out Coding */
   GpakFrameHalfMS     PktFrameHalfMS;     /* Pkt Frame Size */
} LbCdrCgf_t;

// Main channel configuration structure
typedef union GpakChannelConfig_t {

    PcmPktCfg_t PcmPkt;    /* PCM to Packet channel type. */
    PcmPcmCfg_t PcmPcm;    /* PCM to PCM channel type. */
    PktPktCfg_t PktPkt;    /* Packet to Packet channel type. */

    CircuitCfg_t Circuit;  /* Circuit Data channel type. */

    CnfPcmCfg_t ConferPcm;  /* Conference PCM channel type. */
    CnfPktCfg_t ConferPkt;  /* Conference Packet channel type. */
    CnfCmpCfg_t ConferComp; /* Conference Composite channel type. */
    LbCdrCgf_t  LpbkCoder;  /* loopback Coder channel type */

} GpakChannelConfig_t;


/*
 * gpakConfigureChannel - Configure a DSP's Channel.
 *
 * FUNCTION
 *  This function configures a DSP's Channel.
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
extern GpakApiStatus_t gpakConfigureChannel (
    ADT_UInt32 DspId,                    /* DSP Id (0 to MaxDSPCores-1) */
    ADT_UInt16 ChannelId,               /* Channel Id (0 to MaxChannelAlloc-1) */
    GpakChanType ChannelType,           /* Channel Type */
    GpakChannelConfig_t *pChanConfig,   /* pointer to Channel Config info */
    GPAK_ChannelConfigStat_t *pStatus   /* pointer to Channel Config Status */
    );

extern ADT_UInt32 gpakFormatChannelConfig (   /* Returns message length in 16-bit words */
    ADT_UInt16 *Msg,                          /* Message buffer */
    GpakChannelConfig_t *pChan,               /* pointer to Channel Config structure */
    ADT_UInt16 ChannelId,                     /* channel Id */
    GpakChanType ChannelType);                /* channel type */

    
/*
 *   gpakSendRTPCfgMsg 
 *
 * FUNCTION
 *  This function is used to configure a channel's RTP parameters.
 *
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
#define VLAN_NULL_IDX ((ADT_UInt8)(0xff))

typedef struct GpakRTPCfg_t {
   ADT_UInt16 JitterMode;        // Jitter buffer adaption mode
   ADT_UInt16 DelayTargetMinMS;  // Minimum delay (ms) at which target delay is recentered
   ADT_UInt16 DelayTargetMS;     // Initial delay (ms) assigned to jitter buffer
   ADT_UInt16 DelayTargetMaxMS;  // Maximum delay (ms) at which target delay is recentered

   ADT_UInt16 VoicePyldType;     // Payload type to assign to default voice codec payloads
   ADT_UInt16 CNGPyldType;       // Payload type to assign to comfort noise payloads
   ADT_UInt16 TonePyldType;      // Payload type to assign to tone relay payloads
   ADT_UInt16 T38PyldType;       // Payload type to assign to t.38 fax relay payloads

   ADT_UInt16 StartSequence;     // Channel's first transmitting sequence number

   ADT_UInt16 SrcPort;           // Local device's UDP port
   ADT_UInt32 InDestIP;          // Destination IP of in-bound packets (For multicast receives)
   ADT_UInt32 SSRC;              // Channel's transmitting SSRC
   ADT_UInt8  VlanIdx;           // VLAN Idx for RTP stream.  VLAN_NULL_IDX when VLAN is not used.
   ADT_UInt8  DSCP;              // DSCP value [0-63] to place in IP header's type of service field
 
   ADT_UInt8  DestMAC[6];        // Remote device's MAC address
   ADT_UInt32 DestIP;            // Remote device's IP address on in-bound packets
   ADT_UInt32 DestTxIP;          // Remote device's IP address on out-bound packets (for multicast transmits)
   ADT_UInt16 DestPort;          // Remote device's UDP port
   ADT_UInt32 DestSSRC;          // Channel's receiving SSRC

   ADT_UInt16 SamplingHz;        // Sampling rate in Hz of inbound payloads
} GpakRTPCfg_t;

typedef struct GpakIPv6_t {
   ADT_UInt8  IpV6Address[16];        // Jitter buffer adaption mode
}GpakIPv6_t;

typedef struct GpakRTPv6Cfg_t {
   ADT_UInt16 JitterMode;        // Jitter buffer adaption mode
   ADT_UInt16 DelayTargetMinMS;  // Minimum delay (ms) at which target delay is recentered
   ADT_UInt16 DelayTargetMS;     // Initial delay (ms) assigned to jitter buffer
   ADT_UInt16 DelayTargetMaxMS;  // Maximum delay (ms) at which target delay is recentered

   ADT_UInt16 VoicePyldType;     // Payload type to assign to default voice codec payloads
   ADT_UInt16 CNGPyldType;       // Payload type to assign to comfort noise payloads
   ADT_UInt16 TonePyldType;      // Payload type to assign to tone relay payloads
   ADT_UInt16 T38PyldType;       // Payload type to assign to t.38 fax relay payloads

   ADT_UInt16 StartSequence;     // Channel's first transmitting sequence number

   ADT_UInt16 SrcPort;           // Local device's UDP port
   ADT_UInt8  InDestIP[16];      // Destination IP of in-bound packets (For multicast receives)
   ADT_UInt32 SSRC;              // Channel's transmitting SSRC
   ADT_UInt8  VlanIdx;           // VLAN Idx for RTP stream.  VLAN_NULL_IDX when VLAN is not used.
   ADT_UInt8  DSCP;              // DSCP value [0-63] to place in IP header's type of service field
 
   ADT_UInt8  DestMAC[6];        // Remote device's MAC address
   ADT_UInt8  DestIP[16];        // Remote device's IP address on in-bound packets
   ADT_UInt8  DestTxIP[16];      // Remote device's IP address on out-bound packets (for multicast transmits)
   ADT_UInt16 DestPort;          // Remote device's UDP port
   ADT_UInt32 DestSSRC;          // Channel's receiving SSRC

   ADT_UInt16 SamplingHz;        // Sampling rate in Hz of inbound payloads
} GpakRTPv6Cfg_t;
extern GpakApiStatus_t gpakSendRTPMsg (
   ADT_UInt32            DspId,       /* DSP identifier */
   ADT_UInt16            ChannelId,   /* Channel identifier */
   GpakRTPCfg_t         *Cfg,         /* Pointer to RTP configuration data */
   GPAK_RTPConfigStat_t *dspStatus);   /* Pointer to DSP status reply */

extern GpakApiStatus_t gpakSendRTPv6Msg (
   ADT_UInt32            DspId,         /* DSP identifier */
   ADT_UInt16            ChannelId,     /* Channel identifier */
   GpakRTPv6Cfg_t        *Cfg,          /* Pointer to RTP configuration data */
   GPAK_RTPConfigStat_t *dspStatus);    /* Pointer to DSP status reply */
extern ADT_UInt32 gpakFormatRTPMsg (
      ADT_UInt16 *Msg,                   /* pointer to message buffer */
      ADT_UInt16  ChannelId,             /* Channel identifier */
      GpakRTPCfg_t *Cfg);           /* Pointer to configuration structure */
extern ADT_UInt32 gpakFormatRTPv6Msg (
      ADT_UInt16 *Msg,                  /* pointer to message buffer */
      ADT_UInt16  ChannelId,            /* Channel identifier */
      GpakRTPv6Cfg_t *Cfg);             /* Pointer to configuration structure */

/*
 *   gpakSendRTP_RTCPCfgMsg 
 *
 * FUNCTION
 *  This function is used to configure a channel's RTP and RTCP parameters.
 *
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
#define MAX_SDES_ITEMS 8

typedef enum SDES_ITEM_ID {
   RTCP_SDES_END = 0,    RTCP_SDES_CNAME,
   RTCP_SDES_NAME,       RTCP_SDES_EMAIL,
   RTCP_SDES_PHONE,      RTCP_SDES_LOC,
   RTCP_SDES_TOOL,       RTCP_SDES_NOTE,
   RTCP_SDES_PRIV
} SDES_ITEM_ID;

typedef struct SDES_CFG_ITEM {
   ADT_UInt8 type;             // SDES type (see SDES_ITEM_ID)
   ADT_UInt8 firstPktInCycle;  // First packet of pktsPerCycle packets to send pkt on
   ADT_UInt8 txInterval;       // Number of packets to skip between transmissions
   ADT_UInt8 dataBytes;        // Size of text data in bytes
   ADT_UInt8 *textData;        // Pointer to text data for type
} SDES_CFG_ITEM;

typedef struct SDES_CFG {
   ADT_UInt8 itemCnt;          // Number of SDES items configured
   ADT_UInt8 pktsPerCycle;     // Number of packets to form a cycle
   SDES_CFG_ITEM item[MAX_SDES_ITEMS];
} SDES_CFG;

typedef struct GpakRTCPCfg_t {
   ADT_UInt16 localRtcpPort;             // local Udp port for RTCP sesion 
   ADT_UInt16 remoteRtcpPort;            // remote Udp port for RTCP sesion
   ADT_UInt16 transportOverheadBytes;    // Overhead (in bytes) added to RTCP packets due to transport layer headers
   ADT_UInt16 timeoutMultiplier;         // See RFC 3550; section 6.3.5; M
                                         //         - Multiplier to allocate for timeout 
                                         //         - Default is 5
   ADT_UInt32 minRTCPPktPeriodMs;        // See RFC 3550; section 6.3.1; Tmin
                                         //         - Minimum period (milliseconds) between RTCP transmissions
                                         //         - Default is 5000
   ADT_UInt32 rtcpBandwidthBytesPerSec;  // See RFC 3550; section 6.3; rtcp_bw * 'session bandwidth'
                                         //         - Bandwidth (bytes per second) to allocate to RTCP traffic
   ADT_Float32 senderBandwidthFraction;  // See RFC 3550; section 6.2; S / (S + R)
                                         //         - Fraction (0.0 to 1.0) of RTCP bandwidth to allocate to senders
                                         //         - Default is .25
   SDES_CFG sdes;                        // Source descriptor information.
} GpakRTCPCfg_t;

extern GpakApiStatus_t gpakSendRTP_RTCPMsg (
   ADT_UInt32            DspId,       /* DSP identifier */
   ADT_UInt16            ChannelId,   /* Channel identifier */
   GpakRTPCfg_t         *Cfg,         /* Pointer to RTP configuration data */
   GpakRTCPCfg_t        *RtcpCfg,     /* Pointer to RTCP configuration data */
   GPAK_RTPConfigStat_t *dspStatus);   /* Pointer to DSP status reply */

extern ADT_UInt32 gpakFormatRTP_RTCPMsg (
      ADT_UInt16 *Msg,                   /* pointer to message buffer */
      ADT_UInt16  ChannelId,             /* Channel identifier */
      GpakRTPCfg_t *Cfg,           /* Pointer to RTP configuration structure */
      GpakRTCPCfg_t *RtcpCfg);        /* Pointer to RTCP configuration data */
extern gpakRtpStatus_t gpakSendRTPv6_RTCPMsg (
	ADT_UInt32           DspId,        /* DSP identifier */
	ADT_UInt16           ChannelId,    /* Channel identifier */                           
	GpakRTPv6Cfg_t       *Cfg,         /* Pointer to RTP configuration data */
	GpakRTCPCfg_t        *RtcpCfg,     /* Pointer to RTCP configuration data */
	GPAK_RTPConfigStat_t *dspStatus);  /* Pointer to DSP status reply */
extern ADT_UInt32 gpakFormatRTPv6_RTCPMsg (
      ADT_UInt16 *Msg,                /* pointer to message buffer */
      ADT_UInt16  ChannelId,          /* Channel identifier */
      GpakRTPv6Cfg_t *Cfg,            /* Pointer to RTP configuration structure */
      GpakRTCPCfg_t *RtcpCfg);     /* Pointer to RTCP configuration data */


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakSendRTCPByeMsg 
 *
 * FUNCTION
 *    Send an RTCP bye message
 *
 * Inputs
 *      DspId  - Dsp identifier 
 * ChannelId   - Channel identifier
 * ByeMsg      - Pointer to buffer containing the bye msg data
 * ByeMsgI8    - Number of data bytes in the bye msg
 *
 * Outputs
 *    pStatus  - status from DSP
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */

ADT_UInt32  gpakFormatRTCPByeMsg (
   ADT_UInt16 *Msg,  
   ADT_UInt16  ChannelId, 
   ADT_UInt8 *ByeMsg, 
   ADT_UInt8 ByeMsgLenI8);

GpakApiStatus_t  gpakSendRTCPByeMsg (
   ADT_UInt32 DspId,  
   ADT_UInt16 ChannelId, 
   ADT_UInt8 *ByeMsg, 
   ADT_UInt8 ByeMsgLenI8, 
   GPAK_RTCPBye_t *dspStatus);
      
extern int (*customChannelConfig) (ADT_UInt16 *Msg, void *chanCfg, ADT_UInt16 ChannelId);


typedef struct GpakSRTPCfg_t {
   GpakFrameHalfMS FrameHalfMS;
   struct SRTPEncode {   // Encode direction
      GpakSrtpEncryptType_t EncryptionScheme;          // Encryption scheme
      GpakSrtpAuthType_t    AuthenticationScheme;      // Authentication scheme
      ADT_UInt8  AuthenticationI8; // Authentication tag length
      ADT_UInt8  MasterKey[32];    // Initial master keys
      ADT_UInt8  MasterSalt[14];   // Initial master salts
      ADT_UInt16 MasterKeyLife[3]; // 48-bit lifetime of master key
      ADT_UInt8  MKI[4];           // Master key identifier
      ADT_UInt8  MKI_I8;           // Number of bytes for MKI.
      ADT_UInt8  KDR;              // Key derivation rate (See RFC3711) 
	  ADT_UInt16 KeySizeU8;
   } Encode;
   struct SRTPDecide {   // Decode direction
      GpakSrtpEncryptType_t EncryptionScheme;          // Encryption scheme
      GpakSrtpAuthType_t    AuthenticationScheme;      // Authentication scheme
      ADT_UInt8  AuthenticationI8; // Authentication tag length
      ADT_UInt8  MasterKey[32];    // Initial master keys
      ADT_UInt8  MasterSalt[14];   // Initial master salts
      ADT_UInt16 MasterKeyLife[3]; // 48-bit lifetime of master key
      ADT_UInt8  MKI[4];           // Master key identifier
      ADT_UInt8  MKI_I8;           // Number of bytes for MKI.
      ADT_UInt8  KDR;              // Key derivation rate (See RFC3711)
      ADT_UInt16 ROC;              // Initial rollover count
	  ADT_UInt16 KeySizeU8;
   } Decode;
} GpakSRTPCfg_t;

extern GpakApiStatus_t gpakConfigSRTP (
   ADT_UInt32       DspId,       /* DSP identifier */
   ADT_UInt16       ChannelId,   /* Channel identifier */
   GpakSRTPCfg_t   *Cfg,         /* Pointer to RTP configuration data */
   GpakSrtpStatus_t *dspStatus);  /* Pointer to DSP's reply status */

extern ADT_UInt32 gpakFormatSRTPCfg (
      ADT_UInt16 *Msg,                   /* pointer to message buffer */
      ADT_UInt16  ChannelId,             /* Channel identifier */
      GpakSRTPCfg_t *Cfg);           /* Pointer to configuration structure */

      
typedef struct GpakSRTPKey_t {
   GpakDeviceSide_t Direction;
   ADT_UInt8  MasterKey[32];    // Initial master keys
   ADT_UInt8  MasterSalt[14];   // Initial master salts
   ADT_UInt16 MasterKeyLife[3]; // 48-bit lifetime of master key
   ADT_UInt8  MKI[4];           // Master key identifier
   ADT_UInt8  MKI_I8;           // Number of bytes for MKI.
   ADT_UInt8  KDR;              // Key derivation rate (See RFC3711) 
   ADT_UInt16 KeySizeU8;
} GpakSRTPKey_t;

extern GpakApiStatus_t gpakSRTPNewKey (
   ADT_UInt32       DspId,       /* DSP identifier */
   ADT_UInt16       ChannelId,   /* Channel identifier */
   GpakSRTPKey_t   *Cfg,         /* Pointer to RTP configuration data */
   GpakSrtpStatus_t *dspStatus);  /* Pointer to DSP's reply status */

extern ADT_UInt32 gpakFormatSRTPNewKey (
      ADT_UInt16 *Msg,               /* pointer to message buffer */
      ADT_UInt16  ChannelId,         /* Channel identifier */
      GpakSRTPKey_t *Cfg);           /* Pointer to configuration structure */

/*
 * gpakTearDownChannel - Tear Down a DSP's Channel.
 *
 * FUNCTION
 *  This function tears down a DSP's Channel.
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */

extern GpakApiStatus_t gpakTearDownChannel (
    ADT_UInt32 DspId,                    /* DSP Id (0 to MaxDSPCores-1) */
    ADT_UInt16 ChannelId,               /* Channel Id (0 to MaxChannelAlloc-1) */
    GPAK_TearDownChanStat_t *pStatus    /* pointer to Tear Down Status */
    );


//}===============================================================
//    Channel status 
//{===============================================================

//-------  Channel status
typedef struct GpakChannelStatus_t {
    ADT_UInt16 ChannelId;          /* Channel Identifier */
    GpakChanType ChannelType;      /* Channel Type */
    ADT_UInt16 NumPktsToDsp;       /* number of input packets recvd. by DSP */
    ADT_UInt16 NumPktsFromDsp;     /* number of output packets sent by DSP */
    ADT_UInt16 NumPktsToDspB;      /* number of input packets recvd. by DSP (B-side of pkt-pkt only) */
    ADT_UInt16 NumPktsFromDspB;    /* number of output packets sent by DSP (B-side of pkt-pkt only) */
    ADT_UInt16 BadPktHeaderCnt;    /* Invalid Packet Headers count */
    ADT_UInt16 PktOverflowCnt;     /* Packet Overflow count */
    ADT_UInt16 PktUnderflowCnt;    /* Packet Underflow count */
    ADT_UInt16 StatusFlags;        /* Status Flag bits */
    GpakChannelConfig_t ChannelConfig;     /* Channel Configuration */
} GpakChannelStatus_t;


/*
 * gpakGetChannelStatus - Read a DSP's Channel Status.
 *
 * FUNCTION
 *  This function reads a DSP's Channel Status information for a particular
 *  channel.
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
extern GpakApiStatus_t gpakGetChannelStatus (
    ADT_UInt32 DspId,                  /* DSP Identifier (0 to MaxDSPCores-1) */
    ADT_UInt16 ChannelId,             /* Channel Id (0 to MaxChannelAlloc-1) */
    GpakChannelStatus_t *pChanStatus, /* pointer to Channel Status info var */
    GPAK_ChannelStatusStat_t *pStatus /* pointer to Channel Status Status */
    );

extern GpakApiStatus_t gpakParseChanStatusResponse (
    ADT_UInt32 ReplyLength,                 /* length of response */
    ADT_UInt16 *pMsgBuffer,                  /* pointer to response buffer */
    GpakChannelStatus_t *pChanStatus,      /* pointer to Channel Status info var */
    GPAK_ChannelStatusStat_t *pStatus);    /* pointer to Channel Status Status */ 

extern void (*customChannelStatusParse) (
    ADT_UInt16 *Msg,              // Pointer to gpak message buffer
    void *pChanStatus);           // custom channel status structure

typedef struct GPAK_RTP_STATS {
   ADT_UInt16 ChannelId;            // Channel index
   ADT_UInt32 PktsSent;             // Total packets sent
   ADT_UInt32 PktsRcvd;             // Total packets received
   float      LossPercentage;       // % packets never received
   ADT_UInt32 PktsPlayed;           // Packets played on time
   ADT_UInt32 PktsLate;             // Packets discarded due to lateness
   ADT_UInt32 PktsOutOfRange;       // Packets discarded due to invalid range of timestamps
   ADT_UInt32 BufferOverflowCnt;    // Count of times jitter buffer overflowed
   ADT_UInt32 BufferUnderflowCnt;   // Count of times jitter buffer underflowed

   float      JitterMinMS;          // Minimum jitter (MS) detected on 16 samples
   float      JitterCurrentMS;      // Current jitter (MS) on 16 samples
   float      JitterMaxMS;          // Maximum jitter (MS) detected on 16 samples

   ADT_UInt16 JitterMode;           // Jitter buffer mode
   ADT_UInt16 MinDelayMS;           // Minimum jitter buffer depth (MS) to trigger underflow
   ADT_UInt16 CurrentDelayMS;       // Current jitter buffer delay (MS)
   ADT_UInt16 MaxDelayMS;           // Maximum jitter buffer depth (MS) to trigger overflow
} GPAK_RTP_STATS;

// Valid flags indicate which rtcp stats are in the report
#define RR_VALID   0x0001
#define SR_VALID   0x0002
#define SDES_VALID 0x0004
#define BYE_VALID  0x0008
#define APP_VALID  0x0010
#define EXT_VALID  0x0020

//  RTCP Notify reporting structures
typedef struct sdesReportItem_t {
   SDES_ITEM_ID type;
   ADT_UInt8    dataI8;
   ADT_UInt8   *data;
} sdesReportItem_t;

typedef struct sdesReport_t {
   ADT_UInt8 itemCnt;
   sdesReportItem_t item[MAX_SDES_ITEMS];
} sdesReport_t;

typedef struct byeReport_t {
   ADT_UInt8 reasonI8;
   ADT_UInt8 *reason;
} byeReport_t;

typedef struct appReport_t {
   ADT_UInt32 blockI8;
   ADT_UInt8  *block;
} appReport_t;


typedef struct GPAK_RTCP_STATS {
   ADT_UInt16 validFlags;           // indicate which reports are valid
   float      rr_fractionPktsLost;  // receiver report: fraction packets lost since last RTCP report
   ADT_UInt32 rr_cumPktsLost;       // receiver report: far side cumulative packet lost count  
   ADT_UInt32 rr_JitterEstimate;    // receiver report: far side jitter estimate
   ADT_UInt32 rr_SSRC;              // receiver report: far side SSRC reporter
   ADT_UInt32 sr_PktsSent;          // sender report: far side packet sent count
   ADT_UInt32 sr_BytesSent;         // sender report: far side bytes sent count
   ADT_UInt32 sr_SSRC;              // sender report: far side SSRC reporter
   float roundTripTimeMs;           // round trip time on network between local sender and remote receiver
   sdesReport_t sdesReport;
   byeReport_t  byeReport;
   appReport_t  appReport;
} GPAK_RTCP_STATS;

extern GpakApiStatus_t gpakGetRTPStatistics (
   ADT_UInt32 DspId,          ADT_UInt16 ChannelId,
   GPAK_RTP_STATS *RTPStats,  GPAK_RTP_Stat_Rply_t *dspStatus);

extern GpakApiStatus_t gpakGetRTCPStatistics (
   ADT_UInt32 DspId,          ADT_UInt16 ChannelId,
   GPAK_RTP_STATS *RTPStats,  GPAK_RTCP_STATS *RTCPStats, GPAK_RTP_Stat_Rply_t *dspStatus);

extern gpakRtpStatus_t gpakWriteRtpRxTimeoutMsg (
   ADT_UInt32 DspId,         ADT_UInt16 ChannelId, 
   ADT_UInt16 TimeoutValue,  GPAK_RTP_Stat_Rply_t *dspStatus);

//}===============================================================
//    Channel control
//{===============================================================
//
//---  Acoustic echo canceller control
//
#if (defined(AEC_LIB_VERSION) && (AEC_LIB_VERSION >= 0x0420))
typedef struct GpakAECParms_t {
#if (AEC_LIB_VERSION >= 0x0430)
    GpakActivation antiHowlEnable;      // antiHowlEnable
#endif
    ADT_Int16 activeTailLengthMSec;     // activeTailLengthMSec
    ADT_Int16 totalTailLengthMSec;      // totalTailLengthMSec
    ADT_Int16 txNLPAggressiveness;      // txNLPAggressiveness
    ADT_Int16 maxTxLossSTdB;            // maxTxLossSTdB
    ADT_Int16 maxTxLossDTdB;            // maxTxLossDTdB
    ADT_Int16 maxRxLossdB;              // maxRxLossdB
    ADT_Int16 initialRxOutAttendB;      // initialRxOutAttendB
    ADT_Int16 targetResidualLeveldBm;   // targetResidualLeveldBm
    ADT_Int16 maxRxNoiseLeveldBm;       // maxRxNoiseLeveldBm
    ADT_Int16 worstExpectedERLdB;       // worstExpectedERLdB
    ADT_Int16 rxSaturateLeveldBm;       // rxSaturateLeveldBm
    ADT_Int16 noiseReduction1Setting;   // noiseReduction1Setting
    ADT_Int16 noiseReduction2Setting;   // noiseReduction2Setting
    GpakActivation cngEnable;           // cngEnable
    ADT_Int16 fixedGaindB10;            // fixedGaindB10
    GpakActivation txAGCEnable;         // txAGCEnable
    ADT_Int16 txAGCMaxGaindB;           // txAGCMaxGaindB
    ADT_Int16 txAGCMaxLossdB;           // txAGCMaxLossdB
    ADT_Int16 txAGCTargetLeveldBm;      // txAGCTargetLeveldBm
    ADT_Int16 txAGCLowSigThreshdBm;     // txAGCLowSigThreshdBm
    GpakActivation rxAGCEnable;         // rxAGCEnable
    ADT_Int16 rxAGCMaxGaindB;           // rxAGCMaxGaindB
    ADT_Int16 rxAGCMaxLossdB;           // rxAGCMaxLossdB
    ADT_Int16 rxAGCTargetLeveldBm;      // rxAGCTargetLeveldBm
    ADT_Int16 rxAGCLowSigThreshdBm;     // rxAGCLowSigThreshdBm
    GpakActivation rxBypassEnable;      // rxBypassEnable
    ADT_Int16 maxTrainingTimeMSec;      // maxTrainingTimeMSec
    ADT_Int16 trainingRxNoiseLeveldBm;  // trainingRxNoiseLeveldBm
} GpakAECParms_t;
#else
typedef struct GpakAECParms_t {
    ADT_Int16       activeTailLengthMSec;
    ADT_Int16       totalTailLengthMSec;
    ADT_Int16	    maxTxNLPThresholddB;	// TxNLP Threshold (dB)
    ADT_Int16	    maxTxLossdB;			// Maximum TxNLP Attenuation (dB)
    ADT_Int16	    maxRxLossdB;			// Maximum RxNLP Attenuation (dB)
    ADT_Int16	    targetResidualLeveldBm;	// Target TxNLP Level when suppressing (dBm)
    ADT_Int16	    maxRxNoiseLeveldBm;		// Maximum Added Low Level Noise (dBm)
    ADT_Int16	    worstExpectedERLdB;		// Worst Expected Echo Return Loss (dB)
    GpakActivation  noiseReductionEnable;	// Noise Reduction Enable Flag (0 = disable)
    GpakActivation  cngEnable;              // comfort noise enable (1 = enable)
    GpakActivation  agcEnable;				// AGC Enable Flag (0 = disable)
    ADT_Int16	    agcMaxGaindB;			// AGC Maximum Gain (dB)
    ADT_Int16	    agcMaxLossdB;			// AGC Maximum Loss (dB)
    ADT_Int16	    agcTargetLeveldBm;		// AGC Target Power Level (dBm)
    ADT_Int16	    agcLowSigThreshdBm;		// AGC Low Level Signal Threshold (dBm)
    ADT_UInt16	    maxTrainingTimeMSec;    // Maximum Training Duration (msec)
    ADT_Int16	    rxSaturateLeveldBm;		// Rx Saturation Threshold (dBm)
    ADT_Int16	    trainingRxNoiseLeveldBm; // Training Level (dBm)
    ADT_Int16	    fixedGaindB10;			 // Fixed Gain (.1 dB)
} GpakAECParms_t;
#endif

/*
 * gpakReadAECParms
 *
 * FUNCTION
 *  This function reads a DSP's AEC parameters information.
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
/* gpakGetChannelStatus return status. */

extern GpakApiStatus_t gpakReadAECParms (
    ADT_UInt32 DspId,            /* DSP Identifier (0 to MaxDSPCores-1) */
    GpakAECParms_t *AECParms    /* pointer to System Parms info var */
    );

/*
 * gpakWriteAECParms
 *
 * FUNCTION
 *  This function writes a DSP's acoustic ecbo cancellar parameters
 *
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
extern GpakApiStatus_t gpakWriteAECParms (
    ADT_UInt32           DspId,      /* DSP Identifier (0 to MaxDSPCores-1) */
    GpakAECParms_t      *AECParms,  /* pointer to AEC parameter structure  */
    GPAK_AECParmsStat_t *Status     /* pointer to AEC write status return  */
    );

//
//---  Noise reduction control
//
typedef struct GpakNoiseParams_t {
    /* VAD parameters. */
   ADT_Int16 LowThreshdB;        /* VAD Noise Floor Threshold (dBm) -96..0.  Level above
                                    which voice MAY be declared */
   ADT_Int16 HighThreshdB;       /* VAD Noise Ceiling Threshold (dBm) -96..0.  Level above
                                    which voice WILL be declared */
   ADT_Int16 HangMSec;           /* VAD Speech Hangover Time (msec) 0..32767 */
   ADT_Int16 WindowSize;         /* VAD Analysis Window Size (# samples) */


    /* Noise suppression parameters */
    ADT_Int16 MaxAtten;          /* Aggressiveness of noise suppression
                                       0  -  No suppression
                                      35  -  Most aggressive suppression  */
} GpakNoiseParams_t;

/*
 * gpakReadNoiseParms - Read a DSP's System Parameters.
 *
 * FUNCTION
 *  This function reads a DSP's System Parameters information.
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
extern GpakApiStatus_t gpakReadNoiseParms (
    ADT_UInt32 DspId,                  /* DSP Identifier (0 to MaxDSPCores-1) */
    GpakNoiseParams_t *pNoiseParms    /* pointer to System Parms info var */
    );


/*
 * gpakWriteNoiseParms - Write a DSP's Noise Parameters.
 *
 * FUNCTION
 *  This function writes a DSP's System Parameters information.
 *
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
extern GpakApiStatus_t gpakWriteNoiseParms (
    ADT_UInt32 DspId,                    /* DSP Identifier (0 to MaxDSPCores-1) */
    GpakNoiseParams_t   *pNoiseParms,   /* pointer to System Parms info var */
    GPAK_SysParmsStat_t *pStatus    /* pointer to Write System Parms Status */
    );



//}===============================================================
//    Conference configuration
//{===============================================================

/*
 * gpakConfigureConference - Configure a DSP's Conference.
 *
 * FUNCTION
 *  This function configures a DSP's Conference.
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
extern GpakApiStatus_t gpakConfigureConference (
    ADT_UInt32 DspId,                   /* DSP Id (0 to MaxDSPCores-1) */
    ADT_UInt16 ConferenceId,            /* Conference Id */
    GpakFrameHalfMS FrameSizeHalfMS,           /* Frame Size */
    GpakToneTypes InputToneTypes,       /* Input Tone Detection types */
    GPAK_ConferConfigStat_t *pStatus    /* pntr to Conference Config Status */
    );

//}===============================================================
//    Arbitrary Tone Detector configuration
//{===============================================================
typedef struct GpakArbTdParams_t {
   ADT_UInt16 numDistinctFreqs;   // distinct freqs need to be detected
   ADT_UInt16 numTones;           // number of tones needs to be detected
   ADT_Int16  minPower;           // miminum power of tone in dB, 0 == default level
   ADT_UInt16 maxFreqDeviation;   // frequency in spec range, , 0 == means default
} GpakArbTdParams_t;

typedef struct GpakArbTdToneIdx_t {
   ADT_UInt16 f1;           // freq 1 of tone (non-zero)
   ADT_UInt16 f2;           // freq 2 of tone (can be zero if single frequency)
   ADT_UInt16 index;        // detect index reported for this tone
} GpakArbTdToneIdx_t;


/*
 * gpakConfigArbToneDetect - Configure a DSP's Arbitrary Tone Detector.
 *
 * FUNCTION
 *  This function configures a DSP's Arbitrary Tone Detector.
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
GpakApiStatus_t gpakConfigArbToneDetect (
    ADT_UInt32                  DspId,        /* DSP Id (0 to MaxDSPCores-1) */
    ADT_UInt16                  ArbCfgId,     /* Arb Detector Configuration Id */
    GpakArbTdParams_t           *pArbParams,  /* pntr to Arb detector config params */
    GpakArbTdToneIdx_t          *pArbTones,   /* pntr to tones used by this configuration (16 max) */
    ADT_UInt16                  *pArbFreqs,   /* pntr to array of all distinct frequencies (32 max)*/
    GPAK_ConfigArbToneStat_t    *pStatus      /* pntr to Arb Tone detect Config Status */
    );



/*
 * gpakActiveArbToneDetect - Configure a DSP's Arbitrary Tone Detector.
 *
 * FUNCTION
 *  This function activates a DSP's Arbitrary Tone Detector.
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
GpakApiStatus_t gpakActiveArbToneDetect (
    ADT_UInt32                  DspId,        /* DSP Id (0 to MaxDSPCores-1) */
    ADT_UInt16                  ArbCfgId,     /* Arb Detector Configuration Id */
    GPAK_ActiveArbToneStat_t    *pStatus      /* pntr to Arb Tone detect Config Status */
    );

//}===============================================================
//    Packet transfers
//{===============================================================

/*
 * gpakSendPayloadToDsp - Send a Payload to a DSP's Channel.
 *
 * FUNCTION
 *  This function writes a Payload message into DSP memory for a particular
 *  channel.
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */

extern GpakApiStatus_t gpakSendPayloadToDsp (
    ADT_UInt32 DspId,               /* DSP Identifier (0 to MaxDSPCores-1) */
    ADT_UInt16 ChannelId,          /* Channel Id (0 to MaxChannelAlloc-1) */
    GpakPayloadClass PayloadClass, /* Payload Class */
    ADT_Int16  PayloadType,         /* Payload Type code */
    ADT_UInt8 *pPayloadData,       /* pointer to Payload buffer, 32-bit aligned */
    ADT_UInt16 PayloadSize         /* length of Payload data (octets) */
    );



/*
 * gpakGetPayloadFromDsp - Read a Payload from a DSP's Channel.
 *
 * FUNCTION
 *  This function reads a Payload message from DSP memory for a particular
 *  channel.
 *
 *  Note: The payload data is returned in the specified data buffer. The
 *        'pPayloadSize' parameter must be set to the size of the buffer on
 *        entry and is set to the number of octets stored in the buffer on exit
 *        if a payload is read successfully. If the buffer is too short for the
 *        data, a failure indication is returned and the payload remains unread.
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */

extern GpakApiStatus_t gpakGetPayloadFromDsp (
    ADT_UInt32 DspId,                  /* DSP Identifier (0 to MaxDSPCores-1) */
    ADT_UInt16 ChannelId,             /* Channel Id (0 to MaxChannelAlloc-1) */
    GpakPayloadClass *pPayloadClass,  /* pointer to Payload Class var */
    ADT_Int16 *pPayloadType,  /* pointer to Payload Type code var */
    ADT_UInt8 *pPayloadData,  /* pointer to Payload buffer...32-bit aligned */
    ADT_UInt16 *pPayloadSize,  /* length of Payload data (octets) */
    ADT_UInt32 *pTimeStamp);

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakSendPacketToDsp - Send an RTP packet to a DSP's Channel.
 *
 * FUNCTION
 *  Writes a Payload message into DSP memory for a specified channel.
 *
 * Inputs
 *   DspId      - Dsp identifier 
 * ChannelId    - Channel identifier
 * pData        - Pointer to RTP packet data.  MUST be aligned on DSP word boundary
 * PacketI8     - Byte count of RTP packet data.
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
extern GpakApiStatus_t gpakSendPacketToDsp (
    ADT_UInt32 DspId,               /* DSP Identifier (0 to MaxDSPCores-1) */
    ADT_UInt16 ChannelId,          /* Channel Id (0 to MaxChannelAlloc-1) */
    ADT_UInt8 *pData,              /* pointer to RTP packet buffer...32-bit aligned */
    ADT_UInt16 PacketI8);          /* length of RTP packet data (octets) */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakGetPacketFromDsp - Read an RTP Packet from a DSP's Channel.
 *
 * FUNCTION
 *  Reads next available RTP packet from DSP memory.
 *
 *  Note: If the buffer is too short for the data, a failure indication is returned
 *        and all remaining packets are purged.
 * 
 * Inputs
 *     DspId      - Dsp identifier 
 * 
 * Outputs
 *   pChannelId   - Channel identifier
 *   pData        - Pointer to RTP packet data.  MUST be aligned on 32-bit boundary
 *
 * Updates
 *     PacketI8    - Before. Byte count of payload buffer size
 *                   After,  Byte count of actual payload
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
extern GpakApiStatus_t gpakGetPacketFromDsp (
    ADT_UInt32 DspId,               /* DSP Identifier (0 to MaxDSPCores-1) */
    ADT_UInt16 *pChannelId,        /* Channel Id (0 to MaxChannelAlloc-1) */
    ADT_UInt8  *pData,             /* pointer to RTP packet buffer...32-bit aligned */
    ADT_UInt16 *PacketI8);         /* pointer to length of RTP packet data (octets) */

//}===============================================================
//   Run-time control
//{===============================================================


/*
 * gpakTestMode - Configure/perform a DSP's Test mode.
 *
 * FUNCTION
 *  This function configures or performs a DSP's Test mode.
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */


extern ADT_UInt32 formatTestModeMsg(ADT_UInt16 *Msg, GpakTestMode TestModeId, 
                                       GpakTestData_t  *pTestParm, ADT_UInt16 buflenI16, ADT_UInt32 *ReplyLenI16);

extern GpakApiStatus_t gpakTestMode (
    ADT_UInt32      DspId,        /* DSP Id (0 to MaxDSPCores-1) */
    GpakTestMode    TestModeId,   /* Test Mode Id */
    GpakTestData_t  *pTestParm,   /* test parameter (PCM sync byte, etc.) */
    ADT_UInt16      *pRespData,   /* pointer to response data from DSP */
    GPAK_TestStat_t *pStatus      /* pointer to Test Mode status */
    );

// Non-zero frequencies must be in contiguous array elements at start of array.
// Non-zero on and off durations must be in contiguous array elements at start of array.
// Count of off durations may not be less than count of on durations minus one.
typedef struct GpakToneGenParms_t {
    GpakToneGenCmd_t    ToneCmd;        // tone command
    GpakDeviceSide_t    Device;         // Device generating tone 
    GpakToneGenType_t   ToneType;       // Tone Type
    ADT_UInt16          Frequency[4];   // Frequency (Hz)
    ADT_Int16           Level[4];       // Frequency's Level (-19 to 0 dBm  in 1 dBm units)
    ADT_UInt16          OnDuration[4];  // On Duration (1 millisecond units)
    ADT_UInt16          OffDuration[4]; // Off Duration (1 millisecond units) 
} GpakToneGenParms_t;


extern ADT_UInt32 gpakFormatToneGenerator (
      ADT_UInt16 *Msg,                   /* pointer to message buffer */
      ADT_UInt16 ChannelId,            /* Channel Identifier */
      GpakToneGenParms_t *tone);       /* pointer to tone generation structure */

extern GpakApiStatus_t gpakToneGenerate (  /* Returns com link status */
      ADT_UInt32 DspId,                    /* DSP Identifier */
      ADT_UInt16 ChannelId,               /* Channel Identifier */
      GpakToneGenParms_t *Tone,           /* pointer to tone generation structure */
      GPAK_ToneGenStat_t *pStatus         /* pointer to DSP status reply */
      );

typedef struct GpakDtmfParms_t {
   GpakToneCodes_t  DtmfDigits[16];      // Dtmf tone codes (range is 0...15)
   ADT_UInt16       NumDigits;           // number of digits (maximum 16)
   ADT_UInt16       OnTimeMs;            // on-duration in milliseconds
   ADT_UInt16       InterdigitTimeMs;    // interdigit duration in milliseconds
   ADT_Int16        Level[2];            // level in dBm of each freq. (0, ...-19) in dB steps
   GpakDeviceSide_t Device;              // Device Dialing (A or B)
}  GpakDtmfParms_t;   

extern ADT_UInt32 gpakFormatDtmfDial (
      ADT_UInt16 *Msg,                 /* pointer to message buffer */
      ADT_UInt16 ChannelId,            /* Channel Identifier */
      GpakDtmfParms_t *tone);          /* pointer to dtmf parameter structure */

extern GpakApiStatus_t gpakDtmfDial (  /* Returns com link status */
      ADT_UInt32 DspId,                /* DSP Identifier */
      ADT_UInt16 ChannelId,            /* Channel Identifier */
      GpakDtmfParms_t *Parms,          /* pointer to dtmf parameter structure */
      GPAK_DtmfDialStat_t *pStatus         /* pointer to DSP status reply */
      );

extern GpakApiStatus_t gpakRTPInsertTone (ADT_UInt32 DspId, 
                    ADT_UInt16 ChannelId, 
                    ADT_UInt16 TonePyldI8,
					ADT_UInt16 *TonePyld, 
					GPAK_InsertEventStat_t * dspStatus);

typedef struct GpakTonePktGenParms_t {
    GpakToneGenCmd_t    ToneCmd;   // tone command
    GpakDeviceSide_t    Device;    // Device generating tone 
    ADT_UInt16          ToneCode;  // Tone Event Code in EventPkt payload
    ADT_Int16           Level;     // Tone volume (in 1 dBm)
    ADT_UInt16          Duration;  // Tone Duration (in timestamp units, or samples)
    ADT_UInt16          Frequency[2];// Frequency (Hz), reserved for the TonePkt
} GpakTonePktGenParms_t;


extern ADT_UInt32 gpakFormatTonePktGenerator (
      ADT_UInt16 *Msg,                   /* pointer to message buffer */
      ADT_UInt16 ChannelId,            /* Channel Identifier */
      GpakTonePktGenParms_t *tone);       /* pointer to tone generation structure */

extern GpakApiStatus_t gpakTonePktGenerate (  /* Returns com link status */
      ADT_UInt32 DspId,                    /* DSP Identifier */
      ADT_UInt16 ChannelId,               /* Channel Identifier */
      GpakTonePktGenParms_t *Tone,           /* pointer to tone pkt generation structure */
      GPAK_ToneGenStat_t *pStatus         /* pointer to DSP status reply */
      );

/*
 *   gpakSendPlayRecordMsg 
 *
 * FUNCTION
 *  This function is used to issue start/stop voice recording and start
 *  voice playback commands to the DSP.
 *
 *  Notes: The start playback command requires that the payload buffer 
 *         (after a reserved 32 byte header) is filled with voice data in the 
 *         format specified by the recording mode. 
 *
 *         Stop tone = tdsDtmfDigit<x> or tdsNoToneDetected  x=0..9
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */

extern GpakApiStatus_t gpakSendPlayRecordMsg (
      ADT_UInt32            DspId,       /* DSP identifier */
      ADT_UInt16            ChannelId,   /* Channel identifier */
      GpakDeviceSide_t      DeviceSide,  /* Device A or B */
      GpakPlayRecordCmd_t   Cmd,         /* Playback/record command */
      DSP_Address           BufferAddr,  /* Playback buffer start address */
      ADT_UInt32            BufferLength, /* Playback buffer length */
      GpakPlayRecordMode_t  RecMode,     /* Record mode */
      GpakToneCodes_t       StopTone,    /* Stop tone code */
      GPAK_PlayRecordStat_t *pStatus     /* Pointer to DSP status reply */
      );

extern ADT_UInt32 gpakFormatPlayRecordMsg (
      ADT_UInt16 *Msg,                   /* pointer to message buffer */
      ADT_UInt16            ChannelId,   /* Channel identifier */
      GpakDeviceSide_t      DeviceSide,  /* Device A or B */
      GpakPlayRecordCmd_t   Cmd,         /* Playback/record command */
      DSP_Address           BufferAddr,  /* Playback buffer start address */
      ADT_UInt32            BufferLength, /* Playback buffer length */
      GpakPlayRecordMode_t  RecMode,      /* Record mode */
      GpakToneCodes_t       StopTone);    /* Stop tone code */



/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakGetEcanState - Read an Echo Canceller's training state.
 *
 * FUNCTION
 *  This function reads an Echo Canceller's training state information.
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */

/* gpakGetEcanState return status. */
extern void gpakParseEcanState (ADT_UInt16 *Msg, GpakEcanState_t *pEcanState);

extern GpakApiStatus_t gpakGetEcanState (
    ADT_UInt32          DspId,          /* DSP identifier */
    ADT_UInt16          ChannelId,		/* channel identifier */
    GpakEcanState_t     *pEcanState,    /* pointer to Ecan state variable */
    GpakDeviceSide_t    AorB            /* device side of canceller */
    );

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakSetEcanParms - Configure an Echo Canceller.
 *
 * FUNCTION
 *  This function sets an Echo Canceller's configuration parameters.
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */

/* gpakSetEcanState return status. */

extern ADT_UInt32 formatSetEcanState(ADT_UInt16 *Msg, ADT_UInt16 ChannelId, GpakEcanState_t *pEcanState, GpakDeviceSide_t AorB);

extern GpakApiStatus_t gpakSetEcanState (
    ADT_UInt32          DspId,          /* DSP identifier */
    ADT_UInt16          ChannelId,		/* channel identifier */
    GpakEcanState_t     *pEcanState,    /* pointer to Ecan state variable */
    GpakDeviceSide_t    AorB            /* device side of canceller */
    );

	
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadChanAecStatus - Read a channel's AEC status.
 *
 * FUNCTION
 *  This function reads a channel's AEC status.
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 */
typedef struct
{
    ADT_Int16 txInPowerdBm10;
    ADT_Int16 txOutPowerdBm10;
    ADT_Int16 rxInPowerdBm10;
    ADT_Int16 rxOutPowerdBm10;
    ADT_Int16 residualPowerdBm10;
    ADT_Int16 erlDirectdB10;
    ADT_Int16 erlIndirectdB10;
    ADT_Int16 erldB10BestEstimate;
    ADT_Int16 worstPerBinERLdB10BestEstimate;
    ADT_Int16 erledB10;
    ADT_Int16 shortTermERLEdB10;
    ADT_Int16 shadowERLEdB10;
    ADT_Int16 rxVADState;
    ADT_Int16 txVADState;
    ADT_Int16 rxVADStateLatched;
    ADT_Int16 currentBulkDelaySamples;
    ADT_Int16 txAttenuationdB10;
    ADT_Int16 rxAttenuationdB10;
    ADT_Int16 rxOutAttenuationdB10;
    ADT_Int16 nlpThresholddB10;
    ADT_Int16 nlpSaturateFlag;
    ADT_Int16 aecState;
    ADT_Int16 sbcngResidualPowerdBm10;
    ADT_Int16 sbcngCNGPowerdBm10;
    ADT_Int16 rxOutAttendB10;
    ADT_Int16 sbMaxAttendB10;
    ADT_Int16 sbMaxClipLeveldBm10;
#if (AEC_LIB_VERSION >= 0x0430)
    ADT_Int16 instantaneousERLEdB100;
    ADT_Int16 dynamicNLPAggressivenessAdjustdB10;
#endif
} gpakAecStatus_t;

extern gpakGetAecStatusStat_t gpakReadChanAecStatus(
    ADT_UInt32 dspId,                   /* DSP identifier */
    ADT_UInt16 channelId,               /* channel identifier */
    gpakAecStatus_t *pAecStatus,        /* pointer to AEC status info */
    GPAK_AecChanReadStat_t *pDspStatus  /* pointer to DSP reply status */
    );

	
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadChanLecStatus - Read a channel's LEC status.
 *
 * FUNCTION
 *  This function reads a channel's LEC status.
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 */
typedef struct
{
	ADT_Int16 AdaptReport;
	ADT_Int16 CrossCorrReport;
	ADT_UInt16 G165Status;
	ADT_Int16 ConvergenceStat;
	ADT_Int16 NLPFlag;
	ADT_Int16 DoubleTalkFlag;
	ADT_Int16 SmartPacketMode;
	ADT_Int16 ERL1;
	ADT_Int16 ERL2;
	ADT_Int16 ERLE;
	ADT_UInt16 StatusFlags;
	ADT_UInt16 EventFlags;
} gpakLecStatus_t;

extern gpakGetLecStatusStat_t gpakReadChanLecStatus(
    ADT_UInt32 dspId,                   /* DSP identifier */
    ADT_UInt16 channelId,               /* channel identifier */
    GpakDeviceSide_t AorB,              /* device side of canceller */
    gpakLecStatus_t *pLecStatus,        /* pointer to LEC status info */
    GPAK_LecChanReadStat_t *pDspStatus  /* pointer to DSP reply status */
    );

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakAlgControl - Control an Algorithm.
 *
 * FUNCTION
 *  This function controls an Algorithm
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */

/* gpakAlgControl return status. */

extern GpakApiStatus_t gpakAlgControl (
    ADT_UInt32            DspId,          /* DSP identifier */
    ADT_UInt16            ChannelId,	  /* channel identifier */
    GpakAlgCtrl_t         ControlCode,    /* algorithm control code */
    GpakToneTypes         DeactTonetype,  /* tone detector to deactivate */
    GpakToneTypes         ActTonetype,    /* tone detector to activate */
    GpakDeviceSide_t	  AorBSide,       /* A or B device side */
    ADT_UInt16            NLPsetting,     /* NLP value */
    ADT_Int16             GaindB,         /* Gain Value (dB), 1dB steps: -40...40 */
	GpakCodecs CodecType, 
    GPAK_AlgControlStat_t *pStatus        /* pointer to return status */
    );

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakSendCIDPayloadToDsp - Send a transmit payload to a DSP channel
 *
 * FUNCTION
 *  This function sends a CID transmit payload to the DSP.
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */

/* gpakSendCIDPayloadToDsp return status. */

extern GpakApiStatus_t gpakSendCIDPayloadToDsp (
    ADT_UInt32  DspId,           /* DSP identifier */
    ADT_UInt16  ChannelId,       /* channel identifier */
    GpakCIDTypes CIDType,  /* Caller ID on/off type (type I or type II) */
    GpakCIDMsgTypes CIDMsgType,  /* Caller ID message type */
    ADT_UInt8   *pPayloadData,   /* pointer to Payload data */
    ADT_UInt16  PayloadSize, 	 /* length of Payload data (octets) */
    GpakDeviceSide_t AorBSide,   /* device side: A or B */
    GPAK_TxCIDPayloadStat_t *pStatus /* error status code */
    );


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakConferenceGenerateTone - Control tone generation.
 *
 * FUNCTION
 *  This function controls the generation of tones on the specified conference.
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */

extern ADT_UInt32 gpakFormatConfToneGenerator (ADT_UInt16 *Msg, 
                                   ADT_UInt16 ConfId, 
                                   GpakToneGenParms_t *tone);

extern GpakApiStatus_t gpakConferenceGenerateTone (
    ADT_UInt32          DspId,      	/* DSP identifier */
    ADT_UInt16          ConfId,	    	/* conference identifier */
    GpakToneGenParms_t  *pToneParms,    /* pointer to tone gen parameters */
    GPAK_ToneGenStat_t  *pStatus 	    /* pointer to DSP status reply */
    );

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadAgcInputPower - Read short term power
 * 
 * FUNCTION
 *  This function reads a channel's short term power computed by the AGC
 * 
 * RETURNS
 *  Status  code indicating success or a specific error.
 */

/* gpakReadAgcInputPower return status. */

extern GpakApiStatus_t gpakReadAgcInputPower (
    ADT_UInt32          DspId,      /* DSP identifier */
    ADT_UInt16          ChannelId,	/* channel identifier */
    ADT_Int16           *pPowerA,   /* pointer to store A-side Input Power (dBm) */
    ADT_Int16           *pPowerB,   /* pointer to store B-side Input Power (dBm) */
    GPAK_AGCReadStat_t  *pStatus    /* pointer to DSP status reply  */
    );

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakResetChannelStats - Reset a channel's statistics
 * 
 * FUNCTION
 *  This function reset's a channel's statistics specified in the SelectMask
 * 
 * RETURNS
 *  Status  code indicating success or a specific error.
 */


/* bit mask definitions for channel statistics reset */
typedef struct resetMask {
    ADT_UInt16 reserve:15;          /* not used */
    ADT_UInt16 PacketStats:1;       /* 1 == reset packet statistics */
} resetCsMask;

extern GpakApiStatus_t  gpakResetChannelStats (
    ADT_UInt32          DspId,      /* DSP Identifier */
    ADT_UInt16          ChannelId,  /* channel identifier */
    resetCsMask	        SelectMask  /* reset bit Mask */
    );


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakResetSystemStats - Reset system statistics
 * 
 * FUNCTION
 *  This function reset's the system-level statistics specified in the SelectMask
 * 
 * RETURNS
 *  Status  code indicating success or a specific error.
 */

/* bit mask definitions for channel statistics reset */
typedef struct resetSsMask {
    ADT_UInt16 reserve:14;          /* not used */
    ADT_UInt16 FramingStats:1;      /* 1 == reset framing statistics */
    ADT_UInt16 CpuUsageStats:1;     /* 1 == reset Cpu Usage statistics */
} resetSsMask;

extern GpakApiStatus_t  gpakResetSystemStats (
    ADT_UInt32          DspId,      /* DSP Identifier */
    resetSsMask	        SelectMask  /* reset bit Mask */
    );


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadMcBspStats - Read McBsp statistics
 * 
 * FUNCTION
 *  This function reads McBsp Statistics
 * 
 * RETURNS
 *  Status  code indicating success or a specific error.
 */
typedef struct gpakMcBspStats_t {
    GpakActivation  Port1Status;           /* port 1 status */
    ADT_UInt32      Port1RxIntCount;       /* port 1 Rx Interrupt count */
    ADT_UInt16      Port1RxSlips;          /* port 1 Rx Slip count */
    ADT_UInt16      Port1RxDmaErrors;      /* port 1 Rx Dma error count */
    ADT_UInt32      Port1TxIntCount;        /* port 1 Tx interrupt count */
    ADT_UInt16      Port1TxSlips;          /* port 1 Tx Slip count */
    ADT_UInt16      Port1TxDmaErrors;      /* port 1 Tx Dma error count */
    ADT_UInt16      Port1FrameSyncErrors;  /* port 1 Frame Sync error count */
    ADT_UInt16      Port1RestartCount;     /* port 1 McBsp Restart count */

    GpakActivation  Port2Status;           /* port 2 status */
    ADT_UInt32      Port2RxIntCount;       /* port 2 Rx Interrupt count */
    ADT_UInt16      Port2RxSlips;          /* port 2 Rx Slip count */
    ADT_UInt16      Port2RxDmaErrors;      /* port 2 Rx Dma error count */
    ADT_UInt32      Port2TxIntCount;        /* port 2 Tx interrupt count */
    ADT_UInt16      Port2TxSlips;          /* port 2 Tx Slip count */
    ADT_UInt16      Port2TxDmaErrors;      /* port 2 Tx Dma error count */
    ADT_UInt16      Port2FrameSyncErrors;  /* port 2 Frame Sync error count */
    ADT_UInt16      Port2RestartCount;     /* port 2 McBsp Restart count */

    GpakActivation  Port3Status;           /* port 3 status */
    ADT_UInt32      Port3RxIntCount;       /* port 3 Rx Interrupt count */
    ADT_UInt16      Port3RxSlips;          /* port 3 Rx Slip count */
    ADT_UInt16      Port3RxDmaErrors;      /* port 3 Rx Dma error count */
    ADT_UInt32      Port3TxIntCount;        /* port 3 Tx interrupt count */
    ADT_UInt16      Port3TxSlips;          /* port 3 Tx Slip count */
    ADT_UInt16      Port3TxDmaErrors;      /* port 3 Tx Dma error count */
    ADT_UInt16      Port3FrameSyncErrors;  /* port 3 Frame Sync error count */
    ADT_UInt16      Port3RestartCount;     /* port 3 McBsp Restart count */
} gpakMcBspStats_t;

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakSideTone - Side Tone settings
 * 
 * FUNCTION
 *  This function Enables Side Tone and sets gain on a Channel
 * 
 * RETURNS
 *  None
 */

/* bit mask definitions for channel statistics reset */
typedef struct setSideTone {
    ADT_UInt16 ChannelId;          
    ADT_UInt16 Enable;     
    ADT_Int16 Gain;    
	ADT_UInt16 MsgLen;
} setSideTone;

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadMemoryTestStatus - Read Memory Test Results
 * 
 * FUNCTION
 *  This function reads the status of the DSP memory test
 * 
 * RETURNS
 *  None
 */

typedef struct memoryTestResults {          
	ADT_UInt16 TestPattern;
	ADT_UInt32 TestAddress;
} memoryTestResults;

GpakApiStatus_t gpakReadMemoryTestStatus ( 
	ADT_UInt32 DspId,
	memoryTestResults *pResults,
   GPAK_MemoryTestResults_t *pStatus
	);

extern void gpakParseMcBspStats (ADT_UInt16 *Msg, gpakMcBspStats_t *FrameStats);
extern GpakApiStatus_t gpakReadMcBspStats ( 
        ADT_UInt32         DspId,                /* DSP Identifier */
		gpakMcBspStats_t    *FrameStats           /* Pointer to Frame Stats. */
        );

        
        
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakLoadWavefile - Load a wavfile in DSP memory for future playback
 * 
 * FUNCTION
 *  This function loads a wavefile in DSP memory. Supported formats are:
 *     PCM 16-bit, U-LAW/A-LAW 8-bit, 8 kHz sampling
 * 
 * RETURNS
 *  None
 */
typedef enum { 
    WAVREAD_SUCCESS        =0,
    WAVREAD_READ_ERR       =1,
    WAVREAD_FILETYPE_ERR   =2,
    WAVREAD_AUDIOFMT_ERR   =3,  
    WAVREAD_NUMCHAN_ERR    =4,
    WAVREAD_SAMPLERATE_ERR =5,
    WAVREAD_BITSIZE_ERR    =6,
    WAVREAD_EXTENSION_ERR  =7
} WaveReadStatus_t;

GpakApiStatus_t gpakLoadWavefile (
   ADT_UInt32       DspId,         /* DSP Identifier */
   GPAK_FILE_ID     fp,            /* Wavefile handle */
   ADT_UInt16       playbackId,    /* Id used to playback this file */
   WaveReadStatus_t *waveFileStatus,       /* .wav file parse status */
   GPAK_LoadWavefileParmsStat_t *pStatus); /* DSP file download status */
        
GpakApiStatus_t gpakPlayWavefile (
   ADT_UInt32       DspId,              /* DSP Identifier */
   ADT_UInt16       ChannelId,          /* Channel Id to  play on */
   ADT_UInt16       playbackId,         /* Id used to playback this file */
   WavePlaybackCmd  command,            /* Stop, play once, loop */
   GpakDeviceSide_t direction,          /* (ASide or NetToDSP) == to TDM, (BSide or DSPToNet) == to Network */ 
   GPAK_PlayWavefileStat_t *pStatus);   /* DSP play wavefile status */
        
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadVersionString - Read version string from DSP
 * 
 * FUNCTION
 *  This function reads the build version, date string
 * 
 * RETURNS
 *  None
 */
typedef struct gpakVersionString_t {
    char buf[128];
} gpakVersionString_t;

extern gpakTestStatus_t gpakReadVersionString (ADT_UInt32 DspId, gpakVersionString_t *verString);
        
        
        
//}===============================================================
//   Custom platform routines
//{===============================================================

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadDspMemory8 - Read 8-bit values from DSP memory.
 *
 * FUNCTION
 *  This function reads a contiguous block of bytes from DSP memory starting at
 *  the specified address.
 *
 * RETURNS
 *  nothing
 *
 */
extern void gpakReadDspMemory8(
    ADT_UInt32   DspId,         /* DSP Identifier (0 to MAX_DSP_CORES-1) */
    DSP_Address DspAddress,     /* DSP's memory address of first word */
    ADT_UInt32  NumBytes,       /* number of contiguous bytes to read */
    ADT_UInt8  *pByteValues    /* pointer to array of byte values variable */
    );

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadDspMemory16 - Read 16-bit values from DSP memory.
 *
 * FUNCTION
 *  This function reads a contiguous block of words from DSP memory starting at
 *  the specified address.
 *
 * RETURNS
 *  nothing
 *
 */
extern void gpakReadDspMemory16(
    ADT_UInt32  DspId,          /* DSP Identifier (0 to MAX_DSP_CORES-1) */
    DSP_Address DspAddress,     /* DSP's memory address of first word */
    ADT_UInt32  NumWords,       /* number of contiguous words to read */
    ADT_UInt16 *pWordValues    /* pointer to array of word values variable */
    );

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadDspNoSwap16 - Read DSP packet memory without byte swapping.
 *
 * FUNCTION
 *  This function reads a contiguous block of 16-bit values from DSP memory 
 *  starting at the specified address.  DSP packet memory is in big-endian order
 *
 * RETURNS
 *  nothing
 *
 */
extern void gpakReadDspNoSwap16(
    ADT_UInt32  DspId,           /* DSP Identifier (0 to MAX_DSP_CORES-1) */
    DSP_Address DspAddress,      /* DSP's memory address of first word */
    ADT_UInt32  NumShorts,       /* number of contiguous 32-bit vals to read */
    ADT_UInt16 *pShortValues     /* pointer to array of 32-bit values variable */
    );

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadDspMemory32 - Read 32-bit values from DSP memory.
 *
 * FUNCTION
 *  This function reads a contiguous block of 32-bit values from DSP memory 
 *  starting at the specified address.
 *
 * RETURNS
 *  nothing
 *
 */
extern void gpakReadDspMemory32(
    ADT_UInt32  DspId,          /* DSP Identifier (0 to MAX_DSP_CORES-1) */
    DSP_Address DspAddress,     /* DSP's memory address of first word */
    ADT_UInt32  NumLongs,       /* number of contiguous 32-bit vals to read */
    ADT_UInt32  *pLongValues    /* pointer to array of 32-bit values variable */
    );


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadDspNoSwap32 - Read DSP packet memory without byte swapping.
 *
 * FUNCTION
 *  This function reads a contiguous block of 32-bit values from DSP memory 
 *  starting at the specified address.  DSP packet memory is in big-endian order
 *
 * RETURNS
 *  nothing
 *
 */
extern void gpakReadDspNoSwap32(
    ADT_UInt32  DspId,           /* DSP Identifier (0 to MAX_DSP_CORES-1) */
    DSP_Address DspAddress,      /* DSP's memory address of first word */
    ADT_UInt32  NumLongs,        /* number of contiguous 32-bit vals to read */
    ADT_UInt32 *pLongValues      /* pointer to array of 32-bit values variable */
    );

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakWriteDspMemory8 - Write DSP memory.
 *
 * FUNCTION
 *  This function writes a contiguous block of bytes to DSP memory starting at
 *  the specified address.
 *
 * RETURNS
 *  nothing
 *
 */
extern void gpakWriteDspMemory8(
    ADT_UInt32  DspId,          /* DSP Identifier (0 to MAX_DSP_CORES-1) */
    DSP_Address DspAddress,     /* DSP's memory address of first word */
    ADT_UInt32  NumBytes,       /* number of contiguous bytes to write */
    ADT_UInt8  *pByteValues    /* pointer to array of bytes values to write */
    );

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakWriteDspMemory16 - Write DSP memory.
 *
 * FUNCTION
 *  This function writes a contiguous block of wordss to DSP memory starting at
 *  the specified address.
 *
 * RETURNS
 *  nothing
 *
 */
extern void gpakWriteDspMemory16(
    ADT_UInt32  DspId,          /* DSP Identifier (0 to MAX_DSP_CORES-1) */
    DSP_Address DspAddress,     /* DSP's memory address of first word */
    ADT_UInt32  NumWords,       /* number of contiguous words to write */
    ADT_UInt16 *pWordValues    /* pointer to array of word values to write */
    );

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakWriteDspNoSwap16 - Write DSP packet memory without byte swapping.
 *
 * FUNCTION
 *  This function writes a contiguous block of words to DSP memory starting at
 *  the specified address.
 *
 * RETURNS
 *  nothing
 *
 */
extern void gpakWriteDspNoSwap16(
    ADT_UInt32 DspId,            /* DSP Identifier (0 to MAX_DSP_CORES-1) */
    DSP_Address DspAddress,     /* DSP's memory address of first word */
    ADT_UInt32 NumShorts,       /* number of contiguous words to write */
    ADT_UInt16 *pShortValues    /* pointer to array of word values to write */
    );

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakWriteDspMemory32 - Write DSP memory.
 *
 * FUNCTION
 *  This function writes a contiguous block of 32-bit values to DSP memory 
 *  starting at the specified address.
 *
 * RETURNS
 *  nothing
 *
 */
extern void gpakWriteDspMemory32(
    ADT_UInt32  DspId,          /* DSP Identifier (0 to MAX_DSP_CORES-1) */
    DSP_Address DspAddress,     /* DSP's memory address of first word */
    ADT_UInt32  NumLongs,       /* number of contiguous 32-bit vals to write */
    ADT_UInt32 *pLongValues    /* pointer to array of 32-bit values to write */
    );

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakWriteDspNoSwap32 - Write DSP packet memory without byte swapping.
 *
 * FUNCTION
 *  This function writes a contiguous block of words to DSP memory starting at
 *  the specified address.
 *
 * RETURNS
 *  nothing
 *
 */
extern void gpakWriteDspNoSwap32(
    ADT_UInt32 DspId,            /* DSP Identifier (0 to MAX_DSP_CORES-1) */
    DSP_Address DspAddress,     /* DSP's memory address of first word */
    ADT_UInt32 NumLongs,        /* number of contiguous longs to write */
    ADT_UInt32 *pLongValues     /* pointer to array of long values to write */
    );
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakHostDelay - Delay for a fixed time interval.
 *
 * FUNCTION
 *  This function delays for a fixed time interval before returning. The time
 *  interval is the Host Port Interface sampling period when polling a DSP for
 *  replies to command messages.
 *
 * RETURNS
 *  nothing
 *
 */
extern void gpakHostDelay(void);


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakLockAccess - Lock access to the specified DSP.
 *
 * FUNCTION
 *  This function aquires exclusive access to the specified DSP.
 *
 * RETURNS
 *  nothing
 *
 */
extern void gpakLockAccess(
    ADT_UInt32 DspId                   /* DSP Identifier (0 to MAX_DSP_CORES-1) */
    );


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakUnlockAccess - Unlock access to the specified DSP.
 *
 * FUNCTION
 *  This function releases exclusive access to the specified DSP.
 *
 * RETURNS
 *  nothing
 *
 */
extern void gpakUnlockAccess(
    ADT_UInt32 DspId                   /* DSP Identifier (0 to MAX_DSP_CORES-1) */
    );


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadFile - Read a block of bytes from a G.PAK Download file.
 *
 * FUNCTION
 *  This function reads a contiguous block of bytes from a G.PAK Download file
 *  starting at the current file position.
 *
 * RETURNS
 *  The number of bytes read from the file.
 *   -1 indicates an error occurred.
 *    0 indicates all bytes have been read (end of file)
 *
 */
extern int gpakReadFile(
    GPAK_FILE_ID FileId,    /* G.PAK Download File Identifier */
    ADT_UInt8  *pBuffer,	/* pointer to buffer for storing bytes */
    ADT_UInt32  NumBytes   /* number of bytes to read */
    );




//{
// Initialize the network stack
//
// Output parameters:
//      LocalIP -  Local IP address used to communicate with DSPs in network byte order
//
// Returns:
//      0.             Success
//     Negative value. Error value.
//}
int gpakNetworkStackInit (ADT_UInt32 *LocalIP) ;

//{
// Open a TCP socket
//
//  Input Parameters:
//     DspIP    - Dsp's IP address
//     tcpPort  - Dsp's TCP port address
//
//  Returns:
//     NULL - open failure
//     Non-null - Socket handle cast as void pointer
//
//}
void *gpakOpenTCPSocket (ADT_UInt32 DspIP, ADT_UInt16 tcpPort) ;


//{
// Close a TCP socket
//
//  Input Parameters:
//     skt   - Socket handle cast as void pointer
//}
void gpakCloseTCPSocket (void *skt) ;

//{
//  Convert DSP socket to non-blocking
//
//  Input Parameters:
//     skt   - Socket handle cast as void pointer
//
//  Returns:
//     0.              Success
//     Negative value. Error value.
//
//}
int gpakUnblockTCPSocket (void *skt) ;

//{
// Send command message to DSP via socket.
//
//  Input Parameters:
//     skt   - Socket handle cast as void pointer
//     msgBuf   - Pointer to message buffer to be transmitted.  
//     msgBufI8 - Number of bytes to transmit
//
//  Returns:
//     Positive value. Number of bytes transmitted
//     Negative value. Error value.
//
//  NOTE: This routine should return only after message has been 
//        successfully sent or an error has been detected on the socket
//}
int gpakSendDspMsg (ADT_UInt32 DspId, void *skt, void *msgBuf, int msgBufI8) ;

// Send UDP Event pseudo 'connect' message
int gpakSendDspConnectEvtUdp (ADT_UInt32 DspId, void *skt, void *msgBuf, int msgBufI8);

//{
// Receive command reply from DSP via socket
//  Input Parameters:
//     skt   - Socket handle cast as void pointerID.
//     msgBuf   - Pointer to message buffer to be transmitted.  
//     msgBufI8 - Number of bytes to transmit
//
//  Returns:
//     0.              No reply
//     Positive value. Number of bytes received.
//     Negative value. Error value.
//
//}
int gpakRecvDspMsg (ADT_UInt32 DspId, void *skt, void *msgBuf, int msgBufI8) ;

#ifdef __cplusplus
}
#endif
//   Network messaging routines
//{===============================================================
extern int  ADT_printf (char *fmt, ...);

#ifdef NETWORK_MESSAGING
extern int gpakServerInit (int ipVer);
extern int gpakServerConnect (ADT_UInt32 DspId, char *gpakServer, void *remoteIP);
extern void gpakServerDisconnect (ADT_UInt32 DspId);
extern int CheckDspMsgConnect (ADT_UInt32 DspId);
extern int CheckDspEvtConnect (ADT_UInt32 DspId);
extern int RecvDspReplyMessage (ADT_UInt32 DspId, ADT_UInt32 *Msg, ADT_UInt32 *pI16Cnt);
extern int SendDspCmdMessage (ADT_UInt32 DspId, ADT_UInt32 *msg, ADT_UInt32 I16Cnt);
extern ADT_UInt32 gpakRecvDspEvt (ADT_UInt32 DspId, void *EvtBuffer, int EvtBufferI8);
#endif
//}
#endif  /* prevent multiple inclusion */
