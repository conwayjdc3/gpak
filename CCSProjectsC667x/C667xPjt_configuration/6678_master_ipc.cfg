/*
 * image_processing_evm6678l_master.cfg
 *
 * Memory Map and Program initializations for the Image Processing Application
 *
 * Copyright (C) 2011 Texas Instruments Incorporated - http://www.ti.com/ 
 *  
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/*
 *   @file  image_processing_evm6678l_master.cfg
 *
 *   @brief   
 *      Memory Map and Program initializations for the Image Processing Application.
 *
 */
 
/*******************************************************************************************
*  Specify all needed RTSC Moudles and configure them.                                     *
********************************************************************************************/

var Memory    = xdc.useModule('xdc.runtime.Memory');
var Log       = xdc.useModule('xdc.runtime.Log');
var Error     = xdc.useModule('xdc.runtime.Error');
var Diags     = xdc.useModule('xdc.runtime.Diags');
var Timestamp = xdc.useModule('xdc.runtime.Timestamp');
var Startup   = xdc.useModule('xdc.runtime.Startup');
var System    = xdc.useModule('xdc.runtime.System');
var SysStd    = xdc.useModule('xdc.runtime.SysStd');
System.SupportProxy = SysStd;

//var SysMin    = xdc.useModule('xdc.runtime.SysMin');
//System.SupportProxy = SysMin;


/* Load and configure SYSBIOS packages */
var BIOS      = xdc.useModule('ti.sysbios.BIOS');
var Task      = xdc.useModule('ti.sysbios.knl.Task');
var Clock     = xdc.useModule('ti.sysbios.knl.Clock');
var Sem       = xdc.useModule('ti.sysbios.knl.Semaphore');
var Hwi       = xdc.useModule('ti.sysbios.hal.Hwi');
var Ecm       = xdc.useModule('ti.sysbios.family.c64p.EventCombiner');
var BiosCache = xdc.useModule('ti.sysbios.hal.Cache');
var HeapBuf   = xdc.useModule('ti.sysbios.heaps.HeapBuf');
var HeapMem   = xdc.useModule('ti.sysbios.heaps.HeapMem');
var Exc       = xdc.useModule('ti.sysbios.family.c64p.Exception');
var Cache     = xdc.useModule('ti.sysbios.family.c66.Cache');


BIOS.taskEnabled = true;
Task.common$.namedInstance = true;

/*
 * Enable Event Groups here and registering of ISR for specific GEM INTC is done
 * using EventCombiner_dispatchPlug() and Hwi_eventMap() APIs
 */

Ecm.eventGroupHwiNum[0] = 7;
Ecm.eventGroupHwiNum[1] = 8;
Ecm.eventGroupHwiNum[2] = 9;
Ecm.eventGroupHwiNum[3] = 10;

/* Create a Heap. */
var heapMemParams = new HeapMem.Params();
heapMemParams.size = 0x8000000;
heapMemParams.sectionName = "systemHeapMaster";
Program.global.heap0 = HeapMem.create(heapMemParams);
Memory.defaultHeapInstance = Program.global.heap0;

/* Load and configure NDK */
var Global = xdc.useModule('ti.ndk.config.Global');

/* 
** This allows the heart beat (poll function) to be created but does not generate the stack threads 
**
** Look in the cdoc (help files) to see what CfgAddEntry items can be configured. We tell it NOT
** to create any stack threads (services) as we configure those ourselves in our Main Task
** thread.
*/  
Global.enableCodeGeneration = false;

var CpIntc    = xdc.useModule('ti.sysbios.family.c66.tci66xx.CpIntc');

/* Load the PDK packages */
/* Load the CSL package */
var devType 					= 	"c6678";
var Csl 					    = 	xdc.useModule('ti.csl.Settings');
Csl.deviceType 					= 	devType;
Csl.useCSLIntcLib 				= 	true;
var Pa   = xdc.useModule('ti.drv.pa.Settings');
var Cppi = xdc.loadPackage('ti.drv.cppi');     
var Qmss = xdc.loadPackage('ti.drv.qmss');
var Uart = xdc.loadPackage('ti.drv.uart');
Uart.Settings.socType = "c6678";
var Rm = xdc.loadPackage('ti.drv.rm');     

/* Load the Platform/NDK Transport packages */
var PlatformLib  = xdc.loadPackage('ti.platforms.evm6678');
var Ndk = xdc.loadPackage('ti.ndk');
/* Load the OSAL package */ 
var devType = "c6678";
var osType = "tirtos";
var Osal = xdc.useModule('ti.osal.Settings');
Osal.osType = osType;
Osal.socType = devType;

/* Load the Board package and set the board name */
// jdc comment var Board = xdc.loadPackage('ti.board');
// jdc comment Board.Settings.boardName = "evmC6678";

/* Load Profiling package */
var Utils = xdc.loadPackage('ti.utils.profiling');
var socType           = "c6678";
var Nimu 		= xdc.loadPackage('ti.transport.ndk.nimu');
Nimu.Settings.socType  = socType;
var Global       = xdc.useModule('ti.ndk.config.Global');
Global.enableCodeGeneration = false;

/* Load and configure the IPC packages */
var MessageQ     = xdc.useModule('ti.sdo.ipc.MessageQ');
var Ipc          = xdc.useModule('ti.sdo.ipc.Ipc');
var HeapBufMP    = xdc.useModule('ti.sdo.ipc.heaps.HeapBufMP');
var SharedRegion = xdc.useModule('ti.sdo.ipc.SharedRegion');
var MultiProc    = xdc.useModule('ti.sdo.utils.MultiProc');

MultiProc.setConfig(null, ["CORE0", "CORE1"]);

/* Synchronize all processors (this will be done in Ipc_start) */
Ipc.procSync = Ipc.ProcSync_ALL;

/* Shared Memory base address and length */
var SHAREDMEM           = 0x0c200000;
var SHAREDMEMSIZE       = 0x00200000;

SharedRegion.setEntryMeta(0,
    { base: SHAREDMEM, 
      len:  SHAREDMEMSIZE,
      ownerProcId: 0,
      isValid: true,
      name: "MSMCSRAM_IPC",
    });

/* ================ Logger configuration ================ */
///var LoggerCircBuf = xdc.useModule('ti.uia.runtime.LoggerCircBuf');
var Diags         = xdc.useModule('xdc.runtime.Diags');
var Defaults      = xdc.useModule('xdc.runtime.Defaults');
var Main          = xdc.useModule('xdc.runtime.Main');
var Load          = xdc.useModule('ti.sysbios.utils.Load');

Load.windowInMs = 50; 
///LogSync       = xdc.useModule('ti.uia.runtime.LogSync'); 

Exc.common$.logger = Main.common$.logger;
Exc.enablePrint = true;

///var LoggingSetup = xdc.useModule('ti.uia.sysbios.LoggingSetup');
/* Increase the sysbios logger and turn Hwi and Swi logging off */
///LoggingSetup.sysbiosLoggerSize = 32768;
///LoggingSetup.mainLoggerSize = 8*1024;
///LoggingSetup.loadLoggerSize = 32768;
///LoggingSetup.loadLogging = true;
///LoggingSetup.sysbiosTaskLogging = false;
///LoggingSetup.sysbiosSwiLogging = false;
///LoggingSetup.sysbiosHwiLogging = false;
///LoggingSetup.eventUploadMode = LoggingSetup.UploadMode_NONJTAGTRANSPORT;

/* ================ UIA configuration ================ */
/*
 *  This example is a multi-core example, so UIA's ServiceMgr 
 *  must be configured to collect events from multiple cores.
 */ 
///var ServiceMgr = xdc.useModule('ti.uia.runtime.ServiceMgr');
///ServiceMgr.topology = ServiceMgr.Topology_MULTICORE;
///ServiceMgr.masterProcId = 0;

/* The application is using the UIA benchmark events. */ 
/// jdc comments var UIABenchmark  = xdc.useModule('ti.uia.events.UIABenchmark');

/*
** Create a Heap. 
*/
var Memory      = xdc.useModule('xdc.runtime.Memory');
Memory.defaultHeapSize = 0x10000;
Program.heap = 0x10000;
Program.sectMap[".vecs"]             = {loadSegment: "MSMCSRAM", loadAlign:1024};
Program.sectMap[".switch"]           = {loadSegment: "MSMCSRAM", loadAlign:8};
Program.sectMap[".cio"]              = {loadSegment: "L2SRAM", loadAlign:8};
Program.sectMap[".args"]             = {loadSegment: "L2SRAM", loadAlign:8};
Program.sectMap[".cppi"]             = {loadSegment: "L2SRAM", loadAlign:16};
Program.sectMap[".qmss"]             = {loadSegment: "L2SRAM", loadAlign:16};
Program.sectMap[".nimu_eth_ll2"]     = {loadSegment: "L2SRAM", loadAlign:16};
Program.sectMap[".far:NDK_PACKETMEM"]= {loadSegment: "MSMCSRAM", loadAlign: 128};
Program.sectMap[".far:NDK_OBJMEM"]   = {loadSegment: "MSMCSRAM", loadAlign: 16};
Program.sectMap[".resmgr_memregion"] = {loadSegment: "L2SRAM", loadAlign:128};
Program.sectMap[".resmgr_handles"]   = {loadSegment: "L2SRAM", loadAlign:16};
Program.sectMap[".resmgr_pa"]        = {loadSegment: "L2SRAM", loadAlign:8};

Program.sectMap["systemHeapMaster"]     = "DDR3";
Program.sectMap[".cinit"]               = "MSMCSRAM";
Program.sectMap[".const"]               = "MSMCSRAM";
Program.sectMap[".text"]                = "MSMCSRAM";
Program.sectMap[".far"]                 = "L2SRAM";
Program.sectMap[".bss"]                 = "L2SRAM";
Program.sectMap[".rodata"]              = "L2SRAM";
Program.sectMap[".neardata"]            = "L2SRAM";
Program.sectMap[".code"]                = "L2SRAM";
Program.sectMap[".data"]                = "L2SRAM";
Program.sectMap[".sysmem"]              = "L2SRAM";
Program.sectMap[".defaultStackSection"] = "L2SRAM";
Program.sectMap[".stack"]               = "L2SRAM";
Program.sectMap[".plt"]                 = "L2SRAM";
Program.sectMap["platform_lib"]         = "L2SRAM";
Program.sectMap[".sharedGRL"]           = "MSMCSRAM"; 
Program.sectMap[".sharedPolicy"]        = "MSMCSRAM";

/* Add init function */
/// jdc comment Startup.lastFxns.$add('&EVM_init');

BIOS.cpuFreq.lo = 1000000000;
Global.IPv6 = true;
var Idle      = xdc.useModule('ti.sysbios.knl.Idle');
var Timer     = xdc.useModule('ti.sysbios.timers.timer64.Timer');
var hm1Params      = new HeapMem.Params();
hm1Params.instance.name = "waveHeap";
hm1Params.size          = 0x1400000;
hm1Params.sectionName   = "waveHeapSect";
hm1Params.minBlockAlign = 4;
Program.global.waveHeap = HeapMem.create(hm1Params);

var clock0Params = new Clock.Params();
clock0Params.instance.name = "clock0";
clock0Params.period = 1;
clock0Params.startFlag = true;
Program.global.clock0 = Clock.create("&GpakSchedulingTimer", 1, clock0Params);
Idle.idleFxns[0] = "&GpakIdleTaskFunc";

/* Enable cache for DDR3 addresses 0x80000000 thru 0x97ffffff  */
Cache.setMarMeta(0x80000000, 0x18000000, 1);

/* Disable cache for DDR3 addresses 0x98000000 thru 0x9fffffff  */
Cache.setMarMeta(0x98000000, 0x08000000, 0);

/* create per-core heap for dynamic module instance creation */

var hm2Params                           = new HeapMem.Params;
hm2Params.size                          = 4096;
hm2Params.sectionName                   = ".myHeap";
Program.global.heap1                    = HeapMem.create(hm2Params);
Defaults.common$.instanceHeap           = Program.global.heap1;
Program.sectMap[".myHeap"]              = new Program.SectionSpec();
Program.sectMap[".myHeap"].loadSegment  = "L2SRAM";

//var tskSlaveThread       = Task.create("&gpak_ipc_receive");
//tskSlaveThread.stackSize = 0x2000;
//tskSlaveThread.priority  = 0x5;
//tskSlaveThread.instance.name = "gpak_ipc_receive"
