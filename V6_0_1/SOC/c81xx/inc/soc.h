// partial SOC definitions for 814x and 816x chips

#ifndef _SOC_H_
#define _SOC_H_

#define CSL_CACHE_0_REGS            (0x01840000u)

#define CSL_EDMA3_NUM_DMACH         64
#define CSL_EDMA3_NUM_QDMACH        8
#define CSL_EDMA3_CHA_CNT           (CSL_EDMA3_NUM_DMACH + CSL_EDMA3_NUM_QDMACH)

#define CSL_EDMA30CC_0_REGS         (0x09000000u)
#define CSL_EDMA30TC_0_REGS         (0x09800000u)
#define CSL_EDMA30TC_1_REGS         (0x09900000u)
#define CSL_EDMA30TC_2_REGS         (0x09A00000u)
#define CSL_EDMA30TC_3_REGS         (0x09B00000u)

// EDMA synchronization events
#define CSL_EDMA3_CHA_MCASP0_TX     8
#define CSL_EDMA3_CHA_MCASP0_RX     9
#define CSL_EDMA3_CHA_MCASP1_TX     10
#define CSL_EDMA3_CHA_MCASP1_RX     11
#define CSL_EDMA3_CHA_MCASP2_TX     12
#define CSL_EDMA3_CHA_MCASP2_RX     13
#define CSL_EDMA3_CHA_MCASP3_TX     56
#define CSL_EDMA3_CHA_MCASP3_RX     57 
#define CSL_EDMA3_CHA_MCASP4_TX     62
#define CSL_EDMA3_CHA_MCASP4_RX     63
#define CSL_EDMA3_CHA_MCASP5_TX     89
#define CSL_EDMA3_CHA_MCASP5_RX     90
#define CSL_EDMA3_CHA_MCBSP0_TX     14
#define CSL_EDMA3_CHA_MCBSP0_RX     15


#define CSL_MCASP_PER_CNT           6

#define CSL_MCASP_0_CTRL_REGS       (0x08038000u)
#define CSL_MCASP_0_FIFO_REGS       (0x08039000u)
#define CSL_MCASP_0_DATA_REGS       (0x46000000u)

#define CSL_MCASP_1_CTRL_REGS       (0x0803C000u)
#define CSL_MCASP_1_FIFO_REGS       (0x0803D000u)
#define CSL_MCASP_1_DATA_REGS       (0x46400000u)

#define CSL_MCASP_2_CTRL_REGS       (0x08050000u)
#define CSL_MCASP_2_FIFO_REGS       (0x08051000u)
#define CSL_MCASP_2_DATA_REGS       (0x46800000u)

#define CSL_MCASP_3_CTRL_REGS       (0x0A1A2000u)
#define CSL_MCASP_3_FIFO_REGS       (0x0A1A3000u)
#define CSL_MCASP_3_DATA_REGS       (0x4A1A5000u)

#define CSL_MCASP_4_CTRL_REGS       (0x0A1A8000u)
#define CSL_MCASP_4_FIFO_REGS       (0x0A1A9000u)
#define CSL_MCASP_4_DATA_REGS       (0x4A1AB000u)

#define CSL_MCASP_5_CTRL_REGS       (0x0A1AE000u)
#define CSL_MCASP_5_FIFO_REGS       (0x0A1AF000u)
#define CSL_MCASP_5_DATA_REGS       (0x4A1B1000u)

#define CSL_MCBSP_PER_CNT           1
#define CSL_MCBSP_0_CTRL_REGS       (0x47000000u) 


#define CSL_INTC_0_REGS             (0x01800000u)

// System Interrupt Event Sources
#define CSL_INTC_EVENTID_EVT0                   0         
#define CSL_INTC_EVENTID_EVT1                   1         
#define CSL_INTC_EVENTID_EVT2                   2
#define CSL_INTC_EVENTID_EVT3                   3         
#define CSL_INTC_EVENTID_EDMA3_0_CC0_INT1       20
#define CSL_INTC_EVENTID_EDMA3_0_CC0_ERRINT     21
#define CSL_INTC_EVENTID_EDMA3_0_TC0_ERRINT     22
#define CSL_INTC_EVENTID_EDMA3_0_TC1_ERRINT     27
#define CSL_INTC_EVENTID_MCASP0_TXINT           70
#define CSL_INTC_EVENTID_MCASP0_RXINT           71     
#define CSL_INTC_EVENTID_MCASP1_TXINT           72
#define CSL_INTC_EVENTID_MCASP1_RXINT           73     
#define CSL_INTC_EVENTID_MCASP2_TXINT           74     
#define CSL_INTC_EVENTID_MCASP2_RXINT           75     
#define CSL_INTC_EVENTID_MCBSP0_INT             76     
#define CSL_INTC_EVENTID_MCASP3_TXINT           80     
#define CSL_INTC_EVENTID_MCASP3_RXINT           81     
#define CSL_INTC_EVENTID_MCASP4_TXINT           82     
#define CSL_INTC_EVENTID_MCASP4_RXINT           83     
#define CSL_INTC_EVENTID_MCASP5_TXINT           84     
#define CSL_INTC_EVENTID_MCASP5_RXINT           85     
#define CSL_INTC_EVENTID_INTERR                 96

#ifdef CHIP_814
#define EDMA_L3_ADDR 0x40000000     // 814x edma uses this address offset to access L1D and L2 ram
#endif

#ifdef CHIP_816
#define EDMA_L3_ADDR 0              // 816x doesn't need an address offset
#endif

#endif // _SOC_H_
