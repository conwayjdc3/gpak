/*  ============================================================================
 *  Copyright (c) Texas Instruments Inc 2002, 2003, 2004, 2005, 2006
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *  ===========================================================================
 */
/* =============================================================================
 *  Revision History
 *  ===============
 *  04-Sep-2006 Raghu File Created
 *  21-Sep-2006 NG    Updated the file.
 *  22-Sep-2006 Shiv  Updated EDMA related stuffs
 *  07-Oct-2006 NG    Updated VPSS related stuffs
 * =============================================================================
 */
#ifndef _SOC_H_
#define _SOC_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <cslr.h>
/**************************************************************************\
* 64 soc file
\**************************************************************************/

/*****************************************************************************\
* Static inline definition
\*****************************************************************************/
#ifndef CSL_IDEF_INLINE
#define CSL_IDEF_INLINE static inline
#endif

/**************************************************************************\
* Peripheral Instance count
\**************************************************************************/

/** @brief Number of Emac instances */
#define CSL_EMAC_PER_CNT 				 1

/** @brief Number of MDIO instances */
#define CSL_MDIO_PER_CNT 				 1

/**************************************************************************\
* Peripheral Instance definitions.
\**************************************************************************/

/** @brief Peripheral Instance for EMAC */
#define  CSL_EMAC                       (0) 

/** @brief Peripheral Instances for MDIO */
#define CSL_MDIO           	            (0) 

/**************************************************************************\
* Peripheral Base Address
\**************************************************************************/

/** @brief Base address of EMAC memory mapped registers */
#define CSL_EMAC_0_REGS                 (0x01E23000u)

/** @brief MDIO Module memory mapped address    */
#define CSL_MDIO_0_REGS                 (0x01E24000u)

/** @brief Base address of ECTL memory mapped registers */
#define CSL_ECTL_0_REGS                 (0x01E22000u)

/******************************************************************************\
* EMAC Descriptor section
\******************************************************************************/
#define CSL_ECTL_BASE_ADDR              (0x01E22000u)
#define CSL_EMAC_DSC_BASE_ADDR          (0x01E20000u)


/*****************************************************************************\
* Interrupt Event IDs
\*****************************************************************************/
/**
 * @brief   Interrupt Event IDs
 */

/* CPGMAC Wrapper interrupt */
#define    CSL_CIC3_EVENTID_MACINT0         (26)    /* Ethernet MAC interrupt */
#define    CSL_CIC3_EVENTID_MACINTR0        (27)    /* Ethernet MAC interrupt */
#define    CSL_CIC3_EVENTID_MACINTX0        (28)    /* Ethernet MAC interrupt */
#define    CSL_CIC3_EVENTID_MACINT1         (30)    /* Ethernet MAC interrupt */
#define    CSL_CIC3_EVENTID_MACINTR1        (31)    /* Ethernet MAC interrupt */
#define    CSL_CIC3_EVENTID_MACINTX1        (32)    /* Ethernet MAC interrupt */

#ifdef __cplusplus
}
#endif

#endif  /* _SOC_H_ */

