#ifndef _POWER_H
#define _POWER_H

#include "adt_typedef.h"
typedef struct
{
	ADT_Int32 SampleCount;
	ADT_Int32 Interval;
	ADT_Int64 PowerSum;
	ADT_Int16 CurrentPowerdBm10;
}		Power_t;

void PWR_ADT_initObj(
	Power_t *pPowerChannel, // Instance Structure
	ADT_Int32 IntervalInSamples // Power analysis window, in samples
	);
ADT_Int16 PWR_ADT_update(
		Power_t *pPowerChannel, //Instance structure
		short int *pSamples, // pointer to array of input samples
		short int NSamples, //length of input array
		short int *pUpdateFlag	// Set if new power computation was made (interval expired)
	);


#endif //_POWER_H

