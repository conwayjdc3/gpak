/*
 * Copyright (c) 2001-2007, Adaptive Digital Technologies, Inc.
 *
 * File Name: GpakExts.h
 *
 * Description:
 *   This file contains G.PAK external data and function declarations.
 *
 * Version: 1.0
 *
 * Revision History:
 *   11/13/01 - Initial release.
 *
 */

#ifndef _GPAKEXTS_H  /* prevent multiple inclusion */
#define _GPAKEXTS_H

#include <std.h>
#include <string.h>
#include "GpakDefs.h"
#include "GpakEnum.h"
#include "GpakErrs.h"
#include "sysmem.h"
#include "sysconfig.h"

//-----------------------------------------------------------------------
//{ System Configuration related constants and variables.
extern sysConfig_t sysConfig;           // System Configuration Info

// Gpak Software interrupts
extern far SWI_Handle SWI_Dma,   SWI_RTDX;   
extern far SWI_Handle SWI_Frame1, SWI_Frame2, SWI_Frame3;
extern far SWI_Handle SWI_Frame4, SWI_Frame5, SWI_Frame6;
extern far SWI_Handle SWI_Frame7;
extern int TDMRate;

// SWI priorities (higher number = higher priority)
#define DMAPriority      13
#define HostXferPriority 12
#define TimerPriority    11   // Set in .tci file
#define FramePriority    10   // supports at least 1 custom frame rate
#define MsgPriority       2
#define SimPriority       2
#define RtdxPriority      1


//}-----------------------------------------------------------------------
//{ Channel / algorithm structures
extern volatile ADT_UInt16 NumActiveChannels;  // number of active channels
extern struct chanInfo * chanTable[];        // pointers to Channel structures
extern chanInfo_t       GpakChanInstance[];
extern VADCNG_Instance_t *VadCngInstance[];

// Conference instance
extern ConfScratch_t       ConfScratch[];     // Conference scratch data
extern ConferenceInfo_t   *ConferenceInfo[];  // G.Pak conference structure
extern ConfInstance_t     *ConferenceInst[];  // Conference instance data
extern conf_Member_t      *ConfMemberData[];  // Conference member data
extern VADCNG_Instance_t  *ConfVadData[];     // Conference member VAD data
extern AGCInstance_t      *ConfAgcInstance[];
extern TGInstance_t       *ConfToneGenInstance[];
extern TGParams_1_t       *ConfToneGenParams[];

// Echo canceller instances
extern G168Params_t g168_PCM_EcParams;  // PCM Echo Canceller parameters
extern G168Params_t g168_PKT_EcParams;  // Packet Echo Canceller parameters

extern ADT_Word   NumPcmEcansUsed;      // number of PCM Echo Cancellers in use
extern ADT_Word   NumPktEcansUsed;      // number of Packet Echo Cancellers in use

extern ADT_UInt16 PcmEcInUse[];         // flag indicating PCM Echo Canceller in use
extern ADT_UInt16 PktEcInUse[];         // flag indicating Packet Echo Canceller in use

extern ADT_UInt16 *pPcmEcChan[];        // pointer to PCM Echo Canceller channels
extern ADT_UInt16 *pPktEcChan[];        // pointer to Packet Echo Canceller channels

// AEC instances
#ifdef AEC_SUPPORTED
extern IAECG4_Params    AECConfig;          // global parameter structure
extern IALG_MemRec      AECMemTab[];        // AEC memory requirements
extern IAECG4_Handle    AECHandles[];       // list of AEC handles
#endif

extern ADT_UInt32       AECInUse[];         // flag indicating Echo Canceller in use
extern ADT_Word         NumAECEcansUsed;    // number of acoustic Echo Cancellers in use
extern ADT_Int64        *AECChan[];         // AEC instance data
extern ADT_UInt16       *pAECFarPool[];     // pointer to Aec far data pool


// AGC instances
extern ADT_UInt16      numAGCChansAvail;
extern ADT_UInt16      agcChanInUse[];
extern AGCInstance_t  *AgcInstance[];

// Tone generation instances
extern ADT_UInt16   numToneGenChansAvail;
extern ADT_UInt16   toneGenChanInUse[];
extern TGInstance_t *ToneGenInstance[];
extern TGParams_1_t *ToneGenParams[];
extern DtmfDialInfo_t *DtmfDialInstance[];

// Tone detection instances
extern ADT_UInt16    ToneDetectorInUse [];
extern ADT_UInt16    ToneDetectorsAvail;
extern ADT_UInt16    ToneDetectorsTotal;
extern TDInstance_t  *ToneDetInstance[];

// FAX tone detection instances
extern ADT_UInt16       CedDetInUse[];
extern ADT_UInt16       CedDetAvail;
extern ADT_UInt16       CedDetTotal;
extern FaxCEDChannel_t *faxCedInstance[];

extern ADT_UInt16        CngDetInUse[];
extern ADT_UInt16        CngDetAvail;
extern ADT_UInt16        CngDetTotal;
extern FAXCNGInstance_t *faxCngInstance[];

// Tone relay instances
extern TRDetectInstance_t *ToneRlyDetInstance[];
extern TRGenerateInstance_t *ToneRlyGenInstance[];
extern TRGenerateEvent_t    *ToneRlyGenEvent[];
extern int numPastToneRlyGenEvents;

// Arbitrary tone detector Instance Data
extern ADT_UInt16 ArbToneDetectorsTotal;
extern ADT_UInt16 ArbToneDetectorsAvail;
extern ADT_UInt16 ArbToneDetectorInUse[];
extern ARBIT_TDInstance_t *ArbToneDetInstance[];

// Arbitrary tone detector Configurations
extern ADT_UInt16 numArbToneCfgs;
extern ADT_UInt16 activeArbToneCfg;
extern ADT_UInt16 ArbToneConfigured[];
extern ArbitDtInstance_t* ArbToneCfgInst[];
extern Tones_Index_t     ArbToneCfg[];
extern ARBIT_ADT_Param_t ArbToneCfgParams[];
extern ADT_Int16         ArbToneCfgFreqs[][16];

// Noise suppression instances
extern NCAN_Params_t NCAN_Params;            // Noise Suppression paramaeters
extern ADT_Int16     NoiseRemnantPool[];
extern NCAN_Channel_t  *NoiseSuppressInstance[][2];

// T.38 FAX instances
extern ADT_UInt8   *pFaxChan[];
extern ADT_UInt16   faxChanInUse[];
extern ADT_UInt16   numFaxChansUsed;
extern t38Packet_t *pT38Packets[]; 

// Rx CID Instances
extern ADT_UInt16    numRxCidChansAvail;
extern ADT_UInt16    rxCidInUse[];
extern CIDRX_Inst_t *rxCidPool[];
extern ADT_UInt8     rxCidBuffer[];
extern ADT_UInt16    rxCidBufferI8;

// Tx CID Instances
extern ADT_UInt16    numTxCidChansAvail;
extern ADT_UInt16    txCidInUse[];
extern CIDTX_Inst_t *txCidPool[];
extern ADT_UInt8     txCidMsgBuffer[];
extern ADT_UInt8     txCidBrstBuffer[];
extern ADT_UInt16    txCidBufferI8;
extern ADT_UInt16    txCidBurstI8;

// RTP Instances
extern RtpChanData_t *RtpChanData [];
extern SrtpTxInstance_t   *SrtpTxChanData [];
extern SrtpRxInstance_t   *SrtpRxChanData [];
extern  GpakSrtpKeyInfo_t *SrtpRxKeyData[];
extern  GpakSrtpKeyInfo_t *SrtpTxKeyData[];
extern ADT_UInt32 SrtpScratchPtr[];

extern ADT_UInt32    SmallJBPool[];     // Memory pool for 1, 2.5 and 5 millisecond frame rates
extern ADT_UInt32    LargeJBPool[];     // Memory pool for 10, 20, 22.5 and 30 millisecond frame rates
extern ADT_UInt32    HugeJBPool[];      // Memory pool for > 30 millisecond frame rates

extern CircBufInfo_t RTPCircBuffers[2]; // Host interface buffers 1 = To Net; 0 = To DSP
extern ADT_UInt16    ToNetRTPBuffer[];
extern ADT_UInt16    ToDSPRTPBuffer[];

extern TI_T38 TI_Fax[];

// SRC instances
extern ADT_UInt16      numSrcUpBy2ChansAvail;
extern ADT_UInt16      srcUpBy2ChanInUse[];
extern RateUpBy2_Channel_t  *SrcUpBy2Instance[];
extern ADT_UInt16      numSrcDownBy2ChansAvail;
extern ADT_UInt16      srcDownBy2ChanInUse[];
extern RateUpBy2_Channel_t  *SrcDownBy2Instance[];

//}

//-----------------------------------------------------------------------
//{  Memory buffers
extern const blockStatus_t blockStatus;   // memory block allocation spec

extern ADT_UInt16 *encoderPool[];         // channel memory pool
extern ADT_UInt16 *decoderPool[];         // channel memory pool

extern ADT_UInt16 PcmInBufferPool[];      // PCM Input buffer pool
extern ADT_UInt16 PcmOutBufferPool[];     // PCM Output buffer pool
extern ADT_UInt16 PktInBufferPool[];      // Packet Input buffer pool
extern ADT_UInt16 PktOutBufferPool[];     // Packet Output buffer pool
extern ADT_UInt16 BulkDelayBufferPool[];  // Bulk Delay buffer pool


extern ADT_PCM16 BSP0DMA_TxBuffer[];      // DMA Tx buffer for McBSP0
extern ADT_PCM16 BSP0DMA_RxBuffer[];      // DMA Rx buffer for McBSP0
extern ADT_PCM16 BSP1DMA_TxBuffer[];      // DMA Tx buffer for McBSP1
extern ADT_PCM16 BSP1DMA_RxBuffer[];      // DMA Rx buffer for McBSP1
extern ADT_PCM16 BSP2DMA_TxBuffer[];      // DMA Tx buffer for McBSP2
extern ADT_PCM16 BSP2DMA_RxBuffer[];      // DMA Rx buffer for McBSP2

extern ADT_UInt16       *pSlotMap[];      // pointers to the DMA SlotMap buffers
extern ADT_UInt16       *pTxSlotMap[];      // pointers to the DMA SlotMap buffers
extern CircBufInfo_t  **pCrcRxBuff[];     // pointers to the DMA Rx CircBufLists
extern CircBufInfo_t  **pCrcTxBuff[];     // pointers to the DMA Tx CircBufLists

extern EventFifoMsg_t eventFIFOBuff[];
extern CircBufInfo_t  eventFIFOInfo;
extern CircBufInfo_t  pktBuffStructTable[]; // Host/DSP circular buffers

extern ADT_UInt16 PlayRecStartAddr[];

// Buffer Pools
extern ADT_UInt16 PcmBuffAvail;
extern ADT_UInt16 PcmBuffInUse[];
extern ADT_UInt16 BulkDelayBuffAvail;
extern ADT_UInt16 BulkDelayBuffInUse[];
extern ADT_UInt16 PktBuffAvail;
extern ADT_UInt16 PktBuffInUse[];

// equalizer
extern EqChannel_t  micEqChan[];
extern EqChannel_t  spkrEqChan[];
extern ADT_Int16    *pMicEqState[];
extern ADT_Int16    *pSpkrEqState[];
extern ADT_Int16    micEqCoef[];
extern ADT_Int16    spkrEqCoef[];

// Multicore variables
extern ADT_UInt32 CoreOffsets [];
extern far int DSPCore;
extern int DSPTotalCores;


// task work buffers
extern ADT_PCM16    inWork_1msec[],    outWork_1msec[],    ECFarWork_1msec[];
extern ADT_PCM16  inWork_2_5msec[],  outWork_2_5msec[],    ECFarWork_2_5msec[];
extern ADT_PCM16    inWork_5msec[],    outWork_5msec[],    ECFarWork_5msec[];
extern ADT_PCM16   inWork_10msec[],   outWork_10msec[],    ECFarWork_10msec[];
extern ADT_PCM16   inWork_20msec[],   outWork_20msec[],    ECFarWork_20msec[];
extern ADT_PCM16  inWork_22_5msec[], outWork_22_5msec[],   ECFarWork_22_5msec[];
extern ADT_PCM16   inWork_30msec[],   outWork_30msec[],    ECFarWork_30msec[];

extern MipsConserve_t G168MipsConserve_1ms;
extern MipsConserve_t G168MipsConserve_2_5ms;
extern MipsConserve_t G168MipsConserve_5ms;
extern MipsConserve_t G168MipsConserve_10ms;
extern MipsConserve_t G168MipsConserve_20ms;
extern MipsConserve_t G168MipsConserve_22_5ms;
extern MipsConserve_t G168MipsConserve_30ms;

// algorithm scratch buffers
extern ADT_UInt16 G168DAscratch_1ms[];
extern ADT_UInt16 G168DAscratch_2_5ms[];
extern ADT_UInt16 G168DAscratch_5ms[];
extern ADT_UInt16 G168DAscratch_10ms[];
extern ADT_UInt16 G168DAscratch_20ms[];
extern ADT_UInt16 G168DAscratch_22_5ms[];
extern ADT_UInt16 G168DAscratch_30ms[];

extern union { ADT_UInt16 scratch[1]; } G168SAscratch_1ms;
extern union { ADT_UInt16 scratch[1]; } G168SAscratch_2_5ms;
extern union { ADT_UInt16 scratch[1]; } G168SAscratch_5ms;
extern union { ADT_UInt16 scratch[1]; } G168SAscratch_10ms;
extern union { ADT_UInt16 scratch[1]; } G168SAscratch_20ms;
extern union { ADT_UInt16 scratch[1]; } G168SAscratch_22_5ms;
extern union { ADT_UInt16 scratch[1]; } G168SAscratch_30ms;

//}

//========================================================================
// Function prototypes and their return values.
//========================================================================

//--------------------------------------------------------------
//{--- System Initialization
extern void InitGpakInterface(void);
extern void InitGpakPcm(void);
extern void InitFrameTasks(void);
extern struct chanInfo *AllocateInstanceStructures (ADT_UInt16 chanId);
extern void wordUnlock (volatile int *lockField);
extern int wordLock (int DspCore, int volatile *lockField);

//}--------------------------------------------------------------
//{--- McBsp Statistics
extern void ResetMcBspStats();
extern void ReadPortStats(int portId, PortStat_t *pSt);


//}--------------------------------------------------------------
//{--- Channel setup/tear down
extern GPAK_ChannelConfigStat_t setupChannel (chanInfo_t *);
extern void SetupPcm2PcmChan  (chanInfo_t *pChanInfo);
extern void SetupPcm2PktChan  (chanInfo_t *pChanInfo);
extern void SetupPkt2PktChan  (chanInfo_t *pChanInfo);
extern void SetupCircDataChan (chanInfo_t *pChanInfo);
extern void SetupConfPcmChan  (chanInfo_t *pChanInfo);
extern void SetupConfPktChan  (chanInfo_t *pChanInfo);
extern void SetupConfCompChan (chanInfo_t *pChanInfo);
extern GPAK_ChannelConfigStat_t ActivateChannelPktPkt (chanInfo_t *chan);
extern GPAK_ChannelConfigStat_t ActivateChannel (chanInfo_t *);
extern GPAK_TearDownChanStat_t  DeactivateChannel (chanInfo_t *);
extern void setCircPointers (chanInfo_t *chan,
                      pcm2pkt_t *pcmToPkt,  pkt2pcm_t *pktToPcm, 
                      ADT_UInt16 APhase,    ADT_UInt16 BPhase);
extern void tearDownChannel (chanInfo_t *);
extern void DeactivateLpbkCoder(chanInfo_t *pChan);
extern void activateMsgHandler ();

extern void ClampScaleGain(gains_t *g);
extern int ValidNLP(int NLP);
extern int ValidFrameSize (int FrameSize);
extern int ValidCoding    (GpakCodecs, int FrameSize);
extern GPAK_ChannelConfigStat_t ValidToneTypes (GpakToneTypes, int *mfTones, int *cedTones, int *cngTones, int *arbTones);
extern GpakVADMode SetVADMode (GpakCodecs Codec);
extern GpakVADMode SetPLCMode (GpakCodecs Codec);

extern ADT_Word FrameRatePhaseCount (int coreID, int SamplesPerFrame);

extern chanInfo_t **GetPcmToPktQueueHead (int coreID, int SamplesPerFrame);
extern chanInfo_t **GetPktToPcmQueueHead (int coreID, int SamplesPerFrame);
extern ConferenceInfo_t *GetGpakCnfr     (int CoreID, int CnfrID);


extern void AddtoPrePendQueue (chanInfo_t *);
extern void DequeueChannel (chanInfo_t *, chanInfo_t **AHead, chanInfo_t **BHead);
 

extern void initDecoder (pkt2pcm_t *, int scratchFlag, int rate);
extern void initEncoder (pcm2pkt_t *, int scratchFlag, int rate);
extern void InitToneDetection (GpakToneTypes,  TDInst_t *, TRDetectInstance_t *, int SamplesPerFrame);
extern void InitToneRelayGen  (TGInfo_t *);
extern TDScratch_t * getToneDetScratchPtr (int samplesPerFrame);
extern void reInitASideToneDetect (pcm2pkt_t *PcmPkt, int disableTones, int enableTones);
extern void reInitBSideToneDetect (pkt2pcm_t *PktPcm, int disableTones, int enableTones);
extern void *getBGScratch (ADT_UInt16 *bgScratchI16);
extern void EchoCancellerReInit (ADT_UInt16 FrameSize, ADT_UInt16 EcID, G168ChannelInstance_t *pInst, chanInfo_t *chan);
extern void InitCedCngToneDetection (GpakToneTypes toneType, FaxCEDChannel_t **pCedDet,
                             FAXCNGInstance_t **pCngDet);
extern void InitVadCng (VADCNG_Instance_t *vadInst, VADCNG_ADT_Param_t *vadParams, int vadSamplesPerFrame, int cngSamplesPerFrame, int ProcessRate) ;

extern G168ChannelInstance_t * AllocEchoCanceller (ADT_UInt16 EcanType, ADT_UInt16 FrameSize, ADT_UInt16 *EcID, ADT_UInt32 SampleRate);
extern void AllocToneGen   (TGInfo_t *, int enabled);
extern void AllocAGC       (chanInfo_t *, GpakDeviceSide_t, GpakActivation);
extern void AllocFaxRelay  (chanInfo_t *);
extern void *AllocUpSamplingInst   (ADT_UInt16 *index);
extern void *AllocDownSamplingInst (ADT_UInt16 *index);
extern void AllocEncoderSampleRateConv       (chanInfo_t *);
extern void AllocDecoderSampleRateConv       (chanInfo_t *);
extern void DeallocEncoderSampleRateConv     (chanInfo_t *);
extern void DeallocDecoderSampleRateConv     (chanInfo_t *);
extern void DeallocEcans   (chanInfo_t *);
extern void DeallocToneGen (chanInfo_t *);
extern void DeallocAGC     (chanInfo_t *);
extern void DeallocToneDetectors (chanInfo_t *);
extern void DeallocFAXToneDetect (chanInfo_t *pChan);
extern void DeallocFaxRelay(chanInfo_t *);

extern void AllocBuffs(chanInfo_t   *chan);
extern void DeAllocBuffs(chanInfo_t *chan);
extern int CheckIfBuffsAvail(GpakActivation pktEc);

extern void getRxCidPtrs(int i, CIDRX_Inst_t **ppRxCidChan, ADT_UInt8 **pRxCidBuf);
extern void getTxCidPtrs(int i, CIDTX_Inst_t **ppTxCidChan, ADT_UInt8 **pTxCidMsgBuf, ADT_UInt8 **pTxCidBrstBuf);
extern void AllocCID (chanInfo_t *pChan, GpakDeviceSide_t deviceSide, GpakCidMode_t rxEnab, GpakCidMode_t txEnab);
extern void DeallocCID (chanInfo_t *pChan);
extern void NullCID (cidInfo_t *pCid);
extern void txCidProcess(int chanId, GpakDeviceSide_t side, cidInfo_t *pInfo, ADT_PCM16 *pcmBuff, int FrameSize);
extern void rxCidInit(cidInfo_t *pInfo, int FrameSize);
extern void rxCidProcess(int chanId, GpakDeviceSide_t side, cidInfo_t *pInfo, ADT_PCM16 *pcmBuff, int FrameSize);
extern void rxCid2SndProcess(int chanId, GpakDeviceSide_t side, cidInfo_t *pInfo, ADT_PCM16 *pcmBuff, int FrameSize);
extern void txCid2RcvProcess(int chanId, GpakDeviceSide_t side, cidInfo_t *pInfo, ADT_PCM16 *pcmBuff, int FrameSize);
extern ADT_Int32 *getCidScratchPtr(int FrameSize);

extern void InitArbToneDetection (ARBIT_TDInstance_t **pArbChannel, ADT_UInt32 *CfgId);
extern void ArbToneDetect (ARBIT_TDInstance_t *pArbToneDet, ADT_Int16 *pcmBuff, int FrameSize, int ChannelId, 
                           GpakDeviceSide_t deviceSide, ADT_UInt32 FrameTimeStamp, ADT_UInt32 *ToneTimeStamp);
extern void DeallocArbToneDetect (chanInfo_t *pChan);
extern ARBIT_TDInstance_t *AllocArbToneDetectors ();
extern int removeFromIPSession (ADT_UInt16 ChanId);
extern void SetupLoopbackCoder (chanInfo_t *chan);
extern void InitNoiseSuppression (NCAN_Channel_t *NoiseInst, NCAN_Params_t *NCAN_Params);


//}--------------------------------------------------------------------------------
//{--- Messaging

// 
extern GPAK_PortConfigStat_t    ProcConfigSerialPortsMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply);

// Channel setup prototypes.
extern GPAK_ChannelConfigStat_t processChannelMsg  (chanInfo_t *, ADT_UInt16 *pCmd);
extern GPAK_ChannelConfigStat_t ProcCfgPcm2PcmMsg  (ADT_UInt16 *pCmd, chanInfo_t *pChan);
extern GPAK_ChannelConfigStat_t ProcCfgPcm2PktMsg  (ADT_UInt16 *pCmd, chanInfo_t *pChan);
extern GPAK_ChannelConfigStat_t ProcCfgPkt2PktMsg  (ADT_UInt16 *pCmd, chanInfo_t *pChan);
extern GPAK_ChannelConfigStat_t ProcCfgCircDataMsg (ADT_UInt16 *pCmd, chanInfo_t *pChan);
extern GPAK_ChannelConfigStat_t ProcCfgConfPcmMsg  (ADT_UInt16 *pCmd, chanInfo_t *pChan);
extern GPAK_ChannelConfigStat_t ProcCfgConfPktMsg  (ADT_UInt16 *pCmd, chanInfo_t *pChan);
extern GPAK_ChannelConfigStat_t ProcCfgConfCompMsg (ADT_UInt16 *pCmd, chanInfo_t *pChan);
extern GPAK_ChannelConfigStat_t ProcCfgLpbkMsg     (ADT_UInt16 *pCmd, chanInfo_t *pChan);

extern GPAK_RTPConfigStat_t     ProcConfigRTPMsg   (ADT_UInt16 *pCmd, ADT_UInt16 *pReply);
extern GPAK_RTPConfigStat_t     ProcConfigRTPv6Msg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply);
extern GPAK_ConferConfigStat_t  ProcConfigConferenceMsg (ADT_UInt16 *pCmd);
extern ADT_UInt16 ProcConfToneGenMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply);

// Channel status prototypes
extern ADT_UInt16 ProcPcm2PcmStatMsg  (ADT_UInt16 *pReply, chanInfo_t *pChan);
extern ADT_UInt16 ProcPcm2PktStatMsg  (ADT_UInt16 *pReply, chanInfo_t *pChan);
extern ADT_UInt16 ProcPkt2PktStatMsg  (ADT_UInt16 *pReply, chanInfo_t *pChan);
extern ADT_UInt16 ProcCircDataStatMsg (ADT_UInt16 *pReply, chanInfo_t *pChan);
extern ADT_UInt16 ProcConfPcmStatMsg  (ADT_UInt16 *pReply, chanInfo_t *pChan);
extern ADT_UInt16 ProcConfPktStatMsg  (ADT_UInt16 *pReply, chanInfo_t *pChan);
extern ADT_UInt16 ProcConfCompStatMsg (ADT_UInt16 *pReply, chanInfo_t *pChan);
extern ADT_UInt16 ProcCfgLpbkStatMsg  (ADT_UInt16 *pCmd, chanInfo_t *pChan);


extern int updateTdmLoopback   (GpakSerialPort_t, GpakActivation);
extern void LEC_ADT_coefLoad   (G168ChannelInstance_t *, Int16 *load_p);
extern void LEC_ADT_coefStore  (G168ChannelInstance_t *, Int16 *store_p);
extern ADT_Int32 getCoreOffset (ADT_UInt16 CoreID);

#define addByteOffset(ptr,offset) (((ADT_Int32) ptr) + offset)

extern void SendWarningEvent   (ADT_UInt16 ChanId, ADT_UInt16 Warning);
extern int writeEventIntoFifo  (EventFifoMsg_t *);
extern int ProcessGetVLANTagMsg (ADT_UInt16 *cmd, ADT_UInt16 *reply);
extern int ProcessSetVLANTagMsg (ADT_UInt16 *cmd, ADT_UInt16 *reply);
extern int ProcessUserTest (ADT_UInt16 testID, ADT_UInt16 param, ADT_UInt16 *rply);
extern ADT_UInt16 ProcReadNoiseParmsMsg (ADT_Int16 *pCmd, ADT_Int16 *pReply);
extern GPAK_SysParmsStat_t ProcWriteNoiseParmsMsg (ADT_Int16 *pCmd, ADT_UInt16 *pReply);
extern int ProcRTPStatusMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply);
extern void channelAllocsNull (chanInfo_t *chan, pcm2pkt_t *PcmPkt, pkt2pcm_t *PktPcm);
extern void CaptureText (char *s);
extern ADT_UInt16 pktPktLoopBack (int chan1, int chan2);
extern int TogglePCMInSignal (unsigned int port);
extern int TogglePCMOutSignal (unsigned int port);
extern void LogBuff (int flag, CircBufInfo_t *Buff);

//}--------------------------------------------------------------
//{--- Custom interfaces
typedef struct PhaseData {
   ADT_UInt16       Phase, sampsPerFrame;
   CircBufInfo_t    *inBuff, *outBuff, *ecBuff;
   ADT_UInt16       rxDelay, txDelay;
   GpakCompandModes rxCompand, txCompand;
} PhaseData;

extern int  customMsg           (ADT_UInt16 *Cmd, ADT_UInt16 *Reply);
extern void customTeardown (chanInfo_t *);
extern GPAK_ChannelConfigStat_t customProcChanCfgMsg (ADT_UInt16 *pCmd, chanInfo_t *pChan);
extern ADT_UInt16 customProcChanStatMsg (ADT_UInt16 *pReply, chanInfo_t *pChan);
extern void customSetupChan (chanInfo_t *pChan);
extern void customFrameDecode (chanInfo_t *pChan, void  *pktBuff, ADT_PCM16 *pcmBuff,
                      ADT_PCM16 *pECFarWork, int SampsPerFrame, int pcmBuffBytes);
extern void customFrameEncode (chanInfo_t *pChan, ADT_PCM16 *pcmBuff, void *pktBuff,
                     ADT_PCM16 *pECFarWork, int SampsPerFrame, int pktBuffBytes);
extern void customSetDevicePointers (chanInfo_t *chan, 
                      pcm2pkt_t *pcmToPkt,  pkt2pcm_t *pktToPcm, 
                      ADT_UInt16 APhase,    ADT_UInt16 BPhase);
extern void customProcessConference (int cnfrID, ConferenceInfo_t *Cnfr, ConfInstance_t *CnfrInst,
                       ADT_Int16 *pWork1, void *scratch);

extern void customDSPInit ();
extern void customDSPStartUp ();
extern void customTimer ();
extern void *customPktIn  (void *Hdr, ADT_UInt16 *Payload);
extern void *customPktOut (void *Hdr, ADT_UInt16 *Payload);

extern int customTDMOut (ADT_PCM16 *dataOut, int port, int slotCnt);
extern int customTDMIn  (ADT_PCM16 *dataIn,  int port, int slotCnt);

extern void custAecInput  (chanInfo_t *pChan, ADT_Int16 *pNear, ADT_Int16 *pFar, int FrameSize);
extern void custAecOutput (chanInfo_t *pChan, ADT_Int16 *pNear, ADT_Int16 *pFar, int FrameSize);
extern void customVad (int *vad);

extern int customEncodeInit (pcm2pkt_t *PcmPkt);
extern int customDecodeInit (pkt2pcm_t *PktPcm);
extern int customEncode     (chanInfo_t *pChan, void *pcm, void *params);
extern int customDecode     (chanInfo_t *pChan, void *params, void *pcm);
extern int customPack       (void *params, void *payload);
extern int customUnpack     (PktHdr_t *PktHdr, void *payload, void *params);
extern int customMsg (ADT_UInt16 *Cmd, ADT_UInt16 *Reply);
extern void customIdleFunc ();
extern void customGetPlaybackInstances (chanInfo_t *chan, GpakDeviceSide_t deviceSide,
                                 playRecInfo_t **playInfo, playRecInfo_t **recInfo, ADT_UInt16 *frameSize);
extern void* (*customNetIn) (ADT_UInt32 ip, ADT_UInt16 port, ADT_UInt8 *payload);
extern void customInvalidateChannelCache  (chanInfo_t *chan); 


extern void captureDecoderInput  (chanInfo_t *chan, void *data, int samples);
extern void captureDecoderOutput (chanInfo_t *chan, void *data, int samples);
extern void captureEncoderInput  (chanInfo_t *chan, void *data, int samples);
extern void captureEncoderOutput (chanInfo_t *chan, void *data, int samples);
extern ADT_UInt8 appendCheckSum (void *Buff, int BuffI8);

//}--------------------------------------------------------------
//{--- Frame processing
extern ADT_Word *CpuUsage;
extern ADT_Word *CpuPkUsage;

extern int DecodePayload (chanInfo_t *chan, pkt2pcm_t *PktPcm, GpakPayloadClass PayClass,
                          ADT_UInt16 *params, int paramsI8, ADT_PCM16 *pcm, int pcmI8);
extern void voiceEncode (chanInfo_t *, void *pInWork, void *pOutWork, short sampleCnt);
extern void ProcessVadToneEncode (chanInfo_t *, ADT_PCM16 *Pcm, short *dtmfPcm, ADT_UInt16 *PKt, short SampCnt);
extern void ProcessVadToneOnDecodePath (chanInfo_t *chan, ADT_PCM16 *pcmBuff, ADT_Int16 *DTMFBuff, short SampCnt);
extern void toneDetect (toneDetInfo_t *tone, ADT_UInt40 FrameTimeStamp, ADT_UInt32 processRate, ADT_UInt16 SampsPerFrame);
extern void CedCngDetect (toneDetInfo_t *tone, TDInst_t *tdInstances, void *Scratch, ADT_Int16 *pcmBuff, 
                          int FrameSize8k, ADT_UInt40 FrameTimeStamp8k, ADT_UInt40 *ToneTimeStamp8k);

extern GpakAlgCtrl_t AlgorithmControl (chanInfo_t **p, frmType_t);

extern void slipReset         (chanInfo_t *, GpakDeviceSide_t);

extern void FramePcmA2PcmB   (chanInfo_t *, ADT_PCM16 *InWork,  ADT_PCM16 *OutWork,
                                            ADT_PCM16 *Scratch, int FrameSize);

extern void FramePcmB2PcmA  (chanInfo_t *, ADT_PCM16 *InWork,  ADT_PCM16 *OutWork,
                                            ADT_PCM16 *Scratch, int Size);

extern void FramePcmA2PktB   (chanInfo_t *, ADT_PCM16 *pcmBuff,    void *pktBuff,
                                            ADT_PCM16 *pECFarWork, int FrameSize);
                                            
extern void FramePktB2PcmA  (chanInfo_t *, void  *pktBuff, ADT_PCM16 *pcmBuff, ADT_PCM16 *pECFarWk,
                                            int FrameSize, int pcmBuffBytes);

extern void PktPktDecode  (chanInfo_t *, void *pktBuff, ADT_PCM16 *pcmBuff,  ADT_PCM16 *pECFarWk,
                                            int FrameSize, int pcmBuffBytes);

extern void PktPktEncode   (chanInfo_t *,  ADT_PCM16 *pcmBuff,    void *pktBuff,
                                            ADT_PCM16 *pECFarWork, int FrameSize); 

extern void FrameInCircData  (chanInfo_t *, ADT_PCM16 *pcmBuff,    void *pktBuff,
                                            ADT_PCM16 *pECFarWork, int FrameSize);
extern void FrameOutCircData (chanInfo_t *, void *pktBuff,  ADT_PCM16 *pcmBuff, ADT_PCM16 *pECFarWk,
                                             int FrameSize, int pcmBuffBytes);


extern void ProcessConference (ConferenceInfo_t *, ConfInstance_t *,  ADT_PCM16 *pInWork, void *pOutWork, void *work2, ADT_UInt16 workI8);

extern int      fromTDMCirc (chanInfo_t *chan, GpakDeviceSide_t deviceSide, void *pcm, int *SampsPerFrame, GpakRateCvt *cvt);
extern ADT_UInt16 toTDMCirc (chanInfo_t *chan, GpakDeviceSide_t deviceSide, void *pcm, int SampsPerFrame, GpakRateCvt *cvt);

extern int ToneRelayGenerate (TGInfo_t*,  int FrameSize, ADT_PCM16 *pcmOut);
extern void ToggleFrameTaskLED (void);
extern ADT_Bool parse2833Event (TRGenerateEvent_t* toneEvt, ADT_UInt16 *pyld,int SamplesPerFrame);
extern void playRecordEnd (int ChanId, GpakAsyncEventCode_t event, playRecInfo_t *inst, GpakDeviceSide_t deviceSide);
extern MipsConserve_t *getMipsConservePtr (ADT_UInt16 SamplesPerFrame);
extern void loopBackPkts (RTPToHostHdr_t *Hdr);
extern int PayloadToCircCache (CircBufInfo_t *circ, void *Hdr, int HdrI16, ADT_UInt8 *Payload, int PyldI8);
extern int SendPktToHost (chanInfo_t *chan, PktHdr_t *Hdr, ADT_UInt16 *Payload);
extern void runAEC (chanInfo_t *chan, void *pcmBuff, void *farWork, int SampsPerFrame, GpakRateCvt *rateCvt);
extern void ToneGenerate (TGInfo_t *tgInfo, ADT_PCM16 *pcmBuff, int FrameSize);
extern void voicePlayback (playRecInfo_t *play, void *pcm, void *scratch, int FrameSize, GpakDeviceSide_t deviceSide, int chanId);
extern void voiceRecord (playRecInfo_t *rec, void *pcm, void *scratch, int FrameSize, GpakDeviceSide_t deviceSide, int chanId);
                        

extern ADT_UInt8 netI8 (void *Buff, int *Offset);
extern ADT_UInt16 netI16 (void *Buff, int *Offset);
extern ADT_UInt32 netI32 (void *Buff, int *Offset);
extern void netIx (void *Buff, int *Offset, void *Value, int ValueLen);
extern void I8net (void *Buff, int *Offset, ADT_UInt8 value);
extern void I16net (void *Buff, int *Offset, ADT_UInt16 value);
extern void I32net (void *Buff, int *Offset, ADT_UInt32 value);
extern void Ixnet  (void *Buff, int *Offset, void *Value, int ValueLen);

//}--------------------------------------------------------------
//{--- Host port interface routines
extern void SetHostPortInterrupt (int frameSize);
extern void Gen_Host_Interrupt ();

extern void Gen_Core_Interrupt (int frameIdx, int coreID);
extern ADT_UInt32 Read_Core_Interrupt (int coreID);
extern void ACK_Core_Interrupt (int flags, int coreID);

extern GpakPayloadClass parsePacket  (chanInfo_t *, void *params, int *paramsI8, void *pyld, int pyldI8);
extern void buildGpakPacket (chanInfo_t *, GpakEvents, ADT_UInt16 p1,  ADT_UInt16 p2);

extern int PayloadToCirc (CircBufInfo_t *circ, void *Hdr, int HdrI16, ADT_UInt8 *Payload, int PyldI8);
extern int GetPktFromCircBuffer (CircBufInfo_t *circ, void *Hdr, int HdrI16, 
                                 ADT_UInt16 *Payload, ADT_UInt16 maxPayloadI8);
extern void RemovePktFromCircBuffer (CircBufInfo_t *circ, void *Hdr, void *Payload, int TakeIndex);

extern ADT_Word getFreeSpace (CircBufInfo_t *buf);
extern int T38_Add_Packet (chanInfo_t *pChan, void *pyld, int PayldBytes, ADT_UInt32 timeStamp);
extern ADT_UInt16 verifyPacketHeader (PktHdr_t *PktHdr, chanInfo_t *pChanInfo);
extern int addIPToRTPSessions (ADT_UInt32 rmtIPAddr, ADT_UInt16 rmtPort, ADT_UInt32 rmtSSRC, 
                        ADT_UInt8 rmtMAC[], void *RTPSession, ADT_UInt16 ChanId);
extern far void *getSAScratch (int SamplesPerFrame, ADT_UInt16 *len);
extern far void *getDAScratch (int SamplesPerFrame, ADT_UInt16 *len);
extern far void *getConfScratch (int SamplesPerFrame);
extern int getFrameIdx (int SamplesPerFrame);
extern ADT_Int16 updateEcChanCount(ADT_UInt16 DspCore, ADT_UInt16 SamplesPerFrame, ADT_UInt16 EcanType, ADT_Int16 val);
extern MipsConserve_t * getMipsConserve (ADT_UInt16 SamplesPerFrame);
extern void startChanCapture (ADT_Int16 chanId);

//}---------------------------------------------------------
//{--- Critical section locking

#define CNF_TONE_GEN_LOCK   0x10000
#define FRAME_BITS_RESERVED 0x0007F     // Corresponds to a maximum of 7 framing tasks

extern int  FRAME_lock   (ADT_Int32 locks);
extern void FRAME_unlock (ADT_Int32 locks, ADT_UInt32 mask);

extern void CHN_lock   (chanInfo_t *pChan);
extern void CHN_unlock (chanInfo_t *pChan);
extern int ADDRESS_lock (volatile int *lockAddress);
extern void ADDRESS_unlock (volatile int *lockAddress, int mask);
extern void  DMAInitialize ();
extern void SystemHalt (const char *format, ...);
extern void T38_Check_Timeout (chanInfo_t *pChan, int SamplesPerFrame);
extern void T38_Restart_Voice (chanInfo_t *pChan);

extern void DeallocUpConverter (ADT_UInt16 index);
extern void DeallocDownConverter (int index);
extern void dmaAllocateChains();
extern void Enable_Interrupt_From_Core0 (Fxn frameReadyISR);
extern int formatAMRPayload (ADT_UInt16 *AMRData, ADT_UInt16 *Payload);
extern void parseAMRPayload (ADT_UInt16 *Payload, ADT_UInt16 *AMRData);
extern void GpakRtdxSetup ();
extern int getPacketSize (GpakCodecs Coding, int FrameHalfMS);
extern void InitDownSamplingInst (void *Inst, int SamplesPerFrame);
extern void InitUpSamplingInst   (void *Inst, int SamplesPerFrame);
extern void invalidateChannelCache (chanInfo_t *chan);
extern void MultiRateCnfrFrame (chanInfo_t *chan,  void *pktBuff, ADT_PCM16 *pcmBuff,
                    ADT_PCM16 *pECFarWork, int SampsPerFrame, int pcmBuffI8);

extern void packLSB(unsigned short *inword,   // input buffer: unpacked data
         unsigned short *outword,   // output buffer: packed data
         unsigned short num,        // number of codewords to pack
         unsigned short width);      // width (in bits) of each codeword to pack


extern void unpackLSB(unsigned short *inword,   // input buffer: packed data
         unsigned short *outword,   // output buffer: unpacked data
         unsigned short num,        // number of codewords to unpack
         unsigned short width);      // width (in bits) of each codeword to unpack


extern void unpackMSB(unsigned short *inword,   // input buffer: packed data
         unsigned short *outword,   // output buffer: unpacked data
         unsigned short paramCnt,        // number of codewords to unpack
         unsigned short width,      // width (in bits) of each codeword to unpack
         unsigned short outLS);    // number of left shifts to apply to output data


extern void packMSB(unsigned short *inword,   // input buffer: unpacked data
         unsigned short *outword,      // output buffer: packed data
         unsigned short paramCnt,           // number of codewords to pack
         unsigned short width);         // width (in bits) of each codeword to pack



//}
//========================================================================
//     Differences related to DSP types
//========================================================================
//{
#if (DSP_TYPE == 54 || DSP_TYPE == 55)
   #define  i8cpy(dest, src, i8cnt)  memcpy (dest, src, (i8cnt+1)/2);
   #define i16cpy(dest, src, i16cnt) memcpy (dest, src, i16cnt);
   #define  wdcpy(dest, src, wdcnt)  memcpy (dest, src, wdcnt);

   #define CLEAR_INST_CACHE(buff, i8) ;
   #define FLUSH_INST_CACHE(buff, i8) ;
   #define FLUSH_wait()               ;
#else
   void mmCpy (void *restrict pDst, void * restrict pSrc, ADT_UInt32 lenI8);

   #define  i8cpy(dest, src, i8cnt)  mmCpy (dest, src, i8cnt);
   #define i16cpy(dest, src, i16cnt) mmCpy (dest, src, i16cnt*2);
   #define  wdcpy(dest, src, wdcnt)  mmCpy (dest, src, wdcnt*4);
#if defined(_TMS320C6600)
   #define CLEAR_INST_CACHE(buff, i8) if (1 < DSPTotalCores) BCACHE_inv   (buff, i8, TRUE);
   #define FLUSH_INST_CACHE(buff, i8) if (1 < DSPTotalCores) BCACHE_wbInv (buff, i8, FALSE);
   #define FLUSH_wait() if (1 < DSPTotalCores) BCACHE_wait() ;
   #define CACHE_INV(addr, sizeI8)      BCACHE_inv ((Ptr)addr, (size_t)sizeI8, (Bool)TRUE)
   #define CACHE_WB(addr, sizeI8)       BCACHE_wb ((Ptr)addr, (size_t)sizeI8, (Bool)TRUE)
   #define CACHE_WBINV(addr, sizeI8)    BCACHE_wbInv ((Ptr)addr, (size_t)sizeI8, (Bool)TRUE)
#elif defined(_TMS320C6400_PLUS) || defined(_TMS320C6700_PLUS)
   #define CLEAR_INST_CACHE(buff, i8) if (1 < DSPTotalCores) BCACHE_inv   (buff, i8, TRUE);
   #define FLUSH_INST_CACHE(buff, i8) if (1 < DSPTotalCores) BCACHE_wbInv (buff, i8, FALSE);
   #define FLUSH_wait() if (1 < DSPTotalCores) BCACHE_wait() ;
   #define CACHE_INV(addr, sizeI8)      BCACHE_inv ((Ptr)addr, (size_t)sizeI8, (Bool)TRUE)
   #define CACHE_WB(addr, sizeI8)       BCACHE_wb ((Ptr)addr, (size_t)sizeI8, (Bool)TRUE)
   #define CACHE_WBINV(addr, sizeI8)    BCACHE_wbInv ((Ptr)addr, (size_t)sizeI8, (Bool)TRUE)
#else
   #define CLEAR_INST_CACHE(buff, i8) if (1 < DSPTotalCores) CACHE_invL1d   (buff, i8, TRUE);
   #define FLUSH_INST_CACHE(buff, i8) if (1 < DSPTotalCores) CACHE_wbInvL1d (buff, i8, FALSE);
   #define FLUSH_wait() if (1 < DSPTotalCores) CACHE_wait() ;
   #define CACHE_INV(addr, sizeI8)      CACHE_invL1d ((Ptr)addr, (size_t)sizeI8, (Bool)TRUE)
   #define CACHE_WB(addr, sizeI8)       CACHE_wbL2 ((Ptr)addr, (size_t)sizeI8, (Bool)TRUE)
   #define CACHE_WBINV(addr, sizeI8)    CACHE_wbInvL1d ((Ptr)addr, (size_t)sizeI8, (Bool)TRUE)
#endif
   #define cacheWbInvBuf(addr, len) if (sysConfig.enab.Bits.apiCacheEnable) CACHE_WBINV(addr, len);
#endif

#define PackBytes(hi,low)  ((hi << 8) | (low & 0xFF))
#define Byte0(w)           ( w       & 0xFF)
#define Byte1(w)           ((w >> 8) & 0xFF)
#define ntohs(a) ((a >> 8) & 0x00FF) | ((a << 8) & 0xFF00)
//}

void g168Capture     (ADT_UInt16 index, ADT_PCM16 *nearIn,  ADT_PCM16 *farIn, int frameSize);
void g168PostCapture (ADT_UInt16 index, ADT_PCM16 *nearOut, ADT_PCM16 *farOut, int frameSize);
void startEcCapture     (ADT_UInt16 chID, ADT_UInt16 index);
void startEcCircCapture (ADT_UInt16 chID, ADT_UInt16 index);
void triggerEcCircCaptureStop (ADT_UInt16 index);
void initGpakCapture();
void GpakDbg();

extern void ADT_GainBlockWrapper(
	ADT_Int16 GainValue,	// gain value (units of .1 dBm)
	ADT_Int16 *pInput,		// pointer to Input buffer
	ADT_Int16 *pOutput,		// pointer to Output buffer
	ADT_Int16 Length		// Length of Array to be processed
	); 
//  Samples / milliseconds conversions
//

#ifdef _DEBUG
   void SystemError (char * msg);
   void AppError (char *File, int Line, char *Msg, int Error);
   void searchDataBuffer ();

   #define AppErr(msg, cond)  AppError (__FILE__, __LINE__, msg, cond)

#else
   #define SystemError(msg)
   #define AppErr(msg, cond)
   #define searchDataBuffer()
#endif

// Definition of system test variables.
extern GpakTestMode CurrentTestMode;    // current Test Mode
extern ADT_UInt16   CurrentTestParm;    // current Test Parameter
extern int recordDTMF;                  // DTMF record channel

extern void teardownLog(ADT_UInt32 chanId, ADT_UInt16 code);

#endif  /* prevent multiple inclusion */
