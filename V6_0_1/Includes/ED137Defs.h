/*
 * Copyright (c) 20019, Adaptive Digital Technologies, Inc.
 *
 * File Name: GpakApi.h
 *
 * Description:
 *   This file contains the function prototypes and data types for the user
 *   API functions that communicate with DSPs executing G.PAK software. The
 *   file is used by application software in the host processor connected to
 *   G.PAK DSPs via a Host Port Interface.
 *
 * Version: 2.0
 *
 * Revision History:
 *   10/17/01 - Initial release.
 *   07/03/02 - Conferencing and test mode updates.
 *   06/15/04 - Tone type updates.
 *   1/2007   - Combined C54 and C64
 */

#ifndef _GPAKED137BAPI_H  /* prevent multiple inclusion */
#define _GPAKED137BAPI_H

#include "adt_typedef_user.h"
#include "GpakEnum.h"
#include "GpakErrs.h"

#ifdef __cplusplus
extern "C" {
#endif

#define VCS 1
#define GRS 2
/* Voice Activity Detection (VAD) */
#define EventVadStateVoice    8             // Vad state Voice event
#define EventVadStateSilence  9             // Vad state Silence event

	/* ED137B Events */
#define customED137B_PTT_Event				   202			/* Set when a change in PTT Value is detected				*/
#define customED137B_PersistentFeatureEvent    203			/* Set when a change in Peristent Feature Value is detected	*/
#define customED137B_NONPersistentFeatureEvent 204			/* Set when a Non-Peristent Feature Value is received		*/
#define customED137B_Timeout_Event			   205			/* Set when a change in Peristent Feature Value is detected	*/

#define ED137_EVENT_OK_CODE                    0
#define ED137_EVENT_MESSAGE_AVAILABLE_CODE     1
	/* Need to update ServiceGpakInterface in GpakMsg.c and add defines to GpaHpi.h */
	
#define MAX_NONPF 2															 /* Max Number NON Persistent Features in one packet */

#define ExtVer1 0x01
#define ExtED137 0x67
#define ExtED137_EXTENSION_ENABLED_X        0x01
#define ExtED137_DEFAULT_LENGTH             0x08
#define ExtED137_KEEP_ALIVE_PAYLOAD_TYPE    123
#define KEEP_ALIVE_RECEIVED_RESET           0

     /*************************************************************************************************************
     *
     *      ED-137B RTP Extension
     *
     *     0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31
     *
     *   |  Version = 0x01           |     Type = 0x67    |  |                Length                      |
     *
     *
     *      RTPTx
     *
     *     0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31
     *
     *   |  PTT Type  |S|     PTT ID    |PM| P|   rsvd |X | Extension for additional features (X=1)        |
     *                 Q                     T
     *                 U                     T
     *                                       S
     *
     *      RTPRx
     *
     *     0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31
     *
     *   |  PTT Type  |S|     PTT ID    |PM| P|S |rsvd |X | Extension for additional features (X=1)        |
     *                 Q                     T C
     *                 U                     T T
     *                                       S
     *
     ***************************************************************************************************************/
#define PTT_TYPE_SHIFT      0x05
#define SQU_SHIFT           0x04
#define PTT_TYPE_MASK       0x1F
#define SQU_MASK            0x01
#define SQU_SHIFT           0x04
#define PTT_ID_SHIFT_H      0x02
#define PTT_ID_SHIFT_L      0x06
#define PTT_ID_MASK_H       0x3C
#define PTT_ID_MASK_L       0x03
#define PM_SHIFT            0x05
#define PM_MASK             0x01
#define PTTS_SHIFT          0x04
#define PTTS_MASK           0x01
#define SCT_SHIFT           0x04
#define SCT_MASK            0x01
#define X_MASK              0x01
#define PF_TYPE_SHIFT       0x04
#define PF_TYPE_MASK        0x0F
#define PF_LENGTH_SHIFT     0x00
#define PF_LENGTH_MASK      0x0F
#define PF_VALUE_SHIFT      0x00
#define PF_VALUE_MASK       0xFF

#define MIN_ED137_EXT_LENGTH 0x01
#define ED137_EXT_HDR_LENGTH 0x01
#define ED137_PF_LENGTH      0x01
#define VALID_ED137_PF_TYPE  0x0B

struct scustomED137B_PTT_Event {									/*  For PPT Event detection										*/
            ADT_UInt16 PTTx;										/*  RTP extension												*/
} scustomED137B_PTT_Event;

struct scustomED137B_PersistentFeatureEvent {						/*  For Persistent Event detection								*/
            ADT_UInt8 PF;											/*  Persistent Feature Value									*/
} scustomED137B_PersistentFeatureEvent;

struct sED137B_NONPersistentFeature {								/* For Non-Persistent Feature									*/
			ADT_UInt16 NONPF_Lentgh;								/*  Length of NON-Persistent Features reported					*/
            ADT_UInt8  NONPersistentFeature_Value[16];				/*  Non-Persistent Feature										*/
} sED137B_NONPersistentFeature;

struct scustomED137B_NONPersistentFeatureEvent {					/* For Non-Persistent Event detection							*/
			ADT_UInt16 NUM_NONPF;									/*  Number of NON-Persistent Features reported in this event	*/
            struct sED137B_NONPersistentFeature  NONPersistentFeature[MAX_NONPF];	/*  Non-Persistent Feature								*/
} scustomED137B_NONPersistentFeatureEvent;

struct scustomED137B_Timeout_Event {								/*  For Stay-Alive Error Reporting								*/
            ADT_UInt16 StayAliveTO;									/*  Time Out value to be assigned								*/
} scustomED137B_Timeout_Event;


typedef struct ED137Ext {
    ADT_UInt8   VER;
    ADT_UInt8   TYPE;
    ADT_UInt8   LENGTH;
} ED137Ext_t;

typedef struct RTPRxExt {
    ADT_UInt8   PTT_TYPE;
    ADT_Bool    SQU;
    ADT_UInt8   PTT_ID;
    ADT_Bool    PM;
    ADT_Bool    PTTS;
    ADT_Bool    SCT;
    ADT_Bool    X;
} RTPRxExt_t;

typedef struct RTP_PersistentFeatures {
    ADT_UInt8   PF_TYPE;
    ADT_UInt8   PF_LENGTH;
    ADT_UInt8   PF_VALUE;
} RTP_PersistentFeatures_t;

typedef struct ED137Pkt {
    ADT_UInt8   EXT_VER;
    ADT_UInt8   EXT_TYPE;
    ADT_UInt8   RSVD1;
    ADT_UInt8   EXT_LENGTH;
    ADT_UInt8   PTT_VALUE_H;
    ADT_UInt8   PTT_VALUE_L;
    ADT_UInt8   PF_VALUE_H;
    ADT_UInt8   PF_VALUE_L;
    ADT_UInt8   NPF_VALUE[256];
} ED137Pkt_t;

typedef struct ED137RxPkt {
    ADT_UInt8   EXT_VER;
    ADT_UInt8   EXT_TYPE;
    ADT_UInt8   RSVD1;
    ADT_UInt8   EXT_LENGTH;
    ADT_UInt8   PTT_VALUE_H;
    ADT_UInt8   PTT_VALUE_L;
    ADT_UInt8   AF_VALUE[256];
} ED137RxPkt_t;
// gpakED137B Configuration Parameters reply status codes.
typedef enum GPAK_ED137BcfgStat_t {
    ED137Bcfg_Success = 0,						// ED137B Configuration Parameters written successfully

    /* ED137B Configuration parameters errors. */
    ED137Bcfg_BadParam       
} GPAK_ED137BcfgStat_t;

// gpakED137B PTT Update Parameters reply status codes.
typedef enum GPAK_ED137B_PTTupdateStat_t {
    ED137B_PTTupdate_Success = 0,				// PTT Update Parameters written successfully

    /* PTT Update parameters errors. */
    ED137B_PTTupdate_BadParam       
} GPAK_ED137B_PTTupdateStat_t;

// gpakED137B Configure Persistent Feature Parameters reply status codes.
typedef enum GPAK_ED137B_PersistentFeatureStat_t {
    ED137B_PersistentFeature_Success = 0,		// Persistent Feature Parameters written successfully

    /* Persitent Feature parameters errors. */
    ED137B_PersistentFeature_BadParam       
} GPAK_ED137B_PersistentFeatureStat_t;

// gpakED137B Update Persistent Feature Parameters reply status codes.
typedef enum GPAK_ED137B_PersistentFeatureUpdateStat_t {
    ED137B_PersistentFeatureUpdate_Success = 0,	// Persistent Feature Parameters updated successfully

    /* Persitent Feature Update parameters errors. */
    ED137B_PersistentFeatureUpdate_BadParam       
} GPAK_ED137B_PersistentFeatureUpdateStat_t;

// gpakED137B Configure NON Persistent Feature Parameters reply status codes.
typedef enum GPAK_ED137B_NONPersistentFeatureStat_t {
    ED137B_NONPersistentFeature_Success = 0,		// NON Persistent Feature Parameters written successfully

    /* NON Persitent Feature parameters errors. */
    ED137B_NONPersistentFeature_BadParam       
} GPAK_ED137B_NONPersistentFeatureStat_t;

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakED137B - ED137B Configuration settings
*/

typedef enum ED137RxState{
    RX_IDLE,
    RX_ACTIVE,
    RX_KEEP_ALIVE,
}RxState;

typedef enum ExtRXStatus{
    EXT_RX_NO_DATA,
    EXT_RX_XFER_DATA
}ExtRXStatus_t;

typedef enum ED137TxState{
    TX_IDLE          = 0x0000,
    TX_ACTIVE        = 0x0001,
    TX_PTT_OFF       = 0x0002,
    TX_KEEP_ALIVE    = 0x0004,
    TX_UPDATE_PTT    = 0x0008,
    TX_UPDATE_PF_MSG = 0x0010,
    TX_NON_PF_MSG    = 0x0020,
}TxState;

typedef enum ED137PTT_Type{
    PTT_OFF,
    NORMAL_PTT_ON,
    COUPLING_PTT_ON,
    PRIORITY_PTT_ON,
    EMERGENCY_PTT_ON,
    INVALID_PTT,
}ED137PTT_Type;

typedef struct GpakRTPRxState {
    ADT_UInt16  LAST_PTT_VALUE;
    ADT_UInt8   LAST_PF_VALUE;
    ADT_UInt16  KeepAliveCount;
    ADT_UInt16  KeepAliveErrorCount;
    RxState RxState;
} GpakRTPRxState_t;

typedef struct GpakRTPTxState {
    ADT_UInt16  TxState;
    ADT_UInt16  PTT_OffCount;
    ADT_UInt16  KeepAliveCount;
    ADT_UInt16  KeepAliveErrorCount;
    ADT_UInt16  TX_KEEP_ALIVE_CTL_INFO_MSG_CNT;
    ADT_UInt16  TxVAD_DelayCount;
} GpakRTPTxState_t;


typedef struct GpakED137BcfgParms {
    ADT_UInt16 rtp_ext_en;				/* Specifies normal RTP packet or a packet with an ED-137B header extension.	*/
										/* 0=disable, 1=enable															*/
    ADT_UInt16 device;					/* Device Type																	*/
										/* Specifies a VCS or GRS device. This dictates the direction of the parameters */
										/* and the R2S keep-alive implementation.										*/
										/* VCS=1, GRS=2																	*/
	ADT_UInt16 txrxmode;				/* Connection Type																*/
										/* This dictates if the GRS is a transceiver or separate transmitter/receiver.	*/
	ADT_UInt16 follow_squ_with_vad;     /* Set the Squelch bit on RTP/Voice packets being generated by the DSP,         */
	                                    /* clear the Squelch bit on silence packets.                                    */
	ADT_UInt16 ptt_rep;					/* PTT Repetition																*/
										/* Number of audio packets sent with RTPTx HE indicating that PTT is OFF or		*/
										/* RTPRx HE indicating that SQUELCH is OFF (value 0->only 1 PTT OFF or only 1	*/
										/* SQUELCH OFF message). See ED-137B section 3.6.1.8.							*/
										/* 0-3																			*/
	ADT_UInt16 R2SKeepAlivePeriod;		/* R2S Period																	*/
										/* Maximum time between each R2S-Keep Alive packet. integer, in milliseconds.	*/
										/* See ED-137B section 3.6.1.9.													*/
										/* 20-1000																		*/
	ADT_UInt16 R2SKeepAliveMultiplier;	/* R2S Multiplier																*/
										/* Number of Keep Alive packets in error or not received by endpoint before Time*/
										/* Out of the session. See ED-137B section 3.6.1.10.							*/
										/* 2-50																			*/
	ADT_UInt16 Ptt_val;					/* PTT Value																	*/
										/* Initial 16-bit entry for RTP extension										*/
										/* 0000-FFFF																	*/
	ADT_UInt16 mask;					/* PTT Rx Mask																	*/
										/* Mask to indicate which change of events to report							*/
										/* 0000-FFFF																	*/														
	ADT_UInt16 Initial_PF_val;          /* Initial PF Value                                                             */
	                                    /* Initial 16-bit entry for Persistent Feature                                  */
	                                    /* 0000-FFFF                                                                    */
	ADT_UInt16 Initial_PF_Mask;         /* Used to signal which bit changes reflect event generation                    */
	                                    /* 0x00-0xFF                                                                    */
} GpakED137BcfgParms;


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakED137B - ED137B Update PTT Configuration settings
*/

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakED137B - ED137B Update PTT Configuration settings
*/

typedef struct GpakED137B_PTTupdateParms {  																	
	ADT_UInt16 PTT_Type;				/* 0x00 PTT OFF																	*/
										/* 0x01 Normal PTT ON															*/
										/* 0x02 Coupling PTT ON															*/
										/* 0x03 Priority PTT ON															*/
										/* 0x04 Emergency PTT ON														*/
										/* 0x05 Reserved																*/
										/* 0x06 Reserved																*/
										/* 0x07 Reserved																*/
	ADT_UInt16 SQU;                     /* Squelch                                                                      */
	                                    /* 0-1                                                                          */
	ADT_UInt16 PTT_ID;                  /* Set by the GRS Receiver in SDP transaction.  Ranges from 1 to the maximum    */
	                                    /* number of allowed SIP sessions. CTI will extract the SDP value and set this  */
	                                    /* field.                                                                       */
	ADT_UInt16 PM;						/* Used for signaling PTT_ON to non-transmitting (not selected) transmitters in */
										/* a coverage group.															*/
										/* 0-1																			*/
	ADT_UInt16 PTTS; 					/* PTT Summation																*/
										/* 0-1																			*/

} GpakED137B_PTTupdateParms;

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakED137B - ED137B Configure Persistent Features settings
*/

typedef struct GpakED137B_PersistentFeatureParms {
   	ADT_UInt16 PersistentFeature_Type;  /* Additional Features Type definition                      					*/
										/* 0x0B-0xFF																	*/
	ADT_UInt16	PersistentFeature_Mask; /* Used to signal which bit changes reflect event generation                    */
										/* 0x00-0xFF																	*/
	ADT_UInt16 PersistentFeature_Value; /* The initial value used for persistent additional feature						*/								
										/* 0x00-0xFF																	*/

} GpakED137B_PersistentFeatureParms;

typedef struct GpakED137B_GateAudioFeatureParms {
    ADT_UInt16 Enable_Mask;            /* Gate Audio Feature Enabled                                                      */
}GpakED137B_GateAudioFeatureParms;
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakED137B - ED137B Update Persistent Features settings
*/

typedef struct GpakED137B_PersistentFeatureUpdateParms {
   	ADT_UInt16 PersistentFeature_Type;  /* Additional Features Type definition                                          */
                                        /* 0x0B-0xFF                                                                    */
   	ADT_UInt16 PersistentFeature_Value; /* The initial value used for persistent additional feature						*/
										/* 0x00-0xFF																	*/
} GpakED137B_PersistentFeatureUpdateParms;

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakED137B - ED137B Configure NON Persistent Features settings
*/


typedef struct GpakED137B_NONPersistentFeatureParms {
    ADT_UInt16 NONPersistentFeature_Type;			/* Used to signal which bit changes reflect event generation		*/
													/* 0x00-0xFF														*/
	ADT_UInt16 NONPersistentFeature_Length;			/* Length = Number of octets										*/
													/* Persistence = 1, 1, Persistence = 0, 1-15						*/
	ADT_UInt16 NONPersistentFeature_Persistence; 	/* Persistence														*/
													/* Only sent once = 0												*/															
	ADT_UInt8  NONPersistentFeature_Value[16];		/* The value used for NoN-Persistent additional feature Header		*/								
										
} GpakED137B_NONPersistentFeatureParms;

typedef struct GpakED137B_NPF {
    ADT_UInt16 Num_NPF;
    GpakED137B_NONPersistentFeatureParms ED137B_NONPersistentFeatureParms[16];
}GpakED137B_NPF;
#endif  /* prevent multiple inclusion */

