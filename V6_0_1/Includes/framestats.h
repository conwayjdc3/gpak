#ifndef _FRAME_STATS_H
#define _FRAME_STATS_H

#define STATS(stmt)  { stmt }

typedef struct FrameStats_t { 
  unsigned int StatID;
  unsigned int Frames,       MaxMIPS;
  unsigned int LateErrs,     tonesToHost;
  unsigned int pktInTone,    pktOutTone;
  unsigned int duplicateEndTone;

  unsigned int toneRelayCnt, toneRelayGenCnt;
  unsigned int pktInCng,     pktOutCng;
  unsigned int pktInT38,     pktOutT38;
  unsigned int pktInVoice,   pktOutVoice;

  unsigned int toneGenCnt;
  unsigned int None,         Invalid;
} FrameStats_t;

extern FrameStats_t FrameStats;


#endif
