/*
 * Copyright (c) 2001-2007, Adaptive Digital Technologies, Inc.
 *
 * File Name: GpakDefs.h
 *
 * Description:
 *   This file contains common definitions related to G.PAK application
 *   software.
 *
 * Version: 1.0
 *
 * Revision History:
 *   11/07/01 - Initial release.
 *   05/2005  - Added GSM_EFR
 *    2/2007  - Converged C54 and C64
 *
 */
