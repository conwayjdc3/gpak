/*
 * Copyright (c) 2004-2007, Adaptive Digital Technologies, Inc.
 *
 * File Name: Gpakprarma.h
 *
 * Description:
 *   This file contains G.PAK pragmas identifying the code and data sections
 *
 * Version: 1.0
 *
 * Revision History:
 *   9/07 - Initial release.
 *
 *  Sections identified:
 *
 *   FAST_PROG_SECT   - Must be placed in fast access memory
 *   MEDIUM_PROG_SECT - Place in fast if it fits
 *   SLOW_PROG_SECT   - Place in slow access memory
 *   PKT_PROG_SECT    - Packet processing code
 *   TONE_PROG_SECT   - Tone processing code
 */

#ifndef GPAKPRAGMA_H
#define GPAKPRAGMA_H
//====================================================
//  Main functions
//
#pragma CODE_SECTION (GpakSchedulingTimer,  "FAST_PROG_SECT")
#pragma CODE_SECTION (main,                 "SLOW_PROG_SECT")
#pragma CODE_SECTION (LogTransfer,          "SLOW_PROG_SECT")
#pragma CODE_SECTION (logTime,              "SLOW_PROG_SECT")
#pragma CODE_SECTION (SystemError,          "SLOW_PROG_SECT")
#pragma CODE_SECTION (ChangePLL1Mult,       "FAST_PROG_SECT")



//====================================================
//  Channel setup functions
//
#pragma CODE_SECTION (CircBuffAllocate,            "SLOW_PROG_SECT")
#pragma CODE_SECTION (AllocateInstanceStructures,  "SLOW_PROG_SECT")
#pragma CODE_SECTION (channelInstanceInit,         "SLOW_PROG_SECT")
#pragma CODE_SECTION (setupChannel,                "SLOW_PROG_SECT")
#pragma CODE_SECTION (tearDownChannel,             "SLOW_PROG_SECT")
#pragma CODE_SECTION (AllocEchoCanceller,          "SLOW_PROG_SECT")
#pragma CODE_SECTION (DeallocateEchoCanceller,     "SLOW_PROG_SECT")
#pragma CODE_SECTION (DeallocEcans,                "SLOW_PROG_SECT")
#pragma CODE_SECTION (AllocAGC,                    "SLOW_PROG_SECT")
#pragma CODE_SECTION (DeallocAGC,                  "SLOW_PROG_SECT")
#pragma CODE_SECTION (AllocToneGen,                "SLOW_PROG_SECT")
#pragma CODE_SECTION (DeallocToneGen,              "SLOW_PROG_SECT")
#pragma CODE_SECTION (InitToneDetection,           "SLOW_PROG_SECT")
#pragma CODE_SECTION (AllocToneDetectors,          "SLOW_PROG_SECT")
#pragma CODE_SECTION (DeallocToneDetectors,        "SLOW_PROG_SECT")
#pragma CODE_SECTION (DeallocToneDetectInstance,   "SLOW_PROG_SECT")
#pragma CODE_SECTION (DeallocToneDetectors,        "SLOW_PROG_SECT")
#pragma CODE_SECTION (InitToneRelayGen,            "SLOW_PROG_SECT")
#pragma CODE_SECTION (InitCedCngToneDetection,     "SLOW_PROG_SECT")
#pragma CODE_SECTION (pcmPktLoopBack,              "SLOW_PROG_SECT")
#pragma CODE_SECTION (pktPktLoopBack,              "SLOW_PROG_SECT")
#pragma CODE_SECTION (disableLoopBack,             "SLOW_PROG_SECT")
#pragma CODE_SECTION (setDevicePointers,           "SLOW_PROG_SECT")
#pragma CODE_SECTION (setCircPointers,             "SLOW_PROG_SECT")
#pragma CODE_SECTION (ValidFrameSize,              "SLOW_PROG_SECT")
#pragma CODE_SECTION (ValidCoding,                 "SLOW_PROG_SECT")
#pragma CODE_SECTION (ValidToneTypes,              "SLOW_PROG_SECT")
#pragma CODE_SECTION (SetVADMode,                  "SLOW_PROG_SECT")
#pragma CODE_SECTION (AllocEncoderSampleRateConv,                    "SLOW_PROG_SECT")
#pragma CODE_SECTION (AllocDecoderSampleRateConv,                    "SLOW_PROG_SECT")
#pragma CODE_SECTION (DeallocEncoderSampleRateConv,                  "SLOW_PROG_SECT")
#pragma CODE_SECTION (DeallocDecoderSampleRateConv,                  "SLOW_PROG_SECT")


//====================================================
//  Messaging task functions
//
#pragma CODE_SECTION (InitGpakInterface,           "SLOW_PROG_SECT")
#pragma CODE_SECTION (ServiceGpakInterface,        "SLOW_PROG_SECT")
#pragma CODE_SECTION (ProcTestModeMsg,             "SLOW_PROG_SECT")
#pragma CODE_SECTION (ProcSystemConfigMsg,         "SLOW_PROG_SECT")
#pragma CODE_SECTION (ProcReadSystemParmsMsg,      "SLOW_PROG_SECT")
#pragma CODE_SECTION (ProcWriteSystemParmsMsg,     "SLOW_PROG_SECT")
#pragma CODE_SECTION (ProcConfigureChannelMsg,     "SLOW_PROG_SECT")
#pragma CODE_SECTION (ProcTearDownChannelMsg,      "SLOW_PROG_SECT")
#pragma CODE_SECTION (ProcChannelStatusMsg,        "SLOW_PROG_SECT")
#pragma CODE_SECTION (ProcToneGenMsg,              "SLOW_PROG_SECT")
#pragma CODE_SECTION (ProcReadAECParmsMsg,         "SLOW_PROG_SECT")
#pragma CODE_SECTION (ProcWriteAECParmsMsg,        "SLOW_PROG_SECT")
#pragma CODE_SECTION (ProcPlayRecMsg,              "SLOW_PROG_SECT")


//====================================================
//  Framing task functions
//
#pragma CODE_SECTION (FrameRatePhaseCount,         "FAST_PROG_SECT")
#pragma CODE_SECTION (ScheduleFramingTasks,        "FAST_PROG_SECT")
#pragma CODE_SECTION (GpakAdjustChannelPointers,   "FAST_PROG_SECT")
#pragma CODE_SECTION (MovePPendToPendingQueue,     "FAST_PROG_SECT")
#pragma CODE_SECTION (GetPPendingEncodeQueue,      "FAST_PROG_SECT")
#pragma CODE_SECTION (GetPPendingDecodeQueue,      "FAST_PROG_SECT")
#pragma CODE_SECTION (GetEncodeQueue,              "FAST_PROG_SECT")
#pragma CODE_SECTION (GetDecodeQueue,              "FAST_PROG_SECT")


#pragma CODE_SECTION (InitFrameTasks,              "SLOW_PROG_SECT")
#pragma CODE_SECTION (initEncoder,                 "SLOW_PROG_SECT")
#pragma CODE_SECTION (initDecoder,                 "SLOW_PROG_SECT")
#pragma CODE_SECTION (initDMAPhaseCounters,        "SLOW_PROG_SECT")
#pragma CODE_SECTION (GetPcmToPktQueueHead,        "SLOW_PROG_SECT")
#pragma CODE_SECTION (GetPktToPcmQueueHead,        "SLOW_PROG_SECT")
#pragma CODE_SECTION (AddtoPrePendQueue,           "SLOW_PROG_SECT")
#pragma CODE_SECTION (DequeueChannel,              "SLOW_PROG_SECT")
#pragma CODE_SECTION (G729_Encoder_Init,           "SLOW_PROG_SECT")
#pragma CODE_SECTION (G729_Decoder_Init,           "SLOW_PROG_SECT")
#pragma CODE_SECTION (SendWarningEvent,            "SLOW_PROG_SECT")

#pragma CODE_SECTION (GpakFrameTask,               "MEDIUM_PROG_SECT")
#pragma CODE_SECTION (G723_ADT_getSize4Decoder,    "MEDIUM_PROG_SECT")

#pragma CODE_SECTION (PayloadToCirc,               "PKT_PROG_SECT")
#pragma CODE_SECTION (PayloadToCircCache,           "PKT_PROG_SECT")
#pragma CODE_SECTION (SendPktToHost,               "PKT_PROG_SECT")
#pragma CODE_SECTION (buildAudioPacket,            "PKT_PROG_SECT")
#pragma CODE_SECTION (buildAAL2TonePacket,         "PKT_PROG_SECT")
#pragma CODE_SECTION (buildRTPL16Packet,           "PKT_PROG_SECT")
#pragma CODE_SECTION (buildCngPacket,              "PKT_PROG_SECT")
#pragma CODE_SECTION (buildRTPEventPacket,         "PKT_PROG_SECT")
#pragma CODE_SECTION (buildRTPTonePacket,          "PKT_PROG_SECT")
#pragma CODE_SECTION (writeEventIntoFifo,          "PKT_PROG_SECT")

#pragma CODE_SECTION (parse2833Tone,               "PKT_PROG_SECT")
#pragma CODE_SECTION (parse2833Event,              "PKT_PROG_SECT")
#pragma CODE_SECTION (parseAAL2Tone,               "PKT_PROG_SECT")

#pragma CODE_SECTION (GetPktFromCircBuffer,        "PKT_PROG_SECT")
#pragma CODE_SECTION (RemovePktFromCircBuffer,     "PKT_PROG_SECT")
#pragma CODE_SECTION (verifyPacketHeader,          "PKT_PROG_SECT")
#pragma CODE_SECTION (getPacketBuffer,             "PKT_PROG_SECT")
#pragma CODE_SECTION (parsePacket,                 "PKT_PROG_SECT")
#pragma CODE_SECTION (packG728,                    "PKT_PROG_SECT")
#pragma CODE_SECTION (unpackG728,                  "PKT_PROG_SECT")

#pragma CODE_SECTION (voiceEncode,                 "MEDIUM_PROG_SECT")
#pragma CODE_SECTION (ProcessVadToneEncode,        "MEDIUM_PROG_SECT")

#pragma CODE_SECTION (DecodePayload,               "MEDIUM_PROG_SECT")
#pragma CODE_SECTION (voiceDecode,                 "MEDIUM_PROG_SECT")

#pragma CODE_SECTION (sequenceStampOutput,         "MEDIUM_PROG_SECT")
#pragma CODE_SECTION (swCompress,                  "MEDIUM_PROG_SECT")
#pragma CODE_SECTION (swExpand,                    "MEDIUM_PROG_SECT")
#pragma CODE_SECTION (sendRecPlaybackEvent,        "MEDIUM_PROG_SECT")
#pragma CODE_SECTION (voicePlayback,               "MEDIUM_PROG_SECT")
#pragma CODE_SECTION (voiceRecord,                 "MEDIUM_PROG_SECT")

#pragma CODE_SECTION (getToneDetScratchPtr,        "TONE_PROG_SECT")
#pragma CODE_SECTION (reportToneStatus,            "TONE_PROG_SECT")
#pragma CODE_SECTION (toneDetect,                  "TONE_PROG_SECT")
#pragma CODE_SECTION (CedCngDetect,                "TONE_PROG_SECT")
#pragma CODE_SECTION (ToneRelayGenerate,           "TONE_PROG_SECT")
#pragma CODE_SECTION (ToneGenerate,                "TONE_PROG_SECT")



//====================================================
//  Serial port functions
//
#pragma CODE_SECTION (InitGpakPcm,          "SLOW_PROG_SECT")
#pragma CODE_SECTION (StartSerialPortIo,    "SLOW_PROG_SECT")
#pragma CODE_SECTION (StopSerialPortIo,     "SLOW_PROG_SECT")
#pragma CODE_SECTION (StartGpakPcm,         "SLOW_PROG_SECT")
#pragma CODE_SECTION (ValidateSlots,        "SLOW_PROG_SECT")
#pragma CODE_SECTION (LocateTimeSlot,       "SLOW_PROG_SECT")
#pragma CODE_SECTION (channelSetupComplete, "SLOW_PROG_SECT")
#pragma CODE_SECTION (ActivateChannel,      "SLOW_PROG_SECT")
#pragma CODE_SECTION (DeactivateChannel,    "SLOW_PROG_SECT")
#pragma CODE_SECTION (LogBuff,              "SLOW_PROG_SECT")

#pragma CODE_SECTION (pendingChannelSetup,  "FAST_PROG_SECT")
#pragma CODE_SECTION (switchBuffers,        "FAST_PROG_SECT")
#pragma CODE_SECTION (GpakDmaSwi,           "FAST_PROG_SECT")


//====================================================
//  McBSP functions
//
#pragma CODE_SECTION (validCompanding,          "SLOW_PROG_SECT")
#pragma CODE_SECTION (storeMcBSPConfiguration,  "SLOW_PROG_SECT")
#pragma CODE_SECTION (setupMcBSP,               "SLOW_PROG_SECT")
#pragma CODE_SECTION (startMcBSP,               "SLOW_PROG_SECT")
#pragma CODE_SECTION (stopMcBsp,                "SLOW_PROG_SECT")
#pragma CODE_SECTION (SetTransmitEnables,       "SLOW_PROG_SECT")
#pragma CODE_SECTION (ClearTransmitEnables,     "SLOW_PROG_SECT")
#pragma CODE_SECTION (ProcConfigSerialPortsMsg, "SLOW_PROG_SECT")
#pragma CODE_SECTION (ConfigureMcBSP,           "SLOW_PROG_SECT")
#pragma CODE_SECTION (GetMasks,                 "SLOW_PROG_SECT")
#pragma CODE_SECTION (determineLastSlot,        "SLOW_PROG_SECT")
#pragma CODE_SECTION (McBSPFrameError,          "FAST_PROG_SECT")

//====================================================
//  TDM-DMA functions
//
#pragma CODE_SECTION (DMAInitialize,       "SLOW_PROG_SECT")
#pragma CODE_SECTION (initRxTCCParams,     "SLOW_PROG_SECT")
#pragma CODE_SECTION (initTxTCCParams,     "SLOW_PROG_SECT")
#pragma CODE_SECTION (enableDMA,           "SLOW_PROG_SECT")
#pragma CODE_SECTION (disableDMA,          "SLOW_PROG_SECT")
#pragma CODE_SECTION (zeroTxBuffer,        "SLOW_PROG_SECT")
#pragma CODE_SECTION (sineBuffer,          "SLOW_PROG_SECT")
#pragma CODE_SECTION (TogglePCMInSignal,   "SLOW_PROG_SECT")
#pragma CODE_SECTION (TogglePCMOutSignal,  "SLOW_PROG_SECT")
#pragma CODE_SECTION (setEdmaPriorities,   "SLOW_PROG_SECT")

#pragma CODE_SECTION (signalDmaInt,        "FAST_PROG_SECT")
#pragma CODE_SECTION (DmaLostTxSync,       "FAST_PROG_SECT")
#pragma CODE_SECTION (DmaLostRxSync,       "FAST_PROG_SECT")

#pragma CODE_SECTION (FixedPatternTx,      "FAST_PROG_SECT")
#pragma CODE_SECTION (FixedPatternTx,      "FAST_PROG_SECT")
#pragma CODE_SECTION (CheckTxPointers,     "FAST_PROG_SECT")
#pragma CODE_SECTION (CheckRxPointers,     "FAST_PROG_SECT")


#pragma CODE_SECTION (GpakDmaIsr,          "FAST_PROG_SECT")
#pragma CODE_SECTION (pendingChannelSetup, "FAST_PROG_SECT")

#pragma CODE_SECTION (GpakDma0Isr,         "FAST_PROG_SECT")
#pragma CODE_SECTION (GpakDma1Isr,         "FAST_PROG_SECT")
#pragma CODE_SECTION (GpakDma2Isr,         "FAST_PROG_SECT")
#pragma CODE_SECTION (GpakDma3Isr,         "FAST_PROG_SECT")
#pragma CODE_SECTION (GpakDma4Isr,         "FAST_PROG_SECT")
#pragma CODE_SECTION (GpakDma5Isr,         "FAST_PROG_SECT")

#pragma CODE_SECTION (CopyDmaTxBuffers,    "FAST_PROG_SECT")
#pragma CODE_SECTION (CopyDmaRxBuffers,    "FAST_PROG_SECT")
#pragma CODE_SECTION (AdjustDmaBuffers,    "FAST_PROG_SECT")

#pragma CODE_SECTION (ADT_GainBlock,       "FAST_PROG_SECT")
#pragma CODE_SECTION (ADT_SumLinear,       "FAST_PROG_SECT")

//====================================================
//  Instance-DMA functions
//
#pragma CODE_SECTION (dmaXfer,                "FAST_PROG_SECT")
#pragma CODE_SECTION (dmaWaitforXferComplete, "FAST_PROG_SECT")
#pragma CODE_SECTION (submitDmaRequest,       "FAST_PROG_SECT")
#pragma CODE_SECTION (checkDMAComplete,       "FAST_PROG_SECT")
#pragma CODE_SECTION (dmaSetPtrs,             "FAST_PROG_SECT")
#pragma CODE_SECTION (dmaRestoreEcPtrs,       "FAST_PROG_SECT")

#pragma CODE_SECTION (dmaInitInfoStruct,      "MEDIUM_PROG_SECT")
#pragma CODE_SECTION (dmaInitCtrl,            "MEDIUM_PROG_SECT")

#pragma CODE_SECTION (checkCoderType,         "SLOW_PROG_SECT")
#pragma CODE_SECTION (dmaInvalidateL1Cache,   "SLOW_PROG_SECT")
#pragma CODE_SECTION (getInternalQueues,      "SLOW_PROG_SECT")
#pragma CODE_SECTION (geFramingTCCCodes,      "SLOW_PROG_SECT")
#pragma CODE_SECTION (dmaSetupECData,         "SLOW_PROG_SECT")
#pragma CODE_SECTION (initChainedDMAInstances,"SLOW_PROG_SECT")
#pragma CODE_SECTION (dmaTrace,               "SLOW_PROG_SECT")


//====================================================
//  Circular buffer copy routines
//
#pragma CODE_SECTION (copyCircToLinear, "FAST_PROG_SECT")  
#pragma CODE_SECTION (copyLinearToCirc, "FAST_PROG_SECT")  


//====================================================
//  Packet packing routines
//
#pragma CODE_SECTION (packLSB2,        "PKT_PROG_SECT")
#pragma CODE_SECTION (packLSB3,        "PKT_PROG_SECT")
#pragma CODE_SECTION (packLSB4,        "PKT_PROG_SECT")
#pragma CODE_SECTION (packLSB5,        "PKT_PROG_SECT")
#pragma CODE_SECTION (packLSB6,        "PKT_PROG_SECT")
#pragma CODE_SECTION (packLSB7,        "PKT_PROG_SECT")
#pragma CODE_SECTION (packLSB,         "PKT_PROG_SECT")

#pragma CODE_SECTION (unpackLSB2,      "PKT_PROG_SECT")
#pragma CODE_SECTION (unpackLSB3,      "PKT_PROG_SECT")
#pragma CODE_SECTION (unpackLSB4,      "PKT_PROG_SECT")
#pragma CODE_SECTION (unpackLSB5,      "PKT_PROG_SECT")
#pragma CODE_SECTION (unpackLSB6,      "PKT_PROG_SECT")
#pragma CODE_SECTION (unpackLSB7,      "PKT_PROG_SECT")
#pragma CODE_SECTION (unpackLSB,       "PKT_PROG_SECT")

#pragma CODE_SECTION (packMSB2,        "PKT_PROG_SECT")
#pragma CODE_SECTION (packMSB3,        "PKT_PROG_SECT")
#pragma CODE_SECTION (packMSB4,        "PKT_PROG_SECT")
#pragma CODE_SECTION (packMSB5,        "PKT_PROG_SECT")
#pragma CODE_SECTION (packMSB6,        "PKT_PROG_SECT")
#pragma CODE_SECTION (packMSB7,        "PKT_PROG_SECT")
#pragma CODE_SECTION (packMSB,         "PKT_PROG_SECT")

#pragma CODE_SECTION (unpackMSB2,      "PKT_PROG_SECT")
#pragma CODE_SECTION (unpackMSB3,      "PKT_PROG_SECT")
#pragma CODE_SECTION (unpackMSB4,      "PKT_PROG_SECT")
#pragma CODE_SECTION (unpackMSB5,      "PKT_PROG_SECT")
#pragma CODE_SECTION (unpackMSB6,      "PKT_PROG_SECT")
#pragma CODE_SECTION (unpackMSB7,      "PKT_PROG_SECT")
#pragma CODE_SECTION (unpackMSB,       "PKT_PROG_SECT")

//====================================================
//  FAX relay routines
//
#pragma CODE_SECTION (AllocFaxRelay,   "SLOW_PROG_SECT")
#pragma CODE_SECTION (DeallocFaxRelay, "SLOW_PROG_SECT")
#pragma CODE_SECTION (processFaxRelay, "FAX_PROG_SECT")
#pragma CODE_SECTION (T38_Add_Packet,  "FAX_PROG_SECT")

#pragma CODE_SECTION (T38_Check_Timeout, "FAX_PROG_SECT")
#pragma CODE_SECTION (T38_Restart_Voice, "FAX_PROG_SECT")
#pragma CODE_SECTION (T38_Encode_Start,  "FAX_PROG_SECT")

//====================================================
//  Channel type routines
//
#pragma CODE_SECTION (ProcCfgPcm2PcmMsg,  "SLOW_PROG_SECT")
#pragma CODE_SECTION (ProcPcm2PcmStatMsg, "SLOW_PROG_SECT")
#pragma CODE_SECTION (SetupPcm2PcmChan,   "SLOW_PROG_SECT")
#pragma CODE_SECTION (FramePcmA2PcmB,     "MEDIUM_PROG_SECT")
#pragma CODE_SECTION (FramePcmB2PcmA,     "MEDIUM_PROG_SECT")

#pragma CODE_SECTION (ProcCfgPcm2PktMsg,   "SLOW_PROG_SECT")
#pragma CODE_SECTION (ProcPcm2PktStatMsg,  "SLOW_PROG_SECT")
#pragma CODE_SECTION (SetupPcm2PktChan,    "SLOW_PROG_SECT")
#pragma CODE_SECTION (findTestVector,      "MEDIUM_PROG_SECT")
#pragma CODE_SECTION (FramePcmA2PktB,      "MEDIUM_PROG_SECT")
#pragma CODE_SECTION (FramePktB2PcmA,      "MEDIUM_PROG_SECT")



//====================================================
//  RTDX 
//
#pragma CODE_SECTION (PostRTDX,              "FAST_PROG_SECT")
#pragma CODE_SECTION (circGetFreeSpace,      "SLOW_PROG_SECT")
#pragma CODE_SECTION (GpakRtdxSwi,           "SLOW_PROG_SECT")

#pragma CODE_SECTION (GpakRtdxSetup,         "SLOW_PROG_SECT")
#pragma CODE_SECTION (GpakRtdxWriteEvts,     "SLOW_PROG_SECT")
#pragma CODE_SECTION (GpakRtdxReadCmd,       "SLOW_PROG_SECT")
#pragma CODE_SECTION (GpakRtdxWritePkts,     "SLOW_PROG_SECT")
#pragma CODE_SECTION (GpakRtdxReadPkts,      "SLOW_PROG_SECT")
#endif
