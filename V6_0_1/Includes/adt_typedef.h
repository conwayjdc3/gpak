/*___________________________________________________________________________
 |
 |   File: adt_typedef.h
 |
 |   This file contains the Adaptive Digital Technologies, Inc. 
 |   word length assignments based on processor type.
 |   
 |   This software is the property of Adaptive Digital Technologies, Inc.
 |   Unauthorized use is prohibited.
 |
 |   Copyright (c) 2000-2006, Adaptive Digital Technologies, Inc.
 |
 |   www.adaptivedigital.com
 |   610-825-0182
 |
 |______________________________________________________________________________
*/
//.S
#ifndef _ADT_TYPEDEF
#define _ADT_TYPEDEF
#include "adt_typedef_user.h"

#ifdef ADT_USE_FORCELIB
#include "forcelib.h"	//Helps when building mixture of CPP and C files together in Microsoft Visual Studio
#endif


#undef I64_AVAIL
#define bytesToWords(a) a
#define byteSize(a)     sizeof(a)

   #ifdef _TMS320C5XX

      #undef bytesToWords
      #define INLINE static inline
      #undef byteSize
      #define bytesToWords(a) (((a)+1)/2)
      #define byteSize(a)     (sizeof(a)*2)
   #endif

   #ifdef __TMS320C55XX__


      #define DSP_TYPE 55
      #define LowBitFirst 0

      #undef bytesToWords
      #define INLINE static inline
      #undef byteSize
      #define bytesToWords(a) (((a)+1)/2)
      #define byteSize(a)     (sizeof(a)*2)

   #endif

   #ifdef _TMS320C6X

      typedef short               Shortword;   //16b
      #define I64_AVAIL TRUE
      #define DSP_TYPE 64
      #define INLINE static inline
      #define LowBitFirst 1
   #endif
   
   #if defined(WIN32)


      #define I64_AVAIL TRUE
      #define INLINE static _inline
      #define LowBitFirst 1

   #endif 

   #if defined (LINUX) || defined (LINUX32) || defined(__arm)  || defined(__arm__) || defined(__i386)

#ifndef __APPLE_CC__
      #define INLINE static inline
#else
	  #define INLINE static inline
#endif
	  #define I64_AVAIL TRUE
      #define LowBitFirst 1
   #endif 


typedef ADT_Int8   Flag;

typedef unsigned int ADT_Word;  // Processor word size (at least 16-bits)

typedef ADT_Int8   Word8;
typedef ADT_Int16  Word16;
typedef ADT_Int32  Word32;
typedef ADT_UInt16 UWord16;
typedef ADT_UInt32 UWord32;



#ifndef _TI_STD_TYPES

#ifndef xdc_std__include
typedef ADT_Int8   Int8;
typedef ADT_Int16  Int16;
typedef ADT_Int32  Int32;

typedef ADT_UInt8  UInt8;
typedef ADT_UInt16 UInt16;
typedef ADT_UInt32 UInt32;

#ifdef I64_AVAIL
typedef ADT_Int64  Int64;
#endif

#endif 
#endif

#ifndef xdc__INT40__
#ifndef _CSL_STDINC_H_
typedef ADT_Int40  Int40;
#endif
#endif


typedef ADT_Int8   ADT_PCM8;
typedef ADT_Int16  ADT_PCM16;

#ifdef I64_AVAIL
   typedef ADT_Int64  Word64;
#endif

typedef ADT_UInt32 ADT_TimeMS;

#define ADT_TRUE           1
#define ADT_FALSE          0


#define todo(a) 
#endif //_ADT_TYPEDEF
//.E
