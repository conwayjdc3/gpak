#ifndef netbuffer_h
#define netbuffer_h

//==========================================================================
//          File:  netbuffer.h
//
//   Description
//
//    This file contains the prototypes for ADT's platform independent 
//    network APIs for converting between a network buffer (big endian order) 
//    and a  platform's little endian data formats.  In addition to endianess 
//    conversions, these routines also allow for 16-bit and 32-bit 
//    integers to be located on 'misaligned' byte boundaries within
//    the network buffer.
//
//    TI's C54 and C55 CPUs have the further constraint of processing bytes
//    in 16-bit integer fields.  The C54 and C55 implementation will 
//    convert between packed bytes (two bytes per 16-bit integer for network
//    buffer) and unpacked integer formats.
//  
//--------------------------------------------------------------
//    Conversion routines from network buffer to integer formats
//
//    These routines retrieve a 8-bit, 16-bit, or 32-bit integer value
//    whose first byte is located 'Offset' bytes into the 'Buff' buffer.
//    Upon return, the value of Offset is incremented by the number of bytes
//    accessed.
//
//         ADT_UInt8  netI8  (void *Buff, int *Offset);
//         ADT_UInt16 netI16 (void *Buff, int *Offset);
//         ADT_UInt32 netI32 (void *Buff, int *Offset);
//
//--------------------------------------------------------------
//    Conversion routines from integer formats to network buffer
//
//    These routines place a 8-bit, 16-bit, or 32-bit integer value starting
//    at the byte that is located 'Offset' bytes into the 'Buff' buffer.
//    Upon return, the value of Offset is incremented by the number of bytes
//    accessed.
//
//         void I8net  (void *Buff, int *Offset, ADT_UInt8 value);
//         void I16net (void *Buff, int *Offset, ADT_UInt16 value);
//         void I32net (void *Buff, int *Offset, ADT_UInt32 value);
//
//
//--------------------------------------------------------------
//    Conversion routines between network buffers and host buffers
//
//    These routines copy data between host and network buffers.  A byte 
//    'Offset' is applied to the network buffer to position the corresponding
//    Array buffer correctly within the network packet.  'ValueLen' identifies
//    the number of bytes to transfer from the source buffer to the destination
//    buffer. The copies are made maintaining the byte order of the source buffer.
//    The value of Offset is increased by the value of ValueLen.  It is the
//    programmer's responsibility to ensure that the destination buffer is large
//    enough for the transfer.
//
//      void netIx (void *NetBuff, int *Offset, void *Array, int ValueLen);
//      void Ixnet (void *NetBuff, int *Offset, void *Array, int ValueLen);
//

#include "adt_typedef.h"

#ifdef __cplusplus
extern "C" {
#endif

ADT_UInt8 netI8 (void *Buff, int *Offset);
ADT_UInt16 netI16 (void *Buff, int *Offset);
ADT_UInt32 netI32 (void *Buff, int *Offset);
void netIx (void *NetBuff, int *Offset, void *Array, int ValueLen);


void I8net (void *Buff, int *Offset, ADT_UInt8 value);
void I16net (void *Buff, int *Offset, ADT_UInt16 value);
void I32net (void *Buff, int *Offset, ADT_UInt32 value);
void Ixnet (void *NetBuff, int *Offset, void *Array, int ValueLen);

#ifdef __cplusplus
}
#endif

#endif
