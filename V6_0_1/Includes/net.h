/*
 * Copyright (c) 2006, Adaptive Digital Technologies, Inc.
 *
 * File Name: net.h
 *
 * Description:
 *   This file contains network interface definitions to facilitate platform
 *   independence.
 *
 * Version: 1.0
 *
 * Revision History:
 *   8/28/06 - Initial release.
 *
 */
#ifndef NET_H
#define NET_H

#include "adt_typedef.h"
#include <errno.h>

#if WIN32
   #include <winsock2.h>

   typedef struct _SOCKET_LIST {
       ADT_Int32       iAddressCount;
       SOCKET_ADDRESS  Address[10];
   } SOCKET_LIST;

   typedef int socklen_t;
   typedef SOCKET Socket;

   #define NetError        WSAGetLastError
   #define SetNetError(a)  WSASetLastError(a)
   #define closeSocket(a)  closesocket(a)

   #define EWOULDBLOCK             WSAEWOULDBLOCK
   #define EINPROGRESS             WSAEINPROGRESS
   #define EALREADY                WSAEALREADY
   #define ENOTSOCK                WSAENOTSOCK
   #define EDESTADDRREQ            WSAEDESTADDRREQ
   #define EMSGSIZE                WSAEMSGSIZE
   #define EPROTOTYPE              WSAEPROTOTYPE
   #define ENOPROTOOPT             WSAENOPROTOOPT
   #define EPROTONOSUPPORT         WSAEPROTONOSUPPORT
   #define ESOCKTNOSUPPORT         WSAESOCKTNOSUPPORT
   #define EOPNOTSUPP              WSAEOPNOTSUPP
   #define EPFNOSUPPORT            WSAEPFNOSUPPORT
   #define EAFNOSUPPORT            WSAEAFNOSUPPORT
   #define EADDRINUSE              WSAEADDRINUSE
   #define EADDRNOTAVAIL           WSAEADDRNOTAVAIL
   #define ENETDOWN                WSAENETDOWN
   #define ENETUNREACH             WSAENETUNREACH
   #define ENETRESET               WSAENETRESET
   #define ECONNABORTED            WSAECONNABORTED
   #define ECONNRESET              WSAECONNRESET
   #define ENOBUFS                 WSAENOBUFS
   #define EISCONN                 WSAEISCONN
   #define ENOTCONN                WSAENOTCONN
   #define ESHUTDOWN               WSAESHUTDOWN
   #define ETOOMANYREFS            WSAETOOMANYREFS
   #define ETIMEDOUT               WSAETIMEDOUT
   #define ECONNREFUSED            WSAECONNREFUSED
   #define ELOOP                   WSAELOOP
   #define EHOSTDOWN               WSAEHOSTDOWN
   #define EHOSTUNREACH            WSAEHOSTUNREACH
   #define EPROCLIM                WSAEPROCLIM
   #define EUSERS                  WSAEUSERS
   #define EDQUOT                  WSAEDQUOT
   #define ESTALE                  WSAESTALE
   #define EREMOTE                 WSAEREMOTE

   #define HOST_NOT_FOUND   WSAHOST_NOT_FOUND
   #define NO_DATA          WSANO_DATA
   #define NO_RECOVERY      WSANO_RECOVERY
   #define TRY_AGAIN        WSATRY_AGAIN
   #define NET_NO_ERROR        0

   #define sin_IP sin_addr.s_addr

   #define setAddr(addr,IP,port) \
      (addr)->sin_family = AF_INET;           \
      (addr)->sin_IP     = IP;                \
      (addr)->sin_port   = htons (port);        


   #define bzero(a,s) memset (a, 0, s)
   #define getErrno WSAGetLastError
   #define h_addrcnt   h_length
   #define SecsMills(a,b) (a*1000 + b)
   #define setBlocking(skt,blk)  ioctlsocket (skt, FIONBIO, &blk) 

   typedef int SktTimeOut;
   typedef enum  { BLOCK=0, NONBLOCK=1 } BLK_t;
   typedef unsigned long IPN;


   #define IP_OPTIONS          1           /* set/get IP per-packet options    */
   #define IP_MULTICAST_IF     2           /* set/get IP multicast interface   */
   #define IP_MULTICAST_TTL    3           /* set/get IP multicast timetolive  */
   #define IP_MULTICAST_LOOP   4           /* set/get IP multicast loopback    */
   #define IP_ADD_MEMBERSHIP   5           /* add  an IP group membership      */
   #define IP_DROP_MEMBERSHIP  6           /* drop an IP group membership      */
   #define IP_TTL              7           /* set/get IP Time To Live          */
   #define IP_TOS              8           /* set/get IP Type Of Service       */
   #define IP_DONTFRAGMENT     9           /* set/get IP Don't Fragment flag   */

   #define SOCK_STREAMNC SOCK_STREAM
   #define SHUT_RDWR     SD_BOTH
   #define EntAddr(hostent,idx) *((long *)hostEnt->h_addr_list[idx])

#endif



#if _TI_
   #include <netmain.h>

   typedef int Socket;
   typedef int socklen_t;

   #define NetError        fdError
   #define SetNetError(a)  
   #define closeSocket(a)  fdClose(a)

   #define sin_IP sin_addr.s_addr

   #define setAddr(addr,IP,port) \
      (addr)->sin_len    = sizeof (SktInAddr);  \
      (addr)->sin_family = AF_INET;             \
      (addr)->sin_IP     = htonl (IP);          \
      (addr)->sin_port   = htons (port);        

   #define getErrno() errno

   #define EAGAIN                1
   #define WSANOTINITIALISED  EPROTONOSUPPORT

   #define PF_INET AF_INET
   #define getHostByName  DNSGetHostByName
   typedef enum {BLOCK=1, NONBLOCK=0} BLK_t;
   typedef struct timeval SktTimeOut;
   #define SecsMills(a,b) { a, b }
   #define setBlocking(skt,blk)  setsockopt (skt, SOL_SOCKET, SO_BLOCKING, &blk, sizeof (int))
   #define EntAddr(hostent,idx) hostEnt->h_addr[idx]

#endif


typedef struct { char bufr[16];} IPStr;

typedef struct sockaddr     SktAddr;
typedef struct sockaddr_in  SktInAddr;




#ifdef __cplusplus
extern "C" {
#endif

ADT_Bool Connect (Socket skt, IPN, ADT_UInt16 dstPort);

//--------------------------------------------------------------------------------
//      UDP access
Socket   openUDPPort    (IPN, ADT_UInt16 port, int TTL, int TOS);
ADT_Bool getUDPMessage  (Socket, char* buf, int* len, IPN*, ADT_UInt16* srcPort);
ADT_Bool sendUDPMessage (Socket, char* msg, int  len, IPN,  ADT_UInt16  dstPort);

//--------------------------------------------------------------------------------
//      TCP access
Socket openTCPPort (IPN Ip, ADT_UInt16 port, int linger, BLK_t blocking);

//--------------------------------------------------------------------------------
//      DNS access
int getHostByName (char *DomainName, void *hostent, int size);
char *inet_ntop (SktInAddr *addr);

#ifdef __cplusplus
}
#endif


#endif
