/*
   ipStack.h

   G.Pak support for 64x IP stack
   

 */
#ifndef IPSTATS_H
#define IPSTATS_H

#include "adt_typedef_user.h"

#ifdef RESTORE_THIS    // jdc stack merge

#include <ti/ndk/inc/stack/inc/nimuif.h>

#include <ti/transport/ndk/nimu/src/v0/nimu_eth.h>
#include <ti/csl/csl_emac.h>
#include <ti/csl/csl_emacAux.h>
#include <ti/csl/csl_mdio.h>
#include <ti/csl/cslr_ectl.h>
#define UINT8 ADT_UInt8

#ifndef AppErr
#define AppErr(msg,cond) AppError (__FILE__, __LINE__, msg, cond)
#endif

// Force full ethernet stats for debugging.
#define FULL_ETH_STATS

#ifdef DEBUGON
#define FULL_ETH_STATS
#endif

#define     ETHERTYPE_IP            0x800
#define     ETHERTYPE_ARP           0x806
#define     ETHERTYPE_VLAN          0x8100
#define     ETHERTYPE_PPPOECTL      0x8863
#define     ETHERTYPE_PPPOEDATA     0x8864
#define     ETHERTYPE_IPv6          0x86DD

typedef enum statsID {
  emacStatsID = 0x44440001, ethStatsID, nimuStatsID, rawStatsID, ip4StatsID, udpStatsID
} StatsID;

extern NETIF_DEVICE *EMAC_NetIF;
extern PDINFO       *emacInst;
typedef struct emacStats {
   StatsID ID;
   struct emacTx {
      int Queued;
      int Delivered;

      int NoPackets;  // No packet buffers to use for transmission
      int Fatal;
   } tx;
   struct emacRx {
      int Queued;
      int Delivered;

      int RawQueued;
      int RawDelivered;

      int IPQueued;
      int IPDelivered;

      int Dropped;
      int NullPackets;   // Rx interrupt without rx packets
      int NoEmacBuffers;
      
   } rx;
   struct emacQueue {
      int add;
      int deleted;
      int current;
   } queue;

   int statsUpdate;   // Statistic update count
} emacStats_t;
extern emacStats_t emacStats;

typedef struct eth_info {
   PDINFO *pPDI;        // Handle to Device Info
   Handle hMDIO;        // Handle to MDIO instance 
   Uint32 PktMTU;       // Max packet size for ethernet interface
   UINT8 bMacAddr[6];   // Device's ethernet MAC address
   int   rxIntCnt;      // Interrupt counts
   int   txIntCnt;
   int   csl_errors;               /* CSL returned error due to software */
   int   emac_fatal_error;         /* EMAC fatal error */
   StatsID ID;
#ifdef FULL_ETH_STATS
   struct ethTx { 
      int IntError;
      int emacTxQueueEmpty;
      int PacketsBad;
      int PacketsDropped;
      int PacketsQueued;
      int PacketsDequeued;
      int PacketsTotal;
   } tx;  
   struct ethRx {
      int IntError;
      int ZeroLenError;
      int PBMQueueEmpty;
      int RawQueueDelivery;
      int IPQueueDelivery;
      int PacketsDequeued;
      int PacketsTotal;
   } rx;
   struct ethPktQueue {
      int add;
      unsigned int deleted;
      unsigned int current;
   } pktQueue;
   struct ethPbmPkt {
      int add;
      int deleted;
      int rxCurrent;
      int txCurrent;
   } pbmPkt;
#endif
} eth_info_t;
extern eth_info_t ethStats;

typedef struct nimu_stats_t {
   StatsID ID;
   struct nimuRx {
      int raw;
      int ip;
      int arp;
      int vlan;
      int mcast;
      int brdcast;
      int packetSizeErr;
      int total;
   } rx;
   struct nimuTx {
      int raw;
      int ip;
   } tx;
} nimu_stats_t;
extern nimu_stats_t nimuStats;
#endif // jdc

struct _fastRTPStats {
   int locals;
   int memoryDrops;
   int queued;
   int delivered;
   int requests;

   int acked;
};
extern struct _fastRTPStats fastRTPStats;

#ifdef RESTORE_THIS    // jdc stack merge

typedef struct _rawethstat {
    StatsID  ID;
    int      RcvTotal;       
    int      RcvDrops;        
    int      SndTotal;       
    int      SndNoPacket;
} RAWETHSTATS;
extern RAWETHSTATS raweths;

//
// IP Statistics
//
typedef struct IPSTATS {
   StatsID ID;
   struct ipsFrags {        // Statistics on individual fragments
      int  pending;      // pending reassembly

      int  memoryDrops;  // insufficient memory
      int  dropped;      // dropped (memory or timeouts)
      int  reassembled;  // reassembled and delivered
      int  total;        // total (memoryDrops + dtopped + reassembled)
   } Frags;

   struct ipsFragIDs {        // Statistics on fragmented packets
      int  tooLargeDrops;  // too large
      int  memoryDrops;    // insufficient memory
      int  timeoutDrops;   // timed out
      int  reassembled;    // total packets IDs reassembled and delivered
      int  newIDs;         // Total packets ID'd for reassembly
   } FragIDs;

   struct ipsRoute {
      int  cacheHit;   // Route cache hit
      int  cacheMiss;  // Route cache miss
      int  notFound;
      int  found;
      int  requests;
      int  gatewayRoute;
      int  gatewayNotFound;
   } route;

   struct ipsDelivery {
      int  tcp;
      int  udp;
      int  icmp;
      int  igmp;
      int  raw;
   } Delivered;

   struct ipsRx {
      int  ipInactive;
      int  badVers;
      int  badHlen;
      int  badLen;
      int  badSum;
      int  badRoute;
      int  badDest;
      int  badOptionSize;
      int  badOption;
      int  badAddress;
      int  badExpired;

      int  filtered;     // firewall filtering (dropped)
      int  forwarded;    // forwarded on network
      int  fragments;    // sent for re-assembly
      int  delivered;    // delivered to sockets
      int  total;        // total packets received
   } rx;

   struct ipsTx {
      int  redirects;      // packets forwarded with ICMP redirect message
      int  fragments;      // fragments created from multiFragmented packets

      int  singleFragment; // packets transmitted as single fragment
      int  multiFragment;  // packets transmitted as multiple fragments
      int  tooLarge;       // packets discarded as too large or fragmenting disallowed

      int  broadcastDrop;  // broadcast disallowed on socket
      int  forwardDrop;    // forwarding disallowing
      int  routeDrop;      // Local packets discarded due to no route found
      int  blackholeDelivery;
      int  localDelivery;

      int  forwardedRoutes;  // packets forwarded on a new route
      int  localRoutes;      // packets generated locally
      int  routed;           // total packets routed (forwardedRoutes + localRoutes)
                                // also (singleFragment + multiFragment + tooLarge)

      int  total;         // total packets sent (broadcastDrop + forwardDrop + routeDrop +
                             // blackholeDelivery + localDelivery + routed)
   } tx;
} IPSTATS;
extern IPSTATS ip4Stats;

typedef struct _udp4stat {
   StatsID ID;
   struct updRx {
      int  BadHdr;       // Dropped. Length shorter than header
      int  BadLen;       // Dropped. Length shorter than packet
      int  BadSum;       // Dropped. Bad check sum
      int  memoryDrop;   // Dropped. Insufficient memory
      int  droppedBroadcasts;     //  broadcast packets dropped as undeliverable
      int  dropped;               // non-broadcast packets dropped as undeliverable

      int  deliveredBroadcasts;   // Total broadcast packets delivered
      int  delivered;             // Total non-broadcast packets delivered
      int  total;                 // Total received
   } rx;

   struct updTx {
      int  noMemory;    
      int  tooLarge;
      int  undeliverable;
      int  delivered;
      int  total;
   } tx;

} UDP4STATS;
extern UDP4STATS udpStats;

extern far int UseMDIO;
extern far unsigned int EMAC_ModeFlag;

extern EMAC_Device *ethEmac;
extern EMAC_Device emacDev;
extern CSL_EmacRegs *emacRegs;
extern CSL_EctlRegs *ectlRegs;

extern EMAC_Pkt  emacHdrRx[];
extern EMAC_Pkt  emacHdrTx[];


typedef struct pktLog {
   int       type;
   void*     Desc;
   EMAC_Pkt* EMACPkt;
   void*     DataPkt;
   Uint32    Data;
} PKT_LOG;

extern PKT_LOG pktLog[];
extern int pktLogIdx;
extern int pktLogFilter;

typedef struct pkt_queue {
   Uint32 Count;
   EMAC_Pkt *pHead;
   EMAC_Pkt *pTail;
} PKT_QUEUE;

extern PKT_QUEUE emacRxQueue;
extern PKT_QUEUE emacTxQueue;

struct IPVars {
   struct emacStats     *emacStats;
   struct eth_info      *ethStats;
   struct _fastRTPStats *fastRTPStats;

   struct emacLog {
      int      *pktLogIdx;
      int      *pktLogFilter;
      PKT_LOG  (*pktLog)[];
   } emacLog;

   struct emac {
      PDINFO           **emacInst;
      NETIF_DEVICE     **EMAC_NetIF;
      EMAC_Device      **ethEmac;

      EMAC_Device  *emacDev;
      CSL_EmacRegs **emacRegs;
      CSL_EctlRegs **ectlRegs;

      EMAC_Pkt         (*emacHdrRx)[];
      EMAC_Pkt         (*emacHdrTx)[];
      unsigned int     *EMAC_ModeFlag;
   } emac;

   void  *IPConfig;
   int   *useCheckSumAsChannel;
   int   *UseMDIO;
};
#endif // jdc
#endif
