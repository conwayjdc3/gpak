#ifndef V6_0_H
#define V6_0_H

#define inc_aec_user            "iaecg4_v1_51.h"
#define inc_AdtGainSum          "AdtGainSum_v0100.h"
#define inc_adtCidUser          "adtCidUser_v0204.h"
#define inc_ti_t38_user         "ti_t38_user_v0102.h"
#define inc_arbTd_user          "arbit_toneDt_user_v0721.h"
#define inc_rtpapi              "rtpapi_v0313.h"
#define inc_rtpapi_user         "rtpapiuser_V0313.h"
#define inc_faxrelay            "faxrelay_v0100.h"
#define inc_speex_ADT_user      "speex_user_v0102.h"
#define inc_melpe_user          "melpe_user_v0101.h"

#if (DSP_TYPE == 54)

#define inc_vadcng_user         "vad_user.h"
#define inc_agc_user            "agc_user.h"
#define inc_NCAN_user           "NCAN_user.h"
#define inc_TG_USER             "TG_USER.H"
#define inc_tonedet_user        "tonedet_user.h"
#define inc_FAXCNG_user         "FaxCNG_user_v0100.h"
#define inc_FAXCNG              "FaxCNG_v0100.h"
#define inc_FAXCEDdet           "FaxCEDdet_v0100.h"
#define inc_tonerelayDet        "tonerelayDet.h"
#define inc_tonerelayGen        "tonerelayGen.h"
#define inc_g168_user           "g168_user.h"
#define inc_conf_ADT_user       "conf_ADT_user.h"
#define inc_g711_user           "G711.H"

#elif (DSP_TYPE == 55)

#define inc_vadcng_user         "vadcng_user_v0402.h"
#define inc_agc_user            "agc_user_v0209.h"
#define inc_NCAN_user           "NCAN_user_v0103.h"
#define inc_TG_USER             "TG_USER_v0402.H"
#define inc_tonedet_user        "tonedet_user_v0560.h"
#define inc_tonerelayDet        "tonerelayDet_v0302.h"
#define inc_tonerelayGen        "tonerelayGen_v0302.h"
#define inc_g168_user           "g168_user_v1030.h"
#define inc_conf_ADT_user       "conf_ADT_user_v0402.h"
#define inc_aeq_user            "adtEq.h"

#else
// NOTE:  Conference, AGC, and VAD library must all be compatible
#define inc_conf_ADT_user       "conf_ADT_user_v0406.h"
#define inc_vadcng_user         "vadcng_user_v0406.h"
#define inc_agc_user            "agc_user_v0406.h"

// Tone gen and relay gen must be compatible
#define inc_TG_USER             "TG_USER_v0721.H"
#define inc_tonerelayGen        "tonerelayGen_v721.h"

// Tone detect and relay detect must be compatible
#define inc_tonedet_user        "tonedet_user_v0721.h"
#define inc_tonerelayDet        "tonerelayDet_v0721.h"

#define inc_NCAN_user           "NCAN_user_v0102.h"
#define inc_FAXCNG_user         "FaxCNG_user_v0100.h"
#define inc_FAXCNG              "FaxCNG_v0100.h"
#define inc_FAXCEDdet           "FaxCEDdet_v0100.h"

#define inc_g168_user           "g168_user_v111803.h"
#define inc_aeq_user            "adtEq.h"

#define inc_g711_user           "G711.H"
#define inc_g722_ADT_user       "G722_user_v21.h"
#define inc_src2_ADT_user       "rateConvert_user_v1_03.h"
#define inc_srtp_ADT_user       "srtp_user_v0104.h"

#endif

#endif
