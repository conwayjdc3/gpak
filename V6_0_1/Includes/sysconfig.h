//*******************************************************************
//
// Copyright(c) 2001-2007 Adaptive Digital Technologies Inc. 
//              All Rights Reserved
//
//
// File Name:   Sysconfig.h
// 
//
// Description: G.PAK system configuration structures.  The G.PAK
//              build utility and G.PAK Dsp both use this file to
//              identify the build parameters
//
// Initial Version: 11/15/2001
//
//
// Modified:
//   1/2007 - Combined C54 / C64 G.PAK
//  11/2007 - Added MELP
//
//******************************************************************
#ifndef _SYSCONFIG_H
#define _SYSCONFIG_H

#ifdef __BIOS__
#include <std.h>
#endif

#include <adt_typedef.h>
#include "GpakEnum.h"

#define DEFAULT_GPAK_API_VERSION    0x0401u // DO NOT CHANGE THIS!

// Definition of DSP Types.
typedef enum {
   Ti5402 = 5402,
   Ti5410 = 5410,
   Ti5416 = 5416,
   Ti5420 = 5420,
   Ti5421 = 5421,
   Ti5441 = 5441,

   Ti6416 = 6416,

   Ti6424 = 6424,
   Ti6452 = 6452,

   Ti642  = 642,
   Ti6437 = 6437,
   Ti647  = 647,
   Ti648  = 648,
   TiDM642  = Ti642,
   TiDM6437 = Ti6437,
   TiDM648  = Ti648,

   TiOM3530 = 3530,
   Ti5510 = 5510,
   Ti674  = 674,
   TiOML137 = Ti674,
   TiOML138 = Ti674,
   TiDM816x = Ti674,
   Ti665    = 665,
   Ti667    = 667

} DspType_t;

#define MAX_NUM_SERIAL_PORTS 3  // 5420 == 3

// Configured Frame Sizes and Channel Types status bits.
#define FRAME_8_CFG_BIT  0x0001      // Frame Size 8 
#define FRAME_20_CFG_BIT 0x0002      // Frame Size 20 
#define FRAME_40_CFG_BIT 0x0004      // Frame Size 40 
#define FRAME_80_CFG_BIT 0x0008      // Frame Size 80 
#define FRAME_160_CFG_BIT 0x0010     // Frame Size 160 
#define FRAME_180_CFG_BIT 0x0020     // Frame Size 180 
#define FRAME_240_CFG_BIT 0x0040     // Frame Size 240 

#define PCM2PKT_CFG_BIT  0x8000      // PCM to Pkt 
#define PCM2PCM_CFG_BIT  0x4000      // PCM to PCM 
#define PKT2PKT_CFG_BIT  0x2000      // Pkt to Pkt 
#define CIRCDATA_CFG_BIT 0x1000      // Circuit Data 
#define CONFPCM_CFG_BIT  0x0800      // Conference PCM 
#define CONFPKT_CFG_BIT  0x0400      // Conference Pkt 
#define CONFCOMP_CFG_BIT 0x0200      // Conf Composite 

// MSC compiler assigns bits from LSB to MSB
#define gsmEFREnable AMREnable

#ifndef __TMS320C55XX__
typedef struct AlgorithmMap {
   unsigned wbEnable:1;            //  0 - wide band processing is enabled
   unsigned apiCacheEnable:1;      //  1 - api Cache Coherance Enable Flag
   unsigned networkStack:1;        //  2 - network stack is enabled
   unsigned rtpNetDelivery:1;      //  3 - RTP packets delivered to network
   unsigned rtpHostDelivery:1;     //  4 - RTP packets delivered to host
   unsigned spares5:1;             //      Spares
   unsigned toneDetEnable_B:1;     //  6 - B side Detection
   unsigned tasDetEnable:1;        //  7 - TAS tone detector (Rx Cid needed)
   unsigned noiseSuppressEnable:1; //  8 - Noise suppression
   unsigned spares9:4;             //       Spares     
   unsigned g168VarAEnable:1;      //  13 - Short Tail Echo Cancel
   unsigned g168PktEnable:1;       //  14 - Packet Echo Cancel
   unsigned g168PcmEnable:1;       //  15 - PCM Echo Cancel
   unsigned spares16:5;            //       Spares
   unsigned dtmfDialEnable:1;      //  21 - Dtmf Dialing
   unsigned toneRlyDetEnable:1;    //  22 - Tone Relay Detect
   unsigned toneRlyGenEnable:1;    //  23 - Tone Relay Generate
   unsigned cprgEnable:1;          //  24 - Call Progress Detection
   unsigned mfr2BackEnable:1;      //  25 - MFR2 Backward Detection
   unsigned mfr2FwdEnable:1;       //  26 - MFR2 Forward Detection
   unsigned mfr1Enable:1;          //  27 - MFR1 Detection
   unsigned dtmfEnable:1;          //  28 - DTMF Detection
   unsigned toneGenEnable:1;       //  29 - Tone Generation
   unsigned vadCngEnable:1;        //  30 - VAD and CNG
   unsigned agcEnable:1;           //  31 - AGC
} AlgorithmMap;

typedef struct CodecsMap {
   unsigned g722Enable:1;          //   0 - G722
   unsigned speexEnable:1;         //   1 - Speex
   unsigned TI_g729ABEnable:1;     //   2 - TI's G729AB is used
   unsigned speexWBEnable:1;       //   3 - Speex wideband
   unsigned spares4:1;             //       Spares
   unsigned melpEnable:1;          //   5 - Melp
   unsigned melpEEnable:1;         //   6 - MelpE
   unsigned g729ABEnable:1;        //   7 - G.729AB
   unsigned spares8:1;             //       Spares
   unsigned AMREnable:1;           //   9 - AMR
   unsigned adt4800Enable:1;       //  10 - ADT 4800
   unsigned g728Enable:1;          //  11 - G.728
   unsigned g723Enable:1;          //  12 - G.723
   unsigned g726Rate40Enable:1;    //  13 - G.726 Low MIPS 40 kbps
   unsigned g726Rate32Enable:1;    //  14 - G.726 Low MIPS 32 kbps
   unsigned g726Rate24Enable:1;    //  15 - G.726 Low MIPS 24 kbps
   unsigned g726Rate16Enable:1;    //  16 - G.726 Low MIPS 16 kbps
   unsigned g726LowMipsEnable:1;   //  17 - G.726 Low MIPS
   unsigned g726LowMemEnable:1;    //  18 - G.726 Low Memory
   unsigned TI_g7231AEnable:1;     //  19 - TI's G7231A is used
   unsigned spares19:12;           //      Spares
} CodecsMap;
#else
typedef struct AlgorithmMap {
   unsigned agcEnable:1;           //  31 - AGC
   unsigned vadCngEnable:1;        //  30 - VAD and CNG
   unsigned toneGenEnable:1;       //  29 - Tone Generation
   unsigned dtmfEnable:1;          //  28 - DTMF Detection
   unsigned mfr1Enable:1;          //  27 - MFR1 Detection
   unsigned mfr2FwdEnable:1;       //  26 - MFR2 Forward Detection
   unsigned mfr2BackEnable:1;      //  25 - MFR2 Backward Detection
   unsigned cprgEnable:1;          //  24 - Call Progress Detection
   unsigned toneRlyGenEnable:1;    //  23 - Tone Relay Generate
   unsigned toneRlyDetEnable:1;    //  22 - Tone Relay Detect
   unsigned dtmfDialEnable:1;      //  21 - Dtmf Dialing
   unsigned spares16:5;            //       Spares
   unsigned g168PcmEnable:1;       //  15 - PCM Echo Cancel
   unsigned g168PktEnable:1;       //  14 - Packet Echo Cancel
   unsigned g168VarAEnable:1;      //  13 - Short Tail Echo Cancel
   unsigned spares9:4;             //       Spares     
   unsigned noiseSuppressEnable:1; //  8 - Noise suppression
   unsigned spares7:1;             //  7 - Spares
   unsigned toneDetEnable_B:1;     //  6 - B side Detection
   unsigned spares5:1;             //      Spares
   unsigned rtpHostDelivery:1;     //  4 - RTP packets delivered to host
   unsigned rtpNetDelivery:1;      //  3 - RTP packets delivered to network
   unsigned networkStack:1;        //  2 - IP stack included in build
   unsigned apiCacheEnable:1;      //  1 - api Cache Coherance Enable Flag
   unsigned wbEnable:1;            //  0 - wide band processing is enabled
} AlgorithmMap;

struct CodecsMap {
   unsigned spares19:12;           //      Spares
   unsigned g726LowMemEnable:1;    //  18- G.726 Low Memory
   unsigned g726LowMipsEnable:1;   //  17 - G.726 Low MIPS
   unsigned g726Rate16Enable:1;    //  16 - G.726 Low MIPS 16 kbps
   unsigned g726Rate24Enable:1;    //  15 - G.726 Low MIPS 24 kbps
   unsigned g726Rate32Enable:1;    //  14 - G.726 Low MIPS 32 kbps
   unsigned g726Rate40Enable:1;    //  13 - G.726 Low MIPS 40 kbps
   unsigned g723Enable:1;          //  12 - G.723
   unsigned g728Enable:1;          //  11 - G.728
   unsigned adt4800Enable:1;       //  10 - ADT 4800
   unsigned AMREnable:1;           //   9 - AMR
   unsigned spares8:1;             //       Spares
   unsigned g729ABEnable:1;        //   7 - G.729AB
   unsigned melpEEnable:1;         //   6 - MelpE
   unsigned melpEnable:1;          //   5 - Melp
   unsigned spares4:1;             //       Spares
   unsigned speexWBEnable:1;       //   3 - Speex wideband
   unsigned TI_g729ABEnable:1;     //   2 - TI's G729AB is used
   unsigned speexEnable:1;         //   1 - Speex
   unsigned g722Enable:1;          //   0 - G722
} CodecMaps;
#endif

typedef union enableFlags_t {
   ADT_UInt32 LongWord;
   AlgorithmMap Bits;
} enableFlags_t;

typedef union codecFlags_t {
   ADT_UInt32 LongWord;
   CodecsMap  Bits;
} codecFlags_t;

typedef struct sysConfig_t {
    enableFlags_t  enab;
    codecFlags_t   Codecs;
    ADT_UInt32     gpakVersion;
    DspType_t      DspType;
    void          *hpiBlockStartAddress;
    ADT_UInt16     DspMips;
    
    // Serial port initialization
    ADT_UInt16     numSlotsOnStream[MAX_NUM_SERIAL_PORTS];   // Frame size
    ADT_UInt16     maxSlotsSupported[MAX_NUM_SERIAL_PORTS];  // Active channels
    ADT_UInt16     serialWordSize[MAX_NUM_SERIAL_PORTS];     // Bit length
    GpakCompandModes compandingMode[MAX_NUM_SERIAL_PORTS];
    ADT_UInt16     dmaBufferSize[MAX_NUM_SERIAL_PORTS];
    ADT_UInt16     txFrameSyncPolarity[MAX_NUM_SERIAL_PORTS];  
    ADT_UInt16     rxFrameSyncPolarity[MAX_NUM_SERIAL_PORTS];  
    ADT_UInt16     txClockPolarity[MAX_NUM_SERIAL_PORTS];      
    ADT_UInt16     rxClockPolarity[MAX_NUM_SERIAL_PORTS];      
    ADT_UInt16     txDataDelay[MAX_NUM_SERIAL_PORTS];          
    ADT_UInt16     rxDataDelay[MAX_NUM_SERIAL_PORTS];          
    ADT_UInt16     dxDelay[MAX_NUM_SERIAL_PORTS];              
    
    GpakProfiles   packetProfile;
    GpakRTPPkts    rtpPktType;
    ADT_UInt16     cfgFramesChans;   // configured frames and channel types

    // AGC Initialization
    ADT_Int16   agcTargetPower;     // dBm
    ADT_Int16   agcMinGain;         // dB
    ADT_Int16   agcMaxGain;         // dB
    ADT_Int16   agcLowSigThreshDBM; // dBm

    // B-Side AGC Initialization
    ADT_Int16   agcTargetPowerB;      // dBm
    ADT_Int16   agcMinGainB;          // dB
    ADT_Int16   agcMaxGainB;          // dB
    ADT_Int16   agcLowSigThreshDBMB;  // dBm

    // VAD Initialization
    ADT_Int16    vadNoiseFloorZThresh;  // dBm
    ADT_Int16    vadHangOverTime;       // ms
    ADT_Int16    vadWindowMs;           // ms

    // Conferencing Initialization
    ADT_UInt16    maxNumChannels;      // Total channels
    ADT_UInt16    maxNumConferences;   // Gpak conferences
    ADT_UInt16    customCnfrCnt;       // Custom conferences
    ADT_UInt16    maxNumConfChans;     // Channels per conference

    // Echo cancellation
    ADT_UInt16   maxG168PcmTapLength;          // samples
    ADT_UInt16   maxG168PktTapLength;          // samples
    ADT_UInt16   maxG168PcmNFIRSegments;       // segments
    ADT_UInt16   maxG168PktNFIRSegments;       // segments
    ADT_UInt16   maxG168PcmFIRSegmentLength;   // samples
    ADT_UInt16   maxG168PktFIRSegmentLength;   // sample

    // Buffer sizes
    ADT_UInt16   saramBlockSize;
    ADT_UInt16   daramBlockSize;
    ADT_UInt16   pcmInBufferSize;
    ADT_UInt16   pcmOutBufferSize;
    ADT_UInt16   pktInOutBufferSize;
    ADT_UInt16   bulkDelayBufferSize;

    ADT_Int16    numConfDominant;        // num dominant Conference members
    ADT_Int16    maxConfNoiseSuppress;   // max Conference Noise Suppression
    ADT_UInt16   numPcmEcans;            // number of PCM Echo Cancellers
    ADT_UInt16   numPktEcans;            // number of Packet Echo Cancellers
    ADT_UInt16   maxToneDetTypes;        // max number of Tone Detect types
    ADT_UInt16   AECInstances;           // Max acoustic echo canceller instances
    ADT_UInt16   AECTailLen;             // Max tail length for acoustic echo canceller

    ADT_UInt16   numFaxChans;            // number of fax instances
    ADT_UInt16   numTGChans;             // number of tone gen instances
    ADT_UInt16   numAGCChans;            // number of AGC instances

    ADT_UInt16   HostIF;                 // bit map of host interfaces
#define HPI_IF   1
#define PCI_IF   2
#define RMII_IF  4
    
    ADT_UInt16   samplesPerMs;           // PCM sampling rate
    ADT_UInt16   maxJitterms;            // Maximum memory allocation (ms) allowed for jitter buffers
    ADT_UInt16   smallJBBlockI32;        // Size of memory block for allocation from 'small pool'
    ADT_UInt16   largeJBBlockI32;        // Size of memory block for allocation from 'large pool'
    ADT_UInt16   hugeJBBlockI32;         // Size of memory block for allocation from 'huge pool'
    
    ADT_UInt32   RTPBuffersI16;          // Size of memory allocated for transfer of RTP packets

    // Caller Id information
    ADT_UInt16  numTxCidChans;      // number of Tx Caller ID instances
    ADT_UInt16  numRxCidChans;      // number of Rx Caller ID instances
    ADT_Int16   type2CID;           // (Tx and Rx) 1==both type 1 and type 2, 0==Type 1 only
    ADT_Int16   numSeizureBytes;    // (Tx only) num seizure bytes to prepend
    ADT_Int16   numMarkBytes;       // (Tx only) num mark bytes to prepend
    ADT_Int16   numPostambleBytes;  // (Tx only) num postamble bytes to append
    GpakFskType fskType;            // (Tx and Rx) v23 or bell 202 
    ADT_Int16   fskLevel;           // (Tx only) Tx fsk level (0...-19 dBm)
    ADT_UInt16  msgType;            // (Tx only) message type field (valid if format==1)
    ADT_UInt16  evtFifoLenI16;      // Event FIFO length in I16 units
    ADT_UInt16  maxNumPcmBuffs;     // max number of buffers in pcm pool
    ADT_UInt16  maxNumPktBuffs;     // max number of buffers in pkt pool
    ADT_UInt16  maxNumBulkDelayBuffs; // max number of buffers in bulkDelay pool
    GpakActivation VadReport;       // flag to control Vad state reports to host
    ADT_UInt16 EcanPcmSaveLenI16;   // pcm EC coef save length
    ADT_UInt16 EcanPktSaveLenI16;   // pcm EC coef save length
    ADT_UInt16 maxNumLbCoders;      // Max number loopback coders
    ADT_UInt16 rtpMaxBuffsPerChan;  // Max number of RTP buffers per channel
    ADT_UInt16 micEqLength;         // Aec Microphone equalizer length
    ADT_UInt16 spkrEqLength;        // Aec Speaker equalizer length
    ADT_UInt16 g168MipsConserveLev; // MipsConserve Level 0-4
	ADT_UInt16 numSrcChans;				// num of Sampling Rate Converter Channels
	ADT_UInt16 SRTP_Enable;				// 1: enabled, 0: disabled

} sysConfig_t;

#define SysAlgs    sysConfig.enab.Bits
#define SysCodecs  sysConfig.Codecs.Bits

typedef struct EcParams {
    ADT_Int16  TapLength;            // samples
    ADT_Int16  NLPType;              // None, mutb, hoth, random ...
    ADT_Int16  AdaptEnable;
    ADT_Int16  BypassEnable;
    ADT_Int16  G165DetectEnable;
    ADT_Int16  DoubleTalkThres;      // dB
    ADT_Int16  MaxDoubleTalkThres;   // dB
   ADT_Int16 SaturationLevel;	// Far end input level above which significant saturation/nonlinearity may be expected.  
	ADT_Int16 DynamicNLPAggressiveness;
    ADT_Int16  NLPThres;             // dB
    ADT_Int16  NlpUpperThresConv;    // Convergence limit dB
    ADT_Int16  NlpUpperThresUnConv;  // Preconvergence limit dB
   ADT_Int16 NLPSaturationThreshold;	//NLP threshold under conditions of possible saturation
    ADT_Int16  NlpMaxSupp;           // DB limit when NlpType = NLP_SUPP
    ADT_Int16  CNGNoiseThreshold;    // dBm
    ADT_Int16  AdaptLimit;           // % of frame
    ADT_Int16  CrossCorrLimit;       // % of frame
    ADT_Int16  FirTapCheckPeriod;    // Number FIR adaptations before bg processing
    ADT_Int16  NFIRSegments;         // segments
    ADT_Int16  FIRSegmentLength;     // samples

    ADT_Int16  TandemOperation;      // Multi-canceller path
    ADT_Int16  MixedFourWireMode;    // Null echo source
    ADT_Int16  ReconvergenceChk;     // Allow reconvergence of canceller
// Smart Packet Mode specific parameters
	ADT_Int16 SmartPacketModeSelect;			//see SMART_PACKET_SELECT_* options below
	ADT_Int16 SmartPacketBypassERLThresholddB; //If ERL is less than this number, bypass the EC
	ADT_Int16 SmartPacketSuppressERLThresholddB; //if ERL is less than this number, perform suppression only, no cancellation
	ADT_Int16 SmartPacketNLPThresholddB;		//If we are performing suppress only, use this value as the NLP threshold
} EcParams;

typedef struct ecConfig_t {
    EcParams  g168Pcm;
    EcParams  g168Pkt;
} ecConfig_t;

// Definition of limits for System Parameter values.
// AGC parameter ranges
#define MIN_AGC_TARGET_PWR -30    // Target Power (dBm)
#define MAX_AGC_TARGET_PWR   0     
#define MIN_AGC_LOSS_LIMIT -23    // Loss Limit (dB)
#define MAX_AGC_LOSS_LIMIT   0   
#define MIN_AGC_GAIN_LIMIT   0    // Gain Limit (dB)
#define MAX_AGC_GAIN_LIMIT  23    
#define MIN_AGC_LOW_SIG_LIMIT -65 // Noise Limits (dBm)
#define MAX_AGC_LOW_SIG_LIMIT -20

// VAD parameter ranges
#define MIN_VAD_NOISE_FLOOR -96   // Noise Floor (dBm)
#define MAX_VAD_NOISE_FLOOR   0 
#define MIN_VAD_HANG_TIME     0   // Hang Over Time (ms)
#define MAX_VAD_HANG_TIME  4000
#define MIN_VAD_WIN_MS        1   // Window Analysis Size (ms - stored as samples)
#define MAX_VAD_WIN_MS       10

// Echo cancellation parameter ranges
//#define MIN_EC_TAP_LENGTH    64    // TAP Length

#define MIN_EC_DBL_TALK       0  // Double Talk Threshold (dB)
#define MAX_EC_DBL_TALK      18 
#define MAX_EC_MAX_DBL_TALK  50
  
#define MIN_EC_CNG_THRESH     0  // CNG Noise Threshold (dBm)
#define MAX_EC_CNG_THRESH    96   
#define MIN_EC_ADAPT_LIMIT    1  // Adapt Limit (%)
#define MAX_EC_ADAPT_LIMIT  100   
#define MIN_EC_CROSSCOR_LMT   1  // Cross Corr Limit (%)
#define MAX_EC_CROSSCOR_LMT 100   
#define MIN_EC_NUM_FIR_SEGS   1  // Minimum FIR Segments
#define MIN_EC_FIR_SEG_LEN   32  // Minimun FIR segment length (samples)

#define MIN_EC_NLP_TYPE          0  // NLP Type (None, mute, hoth, random...)
#define MAX_EC_NLP_TYPE          6      
#define MIN_EC_NLP_THRESH        0  // NLP Threshold (dB)
#define MAX_EC_NLP_THRESH       40  
#define MIN_EC_NLP_CONVERGE      0  // NLP Convergence Threshold (dB)
#define MAX_EC_NLP_CONVERGE     90
#define MIN_EC_NLP_NONCONVERGE   0  // NLP Non-convergence Threshold (dB)
#define MAX_EC_NLP_NONCONVERGE  90
#define MIN_EC_NLP_MAXSUPP       0  // NLP Max suppression (dB)
#define MAX_EC_NLP_MAXSUPP      90
#define MIN_EC_FIR_TAP_CHECK    10  // FIR Tap check frequency (samples)
#define MAX_EC_FIR_TAP_CHECK  8000
#define MIN_EC_SATURATION      -96  // Saturation level (dBm)
#define MAX_EC_SATURATION        3
#define MIN_EC_NLP_SAT_THRESH    0  // NLP Threshold during saturation condition
#define MAX_EC_NLP_SAT_THRESH   40
#define MIN_MIPS_CONSERVE        0  // Mips Conserve
#define MAX_MIPS_CONSERVE        4



// Acoustic echo canceller parameter ranges
#define MIN_AEC_TAIL_LEN_MS             8
#define MIN_AEC_TX_NLP_THRESH_DB        0 // maxTxNLPThresholddB
#define MAX_AEC_TX_NLP_THRESH_DB       96 
#define MIN_AEC_TX_LOSS_DB              0 // maxTxLossdB
#define MAX_AEC_TX_LOSS_DB             96
#define MIN_AEC_RX_LOSS_DB              0 // maxRxLossdB
#define MAX_AEC_RX_LOSS_DB             96
#define MIN_AEC_TARGET_RESIDUAL_DBM   -96 // targetResidualLeveldBm
#define MAX_AEC_TARGET_RESIDUAL_DBM     0
#define MIN_AEC_RX_NOISELEV_DBM       -96 // maxRxNoiseLeveldBm
#define MAX_AEC_RX_NOISELEV_DBM         0
#define MIN_AEC_WORST_ERL_DB          -40 // worstExpectedERLdB
#define MAX_AEC_WORST_ERL_DB           96
#define MIN_AEC_RX_SATLEV_DB          -96 // rxSaturateLeveldBm
#define MAX_AEC_RX_SATLEV_DB            3
#define MIN_AEC_FIXGAIN_DB            -40 // fixedGaindB10
#define MAX_AEC_FIXGAIN_DB             40
#define MIN_AEC_TRAIN_TIME_MS           0 // maxTrainingTimeMSec
#define MAX_AEC_TRAIN_TIME_MS       32000
#define MIN_AEC_TRAIN_RX_NOISE_DBM    -96 // trainingRxNoiseLeveldBm
#define MAX_AEC_TRAIN_RX_NOISE_DBM      0
#define MIN_AEC_AGC_GAIN_DB             0 
#define MAX_AEC_AGC_GAIN_DB            23 // agcMaxGaindB
#define MIN_AEC_AGC_LOSS_DB            0  
#define MAX_AEC_AGC_LOSS_DB            23 // agcMaxLossdB
#define MIN_AEC_AGC_TRGTLEV_DBM       -30 // agcTargetLeveldBm
#define MAX_AEC_AGC_TRGTLEV_DBM         0
#define MIN_AEC_AGC_LOWSIG_THRESH_DBM -65 // agcLowSigThreshdBm
#define MAX_AEC_AGC_LOWSIG_THRESH_DBM -10
#define MIN_AEC_TX_NLP_AGGRESSIVENESS -40 // maxTxNLPThresholddB
#define MAX_AEC_TX_NLP_AGGRESSIVENESS  40
#define MIN_AEC_NR_SETTING             0
#define MAX_AEC_NR_SETTING             31

// Noise suppression parameter ranges
#define MIN_NOISE_ATTENUATION  0   //  Attenuation
#define MAX_NOISE_ATTENUATION 35
#define MIN_NOISE_WINDOW       8   // Sample window
#define MAX_NOISE_WINDOW     240
#define MAX_MAX_CONF_NOISE_SUPPRESS 95   //  Conference Noise Suppress

// Tone generation parameters ranges
#define MAX_TONEGEN_LEVEL    0   // Level
#define MIN_TONEGEN_LEVEL  -19   // 

// Caller ID parameter ranges
#define CID_MAX_SEIZURE_BYTES   38
#define CID_MAX_MARK_BYTES      23
#define CID_MAX_POST_BYTES      10
#define CID_MIN_FSK_LEVEL       -19

extern sysConfig_t sysConfig;

#endif // conditional include
