#ifndef GPAKPCM_H

#define GPAKPCM_H

#include "adt_typedef.h"
#include "CircBuff.h"

#define NUM_TDM_PORTS   3      // tdm port count is always 3 for gpak.

// Structure to define DMA resources used by a port
typedef struct PortAddr {
  ADT_UInt32 TxAddr, RxAddr;    // Tx and Rx TDM bus transfer address
  ADT_UInt32 TxChn,  RxChn;     // Tx and Rx event parameter block indices
  ADT_UInt32 TxPing, RxPing;    // Tx and Rx Ping parameter indices (pong = ping + 1)
  ADT_PCM16 **TxBuf, **RxBuf;   // Tx and Rx current DMA transfer pointers
  ADT_UInt32 TxTcc,  RxTcc;     // Tx and Rx Tcc Codes
} PortAddr;

//  Globals
#if defined(_TMS320C6600)
#ifdef SOC_665X
   #include "665x.h"
#endif
#ifdef SOC_667X
   #include "667x.h"
#endif
   ADT_UInt32 enableDMA  (int port);
   ADT_UInt32 disableDMA (int port);
#else

#ifdef _TMS320C6740
#ifdef SOC_81XX
   #include "81xx.h"
#else
   #include "674x.h"
#endif
   ADT_UInt32 enableDMA  (int port);
   ADT_UInt32 disableDMA (int port);
#else
   #ifdef _TMS320C6400_PLUS
      #include "64plus.h"
   #endif
   ADT_UInt16 enableDMA  (int port);
   ADT_UInt16 disableDMA (int port);
#endif
#endif


#define SAMPLES_PER_INTERRUPT sysConfig.samplesPerMs  // num stream frames per interrupt
#define SINK_DRAIN_LEN_I16  16                        // Size sink and drain for 16 KHz TDM
#define MAX_SAMPLES_PER_MS  16

#define UNCONFIGURED_SLOT 65535   // reserved slot index for Unconfigured


#if (DSP_TYPE == 54)
   #define MULTI_REG 2   // Number of McBSP multi-channel mask registers

   #define MAX_CHANS_PER_FRAME  32   // max Num of stream time slots
#elif (DSP_TYPE == 55)
   #define MULTI_REG 8   // Number of McBSP multi-channel mask registers
   #define MAX_CHANS_PER_FRAME  128   // max Num of stream time slots
   
#elif (DSP_TYPE == 64)
   #define MULTI_REG 4
   #define MAX_CHANS_PER_FRAME 128    // max Num of stream time slots

   #define REG_WR(addr, val) *(volatile ADT_UInt32 *)(addr) = (ADT_UInt32) val
   #define REG_RD(addr, val) val = *(volatile ADT_UInt32 *)(addr) 
   #define REG_AND(addr,val) *(volatile ADT_UInt32 *)(addr) &= val
   #define REG_OR(addr,val)  *(volatile ADT_UInt32 *)(addr) = (*(volatile ADT_UInt32 *)addr) | val

#endif
extern ADT_UInt16 SltsPerFrame[NUM_TDM_PORTS];   // Time slots on McBSP frame [build time]
extern ADT_UInt16 DmaSlotCnt[NUM_TDM_PORTS];     // Count of active DMA channels [McBSP configuration]
extern ADT_UInt16 MaxDmaSlots [NUM_TDM_PORTS];  // Max slots available for DMA [fixed at build]
extern ADT_UInt16 TxSltsPerFrame[NUM_TDM_PORTS];   // Time slots on McBSP frame [build time]
extern ADT_UInt16 TxDmaSlotCnt[NUM_TDM_PORTS];     // Count of active DMA channels [McBSP configuration]

extern ADT_UInt32 SlotCompandMode[NUM_TDM_PORTS][4]; // 0 == U-law, 1 == A-law
extern int UseSlotCmpMask[NUM_TDM_PORTS];
extern CircBufInfo_t *pSink;
extern CircBufInfo_t *pDrain;
extern ADT_UInt16 sinkBuff[];
extern ADT_UInt16 drainBuff[];

void StartGpakPcm (ADT_UInt16 DmaChanCnt[]);

void DMAInitialize ();


ADT_Bool DmaLostTxSync (int port);
ADT_Bool DmaLostRxSync (int port);


ADT_Bool CopyDmaTxBuffers (int port, int slotCnt, CircBufInfo_t **Tx, int slip);
ADT_Bool CopyDmaRxBuffers (int port, int slotCnt, CircBufInfo_t **Rx, int slip);



// Structure to allow McASP or other non-standard serial 
// port to share DMA with McBSP.

typedef struct tdmPort {
   ADT_Bool (*StartIO) (int port);
   ADT_Bool (*StopIO)  (int port);
   ADT_Bool (*SetupIO) (int port);
} TDM_Port; 

extern TDM_Port *tdmPortFuncs;

typedef struct {
    ADT_UInt32 McASPPinTypes;
    ADT_UInt32 McASPFormat;
    ADT_UInt32 McASPTxFrameSync;
    ADT_UInt32 McASPRxFrameSync;
    ADT_UInt32 McASPClk;
    ADT_UInt32 McASPHFClk;
    ADT_UInt32 McASPInactivePin;
    ADT_UInt32 McASPTransmitPin;
    ADT_UInt32 McASPReceivePin;
    ADT_UInt32 McASPSlotWidth;
    ADT_UInt32 McASPSlotMask;
    ADT_UInt32 McAspPinDir;
    ADT_UInt32 McAspMask;
    ADT_UInt32 McAspGblCtrlRx;
    ADT_UInt32 McAspGblCtrlTx;
} mcASPCfgParms_t;

#endif
