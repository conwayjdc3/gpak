#ifndef GPAK_IPC_H
#define GPAK_IPC_H

#include <stdint.h>
#include <xdc/std.h>

#include <ti/ipc/Ipc.h>
#include <ti/ipc/MessageQ.h>

#define GPAK_MASTER_QUEUE_NAME     "master_queue"
#define MAX_NUM_CORES              8

#define GPAK_IPC_MSG_HEAP_NAME     "gpak_message_heap"
#define GPAK_IPC_MSG_HEAPID        0
#define GPAK_IPC_MAX_MSGLENI8      128

#define GET_SLAVE_QUEUE_NAME(str, core_id) sprintf(str,"core%d_queue", core_id)

typedef struct msg_info_t {
    int            code;
    int            ack;   // 1 == must send ack
    uint8_t *      msg;
    uint32_t       msglenI8;
} msg_info_t;

/* This structure holds the IPC message. The pointers should point to global/shared
 * memory space (like SL2RAM or DDR2).
 * The core_id will always slave core id.
 * In the response message the info.flag will will indicate if there is any failure.
 */
typedef struct ipc_message {
    MessageQ_MsgHeader  header;
    int                 core_id;
    msg_info_t          info;
} ipc_message_t;

extern int gpak_msgQ_init(int number_of_cores);
extern int gpak_msgQ_send (int code, uint8_t *msgBuf, int msglenI8, int number_of_cores, int ack_required);

#endif /* GPAK_IPC_H */
