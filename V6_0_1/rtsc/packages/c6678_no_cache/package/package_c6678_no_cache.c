/*
 *  Do not modify this file; it is automatically 
 *  generated and any modifications will be overwritten.
 *
 * @(#) xdc-K04
 */

#include <xdc/std.h>

__FAR__ char c6678_no_cache__dummy__;

#define __xdc_PKGVERS null
#define __xdc_PKGNAME c6678_no_cache
#define __xdc_PKGPREFIX c6678_no_cache_

#ifdef __xdc_bld_pkg_c__
#define __stringify(a) #a
#define __local_include(a) __stringify(a)
#include __local_include(__xdc_bld_pkg_c__)
#endif

