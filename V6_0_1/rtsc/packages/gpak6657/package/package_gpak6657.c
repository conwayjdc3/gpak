/*
 *  Do not modify this file; it is automatically 
 *  generated and any modifications will be overwritten.
 *
 * @(#) xdc-F07
 */

#include <xdc/std.h>

__FAR__ char gpak6657__dummy__;

#define __xdc_PKGVERS null
#define __xdc_PKGNAME gpak6657
#define __xdc_PKGPREFIX gpak6657_

#ifdef __xdc_bld_pkg_c__
#define __stringify(a) #a
#define __local_include(a) __stringify(a)
#include __local_include(__xdc_bld_pkg_c__)
#endif

