/*
 * etsi.h
 *
 *  Created on: Jan 28, 2010
 *      Author: skurtz
 */

#ifndef ETSI_ADT_H
#define ETSI_ADT_H
#include <common/include/adt_typedef_user.h>
extern ADT_Int16 Overflow, Carry;

#include <common/private/include/etsi_adt_c.h>

#endif	//ETSI_ADT_H

