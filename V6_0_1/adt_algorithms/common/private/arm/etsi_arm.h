/*
 * etsi.h
 *
 *  Created on: Jan 28, 2010
 *      Author: skurtz
 */

#ifndef ETSI_ARM_H_
#define ETSI_ARM_H_

#include "adt_typedef.h"
// Place all overrides here before the include to the generic etsi_adt_c.h

#if defined(__ARM_ARCH_7A__) || \
	defined(__ARM_ARCH_5E__)
#define OVERRIDE_ADD
#define OVERRIDE_SUB
#define OVERRIDE_L_ADD
#define OVERRIDE_L_SUB
#define OVERRIDE_L_MULT
#define OVERRIDE_L_MAC
#define OVERRIDE_L_MSU

#endif


#if defined (__ARM_ARCH_5E__) || \
	defined (__ARM_ARCH_7A__) || \
	defined (__ARM_ARCH_7M__)

#define OVERRIDE_L_SHL
#define OVERRIDE_L_SHR
#define OVERRIDE_NORM_L
#define OVERRIDE_NORM_S

#endif


#ifndef MAX_32
#define MAX_32 (int)0x7fffffffL
#endif
#ifndef MIN_32
#define MIN_32 (int)0x80000000L
#endif


#ifdef __cplusplus 
extern "C" { 
#endif 

#ifdef __APPLE_CC__
#define asm __asm__
#endif


 #ifdef ARM_CPU_FLAGS
/* 
 * This global variable needs to exist somewhere in the compiled 
 * program in order for the flag-using functions to work. You can 
 * either include the declaration 
 *  
 *   armdsp_flagdata_union armdsp_flagdata; 
 *  
 * in at least one of your source files (that includes this 
 * header), or compile in "dspfns.c" from the examples\dsp 
 * directory. 
 */ 
typedef union { 
    unsigned int armdsp_flags_word; 
    struct { 
   #ifdef __BIG_ENDIAN 
        Flag armdsp_n:1, armdsp_z:1, armdsp_c:1, armdsp_v:1, armdsp_q:1, armdsp_unused:27; 
   #else 
        Flag armdsp_unused:27, armdsp_q:1, armdsp_v:1, armdsp_c:1, armdsp_z:1, armdsp_n:1; 
   #endif 
    } armdsp_bitfields; 
} armdsp_flagdata_union; 
extern armdsp_flagdata_union armdsp_flagdata; 
#define Carry ( armdsp_flagdata.armdsp_bitfields.armdsp_c ) 
#define Overflow ( armdsp_flagdata.armdsp_bitfields.armdsp_q ) 
 #else // !ARM_CPU_FLAGS
extern Word16 Overflow,Carry;
 #endif // ARM_CPU_FLAGS
 
#ifdef ANDROID // added by Craig, 4/20/2012

   //#ifndef __ARM_ARCH_7A__

      #undef OVERRIDE_ADD
      #undef OVERRIDE_SUB
      #undef OVERRIDE_L_SHL
      #undef OVERRIDE_L_SHR
      #undef OVERRIDE_L_ADD
      #undef OVERRIDE_L_SUB
      #undef OVERRIDE_L_MULT
      #undef OVERRIDE_L_MAC
      #undef OVERRIDE_L_MSU
      #undef OVERRIDE_NORM_L
      #undef OVERRIDE_NORM_S

   //#endif // __ARM_ARCH_7A__

   //#undef OVERRIDE_L_MAC // crashes here..
   //#undef OVERRIDE_L_ADD // crashes here..
   //#undef OVERRIDE_L_SUB // ?

#endif // ANDROID

#ifdef OVERRIDE_ADD
INLINE Word16 add(Word16 x, Word16 y) 
{ 
	Word32 xs, ys, rs;
#ifdef __GNUC__

	asm (
		"mov %[b], %[a], lsl #16"
		: [b] "=r" (xs)
		: [a] "r" (x)
	);
	asm (
		"mov %[b], %[a], lsl #16"
		: [b] "=r" (ys)
		: [a] "r" (y)
	);
			
	asm (
			"qadd %[r], %[a], %[b]"
			: [r] "=r" (rs)
			: [a] "r" (xs), [b] "r" (ys)
	);
#else
    asm { 
        mov xs, x, lsl #16; 
        mov ys, y, lsl #16; 
        qadd rs, xs, ys; 
    }
#endif
	return (Word16) (rs >> 16); 

}
#endif


#ifdef OVERRIDE_SUB
INLINE Word16 sub(Word16 x, Word16 y) 
{ 
	Word32 xs, ys, rs;
#ifdef __GNUC__

	asm (
		"mov %[b], %[a], lsl #16"
		: [b] "=r" (xs)
		: [a] "r" (x)
	);
	asm (
		"mov %[b], %[a], lsl #16"
		: [b] "=r" (ys)
		: [a] "r" (y)
	);
			
	asm (
		"qsub %[r], %[a], %[b]"
		: [r] "=r" (rs)
		: [a] "r" (xs), [b] "r" (ys)
	);
#else
    asm { 
        mov xs, x, lsl #16; 
        mov ys, y, lsl #16; 
        qsub rs, xs, ys; 
    }
#endif
	return (Word16) (rs >> 16); 

}
#endif

/******************************************************************************/
/* _SHL - Saturating left shift.                                              */
/******************************************************************************/

#ifdef OVERRIDE_L_SHL
INLINE Word32 L_shl(Word32 x, Word16 shift) 
{ 
    int lz; 
    int absx; 
    if (shift <= 0)
	{
		if (shift < -31)
			shift = -31;
        return x >> (-shift); 
	}
    absx = (x < 0 ? -x : x); 
#ifdef __GNUC__
	asm (
			"clz %[s], %[a]"
			: [s] "=r" (lz)
			: [a] "r" (absx)
	);
#else
    asm { 
        clz lz, absx; 
    } 
#endif
	if (shift < lz || x == 0) 
        return x << shift; 
    if (x < 0) 
        return MIN_32; 
    else 
        return MAX_32; 
} 
#endif

/******************************************************************************/
/* _SHR - Saturating right shift.                                             */
/******************************************************************************/

#ifdef OVERRIDE_L_SHR
INLINE Word32 L_shr(Word32 x, Word16 shift) 
{ 
    int lz; 
    int absx; 
    if (shift >= 0)
	{
		if (shift > 32)
			shift = 32;
        return x >> shift; 
	}
    absx = (x < 0 ? -x : x); 
#ifdef __GNUC__
	asm (
			"clz %[s], %[a]"
			: [s] "=r" (lz)
			: [a] "r" (absx)
	);
#else
	__asm { 
        clz lz, absx; 
    } 
#endif
	if (-shift < lz || x == 0) 
        return x << (-shift); 
    if (x < 0) 
        return MIN_32; 
    else 
        return MAX_32; 
}
#endif



/******************************************************************************/
/* Macros for GSM ETSI math operations                                        */
/******************************************************************************/

#ifdef OVERRIDE_L_ADD
INLINE Word32 L_add (Word32 L_var1, Word32 L_var2)    /* Long add,        2 */
{
	Word32 LSum;
	asm (
			"qadd %[s], %[a], %[b]"
			: [s] "=r" (LSum)
			: [a] "r" (L_var1), [b] "r" (L_var2)
		);
	return(LSum);
}
#endif

#ifdef OVERRIDE_L_SUB
INLINE Word32 L_sub (Word32 L_var1, Word32 L_var2)    /* Long sub,        2 */
{
	Word32 LSum;
	asm (
			"qsub %[s], %[a], %[b]"
			: [s] "=r" (LSum)
			: [a] "r" (L_var1), [b] "r" (L_var2)
		);
	return(LSum);
}
#endif


#ifdef OVERRIDE_L_MULT
INLINE Word32 L_mult (Word16 L_var1, Word16 L_var2)    /* Long mult,        2 */
{
	Word32 LProduct;
#ifdef __GNUC__
	asm (
			"smulbb %[s], %[a], %[b]"
			: [s] "=r" (LProduct)
			: [a] "r" (L_var1), [b] "r" (L_var2)
		);
	asm (
			"qadd %[s], %[a], %[b]"
			: [s] "=r" (LProduct)
			: [a] "r" (LProduct), [b] "r" (LProduct)
		);
#else
	 __asm { 
        smulbb LProduct, x, y; 
        qadd LProduct, LProduct, LProduct; 
    } 
#endif

	return(LProduct);
}
#endif

//#define ALMOST_BIT_EXACT_LMAC_LMSU
/* Note regarding ALMOST_BIT_EXACT_LMAC_LMSU
 *   The intermediate saturation operation in the definition: 
 *  
 *    L_mac(-1, -0x8000, -0x8000) 
 *  
 * will give 0x7FFFFFFE and not 0x7FFFFFFF: 
 *    the unshifted product is:   0x40000000 
 *    shift left with saturation: 0x7FFFFFFF 
 *    add to -1 with saturation:  0x7FFFFFFE 
 */ 

#ifdef OVERRIDE_L_MAC
INLINE ADT_Int32 L_mac(ADT_Int32 Sum, ADT_Int16 x, ADT_Int16 y)
{
	ADT_Int32 Prod;
	ADT_Int32 TestValue = 0x40000000;
#ifdef __GNUC__
	asm (
		"smulbb %[p],%[a],%[b]"
		: [p] "=r" (Prod)
		: [a] "r" (x), [b] "r" (y)
		);
 #ifndef ALMOST_BIT_EXACT_LMAC_LMSU
	if (Prod != TestValue)
 #endif
		asm (
				"qdadd %[s],%[a],%[b]"
				: [s] "=r" (Sum)
				: [a] "r" (Sum), [b] "r" (Prod)
			);
 #ifndef ALMOST_BIT_EXACT_LMAC_LMSU
	else
		asm (
				"qadd %[s],%[a],%[b]"
				: [s] "=r" (Sum)
				: [a] "r" (MAX_32), [b] "r" (Sum)
			);
 #endif
#else // !defined __GNUC__
 #ifdef ALMOST_BIT_EXACT_LMAC_LMSU
	__asm {
		smulbb Prod, x, y; 
        qdadd Sum, Sum, Prod; 
	}
 #else
	__asm {
		smulbb Prod, x, y; 
	}
	if (Prod != TestValue)
		__asm {
			qdadd Sum, Sum, Prod; 
		}
	else
		__asm {
			qadd Sum, MAX_32, Sum

 #endif
#endif	//GNUC

	return(Sum);
}
#endif


#ifdef OVERRIDE_L_MSU
INLINE ADT_Int32 L_msu(ADT_Int32 Sum, ADT_Int16 x, ADT_Int16 y)
{
	ADT_Int32 Prod;
	ADT_Int32 TestValue = 0x40000000;
#ifdef __GNUC__
	asm (
		"smulbb %[p],%[a],%[b]"
		: [p] "=r" (Prod)
		: [a] "r" (x), [b] "r" (y)
		);
 #ifndef ALMOST_BIT_EXACT_LMAC_LMSU
	if (Prod != TestValue)
 #endif
		asm (
				"qdsub %[s],%[a],%[b]"
				: [s] "=r" (Sum)
				: [a] "r" (Sum), [b] "r" (Prod)
			);
 #ifndef ALMOST_BIT_EXACT_LMAC_LMSU
	else
		asm (
				"qsub %[s],%[a],%[b]"
				: [s] "=r" (Sum)
				: [a] "r" (MAX_32), [b] "r" (Sum)
			);
 #endif
#else // !defined __GNUC__
 #ifdef ALMOST_BIT_EXACT_LMAC_LMSU
	__asm {
		smulbb Prod, x, y; 
        qdadd Sum, Sum, Prod; 
	}
 #else
	__asm {
		smulbb Prod, x, y; 
	}
	if (Prod != TestValue)
		__asm {
			qdsub Sum, Sum, Prod; 
		}
	else
		__asm {
			qsub Sum, MAX_32, Sum

 #endif
#endif	//__GNUC__

	return(Sum);
}
#endif


#if 0 
#if defined(__arm__) && defined(__MULxy__) && defined(__QADDSUB__)
INLINE ADT_Int32 L_macNs(ADT_Int32 Sum, ADT_Int16 x, ADT_Int16 y)	// L_mac with Q30 result instead of Q31
{
	ADT_Int32 Prod;
	ADT_Int32 LSum;
	asm (
		"smulbb %[p],%[a],%[b]"
		: [p] "=r" (Prod)
		: [a] "r" (x), [b] "r" (y)
		);
	asm (
		"qadd %[s],%[a],%[b]"
		: [s] "=r" (LSum)
		: [a] "r" (Prod), [b] "r" (Sum)
		);
	return(LSum);
}
#endif
#endif

#ifdef OVERRIDE_NORM_L
INLINE Word16 norm_l(Word32 L_var1)    /* Compute exponent (number of leading zeros) */
{
	Word16 ReturnValue;
	unsigned long int AbsVar1;
	unsigned long int TestValue;
   if ((L_var1 == MIN_32) || (L_var1 == 0))
	   return 0;
   else
	   AbsVar1 = (unsigned long int) ((L_var1 < 0) ? -L_var1 : L_var1);
#ifdef __GNUC__
   asm (
			"clz %[s], %[a]"
			: [s] "=r" (ReturnValue)
			: [a] "r" (AbsVar1)
		);
#else
   __asm {
		clz ReturnValue,AbsVar1;
   }
#endif
	if (L_var1 > 0)
		return(ReturnValue-1);
	else
	{
		TestValue = AbsVar1 << (ReturnValue-1);
		if (TestValue < 0x20000000)
			return(ReturnValue+1);
		else if (TestValue == 0x40000000)
			return(ReturnValue);
		else
			return(ReturnValue-1);
	}
}
#endif


#ifdef OVERRIDE_NORM_S
INLINE Word16 norm_s(Word16 x) 
{ 
    int lz; 
    if (x == 0) 
        return 0;                      /* 0 is a special case */ 
    x = (Word16) (x ^ (x >> 15));      /* invert x if it's negative */ 
#ifdef __GNUC__
	asm (
		" clz %[y], %[x]"
		: [y] "=r" (lz)
		: [x] "r" (x)
	);
#else
	__asm { 
        clz lz, x; 
    } 
#endif
	return (Word16) (lz - 17); 
} 
#endif


#include "etsi_adt_c.h"

#endif /* ETSI_ARM_H_ */
