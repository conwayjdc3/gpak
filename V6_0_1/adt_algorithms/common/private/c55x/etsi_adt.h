/*
 * etsi.h
 *
 *  Created on: Jan 28, 2010
 *      Author: skurtz
 */

#ifndef ETSI_ADT_H
#define ETSI_ADT_H

#include <gsm.h>
#define INLINE inline
#ifndef OVERRIDE_SATURATE
INLINE ADT_Int16 saturate(ADT_Int32 x) 
{ 
    if (x > MAX_16)
	{
#ifdef ETSI_ENABLE_OVERFLOW
		Overflow = 1;
#endif
		x = MAX_16; 
	}
    else if (x < MIN_16) 
	{
#ifdef ETSI_ENABLE_OVERFLOW
		Overflow = 1;
#endif
		x = MIN_16; 
	}
    return (ADT_Int16) x; 
} 
#endif
#ifndef OVERRIDE_L_MULT0
#define L_mult0(x, y) ((ADT_Int32)x * (ADT_Int32)y)
#endif
#ifndef OVERRIDE_L_ADD_OV
INLINE ADT_Int32 L_add_ov (ADT_Int32 L_var1, ADT_Int32 L_var2, ADT_Int8 *pOverflow)
{
    ADT_Int32 L_var_out;

    L_var_out = L_var1 + L_var2;

    if (((L_var1 ^ L_var2) & MIN_32) == 0)
    {
        if ((L_var_out ^ L_var1) & MIN_32)
        {
            L_var_out = (L_var1 < 0) ? MIN_32 : MAX_32;
			*pOverflow = 1;
		}
    }
    return (L_var_out);
}
#endif
#ifndef OVERRIDE_L_SUB_OV
INLINE ADT_Int32 L_sub_ov (ADT_Int32 L_var1, ADT_Int32 L_var2, ADT_Int8 *pOverflow)
{
    ADT_Int32 L_var_out;

    L_var_out = L_var1 - L_var2;

    if (((L_var1 ^ L_var2) & MIN_32) != 0)
    {
        if ((L_var_out ^ L_var1) & MIN_32)
        {
            L_var_out = (L_var1 < 0L) ? MIN_32 : MAX_32;
            *pOverflow = 1;
		}
    }
    return (L_var_out);
}
#endif

#endif	//ETSI_ADT_H

