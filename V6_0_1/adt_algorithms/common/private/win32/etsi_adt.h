/*
 * etsi.h for win 32
 *
 *  Created on: Jan 28, 2010
 *      Author: skurtz
 */

#ifndef ETSI_ADT_H
#define ETSI_ADT_H
#include "adt_typedef.h"
extern Word16 Overflow,Carry;

#include "common/private/include/etsi_adt_c.h"

#endif	//ETSI_ADT_H

