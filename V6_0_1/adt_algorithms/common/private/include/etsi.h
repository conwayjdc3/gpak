/*
 * etsi.h
 *
 *  Created on: Oct 25, 2010
 *      Author: cwang
 */

#ifndef ETSI_H_
#define ETSI_H_

#ifdef REAL_VIEW
	#include <dspfns.h>	
	#ifdef G729AB_ADT_REALVIEW
	#include "g729ab_adt_realview.h"
	#endif
	
#elif defined (_TMS320C6X)
	#include "gsm.h"

#elif defined (__TMS320C55X__) || defined (_TMS320C5XX)
	#include "intrindefs.h"

#elif defined (USE_ADT_ETSI)
	#include "etsi_adt.h"

#else
	#include "basic_op.h"

#endif

	
#endif /* ETSI_H_ */
