/*
 Software Timebomb
 
 Purpose: A method to count frames whereby the counter does not
          increment sequentially. If the counter were sequential, 
          it would be relatively easy for somebody to defeat the 
          time-bomb by finding the counter and overwriting it. By
          using a random counter, it is harder to find the counter

 Instructions:
    1. Place an unsigned 32 bit number in your channel structure (R, for example)
    2. Include this file in the .c file where you do channel
       init and where you do frame processing.
    3. In your channel init routine, initialize the time bomb count as follows
       TB_init(TimeoutInSeconds, FramesPerSecond, &R);
    4. In your frame processing code, enable functionality as follows:
        if (!TB_check(&R))
        {
           do normal frame processing
        }
        else
        	return;

*/
#include <adt_typedef.h>

INLINE ADT_UInt32 TB_ADT_encode(ADT_UInt32 x)
{
	return((x*123) ^ 0xa53c1842);
}
INLINE ADT_UInt32 TB_ADT_decode(ADT_UInt32 x)
{
	return((x^0xa53c1842)/123);
}

INLINE int TB_ADT_check(ADT_UInt32 *Count)
{
	ADT_UInt32 DCount;
	DCount = TB_ADT_decode(*Count);
    if (DCount == 0)
	 	return(1);

	DCount -= 1;
	*Count = TB_ADT_encode(DCount);
 	return(0);
}
INLINE int TB_ADT_peek(ADT_UInt32 *Count)
{
	ADT_UInt32 DCount;
	DCount = TB_ADT_decode(*Count);
	 if (DCount == 0)
	 	return(1);
	 else
	 	return(0);
}

INLINE void TB_ADT_init(unsigned short TimeoutInSeconds, unsigned short FramesPerSecond, ADT_UInt32 *Count)
{
	ADT_UInt32 Ticks;
	
	Ticks = TimeoutInSeconds * FramesPerSecond;
	*Count = TB_ADT_encode(Ticks);
}

