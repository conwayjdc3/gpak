/*
 * etsi.h
 *
 *  Created on: Jan 28, 2010
 *      Author: skurtz
 */

#ifndef ETSI_ADT_C
#define ETSI_ADT_C


#ifdef __cplusplus 
extern "C" { 
#endif 

#include "adt_typedef.h"
#ifndef _TMS320C6X
#define _smpy(a, b)						L_mult((ADT_Int16)(a), (ADT_Int16)(b))
#define _smpyh(a, b)					L_mult((ADT_Int16)((a)>>16), (ADT_Int16)((b)>>16))
#define _smpyhl(a, b)					L_mult((ADT_Int16)((a)>>16), (ADT_Int16)(b))
#define _smpylh(a, b)					L_mult((ADT_Int16)(a), (ADT_Int16)((b)>>16))
#define _mpy(a, b)						(((ADT_Int16)(a)) * ((ADT_Int16)(b)))
#define _mpyh(a, b)						((ADT_Int16)((a)>>16) * (ADT_Int16)((b)>>16))
#define _mpyhl(a, b)					((ADT_Int16)((a)>>16) * (ADT_Int16)(b))
#define _mpylh(a, b)					((ADT_Int16)(a) * (ADT_Int16)((b)>>16))
#define _mpyus(a, b)					((ADT_UInt16)(a) * (ADT_Int16)(b))
#define _mpyhslu(a, b)					((ADT_Int16)((a)>>16) * (ADT_UInt16)(b))
#define _mpyluhs(a, b)					((ADT_UInt16)(a) * (ADT_Int16)((b)>>16))
#define _extu(a, b, c)					(((ADT_UInt32)((a)<<(b)))>>(c))
#endif


#ifndef OVERRIDE_EXTRACT_H
#define extract_h(a)   ((unsigned)(a)>>16)          /* extract upper 16 bits  */
#endif

#ifndef OVERRIDE_EXTRACT_L
#define extract_l(a)   ((ADT_Int16) ((a)&0xffff))               /* extract lower 16 bits  */
#endif

#ifndef OVERRIDE_ROUND
#define round(a)       ((ADT_Int16) extract_h(L_add((a),0x8000))) /* round                  */
#endif

#ifndef MAX_32
#define MAX_32 (int)0x7fffffffL
#endif
#ifndef MIN_32
#define MIN_32 (int)0x80000000L
#endif

#ifndef MAX_16
#define MAX_16 (short)0x7fff
#endif
#ifndef MIN_16
#define MIN_16 (short)0x8000
#endif


#ifndef OVERRIDE_ADD
#define add(a,b)       ((ADT_Int16) (L_add((a)<<16,(b)<<16)>>16)) /* short sat add          */
#endif


#ifndef OVERRIDE_SUB
#define sub(a,b)       ((ADT_Int16) (L_sub((a)<<16,(b)<<16)>>16)) /* short sat subtract     */
#endif

#ifndef OVERRIDE_SATURATE
INLINE ADT_Int16 saturate(ADT_Int32 x) 
{ 
    if (x > MAX_16)
	{
#ifdef ETSI_ENABLE_OVERFLOW
		Overflow = 1;
#endif
		x = MAX_16; 
	}
    else if (x < MIN_16) 
	{
#ifdef ETSI_ENABLE_OVERFLOW
		Overflow = 1;
#endif
		x = MIN_16; 
	}
    return (ADT_Int16) x; 
} 
INLINE ADT_Int16 saturate_ov(ADT_Int32 x, ADT_UInt8 *pOverflow) 
{ 
    if (x > MAX_16)
	{
		*pOverflow = 1;
		x = MAX_16; 
	}
    else if (x < MIN_16) 
	{
		*pOverflow = 1;
		x = MIN_16; 
	}
    return (ADT_Int16) x; 
} 

#endif
INLINE short _shl16a(short var1, short var2)
{
   int result;

   if (var1 ==   0)  return 0;
   if (var2 >=  15)  return (var1 < 0) ? MIN_16 : MAX_16;
   if (var2 <= -15)  return (var1 < 0) ? -1     : 0;
//   if (var2 <    0)  return _shr16a(var1, -var2);

   result = var1 << var2;

   if (result != (short)result)  return (var1 < 0) ? MIN_16 : MAX_16;
   return result;
}

INLINE short _shr16a(short var1, short var2)
{
   if (var1 ==   0)  return 0;
   if (var2 >=  15)  return (var1 < 0) ? -1 : 0;
   if (var2 <= -15)  return (var1 < 0) ? MIN_16 : MAX_16;
//   if (var2 <    0)  return _shl16a(var1, -var2);

   return var1 >> var2;
}
/******************************************************************************/
/* _SHR16 - Saturating right shift of a short int.                            */
/******************************************************************************/
#ifndef OVERRIDE_SHR
INLINE ADT_Int16 shr(ADT_Int16 x, ADT_Int16 shift) 
{ 
#if 1
	if (shift >= 0)
		return(_shr16a(x,shift));
	else
		return(_shl16a(x,-shift));
#else
	if (shift >= 0 || x == 0) 
	{
		if (shift > 16)
			shift = 16;
        return (ADT_Int16) (x >> shift); 
	}
	if (shift < -15) 
        shift = -16; 
    return saturate(x << (-shift)); 
#endif
} 
#endif
/******************************************************************************/
/* _SHL16 - Saturating left shift of a short int.                             */
/******************************************************************************/
#ifndef OVERRIDE_SHL
INLINE ADT_Int16 shl(ADT_Int16 x, ADT_Int16 shift) 
{ 
#if 1
	if (shift >= 0)
		return(_shl16a(x,shift));
	else
		return(_shr16a(x,-shift));
#else
    if (shift <= 0 || x == 0) 
	{
		if (shift < -16)
			shift = -16;	
        return (ADT_Int16) (x >> (-shift)); 
	}
    if (shift > 15) 
        shift = 16; 
    return saturate(x << shift); 
#endif
} 
#endif

/******************************************************************************/
/* _SHL - Saturating left shift.                                              */
/******************************************************************************/
INLINE int _shl32a(int var1, short var2)
{
   int tmp;

   if (var1 ==   0)  return 0;
   if (var2 >=  31)  return (var1 < 0) ? MIN_32 : MAX_32;
   if (var2 <= -31)  return (var1 < 0) ? -1     : 0;
//   if (var2 <    0)  return _shr(var1, -var2);

   tmp = var1 >> (31 - var2);
   if (var1 < 0) { if (tmp != -1)  return MIN_32; }
   else            if (tmp !=  0)  return MAX_32;

   return var1 << var2;
}

/******************************************************************************/
/* _SHR - Saturating right shift.                                             */
/******************************************************************************/
INLINE int _shr32a(int var1, short var2)
{
   if (var1 ==   0)  return 0;
   if (var2 >=  31)  return (var1 < 0) ? -1 : 0;
   if (var2 <= -31)  return (var1 < 0) ? MIN_32 : MAX_32;
//   if (var2 <    0)  return _shl(var1, -var2);

   return var1 >> var2;
}

/******************************************************************************/
/* _SHL - Saturating left shift.                                              */
/******************************************************************************/
#ifndef OVERRIDE_L_SHL
INLINE ADT_Int32 L_shl(ADT_Int32 var1, ADT_Int16 var2)
{
   int tmp;

   if (var1 ==   0)  return 0;
   if (var2 >=  31)  return (var1 < 0) ? MIN_32 : MAX_32;
   if (var2 <= -31)  return (var1 < 0) ? -1     : 0;
   if (var2 <    0)  return (ADT_Int32) _shr32a((int)var1,(short) -var2);

   tmp = var1 >> (31 - var2);
   if (var1 < 0) { if (tmp != -1)  return MIN_32; }
   else            if (tmp !=  0)  return MAX_32;

   return var1 << var2;
}
#endif

/******************************************************************************/
/* _SHR - Saturating right shift.                                             */
/******************************************************************************/
#ifndef OVERRIDE_L_SHR
INLINE ADT_Int32 L_shr(ADT_Int32 var1, ADT_Int16 var2)
{
   if (var1 ==   0)  return 0;
   if (var2 >=  31)  return (var1 < 0) ? -1 : 0;
   if (var2 <= -31)  return (var1 < 0) ? MIN_32 : MAX_32;
   if (var2 <    0)  return _shl32a((int)var1,(short) -var2);

   return var1 >> var2;
}
#endif


/******************************************************************************/
/* _ABS16_S - Saturating absolute value of a short int.                       */
/******************************************************************************/
#ifndef OVERRIDE_ABS_S
INLINE short abs_s(short var1)
{
	if (var1 >= 0) return var1;
	if (var1 == MIN_16)  return MAX_16;
	else return (short) -var1;
}
#endif

/******************************************************************************/
/* _ABS_S - Saturating absolute value.                                        */
/******************************************************************************/
#ifndef OVERRIDE_L_ABS
INLINE ADT_Int32 L_abs(ADT_Int32 var1)
{
	if (var1 >= 0) return var1;
	else if (var1 != MIN_32) return -var1;
	else return MAX_32;
}
#endif

/******************************************************************************/
/* Macros for GSM ETSI math operations                                        */
/******************************************************************************/


#ifndef OVERRIDE_L_ADD
INLINE ADT_Int32 L_add (ADT_Int32 L_var1, ADT_Int32 L_var2)
{
    ADT_Int32 L_var_out;

    L_var_out = L_var1 + L_var2;

    if (((L_var1 ^ L_var2) & MIN_32) == 0)
    {
        if ((L_var_out ^ L_var1) & MIN_32)
        {
            L_var_out = (L_var1 < 0) ? MIN_32 : MAX_32;
#ifdef ETSI_ENABLE_OVERFLOW
            Overflow = 1;
#endif
		}
    }
    return (L_var_out);
}
#endif
#ifndef OVERRIDE_L_ADD_OV
INLINE ADT_Int32 L_add_ov (ADT_Int32 L_var1, ADT_Int32 L_var2, ADT_Int8 *pOverflow)
{
    ADT_Int32 L_var_out;

    L_var_out = L_var1 + L_var2;

    if (((L_var1 ^ L_var2) & MIN_32) == 0)
    {
        if ((L_var_out ^ L_var1) & MIN_32)
        {
            L_var_out = (L_var1 < 0) ? MIN_32 : MAX_32;
			*pOverflow = 1;
		}
    }
    return (L_var_out);
}
#endif


#ifndef OVERRIDE_L_SUB
INLINE ADT_Int32 L_sub (ADT_Int32 L_var1, ADT_Int32 L_var2)
{
    ADT_Int32 L_var_out;

    L_var_out = L_var1 - L_var2;

    if (((L_var1 ^ L_var2) & MIN_32) != 0)
    {
        if ((L_var_out ^ L_var1) & MIN_32)
        {
            L_var_out = (L_var1 < 0L) ? MIN_32 : MAX_32;
#ifdef ETSI_ENABLE_OVERFLOW
            Overflow = 1;
#endif
		}
    }
    return (L_var_out);
}
#endif
#ifndef OVERRIDE_L_SUB_OV
INLINE ADT_Int32 L_sub_ov (ADT_Int32 L_var1, ADT_Int32 L_var2, ADT_Int8 *pOverflow)
{
    ADT_Int32 L_var_out;

    L_var_out = L_var1 - L_var2;

    if (((L_var1 ^ L_var2) & MIN_32) != 0)
    {
        if ((L_var_out ^ L_var1) & MIN_32)
        {
            L_var_out = (L_var1 < 0L) ? MIN_32 : MAX_32;
            *pOverflow = 1;
		}
    }
    return (L_var_out);
}
#endif

#ifndef OVERRIDE_L_NEGATE
INLINE ADT_Int32 L_negate(ADT_Int32 a)
{
	if (a != MIN_32)
		return(-a);
	else
		return(MAX_32);
}
#endif

#ifndef OVERRIDE_L_DEPOSIT_H
#define L_deposit_h(a) (((ADT_Int32) a)<<16)                    /* put short in upper 16  */
#endif
#ifndef OVERRIDE_L_DEPOSIT_L
#define L_deposit_l(a) ((ADT_Int32)(a))                   /* put short in lower 16  */
#endif

//#define L_abs(a)        abs_s(a)


#ifndef OVERRIDE_L_MULT
INLINE ADT_Int32 L_mult (ADT_Int16 var1, ADT_Int16 var2)
{
    ADT_Int32 L_var_out;

    L_var_out = (ADT_Int32) var1 *(ADT_Int32) var2;

    if (L_var_out != (ADT_Int32) 0x40000000L)
    {
        L_var_out *= 2;
    }
    else
    {
#ifdef ETSI_ENABLE_OVERFLOW
       Overflow = 1;
#endif
	   L_var_out = MAX_32;
    }

    return (L_var_out);
}
INLINE ADT_Int32 L_mult_ov (ADT_Int16 var1, ADT_Int16 var2, ADT_Int8 *pOverflow)
{
    ADT_Int32 L_var_out;

    L_var_out = (ADT_Int32) var1 *(ADT_Int32) var2;

    if (L_var_out != (ADT_Int32) 0x40000000L)
    {
        L_var_out *= 2;
    }
    else
    {
		*pOverflow = 1;
		L_var_out = MAX_32;
    }

    return (L_var_out);
}

#endif
#ifndef OVERRIDE_LMULT0
#define L_mult0(x, y) ((ADT_Int32)x * (ADT_Int32)y)
#endif

#ifndef OVERRIDE_L_MAC0
#define L_mac0(s, x, y) L_add(s, L_mult0(x,y) )
#endif
#ifndef OVERRIDE_L_MSU0
#define L_msu0(s, x, y) L_sub(s, L_mult0(x,y) )
#endif

#ifndef OVERRIDE_L_MAC
INLINE ADT_Int32 L_mac (ADT_Int32 L_var3, ADT_Int16 var1, ADT_Int16 var2)
{
    ADT_Int32 L_var_out;
    ADT_Int32 L_product;

    L_product = L_mult (var1, var2);
    L_var_out = L_add (L_var3, L_product);
    return (L_var_out);
}
#endif

#ifndef OVERRIDE_L_MSU
INLINE ADT_Int32 L_msu (ADT_Int32 L_var3, ADT_Int16 var1, ADT_Int16 var2)
{
    ADT_Int32 L_var_out;
    ADT_Int32 L_product;

    L_product = L_mult (var1, var2);
    L_var_out = L_sub (L_var3, L_product);
    return (L_var_out);
}
#endif


#ifndef OVERRIDE_L_ADD_C
#if defined(ETSI_ENABLE_OVERFLOW) && defined(ETSI_ENABLE_CARRY)
INLINE ADT_Int32 L_add_c (ADT_Int32 L_var1, ADT_Int32 L_var2)
{
    ADT_Int32 L_var_out;
    ADT_Int32 L_test;
    Flag carry_int = 0;

    L_var_out = L_var1 + L_var2 + Carry;

    L_test = L_var1 + L_var2;

    if ((L_var1 > 0) && (L_var2 > 0) && (L_test < 0))
    {
        Overflow = 1;
        carry_int = 0;
    }
    else
    {
        if ((L_var1 < 0) && (L_var2 < 0))
        {
            if (L_test >= 0)
	    {
                Overflow = 1;
                carry_int = 1;
	    }
            else
	    {
                Overflow = 0;
                carry_int = 1;
	    }
        }
        else
        {
            if (((L_var1 ^ L_var2) < 0) && (L_test >= 0))
            {
                Overflow = 0;
                carry_int = 1;
            }
            else
            {
                Overflow = 0;
                carry_int = 0;
            }
        }
    }

    if (Carry)
    {
        if (L_test == MAX_32)
        {
            Overflow = 1;
            Carry = carry_int;
        }
        else
        {
            if (L_test == (ADT_Int32) 0xFFFFFFFFL)
            {
                Carry = 1;
            }
            else
            {
                Carry = carry_int;
            }
        }
    }
    else
    {
        Carry = carry_int;
    }

    return (L_var_out);
}
#endif //OVERFLOW AND CARRY
#endif
#ifndef OVERRIDE_LMACNS
#ifdef ETSI_ENABLE_CARRY
INLINE ADT_Int32 L_macNs (ADT_Int32 L_var3, ADT_Int16 var1, ADT_Int16 var2)
{
    ADT_Int32 L_var_out;
    L_var_out = L_mult (var1, var2);
    L_var_out = L_add_c (L_var3, L_var_out);
    return (L_var_out);
}
#endif
#endif

#ifndef OVERRIDE_NORM_L
INLINE ADT_Int16 norm_l (ADT_Int32 L_var1)
{
    ADT_Int16 var_out;
    if (L_var1 == 0)
    {
        var_out = 0;
    }
    else
    {
        if (L_var1 == (ADT_Int32) 0xffffffffL)
        {
            var_out = 31;
        }
        else
        {
            if (L_var1 < 0)
            {
                L_var1 = ~L_var1;
            }
            for (var_out = 0; L_var1 < (ADT_Int32) 0x40000000L; var_out++)
            {
                L_var1 <<= 1;
            }
        }
    }

    return (var_out);
}
#endif

#ifndef OVERRIDE_NORM_S
INLINE ADT_Int16 norm_s (ADT_Int16 var1)
{
    ADT_Int16 var_out;
    if (var1 == 0)
    {
        var_out = 0;
    }
    else
    {
        if (var1 == -1)
        {
            var_out = 15;
        }
        else
        {
            if (var1 < 0)
            {
                var1 = ~var1;
            }
            for (var_out = 0; var1 < 0x4000; var_out++)
            {
                var1 <<= 1;
            }
        }
    }

    return (var_out);
}
#endif

#ifndef OVERRIDE_DIV_S
///ADT_Int16 G729AB_ADT_div_s(ADT_Int16 var1, ADT_Int16 var2)
INLINE ADT_Int16 div_s(ADT_Int16 var1, ADT_Int16 var2)
  {
 //  ADT_Int16 var_out = 0;
   ADT_Int32 quot=0;
//   ADT_Int32 L_num, L_denom;

   if ((var1 > var2) || (var1 < 0) || (var2 < 0))
     {
///      printf("Division Error var1=%d  var2=%d\n",var1,var2);
      return(0);
     }

   if (var2 == 0)
     {
///      printf("Division by 0, Fatal error \n");
      return(0);
     }

   if (var1 == 0)
     {
		return(0);
     }
   else
#if 1
	quot = 0x8000 * var1;
	quot /= var2;
	if (quot > MAX_16)
		return(MAX_16);
	else
		return((ADT_Int16) quot);
#else
   {
      if (var1 == var2)
        {
         var_out = MAX_16;
        }
      else
        {
         L_num = L_deposit_l(var1);
         L_denom = L_deposit_l(var2);

         for(iteration=0;iteration<15;iteration++)
           {
            var_out <<=1;
            L_num <<= 1;

            if (L_num >= L_denom)
              {
               L_num = L_sub(L_num,L_denom);
               var_out = add(var_out,1);
              }
           }
        }
     }
   return(var_out);
#endif
  }
#endif	//OVERRIDE_DIV_S

// Basic operations that require overflow support - used by Adaptive Digital for G.729 and possibly other algorithms

#ifndef OVERRIDE_L_ADDOVF
#ifdef ETSI_ENABLE_OVERFLOW
INLINE ADT_Int32 L_addOVF(ADT_Int32 L_var1, ADT_Int32 L_var2)
{
   ADT_Int32 L_var_out;
   long int Sum40;
   long int ZTest;

   L_var_out = L_add(L_var1, L_var2);
   Sum40 = (long int) L_var1 + (long int) L_var2;
   ZTest = Sum40 ^ L_var_out;
   if (ZTest != 0)
	Overflow = 1;
   return(L_var_out);
}
#endif //ETSI_ENABLE_OVERFLOW
#endif

#ifndef OVERRIDE_L_SUBOVF
#ifdef ETSI_ENABLE_OVERFLOW
INLINE ADT_Int32 L_subOVF(ADT_Int32 L_var1, ADT_Int32 L_var2)
{
   ADT_Int32 L_var_out;
   long int Dif40;
   long int ZTest;

   L_var_out = L_sub(L_var1, L_var2);
   Dif40 = (long int) L_var1 - (long int) L_var2;
   ZTest = Dif40 ^ L_var_out;
   if (ZTest != 0)
	Overflow = 1;
   return(L_var_out);
}
#endif
#endif
#ifndef OVERRIDE_L_MULTOVF
#ifdef ETSI_ENABLE_OVERFLOW
INLINE ADT_Int32 L_multOVF(ADT_Int16 var1, ADT_Int16 var2)
  {
   ADT_Int32 L_var_out;
   ADT_Int32 W32;

   W32 = (ADT_Int32)var1 * (ADT_Int32)var2;
   L_var_out = _smpy(var1, var2);
   if (W32 == (ADT_Int32)0x40000000L)
	   Overflow = 1;

   return(L_var_out);
  }
#endif
#endif

#ifndef OVERRIDE_L_MACOVF
#ifdef ETSI_ENABLE_OVERFLOW
INLINE ADT_Int32 L_macOVF(ADT_Int32 L_var3, ADT_Int16 var1, ADT_Int16 var2)
  {
   ADT_Int32 L_var_out;
   ADT_Int32 L_produit;

   L_produit = L_multOVF(var1, var2);
   L_var_out = L_addOVF(L_var3, L_produit);
   return(L_var_out);
  }

#endif
#endif

#ifndef OVERRIDE_L_MSUOVF
#ifdef ETSI_ENABLE_OVERFLOW
INLINE ADT_Int32 L_msuOVF(ADT_Int32 L_var3, ADT_Int16 var1, ADT_Int16 var2)
  {
   ADT_Int32 L_var_out;
   ADT_Int32 L_produit;

   L_produit = L_multOVF(var1, var2);
   L_var_out = L_subOVF(L_var3, L_produit);
   return(L_var_out);
  }
#endif
#endif

#ifndef OVERRIDE_ADDOVF
#ifdef ETSI_ENABLE_OVERFLOW

INLINE ADT_Int16 addOVF(ADT_Int16 var1, ADT_Int16 var2)
  {
   ADT_Int16 var_out;
   ADT_Int32 L_somme;

   L_somme = (L_add(((ADT_Int32) var1) << 16, ((ADT_Int32) var2) << 16) >> 16);
   var_out = var1 + var2;
   Overflow = 0;
   if (var_out != L_somme)
   {
	Overflow = 1;
	var_out = (ADT_Int16) L_somme;
   }
   return(var_out);
  }
#endif
#endif

#ifndef OVERRIDE_SUBOVF
#ifdef ETSI_ENABLE_OVERFLOW
INLINE ADT_Int16 subOVF(ADT_Int16 var1, ADT_Int16 var2)
  {
   ADT_Int16 var_out;
   ADT_Int32 L_somme;

   L_somme = (L_sub(((ADT_Int32) var1) << 16, ((ADT_Int32) var2) << 16) >> 16);
   var_out = var1 - var2;
   Overflow = 0;
   if (var_out != L_somme)
   {
	Overflow = 1;
	var_out = (ADT_Int16) L_somme;
   }
   return(var_out);
  }
#endif
#endif

#ifndef OVERRIDE_ROUNDOVF
#ifdef ETSI_ENABLE_OVERFLOW
INLINE ADT_Int16 roundOVF(ADT_Int32 L_var1)
  {
   ADT_Int16 var_out;
   ADT_Int32 L_arrondi;

   L_arrondi = L_addOVF(L_var1, (ADT_Int32)0x00008000);
   var_out = extract_h(L_arrondi);
   return(var_out);
  }

#endif
#endif

#ifndef OVERRIDE_L_SHLOVF
#ifdef ETSI_ENABLE_OVERFLOW
INLINE ADT_Int32 L_shlOVF(ADT_Int32 L_var1, ADT_Int16 var2)
{
	ADT_Int32 a, b;
	a = L_shl(L_var1, var2);
	if (var2 >= 0)
		b = L_var1 << var2;
	else
		b = L_var1 >> (-var2);
	if (a != b)
		Overflow = 1;
	return(a);
}
#endif
#endif

#ifndef OVERRIDE_L_SHROVF
#ifdef ETSI_ENABLE_OVERFLOW
INLINE ADT_Int32 L_shrOVF(ADT_Int32 L_var1, ADT_Int16 var2)
{
	ADT_Int32 a, b;
	a = L_shr(L_var1, var2);
	if (var2 >= 0)
		b = L_var1 >> var2;
	else
		b = L_var1 << (-var2);
	if (a != b)
		Overflow = 1;
	return(a);
}
#endif
#endif

/*___________________________________________________________________________
 |                                                                           |
 |   Function Name : L_sub_c                                                 |
 |                                                                           |
 |   Purpose :                                                               |
 |                                                                           |
 |   Performs 32 bits subtraction of the two 32 bits variables with carry    |
 |   (borrow) : L_var1-L_var2-C. No saturation. Generate carry and Overflow  |
 |   values. The carry and overflow values are binary variables which can    |
 |   be tested and assigned values.                                          |
 |                                                                           |
 |   Complexity weight : 2                                                   |
 |                                                                           |
 |   Inputs :                                                                |
 |                                                                           |
 |    L_var1   32 bit long signed integer (ADT_Int32) whose value falls in the  |
 |             range : 0x8000 0000 <= L_var3 <= 0x7fff ffff.                 |
 |                                                                           |
 |    L_var2   32 bit long signed integer (ADT_Int32) whose value falls in the  |
 |             range : 0x8000 0000 <= L_var3 <= 0x7fff ffff.                 |
 |                                                                           |
 |   Outputs :                                                               |
 |                                                                           |
 |    none                                                                   |
 |                                                                           |
 |   Return Value :                                                          |
 |                                                                           |
 |    L_var_out                                                              |
 |             32 bit long signed integer (ADT_Int32) whose value falls in the  |
 |             range : 0x8000 0000 <= L_var_out <= 0x7fff ffff.              |
 |                                                                           |
 |   Caution :                                                               |
 |                                                                           |
 |    In some cases the Carry flag has to be cleared or set before using op- |
 |    rators which take into account its value.                              |
 |___________________________________________________________________________|
*/

#ifndef OVERRIDE_L_SUB_C
#if defined (ETSI_ENABLE_OVERFLOW) && defined(ETSI_ENABLE_CARRY)
INLINE ADT_Int32 L_sub_c(ADT_Int32 L_var1, ADT_Int32 L_var2)
  {
   ADT_Int32 L_var_out;
   ADT_Int32 L_test;
   Flag carry_int = 0;

   if (Carry)
     {
      Carry = 0;
      if (L_var2 != MIN_32)
        {
         L_var_out = L_add_c(L_var1,-L_var2);
        }
      else
        {
         L_var_out = L_var1 - L_var2;
         if (L_var1 > 0L)
           {
            Overflow = 1;
            Carry = 0;
           }
        }
     }
   else
     {
      L_var_out = L_var1 - L_var2 - (ADT_Int32)0X00000001;
      L_test = L_var1 - L_var2;

      if ((L_test < 0) && (L_var1 > 0) && (L_var2 < 0))
        {
         Overflow = 1;
         carry_int = 0;
        }
      else if ((L_test > 0) && (L_var1 < 0) && (L_var2 > 0))
        {
         Overflow = 1;
         carry_int = 1;
        }
      else if ((L_test > 0) && ((L_var1 ^ L_var2) > 0))
        {
         Overflow = 0;
         carry_int = 1;
        }


      if (L_test == MIN_32)
        {
         Overflow = 1;
         Carry = carry_int;
        }
      else
        {
         Carry = carry_int;
        }
     }

   return(L_var_out);
  }
#endif //CARRY AND OVERFLOW
#endif

INLINE ADT_Int32 i_mult (ADT_Int16 a, ADT_Int16 b)
{
#ifdef ORIGINAL_G7231
   return a*b ;
#else
   ADT_Int32 register c=a*b;

   return saturate(c) ;
#endif
}



#ifndef OVERRIDE_L_MSU
#define L_msu(a,b,c)   (L_sub((a),L_mult(b,c)))     /* saturated mpy & sub    */
#endif

#if 0
#define L_shl(a,b)     (_shl((a),(b)))              /* sat left shift         */
#define L_shr(a,b)     (_shr((a),(b)))              /* sat right shift        */
#endif

#ifndef OVERRIDE_L_SHR_R
#define L_shr_r(a,b)   (L_shr((a),(b)) + ((b)>0 && (((a) & (1<<((b)-1))) != 0)))
#endif

#ifndef OVERRIDE_SHR_R
//#define shr_r(a,b)     (shr((a),(b)) + ((b)>0 && (((a) & (1<<((b)-1))) != 0)))
INLINE Word16 shr_r (Word16 var1, Word16 var2)
{
    Word16 var_out;

    if (var2 > 15)
    {
        var_out = 0;
    }
    else
    {
        var_out = shr (var1, var2);
#if (WMOPS)
        multiCounter[currCounter].shr--;
#endif

        if (var2 > 0)
        {
            if ((var1 & ((Word16) 1 << (var2 - 1))) != 0)
            {
                var_out++;
            }
        }
    }
#if (WMOPS)
    multiCounter[currCounter].shr_r++;
#endif
    return (var_out);
}
#endif

//#define abs_s(a)       _abs16_s(((a)<<16)>>16)      /* short absolute value   */
//#define add(a,b)       (L_add((a)<<16,(b)<<16)>>16) /* short sat add          */
//#define sub(a,b)       (L_sub((a)<<16,(b)<<16)>>16) /* short sat subtract     */
#ifndef OVERRIDE_EXTRACT_H
#define extract_h(a)   ((unsigned)(a)>>16)          /* extract upper 16 bits  */
#endif

//#define extract_l(a)   (((a)&0xffff))               /* extract lower 16 bits  */
//#define round(a)       extract_h(L_add((a),0x8000)) /* round                  */
#ifndef OVERRIDE_MAC_R
#define mac_r(a,b,c)   (round(L_mac((a),(b),(c))))  /* mac w/ rounding        */
#endif
#ifndef OVERRIDE_MSU_R
#define msu_r(a,b,c)   (round(L_msu((a),(b),(c))))  /* msu w/ rounding        */
#endif

#ifndef OVERRIDE_MULT_R
#define mult_r(a,b)    (round(L_mult((a),(b))))     /* sat mpy w/ round       */
#endif

#ifndef OVERRIDE_MULT
#define mult(a,b)      ((ADT_Int16) (L_mult((a),(b))>>16))      /* short sat mpy upper 16 */
#endif

//#define mult(a,b)      ((L_mult((a),(b))>>16))      /* short sat mpy upper 16 */

/* norm_l as defined in app. implementation differs from the compiler         */
/* implementation of CLZ by a off-by-one error. So where the compiler's       */
/* implementation returns the number of all leading zeroes, the app specific  */
/* implementation will return 1 less. Another difference between the two is   */
/* that the app. specific implementation will handle negative numbers,        */
/* whereas the compiler implementation won't. Commenting this out completely, */
/* and resorting to the app. specific implementation.                         */
/* #define norm_l(a)      (_norm(a))   */
/* #define norm_l(a)      (_norm(a)-1) */           /* return NORM of int     */

//#define norm_s(a)      (norm_l((a))-16)             /* return NORM of short   */
#ifndef OVERRIDE_NEGATE
#define negate(a)      ((ADT_Int16) (L_sub(0, ((a)<<16)) >> 16))  /* short sat negate       */
#endif

#ifdef __cplusplus 
}	//End extern "C"
#endif 

#endif /* ETSI_ARM_H_ */
