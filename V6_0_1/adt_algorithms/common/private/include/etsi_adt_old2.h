/*
 * etsi.h
 *
 *  Created on: Jan 28, 2010
 *      Author: skurtz
 */

#ifndef ETSI_ARM_H_
#define ETSI_ARM_H_

#ifdef __cplusplus 
extern "C" { 
#endif 

#include "adt_typedef.h"
#ifndef _TMS320C6X
#define _smpy(a, b)						L_mult((Word16)(a), (Word16)(b))
#define _smpyh(a, b)					L_mult((Word16)((a)>>16), (Word16)((b)>>16))
#define _smpyhl(a, b)					L_mult((Word16)((a)>>16), (Word16)(b))
#define _smpylh(a, b)					L_mult((Word16)(a), (Word16)((b)>>16))
#define _mpy(a, b)						(((Word16)(a)) * ((Word16)(b)))
#define _mpyh(a, b)						((Word16)((a)>>16) * (Word16)((b)>>16))
#define _mpyhl(a, b)					((Word16)((a)>>16) * (Word16)(b))
#define _mpylh(a, b)					((Word16)(a) * (Word16)((b)>>16))
#define _mpyus(a, b)					((UWord16)(a) * (Word16)(b))
#define _mpyhslu(a, b)					((Word16)((a)>>16) * (UWord16)(b))
#define _mpyluhs(a, b)					((UWord16)(a) * (Word16)((b)>>16))
#define _extu(a, b, c)					(((UWord32)((a)<<(b)))>>(c))
#endif


#define extract_h(a)   ((unsigned)(a)>>16)          /* extract upper 16 bits  */

#define extract_l(a)   ((ADT_Int16) ((a)&0xffff))               /* extract lower 16 bits  */
#define round(a)       ((ADT_Int16) extract_h(L_add((a),0x8000))) /* round                  */
#ifndef MAX_32
#define MAX_32 (int)0x7fffffffL
#endif
#ifndef MIN_32
#define MIN_32 (int)0x80000000L
#endif

#define MAX_16 (short)0x7fff
#define MIN_16 (short)0x8000

#ifdef __APPLE_CC__
#define asm __asm__
#endif
/* Define some constants that define which ARM instructions are available based upon the selected architecture 
   Note that these equations are not all inclusive. More conditions may be needed to expand the set of architectures 
   that can run certain instructions.
*/
#ifdef __arm__
 #if defined(__ARM_ARCH_5E__) || defined(__ARM_ARCH_7A__)
  #define __MULxy__
  #define __QADDSUB__
 #endif
 #if !defined(__ARM_ARCH_6K__)
  #define __CLZ__
 #endif
 #ifdef ARM_CPU_FLAGS
/* 
 * This global variable needs to exist somewhere in the compiled 
 * program in order for the flag-using functions to work. You can 
 * either include the declaration 
 *  
 *   armdsp_flagdata_union armdsp_flagdata; 
 *  
 * in at least one of your source files (that includes this 
 * header), or compile in "dspfns.c" from the examples\dsp 
 * directory. 
 */ 
typedef union { 
    unsigned int armdsp_flags_word; 
    struct { 
   #ifdef __BIG_ENDIAN 
        Flag armdsp_n:1, armdsp_z:1, armdsp_c:1, armdsp_v:1, armdsp_q:1, armdsp_unused:27; 
   #else 
        Flag armdsp_unused:27, armdsp_q:1, armdsp_v:1, armdsp_c:1, armdsp_z:1, armdsp_n:1; 
   #endif 
    } armdsp_bitfields; 
} armdsp_flagdata_union; 
extern armdsp_flagdata_union armdsp_flagdata; 
#define Carry ( armdsp_flagdata.armdsp_bitfields.armdsp_c ) 
#define Overflow ( armdsp_flagdata.armdsp_bitfields.armdsp_q ) 
 #else // !ARM_CPU_FLAGS
extern Word16 Overflow,Carry;
 #endif // ARM_CPU_FLAGS
 #else //!__arm
static Word16 Overflow,Carry;
#endif
extern volatile int G729AB_ADT_Overflow;


#if defined(__arm__) && defined(__QADDSUB__)
INLINE Word16 add(Word16 x, Word16 y) 
{ 
	Word32 xs, ys, rs;
#ifdef __GNUC__

	asm (
		"mov %[b], %[a], lsl #16"
		: [b] "=r" (xs)
		: [a] "r" (x)
	);
	asm (
		"mov %[b], %[a], lsl #16"
		: [b] "=r" (ys)
		: [a] "r" (y)
	);
			
	asm (
			"qadd %[r], %[a], %[b]"
			: [r] "=r" (rs)
			: [a] "r" (xs), [b] "r" (ys)
	);
#else
    asm { 
        mov xs, x, lsl #16; 
        mov ys, y, lsl #16; 
        qadd rs, xs, ys; 
    }
#endif
	return (Word16) (rs >> 16); 

}
#else
	
#define add(a,b)       ((ADT_Int16) (L_add((a)<<16,(b)<<16)>>16)) /* short sat add          */
#endif

#if defined(__arm__) && defined(__QADDSUB__)
INLINE Word16 sub(Word16 x, Word16 y) 
{ 
	Word32 xs, ys, rs;
#ifdef __GNUC__

	asm (
		"mov %[b], %[a], lsl #16"
		: [b] "=r" (xs)
		: [a] "r" (x)
	);
	asm (
		"mov %[b], %[a], lsl #16"
		: [b] "=r" (ys)
		: [a] "r" (y)
	);
			
	asm (
		"qsub %[r], %[a], %[b]"
		: [r] "=r" (rs)
		: [a] "r" (xs), [b] "r" (ys)
	);
#else
    asm { 
        mov xs, x, lsl #16; 
        mov ys, y, lsl #16; 
        qsub rs, xs, ys; 
    }
#endif
	return (Word16) (rs >> 16); 

}
#else
#define sub(a,b)       ((ADT_Int16) (L_sub((a)<<16,(b)<<16)>>16)) /* short sat subtract     */
#endif

INLINE Word16 saturate(Word32 x) 
{ 
    if (x > MAX_16) 
        Overflow = 1, x = MAX_16; 
    else if (x < MIN_16) 
        Overflow = 1, x = MIN_16; 
    return (Word16) x; 
} 
INLINE short _shl16a(short var1, short var2)
{
   int result;

   if (var1 ==   0)  return 0;
   if (var2 >=  15)  return (var1 < 0) ? MIN_16 : MAX_16;
   if (var2 <= -15)  return (var1 < 0) ? -1     : 0;
//   if (var2 <    0)  return _shr16a(var1, -var2);

   result = var1 << var2;

   if (result != (short)result)  return (var1 < 0) ? MIN_16 : MAX_16;
   return result;
}

INLINE short _shr16a(short var1, short var2)
{
   if (var1 ==   0)  return 0;
   if (var2 >=  15)  return (var1 < 0) ? -1 : 0;
   if (var2 <= -15)  return (var1 < 0) ? MIN_16 : MAX_16;
//   if (var2 <    0)  return _shl16a(var1, -var2);

   return var1 >> var2;
}
/******************************************************************************/
/* _SHR16 - Saturating right shift of a short int.                            */
/******************************************************************************/
INLINE Word16 shr(Word16 x, Word16 shift) 
{ 
    if (shift >= 0 || x == 0) 
        return (Word16) (x >> shift); 
    if (shift < -15) 
        shift = -16; 
    return saturate(x << (-shift)); 
} 
/******************************************************************************/
/* _SHL16 - Saturating left shift of a short int.                             */
/******************************************************************************/
INLINE Word16 shl(Word16 x, Word16 shift) 
{ 
    if (shift <= 0 || x == 0) 
        return (Word16) (x >> (-shift)); 
    if (shift > 15) 
        shift = 16; 
    return saturate(x << shift); 
} 

/******************************************************************************/
/* _SHL - Saturating left shift.                                              */
/******************************************************************************/
INLINE int _shl32a(int var1, short var2)
{
   int tmp;

   if (var1 ==   0)  return 0;
   if (var2 >=  31)  return (var1 < 0) ? MIN_32 : MAX_32;
   if (var2 <= -31)  return (var1 < 0) ? -1     : 0;
//   if (var2 <    0)  return _shr(var1, -var2);

   tmp = var1 >> (31 - var2);
   if (var1 < 0) { if (tmp != -1)  return MIN_32; }
   else            if (tmp !=  0)  return MAX_32;

   return var1 << var2;
}

/******************************************************************************/
/* _SHR - Saturating right shift.                                             */
/******************************************************************************/
INLINE int _shr32a(int var1, short var2)
{
   if (var1 ==   0)  return 0;
   if (var2 >=  31)  return (var1 < 0) ? -1 : 0;
   if (var2 <= -31)  return (var1 < 0) ? MIN_32 : MAX_32;
//   if (var2 <    0)  return _shl(var1, -var2);

   return var1 >> var2;
}

/******************************************************************************/
/* _SHL - Saturating left shift.                                              */
/******************************************************************************/
#if defined (__arm) && defined(__CLZ__)
INLINE Word32 L_shl(Word32 x, Word16 shift) 
{ 
    int lz; 
    int absx; 
    if (shift <= 0) 
        return x >> (-shift); 
    absx = (x < 0 ? -x : x); 
#ifdef __GNUC__
	asm (
			"clz %[s], %[a]"
			: [s] "=r" (lz)
			: [a] "r" (absx)
	);
#else
    asm { 
        clz lz, absx; 
    } 
#endif
	if (shift < lz || x == 0) 
        return x << shift; 
    if (x < 0) 
        return MIN_32; 
    else 
        return MAX_32; 
} 
#else
INLINE ADT_Int32 L_shl(ADT_Int32 var1, ADT_Int16 var2)
{
   int tmp;

   if (var1 ==   0)  return 0;
   if (var2 >=  31)  return (var1 < 0) ? MIN_32 : MAX_32;
   if (var2 <= -31)  return (var1 < 0) ? -1     : 0;
   if (var2 <    0)  return _shr32a(var1, -var2);

   tmp = var1 >> (31 - var2);
   if (var1 < 0) { if (tmp != -1)  return MIN_32; }
   else            if (tmp !=  0)  return MAX_32;

   return var1 << var2;
}
#endif

/******************************************************************************/
/* _SHR - Saturating right shift.                                             */
/******************************************************************************/
#if defined (__arm) && defined(__CLZ__)
INLINE Word32 L_shr(Word32 x, Word16 shift) 
{ 
    int lz; 
    int absx; 
    if (shift >= 0) 
        return x >> shift; 
    absx = (x < 0 ? -x : x); 
#ifdef __GNUC__
	asm (
			"clz %[s], %[a]"
			: [s] "=r" (lz)
			: [a] "r" (absx)
	);
#else
	__asm { 
        clz lz, absx; 
    } 
#endif
	if (-shift < lz || x == 0) 
        return x << (-shift); 
    if (x < 0) 
        return MIN_32; 
    else 
        return MAX_32; 
}
#else
INLINE ADT_Int32 L_shr(ADT_Int32 var1, ADT_Int16 var2)
{
   if (var1 ==   0)  return 0;
   if (var2 >=  31)  return (var1 < 0) ? -1 : 0;
   if (var2 <= -31)  return (var1 < 0) ? MIN_32 : MAX_32;
   if (var2 <    0)  return _shl32a(var1, -var2);

   return var1 >> var2;
}
#endif


/******************************************************************************/
/* _ABS16_S - Saturating absolute value of a short int.                       */
/******************************************************************************/
INLINE short abs_s(short var1)
{
	if (var1 >= 0) return var1;
	if (var1 == MIN_16)  return MAX_16;
	else return (short) -var1;
}

/******************************************************************************/
/* _ABS_S - Saturating absolute value.                                        */
/******************************************************************************/
INLINE ADT_Int32 L_abs(ADT_Int32 var1)
{
	if (var1 >= 0) return var1;
	else if (var1 != MIN_32) return -var1;
	else return MAX_32;
}
/******************************************************************************/
/* Macros for GSM ETSI math operations                                        */
/******************************************************************************/

#if defined(__arm__) && defined(__QADDSUB__)
INLINE Word32 L_add (Word32 L_var1, Word32 L_var2)    /* Long add,        2 */
{
	Word32 LSum;
	asm (
			"qadd %[s], %[a], %[b]"
			: [s] "=r" (LSum)
			: [a] "r" (L_var1), [b] "r" (L_var2)
		);
	return(LSum);
}
#else
INLINE Word32 L_add (Word32 L_var1, Word32 L_var2)
{
    Word32 L_var_out;

    L_var_out = L_var1 + L_var2;

    if (((L_var1 ^ L_var2) & MIN_32) == 0)
    {
        if ((L_var_out ^ L_var1) & MIN_32)
        {
            L_var_out = (L_var1 < 0) ? MIN_32 : MAX_32;
            Overflow = 1;
        }
    }
    return (L_var_out);
}

#endif
#if defined(__arm__) && defined(__QADDSUB__)
INLINE Word32 L_sub (Word32 L_var1, Word32 L_var2)    /* Long sub,        2 */
{
	Word32 LSum;
	asm (
			"qsub %[s], %[a], %[b]"
			: [s] "=r" (LSum)
			: [a] "r" (L_var1), [b] "r" (L_var2)
		);
	return(LSum);
}
#else
INLINE Word32 L_sub (Word32 L_var1, Word32 L_var2)
{
    Word32 L_var_out;

    L_var_out = L_var1 - L_var2;

    if (((L_var1 ^ L_var2) & MIN_32) != 0)
    {
        if ((L_var_out ^ L_var1) & MIN_32)
        {
            L_var_out = (L_var1 < 0L) ? MIN_32 : MAX_32;
            Overflow = 1;
        }
    }
    return (L_var_out);
}
#endif
INLINE Word32 L_negate(Word32 a)
{
	if (a != MIN_32)
		return(-a);
	else
		return(MAX_32);
}

#define L_deposit_h(a) (((Word32) a)<<16)                    /* put short in upper 16  */
#define L_deposit_l(a) ((Word32)(a))                   /* put short in lower 16  */
//#define L_abs(a)        abs_s(a)

#if defined(__arm__) && defined(__QADDSUB__) && defined(__MULxy__)
INLINE Word32 L_mult (Word16 L_var1, Word16 L_var2)    /* Long mult,        2 */
{
	Word32 LProduct;
#ifdef __GNUC__
	asm (
			"smulbb %[s], %[a], %[b]"
			: [s] "=r" (LProduct)
			: [a] "r" (L_var1), [b] "r" (L_var2)
		);
	asm (
			"qadd %[s], %[a], %[b]"
			: [s] "=r" (LProduct)
			: [a] "r" (LProduct), [b] "r" (LProduct)
		);
#else
	 __asm { 
        smulbb LProduct, x, y; 
        qadd LProduct, LProduct, LProduct; 
    } 
#endif

	return(LProduct);
}
#else
INLINE Word32 L_mult (Word16 var1, Word16 var2)
{
    Word32 L_var_out;

    L_var_out = (Word32) var1 *(Word32) var2;

    if (L_var_out != (Word32) 0x40000000L)
    {
        L_var_out *= 2;
    }
    else
    {
       Overflow = 1;
        L_var_out = MAX_32;
    }

    return (L_var_out);
}

#endif

#define L_mult0(x, y) ((Word32)x * (Word32)y)

#define L_mac0(s, x, y) L_add(s, L_mult0(x,y) )
#define L_msu0(s, x, y) L_sub(s, L_mult0(x,y) )

//#define ALMOST_BIT_EXACT_LMAC_LMSU
/* Note regarding ALMOST_BIT_EXACT_LMAC_LMSU
 *   The intermediate saturation operation in the definition: 
 *  
 *    L_mac(-1, -0x8000, -0x8000) 
 *  
 * will give 0x7FFFFFFE and not 0x7FFFFFFF: 
 *    the unshifted product is:   0x40000000 
 *    shift left with saturation: 0x7FFFFFFF 
 *    add to -1 with saturation:  0x7FFFFFFE 
 */ 
#if defined(__arm__) && defined(__MULxy__) && defined(__QADDSUB__)

INLINE ADT_Int32 L_mac(ADT_Int32 Sum, ADT_Int16 x, ADT_Int16 y)
{
	ADT_Int32 Prod;
	ADT_Int32 TestValue = 0x40000000;
#ifdef __GNUC__
	asm (
		"smulbb %[p],%[a],%[b]"
		: [p] "=r" (Prod)
		: [a] "r" (x), [b] "r" (y)
		);
 #ifndef ALMOST_BIT_EXACT_LMAC_LMSU
	if (Prod != TestValue)
 #endif
		asm (
				"qdadd %[s],%[a],%[b]"
				: [s] "=r" (Sum)
				: [a] "r" (Sum), [b] "r" (Prod)
			);
 #ifndef ALMOST_BIT_EXACT_LMAC_LMSU
	else
		asm (
				"qadd %[s],%[a],%[b]"
				: [s] "=r" (Sum)
				: [a] "r" (MAX_32), [b] "r" (Sum)
			);
 #endif
#else // !defined __GNUC__
 #ifdef ALMOST_BIT_EXACT_LMAC_LMSU
	__asm {
		smulbb Prod, x, y; 
        qdadd Sum, Sum, Prod; 
	}
 #else
	__asm {
		smulbb Prod, x, y; 
	}
	if (Prod != TestValue)
		__asm {
			qdadd Sum, Sum, Prod; 
		}
	else
		__asm {
			qadd Sum, MAX_32, Sum

 #endif
#endif	//GNUC

	return(Sum);
}
#else
INLINE Word32 L_mac (Word32 L_var3, Word16 var1, Word16 var2)
{
    Word32 L_var_out;
    Word32 L_product;

    L_product = L_mult (var1, var2);
    L_var_out = L_add (L_var3, L_product);
    return (L_var_out);
}
#endif

#if defined(__arm__) && defined(__MULxy__) && defined(__QADDSUB__)

INLINE ADT_Int32 L_msu(ADT_Int32 Sum, ADT_Int16 x, ADT_Int16 y)
{
	ADT_Int32 Prod;
	ADT_Int32 TestValue = 0x40000000;
#ifdef __GNUC__
	asm (
		"smulbb %[p],%[a],%[b]"
		: [p] "=r" (Prod)
		: [a] "r" (x), [b] "r" (y)
		);
 #ifndef ALMOST_BIT_EXACT_LMAC_LMSU
	if (Prod != TestValue)
 #endif
		asm (
				"qdsub %[s],%[a],%[b]"
				: [s] "=r" (Sum)
				: [a] "r" (Sum), [b] "r" (Prod)
			);
 #ifndef ALMOST_BIT_EXACT_LMAC_LMSU
	else
		asm (
				"qsub %[s],%[a],%[b]"
				: [s] "=r" (Sum)
				: [a] "r" (MAX_32), [b] "r" (Sum)
			);
 #endif
#else // !defined __GNUC__
 #ifdef ALMOST_BIT_EXACT_LMAC_LMSU
	__asm {
		smulbb Prod, x, y; 
        qdadd Sum, Sum, Prod; 
	}
 #else
	__asm {
		smulbb Prod, x, y; 
	}
	if (Prod != TestValue)
		__asm {
			qdsub Sum, Sum, Prod; 
		}
	else
		__asm {
			qsub Sum, MAX_32, Sum

 #endif
#endif	//__GNUC__

	return(Sum);
}
#else
INLINE Word32 L_msu (Word32 L_var3, Word16 var1, Word16 var2)
{
    Word32 L_var_out;
    Word32 L_product;

    L_product = L_mult (var1, var2);
    L_var_out = L_sub (L_var3, L_product);
    return (L_var_out);
}
#endif


#if 0// defined(__arm__) getting exceptions using msr

INLINE Word32 L_add_c(Word32 x, Word32 y) 
{ 
    Word32 result, flags; 
#ifdef __GNUC__
	asm (" nop");
	asm (" nop");
	asm (
		"msr cpsr_f,%[flagword]" 	: 	: [flagword] "r" (armdsp_flagdata.armdsp_flags_word)
	);
	asm (
		"adcs %[r], %[x], %[y]"		: [r] "=r" (result) : [x] "r" (x), [y] "r" (y)
	);
	asm (
		"mrs %[f], cpsr"			: [f] "=r" (flags) :
	);
	asm ("bvc .skip");
	asm (
		"orr %[fa], %[fb], #(0x08000000)" : [fa] "=r" (flags)	: [fb] "r" (flags)
	);
	asm (".skip: nop");
	asm (
		"mov %[fy], %[fx]"	: [fy] "=r" (armdsp_flagdata.armdsp_flags_word)	: [fx] "r" (flags)
	);
#else	//!__GNUC__
    __asm { 
        msr CPSR_f, armdsp_flagdata.armdsp_flags_word; 
        adcs result, x, y; 
        mrs flags, CPSR; 
        orrvs flags, flags, #0x08000000; // set Q if V is set 
        mov armdsp_flagdata.armdsp_flags_word, flags; 
    }
#endif
    return result; 
} 
#if 0	// Old way
INLINE Word32 L_add_c (Word32 L_var1, Word32 L_var2)    /* Long add without saturation,        2 */
{
	Word32 LSum;
	asm (
			"add %[s], %[a], %[b]"
			: [s] "=r" (LSum)
			: [a] "r" (L_var1), [b] "r" (L_var2)
		);
	return(LSum);
}
#endif
#else
INLINE Word32 L_add_c (Word32 L_var1, Word32 L_var2)
{
    Word32 L_var_out;
    Word32 L_test;
    Flag carry_int = 0;

    L_var_out = L_var1 + L_var2 + Carry;

    L_test = L_var1 + L_var2;

    if ((L_var1 > 0) && (L_var2 > 0) && (L_test < 0))
    {
        Overflow = 1;
        carry_int = 0;
    }
    else
    {
        if ((L_var1 < 0) && (L_var2 < 0))
        {
            if (L_test >= 0)
	    {
                Overflow = 1;
                carry_int = 1;
	    }
            else
	    {
                Overflow = 0;
                carry_int = 1;
	    }
        }
        else
        {
            if (((L_var1 ^ L_var2) < 0) && (L_test >= 0))
            {
                Overflow = 0;
                carry_int = 1;
            }
            else
            {
                Overflow = 0;
                carry_int = 0;
            }
        }
    }

    if (Carry)
    {
        if (L_test == MAX_32)
        {
            Overflow = 1;
            Carry = carry_int;
        }
        else
        {
            if (L_test == (Word32) 0xFFFFFFFFL)
            {
                Carry = 1;
            }
            else
            {
                Carry = carry_int;
            }
        }
    }
    else
    {
        Carry = carry_int;
    }

    return (L_var_out);
}

#endif
#if 1 // L_macNs doesn't do what I thought - revert to "C" code
INLINE ADT_Int32 L_macNs (ADT_Int32 L_var3, ADT_Int16 var1, ADT_Int16 var2)
{
    ADT_Int32 L_var_out;
    L_var_out = L_mult (var1, var2);
    L_var_out = L_add_c (L_var3, L_var_out);
    return (L_var_out);
}
#else
#if defined(__arm__) && defined(__MULxy__) && defined(__QADDSUB__)
INLINE ADT_Int32 L_macNs(ADT_Int32 Sum, ADT_Int16 x, ADT_Int16 y)	// L_mac with Q30 result instead of Q31
{
	ADT_Int32 Prod;
	ADT_Int32 LSum;
	asm (
		"smulbb %[p],%[a],%[b]"
		: [p] "=r" (Prod)
		: [a] "r" (x), [b] "r" (y)
		);
	asm (
		"qadd %[s],%[a],%[b]"
		: [s] "=r" (LSum)
		: [a] "r" (Prod), [b] "r" (Sum)
		);
	return(LSum);
}
#else
INLINE ADT_Int32 L_macNs (ADT_Int32 L_var3, ADT_Int16 var1, ADT_Int16 var2)
{
    ADT_Int32 L_var_out;
    L_var_out = L_mult (var1, var2);
    L_var_out = L_add_c (L_var3, L_var_out);
    return (L_var_out);
}
#endif //if 1

#endif

#if defined(__arm__) && defined(__CLZ__)
INLINE Word16 norm_l(Word32 L_var1)    /* Compute exponent (number of leading zeros) */
{
	Word16 ReturnValue;
	unsigned long int AbsVar1;
	unsigned long int TestValue;
   if ((L_var1 == MIN_32) || (L_var1 == 0))
	   return 0;
   else
	   AbsVar1 = (unsigned long int) ((L_var1 < 0) ? -L_var1 : L_var1);
#ifdef __GNUC__
   asm (
			"clz %[s], %[a]"
			: [s] "=r" (ReturnValue)
			: [a] "r" (AbsVar1)
		);
#else
   __asm {
		clz ReturnValue,AbsVar1;
   }
#endif
	if (L_var1 > 0)
		return(ReturnValue-1);
	else
	{
		TestValue = AbsVar1 << (ReturnValue-1);
		if (TestValue < 0x20000000)
			return(ReturnValue+1);
		else if (TestValue == 0x40000000)
			return(ReturnValue);
		else
			return(ReturnValue-1);
	}
}
#else

INLINE Word16 norm_l (Word32 L_var1)
{
    Word16 var_out;
    if (L_var1 == 0)
    {
        var_out = 0;
    }
    else
    {
        if (L_var1 == (Word32) 0xffffffffL)
        {
            var_out = 31;
        }
        else
        {
            if (L_var1 < 0)
            {
                L_var1 = ~L_var1;
            }
            for (var_out = 0; L_var1 < (Word32) 0x40000000L; var_out++)
            {
                L_var1 <<= 1;
            }
        }
    }

    return (var_out);
}

#endif
#if defined(__arm__) && defined(__CLZ__)
INLINE Word16 norm_s(Word16 x) 
{ 
    int lz; 
    if (x == 0) 
        return 0;                      /* 0 is a special case */ 
    x = (Word16) (x ^ (x >> 15));      /* invert x if it's negative */ 
#ifdef __GNUC__
	asm (
		" clz %[y], %[x]"
		: [y] "=r" (lz)
		: [x] "r" (x)
	);
#else
	__asm { 
        clz lz, x; 
    } 
#endif
	return (Word16) (lz - 17); 
} 
#else
INLINE Word16 norm_s (Word16 var1)
{
    Word16 var_out;
    if (var1 == 0)
    {
        var_out = 0;
    }
    else
    {
        if (var1 == -1)
        {
            var_out = 15;
        }
        else
        {
            if (var1 < 0)
            {
                var1 = ~var1;
            }
            for (var_out = 0; var1 < 0x4000; var_out++)
            {
                var1 <<= 1;
            }
        }
    }

    return (var_out);
}
#endif

#ifndef DIV_S_ASM
///Word16 G729AB_ADT_div_s(Word16 var1, Word16 var2)
INLINE Word16 G729AB_ADT_div_s(Word32 var1, Word32 var2)
  {
   Word16 var_out = 0;
   Word32 quot=0;
//   Word32 L_num, L_denom;

   if ((var1 > var2) || (var1 < 0) || (var2 < 0))
     {
///      printf("Division Error var1=%d  var2=%d\n",var1,var2);
      return(0);
     }

   if (var2 == 0)
     {
///      printf("Division by 0, Fatal error \n");
      return(0);
     }

   if (var1 == 0)
     {
      var_out = 0;
     }
   else
#if 1
	quot = 0x8000 * var1;
	quot /= var2;
	if (quot > MAX_16)
		return(MAX_16);
	else
		return((Word16) quot);
#else
   {
      if (var1 == var2)
        {
         var_out = MAX_16;
        }
      else
        {
         L_num = L_deposit_l(var1);
         L_denom = L_deposit_l(var2);

         for(iteration=0;iteration<15;iteration++)
           {
            var_out <<=1;
            L_num <<= 1;

            if (L_num >= L_denom)
              {
               L_num = L_sub(L_num,L_denom);
               var_out = add(var_out,1);
              }
           }
        }
     }
   return(var_out);
#endif
  }
#endif

// Basic operations that require overflow support - used by Adaptive Digital for G.729 and possibly other algorithms


INLINE Word32 L_addOVF(Word32 L_var1, Word32 L_var2)
{
   Word32 L_var_out;
   long int Sum40;
   long int ZTest;

   L_var_out = L_add(L_var1, L_var2);
   Sum40 = (long int) L_var1 + (long int) L_var2;
   ZTest = Sum40 ^ L_var_out;
   if (ZTest != 0)
	G729AB_ADT_Overflow = 1;
   return(L_var_out);
}

INLINE Word32 L_subOVF(Word32 L_var1, Word32 L_var2)
{
   Word32 L_var_out;
   long int Dif40;
   long int ZTest;

   L_var_out = L_sub(L_var1, L_var2);
   Dif40 = (long int) L_var1 - (long int) L_var2;
   ZTest = Dif40 ^ L_var_out;
   if (ZTest != 0)
	G729AB_ADT_Overflow = 1;
   return(L_var_out);
}

INLINE Word32 L_multOVF(Word16 var1, Word16 var2)
  {
   Word32 L_var_out;
   Word32 W32;

   W32 = (Word32)var1 * (Word32)var2;
   L_var_out = _smpy(var1, var2);
   if (W32 == (Word32)0x40000000L)
	   G729AB_ADT_Overflow = 1;

   return(L_var_out);
  }

INLINE Word32 L_macOVF(Word32 L_var3, Word16 var1, Word16 var2)
  {
   Word32 L_var_out;
   Word32 L_produit;

   L_produit = L_multOVF(var1, var2);
   L_var_out = L_addOVF(L_var3, L_produit);
   return(L_var_out);
  }

INLINE Word32 L_msuOVF(Word32 L_var3, Word16 var1, Word16 var2)
  {
   Word32 L_var_out;
   Word32 L_produit;

   L_produit = L_multOVF(var1, var2);
   L_var_out = L_subOVF(L_var3, L_produit);
   return(L_var_out);
  }

INLINE Word16 addOVF(Word16 var1, Word16 var2)
  {
   Word16 var_out;
   Word32 L_somme;

   L_somme = (L_add(((Word32) var1) << 16, ((Word32) var2) << 16) >> 16);
   var_out = var1 + var2;
   G729AB_ADT_Overflow = 0;
   if (var_out != L_somme)
   {
	G729AB_ADT_Overflow = 1;
	var_out = (Word16) L_somme;
   }
   return(var_out);
  }

INLINE Word16 subOVF(Word16 var1, Word16 var2)
  {
   Word16 var_out;
   Word32 L_somme;

   L_somme = (L_sub(((Word32) var1) << 16, ((Word32) var2) << 16) >> 16);
   var_out = var1 - var2;
   G729AB_ADT_Overflow = 0;
   if (var_out != L_somme)
   {
	G729AB_ADT_Overflow = 1;
	var_out = (Word16) L_somme;
   }
   return(var_out);
  }

INLINE Word16 roundOVF(Word32 L_var1)
  {
   Word16 var_out;
   Word32 L_arrondi;

   L_arrondi = L_addOVF(L_var1, (Word32)0x00008000);
   var_out = extract_h(L_arrondi);
   return(var_out);
  }


INLINE Word32 L_shlOVF(Word32 L_var1, Word16 var2)
{
	Word32 a, b;
	a = L_shl(L_var1, var2);
	if (var2 >= 0)
		b = L_var1 << var2;
	else
		b = L_var1 >> (-var2);
	if (a != b)
		G729AB_ADT_Overflow = 1;
	return(a);
}
INLINE Word32 L_shrOVF(Word32 L_var1, Word16 var2)
{
	Word32 a, b;
	a = L_shr(L_var1, var2);
	if (var2 >= 0)
		b = L_var1 >> var2;
	else
		b = L_var1 << (-var2);
	if (a != b)
		G729AB_ADT_Overflow = 1;
	return(a);
}

/*___________________________________________________________________________
 |                                                                           |
 |   Function Name : L_sub_c                                                 |
 |                                                                           |
 |   Purpose :                                                               |
 |                                                                           |
 |   Performs 32 bits subtraction of the two 32 bits variables with carry    |
 |   (borrow) : L_var1-L_var2-C. No saturation. Generate carry and Overflow  |
 |   values. The carry and overflow values are binary variables which can    |
 |   be tested and assigned values.                                          |
 |                                                                           |
 |   Complexity weight : 2                                                   |
 |                                                                           |
 |   Inputs :                                                                |
 |                                                                           |
 |    L_var1   32 bit long signed integer (Word32) whose value falls in the  |
 |             range : 0x8000 0000 <= L_var3 <= 0x7fff ffff.                 |
 |                                                                           |
 |    L_var2   32 bit long signed integer (Word32) whose value falls in the  |
 |             range : 0x8000 0000 <= L_var3 <= 0x7fff ffff.                 |
 |                                                                           |
 |   Outputs :                                                               |
 |                                                                           |
 |    none                                                                   |
 |                                                                           |
 |   Return Value :                                                          |
 |                                                                           |
 |    L_var_out                                                              |
 |             32 bit long signed integer (Word32) whose value falls in the  |
 |             range : 0x8000 0000 <= L_var_out <= 0x7fff ffff.              |
 |                                                                           |
 |   Caution :                                                               |
 |                                                                           |
 |    In some cases the Carry flag has to be cleared or set before using op- |
 |    rators which take into account its value.                              |
 |___________________________________________________________________________|
*/

INLINE Word32 L_sub_c(Word32 L_var1, Word32 L_var2)
  {
   Word32 L_var_out;
   Word32 L_test;
   Flag carry_int = 0;

   if (Carry)
     {
      Carry = 0;
      if (L_var2 != MIN_32)
        {
         L_var_out = L_add_c(L_var1,-L_var2);
        }
      else
        {
         L_var_out = L_var1 - L_var2;
         if (L_var1 > 0L)
           {
            Overflow = 1;
            Carry = 0;
           }
        }
     }
   else
     {
      L_var_out = L_var1 - L_var2 - (Word32)0X00000001;
      L_test = L_var1 - L_var2;

      if ((L_test < 0) && (L_var1 > 0) && (L_var2 < 0))
        {
         Overflow = 1;
         carry_int = 0;
        }
      else if ((L_test > 0) && (L_var1 < 0) && (L_var2 > 0))
        {
         Overflow = 1;
         carry_int = 1;
        }
      else if ((L_test > 0) && ((L_var1 ^ L_var2) > 0))
        {
         Overflow = 0;
         carry_int = 1;
        }


      if (L_test == MIN_32)
        {
         Overflow = 1;
         Carry = carry_int;
        }
      else
        {
         Carry = carry_int;
        }
     }

   return(L_var_out);
  }



#define L_msu(a,b,c)   (L_sub((a),L_mult(b,c)))     /* saturated mpy & sub    */

#if 0
#define L_shl(a,b)     (_shl((a),(b)))              /* sat left shift         */
#define L_shr(a,b)     (_shr((a),(b)))              /* sat right shift        */
#endif

#define L_shr_r(a,b)   (L_shr((a),(b)) + ((b)>0 && (((a) & (1<<((b)-1))) != 0)))


//#define abs_s(a)       _abs16_s(((a)<<16)>>16)      /* short absolute value   */
//#define add(a,b)       (L_add((a)<<16,(b)<<16)>>16) /* short sat add          */
//#define sub(a,b)       (L_sub((a)<<16,(b)<<16)>>16) /* short sat subtract     */
#define extract_h(a)   ((unsigned)(a)>>16)          /* extract upper 16 bits  */
//#define extract_l(a)   (((a)&0xffff))               /* extract lower 16 bits  */
//#define round(a)       extract_h(L_add((a),0x8000)) /* round                  */
#define mac_r(a,b,c)   (round(L_mac((a),(b),(c))))  /* mac w/ rounding        */
#define msu_r(a,b,c)   (round(L_msu((a),(b),(c))))  /* msu w/ rounding        */
#define mult_r(a,b)    (round(L_mult((a),(b))))     /* sat mpy w/ round       */
#define mult(a,b)      ((ADT_Int16) (L_mult((a),(b))>>16))      /* short sat mpy upper 16 */

//#define mult(a,b)      ((L_mult((a),(b))>>16))      /* short sat mpy upper 16 */

/* norm_l as defined in app. implementation differs from the compiler         */
/* implementation of CLZ by a off-by-one error. So where the compiler's       */
/* implementation returns the number of all leading zeroes, the app specific  */
/* implementation will return 1 less. Another difference between the two is   */
/* that the app. specific implementation will handle negative numbers,        */
/* whereas the compiler implementation won't. Commenting this out completely, */
/* and resorting to the app. specific implementation.                         */
/* #define norm_l(a)      (_norm(a))   */
/* #define norm_l(a)      (_norm(a)-1) */           /* return NORM of int     */

//#define norm_s(a)      (norm_l((a))-16)             /* return NORM of short   */
#define negate(a)      ((ADT_Int16) (L_sub(0, ((a)<<16)) >> 16))  /* short sat negate       */


#ifdef __cplusplus 
} 
#endif 

#endif /* ETSI_ARM_H_ */
