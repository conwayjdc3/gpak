/*==============================================================================
 *
 *            ADAPTIVE DIGITAL TECHNOLOGIES, INC. PROPRIETARY INFORMATION
 *
 *   Property of Adaptive Digital Technologies, Inc.
 *   For Use by Adaptive Digital and/or its Licensees Only.
 *   Restricted rights to use, duplicate or disclose this code are
 *   granted through contract.
 *   Unauthorized reproduction and/or distribution are strictly prohibited.
 *   This product is protected under copyright law and trade secret law as an
 *   Unpublished work.
 *   Created 2011, (C) Copyright 2011 Adaptive Digital Technologies, Inc.  All rights reserved.
 *
 *   Component  :
 *
 *   Filename   : 	adt_memory.h
 *
 *   Description:
 *
 *   $Archive:   P:/ADT_Algorithms/archives/common/private/utilities/adt_memory.h-arc  $
 *   $Author:   skurtz  $
 *   $Date:   03 Nov 2011 11:05:50  $
 *   $Header:   P:/ADT_Algorithms/archives/common/private/utilities/adt_memory.h-arc   1.0   03 Nov 2011 11:05:50   skurtz  $
 *   $Log:   P:/ADT_Algorithms/archives/common/private/utilities/adt_memory.h-arc  $
 * 
 *    Rev 1.0   03 Nov 2011 11:05:50   skurtz
 * Initial revision.
 *   $Modtime:   02 Nov 2011 13:27:42  $
 *   $Revision:   1.0  $
 *   $Workfile:   adt_memory.h  $
 *=============================================================================*/

#ifndef _ADT_MEMORY_H
#define _ADT_MEMORY_H
#include "adt_typedef_user.h"

/* 
   Each of these functions includes Length and DestLength
   Length is the number of words to be copied or set
   DestSize is the size in bytes of the destination buffer
   Each function returns the number of words copied or set. If the destination buffer 
   size is not sufficient for the entire copy / set operation is ended early.
   to prevent writes beyond the end of the buffer
*/

ADT_UInt32 ADT_MemCopy8(ADT_Int8 *pDest, ADT_Int8 *pSrc, ADT_Int32 Length, ADT_Int32 DestSize8);
ADT_UInt32 ADT_MemCopy16(ADT_Int16 *pDest, ADT_Int16 *pSrc, ADT_Int32 Length, ADT_Int32 DestSize8);
ADT_UInt32 ADT_MemCopy32(ADT_Int32 *pDest, ADT_Int32 *pSrc, ADT_Int32 Length, ADT_Int32 DestSize8);
ADT_UInt32 MemSet8(ADT_Int8 *pDest, ADT_Int8 Value, ADT_Int32 Length, ADT_Int32 DestSize8);
ADT_UInt32 MemSet16(ADT_Int16 *pDest, ADT_Int16 Value, ADT_Int32 Length, ADT_Int32 DestSize8);
ADT_UInt32 MemSet32(ADT_Int32 *pDest, ADT_Int32 Value, ADT_Int32 Length, ADT_Int32 DestSize8);
#endif // _ADT_MEMORY_H