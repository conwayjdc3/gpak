#ifndef _ADT_CHECKSUM_H
#define _ADT_CHECKSUM_H
/*==============================================================================
 *
 *            ADAPTIVE DIGITAL TECHNOLOGIES, INC. PROPRIETARY INFORMATION
 *
 *   Property of Adaptive Digital Technologies, Inc.
 *   For Use by Adaptive Digital and/or its Licensees Only.
 *   Restricted rights to use, duplicate or disclose this code are
 *   granted through contract.
 *   Unauthorized reproduction and/or distribution are strictly prohibited.
 *   This product is protected under copyright law and trade secret law as an
 *   Unpublished work.
 *   Created 2011, (C) Copyright 2011 Adaptive Digital Technologies, Inc.  All rights reserved.
 *
 *   Component  :
 *
 *   Filename   : 	adt_checksum.h
 *
 *   Description:
 *
 *   $Archive:   P:/ADT_Algorithms/archives/common/private/utilities/adt_checksum.h-arc  $
 *   $Author:   skurtz  $
 *   $Date:   03 Nov 2011 11:05:50  $
 *   $Header:   P:/ADT_Algorithms/archives/common/private/utilities/adt_checksum.h-arc   1.0   03 Nov 2011 11:05:50   skurtz  $
 *   $Log:   P:/ADT_Algorithms/archives/common/private/utilities/adt_checksum.h-arc  $
 * 
 *    Rev 1.0   03 Nov 2011 11:05:50   skurtz
 * Initial revision.
 *   $Modtime:   02 Nov 2011 13:06:56  $
 *   $Revision:   1.0  $
 *   $Workfile:   adt_checksum.h  $
 *=============================================================================*/

#include "adt_typedef_user.h"


ADT_UInt32 ADT_Checksum32Compute(
		ADT_UInt8 *p,			/* buffer to be checksum'ed */
		ADT_UInt32 size			/* size(in bytes) of buffer checksum */
		);

ADT_UInt8 ADT_Checksum32Verify( /* returns 1 for error, 0 for OK */
		ADT_UInt8 *p,			/* buffer to be checked */
		ADT_UInt32 size,		/* size (in bytes) of buffer to be checked*/
		ADT_UInt32 Checksum		/* expected checksum */
		);


#endif
