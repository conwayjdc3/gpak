/*==============================================================================
 *
 *            ADAPTIVE DIGITAL TECHNOLOGIES, INC. PROPRIETARY INFORMATION
 *
 *   Property of Adaptive Digital Technologies, Inc.
 *   For Use by Adaptive Digital and/or its Licensees Only.
 *   Restricted rights to use, duplicate or disclose this code are
 *   granted through contract.
 *   Unauthorized reproduction and/or distribution are strictly prohibited.
 *   This product is protected under copyright law and trade secret law as an
 *   Unpublished work.
 *   Created 2011, (C) Copyright 2011 Adaptive Digital Technologies, Inc.  All rights reserved.
 *
 *   Component  :
 *
 *   Filename   : 	adt_log.h
 *
 *   Description:
 *
 *   $Archive:   P:/ADT_Algorithms/archives/common/private/utilities/adt_log.h-arc  $
 *   $Author:   skurtz  $
 *   $Date:   03 Nov 2011 11:05:50  $
 *   $Header:   P:/ADT_Algorithms/archives/common/private/utilities/adt_log.h-arc   1.0   03 Nov 2011 11:05:50   skurtz  $
 *   $Log:   P:/ADT_Algorithms/archives/common/private/utilities/adt_log.h-arc  $
 * 
 *    Rev 1.0   03 Nov 2011 11:05:50   skurtz
 * Initial revision.
 *   $Modtime:   02 Nov 2011 15:09:22  $
 *   $Revision:   1.0  $
 *   $Workfile:   adt_log.h  $
 *=============================================================================*/
#ifndef _ADT_LOG_H
#define _ADT_LOG_H


#define ADTLOG_ALWAYS 0xffff
#define ADTLOG_DEBUG 0x1		/*debug message, not during initialization			*/
#define ADTLOG_INFO 0x2			/*informational message, not during initialization	*/
#define ADTLOG_WARNING 0x4		/*warning, not during initialization				*/
#define ADTLOG_ERROR 0x8		/*error, not during initialization					*/
#define ADTLOG_INIT_DEBUG 0x10	/*debug message during initialization				*/
#define ADTLOG_INIT_INFO 0x20	/*informational message during initialization		*/
#define ADTLOG_INIT_WARNING 0x40/*warning during initialization						*/
#define ADTLOG_INIT_ERROR 0x80	/*error during initialization						*/


void ADT_LogInit(short int Filter, char *FilePath);
void ADT_LogClose();
void ADT_Log(short int Filter, short int TimingEnable, const char *fmt, ...);
short int ADT_LogGetFilter();
double ADT_RealTimeFunction();

#endif _ADT_LOG_H