/*==============================================================================
 *
 *            ADAPTIVE DIGITAL TECHNOLOGIES, INC. PROPRIETARY INFORMATION
 *
 *   Property of Adaptive Digital Technologies, Inc.
 *   For Use by Adaptive Digital and/or its Licensees Only.
 *   Restricted rights to use, duplicate or disclose this code are
 *   granted through contract.
 *   Unauthorized reproduction and/or distribution are strictly prohibited.
 *   This product is protected under copyright law and trade secret law as an
 *   Unpublished work.
 *   Created 2011, (C) Copyright 2011 Adaptive Digital Technologies, Inc.  All rights reserved.
 *
 *   Component  :
 *
 *   Filename   : 	adt_memory_os_specific.h
 *
 *   Description:
 *
 *   $Archive:   P:/ADT_Algorithms/archives/common/private/utilities/win32/adt_memory_os_specific.h-arc  $
 *   $Author:   skurtz  $
 *   $Date:   03 Nov 2011 11:04:24  $
 *   $Header:   P:/ADT_Algorithms/archives/common/private/utilities/win32/adt_memory_os_specific.h-arc   1.0   03 Nov 2011 11:04:24   skurtz  $
 *   $Log:   P:/ADT_Algorithms/archives/common/private/utilities/win32/adt_memory_os_specific.h-arc  $
 * 
 *    Rev 1.0   03 Nov 2011 11:04:24   skurtz
 * Initial revision.
 *   $Modtime:   02 Nov 2011 15:09:56  $
 *   $Revision:   1.0  $
 *   $Workfile:   adt_memory_os_specific.h  $
 *=============================================================================*/
#ifndef _ADT_MEMORY_OS_SPECIFIC_H
#define _ADT_MEMORY_OS_SPECIFIC_H
#include <stdio.h>
#include "string.h"

#define _ADT_MEMCOPY8_OVERRIDE
INLINE ADT_UInt32 ADT_MemCopy8(ADT_Int8 *pDest, ADT_Int8 *pSrc, ADT_Int32 Length, ADT_Int32 DestSize8)
{
	ADT_UInt32 NWordsToCopy = Length;
	if (NWordsToCopy > (DestSize8/sizeof(ADT_UInt8)))
		NWordsToCopy = DestSize8/sizeof(ADT_UInt8);
	memcpy(pDest,  pSrc, (size_t) NWordsToCopy*sizeof(ADT_UInt8));
	return(NWordsToCopy);
}

#define _ADT_MEMCOPY16_OVERRIDE
ADT_UInt32 ADT_MemCopy16(ADT_Int16 *pDest, ADT_Int16 *pSrc, ADT_Int32 Length, ADT_Int32 DestSize8)
{
	ADT_UInt32 NWordsToCopy = Length;
	if (NWordsToCopy > DestSize8/sizeof(ADT_UInt16))
		NWordsToCopy = DestSize8/sizeof(ADT_UInt16);
	memcpy(pDest,  pSrc, (size_t) NWordsToCopy*sizeof(ADT_UInt16));
	return(NWordsToCopy);
}

#define _ADT_MEMCOPY32_OVERRIDE
ADT_UInt32 ADT_MemCopy32(ADT_Int32 *pDest, ADT_Int32 *pSrc, ADT_Int32 Length, ADT_Int32 DestSize8)
{
	ADT_UInt32 NWordsToCopy = Length;
	if (NWordsToCopy > DestSize8/sizeof(ADT_UInt32))
		NWordsToCopy = DestSize8/sizeof(ADT_UInt32);
	memcpy(pDest,  pSrc, (size_t) NWordsToCopy*sizeof(ADT_UInt32));
	return(NWordsToCopy);
}
#define _ADT_MEMSET8_OVERRIDE
ADT_UInt32 ADT_MemSet8(ADT_Int8 *pDest, ADT_Int8 Value, ADT_Int32 Length, ADT_Int32 DestSize8)
{
	ADT_UInt32 NWordsToSet = Length;
	if (NWordsToSet > DestSize8/sizeof(ADT_UInt8))
		NWordsToSet = DestSize8/sizeof(ADT_UInt8);
	memset(pDest, Value, (size_t) NWordsToSet*sizeof(ADT_UInt8));
	return(NWordsToSet);
}

#define _ADT_MEMSET16_OVERRIDE
ADT_UInt32 ADT_MemSet16(ADT_Int16 *pDest, ADT_Int16 Value, ADT_Int32 Length, ADT_Int32 DestSize8)
{
	ADT_UInt32 NWordsToSet = Length;
	if (NWordsToSet > DestSize8/sizeof(ADT_UInt16))
		NWordsToSet = DestSize8/sizeof(ADT_UInt16);
	memset(pDest, Value, (size_t) NWordsToSet*sizeof(ADT_UInt16));
	return(NWordsToSet);
}

#define _ADT_MEMSET32_OVERRIDE
ADT_UInt32 ADT_MemSet32(ADT_Int32 *pDest, ADT_Int32 Value, ADT_Int32 Length, ADT_Int32 DestSize8)
{
	ADT_UInt32 NWordsToSet = Length;
	if (NWordsToSet > DestSize8/sizeof(ADT_UInt32))
		NWordsToSet = DestSize8/sizeof(ADT_UInt32);
	memset(pDest, Value, (size_t) NWordsToSet*sizeof(ADT_UInt32));
	return(NWordsToSet);
}

#endif //_ADT_MEMORY_OS_SPECIFIC_H

