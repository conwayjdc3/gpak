/*==============================================================================
 *
 *            ADAPTIVE DIGITAL TECHNOLOGIES, INC. PROPRIETARY INFORMATION
 *
 *   Property of Adaptive Digital Technologies, Inc.
 *   For Use by Adaptive Digital and/or its Licensees Only.
 *   Restricted rights to use, duplicate or disclose this code are
 *   granted through contract.
 *   Unauthorized reproduction and/or distribution are strictly prohibited.
 *   This product is protected under copyright law and trade secret law as an
 *   Unpublished work.
 *   Created 2011, (C) Copyright 2011 Adaptive Digital Technologies, Inc.  All rights reserved.
 *
 *   Component  :
 *
 *   Filename   : 	adt_memory.h
 *
 *   Description:
 *
 *   $Archive:   P:/ADT_Algorithms/archives/common/private/utilities/adt_memory.c-arc  $
 *   $Author:   skurtz  $
 *   $Date:   03 Nov 2011 11:05:50  $
 *   $Header:   P:/ADT_Algorithms/archives/common/private/utilities/adt_memory.c-arc   1.0   03 Nov 2011 11:05:50   skurtz  $
 *   $Log:   P:/ADT_Algorithms/archives/common/private/utilities/adt_memory.c-arc  $
 * 
 *    Rev 1.0   03 Nov 2011 11:05:50   skurtz
 * Initial revision.
 *   $Modtime:   02 Nov 2011 13:57:12  $
 *   $Revision:   1.0  $
 *   $Workfile:   adt_memory.c  $
 *=============================================================================*/
#include "adt_typedef.h"
#include "adt_memory.h"
#include "adt_memory_os_specific.h"

#ifndef _ADT_MEMCOPY8_OVERRIDE
ADT_UInt32 ADT_MemCopy8(ADT_Int8 *pDest, ADT_Int8 *pSrc, ADT_Int32 Length, ADT_Int32 DestSize8)
{
	ADT_UInt32 i;
	ADT_UInt32 NWordsToCopy = Length;
	if (NWordsToCopy > (DestSize8/sizeof(ADT_UInt8)))
		NWordsToCopy = DestSize8/sizeof(ADT_UInt8);
	for (i = 0; i < NWordsToCopy; i++)
		pDest[i] = pSrc[i];
	return(NWordsToCopy);
}
#endif //_ADT_MEMCOPY8_OVERRIDE

#ifndef _ADT_MEMCOPY16_OVERRIDE
ADT_UInt32 ADT_MemCopy16(ADT_Int16 *pDest, ADT_Int16 *pSrc, ADT_Int32 Length, ADT_Int32 DestSize8)
{
	ADT_UInt32 i;
	ADT_UInt32 NWordsToCopy = Length;
	if (NWordsToCopy > DestSize8/sizeof(ADT_UInt16))
		NWordsToCopy = DestSize8/sizeof(ADT_UInt16);
	for (i = 0; i < NWordsToCopy; i++)
		pDest[i] = pSrc[i];
	return(NWordsToCopy);
}
#endif //_ADT_MEMCOPY16_OVERRIDE

#ifndef _ADT_MEMCOPY32_OVERRIDE
ADT_UInt32 ADT_MemCopy32(ADT_Int32 *pDest, ADT_Int32 *pSrc, ADT_Int32 Length, ADT_Int32 DestSize8)
{
	ADT_UInt32 i;
	ADT_UInt32 NWordsToCopy = Length;
	if (NWordsToCopy > DestSize8/sizeof(ADT_UInt32))
		NWordsToCopy = DestSize8/sizeof(ADT_UInt32);
	for (i = 0; i < NWordsToCopy; i++)
		pDest[i] = pSrc[i];
	return(NWordsToCopy);
}
#endif //_ADT_MEMCOPY32_OVERRIDE

#ifndef _ADT_MEMSET8_OVERRIDE
ADT_UInt32 ADT_MemSet8(ADT_Int8 *pDest, ADT_Int8 Value, ADT_Int32 Length, ADT_Int32 DestSize8)
{
	ADT_UInt32 i;
	ADT_UInt32 NWordsToSet = Length;
	if (NWordsToSet > DestSize8/sizeof(ADT_UInt8))
		NWordsToSet = DestSize8/sizeof(ADT_UInt8);
	for (i = 0; i < NWordsToSet; i++)
		pDest[i] = Value;
	return(NWordsToSet);
}
#endif //_ADT_MEMSET8_OVERRIDE

#ifndef _ADT_MEMSET16_OVERRIDE
ADT_UInt32 ADT_MemSet16(ADT_Int16 *pDest, ADT_Int16 Value, ADT_Int32 Length, ADT_Int32 DestSize8)
{
	ADT_UInt32 i;
	ADT_UInt32 NWordsToSet = Length;
	if (NWordsToSet > DestSize8/sizeof(ADT_UInt16))
		NWordsToSet = DestSize8/sizeof(ADT_UInt16);
	for (i = 0; i < NWordsToSet; i++)
		pDest[i] = Value;
	return(NWordsToSet);
}
#endif //_ADT_MEMSET16_OVERRIDE

#ifndef _ADT_MEMSET32_OVERRIDE
ADT_UInt32 ADT_MemSet32(ADT_Int32 *pDest, ADT_Int32 Value, ADT_Int32 Length, ADT_Int32 DestSize8)
{
	ADT_UInt32 i;
	ADT_UInt32 NWordsToSet = Length;
	if (NWordsToSet > DestSize8/sizeof(ADT_UInt32))
		NWordsToSet = DestSize8/sizeof(ADT_UInt32);
	for (i = 0; i < NWordsToSet; i++)
		pDest[i] = Value;
	return(NWordsToSet);
}
#endif //_ADT_MEMSET32_OVERRIDE
