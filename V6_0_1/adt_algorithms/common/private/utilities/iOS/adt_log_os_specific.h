/*==============================================================================
 *
 *            ADAPTIVE DIGITAL TECHNOLOGIES, INC. PROPRIETARY INFORMATION
 *
 *   Property of Adaptive Digital Technologies, Inc.
 *   For Use by Adaptive Digital and/or its Licensees Only.
 *   Restricted rights to use, duplicate or disclose this code are
 *   granted through contract.
 *   Unauthorized reproduction and/or distribution are strictly prohibited.
 *   This product is protected under copyright law and trade secret law as an
 *   Unpublished work.
 *   Created 2011, (C) Copyright 2011 Adaptive Digital Technologies, Inc.  All rights reserved.
 *
 *   Component  :
 *
 *   Filename   : 	adt_log_os_specific.h
 *
 *   Description:
 *
 *   $Archive:   P:/ADT_Algorithms/archives/common/private/utilities/iOS/adt_log_os_specific.h-arc  $
 *   $Author:   skurtz  $
 *   $Date:   03 Nov 2011 11:05:26  $
 *   $Header:   P:/ADT_Algorithms/archives/common/private/utilities/iOS/adt_log_os_specific.h-arc   1.0   03 Nov 2011 11:05:26   skurtz  $
 *   $Log:   P:/ADT_Algorithms/archives/common/private/utilities/iOS/adt_log_os_specific.h-arc  $
 * 
 *    Rev 1.0   03 Nov 2011 11:05:26   skurtz
 * Initial revision.
 *   $Modtime:   03 Nov 2011 07:20:28  $
 *   $Revision:   1.0  $
 *   $Workfile:   adt_log_os_specific.h  $
 *=============================================================================*/

#define _ADT_REALTIME_FUNCTION_OVERRIDE

#define ADT_RealTimeFunction GetHiResTime
