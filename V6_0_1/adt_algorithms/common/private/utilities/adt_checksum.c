/*==============================================================================
 *
 *            ADAPTIVE DIGITAL TECHNOLOGIES, INC. PROPRIETARY INFORMATION
 *
 *   Property of Adaptive Digital Technologies, Inc.
 *   For Use by Adaptive Digital and/or its Licensees Only.
 *   Restricted rights to use, duplicate or disclose this code are
 *   granted through contract.
 *   Unauthorized reproduction and/or distribution are strictly prohibited.
 *   This product is protected under copyright law and trade secret law as an
 *   Unpublished work.
 *   Created 2011, (C) Copyright 2011 Adaptive Digital Technologies, Inc.  All rights reserved.
 *
 *   Component  :
 *
 *   Filename   : 	adt_checksum.c
 *
 *   Description:
 *
 *   $Archive:   P:/ADT_Algorithms/archives/common/private/utilities/adt_checksum.c-arc  $
 *   $Author:   skurtz  $
 *   $Date:   03 Nov 2011 11:05:48  $
 *   $Header:   P:/ADT_Algorithms/archives/common/private/utilities/adt_checksum.c-arc   1.0   03 Nov 2011 11:05:48   skurtz  $
 *   $Log:   P:/ADT_Algorithms/archives/common/private/utilities/adt_checksum.c-arc  $
 * 
 *    Rev 1.0   03 Nov 2011 11:05:48   skurtz
 * Initial revision.
 *   $Modtime:   02 Nov 2011 13:06:12  $
 *   $Revision:   1.0  $
 *   $Workfile:   adt_checksum.c  $
 *=============================================================================*/

#include "adt_typedef_user.h"
#include "adt_checksum.h"

ADT_UInt32 ADT_Checksum32Compute(
		ADT_UInt8 *p,			/* buffer to be checksum'ed */
		ADT_UInt32 size			/* size(in bytes) of buffer checksum */
		)
{
	ADT_UInt32 i, Sum = 0;
	for (i = 0; i < size; i++)
		Sum += *p++;
	return(Sum);
}

ADT_UInt8 ADT_Checksum32Verify( /* returns 1 for error, 0 for OK */
		ADT_UInt8 *p,			/* buffer to be checked */
		ADT_UInt32 size,		/* size (in bytes) of buffer to be checked*/
		ADT_UInt32 Checksum		/* expected checksum */
		)
{
	ADT_UInt32 Sum;
	Sum = ADT_Checksum32Compute(p, size);
	return(Sum != Checksum);
}



