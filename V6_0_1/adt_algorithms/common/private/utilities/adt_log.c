/*==============================================================================
 *
 *            ADAPTIVE DIGITAL TECHNOLOGIES, INC. PROPRIETARY INFORMATION
 *
 *   Property of Adaptive Digital Technologies, Inc.
 *   For Use by Adaptive Digital and/or its Licensees Only.
 *   Restricted rights to use, duplicate or disclose this code are
 *   granted through contract.
 *   Unauthorized reproduction and/or distribution are strictly prohibited.
 *   This product is protected under copyright law and trade secret law as an
 *   Unpublished work.
 *   Created 2011, (C) Copyright 2011 Adaptive Digital Technologies, Inc.  All rights reserved.
 *
 *   Component  :
 *
 *   Filename   : 	adt_log.c
 *
 *   Description:
 *
 *   $Archive:   P:/ADT_Algorithms/archives/common/private/utilities/adt_log.c-arc  $
 *   $Author:   skurtz  $
 *   $Date:   03 Nov 2011 11:05:50  $
 *   $Header:   P:/ADT_Algorithms/archives/common/private/utilities/adt_log.c-arc   1.0   03 Nov 2011 11:05:50   skurtz  $
 *   $Log:   P:/ADT_Algorithms/archives/common/private/utilities/adt_log.c-arc  $
 * 
 *    Rev 1.0   03 Nov 2011 11:05:50   skurtz
 * Initial revision.
 *   $Modtime:   03 Nov 2011 07:25:24  $
 *   $Revision:   1.0  $
 *   $Workfile:   adt_log.c  $
 *=============================================================================*/
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include "adt_typedef.h"
#include "adt_log.h"
#include "adt_log_os_specific.h"
#define ADT_LOG_DEBUG

#ifndef _ADT_REALTIME_FUNCTION_OVERRIDE
double ADT_RealTimeFunction()	// returns time in seconds, with fractional portion.
{
	return(-1.0);
}
#endif

static void RealTime(char *pString)
{
	ADT_UInt64 MicrosecondCount;
	ADT_UInt16 Seconds,Milliseconds,Microseconds;
	MicrosecondCount = (ADT_UInt64) (ADT_RealTimeFunction() * 1000000.0);
	if (MicrosecondCount < 0.0)
	{
		*pString = 0;
		return;
	}
	Seconds = (ADT_UInt16) (MicrosecondCount / 1000000);
	MicrosecondCount = MicrosecondCount % 1000000;
	Milliseconds = (ADT_UInt16) (MicrosecondCount / 1000);
	Microseconds = (ADT_UInt16) (MicrosecondCount % 1000);
	sprintf(pString,"%6d:%3d:%3d",Seconds,Milliseconds,Microseconds);
}
#define MAX_STRING_LENGTH 256
static short int LogFilter=0;

#ifdef WIN32
static FILE *LogFile=0;
#endif

void ADT_LogInit(short int Filter, char *FilePath)
{
	LogFilter = Filter;
#if defined(WIN32) && defined(ADTLOG_DEBUG)
	LogFile = fopen(FilePath,"w");
#endif
}
ADT_Int16 ADT_LogGetFilter() {return(LogFilter);}
void ADT_LogClose()
{
#if defined(WIN32) && defined(ADTLOG_DEBUG)
	if (LogFile != 0)
		fclose(LogFile);
	LogFile = 0;
#endif
}

void ADT_Log(short int Filter, short int TimingEnable, const char *fmt, ...)
{
	const char *p;
	va_list argp;
	int i;
	unsigned int u;
	char c;
	char *s;
	size_t Length;
	char FullString[MAX_STRING_LENGTH];
	char String[MAX_STRING_LENGTH];
	char Time[MAX_STRING_LENGTH];
	int Done = 0;
	size_t FullStringLength;
	char FmtWithLength[4];		/* %x or %u.. or %3x, %7u.. */
	int FmtHasLength = 0;
	int DoneFormat = 0;

	if ((Filter & LogFilter) == 0 && (Filter != ADTLOG_ALWAYS))
		return;
	va_start(argp, fmt);
	strcpy(FullString,"");
	if (TimingEnable)
	{
		RealTime(Time);
		strcpy(FullString,Time);
		strcat(FullString,": ");
	}
	FullStringLength = strlen(FullString);
	FmtWithLength[0] = '%';

	for(p = fmt; (*p != '\0') && !Done; p++)
	{
		FmtWithLength[1] = 0;
		if (*p != '%') 
		{
			//putchar(*p);
			sprintf(String, "%c",*p);
			Length = 1;
			if (FullStringLength + Length < MAX_STRING_LENGTH)
				strcat(FullString,String);
			else
				Done = 1;
			continue;
		}
		DoneFormat = 0;
		while (!DoneFormat && !Done)
		{
			switch(*++p)
			{
			case 'c':
				c = va_arg(argp, char);
				strcat(FmtWithLength,"c");
				sprintf(String, FmtWithLength,c);
				Length = strlen(String);
				if (FullStringLength + Length < MAX_STRING_LENGTH)
					strcat(FullString,String);
				else
					Done = 1;
				//putchar(i);
				DoneFormat = 1;	
				break;
			case 'd':
				i = va_arg(argp, int);
				strcat(FmtWithLength,"d");
				sprintf(String, FmtWithLength,i);
				Length = strlen(String);
				if (FullStringLength + Length < MAX_STRING_LENGTH)
				{	
					strcat(FullString,String);
					FullStringLength += Length;
				}
				else
					Done = 1;
				//s = itoa(i, fmtbuf, 10);
				//fputs(s, stdout);
				DoneFormat = 1;
				break;

			case 'u':
				u = va_arg(argp, unsigned);
				strcat(FmtWithLength,"u");
				sprintf(String, FmtWithLength,u);
				Length = strlen(String);
				if (FullStringLength + Length < MAX_STRING_LENGTH)
				{
					strcat(FullString,String);
					FullStringLength += Length;
				}
				else
					Done = 1;
				//s = itoa(i, fmtbuf, 10);
				//fputs(s, stdout);
				DoneFormat = 1;
				break;

			case 's':
				s = va_arg(argp, char *);
				Length = strlen(s);
				if (FullStringLength + Length < MAX_STRING_LENGTH)
				{
					strcat(FullString,s);
					FullStringLength += Length;
				}
				else
					Done = 1;
				//fputs(s, stdout);
				DoneFormat = 1;
				break;

			case 'x':
				i = va_arg(argp, int);
				strcat(FmtWithLength,"x");
				sprintf(String, FmtWithLength,i);
				Length = strlen(String);
				if (FullStringLength + Length < MAX_STRING_LENGTH)
				{
					strcat(FullString,String);
					FullStringLength += Length;
				}
				else
					Done = 1;
				//s = itoa(i, fmtbuf, 16);
				//fputs(s, stdout);
				DoneFormat = 1;
				break;

			case '%':
				if (FullStringLength < MAX_STRING_LENGTH)
				{
					strcat(FullString,"%");
					FullStringLength += 1;
				}
				//putchar('%');
				DoneFormat = 1;
				break;
			default:
				{
					FmtWithLength[1] = *p;
					FmtWithLength[2] = 0;
					FmtWithLength[3] = 0;
				}
			}	//End while !DoneFormat
		}
	}

	va_end(argp);
	printf("%s\n",FullString);
#if defined(WIN32) && defined(ADTLOG_DEBUG)
	if (LogFile != 0)
		fprintf(LogFile,"%s\n",FullString);
#endif
}
