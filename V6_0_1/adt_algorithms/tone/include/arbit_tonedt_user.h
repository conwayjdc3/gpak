#ifndef ARBIT_TONEDT_USER_H
#define ARBIT_TONEDT_USER_H

#include <common/include/adt_typedef_user.h>
#include "arbit_tonedt_opts.h"
#include "tonedetlowmem_user.h"

#if defined(__LP64__) || defined (_WIN64)
#define ARBIT_CHANNEL_SIZE ((((6 + NUM_DISTINCT_FREQS)* NUM_DISTINCT_FREQS+3)&0xfffC)*sizeof(ADT_Int16) + \
							sizeof(ADT_Int32) + \
							4*sizeof(ADT_Int16) + \
							4*sizeof(ADT_Int16 *))
#else
#define ARBIT_CHANNEL_SIZE ((((4 + NUM_DISTINCT_FREQS)* NUM_DISTINCT_FREQS+3)&0xfffC)*sizeof(ADT_Int16) + \
							sizeof(ADT_Int32) + \
							4*sizeof(ADT_Int16) + \
							4*sizeof(ADT_Int16 *))
#endif

typedef struct 
{
#if defined (__TMS320C55XX__)
	ADT_Int32	CData[ARBIT_CHANNEL_SIZE>>1];
#else
	ADT_Int32	CData[ARBIT_CHANNEL_SIZE>>2];
#endif
} ArbitDtInstance_t;

typedef struct 
{
	//Arbitrary tone detecet requirements
	ADT_Int16 ToneInfo[NUM_TONES][3];

} Tones_Index_t;


typedef struct 
{
	ADT_Int16 structSize; // parameter structure's size
	ADT_Int16 num_Distinct_Freqs; // distinct freqs need to be detected
	ADT_Int16 num_Tones;  // number of tones needs to be detected
	ADT_Int16 min_Power; // miminum power of tone in dB,'0' means default evel
	ADT_Int16 max_Freg_Deviation; // frequency in spec range, , '0' means default
							  // default is 2.5%
} ARBIT_ADT_Param_t;

ADT_API void ARBIT_ADT_config(ArbitDtInstance_t *ArbitChan, Tones_Index_t *pTones,
					ADT_Int16 *freqs, ARBIT_ADT_Param_t *ArbitParams);

ADT_API void ARBIT_ADT_init(ArbitDtInstance_t *ArbitChan, ARBIT_TDInstance_t *CprgInst);

ADT_API void TDLOWMEM_ADT_toneDetect(ARBIT_TDInstance_t *TDChannel, ADT_Int16 InputSamples[], ADT_Int16 FrameSize);

#endif

