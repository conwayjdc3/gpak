#ifndef _TG_USER
#define _TG_USER

#include <common/include/adt_typedef_user.h>

#define MAX_FREQ 4          // Do not modify this!
#define MAX_ONOFF 4

// Definitions of ToneType options
#define TONE_TYPE_CADENCE 0
#define TONE_TYPE_CONTINUOUS 1
#define TONE_TYPE_BURST 2

typedef struct
{
      ADT_Int32 Data[30]; //28: w/Am modualte  16:wo/AM 
}     TGInstance_t;

typedef struct
{
	short int AMEnable;		//Enable AM,
	short int AMFreq;		//AM frequencies
	short int AMRatio;		//AM modulation index
}TGAMParams_t;

typedef struct
{
   short int ToneType;        //See definitons above
   short int NFreqs;          //Number of frequencies
   short int Level;           //Output signal level (per frequency)
   short int OnTime;          //Tone On-Time (samples)
   short int OffTime;         //Tone Off-Time (samples) (0 = continuous)
   short int Freq[MAX_FREQ];  //Frequencies in Hz
} TGParams_t;

typedef struct
{
   short int ToneType;        //See definitons above
   short int NFreqs;          //Number of frequencies
   short int NOnOffs;		  //Number of OnOff element needed in one Pulse for TONE_TYPE_CADENCE, ?for burst?
   short int Level[MAX_FREQ];           //Output signal level (per frequency)
   short int OnTime[MAX_ONOFF];          //Tone On-Time (msec)
   short int OffTime[MAX_ONOFF];         //Tone Off-Time (msec) 
   short int Freq[MAX_FREQ];  //Frequencies in Hz
} TGParams_1_t;

void TG_ADT_AM_init(TGInstance_t    *Channel, 
					TGAMParams_t    *pAMParams);

ADT_API void TG_ADT_init(TGInstance_t    *Instance, 
                 TGParams_t      *CPParams);

// Per-Frequency level adjustable in 1 dB steps from 0 dBm to -19 dBm
// Specify the level parameter as a negative integer from 0 to -19.
ADT_API void TG_ADT_init_1(TGInstance_t    *Instance, 
                  TGParams_1_t    *CPParams);

// Per-Frequency level adjustable in 0.5 dB steps from 0 dBm to -34.5 dBm
// Specify the level parameter as a negative integer from 0 to -345. Values must
// be a multiple of 5. For example a level of -15.5 dBm is specified as -155
ADT_API void TG_ADT_init_highres(TGInstance_t    *Instance, 
                  TGParams_1_t    *CPParams);

// returns the number of samples generated during the call 
ADT_API short int TG_ADT_generate(TGInstance_t   *Instance, 
                          short int       NSamples, 
                          short int       OutSignal[]);


#endif //_TG_USER
