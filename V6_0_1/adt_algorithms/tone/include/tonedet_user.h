/*___________________________________________________________________________
 |
 |   File: tonedet_user.h
 |
 |   This file is the Adaptive Digital Technologies, Inc. 
 |   public header file for the DTMF and MFR1 tone detectors.
 |   
 |   This software is the property of Adaptive Digital Technologies, Inc.
 |   Unauthorized use is prohibited.
 |
 |   Copyright (c) 2000, Adaptive Digital Technologies, Inc.
 |
 |   Version 1.1
 |   February 7, 2000
 |
 |   www.adt-inc.com
 |   610-825-0182
 |___________________________________________________________________________
*/
#ifndef _TONEDET_USER_H
#define _TONEDET_USER_H
#include <common/include/adt_typedef_user.h>
//#define FAST_DETECT

// Define the result (event) codes
#define TD_NULL 0
#define TD_EARLY -1
#define TD_FALSE_EARLY -2
#define TD_LEAD_EDGE -3
#define TD_TRAIL_EDGE -4
#define TD_DIGIT -5
#define TD_VERY_EARLY -6

// Define validity mask bits
// Once a tone has been detected and is still active, the detector may be programmed
// to relax the detector criteria for the duration of the tone.
#define VALID_POWER_B 0			//Enable power threshold check
#define VALID_FREQ_OFFSET_B 1	//Enable frequency offset check
#define VALID_TWIST_B 2			//Enable twist check
#define VALID_SNR_B 3			//Enable SNR check
#define VALID_INDEX_SAME 4		//Enable check to see if index is same as previous


typedef struct 
{
	ADT_Int16 Result;
	ADT_Int16 ToneIndex;
}	TDStatus_t;
#ifdef FAST_DETECT
#define TDINST_SIZE (8+37+14) 
#else
#define TDINST_SIZE 46 
#endif
typedef struct
{
	TDStatus_t Status;
	ADT_Int32 DTMFInstanceSpace[TDINST_SIZE];
} TDInstance_t;

#define MAX_DFT_N_4 184
#define MAX_SCRATCH_SIZE  (MAX_DFT_N_4+16)/4
typedef struct
{
	ADT_Int64 DTMFScratchSpace[MAX_SCRATCH_SIZE];
} TDScratch_t;

ADT_API void DTMF_ADT_initialize(TDInstance_t *DTMFChannel, TDScratch_t *ScratchPtr);
ADT_API void MFR1_ADT_initialize(TDInstance_t *DTMFChannel, TDScratch_t *ScratchPtr);
ADT_API void MFR2F_ADT_initialize(TDInstance_t *DTMFChannel, TDScratch_t *ScratchPtr);
ADT_API void MFR2R_ADT_initialize(TDInstance_t *DTMFChannel, TDScratch_t *ScratchPtr);
ADT_API void CPRG_ADT_initialize(TDInstance_t *DTMFChannel, TDScratch_t *ScratchPtr);

ADT_API void TD_ADT_toneDetect(
				TDInstance_t *DTMFChannel, 
				ADT_Int16 InputSamples[],
				ADT_Int16 FrameSize);

// DTMF_ADT_config is only used to modify the configurable parameters, 
// Default params will be applied if this function is not called.
// It's user's resposibilty to make sure the Config params will meet the DTMF spec
// ADT recommends to use default params which already passed BellCore test.
typedef ADT_Int16 ADT_Flag_t;

typedef struct 
{
	ADT_Flag_t UpdateMinSigLevel; 
	ADT_Int16 Minimum_Signal_Level;

	ADT_Flag_t UpdateSNRFlag;	
	ADT_Int16 SNRFlag;
	
	ADT_Flag_t UpdateMaxFreqDev;
	ADT_Int16 Max_Freq_Deviation;
	
	ADT_Flag_t UpdateMaxTwist;
	ADT_Int16 MaxFwdTwist; 
	ADT_Int16 MaxRevTwist;

	ADT_Flag_t UpdateTrailingEdgeRelaxation;
	ADT_Int16 ValidityMask;

	ADT_Int16 UpdateMaxWaivedCriteria;
	ADT_Int16 MaxWaivedCriteria;
#ifdef FAST_DETECT
	ADT_Int16 UpdateFastDetect;
	ADT_Int16 FastDetect;
#endif
} DTMF_Config_Param_t;

// UpdateMinSigLevel: Flag 
//      Minimum_Signal_Level update flag: no update = 0, update = 1
// Minimum_Signal_Level: 
//	0 means using default value,  -25dbm
//	non-zero negtive value: the minimum power in dbm of signal for DTMF detector to detect.

// UpdateSNRFlag: Flag  
//      SNRFlag update flag: no update = 0, update = 1
// SNRFlag: 
//  0 means using default value,  
//  non-default: 1, loose SNR allow more noisy signal to pass

// UpdateMaxFreqDev:  Flag 
//      Max_Freq_Deviation update flag: no update = 0, update = 1
// Max_Freq_Deviation:
//  0 means using default value, 17 means 1.7%
//  non-default: Set Default to Devation*1000
//				for example, if user want to set deviation to 1.9%
//				the number for this parameter is 19.

// UpdateMaxTwist:  Flag 
//      Max_Freq_Deviation update flag: no update = 0, update = 1
// MaxFwdTwist and MaxRevTwist is specified in dB. A value of 0 means use default value
//				Valid settings range from 4 to 8.

// UpdateTrailingEdgeRelaxation:  Flag 
//      TrailingEdgeRelaxation update flag: no update = 0, update = 1
// ValidityMask is 5 bits flag. 
//
//      bit4       bit3   bit2     bit1         bit0
//      INDEX_SAME  SNR   TWIST  FREQ_OFFSET   POWER
//
//      Value of 0x1F means use default value, all the criteria must be checked
//      Valid settings range from 1 to 0x1F.

// Nominal: if 1, set nominal limits, if 0, set "waive" limits
ADT_API void DTMF_ADT_config(DTMF_Config_Param_t *DTMFConfigParam, short int Nominal);

typedef struct 
{
	ADT_Int16 Minimum_Signal_Level;
	ADT_Int16 SNRFlag;
	ADT_Int16 Max_Freq_Deviation;
	ADT_Int16 MaxFwdTwist; 
	ADT_Int16 MaxRevTwist;
	ADT_Int16 ValidityMask;

} DTMF_Config_Status_t;
// Minimum_Signal_Level: the minimum power in dbm of signal for DTMF detector to detect.

// SNRFlag: 
//  0 means using default value,  
//  non-default: 1, loose SNR allow more noisy signal to pass

// Max_Freq_Deviation: for example, this parameter 19 means deviation is 1.9%

// MaxFwdTwist and MaxRevTwist is specified in dB. Valid settings range from 4 to 8.

// ValidityMask: Trailing edge relaxation control bits setting

ADT_API void DTMF_ADT_getConfigStatus(DTMF_Config_Status_t *DTMFConfigStatus, short int Nominal);




// Tone Suppressor specific definitions

#define NO_SUPPRESSION 0
#define MUTE_AFTER_DETECT 1
#define DEFAULT_SUPPRESSION 2
#define BEST_SUPPRESSION 3

#ifdef FAST_DETECT
#error Fast Detect currently not supported
// DTMFSUPP_ADT_config is only used to modify the configurable parameters, 
// Suppression will be passed to toneSuppress if this function is not called.
// If config is called, ignore passed value, follow settings during config.
typedef struct 
{
	ADT_Int16 SuppressionDuring_VERY_EARLY;
	ADT_Int16 SuppressionDuring_EARLY;
	ADT_Int16 SuppressionDuring_DIGIT;
} DTMFSUPP_Config_Param_t;

ADT_API DTMFSUPP_ADT_config(DTMFSUPP_Config_Param_t *DTMFSUPP_ConfigParam);
#endif

ADT_API void DTMF_ADT_toneSuppress(
 	    void *pToneRelayDt,		//Tone relay instance
		TDInstance_t *DTMFInstance,
		ADT_Int16 InputSamples[],
		ADT_Int16 FrameSize,
		ADT_Int16 Suppression,
		ADT_Int16 OutputSamples[]);

#endif //#ifndef _TONEDET_USER_H

/* ============================== end of file ==================================== */
