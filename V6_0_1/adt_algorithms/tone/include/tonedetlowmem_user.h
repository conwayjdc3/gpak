/*___________________________________________________________________________
 |
 |   File: tonedetlowmem_user.h
 |
 |   This file is the Adaptive Digital Technologies, Inc. 
 |   public header file for the DTMF, MFR1, MFR2 fwd, MFR2 rev, and CallProgress
 |   tone detectors.
 |   
 |   This software is the property of Adaptive Digital Technologies, Inc.
 |   Unauthorized use is prohibited.
 |
 |   Copyright (c) 2000 - 2019, Adaptive Digital Technologies, Inc.
 |
 |   Version 1.1
 |   February 7, 2000
 |
 |   www.adt-inc.com
 |   610-825-0182
 |___________________________________________________________________________
*/
#ifndef _TONEDETLOWMEM_USER_H
#define _TONEDETLOWMEM_USER_H

#include <common/include/adt_typedef_user.h>
#include "arbit_tonedt_opts.h"

// Define the result (event) codes
#define ARBIT_TD_NULL           (0)
#define ARBIT_TD_EARLY          (-1)
#define ARBIT_TD_FALSE_EARLY    (-2)
#define ARBIT_TD_LEAD_EDGE      (-3)
#define ARBIT_TD_TRAIL_EDGE     (-4)
#define ARBIT_TD_DIGIT          (-5)

typedef struct 
{
	ADT_Int16 Result;
	ADT_Int16 ToneIndex;
}	ARBIT_TDStatus_t;


typedef struct
{
	ARBIT_TDStatus_t Status;
#if defined(__LP64__) || defined (_WIN64)
	ADT_Int32 TDInstanceSpace[26];
#else
	ADT_Int32 TDInstanceSpace[22];
#endif
	ADT_Int16 Gor[NUM_DISTINCT_FREQS * 2];
} ARBIT_TDInstance_t;

#endif //#ifndef _TONEDET_USER_H

/* ============================== end of file ==================================== */
