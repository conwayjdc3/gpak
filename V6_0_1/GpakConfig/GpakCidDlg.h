#if !defined(AFX_GPAKCIDDLG_H__3E0BCA9D_50A9_412C_BB37_F64AA3BC906A__INCLUDED_)
#define AFX_GPAKCIDDLG_H__3E0BCA9D_50A9_412C_BB37_F64AA3BC906A__INCLUDED_

#define V23     0
#define BELL202 1

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GpakCidDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// GpakCidDlg dialog

class GpakCidDlg : public CDialog
{
// Construction
public:
	GpakCidDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(GpakCidDlg)
	enum { IDD = IDD_DIALOG_CID };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(GpakCidDlg)
	public:
	virtual int DoModal();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(GpakCidDlg)
	afx_msg void DrawCidWindow();
	afx_msg void OnButtonCidDef();
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GPAKCIDDLG_H__3E0BCA9D_50A9_412C_BB37_F64AA3BC906A__INCLUDED_)
