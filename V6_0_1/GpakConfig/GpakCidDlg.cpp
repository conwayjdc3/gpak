// GpakCidDlg.cpp : implementation file
//

#include "stdafx.h"
#include "gpakconfig.h"
#include "GpakCidDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// GpakCidDlg dialog

extern int type2CID;
GpakCidDlg::GpakCidDlg(CWnd* pParent /*=NULL*/)
	: CDialog(GpakCidDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(GpakCidDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void GpakCidDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(GpakCidDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(GpakCidDlg, CDialog)
	//{{AFX_MSG_MAP(GpakCidDlg)
	ON_BN_CLICKED(IDC_BUTTON_CID_DEF, OnButtonCidDef)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// GpakCidDlg message handlers

int GpakCidDlg::DoModal() 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CDialog::DoModal();
}

void GpakCidDlg::DrawCidWindow() {

   if (CIDConfigDlg.fskType == V23) {
        CheckDlgButton(IDC_RADIO_CID_V23, 1);
        CheckDlgButton(IDC_RADIO_CID_BELL202, 0);
   } else {
        CheckDlgButton(IDC_RADIO_CID_V23, 0);
        CheckDlgButton(IDC_RADIO_CID_BELL202, 1);
   }

   CheckDlgButton (IDC_CHECK_FMTTXCID,        type2CID);
   SetDlgItemInt  (IDC_EDIT_TXCID_NUMPOST,    CIDConfigDlg.numPostambleBytes, UNSIGNED);
   SetDlgItemInt  (IDC_EDIT_TXCID_LEVEL,      CIDConfigDlg.fskLevel, SIGNED);
   SetDlgItemInt  (IDC_EDIT_TXCID_MSG,        CIDConfigDlg.msgType, UNSIGNED);
   SetDlgItemInt  (IDC_EDIT_TXCID_NUMSIEZ,    CIDConfigDlg.numSeizureBytes, UNSIGNED);
   SetDlgItemInt  (IDC_EDIT_TXCID_NUMMARK,    CIDConfigDlg.numMarkBytes, UNSIGNED);


}
void GpakCidDlg::OnButtonCidDef() 
{
	// TODO: Add your control notification handler code here
   CIDConfigDlg.type2CID           = 0;
   CIDConfigDlg.fskLevel           = -10;
   CIDConfigDlg.fskType            = BELL202;
   CIDConfigDlg.msgType            = 0;
   CIDConfigDlg.numMarkBytes       = 23;
   CIDConfigDlg.numPostambleBytes  = 10;
   CIDConfigDlg.numSeizureBytes    = 38;
   DrawCidWindow();
}

BOOL GpakCidDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
    DrawCidWindow();
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void GpakCidDlg::OnOK() 
{
    BOOL ValidInt;  // flag indicating valid integer

    if (IsDlgButtonChecked(IDC_RADIO_CID_V23)) 	 CIDConfigDlg.fskType = V23;

	if (IsDlgButtonChecked(IDC_CHECK_FMTTXCID))  {
        type2CID    = 1; //CIDConfigDlg.type2CID      = 1;
	} else {
		type2CID    = 0;
	}
    CIDConfigDlg.numPostambleBytes = (unsigned int) GetDlgItemInt (IDC_EDIT_TXCID_NUMPOST, &ValidInt, UNSIGNED);
    CIDConfigDlg.msgType           = (unsigned int) GetDlgItemInt (IDC_EDIT_TXCID_MSG, &ValidInt, UNSIGNED);

    CIDConfigDlg.fskLevel        = GetDlgItemInt (IDC_EDIT_TXCID_LEVEL,   &ValidInt, SIGNED);
    CIDConfigDlg.numSeizureBytes = (unsigned int) GetDlgItemInt (IDC_EDIT_TXCID_NUMSIEZ, &ValidInt, UNSIGNED);
    CIDConfigDlg.numMarkBytes    = (unsigned int) GetDlgItemInt (IDC_EDIT_TXCID_NUMMARK, &ValidInt, UNSIGNED);

	CDialog::OnOK();
}
