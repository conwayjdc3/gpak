// ExternalMem.cpp : implementation file
//

#include "stdafx.h"
#include "GpakConfig.h"
#include "ExternalMem.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
extern ExternalMemInfo_t ExtMemConfig;
extern DspTypeDepInfo_t DspTypeInfo;   // selected DSP Type information


/////////////////////////////////////////////////////////////////////////////
// ExternalMem dialog


ExternalMem::ExternalMem(CWnd* pParent /*=NULL*/)
	: CDialog(ExternalMem::IDD, pParent)
{
	//{{AFX_DATA_INIT(ExternalMem)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void ExternalMem::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ExternalMem)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(ExternalMem, CDialog)
	//{{AFX_MSG_MAP(ExternalMem)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ExternalMem message handlers

int ExternalMem::DoModal() {
	
	return CDialog::DoModal();
}

void ExternalMem::OnOK() {
	 if (IsDlgButtonChecked (AlgorithmDMA))
       ExtMemConfig.type = EXT_DMA;
	 else if (IsDlgButtonChecked (Cached))
       ExtMemConfig.type = EXT_CACHE;
    else 
       ExtMemConfig.type = EXT_NONE;

    CDialog::OnOK();
}




BOOL ExternalMem::OnInitDialog() {
	CDialog::OnInitDialog();
   
	CWnd *pObj;			// pointer to Window object

	if ((pObj = GetDlgItem(AlgorithmDMA)) != NULL)	
      pObj->EnableWindow(DspTypeInfo.IsC64x && !DspTypeInfo.IsC64xPlus);

   if (DspTypeInfo.IsC64xPlus && ExtMemConfig.type == EXT_DMA)
      ExtMemConfig.type = EXT_CACHE;

	CheckDlgButton (NoExtMem,     ExtMemConfig.type == EXT_NONE);
   CheckDlgButton (AlgorithmDMA, ExtMemConfig.type == EXT_DMA);
	CheckDlgButton (Cached,       ExtMemConfig.type == EXT_CACHE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


