#include <stdafx.h>
#include "iaecg4.h"

#include "GpakConfig.h"
#pragma warning(disable:4996)

#include <stdio.h>
#include <math.h>
#include <string.h>

#include "sysconfig.h"
#include <sysmem.h>


// AEC library related variables and function prototypes.
extern int aecLibVersion;       // AEC library version
extern void WriteAEC0420Data(FILE *fp);

static void WriteAECData (FILE *fp);
static unsigned int coreCnt = 1;
int frameTaskCnt = 8; // supports at least 1 custom frame
extern int MaxCustomFrameRateMs;

unsigned int BG_SCRATCH_I16 = 0;

static char *IDStr [] = {   "ENCODER_ID", "DECODER_ID"};
static char *StrpacketProfile [] = {   "RTPAVP",  "AAL2Trunking"};
static char *StrcompandingMode [] = {   "cmpPCMU", "cmpPCMA", "cmpNone16", "cmpL8PcmU", "cmpL8PcmA", "cmpNone8" };



#define ADD_STUB

#if (VERS < 0x401)
   #define TONEINSTANCES() SysCfg.maxNumChannels*2
   #define MCASP_ALLOCATION
#else
   #define INSTANCE_ALLOCATION
   #define TONEINSTANCES() (ToneDetInstances * SysCfg.maxToneDetTypes + TxCidDTMFIntances)
#endif

#if (0x401 <= VERS)
   #define INSTANCE_ALLOCATION
   #define MELP
   #define OBJECT_EXTENSIONS
   #define DMA_QUEUE_ALLOCATION
#endif

#if (0x402 <= VERS)
   #define RTP_ALLOCATION
#endif

#if (0x500 <= VERS)
   #define CID_ALLOCATION
   #undef ADD_STUB
#endif

#if (0x501 <= VERS)
   #define COMBINED_SCRATCH
   #define MULTI_CORE
#endif


#if (0x501 == VERS)
   #undef   VERS
   #define VERS 0x500
#endif

#if (0x600 == VERS)
   #undef   VERS
   #define VERS 0x600
#endif

#ifdef OBJECT_EXTENSIONS
   #define OBJ_EXT_66 ".obj"
   #define LIB_EXT_66 ".l66"
   #define OBJ_EXT_67p ".o6740"
   #define LIB_EXT_67p ".l6740"
   #define OBJ_EXT_64p ".o64p"
   #define LIB_EXT_64p ".l64p"
   #define OBJ_EXT_64  ".o64"
   #define LIB_EXT_64  ".l64"
   #define OBJ_EXT_54  ".54obj"
   #define LIB_EXT_54  ".l54"
   #define OBJ_EXT_55  ".o55xh"
   #define LIB_EXT_55  ".l55x"
   #define DMAFILE() "64xAlgDma"
#else
   #define OBJ_EXT_64p ".o64p"
   #define OBJ_EXT_64 ".obj"
   #define OBJ_EXT_54 ".obj"
   #define OBJ_EXT_55 ".obj"
   #define DMAFILE() "GpakDma"
#endif

#define ENABLED_TEXT  "ENABLED"
#define DISABLED_TEXT "             DISABLED"
#define DEMO_TEXT     "        DEMO"

#define HOST_RTP (SysCfg.maxJitterms == 0)

//-------------------------------------------
//  Platfrom globals
static int tsipEnable;
DspTypeDepInfo_t *DspInfo;
sysConfig_t SysCfg;
#define algBits  SysCfg.enab.Bits
#define cdcBits  SysCfg.Codecs.Bits

ADT_Bool WBCodecEnabled;
ADT_Bool IsC54, IsC55, IsC64, IsC64Plus;
char *platform, *libExt;



extern ADT_UInt16 SRTPInstances;
extern ADT_UInt16 SrcInstances;
extern unsigned int AECInstanceSize16, AECScratchSize16;
extern ADT_UInt16 CEDDetInstances;
extern ADT_UInt16 CNGDetInstances;
extern ADT_UInt16 ARBDetInstances;
extern int ARBCfgCount;
extern int ToneDetInstances;
extern int TxCidDTMFIntances;
extern ADT_UInt16 NumT38Instances;
extern ADT_UInt16 NumTIT38Instances;
extern int NumGpakConferences;
extern int NumCustomConferences;

extern NCAN_Params_t NoiseSuppression;
extern int numAtccBlocks;
extern char CompPath[], StubPath[], GpakPath[];

extern ADT_UInt16 shortTailEC;
extern ADT_UInt16 longTailEC;

extern int TSIPEnable[3];

extern ADT_Bool L16Enabled, G722Enabled, G711WBEnabled, L16WBEnabled, SpeexWBEnabled;
extern int MaxPktBufDelayMS;


extern char *identifyActiveComponents (sysConfig_t *cfg, char *rptBuff);
extern void writeComponentStatus (FILE *fp);
extern void writeComponentVersions (FILE *fp);
extern void writeComponentSectionDescriptors (FILE *fp);
extern void addComponentsToCmdFile (FILE *fp);


//
//  These string pointers are set according to the DSP memory layout and 
//  determine where the linker will place vocoder and component sections
//
char *FastData, *FastConst, *cinitData, *SlowDataFirst, *Daram, *FastDataFirst;          // pointer to a string
char *InstanceData;
char *SlowData, *SlowConst, *MediumData;
char *PerCoreData, *Core0Data, *bssData, *HostData, *Core1OnlyData, *AnyCoreData, *NonCacheData;
char *InternalProg, *FastProg, *MediumProg,  *SlowProg;

char *InternalMem, *ExtDmaArea, *MemArea10Ms, *MemArea20Ms, *MemArea22Ms, *MemArea30Ms;
char strBuff1[128], strBuff2[128], strBuff3[128], strBuff4[128], strBuff5[128], strBuff6[128];

typedef struct ecMemSpec_t {
    int InstanceSize;
    int DaStateSize;
    int SaStateSize;
    int EchoPathSize;
    int BackGroundEpSize; 
} ecMemSpec_t;


//{ file output Macros
#define WriteIntField(strc,field)    \
    fprintf (fp,"%6d,\t\t// %s\n",strc->field, #field);

#define WritePtrField(strc,field)    \
    fprintf (fp,"(void *) 0x%X,\t\t// %s\n",strc->field, #field);

#define WriteEnumField(strc,field)    \
    fprintf (fp,"\t%s,\t\t// %s\n", Str##field [strc->field], #field);

#define WriteEnumArrayField(strc,field,index)    \
    fprintf (fp,"\t%s,\t\t// %s[%d]\n", Str##field [strc->field[index]], #field, index);

#define WriteConferenceScratch(bit) \
   if ((SysCfg.cfgFramesChans & bit) != 0) \
       fprintf (fp, "\t&(ConfScratch[%d]),\n", i++); \
   else \
       fprintf (fp, "\t0,\n");

#define WriteToneDetectScratch(bit,framer) \
   if ((SysCfg.cfgFramesChans & bit) != 0) {\
       fprintf (fp, "TDScratch_t TdScratch%s;\n", framer); \
       fprintf (fp, "#pragma DATA_ALIGN (TdScratch%s, 8);\n", framer); \
   } else \
       fprintf (fp, "ADT_UInt16 TdScratch%s;\n", framer);

#define WriteCidScratch(bit,framer, len) \
   if ((SysCfg.cfgFramesChans & bit) != 0) {\
       fprintf (fp, "ADT_Int32 cidScratch%s[%d];\n", framer,len); \
   } else \
       fprintf (fp, "ADT_Int32 cidScratch%s[1];\n", framer);

#define WriteFrameBit(value) \
   if ((SysCfg.cfgFramesChans & FRAME_##value##_CFG_BIT) != 0) \
       fprintf (fp, "\t%5.1f", value/8.0); 

#define WriteChannelTypeBit(Type) \
   if ((SysCfg.cfgFramesChans & ##Type##_CFG_BIT) != 0) \
       fprintf (fp, "\t%s", #Type); 

#define WriteIntArray(strc,field,size)    \
    fprintf (fp,"{");                       \
    for (i=0; i<size-1; i++) fprintf (fp,"%6d,",strc->field[i]);  \
    fprintf (fp,"%6d},\t\t// %s[ ]\n",strc->field[i], #field);

#define writeElementNameArray(size, prefix, element)      \
   for (ii = 0; (ii + 1) < size; ii++) fprintf (fp, "\t%s%s%d,\n", prefix, element, ii);     \
   fprintf (fp, "\t%s%s%d\n};\n\n", prefix, element, ii);

#define writeElementPtrArray(size, name)        \
   for (ii = 0; (ii + 1) < size; ii++) fprintf (fp, "\t&(%s[%d]),\n", name, ii); \
   fprintf (fp, "\t&(%s[%d])\n};\n", name, ii);

#define Align(field, size, section)  \
    fprintf (fp,"#pragma DATA_SECTION (%s, %s)\n", #field, #section);     \
    if (size != 0)                                                        \
       fprintf (fp, "#pragma DATA_ALIGN   (%s, %d)\n", #field, size);       

#define AlignSection(field, section, size)  \
    if (!(IsC64 || IsC55))                                               \
       fprintf (fp,"#pragma DATA_SECTION (%s,\"%s\")\n", #field, #section);     \
    else if (size != 0)                                                        \
       fprintf (fp, "#pragma DATA_ALIGN (%s, %d)\n", #field, size);       

#define ArrayAlign(prefix, field, element, c54size, c64size, c55size)        \
   fprintf (fp, "#pragma DATA_SECTION (%s%s%d,\"%s%s\")\n",        \
               prefix, #field, element, prefix, #field, element);   \
   if (IsC64)       align->field = c64size;            \
   else if (IsC55)  align->field = c55size;            \
   else             align->field = c64size;



#define pragmaSect(varName, sectName) fprintf (fp,"#pragma DATA_SECTION (%s,\"%s\")\n", #varName, #sectName)
#define createArray(arrayName, arrayLen) fprintf (fp,"ADT_UInt16 %s[%d];\n",#arrayName, arrayLen)

#define create3Arrays(arrayName, ext, arrayLen, ptrName)  \
    fprintf (fp,"ADT_UInt16 %s0_%s[%d];\n",#arrayName, ext, arrayLen);   \
    fprintf (fp,"ADT_UInt16 %s1_%s[%d];\n",#arrayName, ext, arrayLen);   \
    fprintf (fp,"ADT_UInt16 %s2_%s[%d];\n",#arrayName, ext, arrayLen);   \
    fprintf (fp,"#pragma DATA_SECTION (%s0_%s, \"%s0_%s\")\n", #arrayName, ext, #arrayName, ext); \
    fprintf (fp,"#pragma DATA_SECTION (%s1_%s, \"%s1_%s\")\n", #arrayName, ext, #arrayName, ext); \
    fprintf (fp,"#pragma DATA_SECTION (%s2_%s, \"%s2_%s\")\n", #arrayName, ext, #arrayName, ext); \
    fprintf (fp,"ADT_UInt16 *%s_%s[] = \n", #ptrName, ext); \
    fprintf (fp,"            {%s0_%s, %s1_%s, %s2_%s};\n\n",#arrayName, ext, #arrayName, ext, #arrayName, ext);
   
#define StructureAlign(Struct, alignI8) fprintf (fp, "#pragma STRUCT_ALIGN (%-20s, %u)\n", #Struct, alignI8);

#define writeDummyScratch(rate)                           \
   fprintf (fp, "\nADT_UInt16 G168DAscratch_%sms;\n"  \
               "union {\n"                       \
               "   ADT_UInt16 TDScratch;\n"      \
               "   ADT_UInt16 CnfrScratch;\n"    \
               "   ADT_UInt16 SAScratch;\n"      \
               "   ADT_UInt16 SRTPScratch;\n"    \
               "} G168SAscratch_%sms;\n",        \
              #rate, #rate); \
   fprintf (fp, "#pragma DATA_SECTION (G168DAscratch_%sms, \"SLOW_SCRATCH\")\n", #rate); \
   fprintf (fp, "#pragma DATA_SECTION (G168SAscratch_%sms, \"SLOW_SCRATCH\")\n\n", #rate); 

// Echo canceller memory related definitions.
#define writeECMipsConserve(rate) \
    fprintf (fp, "MipsConserve_t G168MipsConserve_%sms;\n",#rate); \
    fprintf (fp, "const ADT_UInt16 G168SAscratch_%smsLenI16 = sizeof(G168SAscratch_%sms)/sizeof(ADT_UInt16);\n\n",#rate, #rate); \
    fprintf (fp, "const ADT_UInt16 G168DAscratch_%smsLenI16 = sizeof(G168DAscratch_%sms)/sizeof(ADT_UInt16);\n\n",#rate, #rate); 

#ifdef MULTI_CORE   // framing task buffers
   #define frameSects(framesize) \
      fprintf (fp,"#pragma DATA_SECTION (inWork_%smsec,    \"IN_WORK\")\n",    framesize); \
      fprintf (fp,"#pragma DATA_SECTION (outWork_%smsec,   \"OUT_WORK\")\n",   framesize); \
      fprintf (fp,"#pragma DATA_SECTION (ECFarWork_%smsec, \"ECFAR_WORK\")\n", framesize);
#else
   #define frameSects(framesize) \
       fprintf (fp,"#pragma DATA_SECTION (inWork_%smsec,    \"inWork_%smsec\")\n",    framesize, framesize); \
       fprintf (fp,"#pragma DATA_SECTION (outWork_%smsec,   \"outWork_%smsec\")\n",   framesize, framesize); \
       fprintf (fp,"#pragma DATA_SECTION (ECFarWork_%smsec, \"ECFarWork_%smsec\")\n", framesize, framesize);
#endif

#define allocBuffers(framesize, in, out, echo) \
  { fprintf (fp, BufferFormat, framesize, in, framesize, out, framesize, echo);  \
    if (IsC64) fprintf (fp, AlignFormat, framesize, framesize, framesize); \
    frameSects(framesize); \
  }

//}


//{  #defines
#define MAX(a, b) (a > b)  ? a : b

#define FRAME_1ms_ENABLED  (SysCfg.cfgFramesChans & FRAME_8_CFG_BIT)
#define FRAME_2ms_ENABLED  (SysCfg.cfgFramesChans & FRAME_20_CFG_BIT)
#define FRAME_5ms_ENABLED  (SysCfg.cfgFramesChans & FRAME_40_CFG_BIT)
#define FRAME_10ms_ENABLED (SysCfg.cfgFramesChans & FRAME_80_CFG_BIT)
#define FRAME_20ms_ENABLED (SysCfg.cfgFramesChans & FRAME_160_CFG_BIT)
#define FRAME_22ms_ENABLED (SysCfg.cfgFramesChans & FRAME_180_CFG_BIT)
#define FRAME_30ms_ENABLED (SysCfg.cfgFramesChans & FRAME_240_CFG_BIT)
#define NOISE_SUPPRESS_ALIGN 16

#define RoundForDMA(Words) Words = Words + (2 * numAtccBlocks) - (Words % (2 * numAtccBlocks));

#define nextPower2(x,temp)  \
      y = (log ((double) (x + 1))) / log2;  \
      z = (double) ((int) (y + 1.0));      \
      temp = (int) (pow (2.0, z));          

#define FREE            0
#define ASSIGNED        1
#define OPEN            0
#define CLOSED          1

#define ENCODE_BIT 1
#define DECODE_BIT 2

#define MemSpec(vocoder, system)  { \
     if (vocoder->type == DARAM)                system.type     = DARAM;            \
     if (vocoder->id   == ENCODER_ID)           memoryTypes |= ENCODE_BIT;          \
     if (vocoder->id   == DECODER_ID)           memoryTypes |= DECODE_BIT;          \
     if (vocoder->align    > system.align)      system.align = vocoder->align;      \
     if ((vocoder->Words + 1) > system.Words)   system.Words = vocoder->Words + 1;  \
     if (vocoder->Scratch  > system.Scratch)    system.Scratch = vocoder->Scratch; }


//}

unsigned int internalQueueCnt = 0;
unsigned int MaxFrameSize;   // maximum Frame Size
unsigned int NumFrameRates;      // number of framing rates (tasks)

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//   Identify the size of memory (C54X:16-bit words, C64X:8-bit bytes) to allocate 
//   per/channel for each vocoder.  Memory allocated from these structures is used
//   to hold encoder and decoder channel data that needs to be maintained between frames.
//
// Note: 1) Set the default alignment to 2 to ensure that C structures occur on
//       even word boundaries.
//       2) Make sure that each C64x vocoder instance length is an even multiple
//       of 4-bytes to meet DSP's algorithm DMA 32-bit element requirement.
//

typedef struct memSpec_t {
    enum memID      id;
    unsigned int    Words;
    int             type; 
    int             align;
    unsigned int    Scratch;
} memSpec_t;

//{    Vocoder instance memory specs
//                                     MEM ID      INSTANCE TYPE  ALIGN SCRATCH 
//                                C54               (WORDS)              (WORDS)
//                                C55               (WORDS)              (WORDS)
//                                C64               (BYTES)              (WORDS)

//  G723  (C55x)
const memSpec_t g723C55EncMem       = {ENCODER_ID,    740,  DARAM,  2,     992};
const memSpec_t g723C55DecMem       = {DECODER_ID,    214,  DARAM,  2,     860};

//  G723  (C54x and C64x)
const memSpec_t g723C54EncMem       = {ENCODER_ID,    622,  DARAM,  2,     0};
const memSpec_t g723C54DecMem       = {DECODER_ID,    191,  DARAM,  2,     0};

const memSpec_t g723C64EncMem =       {ENCODER_ID,   1420,  SARAM,  8,    4880 / 2};
const memSpec_t g723C64DecMem =       {DECODER_ID,    400,  SARAM,  8,    2432 / 2};

// TI's C64x plus G7231A implementation
const memSpec_t TI_g7231AC64EncMem =  {ENCODER_ID,   1568, SARAM, 8,    3314 / 2};
const memSpec_t TI_g7231AC64DecMem =  {DECODER_ID,   440, SARAM, 8,    1672 / 2};

//  G726  (C55x LoMem,  C55x LoMips)
const memSpec_t g726C55EncLoMemMem  = {ENCODER_ID,    128, SARAM, 0,     0};
const memSpec_t g726C55DecLoMemMem  = {DECODER_ID,    128, SARAM, 0,     0};

const memSpec_t g726C55EncLoMipsMem = {ENCODER_ID,    128, SARAM, 0,     0};
const memSpec_t g726C55DecLoMipsMem = {DECODER_ID,    128, SARAM, 0,     0};
//  G726  (C54x LoMem,  C54x LoMips,  C64x)
const memSpec_t g726C54EncLoMemMem  = {ENCODER_ID,    128, SARAM, 128,     0};
const memSpec_t g726C54DecLoMemMem  = {DECODER_ID,    128, SARAM, 128,     0};

const memSpec_t g726C54EncLoMipsMem = {ENCODER_ID,    128, SARAM, 128,     0};
const memSpec_t g726C54DecLoMipsMem = {DECODER_ID,    128, SARAM, 128,     0};

const memSpec_t g726C64EncMem       = {ENCODER_ID,     128, SARAM,   8,     0};
const memSpec_t g726C64DecMem       = {DECODER_ID,     128, SARAM,   8,     0};

//  G728 (C54x and C64x)
const memSpec_t g728C54Mem          = {ENCODER_ID,   1792, DARAM, 256,     0};
const memSpec_t g728C55Mem          = {ENCODER_ID,   1792, DARAM, 256,     0};

const memSpec_t g728C64EncMem       = {ENCODER_ID,   1448, SARAM,   8,     412/2};
const memSpec_t g728C64DecMem       = {DECODER_ID,   2144, SARAM,   8,     412/2};

//  G729AB (C55x)
const memSpec_t g729ABC55EncMem     = {ENCODER_ID,    1078, SARAM, 4,      1208};
const memSpec_t g729ABC55DecMem     = {DECODER_ID,    924, SARAM, 4,       218};
//  G729AB (C54x and C64x)
const memSpec_t g729ABC54EncMem     = {ENCODER_ID,    940, SARAM, 4,       0};
const memSpec_t g729ABC54DecMem     = {DECODER_ID,    848, SARAM, 4,       0};

const memSpec_t g729ABC64EncMem    =  {ENCODER_ID,   2148, SARAM, 8,    2400 / 2};
const memSpec_t g729ABC64DecMem    =  {DECODER_ID,   2200, SARAM, 8,     800 / 2};

// TI's C64x plus G729 implementation
const memSpec_t TI_g729ABC64EncMem =  {ENCODER_ID,   1508, SARAM, 8,    2616 / 2};
const memSpec_t TI_g729ABC64DecMem =  {DECODER_ID,   1248, SARAM, 8,    2048 / 2};

//  ADT4800 (C54x)
const memSpec_t adt4800C54EncMem    = {ENCODER_ID,    622, DARAM, 2,       0};
const memSpec_t adt4800C54DecMem    = {DECODER_ID,    191, DARAM, 2,       0};

//  AMR (C54x)
const memSpec_t AMR_54EncMem =     {ENCODER_ID,      1016, SARAM, 4,       0};
const memSpec_t AMR_54DecMem =     {DECODER_ID,       648, SARAM, 4,       0};

//  AMR (C55x)
const memSpec_t AMR_55EncMem =     {ENCODER_ID,      998, SARAM, 4,       3212};
const memSpec_t AMR_55DecMem =     {DECODER_ID,       658, SARAM, 4,       600};

//  AMR (C64x)
const memSpec_t AMR_64EncMem =     {ENCODER_ID,      2096, SARAM, 8,       9280/2};
const memSpec_t AMR_64DecMem =     {DECODER_ID,      1232, SARAM, 8,       1392/2};

//  MELP (C54x)
const memSpec_t Melp_54EncMem =     {ENCODER_ID,         0, SARAM, 4,       0};
const memSpec_t Melp_54DecMem =     {DECODER_ID,         0, SARAM, 4,       0};

//  MELP (C55x)
const memSpec_t Melp_55EncMem =     {ENCODER_ID,         1242, SARAM, 4,    1540};
const memSpec_t Melp_55DecMem =     {DECODER_ID,         0, SARAM, 4,       1540};


//  MELP (C64x)
const memSpec_t Melp_64EncMem =     {ENCODER_ID,      450, SARAM, 8,   3064/2};
const memSpec_t Melp_64DecMem =     {DECODER_ID,      908, SARAM, 8,   3064/2};

//  MELPE (C64x)
const memSpec_t MelpE_64EncMem =    {ENCODER_ID,     6560, SARAM, 8,   5672/2};
const memSpec_t MelpE_64DecMem =    {DECODER_ID,     1376, SARAM, 8,   5672/2};

//  MELPE (C64xp)
const memSpec_t MelpE_64pEncMem =   {ENCODER_ID,     6560, SARAM, 8,   6984/2};
const memSpec_t MelpE_64pDecMem =   {DECODER_ID,     1376, SARAM, 8,   6984/2};
//                                     MEM ID      INSTANCE TYPE  ALIGN SCRATCH 
//                                C64               (BYTES)              (WORDS)

//  G722 (C64x)
const memSpec_t G722_64EncMem =     {ENCODER_ID,      192+568, SARAM, 8,   0};
const memSpec_t G722_64DecMem =     {DECODER_ID,      192+568, SARAM, 8,   0};

const memSpec_t Speex_64EncMem =    {ENCODER_ID,      6470,    SARAM, 8,  32040};
const memSpec_t Speex_64DecMem =    {DECODER_ID,      5780,    SARAM, 8,  16040};

const memSpec_t SpeexWB_64EncMem =  {ENCODER_ID,      7720,    SARAM, 8,  32040};
const memSpec_t SpeexWB_64DecMem =  {DECODER_ID,      6850,    SARAM, 8,  16040};

//  G711A1A2 (C64x)
const memSpec_t G711A1A2_64EncMem = {ENCODER_ID,      (1672+16), SARAM, 8,   632/2};
const memSpec_t G711A1A2_64DecMem = {DECODER_ID,      1804, SARAM, 8,   624/2};

//const memSpec_t RateConvert_64EncMem =     {ENCODER_ID,      568, SARAM, 8,   0};
//const memSpec_t G722_64DecMem =     {DECODER_ID,       568, SARAM, 8,   0};

//}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{     Vocoder section memory requirements
//
//  Identifies section names, alignment and memory placement (DARAM, SARAM, etc)
//
typedef struct SectInfo_t {
   char **memName;
   char *Name;
} SectInfo_t;

SectInfo_t NullSection [] = { 
   { NULL, NULL } 
};

// G723 (C54x and C64x)
SectInfo_t g723C54FastMem [] = { 
   { &FastDataFirst, "GC128DI  : {*(C128DI)} ALIGN(128)" },
   { NULL, NULL } 
};
SectInfo_t g723C54SlowMem [] = { 
   { &MediumData, "GC1024E  : {*(C1024E)} ALIGN(1024)" },
   { &MediumData, "GSARAMD  : {*(SARAMD)}" },
   { NULL, NULL } 
};
SectInfo_t g723C64FastMem [] = { 
   { &FastProg, "GROUP (G723_PROG) : RUN_START(_Start$G723Prog), RUN_END(_End$G723Prog) {\n"
                "\t\t.G723_COM\n"
                "\t\t.G723_DEC\n"
                "\t\t.G723_ENC\n"
                "\t\t.G723_ENC_R63\n"
                "\t\t.G723_ENC_R53\n"
                "  }" },
   { NULL, NULL } 
};

SectInfo_t g723C64SlowMem [] = { 
   { &SlowConst, "GROUP (G723_TABLES) : RUN_START(_Start$G723Data), RUN_END(_End$G723Data) {\n"
                 "\t\tG723_ADT_TABLE1\n"
                 "\t\tG723_ADT_TABLE2\n"
                 "\t\tG723_ADT_TABLE3\n"
                 "\t\tG723_ADT_TABLE4\n"
                 "\t\tG723_ADT_TABLE5\n"
                 "  }"  },
   { NULL, NULL } 
};

SectInfo_t ti_g723C64FastMem [] = {
   { &FastProg,  "GROUP (G7231A_PROG) : RUN_START(_Start$G723FastProg), RUN_END(_End$G723FastProg) {\n"
      "\t\t.text:G7231AEncAlgAlloc:_G7231AENC_TII_alloc\n"
      "\t\t.text:G7231AEncAlgFree:_G7231AENC_TII_free\n"
      "\t\t.text:G7231AEncAlgInit:_G7231AENC_TII_initObj\n"
      "\t\t.text:G7231AEncNumAlloc:_G7231AENC_TII_numAlloc\n"
      "\t\t.text:G7231AEncAlgMoved:_G7231AENC_TII_moved\n"
      "\t\t.text:INIT:_G7231A_TII_encgetbufs\n"
      "\t\t.text:INIT:_G7231A_TII_encgetbase\n"
      "\t\t.text:INIT:_G7231A_TII_initencinst\n"
      "\t\t.text:INIT:_G7231A_TII_new_encoder\n"
      "\t\t.text:INIT:_G7231A_TII_encReset\n"
      "\t\t.text:ENC:_G7231AENC_TII_g7231aEncode\n"
      "\t\t.text:ENC:_G7231AENC_TII_g7231aControl\n"
      "\t\t.text:ENC:_G7231A_TII_g7231aEncoder\n"
      "\t\t.text:ENC:_G7231A_TII_Cod_Cng\n"
      "\t\t.text:COM:_G7231A_TII_Calc_Exc_Rand\n"
      "\t\t.text:ENC:_G7231A_TII_ComputePastAvFilter\n"
      "\t\t.text:COM:_G7231A_TII_DecSidGain\n"
      "\t\t.text:ENC:_G7231A_TII_CalcRC\n"
      "\t\t.text:ENC:_G7231A_TII_LpcDiff\n"
      "\t\t.text:COM:_G7231A_TII_Qua_SidGain\n"
      "\t\t.text:ENC:_G7231A_TII_Mem_Shift\n"
      "\t\t.text:ENC:_G7231A_TII_Error_Wght\n"
      "\t\t.text:COM:_G7231A_TII_Vec_Norm\n"
      "\t\t.text:ENC:_G7231A_TII_Estim_Pitch\n"
      "\t\t.text:ENC:_G7231A_TII_Comp_Pw\n"
      "\t\t.text:ENC:_G7231A_TII_Filt_Pw\n"
      "\t\t.text:COM:_G7231A_TII_Lsp_Inq\n"
      "\t\t.text:COM:_G7231A_TII_LsptoA\n"
      "\t\t.text:ENC:_G7231A_TII_Find_Best\n"
      "\t\t.text:ENC:_G7231A_TII_Fcbk_Pack\n"
      "\t\t.text:COM:_G7231A_TII_Gen_Trn\n"
      "\t\t.text:ENC:_G7231A_TII_Line_Pack\n"
      "\t\t.text:COM:_G7231A_TII_Lsp_Int\n"
      "\t\t.text:ENC:_G7231A_TII_Wght_Lpc\n"
      "\t\t.text:ENC:_G7231A_TII_Find_Fcbk_10ms\n"
      "\t\t.text:ENC:_G7231A_TII_D4i64_LBC_10ms\n"
      "\t\t.text:ENC:_G7231A_TII_ACELP_LBC_code_10ms\n"
      "\t\t.text:ENC:_G7231A_TII_Coder10ms_FrmIndx0\n"
      "\t\t.text:ENC:_G7231A_TII_Coder10ms_FrmIndx2\n"
      "\t\t.text:ENC:_G7231A_TII_Find_Fcbk\n"
      "\t\t.text:ENC:_G7231A_TII_ACELP_LBC_code\n"
      "\t\t.text:ENC:_G7231A_TII_D4i64_LBC\n"
      "\t\t.text:ENC:_G7231a_TII_D4i64_lbc_loop\n"
      "\t\t.text:ENC:_G7231A_TII_Cor_h_X\n"
      "\t\t.text:ENC:_G7231A_TII_Cor_h\n"
      "\t\t.text:ENC:_G7231A_TII_G_code\n"
      "\t\t.text:COM:_G7231A_TII_Decod_Acbk\n"
      "\t\t.text:COM:_G7231A_TII_Get_Rez\n"
      "\t\t.text:ENC:_G7231A_TII_Update_Err\n"
      "\t\t.text:ENC:_G7231A_TII_Upd_Ring\n"
      "\t\t.text:ENC:_G7231A_TII_Comp_Ir\n"
      "\t\t.text:ENC:_G7231A_TII_Sub_Ring\n"
      "\t\t.text:ENC:_G7231A_TII_Find_Acbk\n"
      "\t\t.text:ENC:_G7231A_TII_Coder10ms_FrmIndx1\n"
      "\t\t.text:COM:_G723_TII_copyloop\n"
      "\t\t.text:COM:_G723_TII_copyloopalligned\n"
      "\t\t.text:INIT:_G7231A_TII_Enc_Init\n"
      "\t\t.text:INIT:_G7231A_TII_Init_Vad\n"
      "\t\t.text:INIT:_G7231A_TII_Init_Cod_Cng\n" 
      "\t\t.text:ENC:_G7231A_TII_Encode\n"
      "\t\t.text:ENC:_G7231A_TII_Rem_Dc\n"
      "\t\t.text:ENC:_G7231A_TII_Comp_Lpc\n"
      "\t\t.text:ENC:_G7231A_TII_Durbin\n"
      "\t\t.text:ENC:_G7231A_TII_Update_Acf\n"
      "\t\t.text:ENC:_G7231A_TII_AtoLsp\n"
      "\t\t.text:ENC:_G7231A_TII_Comp_Vad\n"
      "\t\t.text:ENC:_G7231A_TII_Lsp_Qnt\n"
      "\t\t.text:ENC:_G7231A_TII_Lsp_Svq\n"
        "\t\t.text:g7231aDecAlgAlloc:_G7231ADEC_TII_alloc\n"
        "\t\t.text:g7231aDecAlgFree:_G7231ADEC_TII_free\n"
        "\t\t.text:g7231aDecAlgInit:_G7231ADEC_TII_initObj\n"  
        "\t\t.text:g7231aDecNumAlloc:_G7231ADEC_TII_numAlloc\n"
        "\t\t.text:g7231aDecAlgMove:_G7231ADEC_TII_moved\n"
        "\t\t.text:INIT:_G7231A_TII_decgetbufs\n"
        "\t\t.text:INIT:_G7231A_TII_decgetbase\n"
        "\t\t.text:INIT:_G7231A_TII_new_decoder\n"
        "\t\t.text:INIT:_G7231A_TII_init_decinst\n"
        "\t\t.text:INIT:_G7231A_TII_decReset\n"
        "\t\t.text:DEC:_G7231ADEC_TII_g7231aDecode\n"
        "\t\t.text:DEC:_G7231ADEC_TII_g7231aControl\n"
        "\t\t.text:DEC:_G7231A_TII_g7231aDecoder\n"
        "\t\t.text:DEC:_G7231A_TII_Comp_Info\n"  
        "\t\t.text:DEC:_G7231A_TII_Comp_Lpf\n"
        "\t\t.text:DEC:_G7231A_TII_Dec_Cng\n"
        "\t\t.text:DEC:_G7231A_TII_Dec_Init\n"
        "\t\t.text:DEC:_G7231A_TII_Decode\n"
        "\t\t.text:DEC:_G7231A_TII_Fcbk_Unpk\n"
        "\t\t.text:DEC:_G7231A_TII_Filt_Lpf\n"
        "\t\t.text:DEC:_G7231A_TII_Find_B\n"
        "\t\t.text:DEC:_G7231A_TII_Find_F\n"
        "\t\t.text:DEC:_G7231A_TII_Get_Ind\n"
        "\t\t.text:DEC:_G7231A_TII_Line_Unpk\n"
        "\t\t.text:DEC:_G7231A_TII_Regen\n"
		"\t\t.text:DEC:_G7231A_TII_Synt\n"
        "\t\t.text:DEC:_G7231A_TII_Scale\n"
        "\t\t.text:DEC:_G7231A_TII_Spf\n"
                 "  }" },
   { NULL, NULL } 
};

SectInfo_t ti_g723C64SlowMem [] = {
   { &FastConst,  "GROUP (G7231A_TABLES) : RUN_START(_Start$G723Data), RUN_END(_End$G723Data) {\n"
                 "\t\t.const:g723_tii_tables_CirBuf \n"
                 "\t\t.const:g723_tii_table2\n"
                 "\t\t.const:g723_tii_table3\n"
                 "\t\t.const:g723_tii_table4\n"
                 "\t\t.const:g723_tii_table5\n"
                 "\t\t.const:g723_tii_table6\n"
                 "\t\t.const:g723_tii_table7\n"
                 "\t\t.const:g723_tii_table8\n"
                 "  }  " },
   { NULL, NULL } 
};

SectInfo_t ti_g729C64EncodeMem [] = {
   { &FastProg,  "GROUP (G729AB_ENCODE_PROG) : RUN_START(_Start$G729EncProg), RUN_END(_End$G729EncProg)  {\n"
                 "\t\t.text:_G729ABENC_TII_g729abEncode\n"
                 "\t\t.text:_G729AB_TII_g729abEncoder\n"
                 "\t\t.text:_G729AB_TII_encoder\n"
                 "\t\t.text:_G729AB_TII_copy160\n"
                 "\t\t.text:_G729AB_TII_hpfilter\n"
                 "\t\t.text:_G729AB_TII_autocorr\n"
                 "\t\t.text:_G729AB_TII_lagwindow\n"
                 "\t\t.text:_G729AB_TII_levinsondurbin\n"
                 "\t\t.text:_G729AB_TII_lpctolsp\n"
                 "\t\t.text:_G729AB_TII_lsptolsf\n"
                 "\t\t.text:_G729AB_TII_compvad\n"
                 "\t\t.text:_G729AB_TII_log2\n"
                 "\t\t.text:_G729AB_TII_copy11\n"
                 "\t\t.text:_G729AB_TII_acf32to16\n"
                 "\t\t.text:_G729AB_TII_updatecngsumacf\n"
                 "\t\t.text:_G729AB_TII_calcsumacf\n"
                 "\t\t.text:_G729AB_TII_copy10\n"
                 "\t\t.text:_G729AB_TII_lsptolsf2\n"
                 "\t\t.text:_G729AB_TII_quantlsf\n"
                 "\t\t.text:_G729AB_TII_getmseweights\n"
                 "\t\t.text:_G729AB_TII_lsfprevextract\n"
                 "\t\t.text:_G729AB_TII_lsfselect\n"
                 "\t\t.text:_G729AB_TII_lsfexpand\n"
                 "\t\t.text:_G729AB_TII_lsfgetdist\n"
                 "\t\t.text:_G729AB_TII_lsflastselect\n"
                 "\t\t.text:_G729AB_TII_unqlsf\n"
                 "\t\t.text:_G729AB_TII_getlsfvec\n"
                 "\t\t.text:_G729AB_TII_lsfprevcompose\n"
                 "\t\t.text:_G729AB_TII_lsfstability\n"
                 "\t\t.text:_G729AB_TII_lsfprevupdate\n"
                 "\t\t.text:_G729AB_TII_lsftolsp2\n"
                 "\t\t.text:_G729AB_TII_interplsptolpc\n"
                 "\t\t.text:_G729AB_TII_interplsp\n"
                 "\t\t.text:_G729AB_TII_lsptolpc\n"
                 "\t\t.text:_G729AB_TII_weightlpc\n"
                 "\t\t.text:_G729AB_TII_residu1\n"
                 "\t\t.text:_G729AB_TII_smoothlpc\n"
                 "\t\t.text:_G729AB_TII_copy143\n"
                 "\t\t.text:_G729AB_TII_synfilt\n"
                 "\t\t.text:_G729AB_TII_scalwsp\n"
                 "\t\t.text:_G729AB_TII_olpitchsearch\n"
                 "\t\t.text:_G729AB_TII_invsqrt\n"
                 "\t\t.text:_G729AB_TII_copy154\n"
                 "\t\t.text:_G729AB_TII_setzero40align\n"
                 "\t\t.text:_G729AB_TII_setzero20\n"
                 "\t\t.text:_G729AB_TII_clpitchsearch\n"
                 "\t\t.text:_G729AB_TII_corrirtarget\n"
                 "\t\t.text:_G729AB_TII_dotproduct\n"
                 "\t\t.text:_G729AB_TII_Dot_Product_temp\n"
                 "\t\t.text:_G729AB_TII_fracpredictionenc\n"
                 "\t\t.text:_G729AB_TII_copy1\n"
                 "\t\t.text:_G729AB_TII_encpitchsf0\n"
                 "\t\t.text:_G729AB_TII_setzero10\n"
                 "\t\t.text:_G729AB_TII_comppitchgain\n"
                 "\t\t.text:_G729AB_TII_tamepitchgain\n"
                 "\t\t.text:_G729AB_TII_compcbtarget\n"
                 "\t\t.text:_G729AB_TII_acelpsearch\n"
                 "\t\t.text:_G729AB_TII_pitchemphasis\n"
                 "\t\t.text:_G729AB_TII_compcorrmatrix\n"
                 "\t\t.text:_G729AB_TII_sign\n"
                 "\t\t.text:_G729AB_TII_D4i40_17_fast\n"
                 "\t\t.text:_G729AB_TII_corrtargexc\n"
                 "\t\t.text:_G729AB_TII_predcodegain\n"
                 "\t\t.text:_G729AB_TII_gainsvqsearch\n"
                 "\t\t.text:_G729AB_TII_updatequantenergy\n"
                 "\t\t.text:_G729AB_TII_comptotalexc\n"
                 "\t\t.text:_G729AB_TII_updateexcerror\n"
                 "\t\t.text:_G729AB_TII_updatepitchtargetmem\n"
                 "\t\t.text:_G729AB_TII_encpitchsf1\n"
                 "\t\t.text:_G729AB_TII_pack\n"
                 "\t\t.text:_G729AB_TII_quantsidlsf\n"
                 "\t\t.text:_G729AB_TII_calcrcoeff\n"
                 "\t\t.text:_G729AB_TII_itakura\n"
                 "\t\t.text:_G729AB_TII_smoothlpcnoise\n"
                 "\t\t.text:_G729AB_TII_arrangesidlsf\n"
                 "\t\t.text:_G729AB_TII_subvector\n"
                 "\t\t.text:_G729AB_TII_acf16to32\n"
                 "\t\t.text:_G729AB_TII_unpack\n"
                 "\t\t.text:_G729AB_TII_codcng\n"
                 "\t\t.text:_G729AB_TII_initencoder\n"
                 "\t\t.text:_G729AB_TII_createencoder\n"
                 "\t\t.text:_G729AB_TII_compaddcngcbgain\n"
                 "\t\t.text:_G729AB_TII_genrandcbexc\n"
                 "\t\t.text:_G729AB_TII_quasidgain\n"
                 "\t\t.text:_G729AB_TII_sumscalcngexc\n"
                 "\t\t.text:_G729AB_TII_genrandparams\n"
                 "\t\t.text:_G729AB_TII_setzero\n"
                 "\t\t.text:_G729AB_TII_divs\n"
                 "\t\t.text:_G729AB_TII_interpsidgain\n"
                 "\t\t.text:_G729AB_TII_initoldlsf\n"
                 "\t\t.text:_G729AB_TII_setzero40\n"
                 "\t\t.text:_G729AB_TII_randomnumgen\n"
                 "\t\t.text:_G729ABENC_TII_g729abControl\n"
                 "\t\t.text:_G729AB_TII_encgetbufs\n"
                 "\t\t.text:_G729AB_TII_new_encoder\n"
                 "\t\t.text:_G729ABENC_TII_initObj\n"
                 "\t\t.text:_G729ABENC_TII_free\n"
                 "\t\t.text:_G729AB_TII_encgetbase\n"
                 "\t\t.text:_G729AB_TII_encReset\n"
                 "\t\t.text:_G729ABENC_TII_moved\n"
                 "\t\t.text:_G729ABENC_TII_numAlloc\n"
                 "\t\t.text:_G729ABENC_TII_alloc\n"
                 "  }" },
   { NULL, NULL } 
};

SectInfo_t ti_g729C64DecodeMem [] = {
   { &FastProg,  "GROUP (G729AB_DECODE_PROG) : RUN_START(_Start$G729DecPog), RUN_END(_End$G729DecProg) {\n"
                 "\t\t.text:_G729ABDEC_TII_g729abDecode\n"
                 "\t\t.text:_G729AB_TII_decoder\n"
                 "\t\t.text:_G729AB_TII_deccng\n"
                 "\t\t.text:_G729AB_TII_fracpredictiondec\n"
                 "\t\t.text:_G729AB_TII_synfilt_a4\n"
                 "\t\t.text:_G729AB_TII_scaldownexc\n"
                 "\t\t.text:_G729AB_TII_postfilter\n"
                 "\t\t.text:_G729AB_TII_pitchpostfilter\n"
                 "\t\t.text:_G729AB_TII_setzero11\n"
                 "\t\t.text:_G729AB_TII_comptiltfact\n"
                 "\t\t.text:_G729AB_TII_preemphasis\n"
                 "\t\t.text:_G729AB_TII_agc\n"
                 "\t\t.text:_G729AB_TII_updatesidenergy\n"
                 "\t\t.text:_G729AB_TII_complsferrvect\n"
                 "\t\t.text:_G729AB_TII_decodepitchlag\n"
                 "\t\t.text:_G729AB_TII_deccodebookindices\n"
                 "\t\t.text:_G729AB_TII_gainupdateerasure\n"
                 "\t\t.text:_G729AB_TII_decgains\n"
                 "\t\t.text:_G729AB_TII_decodelspindices\n"
                 "\t\t.text:_G729AB_TII_checkpitparity\n"
                 "\t\t.text:_G729AB_TII_initg729adecoder\n"
                 "\t\t.text:_G729AB_TII_createdecoder\n"
                 "\t\t.text:_G729ABDEC_TII_g729abControl\n"
                 "\t\t.text:_G729AB_TII_decgetbufs\n"
                 "\t\t.text:_G729AB_TII_new_decoder\n"
                 "\t\t.text:_G729ABDEC_TII_initObj\n"
                 "\t\t.text:_G729AB_TII_decReset\n"
                 "\t\t.text:_G729AB_TII_decgetbase\n"
                 "\t\t.text:_G729ABDEC_TII_moved\n"
                 "\t\t.text:_G729ABDEC_TII_free\n"
                 "\t\t.text:_G729ABDEC_TII_numAlloc\n"
                 "\t\t.text:_G729ABDEC_TII_alloc\n"
                 "  }" },
   { NULL, NULL } 
};

// G726 (C54x LowMips and C54x LowMem)
SectInfo_t g726C54SlowMem [] = { 
   { &MediumData, "TableRAM   : {}" },
   { &MediumData, "ITblRAM    : {}" },
   { NULL, NULL } 
};


// G729 (C54x and C64x)
SectInfo_t g729ABC54FastMem [] = {
   { &FastDataFirst, "DARAMP   : {*(DARAMP)}" },
   { NULL, NULL } 
};
SectInfo_t g729ABC54SlowMem [] = { 
   { &MediumData, "C32I    : {*(C32DI)} ALIGN(32)" },
   { &MediumData, "EXTRAMD : {*(EXTRAMD)}" },
   { NULL, NULL } 
};

SectInfo_t g729ABC55SlowMem [] = { 
   { &SlowConst, "GROUP (G729AB_SLOW_DATA) :\n"
                 "\t\tTABSECT\n"
                 "\t\tTABSECT_1\n"
                 "\t\tTABSECT_2\n"
                 "\t\tTABSECT_CDP\n"
                 "\t\tTABSECT_DTX\n"
                 "\t\tSCOEFF\n"
                 "\t\tSCOEFF\n"
                 "  }  " },
   { NULL, NULL } 
};

SectInfo_t g729ABC64FastMem [] = {
   { &FastProg, ".G729AB   : RUN_START(_Start$G729Prog), RUN_END(_End$G729Prog) {}"  },
   { NULL, NULL } 
};
SectInfo_t g729ABC64SlowMem [] = { 
   { &SlowProg,  ".G729AB_INIT           : RUN_START(_Start$G729SlowProg), RUN_END(_End$G729SlowProg) {}\n" },
   { &FastConst,  "GROUP (G729AB_TABLES) : RUN_START(_Start$G729Data), RUN_END(_End$G729Data)  {\n"
                 "\t\tG729_ADT_TABLE1 \n"
                 "\t\tG729_ADT_TABLE2 \n"
                 "\t\tG729_ADT_TABLE3 \n"
                 "\t\tG729_ADT_TABLE4 \n"
                 "  }  " },
   { NULL, NULL } 
};

// ADT4800 (C54x)
SectInfo_t adt4800C54FastMem [] = {
   { &FastDataFirst, "AC128DI  : {*(ADT48_C128DI)} ALIGN(128)" },
   { NULL, NULL } 
};
SectInfo_t adt4800C54SlowMem [] = { 
   { &MediumData, "AC1024E  : {*(ADT48_C1024E)} ALIGN(1024)" },
   { &MediumData, "ASARAMD  : {*(ADT48_SARAMD)}" },
   { NULL, NULL } 
};

// AMR (C54x)
SectInfo_t AMR_54SlowMem [] = { 
   { &MediumData, "C32CI   : {*(C32CI)   }  ALIGN(128)" },
   { &MediumData, "C32DI   : {*(C32DI)   }  ALIGN(32)" },
   { &MediumData, "C32CI_1 : {*(C32CI_1) }  ALIGN(128)" },
   { &MediumData, "C256DI  : {*(C256DI)  }  ALIGN(256)" },
   { &MediumData, "scratch_sec : {}" },
   { NULL, NULL } 
};
SectInfo_t AMR_54FastMem [] = {   
   { &FastProg,   ".amrtext  : { components\\AMR_Dec.l54f(.text) \n\t\t\tcomponents\\AMR_Enc.l54f(.text) }" },
   { NULL, NULL } 
};

//}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{                       Vocoder info
// 
//  Identifies configurable vocoders, version, link files, memory requirements
//  and section allocation information.
//

//  Active file types
enum fileType { STUB = 0, PRODUCTION, DEMO };

typedef struct VocoderInfor_t {
   const char *name;
   int  cfgMask;                 //  Enable bit within config.Bits
   ADT_UInt16 *instanceCnt;                    //  Instance count
   int  Version;                               //  Version
   const char *EncLib,  *DecLib, *Stub;        // Libraries
   const char *EncDemo, *DecDemo;
   const memSpec_t *EncoderMem, *DecoderMem;   // En/Decoder instance memory
   SectInfo_t *fastMem, *slowMem;              // Fast memory sections
   enum fileType activeFile;                   // PRODUCTION, DEMO or STUB
} VocoderInfo_t;

//  Vocoder      G.711   G.723    G.726    G.728    G.729   AMR   MELP
//   Rate (ms)    Var      30      Var       2.5      10     20   22.5
//   RTP Rate      20      N/A      20        20      20     20    N/A
VocoderInfo_t *Vocoders = NULL;

VocoderInfo_t C54Vocoders [] = {

   
// Name          CfgMask   *Instance count              Version
//               EncoderLib                DecoderLib               Stub  
//               EncoderDemo               DecoderDemo
//               EncoderMem                DecoderMem    
//               Fast Section Memory       SlowSection Memory


{ "G723",         1<<12,  NULL,                              0x0100,
                  "encdec_g723F.out",       NULL,                     "g723stub",
                   NULL,                    NULL,
                  &g723C54EncMem,           &g723C54DecMem,
                   g723C54FastMem,           g723C54SlowMem,
},

{ "G726_LowMips", 1<<17,  NULL,                               0x0201,  
                  "g726enc_adt.l54f",       "g726dec_adt.l54f",      "g726lomipsstub",
                   NULL,                    NULL,
                  &g726C54EncLoMipsMem,     &g726C54DecLoMipsMem,
                   NullSection,              g726C54SlowMem,
},

{ "G726_LowMem",  1<<18,  NULL,                               0x0203,  
                  "g726enc_adt_lmem.l54f",  "g726dec_adt_lmem.l54f",   "g726lomemstub",
                   NULL,                    NULL,
                  &g726C54EncLoMemMem,      &g726C54DecLoMemMem, 
                   NullSection,              g726C54SlowMem,
},

{ "G728",         1<<11,  NULL,                               0x0202,  
                  "g728_adt.l54f",          "G728Pack.54obj",          "G728Stub",
                   NULL,                    NULL,
                  &g728C54Mem,              NULL,
                  NullSection,              NullSection
},

{ "G729AB",       1<<7,  NULL,                                0x0304,
                  "g729abencF.lib",       "g729abdecF.lib",         "G729ABStub",
                   NULL,                    NULL,
                  &g729ABC54EncMem,        &g729ABC54DecMem,
                   g729ABC54FastMem,        g729ABC54SlowMem,
},

{ "ADT_4800",     1<<10,  NULL,                               0x0100,  
                  "adt4800enc_decF.out",    NULL,                   "adt4800stub",
                   NULL,                    NULL,
                  &adt4800C54EncMem,        &adt4800C54DecMem,
                   adt4800C54FastMem,        adt4800C54SlowMem,
},

{ "AMR",           1<<9,  NULL,                                0x0201,  

                  "AMR_Enc.l54f",           "AMR_Dec.l54f",          "AMRstub",
                   NULL,                    NULL,
                  &AMR_54EncMem,           &AMR_54DecMem,
                   AMR_54FastMem,             AMR_54SlowMem,
},


{ "AMRFormatter",            1<<9,  NULL,                                  0xFFFF,  
                  "AMRFormat.54obj",       NULL,               "AMRFormatStub",
                   NULL,                    NULL,
                   NULL,                    NULL,
                   NullSection,             NullSection,
},



{ "MELP",         1<<5,  NULL,                                 0x0000,  
                  NULL,                     NULL,              "melpstub",
                  NULL,                     NULL,
                  NULL,                     NULL,
                  NullSection,              NullSection
},


{ NULL },

};

VocoderInfo_t C55Vocoders [] = {

   
// Name          CfgMask   *Instance count              Version
//               EncoderLib                DecoderLib               Stub  
//               EncoderDemo               DecoderDemo
//               EncoderMem                DecoderMem    
//               Fast Section Memory       SlowSection Memory


{ "G723",         1<<12,  NULL,                              0x0208,
                   NULL,       NULL,                     "g723stub",
                   NULL,                    NULL,
                   &g723C55EncMem,                    &g723C55DecMem,
                   NullSection,           NullSection,
},

{ "G726_LowMips", 1<<17,  NULL,                               0x0200,  
                  "NULL",       "NULL",      "g726lomipsstub",
                   NULL,                    NULL,
                   &g726C55EncLoMipsMem,                     &g726C55DecLoMipsMem,
                   NullSection,            NullSection,
},

{ "G726_LowMem",  1<<18,  NULL,                               0x0200,  
                  "NULL",  "NULL",   "g726lomemstub",
                   NULL,                    NULL,
                   &g726C55EncLoMemMem,      &g726C55DecLoMemMem, 
                   NullSection,              NullSection,
},

{ "G728",         1<<11,  NULL,                               0x0000,  
                   NULL,                       NULL,          "G728Stub",
                   NULL,                    NULL,
                  &g728C54Mem,              NULL,
                  NullSection,              NullSection
},

{ "G729AB",       1<<7,  NULL,                                0x0211,
                  "g729ab_adt_encdec_v0211.out",   NULL,         "G729ABStub",
                   NULL,                    NULL,
                  &g729ABC55EncMem,        &g729ABC55DecMem,
                   NullSection,        g729ABC55SlowMem,
},

{ "ADT_4800",     1<<10,  NULL,                               0x0000,  
                   NULL,                  NULL,                   "adt4800stub",
                   NULL,                    NULL,
                   NULL,        NULL,
                   NullSection,        NullSection,
},

{ "AMR",           1<<9,  NULL,                                0x0302,  

                   NULL,                    NULL,          "AMRstub",
                   NULL,                    NULL,
                   NULL,                    NULL,
                   NullSection,             NullSection,
},

{ "AMRFormatter",            1<<9,  NULL,                                  0xFFFF,  
                  NULL,       NULL,               "AMRFormatStub",
                   NULL,                    NULL,
                   NULL,                    NULL,
                   NullSection,             NullSection,
},

{ "MELP",         1<<5,  NULL,                                 0x0000,  
                  NULL,                     NULL,              "melpstub",
                  NULL,                     NULL,
                  NULL,                     NULL,
                  NullSection,              NullSection
},


{ NULL },

};

VocoderInfo_t C64Vocoders [] = {

   
// Name          CfgMask  &Instances                      Version
//               EncoderLib                DecoderLib               Stub  
//               EncoderDemo               DecoderDemo
//               EncoderMem                DecoderMem    
//               Fast Section Memory       SlowSection Memory


{ "G723",         1<<12,  NULL,                            0x0304,
                  "g723_adt.l64",          NULL,                    "g723stub",
                  NULL,                    NULL,
                  &g723C64EncMem,         &g723C64DecMem,
                   g723C64FastMem,         g723C64SlowMem 
},

{ "G726_LowMips", 1<<17,  NULL,                             0x0101,  
                  "G726Enc_adt.l64",        "G726Dec_adt.l64",       "g726stub",
                  "G726Enc_adt_demo.l64",   "G726Dec_adt_demo.l64 ",
                  &g726C64EncMem,           &g726C64DecMem, 
                   NullSection,             NullSection
},

{ "G726_LowMem", 1<<18,  NULL,                            0x0000,  
                  NULL,                     NULL,                     NULL,
                  NULL,                     NULL,
                  NULL,                     NULL,
                  NullSection,             NullSection
},

{ "G728",         1<<11,  NULL,                             0x0102,  
                  "G728Enc_adt.l64",        "G728Dec_adt.l64",       "G728Stub",
                  "G728Enc_adt_demo.l64",   "G728Dec_adt_demo.l64",
                  &g728C64EncMem,           &g728C64DecMem,
                  NullSection,              NullSection
},

{ "G729AB",       1<<7,  NULL,                               0x0303,
                  "g729AB_ADT.l64",         NULL,                    "G729ABStub",
                  "g729AB_ADT_DEMO.l64",    NULL,
                  &g729ABC64EncMem,        &g729ABC64DecMem,
                   g729ABC64FastMem,        g729ABC64SlowMem,
},

{ "ADT_4800",     1<<10,  NULL,                             0x0000,  
                  NULL,                     NULL,              "adt4800stub",
                  NULL,                     NULL,
                  NULL,                     NULL,
                  NullSection,              NullSection
},

{ "AMR",      1<<9,  NULL,                                 0x0205,  
                  "ADT_AMR_ENCDEC_c64x_V2_5_EL_Mml3_CG6_0_8.l64",    NULL,                   "AMRstub",
                  "ADT_AMR_ENCDEC_demo_c64x_V2_5_EL_Mml3_CG6_0_8.l64",    NULL,
                  &AMR_64EncMem,           &AMR_64DecMem,
                  NullSection,              NullSection
},

{ "AMRFormatter",            1<<9,  NULL,                             0xFFFF,  
                 "amrformat.o64",        NULL,               "amrformatStub",
                  NULL,                     NULL,
                  NULL,                     NULL,
                  NullSection,              NullSection,
},


{ "MELP",       1<<5,  NULL,                               0x0401,
                  "MELPENC_ADT.l64l",     "MELPDEC_ADT.l64l",   "melpstub",
                  "MELPENCDEMO_ADT.l64l", "MELPDECDEMO_ADT.l64l",
                  &Melp_64EncMem,          &Melp_64DecMem,
                  NullSection,              NullSection,
},

{ "MELPE",      1<<6,  NULL,                               0x0101,
                  "MelpE_adt.l64",         NULL,        NULL,
                  NULL,                    NULL,
                  &Melp_64EncMem,          &Melp_64DecMem,
                  NullSection,              NullSection,
},

{ "SpeexWB",     1<<3,  NULL,                               0x0100,
                  NULL,                        NULL,   "speexstub",
                  NULL,                        NULL,
                  NULL,                        NULL,
                  NullSection,              NullSection,
},

{ "Speex",     1<<1,  NULL,                               0x0100,
                  NULL,                        NULL,   "speexstub",
                  NULL,                        NULL,
                  NULL,                        NULL,
                  NullSection,              NullSection,
},

{ NULL },

};

VocoderInfo_t C64PlusVocoders [] = {

   
// Name          CfgMask  &Instances                      Version
//               EncoderLib                DecoderLib               Stub  
//               EncoderDemo               DecoderDemo
//               EncoderMem                DecoderMem    
//               Fast Section Memory       SlowSection Memory


{ "G722",       1<<0,  NULL,                               0x0201,
                  "G722enc_adt.l64",     "G722dec_adt.l64",   "G722Stub",
                  NULL,                        NULL,
                  &G722_64EncMem,          &G722_64DecMem,
                  NullSection,              NullSection,
},

{ "G723",         1<<12,  NULL,                            0x0304,
                  "g723_adt.l64p",          NULL,                    "g723stub",
                  NULL,                      NULL,
                  &g723C64EncMem,         &g723C64DecMem,
                   g723C64FastMem,         g723C64SlowMem 
},
{ "TI_G7231A",    1<<19,  NULL,                             0x0110,
                  "g7231aenc_tii.l64P",     "g7231adec_tii.l64P",    "g723stub",
                  NULL,                      NULL,
                  &TI_g7231AC64EncMem,      &TI_g7231AC64DecMem,
                  ti_g723C64FastMem,        ti_g723C64SlowMem,
},

{ "G726_LowMips", 1<<17,  NULL,                            0x0101,  
                  "G726Enc_adt.l64",        "G726Dec_adt.l64",       "g726stub",
                  "G726Enc_adt_demo.l64",   "G726Dec_adt_demo.l64 ",
                  &g726C64EncMem,           &g726C64DecMem, 
                   NullSection,             NullSection
},

{ "G726_LowMem", 1<<18,  NULL,                            0x0000,  
                  NULL,                     NULL,                     NULL,
                  NULL,                     NULL,
                  NULL,                     NULL,
                  NullSection,             NullSection
},

{ "G728",         1<<11,  NULL,                            0x0102,  
                  "G728Enc_adt.l64",        "G728Dec_adt.l64",       "G728Stub",
                  "G728Enc_adt_demo.l64",   "G728Dec_adt_demo.l64",
                  &g728C64EncMem,           &g728C64DecMem,
                  NullSection,              NullSection
},

{ "ADT_G729AB",   1<<7,  NULL,                             0x0301,
                  "g729AB_ADT.l64",         NULL,            "G729ABStub",
                  "g729AB_ADT_DEMO.l64",    NULL,
                  &g729ABC64EncMem,        &g729ABC64DecMem,
                   g729ABC64FastMem,        g729ABC64SlowMem,
},

{ "TI_G729AB",    1<<2,  NULL,                             0x0112,
                  "g729abenc_tii.l64P",     "g729abdec_tii.l64P",    "G729ABStub",
                  NULL,                      NULL,
                  &TI_g729ABC64EncMem,      &TI_g729ABC64DecMem,
                  ti_g729C64EncodeMem,        ti_g729C64DecodeMem,
},

{ "ADT_4800",     1<<10,  NULL,                            0x0000,  
                  NULL,                     NULL,              "adt4800stub",
                  NULL,                     NULL,
                  NULL,                     NULL,
                  NullSection,              NullSection
},

{ "AMR",          1<<9,  NULL,                             0x0205,  
                  "ADT_AMR_ENCDEC_c64xp_V2_5_EL_Mml3_CG6_0_8.l64p",    NULL,                   "AMRstub",
                  "ADT_AMR_ENCDEC_demo_c64xp_V2_5_EL_Mml3_CG6_0_8.l64p",    NULL,
                  &AMR_64EncMem,           &AMR_64DecMem,
                  NullSection,              NullSection
},

{ "AMRFormatter", 1<<9,  NULL,                             0xFFFF,  
                 "AMRFormat.o64p",        NULL,               "amrformatStub",
                  NULL,                     NULL,
                  NULL,                     NULL,
                  NullSection,              NullSection,
},

{ "MELP",         1<<5,  NULL,                             0x0401,
                  "MELPENC_ADT.l64l",     "MELPDEC_ADT.l64l",   "melpstub",
                  "MELPENCDEMO_ADT.l64l", "MELPDECDEMO_ADT.l64l",
                  &Melp_64EncMem,          &Melp_64DecMem,
                  NullSection,              NullSection,
},

{ "MELPE",        1<<6,  NULL,                             0x0101,
                  "MelpE_adt.l64p",         NULL,        NULL,
                  NULL,                    NULL,
                  &MelpE_64pEncMem,          &MelpE_64pDecMem,
                  NullSection,              NullSection,
},

{ "SpeexWB",   1<<3,  NULL,                              0x0100,
                  "Not Implemented",     NULL,   "SpeexStub",
                  NULL,                        NULL,
                  &SpeexWB_64EncMem,          &SpeexWB_64DecMem,
                  NullSection,              NullSection,
},

{ "Speex",     1<<1,  NULL,                               0x0100,
                  "Not Implemented",     NULL,   "SpeexStub",
                  NULL,                        NULL,
                  &Speex_64EncMem,          &Speex_64DecMem,
                  NullSection,              NullSection,
},

{ NULL },

};

VocoderInfo_t C66Vocoders [] = {

   
// Name          CfgMask  &Instances                      Version
//               EncoderLib                DecoderLib               Stub  
//               EncoderDemo               DecoderDemo
//               EncoderMem                DecoderMem    
//               Fast Section Memory       SlowSection Memory

{ "G722",      1<<0,  NULL,                                0x0000,
                  NULL,						NULL,   "G722Stub",
                  NULL,                        NULL,
                  NULL,							NULL,
                  NullSection,              NullSection,
},

{ "G723",         1<<12,  NULL,                            0x0000,
                  NULL,          NULL,                    "g723stub",
                  NULL,                     NULL,
                  NULL,         NULL,
                   NullSection,         NullSection 
},

{ "TI_G7231A",    1<<19,  NULL,                             0x0000,
                  NULL,     NULL,    "g723stub",
                  NULL,                      NULL,
                  NULL,      NULL,
                  NullSection,        NullSection,
},

{ "G726_LowMips", 1<<17,  NULL,                            0x0101,  
                  "ADT_G726_c66x_V1_01_EL_Mml3_CG8_2_2.l66",        NULL, "g726stub",
                  "ADT_G726_demo_c66x_V1_01_EL_Mml3_CG8_2_2.l66",   NULL,
                  &g726C64EncMem,           &g726C64DecMem, 
                   NullSection,             NullSection
},

{ "G726_LowMem", 1<<18,  NULL,                            0x0000,  
                  NULL,                     NULL,                     NULL,
                  NULL,                     NULL,
                  NULL,                     NULL,
                  NullSection,             NullSection
},

{ "G728",         1<<11,  NULL,                            0x0102,  
                  NULL,        NULL,       "G728Stub",
                  NULL,   NULL,
                  NULL,           NULL,
                  NullSection,              NullSection
},

{ "ADT_G729AB",   1<<7,  NULL,                              0x0303,
                  "ADT_g729ab_EABI_c66x_V3_3_2_EL_Mml3_CG8_2_2.l66",         NULL,            "G729ABStub",
                  "ADT_g729ab_EABI_DEMO_c66x_V3_3_2_EL_Mml3_CG8_2_2.l66",    NULL,
                  &g729ABC64EncMem,        &g729ABC64DecMem,
                   g729ABC64FastMem,        g729ABC64SlowMem,
},

{ "TI_G729AB",    1<<2,  NULL,                              0x0000,
                  NULL,     NULL,    "G729ABStub",
                  NULL,                      NULL,
                  NULL,      NULL,
                  NullSection,        NullSection,
},

{ "ADT_4800",     1<<10,  NULL,                           0x0000,  
                  NULL,                     NULL,              "adt4800stub",
                  NULL,                     NULL,
                  NULL,                     NULL,
                  NullSection,              NullSection
},

{ "AMR",       1<<9,  NULL,                                0x0205,  
                  NULL,    NULL,                   "AMRstub",
                  NULL,    NULL,
                  NULL,           NULL,
                  NullSection,              NullSection
},

{ "AMRFormatter",            1<<9,  NULL,                  0xFFFF,  
                 NULL,        NULL,               "amrformatStub",
                  NULL,                     NULL,
                  NULL,                     NULL,
                  NullSection,              NullSection,
},

{ "MELP",      1<<5,  NULL,                                0x0401,
                  NULL,     NULL,   "melpstub",
                  NULL, "MELPDECDEMO_ADT.l64l",
                  NULL,          NULL,
                  NullSection,              NullSection,
},

{ "MELPE",     1<<6,  NULL,                                0x0101,
                  NULL,         NULL,        NULL,
                  NULL,                    NULL,
                  NULL,          NULL,
                  NullSection,              NullSection,
},

{ "SpeexWB",   1<<3,  NULL,                                0x0102,
                  NULL,     NULL,   NULL,
                  NULL,                        NULL,
                  NULL,          NULL,
                  NullSection,              NullSection,
},

{ "Speex",     1<<1,  NULL,                                0x0102,
                  NULL,     NULL,   NULL,
                  NULL,                        NULL,
                  NULL,          NULL,
                  NullSection,              NullSection,
},

{ NULL },

};
//}

VocoderInfo_t C674Vocoders [] = {

   
// Name          CfgMask  &Instances                      Version
//               EncoderLib                DecoderLib               Stub  
//               EncoderDemo               DecoderDemo
//               EncoderMem                DecoderMem    
//               Fast Section Memory       SlowSection Memory

{ "G722",      1<<0,  NULL,                                0x0201,
                  "G722enc_adt.l64",     "G722dec_adt.l64",   "G722Stub",
                  NULL,                        NULL,
                  &G722_64EncMem,          &G722_64DecMem,
                  NullSection,              NullSection,
},

{ "G723",         1<<12,  NULL,                            0x0304,
                  "g723_adt.l64p",          NULL,                    "g723stub",
                  NULL,                     NULL,
                  &g723C64EncMem,         &g723C64DecMem,
                   g723C64FastMem,         g723C64SlowMem 
},

{ "TI_G7231A",    1<<19,  NULL,                             0x0110,
                  "g7231aenc_tii.l64P",     "g7231adec_tii.l64P",    "g723stub",
                  NULL,                      NULL,
                  &TI_g7231AC64EncMem,      &TI_g7231AC64DecMem,
                  ti_g723C64FastMem,        ti_g723C64SlowMem,
},

{ "G726_LowMips", 1<<17,  NULL,                            0x0101,  
                  "G726Enc_adt.l64",        "G726Dec_adt.l64",       "g726stub",
                  "G726Enc_adt_demo.l64",   "G726Dec_adt_demo.l64 ",
                  &g726C64EncMem,           &g726C64DecMem, 
                   NullSection,             NullSection
},

{ "G726_LowMem",  1<<18,  NULL,                            0x0000,  
                  NULL,                     NULL,                     NULL,
                  NULL,                     NULL,
                  NULL,                     NULL,
                  NullSection,             NullSection
},

{ "G728",         1<<11,  NULL,                            0x0102,  
                  "G728Enc_adt.l64",        "G728Dec_adt.l64",       "G728Stub",
                  "G728Enc_adt_demo.l64",   "G728Dec_adt_demo.l64",
                  &g728C64EncMem,           &g728C64DecMem,
                  NullSection,              NullSection
},

{ "ADT_G729AB",   1<<7,  NULL,                              0x0301,
                  "g729AB_ADT.l64",         NULL,            "G729ABStub",
                  "g729AB_ADT_DEMO.l64",    NULL,
                  &g729ABC64EncMem,        &g729ABC64DecMem,
                   g729ABC64FastMem,        g729ABC64SlowMem,
},

{ "TI_G729AB",    1<<2,  NULL,                              0x0112,
                  "g729abenc_tii.l64P",     "g729abdec_tii.l64P",    "G729ABStub",
                  NULL,                      NULL,
                  &TI_g729ABC64EncMem,      &TI_g729ABC64DecMem,
                  ti_g729C64EncodeMem,        ti_g729C64DecodeMem,
},

{ "ADT_4800",     1<<10,  NULL,                           0x0000,  
                  NULL,                     NULL,              "adt4800stub",
                  NULL,                     NULL,
                  NULL,                     NULL,
                  NullSection,              NullSection
},

{ "AMR",       1<<9,  NULL,                                0x0205,  
                  "ADT_AMR_ENCDEC_c64xp_V2_5_EL_Mml3_CG6_0_8.l64p",    NULL,                   "AMRstub",
                  "ADT_AMR_ENCDEC_demo_c64xp_V2_5_EL_Mml3_CG6_0_8.l64p",    NULL,
                  &AMR_64EncMem,           &AMR_64DecMem,
                  NullSection,              NullSection
},

{ "AMRFormatter",            1<<9,  NULL,                  0xFFFF,  
                 "AMRFormat.o6740",        NULL,               "amrformatStub",
                  NULL,                     NULL,
                  NULL,                     NULL,
                  NullSection,              NullSection,
},

{ "MELP",      1<<5,  NULL,                                0x0401,
                  "MELPENC_ADT.l64l",     "MELPDEC_ADT.l64l",   "melpstub",
                  "MELPENCDEMO_ADT.l64l", "MELPDECDEMO_ADT.l64l",
                  &Melp_64EncMem,          &Melp_64DecMem,
                  NullSection,              NullSection,
},

{ "MELPE",     1<<6,  NULL,                                0x0101,
                  "MelpE_adt.l64p",         NULL,        NULL,
                  NULL,                    NULL,
                  &MelpE_64pEncMem,          &MelpE_64pDecMem,
                  NullSection,              NullSection,
},

{ "SpeexWB",   1<<3,  NULL,                                0x0102,
                  "speexc674x_adt_V1_02_CGTV_6_1_19.l6740",     NULL,   NULL,
                  NULL,                        NULL,
                  &SpeexWB_64EncMem,          &SpeexWB_64DecMem,
                  NullSection,              NullSection,
},

{ "Speex",     1<<1,  NULL,                                0x0102,
                  "speexc674x_adt_V1_02_CGTV_6_1_19.l6740",     NULL,   NULL,
                  NULL,                        NULL,
                  &Speex_64EncMem,          &Speex_64DecMem,
                  NullSection,              NullSection,
},

{ NULL },

};
//}
typedef struct EcOverhead_t {
   int Version;
   int ShortInst;
   int SaShortState;
   int SaShortScratch;
   float shortFrameMult;
   int Inst;
   int SaState;
   int SaScratch;
   float frameMult;
} Overhead_t;

//  NOTE:  All echo canceller memory is calculated as 16-bit words
static Overhead_t C55_V1030_Overhead = { 0x1030,  174, 114,   2, 2.0,   202, 114,  2, 2.0 };
static Overhead_t C54Overhead     = { 0x802,  128, 108,   1, 1.0,   128, 120,  40, 1.0 };
static Overhead_t V8_25Overhead   = { 0x825,  158,  90,  38, 1.0,   170, 196, 164, 1.0 }; // Obsolete
static Overhead_t V9_02_Overhead  = { 0x902,  170,  90,  38, 1.0,   184, 196, 164, 1.0 };
static Overhead_t V10_04_Overhead = { 0x1004, 204,  96,  42, 2.0,   216, 224, 260, 2.0 }; // Faulty release
static Overhead_t V10_10_Overhead = { 0x1010, 210, 100,  46, 2.0,   220, 230, 265, 2.0 };
static Overhead_t V10_11_Overhead = { 0x1011, 212, 100,  26, 2.25,  224, 230, 225, 2.5 };
static Overhead_t V11_00_Overhead = { 0x1100, 212, 100,  26, 2.25,  224, 230, 225, 2.5 };
static Overhead_t V11_03_Overhead = { 0x1103, 260, 100,  40, 4.5,   260, 230, 240, 4.5 };
static Overhead_t V11_18_03_Overhead = { 0x111803, 368, 100,  40, 4.5,   368, 230, 240, 4.5 };
static Overhead_t V11_19_03_Overhead = { 0x111903, 368, 100,  40, 4.5,   368, 230, 240, 4.5 };

Overhead_t *ActiveC55xOverhead = &C55_V1030_Overhead;
Overhead_t *ActiveC54xOverhead = &C54Overhead;
Overhead_t *ActiveC64xOverhead = &V11_19_03_Overhead;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
typedef struct memBlock_t {
    int start, stop;
    int len;
    int status;         // FREE, ASSIGNED
} memBlock_t;

typedef struct holeBlock_t {
    int start, stop;
    int len;
    int status;         // OPEN, CLOSED
    int nextFreeLoc;
    int freeSpaceAvail;
} holeBlock_t;

// prototypes
int fillMemTab (memSpec_t *memTab, ecConfig_t *ecCfg);
void sortMemTab (int n, memSpec_t *memTab, blockStatus_t *bs);
int assignAddress (int Count, int ACount, memSpec_t *Tab, memBlock_t *List);
int align (int addr, int boundary);
int getVocoderMemSpec (int nrates, memSpec_t *mem);

extern BOOL ForceRebuild;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
memBlock_t saramList[NUM_IDS], daramList[NUM_IDS];
memSpec_t  saramTab[NUM_IDS],  daramTab[NUM_IDS];
memSpec_t  encodeMemory = { ENCODER_ID, 0, SARAM, 0, 0};
memSpec_t  decodeMemory = { DECODER_ID, 0, SARAM, 0, 0};


int ExtDmaActive = FALSE;   // Flag indicating DMA of external buffers requested
int CacheActive = FALSE;

//  Rounds to L2 cache line size which is 128 bytes or 64 I16 words
static int RoundForCache (int I16Cnt) {
  
  if (!CacheActive) return I16Cnt;

  return ((I16Cnt + 0x3F) & 0xFFFFFC0);
}

void setPlatformGlobals (sysConfig_t *cfg, DspTypeDepInfo_t *dspInfo) {
   int i;
   SysCfg = *cfg;
   DspInfo = dspInfo;

   WBCodecEnabled = G711WBEnabled | L16WBEnabled | SpeexWBEnabled | G722Enabled;

   for (i=0, tsipEnable=0; i<MAX_NUM_SERIAL_PORTS; i++)
      tsipEnable |= TSIPEnable[i];

   IsC55     = DspInfo->IsC55x;
   IsC64     = DspInfo->IsC64x;
   IsC64Plus = DspInfo->IsC64xPlus;

   coreCnt = 1;
   if (DspInfo->DspType == Ti647)   coreCnt = 6;
   else if (DspInfo->DspType == Ti667)  coreCnt = 6;

   if (DspInfo->DspType == Ti674) {
      Vocoders   = C674Vocoders;
      platform   = OBJ_EXT_67p;
      libExt     = LIB_EXT_67p;
   } else if (DspInfo->DspType == Ti665) {
      Vocoders   = C66Vocoders;
      platform   = OBJ_EXT_66;
      libExt     = LIB_EXT_66;
   } else if (DspInfo->DspType == Ti667) {
      Vocoders   = C66Vocoders;
      platform   = OBJ_EXT_66;
      libExt     = LIB_EXT_66;
   } else if (IsC64Plus) {
      Vocoders   = C64PlusVocoders;
      platform   = OBJ_EXT_64p;
      libExt     = LIB_EXT_64p;
   } else if (IsC64)  {
      Vocoders   = C64Vocoders;
      platform   = OBJ_EXT_64;
      libExt     = LIB_EXT_64;
   } else if (IsC55)  {
      Vocoders   = C55Vocoders;
      platform   = OBJ_EXT_55;
      libExt     = LIB_EXT_55;
   } else {
      Vocoders   = C54Vocoders;
      platform   = OBJ_EXT_54;
      libExt     = LIB_EXT_54;
      IsC54      = ADT_TRUE;
   }
}

void computeBlockSize (sysConfig_t *cfg, blockStatus_t *blockStatus, ecConfig_t *ecCfg,
                       DspTypeDepInfo_t *dspInfo) {

   memSpec_t memTable[NUM_IDS];
   int n;

   setPlatformGlobals (cfg, dspInfo);
   memset (memTable, 0, sizeof (memTable));

    n = fillMemTab (memTable, ecCfg);

#ifdef EXT_DMA_TRANSFERS
    if (numAtccBlocks)  {
        int i;
        // Adjust the size of the vocoder memory instances so that they
        // are an even multiple of the number of ATCC sub-blocks (i.e. 8).
        // The factor of 2 is necessary to ensure that the adjusted instance
        // length is an integer multiple of subblocks comprised  of 32-bit
        // elements. The memory is specified and allocated in units of 16-bit
        // words, but the DMA element size is in 32-bit words - hence the 
        // factor of 2.
        //
        for (i=0; i<n; i++) {
            if (memTable[i].Words < 2) continue;
            RoundForDMA (memTable[i].Words);
        }    
        RoundForDMA (encodeMemory.Words);
        RoundForDMA (decodeMemory.Words);


    }
#endif

    sortMemTab     (n, memTable, blockStatus);

}

static void sortMemTab (int n, memSpec_t *memTab, blockStatus_t *bs) {
   int i;
   int daramCount=0;
   int saramCount=0;
   int daramAlignCount=0;
   int saramAlignCount=0;

   memset (daramTab,  0, sizeof (daramTab));
   memset (daramList, 0, sizeof (daramList));

   memset (saramTab,  0, sizeof (saramTab));
   memset (saramList, 0, sizeof (saramList));

   for (i=0; i<NUM_IDS; i++)    {
        bs->daramAllocTable[i].start = 0;
        bs->daramAllocTable[i].id = ENCODER_ID;

        bs->saramAllocTable[i].start = 0;
        bs->saramAllocTable[i].id = ENCODER_ID;

    }

    // first separate the memtab into saram components and daram components
    for (i=0; i<n; i++)    {
        if (memTab[i].type == SARAM)    {
            saramList[saramCount].len = memTab[i].Words;
            saramTab[saramCount] = memTab[i];

            if (memTab[i].align > 0)    saramAlignCount++;

            saramList[saramCount].status = FREE;
            saramCount++;
        }   else     {
            daramList[daramCount].len = memTab[i].Words;
            daramTab[daramCount] = memTab[i];

            if (memTab[i].align > 0)
                daramAlignCount++;

            daramList[daramCount].status = FREE;
            daramCount++;
        }
    }
    bs->saramCount = saramCount;
    bs->saramblockSize = assignAddress (saramCount, saramAlignCount, saramTab, saramList);
    bs->saramBaseAlignment = saramTab[0].align;
    
    bs->daramCount = daramCount;
    bs->daramblockSize = assignAddress (daramCount, daramAlignCount, daramTab, daramList);
    bs->daramBaseAlignment = daramTab[0].align;

    // fill the allocation tables
    for (i=0; i<saramCount; i++){
        bs->saramAllocTable[i].id    = saramTab[i].id;
        bs->saramAllocTable[i].start = saramList[i].start;
    }

    for (i=0; i<daramCount; i++) {
        bs->daramAllocTable[i].id    = daramTab[i].id;
        bs->daramAllocTable[i].start = daramList[i].start;
    }
}

static int assignAddress (int Count, int ACount, memSpec_t *Tab, memBlock_t *List) {

   int i, j, m, nn;
   int holeCount=0;
   int nextAvailAddr=0;
   int offset=0;

   holeBlock_t holes[NUM_IDS];
   memBlock_t tmb;
   holeBlock_t tmh;
   memSpec_t tms;

    // sort the components in order of alignment size from
    // largest alignment to lowest alignment, followed by all non-aligned
    // components.
    if (Count <= 0)    return 0;

    // sort the saram table and allocation order lists by alignment
    for (j=0; j<Count-1; j++)  {
    for (i=j+1; i<Count; i++)  {
        if (Tab[i].align <= Tab[j].align) continue;
         tms = Tab[j];
         Tab[j] = Tab[i];
         Tab[i] = tms;

         tmb = List[j];
         List[j] = List[i];
         List[i] = tmb;
    } }

    // determine the start and stop addresses of all aligned blocks
    // and flag any holes that are created as a result of alignment
    for (i=0; i<ACount; i++)      {
         offset = align (nextAvailAddr, Tab[i].align);
         if (offset > 0) {
             holes[holeCount].start = nextAvailAddr;
             holes[holeCount].len = offset;
             holes[holeCount].stop = nextAvailAddr + offset - 1;
             holes[holeCount].nextFreeLoc = nextAvailAddr;
             holes[holeCount].freeSpaceAvail = offset;
             holes[holeCount].status = OPEN;
             holeCount++;
             nextAvailAddr += offset;
         }
    
         // assign addresses to aligned blocks
         List[i].start = nextAvailAddr;
         List[i].stop  = nextAvailAddr + Tab[i].Words - 1;
         List[i].len = Tab[i].Words;
         List[i].status = ASSIGNED;
         nextAvailAddr += Tab[i].Words;
     }

     if (holeCount > 0)  {
         // Sort the holes from largest to smallest
         for (j=0;   j<holeCount-1; j++)   {
         for (i=j+1; i<holeCount; i++)   {
             if (holes[i].len <= holes[j].len) continue;
             tmh = holes[j];
             holes[j] = holes[i];
             holes[i] = tmh;
         }  }

         // Sort the non-aligned sections from largest to smallest
         for (j=ACount; j<Count-1; j++) {
         for (i=j+1;    i<Count; i++)  {
             if (Tab[i].Words <= Tab[j].Words)  continue;
             tms = Tab[j];
             Tab[j] = Tab[i];
             Tab[i] = tms;

             tmb = List[j];
             List[j] = List[i];
             List[i] = tmb;
         } }
     }

      // Assign addresses to the nonaligned blocks; place them in holes
      // if they fit. Attempt to place the largest blocks into the largest
      // holes first.
      for (m=ACount; m<Count; m++)        {

          for (nn=0; nn<holeCount; nn++)  {
              if ((holes[nn].freeSpaceAvail <= 0) || (List[m].len > holes[nn].freeSpaceAvail)) continue;

              List[m].start = holes[nn].nextFreeLoc;
              List[m].stop = List[m].start + List[m].len - 1;
              List[m].status = ASSIGNED;
              holes[nn].nextFreeLoc += List[m].len;
              holes[nn].freeSpaceAvail -= List[m].len;
              if (holes[nn].freeSpaceAvail == 0)  holes[nn].status = CLOSED; 
         }
            
         if (List[m].status != FREE)   continue;
         // these sections either couldn't fit into open holes,
         // or there were no open holes to put them in. Assign
         // addresses starting at nextAvailAddr
         List[m].start = nextAvailAddr;
         List[m].stop = List[m].start + List[m].len - 1;
         List[m].status = ASSIGNED;
         nextAvailAddr += List[m].len;
         
     }
#ifdef PAD_BLOCK
     // pad the block size if necessary so that an array of blocks can
     // be allocated and still meet the alignment requirements of 1st section
     if (ACount > 0)        {
         offset = align (nextAvailAddr, Tab[0].align);
         nextAvailAddr += offset;
     }
#endif



    // sort the list and Tab in order of ascending addresses
    for (j=0; j<Count-1; j++)  {
    for (i=j+1; i<Count; i++)  {
        if (List[i].start >= List[j].start) continue;
        
        tms = Tab[j];
        Tab[j] = Tab[i];
        Tab[i] = tms;

        tmb = List[j];
        List[j] = List[i];
        List[i] = tmb;
                
    } }
    

    return (nextAvailAddr);
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// returns an offset that must be added to addr in order to ensure
// that the sum (addr+offset) equals an integer multiple of the alignment 
// specified by boundary
static int align (int addr, int boundary) {

   int offset;
   double x, fract;

    if (boundary == 0)    return 0;
    
    x = (double)addr/(double)boundary;

    fract = x - (int)x;

    if (fract == 0.0)   offset = 0;
    else                offset  = (int)((1.0 - fract) * ((double)boundary));
    
    return offset;

}

//-----------------------------------------------------------------
//      MACROS 
//-----------------------------------------------------------------
// Macro to generate typedef structure pointer arrays and their elements.
static void generateArrayElements (FILE *fp, unsigned int cnt, char *typeDef, char *instName, char *instSection, unsigned int alignI8) {
   int i;
   char *hideSymbols = "";     // Hide symbol names from map file.  First one not hidden for base address.
   fprintf (fp, "//{   %20s %20s STRUCTURES to %s\n", typeDef, instName, instSection);
   for (i = 0; i < ((int) cnt); i++) {
      fprintf (fp, "\n%s%s %s%d;\n", hideSymbols, typeDef, instName, i);
      fprintf (fp, "#pragma DATA_SECTION (%s%d, \"%s\")\n", instName, i, instSection);
      hideSymbols = "static ";
      if (alignI8 == 0) continue;
      fprintf (fp, "#pragma DATA_ALIGN   (%s%d, %d)\n", instName, i, alignI8);
   }
   
   fprintf (fp,"\n%s* const %s[] = {\n", typeDef, instName);
   for (i = 0; i < ((int)(cnt - 1)); i++) {
      fprintf (fp, "\t&%s%d,\n", instName, i);
   }
   fprintf (fp, "\t&%s%d\n", instName, i);

   fprintf (fp, "};\n//}\n\n");
}

typedef struct EcAlign {
   int EcEchoPath, EcBackEp, EcChan, EcDaState, EcSaState, EcBackFar, EcBackNear;
} EcAlign;

typedef struct align {
   int saramChan, daramChan;

   EcAlign Pcm;
   EcAlign Pkt;

} Align;

static char Unsupported [30000];
static char FileName [100];
char *IdentifyActiveFiles (sysConfig_t *cfg) {

   char *rptBuff = Unsupported;
   char *format;

   FILE *fp;

   SysCfg = *cfg;

   VocoderInfo_t   *Vocoder;

   memset (rptBuff, 0, sizeof (rptBuff));
   if (IsC64 && (ActiveC64xOverhead->Version == 0x1103) && (algBits.g168VarAEnable)) {
      algBits.g168PktEnable  = 1;
      algBits.g168VarAEnable = 0;
   }


   // ---------------------------------------------------------------------------
   //                   Verify existence of vocoder files

   //---- Vocoder components
   for (Vocoder = Vocoders; Vocoder->name != NULL; Vocoder++) {   
       ADT_Bool stub;

       Vocoder->activeFile = STUB;
       format = "No library exists for %s (%s). This vocoder has been rendered inactive.\r\n";

       // Vocoder not requested -> use stub
       stub = TRUE;
       if (Vocoder->cfgMask & SysCfg.Codecs.LongWord) stub = FALSE;
       if ((Vocoder->instanceCnt != NULL) && (*(Vocoder->instanceCnt) != 0)) stub = FALSE;
       if (stub) continue;

       // Vocoder requested and production library exist -> use production library
       if (Vocoder->EncLib != NULL) {
          sprintf (FileName, "%s\\%s", CompPath, Vocoder->EncLib);
          fp = fopen (FileName, "r");
          if (fp != 0)  {
             Vocoder->activeFile = PRODUCTION;
             fclose (fp);
             continue;
          }
       }

       // Check if demo is available
       if (Vocoder->EncDemo != NULL) {
          sprintf (FileName, "%s\\%s", CompPath, Vocoder->EncDemo);
          fp = fopen (FileName, "r");
          if (fp != 0)  {
             Vocoder->activeFile = DEMO;
             format = "The demostration version of %s has been selected.\r\n";
             fclose (fp);
          } 
       }

       // No production or demo version is available. Disable vocoder.
       if ((Vocoder->activeFile == STUB) && (Vocoder->cfgMask != 0xffffffff))
          SysCfg.Codecs.LongWord &= (~Vocoder->cfgMask);

       rptBuff += sprintf (rptBuff, format, Vocoder->name, Vocoder->EncLib);

   }

   rptBuff = identifyActiveComponents (&SysCfg, rptBuff);

   *cfg = SysCfg;
    if (rptBuff == Unsupported) return NULL;
    return Unsupported;
}

static int bufferSizeI16 (int baseI16, int buffAlignI8) {
   int alignI8;
   int lowBits, highBits;
   
   lowBits = buffAlignI8 - 1;
   highBits = ~lowBits;

   alignI8 = (baseI16 * 2) + lowBits;
   alignI8 &= highBits;
   return alignI8;
}
inline unsigned int roundToNextMillisecond (unsigned int lenI16) {
   return (lenI16 + 15) & ~0xf;
}

//===================================================================
//                         System Sizing Defines  <project>.h
//{
static void writeSystemSizing (FILE *fp, blockStatus_t *bs, ecConfig_t *ecCfg,
                        unsigned int numChannels) {

   unsigned int i, buffAlignI8;
   unsigned int pcmInI16, pcmOutI16, pktI16, bulkDelayI16;
   unsigned NumConfScratchMem;  // num Conference scratch memory instances

   if (SysCfg.maxNumConferences < NumFrameRates)    {
      NumConfScratchMem = SysCfg.maxNumConferences;
   } else {
      NumConfScratchMem = NumFrameRates;
   }

   if (NumT38Instances != 0) {
      SysCfg.numFaxChans = NumT38Instances;
   } else { 
      SysCfg.numFaxChans = NumTIT38Instances;
   }

   //  PCM and bulk delay buffer sizing
   pcmInI16  = MaxFrameSize * NUMFRAMES_PER_PCM_BUFF;
   pcmOutI16 = pcmInI16;
   bulkDelayI16 = (NUMFRAMES_OF_PKT_BULKDELAY * MaxFrameSize) + 1;

   if (SysCfg.numPcmEcans + SysCfg.numPktEcans != 0)
      pcmOutI16 += (NUMFRAMES_OF_PCM_BULKDELAY * MaxFrameSize);

   if ((SysCfg.samplesPerMs == 16) || WBCodecEnabled) {
      pcmInI16  *= 2;
      pcmOutI16 *= 2;
      bulkDelayI16 *= 2;
   }

   //  Packet buffer sizing
   pktI16 = (MaxFrameSize + 12) * NUMFRAMES_PER_PCM_BUFF;   // Allow RTP header
   if (L16WBEnabled) pktI16 *= 4;
   else if ((SysCfg.samplesPerMs == 16) || L16Enabled || WBCodecEnabled) pktI16 *= 2;


   // Allow room for distinction between empty and full double buffers.
   pktI16    += 12;
   pcmInI16  += 12;
   pcmOutI16 += 12;

   // Make packet buffers an even multiple of 16 samples for TSIP driver.
   // This allows the TSIP driver to copy a full millisecond of data to the
   // channel circular buffers without concern about wrapping mid-way through 
   // the copy.
   if (tsipEnable) {
      pktI16    = roundToNextMillisecond (pktI16);
      pcmInI16  = roundToNextMillisecond (pcmInI16);
      pcmOutI16 = roundToNextMillisecond (pcmOutI16);
   }

   // Align pcm and pkt buffers on cache boundaries.
   if (SysCfg.DspType == Ti647 && CacheActive)
      buffAlignI8 = 128;
   else if (SysCfg.DspType == Ti667 && CacheActive)
      buffAlignI8 = 128;
   else
      buffAlignI8 = 8;

   // Vocoder instance buffering
   if (bs->saramblockSize <= 0) bs->saramblockSize = 1;                    
   if (bs->daramblockSize <= 0) bs->daramblockSize = 1;                    

   if (encodeMemory.Words == 0) encodeMemory.Words = 1;
   if (decodeMemory.Words == 0) decodeMemory.Words = 1;

   fprintf (fp,"\n\n\n/*================================== */\n");
   fprintf (fp,      "/*           System Sizing           */\n");

   fprintf (fp, "\n//{");

   fprintf (fp,"\n#define MAX_NUM_CHANNELS    %5d\n", numChannels);
   fprintf (fp,  "#define MAX_NUM_LB_CHANNELS %5d\n", SysCfg.maxNumLbCoders);
   fprintf (fp,  "#define MAX_FRAME_SIZE      %5d\n", MaxFrameSize);
   fprintf (fp,  "#define CORE_CNT            %5d\n", coreCnt);
   fprintf (fp,  "#define NUM_TRGEN_EVENTS    %5d\n", 10);

   fprintf (fp,  "\n// Conferencing sizes\n");
   fprintf (fp,  "#define MAX_TOTAL_CONFERENCES    %5d\n", SysCfg.maxNumConferences);
   fprintf (fp,  "#define MAX_GPAK_CONFERENCES     %5d\n", NumGpakConferences);
   fprintf (fp,  "#define MAX_CUSTOM_CONFERENCES   %5d\n", NumCustomConferences);
   fprintf (fp,  "#define MAX_NUM_CONF_CHANS       %5d\n", SysCfg.maxNumConfChans);
   if (0 < SysCfg.maxNumConferences) {
      fprintf (fp,  "#define MAX_DOMINANT_N           %5d\n",   SysCfg.numConfDominant);
      fprintf (fp,  "#define MAX_CONF_FRAME_SIZE      %5d\n",   MaxFrameSize);
   }
   fprintf (fp,  "#define NUM_CONF_SCRATCH_MEM     %5d\n",  NumConfScratchMem);

   fprintf (fp,  "\n// Algorithm pool sizes\n");
   fprintf (fp,  "#define MAX_NUM_AGC_CHANNELS     %5d  // AGC\n", SysCfg.numAGCChans);
   fprintf (fp,  "#define MAX_NUM_RXCID_CHANNELS   %5d  // Caller ID\n", SysCfg.numRxCidChans);
   fprintf (fp,  "#define MAX_NUM_TXCID_CHANNELS   %5d  // Caller ID\n", SysCfg.numTxCidChans);
   fprintf (fp,  "#define MAX_NUM_TONEGEN_CHANNELS %5d  // Tone generate\n", SysCfg.numTGChans);
   fprintf (fp,  "#define MAX_NUM_TONEDET_CHANNELS %5d  // Tone detect\n", TONEINSTANCES());
   fprintf (fp,  "#define MAX_TONE_DET_TYPES       %5d  // Tone detect\n", SysCfg.maxToneDetTypes);
   fprintf (fp,  "#define CED_DETECT_INSTANCES     %5d  // CED detect\n", CEDDetInstances);
   fprintf (fp,  "#define CNG_DETECT_INSTANCES     %5d  // CNG detect\n", CNGDetInstances);
   fprintf (fp,  "#define ARB_DETECT_INSTANCES     %5d  // Generic tone detect\n", ARBDetInstances);
   fprintf (fp,  "#define NUM_ARB_TONE_CONFIGS     %5d  // Generic tone detect\n", ARBCfgCount);
   fprintf (fp,  "#define MAX_NUM_FAX_CHANS        %5d  // T.38\n", SysCfg.numFaxChans);
   fprintf (fp,  "#define MAX_NUM_SRC_CHANNELS     %5d  // Rate conversion\n",  SysCfg.numSrcChans);
   fprintf (fp,  "#define SRCUPBY2_BUFFER_I16      %5d  // Rate conversion\n",  (MaxFrameSize*2+112+8));
   fprintf (fp,  "#define SRCDOWNBY2_BUFFER_I16    %5d  // Rate conversion\n",  (MaxFrameSize*2+116+8));
   fprintf (fp,  "#define SRTPRX_SIZE_I4 ((1184+3)/4)\n");
   fprintf (fp,  "#define SRTPTX_SIZE_I4 ((1184+3)/4)\n");

   fprintf (fp,  "\n// Channel buffer pool sizes\n");
   fprintf (fp,  "#define MAX_DELAY_MS_PKT_BUFS  %5d\n", MaxPktBufDelayMS);
   pktI16 += (MaxPktBufDelayMS * SysCfg.samplesPerMs);

   fprintf (fp,  "#define MAX_NUM_PCM_BUFFS        MAX_NUM_CHANNELS\n");
   fprintf (fp,  "#define MAX_NUM_PKT_BUFFS        MAX_NUM_CHANNELS\n");
   fprintf (fp,  "#define MAX_NUM_BULKDELAY_BUFFS  MAX_NUM_CHANNELS\n");
   fprintf (fp,  "#define PCMIN_BUFFER_I16         %5d\n", pcmInI16);
   fprintf (fp,  "#define PCMIN_ALIGN_I16          %5d\n", bufferSizeI16 (pcmInI16, buffAlignI8) / 2);
   fprintf (fp,  "#define PCMOUT_BUFFER_I16        %5d\n", pcmOutI16);
   fprintf (fp,  "#define PCMOUT_ALIGN_I16         %5d\n", bufferSizeI16 (pcmOutI16, buffAlignI8) / 2);
   fprintf (fp,  "#define PKT_BUFFER_I16           %5d\n", pktI16);
   fprintf (fp,  "#define PKT_ALIGN_I16            %5d\n", bufferSizeI16 (pktI16, buffAlignI8) / 2);

   // Bulk delay buffers sizes for echo cancellation
   if ((SysCfg.numPktEcans != 0) || 
      (((SysCfg.cfgFramesChans & PCM2PCM_CFG_BIT) != 0) && (SysCfg.numPcmEcans + SysCfg.AECInstances != 0))) {
      fprintf (fp, "#define BULKDELAY_BUFFER_I16     %5d\n",   bulkDelayI16);
      fprintf (fp, "#define BULKDELAY_ALIGN_I16      %5d\n\n", bufferSizeI16 (bulkDelayI16, buffAlignI8) / 2);
   } else {
      fprintf (fp, "#define BULKDELAY_BUFFER_I16         0\n\n");
      fprintf (fp, "#define BULKDELAY_ALIGN_I16          1\n\n");
   }
   // For G711A1 channel instance
   if(decodeMemory.Words < 1804/2)
	   decodeMemory.Words = 1804/2;
   fprintf (fp,"#define ENCODER_INST_I16   %5d\n", encodeMemory.Words);
   fprintf (fp,"#define DECODER_INST_I16   %5d\n", decodeMemory.Words);
   fprintf (fp,"#define ENCODER_BUFFER_I16 %5d\n", RoundForCache (encodeMemory.Words));
   fprintf (fp,"#define DECODER_BUFFER_I16 %5d\n", RoundForCache (decodeMemory.Words));

   // For G711A1 channel instance
   if(BG_SCRATCH_I16 < 624/2)
	   BG_SCRATCH_I16 = 624/2;
   fprintf (fp,"#define BG_SCRATCH_I16     %5d\n",   BG_SCRATCH_I16);
   fprintf (fp,"#define SARAM_BLOCK_I16    %5d\n",   bs->saramblockSize); 
   fprintf (fp,"#define DARAM_BLOCK_I16    %5d\n\n", bs->daramblockSize); 

   fprintf (fp,  "\n// DMA buffer sizes\n");
 
#ifdef MCASP_ALLOCATION
   // Temporarily hard-code enable of McASP0 if 64xx part. Used on DM642 DSK
   // 8 mS, double-buffered, 1 timeslot, 2 words-per-sample (for now), 1 serializer (per direction)
   if (IsC64) {
      fprintf (fp,"#define ASP%d_DMA_BUFFER_I16 %5d\n", 0, 32);
      fprintf (fp,"#define ASP%d_DMA_BUFFER_I16 %5d\n", 1, 1);
   }
#endif
   // DMA Transfer buffers
   if (tsipEnable) {
      for (i=0; i<MAX_NUM_SERIAL_PORTS; i++) {
         fprintf (fp,"#define TSIP_MAX_ACTIVE_SLOTS_%d    %5d\n",i, SysCfg.maxSlotsSupported[i]);
         if (SysCfg.maxSlotsSupported[i] != 0) {
            fprintf (fp,"#define TSIP_FALLOC_I8_%d       (TSIP_MAX_ACTIVE_SLOTS_%d + 8)     // 8-bytes for pre/post frame count headers\n", i, i);
            fprintf (fp,"#define TSIP_BUFLEN_I8_%d       (TSIP_FALLOC_I8_%d * 8 * 2)        // 8 samps/interrupt, 1 bytes/sample, double buffered\n", i, i);
            fprintf (fp,"#define BSP%d_DMA_BUFFER_I16    (TSIP_BUFLEN_I8_%d/2)\n",i, i);
            fprintf (fp,"#define TDM%d_DMA_BUFFER_I8     (TSIP_MAX_ACTIVE_SLOTS_%d * 64)    // 64 = 4-bytes (16-bit WB) * 8 (frames/msec) * 2 (ping+pong) \n\n",i, i);
         } else {
            fprintf (fp,"#define TSIP_FALLOC_I8_%d       1\n",i);
            fprintf (fp,"#define TSIP_BUFLEN_I8_%d       1\n",i);
            fprintf (fp,"#define BSP%d_DMA_BUFFER_I16    1\n",i);
            fprintf (fp,"#define TDM%d_DMA_BUFFER_I8     2\n\n",i);
         }
      }
      fprintf (fp,"\n");
   } else {
      for (i=0; i<MAX_NUM_SERIAL_PORTS; i++) {
         fprintf (fp,"#define BSP%d_DMA_BUFFER_I16 %5d\n",i,SysCfg.dmaBufferSize[i]);
      }
   }


   fprintf (fp,"\n/*  Echo canceller config-time settings. \n   Run time over-rides must not exceed any of these. */\n");
   if (algBits.g168VarAEnable) 
      fprintf (fp, "#define SHORT_TAIL TRUE\n");

   fprintf (fp,"#define MAX_PCM_ECANS       %5d\n",   SysCfg.numPcmEcans);
   fprintf (fp,"#define CFG_PCM_TAP_LEN     %5d\n",   ecCfg->g168Pcm.TapLength);
   fprintf (fp,"#define CFG_PCM_NFIR_SEG    %5d\n",   ecCfg->g168Pcm.NFIRSegments);
   fprintf (fp,"#define CFG_PCM_FIR_SEG_LEN %5d\n\n", ecCfg->g168Pcm.FIRSegmentLength);

   fprintf (fp,"#define MAX_PKT_ECANS       %5d\n",   SysCfg.numPktEcans);
   fprintf (fp,"#define CFG_PKT_TAP_LEN     %5d\n",   ecCfg->g168Pkt.TapLength);
   fprintf (fp,"#define CFG_PKT_NFIR_SEG    %5d\n",   ecCfg->g168Pkt.NFIRSegments);
   fprintf (fp,"#define CFG_PKT_FIR_SEG_LEN %5d\n\n", ecCfg->g168Pkt.FIRSegmentLength);

   fprintf (fp,"#define CFG_PCM_ECAN_SAVELEN (6 + CFG_PCM_NFIR_SEG * (2 + CFG_PCM_FIR_SEG_LEN) + CFG_PCM_TAP_LEN/4)\n");
   fprintf (fp,"#define CFG_PKT_ECAN_SAVELEN (6 + CFG_PKT_NFIR_SEG * (2 + CFG_PKT_FIR_SEG_LEN) + CFG_PKT_TAP_LEN/4)\n\n");

   fprintf (fp,"#define MAX_AEC_ECANS       %5d\n",   SysCfg.AECInstances);
   fprintf (fp,"#define MAX_AEC_TAIL        %5d\n\n", SysCfg.AECTailLen);


   fprintf (fp,"#define MIC_COEF_I16        %5d\n",SysCfg.micEqLength);
   fprintf (fp,"#define SPKR_COEF_I16       %5d\n",SysCfg.spkrEqLength);

   if (SysCfg.micEqLength) 
      fprintf (fp,"#define MIC_EQ_STATE_I16    (MAX_AEC_ECANS*MIC_COEF_I16)\n");
   else
      fprintf (fp,"#define MIC_EQ_STATE_I16        1\n");


   if (SysCfg.spkrEqLength)
      fprintf (fp,"#define SPKR_EQ_STATE_I16   (MAX_AEC_ECANS*SPKR_COEF_I16)\n");
   else 
      fprintf (fp,"#define SPKR_EQ_STATE_I16       1\n");

#ifdef RTP_ALLOCATION
   fprintf (fp,  "\n// RTP jitter buffer sizes\n");
   fprintf (fp,"#define MAX_JITTER_MS       %5d\n",   SysCfg.maxJitterms);
   fprintf (fp,"#define SMALL_JB_BLK_I32    %5d\n",   SysCfg.smallJBBlockI32);
   fprintf (fp,"#define LARGE_JB_BLK_I32    %5d\n",   SysCfg.largeJBBlockI32);
   fprintf (fp,"#define HUGE_JB_BLK_I32    %5d\n",    SysCfg.hugeJBBlockI32);
   fprintf (fp,"#define MAX_RTP_BUFFS_PER_CHAN %5d\n", SysCfg.rtpMaxBuffsPerChan);

   if (SysCfg.smallJBBlockI32 == 0)    fprintf (fp,"#define SMALL_JB_POOL_I32       1\n");
   else                                fprintf (fp,"#define SMALL_JB_POOL_I32  (SMALL_JB_BLK_I32 * MAX_NUM_CHANNELS * MAX_JITTER_MS)\n");
   if (SysCfg.largeJBBlockI32 == 0)    fprintf (fp,"#define LARGE_JB_POOL_I32       1\n");
   else                                fprintf (fp,"#define LARGE_JB_POOL_I32  (LARGE_JB_BLK_I32 * MAX_NUM_CHANNELS * ((MAX_JITTER_MS / 10) + 1))\n");
   if (SysCfg.hugeJBBlockI32 == 0)     fprintf (fp,"#define HUGE_JB_POOL_I32       1\n");
   else                                fprintf (fp,"#define HUGE_JB_POOL_I32   (HUGE_JB_BLK_I32 * MAX_NUM_CHANNELS * ((MAX_JITTER_MS / 30) + 1))\n");

   if (SysCfg.hugeJBBlockI32 != 0)       fprintf (fp,"#define RTP_BUFFERS_I16    (MAX_RTP_BUFFS_PER_CHAN * HUGE_JB_BLK_I32 * MAX_NUM_CHANNELS*2)\n");
   else if (SysCfg.largeJBBlockI32 != 0) fprintf (fp,"#define RTP_BUFFERS_I16    (MAX_RTP_BUFFS_PER_CHAN * LARGE_JB_BLK_I32 * MAX_NUM_CHANNELS*2)\n");
   else if (SysCfg.smallJBBlockI32 != 0) fprintf (fp,"#define RTP_BUFFERS_I16    (MAX_RTP_BUFFS_PER_CHAN * SMALL_JB_BLK_I32 * MAX_NUM_CHANNELS*2)\n");
   else                                  fprintf (fp,"#define RTP_BUFFERS_I16    1\n\n");
#endif

   fprintf (fp, "\n//}\n");

}

static void writeCallerIDSizing (FILE *fp) {
   //  Caller ID sizing
   if (SysCfg.numRxCidChans == 0) {
      fprintf (fp, "#define RX_CID_MSG_I8  0\n");
   } else if (SysCfg.DspType == 647) {
      fprintf (fp, "#define RX_CID_MSG_I8  ((MAX_MESSAGE_BYTES + 0x7f) & 0xff80)\n");
   } else if (SysCfg.DspType == 667) {
      fprintf (fp, "#define RX_CID_MSG_I8  ((MAX_MESSAGE_BYTES + 0x7f) & 0xff80)\n");
   } else {
      fprintf (fp, "#define RX_CID_MSG_I8  MAX_MESSAGE_BYTES\n");
   }
   if (SysCfg.numTxCidChans == 0) {
      fprintf (fp, "#define TX_CID_MSG_I8  0\n");
      fprintf (fp, "#define TX_CID_BRST_I8 0\n");
   } else if (SysCfg.DspType == 647) {
      fprintf (fp, "#define TX_CID_MSG_I8  ((NUM_BURST_DATA_BYTES + 0x7f) & 0xff80)\n");
      fprintf (fp, "#define TX_CID_BRST_I8 ((MAX_BURST_BUFLEN + 0x7f)     & 0xff80)\n");
   } else if (SysCfg.DspType == 667) {
      fprintf (fp, "#define TX_CID_MSG_I8  ((NUM_BURST_DATA_BYTES + 0x7f) & 0xff80)\n");
      fprintf (fp, "#define TX_CID_BRST_I8 ((MAX_BURST_BUFLEN + 0x7f)     & 0xff80)\n");
   } else {
      fprintf (fp, "#define TX_CID_MSG_I8  NUM_BURST_DATA_BYTES\n");
      fprintf (fp, "#define TX_CID_BRST_I8 MAX_BURST_BUFLEN\n");
   }
}

static void writeIncludeFiles (FILE *fp) {
  
   //------------------------------------------------------------
   //   Include files
   //------------------------------------------------------------   
   fprintf (fp, "\n\n\n/*======================================= */\n");
   fprintf (fp, "//           Include files           \n\n");
   if ((DspInfo->DspType == Ti665) || ((DspInfo->DspType == Ti667))) {
        fprintf (fp, "#define _CSL_H_         // Avoid csl definitions\n");
        fprintf (fp, "#include <std.h>\n");
   }
   fprintf (fp, "#include \"sysconfig.h\"\n");
   fprintf (fp, "#include \"sysmem.h\"\n");    
   fprintf (fp, "#include \"GpakDefs.h\"\n");    
   if (IsC64) {
      fprintf (fp, "#include \"gpakpcmglob.h\"\n");
      fprintf (fp, "#include \"GpakDma.h\"\n\n");
   }
}

//=============================================================================
//                <project>.h   Main
//=============================================================================
static BOOL createGlobalHFile (blockStatus_t *bs, unsigned int numChannels,
                               ecConfig_t *ecCfg, char **pErrText, char *Root) {

   FILE *fp;
   char HeaderFileName[100];

   *pErrText = "?";

   // Open the Global Data file.
   sprintf (HeaderFileName, "%s.h", Root);
   fp = fopen (HeaderFileName,"w");
   if (fp == 0)   {
      *pErrText = "Unable to create header file !";
      return (FALSE);
   }


   //------------------------------------------------------------
   //   Versions
   //------------------------------------------------------------

   fprintf (fp, "\n\n\n/*======================================= */\n");
   fprintf (fp, "#define GPAK_SWVER 0x%04XU\n", VERS);
   fprintf (fp, "#define DSP_Type Ti%d", SysCfg.DspType);

   //------------------------------------------------------------
   //   Sizing definitions
   //------------------------------------------------------------
   writeSystemSizing (fp, bs, ecCfg, numChannels);
   writeIncludeFiles (fp);
   writeCallerIDSizing (fp);
   fprintf (fp,"\n#define EVENT_FIFO_BUFLEN_I16 ((sizeof(EventFifoCIDMsg_t) + (MAX_NUM_CHANNELS * sizeof(EventFifoMsg_t)) + sizeof(ADT_UInt32) + 1) / (sizeof(ADT_Int16)))\n");

   fprintf (fp,"\n");
   // Close the Global Data file.
   fclose (fp);
   return TRUE;
}


//}===================================================================
//                        Memory allocation file  "<project>.c" . 
//{
//   DMA Buffers
//   PCM Buffers
//   Packet Buffers
//   Encoder/Decoder Instance Buffers
//   Channel Info Buffers
//===================================================================

//=============================================================================
//            MEMORY SECTION ASSIGNMENTS   <project>.c & .cmd
//=============================================================================
void assignExtMemory (ExternalMemInfo_t *pExtMem, char *extMem, char *roMem, char *intProgMem, char *intDataMem) {

   // build the strings for the DMA and Overflow external memory areas
   //
   //  strBuff1 = External memory sections only
   //  strBuff2 = External and internal program sections
   //  strBuff3 = External and internal data sections
   //
   //     ExtDma -> External sections only
   //     cinitData, Daram, and FastDataFirst-> Internal and external (non-split)       
   //     SlowData, MediumData, SlowProg-> Internal and external (split)
   //
   char AppendExt[80];
   char PrependExt[80];
   char AppendRoExt[80];
   char PrependRoExt[80];
   if (pExtMem->type == EXT_NONE) extMem = NULL;
   int align = 8;

   if (SysCfg.DspType == Ti647) align = 128;
   else if (SysCfg.DspType == Ti667) align = 128;

   // strBuff1 = '> EXT'
   if (extMem) {
      sprintf (strBuff1,   "> %s", extMem);
      sprintf (AppendExt,  "| %s", extMem);
      sprintf (PrependExt, "%s |", extMem);
      ExtDmaArea = strBuff1;
   } else {
     AppendExt[0]  = 0;
     PrependExt[0] = 0;
     memset (strBuff1, 0, sizeof (strBuff1));
     ExtDmaArea = NULL;
   }

   if (roMem) {
      sprintf (AppendRoExt,  "| %s", roMem);
      sprintf (PrependRoExt, "%s |", roMem);
   } else {
     AppendRoExt[0]  = 0;
     PrependRoExt[0] = 0;
   }

   if (pExtMem->type != EXT_CACHE) {
     AppendExt[0]  = 0;
     PrependExt[0] = 0;
     AppendRoExt[0]  = 0;
     PrependRoExt[0] = 0;
     ExtDmaArea = NULL;
   }

   // strBuff2 = '>> EXT | INTP'
   sprintf (strBuff2, "ALIGN (%d) >> %s %s", align, PrependRoExt, intProgMem);
   SlowProg = strBuff2;
   
   // strBuff3 = '>  EXT | INTD'
   sprintf (strBuff3, "ALIGN (%d) > %s %s", align, PrependRoExt, intDataMem);
   SlowConst = cinitData = strBuff3;

   // strBuff4 = '>> EXT | INTD'
   sprintf (strBuff4, "ALIGN (%d) >> %s %s", align, PrependExt, intDataMem);
   SlowData = Daram = FastDataFirst = strBuff4;

   sprintf (strBuff5, "ALIGN (%d) >> %s %s", align, intProgMem, AppendRoExt);
   MediumProg = strBuff5;

   // strBuff6 = '>> INTD | EXT'
   sprintf (strBuff6, "ALIGN (%d) >> %s %s", align, intDataMem, AppendExt);
   MediumData = strBuff6;


}

//------------------------------------------------------------------
// Allocate the encoder and decoder instance structures <project>.c
//
static void writeCodecInstances (FILE *fp, unsigned numChannels) {
   
   unsigned i, ii;
   char *hideSymbols = "";  // Hide symbol names from map file.  First symbol not hidden to provide base address.

   //------- Channel Instance encode/decode buffers
   fprintf (fp,"\n\n//{ Per Channel ENCODE/DECODE BUFFERS\n");
   fprintf (fp,"const ADT_Word encoderInstI16 = ENCODER_INST_I16;\n");
   fprintf (fp,"const ADT_Word decoderInstI16 = DECODER_INST_I16;\n\n");

   for (i=0; i<numChannels; i++)   {
      fprintf (fp,"/* Channel %d */\n",i);
      fprintf (fp,"%sADT_UInt16 encoderInstance%d [ENCODER_BUFFER_I16];\n", hideSymbols, i);
      fprintf (fp,"%sADT_UInt16 decoderInstance%d [DECODER_BUFFER_I16];\n", hideSymbols, i);
      fprintf (fp, "#pragma DATA_SECTION (encoderInstance%d, \"ENCODER_INST_DATA\")\n", i);
      fprintf (fp, "#pragma DATA_SECTION (decoderInstance%d, \"DECODER_INST_DATA\")\n", i);
      fprintf (fp, "#pragma DATA_ALIGN   (encoderInstance%d, %d);\n", i, encodeMemory.align);
      fprintf (fp, "#pragma DATA_ALIGN   (decoderInstance%d, %d);\n", i, decodeMemory.align);
      hideSymbols = "static ";
   }

   if (numChannels > 1)    {
      fprintf (fp,"ADT_UInt16 * const encoderPool[MAX_NUM_CHANNELS] = {\n");
      writeElementNameArray (numChannels, "", "encoderInstance");

      fprintf (fp,"ADT_UInt16 * const decoderPool[MAX_NUM_CHANNELS] = {\n");
      writeElementNameArray (numChannels, "", "decoderInstance");

   }  else if (numChannels == 1)    {
      fprintf (fp,"ADT_UInt16 * const encoderPool[MAX_NUM_CHANNELS] = {encoderInstance0};\n");
      fprintf (fp,"ADT_UInt16 * const decoderPool[MAX_NUM_CHANNELS] = {decoderInstance0};\n");
   }
   fprintf (fp,"//}\n");

}

static void writeSARAM_DARAM_blocks (FILE *fp, blockStatus_t *bs, 
                                     Align *align, unsigned numChannels) {
    unsigned i, ii;

    // SARAM/DARAM Block status
    fprintf (fp,"\n\n/* MEMORY BLOCK DECLARATION */\n");
    fprintf (fp, "const blockStatus_t blockStatus = {\n");
    fprintf (fp,"%5d,\t\t // daramCount \n",         bs->daramCount);
    fprintf (fp,"\tDARAM_BLOCK_I16,\t\t // daramBlockSize\n");
    fprintf (fp,"%5d,\t\t\t\t\t // daramBaseAlignment\n   {\t",bs->daramBaseAlignment);

    for (i=0; i<NUM_IDS - 1; i++)    {
        fprintf (fp,"\t{%s, %4d},\t\t // daramAllocTable[] id, start\n",
                IDStr[bs->daramAllocTable[i].id], bs->daramAllocTable[i].start);
    }
    fprintf (fp,"\t{%s, %4d} },\t\t // daramAllocTable[] id, start\n",
                 IDStr[bs->daramAllocTable[i].id], bs->daramAllocTable[i].start);
 
    fprintf (fp,"%5d,\t\t // saramCount\n",         bs->saramCount);
    fprintf (fp,"\tSARAM_BLOCK_I16,\t\t // saramBlockSize\n");
    fprintf (fp,"%5d,\t\t\t\t\t // saramBaseAlignment\n   {\t",bs->saramBaseAlignment);

    for (i=0; i<NUM_IDS - 1; i++)    {
       fprintf (fp,"\t{%s, %4d},\t\t // saramAllocTable[] id, start\n",
                IDStr[bs->saramAllocTable[i].id], bs->saramAllocTable[i].start);
    }
    fprintf (fp,"\t{%s, %4d} }\t\t // saramAllocTable[] id, start\n",
                IDStr[bs->saramAllocTable[i].id], bs->saramAllocTable[i].start);
    fprintf (fp, "};\n");


    //------- Channel Instance encode/decode buffers
    fprintf (fp,"\n\n/* Per Channel ENCODE/DECODE BUFFERS */\n");
    for (i=0; i<numChannels; i++)   {
        fprintf (fp,"/* Channel %d */\n",i);
        fprintf (fp,"ADT_UInt16 saramChan%d [SARAM_BLOCK_I16];\n",i);
        fprintf (fp,"ADT_UInt16 daramChan%d [DARAM_BLOCK_I16];\n",i);
        ArrayAlign ("", saramChan, i, bs->saramBaseAlignment, 8, 8);
        ArrayAlign ("", daramChan, i, bs->daramBaseAlignment, 8, 8);
    }

    if (numChannels > 1)    {
        fprintf (fp,"ADT_UInt16 *saramPool[MAX_NUM_CHANNELS] = {\n");
        writeElementNameArray (numChannels, "", "saramChan");

        fprintf (fp,"ADT_UInt16 *daramPool[MAX_NUM_CHANNELS] = {\n");
        writeElementNameArray (numChannels, "", "daramChan");

    }  else   if (numChannels == 1)    {
        fprintf (fp,"ADT_UInt16 *saramPool[MAX_NUM_CHANNELS] = {saramChan0};\n");
        fprintf (fp,"ADT_UInt16 *daramPool[MAX_NUM_CHANNELS] = {daramChan0};\n");
    }

}


static void writeBufferAllocation (FILE *fp, blockStatus_t *bs,
                                   Align *align, unsigned numChannels) {
   unsigned i, poolAlignI8;
   char* Static;

    //--------------  Event info
    fprintf (fp, "\n/* Host event notification.   */\n");
    fprintf (fp, "ADT_UInt32 eventFIFOBuff[EVENT_FIFO_BUFLEN_I16/2];\n");
    fprintf (fp, "CircBufInfo_t  eventFIFOInfo;\n");
    pragmaSect (eventFIFOBuff, evt_POOL);
    pragmaSect (eventFIFOInfo, evt_CIRC);


    //----------- DMA buffers
    fprintf (fp,"\n\n/* DMA BLOCK DECLARATION */\n");
    fprintf (fp, "\n//{\n");

#ifdef MCASP_ALLOCATION
    // Temporarily hard-code enable of McASP0 if 64xx part. Used on DM642 DSK
    // 8 mS, double-buffered, 1 timeslot, 2 words-per-sample (for now), 1 serializer (per direction)
    if (IsC64) {
        fprintf (fp,"ADT_UInt16 ASP%dDMA_TxBuffer[ASP%d_DMA_BUFFER_I16];\n",0,0);
        fprintf (fp,"ADT_UInt16 ASP%dDMA_RxBuffer[ASP%d_DMA_BUFFER_I16];\n",0,0);
        fprintf (fp,"#pragma DATA_SECTION (ASP%dDMA_TxBuffer,\"ASP_DMABUFF%dT\")\n",0,0);
        fprintf (fp,"#pragma DATA_SECTION (ASP%dDMA_RxBuffer,\".dam:ASP_DMABUFF%dR\")\n\n",0,0);

        fprintf (fp,"ADT_UInt16 ASP%dDMA_TxBuffer[ASP%d_DMA_BUFFER_I16];\n",1,1);
        fprintf (fp,"ADT_UInt16 ASP%dDMA_RxBuffer[ASP%d_DMA_BUFFER_I16];\n",1,1);
        fprintf (fp,"#pragma DATA_SECTION (ASP%dDMA_TxBuffer,\"ASP_DMABUFF%dT\")\n",1,1);
        fprintf (fp,"#pragma DATA_SECTION (ASP%dDMA_RxBuffer,\"ASP_DMABUFF%dR\")\n\n",1,1);
    }
#endif
    
    for (i=0; i<MAX_NUM_SERIAL_PORTS; i++)    {
            fprintf (fp,"ADT_UInt16 BSP%dDMA_TxBuffer[BSP%d_DMA_BUFFER_I16];\n",i,i);
            fprintf (fp,"ADT_UInt16 BSP%dDMA_RxBuffer[BSP%d_DMA_BUFFER_I16];\n",i,i);

            fprintf (fp,"#pragma DATA_SECTION (BSP%dDMA_TxBuffer,\"DMABUFF%dT\")\n",i,i);
            fprintf (fp,"#pragma DATA_SECTION (BSP%dDMA_RxBuffer,\"DMABUFF%dR\")\n",i,i);
            if (tsipEnable) {
                fprintf (fp,"#pragma DATA_ALIGN   (BSP%dDMA_TxBuffer, 128)\n",i);
                fprintf (fp,"#pragma DATA_ALIGN   (BSP%dDMA_RxBuffer, 128)\n\n",i);
            } else {
                fprintf (fp,"\n");
            }
    }

     if (tsipEnable) {
         fprintf (fp,"ADT_UInt16 const TsipBuflenI8[3] = {\n");
         for (i=0; i<MAX_NUM_SERIAL_PORTS-1; i++)
             fprintf (fp,"    TSIP_BUFLEN_I8_%d,\n",i);
         fprintf (fp,"    TSIP_BUFLEN_I8_%d};\n",i);

         fprintf (fp,"ADT_UInt16 const TsipFallocI8[3] = {\n");
         for (i=0; i<MAX_NUM_SERIAL_PORTS-1; i++)
             fprintf (fp,"    TSIP_FALLOC_I8_%d,\n",i);
         fprintf (fp,"    TSIP_FALLOC_I8_%d};\n\n",i);

         // Transfer bufffer assignments
         Static = "";
         for (i=0; i<MAX_NUM_SERIAL_PORTS; i++) {
            fprintf (fp,"%sADT_PCM16 RxTransBuff%d[(TDM%d_DMA_BUFFER_I8)/2], TxTransBuff%d[(TDM%d_DMA_BUFFER_I8)/2];\n", Static, i,i,i,i);
            fprintf (fp,"#pragma DATA_ALIGN (RxTransBuff%d, 128)\n",i);
            fprintf (fp,"#pragma DATA_ALIGN (TxTransBuff%d, 128)\n",i);
            fprintf (fp,"#pragma DATA_SECTION (RxTransBuff%d, \"RxTransBuff\")\n",i);
            fprintf (fp,"#pragma DATA_SECTION (TxTransBuff%d, \"TxTransBuff\")\n\n",i);
            Static = "static ";
         }
         fprintf (fp,"const ADT_PCM16* RxTransBuffers[] = {\n");
         for (i=0; i<MAX_NUM_SERIAL_PORTS; i++) fprintf (fp," RxTransBuff%d, ",i);
         fprintf (fp,"};\n",i);
         fprintf (fp,"const ADT_PCM16* TxTransBuffers[] = {\n");
         for (i=0; i<MAX_NUM_SERIAL_PORTS; i++) fprintf (fp," TxTransBuff%d, ",i);
         fprintf (fp,"};\n\n",i);

    }


    for (i=0; i<MAX_NUM_SERIAL_PORTS; i++)    {
        if (SysCfg.numSlotsOnStream[i] != 0) {
            fprintf (fp,"ADT_UInt16 SlotMap%d[%d];\n",i, SysCfg.numSlotsOnStream[i]);
            fprintf (fp,"#pragma DATA_SECTION (SlotMap%d, \"FAST_DATA_SECT:TDM\")\n",i);
        } else {
            fprintf (fp,"ADT_UInt16 SlotMap%d[1];\n",i);
            fprintf (fp,"#pragma DATA_SECTION (SlotMap%d, \"STUB_DATA_SECT\")\n",i);
        }
    }

    fprintf (fp,"ADT_UInt16* const pSlotMap[%d] = {",MAX_NUM_SERIAL_PORTS);
    for (i=0; i<MAX_NUM_SERIAL_PORTS-1; i++)
        fprintf (fp,"SlotMap%d, ",i);
    fprintf (fp,"SlotMap%d};\n\n\n",i);


    for (i=0; i<MAX_NUM_SERIAL_PORTS; i++)    {
        if (SysCfg.numSlotsOnStream[i] != 0) {
            fprintf (fp,"ADT_UInt16 TxSlotMap%d[%d];\n",i, SysCfg.numSlotsOnStream[i]);
            fprintf (fp,"#pragma DATA_SECTION (TxSlotMap%d, \"FAST_DATA_SECT:TDM\")\n",i);
        } else {
            fprintf (fp,"ADT_UInt16 TxSlotMap%d[1];\n",i);
            fprintf (fp,"#pragma DATA_SECTION (TxSlotMap%d, \"STUB_DATA_SECT\")\n",i);
        }
    }

    fprintf (fp,"ADT_UInt16* const pTxSlotMap[%d] = {",MAX_NUM_SERIAL_PORTS);
    for (i=0; i<MAX_NUM_SERIAL_PORTS-1; i++)
        fprintf (fp,"TxSlotMap%d, ",i);
    fprintf (fp,"TxSlotMap%d};\n\n\n",i);

    for (i=0; i<MAX_NUM_SERIAL_PORTS; i++)    {
        if (SysCfg.maxSlotsSupported[i] != 0) {
            fprintf (fp,"CircBufInfo_t *CrcRxBuff%d[%d];\n",i, SysCfg.maxSlotsSupported[i]);
            fprintf (fp,"CircBufInfo_t *CrcTxBuff%d[%d];\n",i, SysCfg.maxSlotsSupported[i]);
            fprintf (fp,"#pragma DATA_SECTION (CrcRxBuff%d,  \"FAST_DATA_SECT:TDM\")\n",i);
            fprintf (fp,"#pragma DATA_SECTION (CrcTxBuff%d,  \"FAST_DATA_SECT:TDM\")\n",i);
        } else {
            fprintf (fp,"CircBufInfo_t *CrcRxBuff%d[1];\n",i);
            fprintf (fp,"CircBufInfo_t *CrcTxBuff%d[1];\n",i);
            fprintf (fp,"#pragma DATA_SECTION (CrcRxBuff%d,  \"STUB_DATA_SECT\")\n",i);
            fprintf (fp,"#pragma DATA_SECTION (CrcTxBuff%d,  \"STUB_DATA_SECT\")\n",i);
        }
    }

    fprintf (fp,"CircBufInfo_t** const pCrcRxBuff[%d] = {",MAX_NUM_SERIAL_PORTS);
    for (i=0; i<MAX_NUM_SERIAL_PORTS-1; i++)
        fprintf (fp,"CrcRxBuff%d, ",i);
    fprintf (fp,"CrcRxBuff%d};\n",i);

    fprintf (fp,"CircBufInfo_t** const pCrcTxBuff[%d] = {",MAX_NUM_SERIAL_PORTS);
    for (i=0; i<MAX_NUM_SERIAL_PORTS-1; i++)
        fprintf (fp,"CrcTxBuff%d, ",i);
    fprintf (fp,"CrcTxBuff%d};\n",i);
    fprintf (fp, "\n//}\n");


    //--------------- PCM Buffers
    fprintf (fp, "\n\n/* PCM BUFFER POOL DECLARATIONS */\n");
    fprintf (fp, "//{\n");
    fprintf (fp, "const ADT_UInt16 pcmInAlignI16     = PCMIN_ALIGN_I16;\n");
    fprintf (fp, "const ADT_UInt16 pcmOutAlignI16    = PCMOUT_ALIGN_I16;\n");
    fprintf (fp, "const ADT_UInt16 bulkDelayAlignI16 = BULKDELAY_ALIGN_I16;\n");

    fprintf (fp,"ADT_UInt16 PcmInBufferPool  [MAX_NUM_PCM_BUFFS][PCMIN_ALIGN_I16];\n");
    fprintf (fp,"ADT_UInt16 PcmOutBufferPool [MAX_NUM_PCM_BUFFS][PCMOUT_ALIGN_I16];\n");

   // Bulk delay buffers for PCM and packet echo cancellation
    if ((SysCfg.numPktEcans != 0) || 
       (((SysCfg.cfgFramesChans & PCM2PCM_CFG_BIT) != 0) && (SysCfg.numPcmEcans + SysCfg.AECInstances != 0))) {
        fprintf (fp,"ADT_UInt16 BulkDelayBufferPool[MAX_NUM_BULKDELAY_BUFFS][BULKDELAY_ALIGN_I16];\n");
    } else {
        fprintf (fp,"ADT_UInt16 BulkDelayBufferPool;  // Stub for linker\n");
    }

    if ((SysCfg.DspType == Ti647) && CacheActive) {
        poolAlignI8 = 128;
    } else if ((SysCfg.DspType == Ti667) && CacheActive) {
        poolAlignI8 = 128;
    } else if (tsipEnable) {
        poolAlignI8 = 8;
    } else {
        poolAlignI8 = 8;  // TG: buffers need to be 8bytes alliged for TI algo's to work on them
    }
    Align (PcmInBufferPool,     poolAlignI8, "pcmIn_POOL");
    Align (PcmOutBufferPool,    poolAlignI8, "pcmOut_POOL");
    Align (BulkDelayBufferPool, poolAlignI8, "blkDly_POOL");

    fprintf (fp,"ADT_UInt16 PcmBuffAvail = MAX_NUM_PCM_BUFFS;\n");
    fprintf (fp,"ADT_UInt16 PcmBuffInUse[MAX_NUM_PCM_BUFFS];\n");
    fprintf (fp,"#pragma DATA_SECTION (PcmBuffAvail, \"POOL_ALLOC\")\n");
    fprintf (fp,"#pragma DATA_SECTION (PcmBuffInUse, \"POOL_ALLOC\")\n");

    fprintf (fp,"ADT_UInt16 BulkDelayBuffAvail = MAX_NUM_BULKDELAY_BUFFS;\n");
    fprintf (fp,"ADT_UInt16 BulkDelayBuffInUse[MAX_NUM_BULKDELAY_BUFFS];\n");
    fprintf (fp,"#pragma DATA_SECTION (BulkDelayBuffAvail, \"POOL_ALLOC\")\n");
    fprintf (fp,"#pragma DATA_SECTION (BulkDelayBuffInUse, \"POOL_ALLOC\")\n");

    //----------- Packet Buffers
    fprintf (fp,"\n\n/*  Packet Buffer Info Structure  */\n");
    fprintf (fp,"/*    2 buffers (in and out) per channel */\n\n");
    fprintf (fp, "const ADT_UInt16 MaxDelayMs          = MAX_DELAY_MS_PKT_BUFS;\n");
    fprintf (fp, "const ADT_UInt16 pktBuffAlignI16     = PKT_ALIGN_I16;\n");

    fprintf (fp,"CircBufInfo_t pktBuffStructTable [2][MAX_NUM_CHANNELS];\n");
    fprintf (fp,"ADT_UInt16 PktInBufferPool    [MAX_NUM_PKT_BUFFS][PKT_ALIGN_I16];\n");
    fprintf (fp,"ADT_UInt16 PktOutBufferPool   [MAX_NUM_PKT_BUFFS][PKT_ALIGN_I16];\n");
    Align (pktBuffStructTable, 4, "pkt_CIRC");
    Align (PktInBufferPool, poolAlignI8, "pktIn_POOL");
    Align (PktOutBufferPool, poolAlignI8, "pktOut_POOL");
    fprintf (fp,"ADT_UInt16 PktBuffAvail = MAX_NUM_PKT_BUFFS;\n");
    fprintf (fp,"ADT_UInt16 PktBuffInUse[MAX_NUM_PKT_BUFFS];\n");
    fprintf (fp,"#pragma DATA_SECTION (PktBuffAvail, \"POOL_ALLOC\")\n");
    fprintf (fp,"#pragma DATA_SECTION (PktBuffInUse, \"POOL_ALLOC\")\n");
    fprintf (fp, "\n//}\n");

    //------------- Channel info
    fprintf (fp,"\n\n\n/*======================================= */\n");
    fprintf (fp,"//      G.PAK channel data.\n");
    fprintf (fp, "struct chanInfo* chanTable[MAX_NUM_CHANNELS];\n");
    fprintf (fp, "chanInfo_t GpakChanInstance[MAX_NUM_CHANNELS];\n");
    fprintf (fp, "#pragma DATA_SECTION (chanTable, \"CHAN_INST_DATA:Chan\")\n");
    fprintf (fp, "#pragma DATA_ALIGN   (chanTable, 128)\n");
    fprintf (fp, "#pragma DATA_SECTION (GpakChanInstance, \"CHAN_INST_DATA:Chan\")\n\n");



#ifdef INSTANCE_ALLOCATION
    writeCodecInstances (fp, numChannels);
#else
    writeSARAM_DARAM_blocks (fp, bs, align, numChannels);
#endif
    
    
}

//=============================================================================
//             System Configuration (sysCfg) Structure Allocation <project>.c
//=============================================================================
static void writeSystemConfiguration (FILE *fp) {

   unsigned i;
   char *suffix;
   sysConfig_t *cfg = &SysCfg;

   fprintf (fp,"sysConfig_t sysConfig = {\n");
   fprintf (fp,"0x%08lX,\t\t// enab.Bits\n",   SysCfg.enab.LongWord);

   // Config tool must only set speex or speexWB.  DSP needs speex set if speexWB is set.
   if (cdcBits.speexWBEnable) cdcBits.speexEnable = 1;
   fprintf (fp,"0x%08lX,\t\t// Codecs.Bits\n", SysCfg.Codecs.LongWord);
   if (cdcBits.speexWBEnable) cdcBits.speexEnable = 0;

   fprintf (fp,"GPAK_SWVER,\t\t// gpakVersion\n");

   if (SysCfg.DspType == 647) suffix = "";
   else if (SysCfg.DspType == 674) suffix = "";
   else if (SysCfg.DspType == 665)  suffix = "";
   else if (SysCfg.DspType == 667)  suffix = "";
   else if (SysCfg.DspType < 1000)  suffix = "DM";
   else if (SysCfg.DspType == 3530) suffix = "OM";
   else                           suffix = "";
   fprintf (fp,"Ti%s%d,\t\t// DspType\n", suffix, SysCfg.DspType);

    WritePtrField (cfg, hpiBlockStartAddress);
    WriteIntField (cfg, DspMips);

    WriteIntArray (cfg, numSlotsOnStream,    MAX_NUM_SERIAL_PORTS);
    WriteIntArray (cfg, maxSlotsSupported,   MAX_NUM_SERIAL_PORTS);
    WriteIntArray (cfg, serialWordSize,      MAX_NUM_SERIAL_PORTS);

    fprintf (fp,"{");                       
    for (i=0; i<MAX_NUM_SERIAL_PORTS; i++)
      WriteEnumArrayField (cfg, compandingMode, i);
    fprintf (fp,"},\n");                       

    WriteIntArray (cfg, dmaBufferSize,       MAX_NUM_SERIAL_PORTS);
    WriteIntArray (cfg, txFrameSyncPolarity, MAX_NUM_SERIAL_PORTS);
    WriteIntArray (cfg, rxFrameSyncPolarity, MAX_NUM_SERIAL_PORTS);
    WriteIntArray (cfg, txClockPolarity,     MAX_NUM_SERIAL_PORTS);
    WriteIntArray (cfg, rxClockPolarity,     MAX_NUM_SERIAL_PORTS);
    WriteIntArray (cfg, txDataDelay,         MAX_NUM_SERIAL_PORTS);
    WriteIntArray (cfg, rxDataDelay,         MAX_NUM_SERIAL_PORTS);
    WriteIntArray (cfg, dxDelay,             MAX_NUM_SERIAL_PORTS);

    WriteEnumField (cfg, packetProfile);
    fprintf (fp,"(GpakRTPPkts) %u,\t\t// rtpPktType\n",SysCfg.rtpPktType);
    fprintf (fp,"0x%04X,\t\t// cfgFramesChans\n",SysCfg.cfgFramesChans);
    
    WriteIntField (cfg, agcTargetPower);
    WriteIntField (cfg, agcMinGain);
    WriteIntField (cfg, agcMaxGain);

    WriteIntField (cfg, agcLowSigThreshDBM);

    WriteIntField (cfg, agcTargetPowerB);
    WriteIntField (cfg, agcMinGainB);
    WriteIntField (cfg, agcMaxGainB);
    WriteIntField (cfg, agcLowSigThreshDBMB);

    WriteIntField (cfg, vadNoiseFloorZThresh);
    WriteIntField (cfg, vadHangOverTime);
    WriteIntField (cfg, vadWindowMs);

    fprintf (fp,"MAX_NUM_CHANNELS,       // maxNumChannels\n");
    fprintf (fp,"MAX_GPAK_CONFERENCES,   // maxNumConferences\n");
    fprintf (fp,"MAX_CUSTOM_CONFERENCES, // customCnfrCnt\n");
    fprintf (fp,"MAX_NUM_CONF_CHANS,     // maxNumConfChans\n");

    fprintf (fp,"CFG_PCM_TAP_LEN,        // maxG168PcmTapLength\n");
    fprintf (fp,"CFG_PKT_TAP_LEN,        // maxG168PktTapLength\n");
    fprintf (fp,"CFG_PCM_NFIR_SEG,       // maxG168PcmNFIRSegments\n");
    fprintf (fp,"CFG_PKT_NFIR_SEG,       // maxG168PktNFIRSegments\n");
    fprintf (fp,"CFG_PCM_FIR_SEG_LEN,    // maxG168PcmFIRSegmentLength\n");
    fprintf (fp,"CFG_PKT_FIR_SEG_LEN,    // maxG168PktFIRSegmentLength\n");
    fprintf (fp,"SARAM_BLOCK_I16,        // saramBlockSize\n");
    fprintf (fp,"DARAM_BLOCK_I16,        // daramBlockSize\n");
    fprintf (fp,"PCMIN_BUFFER_I16,       // pcmInBufferSize\n");
    fprintf (fp,"PCMOUT_BUFFER_I16,      // pcmOutBufferSize\n");
    fprintf (fp,"PKT_BUFFER_I16,         // pktInOutBufferSize\n");
    fprintf (fp,"BULKDELAY_BUFFER_I16,   // bulkDelayBufferSize\n");
    fprintf (fp,"MAX_DOMINANT_N,         // numConfDominant\n");
    WriteIntField (cfg, maxConfNoiseSuppress);
    fprintf (fp,"MAX_PCM_ECANS,          // numPcmEcans\n");
    fprintf (fp,"MAX_PKT_ECANS,          // numPktEcans\n");
    fprintf (fp,"MAX_TONE_DET_TYPES,     // maxToneDetTypes\n");

    fprintf (fp,"MAX_AEC_ECANS,            // AECInstances\n");
    fprintf (fp,"MAX_AEC_TAIL,             // AECTailLen\n");
    fprintf (fp,"MAX_NUM_FAX_CHANS,        // numFaxChans\n");
    fprintf (fp,"MAX_NUM_TONEGEN_CHANNELS, // numTGChans\n");
    fprintf (fp,"MAX_NUM_AGC_CHANNELS,     // numAGCChans\n");

    WriteIntField (cfg, HostIF);
    WriteIntField (cfg, samplesPerMs);
    fprintf (fp,"MAX_JITTER_MS,            // maxJitterms\n");
    fprintf (fp,"SMALL_JB_BLK_I32,         // smallJBBlockI32\n");
    fprintf (fp,"LARGE_JB_BLK_I32,         // largeJBBlockI32\n");
    fprintf (fp,"HUGE_JB_BLK_I32,          // hugeJBBlockI32\n");
    fprintf (fp,"RTP_BUFFERS_I16,          // RTPBuffersI16\n");

    fprintf (fp,"MAX_NUM_TXCID_CHANNELS,   // numTxCidChans,\n");
    fprintf (fp,"MAX_NUM_RXCID_CHANNELS,   // numRxCidChans,\n");
    WriteIntField (cfg, type2CID);

    WriteIntField (cfg, numSeizureBytes);
    WriteIntField (cfg, numMarkBytes);
    WriteIntField (cfg, numPostambleBytes);
    fprintf (fp,"(GpakFskType) %3d,          // fskType\n", SysCfg.fskType);
    WriteIntField (cfg, fskLevel);
    WriteIntField (cfg, msgType);

    fprintf (fp,"EVENT_FIFO_BUFLEN_I16,    // evtFifoLenI16\n");
    fprintf (fp,"MAX_NUM_PCM_BUFFS,        // maxNumPcmBuffs\n");
    fprintf (fp,"MAX_NUM_PKT_BUFFS,        // maxNumPktBuffs\n");
    fprintf (fp,"MAX_NUM_BULKDELAY_BUFFS,  // maxNumBulkDelayBuffs\n");
    fprintf (fp,"(GpakActivation) %3d,     // VadReport\n", SysCfg.VadReport);
    fprintf (fp,"CFG_PCM_ECAN_SAVELEN,     // EcanPcmSaveLenI16\n");
    fprintf (fp,"CFG_PKT_ECAN_SAVELEN,     // EcanPktSaveLenI16\n");
    fprintf (fp,"MAX_NUM_LB_CHANNELS,      // maxNumLbCoders\n");
    fprintf (fp,"MAX_RTP_BUFFS_PER_CHAN,   // rtpMaxBuffsPerChan\n");
    fprintf (fp,"MIC_COEF_I16,             // Aec Microphone equalizer length\n");
    fprintf (fp,"SPKR_COEF_I16,            // Aec Speaker equalizer length\n");
    fprintf (fp,"%d,                        // g168MipsConserveLev;\n",0); // todo... must fill-in via gui: SysCfg.g168MipsConserveLev);
    fprintf (fp,"MAX_NUM_SRC_CHANNELS, // numSrcChans\n");
    WriteIntField (cfg, SRTP_Enable);
    fprintf (fp,"};\n");
    fprintf (fp, "#pragma DATA_SECTION (sysConfig, \"NON_CACHED_DATA:sysConfig\")\n");

}

//=============================================================================
//             Frame Task Buffer Allocation  <project>.c
//=============================================================================

static char *BufferFormat = "\n\nADT_UInt16 inWork_%smsec[%3d], \toutWork_%smsec[%3d], \tECFarWork_%smsec[%3d];\n";
static char *AlignFormat  = "#pragma DATA_ALIGN (inWork_%smsec,   4)\n#pragma DATA_ALIGN (outWork_%smsec,   4)\n#pragma DATA_ALIGN (ECFarWork_%smsec, 4)\n\n";

static void WriteFramingStructures (FILE *fp) {
   int m, frameCnt;
   fprintf (fp, "\n//{\n");

   frameCnt = frameTaskCnt * coreCnt;
   fprintf (fp, "FrameTaskData_t FrameTaskData[%u];\n", frameCnt);
   fprintf (fp, "#pragma DATA_SECTION (FrameTaskData,  \"SHARED_DATA_SECT:FrameTaskData\")\n\n", frameCnt);

   fprintf (fp, "chanInfo_t *PendingEncodeQueue [%u];\n", frameCnt);
   fprintf (fp, "chanInfo_t *PendingDecodeQueue [%u];\n", frameCnt);
   fprintf (fp, "#pragma DATA_SECTION (PendingEncodeQueue,  \"SHARED_DATA_SECT:EncodeQueue\")\n");
   fprintf (fp, "#pragma DATA_SECTION (PendingDecodeQueue,  \"SHARED_DATA_SECT:DecodeQueue\")\n\n");

   fprintf (fp, "chanInfo_t *PPendingEncodeQueue [%u];\n", frameCnt);
   fprintf (fp, "chanInfo_t *PPendingDecodeQueue [%u];\n", frameCnt);
   fprintf (fp, "#pragma DATA_SECTION (PPendingEncodeQueue,  \"SHARED_DATA_SECT:EncodeQueue\")\n");
   fprintf (fp, "#pragma DATA_SECTION (PPendingDecodeQueue,  \"SHARED_DATA_SECT:DecodeQueue\")\n\n");

   if (WBCodecEnabled || SysCfg.samplesPerMs == 16) m = 2;
   else                                           m = 1;

   if (FRAME_1ms_ENABLED)   allocBuffers ("1", 40*m, 24*m, 8*m)
   else                     allocBuffers ("1", 1, 1, 1)

   if (FRAME_2ms_ENABLED)   allocBuffers ("2_5", 40*m, 24*m, 20*m)
   else                     allocBuffers ("2_5", 1, 1, 1)

   if (FRAME_5ms_ENABLED)   allocBuffers ("5", 40*m, 40*m, 40*m)
   else                     allocBuffers ("5", 1, 1, 1)

   if (FRAME_10ms_ENABLED)  allocBuffers ("10", 80*m, 80*m, 80*m)
   else                     allocBuffers ("10", 1, 1, 1)

   if (FRAME_20ms_ENABLED)  allocBuffers ("20", 160*m, 160*m, 160*m)
   else                     allocBuffers ("20", 1, 1, 1)

#ifdef MELP
   if (FRAME_22ms_ENABLED)  allocBuffers ("22_5", 180*m, 180*m, 180*m)
   else                     allocBuffers ("22_5", 1, 1, 1)
#endif

   if (FRAME_30ms_ENABLED)  allocBuffers ("30", 240*m, 240*m, 240*m)
   else                     allocBuffers ("30", 1, 1, 1)
   

   fprintf (fp,"\n\n\n/*=============================== */\n");
   fprintf (fp, "/* Host Port Interrupt flags. */\n\n");
#ifdef MELP
   fprintf (fp, "const ADT_UInt16 Hint_1ms    = %d;\n", Hint8);
   fprintf (fp, "const ADT_UInt16 Hint_2_5ms  = %d;\n", Hint20);
   fprintf (fp, "const ADT_UInt16 Hint_5ms    = %d;\n", Hint40);
   fprintf (fp, "const ADT_UInt16 Hint_10ms   = %d;\n", Hint80);
   fprintf (fp, "const ADT_UInt16 Hint_20ms   = %d;\n", Hint160);
   fprintf (fp, "const ADT_UInt16 Hint_22_5ms = %d;\n", Hint180);
   fprintf (fp, "const ADT_UInt16 Hint_30ms   = %d;\n", Hint240);
#else
   fprintf (fp, "const ADT_UInt16 Hint   = %d;\n", Hint8);
   fprintf (fp, "const ADT_UInt16 Hint20  = %d;\n", Hint20);
   fprintf (fp, "const ADT_UInt16 Hint40  = %d;\n", Hint40);
   fprintf (fp, "const ADT_UInt16 Hint80  = %d;\n", Hint80);
   fprintf (fp, "const ADT_UInt16 Hint160 = %d;\n", Hint160);
   fprintf (fp, "const ADT_UInt16 Hint240 = %d;\n", Hint240);
#endif
   fprintf (fp, "\n//}\n");

}

/*=============================== */
// T.38 Fax Relay Structures. 
static void WriteT38Structures (FILE *fp) {

    int i;

    if (SysCfg.numFaxChans <= 0)   {
        // satisfy Linker if no T38 members are configured.
        fprintf (fp, "ADT_UInt16 const faxChanInUse[1] = {0};\n");
        fprintf (fp, "ADT_UInt16 const numFaxChansUsed = 0;\n");
        fprintf (fp, "ADT_UInt16  * const pFaxChan[1] = {0};\n");
        fprintf (fp, "t38Packet_t * const pT38Packets[1] = {0};\n");
        return;
    }

    fprintf (fp, "ADT_UInt16 numFaxChansUsed = 0;\n");
    fprintf (fp, "ADT_UInt16 faxChanInUse[MAX_NUM_FAX_CHANS];\n");
    fprintf (fp,"#pragma DATA_SECTION (faxChanInUse,    \"POOL_ALLOC\")\n\n");

    for (i=0; i<SysCfg.numFaxChans; i++)
        fprintf (fp, "ADT_UInt16 faxChan%d[RLY_CHANNEL_SIZE];\n", i);
    
    fprintf (fp, "ADT_UInt16 * const pFaxChan[] = {\n");
    if (SysCfg.numFaxChans > 1)  {
        for (i=0; i<SysCfg.numFaxChans - 1; i++)
            fprintf (fp, "\tfaxChan%d,\n", i);
    }
    i = SysCfg.numFaxChans - 1;
    fprintf (fp, "\tfaxChan%d\n};\n",i);

   for (i=0; i<SysCfg.numFaxChans; i++) {
       fprintf (fp,"#pragma DATA_SECTION (faxChan%d, \"CHAN_INST_DATA:FAX\")\n", i);
       fprintf (fp,"#pragma DATA_ALIGN   (faxChan%d, %d)\n", i, 8);
   }
   fprintf (fp, "\n");

   for (i=0; i<SysCfg.numFaxChans; i++) {
        fprintf (fp, "t38Packet_t t38Packet%d;\n", i);
        //fprintf (fp, "#pragma DATA_SECTION (t38Packet%d, \"CHAN_INST_DATA:FAX\")\n", i);
        fprintf (fp,"#pragma DATA_SECTION (t38Packet%d, \"T38PACKET\")\n", i,i);
   }

   fprintf (fp, "t38Packet_t *pT38Packets[] = {\n");
   if (SysCfg.numFaxChans > 1) {
        for (i=0; i<SysCfg.numFaxChans - 1; i++)
            fprintf (fp, "\t&t38Packet%d,\n", i);
   }
   
   i = SysCfg.numFaxChans - 1;
   fprintf (fp, "\t&t38Packet%d\n};\n", i);
/*
   for (i=0; i<SysCfg.numFaxChans; i++) {
      fprintf (fp,"#pragma DATA_SECTION (t38Packet%d, \"T38PACKET\")\n", i,i);
   }
*/  
   fprintf (fp, "\n");

}

static void T38DataAlign (FILE *fp, char *Section) {
 
   if (SysCfg.numFaxChans <= 0)  return;
   
   fprintf (fp, "\n\n/* T38 Fax Relay Memory Requirements */\n");
   fprintf (fp, "  CHAN_INST_DATA:FAX   : {} %s\n", Section);
   fprintf (fp, "  T38PACKET : {} %s\n", Section);
}

static void WriteTIT38Structures (FILE *fp) {


   fprintf (fp, "ADT_UInt16 numFaxChansUsed = 0;\n");
   fprintf (fp,"#pragma DATA_SECTION (numFaxChansUsed,  \"POOL_ALLOC\")\n");
   if (SysCfg.numFaxChans <= 0)   {
      // satisfy Linker if no T38 members are configured.
      fprintf (fp, "ADT_UInt16 faxChanInUse[1] = {0};\n");
      return;
   }
   fprintf (fp, "ADT_UInt16 faxChanInUse[MAX_NUM_FAX_CHANS];\n");
   fprintf (fp,"#pragma DATA_SECTION (faxChanInUse,  \"POOL_ALLOC\")\n\n");
 
   // TI Instance sizes
   if (SysCfg.DspType != Ti647) {
      fprintf (fp, "#define FM_INST_I8  3936\n");
      fprintf (fp, "#define FIU_INST_I8 6104\n\n");
   } else {
      // Align on extern cache boundary for usage across cores
      fprintf (fp, "#define FM_INST_I8  3968\n");
      fprintf (fp, "#define FIU_INST_I8 6144\n\n");
   }
   
   fprintf (fp, "const ADT_UInt32 fmInstI8  = FM_INST_I8;\n");
   fprintf (fp, "const ADT_UInt32 fiuInstI8 = FIU_INST_I8;\n");
   fprintf (fp, "ADT_UInt64 fmInst [((FM_INST_I8 + 7) / 8) * MAX_NUM_FAX_CHANS];\n");
   fprintf (fp, "ADT_UInt64 fiuInst[((FIU_INST_I8 + 7) / 8) * MAX_NUM_FAX_CHANS];\n\n");
   fprintf (fp, "TI_T38     TI_Fax[MAX_NUM_FAX_CHANS];\n");
   Align (fmInst,  128, "CHAN_INST_DATA:FAX");
   Align (fiuInst, 128, "CHAN_INST_DATA:FAX");
   Align (TI_Fax,  8,   "NON_CACHED_DATA:FAX");
   fprintf (fp, "\n");

}

static void TIT38DataAlign (FILE *fp, char *Section) {
 
   if (SysCfg.numFaxChans <= 0)  return;
   
   fprintf (fp, "\n\n/* T38 Fax Relay Memory Requirements */\n");
   fprintf (fp, "  CHAN_INST_DATA:FAX   : {} %s\n", Section);
}
   

//=============================================================================
//             Conference Data Structure Allocation  <project>.c
//=============================================================================
static void WriteConferenceStructures (FILE *fp) {

   unsigned NumConfScratchMem;  // num Conference scratch memory instances
   unsigned alignI8, cnfrCnt, tgCnt;

   if (SysCfg.DspType == Ti647)
      alignI8 = 128;
   else if (SysCfg.DspType == Ti667)
      alignI8 = 128;
   else
      alignI8 = 0;

   if (SysCfg.maxNumConferences <= 0)   {
      // satisfy Linker if no conferences are configured.
      fprintf (fp, "ConferenceInfo_t *ConferenceInfo[1] = {0};\n");
      fprintf (fp, "ADT_UInt16 ConferenceInst;\n");
      fprintf (fp, "ADT_UInt16 ConfScratch;\n");
      fprintf (fp, "ADT_UInt16 ConfMemberData;\n");
      fprintf (fp, "ADT_UInt16 ConfVadData;\n");
      fprintf (fp, "ADT_UInt16 pConfScratch;\n");
      fprintf (fp, "ADT_UInt16 ConfIndexType;\n");
      fprintf (fp, "ADT_UInt16 ConfAgcInstance;\n");
      fprintf (fp, "ADT_UInt16 ConfToneGenInstance;\n");
      fprintf (fp, "ADT_UInt16 ConfToneGenParams;\n");
      fprintf (fp, "ADT_UInt16 PendingMemberQueue;\n");
      fprintf (fp, "ADT_UInt16 PendingCompositeQueue;\n");
      fprintf (fp, "ADT_UInt16 PPendingMemberQueue;\n");
      fprintf (fp, "ADT_UInt16 PPendingCompositeQueue;\n");
      return;
   }
   cnfrCnt = SysCfg.maxNumConferences * coreCnt;

   if (SysCfg.maxNumConferences < NumFrameRates)    {
      NumConfScratchMem = SysCfg.maxNumConferences;
      fprintf (fp,  "const ADT_UInt16 ConfIndexType = 0;   //  Scratch indexed by Conference\n");
   } else {
      NumConfScratchMem = NumFrameRates;
      fprintf (fp,  "const ADT_UInt16 ConfIndexType = 1;   //  Scratch indexed by Frame Rate\n");
   }

   //----------------------------------  GPAK conference data ------------------------------------------
   fprintf (fp, "//   G.PAK conference information structures\n");

   if (!IsC64) {
      fprintf (fp, "ConfScratch_User_t  ConfScratch  [NUM_CONF_SCRATCH_MEM];\n");
   } else {
#ifdef COMBINED_SCRATCH
      fprintf (fp, "ADT_UInt32     ConfScratch  [1];\n");
#else
      fprintf (fp, "ConfScratch_t  ConfScratch  [NUM_CONF_SCRATCH_MEM];\n");
      fprintf (fp, "#pragma DATA_SECTION (ConfScratch, \"PER_CORE_DATA:Scratch\")\n");
#endif
   }

    //-----------------------------  ConfInstances ---------------------------------------
   generateArrayElements (fp, cnfrCnt, "ConferenceInfo_t", "ConferenceInfo", 
                                       "SHARED_DATA_SECT:Conference", alignI8);

   if (!IsC64) {
      generateArrayElements (fp, cnfrCnt, "Conf_ChanInst_ADT_t", "ConferenceInst",
                                       "CONF_INST_DATA:Conference", alignI8);
   } else {
      generateArrayElements (fp, cnfrCnt, "ConfInstance_t", "ConferenceInst",
                                       "CONF_INST_DATA:Conference", alignI8);
   }
   generateArrayElements (fp, cnfrCnt, "AGCInstance_t", "ConfAgcInstance",
                                       "CONF_INST_DATA:AGC", alignI8);

   tgCnt = cnfrCnt;
   if (algBits.wbEnable) tgCnt *= 2;
   generateArrayElements (fp, tgCnt, "TGInstance_t", "ConfToneGenInstance",
                                       "CONF_INST_DATA:ToneGen", alignI8);

   generateArrayElements (fp, tgCnt, "TGParams_1_t", "ConfToneGenParams",
                                       "CONF_INST_DATA:ToneGenParams", alignI8);

   fprintf (fp, "chanInfo_t *PendingMemberQueue    [MAX_GPAK_CONFERENCES * CORE_CNT];\n");
   fprintf (fp, "chanInfo_t *PendingCompositeQueue [MAX_GPAK_CONFERENCES * CORE_CNT];\n");
   fprintf (fp, "#pragma DATA_SECTION (PendingMemberQueue,    \"SHARED_DATA_SECT:MemberQueue\")\n" );
   fprintf (fp, "#pragma DATA_SECTION (PendingCompositeQueue, \"SHARED_DATA_SECT:CompositeQueue\")\n\n" );

   fprintf (fp, "chanInfo_t *PPendingMemberQueue    [MAX_GPAK_CONFERENCES * CORE_CNT];\n");
   fprintf (fp, "chanInfo_t *PPendingCompositeQueue [MAX_GPAK_CONFERENCES * CORE_CNT];\n");
   fprintf (fp, "#pragma DATA_SECTION (PPendingMemberQueue,    \"SHARED_DATA_SECT:MemberQueue\")\n" );
   fprintf (fp, "#pragma DATA_SECTION (PPendingCompositeQueue, \"SHARED_DATA_SECT:CompositeQueue\")\n\n" );

   fprintf (fp, "\n//   Per channel conferencing structures\n");
   generateArrayElements (fp, SysCfg.maxNumChannels, "conf_Member_t", "ConfMemberData",
                                                   "CHAN_INST_DATA:Conference", alignI8);
    
   generateArrayElements (fp, SysCfg.maxNumChannels, "VADCNG_Instance_t", "ConfVadData",
                                                   "CHAN_INST_DATA:Conference", alignI8);


#ifndef COMBINED_SCRATCH   
    if (SysCfg.maxNumConferences < NumFrameRates) {
        if (!IsC64)
            fprintf (fp, "ConfScratch_User_t *pConfScratch[NUM_CONF_SCRATCH_MEM] = {\n");
        else
            fprintf (fp, "ConfScratch_t *pConfScratch[NUM_CONF_SCRATCH_MEM] = {\n");
        writeElementPtrArray (NumConfScratchMem, "ConfScratch");
    } else {

        if (!IsC64)
            fprintf (fp, "ConfScratch_User_t *pConfScratch[] = {\n");
        else
            fprintf (fp, "ConfScratch_t *pConfScratch[] = {\n");
       i = 0;

       WriteConferenceScratch (FRAME_8_CFG_BIT);
       WriteConferenceScratch (FRAME_20_CFG_BIT);
       WriteConferenceScratch (FRAME_40_CFG_BIT);
       WriteConferenceScratch (FRAME_80_CFG_BIT);
       WriteConferenceScratch (FRAME_160_CFG_BIT);
#ifdef MELP
       WriteConferenceScratch (FRAME_180_CFG_BIT);
#endif
       WriteConferenceScratch (FRAME_240_CFG_BIT);

       fprintf (fp, "};\n");  
    }
#endif

}

//=============================================================================
//            ECHO CANCELLATION CONFIGURATION    <project>.c
//=============================================================================
#ifdef COMBINED_SCRATCH   
   #define writeECScratch(rate)                           \
       fprintf (fp, "\nADT_UInt16 G168DAscratch_%sms [%3d];\n"     \
                    "union {\n"                                    \
                    "   GPAK_TD_Scratch   TDScratch;\n"            \
                    "   GPAK_Cnfr_Scratch CnfrScratch;\n"          \
                    "   ADT_UInt16        SAScratch [%3d];\n"      \
                    "   ADT_UInt32        SRTPScratch [%3d];\n"      \
                    "} G168SAscratch_%sms;\n",                     \
                   #rate, DAI16_##rate##ms, SAI16_##rate##ms, SAI32_srtp, #rate); \
       fprintf (fp, "#pragma DATA_SECTION (G168DAscratch_%sms, \"SLOW_SCRATCH\")\n", #rate); \
       fprintf (fp, "#pragma DATA_SECTION (G168SAscratch_%sms, \"FAST_SCRATCH\")\n\n", #rate); 

#else
   #define writeECScratch(rate,)                                    \
       fprintf (fp, "ADT_UInt16 G168SAscratch_%sms [%3d],\t G168DAscratch_%sms [%3d];\n", \
              #rate, SAI16_##rate##ms, #rate, DAI16_##rate##ms);
#endif


void EchoCancellerAlign (FILE *fp, char *Type, int numEcans, EcAlign *align, ADT_Bool ShortTail);

static void WriteEchoCancellerParams (FILE *fp, char *Type, EcParams *ecCfg) {

    int Version;
    int segmentLength;

    if (IsC64)
      Version = ActiveC64xOverhead->Version;
    else if (IsC55)
      Version = ActiveC55xOverhead->Version;
    else
      Version = ActiveC54xOverhead->Version;

    // Echo canceller parameters
    fprintf (fp,"\n\n/* %s ECHO CANCELLER DECLARATION */\n", Type);
    fprintf (fp, "G168Params_t g168_%s_EcParams = {\n", Type);
    if (0x1100 <= Version) {
        fprintf (fp, "ADT_EC_API_VERSION,    // APIVersion\n");
        fprintf (fp, "    0,                 // ChannelSizeI8    Channel (Instance) structure size, in bytes\n");
        fprintf (fp, "    0,                 // EchoPathSizeI8   Echo path array size, in bytes\n");
        fprintf (fp, "    0,                 // DAStateSizeI8    DAState structure size in bytes\n");
        fprintf (fp, "    0,                 // SAStateSizeI8    SAState structure size in bytes\n");
        fprintf (fp, "    0,                 // SAScratchSizeI8  SAScratch structure size in bytes\n");
        fprintf (fp, "    0,                 // BGEchoPathSizeI8 BGEchoPath array size in bytes\n");
        fprintf (fp, "    0,                 // DAScratchSizeI8  DAScratch structure size in bytes\n");
    }
    fprintf (fp, "    0,\t\t// FrameSize\n");
    fprintf (fp,"CFG_%s_TAP_LEN,\t\t// TapLength\n" , Type);
    fprintf (fp,"%5d,\t\t// NLPType\n"          , ecCfg->NLPType);
    fprintf (fp,"%5d,\t\t// AdaptEnable\n"      , ecCfg->AdaptEnable);
    fprintf (fp,"%5d,\t\t// BypassEnable\n"     , ecCfg->BypassEnable);
    fprintf (fp,"%5d,\t\t// G165DetectEnable\n" , ecCfg->G165DetectEnable);
    fprintf (fp,"%5d,\t\t// DoubleTalkThres\n"  , ecCfg->DoubleTalkThres);

    if (0x1000 <= Version)
       fprintf (fp,"%5d,\t\t// MaxDoubleTalkThres\n" , ecCfg->MaxDoubleTalkThres);
    if (0x1030 <= Version) {
      ecCfg->SaturationLevel = 3; // before update the GUI, set the new parameter to default
        fprintf (fp,"%5d,\t\t// SaturationLevel\n" , ecCfg->SaturationLevel);
    }
    if (0x1100 <= Version) {
       fprintf (fp,"   -1,      // DynamicNLPAggressiveness\n");
    }
   
    fprintf (fp,"%5d,\t\t// NLPThres\n"         , ecCfg->NLPThres);
    fprintf (fp,"%5d,\t\t// NLPUpperLimitThresConv\n",   ecCfg->NlpUpperThresConv);
    fprintf (fp,"%5d,\t\t// NLPUpperLimitThresUnConv\n", ecCfg->NlpUpperThresUnConv);

    if (0x1030 <= Version) {
       ecCfg->NLPSaturationThreshold = 6;// before update the GUI, set the new parameter to default
       fprintf (fp,"%5d,\t\t// NLPSaturationThreshold\n" , ecCfg->NLPSaturationThreshold);
    }

    fprintf (fp,"%5d,\t\t// NLPMaxSupp\n"       , ecCfg->NlpMaxSupp);
    fprintf (fp,"%5d,\t\t// CNGNoiseThreshold\n", ecCfg->CNGNoiseThreshold);
    fprintf (fp,"%5d,\t\t// AdaptLimit\n"       , ecCfg->AdaptLimit);

   if ((1 < ecCfg->NFIRSegments) && (192 < (ecCfg->NFIRSegments * ecCfg->FIRSegmentLength)))
      segmentLength = 48;
   else
      segmentLength = ecCfg->FIRSegmentLength;

    if (!algBits.g168VarAEnable || 0x1010 <= Version) {
       fprintf (fp,"%5d,\t\t// CrossCorrLimit\n"  , ecCfg->CrossCorrLimit);
       if (0x1100 <= Version)      fprintf (fp,"%5d,\t\t// FIRTapCheckPeriod\n", 80);
       else if (0x1000 <= Version) fprintf (fp,"%5d,\t\t// FIRTapCheckPeriod\n", ecCfg->FirTapCheckPeriod);

       fprintf (fp,"%5d,\t\t// NFIRSegments\n", ecCfg->NFIRSegments);
       fprintf (fp,"%5d,\t\t// FIRSegmentLength\n", segmentLength);
       if (Version < 0x1000) fprintf (fp,"%5d,\t\t// FIRTapCheckPeriod\n", ecCfg->FirTapCheckPeriod);
    }
    if (0x900 < Version && Version < 0x1000) {
       fprintf (fp,"    0,\t\t// PcmSlipCheckEnable\n");
    }
    if (0x900 < Version) {
       fprintf (fp,"%5d,\t\t// TandemOperation\n"  , ecCfg->TandemOperation);
       fprintf (fp,"%5d,\t\t// MixedFourWireMode\n", ecCfg->MixedFourWireMode);
    }
    if (0x1000 < Version) {
       fprintf (fp,"%5d,\t\t// ReconvergenceChk\n" , ecCfg->ReconvergenceChk);
    }
    if (0x1100 <= Version) {
       fprintf (fp,"    0,      // pMIPSConserve\n");
       fprintf (fp,"    0,      // ChannelNumber\n");
    }
    if (0x1030 <= Version) {
      ecCfg->SmartPacketModeSelect = 0;// before update the GUI, set the new parameter to default
      ecCfg->SmartPacketBypassERLThresholddB = 50;// before update the GUI, set the new parameter to default
      ecCfg->SmartPacketSuppressERLThresholddB = 25;// before update the GUI, set the new parameter to default
      ecCfg->SmartPacketNLPThresholddB = 6;// before update the GUI, set the new parameter to default
       fprintf (fp,"%5d,\t\t// SmartPacketModeSelect\n" , ecCfg->SmartPacketModeSelect);
       fprintf (fp,"%5d,\t\t// SmartPacketBypassERLThresholddB\n" , ecCfg->SmartPacketBypassERLThresholddB);
       fprintf (fp,"%5d,\t\t// SmartPacketSuppressERLThresholddB\n" , ecCfg->SmartPacketSuppressERLThresholddB);
       fprintf (fp,"%5d,\t\t// SmartPacketNLPThresholddB\n" , ecCfg->SmartPacketNLPThresholddB);
   }

    fprintf (fp, "};\n");

}

void g168_64x_data(int tap_length, int num_fs, int fs_length, int maxFrameSize, ecMemSpec_t *ecMemSpec) {
   ecMemSpec->InstanceSize     = (368);
   ecMemSpec->DaStateSize      = (tap_length/4 + 16);
   ecMemSpec->SaStateSize      = (80 + 32 + 8 + (3*fs_length) + (3*tap_length/16+3+1) + (tap_length/16 + 2) + (num_fs*fs_length) + (tap_length/16) + (maxFrameSize/4) + (2*(48+16)));
   ecMemSpec->EchoPathSize     = (tap_length + maxFrameSize + 8);
   ecMemSpec->BackGroundEpSize = (tap_length/4 + maxFrameSize + 1);
}
static void WriteEchoCancellerData (FILE *fp, char *Type, int ecCnt, EcParams *ecParams,
                                    Overhead_t *ovrhd, int MaxFrame, EcAlign *align,
                                    ecMemSpec_t *ecMemSpec) {
   char type[8];
   int InstanceSize, DaStateSize, SaStateSize, EchoPathSize, BackGroundEpSize; 
   int EchoPathAlignment, BackGroundAlignment;
   char *hideSymbols = "";     // Hide symbol names from map file.  First one not hidden for base address.
   int T;      // TAP length
   int NF_FS;  // Number of FIR Segments * FIR Segment length
   int FS;
   int FSL;

   double log2, y, z;            // temp floating point variables

   int i, ii;
   strncpy (type, Type, 8);

   type[0] |= 0x20;  // Set to lower case
   ADT_Bool ShortTail;
   int Version;
   int MaxCustomFrameSize = MaxCustomFrameRateMs*SysCfg.samplesPerMs;
    
   if (IsC64)
      Version = ActiveC64xOverhead->Version;
   else if (IsC55)
      Version = ActiveC55xOverhead->Version;
   else
      Version = ActiveC54xOverhead->Version;

   ShortTail = algBits.g168VarAEnable;

   fprintf (fp, "\n//{ ------ %s Echo canceller buffer allocation\n", Type);
   if (ecCnt <= 0) {
      // Satisfy linker if there are no Echo Cancellers configured.
      
      fprintf (fp,"#define %sECChanI16   0\n",Type);
      fprintf (fp,"#define %sECDAStateI16   0\n",Type);
      fprintf (fp,"#define %sECSAStateI16   0\n",Type);
      fprintf (fp,"#define %sEchoPathI16   0\n",Type);
      fprintf (fp,"#define %sBgEchoPathI16  0\n\n",Type);
      fprintf (fp, "ADT_UInt16 %sEcInUse=0;\n", Type);

      fprintf (fp, "ADT_UInt16 * const p%sEcChan[] = {0};\n", Type);
      fprintf (fp, "ADT_UInt16 * const p%sEcDaState[] = {0};\n", Type);
      fprintf (fp, "ADT_UInt16 * const p%sEcSaState[] = {0};\n", Type);
      fprintf (fp, "ADT_UInt16 * const p%sEcEchoPath[] = {0};\n", Type);
      fprintf (fp, "ADT_UInt16 * const p%sEcBackEp[] = {0};\n", Type);
      fprintf (fp, "ADT_UInt16 * const p%sEcBackFar[] = {0};\n", Type);
      fprintf (fp, "ADT_UInt16 * const p%sEcBackNear[] = {0};\n\n", Type);

      fprintf (fp, "ADT_UInt16 const %sEcInfo;\n\n", type);
      fprintf (fp, "//}\n\n");
      return;
   }


   log2 = log ((double) 2);

   // Determine the sizes and alignments of the PCM Echo Canceller structures.
   T  = ecParams->TapLength;
   NF_FS = ecParams->NFIRSegments * ecParams->FIRSegmentLength;
   FS  = ecParams->NFIRSegments;
   FSL = ecParams->FIRSegmentLength;
      
   if (ShortTail)    {
      // Variant A (short tail) sizes in 16-bit words.
      //    InstanceSize = Ovrhd.ShortInst                (C54 = 128;  C64 = 158 [316/2])
      //    DaStateSize  = T
      //    SaStateSize  = T + T/16 + Ovrhd.SaShortState  (C54 = 108;  C64 = 90)
      //    EcPathSize   = T + MaxFrame + 1                   MaxFrame = 0 for C54 calculations
      //    BGPathSize   = 1

      InstanceSize = ovrhd->ShortInst;
      if (IsC55)
         EchoPathSize = T + MaxFrame + 2;
      else
         EchoPathSize = T + MaxFrame + 1;
      DaStateSize = T;
      if (0x1000 < Version) SaStateSize = (19*T / 16) + ovrhd->SaShortState;
      else                SaStateSize = (17*T / 16) + ovrhd->SaShortState;

      BackGroundEpSize = 1;
   }  else  {
      // Standard (long tail).
      //    InstanceSize = Ovrhd.Inst (C54 = 128;  C64 = 170 [340/2])
      //    DaStateSize  = T / 4
      //    SaStateSize  = 3/16 * T + MaxFrame + 2 * NF_FS + Ovrhd.SaState (C54 = 120; C64 = 196)
      //    EcPathSize   = T + MaxFrame + 1            MaxFrame = 0 for C54 calculations
      //    BGPathSize   = ((T + MaxFrame) / 4) + 1    MaxFrame = 0 for C54 calculations

      InstanceSize = ovrhd->Inst;
      if (IsC55) {
         EchoPathSize = T + MaxFrame + 2;
         BackGroundEpSize = (T/4 + 2);
         DaStateSize  = 96 + T / 4;
         SaStateSize = ovrhd->SaState + (4*ecParams->NFIRSegments) + (2*NF_FS) + ((5*T) / 16);
     } else {
         EchoPathSize = T + MaxFrame + 1;
         BackGroundEpSize = ((T + MaxFrame)/ 4) + 10;
         DaStateSize  = T / 4;
         
       if   (0x1000 <  Version) SaStateSize = ovrhd->SaState + ((5*T) / 16) + (2*NF_FS) + (MaxFrame/4);
       else                   SaStateSize = ovrhd->SaState + ((3*T) / 16) + (2*NF_FS) + MaxFrame;
     }

   }

   if ((IsC64) && (Version >= 0x111803)) {
        ecMemSpec_t memSpec;
//        g168_64x_data(T, FS, FSL, MaxFrame, &memSpec);
        if (MaxCustomFrameSize > MaxFrame)  // jdc avaya mod
            g168_64x_data(T, FS, FSL, MaxCustomFrameSize, &memSpec);
        else
            g168_64x_data(T, FS, FSL, MaxFrame, &memSpec);
        InstanceSize     = memSpec.InstanceSize;
        DaStateSize      = memSpec.DaStateSize;
        SaStateSize      = memSpec.SaStateSize;
        EchoPathSize     = memSpec.EchoPathSize;
        BackGroundEpSize = memSpec.BackGroundEpSize;
   }

   nextPower2 (EchoPathSize,      EchoPathAlignment);
   nextPower2 (BackGroundEpSize,  BackGroundAlignment);

    // Adjust the size of the Ec memory instances so that they
    // are an even multiple of the number of ATCC sub-blocks (i.e. 10).
    // The factor of 2 is necessary to ensure that the adjusted instance
    // length is an integer multiple of subblocks comprised  of 32-bit
    // elements. The memory is specified and allocated in units of 16-bit
    // words, but the DMA element size is in 32-bit words - hence the 
    // factor of 2.
    //
#ifdef EXT_DMA_TRANSFERS
    if (ExtDmaActive) {
        RoundForDMA (InstanceSize);
        RoundForDMA (DaStateSize);
        RoundForDMA (SaStateSize);
        RoundForDMA (EchoPathSize);
        RoundForDMA (BackGroundEpSize);
    }
#endif

   ecMemSpec->InstanceSize     = InstanceSize;
   ecMemSpec->DaStateSize      = DaStateSize;
   ecMemSpec->SaStateSize      = SaStateSize;
   ecMemSpec->EchoPathSize     = EchoPathSize;
   ecMemSpec->BackGroundEpSize = BackGroundEpSize;


   fprintf (fp,"\n");   
   fprintf (fp,"#define %sECChanI16       %4d\n",  Type, InstanceSize);
   fprintf (fp,"#define %sECChanI16_EXT   %4d\n\n",Type, RoundForCache (InstanceSize));

   fprintf (fp,"#define %sECDAStateI16       %4d\n",  Type, DaStateSize);
   fprintf (fp,"#define %sECDAStateI16_EXT   %4d\n\n",Type, RoundForCache (DaStateSize));

   fprintf (fp,"#define %sECSAStateI16       %4d\n",  Type, SaStateSize);
   fprintf (fp,"#define %sECSAStateI16_EXT   %4d\n\n",Type, RoundForCache (SaStateSize));
   
   fprintf (fp,"#define %sEchoPathI16       %4d\n",  Type, EchoPathSize);
   fprintf (fp,"#define %sEchoPathI16_EXT   %4d\n\n",Type, RoundForCache (EchoPathSize));

   fprintf (fp,"#define %sBgEchoPathI16      %4d\n",  Type, BackGroundEpSize);
   fprintf (fp,"#define %sBgEchoPathI16_EXT  %4d\n\n",Type, RoundForCache (BackGroundEpSize));



   // Echo canceller memory structures
   for (i = 0; i < ecCnt; i++)    {
      fprintf (fp, "\n%sADT_UInt16 %sEcChan%d[%sECChanI16_EXT], %sEcDaState%d[%sECDAStateI16_EXT], %sEcSaState%d[%sECSAStateI16_EXT];\n",
             hideSymbols, Type, i, Type, Type, i, Type, Type, i, Type);
     //                              C54  C64
      ArrayAlign (Type, EcChan,    i, 128,  8, 8);
      ArrayAlign (Type, EcDaState, i,   2,  8, 8);
      ArrayAlign (Type, EcSaState, i,   2,  8, 8);

      if (ShortTail || IsC64 || IsC55) continue;

      fprintf (fp, "%sADT_UInt16 %sEcBackFar%d[64], %sEcBackNear%d[64];\n", hideSymbols, Type, i, Type, i);
         
      ArrayAlign (Type, EcBackFar,  i, 64, 8, 8);
      ArrayAlign (Type, EcBackNear, i, 64, 8, 8);
      hideSymbols = "static ";
   }
   fprintf (fp, "\n");
   fprintf (fp, "//}\n//{\n");


   hideSymbols = "";
   for (i = 0; i < ecCnt; i++) {
      fprintf   (fp, "\n%sADT_UInt16 %sEcEchoPath%d[%sEchoPathI16_EXT];\n", hideSymbols, Type, i, Type);
      if (!ShortTail) 
          fprintf (fp, "%sADT_UInt16 %sEcBackEp%d  [%sBgEchoPathI16_EXT];\n", hideSymbols, Type, i, Type);

      ArrayAlign (Type, EcEchoPath, i, EchoPathAlignment, 8, 8);
      if (!ShortTail)
         ArrayAlign (Type, EcBackEp, i, BackGroundAlignment, 8, 8);
      hideSymbols = "static ";
   }
   
   fprintf (fp, "\n");
  
   // Echo canceller alignment pragmas
   EchoCancellerAlign (fp, Type, ecCnt, align, ShortTail);
   
   fprintf (fp, "//}\n//{\n");

   // Assignment buffers
   fprintf (fp, "ADT_UInt16 %sEcInUse[%d];\n", Type, ecCnt);
   fprintf (fp, "#pragma DATA_SECTION (%sEcInUse, \"POOL_ALLOC\")\n", Type);

   fprintf (fp, "ADT_UInt16 * const p%sEcChan[] = {\n", Type);
   writeElementNameArray (ecCnt, Type, "EcChan");
      
   fprintf (fp, "ADT_UInt16 * const p%sEcDaState[] = {\n", Type);
   writeElementNameArray (ecCnt, Type, "EcDaState");

   fprintf (fp, "ADT_UInt16 * const p%sEcSaState[] = {\n", Type);
   writeElementNameArray (ecCnt, Type, "EcSaState");

   fprintf (fp, "ADT_UInt16 * const p%sEcEchoPath[] = {\n", Type);
   writeElementNameArray (ecCnt, Type, "EcEchoPath");

   if (!ShortTail) {
      fprintf (fp, "ADT_UInt16 * const p%sEcBackEp[] = {\n", Type);
      writeElementNameArray (ecCnt, Type, "EcBackEp");

      if (!(IsC64 || IsC55)) {
         fprintf (fp, "ADT_UInt16 * const p%sEcBackFar[] = {\n", Type);
         writeElementNameArray (ecCnt, Type, "EcBackFar");

         fprintf (fp, "ADT_UInt16 * const p%sEcBackNear[] = {\n", Type);
         writeElementNameArray (ecCnt, Type, "EcBackNear");
     }
   }  else  {
      fprintf (fp, "ADT_UInt16 * const p%sEcBackEp[] = {0};\n", Type);
      if (!(IsC64 || IsC55)) {
         fprintf (fp, "ADT_UInt16 * const p%sEcBackFar[] = {0};\n", Type);
         fprintf (fp, "ADT_UInt16 * const p%sEcBackNear[] = {0};\n", Type);
     }
   }

   fprintf (fp,"\necInstanceInfo_t const %sEcInfo = {\n", type);
   fprintf (fp,"    (void *)p%sEcChan,  (void *) p%sEcSaState, (void *)p%sEcDaState, (void *) p%sEcEchoPath, (void *) p%sEcBackEp,\n", 
                            Type, Type, Type, Type, Type);
   fprintf (fp,"    %sECChanI16,        %sECSAStateI16,        %sECDAStateI16,       %sEchoPathI16,          %sBgEchoPathI16\n};\n",
                            Type, Type, Type, Type, Type);
   fprintf (fp, "\n//}");

}


#define setToLarger(a,b) if (a<(b)) a=(b);


void g168_64x_scratch(int tap_length, int segment_length,  int frameSize, unsigned int *sa_scr, unsigned int *da_scr) {
    *sa_scr = (4*frameSize + 32 + tap_length/16 + 2*(tap_length/16) + 2 + 8 + 3*segment_length +  frameSize/2);
    *da_scr = (tap_length + 16);
}
static void WriteEchoCancellerStructures (ecConfig_t *ecCfg, 
                                          Align *align, FILE *fp, int MaxFrame, 
                                          ecMemSpec_t *pcmEcMem, ecMemSpec_t *pktEcMem) {

   unsigned int SAI16_1ms,    DAI16_1ms;    // 1 msec task ECAN scratch buffer sizes
   unsigned int SAI16_2_5ms,  DAI16_2_5ms;  // 2.5 msec task ECAN scratch buffer sizes
   unsigned int SAI16_5ms,    DAI16_5ms;    // 5 msec task ECAN scratch buffer sizes
   unsigned int SAI16_10ms,   DAI16_10ms;   // 10 msec task ECAN scratch buffer sizes
   unsigned int SAI16_20ms,   DAI16_20ms;   // 20 msec task ECAN scratch buffer sizes
   unsigned int SAI16_22_5ms, DAI16_22_5ms; // 22.5 msec task ECAN scratch buffer sizes
   unsigned int SAI16_30ms,   DAI16_30ms;   // 30 msec task ECAN scratch buffer sizes

   unsigned int SAI32_srtp;
   const memSpec_t *g723EncMem = NULL, *g728EncMem = NULL, *g729EncMem = NULL; 
   const memSpec_t *melpEncMem = NULL, *g722EncMem = NULL, *speexEncMem = NULL;
   const memSpec_t *amrEncMem = NULL,  *adtEncMem = NULL;
   const memSpec_t *g723DecMem = NULL, *g728DecMem = NULL, *g729DecMem = NULL;
   const memSpec_t *melpDecMem = NULL, *g722DecMem = NULL, *speexDecMem = NULL;
   const memSpec_t *amrDecMem = NULL,  *adtDecMem = NULL;
   const memSpec_t *g711a1a2EncMem = NULL,  *g711a1a2DecMem = NULL;

   int T;    // maximum TAP length
   int NFS;
   int FSL;
   int NF_FS;  // maximum of ( Number of FIR Segments * FIR Segment length )
   int temp;
   int commonSa, commonDa;
   float frameFactor;
   int Version;

   Overhead_t *Overhead;

   if ((!IsC64) && (!IsC55)) {
      Overhead   = ActiveC54xOverhead;
      adtEncMem  = &adt4800C54EncMem;   adtDecMem  = &adt4800C54DecMem;
      amrEncMem  = &AMR_54EncMem;       amrDecMem  = &AMR_54DecMem;
      g723EncMem = &g723C54EncMem;      g723DecMem = &g723C54DecMem;
      g728EncMem = &g728C54Mem;         g728DecMem = &g728C54Mem;
      g729EncMem = &g729ABC54EncMem;    g729DecMem = &g729ABC54DecMem;
      melpEncMem = &Melp_54EncMem;      melpDecMem = &Melp_54DecMem;
      Version = ActiveC54xOverhead->Version;
   } else if (IsC55) { 
      Overhead   = ActiveC55xOverhead;
      amrEncMem  = &AMR_55EncMem;       amrDecMem  = &AMR_55DecMem;
      g723EncMem = &g723C55EncMem;      g723DecMem = &g723C55DecMem;
      g728EncMem = &g728C55Mem;         g728DecMem = &g728C55Mem;
      g729EncMem = &g729ABC55EncMem;    g729DecMem = &g729ABC55DecMem;
      melpEncMem = &Melp_55EncMem;      melpDecMem = &Melp_55DecMem;
      Version = ActiveC55xOverhead->Version;
   } else {
      Overhead   = ActiveC64xOverhead;
      amrEncMem  = &AMR_64EncMem;       amrDecMem  = &AMR_64DecMem;
	  if (cdcBits.TI_g7231AEnable) {
         g723EncMem = &TI_g7231AC64EncMem; g723DecMem = &TI_g7231AC64DecMem;
      } else {
         g723EncMem = &g723C64EncMem;      g723DecMem = &g723C64DecMem;
	  }
	  g728EncMem = &g728C64EncMem;      g728DecMem = &g728C64DecMem;
      if (cdcBits.TI_g729ABEnable) {
         g729EncMem = &TI_g729ABC64EncMem; g729DecMem = &TI_g729ABC64DecMem;
      } else {
         g729EncMem = &g729ABC64EncMem;    g729DecMem = &g729ABC64DecMem;
      }

      if (cdcBits.melpEEnable) {
         if (IsC64Plus) {
            melpEncMem = &MelpE_64pEncMem;   melpDecMem = &MelpE_64pDecMem;
         } else {
            melpEncMem = &MelpE_64EncMem;    melpDecMem = &MelpE_64DecMem;
         }
      } else {
         melpEncMem = &Melp_64EncMem;     melpDecMem = &Melp_64DecMem;
      }

      g722EncMem = &G722_64EncMem;
      g722DecMem = &G722_64DecMem;
      g711a1a2EncMem = &G711A1A2_64EncMem;
      g711a1a2DecMem = &G711A1A2_64DecMem;

      if (cdcBits.speexWBEnable) {
         speexEncMem = &SpeexWB_64EncMem;
         speexDecMem = &SpeexWB_64DecMem;
      } else {
         speexEncMem = &Speex_64EncMem;
         speexDecMem = &Speex_64DecMem;
      }
      Version = ActiveC64xOverhead->Version;
   }

   //---------------------------------------------------------
   //  Determine Echo Canceller Scratch Sizes per Frame
   //
   SAI16_1ms    = DAI16_1ms    = 1;
   SAI16_2_5ms  = DAI16_2_5ms  = 1;
   SAI16_5ms    = DAI16_5ms    = 1;
   SAI16_10ms   = DAI16_10ms   = 1;
   SAI16_20ms   = DAI16_20ms   = 1;
   SAI16_22_5ms = DAI16_22_5ms = 1;
   SAI16_30ms   = DAI16_30ms   = 1;
   BG_SCRATCH_I16     = 1;

   if (SysCfg.SRTP_Enable)
      SAI32_srtp = 320/4;
   else
      SAI32_srtp = 1;

   if (SysCfg.numPcmEcans + SysCfg.numPktEcans != 0)  {

      // Determine the maximum TAP length, FIR segments, and FIR segment lengths.
      T  = 0;
      NF_FS = 0;
      NFS = 0;
      FSL = 0;
      if (SysCfg.numPcmEcans != 0)  {
         T  = ecCfg->g168Pcm.TapLength;
         NFS = ecCfg->g168Pcm.NFIRSegments;
         FSL = ecCfg->g168Pcm.FIRSegmentLength;
         NF_FS = ecCfg->g168Pcm.NFIRSegments * ecCfg->g168Pcm.FIRSegmentLength;
      }

      if (SysCfg.numPktEcans != 0)  {
         temp = ecCfg->g168Pkt.NFIRSegments * ecCfg->g168Pkt.FIRSegmentLength;
         if (T < ecCfg->g168Pkt.TapLength)  T = ecCfg->g168Pkt.TapLength;
         if (NF_FS < temp) NF_FS = temp;
         if (FSL < ecCfg->g168Pkt.FIRSegmentLength) FSL = ecCfg->g168Pkt.FIRSegmentLength;
      }

 
      //  Single and dual access scratch memory calculations
      if (algBits.g168VarAEnable)      {
         // Variant A (short tail).
         //   SaScratch = Fr + T + Overhead.SaShortScratch  (C54 = 1;  C64 = 38)
         //   DaScratch = 1
         frameFactor = Overhead->frameMult;
         commonSa = 2*T + Overhead->SaShortScratch;
         commonDa = 1;
      } else {
         // Standard (long tail).
         //   SaScratch = Fr + 3/16 T + NF_FS + Overhead.SaScratch (C54 = 40; C64 = 164)
         //   DaScratch = T
         frameFactor = Overhead->frameMult;
         commonSa = Overhead->SaScratch + T + NF_FS + 2*ecCfg->g168Pcm.NFIRSegments+(3*T/16);
         commonDa = T + 8;
      }

      // Include AEC scratch in scratch memory determination
      if (FRAME_1ms_ENABLED) {
         SAI16_1ms = (int) (8 * frameFactor) + commonSa;
         DAI16_1ms = commonDa;
         if (IsC64 && Version >= 0x111803) {
            g168_64x_scratch(T, FSL, 8, &SAI16_1ms, &DAI16_1ms);
         }
      }
      if (FRAME_2ms_ENABLED) {
         SAI16_2_5ms = (int) (20 * frameFactor) + commonSa;
         DAI16_2_5ms = commonDa;
         if (IsC64 && Version >= 0x111803) {
            g168_64x_scratch(T, FSL, 20, &SAI16_2_5ms, &DAI16_2_5ms);
         }
      }
      if (FRAME_5ms_ENABLED) {
         SAI16_5ms = (int) (40 * frameFactor) + commonSa;
         DAI16_5ms = commonDa;
         if (IsC64 && Version >= 0x111803) {
            g168_64x_scratch(T, FSL, 40, &SAI16_5ms, &DAI16_5ms);
         }
      }
      if (FRAME_10ms_ENABLED) {
         SAI16_10ms = (int) (80 * frameFactor) + commonSa;
         DAI16_10ms = commonDa;
         if (IsC64 && Version >= 0x111803) {
            g168_64x_scratch(T, FSL, 80, &SAI16_10ms, &DAI16_10ms);
         }
      }
      if (FRAME_20ms_ENABLED) {
         SAI16_20ms = (int) (160 * frameFactor) + commonSa;
         DAI16_20ms = commonDa;
         if (IsC64 && Version >= 0x111803) {
            g168_64x_scratch(T, FSL, 160, &SAI16_20ms, &DAI16_20ms);
         }
      }
      if (FRAME_22ms_ENABLED) {
         SAI16_22_5ms = (int) (180 * frameFactor) + commonSa;
         DAI16_22_5ms = commonDa;
         if (IsC64 && Version >= 0x111803) {
            g168_64x_scratch(T, FSL, 180, &SAI16_22_5ms, &DAI16_22_5ms);
         }
      }
      if (FRAME_30ms_ENABLED) {
         SAI16_30ms = (int) (240 * frameFactor) + commonSa;
         DAI16_30ms = commonDa;
         if (IsC64 && Version >= 0x111803) {
            g168_64x_scratch(T, FSL, 240, &SAI16_30ms, &DAI16_30ms);
         }
      }

      
   } else {
      commonSa = 1;
      commonDa = 1;
   }
#ifdef COMBINED_SCRATCH   
   if (FRAME_1ms_ENABLED)  SAI16_1ms    = max (SAI16_1ms,     32);
   if (FRAME_2ms_ENABLED)  SAI16_2_5ms  = max (SAI16_2_5ms,   80);
   if (FRAME_5ms_ENABLED)  SAI16_5ms    = max (SAI16_5ms,    160);
   if (FRAME_10ms_ENABLED) SAI16_10ms   = max (SAI16_10ms,   320);
   if (FRAME_20ms_ENABLED) SAI16_20ms   = max (SAI16_20ms,   640);
   if (FRAME_22ms_ENABLED) SAI16_22_5ms = max (SAI16_22_5ms, 720);
   if (FRAME_30ms_ENABLED) SAI16_30ms   = max (SAI16_30ms,   960);


#if 0 // jdc... run once to establishcheck custom frame scratch
  {
      // 40,50,60 ms custom frames
   unsigned int SAI16_40ms,   DAI16_40ms;   // 40 msec task ECAN scratch buffer sizes
   unsigned int SAI16_50ms,   DAI16_50ms;   // 50 msec task ECAN scratch buffer sizes
   unsigned int SAI16_60ms,   DAI16_60ms;   // 60 msec task ECAN scratch buffer sizes

    // 40 ms ------------------------------------------------------
    SAI16_40ms = (int) (320 * frameFactor) + commonSa;
    DAI16_40ms = commonDa;
    g168_64x_scratch(T, FSL, 320, &SAI16_40ms, &DAI16_40ms);
    SAI16_40ms   = max (SAI16_40ms,   1280);
    if (cdcBits.g728Enable) {
       setToLarger (SAI16_40ms, g728EncMem->Scratch);
       setToLarger (SAI16_40ms, g728DecMem->Scratch);
    }
    if (cdcBits.g729ABEnable || cdcBits.TI_g729ABEnable) {
       setToLarger (SAI16_40ms, g729EncMem->Scratch);
       setToLarger (SAI16_40ms, g729DecMem->Scratch);
    }
    if (cdcBits.g722Enable) {
       setToLarger (SAI16_40ms, g722EncMem->Scratch);
       setToLarger (SAI16_40ms, g722DecMem->Scratch);
    }
	{
       setToLarger (SAI16_40ms, g711a1a2EncMem->Scratch);
       setToLarger (SAI16_40ms, g711a1a2DecMem->Scratch);
    }
    //setToLarger (BG_SCRATCH_I16, SAI16_40ms);

    if (SysCfg.SRTP_Enable) {
       setToLarger (SAI16_40ms, (SAI32_srtp*2));
    }
    setToLarger (SAI16_40ms, AECScratchSize16);
    fprintf (fp, "\n// Custom Framing task scratch memory 40 ms.\n");
    fprintf (fp, "\n//{\n");
    writeECScratch ( 40);
    fprintf (fp, "\n//}\n");

    // 50 ms ------------------------------------------------------
    SAI16_50ms = (int) (400 * frameFactor) + commonSa;
    DAI16_50ms = commonDa;
    g168_64x_scratch(T, FSL, 400, &SAI16_50ms, &DAI16_50ms);
    SAI16_50ms   = max (SAI16_50ms,   1600);
    if (cdcBits.g729ABEnable || cdcBits.TI_g729ABEnable) {
       setToLarger (SAI16_50ms, g729EncMem->Scratch);
       setToLarger (SAI16_50ms, g729DecMem->Scratch);
    }
    if (cdcBits.g722Enable) {
       setToLarger (SAI16_50ms, g722EncMem->Scratch);
       setToLarger (SAI16_50ms, g722DecMem->Scratch);
    }
	{
       setToLarger (SAI16_50ms, g711a1a2EncMem->Scratch);
       setToLarger (SAI16_50ms, g711a1a2DecMem->Scratch);
    }
    //setToLarger (BG_SCRATCH_I16, SAI16_50ms);

    if (SysCfg.SRTP_Enable) {
       setToLarger (SAI16_50ms, (SAI32_srtp*2));
    }
    fprintf (fp, "\n// Custom Framing task scratch memory 50 ms.\n");
    fprintf (fp, "\n//{\n");
    writeECScratch ( 50);
    fprintf (fp, "\n//}\n");

    // 60 ms ------------------------------------------------------
    SAI16_60ms = (int) (480 * frameFactor) + commonSa;
    DAI16_60ms = commonDa;
    g168_64x_scratch(T, FSL, 480, &SAI16_60ms, &DAI16_60ms);
    SAI16_60ms   = max (SAI16_60ms,   1920);
      if (cdcBits.g723Enable | cdcBits.TI_g7231AEnable) {
         setToLarger (SAI16_60ms, g723EncMem->Scratch);
         setToLarger (SAI16_60ms, g723DecMem->Scratch);
      }
      if (cdcBits.g728Enable) {
         setToLarger (SAI16_60ms, g728EncMem->Scratch);
         setToLarger (SAI16_60ms, g728DecMem->Scratch);
      }
      if (cdcBits.g729ABEnable | cdcBits.TI_g729ABEnable) {
         setToLarger (SAI16_60ms, g729EncMem->Scratch);
         setToLarger (SAI16_60ms, g729DecMem->Scratch);
      }
	  {
         setToLarger (SAI16_60ms, g711a1a2EncMem->Scratch);
         setToLarger (SAI16_60ms, g711a1a2DecMem->Scratch);
      }
      //setToLarger (BG_SCRATCH_I16, SAI16_60ms);

      if (SysCfg.SRTP_Enable) {
         setToLarger (SAI16_60ms, (SAI32_srtp*2));
      }
    fprintf (fp, "\n// Custom Framing task scratch memory 60 ms.\n");
    fprintf (fp, "\n//{\n");
    writeECScratch ( 60);
    fprintf (fp, "\n//}\n");

  }
#endif

#endif

   if (FRAME_1ms_ENABLED)  TRACE ("   1) EC SA Scratch: %d  EC DA Scratch: %d\n", SAI16_1ms,    DAI16_1ms);
   if (FRAME_2ms_ENABLED)  TRACE (" 2.5) EC SA Scratch: %d  EC DA Scratch: %d\n", SAI16_2_5ms,  DAI16_2_5ms);
   if (FRAME_5ms_ENABLED)  TRACE ("   5) EC SA Scratch: %d  EC DA Scratch: %d\n", SAI16_5ms,    DAI16_5ms);
   if (FRAME_10ms_ENABLED) TRACE ("  10) EC SA Scratch: %d  EC DA Scratch: %d\n", SAI16_10ms,   DAI16_10ms);
   if (FRAME_20ms_ENABLED) TRACE ("  20) EC SA Scratch: %d  EC DA Scratch: %d\n", SAI16_20ms,   DAI16_20ms);
   if (FRAME_22ms_ENABLED) TRACE ("22.5) EC SA Scratch: %d  EC DA Scratch: %d\n", SAI16_22_5ms, DAI16_22_5ms);
   if (FRAME_30ms_ENABLED) TRACE ("  30) EC SA Scratch: %d  EC DA Scratch: %d\n", SAI16_30ms,   DAI16_30ms);

   
   
   // NOTE: echo canceller SA Scratch buffers are shared with encoder, decoder and AEC scratch buffers
   if (FRAME_1ms_ENABLED) {
      if (SysCfg.SRTP_Enable) {
         setToLarger (SAI16_1ms, (SAI32_srtp*2));
      }
      setToLarger (SAI16_1ms, AECScratchSize16);
   }
   if (FRAME_2ms_ENABLED) {
      if (cdcBits.g728Enable) {
         setToLarger (SAI16_2_5ms, g728EncMem->Scratch);
         setToLarger (SAI16_2_5ms, g728DecMem->Scratch);
      }
      setToLarger (BG_SCRATCH_I16, SAI16_2_5ms);

      if (SysCfg.SRTP_Enable) {
         setToLarger (SAI16_2_5ms, (SAI32_srtp*2));
      }
      setToLarger (SAI16_2_5ms, AECScratchSize16);
   }
   if (FRAME_5ms_ENABLED) {
      if (cdcBits.g728Enable) {
         setToLarger (SAI16_5ms, g728EncMem->Scratch);
         setToLarger (SAI16_5ms, g728DecMem->Scratch);
      }
      setToLarger (BG_SCRATCH_I16, SAI16_5ms);

      if (SysCfg.SRTP_Enable) {
         setToLarger (SAI16_5ms, (SAI32_srtp*2));
      }
      setToLarger (SAI16_5ms, AECScratchSize16);
   }
   if (FRAME_10ms_ENABLED) {
      if (cdcBits.g728Enable) {
         setToLarger (SAI16_10ms, g728EncMem->Scratch);
         setToLarger (SAI16_10ms, g728DecMem->Scratch);
      }
      if (cdcBits.g729ABEnable || cdcBits.TI_g729ABEnable) {
         setToLarger (SAI16_10ms, g729EncMem->Scratch);
         setToLarger (SAI16_10ms, g729DecMem->Scratch);
      }
      if (cdcBits.g722Enable) {
         setToLarger (SAI16_10ms, g722EncMem->Scratch);
         setToLarger (SAI16_10ms, g722DecMem->Scratch);
      }
      //if (cdcBits.g722Enable) 
	  {
         setToLarger (SAI16_10ms, g711a1a2EncMem->Scratch);
         setToLarger (SAI16_10ms, g711a1a2DecMem->Scratch);
      }
      setToLarger (BG_SCRATCH_I16, SAI16_10ms);

      if (SysCfg.SRTP_Enable) {
         setToLarger (SAI16_10ms, (SAI32_srtp*2));
      }
      setToLarger (SAI16_10ms, AECScratchSize16);
   }

   if (FRAME_20ms_ENABLED) {
      if (cdcBits.AMREnable) {
         setToLarger (SAI16_20ms, amrEncMem->Scratch);
         setToLarger (SAI16_20ms, amrDecMem->Scratch);
      }
      if (cdcBits.g728Enable) {
         setToLarger (SAI16_20ms, g728EncMem->Scratch);
         setToLarger (SAI16_20ms, g728DecMem->Scratch);
      }
      if (cdcBits.g729ABEnable || cdcBits.TI_g729ABEnable) {
         setToLarger (SAI16_20ms, g729EncMem->Scratch);
         setToLarger (SAI16_20ms, g729DecMem->Scratch);
      }
      if (cdcBits.g722Enable) {
         setToLarger (SAI16_20ms, g722EncMem->Scratch);
         setToLarger (SAI16_20ms, g722DecMem->Scratch);
      }
	  {
         setToLarger (SAI16_20ms, g711a1a2EncMem->Scratch);
         setToLarger (SAI16_20ms, g711a1a2DecMem->Scratch);
      }
      if (cdcBits.speexEnable || cdcBits.speexWBEnable) {
         setToLarger (SAI16_20ms, speexEncMem->Scratch);
         setToLarger (SAI16_20ms, speexDecMem->Scratch);
      }
      setToLarger (BG_SCRATCH_I16, SAI16_20ms);

      if (SysCfg.SRTP_Enable) {
         setToLarger (SAI16_20ms, (SAI32_srtp*2));
      }
      setToLarger (SAI16_20ms, AECScratchSize16);
   }
   if (FRAME_22ms_ENABLED) {
      if (cdcBits.g728Enable) {
         setToLarger (SAI16_22_5ms, g728EncMem->Scratch);
         setToLarger (SAI16_22_5ms, g728DecMem->Scratch);
      }
      if ((cdcBits.melpEnable) || (cdcBits.melpEEnable)) {
         setToLarger (SAI16_22_5ms, melpEncMem->Scratch);
         setToLarger (SAI16_22_5ms, melpDecMem->Scratch);
      }
      setToLarger (BG_SCRATCH_I16, SAI16_22_5ms);

      if (SysCfg.SRTP_Enable) {
         setToLarger (SAI16_22_5ms, (SAI32_srtp*2));
      }
      setToLarger (SAI16_22_5ms, AECScratchSize16);
   }

   if (FRAME_30ms_ENABLED) {
      if (cdcBits.adt4800Enable) {
         setToLarger (SAI16_20ms, adtEncMem->Scratch);
         setToLarger (SAI16_20ms, adtDecMem->Scratch);
      }
      if (cdcBits.g723Enable | cdcBits.TI_g7231AEnable) {
         setToLarger (SAI16_30ms, g723EncMem->Scratch);
         setToLarger (SAI16_30ms, g723DecMem->Scratch);
      }
      if (cdcBits.g728Enable) {
         setToLarger (SAI16_30ms, g728EncMem->Scratch);
         setToLarger (SAI16_30ms, g728DecMem->Scratch);
      }
      if (cdcBits.g729ABEnable | cdcBits.TI_g729ABEnable) {
         setToLarger (SAI16_30ms, g729EncMem->Scratch);
         setToLarger (SAI16_30ms, g729DecMem->Scratch);
      }
	  {
         setToLarger (SAI16_30ms, g711a1a2EncMem->Scratch);
         setToLarger (SAI16_30ms, g711a1a2DecMem->Scratch);
      }
      setToLarger (BG_SCRATCH_I16, SAI16_30ms);

      if (SysCfg.SRTP_Enable) {
         setToLarger (SAI16_30ms, (SAI32_srtp*2));
      }
      setToLarger (SAI16_30ms, AECScratchSize16);
   }



   // Per frame scratch data (single and dual access buffers)
   fprintf (fp, "\n// ------ Framing task scratch memory.\n");
   fprintf (fp, "\n//{\n");

   if (FRAME_1ms_ENABLED) {
      writeECScratch ( 1);
   } else {
      writeDummyScratch ( 1);
   }

   if (FRAME_2ms_ENABLED) {
     writeECScratch (2_5);
   } else {
      writeDummyScratch (2_5);
   }
   
   if (FRAME_5ms_ENABLED) {
      writeECScratch ( 5);
   } else {
      writeDummyScratch ( 5);
   }
   
   if (FRAME_10ms_ENABLED) {
      writeECScratch (10);
   } else {
      writeDummyScratch (10);
   }

   if (FRAME_20ms_ENABLED) {
      writeECScratch (20);
   } else {
      writeDummyScratch (20);
   }

   if (FRAME_22ms_ENABLED) {
      writeECScratch (22_5);
   } else {
      writeDummyScratch (22_5);
   }

   if (FRAME_30ms_ENABLED) {
      writeECScratch (30);
   } else {
      writeDummyScratch (30);
   }

    if (0x1100 <= Version) {
        writeECMipsConserve ( 1);
        writeECMipsConserve ( 2_5);
        writeECMipsConserve ( 5);
        writeECMipsConserve (10);
        writeECMipsConserve (20);
        writeECMipsConserve (22_5);
        writeECMipsConserve (30);
    }



    AlignSection (G168DAscratch_1ms,    ECDABUFRS, 8);
    AlignSection (G168DAscratch_2_5ms,  ECDABUFRS, 8);
    AlignSection (G168DAscratch_5ms,    ECDABUFRS, 8);
    AlignSection (G168DAscratch_10ms,   ECDABUFRS, 8);
    AlignSection (G168DAscratch_20ms,   ECDABUFRS, 8);
    AlignSection (G168DAscratch_22_5ms, ECDABUFRS, 8);
    AlignSection (G168DAscratch_30ms,   ECDABUFRS, 8);

#ifdef COMBINED_SCRATCH   
    AlignSection (G168SAscratch_1ms,    ECDABUFRS, 8);
    AlignSection (G168SAscratch_2_5ms,  ECDABUFRS, 8);
    AlignSection (G168SAscratch_5ms,    ECDABUFRS, 8);
    AlignSection (G168SAscratch_10ms,   ECDABUFRS, 8);
    AlignSection (G168SAscratch_20ms,   ECDABUFRS, 8);
    AlignSection (G168SAscratch_22_5ms, ECDABUFRS, 8);
    AlignSection (G168SAscratch_30ms,   ECDABUFRS, 8);
#endif
   fprintf (fp, "\n//}\n");

   // ---------------------------------------------------------
   // Create PCM Echo Canceller data structures.
   if (!IsC64) MaxFrame = 0;


   WriteEchoCancellerData (fp, "Pcm", SysCfg.numPcmEcans, &ecCfg->g168Pcm,
                                    Overhead, MaxFrame, &align->Pcm, pcmEcMem);

   WriteEchoCancellerData (fp, "Pkt", SysCfg.numPktEcans, &ecCfg->g168Pkt,
                                    Overhead, MaxFrame, &align->Pkt, pktEcMem);

#if 0 // jdc   // AEC library version dependent mod
   if (aecLibVersion >= 0x0420)
   {
      WriteAEC0420Data(fp);
   }
   else
   {
      WriteAECData(fp);
   }
#else
   WriteAECData (fp);
#endif

}

void alignECInternalBuffers (FILE *fp, EcAlign *align, char *Section, char *ext) {
   int i;


   for (i= 0; i<3; i++) {

      fprintf (fp, " ecChanIntBuff%01d_%2s     : {} ALIGN(%d) %s\n",  i, ext, align->EcChan, Section);  

      fprintf (fp, " ecSaStateIntBuff%01d_%2s  : {} ALIGN(%d) %s\n",  i, ext, align->EcSaState, Section);  
      
      fprintf (fp, " ecDaStateIntBuff%01d_%2s  : {} ALIGN(%d) %s\n",  i, ext, align->EcDaState, Section);  

      fprintf (fp, " ecEchoPathIntBuff%01d_%2s : {} ALIGN(%d) %s\n",  i, ext, align->EcEchoPath, Section);

      fprintf (fp, " ecBgEpIntBuff%01d_%2s     : {} ALIGN(%d) %s\n\n",i, ext, align->EcBackEp, Section);
   }
}

void EchoCancellerAlign (FILE *fp, char *Type, int numEcans, EcAlign *align, ADT_Bool ShortTail) {
                                
    int i, DmaAlign;

    // Echo Canceller memory location and alignment specs.
    if (numEcans == 0) 
        return;

    if (ExtDmaActive)  {
       DmaAlign = 128;
    } else if (ExtDmaArea != NULL) {
       DmaAlign = 128;
    } else {
       DmaAlign = 4;
    }

    for (i = 0; i < numEcans; i++) {
       fprintf (fp, "#pragma DATA_ALIGN (%sEcChan%d,     %d)\n", Type, i, MAX (DmaAlign, align->EcChan));
       fprintf (fp, "#pragma DATA_ALIGN (%sEcDaState%d,  %d)\n", Type, i, MAX (DmaAlign, align->EcDaState));
       fprintf (fp, "#pragma DATA_ALIGN (%sEcSaState%d,  %d)\n", Type, i, MAX (DmaAlign, align->EcSaState));
       fprintf (fp, "#pragma DATA_ALIGN (%sEcEchoPath%d, %d)\n", Type, i, MAX (DmaAlign, align->EcEchoPath));
                  
       if (ShortTail) continue;

       fprintf (fp, "#pragma DATA_ALIGN (%sEcBackEp%d,   %d)\n", Type, i, MAX (DmaAlign, align->EcBackEp));

       if (IsC64 || IsC55) continue;
       fprintf (fp, "#pragma DATA_ALIGN (%sEcBackFar%d,  %d)\n", Type, i, MAX (DmaAlign, align->EcBackFar));
       fprintf (fp, "#pragma DATA_ALIGN (%sEcBackNear%d, %d)\n", Type, i, MAX (DmaAlign, align->EcBackNear));

   }
}

void EchoCancellerFastSectionAssign (FILE *fp, char *Type, int numEcans, EcAlign *align, ADT_Bool ShortTail) {
                                
    char *EcFast;

    // Echo Canceller memory location and alignment specs.
    if (numEcans == 0) 
        return;

    fprintf (fp, "\n/* %s Echo Canceller fast memory requirements. */\n", Type);
    if (SysCfg.DspType == Ti647) {
       EcFast = ExtDmaArea;
       if (EcFast == NULL) EcFast = FastData;
    } else if (SysCfg.DspType == Ti667) {
       EcFast = ExtDmaArea;
       if (EcFast == NULL) EcFast = FastData;
    } else if (ExtDmaActive)  {
       EcFast = FastData; 
    } else if (ExtDmaArea != NULL) {
       EcFast = MediumData; 
    } else {
       EcFast = InternalMem;
    }
    if (EcFast[1] == '>') EcFast++;

    fprintf (fp, "  %sEcDaState :  {} %s\n", Type, EcFast);
    fprintf (fp, "  %sEcEchoPath : {} %s\n", Type, EcFast);
                  
    if (ShortTail) return;

    fprintf (fp, "  %sEcBackEp :   {} %s\n", Type, EcFast);

}


void EchoCancellerSlowSectionAssign (FILE *fp, char *Type, int numEcans, EcAlign *align, ADT_Bool ShortTail) {
    char *EcSlow;

    // PCM Echo Canceller memory location and alignment specs.
    if (numEcans == 0) return;

    fprintf (fp, "\n/* %s Echo Canceller slow memory requirements. */\n", Type);
    if (ExtDmaActive)  {
       EcSlow = ExtDmaArea; 
    } else {
       EcSlow = InternalMem;
   }
   fprintf (fp, "  %sEcChan :     {} %s\n", Type, EcSlow);
   fprintf (fp, "  %sEcSaState :  {} %s\n", Type, EcSlow);
                  
   if (ShortTail || IsC64 || IsC55) return;
   
   fprintf (fp, "  %sEcBackFar :  {} %s\n", Type, EcSlow);
   fprintf (fp, "  %sEcBackNear : {} %s\n", Type, EcSlow);

}

//=============================================================================
//            TONE DETECTION / GENERATION  CONFIGURATION    <project>.c
//=============================================================================
static void WriteToneStructures (FILE *fp) {
   unsigned int alignI8, tdInstCnt, tdCnt, tgCnt;
   int i;

   if (SysCfg.DspType == Ti647)
      alignI8 = 128;
   else if (SysCfg.DspType == Ti667)
      alignI8 = 128;
   else
      alignI8 = 0;

   tdCnt     = ToneDetInstances;  // Number of streams to perform tone detection on
   tdInstCnt = TONEINSTANCES();   // Number of tone detection steams * concurrent tone detection types
   tgCnt     = SysCfg.numTGChans;
   fprintf (fp,"\n//{\n");

#ifndef INSTANCE_ALLOCATION
   // Tone Detection data structures.
   if (SysCfg.maxToneDetTypes) {
      fprintf (fp, "TDInstance_t  ToneDetInstance[MAX_NUM_TONEDET_CHANNELS * MAX_TONE_DET_TYPES];\n");
      if (algBits.toneDetEnable_B) {
         fprintf (fp, "TDInstance_t  ToneDetInstanceB[MAX_NUM_TONEDET_CHANNELS * MAX_TONE_DET_TYPES];\n\n");
      } else {
         fprintf (fp, "ADT_UInt16    ToneDetInstanceB;     // Stub for linker\n\n");
      }
   } else {
      fprintf (fp, "ADT_UInt16    ToneDetInstance;      // Stub for linker\n");
      fprintf (fp, "ADT_UInt16    ToneDetInstanceB;     // Stub for linker\n\n");
   }

   if (CEDDetInstances != 0) {
      fprintf (fp, "ADT_UInt16       CedDetAvail = CED_DETECT_INSTANCES;\n");
      fprintf (fp, "ADT_UInt16       CedDetInUse[CED_DETECT_INSTANCES];\n");
      fprintf (fp, "FaxCEDChannel_t  faxCedInstanceA[CED_DETECT_INSTANCES];\n");
      if (algBits.toneDetEnable_B) 
         fprintf (fp, "FaxCEDChannel_t  faxCedInstanceB[CED_DETECT_INSTANCES];\n\n");
      else
         fprintf (fp, "ADT_UInt16 faxCedInstanceB;\n\n");
   } else {
      fprintf (fp, "ADT_UInt16 CedDetInUse;\n");
      fprintf (fp, "ADT_UInt16 CedDetAvail;\n");
      fprintf (fp, "ADT_UInt16 faxCedInstanceA;\n");
      fprintf (fp, "ADT_UInt16 faxCedInstanceB;\n\n");
   }

   if (CNGDetInstances != 0) {
      fprintf (fp, "ADT_UInt16       CngDetInUse[CNG_DETECT_INSTANCES];\n");
      fprintf (fp, "ADT_UInt16       CngDetAvail = CNG_DETECT_INSTANCES;\n");
      fprintf (fp, "FAXCNGInstance_t faxCngInstanceA[CNG_DETECT_INSTANCES];\n");
      if (algBits.toneDetEnable_B) 
         fprintf (fp, "FAXCNGInstance_t faxCngInstanceB[CNG_DETECT_INSTANCES];\n\n");
      else
         fprintf (fp, "ADT_UInt16 faxCngInstanceB;\n\n");
   } else {
      fprintf (fp, "ADT_UInt16 CngDetInUse;\n");
      fprintf (fp, "ADT_UInt16 CngDetAvail;\n");
      fprintf (fp, "ADT_UInt16 faxCngInstanceA;\n");
      fprintf (fp, "ADT_UInt16 faxCngInstanceB;\n\n");
   }

#else
   fprintf   (fp, "ADT_UInt16    ToneDetectorsAvail = MAX_NUM_TONEDET_CHANNELS;\n");
   if (SysCfg.maxToneDetTypes) {
      fprintf (fp, "ADT_UInt16    ToneDetectorInUse[MAX_NUM_TONEDET_CHANNELS];\n");
      fprintf (fp, "#pragma DATA_SECTION (ToneDetectorsAvail, \"POOL_ALLOC\")\n");
      fprintf (fp, "#pragma DATA_SECTION (ToneDetectorInUse,  \"POOL_ALLOC\")\n");
      generateArrayElements (fp, tdInstCnt, "TDInstance_t", "ToneDetInstance",
                                            "CHAN_INST_DATA:ToneDetect", alignI8);
   } else {
      fprintf (fp, "ADT_UInt16    ToneDetectorInUse;\n");
      fprintf (fp, "ADT_UInt16    ToneDetInstance;      // Stub for linker\n");
      fprintf (fp, "#pragma DATA_SECTION (ToneDetectorsAvail, \"STUB_DATA_SECT\")\n");
      fprintf (fp, "#pragma DATA_SECTION (ToneDetectorInUse,  \"STUB_DATA_SECT\")\n");
      fprintf (fp, "#pragma DATA_SECTION (ToneDetInstance,    \"STUB_DATA_SECT\")\n\n" );
   }

   fprintf (fp,    "ADT_UInt16    CedDetAvail = CED_DETECT_INSTANCES;\n");
   if (CEDDetInstances != 0) {
      fprintf (fp, "ADT_UInt16    CedDetInUse[CED_DETECT_INSTANCES];\n");
      fprintf (fp, "#pragma DATA_SECTION (CedDetAvail, \"POOL_ALLOC\")\n");
      fprintf (fp, "#pragma DATA_SECTION (CedDetInUse, \"POOL_ALLOC\")\n");
      generateArrayElements (fp, CEDDetInstances, "FaxCEDChannel_t", "faxCedInstance",
                                                  "CHAN_INST_DATA:ToneDetect", alignI8);
   } else {
      fprintf (fp, "ADT_UInt16    CedDetInUse;\n");
      fprintf (fp, "ADT_UInt16    faxCedInstance;\n");
      fprintf (fp, "#pragma DATA_SECTION (CedDetAvail,    \"STUB_DATA_SECT\")\n");
      fprintf (fp, "#pragma DATA_SECTION (CedDetInUse,    \"STUB_DATA_SECT\")\n");
      fprintf (fp, "#pragma DATA_SECTION (faxCedInstance, \"STUB_DATA_SECT\")\n\n" );
   }

   fprintf (fp, "ADT_UInt16    CngDetAvail = CNG_DETECT_INSTANCES;\n");
   if (CNGDetInstances != 0) {
      fprintf (fp, "ADT_UInt16    CngDetInUse[CNG_DETECT_INSTANCES];\n");
      fprintf (fp, "#pragma DATA_SECTION (CngDetAvail, \"POOL_ALLOC\")\n");
      fprintf (fp, "#pragma DATA_SECTION (CngDetInUse, \"POOL_ALLOC\")\n");
      generateArrayElements (fp, CNGDetInstances, "FAXCNGInstance_t", "faxCngInstance",
                                                  "CHAN_INST_DATA:ToneDetect", alignI8);
   } else {
      fprintf (fp, "ADT_UInt16    CngDetInUse;\n");
      fprintf (fp, "ADT_UInt16    faxCngInstance;\n");
      fprintf (fp, "#pragma DATA_SECTION (CngDetAvail, \"POOL_ALLOC\")\n");
      fprintf (fp, "#pragma DATA_SECTION (CngDetInUse, \"POOL_ALLOC\")\n");
      fprintf (fp, "#pragma DATA_SECTION (faxCngInstance, \"STUB_DATA_SECT\")\n\n" );
   }

   // Channel instance allocation
   fprintf (fp, "ADT_UInt16 ArbToneDetectorsAvail = ARB_DETECT_INSTANCES;\n");
   if (ARBDetInstances != 0) {
      fprintf (fp, "ADT_UInt16 ArbToneDetectorInUse      [ARB_DETECT_INSTANCES];\n");
      fprintf (fp, "#pragma DATA_SECTION (ArbToneDetectorsAvail, \"POOL_ALLOC\")\n");
      fprintf (fp, "#pragma DATA_SECTION (ArbToneDetectorInUse,  \"POOL_ALLOC\")\n");
      generateArrayElements (fp, ARBDetInstances, "ARBIT_TDInstance_t", "ArbToneDetInstance",
                                                  "CHAN_INST_DATA:ToneDetect", alignI8);
   } else {
      fprintf (fp, "ADT_UInt16 ArbToneDetectorInUse;\n");
      fprintf (fp, "#pragma DATA_SECTION (ArbToneDetectorsAvail, \"STUB_DATA_SECT\")\n");
      fprintf (fp, "#pragma DATA_SECTION (ArbToneDetectorInUse,  \"STUB_DATA_SECT\")\n\n");
   }

   // Group instance allocation
   fprintf (fp, "ADT_UInt16 activeArbToneCfg = VOID_INDEX;\n");
   fprintf (fp, "ADT_UInt16 numArbToneCfgs = NUM_ARB_TONE_CONFIGS;\n");
   if (ARBDetInstances != 0) {
      fprintf (fp, "ADT_UInt16 ArbToneConfigured       [NUM_ARB_TONE_CONFIGS];\n");
      fprintf (fp, "#pragma DATA_SECTION (ArbToneConfigured, \"POOL_ALLOC\")\n");
      fprintf (fp, "#pragma DATA_SECTION (activeArbToneCfg,  \"POOL_ALLOC\")\n");
      fprintf (fp, "#pragma DATA_SECTION (numArbToneCfgs,    \"POOL_ALLOC\")\n\n");
      generateArrayElements (fp, ARBCfgCount, "ArbitDtInstance_t", "ArbToneCfgInst",
                                                  "CHAN_INST_DATA:ToneDetect", alignI8);
   } else {
      fprintf (fp, "ADT_UInt16 ArbToneConfigured;\n");
      fprintf (fp, "#pragma DATA_SECTION (activeArbToneCfg,  \"STUB_DATA_SECT\")\n");
      fprintf (fp, "#pragma DATA_SECTION (numArbToneCfgs,    \"STUB_DATA_SECT\")\n");
      fprintf (fp, "#pragma DATA_SECTION (ArbToneConfigured, \"STUB_DATA_SECT\")\n\n");
   }

#endif

   if (algBits.toneRlyDetEnable) {
      generateArrayElements (fp, tdCnt, "TRDetectInstance_t", "ToneRlyDetInstance",
                                        "CHAN_INST_DATA:ToneDetect", alignI8);
   } else {
      fprintf (fp, "ADT_UInt16    ToneRlyDetInstance;   // Stub for linker\n");
      fprintf (fp, "#pragma DATA_SECTION (ToneRlyDetInstance, \"STUB_DATA_SECT\")\n\n" );
   }
   // Tone Generation data structures.
   if (SysCfg.numTGChans) {
      fprintf (fp, "ADT_UInt16   numToneGenChansAvail = MAX_NUM_TONEGEN_CHANNELS;\n");
      fprintf (fp, "ADT_UInt16   toneGenChanInUse[MAX_NUM_TONEGEN_CHANNELS];\n");
       
      fprintf (fp, "#pragma DATA_SECTION (numToneGenChansAvail, \"POOL_ALLOC\")\n");
      fprintf (fp, "#pragma DATA_SECTION (toneGenChanInUse,     \"POOL_ALLOC\")\n");
      generateArrayElements (fp, tgCnt, "TGInstance_t", "ToneGenInstance",
                                        "CHAN_INST_DATA:ToneGen", alignI8);
      generateArrayElements (fp, tgCnt, "TGParams_1_t", "ToneGenParams",
                                        "CHAN_INST_DATA:ToneGen", alignI8);

      if (algBits.dtmfDialEnable) {
            generateArrayElements (fp, tgCnt, "DtmfDialInfo_t", "DtmfDialInstance",
                                       "CHAN_INST_DATA:DtmfDial", alignI8);
      } else {
          fprintf (fp, "ADT_UInt16 DtmfDialInstance;\n");
      }


    } else {
      fprintf (fp, "ADT_UInt16 numToneGenChansAvail;\n");
      fprintf (fp, "ADT_UInt16 toneGenChanInUse;\n");
      fprintf (fp, "ADT_UInt16 ToneGenInstance;\n");
      fprintf (fp, "ADT_UInt16 ToneGenParams;\n");

      fprintf (fp, "#pragma DATA_SECTION (numToneGenChansAvail,  \"STUB_DATA_SECT\")\n");
      fprintf (fp, "#pragma DATA_SECTION (toneGenChanInUse,      \"STUB_DATA_SECT\")\n");
      fprintf (fp, "#pragma DATA_SECTION (ToneGenInstance,       \"STUB_DATA_SECT\")\n" );
      fprintf (fp, "#pragma DATA_SECTION (ToneGenParams,         \"STUB_DATA_SECT\")\n\n");
    }
   /*
    if(algBits.tasDetEnable) {
      fprintf (fp, "ADT_UInt16   numTasDetChansAvail = MAX_NUM_RXCID_CHANNELS;\n");
      fprintf (fp, "ADT_UInt16   tasDetChanInUse[MAX_NUM_RXCID_CHANNELS];\n");
       
      fprintf (fp, "#pragma DATA_SECTION (numTasDetChansAvail, \"POOL_ALLOC\")\n");
      fprintf (fp, "#pragma DATA_SECTION (tasDetChanInUse,     \"POOL_ALLOC\")\n");
      generateArrayElements (fp, tgCnt, "CIDRX_TASDET_Inst_t", "TasDetInstance",
                                        "CHAN_INST_DATA:TasDet", alignI8);
    } else {
      fprintf (fp, "ADT_UInt16 numTasDetChansAvail;\n");
      fprintf (fp, "ADT_UInt16 tasDetChanInUse;\n");
      fprintf (fp, "ADT_UInt16 TasDetInstance;\n");

      fprintf (fp, "#pragma DATA_SECTION (numTasDetChansAvail,  \"STUB_DATA_SECT\")\n");
      fprintf (fp, "#pragma DATA_SECTION (tasDetChanInUse,      \"STUB_DATA_SECT\")\n");
      fprintf (fp, "#pragma DATA_SECTION (TasDetInstance,       \"STUB_DATA_SECT\")\n" );
    }
	*/
    fprintf (fp, "int numPastToneRlyGenEvents = NUM_TRGEN_EVENTS;\n");
    if (algBits.toneRlyGenEnable) {
      generateArrayElements (fp, SysCfg.maxNumChannels, "TRGenerateInstance_t", "ToneRlyGenInstance",
                                        "CHAN_INST_DATA:ToneGen", alignI8);
#if 0
      generateArrayElements (fp, SysCfg.maxNumChannels, "TRGenerateEvent_t", "ToneRlyGenEvent",
                                        "CHAN_INST_DATA:ToneGen", alignI8);
#else
      fprintf (fp, "//{   TRGenerateEvent_t ToneRlyGenEvent STRUCTURES to CHAN_INST_DATA:ToneGen\n");
      for (i = 0; i < (int)SysCfg.maxNumChannels; i++) {
         fprintf (fp, "\nstatic TRGenerateEvent_t ToneRlyGenEvent%d[NUM_TRGEN_EVENTS];\n",i);
         fprintf (fp, "#pragma DATA_SECTION (ToneRlyGenEvent%d, \"CHAN_INST_DATA:ToneGen\")\n", i);
         fprintf (fp, "#pragma DATA_ALIGN   (ToneRlyGenEvent%d, %d)\n", i, alignI8);
      }
      
      fprintf (fp,"\nTRGenerateEvent_t* const ToneRlyGenEvent[] = {\n");
      for (i = 0; i < ((int)(SysCfg.maxNumChannels - 1)); i++) {
         fprintf (fp, "\t&ToneRlyGenEvent%d[0],\n", i);
      }
      fprintf (fp, "\t&ToneRlyGenEvent%d[0]\n", i);
   
      fprintf (fp, "};\n//}\n\n");
#endif
    } else {
       fprintf (fp, "ADT_UInt16 ToneRlyGenInstance;   // Stub for linker\n");
       fprintf (fp, "ADT_UInt16 ToneRlyGenEvent;      // Stub for linker\n");
       fprintf (fp, "#pragma DATA_SECTION (ToneRlyGenInstance, \"STUB_DATA_SECT\")\n" );
       fprintf (fp, "#pragma DATA_SECTION (ToneRlyGenEvent,    \"STUB_DATA_SECT\")\n\n" );
    }
   fprintf (fp,"//}\n\n");

    // Per frame scratch data
    if (!IsC64) return;


#ifndef COMBINED_SCRATCH   

    WriteToneDetectScratch (FRAME_8_CFG_BIT,    "1ms");
    WriteToneDetectScratch (FRAME_20_CFG_BIT, "2_5ms");
    WriteToneDetectScratch (FRAME_40_CFG_BIT,   "5ms");
    WriteToneDetectScratch (FRAME_80_CFG_BIT,  "10ms");
    WriteToneDetectScratch (FRAME_160_CFG_BIT, "20ms");
#ifdef MELP
    WriteToneDetectScratch (FRAME_180_CFG_BIT, "22_5ms");
#endif
    WriteToneDetectScratch (FRAME_240_CFG_BIT, "30ms");
#endif
}
static void WriteLbStructures (FILE *fp) {

   if (SysCfg.maxNumLbCoders == 0) {
      // Loopback Coder is disabled... allocate to satisfy linker
      fprintf (fp, "\n// Loopback Coder is disabled... allocate to satisfy linker\n");
      fprintf (fp, "int lbScratch;\n");
      fprintf (fp, "ADT_UInt16 lbScratchI16 = 0;\n");
      fprintf (fp, "#pragma DATA_SECTION (lbScratch,    \"STUB_DATA_SECT\")\n");
      fprintf (fp, "#pragma DATA_SECTION (lbScratchI16, \"STUB_DATA_SECT\")\n");

      fprintf (fp, "const int numLbsInUse = 0;\n");
      fprintf (fp, "int teardownLBPending;\n");
      fprintf (fp, "int teardownLBCoder;\n");
      fprintf (fp, "int LoopbackList;\n");
      fprintf (fp, "#pragma DATA_SECTION (teardownLBPending, \"STUB_DATA_SECT\")\n");
      fprintf (fp, "#pragma DATA_SECTION (teardownLBCoder,   \"STUB_DATA_SECT\")\n");
      fprintf (fp, "#pragma DATA_SECTION (LoopbackList,      \"STUB_DATA_SECT\")\n\n");
      
   } else {
      // Loopback Coder allocation
      fprintf (fp, "\n// Loopback Coder allocation\n");
      fprintf (fp, "ADT_UInt16 lbScratchI16 = BG_SCRATCH_I16;\n");
      fprintf (fp, "#pragma DATA_SECTION (lbScratchI16,       \"PER_CORE_DATA:BackGroundCoder\")\n\n");

      fprintf (fp, "ADT_UInt32 lbScratch[BG_SCRATCH_I16/2];\n");
      Align (lbScratch,  8, "FAST_SCRATCH");

      fprintf (fp, "int numLbsInUse = 0;\n");
      fprintf (fp, "int teardownLBPending[MAX_NUM_LB_CHANNELS];\n");
      fprintf (fp, "int teardownLBCoder[MAX_NUM_LB_CHANNELS];\n");
      fprintf (fp, "chanInfo_t *LoopbackList[MAX_NUM_LB_CHANNELS];\n");
      fprintf (fp, "#pragma DATA_SECTION (numLbsInUse,       \"PER_CORE_DATA:BackGroundCoder\")\n");
      fprintf (fp, "#pragma DATA_SECTION (teardownLBPending, \"PER_CORE_DATA:BackGroundCoder\")\n");
      fprintf (fp, "#pragma DATA_SECTION (teardownLBCoder,   \"PER_CORE_DATA:BackGroundCoder\")\n");
      fprintf (fp, "#pragma DATA_SECTION (LoopbackList,      \"PER_CORE_DATA:BackGroundCoder\")\n\n");
      
  }


}
  
//=============================================================================
//            NOISE SUPPRESSION  CONFIGURATION     <project>.c
//=============================================================================
static void  WriteNoiseStructures (FILE *fp, unsigned int numChannels) {
   unsigned int i, j;
   unsigned int alignment;

   if (SysCfg.DspType == Ti647) 
      alignment = 128;  // External Cache size
   else if (SysCfg.DspType == Ti667) 
      alignment = 128;  // External Cache size
   else
      alignment = NOISE_SUPPRESS_ALIGN;
   (void) i;
   (void) j;
 
#ifdef NOT_SUPPORTED
    // Noise Suppression structures
    if (algBits.noiseSuppressEnable) {
      fprintf (fp, "//{\n");
      for (j=0; j<2; j++) {
      for (i=0; i<numChannels; i++) {
         fprintf (fp, "NCAN_Channel_t  NoiseInstance%d_%d;\n", j, i);
#ifdef MULTI_CORE
         fprintf (fp, "#pragma DATA_SECTION (NoiseInstance%d_%d, \"NCAN_INST_DATA\")\n", j, i);
         fprintf (fp, "#pragma DATA_ALIGN   (NoiseInstance%d_%d, %d)\n", j, i, alignment);
#else
         fprintf (fp, "#pragma DATA_SECTION (NoiseInstance%d_%d, \"NCAN%u_%u\")\n", j, i, j, i);
#endif
      } }
      fprintf (fp, "//}\n");

      fprintf (fp, "\nADT_Int16  NoiseRemnantPool [2][MAX_NUM_CHANNELS * NCAN_FRAME_SIZE];\n");
      fprintf (fp, "NCAN_Channel_t* const NoiseSuppressInstance [][2] = {\n");     
      
      for (i=0; i < numChannels-1; i++) 
         fprintf (fp, "\t{ &NoiseInstance0_%d, \t&NoiseInstance1_%d },\n", i, i);
      fprintf (fp, "\t{ &NoiseInstance0_%d, \t&NoiseInstance1_%d }\n};\n\n", i, i);

      fprintf (fp, "NCAN_Params_t NCAN_Params = { %d, %d, %d, %d, NCAN_FRAME_SIZE, %d };\n",
                                                  NoiseSuppression);
      fprintf (fp, "#pragma DATA_SECTION (NoiseRemnantPool, \"CHAN_INST_DATA:NoiseSuppression\")\n\n" );

    } else
#endif
    {
        fprintf (fp, "// Noise cancellation not supported at this time\n");
        fprintf (fp, "ADT_UInt16*  const NoiseSuppressInstance = 0;  // Stub for linker\n");
        fprintf (fp, "ADT_Int16  NoiseRemnantPool;        // Stub for linker\n");
        fprintf (fp, "ADT_UInt16 NCAN_Params;             // Stub for linker\n");
        fprintf (fp, "#pragma DATA_SECTION (NoiseRemnantPool, \"STUB_DATA_SECT\")\n" );
        fprintf (fp, "#pragma DATA_SECTION (NCAN_Params,      \"STUB_DATA_SECT\")\n\n" );
    }

}

//=============================================================================
//            VAD/CNG/AGC CONFIGURATION      <project>.c
//=============================================================================
static void  WriteVadAgcStructures (FILE *fp) {
   unsigned int alignI8;

   if (SysCfg.DspType == Ti647)
      alignI8 = 128;
   else if (SysCfg.DspType == Ti667)
      alignI8 = 128;
   else
      alignI8 = 0;

    if (algBits.vadCngEnable) {
       if (!IsC64)
          generateArrayElements (fp, 2*SysCfg.maxNumChannels, "VadChannelInst_t", "VadCngInstance",
                                                         "CHAN_INST_DATA:VAD", alignI8);
       else
          generateArrayElements (fp, 2*SysCfg.maxNumChannels, "VADCNG_Instance_t", "VadCngInstance",
                                                          "CHAN_INST_DATA:VAD", alignI8);

    } else {
      fprintf (fp, "ADT_UInt16 VadCngInstance;       // Stub for linker\n");
      fprintf (fp, "#pragma DATA_SECTION (VadCngInstance, \"STUB_DATA_SECT\")\n\n" );
    }

    if (SysCfg.numAGCChans) {

      fprintf (fp, "ADT_UInt16      numAGCChansAvail = MAX_NUM_AGC_CHANNELS;\n");
      fprintf (fp, "ADT_UInt16      agcChanInUse[MAX_NUM_AGC_CHANNELS];\n");

      fprintf (fp, "#pragma DATA_SECTION (numAGCChansAvail, \"POOL_ALLOC\")\n");
      fprintf (fp, "#pragma DATA_SECTION (agcChanInUse,     \"POOL_ALLOC\")\n");
      generateArrayElements (fp, SysCfg.numAGCChans, "AGCInstance_t", "AgcInstance",
                                                      "CHAN_INST_DATA:AGC", alignI8);
   } else {
      fprintf (fp, "const ADT_UInt16 numAGCChansAvail = 0;\n");
      fprintf (fp, "ADT_UInt16 agcChanInUse;\n");
      fprintf (fp, "ADT_UInt16 AgcInstance;\n");
      fprintf (fp, "#pragma DATA_SECTION (agcChanInUse,     \"STUB_DATA_SECT\")\n");
      fprintf (fp, "#pragma DATA_SECTION (AgcInstance,      \"STUB_DATA_SECT\")\n\n" );
    }
}

//=============================================================================
//            Sampling rate converter CONFIGURATION      <project>.c
//=============================================================================
static void  WriteSrcStructures (FILE *fp) {
   unsigned int alignI8;
   unsigned i, ii;
   char *hideSymbols = "";     // Hide symbol names from map file.  First one not hidden for base address.

   if (SysCfg.DspType == Ti647)
      alignI8 = 128;
   else if (SysCfg.DspType == Ti667)
      alignI8 = 128;
   else
      alignI8 = 8;

    if (SysCfg.numSrcChans) {

      fprintf (fp, "ADT_UInt16      numSrcUpBy2ChansAvail = MAX_NUM_SRC_CHANNELS;\n");
      fprintf (fp, "ADT_UInt16      srcUpBy2ChanInUse[MAX_NUM_SRC_CHANNELS];\n");

      fprintf (fp, "#pragma DATA_SECTION (numSrcUpBy2ChansAvail, \"POOL_ALLOC\")\n");
      fprintf (fp, "#pragma DATA_SECTION (srcUpBy2ChanInUse,     \"POOL_ALLOC\")\n");
      //generateArrayElements (fp, SysCfg.maxNumChannels, "RateUpBy2_Channel_t", "SrcUpBy2Instance",
      //                                                "CHAN_INST_DATA:SRC_UP", alignI8);

      fprintf (fp, "ADT_UInt16      numSrcDownBy2ChansAvail = MAX_NUM_SRC_CHANNELS;\n");
      fprintf (fp, "ADT_UInt16      srcDownBy2ChanInUse[MAX_NUM_SRC_CHANNELS];\n");

      fprintf (fp, "#pragma DATA_SECTION (numSrcDownBy2ChansAvail, \"POOL_ALLOC\")\n");
      fprintf (fp, "#pragma DATA_SECTION (srcDownBy2ChanInUse,     \"POOL_ALLOC\")\n");
      //generateArrayElements (fp, SysCfg.maxNumChannels, "RateDownBy2_Channel_t", "SrcDownBy2Instance",
      //                                                "CHAN_INST_DATA:SRC_DOWN", alignI8);
      fprintf (fp, "\n\n//{\n");
      for (i = 0; i < SysCfg.numSrcChans; i++) {
         fprintf (fp,"/* Sampling Rate Channel %d */\n",i);
         fprintf (fp,"%sADT_UInt16 SrcDownBy2Instance%d [SRCDOWNBY2_BUFFER_I16];\n", hideSymbols, i);
         fprintf (fp,"%sADT_UInt16 SrcUpBy2Instance%d [SRCUPBY2_BUFFER_I16];\n", hideSymbols, i);
         fprintf (fp, "#pragma DATA_SECTION (SrcDownBy2Instance%d, \"CHAN_INST_DATA:SRC_DOWN\")\n", i);
         fprintf (fp, "#pragma DATA_SECTION (SrcUpBy2Instance%d, \"CHAN_INST_DATA:SRC_UP\")\n", i);
         fprintf (fp, "#pragma DATA_ALIGN   (SrcDownBy2Instance%d, %d);\n", i, alignI8);
         fprintf (fp, "#pragma DATA_ALIGN   (SrcUpBy2Instance%d, %d);\n", i, alignI8);
         hideSymbols = "static ";
      }
      fprintf (fp,"\n\n//}\n");
      if (1 < SysCfg.numSrcChans)    {
        fprintf (fp,"ADT_UInt16 * const SrcDownBy2Instance[] = {\n");
        writeElementNameArray (SysCfg.numSrcChans, "", "SrcDownBy2Instance");

        fprintf (fp,"ADT_UInt16 * const SrcUpBy2Instance[] = {\n");
        writeElementNameArray (SysCfg.numSrcChans, "", "SrcUpBy2Instance");

      }  else if (SysCfg.numSrcChans == 1)    {
        fprintf (fp,"ADT_UInt16 * const SrcDownBy2Instance[] = {SrcDownBy2Instance0};\n");
        fprintf (fp,"ADT_UInt16 * const SrcUpBy2Instance[] = {SrcUpBy2Instance0};\n");
      }

    } else {
      fprintf (fp, "const ADT_UInt16 numSrcUpBy2ChansAvail = 0;\n");
      fprintf (fp, "ADT_UInt16 srcUpBy2ChanInUse;\n");
      fprintf (fp, "ADT_UInt16 SrcUpBy2Instance;\n");
      fprintf (fp, "#pragma DATA_SECTION (srcUpBy2ChanInUse,     \"STUB_DATA_SECT\")\n");
      fprintf (fp, "#pragma DATA_SECTION (SrcUpBy2Instance,      \"STUB_DATA_SECT\")\n\n" );

      fprintf (fp, "const ADT_UInt16 numSrcDownBy2ChansAvail = 0;\n");
      fprintf (fp, "ADT_UInt16 srcDownBy2ChanInUse;\n");
      fprintf (fp, "ADT_UInt16 SrcDownBy2Instance;\n");
      fprintf (fp, "#pragma DATA_SECTION (srcDownBy2ChanInUse,     \"STUB_DATA_SECT\")\n");
      fprintf (fp, "#pragma DATA_SECTION (SrcDownBy2Instance,      \"STUB_DATA_SECT\")\n\n" );
    }
}

//=============================================================================
//            RTP and JitterBuffer CONFIGURATION      <project>.c
//=============================================================================
static void  WriteRTPStructures (FILE *fp) {
   int align;

   if (! (algBits.rtpNetDelivery || algBits.rtpHostDelivery)) 
      return;

   if (SysCfg.DspType == Ti647) 
        align = 128;
   else if (SysCfg.DspType == Ti667) 
        align = 128;
   else
        align = 8;

   generateArrayElements (fp, SysCfg.maxNumChannels, "RtpChanData_t", "RtpChanData", 
                                                   "CHAN_INST_DATA:RTP", align);
   fprintf (fp,"\n\n//{\n");

   fprintf (fp, "ADT_UInt32 SmallJBPool[SMALL_JB_POOL_I32];     // Memory pool for 1, 2.5 and 5 millisecond frame rates\n");
   fprintf (fp, "ADT_UInt32 LargeJBPool[LARGE_JB_POOL_I32];     // Memory pool for 10, 20, 22.5 and 30 millisecond frame rates\n");
   fprintf (fp, "ADT_UInt32 HugeJBPool[HUGE_JB_POOL_I32];       // Memory pool for custom frame rates greater than 30 milliseconds\n");
   fprintf (fp, "#pragma DATA_SECTION (SmallJBPool,  \"JB_POOL\")\n");
   fprintf (fp, "#pragma DATA_SECTION (LargeJBPool,  \"JB_POOL\")\n");
   fprintf (fp, "#pragma DATA_SECTION (HugeJBPool,  \"JB_POOL\")\n");
   fprintf (fp, "#pragma DATA_ALIGN   (SmallJBPool, %d)\n", align);
   fprintf (fp, "#pragma DATA_ALIGN   (LargeJBPool, %d)\n\n", align);
   fprintf (fp, "#pragma DATA_ALIGN   (HugeJBPool, %d)\n\n", align);

   if (algBits.rtpNetDelivery) {
      fprintf (fp, "CircBufInfo_t RTPFreeBuffers[CORE_CNT];\n");
      fprintf (fp, "#pragma DATA_SECTION (RTPFreeBuffers,  \"RTP_CIRC\")\n");
   }
   fprintf (fp, "CircBufInfo_t RTPCircBuffers[CORE_CNT * 2];              // 1 = To Net, 0 = To DSP\n");
   fprintf (fp, "#pragma DATA_SECTION (RTPCircBuffers,  \"RTP_CIRC\")\n\n");

   fprintf (fp, "ADT_UInt16    ToNetRTPBuffer[RTP_BUFFERS_I16];\n");
   fprintf (fp, "ADT_UInt16    ToDSPRTPBuffer[RTP_BUFFERS_I16];\n");
   fprintf (fp, "#pragma DATA_SECTION (ToNetRTPBuffer, \"RTP_POOL:ToNet\")\n");
   fprintf (fp, "#pragma DATA_SECTION (ToDSPRTPBuffer, \"RTP_POOL:ToDSP\")\n");
   fprintf (fp,"//}\n\n");
}
//=============================================================================
//            RTCP     <project>.c
//=============================================================================
static void  WriteRTCPStructures (FILE *fp) {
   int align;

   if (! (algBits.rtpNetDelivery || algBits.rtpHostDelivery)) {
        // stubs for linker
      fprintf (fp, "ADT_UInt16 numRtcpChannels = 0;\n");
      fprintf (fp, "ADT_UInt16  rtcpData;     // Stub for linker\n\n");
      fprintf (fp, "#pragma DATA_SECTION (numRtcpChannels, \"STUB_DATA_SECT\")\n");
      fprintf (fp, "#pragma DATA_SECTION (rtcpData, \"STUB_DATA_SECT\")\n");
      return;
  }
   
   if (SysCfg.DspType == Ti647) 
        align = 128;
   else if (SysCfg.DspType == Ti667) 
        align = 128;
   else
        align = 8;

   generateArrayElements (fp, SysCfg.maxNumChannels, "rtcpdata_t", "rtcpData", 
                                                   "CHAN_INST_DATA:RTCP", align);
   fprintf (fp, "ADT_UInt16 numRtcpChannels = %d;\n",SysCfg.maxNumChannels);
   fprintf (fp, "#pragma DATA_SECTION (numRtcpChannels, \"POOL_ALLOC\")\n");

   fprintf (fp,"\n\n//{\n");
}
//=============================================================================
//            SRTP      <project>.c
//=============================================================================
static void  WriteSRTPStructures (FILE *fp) {
   int align;
   

   if (SysCfg.DspType == Ti647) 
        align = 128;
   else if (SysCfg.DspType == Ti667) 
        align = 128;
   else
        align = 8;


   if (SysCfg.SRTP_Enable) {

      fprintf (fp, "#pragma DATA_SECTION (SrtpScratchPtr, \"PER_CORE_DATA:SRTP\")\n");
      fprintf (fp, "#pragma DATA_ALIGN   (SrtpScratchPtr, %d)\n", align);
      fprintf (fp, "ADT_UInt32 SrtpScratchPtr[340/4];\n\n");
      fprintf (fp, "const int srtpTxI8 = (SRTPTX_SIZE_I4 * 4);\n");
      generateArrayElements (fp, SysCfg.maxNumChannels, "SrtpTxInstance_t", "SrtpTxChanData", 
                                                   "CHAN_INST_DATA:SRTP", align);

      generateArrayElements (fp, SysCfg.maxNumChannels, "GpakSrtpKeyInfo_t", "SrtpTxKeyData", 
                                                   "CHAN_INST_DATA:SRTP_KEY", align);

      fprintf (fp, "const int srtpRxI8 = (SRTPRX_SIZE_I4 * 4);\n");
      generateArrayElements (fp, SysCfg.maxNumChannels, "SrtpRxInstance_t", "SrtpRxChanData",
                                                   "CHAN_INST_DATA:SRTP", align);

      generateArrayElements (fp, SysCfg.maxNumChannels, "GpakSrtpKeyInfo_t", "SrtpRxKeyData", 
                                                   "CHAN_INST_DATA:SRTP_KEY", align);
   } else {
      fprintf (fp, "#pragma DATA_SECTION (SrtpScratchPtr, \"STUB_DATA_SECT\")\n");
      fprintf (fp, "#pragma DATA_ALIGN   (SrtpScratchPtr, %d)\n", align);
      fprintf (fp, "ADT_UInt8 SrtpScratchPtr[1]; \n");
      fprintf (fp, "SrtpTxInstance_t*  const SrtpTxChanData[] = {0};\n");
      fprintf (fp, "GpakSrtpKeyInfo_t* const SrtpTxKeyData[] = {0};\n");
      fprintf (fp, "const int srtpTxI8 = 0;\n");
      fprintf (fp, "SrtpRxInstance_t*  const SrtpRxChanData[] = {0};\n");
      fprintf (fp, "GpakSrtpKeyInfo_t* const SrtpRxKeyData[] = {0};\n");
      fprintf (fp, "const int srtpRxI8 = 0;\n");
   }
}

//=============================================================================
//            Caller Id CONFIGURATION      <project>.c
//=============================================================================
static void  WriteCidStructures (FILE *fp) {

   int align;

   if (SysCfg.DspType == Ti647) 
        align = 128;
   else if (SysCfg.DspType == Ti667) 
        align = 128;
   else
        align = 8;

   fprintf (fp, "\nconst ADT_UInt16 rxCidBufferI8 = RX_CID_MSG_I8;\n");
   if (SysCfg.numRxCidChans == 0) {
#ifndef COMBINED_SCRATCH   
        WriteCidScratch (FRAME_8_CFG_BIT,    "1ms", 1);
        WriteCidScratch (FRAME_20_CFG_BIT, "2_5ms", 1);
        WriteCidScratch (FRAME_40_CFG_BIT,   "5ms", 1);
        WriteCidScratch (FRAME_80_CFG_BIT,  "10ms", 1);
        WriteCidScratch (FRAME_160_CFG_BIT, "20ms", 1);
        WriteCidScratch (FRAME_180_CFG_BIT, "22_5ms", 1);
        WriteCidScratch (FRAME_240_CFG_BIT, "30ms", 1);
#endif
      fprintf (fp, "ADT_UInt16 numRxCidChansAvail=0;\n");
      fprintf (fp, "ADT_UInt16 rxCidInUse;\n");
      fprintf (fp, "ADT_UInt16 rxCidPool;\n");
      fprintf (fp, "ADT_UInt8  rxCidBuffer;\n");
      fprintf (fp, "#pragma DATA_SECTION (numRxCidChansAvail,  \"POOL_ALLOC\")\n");
      fprintf (fp, "#pragma DATA_SECTION (rxCidInUse,  \"STUB_DATA_SECT\")\n");
      fprintf (fp, "#pragma DATA_SECTION (rxCidPool,   \"STUB_DATA_SECT\")\n");
      fprintf (fp, "#pragma DATA_SECTION (rxCidBuffer, \"STUB_DATA_SECT\")\n\n");
   } else {
#ifndef COMBINED_SCRATCH   
      WriteCidScratch (FRAME_8_CFG_BIT,    "1ms", 16);
      WriteCidScratch (FRAME_20_CFG_BIT, "2_5ms", 40);
      WriteCidScratch (FRAME_40_CFG_BIT,   "5ms", 80);
      WriteCidScratch (FRAME_80_CFG_BIT,  "10ms", 160);
      WriteCidScratch (FRAME_160_CFG_BIT, "20ms", 320);
      WriteCidScratch (FRAME_180_CFG_BIT, "22_5ms", 360);
      WriteCidScratch (FRAME_240_CFG_BIT, "30ms", 480);
#endif        
      fprintf (fp, "ADT_UInt16 numRxCidChansAvail=MAX_NUM_RXCID_CHANNELS;\n");
      fprintf (fp, "ADT_UInt16 rxCidInUse [MAX_NUM_RXCID_CHANNELS];\n");
      fprintf (fp, "ADT_UInt8  rxCidBuffer[MAX_NUM_RXCID_CHANNELS * RX_CID_MSG_I8];\n");
      fprintf (fp, "#pragma DATA_SECTION (numRxCidChansAvail,  \"POOL_ALLOC\")\n");
      fprintf (fp, "#pragma DATA_SECTION (rxCidInUse,          \"POOL_ALLOC\")\n");
      fprintf (fp, "#pragma DATA_SECTION (rxCidBuffer,         \"RX_CID_BUFF_SECT\")\n");
      fprintf (fp, "#pragma DATA_ALIGN   (rxCidBuffer,         %d)\n\n", align);

      if (SysCfg.type2CID) {
         generateArrayElements (fp, SysCfg.numRxCidChans, "CIDRX2_Inst_t", "rxCidPool", 
                                                     "CHAN_INST_DATA:RxCID", align);
      } else {
         generateArrayElements (fp, SysCfg.numRxCidChans, "CIDRX_Inst_t", "rxCidPool", 
                                                     "CHAN_INST_DATA:RxCID", align);
      }

   }

   fprintf (fp, "\nconst ADT_UInt16 txCidBufferI8 = TX_CID_MSG_I8;\n");
   fprintf (fp,   "const ADT_UInt16 txCidBurstI8  = TX_CID_BRST_I8;\n");
   if (SysCfg.numTxCidChans == 0) {
      fprintf (fp, "ADT_UInt16 numTxCidChansAvail=0;\n");
      fprintf (fp, "ADT_UInt16 txCidInUse;\n");
      fprintf (fp, "ADT_UInt16 txCidPool;\n");
      fprintf (fp, "ADT_UInt8  txCidMsgBuffer;\n");
      fprintf (fp, "ADT_UInt8  txCidBrstBuffer;\n");

      fprintf (fp, "#pragma DATA_SECTION (numTxCidChansAvail,  \"POOL_ALLOC\")\n");
      fprintf (fp, "#pragma DATA_SECTION (txCidInUse,      \"STUB_DATA_SECT\")\n");
      fprintf (fp, "#pragma DATA_SECTION (txCidPool,       \"STUB_DATA_SECT\")\n");
      fprintf (fp, "#pragma DATA_SECTION (txCidMsgBuffer,  \"STUB_DATA_SECT\")\n");
      fprintf (fp, "#pragma DATA_SECTION (txCidBrstBuffer, \"STUB_DATA_SECT\")\n\n");
   } else {
      fprintf (fp, "ADT_UInt16 numTxCidChansAvail=MAX_NUM_TXCID_CHANNELS;\n");
      fprintf (fp, "ADT_UInt16 txCidInUse     [MAX_NUM_TXCID_CHANNELS];\n");
      fprintf (fp, "ADT_UInt8  txCidMsgBuffer [MAX_NUM_TXCID_CHANNELS * TX_CID_MSG_I8];\n");
      fprintf (fp, "ADT_UInt8  txCidBrstBuffer[MAX_NUM_TXCID_CHANNELS * TX_CID_BRST_I8];\n\n");

      fprintf (fp, "#pragma DATA_SECTION (numTxCidChansAvail,  \"POOL_ALLOC\")\n");
      fprintf (fp, "#pragma DATA_SECTION (txCidInUse,          \"POOL_ALLOC\")\n");
      fprintf (fp, "#pragma DATA_SECTION (txCidMsgBuffer,      \"TX_CID_MSGBUFF_SECT\")\n");
      fprintf (fp, "#pragma DATA_ALIGN   (txCidMsgBuffer,      %d)\n", align);
      fprintf (fp, "#pragma DATA_SECTION (txCidBrstBuffer,     \"TX_CID_BRSTBUFF_SECT\")\n");
      fprintf (fp, "#pragma DATA_ALIGN   (txCidBrstBuffer,     %d)\n\n", align);

     if(SysCfg.type2CID) {
        generateArrayElements (fp, SysCfg.numTxCidChans, "CIDTX2_Inst_t", "txCidPool", 
                                                     "CHAN_INST_DATA:TxCID", align);
     } else {
        generateArrayElements (fp, SysCfg.numTxCidChans, "CIDTX_Inst_t", "txCidPool", 
                                                     "CHAN_INST_DATA:TxCID", align);
     }

   }
}

//=============================================================================
//            Multicore CONFIGURATION      <project>.c
//=============================================================================
static void  WriteMultiCoreStructures (FILE *fp) {
   unsigned int i, frameCnt;
   int Ti647Offsets[] = { 0x10000000, 0x11000000, 0x12000000, 0x13000000, 0x14000000, 0x15000000 };
   int Ti667Offsets[] = { 0x10000000, 0x11000000, 0x12000000, 0x13000000, 0x14000000, 0x15000000 };
   int nullOffsets [] = { 0 };
   int *Offsets;

   if (SysCfg.DspType == Ti647) {
      Offsets = Ti647Offsets;
      frameCnt = 6 * frameTaskCnt;
      fprintf (fp, "ADT_Word MaxFrameCnt   = %u;\n", frameCnt);
      fprintf (fp, "ADT_Word MultiCoreCpuUsage[%u];\n", frameCnt);
      fprintf (fp, "ADT_Word MultiCoreCpuPeakUsage[%u];\n", frameCnt);
      fprintf (fp, "#pragma DATA_SECTION (MultiCoreCpuUsage, \"SHARED_DATA_SECT:CpuUsage\")\n");
      fprintf (fp, "#pragma DATA_SECTION (MultiCoreCpuPeakUsage, \"SHARED_DATA_SECT:CpuUsage\")\n\n");
   }
   else if (SysCfg.DspType == Ti667) {
      Offsets = Ti667Offsets;
      frameCnt = 6 * frameTaskCnt;
      fprintf (fp, "ADT_Word MaxFrameCnt   = %u;\n", frameCnt);
      fprintf (fp, "ADT_Word MultiCoreCpuUsage[%u];\n", frameCnt);
      fprintf (fp, "ADT_Word MultiCoreCpuPeakUsage[%u];\n", frameCnt);
      fprintf (fp, "#pragma DATA_SECTION (MultiCoreCpuUsage, \"SHARED_DATA_SECT:CpuUsage\")\n");
      fprintf (fp, "#pragma DATA_SECTION (MultiCoreCpuPeakUsage, \"SHARED_DATA_SECT:CpuUsage\")\n\n");
   } else {
      Offsets = nullOffsets;
      fprintf (fp, "ADT_Word MaxFrameCnt   = %u;\n", frameTaskCnt);
      fprintf (fp, "ADT_Word MultiCoreCpuUsage[%u];\n", frameTaskCnt);
      fprintf (fp, "ADT_Word MultiCoreCpuPeakUsage[%u];\n", frameTaskCnt);
   }

   fprintf (fp, "far int DSPCore = 0;\n");
   fprintf (fp, "#pragma DATA_SECTION (DSPCore, \"PER_CORE_DATA:CoreID\")\n");

   fprintf (fp, "int DSPCoreLocks;\n");
   fprintf (fp, "#pragma DATA_SECTION (DSPCoreLocks, \"PER_CORE_DATA:Locks\")\n");
   
   fprintf (fp, "const int DSPTotalCores = CORE_CNT;\n");
   fprintf (fp, "const ADT_UInt32 CoreOffsets[] = {\n");
   for (i = 0; i < coreCnt; i++) {
       fprintf (fp, "   0x%x,\n", Offsets[i]);
   }
   fprintf (fp, "};\n");
}

//=============================================================================
//            EXTERNAL MEMORY DMA CONFIGURATION    <project>.c
//=============================================================================
void WriteDMAStructures (blockStatus_t *bs,  ecMemSpec_t *pcmEcMem,  ecMemSpec_t *pktEcMem, 
                         ExternalMemInfo_t *extMem, FILE *fp) {
    int encSize, decSize, maxCoderSize;
    ecMemSpec_t maxEcMem;
    

    maxEcMem.InstanceSize       = MAX (pcmEcMem->InstanceSize, pktEcMem->InstanceSize);
    maxEcMem.DaStateSize        = MAX (pcmEcMem->DaStateSize, pktEcMem->DaStateSize);     
    maxEcMem.SaStateSize        = MAX (pcmEcMem->SaStateSize, pktEcMem->SaStateSize);     
    maxEcMem.EchoPathSize       = MAX (pcmEcMem->EchoPathSize, pktEcMem->EchoPathSize);    
    maxEcMem.BackGroundEpSize   = MAX (pcmEcMem->BackGroundEpSize, pktEcMem->BackGroundEpSize);

    if (maxEcMem.InstanceSize == 0)     maxEcMem.InstanceSize = 1;
    if (maxEcMem.DaStateSize == 0)      maxEcMem.DaStateSize = 1;
    if (maxEcMem.SaStateSize == 0)      maxEcMem.SaStateSize = 1;
    if (maxEcMem.EchoPathSize  == 0)    maxEcMem.EchoPathSize  = 1;
    if (maxEcMem.BackGroundEpSize == 0) maxEcMem.BackGroundEpSize = 1; 

    encSize = encodeMemory.Words;
    decSize = decodeMemory.Words;
    if (encSize == 0) encSize = 1;
    if (decSize == 0) decSize = 1;
    maxCoderSize = MAX (encSize, decSize);

    if (SysCfg.DspType == Ti647) return;
    if (SysCfg.DspType == Ti667) return;
    if (extMem->type != EXT_DMA)
        return;

    fprintf (fp,"\n// external memory dma transfer information\n");
    fprintf (fp,"dmaXferInfo_t xferInfoLoadA[MAX_NUM_CHANNELS];\n");
    fprintf (fp,"dmaXferInfo_t xferInfoLoadB[MAX_NUM_CHANNELS];\n");
    fprintf (fp,"dmaXferInfo_t xferInfoStoreA[MAX_NUM_CHANNELS];\n");
    fprintf (fp,"dmaXferInfo_t xferInfoStoreB[MAX_NUM_CHANNELS];\n");
    pragmaSect (xferInfoLoadA, xferInfo);
    pragmaSect (xferInfoLoadB, xferInfo);
    pragmaSect (xferInfoStoreA, xferInfo);
    pragmaSect (xferInfoStoreB, xferInfo);

    fprintf (fp,"\n// ===========================================================");
    fprintf (fp,"\n// internal DMA codec and echo cancellationl buffers\n");

#ifdef DMA_QUEUE_ALLOCATION
    create3Arrays (coderIntBuff,      "q1", 1 <= internalQueueCnt ? maxCoderSize : 1, coderIntPtrs);
    create3Arrays (ecChanIntBuff,     "q1", 1 <= internalQueueCnt ? maxEcMem.InstanceSize : 1, ecChanIntPtrs);  
    create3Arrays (ecSaStateIntBuff,  "q1", 1 <= internalQueueCnt ? maxEcMem.SaStateSize  : 1, ecSaStateIntPtrs);  
    create3Arrays (ecDaStateIntBuff,  "q1", 1 <= internalQueueCnt ? maxEcMem.DaStateSize  : 1, ecDaStateIntPtrs);      
    create3Arrays (ecEchoPathIntBuff, "q1", 1 <= internalQueueCnt ? maxEcMem.EchoPathSize : 1, ecEchoPathIntPtrs);      
    create3Arrays (ecBgEpIntBuff,     "q1", 1 <= internalQueueCnt ? maxEcMem.BackGroundEpSize  : 1, ecBgEchoPathIntPtrs);  

    create3Arrays (coderIntBuff,      "q2", 2 <= internalQueueCnt ? maxCoderSize : 1, coderIntPtrs);
    create3Arrays (ecChanIntBuff,     "q2", 2 <= internalQueueCnt ? maxEcMem.InstanceSize : 1, ecChanIntPtrs);  
    create3Arrays (ecSaStateIntBuff,  "q2", 2 <= internalQueueCnt ? maxEcMem.SaStateSize  : 1, ecSaStateIntPtrs);  
    create3Arrays (ecDaStateIntBuff,  "q2", 2 <= internalQueueCnt ? maxEcMem.DaStateSize  : 1, ecDaStateIntPtrs);      
    create3Arrays (ecEchoPathIntBuff, "q2", 2 <= internalQueueCnt ? maxEcMem.EchoPathSize : 1, ecEchoPathIntPtrs);      
    create3Arrays (ecBgEpIntBuff,     "q2", 2 <= internalQueueCnt ? maxEcMem.BackGroundEpSize  : 1, ecBgEchoPathIntPtrs);  

    create3Arrays (coderIntBuff,      "q3", 3 <= internalQueueCnt ? maxCoderSize : 1, coderIntPtrs);
    create3Arrays (ecChanIntBuff,     "q3", 3 <= internalQueueCnt ? maxEcMem.InstanceSize : 1, ecChanIntPtrs);  
    create3Arrays (ecSaStateIntBuff,  "q3", 3 <= internalQueueCnt ? maxEcMem.SaStateSize  : 1, ecSaStateIntPtrs);  
    create3Arrays (ecDaStateIntBuff,  "q3", 3 <= internalQueueCnt ? maxEcMem.DaStateSize  : 1, ecDaStateIntPtrs);      
    create3Arrays (ecEchoPathIntBuff, "q3", 3 <= internalQueueCnt ? maxEcMem.EchoPathSize : 1, ecEchoPathIntPtrs);      
    create3Arrays (ecBgEpIntBuff,     "q3", 3 <= internalQueueCnt ? maxEcMem.BackGroundEpSize  : 1, ecBgEchoPathIntPtrs);  

#else
        fprintf (fp,"\n// --------  10 millisecond buffers -------------\n");
    create3Arrays (coderIntBuff, "10ms", FRAME_10ms_ENABLED ? maxCoderSize : 1, coderIntPtrs);
    create3Arrays (ecChanIntBuff,     "10ms", FRAME_10ms_ENABLED ? maxEcMem.InstanceSize : 1, ecChanIntPtrs);  
    create3Arrays (ecSaStateIntBuff,  "10ms", FRAME_10ms_ENABLED ? maxEcMem.SaStateSize  : 1, ecSaStateIntPtrs);  
    create3Arrays (ecDaStateIntBuff,  "10ms", FRAME_10ms_ENABLED ? maxEcMem.DaStateSize  : 1, ecDaStateIntPtrs);      
    create3Arrays (ecEchoPathIntBuff, "10ms", FRAME_10ms_ENABLED ? maxEcMem.EchoPathSize : 1, ecEchoPathIntPtrs);      
    create3Arrays (ecBgEpIntBuff,     "10ms", FRAME_10ms_ENABLED ? maxEcMem.BackGroundEpSize  : 1, ecBgEchoPathIntPtrs);  

    fprintf (fp,"\n// --------  20 millisecond buffers -------------\n");
    create3Arrays (coderIntBuff, "20ms", FRAME_20ms_ENABLED ? maxCoderSize : 1, coderIntPtrs);
    create3Arrays (ecChanIntBuff,     "20ms", FRAME_20ms_ENABLED ? maxEcMem.InstanceSize : 1, ecChanIntPtrs);  
    create3Arrays (ecSaStateIntBuff,  "20ms", FRAME_20ms_ENABLED ? maxEcMem.SaStateSize  : 1, ecSaStateIntPtrs);  
    create3Arrays (ecDaStateIntBuff,  "20ms", FRAME_20ms_ENABLED ? maxEcMem.DaStateSize  : 1, ecDaStateIntPtrs);      
    create3Arrays (ecEchoPathIntBuff, "20ms", FRAME_20ms_ENABLED ? maxEcMem.EchoPathSize : 1, ecEchoPathIntPtrs);      
    create3Arrays (ecBgEpIntBuff,     "20ms", FRAME_20ms_ENABLED ? maxEcMem.BackGroundEpSize  : 1, ecBgEchoPathIntPtrs);  

    fprintf (fp,"\n// --------  30 millisecond buffers -------------\n");
    create3Arrays (coderIntBuff, "30ms", FRAME_30ms_ENABLED ? maxCoderSize : 1, coderIntPtrs);
    create3Arrays (ecChanIntBuff,     "30ms", FRAME_30ms_ENABLED ? maxEcMem.InstanceSize : 1, ecChanIntPtrs);  
    create3Arrays (ecSaStateIntBuff,  "30ms", FRAME_30ms_ENABLED ? maxEcMem.SaStateSize  : 1, ecSaStateIntPtrs);  
    create3Arrays (ecDaStateIntBuff,  "30ms", FRAME_30ms_ENABLED ? maxEcMem.DaStateSize  : 1, ecDaStateIntPtrs);      
    create3Arrays (ecEchoPathIntBuff, "30ms", FRAME_30ms_ENABLED ? maxEcMem.EchoPathSize : 1, ecEchoPathIntPtrs);      
    create3Arrays (ecBgEpIntBuff,     "30ms", FRAME_30ms_ENABLED ? maxEcMem.BackGroundEpSize  : 1, ecBgEchoPathIntPtrs);  

#endif
}

#define MAX_FRAME_SIZE 320
#define MAX_AEC_TAIL   128

//=============================================================================
//            AEC CONFIGURATION    <project>.c
//=============================================================================
IAECG4_Params AECConfig = {
    sizeof(IAECG4_Params), // size
    MAX_FRAME_SIZE,        // frameSize
    1,                     // backgroundMode
    8000,                  // samplingRate
    4000,                  // maxAudioFreq
    MAX_FRAME_SIZE*2,      // bulkDelaySamples
    MAX_AEC_TAIL/2,        // activeTailLengthMSec
    MAX_AEC_TAIL,          // totalTailLengthMSec
    20,                    // maxTxNLPThresholddB
    60,                    // maxTxLossdB
    0,                     // maxRxLossdB
    -85,                   // targetResidualLeveldBm
    -90,                   // maxRxNoiseLeveldBm
    -12,                   // worstExpectedERLdB
     3,                    // rxSaturateLeveldBm
    1,                     // noiseReductionSetting
    Enabled,               // cngEnable
    0,                     // fixedGaindB10
    Disabled,              // agcEnable
    0,                     // agcMaxGaindB
    0,                     // agcMaxLossdB
    -10,                   // agcTargetLeveldBm
    -40,                   // agcLowSigThreshdBm
    0,                     // maxTrainingTimeMSec
    -30,                   // trainingRxNoiseLeveldBm
    NULL                   // pTxEqualizerdB10
};
//extern Int StaticAllocHelper (const IALG_Params *AECG4Params, IALG_Fxns **fxns, IALG_MemRec memTab[]);

static void WriteAECData (FILE *fp) {
   int i, sectionI8, AECInstI8;
   IALG_MemRec memTab[MTAB_NRECS];
   AECInstI8 = 0;

   fprintf (fp,"\n\n/*=================================*/\n");
   fprintf (fp,    "//{ Acoustic canceller declarations\n");

   if (SysCfg.AECInstances == 0) {
      // Allocate dummy externals to satisfy references
      fprintf (fp,"\nADT_Int16 AECInUse;\n");
      fprintf (fp,  "ADT_Int16 AECHandles;\n\n");
      fprintf (fp,  "ADT_Int16 AECConfig;\n");
      fprintf (fp,  "ADT_Int16 AECMemTab;\n");
      fprintf (fp,  "ADT_Int16 AECChan;\n");
      fprintf (fp,  "ADT_Int16 pAECFarPool;\n");
      fprintf (fp, "const int AEC_Inst_I8 = 0;\n");
      fprintf (fp,"#pragma DATA_SECTION (AECInUse,    \"STUB_DATA_SECT\")\n");
      fprintf (fp,"#pragma DATA_SECTION (AECHandles,  \"STUB_DATA_SECT\")\n");
      fprintf (fp,"#pragma DATA_SECTION (AECConfig,   \"STUB_DATA_SECT\")\n");
      fprintf (fp,"#pragma DATA_SECTION (AECMemTab,   \"STUB_DATA_SECT\")\n");
      fprintf (fp,"#pragma DATA_SECTION (AECChan,     \"STUB_DATA_SECT\")\n");
      fprintf (fp,"#pragma DATA_SECTION (pAECFarPool, \"STUB_DATA_SECT\")\n\n");

      fprintf (fp,"ADT_Int16 micEqChan;\n");
      fprintf (fp,"ADT_Int16 micEqState;\n");
      fprintf (fp,"#pragma DATA_SECTION (micEqChan,  \"STUB_DATA_SECT\")\n");
      fprintf (fp,"#pragma DATA_SECTION (micEqState, \"STUB_DATA_SECT\")\n\n");

      fprintf (fp,"ADT_Int16 spkrEqChan;\n");
      fprintf (fp,"ADT_Int16 spkrEqState;\n");
      fprintf (fp,"#pragma DATA_SECTION (spkrEqChan,  \"STUB_DATA_SECT\")\n");
      fprintf (fp,"#pragma DATA_SECTION (spkrEqState, \"STUB_DATA_SECT\")\n");
      fprintf (fp,    "//}");
      return;
   }
#if 0 // jdc AEC not working!

{
   fprintf (fp,"IAECG4_Params AECConfig = {\n");
   fprintf (fp,"    sizeof(IAECG4_Params),  // size\n");
   fprintf (fp,"    MAX_FRAME_SIZE,         // frameSize\n");
   fprintf (fp,"    1,                      // backgroundMode\n");
   fprintf (fp,"    8000,                   // samplingRate\n");
   fprintf (fp,"    4000,                   // maxAudioFreq\n");
   fprintf (fp,"    MAX_FRAME_SIZE*2,       // bulkDelaySamples\n");
   fprintf (fp,"    MAX_AEC_TAIL/2,         // activeTailLengthMSec\n");
   fprintf (fp,"    MAX_AEC_TAIL,           // totalTailLengthMSec\n");
   fprintf (fp,"    20,                     // maxTxNLPThresholddB\n");
   fprintf (fp,"    60,                     // maxTxLossdB\n");
   fprintf (fp,"    0,                      // maxRxLossdB\n");
   fprintf (fp,"    -85,                    // targetResidualLeveldBm\n");
   fprintf (fp,"    -90,                    // maxRxNoiseLeveldBm\n");
   fprintf (fp,"    -12,                    // worstExpectedERLdB\n");
   fprintf (fp,"     3,                     // rxSaturateLeveldBm\n");
   fprintf (fp,"    1,                      // noiseReductionSetting\n");
   fprintf (fp,"    Enabled,                // cngEnable\n");
   fprintf (fp,"    0,                      // fixedGaindB10\n");
   fprintf (fp,"    Disabled,               // agcEnable\n");
   fprintf (fp,"    0,                      // agcMaxGaindB\n");
   fprintf (fp,"    0,                      // agcMaxLossdB\n");
   fprintf (fp,"    -10,                    // agcTargetLeveldBm\n");
   fprintf (fp,"    -40,                    // agcLowSigThreshdBm\n");
   fprintf (fp,"    0,                      // maxTrainingTimeMSec\n");
   fprintf (fp,"    -30,                    // trainingRxNoiseLeveldBm\n");
   fprintf (fp,"    NULL                   // pTxEqualizerdB10\n");
   fprintf (fp,"};\n\n");
}
   AECConfig.frameSize            = MaxFrameSize;
   AECConfig.bulkDelaySamples     = MaxFrameSize * 2;
   AECConfig.samplingRate         = SysCfg.samplesPerMs * 1000;
   AECConfig.maxAudioFreq         = SysCfg.samplesPerMs * 500;
   AECConfig.activeTailLengthMSec = (SysCfg.AECTailLen / 2) & (~7);    // Active tail must be multiple of 8 msec.
   AECConfig.totalTailLengthMSec  = SysCfg.AECTailLen;

   AECG4_ADT_alloc ((const IALG_Params *) &AECConfig, NULL, memTab);

   fprintf (fp,"\nIALG_MemRec AECMemTab[] = {\n");
   

   for (i = 0; i < MTAB_NRECS; i++) {
      sectionI8 = (memTab[i].size + 7) & 0xfffffff8;
      fprintf (fp," {\t%5d, 8, IALG_SARAM, IALG_PERSIST, NULL},\n", sectionI8);
      AECInstI8 += sectionI8;
   }
   fprintf (fp," {\t    0, 8, IALG_SARAM, IALG_PERSIST, NULL},\n");
   fprintf (fp,"};\n\n");


   if ((SysCfg.samplesPerMs == 16) || WBCodecEnabled)
      fprintf (fp,"ADT_UInt32 AECFarBuff[MAX_AEC_ECANS][MAX_FRAME_SIZE];\n");
   else
      fprintf (fp,"ADT_UInt32 AECFarBuff[MAX_AEC_ECANS][MAX_FRAME_SIZE/2];\n");

   fprintf (fp,"ADT_UInt32 AECInUse[MAX_AEC_ECANS];\n");
   fprintf (fp,"#pragma DATA_SECTION (AECInUse, \"POOL_ALLOC\")\n");

   fprintf (fp,"IAECG4_Handle AECHandles[MAX_AEC_ECANS];\n");

   fprintf (fp, "#define AEC_INST_I8 %u\n", AECInstI8);
   fprintf (fp, "const int AEC_Inst_I8 = AEC_INST_I8;\n");
   for (i=0; i<SysCfg.AECInstances; i++) {
      fprintf (fp,"ADT_Int64  AECChan%d[AEC_INST_I8/8];\n", i);
      fprintf (fp,"#pragma DATA_SECTION (AECChan%d,      \"CHAN_INST_DATA:AEC\")\n",i);

   }

   fprintf (fp,"ADT_Int64* const AECChan[] = {\n");
   for (i=0; i<SysCfg.AECInstances; i++)
      fprintf (fp,"   AECChan%d,\n",i);
   fprintf (fp,"};\n");

   fprintf (fp,"ADT_UInt16 * const pAECFarPool[] = {\n");
   for (i=0; i<SysCfg.AECInstances; i++)
      fprintf (fp,"    (ADT_UInt16 *) AECFarBuff[%d],\n",i);
   fprintf (fp,"};\n");

   fprintf (fp, "ADT_UInt8 aecStateBufr[1];\n");
   fprintf (fp, "ADT_UInt32 aecStateBufrSize = 1;\n");

   fprintf (fp,"// AEC equalizers\n");
   fprintf (fp,"ADT_Int16   micEqState [MIC_EQ_STATE_I16];\n");
   if (SysCfg.micEqLength) {
      fprintf (fp,"#pragma DATA_SECTION (micEqState, \"RECOMMENDED_FAST_DATA_SECT\")\n\n");
      fprintf (fp,"EqChannel_t  micEqChan[MAX_AEC_ECANS];\n");
      fprintf (fp,"#pragma DATA_SECTION (micEqChan, \"CHAN_INST_DATA:AEC\")\n");
   } else {
      fprintf (fp,"#pragma DATA_SECTION (micEqState, \"STUB_DATA_SECT\")\n\n");
      fprintf (fp,"EqChannel_t  micEqChan[1];\n");
      fprintf (fp,"#pragma DATA_SECTION (micEqChan, \"STUB_DATA_SECT\")\n\n");
   }

   fprintf (fp,"ADT_Int16   spkrEqState[SPKR_EQ_STATE_I16];\n");
   if (SysCfg.spkrEqLength) {
      fprintf (fp,"#pragma DATA_SECTION (spkrEqState,  \"RECOMMENDED_FAST_DATA_SECT\")\n");
      fprintf (fp,"EqChannel_t  spkrEqChan[MAX_AEC_ECANS];\n");
      fprintf (fp,"#pragma DATA_SECTION (spkrEqChan, \"CHAN_INST_DATA:AEC\")\n");
   } else {
      fprintf (fp,"#pragma DATA_SECTION (spkrEqState,  \"STUB_DATA_SECT\")\n");
      fprintf (fp,"EqChannel_t  spkrEqChan[1];\n");
      fprintf (fp,"#pragma DATA_SECTION (spkrEqChan, \"STUB_DATA_SECT\")\n");
   }
   fprintf (fp,    "//}");
#endif

}

#ifdef DMA_QUEUE_ALLOCATION

void AlignInternalCodecs (FILE *fp, Align *align) {

    int instAlign;
    unsigned i;

    instAlign = MAX (encodeMemory.align, decodeMemory.align);

    if (ExtDmaActive)  {
        fprintf (fp, "\n/* Internal Memory Coder Queue buffers */\n");

        for (i = 1; i <= internalQueueCnt; i++) {
           fprintf (fp, "  coderIntBuff0_q%d : {} ALIGN(%d) %s\n", i, instAlign, InternalMem);
           fprintf (fp, "  coderIntBuff1_q%d : {} ALIGN(%d) %s\n", i, instAlign, InternalMem);
           fprintf (fp, "  coderIntBuff2_q%d : {} ALIGN(%d) %s\n", i, instAlign, InternalMem);
        }
        for (; i <= 3; i++) {
           fprintf (fp, "  coderIntBuff0_q%d : {} ALIGN(%d) %s\n", i, 8, ExtDmaArea);
           fprintf (fp, "  coderIntBuff1_q%d : {} ALIGN(%d) %s\n", i, 8, ExtDmaArea);
           fprintf (fp, "  coderIntBuff2_q%d : {} ALIGN(%d) %s\n", i, 8, ExtDmaArea);
        }
    
    }
}
void AlignEchoCancellerQueue (FILE *fp, EcAlign *align) {

    fprintf (fp, "\n/* Internal Memory EC Queue buffers */\n");
    

    alignECInternalBuffers (fp, align, (1 <= internalQueueCnt) ? InternalMem:ExtDmaArea, "q1");  
    alignECInternalBuffers (fp, align, (2 <= internalQueueCnt) ? InternalMem:ExtDmaArea, "q2");  
    alignECInternalBuffers (fp, align, (3 <= internalQueueCnt) ? InternalMem:ExtDmaArea, "q3");  

}

#else

void AlignInternalCodecs (FILE *fp, Align *align) {

    int instAlign;

    instAlign = MAX (align->daramChan, align->saramChan);

    if (ExtDmaActive)  {
        fprintf (fp, "\n/* Internal Memory Coder Queue buffers */\n");

        if (FRAME_10ms_ENABLED) MemArea10Ms = InternalMem;
        else                    MemArea10Ms = ExtDmaArea;

        fprintf (fp, "  coderIntBuff0_10ms : {} ALIGN(%d) %s\n", instAlign, MemArea10Ms);
        fprintf (fp, "  coderIntBuff1_10ms : {} ALIGN(%d) %s\n", instAlign, MemArea10Ms);
        fprintf (fp, "  coderIntBuff2_10ms : {} ALIGN(%d) %s\n", instAlign, MemArea10Ms);

        if (FRAME_20ms_ENABLED) MemArea20Ms = InternalMem;
        else                    MemArea20Ms = ExtDmaArea;

        fprintf (fp, "  coderIntBuff0_20ms : {} ALIGN(%d) %s\n", instAlign, MemArea20Ms);
        fprintf (fp, "  coderIntBuff1_20ms : {} ALIGN(%d) %s\n", instAlign, MemArea20Ms);
        fprintf (fp, "  coderIntBuff2_20ms : {} ALIGN(%d) %s\n", instAlign, MemArea20Ms);


        if (FRAME_30ms_ENABLED) MemArea30Ms = InternalMem;
        else                    MemArea30Ms = ExtDmaArea;

        fprintf (fp, "  coderIntBuff0_30ms : {} ALIGN(%d) %s\n", instAlign, MemArea30Ms);
        fprintf (fp, "  coderIntBuff1_30ms : {} ALIGN(%d) %s\n", instAlign, MemArea30Ms);
        fprintf (fp, "  coderIntBuff2_30ms : {} ALIGN(%d) %s\n", instAlign, MemArea30Ms);

    }
}

void AlignEchoCancellerQueue (FILE *fp, EcAlign *align) {

    fprintf (fp, "\n/* Internal Memory EC Queue buffers */\n");
    alignECInternalBuffers (fp, align, MemArea10Ms, "10ms");  
    alignECInternalBuffers (fp, align, MemArea20Ms, "20ms");  
    alignECInternalBuffers (fp, align, MemArea30Ms, "30ms");  
}

#endif

//=============================================================================
//                <project>.C   Main
//=============================================================================
static BOOL createGlobalCFile (blockStatus_t *bs, unsigned int numChannels, 
                               ecConfig_t *ecCfg, Align *align, char **pErrText, 
                               ExternalMemInfo_t *pExtMem, char *Root) {

   char GlobalFileName[100];
   char *RootFile;

   FILE *fp;
   char *str1, *str2, *str3, *str4;
   ecMemSpec_t pcmEcMem, pktEcMem;


   VocoderInfo_t     *Vocoder;

   memset (&pcmEcMem, 0, sizeof (pcmEcMem));
   memset (&pktEcMem, 0, sizeof (pcmEcMem));

   *pErrText = "?";

   // Open the Global Data file.
   sprintf (GlobalFileName, "%s.c", Root);
   fp = fopen (GlobalFileName,"w");
   if (fp == 0)   {
      *pErrText = "Unable to create memory allocation file !";
      return (FALSE);
   }

   //------------------------------------------------------------
   //  Configuration comments
   //------------------------------------------------------------
   fprintf (fp,"/*-------------------------------------------------------------------\n");
   fprintf (fp,"   This memory allocation supports the following configuration:\n\n");


   for (Vocoder = Vocoders; Vocoder->name != NULL; Vocoder++) {
      if (Vocoder->Version == 0xFFFF) continue;        
      if (Vocoder->activeFile == PRODUCTION)
         str1 = ENABLED_TEXT;
      else if (Vocoder->activeFile == DEMO)
         str1 = DEMO_TEXT;
      else                   
         str1 = DISABLED_TEXT;

      fprintf (fp,"\t%-30s [%4x]\t\t\t= %s\n", Vocoder->name, Vocoder->Version, str1);
   }
   fprintf (fp,"\n");


   writeComponentStatus (fp);

   fprintf (fp,"\n\nFrame sizes (ms): ");
   WriteFrameBit (8);    WriteFrameBit (20);    WriteFrameBit (40);
   WriteFrameBit (80);   WriteFrameBit (160);   WriteFrameBit (180);
   WriteFrameBit (240);


   fprintf (fp,"\nChannel types: ");
   WriteChannelTypeBit (PCM2PCM);    WriteChannelTypeBit (PCM2PKT);
   WriteChannelTypeBit (PKT2PKT);    WriteChannelTypeBit (CIRCDATA);
   WriteChannelTypeBit (CONFPCM);    WriteChannelTypeBit (CONFPKT);
   WriteChannelTypeBit (CONFCOMP);
   if (SysCfg.maxNumLbCoders > 0)
     fprintf (fp,"\tLB_CODER");

   if (pExtMem->type == EXT_NONE)
     str1 = "unused";
   else if (pExtMem->type == EXT_DMA)
     str1 = "used for algorithm DMA";  
   else 
     str1 = "used with cache";

   fprintf (fp,"\nExternal memory is %s\n", str1);


   //------------------------------------------------------------
   //   Versions
   //------------------------------------------------------------
   fprintf (fp,"\n-------------------------------------------------------------------*/\n\n");
   RootFile = strrchr (Root, '\\');
   if (RootFile) RootFile++;
   else          RootFile = Root;
   fprintf (fp, "#include \"%s.h\"\n", RootFile);

   fprintf (fp,"\n\n\n/*======================================= */\n");
   fprintf (fp,"/*  SW Versions of installed components */\n");


   // Vocoder software versions 
   for (Vocoder = Vocoders; Vocoder->name != NULL; Vocoder++) {
      if (! (Vocoder->cfgMask & SysCfg.Codecs.LongWord)) continue;

      fprintf (fp,"const unsigned int %s_SWVER=    0x%04X;\n", Vocoder->name, Vocoder->Version);
   }
   writeComponentVersions (fp);


#ifdef COMBINED_SCRATCH
   if (SysCfg.maxToneDetTypes != 0)   fprintf (fp, "typedef TDScratch_t GPAK_TD_Scratch;\n");
   else                             fprintf (fp, "typedef int         GPAK_TD_Scratch;\n");

   if (SysCfg.maxNumConferences != 0) fprintf (fp, "typedef ConfScratch_t GPAK_Cnfr_Scratch;\n");
   else                             fprintf (fp, "typedef int           GPAK_Cnfr_Scratch;\n");
#endif

   //------------------------------------------------------------
   //   Structure allocation
   //------------------------------------------------------------
   fprintf (fp,"\n\n\n/*=============================== */\n");
   fprintf (fp,"// System configuration structure \n\n");
   writeSystemConfiguration (fp);

   fprintf (fp,"\n\n\n/*=============================== */\n");
   fprintf (fp,"// Echo canceller configuration structures \n\n");
   WriteEchoCancellerParams (fp, "PCM", &ecCfg->g168Pcm);
   WriteEchoCancellerParams (fp, "PKT", &ecCfg->g168Pkt);

   fprintf (fp,"\n\n\n/*=============================== */\n");
   fprintf (fp, "// Framing task buffers. \n");
   WriteFramingStructures (fp);

   fprintf (fp,"\n\n\n/*=============================== */\n");
   fprintf (fp, "// Conference configuration data. \n\n");
   WriteConferenceStructures (fp);

   fprintf (fp,"\n\n\n/*=============================== */\n");
   fprintf (fp, "// Background transcoder structures\n\n");
   WriteLbStructures (fp);

   fprintf (fp,"\n\n\n/*======================================= */\n");
   fprintf (fp,"//      Event/DMA/Pkt buffer/Channel/En-DeCoder allocation         \n");
   writeBufferAllocation (fp, bs, align, numChannels);

   fprintf (fp,"\n\n\n/*=============================== */\n");
   fprintf (fp, "// Echo cancellation structures. \n\n");
   WriteEchoCancellerStructures (ecCfg, align, fp, MaxFrameSize, &pcmEcMem, &pktEcMem);

   fprintf (fp,"\n\n\n/*=============================== */\n");
   fprintf (fp, "// Tone detect, detect relay, generate and generate relay data structures.\n");
   WriteToneStructures (fp);

   fprintf (fp,"\n\n\n/*=============================== */\n");
   fprintf (fp, "// VAD/CNG and AGC data structures.\n");
   WriteVadAgcStructures (fp);

   fprintf (fp,"\n\n\n/*=============================== */\n");
   fprintf (fp, "// Sampling rate converter data structures.\n");
   WriteSrcStructures (fp);

   fprintf (fp,"\n\n\n/*=============================== */\n");
   fprintf (fp, "// Noise Suppression Structures. \n\n");
   WriteNoiseStructures (fp, numChannels);

   fprintf (fp,"\n\n\n/*=============================== */\n");
   fprintf (fp, "// T.38 Fax relay Structures. \n\n");
   if (NumT38Instances != 0) {
      WriteT38Structures (fp);
   } else { 
      WriteTIT38Structures (fp);
   }


#ifdef CID_ALLOCATION
   fprintf (fp,"\n\n\n/*=============================== */\n");
   fprintf (fp, "// Caller Id data structures.\n");
   WriteCidStructures (fp);
#endif

#ifdef RTP_ALLOCATION
   fprintf (fp,"\n\n\n/*=============================== */\n");
   fprintf (fp, "// RTP and jitter buffer data structures.\n");
   WriteRTPStructures (fp);
   fprintf (fp, "// SRTP data structures.\n");
   WriteSRTPStructures (fp);
   fprintf (fp, "// RTCP data structures.\n");
   WriteRTCPStructures (fp);
#endif

   fprintf (fp,"\n\n\n/*=============================== */\n");
   fprintf (fp, "//    Component dependent structures \n");

   // G.726 Data
   str1 = str2 = str3 = str4 = "NULL";
   if (!IsC64) {
     fprintf (fp,"\n\n// G726 Global buffers \n");
     fprintf (fp,"ADT_Int16 *G726QuanTables[4] = {%s, %s, %s, %s};\n",str1,str2,str3,str4);
   }

   if ((cdcBits.g726LowMipsEnable) && (!IsC64))  {
      if (cdcBits.g726Rate16Enable)  {
         fprintf (fp,"ADT_Int16 Table16[8192];\n");
         str1 = "Table16";
      }  

      if (cdcBits.g726Rate24Enable) {
         fprintf (fp,"ADT_Int16 Table24[8192];\n");
         str2 = "Table24";
      }  

      if (cdcBits.g726Rate32Enable) {
         fprintf (fp,"ADT_Int16 Table32[8192];\n");
         str3 = "Table32";
      }  

      if (cdcBits.g726Rate40Enable) {
         fprintf (fp,"ADT_Int16 Table40[8192];\n");
         str4 = "Table40";
      }  
   } 

#ifdef EXT_DMA_TRANSFERS
   fprintf (fp,"\n\n\n/*=============================== */\n");
   fprintf (fp, "// External Memory DMA Structures. \n\n");
   fprintf (fp,"const ADT_UInt16 numAtccBlocks = %d;\n\n", numAtccBlocks);
   WriteDMAStructures (bs, &pcmEcMem, &pktEcMem, pExtMem, fp);
#endif

   fprintf (fp,"\n\n\n/*=============================== */\n");
   fprintf (fp, "// Playback/record memory location. \n\n");
   fprintf (fp, "#pragma DATA_SECTION (playRec, \"PLAYREC\")\n");
   fprintf (fp, "ADT_UInt16 playRec;\n\n");

#ifdef MULTI_CORE
   fprintf (fp,"\n\n\n/*=============================== */\n");
   fprintf (fp, "// Multi-core structures.\n");
   WriteMultiCoreStructures (fp);
#endif
   fprintf (fp,"\n");

   fclose (fp);
   return TRUE;
}

//}===================================================================
//                         Linker Command custom file  "<project>Cust.cmd" . 
//{
//===================================================================

void sectionStart (FILE *fp, char *Section) {
   // Add _Start$xxx and _End$xxx variables for delimiting start and end address of names sections for multicore systems
   fprintf (fp, "\n  %-10s: ", Section);
   if (DspInfo->DspType == Ti647) {
      if (*Section == '.') Section++;
      fprintf (fp, "RUN_START(_Start$%s), RUN_END (_End$%s) {", Section, Section);
   } else if (DspInfo->DspType == Ti667) {
      if (*Section == '.') Section++;
      fprintf (fp, "RUN_START(_Start$%s), RUN_END (_End$%s) {", Section, Section);
   } else {
      fprintf (fp, "{");
   }
}
void groupStart (FILE *fp, char *Group) {
   // Add _Start$xxx and _End$xxx variables for delimiting start and end address of names sections for multicore systems
   fprintf (fp, "\n  GROUP (%s) : ", Group);
   if (DspInfo->DspType == Ti647) {
      fprintf (fp, "RUN_START(_Start$%s), RUN_END (_End$%s) {", Group, Group);
   } else if (DspInfo->DspType == Ti667) {
      fprintf (fp, "RUN_START(_Start$%s), RUN_END (_End$%s) {", Group, Group);
   } else {
      fprintf (fp, "{");
   }
}

static BOOL createCustCmdFile (FILE *fpCust, Align *align, int BufrAlign, ExternalMemInfo_t *pExtMem) {
    
   int  j;
   char *fastInst, *slowInst;
   VocoderInfo_t   *Vocoder;

   if (pExtMem->type != EXT_NONE) {
      fastInst = ExtDmaArea;
   } else {
      if (DspInfo->DspType == Ti647)
         fastInst = FastDataFirst;
      else if (DspInfo->DspType == Ti667)
         fastInst = FastDataFirst;
      else
         fastInst = InternalMem;     
   }
   if (fastInst[1] == '>') fastInst++;
   

    if (pExtMem->type == EXT_NONE) {
       slowInst = SlowDataFirst;
    } else {
       slowInst = ExtDmaArea;
    }

   fprintf (fpCust, "/* G.PAK Config Tool generated command file. */\n\n");

   fprintf (fpCust, "SECTIONS\n{\n");

   if (pExtMem->type == EXT_CACHE && !IsC64Plus) { 
      //---------------------
      // CACHE RESERVATION
      fprintf (fpCust, "\n/* ----------------------- */\n");
      fprintf (fpCust,   "/* Cache reservation */\n");
      fprintf (fpCust, "  .cache: {\n");
      fprintf (fpCust, "     GBL_cacheStart = .;\n");
      fprintf (fpCust, "     . += 0x8000;\n");
      fprintf (fpCust, "     GBL_cacheEnd = .;\n");
      fprintf (fpCust, "  } > CACHE_L2\n\n");
   }
   if (SysCfg.DspType == Ti647) {
      fprintf (fpCust, "\n/* ----------------------- */\n");
      fprintf (fpCust,   "/* NOTE:  CCS debugger does not remove CIO breakpoint when new image is loaded   */\n");
      fprintf (fpCust,   "/*        Fix address so old CIO breakpoint does not corrupt memory at old CIO location */\n");
      fprintf (fpCust, "  CIO: {\n");
      fprintf (fpCust, "     *(.text:_writemsg)\n");
      fprintf (fpCust, "  } > Core5\n\n");
   }   


   if (IsC55) {
      fprintf (fpCust,   "  BootSect :  ALIGN(2) > 0x10000 /* branch from 0x10000 byte address */\n\n");
   }

   if (encodeMemory.type == DARAM) fprintf (fpCust, "  ENCODER_INST_DATA : {} %s\n", fastInst);
   if (decodeMemory.type == DARAM) fprintf (fpCust, "  DECODER_INST_DATA : {} %s\n", fastInst);

   fprintf (fpCust, "\n\n/* ---------------------------------------- */\n");
   fprintf (fpCust,   "/* -------  Algorithm fast access  ------- */\n");

   //---------------------
   // T.38
   if (0 < NumT38Instances) {
      fprintf (fpCust, "\n  vtext : {} %s\n", FastProg);
   }


    //-----------------------------------------------
    // Vocoder SECTIONS requiring fast access
    for (Vocoder=Vocoders; Vocoder->name != NULL; Vocoder++) {
       SectInfo_t *sect;

       if (! (Vocoder->cfgMask & SysCfg.Codecs.LongWord)) continue;

       sect = Vocoder->fastMem;
       if (sect->memName == NULL) continue;

       fprintf (fpCust, "\n/* %s fast memory requirements. */\n", Vocoder->name);
       do {
           fprintf (fpCust, "  %s   %s\n", sect->Name, *sect->memName);
           sect++;
       } while (sect->memName != NULL); 
    }

   //---------------------
   // T38 Instance data 
   if (NumT38Instances != 0)
      T38DataAlign (fpCust, FastDataFirst);

   //-----------------------------------------------
   //---- Voice quality components memory section descriptors
   writeComponentSectionDescriptors (fpCust);

   EchoCancellerFastSectionAssign (fpCust, "Pcm", SysCfg.numPcmEcans, &align->Pcm, algBits.g168VarAEnable);
   EchoCancellerFastSectionAssign (fpCust, "Pkt", SysCfg.numPktEcans, &align->Pkt, algBits.g168VarAEnable);

   //----------------------------
   //  Algorithm instance data
   fprintf (fpCust, "\n\n/* Uninitialized fast memory */\n");
   
   
    //-----------------------------------------------
    // FAST INSTANCE DATA
   fprintf (fpCust, "  /* Cacheable instance memory */\n");
   fprintf (fpCust, "  FAST_INSTANCE_DATA: {\n");
   {
      if (DspInfo->DspType == Ti647) {  // Per core memory on 647
         if (ARBDetInstances != 0) fprintf (fpCust, "\t\t*(TONE_INST_DATA)\n");
      }
      else if (DspInfo->DspType == Ti667) {  // Per core memory on 667
         if (ARBDetInstances != 0) fprintf (fpCust, "\t\t*(TONE_INST_DATA)\n");
      } else {
         fprintf (fpCust, "\t\t*(FAST_SCRATCH)\n");  
         fprintf (fpCust, "\t\t*(CHAN_INST_DATA:Chan)\n");
      }
      if (SysCfg.AECInstances)         fprintf (fpCust, "\t\t*(CHAN_INST_DATA:AEC)\n");
      if (SysCfg.numAGCChans)          fprintf (fpCust, "\t\t*(CHAN_INST_DATA:AGC)\n");
      if (algBits.vadCngEnable)        fprintf (fpCust, "\t\t*(CHAN_INST_DATA:VAD)\n");
      if (SysCfg.SRTP_Enable) fprintf (fpCust, "\t\t*(CHAN_INST_DATA:SRTP)\n");
      if ((SysCfg.maxToneDetTypes + CEDDetInstances + CNGDetInstances + ARBDetInstances)!= 0) 
                                       fprintf (fpCust, "\t\t*(CHAN_INST_DATA:ToneDetect)\n");
      if (SysCfg.numTGChans)           fprintf (fpCust, "\t\t*(CHAN_INST_DATA:ToneGen)\n");
      if (algBits.dtmfDialEnable)      fprintf (fpCust, "\t\t*(CHAN_INST_DATA:DtmfDial)\n");
      if (algBits.noiseSuppressEnable) fprintf (fpCust, "\t\t*(CHAN_INST_DATA:NoiseSuppression)\n");
      if (SysCfg.numRxCidChans)        fprintf (fpCust, "\t\t*(CHAN_INST_DATA:RxCID)\n");
      if (SysCfg.numTxCidChans)        fprintf (fpCust, "\t\t*(CHAN_INST_DATA:TxCID)\n");
      if ((algBits.rtpNetDelivery || algBits.rtpHostDelivery)) {
                                       fprintf (fpCust, "\t\t*(CHAN_INST_DATA:RTP)\n");
                                       fprintf (fpCust, "\t\t*(CHAN_INST_DATA:RTCP)\n");
      }
      if (SysCfg.numSrcChans) {
         fprintf (fpCust, "\t\t*(CHAN_INST_DATA:SRC_DOWN)\n");
         fprintf (fpCust, "\t\t*(CHAN_INST_DATA:SRC_UP)\n");
      }

      if ((DspInfo->DspType != Ti667)  && (DspInfo->DspType != Ti665)) {
         fprintf (fpCust, "\t\t*(CHAN_INST_DATA)\n");
      }

      if (SysCfg.maxNumConferences) {
         fprintf (fpCust, "\t\t*(CHAN_INST_DATA:Conference)\n");
         fprintf (fpCust, "\t\t*(CONF_INST_DATA:Conference)\n");
         fprintf (fpCust, "\t\t*(CONF_INST_DATA:AGC)\n");
         fprintf (fpCust, "\t\t*(CONF_INST_DATA:ToneGen)\n");
         fprintf (fpCust, "\t\t*(CONF_INST_DATA:ToneGenParams)\n");
      }
   }
   fprintf (fpCust, "  } %s\n", InstanceData);

   //----------------------------
   // SWITCH TABLES
   sectionStart (fpCust, "SWITCH_AND_DATA_TABLES");
      fprintf (fpCust, "\n\t\t*(.switch)\n");
      if (NumT38Instances != 0) fprintf (fpCust, "\t\t*(.data)\n");
   fprintf (fpCust, "  } %s\n", FastConst);

   //-------------------- TIME SEMI-CRITICAL (fast access recommended) --------------------------------
   fprintf (fpCust, "\n\n/* ----------------------- */\n");

   //----------------------------
   // RECOMMEND FAST DATA
   fprintf (fpCust, "  RECOMMENDED_FAST_DATA_SECT: {\n");
   {
      if (SysCfg.numRxCidChans) fprintf (fpCust, "\t\t*(RX_CID_BUFF_SECT)\n");
      if (SysCfg.numTxCidChans) {
         fprintf (fpCust, "\t\t*(TX_CID_MSGBUFF_SECT)\n");
         fprintf (fpCust, "\t\t*(TX_CID_BRSTBUFF_SECT)\n");
      }

      if ((SysCfg.DspType != Ti647) && (SysCfg.DspType != Ti667))  {
         fprintf (fpCust, "\t\t*(POOL_ALLOC)      /* Should stay internal */\n");
         fprintf (fpCust, "\t\t*(pcmIn_POOL)      /* Should stay internal */\n");
         fprintf (fpCust, "\t\t*(pcmOut_POOL)     /* Should stay internal */\n");
         fprintf (fpCust, "\t\t*(SHARED_LOCK_SECT)\n");

      }
      fprintf (fpCust, "\t\t*(blkDly_POOL)\n");

#ifndef MULTI_CORE
      fprintf (fCust, "\t\t*(inWork_1msec)\n");
      fprintf (fpCust, "\t\t*(outWork_1msec)\n");
      fprintf (fpCust, "\t\t*(ECFarWork_1msec)\n\n");

      fprintf (fpCust, "\t\t*(inWork_2_5msec)\n");
      fprintf (fpCust, "\t\t*(outWork_2_5msec)\n");
      fprintf (fpCust, "\t\t*(ECFarWork_2_5msec)\n\n");

      fprintf (fpCust, "\t\t*(inWork_5msec)\n");
      fprintf (fpCust, "\t\t*(outWork_5msec)\n");
      fprintf (fpCust, "\t\t*(ECFarWork_5msec)\n\n");

      fprintf (fpCust, "\t\t*(inWork_10msec)\n");
      fprintf (fpCust, "\t\t*(outWork_10msec)\n");
      fprintf (fpCust, "\t\t*(ECFarWork_10msec)\n\n");

      fprintf (fpCust, "\t\t*(inWork_20msec)\n");
      fprintf (fpCust, "\t\t*(outWork_20msec)\n");
      fprintf (fpCust, "\t\t*(ECFarWork_20msec)\n\n");
#ifdef MELP
      fprintf (fpCust, "\t\t*(inWork_22_5msec)\n");
      fprintf (fpCust, "\t\t*(outWork_22_5msec)\n");
      fprintf (fpCust, "\t\t*(ECFarWork_22_5msec)\n\n");
#endif
      fprintf (fpCust, "\t\t*(inWork_30msec)\n");
      fprintf (fpCust, "\t\t*(outWork_30msec)\n");
      fprintf (fpCust, "\t\t*(ECFarWork_30msec)\n\n");
#endif

      if (!HOST_RTP) {
         if ((DspInfo->DspType != Ti647) && (DspInfo->DspType != Ti667)) {
            fprintf (fpCust, "\t\t*(pktIn_POOL)\n");
            fprintf (fpCust, "\t\t*(pktOut_POOL)\n");
            fprintf (fpCust, "\t\t*(pkt_CIRC)\n");
         }
      }
   }
   fprintf (fpCust, "  } %s\n\n", FastDataFirst);


   //-------------------- TIME SEMI-CRITICAL (Medium access memory) --------------------------------
   // MEDIUM SPEED PROGRAM SECTIONS
   fprintf (fpCust, "\n\n\n/* ---------------------------------------- */\n");
   fprintf (fpCust,        "/* -------  Program medium access  ------- */\n");
   groupStart (fpCust, "MEDIUM_PROG");
   fprintf (fpCust,"\n\t\tMEDIUM_PROG_SECT\n");
   fprintf (fpCust,"  } %s\n", MediumProg);

   groupStart (fpCust, "PKT_PROG");
   fprintf (fpCust,"\n\t\tPKT_PROG_SECT\n");
   fprintf (fpCust,"  } %s\n", MediumProg);

   groupStart (fpCust, "TONE_PROG");
   fprintf (fpCust,"\n\t\tTONE_PROG_SECT\n");
   fprintf (fpCust,"  } %s\n", MediumProg);

   groupStart (fpCust, "FAX_PROG");
   fprintf (fpCust,"\n\t\tFAX_PROG_SECT\n");
   fprintf (fpCust,"  } %s\n", MediumProg);

   //-------------------- TIME NOT CRITICAL (Slow memory) --------------------------------

   // Create per channel aligned SARAM sections.
   fprintf (fpCust, "\n\n\n/* ---------------------------------------- */\n");
   fprintf (fpCust,        "/* -------  Algorithm slow access  ------- */\n");

   if (encodeMemory.type == SARAM)  fprintf (fpCust, "  ENCODER_INST_DATA : {} %s\n", slowInst);
   if (decodeMemory.type == SARAM)  fprintf (fpCust, "  DECODER_INST_DATA : {} %s\n", slowInst);

    //-----------------------------------------------
    //  Define Vocoder SECTIONS allowed slow memory access
    for (Vocoder=Vocoders; Vocoder->name != NULL; Vocoder++) {
       SectInfo_t *sect;

       if (! (Vocoder->cfgMask & SysCfg.Codecs.LongWord)) continue;

       sect = Vocoder->slowMem;
       if (sect == NULL) continue;
       if (sect->memName == NULL) continue;

       fprintf (fpCust, "\n/*  %s slower memory recommended. */\n", Vocoder->name);
       do {
           fprintf (fpCust, "  %s   %s\n", sect->Name, *sect->memName);
           sect++;
       } while (sect->memName != NULL); 
    }

   if (NumTIT38Instances != 0)
      TIT38DataAlign (fpCust, SlowDataFirst);

   //  Noise suppression section requirements
   if (algBits.noiseSuppressEnable) {
#ifdef MULTI_CORE
       fprintf (fpCust, "  NCAN_INST_DATA : {} %s\n", SlowDataFirst);
       (void) j;
#else
       for (j = 0; j < 2; j++)
       for (i = 0; i < SysCfg.maxNumChannels; i++)
           fprintf (fpCust, "  NCAN%u_%u : {} ALIGN(%d) %s\n", j, i,  NOISE_SUPPRESS_ALIGN, SlowDataFirst);
#endif
   }

   if (!IsC64) {
      if (SysCfg.DspType == Ti5420) fprintf (fpCust, "  _ADT_RESET : > 0xff80\n");
      fprintf (fpCust, "  .traceXX : {} %s\n", SlowData);
      fprintf (fpCust, "  .const :  {} %s\n", SlowConst);
      fprintf (fpCust, "  .text  :  {} %s\n", FastProg);
      fprintf (fpCust, "  .pinit :  {} %s\n", FastProg);
      fprintf (fpCust, "  .switch : {} %s\n", FastProg);
   }

   EchoCancellerSlowSectionAssign (fpCust, "Pcm", SysCfg.numPcmEcans, &align->Pcm, algBits.g168VarAEnable);
   EchoCancellerSlowSectionAssign (fpCust, "Pkt", SysCfg.numPktEcans, &align->Pkt, algBits.g168VarAEnable);
   fprintf (fpCust, "\n");



#ifdef MCASP_ALLOCATION
   fprintf (fpCust, "\n\n\n/* ---------------------------------------- */\n");
   fprintf (fpCust, "/* Buffers for non-configured TDM ports */\n");
   fprintf (fpCust, "  ASP_DMABUFF0T : {} ALIGN(%d) %s\n", BufrAlign, SlowDataFirst);
   fprintf (fpCust, "  ASP_DMABUFF0R : {} ALIGN(%d) %s\n", BufrAlign, SlowDataFirst);
   fprintf (fpCust, "  ASP_DMABUFF1T : {} ALIGN(%d) %s\n", BufrAlign, SlowDataFirst);
   fprintf (fpCust, "  ASP_DMABUFF1R : {} ALIGN(%d) %s\n", BufrAlign, SlowDataFirst);
#endif


   if ((DspInfo->DspType != Ti647) && (DspInfo->DspType != Ti667)) {
      fprintf (fpCust, "  SLOW_SCRATCH:  {} %s\n", SlowData);
   }

   //---------------------
   // SLOW DATA SECTIONS
   fprintf (fpCust, "\n\n/* ----------------------- */\n");
   fprintf (fpCust, "/* Uninitialized slow memory */\n");
   if (DspInfo->DspType == Ti647) {
      fprintf (fpCust,"\t.cio :            {\n");
      fprintf (fpCust,"\t\t*(.cio)\n");
      fprintf (fpCust,"\t\t. += 128;\n");
      fprintf (fpCust,"\t} ALIGN (128) %s\n", bssData);
   }
   if (DspInfo->DspType == Ti667) {
      fprintf (fpCust,"\t.cio :            {\n");
      fprintf (fpCust,"\t\t*(.cio)\n");
      fprintf (fpCust,"\t\t. += 128;\n");
      fprintf (fpCust,"\t} ALIGN (128) %s\n", bssData);
   }

   sectionStart (fpCust, "SLOW_DATA_SECTIONS");
      fprintf (fpCust, "\n\t\t*(STATS)\n");
      fprintf (fpCust,   "\t\t*(SLOW_DATA_SECT)\n");
      if ((DspInfo->DspType != Ti647) && (DspInfo->DspType != Ti667)) {
         fprintf (fpCust,   "\t\t*(STUB_DATA_SECT)\n");
         fprintf (fpCust,"\t\t*(.cio)\n");
      }
   fprintf (fpCust, "  } %s\n\n", SlowData);

#ifdef RTP_ALLOCATION
   fprintf (fpCust, "\n  JB_POOL: {\n");
   fprintf (fpCust,    "\t\t*(JB_POOL)\n");
   if (DspInfo->DspType == Ti647) 
      fprintf (fpCust, "  } %s\n\n", InstanceData);
   else if (DspInfo->DspType == Ti667) 
      fprintf (fpCust, "  } %s\n\n", InstanceData);
   else
      fprintf (fpCust, "  } %s\n\n", SlowData);
#endif

   //---------------------
   // SLOW CONSTANT SECTIONS
   groupStart (fpCust, "CONSTANTS");
      fprintf (fpCust, "\n\t\t.pinit:  {}\n");
      fprintf (fpCust, "\t\t.const:  {}\n");
      fprintf (fpCust, "\t\t.printf: {}\n");
   fprintf (fpCust, "  } %s\n", SlowConst);


   //---------------------
   // SLOW PROGRAM SECTIONS
   fprintf (fpCust, "\n\n/* ----------------------- */\n");
   fprintf (fpCust,   "/* Slow access program sections*/\n");

   groupStart (fpCust, "SLOW_PROG");
      fprintf (fpCust, "\n\t\tSLOW_PROG_SECT\n", SlowProg);
      fprintf (fpCust,   "\t\t.text\n");
   fprintf (fpCust, "  } %s\n", SlowProg);

   //---------------------
   // Playback/Record
   if ((SysCfg.DspType == Ti667) || (SysCfg.DspType == Ti665)) {
      fprintf (fpCust, "\n\n/* Playback record buffer's start address */\n");
      sectionStart (fpCust, "PlayRecBuff");
      fprintf (fpCust, "\n\t\t*(PLAYRECSTARTADDR)\n");
      fprintf (fpCust, "\t\t. += 0x80000;\n");
      fprintf (fpCust, "  } ALIGN (0x1000) > DDR3\n\n");
      fprintf (fpCust, "  platform_lib : {} %s\n", FastProg);
      fprintf (fpCust, "  waveHeapSect : {} %s\n", SlowData);
   }
   else if ((IsC64) || (IsC55)) {
      fprintf (fpCust, "\n\n/* Playback record buffer's start address */\n");
      sectionStart (fpCust, "PlayRecBuff");
      fprintf (fpCust, "\n\t\t_PlayRecStartAddr = .;\n");
      fprintf (fpCust,   "\t\t*(PLAYREC)\n");
      if (IsC55)
         fprintf (fpCust, "\t\t. += 0x1000;\n");
       else
         fprintf (fpCust, "\t\t. += 0x80000;\n");
      fprintf (fpCust, "  } ALIGN (0x1000)\n");
   }

   fprintf (fpCust, "}\n");
   fclose (fpCust);

    // Close the generated G.PAK Command file.
    return TRUE;
}

//}===================================================================
//                         Linker Command file  "<project>.cmd" . 
//{
//===================================================================

#ifdef ADD_STUB

#define AddReleaseFile(cond,name,platform)  \
       if (cond) fprintf (fp, "-l \"%s%s\"\n",    name, platform); \
       else      fprintf (fp, "-l \"%sStub%s\"\n", name, platform);

#define AddChannelFile(ChanType,name, platform)  \
      if ((SysCfg.cfgFramesChans & ChanType) != 0) \
               fprintf (fp, "-l \"%s%s\"\n",     name, platform); \
       else    fprintf (fp, "-l \"%sStub%s\"\n", name, platform);

#else
#define AddReleaseFile(cond,name,platform)  \
       if (cond) fprintf (fp, "-l \"%s%s\"\n",    name, platform); 

#define AddChannelFile(ChanType,name, platform)  \
      if ((SysCfg.cfgFramesChans & ChanType) != 0) \
               fprintf (fp, "-l \"%s%s\"\n",     name, platform); 
#endif


static BOOL createLinkCmdFile (blockStatus_t *bs, Align *align, char **pErrText, 
                               ExternalMemInfo_t *pExtMem, char *Root) {
    char CmdFileName [100];
    char CustCmdFileName [100];
    char *fastInst;
    char *CustCmdName, *RootPath;

    FILE *fp, *fpCust;
    unsigned i;

    int PowerOf2;               // power of 2
    int BufrAlign;              // buffer alignment

    VocoderInfo_t   *Vocoder;
    
    EcAlign maxAlign;


    // Open (create) the G.PAK Command file.
    sprintf (CmdFileName, "%s.cmd", Root);
    fp = fopen (CmdFileName, "w");
    if (fp == 0)    {
        *pErrText = "Unable to create linker command file!";
        return (FALSE);
    }

    // Build custom command file if:
    //    1) it does not exist or
    //    2) rebuild requested
    sprintf (CustCmdFileName, "%sCust.cmd", Root);
    if (ForceRebuild) {
       fpCust = NULL;
       ForceRebuild = FALSE;
    } else
       fpCust = fopen (CustCmdFileName, "r");

    if (fpCust) {
        // custom cmd file already exists... close it and don't generate new one
        fclose (fpCust);
        fpCust = 0;
    } else {
        fpCust = fopen (CustCmdFileName, "w");
        if (fpCust == 0)    {
            *pErrText = "Unable to create custom linker command file!";
            return (FALSE);
        }
    }

    // Transform CustCmdFileName into root path pointed to by CustCmdName
    CustCmdName = strrchr (CustCmdFileName, '\\');
    if (CustCmdName == NULL) {
       CustCmdName = Root;
       RootPath = NULL;
    } else {
       *CustCmdName++ = 0;
       RootPath = CustCmdFileName;
    }

    fprintf (fp, "/* G.PAK Config Tool generated command file. */\n\n");
    fprintf (fp, "-priority\n\n");

    // Include the modified BIOS configuration generated command file.
    if (DspInfo->DspType == Ti674) {
	} else if (DspInfo->DspType == Ti665) {
	} else if (DspInfo->DspType == Ti667) {
    } else if (IsC64Plus) {
    } else if (IsC64)  {
    } else if (IsC55)  {
    } else {
       fprintf (fp, "-l Gpak%dMod.cmd\n\n", SysCfg.DspType);
    }

    // ---------------------------------------------------------------------------
    //                   Components (library/stubs)
    if ((DspInfo->DspType == Ti665) || ((DspInfo->DspType == Ti667))) {
        fprintf (fp, "-I \"..\\..\\..\\ProjectLibs\\c667x\\Debug\"\n");
        fprintf (fp, "-I \"..\\..\\..\\ComponentObjs\\c667x\\Debug\"\n");
        fprintf (fp, "-I \"..\\..\\..\\V6_0_1\\AvayaC667x\"\n");
        fprintf (fp, "-I \".\"\n");
        fprintf (fp, "-I \"..\\..\\..\\V6_0_1\\GpakDsp\\Components64p\"\n");
    } else {
    //  Establish search paths
    if (RootPath) fprintf (fp, "-I \"%s\"\n", RootPath);
        fprintf (fp, "-I \".\"\n");
        fprintf (fp, "-I \"%s\"\n",   CompPath);
    #ifdef ADD_STUB
        fprintf (fp, "-I \".\\Stubs\"\n");
        fprintf (fp, "-I \"%s\"\n", StubPath);
    #endif  
    }

    if ((DspInfo->DspType == Ti665) || ((DspInfo->DspType == Ti667))) {
		fprintf (fp, "\n-l \"ADT_g711a1a2_EABI_c66x_V5_04_01_EL_Mml3_CG8_2_2.l66\"\n");
	} else if ((DspInfo->DspType == Ti674)|| (IsC64Plus) || (IsC64)) {
		fprintf (fp, "\n-l \"g711_adt_V3_0_1.l64\"\n");
	} 

    //---- Vocoder libraries (production/demo.stub)
    for (Vocoder = Vocoders; Vocoder->name != NULL; Vocoder++) {

       if (Vocoder->activeFile == PRODUCTION) {
          if (Vocoder->EncLib != NULL) fprintf (fp, "-l \"%s\"\n", Vocoder->EncLib);
          if (Vocoder->DecLib != NULL) fprintf (fp, "-l \"%s\"\n", Vocoder->DecLib);
          continue;
       }
       if (Vocoder->activeFile == DEMO) {
          if (Vocoder->EncDemo != NULL) fprintf (fp, "-l \"%s\"\n", Vocoder->EncDemo);
          if (Vocoder->DecDemo != NULL) fprintf (fp, "-l \"%s\"\n", Vocoder->DecDemo);
          continue;
       }
#ifdef ADD_STUB
       if ((Vocoder->cfgMask & G722Flags) && (cdcBits.g722Enable)) {
          fprintf (fp, "-l \"%s\"\n", "RateConvert_ADT.l64");
       }

       // Don't use a stub for G29AB and either ADT's or TI's has been configured.
       UseStub = 1;
       if ((Vocoder->cfgMask & G729Flags) &&  (SysCfg.Codecs.LongWord & G729Flags))
            UseStub = 0;

       // If neither ADT nor TI's g729 was configured then don't skips the stub
       // for the TI vocoder; it gets included with ADT's
       if ((Vocoder->cfgMask & TIg729Flag) &&  ((SysCfg.Codecs.LongWord & G729Flags) == 0))
            UseStub = 0;
       if ((Vocoder->Stub != NULL) && UseStub) {
            fprintf (fp, "-l \"%s%s\"\n", Vocoder->Stub, platform);

       }
#endif
    }

    fprintf (fp, "\n");

    addComponentsToCmdFile (fp);

    //---- Framing components 
    AddChannelFile (PCM2PCM_CFG_BIT, "Pcm2Pcm", platform);
    AddChannelFile (PCM2PKT_CFG_BIT, "Pcm2Pkt", platform);
    AddChannelFile (PKT2PKT_CFG_BIT, "Pkt2Pkt", platform);
    AddChannelFile (CIRCDATA_CFG_BIT, "CircData", platform);
    AddChannelFile (CONFPCM_CFG_BIT,  "ConfPcm", platform);
    AddChannelFile (CONFPKT_CFG_BIT,  "ConfPkt", platform);
    AddChannelFile (CONFCOMP_CFG_BIT, "ConfComp", platform);

    if (SysCfg.maxNumLbCoders != 0)
      fprintf (fp, "-l \"LbCoder%s\"\n", platform);
#ifdef ADD_STUB
    else
      fprintf (fp, "-l \"LbCoderStub%s\"\n", platform);
#endif

 
 
    //----- Packing components
   if (IsC64)  {
       ADT_Bool AddMSB, AddLSB, AAL2;
       
       AAL2 = (SysCfg.packetProfile == AAL2Trunking);
       AddLSB = !AAL2;


       AddMSB = AAL2 || cdcBits.g728Enable;
       AddReleaseFile (AddMSB, "GpakMSB", platform);
       if (AddMSB) {
          AddReleaseFile (cdcBits.g726Rate16Enable && AAL2, "GpakMSB2", platform);
          AddReleaseFile (cdcBits.g726Rate24Enable && AAL2, "GpakMSB3", platform);
          AddReleaseFile (cdcBits.g726Rate32Enable && AAL2, "GpakMSB4", platform);
          AddReleaseFile (cdcBits.g726Rate40Enable && AAL2, "GpakMSB5", platform);
          AddReleaseFile (cdcBits.g728Enable, "GpakMSB10", platform);
       } 

       AddReleaseFile (AddLSB, "GpakLSB", platform);
       if (AddLSB) {
          AddReleaseFile (cdcBits.g726Rate16Enable, "GpakLSB2", platform);
          AddReleaseFile (cdcBits.g726Rate24Enable, "GpakLSB3", platform);
          AddReleaseFile (cdcBits.g726Rate32Enable, "GpakLSB4", platform);
          AddReleaseFile (cdcBits.g726Rate40Enable, "GpakLSB5", platform);
      }
      
    }
    /* Gpak Dma component */
    AddReleaseFile (ExtDmaActive, DMAFILE(), platform);

    /* G729AB Wrapper functions */
   if (IsC64Plus || IsC64 || IsC55) {
        if (SysCfg.Codecs.Bits.TI_g729ABEnable) fprintf (fp,"-l \"G729_TIWrapper%s\"\n", platform);
        if (SysCfg.Codecs.Bits.g729ABEnable) fprintf (fp,"-l \"G729_ADTWrapper%s\"\n", platform);
        if (SysCfg.Codecs.Bits.TI_g7231AEnable) fprintf (fp,"-l \"G723_TIWrapper%s\"\n", platform);
    }

    /* Rtp Components */
   if (IsC64Plus || IsC64) {
        if (algBits.rtpHostDelivery) fprintf (fp,"-l \"64xRtpHostDelivery%s\"\n", platform);
        if (algBits.rtpNetDelivery) {
            fprintf (fp,"-l \"64xRtpNetDelivery%s\"\n", platform);
            if ((DspInfo->DspType == Ti665) || ((DspInfo->DspType == Ti667))) {
                fprintf (fp,"-l \"ipStackSupport%s\"\n", platform);
                fprintf (fp,"-l \"gpakRtcp%s\"\n", platform);
            }
        }
    }

    /* AEC Equalizer Component */
   if (IsC64Plus || IsC64) {
        if ((SysCfg.micEqLength) || (SysCfg.spkrEqLength)) fprintf (fp,"-l \"adtEq%s\"\n", platform);    
    }

#ifndef ADD_STUB
    if ((DspInfo->DspType == Ti665) || ((DspInfo->DspType == Ti667))) {
        fprintf (fp,"\n-l \"GpakDriverPjt.lib\"\n");
        fprintf (fp,"-l \"GpakStdPjt.lib\"\n");
        fprintf (fp,"-l \"GpakStubsPjt.lib\"\n");
    } else {
        fprintf (fp, "-l \"GpakStubsV%x_%x%s\"\n", VERS>>8, (VERS>>4)&0xF, libExt);  
    }
#endif

    // include the custom command file
    //fprintf (fp, "-l \"%s\"\n", CustCmdName);    


   //-------------------------------------------------------------------------
   //                  Sections (location and alignment)
   // Create the G.PAK memory sections as needed.

   // These sections are allocated in order of time criticalness with most critical first
   // Establish data section priorities
   ExtDmaArea = NULL;
   bssData = NULL;

    //assignExtMemory (ExternalMemInfo_t,  *extMem, *roMem, *intProgMem, *intDataMem)

   // default memory area assignments
   if (DspInfo->DspType == Ti647) {
      // NOTE:  LL2RAM is used for uninitialized structures that are core dependent.  
      //        This allows a single image to be downloaded that all cores can run with.
      bssData      = "> LL2RAM    /* bss must be restricted to local core access */";
      PerCoreData  = "> LL2RAM    /* per code data must be restricted to local core access */";
      Core0Data    = "> Core0 | DDR2";
      Core1OnlyData = "> Core1";
      AnyCoreData  = ">> Core2 | Core3 | Core4 | Core5";
      InternalMem = FastData = "ALIGN (8) >> LL2RAM";
      FastConst = "ALIGN (128) > ROCache";
      HostData  = "ALIGN (8) > NonCached";
      NonCacheData = "ALIGN (128) > NonCached";
      assignExtMemory (pExtMem, "DDR2", "ROCache", "Core5", "Core0");

      InternalProg = "ALIGN (128) > Core0 | ROCache";
      FastProg     = "ALIGN (128) > ROCache";
      SlowConst = MediumProg = SlowProg = FastProg;

      SlowData = "ALIGN (8) > NonCached";
      SlowDataFirst = "ALIGN (128) >> DDR2";
      FastDataFirst = "ALIGN (128) >> DDR2";
      MediumData = InstanceData = FastDataFirst;

   } else if (DspInfo->DspType == Ti674) {
      InstanceData = FastConst = FastData = InternalMem = ">>  IRAM";
      InternalProg  = FastProg = ">>  IRAM";
      assignExtMemory (pExtMem, "EXT", "EXT", "IRAM", "IRAM | L3_CBA_RAM");
      SlowDataFirst = SlowData;
      HostData     = "ALIGN (8) > L3_CBA_RAM | NonCached";
      NonCacheData = "ALIGN (128) > L3_CBA_RAM | NonCached";
      Core0Data = FastData;
      PerCoreData = &FastData[1];
   } else if (DspInfo->DspType == Ti665) {
      InstanceData = FastConst = FastData = InternalMem = ">>  L2SRAM";
      InternalProg  = FastProg = ">>  L2SRAM";
      assignExtMemory (pExtMem, "DDR3", "DDR3", "L2SRAM", "L2SRAM | MSMCSRAM");
      SlowDataFirst = SlowData;
      HostData     = "ALIGN (8) > MSMCSRAM | DDR3_NOCACHE";
      NonCacheData = "ALIGN (128) > MSMCSRAM | DDR3_NOCACHE";
      Core0Data = FastData;
      PerCoreData = &FastData[1];
   } else if (DspInfo->DspType == Ti667) {
      // NOTE:  LL2RAM is used for uninitialized structures that are core dependent.  
      //        This allows a single image to be downloaded that all cores can run with.
      bssData       = "> L2SRAM";
      PerCoreData   = "> L2SRAM";
      Core0Data     = "> L2SRAM";
      Core1OnlyData = "> L2SRAM";
      AnyCoreData   = "> L2SRAM";
      InternalMem   = FastData = "ALIGN (8) >> L2SRAM";
      FastConst = "ALIGN (128) > DDR3";
      HostData  = "ALIGN (8) > MSMCSRAM";
      NonCacheData = "ALIGN (128) > DDR3";
      assignExtMemory (pExtMem, "DDR3", "DDR3", "L2SRAM", "L2SRAM | MSMCSRAM");
      InternalProg = "ALIGN (128) > L2SRAM";
      FastProg     = "ALIGN (128) > DDR3";
      SlowConst = MediumProg = SlowProg = FastProg;
      SlowData      = "ALIGN (128) > DDR3";
      SlowDataFirst = "ALIGN (128) >> DDR3";
      FastDataFirst = "ALIGN (128) >> DDR3";
      MediumData = InstanceData = FastDataFirst;
   } else if (IsC64Plus) {
      SlowDataFirst = "> L1DSRAM | IRAM";
      InstanceData = FastConst = FastData = InternalMem = ">> L1DSRAM | IRAM";
      InternalProg  = FastProg = ">> L1PSRAM | IRAM";
      if (DspInfo->DspType == TiOM3530) 
         assignExtMemory (pExtMem, "DDR2", "DDR2", "L1PSRAM | IRAM", "L1DSRAM | IRAM");
      else
         assignExtMemory (pExtMem, "EXT", "EXT", "L1PSRAM | IRAM", "L1DSRAM | IRAM");

      if (DspInfo->DspType == Ti6424) {
         Core0Data = FastData;
         PerCoreData = &FastData[1];
         HostData = ">>L1DSRAM | EXT_NONCACHE";
      } else {
         HostData = Core0Data = FastData;
         PerCoreData = &FastData[1];
      }
   } else if (IsC64) {

      // default memory area assignments
      InstanceData = InternalProg = SlowDataFirst = FastData = FastProg = InternalMem = ">> ISRAM";
      assignExtMemory (pExtMem, "EMIFA_CE0", "EMIFA_CE0", "ISRAM", "ISRAM");

      HostData = Core0Data = FastConst = FastData;
      PerCoreData = &FastData[1];

   } else if (IsC55) {
      SlowDataFirst  = ">> SARAM_A | SARAM | SARAM_B | DARAM PAGE 0";
      MediumData     = ">> SARAM_A | SARAM | SARAM_B | DARAM PAGE 0";
      SlowData       = ">> SARAM_A | SARAM | SARAM_B PAGE 0";

      FastDataFirst = ">> DARAM | SARAM_A | SARAM | SARAM_B PAGE 0";
      InstanceData = FastConst = FastData   = ">> DARAM | SARAM_A | SARAM | SARAM_B PAGE 0";

      Daram       = ">> DARAM PAGE 0";
      InternalMem = ">> DARAM | SARAM_A | SARAM | SARAM_B  PAGE 0";

      InternalProg = FastProg    = "ALIGN(2)  >> SARAM_A | SARAM | SARAM_B PAGE 0";
      MediumProg  = "ALIGN(2)  >> SARAM_A | SARAM | SARAM_B PAGE 0";
      SlowProg    = "ALIGN(2)  >> SDRAM PAGE 0";  

      assignExtMemory (pExtMem, "SDRAM", "SDRAM",  "SARAM_A | SARAM | SARAM_B",
                                         "DARAM | SARAM_A | SARAM | SARAM_B");

      Core0Data  = ">> DARAM PAGE 0";
      PerCoreData = "> DARAM PAGE 0";
      HostData = FastData;
   } else {
      SlowDataFirst           = ">> IDATA | DARAM PAGE 1";
      InstanceData = FastData = MediumData   = ">> IDATA | DARAM PAGE 1";
      FastDataFirst              = ">> DARAM | IDATA PAGE 1";
      SlowData                = ">> DARAM | IDATA PAGE 1";

      InternalMem = Daram   = ">> DARAM PAGE 1";
      InternalProg = FastProg              = ">> IPROG | EPROG0 | EPROG1 | EPROG2 | EPROG3 PAGE 0";
      MediumProg = SlowProg = ">> EPROG0 | EPROG1 | EPRGO2 | EPROG3 | IPROG PAGE 0";  

      Core0Data = FastConst = FastData;
      PerCoreData = &FastData[1];
      HostData = FastData;
   }

   if (bssData == NULL)
      bssData = &FastData[1];

    //-------------------- TIME CRITICAL (Fast memory) --------------------------------
    for (i = 0; i < MAX_NUM_SERIAL_PORTS; i++)    {
        if (SysCfg.maxSlotsSupported[i] == 0) {
           BufrAlign = 8;
        } else {
           PowerOf2 = (int) (log10 ((double) SysCfg.dmaBufferSize[i]) / log10((double) 2));
           BufrAlign = (int) (pow (2.0, (double) (PowerOf2 + 1)));
        }
    }

    if (IsC64 && SysCfg.AECInstances != 0) {
        fprintf (fp, "\n--symbol_map=_AGC_ADT_agcApplyGain=_AGC_ADT_applyGain");
        fprintf (fp, "\n--symbol_map=_AGC_ADT_agcComputeGain=_AGC_ADT_computeGain");
        fprintf (fp, "\n--symbol_map=_AGC_ADT_agcInit=_AGC_ADT_agcinit");
    }
    

    if (SysCfg.DspType == Ti647) {
       fprintf (fp, "\n_Start$Macros = 0;\n");
       fprintf (fp, "_End$Macros = 0x001FFFFF;\n\n");
    }
    else if (SysCfg.DspType == Ti667) {
       fprintf (fp, "\n_Start$Macros = 0;\n");
       fprintf (fp, "_End$Macros = 0x001FFFFF;\n\n");
    }

    fprintf (fp, "\nSECTIONS\n{\n");

    // Create per port aligned DARAM buffer sections.
    fprintf (fp, "\n/* Per Port DARAM */\n");
    if (!IsC64) 
       fprintf (fp, "  HOST_API : > 0x%04X\n\n", SysCfg.hpiBlockStartAddress);
    else {
       fprintf (fp,     "  HOST_API : > 0x%08X    /* Address of G.PAK interface structure */\n", SysCfg.hpiBlockStartAddress);
       if (SysCfg.HostIF & RMII_IF)
           fprintf (fp, "  IPCFG :    > 0x%08X    /* Address of IP Configuration structure */\n", ((int) (SysCfg.hpiBlockStartAddress)) + 4);
       if (tsipEnable)
           fprintf (fp, "  TSIPCFG :  > 0x%08X    /* Address of TSIP Configuration structure */\n", ((int) (SysCfg.hpiBlockStartAddress)) + 0x100);
	   else 
			fprintf (fp, "  TDMCFG :  > 0x%08X    /* Address of TDM(MCBSP)Configuration structure */\n", ((int) (SysCfg.hpiBlockStartAddress)) + 0x100);
       fprintf (fp,     "\n");
    }
    //  Fast programming
   if ( (!IsC64) && (!IsC55) ) {
      fprintf (fp, " .gpakCritical : {} > IPROG | EPROG0 | EPROG1 PAGE 0\n");
      fprintf (fp, " .gpaktext : {} >> IPROG  | EPROG0 | EPROG1 | EPROG2 | EPROG3 PAGE 0\n");
      fprintf (fp, " .gpakstub : {} >> EPROG0 | EPROG1 | EPROG2 | EPROG3 PAGE 0\n");
   } else if (IsC55) {
      fprintf (fp, " .gpakCritical : {} %s\n", FastProg);
   }
   if (SysCfg.DspType == Ti647) {
      fprintf (fp, "  .core0init :  > 0x10800000\n");
      fprintf (fp, "  .core1init :  > 0x11800000\n");
      fprintf (fp, "  .core2init :  > 0x12800000\n");
      fprintf (fp, "  .core3init :  > 0x13800000\n");
      fprintf (fp, "  .core4init :  > 0x14800000\n");
      fprintf (fp, "  .core5init :  > 0x15800000\n");
   }
   else if (SysCfg.DspType == Ti667) {
      fprintf (fp, "  .core0init :  > 0x10800000\n");
      fprintf (fp, "  .core1init :  > 0x11800000\n");
      fprintf (fp, "  .core2init :  > 0x12800000\n");
      fprintf (fp, "  .core3init :  > 0x13800000\n");
      fprintf (fp, "  .core4init :  > 0x14800000\n");
      fprintf (fp, "  .core5init :  > 0x15800000\n");
   }

   fprintf (fp, "\n/* Pre-Cache initialization */\n");
   if (IsC55)
      fprintf (fp, "  .text:_memcpy : {} %s\n", FastProg);
   else {
      sectionStart (fp, "INIT_PROG_SECT");
      if (SysCfg.DspType == Ti647) {
         fprintf (fp, "\n\t\t*(.text:_memcpy)\n");
         fprintf (fp, "  } %s\n\n", FastProg);
      } else if (SysCfg.DspType == Ti667)
         fprintf (fp, "  } %s\n\n", FastProg);
      else {
         fprintf (fp, "\n\t\t*(.text:_memcpy)\n");
         fprintf (fp, "  } %s\n\n", InternalProg);
      }
   }

   fprintf (fp, "  .cinit   : {}          %s\n", cinitData);

   sectionStart (fp, ".bss");
   fprintf (fp, "}          %s\n", bssData);

   sectionStart (fp, ".far_bss");
   fprintf (fp, " *(.far) } %s\n", bssData);


   if (pExtMem->type != EXT_NONE) {
      // Create per channel DMA transfer sections.
      fastInst = ExtDmaArea;
   } else {
      fastInst = InternalMem;     
   }
   if (fastInst[1] == '>') fastInst++;


   AlignInternalCodecs (fp, align);


   /* DMA internal queue sections for EC. Use the worst-case alignment if both pkt and pcm ECs are configured */
   if (ExtDmaActive) { 
        if (SysCfg.numPcmEcans && SysCfg.numPktEcans)  {
            maxAlign.EcEchoPath = MAX (align->Pcm.EcEchoPath, align->Pkt.EcEchoPath);
            maxAlign.EcBackEp   = MAX (align->Pcm.EcBackEp,   align->Pkt.EcBackEp);
            maxAlign.EcChan     = MAX (align->Pcm.EcChan,     align->Pkt.EcChan);
            maxAlign.EcDaState  = MAX (align->Pcm.EcDaState,  align->Pkt.EcDaState);
            maxAlign.EcSaState  = MAX (align->Pcm.EcSaState,  align->Pkt.EcSaState);
        } else if (SysCfg.numPcmEcans && (SysCfg.numPktEcans == 0)) {
            maxAlign.EcEchoPath = align->Pcm.EcEchoPath;
            maxAlign.EcBackEp   = align->Pcm.EcBackEp;
            maxAlign.EcChan     = align->Pcm.EcChan;
            maxAlign.EcDaState  = align->Pcm.EcDaState;
            maxAlign.EcSaState  = align->Pcm.EcSaState;
        } else if ((SysCfg.numPcmEcans == 0) && SysCfg.numPktEcans) {
            maxAlign.EcEchoPath = align->Pkt.EcEchoPath;
            maxAlign.EcBackEp   = align->Pkt.EcBackEp;
            maxAlign.EcChan     = align->Pkt.EcChan;
            maxAlign.EcDaState  = align->Pkt.EcDaState;
            maxAlign.EcSaState  = align->Pkt.EcSaState;
        } else {
            maxAlign.EcEchoPath = 8;
            maxAlign.EcBackEp   = 8;
            maxAlign.EcChan     = 8;
            maxAlign.EcDaState  = 8;
            maxAlign.EcSaState  = 8;
        }

        AlignEchoCancellerQueue (fp, &maxAlign);
   }

   // Fast program sections
   fprintf (fp, "\n/* Fast access memory sections */\n");
   groupStart (fp, "FAST_PROG");
   fprintf (fp,"\n\t\tFAST_PROG_SECT\n");
   if (SysCfg.DspType == Ti647) 
      fprintf (fp,"  } %s\n\n", FastProg);
   else if (SysCfg.DspType == Ti667) 
      fprintf (fp,"  } %s\n\n", FastProg);
   else
      fprintf (fp,"  } %s\n\n", InternalProg);

   // DMA sections
   if (SysCfg.DspType == Ti647) BufrAlign = 128;
   else if (SysCfg.DspType == Ti667) BufrAlign = 128;
   else BufrAlign = 8;

   for (i = 0; i < MAX_NUM_SERIAL_PORTS; i++)    {
      if (SysCfg.maxSlotsSupported[i] == 0) {
         fprintf (fp, "  DMABUFF%uT : {} %s\n",   i, SlowDataFirst);
         fprintf (fp, "  DMABUFF%uR : {} %s\n\n", i, SlowDataFirst);
      } else {
        if (SysCfg.DspType == Ti647) {
            fprintf (fp, "  DMABUFF%uT : {} ALIGN(%d) %s\n",   i, BufrAlign, Core1OnlyData);
            fprintf (fp, "  DMABUFF%uR : {} ALIGN(%d) %s\n\n", i, BufrAlign, Core1OnlyData);
        } else if (SysCfg.DspType == Ti667) {
            fprintf (fp, "  DMABUFF%uT : {} ALIGN(%d) %s\n",   i, BufrAlign, Core1OnlyData);
            fprintf (fp, "  DMABUFF%uR : {} ALIGN(%d) %s\n\n", i, BufrAlign, Core1OnlyData);
        } else if (SysCfg.DspType == Ti674) {
            fprintf (fp, "  DMABUFF%uT : {} %s\n",   i, HostData);
            fprintf (fp, "  DMABUFF%uR : {} %s\n\n", i, HostData);
        } else {
            fprintf (fp, "  DMABUFF%uT : {} ALIGN(%d) %s\n",   i, BufrAlign, &FastData[0]);
            fprintf (fp, "  DMABUFF%uR : {} ALIGN(%d) %s\n\n", i, BufrAlign, &FastData[0]);
        }
      }
   }
   if ((SysCfg.DspType == Ti647) && tsipEnable) {
      fprintf (fp, "  RxTransBuff : {} ALIGN(%d) %s\n",   BufrAlign, Core1OnlyData);
      fprintf (fp, "  TxTransBuff : {} ALIGN(%d) %s\n\n", BufrAlign, Core1OnlyData);
   }
   else if ((SysCfg.DspType == Ti667) && tsipEnable) {
      fprintf (fp, "  RxTransBuff : {} ALIGN(%d) %s\n",   BufrAlign, Core1OnlyData);
      fprintf (fp, "  TxTransBuff : {} ALIGN(%d) %s\n\n", BufrAlign, Core1OnlyData);
   }

#ifdef MULTI_CORE
   // Data required by HWIs and DMASWI
   sectionStart (fp, "FAST_DATA_SECT");
   if (SysCfg.DspType == Ti647) 
      fprintf (fp, "} ALIGN (128) %s\n", Core1OnlyData);
   else if (SysCfg.DspType == Ti667) 
      fprintf (fp, "} ALIGN (128) %s\n", Core1OnlyData);
   else
      fprintf (fp, "} %s\n", FastData);

   if (SysCfg.DspType == Ti647) {
      fprintf (fp, "\n  SHARED_LOCK_SECT: {} > SL2RAM fill=0xffffffff  /* Lock values initialized to -1 to force waits */\n");
      sectionStart (fp, "SHARED_DATA_SECT");
      fprintf (fp, "\n  } %s   /* Data shared across CPUs */\n", NonCacheData);
   } else if (SysCfg.DspType == Ti667) {
      fprintf (fp, "\n  SHARED_LOCK_SECT: {} > L2SRAM fill=0xffffffff  /* Lock values initialized to -1 to force waits */\n");
      sectionStart (fp, "SHARED_DATA_SECT");
      fprintf (fp, "\n  } %s   /* Data shared across CPUs */\n", NonCacheData);
   } else {
      fprintf (fp, "\n  SHARED_DATA_SECT: {} %s\n", FastData);
   }
   // Host messaging interface
   sectionStart (fp, "PER_CORE_DATA");
   fprintf (fp, "\n");
   if (SysCfg.DspType == Ti647) {
      fprintf (fp, "\t\t*(FAST_SCRATCH)\n");
      fprintf (fp, "\t\t*(SLOW_SCRATCH)\n");
   } else if (SysCfg.DspType == Ti667) {
      fprintf (fp, "\t\t*(FAST_SCRATCH)\n");
      fprintf (fp, "\t\t*(SLOW_SCRATCH)\n");
   }
   fprintf (fp,    "\t\t*(IN_WORK)\n");
   fprintf (fp,    "\t\t*(OUT_WORK)\n");
   fprintf (fp,    "\t\t*(ECFAR_WORK)\n");
   fprintf (fp, "  } %s\n\n", PerCoreData);

   // Allocation for network task stacks
   sectionStart (fp, "NETWORK_TASK_STACK_SECT");
   fprintf (fp,    "\n\t\t*(STACK_ALLOCATION:MainIPTask)\n");
   fprintf (fp,      "\t\t*(STACK_ALLOCATION:RTPTask)\n");
   if (SysCfg.HostIF & RMII_IF)
      fprintf (fp, "  } ALIGN (128) %s\n\n", Core0Data);
   else
      fprintf (fp, "  } %s\n\n", SlowConst);

   sectionStart (fp, "MSG_TASK_STACK_SECT");
   fprintf (fp,    "\n\t\t*(STACK_ALLOCATION:MsgTask)\n");
   if (SysCfg.HostIF & RMII_IF)
      fprintf (fp, "  } %s\n\n", FastDataFirst);
   else
      fprintf (fp, "  } %s\n\n", SlowConst);

   // Host packet interface  / Shared data section
   sectionStart (fp, "HOST_DATA_SECT");
   fprintf (fp,   "\n");
   fprintf (fp, "\t\t*(GPAK_IF)\n");
   fprintf (fp, "\t\t*(CMDMSGBUF)\n");
   fprintf (fp, "\t\t*(RPLYMSGBUF)\n");
   fprintf (fp, "\t\t*(evt_CIRC)\n");
   fprintf (fp, "\t\t*(evt_POOL)\n\n");

   if (HOST_RTP) {
      fprintf (fp, "\t\t*(pkt_CIRC)\n");
      if (SysCfg.DspType != Ti647)  fprintf (fp, "\t\t*(pktIn_POOL)        /* Host payload interface */\n");
      if (SysCfg.DspType != Ti647)  fprintf (fp, "\t\t*(pktOut_POOL)       /* Host payload interface */\n");
   } else {
      fprintf (fp, "\t\t*(RTP_CIRC)        /* RTP host packet interface */\n\n");
      if (SysCfg.DspType == Ti647)   
        fprintf (fp, "\t\t*(pkt_CIRC)\n");
      else if (SysCfg.DspType == Ti667)   
        fprintf (fp, "\t\t*(pkt_CIRC)\n");
   }
   fprintf (fp, "  } %s\n\n", HostData);

   fprintf (fp,   "  RTP_DATA_SECT: {\n");
   fprintf (fp, "\t\t*(RTP_POOL:ToNet)   /* RTP host packet interface */\n");
   fprintf (fp, "\t\t*(RTP_POOL:ToDSP)   /* RTP host packet interface */\n");
   if (SysCfg.HostIF & RMII_IF)
      fprintf (fp, "  } %s\n\n", MediumData);  // Network interface
   else
      fprintf (fp, "  } %s\n\n", HostData);

   if (SysCfg.DspType == Ti647) {
      sectionStart (fp, "NON_CACHED_DATA");
      fprintf (fp, "\n\t\t*(CHAN_INST_DATA:Chan)\n");
      fprintf (fp,   "\t\t*(POOL_ALLOC)\n");
      fprintf (fp,   "\t\t*(SHARED_DATA_SECT:logging)\n");
      fprintf (fp,   "\t\t*(STUB_DATA_SECT)\n");
      fprintf (fp, "  } %s\n\n", HostData);

      fprintf (fp,   "  PCM_DATA: {\n");
      fprintf (fp, "\t\t*(pktIn_POOL)\n");
      fprintf (fp, "\t\t*(pktOut_POOL)\n");
      fprintf (fp, "\t\t*(pcmIn_POOL)\n");
      fprintf (fp, "\t\t*(pcmOut_POOL)\n");
      fprintf (fp, "  } %s\n\n", NonCacheData);
   } else if (SysCfg.DspType == Ti667) {
      sectionStart (fp, "NON_CACHED_DATA");
      fprintf (fp, "\n\t\t*(CHAN_INST_DATA:Chan)\n");
      fprintf (fp,   "\t\t*(POOL_ALLOC)\n");
      fprintf (fp,   "\t\t*(SHARED_DATA_SECT:logging)\n");
      fprintf (fp,   "\t\t*(STUB_DATA_SECT)\n");
      fprintf (fp, "  } %s\n\n", HostData);

      fprintf (fp,   "  PCM_DATA: {\n");
      fprintf (fp, "\t\t*(pktIn_POOL)\n");
      fprintf (fp, "\t\t*(pktOut_POOL)\n");
      fprintf (fp, "\t\t*(pcmIn_POOL)\n");
      fprintf (fp, "\t\t*(pcmOut_POOL)\n");
      fprintf (fp, "  } %s\n\n", NonCacheData);
   } else {
      fprintf (fp,   "  NON_CACHED_DATA: {} %s\n\n", HostData);
   }
    
#else
{
   // Host messaging interface
   fprintf (fp, "\n  FAST_DATA_SECT: {\n");

   fprintf (fp, "\t\t*(GPAK_IF)\n");
   fprintf (fp, "\t\t*(CMDMSGBUF)\n");
   fprintf (fp, "\t\t*(RPLYMSGBUF)\n");
   if (SysCfg.HostIF & RMII_IF)
      fprintf (fp, "\t\t*(STACK_ALLOCATION)\n");
   fprintf (fp, "\t\t*(evt_CIRC)\n");
   fprintf (fp, "\t\t*(evt_POOL)\n\n");

   // Host packet interface
   if (HOST_RTP) {
      fprintf (fp, "\t\t*(pkt_POOL)        /* Host payload interface */\n");
   }
#ifdef RTP_ALLOCATION
   else {
      fprintf (fp, "\t\t*(RTP_POOL)        /* RTP host packet interace */\n");
      fprintf (fp, "\t\t*(RTP_CIRC)        /* RTP host packet interace */\n");
   }
#endif
   fprintf (fp, "\t\t*(pkt_CIRC)   \n\n");
   fprintf (fp, "  } %s\n\n", FastData);
    
}
#endif   
   
   // End of sections.
   fprintf (fp, "}\n");

   // include the custom command file
   fprintf (fp, "-l \"%s\"\n", CustCmdName);    
     

   fclose (fp);

   if (fpCust)
      return createCustCmdFile (fpCust, align, BufrAlign, pExtMem);
   else 
      return TRUE;

}

//}===================================================================
//{ - - - - - - - -  Modify the BIOS generated Command file.  - - - - - - -
//
//    Gpak<dsp>cfg.cmd ==> Gpak<dsp>Mod.cmd
//
//
//         5420 -  IPROG expanded with extra-program memory size, IDATA1 + IPROG = 0x8000
//                 .text > EPROG   ==> .text > EPROG  | IPROG
//                 .text > EPROG0  ==> .text > EPROG0 | IPROG
//
//   5421, 5441 -  .text > EPROG0  ==> .text > EPROG0 | ... | EPROG3
//
//         5416 -  .text > EPROG0  ==> .text > EPROG0 | EPROG1
//}
static BOOL createBIOSCfgFile (char **pErrText) {
    FILE *fp, *fp2;

    unsigned int IprogSize;
    unsigned int Idata1Size;
   unsigned int ProgramStart;
    unsigned int DataStart;

    char LineBufr[101];         // line buffer
    char *pToken;               // pointer to string token
    char BiosCfgName[40];
   char BiosModName[40];

    int State;                  // state variable

    // - - - - - - - -  Modify the BIOS generated Command file.  - - - - - - -

    // Attempt to open the BIOS generated Command file.
   sprintf (BiosCfgName, "Gpak%dcfg.cmd", DspTypeInfo.DspType);
   sprintf (BiosModName, "Gpak%dMod.cmd", DspTypeInfo.DspType);
    fp = fopen (BiosCfgName, "r");
    if (fp == 0)    {
        *pErrText = "Unable to open BIOS Command file! (GpakBioscfg.cmd)";
        return (FALSE);
    }

    // Attempt to open (create) the modified BIOS Command file.
    fp2 = fopen (BiosModName, "w");
    if (fp2 == 0)    {

        // Close the BIOS generated Command file.
        fclose (fp);

        *pErrText = "Unable to create modified BIOS Command file! (GpakBiosMod.cmd)";
        return (FALSE);
    }

    // Determine the amount of Program Memory to allocate, Data Memory receives remainder
    if (DataMemory < 0) {
       DataMemory = 0;
   }

    // Copy and modify the .cmd file based on the DSP type.
    State = 1;
    switch (DspTypeInfo.DspType) {
    case Ti5416:    // Address 0x80-0x7fff is split between data (IDATA) and program (IPROG)
    case Ti5420:
    case Ti5421:
    case Ti5441:
       if (DataMemory <= 0)  break;
       Idata1Size = DataMemory;
       DataStart    = 0x80;
       ProgramStart = DataStart + Idata1Size;
       IprogSize    = 0x7f00 - Idata1Size;
      break;

    case Ti5510:
       if (DataMemory <= 0)  break;
       Idata1Size = DataMemory;
       DataStart    = 0x80;
       ProgramStart = DataStart + Idata1Size;
       IprogSize    = 0x7f00 - Idata1Size;
      break;

   default:
      State = 2;
      DataMemory = 0;
      break;
   }


    while (1) {
        if (fgets (LineBufr, 100, fp) == NULL)  break;
        switch (State)   {
        case 1:   // Ti54xx -  Wait for MEMORY Identifier
            if (strncmp (LineBufr, "MEMORY", strlen ("MEMORY")) == 0)
                State = 2;
            break;

        case 2:  // Ti54xx   IPROG: origin = ..., len = ...
        case 3:  //          IDATA: origin = ..., len = 
        case 4:  //         .text > EPROG0   ==>  {*(.text)} > EPROG0 | ... | EPROG3   PAGE 0
            if (((pToken = strstr (LineBufr, ".text:")) != NULL) && (strstr (pToken, "> EPROG0") != NULL))       {
                strcpy (pToken + strlen (".text:"),
                    "    {*(.text)} >> EPROG0 | EPROG1 | EPROG2 | EPROG3 PAGE 0  /* GPAK mod */\n");
            continue;
            }
         if (DataMemory <= 0) break;
            if ((pToken = strstr (LineBufr, "IDATA:")) != NULL)   {
                sprintf (pToken,
                        "IDATA: origin = 0x0080, len = 0x%x  /* GPAK mod */\n", Idata1Size & 0xffff);
                State++;
            } else if ((pToken = strstr (LineBufr, "IPROG:")) != NULL)  {
                sprintf (pToken,
                        "IPROG: origin = 0x%x, len = 0x%x  /* GPAK mod */\n",
                        (ProgramStart & 0xffff), (IprogSize & 0xffff));
                State++;
         }
          break;

        
        default:
            break;
        }
        fputs (LineBufr, fp2);
    }

    // Close the BIOS generated Command file.
    fclose (fp);

    // Close the modified BIOS Command file.
    fclose (fp2);

    // Return with an indication of success.
    return TRUE;
}

int fillMemTab (memSpec_t *memTab, ecConfig_t *ecCfg) {

   int memTabCount = 0;
   int numRates=0;
   int n;

    // ================ VOCODERS =====================
    if (cdcBits.g726LowMipsEnable == 1)    {
        if (cdcBits.g726Rate16Enable == 1) numRates++;
        if (cdcBits.g726Rate24Enable == 1) numRates++;
        if (cdcBits.g726Rate32Enable == 1) numRates++;
        if (cdcBits.g726Rate40Enable == 1) numRates++;
    }
    n = getVocoderMemSpec (numRates, &memTab[memTabCount]);
    memTabCount += n;

    return (memTabCount);
}

int getVocoderMemSpec (int nrates, memSpec_t *mem) {


   int memoryTypes = 0;
   int count = 0;
   VocoderInfo_t *Vocoder;



    // 1) Determine which enabled vocoder has the largest chanSize - this will be
    //    the default chanSize for all vocoders.
    //
    // 2) Determine which enabled vocoder has the worst case alignment - this will be the 
    //    the default alignment for all vocoders.
    //
    // 3) Determine the memory type of all enabled vocoders. If any have DARAM, then
    //    that will be the default memory type for all vocoders.
    //
    // 4) Sum the text and data requirements of all enabled vocoders.
    //

    for (Vocoder=Vocoders; Vocoder->name != NULL; Vocoder++) {
       if (! (Vocoder->cfgMask & SysCfg.Codecs.LongWord)) continue;

       if (Vocoder->EncoderMem != NULL) {
          MemSpec (Vocoder->EncoderMem, encodeMemory);
       }

       if (Vocoder->DecoderMem != NULL) {
          MemSpec (Vocoder->DecoderMem, decodeMemory);
       }

    }

    if (IsC64) {  //  Convert from bytes to shorts
       encodeMemory.Words = encodeMemory.Words / 2;
       decodeMemory.Words = decodeMemory.Words / 2;
    }

    if (memoryTypes & ENCODE_BIT) {
        mem[count++] = encodeMemory;
    }

    if (memoryTypes & DECODE_BIT) {
       mem[count++] = decodeMemory;
    }

    return (count);
}


//===================================================================
//                     Main Configuration Build Call
//===================================================================
BOOL createDspSrcFiles (sysConfig_t *cfg, blockStatus_t *bs, unsigned int numChannels, 
    ecConfig_t *ecCfg,  char **pErrText, ExternalMemInfo_t *pExtMem,
    char *RootFile, DspTypeDepInfo_t *dspInfo) {

   Align align;

   ExtDmaActive = pExtMem->type == EXT_DMA;
   CacheActive  = pExtMem->type == EXT_CACHE;
   if (pExtMem->type != EXT_NONE) {
       encodeMemory.align = MAX (encodeMemory.align, 128);
       decodeMemory.align = MAX (decodeMemory.align, 128);
    } else {
       encodeMemory.align = MAX (encodeMemory.align, 8);
       decodeMemory.align = MAX (decodeMemory.align, 8);
   }
 
   internalQueueCnt = 0;
   NumFrameRates = 0;
   MaxFrameSize = 0;

   TRACE ("G.PAK version %x\n", VERS);

   if (FRAME_1ms_ENABLED)  { NumFrameRates++; MaxFrameSize =   8; }
   if (FRAME_2ms_ENABLED)  { NumFrameRates++; MaxFrameSize =  20; }
   if (FRAME_5ms_ENABLED)  { NumFrameRates++; MaxFrameSize =  40; }
   if (FRAME_10ms_ENABLED) { NumFrameRates++; MaxFrameSize =  80; internalQueueCnt++; }
   if (FRAME_20ms_ENABLED) { NumFrameRates++; MaxFrameSize = 160; internalQueueCnt++; }
   if (FRAME_22ms_ENABLED) { NumFrameRates++; MaxFrameSize = 180; internalQueueCnt++; }
   if (FRAME_30ms_ENABLED) { NumFrameRates++; MaxFrameSize = 240; internalQueueCnt++; }
   if (30 < MaxCustomFrameRateMs) MaxFrameSize = MaxCustomFrameRateMs * 8;

   if (3 < internalQueueCnt)  internalQueueCnt = 3;

   if (algBits.wbEnable) MaxFrameSize *= 2;

   if (createLinkCmdFile (bs, &align, pErrText, pExtMem, RootFile) == FALSE) return FALSE;
   if (createGlobalCFile (bs, numChannels, ecCfg, &align, pErrText, pExtMem, RootFile) == FALSE) return FALSE;
   if (createGlobalHFile (bs, numChannels, ecCfg, pErrText, RootFile) == FALSE) return FALSE;

   if (IsC55) return TRUE;
   if (IsC64) return TRUE;
   return createBIOSCfgFile (pErrText);
}

