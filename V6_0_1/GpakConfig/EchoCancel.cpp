// EchoCancel.cpp : implementation file
//

#include "stdafx.h"
#include "GpakConfig.h"
#include "EchoCancel.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#pragma warning (once:4996)
static char Buffer[200];
extern int MinFrameSize;
/////////////////////////////////////////////////////////////////////////////
// EchoCancel dialog


EchoCancel::EchoCancel(CWnd* pParent /*=NULL*/)
	: CDialog(EchoCancel::IDD, pParent)
{
	//{{AFX_DATA_INIT(EchoCancel)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void EchoCancel::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(EchoCancel)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(EchoCancel, CDialog)
	//{{AFX_MSG_MAP(EchoCancel)
	ON_BN_CLICKED(IDDEFAULTS, OnDefaults)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// EchoCancel message handlers

BOOL EchoCancel::OnInitDialog() 
{

    CDialog::OnInitDialog();

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    // Display the appropriate dialog title.
    if (m_EcanType == 0)
        SetWindowText("PCM Echo Cancel Configuration");
    else
        SetWindowText("Packet Echo Cancel Configuration");

    RedrawWindow ();
    return TRUE;
}

#define SetItemState(item,state) \
    if ((pObj = GetDlgItem(IDC_STATIC_##item)) != NULL) \
		pObj->EnableWindow(State);                        \
    if ((pObj = GetDlgItem(IDC_EDIT_##item)) != NULL) \
		pObj->EnableWindow(State);                        


void EchoCancel::RedrawWindow ()
{
	CWnd *pObj;			// pointer to Window object
   BOOL State;         // enable state

    // Enable / Disable Short Tail Echo Canceller dependent fields.
    State =  (!m_ShortTail);

    SetItemState (CROSSCORRLIMIT, State);
    SetItemState (NUMFIRSEGS, State);
    SetItemState (FIRSEGLENGTH, State);
    SetItemState (FIRTAPCHECK, State);

    // Initialize the display to the current values.
    CheckDlgButton(IDC_RADIO_NLPOFF, EcanConfig.NlpType == NlpOff);   
    CheckDlgButton(IDC_RADIO_NLPMUTE, EcanConfig.NlpType == NlpMute);
    CheckDlgButton(IDC_RADIO_NLPRAND, EcanConfig.NlpType == NlpRandom);
    CheckDlgButton(IDC_RADIO_NLPSUPP, EcanConfig.NlpType == NlpSpectral);
    CheckDlgButton(IDC_RADIO_NLPHOTH, EcanConfig.NlpType == NlpHoth);
    CheckDlgButton(IDC_RADIO_NLPSUPPAUTO, EcanConfig.NlpType == NlpSuppAuto);

 	 CheckDlgButton (IDC_CHECK_165DETENABLE, EcanConfig.G165DetectEnable);
 	 CheckDlgButton (ChkTandemOperation, EcanConfig.TandemOperationEnable);
 	 CheckDlgButton (ChkEchoSource,      EcanConfig.EchoSourceAttached);
 	 CheckDlgButton (ChkReconvergence,   EcanConfig.ReconvergenceCheck);
    
    SetDlgItemInt (IDC_EDIT_TAPLENGTH,     EcanConfig.TapLength/8, UNSIGNED);
    SetDlgItemInt (IDC_EDIT_ADAPTLIMIT,    EcanConfig.AdaptLimit, UNSIGNED);
    SetDlgItemInt (IDC_EDIT_NLPTHRESHOLD,  EcanConfig.NlpThrehold, UNSIGNED);
    SetDlgItemInt (IDC_EDIT_CNGTHRESHOLD,  0 - EcanConfig.CngThreshold, SIGNED);
    SetDlgItemInt (IDC_EDIT_DBLTALKTHRESH, EcanConfig.DblTalkThreshold, UNSIGNED);
    SetDlgItemInt (IDC_EDIT_MAX_DBLTALKTHRESH, EcanConfig.MaxDblTalkThreshold, UNSIGNED);

    SetDlgItemInt (IDC_EDIT_NLPCONVERGE,    EcanConfig.NlpUpperThresConv, UNSIGNED);
    SetDlgItemInt (IDC_EDIT_NLPNONCONVERGE, EcanConfig.NlpUpperThresUnConv, UNSIGNED);
    SetDlgItemInt (IDC_EDIT_NLPMAXSUPPRESS, EcanConfig.NlpMaxSupp, UNSIGNED);

    if (!m_ShortTail)  {
        SetDlgItemInt(IDC_EDIT_CROSSCORRLIMIT, EcanConfig.CrossCorrLimit, UNSIGNED);
        SetDlgItemInt(IDC_EDIT_NUMFIRSEGS,     EcanConfig.NumFirSegments, UNSIGNED);
        SetDlgItemInt(IDC_EDIT_FIRSEGLENGTH,   EcanConfig.FirSegmentLength/8, UNSIGNED);
        SetDlgItemInt(IDC_EDIT_FIRTAPCHECK,    EcanConfig.FirTapCheckPeriod/8, UNSIGNED);
    }

    CDialog::RedrawWindow ();
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//
// OK button hit - Validate and save entered parameters.
//
void EchoCancel::OnOK() 
{
    BOOL ValidInt;  // flag indicating valid integer

    int TailLength, AdaptationFreq, NlpMaxThreshold, CngThreshold, DoubleTalkThreshold, MaxDoubleTalkThreshold;
    int CrossCorrelationFreq, EchoNumSegments, EchoSegmentLength, EchoSegmentSearch;
    int NlpMinConverge, NlpMinPreConverge, NlpMaxSuppress, minTapLength, maxTapLength;
    
    char msg[100];

   CrossCorrelationFreq = EchoNumSegments = EchoSegmentLength = EchoSegmentSearch = 0;

    // Get and validate the selected NLP Type.
	if (IsDlgButtonChecked(IDC_RADIO_NLPOFF))         EcanConfig.NlpType = NlpOff;
	else if (IsDlgButtonChecked(IDC_RADIO_NLPMUTE))   EcanConfig.NlpType = NlpMute;
	else if (IsDlgButtonChecked(IDC_RADIO_NLPRAND))   EcanConfig.NlpType = NlpRandom;
	else if (IsDlgButtonChecked(IDC_RADIO_NLPHOTH))   EcanConfig.NlpType = NlpHoth;
	else if (IsDlgButtonChecked(IDC_RADIO_NLPSUPP))   EcanConfig.NlpType = NlpSpectral;
	else if (IsDlgButtonChecked(IDC_RADIO_NLPSUPPAUTO)) EcanConfig.NlpType = NlpSuppAuto;
   else    {
		AfxMessageBox("The NLP Type is invalid!");
        return;
    }

    // Get the selected G.165 Detect Enable state.
    EcanConfig.G165DetectEnable      = IsDlgButtonChecked(IDC_CHECK_165DETENABLE);
    EcanConfig.TandemOperationEnable = IsDlgButtonChecked(ChkTandemOperation);
 	 EcanConfig.EchoSourceAttached    = IsDlgButtonChecked(ChkEchoSource);
 	 EcanConfig.ReconvergenceCheck    = IsDlgButtonChecked(ChkReconvergence);


    // Get and validate the entered Tap Length value (ms).
    if (m_ShortTail) {
      minTapLength = 8;
      maxTapLength = 32;
    } else {
      minTapLength = 32;
      maxTapLength = 512;
    }
	TailLength = (int) GetDlgItemInt(IDC_EDIT_TAPLENGTH, &ValidInt, UNSIGNED);
   ItemRangeCheck (minTapLength, TailLength, maxTapLength);
    if ((TailLength % minTapLength) != 0)   {
       sprintf (msg, "TAP Length must be an integer multiple of %d", minTapLength);
		 AfxMessageBox (msg);
        return;
    }

    // Get and validate the entered Adapt Limit value.
	AdaptationFreq = (int) GetDlgItemInt(IDC_EDIT_ADAPTLIMIT, &ValidInt, UNSIGNED);
   ItemRangeCheck (MIN_EC_ADAPT_LIMIT, AdaptationFreq, MAX_EC_ADAPT_LIMIT);

    // Get and validate the entered NLP Threshold value.
   NlpMaxThreshold = (int) GetDlgItemInt(IDC_EDIT_NLPTHRESHOLD, &ValidInt, UNSIGNED);
   ItemRangeCheck (MIN_EC_NLP_THRESH, NlpMaxThreshold, MAX_EC_NLP_THRESH);

   // Get and validate the entered CNG Threshold value.
   // NOTE: Negate because echo canceller expected -dBm
	CngThreshold = (int) GetDlgItemInt(IDC_EDIT_CNGTHRESHOLD, &ValidInt, SIGNED);
   ItemRangeCheck (0-MAX_EC_CNG_THRESH, CngThreshold, 0-MIN_EC_CNG_THRESH);

    // Get and validate the entered Double Talk Threshold value.
	DoubleTalkThreshold = (int) GetDlgItemInt(IDC_EDIT_DBLTALKTHRESH, &ValidInt, UNSIGNED);
   ItemRangeCheck (MIN_EC_DBL_TALK, DoubleTalkThreshold, MAX_EC_DBL_TALK);

    // Get and validate the entered Max Double Talk Threshold value.
	MaxDoubleTalkThreshold = (int) GetDlgItemInt(IDC_EDIT_MAX_DBLTALKTHRESH, &ValidInt, UNSIGNED);
   ItemRangeCheck (DoubleTalkThreshold, MaxDoubleTalkThreshold, MAX_EC_MAX_DBL_TALK);

   
    // Get and validate the entered NLP Converge Threshold value.
	NlpMinConverge = (int) GetDlgItemInt(IDC_EDIT_NLPCONVERGE, &ValidInt, UNSIGNED);
   ItemRangeCheck (MIN_EC_NLP_CONVERGE, NlpMinConverge, MAX_EC_NLP_CONVERGE);

   // Get and validate the entered NLP NonConverge Threshold value.
	NlpMinPreConverge = (int) GetDlgItemInt(IDC_EDIT_NLPNONCONVERGE, &ValidInt, UNSIGNED);
   ItemRangeCheck (MIN_EC_NLP_NONCONVERGE, NlpMinPreConverge, MAX_EC_NLP_NONCONVERGE);

    // Get and validate the entered NLP Noise suppression value.
	NlpMaxSuppress = (int) GetDlgItemInt(IDC_EDIT_NLPMAXSUPPRESS, &ValidInt, UNSIGNED);
   ItemRangeCheck (MIN_EC_NLP_MAXSUPP, NlpMaxSuppress, MAX_EC_NLP_MAXSUPP);

   // Get and validate Long Tail variant only parameters.
    if (!m_ShortTail)  {

        // Get and validate the entered Cross Correlation Limit value.
	    CrossCorrelationFreq = (int) GetDlgItemInt(IDC_EDIT_CROSSCORRLIMIT, &ValidInt, UNSIGNED);
       ItemRangeCheck (MIN_EC_CROSSCOR_LMT, CrossCorrelationFreq, MAX_EC_CROSSCOR_LMT);

        // Get and validate the entered Number Of FIR Segments value.
	    EchoNumSegments = (int) GetDlgItemInt(IDC_EDIT_NUMFIRSEGS, &ValidInt, UNSIGNED);
       ItemRangeCheck (MIN_EC_NUM_FIR_SEGS, EchoNumSegments, MAX_EC_NUM_FIR_SEGS);

        // Get and validate the entered FIR Segment Length value.
	    EchoSegmentLength = (int) GetDlgItemInt(IDC_EDIT_FIRSEGLENGTH, &ValidInt, UNSIGNED);
       ItemRangeCheck (MIN_EC_FIR_SEG_LEN/8, EchoSegmentLength, MAX_EC_FIR_SEG_LEN/8);

        // Get and validate the entered FIR Segment Length value.
	    EchoSegmentSearch = (int) GetDlgItemInt(IDC_EDIT_FIRTAPCHECK, &ValidInt, UNSIGNED);
       ItemRangeCheck (MIN_EC_FIR_TAP_CHECK/8, EchoSegmentSearch, MAX_EC_FIR_TAP_CHECK/8);    
    } else {
       CrossCorrelationFreq = EcanConfig.CrossCorrLimit;
       EchoNumSegments = 1;
       EchoSegmentLength = TailLength;
       EchoSegmentSearch = EcanConfig.FirTapCheckPeriod;
    }

	EcanConfig.TapLength = TailLength * 8;
	EcanConfig.DblTalkThreshold   = DoubleTalkThreshold;
	EcanConfig.MaxDblTalkThreshold = MaxDoubleTalkThreshold;
	EcanConfig.AdaptLimit = AdaptationFreq;

   EcanConfig.NumFirSegments     = EchoNumSegments;
   EcanConfig.FirSegmentLength   = EchoSegmentLength * 8;
   EcanConfig.FirTapCheckPeriod  = EchoSegmentSearch * 8;
   EcanConfig.CrossCorrLimit     = CrossCorrelationFreq;

   EcanConfig.NlpThrehold = NlpMaxThreshold;
	EcanConfig.NlpUpperThresConv  = NlpMinConverge;
	EcanConfig.NlpUpperThresUnConv = NlpMinPreConverge;
	EcanConfig.NlpMaxSupp         = NlpMaxSuppress;
	EcanConfig.CngThreshold       = 0-CngThreshold;

    // Do the base class's method to exit the dialog with success.
	CDialog::OnOK();
}
void EchoCancel::SetDefaults() 
{

    // Different defaults for Short Tail or PCM.
   if ((m_ShortTail) || (m_EcanType == 0)) {
      if (m_ShortTail)
         EcanConfig.TapLength      = 128;
      else
         EcanConfig.TapLength      = 256;

      EcanConfig.NumFirSegments    =   1;
      EcanConfig.FirSegmentLength  = 128;
      EcanConfig.FirTapCheckPeriod =  80;
   } else {
      EcanConfig.TapLength         = 1024;
      EcanConfig.NumFirSegments    =    3;
      EcanConfig.FirSegmentLength  =   64;
      EcanConfig.FirTapCheckPeriod =   80;
   }

   EcanConfig.NlpType     = NlpHoth;
	EcanConfig.AdaptEnable      = TRUE;
	EcanConfig.G165DetectEnable = TRUE;
   
	EcanConfig.DblTalkThreshold    = 6;
   EcanConfig.MaxDblTalkThreshold = 40;
   
	EcanConfig.NlpThrehold          = 24;
	EcanConfig.NlpUpperThresConv    = 18;
	EcanConfig.NlpUpperThresUnConv  = 12;
	EcanConfig.NlpMaxSupp           = 10;
   
	EcanConfig.CngThreshold = 43;
   EcanConfig.AdaptLimit   = 50;   // % of frame size
   EcanConfig.CrossCorrLimit = 15; // % of frame size

   EcanConfig.TandemOperationEnable = FALSE; 
   EcanConfig.EchoSourceAttached    = TRUE;
   EcanConfig.ReconvergenceCheck    = TRUE;

}


void EchoCancel::OnDefaults() 
{
   SetDefaults ();
   RedrawWindow ();
	
}
