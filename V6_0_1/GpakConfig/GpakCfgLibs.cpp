#include <stdafx.h>

#include "GpakConfig.h"
#pragma warning(disable:4996)  // Disable 'unsafe' warnings

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "sysconfig.h"
#include "sysmem.h"
#include "DirDialog.h"

enum fileType { STUB = 0, PRODUCTION, DEMO, NOT_FOUND };
enum headerOpen { OVERWRITE, NEW_ONLY };

extern char RootFileName[];
extern char GpakPath[];

extern DspTypeDepInfo_t *DspInfo;

extern ADT_UInt16 SRTPInstances;
extern ADT_UInt16 SrcInstances;
extern ADT_UInt16 CEDDetInstances;
extern ADT_UInt16 CNGDetInstances;
extern ADT_UInt16 ARBDetInstances;
extern int ARBCfgCount;
extern int ToneDetInstances;

extern ADT_UInt16 NumT38Instances;
extern ADT_UInt16 NumTIT38Instances;
extern int NumGpakConferences;
extern int NumCustomConferences;


extern NCAN_Params_t NoiseSuppression;
extern char CompPath[], StubPath[];

extern ADT_UInt16 shortTailEC;
extern ADT_UInt16 longTailEC;

//
//  These string pointers are set according to the DSP memory layout and 
//  determine where the linker will place vocoder and component sections
//
extern char *FastData, *FastConst, *cinitData, *SlowDataFirst, *Daram, *DaramFirst;          // pointer to a string
extern char *InstanceData;
extern char *SlowData, *SlowConst, *MediumData;
extern char *PerCoreData, *Core0Data, *bssData, *HostData, *Core1OnlyData, *NonCacheData;
extern char *InternalProg, *FastProg, *MediumProg,  *SlowProg;

extern char *InternalMem, *ExtDmaArea, *MemArea10Ms, *MemArea20Ms, *MemArea22Ms, *MemArea30Ms;
extern ADT_Bool IsC54, IsC55, IsC64, IsC64Plus;
extern char  *platform, *libExt;

extern sysConfig_t SysCfg;

// AEC library related variables.
int aecLibVersion = 0;          // AEC library version

//{  #defines
#define ENABLED_TEXT  "ENABLED"
#define DISABLED_TEXT "             DISABLED"
#define DEMO_TEXT     "        DEMO"
//}



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{                       Component info
// 
//  Identifies configurable components, version, section allocation information and link files
typedef struct userHdrs_t {
   char *key;     // A null indicates not to append library version to generated header string
   char *defVar;
   char *header;
} userHdrs_t;

userHdrs_t userHdrs[] = {
// Vocoders
{ NULL,  "inc_g711_user",     "G711" },
{ NULL,  "inc_g722_ADT_user", "G722_user_v21" },
{ NULL,  "inc_speex_ADT_user","speex_user_v0102" },
{ NULL,  "inc_melpe_user",    "melpe_user_v0101" },
{ NULL,  "inc_amr_user",      "amr_adt_user_v0205" },
// Algorithms
{ NULL,   "inc_aeq_user",     "adtEq" },
{ "AEC",  "inc_aec_user",     "iaecg4" },
{ "AGC",  "inc_agc_user",     "agc_user" },
{ "ARB",  "inc_arbTd_user",   "arbit_toneDt_user" },
{ "CED",  "inc_FAXCEDdet",    "FaxCEDdet" },
{ "CNG",  "inc_FAXCNG_user",  "FaxCNG_user" },
{ "CNG",  "inc_FAXCNG",       "FaxCNG" },
{ "CNFR", "inc_conf_ADT_user","conf_ADT_user" },
{ "DTMF", "inc_tonedet_user", "tonedet_user" },
{ "GAIN", "inc_AdtGainSum",   "AdtGainSum" },
//{ "LEC",  "inc_g168_user",    "g168_user" },
{ "NEC",  "inc_g168_user",    "g168_user" },
{ "NSPR", "inc_NCAN_user",    "NCAN_user" },
{ "RCID", "inc_adtCidUser",   "adtCidUser" },
{ "RDET", "inc_tonerelayDet", "tonerelayDet" },
{ "RGEN", "inc_tonerelayGen", "tonerelayGen" },
{ "RTP",  "inc_rtpapi_user",  "rtpuser" },
{ "RTP",  "inc_rtpapi",       "rtpapi" },
{ "SRC",  "inc_src2_ADT_user","rateConvert_user" },
{ "SRTP", "inc_srtp_ADT_user","srtp_user" },
{ "STK",  "inc_ipStackUser",  "ipStackUser" },
{ "T38",  "inc_faxrelay",     "faxrelay" },
{ "T38t", "inc_ti_t38_user",  "ti_t38_user" },
{ "TCID", "inc_adtCidUser",   "adtCidUser" },
{ "TGEN", "inc_TG_USER",      "TG_USER" },
{ "VAD",  "inc_vadcng_user",  "vadcng_user" },
{ "TAS",  "inc_tasdet_user",  "tas_user" },
{ NULL, NULL }
};


typedef struct CompKey_t {
   char        *key;           // NOTE:   Must be string of three or four characters
   char        *ID;
   int          cfgMask;
   ADT_UInt16  *instanceCnt;
   char        *wrapper;
   int          version;
   long         sectDescr;    // File seek pointer for section info.
   enum fileType  activeFile;   
} CompKey_t;

CompKey_t Components[] = {
{ "AEC",  "Acoustic Echo Canceller", 0, &SysCfg.AECInstances,       NULL,      NULL },
{ "AGC",  "AGC",                1<<31,  &SysCfg.numAGCChans,        NULL,      NULL },
{ "ARB",  "Generic Tone Detection", 0,  &ARBDetInstances,           NULL,      NULL },
{ "CED",  "CED",                    0,  &CEDDetInstances,           NULL,      NULL },
{ "CNFR", "Conferencing",           0,  &SysCfg.maxNumConferences, "GpakConf", NULL },
{ "CNG",  "CNG",                    0,  &CNGDetInstances,           NULL,      NULL },
{ "CPRG", "CPRG",               1<<24,  NULL,                       NULL,      NULL },
{ "DTMF", "DTMF",               1<<28,  NULL,                       NULL,      NULL },
{ "GAIN", "Fixed Gain",             0,  &SysCfg.maxNumChannels,     NULL,      NULL },
// JDC NEC covers short and longtail EC { "LEC",  "G168A",                  0,  &shortTailEC,               NULL,      NULL },
{ "MFR1", "MFR1",               1<<27,  NULL,                       NULL,      NULL },
{ "MF2F", "MFR2F",              1<<26,  NULL,                       NULL,      NULL },
{ "MF2R", "MFR2R",              1<<25,  NULL,                       NULL,      NULL },
{ "NEC",  "G168",                   0,  &longTailEC,                NULL,      NULL },
{ "NSPR", "Noise Suppression",   1<<8,  NULL,                      "NoiseSuppress",  NULL },
{ "RCID", "Rx Caller Id",           0,  &SysCfg.numRxCidChans,     "GpakCID",  NULL },
{ "RDET", "Relay Detection",    1<<22,  NULL,                       NULL,      NULL },
{ "RGEN", "Relay Generation",   1<<23,  NULL,                       NULL,      NULL },
{ "RTCV", "Rate conversion (conf)", 0,  &SysCfg.maxNumConferences,  NULL,      NULL }, 
{ "RTP",  "RTP",                 3<<3,  NULL,                      "64xRtpSupport",  NULL },
{ "SRC",  "Sampling Rate Converter", 0,  &SrcInstances,             NULL,      NULL },
{ "SRTP", "Secure RTP",             0,  &SRTPInstances,             NULL,      NULL },
{ "STK",  "Network Stack",       1<<2,  NULL,                      "ipStackSupport",  NULL },
{ "T38",  "T38FaxRelay",            0,  &NumT38Instances,          "FaxRelay", NULL },
{ "T38t", "T38FaxRelay[TI]",        0,  &NumTIT38Instances,        "ti_t38",   NULL },
{ "TCID", "Tx Caller Id",           0,  &SysCfg.numTxCidChans,     "GpakCID",  NULL },
{ "TGEN", "Tone Generation",        0,  &SysCfg.numTGChans,         NULL,      NULL },
{ "VAD",  "Voice Detect",       1<<30,  NULL,                       NULL,      NULL },
{ "TAS",  "TAS tone Detect",    1<<7,   &SysCfg.numRxCidChans,      NULL,      NULL },
{ NULL }
};

//}
#define algBits cfg->enab.Bits
static char *ComponentDefFile;
static char RootDirectory[100] = { 0 };
static char DescriptorDirectory [100];
static char *HeaderExt;

static char *activeLibs   = NULL;
static unsigned int   activeLibsI8 = 0;

#define VERSION "V6_0"

// Find Component entry for specified key value
#define charAsInt(val) (*((ADT_UInt32 *)val))
CompKey_t *matchComponentKey (char *key) {
   CompKey_t *Component;

    // Find component with matching key entry
   for (Component = Components; Component->key != NULL; Component++) {
      if (charAsInt(key) == charAsInt(Component->key)) return Component;
   }
   return NULL;
}


void obtainGpakPath(void)  {
      CDirDialog cfdlg;

      cfdlg.m_ofn.lpstrTitle = _T ("Select G.PAK Root Path");
      cfdlg.m_ofn.lpstrInitialDir = GpakPath;
      cfdlg.m_strFilter = _T("Definition Files|*.DEF|Folders Only|.|All Files|*.*||");
      cfdlg.m_strFilter.Replace('|', '\0');
      if (IDOK==cfdlg.DoModal()) strcpy (GpakPath, (LPCTSTR) cfdlg.m_rawPath);
}

// Generate GPAK header files from component library version numbers.
void generateGPAKHeader (int openCondition) {
   FILE *fpHdr;
   CDirDialog cfdlg;
   CompKey_t *Component;
   userHdrs_t *hdrDesc;
   char HeaderName[130];
   char Buffer [200];
   int Value;

   if (GpakPath[0] == 0)
        obtainGpakPath();

   if (SysCfg.DspType == 674)  HeaderExt = ".h674";
   else if (SysCfg.DspType == 665)   HeaderExt = ".h665";
   else if (SysCfg.DspType == 667)   HeaderExt = ".h667";
   else if (IsC64Plus)         HeaderExt = ".h64p";
   else if (IsC64)             HeaderExt = ".h64";
   else if (IsC55)             HeaderExt = ".h55";
   else if (IsC54)             HeaderExt = ".h54";
   sprintf (HeaderName, "%sIncludes\\%s%s", GpakPath, VERSION, HeaderExt);
   if (openCondition == NEW_ONLY) {
      fpHdr = fopen ((const char *) HeaderName, (const char *) "r");
      if (fpHdr != NULL) {
         fclose (fpHdr);
         return;
      }
   }

   // Prompt for generation of new header includes file
   Value = AfxMessageBox("Generate new version include file?",
                         MB_YESNO | MB_ICONQUESTION);

   if (Value != IDYES) return;

   sprintf (Buffer, "Header Name is %s.", HeaderName);
   AfxMessageBox (Buffer);
   Value = AfxMessageBox("Change GpakPath?",
                         MB_YESNO | MB_ICONQUESTION);
   if (Value == IDYES) {
        obtainGpakPath();
        if (SysCfg.DspType == 674)  HeaderExt = ".h674";
        else if (SysCfg.DspType == 665)  HeaderExt = ".h665";
        else if (SysCfg.DspType == 667)  HeaderExt = ".h667";
        else if (IsC64Plus)         HeaderExt = ".h64p";
        else if (IsC64)             HeaderExt = ".h64";
        else if (IsC55)             HeaderExt = ".h55";
        else if (IsC54)             HeaderExt = ".h54";
        sprintf (HeaderName, "%sIncludes\\%s%s", GpakPath, VERSION, HeaderExt);
   }

   fpHdr = fopen ((const char *) HeaderName, (const char *) "w");

   if (fpHdr == NULL) {
        sprintf (Buffer, "Failed to open %s for write.",HeaderName);
        AfxMessageBox (Buffer);
        return;
   }

   fprintf (fpHdr, "#ifndef %s_H\n", VERSION);
   fprintf (fpHdr, "#define %s_H\n", VERSION);
   if (SysCfg.type2CID) fprintf (fpHdr, "#define TAS_ENABLE_TYPE_II 1\n");

   // Scan through all header descriptors and match against component for version.
   for (hdrDesc=userHdrs; hdrDesc->defVar != NULL; hdrDesc++) {
      if (hdrDesc->key == NULL) {
         fprintf (fpHdr, "#define %-30s \"%s.h\"\n", hdrDesc->defVar, hdrDesc->header);
         continue;
      }
      Component = matchComponentKey (hdrDesc->key);
      if (Component == NULL) {
         fprintf (fpHdr, "#define %-30s \"stub.h\"\n", hdrDesc->defVar);
         continue;
      }
      if (Component->version == 0) continue;

      if ((SysCfg.DspType == 665) || (SysCfg.DspType == 667)) {
        if (strcmp(hdrDesc->key, "RTP") == 0) {
            fprintf (fpHdr, "#define %-30s \"./rtp_v%04x/include/%s.h\"\n", hdrDesc->defVar, Component->version, hdrDesc->header);
            continue;
        }
        if (strcmp(hdrDesc->key, "STK") == 0) {
            fprintf (fpHdr, "#define %-30s \"%s.h\"\n", hdrDesc->defVar, hdrDesc->header);
            continue;
        }
        if (strcmp(hdrDesc->key, "T38") == 0) {
            fprintf (fpHdr, "#define %-30s \"./faxrelay_v%04x/%s.h\"\n", hdrDesc->defVar, Component->version, hdrDesc->header);
            continue;
        }
      }

      fprintf (fpHdr, "#define %-30s \"%s_v%04x.h\"\n", hdrDesc->defVar, hdrDesc->header, Component->version);
#if 1
      if (strcmp(hdrDesc->key, "AEC") == 0)
      {
         fprintf(fpHdr, "#define AEC_LIB_VERSION 0x%04x\n", Component->version);
      }
#endif
   }
   fprintf (fpHdr, "#endif\n");
   fclose (fpHdr);
   sprintf (Buffer, "New %s file created.", HeaderName);
   AfxMessageBox (Buffer);
}


// Append library to list of active libraries.  Reallocate memory if required.
void addToActiveList (char *fileName, char *ext) {
   char buff[200];
   sprintf (buff, "-l \"%s%s\"\n", fileName, ext);
   if (activeLibsI8 < strlen (activeLibs) + strlen (buff)) {
      activeLibsI8 += 500;
      activeLibs = (char *) realloc (activeLibs, activeLibsI8);
   }
   strcat (activeLibs, buff);
}

FILE *OpenFile (char *FileCategory, char *InitialDir, int InitDirI8, char *FileName, char *attrib) {

   CDirDialog cfdlg;
   char FullFile[130];
   char Title[80];
   FILE *fp;

   fp = fopen (FileName, attrib);
   if (fp != NULL) return fp;

   sprintf (FullFile, "%s\\%s", InitialDir, FileName);
   fp = fopen (FullFile, attrib);
   if (fp != NULL) return fp;

   sprintf (Title, "Searching for %s", FileName);

   cfdlg.m_ofn.lpstrTitle = Title;
   if (InitialDir == NULL) cfdlg.m_ofn.lpstrInitialDir = ".";
   else                    cfdlg.m_ofn.lpstrInitialDir = InitialDir;

   cfdlg.m_strFilter = CString (FileCategory) + "|" + FileName + _T("|All Files|*.*||");
   cfdlg.m_strFilter.Replace('|', '\0');
   cfdlg.m_ofn.lpstrFilter =  cfdlg.m_strFilter.GetBuffer(cfdlg.m_strFilter.GetLength());

   do {
      if (IDOK!=cfdlg.DoModal()) return NULL;
      strncpy (FullFile, (LPCTSTR) cfdlg.m_ofn.lpstrFile, sizeof (FullFile));
      fp = fopen (FullFile, attrib);
   } while (fp == NULL);
   strncpy (InitialDir, FullFile, InitDirI8);
   InitialDir = strrchr (InitialDir, '\\');
   if (InitialDir != NULL) *InitialDir = 0;
   return fp;
}

// Search for component libraries and identify which are active
char *identifyActiveComponents (sysConfig_t *cfg, char *rptBuff) {

   FILE *cfp, *fp;
   long pos;
   char *format;
   ADT_Bool active;
   CompKey_t *Component;
   char buffer[200];
   char FileName [200];
   int tr_found = 0;

   char *key, *vers, *libName, *demoName, *tmpStr;

   if (activeLibs == NULL) {
      activeLibs = (char *) malloc (500);
      activeLibsI8 = 500;
      activeLibs[0] = 0;
   }

   // Open component defintion file
   if (DspInfo->DspType == Ti674) {
      ComponentDefFile = "GPAK6740Components.DEF";
   } else if (DspInfo->DspType == Ti665) {
      ComponentDefFile = "GPAK66Components.DEF";
   } else if (DspInfo->DspType == Ti667) {
      ComponentDefFile = "GPAK66Components.DEF";
   } else if (IsC64Plus) {
      ComponentDefFile = "GPAK64PlusComponents.DEF";
   } else if (IsC64) {
      ComponentDefFile = "GPAK64Components.DEF";
   } else if (IsC55) {
      ComponentDefFile = "GPAK55Components.DEF";
   } else {
      ComponentDefFile = "GPAK54Components.DEF";
   }
   strncpy (DescriptorDirectory, RootDirectory, sizeof (DescriptorDirectory));
   if (DescriptorDirectory[0] == 0) {
      // Back up to G.PAK base directory for descriptors
      strncpy (DescriptorDirectory, RootFileName, sizeof (DescriptorDirectory));
      tmpStr = strrchr (DescriptorDirectory, '\\');
      if (tmpStr) *tmpStr = 0;
      tmpStr = strrchr (DescriptorDirectory, '\\');
      if (tmpStr) *tmpStr = 0;
      strcat (DescriptorDirectory, "\\");
   }

   cfp = OpenFile ("Descriptors", DescriptorDirectory, sizeof (DescriptorDirectory), ComponentDefFile, "r");
   if (cfp == NULL) {
      rptBuff += sprintf (rptBuff, "%s not found", ComponentDefFile);
      return rptBuff;
   }

   // No echo cancellers, disable short tail to avoid library inclusion
   // Long tail version, enable Pkt canceller to force library inclusion
   if ((cfg->numPcmEcans + cfg->numPktEcans) == 0) {
      algBits.g168VarAEnable = 0;
   } else if (algBits.g168VarAEnable == 0) {
      algBits.g168PktEnable = 1;
   }
   algBits.agcEnable = cfg->numAGCChans != 0;

   // Library dependencies.
   //    AEC requires AGC
   //    CNFR requires AGC and VAD
   if (cfg->AECInstances)
      algBits.agcEnable = 1;

   if (cfg->maxNumConferences != 0) {
      algBits.agcEnable = 1;
      algBits.vadCngEnable = 1;
   }

   // Initialize component information
   for (Component = Components; Component->key != NULL; Component++) {
      Component->activeFile = NOT_FOUND;
      Component->version    = 0;
      Component->sectDescr = 0;
   }

   // ---------------------------------------------------------------------------
   //  Parse component definition file
   //  Match component key against known components.
   //  Add active components to activeLibs string
   Component = NULL;
   do {

      pos = ftell (cfp);
      if (fgets (buffer, sizeof (buffer), cfp) == NULL) break;
      strtok (buffer, "\r\n");    // Strip end-of-line characters
      if (buffer[0] == '#') continue;  // Comment line -> skip.
      if (buffer[0] == '+') {          // Start of section data -> store file position and skip past continuations
         if ((Component != NULL) && (Component->activeFile != STUB) && (Component->sectDescr == 0))
            Component->sectDescr = pos + 1;
         while (buffer[strlen(buffer)-1] == '\\') {
            if (fgets (buffer, sizeof (buffer), cfp) == NULL) break;
            strtok (buffer, "\r\n");    // Strip end-of-line characters
         }
         continue;
      }
      // Parse string for tag, version, libname, and demoname
      key  = strtok (buffer, ", \r\n");
      vers = strtok (NULL, ", \r\n");
      libName  = strtok (NULL, ", \r\n");
      demoName = strtok (NULL, ", \r\n");
      if (key == NULL) continue;  // Blank

      Component = matchComponentKey (key);
      if (Component == NULL) {
         rptBuff += sprintf (rptBuff, "Unknown component key %s in %s\r\n", key, ComponentDefFile);
         continue;
      }
      Component->version = strtol (vers, NULL, 16);

      // Save the version of the AEC library.
      if (strcmp(Component->key, "AEC") == 0)
      {
         aecLibVersion = Component->version;
      }

      if ((DspInfo->DspType == Ti665) || ((DspInfo->DspType == Ti667))) {
         if (strcmp(Component->key, "STK") == 0) {
            Component->activeFile = PRODUCTION;
            continue;
         }
      }

      // only include tone relay library once
      if ((DspInfo->DspType == Ti665) || ((DspInfo->DspType == Ti667))) {
         if (strcmp(Component->key, "RDET") == 0) {
            if (tr_found) {
                Component->activeFile = PRODUCTION;
                continue;
            } else
                tr_found = 1;
         }
      }
      if ((DspInfo->DspType == Ti665) || ((DspInfo->DspType == Ti667))) {
         if (strcmp(Component->key, "RGEN") == 0) {
            if (tr_found) {
                Component->activeFile = PRODUCTION;
                continue;
            } else
                tr_found = 1;
         }
      }

      //  Skip inactive components
      active = (Component->cfgMask & cfg->enab.LongWord) ||
               ((Component->instanceCnt != NULL) && (*(Component->instanceCnt) != 0));
      if (!active) {
         Component->activeFile = STUB;
         continue;
      }

      // Find release or demo library for active components and add to activeLibs list
      if (libName == NULL) {
         rptBuff += sprintf (rptBuff, "No library specified for %s.\r\n", Component->ID);
         continue;
      }

      // Check if production version is available
      sprintf (FileName, "%s\\%s", CompPath, libName);
      fp = fopen (FileName, "r");
      if (fp != 0)  {
         fclose (fp);
         Component->activeFile = PRODUCTION;
         addToActiveList (libName, "");
         if (Component->wrapper == NULL) continue;
         addToActiveList (Component->wrapper, platform);       
         continue;
      }

      // Check if demo version is available
      if (demoName != NULL) {
         sprintf (FileName, "%s\\%s", CompPath, demoName);
         fp = fopen (FileName, "r");
         if (fp != 0) {
            fclose (fp);
            Component->activeFile = DEMO;
            addToActiveList (demoName,  "");
            if (Component->wrapper == NULL) continue;
            addToActiveList (Component->wrapper, platform);       
            rptBuff += sprintf (rptBuff, "The demostration version of %s has been selected.\r\n", Component->ID);
            continue;
         } 
      }
      rptBuff += sprintf (rptBuff, "Library %s not found.\r\n", libName);
   } while (!feof (cfp));
   fclose (cfp);

   // ---------------------------------------------------------------------------
   //     Identify selected components without component definition file entry
   for (Component = Components; Component->key != NULL; Component++) {

      format = "No component entry exists for %s. This function has been rendered inactive.\r\n";

      // Component not requested -> use stub
      if (Component->activeFile != NOT_FOUND) continue;

      rptBuff += sprintf (rptBuff, format, Component->ID);
   }

   generateGPAKHeader (NEW_ONLY);
   return rptBuff;
}

// Include active component library names in <project>.cmd
void addComponentsToCmdFile (FILE *fp) {
   if (activeLibs == NULL) return;
   fprintf (fp, activeLibs);
   fprintf (fp, "\n");
   free (activeLibs);
   activeLibs = NULL;
   activeLibsI8 = 0;
}

// Write component library status to <project>.c
void writeComponentStatus (FILE *fp) {
   char *str1;
   CompKey_t *Component;
   for (Component = Components; Component->key != NULL; Component++) {
      if (Component->activeFile == PRODUCTION)
         str1 = ENABLED_TEXT;
      else if (Component->activeFile == DEMO)
         str1 = DEMO_TEXT;
      else
         str1 = DISABLED_TEXT;  

      fprintf (fp,"\t%-30s [%4x]\t\t\t= %s\n", Component->ID, Component->version, str1);
   }
   fprintf (fp,"\n");
}

// Write component library status to <project>.c
void writeComponentVersions (FILE *fp) {
   CompKey_t *Component;
   for (Component = Components; Component->key != NULL; Component++) {
      fprintf (fp,"const unsigned int %s_SWVER=    0x%04X;\n", Component->key, Component->version);
   }
   fprintf (fp,"\n");
}
// Write component specified sections to <project>Cust.cmd
void writeComponentSectionDescriptors (FILE *fp) {
   FILE *cfp;
   CompKey_t *Component;
   char Buffer[100], *buffer;
   char *sectName, *sectDescr;

   cfp = OpenFile ("Descriptors", DescriptorDirectory, sizeof (DescriptorDirectory), ComponentDefFile, "r");
   if (cfp == NULL) return;

   for (Component=Components; Component->key != NULL; Component++) {
      if (Component->sectDescr == 0) continue;

      // Goto location of the section data within the component definition file
      fseek (cfp, Component->sectDescr, SEEK_SET);

      buffer = fgets (Buffer, sizeof (Buffer), cfp);
      if (buffer == NULL) continue;

      do {
         // Parse out section name ID.
         sectName  = strtok (buffer, ", ");
         if (sectName == NULL) continue;
         sectDescr = strtok (NULL, "\r\n");

         // Match section name ID to active section name string.
         if (strcmp ("FastP", sectName) == 0)       sectName = FastProg;
         else if (strcmp ("MedP",   sectName) == 0) sectName = MediumProg;
         else if (strcmp ("SlowP",  sectName) == 0) sectName = SlowProg;
         else if (strcmp ("IntlP",  sectName) == 0) sectName = InternalProg;
         else if (strcmp ("FastD",  sectName) == 0) sectName = FastData;
         else if (strcmp ("MedD",   sectName) == 0) sectName = MediumData;
         else if (strcmp ("SlowD",  sectName) == 0) sectName = SlowData;
         else if (strcmp ("Core0D", sectName) == 0) sectName = Core0Data;
         else if (strcmp ("InstD",  sectName) == 0) sectName = InstanceData;
         else if (strcmp ("FastC",  sectName) == 0) sectName = FastConst;
         else if (strcmp ("SlowC",  sectName) == 0) sectName = SlowConst;
         else sectDescr = NULL;

         // Copy section description to command file. '\' on end of line indicates continuation on next line
         fprintf (fp, "\n/*  %s memory requirements. */\n", Component->ID);
         fprintf (fp, "  ");

         while (sectDescr) {
            if (sectDescr[strlen(sectDescr)-1] != '\\') {
               // End of description.  Write to command file and exit loop
               // Append active section name string after section description.
               fprintf (fp, "  %s", sectDescr);
               if (IsC55)
                  fprintf (fp, "ALIGN(2)  %s\n", sectName);
               else 
                  fprintf (fp, "  %s\n", sectName);
               break;
            }
            // Continuation line.  Place <CR> at end and read next line.
            sectDescr[strlen(sectDescr)-1] = 0;
            fprintf (fp, "  %s\n", sectDescr);
            fgets (Buffer, sizeof (Buffer), cfp);
            sectDescr = strtok (Buffer, "\r\n");
         }
         // Check next line for new descriptor
         buffer = fgets (Buffer, sizeof (Buffer), cfp);
         if ((buffer == NULL) || (buffer[0] != '+')) break;
         buffer[0] = ' ';
      } while (ADT_TRUE);

   }
   fclose (cfp);

   generateGPAKHeader (OVERWRITE);

}
