#if !defined(AFX_EXTERNALMEM_H__55762EC1_D24D_42A1_A2CE_1030B07B36CD__INCLUDED_)
#define AFX_EXTERNALMEM_H__55762EC1_D24D_42A1_A2CE_1030B07B36CD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ExternalMem.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// ExternalMem dialog

class ExternalMem : public CDialog
{
// Construction
public:
	ExternalMem(CWnd* pParent = NULL);   // standard constructor
	void UpdateOverFlow();
// Dialog Data
	//{{AFX_DATA(ExternalMem)
	enum { IDD = IDD_EXTMEM_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ExternalMem)
	public:
	virtual int DoModal();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(ExternalMem)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnCached();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EXTERNALMEM_H__55762EC1_D24D_42A1_A2CE_1030B07B36CD__INCLUDED_)
