// GpakConfigDlg.h : header file
//

#if !defined(AFX_GPAKCONFIGDLG_H__890F3D94_435B_4C58_B1C9_D992E56B1F69__INCLUDED_)
#define AFX_GPAKCONFIGDLG_H__890F3D94_435B_4C58_B1C9_D992E56B1F69__INCLUDED_

#include "EchoCancel.h"	// Added by ClassView
#include "SerialPort.h"	// Added by ClassView
#include "ExternalMem.h"
#include "GpakCidDlg.h"
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CGpakConfigDlg dialog

class CGpakConfigDlg : public CDialog
{
// Construction
public:
	BOOL ParmFound(char *pString, char *pToken, int *pValue);
    BOOL StringFound(char *pString, char *pToken, char **pStr, size_t *Len);
	void ReadConfigFile(void);
	void WriteConfigFile(void);
	void UpdateMaxFrameSizeSelection();
	BOOL BuildGpakConfigFiles(char **pErrText);
	void UpdatePort3SlotsSelection();
	void UpdatePort2SlotsSelection();
	void UpdatePort1SlotsSelection();
	void UpdateG726LowMipsSelection();
	void UpdateG726LowMemSelection();
	void UpdateVadSelection();
	void UpdatePcktEcSelection();
	void UpdatePcmEcSelection();
	void UpdateAECSelection();
	void UpdateAgcSelection();
	void UpdatePktTypeSelection();
	void UpdateDspSelection(int DspType);
	void UpdateHostIFSelection(int HostIFValue);
	void UpdateConferenceSelection();
	void UpdateToneDetSelection();
   void UpdateNoiseSuppressionSelection ();
   void GetProjectInfo ();
   void CGpakConfigDlg::UpdateT38Instances ();
 	afx_msg void OnButtonPath();

	SerialPort m_SerialPort;
	EchoCancel m_EchoCancel;
	ExternalMem m_ExternalMem;
    GpakCidDlg m_CallerId;
	CGpakConfigDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CGpakConfigDlg)
	enum { IDD = IDD_GPAKCONFIG_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGpakConfigDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
   
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CGpakConfigDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnButtonPcmEcConfig();
	afx_msg void OnButtonPktEcConfig();
	afx_msg void OnButtonP1Config();
	afx_msg void OnButtonP2Config();
	afx_msg void OnButtonP3Config();
	afx_msg void OnButtonBuild();
	afx_msg void OnRadioAAL2Profile();
	afx_msg void OnRadioRtpProfile();
	afx_msg void OnCheckEcShortTail();
	afx_msg void OnCheckVadEnable();
	afx_msg void OnCheckG726LowMem();
	afx_msg void OnCheckG726LowMips();
	afx_msg void OnChangeEditP1NumSlots();
	afx_msg void OnChangeEditP2NumSlots();
	afx_msg void OnChangeEditP3NumSlots();
	afx_msg void OnCheckG72616k();
	afx_msg void OnCheckG72624k();
	afx_msg void OnCheckG72632k();
	afx_msg void OnCheckG72640k();
	afx_msg void OnCheckG723();
	afx_msg void OnCheckG728();
	afx_msg void OnCheckGSMEFR();
	afx_msg void OnCheckAdt4800();
	afx_msg void OnCheckG722();
	afx_msg void OnRadioRtpToneEvent();
	afx_msg void OnRadioRtpToneTone();
	afx_msg void OnRadioRtpToneNone();
	afx_msg void OnCheckFrame();
	afx_msg void OnChangeEditPcmecnum();
	afx_msg void OnKillFocusPcmecnum();
	afx_msg void OnChangeEditPktecnum();
	afx_msg void OnKillFocusPktecnum();
	afx_msg void OnCheckTonerlygenenab();
   afx_msg void OnCheckNoiseSuppressionEnable();
	afx_msg void OnCheckG729();
	afx_msg void OnButtonExtmemConfig();
	afx_msg void OnChangeEditAECecnum();
	afx_msg void OnKillFocusAECecnum();
	afx_msg void OnChangeEditNumt38Instances();
	afx_msg void OnUpdateFldAGCInstances();
	afx_msg void OnUpdateEditNumconferences();
	afx_msg void OnUpdateEditMaxdettones();
	afx_msg void OnCheckFlgToneDetectB();
	afx_msg void OnChangeFldToneGenInstances();
	afx_msg void OnSelchangeCmbDSP();
	afx_msg void OnCheckMelp();
	afx_msg void OnButtonBuild2();
	afx_msg void OnButtonCidcfg();
	afx_msg void OnCheckTiG729ab();
	afx_msg void OnCheckVadreport();
	afx_msg void OnChangeFldLbCoderInstances();
	afx_msg void OnT38Checkbox();
	afx_msg void OnCheckedArb();
	afx_msg void OnDSPStackCheckBox();
	afx_msg void SRTP_checkbox();
	afx_msg void OnCheck16khzTdm();
	afx_msg void OnCheckG711();
	afx_msg void OnCheckG711Wb();
	afx_msg void OnCheckL16();
	afx_msg void OnCheckL16Wb();
	afx_msg void OnCheckSpeex();
	afx_msg void OnCheckSpeexWb();
	afx_msg void OnCheckMelpe();
	afx_msg void OnCheckTiG7231a();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GPAKCONFIGDLG_H__890F3D94_435B_4C58_B1C9_D992E56B1F69__INCLUDED_)
