#
#  MAKE FILE TEMPLATE
#
#
.DEFAULT_GOAL:=execs
ifeq ($(OS),Windows_NT)
   UTIL_DIR=W:/Utilities
else
   export UTIL_DIR=/mnt/engineering/Work/Utilities
endif

include $(UTIL_DIR)/PlatformDefs.Make

#------------------------------------
#  Flags for ALL builds
#
#  RENAMEFLAGS - .C to .c rename flags
#  CMPFLAGS   - Compiler flags for all builds
#  DEBUG      - Debug flags for all builds     (defaults on)
#  OPTIMIZE   - Optimizer flags for all builds (defaults on)
#  STRIPFLAGS - Symbol stripper flags
#  LIBFLAGS   - Library build flags
#  LNKFLAGS   - Linker flags
#  PRVFLAGS   - Attribute setting flags
#  DELFLAGS   - File deletion flags
#  MKDFLAGS   - Directory creation flags
#
# VERS= g.pak version
OPTS=VERS=0x601

#------------------------------------
#  Directories and sub-directories
#
# Single directories below must be terminated with '/'
#
#  ROOTDIR - Defaults to ./
#
#  SRCDIR  - Defaults to <ROOTDIR>/Src/
#  DEPDIR  - Defaults to <ROOTDIR>/<OS>/
#
# Directories in lists below must NOT be terminated with '/' or '\'
#
#  SUBDIRS - List of sub-directories (from ROOTDIR) containing source/header files
#  INCDIRS - Defaults to source, subdirectories, and W:/Utilities
#
ROOTDIR=./
SUBDIRS=Test
INCDIRS=. ..$_Includes
#------------------------------------
#  Objects
#
#  OBJBLD   - Identifies build for objects
#  OBJ_SRC  - Source for objects
#  OBJDIR  - Defaults to <ROOTDIR>/<OS>/OBJS/

#------------------------------------
#  Libraries
#
#  LIBS        - Base library names
#  LIBBLDS     - Identifies builds for libraries
#  LIBDIR  - Defaults to <ROOTDIR>/<OS>/LIBS/
#  <lib>_SRC   - Source for libraries
#  OPTS_<bld>  - Identifies build specific predefinitions (without -D)
#  FLAGS_<bld> - Identifies build specific flags
#  COMMON_HDR  - Main header files for dependencies


#------------------------------------
#  Executables 
#
#  EXECS              - Base executable names
#  EXEBLDS            - Identifies builds for executables
#  EXEDIR             - Defaults to <ROOTDIR>/<OS>/EXES/
#  <exe>_SRC          - Source for base executables
#  OPTS_<exebld>      - Identifies build specific options
#  FLAGS_<exe>        - Identifies build specific flags
#  LIBNAMES_<exe>     - Identifies libraries for executable builds
#
#  LIBNAMES_<exebld>  = ${call lib_name,<lib>,_<libbld>}
EXEDIR=.
FLAGS_GpakCfg=/SUBSYSTEM:WINDOWS
EXECS=GpakCfg
GpakCfg_SRC=DirDialog.cpp EchoCancel.cpp ExternalMem.cpp GpakCfgLibs.cpp \
            GpakCidDlg.cpp GpakConfig.cpp                     \
            GpakConfigDlg.cpp GpakConfigFileGen.cpp           \
            SerialPort.cpp StdAfx.cpp GpakConfigAec0420.cpp
GpakCfg_RC=GpakConfig.rc

LIBNAMES_GpakCfg+=ADT_AECDLL_V1_50_CGTVS2008.lib $(GpakCfg_RC:.rc=.res)
LIBNAMES_GpakCfg+=imagehlp.lib

#============================================
#  Standard build targets
#
#  dependencies -> <osdir>/<libname>_<bld>_<platform>_<version>_<compile_opts>_<CGT>.dep
#  objects      -> <objdir>/<srcname>_<bld>_<platform>_<version>_<compile_opts>_<CGT>.<object suffix>
#  libraries    -> <libdir>/ADT_<libname>_<bld>_<platform>_<version>_<compile_opts>_<CGT>.<lib suffix>
#  executables  -> <exedir>/<exename>_<exebld>_<platform>_<version>_<compile_opts>_<CGT>.<exe suffix>
#
#
#   targets = objs        clean        libs         execs
#             clean_<bld>  <lib>_<bld>  exec_<bld>
#

include $(UTIL_DIR)/Make.Procs


GpakConfig.res:: GpakConfig.rc resource.h  resource.hm
	@echo Generating $@ from $<
	$(WINTOOLSDIR)rc.exe /d "_DEBUG" /d "_VC80_UPGRADE=0x0600" /l 0x409 ${addprefix /i,$(INCDIRS)} /fo$(GpakCfg_RC:.rc=.res) $<

