// theFileDlg.h: interface for the CtheFileDlg class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_THEFILEDLG_H__88601636_7831_417A_9954_9228E6C7C3A6__INCLUDED_)
#define AFX_THEFILEDLG_H__88601636_7831_417A_9954_9228E6C7C3A6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CtheFileDlg : public CFileDialog  
{
public:
    
// Public data members

   BOOL m_bDlgJustCameUp;
    
// Constructors

    CtheFileDlg(BOOL bOpenFileDialog, // TRUE for FileOpen, FALSE for FileSaveAs
               LPCSTR lpszDefExt = NULL,
               LPCSTR lpszFileName = NULL,
               DWORD dwFlags = OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
               LPCSTR lpszFilter = NULL,
               CWnd* pParentWnd = NULL);
                                          
// Implementation
protected:
    //{{AFX_MSG(CtheFileDlg)
    virtual BOOL OnInitDialog();
    afx_msg void OnPaint();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()





};

#endif // !defined(AFX_THEFILEDLG_H__88601636_7831_417A_9954_9228E6C7C3A6__INCLUDED_)
