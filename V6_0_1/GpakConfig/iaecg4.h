#if (defined (WIN32) && defined(_WINDOWS)) || defined(_USRDLL) 
//#include "forcelib.h"
#endif
/*
//============================================================================
//
//    FILE NAME : IAECG4.h
//
//    ALGORITHM : AECG4
//
//    VENDOR    : ADT
//
//    TARGET DSP: C64x
//
//    PURPOSE   : IAECG4 Interface Header
//
//    Component Wizard for eXpressDSP Version 1.33.00 Auto-Generated Component
//
//    Number of Inputs : 1
//    Number of Outputs: 1
//
//    Creation Date: Thu - 23 October 2008
//    Creation Time: 01:55 PM
//
//============================================================================
*/

#ifndef IAECG4_
#define IAECG4_
#ifndef WIN32
#include <common/xdm_packages/ti/xdais/xdas.h>
#include <common/xdm_packages/ti/xdais/ialg.h>
#else
#include <common/xdm_packages/ti/xdais/std.h>
#include <common/xdm_packages/ti/xdais/xdas.h>
#include <common/xdm_packages/ti/xdais/ialg.h>
#endif
#include <common/include/adt_typedef_user.h>

#define SAVE_RESTORE_ACTION_GET_LENGTH 0
#define SAVE_RESTORE_ACTION_SAVE 1
#define SAVE_RESTORE_ACTION_RESTORE 2

#define NOISE_REDUCTION_OFF 0
#define NOISE_REDUCTION_LC 1	/* low complexity noise reduction */
#define MIN_NOISE_REDUCTION_HC 2	/* high complexity noise reduction 2..31 */

#define MTAB_NRECS 7
/*
// ===========================================================================
// IAECG4_Handle
//
// This handle is used to reference all AECG4 instance objects
*/
typedef struct IAECG4_Obj *IAECG4_Handle;

/*
// ===========================================================================
// IAECG4_Obj
//
// This structure must be the first field of all AECG4 instance objects
*/
typedef struct IAECG4_Obj {
    struct IAECG4_Fxns *fxns;
} IAECG4_Obj;

/*
// ===========================================================================
// IAECG4_Status
//
// Status structure defines the parameters that can be changed or read
// during real-time operation of the alogrithm.
*/
typedef struct IAECG4_Status {
    Int             size;  /* must be first field of all status structures */
    XDAS_Int16      txInPowerdBm10;
    XDAS_Int16      txOutPowerdBm10;
    XDAS_Int16      rxInPowerdBm10;
    XDAS_Int16      rxOutPowerdBm10;
	XDAS_Int16		residualPowerdBm10;
    XDAS_Int16      erlDirectdB10;
    XDAS_Int16      erlIndirectdB10;
    XDAS_Int16		erledB10;
	XDAS_Int16		shadowERLEdB10;
	XDAS_Int16		rxVADState;
	XDAS_Int16		txVADState;
} IAECG4_Status;

/*
// ===========================================================================
// IAECG4_Cmd
//
// The Cmd enumeration defines the control commands for the AECG4
// control method.
*/
typedef enum IAECG4_Cmd {
  IAECG4_GETSTATUS,
  IAECG4_SETSTATUS,
  IAECG4_PAUSE,
  IAECG4_RESUME
} IAECG4_Cmd;

/*
// ===========================================================================
// IAECG4_Params
//
// This structure defines the creation parameters for all AECG4 objects
*/
//TAG_PARAMS
#define __AECG4_PARAMS \
	XDAS_UInt16 	frameSize; \
	XDAS_Int16 		backgroundMode; \
	XDAS_Int32	 	samplingRate; \
	XDAS_Int32		maxAudioFreq; \
	XDAS_Int16 		bulkDelaySamples; \
    XDAS_Int16 		activeTailLengthMSec; \
    XDAS_Int16 		totalTailLengthMSec; \
	XDAS_Int16		maxTxNLPThresholddB; \
    XDAS_Int16      maxTxLossdB; \
    XDAS_Int16      maxRxLossdB; \
    XDAS_Int16      targetResidualLeveldBm; \
    XDAS_Int16      maxRxNoiseLeveldBm; \
	XDAS_Int16		worstExpectedERLdB; \
	XDAS_Int16		rxSaturateLeveldBm; \
	XDAS_Int16		noiseReductionSetting; \
	XDAS_Int16		cngEnable;\
	XDAS_Int8		fixedGaindB10; \
    XDAS_Int8       agcEnable; \
    XDAS_Int8       agcMaxGaindB; \
    XDAS_Int8       agcMaxLossdB; \
    XDAS_Int8       agcTargetLeveldBm; \
    XDAS_Int8       agcLowSigThreshdBm; \
	XDAS_Int16		maxTrainingTimeMSec; \
	XDAS_Int16		trainingRxNoiseLeveldBm; \
    XDAS_Int16 *    pTxEqualizerdB10; \

typedef struct IAECG4_Params {
    Int size;	  /* must be first field of all params structures */
	__AECG4_PARAMS

} IAECG4_Params;

/*
// ===========================================================================
// IAECG4_PARAMS
//
// Default parameter values for AECG4 instance objects
*/
extern IAECG4_Params IAECG4_PARAMS;

/*
// ===========================================================================
// IAECG4_Fxns
//
// This structure defines all of the operations on AECG4 objects
*/
typedef struct IAECG4_Fxns {
    IALG_Fxns	ialg;    /* IAECG4 extends IALG */
    XDAS_Void (*apply)(IAECG4_Handle handle, XDAS_Int16 * ptrRxIn, XDAS_Int16 * ptrRxOut, XDAS_Int16 * ptrTxIn, XDAS_Int16 * ptrTxOut);
    XDAS_Void (*applyTx)(IAECG4_Handle handle, XDAS_Int16 * ptrTxIn, XDAS_Int16 * ptrTxOut);
    XDAS_Void (*applyRx)(IAECG4_Handle handle, XDAS_Int16 * ptrRxIn, XDAS_Int16 * ptrRxOut);
    XDAS_Void (*backgroundHandler)(IAECG4_Handle handle);
	XDAS_Int32 (*saveRestoreState) (IAECG4_Handle handle, XDAS_Int8 *pState, XDAS_Int32 Length, XDAS_Int8 Action);
	Int (*reset) (IAECG4_Handle handle, const IAECG4_Params *iAECG4Params);

#ifdef DUMP_ENABLE
	XDAS_Void (*dumpInit) (IAECG4_Handle handle, const DumpParams_t *iAECG4DumpParams);
	XDAS_Void (*dump) (IAECG4_Handle handle);
#endif

} IAECG4_Fxns;


/* Concrete interface to all AECG4 functions */
ADT_API IAECG4_Handle AECG4_ADT_create(const IALG_Fxns *fxns, const IAECG4_Params *prms);
ADT_API IAECG4_Handle AECG4_ADT_createStatic(IALG_Fxns *fxns, IAECG4_Params *params, IALG_MemRec *memTab);
ADT_API void AECG4_ADT_staticAllocHelper(const IAECG4_Params *prms);
ADT_API Int AECG4_ADT_alloc(const IALG_Params *prms, struct IALG_Fxns **, IALG_MemRec memTab[]);
ADT_API Int AECG4_ADT_control(IALG_Handle handle, IALG_Cmd cmd, IALG_Status *status);
ADT_API Void AECG4_ADT_delete(IAECG4_Handle handle);
ADT_API XDAS_Void AECG4_ADT_apply(IAECG4_Handle handle, XDAS_Int16 * ptrRxIn, XDAS_Int16 * ptrRxOut, XDAS_Int16 * ptrTxIn, XDAS_Int16 * ptrTxOut);
ADT_API XDAS_Void AECG4_ADT_applyTx(IAECG4_Handle handle, XDAS_Int16 * ptrTxIn, XDAS_Int16 * ptrTxOut);
ADT_API XDAS_Void AECG4_ADT_applyRx(IAECG4_Handle handle, XDAS_Int16 * ptrRxIn, XDAS_Int16 * ptrRxOut);
ADT_API XDAS_Void AECG4_ADT_backgroundHandler(IAECG4_Handle handle);
ADT_API XDAS_Int32 AECG4_ADT_saveRestoreState(IAECG4_Handle handle, XDAS_Int8 *pState, XDAS_Int32 Length, XDAS_Int8 Action);
ADT_API Int AECG4_ADT_reset(IAECG4_Handle handle, const IAECG4_Params *prms);

#ifdef DUMP_ENABLE
/*
// ===========================================================================
// AECG4_dumpInit
*/

ADT_API XDAS_Void AECG4_ADT_dumpInit(IAECG4_Handle handle, const DumpParams_t *pDumpParams);

/*
// ===========================================================================
// AECG4_dump
*/
ADT_API XDAS_Void AECG4_ADT_dump(IAECG4_Handle handle);

#endif

#endif	/* IAECG4_ */
