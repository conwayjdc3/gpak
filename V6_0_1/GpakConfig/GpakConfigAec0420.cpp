#include <stdafx.h>
#include "AEC_0420\iaecg4.h"

#include "GpakConfig.h"
#pragma warning(disable:4996)

#include <stdio.h>
#include <math.h>
#include <string.h>

#include "sysconfig.h"
#include <sysmem.h>

extern sysConfig_t SysCfg;
extern unsigned int MaxFrameSize;
extern ADT_Bool WBCodecEnabled;

// AEC DLL function pointer prototypes.
typedef Int (* fpAECG4alloc)(const IALG_Params *prms, struct IALG_Fxns **, IALG_MemRec memTab[]);
typedef IAECG4_Handle (* fpAECG4createStatic)(IALG_Fxns *fxns, IAECG4_Params *params, IALG_MemRec *memTab);
typedef XDAS_Int32 (* fpAECG4saveRestoreState)(IAECG4_Handle handle, XDAS_Int8 *pState, XDAS_Int32 Length, XDAS_Int8 Action);


#define MAX_FRAME_SIZE 320
#define MAX_AEC_TAIL   512

//=============================================================================
//            AEC CONFIGURATION    <project>.c
//=============================================================================
static IAECG4_Params Aec0420Config = {
    sizeof(IAECG4_Params), // size
    NULL,                  // lockCallback
    MAX_FRAME_SIZE,        // frameSize
    1,                     // backgroundMode
    8000,                  // samplingRate
    4000,                  // maxAudioFreq
    (MAX_FRAME_SIZE/8)*2,  // fixedBulkDelayMSec
    0,                     // variableBulkDelayMSec
    0,                     // initialBulkDelayMSec
    MAX_AEC_TAIL,          // activeTailLengthMSec
    MAX_AEC_TAIL,          // totalTailLengthMSec
    0,                     // txNLPAggressiveness
    40,                    // maxTxLossSTdB
    10,                    // maxTxLossDTdB
    12,                    // maxRxLossdB
    10,                    // initialRxOutAttendB
    -85,                   // targetResidualLeveldBm
    -90,                   // maxRxNoiseLeveldBm
    -23,                   // worstExpectedERLdB
    3,                     // rxSaturateLeveldBm
    1,                     // noiseReduction1Setting
    0,                     // noiseReduction2Setting
    Enabled,               // cngEnable
    0,                     // fixedGaindB10
    Disabled,              // txAGCEnable
    0,                     // txAGCMaxGaindB
    0,                     // txAGCMaxLossdB
    -10,                   // txAGCTargetLeveldBm
    -40,                   // txAGCLowSigThreshdBm
    Disabled,              // rxAGCEnable
    10,                    // rxAGCMaxGaindB
    10,                    // rxAGCMaxLossdB
    -10,                   // rxAGCTargetLeveldBm
    -40,                   // rxAGCLowSigThreshdBm
    0,                     // rxBypassEnable
    0,                     // maxTrainingTimeMSec
    -30,                   // trainingRxNoiseLeveldBm
    NULL,                  // pTxEqualizerdB10
    0,                     // mipsMemReductionSetting
    0                      // mipsReductionSetting2
};

void WriteAEC0420Data (FILE *fp) {
   HMODULE hDll;
   fpAECG4alloc allocFunc;
   fpAECG4createStatic createStaticFunc;
   fpAECG4saveRestoreState saveRestoreFunc;
   int i, sectionI8, AECInstI8;
   IALG_MemRec memTab[MTAB_NRECS];
   ADT_UInt8 *pTempMem;
   ADT_UInt8 *pMemSect;
   IAECG4_Handle hAEC;
   ADT_UInt32 aecStateSize;

   // Get the version 0420 AEC DLL functions.
   hDll = LoadLibrary("AEC_0420\\ADT_AECDLL_VJUNE12_2013_CGTVS2008.dll");
   if (hDll == NULL)
   {
      return;
   }
   allocFunc = (fpAECG4alloc) GetProcAddress(hDll, "AECG4_ADT_alloc");
   createStaticFunc =
      (fpAECG4createStatic) GetProcAddress(hDll, "AECG4_ADT_createStatic");
   saveRestoreFunc =
      (fpAECG4saveRestoreState) GetProcAddress(hDll, "AECG4_ADT_saveRestoreState");
   if ((allocFunc == NULL) || (createStaticFunc == NULL) ||
       (saveRestoreFunc == NULL))
   {
      FreeLibrary(hDll);
      return;
   }

   fprintf (fp,"\n\n/*=================================*/\n");
   fprintf (fp,    "//{ Acoustic canceller declarations\n");

   if (SysCfg.AECInstances == 0) {
      // Allocate dummy externals to satisfy references
      fprintf (fp,"\nADT_Int16 AECInUse;\n");
      fprintf (fp,  "ADT_Int16 AECHandles;\n\n");
      fprintf (fp,  "ADT_Int16 AECConfig;\n");
      fprintf (fp,  "ADT_Int16 AECMemTab;\n");
      fprintf (fp,  "ADT_Int16 AECChan;\n");
      fprintf (fp,  "ADT_Int16 pAECFarPool;\n");
      fprintf (fp, "const int AEC_Inst_I8 = 0;\n");
      fprintf (fp,"#pragma DATA_SECTION (AECInUse,    \"STUB_DATA_SECT\")\n");
      fprintf (fp,"#pragma DATA_SECTION (AECHandles,  \"STUB_DATA_SECT\")\n");
      fprintf (fp,"#pragma DATA_SECTION (AECConfig,   \"STUB_DATA_SECT\")\n");
      fprintf (fp,"#pragma DATA_SECTION (AECMemTab,   \"STUB_DATA_SECT\")\n");
      fprintf (fp,"#pragma DATA_SECTION (AECChan,     \"STUB_DATA_SECT\")\n");
      fprintf (fp,"#pragma DATA_SECTION (pAECFarPool, \"STUB_DATA_SECT\")\n\n");

      fprintf (fp,"ADT_Int16 micEqChan;\n");
      fprintf (fp,"ADT_Int16 micEqState;\n");
      fprintf (fp,"#pragma DATA_SECTION (micEqChan,  \"STUB_DATA_SECT\")\n");
      fprintf (fp,"#pragma DATA_SECTION (micEqState, \"STUB_DATA_SECT\")\n\n");

      fprintf (fp,"ADT_Int16 spkrEqChan;\n");
      fprintf (fp,"ADT_Int16 spkrEqState;\n");
      fprintf (fp,"#pragma DATA_SECTION (spkrEqChan,  \"STUB_DATA_SECT\")\n");
      fprintf (fp,"#pragma DATA_SECTION (spkrEqState, \"STUB_DATA_SECT\")\n");
      fprintf (fp,    "//}");
      return;
   }

{
   fprintf (fp,"IAECG4_Params AECConfig = {\n");
   fprintf (fp,"    sizeof(IAECG4_Params),  // size\n");
   fprintf (fp,"    NULL,                   // lockCallback\n");
   fprintf (fp,"    MAX_FRAME_SIZE,         // frameSize\n");
   fprintf (fp,"    1,                      // backgroundMode\n");
   fprintf (fp,"    8000,                   // samplingRate\n");
   fprintf (fp,"    4000,                   // maxAudioFreq\n");
   fprintf (fp,"    (MAX_FRAME_SIZE/8)*2,   // fixedBulkDelayMSec\n");
   fprintf (fp,"    0,                      // variableBulkDelayMSec\n");
   fprintf (fp,"    0,                      // initialBulkDelayMSec\n");
   fprintf (fp,"    MAX_AEC_TAIL,           // activeTailLengthMSec\n");
   fprintf (fp,"    MAX_AEC_TAIL,           // totalTailLengthMSec\n");
   fprintf (fp,"    0,                      // txNLPAggressiveness\n");
   fprintf (fp,"    40,                     // maxTxLossSTdB\n");
   fprintf (fp,"    10,                     // maxTxLossDTdB\n");
   fprintf (fp,"    12,                     // maxRxLossdB\n");
   fprintf (fp,"    10,                     // initialRxOutAttendB\n");
   fprintf (fp,"    -85,                    // targetResidualLeveldBm\n");
   fprintf (fp,"    -90,                    // maxRxNoiseLeveldBm\n");
   fprintf (fp,"    -23,                    // worstExpectedERLdB\n");
   fprintf (fp,"    3,                      // rxSaturateLeveldBm\n");
   fprintf (fp,"    1,                      // noiseReduction1Setting\n");
   fprintf (fp,"    0,                      // noiseReduction2Setting\n");
   fprintf (fp,"    Enabled,                // cngEnable\n");
   fprintf (fp,"    0,                      // fixedGaindB10\n");
   fprintf (fp,"    Disabled,               // txAGCEnable\n");
   fprintf (fp,"    0,                      // txAGCMaxGaindB\n");
   fprintf (fp,"    0,                      // txAGCMaxLossdB\n");
   fprintf (fp,"    -10,                    // txAGCTargetLeveldBm\n");
   fprintf (fp,"    -40,                    // txAGCLowSigThreshdBm\n");
   fprintf (fp,"    Disabled,               // rxAGCEnable\n");
   fprintf (fp,"    10,                     // rxAGCMaxGaindB\n");
   fprintf (fp,"    10,                     // rxAGCMaxLossdB\n");
   fprintf (fp,"    -10,                    // rxAGCTargetLeveldBm\n");
   fprintf (fp,"    -40,                    // rxAGCLowSigThreshdBm\n");
   fprintf (fp,"    0,                      // rxBypassEnable\n");
   fprintf (fp,"    0,                      // maxTrainingTimeMSec\n");
   fprintf (fp,"    -30,                    // trainingRxNoiseLeveldBm\n");
   fprintf (fp,"    NULL,                   // pTxEqualizerdB10\n");
   fprintf (fp,"    0,                      // mipsMemReductionSetting\n");
   fprintf (fp,"    0                       // mipsReductionSetting2\n");
   fprintf (fp,"};\n\n");
}
   Aec0420Config.frameSize            = MaxFrameSize;
   Aec0420Config.fixedBulkDelayMSec   = (MaxFrameSize * 2) / SysCfg.samplesPerMs;
   Aec0420Config.samplingRate         = SysCfg.samplesPerMs * 1000;
   Aec0420Config.maxAudioFreq         = SysCfg.samplesPerMs * 500;
   Aec0420Config.activeTailLengthMSec = SysCfg.AECTailLen & (~7);    // Active tail must be multiple of 8 msec.
   Aec0420Config.totalTailLengthMSec  = SysCfg.AECTailLen;

   // Determine the memory needed by the AEC.
   (allocFunc)((const IALG_Params *) &Aec0420Config, NULL, memTab);

   fprintf (fp,"\nIALG_MemRec AECMemTab[] = {\n");

   AECInstI8 = 0;
   for (i = 0; i < MTAB_NRECS; i++) {
      sectionI8 = (memTab[i].size + 7) & 0xfffffff8;
      fprintf (fp," {\t%5d, 8, IALG_SARAM, IALG_PERSIST, NULL},\n", sectionI8);
      AECInstI8 += sectionI8;
   }
   fprintf (fp," {\t    0, 8, IALG_SARAM, IALG_PERSIST, NULL},\n");
   fprintf (fp,"};\n\n");

   // Determine the size of the AEC state data.
   pTempMem = (ADT_UInt8 *) calloc(AECInstI8, sizeof(ADT_UInt8 *));
   if (pTempMem != NULL)
   {
      pMemSect = pTempMem;
      for (i = 0; i < MTAB_NRECS; i++)
      {
         sectionI8 = (memTab[i].size + 7) & 0xfffffff8;
         if (sectionI8 == 0)
         {
            break;
         }
         memTab[i].base = pMemSect;
         memset(memTab[i].base, 0, sectionI8);
         pMemSect += sectionI8;
      }
      hAEC = (createStaticFunc)(NULL, &Aec0420Config, memTab);
      aecStateSize =
         (saveRestoreFunc)(hAEC, NULL, 0, SAVE_RESTORE_ACTION_GET_LENGTH);
      free(pTempMem);
   }
   else
   {
      aecStateSize = 0;
   }

   if ((SysCfg.samplesPerMs == 16) || WBCodecEnabled)
      fprintf (fp,"ADT_UInt32 AECFarBuff[MAX_AEC_ECANS][MAX_FRAME_SIZE];\n");
   else
      fprintf (fp,"ADT_UInt32 AECFarBuff[MAX_AEC_ECANS][MAX_FRAME_SIZE/2];\n");

   fprintf (fp,"ADT_UInt32 AECInUse[MAX_AEC_ECANS];\n");
   fprintf (fp,"#pragma DATA_SECTION (AECInUse, \"POOL_ALLOC\")\n");

   fprintf (fp,"IAECG4_Handle AECHandles[MAX_AEC_ECANS];\n");

   fprintf (fp, "#define AEC_INST_I8 %u\n", AECInstI8);
   fprintf (fp, "const int AEC_Inst_I8 = AEC_INST_I8;\n");
   for (i=0; i<SysCfg.AECInstances; i++) {
      fprintf (fp,"ADT_Int64  AECChan%d[AEC_INST_I8/8];\n", i);
      fprintf (fp,"#pragma DATA_SECTION (AECChan%d,      \"CHAN_INST_DATA:AEC\")\n",i);

   }

   fprintf (fp,"ADT_Int64* const AECChan[] = {\n");
   for (i=0; i<SysCfg.AECInstances; i++)
      fprintf (fp,"   AECChan%d,\n",i);
   fprintf (fp,"};\n");

   fprintf (fp,"ADT_UInt16 * const pAECFarPool[] = {\n");
   for (i=0; i<SysCfg.AECInstances; i++)
      fprintf (fp,"    (ADT_UInt16 *) AECFarBuff[%d],\n",i);
   fprintf (fp,"};\n");

   if (aecStateSize == 0)
   {
      aecStateSize = 1;
   }
   fprintf (fp, "#define MAX_AEC_STATE_SIZE %u\n", aecStateSize);
   fprintf (fp, "ADT_UInt8 aecStateBufr[MAX_AEC_STATE_SIZE];\n");
   fprintf (fp, "ADT_UInt32 aecStateBufrSize = MAX_AEC_STATE_SIZE;\n");

   fprintf (fp,"// AEC equalizers\n");
   fprintf (fp,"ADT_Int16   micEqState [MIC_EQ_STATE_I16];\n");
   if (SysCfg.micEqLength) {
      fprintf (fp,"#pragma DATA_SECTION (micEqState, \"FAST_DATA_SECT\")\n\n");
      fprintf (fp,"EqChannel_t  micEqChan[MAX_AEC_ECANS];\n");
      fprintf (fp,"#pragma DATA_SECTION (micEqChan, \"CHAN_INST_DATA:AEC\")\n");
   } else {
      fprintf (fp,"#pragma DATA_SECTION (micEqState, \"STUB_DATA_SECT\")\n\n");
      fprintf (fp,"EqChannel_t  micEqChan[1];\n");
      fprintf (fp,"#pragma DATA_SECTION (micEqChan, \"STUB_DATA_SECT\")\n\n");
   }

   fprintf (fp,"ADT_Int16   spkrEqState[SPKR_EQ_STATE_I16];\n");
   if (SysCfg.spkrEqLength) {
      fprintf (fp,"#pragma DATA_SECTION (spkrEqState,  \"FAST_DATA_SECT\")\n");
      fprintf (fp,"EqChannel_t  spkrEqChan[MAX_AEC_ECANS];\n");
      fprintf (fp,"#pragma DATA_SECTION (spkrEqChan, \"CHAN_INST_DATA:AEC\")\n");
   } else {
      fprintf (fp,"#pragma DATA_SECTION (spkrEqState,  \"STUB_DATA_SECT\")\n");
      fprintf (fp,"EqChannel_t  spkrEqChan[1];\n");
      fprintf (fp,"#pragma DATA_SECTION (spkrEqChan, \"STUB_DATA_SECT\")\n");
   }
   fprintf (fp,    "//}");

   // Free the DLL.
   FreeLibrary(hDll);

   return;
}
