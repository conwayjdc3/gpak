// SerialPort.cpp : implementation file
//

#include "stdafx.h"
#include "GpakConfig.h"
#include "SerialPort.h"

#pragma warning(disable:4996) // 'unsafe' calls

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


static char Buffer[200];
/////////////////////////////////////////////////////////////////////////////
// SerialPort dialog


SerialPort::SerialPort(CWnd* pParent /*=NULL*/)
	: CDialog(SerialPort::IDD, pParent)
{
	//{{AFX_DATA_INIT(SerialPort)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void SerialPort::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(SerialPort)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(SerialPort, CDialog)
	//{{AFX_MSG_MAP(SerialPort)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// SerialPort message handlers

#define AddEnum(Enum) \
   index = pCombo->AddString((LPCTSTR) #Enum); \
   if (index != CB_ERR) {                       \
      pCombo->SetItemData (index, Enum);       \
      if (Enum == InitValue) selected_index = index; \
      if (Enum == Deflt)     default_index = index; \
   }

#define EnumText(a)   case a:  StatusText = #a;  break;


void InitCompandComboBox (CDialog *dialog, int Fld, int InitValue) {
    int index, default_index = CB_ERR, selected_index = CB_ERR;  
    int Deflt;

   CComboBox *pCombo;
   pCombo = (CComboBox *) dialog->GetDlgItem (Fld);

   if (pCombo == NULL) return;

   Deflt = cmpNone16;

   AddEnum (cmpPCMU);
   AddEnum (cmpPCMA);
   AddEnum (cmpNone16);

   if (DspTypeInfo.SwCmpd) {
      AddEnum (cmpNone8);
      AddEnum (cmpL8PcmU);
      AddEnum (cmpL8PcmA);
   }
   if (selected_index == CB_ERR) selected_index = default_index;
   pCombo->SetCurSel (selected_index);

}


BOOL SerialPort::OnInitDialog() 
{
    CDialog::OnInitDialog();
	
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    // Display the appropriate dialog title.
    if (m_PortNum == 1)        SetWindowText("Serial Port 1 Configuration");
    else if (m_PortNum == 2)   SetWindowText("Serial Port 2 Configuration");
    else                       SetWindowText("Serial Port 3 Configuration");

    // Initialize the display to the current values.
    InitCompandComboBox  (this, CmbCompand,  PortConfig.DataFormat);
    SetDlgItemInt(IDC_EDIT_XDATDELAY, PortConfig.XmtDataDelay, FALSE);
    SetDlgItemInt(IDC_EDIT_RDATDELAY, PortConfig.RcvDataDelay, FALSE);

    if (PortConfig.XmtSyncHigh)	CheckDlgButton(IDC_RADIO_XSYNCHIGH, 1);
    else                       	CheckDlgButton(IDC_RADIO_XSYNCLOW, 1);

    if (PortConfig.RcvSyncHigh) CheckDlgButton(IDC_RADIO_RSYNCHIGH, 1);
    else                      	CheckDlgButton(IDC_RADIO_RSYNCLOW, 1);

    if (PortConfig.XmtClockRise)CheckDlgButton(IDC_RADIO_XCLKRISE, 1);
    else                      	CheckDlgButton(IDC_RADIO_XCLKFALL, 1);

    if (PortConfig.RcvClockRise)CheckDlgButton(IDC_RADIO_RCLKRISE, 1);
    else                      	CheckDlgButton(IDC_RADIO_RCLKFALL, 1);

	CheckDlgButton(IDC_CHECK_DXDELAY, PortConfig.DxDelayEnable);
	CheckDlgButton(IDC_CHECK_TSIP, PortConfig.TsipEnable);

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//
// OK button hit - Validate and save entered parameters.
//
extern int GetComboItem (CDialog * dialog, int Fld, int Dflt);

void SerialPort::OnOK() 
{
    BOOL ValidInt;  // flag indicating valid integer
    int TransmitDataDelay, ReceiveDataDelay;      // converted integer value

    // Get and validate the selected Data Format.
    PortConfig.DataFormat = (GpakCompandModes) GetComboItem (this, CmbCompand, cmpNone16);

    // Get and validate the entered Transmit Data Delay value.
	TransmitDataDelay = (int) GetDlgItemInt(IDC_EDIT_XDATDELAY, &ValidInt, FALSE);
   ItemRangeCheck (MIN_PORT_DATA_DELAY, TransmitDataDelay, MAX_PORT_DATA_DELAY);
	PortConfig.XmtDataDelay = TransmitDataDelay;

    // Get and validate the entered Receive Data Delay value.
	ReceiveDataDelay = (int) GetDlgItemInt(IDC_EDIT_RDATDELAY, &ValidInt, FALSE);
   ItemRangeCheck (MIN_PORT_DATA_DELAY, ReceiveDataDelay, MAX_PORT_DATA_DELAY);
	PortConfig.RcvDataDelay = ReceiveDataDelay;

    // Get and validate the entered Transmit Frame Sync Polarity.
	if (IsDlgButtonChecked(IDC_RADIO_XSYNCHIGH))
        PortConfig.XmtSyncHigh = TRUE;
	else if (IsDlgButtonChecked(IDC_RADIO_XSYNCLOW))
        PortConfig.XmtSyncHigh = FALSE;
    else
    {
		AfxMessageBox("The Transmit Frame Sync Polarity is invalid!");
        return;
    }

    // Get and validate the entered Receive Frame Sync Polarity.
	if (IsDlgButtonChecked(IDC_RADIO_RSYNCHIGH))
        PortConfig.RcvSyncHigh = TRUE;
	else if (IsDlgButtonChecked(IDC_RADIO_RSYNCLOW))
        PortConfig.RcvSyncHigh = FALSE;
    else
    {
		AfxMessageBox("The Receive Frame Sync Polarity is invalid!");
        return;
    }

    // Get and validate the entered Transmit Clock Polarity.
	if (IsDlgButtonChecked(IDC_RADIO_XCLKRISE))
        PortConfig.XmtClockRise = TRUE;
	else if (IsDlgButtonChecked(IDC_RADIO_XCLKFALL))
        PortConfig.XmtClockRise = FALSE;
    else
    {
		AfxMessageBox("The Transmit Clock Polarity is invalid!");
        return;
    }

    // Get and validate the entered Receive Clock Polarity.
	if (IsDlgButtonChecked(IDC_RADIO_RCLKRISE))
        PortConfig.RcvClockRise = TRUE;
	else if (IsDlgButtonChecked(IDC_RADIO_RCLKFALL))
        PortConfig.RcvClockRise = FALSE;
    else
    {
		AfxMessageBox("The Receive Clock Polarity is invalid!");
        return;
    }

    // Get the selected DX Delay Enable state.
    PortConfig.DxDelayEnable = IsDlgButtonChecked(IDC_CHECK_DXDELAY);
    PortConfig.TsipEnable = IsDlgButtonChecked(IDC_CHECK_TSIP);
    if (PortConfig.TsipEnable && (PortConfig.DataFormat != cmpL8PcmU) &&
                                 (PortConfig.DataFormat != cmpL8PcmA) &&
                                 (PortConfig.DataFormat != cmpNone8))
    {
		AfxMessageBox("Invalid data format: Tsip requires 8-bit data!");
        return;
    }
    
    // Do the base class's method to exit the dialog with success.
	CDialog::OnOK();
}
