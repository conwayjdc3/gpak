// GpakConfig.h : main header file for the GPAKCONFIG application
//

#if !defined(AFX_GPAKCONFIG_H__EEA4B260_732F_431D_B46F_992A6514F5C2__INCLUDED_)
#define AFX_GPAKCONFIG_H__EEA4B260_732F_431D_B46F_992A6514F5C2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

#include "GpakEnum.h"
#include "sysconfig.h"
#include "sysmem.h"

#define UNSIGNED FALSE
#define SIGNED   TRUE


#define ItemRangeCheck(min,field,max)  \
   if (!ValidInt) {                                                \
      sprintf (Buffer, "%s error.  Must be integer value", #field); \
      AfxMessageBox (Buffer);                                      \
      return;                                                      \
   }                                                               \
   if (field < min || max < field) {                               \
      sprintf (Buffer, "%s error.  Must be between %d and %d", #field, min, max); \
      AfxMessageBox (Buffer);                                      \
      return;                                                      \
   }


// DSP Type dependent information structure (for a single core).
typedef struct {
    DspType_t DspType;      // DSP Type Id (5410, 5416, etc.)
    int NumSerialPorts;     // number of serial ports
    int Mips;               // MIPS
	int HostInterface;      // Host interface address
	int SwCmpd;             // Software companding allowed
	int IsC64x;             // C64 build
    int IsC64xPlus;         // C64+ build
    int IsC55x;             // C55 build
} DspTypeDepInfo_t;

// Definition of Echo Canceller NLP types.
typedef enum
{
    NlpOff = 0,         // NLP Off
    NlpMute = 1,        // NLP Mute
    NlpRandom = 2,      // NLP Random Noise
    NlpHoth = 3,        // NLP Hoth Noise
    NlpSpectral = 4,    // NLP Spectral Noise
    NlpSuppAuto =6      // NLP Attentuate
} EcanNlpType_t;

// Echo Canceller Configuration information structure.
typedef struct
{
    int TapLength;          // TAP Length
    EcanNlpType_t NlpType;  // NLP Type
    BOOL AdaptEnable;       // Adapt Enable
    BOOL G165DetectEnable;  // G.165 Detect Enable
    int DblTalkThreshold;   // Double Talk Threshold
    int MaxDblTalkThreshold; // Maximum double talk
    int NlpThrehold;        // NLP Threshold
    int NlpUpperThresConv;    // Convergence threshold
    int NlpUpperThresUnConv;  // Non-convergence threshold
    int NlpMaxSupp;         // DB limit when NlpType = NLP_SUPP
    int CngThreshold;       // CNG Threshold
    int AdaptLimit;         // Adapt Limit
    int CrossCorrLimit;     // Cross Correlation Limit
    int NumFirSegments;     // Number of FIR Segments
    int FirSegmentLength;   // FIR Segment Length
    int FirTapCheckPeriod;  // Number FIR adaptations before bg processing
    int TandemOperationEnable; // Second echo canceller
    int EchoSourceAttached; // Source of echo (hybrid) is in circuit
    int ReconvergenceCheck; // Reconvergence checking
} EchoCancelInfo_t;


// Serial Port Configuration information structure.
typedef struct
{
    GpakCompandModes DataFormat;    // Data Format
    int XmtDataDelay;               // Transmit Data Delay
    int RcvDataDelay;               // Receive Data Delay
    BOOL XmtSyncHigh;               // Transmit Sync Polarity Active High
    BOOL RcvSyncHigh;               // Receive Sync Polarity Active High
    BOOL XmtClockRise;              // Transmit Clock Polarity Rising Edge
    BOOL RcvClockRise;              // Receive Clock Polarity Rising Edge
    BOOL DxDelayEnable;             // DX Delay enabled
    BOOL TsipEnable;                // Tsip Device Enabled
} SerialPortInfo_t;


enum { EXT_NONE, EXT_DMA, EXT_CACHE };
typedef struct {
	int type;	   // External memory selection
} ExternalMemInfo_t;

typedef struct {
  int MaxAttendB;
  int VadLowThreshdB;
  int VadHighThreshdB;
  unsigned VadHangMSec;
  unsigned VadWindowSize;
} NCAN_Params_t;

typedef struct {
    unsigned  type2CID;       // (Tx and Rx) 1==both type 1 and type 2, 0==Type 1 only
    unsigned  numSeizureBytes;    // (Tx only) num seizure bytes to prepend
    unsigned  numMarkBytes;       // (Tx only) num mark bytes to prepend
    unsigned  numPostambleBytes;  // (Tx only) num postamble bytes to append
    unsigned  fskType;            // (Tx and Rx) v23 or bell 202 
    int       fskLevel;           // (Tx only) Tx fsk level (0...-19 dBm)
    unsigned  msgType;            // (Tx only) message type field (valid if format==1)
} CID_Params_t;
// Dialog entry variables.
extern EchoCancelInfo_t EcanConfig; // dialog copy of Echo Canceller config info
extern SerialPortInfo_t PortConfig; // dialog copy of Serial Port config info
extern CID_Params_t CIDConfigDlg;   // dialog copy of Caller ID config info
// Miscellaneous.
extern int ExtraProgramMem;         // number of words of extra program memory

// Definition of valid parameter ranges.
#define MAX_CONFIGURABLE_CHANNELS    256
#define MAX_CONFIGURABLE_CONFERENCES 100      // maximum number of Conferences
#define MAX_EC_TAP_LENGTH 32767 	// maximum value of Echo Cancel TAP Length
#define MAX_EC_NUM_FIR_SEGS 3	    // maximum value of Ecan Num FIR Segments
#define MAX_EC_FIR_SEG_LEN 32767	// maximum value of Ecan FIR Segment Length
#define MIN_PORT_DATA_DELAY 0       // minimum value of Serial Port Data Delay
#define MAX_PORT_DATA_DELAY 2       // maximum value of Serial Port Data Delay
#define MAX_PORT_SLOTS 128          // maximum value of Serial Port Slots
#define MAX_TSIP_SLOTS 1024         // maximum value of Serial Port Slots

// Definition of selected parameter values.
extern DspTypeDepInfo_t DspTypeInfo;    // selected DSP Type information
extern int Hint8;                       // Host Interrupt flag for 1 msec frame
extern int Hint20;                      // Host Interrupt flag for 2.5 msec frame
extern int Hint40;                      // Host Interrupt flag for 5 msec frame
extern int Hint80;                      // Host Interrupt flag for 10 msec frame
extern int Hint160;                     // Host Interrupt flag for 20 msec frame
extern int Hint180;                     // Host Interrupt flag for 22.5 msec frame
extern int Hint240;                     // Host Interrupt flag for 30 msec frame
extern int DataMemory;
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


/////////////////////////////////////////////////////////////////////////////
// CGpakConfigApp:
// See GpakConfig.cpp for the implementation of this class
//

class CGpakConfigApp : public CWinApp
{
public:
	CGpakConfigApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGpakConfigApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CGpakConfigApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GPAKCONFIG_H__EEA4B260_732F_431D_B46F_992A6514F5C2__INCLUDED_)
