#if !defined(AFX_ECHOCANCEL_H__F930E21F_AD42_4867_9023_689E7ACE2278__INCLUDED_)
#define AFX_ECHOCANCEL_H__F930E21F_AD42_4867_9023_689E7ACE2278__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// EchoCancel.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// EchoCancel dialog

class EchoCancel : public CDialog
{
// Construction
public:
	BOOL m_ShortTail;
	int m_EcanType;
	EchoCancel(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(EchoCancel)
	enum { IDD = IDD_ECHOCANCEL_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(EchoCancel)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	afx_msg void RedrawWindow();
	afx_msg void SetDefaults();

	// Generated message map functions
	//{{AFX_MSG(EchoCancel)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnDefaults();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ECHOCANCEL_H__F930E21F_AD42_4867_9023_689E7ACE2278__INCLUDED_)
