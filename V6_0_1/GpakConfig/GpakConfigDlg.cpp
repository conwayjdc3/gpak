// GpakConfigDlg.cpp : implementation file
//

#include <stdafx.h>
#include "GpakConfig.h"
#include "GpakConfigDlg.h"
#include <stdio.h>
#include <string.h>
#include <thefileDlg.h>
#include <dlgs.h>
#include "DirDialog.h"
#pragma warning(disable:4996) // 'unsafe' calls

extern ADT_Bool WBCodecEnabled;

// Definition of configuration utility functions.
extern void computeBlockSize (sysConfig_t *,  blockStatus_t *, ecConfig_t *, DspTypeDepInfo_t *);

extern unsigned int getPoolSize (sysConfig_t *cfg, blockStatus_t *bs,
   unsigned sys_saram_data,   // saram data memory used by system
   unsigned sys_daram_data,   // daram data memory used by system
   unsigned saram_channel_buff,   // total size of saram buffers needed per channel
   unsigned daram_channel_buff,   // total size of daram buffers needed per channel
   unsigned contiguous_saram_avail,  // total amount of contiguous saram memory available for channels
   unsigned contiguous_daram_avail   // total amount of contiguous daram memory available for channels
   );


extern char *IdentifyActiveFiles (sysConfig_t *cfg);
void InitDSPComboBox (CDialog *Dlg, int InitValue);

extern ADT_Bool createDspSrcFiles (sysConfig_t *cfg, blockStatus_t *bs,  unsigned poolSize,
   ecConfig_t *ecCfg,  char **pErrText, ExternalMemInfo_t *pExtMem, char *Root,
   DspTypeDepInfo_t *dspInfo);


#if (VERS < 401)
   #undef MELP
#else
   #define MELP                      1
#endif

#if (500 <= VERS)
   #undef ADD_STUB
#else
   #define ADD_STUB

#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ Miscellaneous definitions.
#define SIGNED   TRUE
#define UNSIGNED FALSE

#define JB_HEAD_ROOM  48 //  Must leave room for pool pointer (4),  RTP_HEADER (12), and JITTER_BUFF_HEADER (16)
#define FAST_RTP_HDR_I32 16  //  PBM Struct + Ethernet Hdr + IP Hdr + UDP Hdr
#define JB_NET_SOCKET_HUGE_I32   172 // Must allocate Huge JB when using network stack for socket allocation during startup.

#define CONFIG_PARMS_FILE "GpakCfgParms.txt"   // Config Parms file name

#define WarnMsg(s1, s2) \
   AfxMessageBox ("There are no " s1 " configured. Configure " s2 " channels?", MB_YESNO)
   
#define algBits  SysCfg.enab.Bits
#define cdcBits  SysCfg.Codecs.Bits

#define SetItemState(item,state) \
   if ((pObj = GetDlgItem(IDC_STATIC_##item)) != NULL) \
      pObj->EnableWindow(State);    \
   if ((pObj = GetDlgItem(IDC_EDIT_##item)) != NULL) \
      pObj->EnableWindow(State);    

#define SetState(item,state) \
   if ((pObj = GetDlgItem(item)) != NULL) \
      pObj->EnableWindow(State);    

#define DisableObj(id, state)   \
   if ((pObj = GetDlgItem (id)) != NULL) pObj->EnableWindow (State);

#define AddEnum(Enum, Deflt) \
   index = pCombo->AddString((LPCTSTR) #Enum); \
   if (index != CB_ERR) {    \
   pCombo->SetItemData (index, Enum);   \
   if (Enum == InitValue) selected_index = index; \
   if (Enum == Deflt)   default_index = index; \
   }

#define SetFrameInterrupt(ms,size) \
   FrameMS_##ms = IsDlgButtonChecked (IDC_CHECK_FRAME##size);  \
   if (FrameMS_##ms && size < MinFrameSize)  MinFrameSize = size; \
   if (FrameMS_##ms)                         MaxFrameSize = size; \
   if ((pObj = GetDlgItem (IDC_CHECK_HINT##size)) != NULL)   \
      pObj->EnableWindow (FrameMS_##ms);

//}

//{  DSP Specifications
// 55xx DSP specific information          ID   MBsps   Mips     HostInter  SwCmpd  IsC64x   IsC64xPlus, IsC55x
const DspTypeDepInfo_t Dsp5510Info = {   Ti5510,  3,   200,        0x0200,  FALSE,  FALSE, FALSE, TRUE  };

// 54xx DSP specific information          ID   MBsps   Mips     HostInter  SwCmpd  IsC64x   IsC64xplus, IsC55x
const DspTypeDepInfo_t Dsp5410Info = {   Ti5410,  3,   100,        0x1000,  FALSE,  FALSE, FALSE, FALSE  };
const DspTypeDepInfo_t Dsp5416Info = {   Ti5416,  3,   120,        0x1000,  FALSE,  FALSE, FALSE, FALSE  };
const DspTypeDepInfo_t Dsp5420Info = {   Ti5420,  3,   100,        0x1000,  FALSE,  FALSE, FALSE, FALSE  };
const DspTypeDepInfo_t Dsp5421Info = {   Ti5421,  3,   100,        0x1000,  FALSE,  FALSE, FALSE, FALSE  };
const DspTypeDepInfo_t Dsp5441Info = {   Ti5441,  3,   133,        0x1000,  FALSE,  FALSE, FALSE, FALSE  };

// 64xx DSP specific information
const DspTypeDepInfo_t Dsp64xxInfo = {   TiDM642,  3,   500,   0x00000200,   TRUE,   TRUE, FALSE, FALSE  };
const DspTypeDepInfo_t Dsp6416Info = {   Ti6416,   3,  1000,   0x00000200,   TRUE,   TRUE, FALSE, FALSE  };

const DspTypeDepInfo_t Dsp6424Info = {   Ti6424,   2,   594,   0x10800000,   TRUE,   TRUE, TRUE, FALSE   };
const DspTypeDepInfo_t Dsp6452Info = {   Ti6452,   2,   600,   0x00A00000,   TRUE,   TRUE, TRUE, FALSE   };

const DspTypeDepInfo_t Dsp647Info =  {   Ti647,    3,   700,   0xe4000000,   TRUE,   TRUE, TRUE, FALSE   };
const DspTypeDepInfo_t Dsp648Info =  {   TiDM648,  2,   600,   0x00A00000,   TRUE,   TRUE, TRUE, FALSE   };
const DspTypeDepInfo_t Dsp6437Info = {   TiDM6437, 2,   600,   0x10800000,   TRUE,   TRUE, TRUE, FALSE   };
const DspTypeDepInfo_t Dsp3530Info = {   TiOM3530, 2,   360,   0x10f04000,   TRUE,   TRUE, TRUE, FALSE   };
const DspTypeDepInfo_t Dsp674Info  = {   Ti674,    2,   300,   0x80000000,   TRUE,   TRUE, TRUE, FALSE   };
const DspTypeDepInfo_t Dsp665xInfo  =  { Ti665,   2,   850,   0x90000000,   TRUE,   TRUE, TRUE, FALSE   };
const DspTypeDepInfo_t Dsp667xInfo  =  { Ti667,   2,   1000,  0x90000000,   TRUE,   TRUE, TRUE, FALSE   };
//}
DspTypeDepInfo_t DspTypeInfo;   // selected DSP Type information

//----------------------------------------------------------------------
//{ Configuration parameter variables.
char CompPath[100], StubPath[100], GpakPath[100];

// Processor
int DspType;
int DspSpeed;
ADT_Bool TDM_16kHz;
int NumChannels;   // selected Number of Channels

// Conference values
int NumGpakConferences;    // count of g.pak conferences
int ChansPerConference;   // selected Number of Channels per Conference
int NumConfDominant;      // selected Number of Dominant Conference Members
int MaxConfNoiseSuppress;   // selected Maximum Conference Noise Suppression
int NumCustomConferences;  // count of custom conferences

// T.38
ADT_UInt16 NumT38Instances;
ADT_UInt16 NumTIT38Instances;

// CallerID
int NumRxCidInstances;
int NumTxCidInstances;
int type2CID;

//  AGC
int AgcInstances;   // selected AGC Enable
int AgcTargetPowerA, AgcTargetPowerB; // selected AGC Target Power
int AgcLossLimitA,   AgcLossLimitB;   // selected AGC Max Negative Gain
int AgcGainLimitA,   AgcGainLimitB;   // selected AGC Max Positive Gain
int AgcLowSigThreshA, AgcLowSigThreshB;

// Echo Cancelling
ADT_Bool EcShortTailEnabled;   // selected Short Tail Echo Canceller Enable
int NumPcmEcans, NumPktEcans;                  // selected Number of PCM and PKT Echo Cancellers
EchoCancelInfo_t PcmEcanConfig, PktEcanConfig; // Echo Canceller configuration info

int NumAECEcans;   // selected Number of AEC Echo cancellers
int AECTailLen;
int MicEqLength;      // AEC Microphone equalizer length
int SpkrEqLength;     // AEC Speaker equalizer length

// VAD
ADT_Bool VADEnabled;   // selected VAD Enable
int VadNoiseFloor;   // selected VAD Noise Floor
int VadHangTime;   // selected VAD Hangover Time
int VadWindowMs;   // selected VAD Window Size
ADT_Bool VadReportEvents;

// Tone Detect / Generation
ADT_Bool DtmfEnabled;   // selected DTMF Enable
ADT_Bool Mfr1Enabled;   // selected MFR1 Enable
ADT_Bool Mfr2FwdEnabled;   // selected MFR2 Forward Enable
ADT_Bool Mfr2RevEnabled;   // selected MFR2 Reverse Enable
ADT_Bool CallProgEnabled;   // selected Call Progress Enable
ADT_Bool ARBEnabled;
ADT_Bool CEDEnabled;
ADT_Bool CNGEnabled;
ADT_Bool ToneDetectBEnabled;

int MaxConcurrentTones;   // selected Max Concurrent Tones
int ToneGenInstances;
ADT_Bool ToneRlyGenEnabled;   // selected Tone Relay Generate Enable
int DtmfDialEnabled;
int ToneDetInstances;
int ARBCfgCount;

// Vocoders
ADT_Bool L16Enabled,         L16WBEnabled;
ADT_Bool G711Enabled,        G711WBEnabled;
ADT_Bool G722Enabled;
ADT_Bool G723Enabled,        TI_G7231AEnabled;
ADT_Bool G726LowMemEnabled,  G726LowMipsEnabled;
ADT_Bool G726Rate16Enabled,  G726Rate24Enabled;
ADT_Bool G726Rate32Enabled,  G726Rate40Enabled;
ADT_Bool G728Enabled;
ADT_Bool G729ABEnabled,      TI_G729ABEnabled;
ADT_Bool melpEnabled,        melpEEnabled;
ADT_Bool gsmEFREnabled;
ADT_Bool SpeexEnabled,       SpeexWBEnabled;
ADT_Bool Adt4800Enabled;
int LbCoderInstances;

// Serial Ports
int Port1NumSlots, Port1SlotsUsed;
int Port2NumSlots, Port2SlotsUsed;
int Port3NumSlots, Port3SlotsUsed;

SerialPortInfo_t Port1Config;   // Serial Port 1 configuration info
SerialPortInfo_t Port2Config;   // Serial Port 2 configuration info
SerialPortInfo_t Port3Config;   // Serial Port 3 configuration info

//  Frame processing
ADT_Bool FrameMS_1,  FrameMS_2_5,  FrameMS_5, FrameMS_10;
ADT_Bool FrameMS_20, FrameMS_22_5, FrameMS_30;

ADT_Bool Hint8,   Hint20,  Hint40,   Hint80;
ADT_Bool Hint160, Hint180, Hint240;  

ADT_Bool Pcm2PktChan,  Pcm2PcmChan, Pkt2PktChan;
ADT_Bool CircDataChan, ConfPcmChan, ConfPktChan;
ADT_Bool ConfCompChan;

// Noise suppression
ADT_Bool NoiseSuppressionEnabled;
NCAN_Params_t NseSpr, NoiseSuppression;

// Miscellaneous
int HostInterfaceAddress;   // selected Host Interface Address
int DataMemory;             // number of words of data memory
int HostIF;
ExternalMemInfo_t ExtMemConfig;   // External Memory config info

// RTP / SRTP
GpakProfiles PktProfile;   // selected Packet Profile
unsigned int JBMaxMS;
unsigned int RtpMaxBuffsPerChan;
GpakRTPPkts RtpToneType;   // selected RTP Tone Packet Type
ADT_Bool SRTP_Enable;
ADT_UInt16 SRTPInstances;

//  Hand manipulated
ADT_UInt16 SrcInstances; // sampling rate converter
int MaxCustomFrameRateMs;
int MaxPktBufDelayMS;

typedef struct GpakVarDef {
   char* name;
   void* pointer;
   int   varI8;
   int   dflt;
} GpakVarDef;
#define GpakCfgVar(var,type,default)  { #var, &var, sizeof (var), default },

GpakVarDef gpakVars[] = {

   GpakCfgVar (DspType,  int, Ti647)
   GpakCfgVar (DspSpeed, int, 625)
   GpakCfgVar (TDM_16kHz, ADT_Bool, ADT_FALSE)
   GpakCfgVar (NumChannels, int, 1)

   // Conferencing
   GpakCfgVar (NumGpakConferences, int, 0)    GpakCfgVar (ChansPerConference, int, 0)
   GpakCfgVar (NumConfDominant, int, 0)       GpakCfgVar (MaxConfNoiseSuppress, int, 24)
   GpakCfgVar (NumCustomConferences, int, 0)

   // T.38
   GpakCfgVar (NumT38Instances, int, 0)      GpakCfgVar (NumTIT38Instances, int, 0)

   // CallerID
   GpakCfgVar (NumRxCidInstances, int, 0)    GpakCfgVar (NumTxCidInstances, int, 0)
   GpakCfgVar (type2CID, int, 0)

   // AGC
   GpakCfgVar (AgcInstances, int, 0)   
   GpakCfgVar (AgcTargetPowerA, int, -18)    GpakCfgVar (AgcTargetPowerB, int, -18)
   GpakCfgVar (AgcLossLimitA, int, -10)      GpakCfgVar (AgcLossLimitB, int, -10)  
   GpakCfgVar (AgcGainLimitA, int,  10)      GpakCfgVar (AgcGainLimitB, int,  10)
   GpakCfgVar (AgcLowSigThreshA, int, -47)   GpakCfgVar (AgcLowSigThreshB, int, -47)

   // Echo Cancelling
   GpakCfgVar (EcShortTailEnabled, ADT_Bool, ADT_TRUE)
   GpakCfgVar (NumPcmEcans, int, 0)                                  GpakCfgVar (NumPktEcans, int, 0)
   GpakCfgVar (PcmEcanConfig.TapLength, int, 128)                    GpakCfgVar (PktEcanConfig.TapLength, int, 128)
   GpakCfgVar (PcmEcanConfig.NlpType, EcanNlpType_t, NlpHoth)        GpakCfgVar (PktEcanConfig.NlpType, EcanNlpType_t, NlpHoth)
   GpakCfgVar (PcmEcanConfig.G165DetectEnable, ADT_Bool, ADT_TRUE)   GpakCfgVar (PktEcanConfig.G165DetectEnable, ADT_Bool, ADT_TRUE)
   GpakCfgVar (PcmEcanConfig.AdaptEnable, ADT_Bool, ADT_TRUE)        GpakCfgVar (PktEcanConfig.AdaptEnable, ADT_Bool, ADT_TRUE)
   GpakCfgVar (PcmEcanConfig.AdaptLimit, int, 75)                    GpakCfgVar (PktEcanConfig.AdaptLimit, int, 75)
   GpakCfgVar (PcmEcanConfig.NlpThrehold, int, 24)                   GpakCfgVar (PktEcanConfig.NlpThrehold, int, 24)
   GpakCfgVar (PcmEcanConfig.NlpUpperThresConv, int, 18)             GpakCfgVar (PktEcanConfig.NlpUpperThresConv, int, 18)
   GpakCfgVar (PcmEcanConfig.NlpUpperThresUnConv, int, 12)           GpakCfgVar (PktEcanConfig.NlpUpperThresUnConv, int, 12)
   GpakCfgVar (PcmEcanConfig.NlpMaxSupp, int, 10)                    GpakCfgVar (PktEcanConfig.NlpMaxSupp, int, 10)
   GpakCfgVar (PcmEcanConfig.CngThreshold, int, 43)                  GpakCfgVar (PktEcanConfig.CngThreshold, int, 43)
   GpakCfgVar (PcmEcanConfig.DblTalkThreshold, int, 6)               GpakCfgVar (PktEcanConfig.DblTalkThreshold, int, 6)
   GpakCfgVar (PcmEcanConfig.CrossCorrLimit, int, 25)                GpakCfgVar (PktEcanConfig.CrossCorrLimit, int, 25)
   GpakCfgVar (PcmEcanConfig.FirSegmentLength, int, 48)              GpakCfgVar (PktEcanConfig.FirSegmentLength, int, 48)
   GpakCfgVar (PcmEcanConfig.NumFirSegments, int, 3)                 GpakCfgVar (PktEcanConfig.NumFirSegments, int, 3)
   GpakCfgVar (PcmEcanConfig.FirTapCheckPeriod, int, 80)             GpakCfgVar (PktEcanConfig.FirTapCheckPeriod, int, 80)

   GpakCfgVar (PcmEcanConfig.MaxDblTalkThreshold, int, 6)             GpakCfgVar (PktEcanConfig.MaxDblTalkThreshold, int, 6)
   GpakCfgVar (PcmEcanConfig.TandemOperationEnable, int, ADT_FALSE)   GpakCfgVar (PktEcanConfig.TandemOperationEnable, int, ADT_FALSE)
   GpakCfgVar (PcmEcanConfig.EchoSourceAttached, int, ADT_TRUE)       GpakCfgVar (PktEcanConfig.EchoSourceAttached, int, ADT_TRUE)
   GpakCfgVar (PcmEcanConfig.ReconvergenceCheck, int, ADT_TRUE)      GpakCfgVar (PktEcanConfig.ReconvergenceCheck, int, ADT_TRUE)

   GpakCfgVar (NumAECEcans, int, 0)    GpakCfgVar (AECTailLen, int, 128)
   GpakCfgVar (MicEqLength, int, 0)    GpakCfgVar (SpkrEqLength, int, 0)

   // VAD
   GpakCfgVar (VADEnabled, ADT_Bool, ADT_FALSE)      GpakCfgVar (VadNoiseFloor, int, -50)
   GpakCfgVar (VadHangTime, int, 500)                GpakCfgVar (VadWindowMs, int, 3)
   GpakCfgVar (VadReportEvents, ADT_Bool, ADT_FALSE)

   // Tone Detect / Generation
   GpakCfgVar (DtmfEnabled,     ADT_Bool, ADT_FALSE)   GpakCfgVar (Mfr1Enabled,    ADT_Bool, ADT_FALSE)
   GpakCfgVar (Mfr2FwdEnabled,  ADT_Bool, ADT_FALSE)   GpakCfgVar (Mfr2RevEnabled, ADT_Bool, ADT_FALSE)
   GpakCfgVar (CallProgEnabled, ADT_Bool, ADT_FALSE)   GpakCfgVar (ARBEnabled,     ADT_Bool, ADT_FALSE)
   GpakCfgVar (CEDEnabled,      ADT_Bool, ADT_FALSE)   GpakCfgVar (CNGEnabled,     ADT_Bool, ADT_FALSE)
   GpakCfgVar (ToneDetectBEnabled, ADT_Bool, ADT_FALSE)

   GpakCfgVar (MaxConcurrentTones, int, 1)
   GpakCfgVar (ToneGenInstances, int, 0)   GpakCfgVar (ToneRlyGenEnabled, ADT_Bool, ADT_FALSE)
   GpakCfgVar (DtmfDialEnabled, ADT_Bool, ADT_FALSE)
   GpakCfgVar (ToneDetInstances, int, 0)
   GpakCfgVar (ARBCfgCount, int, 0)

   // Vocoders
   GpakCfgVar (L16Enabled,     ADT_Bool, ADT_FALSE)      GpakCfgVar (L16WBEnabled,   ADT_Bool, ADT_FALSE)
   GpakCfgVar (G711Enabled,    ADT_Bool, ADT_TRUE)       GpakCfgVar (G711WBEnabled,  ADT_Bool, ADT_FALSE)
   GpakCfgVar (G722Enabled,    ADT_Bool, ADT_FALSE)
   GpakCfgVar (G723Enabled,    ADT_Bool, ADT_FALSE)      GpakCfgVar (TI_G7231AEnabled, ADT_Bool, ADT_FALSE)
   GpakCfgVar (G726LowMemEnabled, ADT_Bool, ADT_FALSE)   GpakCfgVar (G726LowMipsEnabled, ADT_Bool, ADT_FALSE)
   GpakCfgVar (G726Rate16Enabled, ADT_Bool, ADT_FALSE)   GpakCfgVar (G726Rate24Enabled, ADT_Bool, ADT_FALSE)
   GpakCfgVar (G726Rate32Enabled, ADT_Bool, ADT_FALSE)   GpakCfgVar (G726Rate40Enabled, ADT_Bool, ADT_FALSE)
   GpakCfgVar (G728Enabled,    ADT_Bool, ADT_FALSE)
   GpakCfgVar (G729ABEnabled,  ADT_Bool, ADT_FALSE)      GpakCfgVar (TI_G729ABEnabled, ADT_Bool, ADT_FALSE)
   GpakCfgVar (melpEnabled,    ADT_Bool, ADT_FALSE)      GpakCfgVar (melpEEnabled,   ADT_Bool, ADT_FALSE)
   GpakCfgVar (gsmEFREnabled,  ADT_Bool, ADT_FALSE)
   GpakCfgVar (SpeexEnabled,   ADT_Bool, ADT_FALSE)      GpakCfgVar (SpeexWBEnabled, ADT_Bool, ADT_FALSE)
   GpakCfgVar (Adt4800Enabled, ADT_Bool, ADT_FALSE)
   GpakCfgVar (LbCoderInstances, int, 0)

   // Serial Ports
   GpakCfgVar (Port1NumSlots, int, 0)   GpakCfgVar (Port1SlotsUsed, int, 0)
   GpakCfgVar (Port2NumSlots, int, 0)   GpakCfgVar (Port2SlotsUsed, int, 0)
   GpakCfgVar (Port3NumSlots, int, 0)   GpakCfgVar (Port3SlotsUsed, int, 0)

   GpakCfgVar (Port1Config.DataFormat, GpakCompandModes, cmpNone16)   GpakCfgVar (Port1Config.XmtDataDelay, int, 1)
   GpakCfgVar (Port1Config.RcvDataDelay, int, 1)                      GpakCfgVar (Port1Config.XmtSyncHigh, ADT_Bool, ADT_TRUE)
   GpakCfgVar (Port1Config.RcvSyncHigh,  ADT_Bool, ADT_TRUE)          GpakCfgVar (Port1Config.XmtClockRise, ADT_Bool, ADT_TRUE)
   GpakCfgVar (Port1Config.RcvClockRise, ADT_Bool, ADT_FALSE)         GpakCfgVar (Port1Config.DxDelayEnable, ADT_Bool, ADT_FALSE)
   GpakCfgVar (Port1Config.TsipEnable,   ADT_Bool, ADT_FALSE)

   GpakCfgVar (Port2Config.DataFormat, GpakCompandModes, cmpNone16)   GpakCfgVar (Port2Config.XmtDataDelay, int, 1)
   GpakCfgVar (Port2Config.RcvDataDelay, int, 1)                      GpakCfgVar (Port2Config.XmtSyncHigh, ADT_Bool, ADT_TRUE)
   GpakCfgVar (Port2Config.RcvSyncHigh,  ADT_Bool, ADT_TRUE)          GpakCfgVar (Port2Config.XmtClockRise, ADT_Bool, ADT_TRUE)
   GpakCfgVar (Port2Config.RcvClockRise, ADT_Bool, ADT_FALSE)         GpakCfgVar (Port2Config.DxDelayEnable, ADT_Bool, ADT_FALSE)
   GpakCfgVar (Port2Config.TsipEnable,   ADT_Bool, ADT_FALSE)

   GpakCfgVar (Port3Config.DataFormat, GpakCompandModes, cmpNone16)   GpakCfgVar (Port3Config.XmtDataDelay, int, 1)
   GpakCfgVar (Port3Config.RcvDataDelay, int, 1)                      GpakCfgVar (Port3Config.XmtSyncHigh, ADT_Bool, ADT_TRUE)
   GpakCfgVar (Port3Config.RcvSyncHigh,  ADT_Bool, ADT_TRUE)          GpakCfgVar (Port3Config.XmtClockRise, ADT_Bool, ADT_TRUE)
   GpakCfgVar (Port3Config.RcvClockRise, ADT_Bool, ADT_FALSE)         GpakCfgVar (Port3Config.DxDelayEnable, ADT_Bool, ADT_FALSE)
   GpakCfgVar (Port3Config.TsipEnable,   ADT_Bool, ADT_FALSE)

   // Frame Processing
   GpakCfgVar (FrameMS_1,    ADT_Bool, ADT_FALSE)   GpakCfgVar (Hint8,   ADT_Bool, ADT_FALSE)
   GpakCfgVar (FrameMS_2_5,  ADT_Bool, ADT_FALSE)   GpakCfgVar (Hint20,  ADT_Bool, ADT_FALSE)
   GpakCfgVar (FrameMS_5,    ADT_Bool, ADT_FALSE)   GpakCfgVar (Hint40,  ADT_Bool, ADT_FALSE)
   GpakCfgVar (FrameMS_10,   ADT_Bool, ADT_TRUE)    GpakCfgVar (Hint80,  ADT_Bool, ADT_FALSE)
   GpakCfgVar (FrameMS_20,   ADT_Bool, ADT_FALSE)   GpakCfgVar (Hint160, ADT_Bool, ADT_FALSE)
   GpakCfgVar (FrameMS_22_5, ADT_Bool, ADT_FALSE)   GpakCfgVar (Hint180, ADT_Bool, ADT_FALSE)
   GpakCfgVar (FrameMS_30,   ADT_Bool, ADT_FALSE)   GpakCfgVar (Hint240, ADT_Bool, ADT_FALSE)

   GpakCfgVar (Pcm2PktChan, ADT_Bool, ADT_TRUE)    GpakCfgVar (Pcm2PcmChan, ADT_Bool, ADT_FALSE)
   GpakCfgVar (Pkt2PktChan, ADT_Bool, ADT_FALSE)   GpakCfgVar (CircDataChan, ADT_Bool, ADT_FALSE)
   GpakCfgVar (ConfPcmChan, ADT_Bool, ADT_FALSE)   GpakCfgVar (ConfPktChan, ADT_Bool, ADT_FALSE)
   GpakCfgVar (ConfCompChan, ADT_Bool, ADT_FALSE)

   // Noise Suppression
   GpakCfgVar (NoiseSuppressionEnabled, ADT_Bool, ADT_FALSE)
   GpakCfgVar (NseSpr.MaxAttendB, int, 25)        GpakCfgVar (NseSpr.VadLowThreshdB, int, -50)
   GpakCfgVar (NseSpr.VadHighThreshdB, int, -30)  GpakCfgVar (NseSpr.VadHangMSec, int, 200)
   GpakCfgVar (NseSpr.VadWindowSize, int, 5)

   // Miscellaneous
   GpakCfgVar (HostInterfaceAddress, int, 0)
   GpakCfgVar (DataMemory, int, 0)
   GpakCfgVar (HostIF, int, 0)
   GpakCfgVar (ExtMemConfig.type, int, EXT_NONE)

   // RTP / SRTP
   GpakCfgVar (PktProfile, GpakProfiles, RTPAVP)
   GpakCfgVar (JBMaxMS, int, 0)
   GpakCfgVar (RtpMaxBuffsPerChan, int, 2)
   GpakCfgVar (RtpToneType, GpakRTPPkts, NoTonePkt)

   GpakCfgVar (SRTP_Enable, ADT_Bool, ADT_FALSE)
   GpakCfgVar (SRTPInstances, int, 0)

   //  Hand manipulated
   GpakCfgVar (SrcInstances, int, 0)
   GpakCfgVar (MaxCustomFrameRateMs, int, 0)
   GpakCfgVar (MaxPktBufDelayMS, int, 0)
   { NULL }
};

//}

int TSIPEnable[3] = {0,0,0};

// Subordinate dialog entry variables.
EchoCancelInfo_t EcanConfig; // dialog copy of Echo Canceller config info
SerialPortInfo_t PortConfig; // dialog copy of Serial Port config info

int NumExtMemObjs = 0;   // Number of objects located in ext memory
int NumConferences = 0;  // total number of Conferences
int numAtccBlocks = 0;
int MinFrameSize  = 240;
int MaxFrameSize  = 0;
int TxCidDTMFIntances = 0;
unsigned int AECScratchSize16 = 0;
unsigned int AECInstanceSize16 = 0;
ADT_UInt16 shortTailEC = 0;
ADT_UInt16 longTailEC  = 0;
ADT_UInt16 CEDDetInstances = 0;
ADT_UInt16 CNGDetInstances = 0;
ADT_UInt16 ARBDetInstances = 0;

CID_Params_t CIDConfigDlg;      // dialog copy of Caller ID config info
CID_Params_t CIDConfigLcl;      // local copy of Caller ID config info

ADT_Bool ForceRebuild = FALSE;
char *LibErrors;
char Title[200];
char CfgFileName [100];
char RootFileName[100];
char RootPath[100];

static char Buffer[200];  // text string buffer

typedef struct AECSizeDef {
   int tailLengthms;
   int scratchSize16;
   int instanceSize16;
} AECSizeDef;

AECSizeDef AECSizing [] = {
   {  16, 0, 0 },
   {  32, 0, 0 },
   {  64, 0, 0 },
   { 128, 0, 0 },
   { 256, 0, 0 },
   { 384, 0, 0 },
   { 512, 0, 0 },
   {   0 }
};


   
static bool StringToInt (char *pText, int TextLen, int *pAddress) {
   char c;      // temp character
   int Value;   // converted value
   int Sign;

   if (TextLen < 1)      return (FALSE);
   while (*pText == ' ')   pText++;

   if (*pText == '-') {
      Sign = -1;
      pText++;
   } else {
      Sign = 1;
   }

   Value = 0;
   if ((pText[0] == '0') && ((pText[1] == 'x') || (pText[1] == 'X')))   {
      pText += 2;
      if (*pText == '\0')         return (FALSE);
      while ((c = *pText++) != '\0')      {
         Value = Value << 4;
         if ((c >= '0') && (c <= '9'))      Value += ((int) (c - '0'));
         else if ((c >= 'A') && (c <= 'F'))   Value += (10 + ((int) (c - 'A')));
         else if ((c >= 'a') && (c <= 'f'))   Value += (10 + ((int) (c - 'a')));
         else            return (FALSE);
      }
   }   else   {
      if (*pText == '\0')         return (FALSE);
      while ((c = *pText++) != '\0')      {
         if ((c < '0') || (c > '9'))            return (FALSE);
         Value = (Value * 10) + ((int) (c - '0'));
      }
   }
   *pAddress = Sign * Value;
   return (TRUE);
}
void SetDlgItemLongHex (CDialog *Dlg, int Fld, long value) {
   char temp [12];
   sprintf (temp, "0x%08lX", value);
   Dlg->SetDlgItemText  (Fld,  (LPSTR) temp);
}
void SetDlgItemHex (CDialog *Dlg, int Fld, int value) {
   char temp [8];
   sprintf (temp, "0x%04X", value);
   Dlg->SetDlgItemText  (Fld,  (LPSTR) temp);
}
int GetDlgItemHex  (CDialog *Dlg, int FieldIdx) {
   int Field = 0;
   int TextLen;
   char TextBufr [16];
   bool success;

   TextLen = Dlg->GetDlgItemText (FieldIdx, (LPSTR) &TextBufr, sizeof(TextBufr));
  
   success = StringToInt (TextBufr, TextLen, &Field);
   if (!success)  AfxMessageBox ("The field must be integer or hex!");

    return Field;

}
int GetComboItem   (CDialog * dialog, int Fld, int Dflt) {

   CComboBox *pCombo;
   int Index, SelectedValue;
   
   pCombo = (CComboBox *) dialog->GetDlgItem (Fld);
   if (pCombo == NULL) return Dflt;

   Index = pCombo->GetCurSel();
   if (Index == CB_ERR) return Dflt;

   SelectedValue = pCombo->GetItemData(Index);
   if (SelectedValue == CB_ERR) return Dflt;

   return SelectedValue;
}

/////////////////////////////////////////////////////////////////////////////
//{ CAboutDlg dialog used for App About
class CAboutDlg : public CDialog {
public:
   CAboutDlg();

// Dialog Data
   //{{AFX_DATA(CAboutDlg)
   enum { IDD = IDD_ABOUTBOX };
   //}}AFX_DATA

   // ClassWizard generated virtual function overrides
   //{{AFX_VIRTUAL(CAboutDlg)
   public:
   protected:
   virtual void DoDataExchange(CDataExchange* pDX);   // DDX/DDV support
   //}}AFX_VIRTUAL

// Implementation
protected:
   //{{AFX_MSG(CAboutDlg)
   virtual ADT_Bool OnInitDialog();
   //}}AFX_MSG
   DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog (CAboutDlg::IDD) {
   //{{AFX_DATA_INIT(CAboutDlg)
   //}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange (CDataExchange* pDX) {
   CDialog::DoDataExchange(pDX);
   //{{AFX_DATA_MAP(CAboutDlg)
   //}}AFX_DATA_MAP
}

ADT_Bool CAboutDlg::OnInitDialog() {
   CDialog::OnInitDialog();

   SetDlgItemText  (FldLibInfo,  (LPSTR) LibErrors);
    
   return TRUE;  // return TRUE unless you set the focus to a control
      // EXCEPTION: OCX Property Pages should return FALSE
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
   //{{AFX_MSG_MAP(CAboutDlg)
   //}}AFX_MSG_MAP
END_MESSAGE_MAP()
//}


/////////////////////////////////////////////////////////////////////////////
CGpakConfigDlg::CGpakConfigDlg(CWnd* pParent) : CDialog (CGpakConfigDlg::IDD, pParent) {
   //{{AFX_DATA_INIT(CGpakConfigDlg)
      // NOTE: the ClassWizard will add member initialization here
   //}}AFX_DATA_INIT
   // Note that LoadIcon does not require a subsequent DestroyIcon in Win32
   m_hIcon = AfxGetApp()->LoadIcon(IDI_ICON1);
}
void CGpakConfigDlg::DoDataExchange(CDataExchange* pDX) {
   CDialog::DoDataExchange(pDX);
   //{{AFX_DATA_MAP(CGpakConfigDlg)
      // NOTE: the ClassWizard will add DDX and DDV calls here
   //}}AFX_DATA_MAP
}
ADT_Bool CGpakConfigDlg::OnInitDialog () {


   CDialog::OnInitDialog ();

   GpakVarDef *var = gpakVars;

   // Add "About..." menu item to main menu.

   // IDM_ABOUTBOX must be in the system command range.
   ASSERT ((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
   ASSERT (IDM_ABOUTBOX < 0xF000);

   CMenu* pSysMenu = GetSystemMenu (FALSE);
   if (pSysMenu != NULL)   {
      CString strAboutMenu;
      strAboutMenu.LoadString (IDS_ABOUTBOX);
      if (!strAboutMenu.IsEmpty())      {
         pSysMenu->AppendMenu (MF_SEPARATOR);
         pSysMenu->AppendMenu (MF_STRING, IDM_ABOUTBOX, strAboutMenu);
      }
   }

   // Set the icon for this dialog.  The framework does this automatically
   //  when the application's main window is not a dialog
   SetIcon (m_hIcon, TRUE);      // Set big icon

   // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   // Set all configurable parameters to their default values.
   while (var->name != NULL) {
      switch (var->varI8) {
      case 2: * ((ADT_Int16 *) var->pointer) = var->dflt; break;
      case 4: * ((ADT_Int32 *) var->pointer) = var->dflt; break;
      default: printf ("Unknown size %d", var->varI8); break;
      }
      var++;
   }
   
   //---------------------------------------------------
   // Non-saved values
   CIDConfigLcl.type2CID    = 0;
   CIDConfigLcl.fskLevel           = -10;
   CIDConfigLcl.fskType            = fskNone;
   CIDConfigLcl.msgType            = 0;
   CIDConfigLcl.numMarkBytes       = 23;
   CIDConfigLcl.numPostambleBytes  = 10;
   CIDConfigLcl.numSeizureBytes    = 38;

   NumConferences = 0;      
   TxCidDTMFIntances = 0;

   // default stub and component path
   strcpy (CompPath, "components");
   strcpy (StubPath, "stubs");
   strcpy (GpakPath, "");

   GetProjectInfo ();   
   return TRUE;  // return TRUE  unless you set the focus to a control
}
// CGpakConfigDlg dialog
BEGIN_MESSAGE_MAP(CGpakConfigDlg, CDialog)
   //{{AFX_MSG_MAP(CGpakConfigDlg)
   ON_WM_SYSCOMMAND()
   ON_WM_PAINT()
   ON_WM_QUERYDRAGICON()
   ON_BN_CLICKED(IDC_BUTTON_PCMECCONFIG, OnButtonPcmEcConfig)
   ON_BN_CLICKED(IDC_BUTTON_PKTECCONFIG, OnButtonPktEcConfig)
   ON_BN_CLICKED(IDC_BUTTON_P1CONFIG, OnButtonP1Config)
   ON_BN_CLICKED(IDC_BUTTON_P2CONFIG, OnButtonP2Config)
   ON_BN_CLICKED(IDC_BUTTON_P3CONFIG, OnButtonP3Config)
   ON_BN_CLICKED(IDC_BUTTON_BUILD, OnButtonBuild)
   ON_BN_CLICKED(IDC_RADIO_AAL2PROFILE, OnRadioAAL2Profile)
   ON_BN_CLICKED(IDC_RADIO_RTPPROFILE, OnRadioRtpProfile)
   ON_BN_CLICKED(IDC_CHECK_ECSHORTTAIL, OnCheckEcShortTail)
   ON_BN_CLICKED(IDC_CHECK_VADENABLE, OnCheckVadEnable)
   ON_BN_CLICKED(IDC_CHECK_G726LOWMEM, OnCheckG726LowMem)
   ON_BN_CLICKED(IDC_CHECK_G726LOWMIPS, OnCheckG726LowMips)
   ON_EN_CHANGE(IDC_EDIT_P1NUMSLOTS,  OnChangeEditP1NumSlots)
   ON_EN_CHANGE(IDC_EDIT_P2NUMSLOTS,  OnChangeEditP2NumSlots)
   ON_EN_CHANGE(IDC_EDIT_P3NUMSLOTS,  OnChangeEditP3NumSlots)
   ON_BN_CLICKED(IDC_CHECK_G72616K, OnCheckG72616k)
   ON_BN_CLICKED(IDC_CHECK_G72624K, OnCheckG72624k)
   ON_BN_CLICKED(IDC_CHECK_G72632K, OnCheckG72632k)
   ON_BN_CLICKED(IDC_CHECK_G72640K, OnCheckG72640k)
   ON_BN_CLICKED(IDC_CHECK_G723, OnCheckG723)
   ON_BN_CLICKED(IDC_CHECK_G728, OnCheckG728)
   ON_BN_CLICKED(IDC_CHECK_GSM_EFR, OnCheckGSMEFR)
   ON_BN_CLICKED(IDC_CHECK_ADT4800, OnCheckAdt4800)
   ON_BN_CLICKED(IDC_CHECK_G722, OnCheckG722)
   ON_BN_CLICKED(IDC_RADIO_RTPTONEEVENT, OnRadioRtpToneEvent)
   ON_BN_CLICKED(IDC_RADIO_RTPTONETONE,  OnRadioRtpToneTone)
   ON_BN_CLICKED(IDC_RADIO_RTPTONE_NONE, OnRadioRtpToneNone)
   ON_BN_CLICKED(IDC_CHECK_FRAME8, OnCheckFrame)
   ON_EN_CHANGE(IDC_EDIT_PCMECNUM, OnChangeEditPcmecnum)
   ON_EN_KILLFOCUS(IDC_EDIT_PCMECNUM, OnKillFocusPcmecnum)
   ON_EN_CHANGE(IDC_EDIT_PKTECNUM, OnChangeEditPktecnum)
   ON_EN_KILLFOCUS(IDC_EDIT_PKTECNUM, OnKillFocusPktecnum)
   ON_BN_CLICKED(IDC_CHECK_TONERLYGENENAB, OnCheckTonerlygenenab)
   ON_BN_CLICKED(FLG_NoiseSuppressionEnabled, OnCheckNoiseSuppressionEnable)
   ON_BN_CLICKED(IDC_CHECK_G729AB, OnCheckG729)
   ON_BN_CLICKED(IDC_BUTTON_EXTMEM_CONFIG, OnButtonExtmemConfig)
   ON_EN_CHANGE(FldNumAEC, OnChangeEditAECecnum)
   ON_EN_KILLFOCUS(FldNumAEC, OnKillFocusAECecnum)
   ON_EN_CHANGE(IDC_EDIT_NUMT38_INSTANCES, OnChangeEditNumt38Instances)
   ON_EN_UPDATE(FldAGCInstances, OnUpdateFldAGCInstances)
   ON_EN_UPDATE(IDC_EDIT_NUMCONFERENCES, OnUpdateEditNumconferences)
   ON_EN_UPDATE(IDC_EDIT_MAXDETTONES, OnUpdateEditMaxdettones)
   ON_BN_CLICKED(FlgToneDetectB, OnCheckFlgToneDetectB)
   ON_EN_CHANGE(FldToneGenInstances, OnChangeFldToneGenInstances)
   ON_CBN_SELCHANGE(CmbDSP, OnSelchangeCmbDSP)
   ON_BN_CLICKED(IDC_BUTTON_PATH, OnButtonPath)
   ON_BN_CLICKED(IDC_CHECK_MELP, OnCheckMelp)
   ON_BN_CLICKED(IDC_BUTTON_BUILD2, OnButtonBuild2)
   ON_BN_CLICKED(IDC_BUTTON_CIDCFG, OnButtonCidcfg)
	ON_BN_CLICKED(IDC_CHECK_TI_G729AB, OnCheckTiG729ab)
	ON_BN_CLICKED(IDC_CHECK_TI_G7231A, OnCheckTiG7231a)
	ON_BN_CLICKED(IDC_CHECK_VADREPORT, OnCheckVadreport)
	ON_EN_CHANGE(FldLbCoderInstances, OnChangeFldLbCoderInstances)
	ON_BN_CLICKED(TI_T38_CHECKBOX, OnT38Checkbox)
	ON_BN_CLICKED(FlgARB, OnCheckedArb)
	ON_BN_CLICKED(DSPStackCheckBox, OnDSPStackCheckBox)
	ON_BN_CLICKED(SRTP_Enabled, SRTP_checkbox)
	ON_BN_CLICKED(IDC_CHECK_16KHZ_TDM, OnCheck16khzTdm)
	ON_BN_CLICKED(IDC_CHECK_G711, OnCheckG711)
	ON_BN_CLICKED(IDC_CHECK_G711_WB, OnCheckG711Wb)
	ON_BN_CLICKED(IDC_CHECK_L16, OnCheckL16)
	ON_BN_CLICKED(IDC_CHECK_L16_WB, OnCheckL16Wb)
	ON_BN_CLICKED(IDC_CHECK_SPEEX, OnCheckSpeex)
	ON_BN_CLICKED(IDC_CHECK_SPEEX_WB, OnCheckSpeexWb)
   ON_EN_CHANGE(IDC_EDIT_P1SLOTSUSED, OnChangeEditP1NumSlots)
   ON_EN_CHANGE(IDC_EDIT_P2SLOTSUSED, OnChangeEditP2NumSlots)
   ON_EN_CHANGE(IDC_EDIT_P3SLOTSUSED, OnChangeEditP3NumSlots)
   ON_BN_CLICKED(IDC_CHECK_FRAME20, OnCheckFrame)
   ON_BN_CLICKED(IDC_CHECK_FRAME40, OnCheckFrame)
   ON_BN_CLICKED(IDC_CHECK_FRAME80, OnCheckFrame)
   ON_BN_CLICKED(IDC_CHECK_FRAME160, OnCheckFrame)
   ON_BN_CLICKED(IDC_CHECK_FRAME240, OnCheckFrame)
   ON_BN_CLICKED(IDC_CHECK_FRAME180, OnCheckFrame)
   ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_CHECK_MELPE, OnCheckMelpe)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// CGpakConfigDlg message handlers
void CGpakConfigDlg::OnSysCommand (UINT nID, LPARAM lParam) {
   if ((nID & 0xFFF0) == IDM_ABOUTBOX)   {
      CAboutDlg dlgAbout;
      dlgAbout.DoModal();
   }   else   {
      CDialog::OnSysCommand(nID, lParam);
   }
}
void CGpakConfigDlg::OnPaint()  {


   if (IsIconic())    {
      CPaintDC dc(this); // device context for painting

      SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

      // Center icon in client rectangle
      int cxIcon = GetSystemMetrics(SM_CXICON);
      int cyIcon = GetSystemMetrics(SM_CYICON);
      CRect rect;
      GetClientRect(&rect);
      int x = (rect.Width() - cxIcon + 1) / 2;
      int y = (rect.Height() - cyIcon + 1) / 2;

      // Draw the icon
      dc.DrawIcon(x, y, m_hIcon);
   }   else   {
      CDialog::OnPaint();
   }
}
HCURSOR CGpakConfigDlg::OnQueryDragIcon() {
   return (HCURSOR) m_hIcon;
}
/////////////////////////////////////////////////////////////////////////////



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Frame Sizes
void CGpakConfigDlg::OnCheckFrame() {

   UpdateMaxFrameSizeSelection();
}
void CGpakConfigDlg::UpdateMaxFrameSizeSelection() {
   CWnd *pObj;      // pointer to Window object

   // Determine the maximum frame size and enable/disable the Host Port
   // Interrupt selection fields.
   MinFrameSize = 240;
   MaxFrameSize = 0;

   SetFrameInterrupt  (1,    8);
   SetFrameInterrupt  (2_5, 20);
   SetFrameInterrupt  (5,   40);
   SetFrameInterrupt  (10,  80);
   SetFrameInterrupt  (20, 160);
   SetFrameInterrupt (22_5, 180);
   SetFrameInterrupt (30,   240);

   return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Function to initialize an DSP selection Combo Box.
void InitDSPComboBox (CDialog *Dlg, int InitValue) {
   int index, default_index = CB_ERR, selected_index = CB_ERR;  
   CComboBox *pCombo;

   pCombo = (CComboBox *) Dlg->GetDlgItem (CmbDSP);
   if (pCombo == NULL) return;

   AddEnum (Ti5410, Ti6424);
   AddEnum (Ti5416, Ti6424);
   AddEnum (Ti5420, Ti6424);
   AddEnum (Ti5421, Ti6424);
   AddEnum (Ti5441, Ti6424);

   AddEnum (Ti5510, Ti6424);
   AddEnum (Ti6416,  Ti6424);
   AddEnum (Ti6424,  Ti6424);
   AddEnum (Ti6452,  Ti6424);
   AddEnum (Ti647,  Ti6424);
   AddEnum (Ti674,  Ti6424);
   AddEnum (Ti665,  Ti6424);
   AddEnum (Ti667,  Ti6424);

   AddEnum (TiDM642, Ti6424);
   AddEnum (TiDM6437,Ti6424);
   AddEnum (TiDM648, Ti6424);
   AddEnum (TiDM816x,Ti6424);

   AddEnum (TiOML137, Ti6424);
   AddEnum (TiOML138, Ti6424);
   AddEnum (TiOM3530, Ti6424);

   if (selected_index == CB_ERR) selected_index = default_index;
   pCombo->SetCurSel (selected_index);

}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Function to initialize an AEC selection Combo Box.
void InitAECComboBox (CDialog *Dlg, int InitValue) {
   int i, index, default_index = CB_ERR, selected_index = CB_ERR;  
   int indexCnt = 0;
   char buffer[20];
   CComboBox *pCombo;
   AECSizeDef *AECInst;

   pCombo = (CComboBox *) Dlg->GetDlgItem (CmbAECTailLen);
   if (pCombo == NULL) return;

   // Add strings to combo box.
   for (i=0; AECSizing[i].tailLengthms != 0; i++) {
      itoa (AECSizing[i].tailLengthms, buffer, 10);
      index = pCombo->InsertString (-1, (LPCTSTR) buffer);
      if (index == CB_ERR) continue;
      pCombo->SetItemDataPtr (index, &AECSizing[i]);
      indexCnt++;
   }

   // Find element matching initialization value and select that value as default.
   for (index=0; index < indexCnt; index++) {
      AECInst = (AECSizeDef *) pCombo->GetItemDataPtr (index);
      if (AECInst->tailLengthms == InitValue) selected_index = index;
      if (default_index == CB_ERR)            default_index = index;
   }

   if (selected_index == CB_ERR) selected_index = default_index;
   pCombo->SetCurSel (selected_index);

}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// AGC 
void CGpakConfigDlg::OnUpdateFldAGCInstances() {

   UpdateAgcSelection();
   
}
void CGpakConfigDlg::UpdateAgcSelection() {
   CWnd *pObj;         // pointer to Window object

   ADT_Bool State;   // enable state
   ADT_Bool ValidInt;   // flag indicating valid integer

   // Enable / Disable AGC parameter fields.
   AgcInstances = GetDlgItemInt (FldAGCInstances, &ValidInt, UNSIGNED);
   State = (AgcInstances != 0 && ValidInt);

   SetState (FldAgcTargetPowerA, State);
   SetState (FldAgcLossLimitA, State);
   SetState (FldAgcGainLimitA, State);
   SetState (FldAgcLowSigThreshA, State);

   SetState (FldAgcTargetPowerB, State);
   SetState (FldAgcLossLimitB, State);
   SetState (FldAgcGainLimitB, State);
   SetState (FldAgcLowSigThreshB, State);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Short Tail Echo Canceller check box.
void CGpakConfigDlg::OnCheckEcShortTail() {

   EcShortTailEnabled = IsDlgButtonChecked(IDC_CHECK_ECSHORTTAIL);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//  PCM Echo Cancellers
void CGpakConfigDlg::OnKillFocusPcmecnum () {
   ADT_Bool ValidInt;   // flag indicating valid integer
   int Value;      // integer value

   // Get and validate the entered Number Of PCM Echo Cancellers value.
   Value = (int) GetDlgItemInt (IDC_EDIT_PCMECNUM, &ValidInt, UNSIGNED);
   if (!ValidInt)   {
   AfxMessageBox ("The Number Of PCM Echo Cancellers is invalid!");
      NumPcmEcans = 0;
   }   else
      NumPcmEcans = Value;

   UpdatePcmEcSelection();
}
void CGpakConfigDlg::OnChangeEditPcmecnum () {
   ADT_Bool ValidInt;   // flag indicating valid integer
   int Value;      // integer value

   // Get and validate the entered Number Of PCM Echo Cancellers value.
   Value = (int) GetDlgItemInt (IDC_EDIT_PCMECNUM, &ValidInt, UNSIGNED);
   if (!ValidInt)   {
      NumPcmEcans = 0;
   }   else
      NumPcmEcans = Value;

   UpdatePcmEcSelection();
}
void CGpakConfigDlg::UpdatePcmEcSelection () {
   CWnd *pObj;         // pointer to Window object
   ADT_Bool State;   // enable state

   // Enable / Disable the PCM Echo Cancel Configuration button.
   State =  (NumPcmEcans != 0);

   if ((pObj = GetDlgItem (IDC_BUTTON_PCMECCONFIG)) != NULL)   pObj->EnableWindow (State);
}
void CGpakConfigDlg::OnButtonPcmEcConfig() {

   // Use the Echo Canceller dialog to configure the PCM Echo Canceller.
   m_EchoCancel.m_EcanType = 0;
   m_EchoCancel.m_ShortTail = EcShortTailEnabled;
   EcanConfig = PcmEcanConfig;
   if (m_EchoCancel.DoModal() == IDOK)   {
   PcmEcanConfig = EcanConfig;
   }
}

//-----------------------------------------------------------------
// Packet Echo Cancellers 
void CGpakConfigDlg::OnKillFocusPktecnum () {
   ADT_Bool ValidInt;   // flag indicating valid integer
   int Value;      // integer value

   // Get and validate the entered Number Of Packet Echo Cancellers value.
   Value = (int) GetDlgItemInt(IDC_EDIT_PKTECNUM, &ValidInt, UNSIGNED);
   if (!ValidInt)   {
      AfxMessageBox("The Number Of Packet Echo Cancellers is invalid!");
      NumPktEcans = 0;
   }   else
      NumPktEcans = Value;
   UpdatePcktEcSelection();
}
void CGpakConfigDlg::OnChangeEditPktecnum () {
   ADT_Bool ValidInt;   // flag indicating valid integer
   int Value;      // integer value

   // Get and validate the entered Number Of Packet Echo Cancellers value.
   Value = (int) GetDlgItemInt(IDC_EDIT_PKTECNUM, &ValidInt, UNSIGNED);
   if (!ValidInt)   {
      NumPktEcans = 0;
   }   else
      NumPktEcans = Value;
   UpdatePcktEcSelection();
}
void CGpakConfigDlg::UpdatePcktEcSelection () {
   CWnd *pObj;         // pointer to Window object
   ADT_Bool State;   // enable state

   // Enable / Disable the Packet Echo Cancel Configuration button.
   State = (NumPktEcans != 0);

   if ((pObj = GetDlgItem(IDC_BUTTON_PKTECCONFIG)) != NULL)      pObj->EnableWindow(State);
}
void CGpakConfigDlg::OnButtonPktEcConfig () {

   // Use the Echo Canceller dialog to configure the Packet Echo Canceller.
   m_EchoCancel.m_EcanType = 1;
   m_EchoCancel.m_ShortTail = EcShortTailEnabled;
   EcanConfig = PktEcanConfig;
   if (m_EchoCancel.DoModal() == IDOK)   {
   PktEcanConfig = EcanConfig;
   }
}

//-----------------------------------------------------------------
// Acoustic Echo Cancellers 
void CGpakConfigDlg::OnKillFocusAECecnum () {
   ADT_Bool ValidInt;   // flag indicating valid integer
   int Value;      // integer value

   // Get and validate the entered Number Of Packet Echo Cancellers value.
   Value = (int) GetDlgItemInt (FldNumAEC, &ValidInt, UNSIGNED);
   if (!ValidInt)   {
      AfxMessageBox ("The Number Of Acoustic Echo Cancellers is invalid!");
      NumAECEcans = 0;
   }   else
      NumAECEcans = Value;
   UpdateAECSelection();
}
void CGpakConfigDlg::OnChangeEditAECecnum () {
   ADT_Bool ValidInt;   // flag indicating valid integer
   int Value;      // integer value

   // Get and validate the entered Number Of Packet Echo Cancellers value.
   Value = (int) GetDlgItemInt (FldNumAEC, &ValidInt, UNSIGNED);
   if (!ValidInt)   {
      NumAECEcans = 0;
   }   else
      NumAECEcans = Value;
   UpdateAECSelection();
}
void CGpakConfigDlg::UpdateAECSelection () {
   CWnd *pObj;         // pointer to Window object
   ADT_Bool TailState, InstanceState;   // enable state

   // Enable / Disable the Packet Echo Cancel Configuration button.
   InstanceState = (DspTypeInfo.IsC64x);
   TailState = (NumAECEcans != 0) & InstanceState;
   if ((pObj = GetDlgItem(FldNumAEC)) != NULL)       pObj->EnableWindow(InstanceState);
   if ((pObj = GetDlgItem(CmbAECTailLen)) != NULL)   pObj->EnableWindow(TailState);
}
void GetAECInfo (CDialog *Dlg) {
   CComboBox  *pCombo;
   AECSizeDef *AECInst;
   int Index;

   AECTailLen = 0;
   AECScratchSize16 = 0;
   AECInstanceSize16 = 0;

   if (NumAECEcans == 0) return;

   pCombo = (CComboBox *) Dlg->GetDlgItem (CmbAECTailLen);
   if (pCombo == NULL) return;
   
   Index = pCombo->GetCurSel();
   if (Index == CB_ERR) return;


   AECInst = (AECSizeDef *) pCombo->GetItemDataPtr (Index);
   if (AECInst == NULL) return;

   AECTailLen        = AECInst->tailLengthms;
   AECScratchSize16  = AECInst->scratchSize16;
   AECInstanceSize16 = AECInst->instanceSize16;
}

//--------------------------------------------------------
// VAD 
void CGpakConfigDlg::OnCheckVadEnable() {
   UpdateVadSelection();
}
void CGpakConfigDlg::UpdateVadSelection() {
   CWnd *pObj;         // pointer to Window object
   ADT_Bool State;   // enable state

   // Enable / Disable VAD parameter fields.
   State = (IsDlgButtonChecked(IDC_CHECK_VADENABLE));

   SetItemState (VADNOISEFLOOR, State);
   SetItemState (VADHANGTIME, State);
   SetItemState (VADWINSIZE, State);
   VADEnabled = State;

   if (VADEnabled) {
        if ((pObj = GetDlgItem(IDC_CHECK_VADREPORT)) != NULL) {
            pObj->EnableWindow(1);
            CheckDlgButton (IDC_CHECK_VADREPORT,  VadReportEvents);
        }
   } else {
        CheckDlgButton (IDC_CHECK_VADREPORT,  VadReportEvents);
        if ((pObj = GetDlgItem(IDC_CHECK_VADREPORT)) != NULL)       
            pObj->EnableWindow(0);
   }

}
void CGpakConfigDlg::OnCheckVadreport() {
    VadReportEvents	 = IsDlgButtonChecked (IDC_CHECK_VADREPORT);   
}


//---------------------------------------------------------
// RTP Packet Type and Tone
void CGpakConfigDlg::OnRadioAAL2Profile () {

   if (IsDlgButtonChecked(IDC_RADIO_AAL2PROFILE))   {
   PktProfile = AAL2Trunking;
   UpdatePktTypeSelection();
   }
}
void CGpakConfigDlg::OnRadioRtpProfile () {

   if (IsDlgButtonChecked(IDC_RADIO_RTPPROFILE))   {
   PktProfile = RTPAVP;
   UpdatePktTypeSelection();
   }
}
void CGpakConfigDlg::OnRadioRtpToneTone () {

   if (IsDlgButtonChecked(IDC_RADIO_RTPTONETONE))   RtpToneType = TonePkt;
   UpdatePktTypeSelection();
}
void CGpakConfigDlg::OnRadioRtpToneEvent () {

   if (IsDlgButtonChecked(IDC_RADIO_RTPTONEEVENT))
      if (IsDlgButtonChecked(IDC_RADIO_RTPPROFILE)) 
   RtpToneType = EventPkt;
   else 
   RtpToneType = (GpakRTPPkts) DialedDigitsPkt;
   else
   RtpToneType = NoTonePkt;

   UpdatePktTypeSelection ();
}
void CGpakConfigDlg::OnRadioRtpToneNone () {

   if (IsDlgButtonChecked(IDC_RADIO_RTPTONE_NONE))   RtpToneType = NoTonePkt;
   UpdatePktTypeSelection ();
}
void CGpakConfigDlg::UpdatePktTypeSelection () {
   CWnd *pObj;         // pointer to Window object
   ADT_Bool RtpState;   // RTP enable state

   // Enable / Disable RTP Tone Packet Type fields.
   RtpState =  (PktProfile == RTPAVP);

   if ((pObj = GetDlgItem (IDC_STATIC_RTPTONEPKTTYPE)) != NULL)   pObj->EnableWindow (RtpState);
   if ((pObj = GetDlgItem (IDC_RADIO_RTPTONETONE)) != NULL)      pObj->EnableWindow (RtpState);

   // Update the RTP Tone Type display.
   if (RtpState == FALSE) { 
   if (RtpToneType == EventPkt)
   RtpToneType = (GpakRTPPkts) DialedDigitsPkt;
   else if (RtpToneType != DialedDigitsPkt)
   RtpToneType = NoTonePkt;
   }

   CheckDlgButton (IDC_RADIO_RTPTONETONE,  RtpToneType == TonePkt);
   CheckDlgButton (IDC_RADIO_RTPTONEEVENT, (RtpToneType == EventPkt) || (RtpToneType == DialedDigitsPkt));
   CheckDlgButton (IDC_RADIO_RTPTONE_NONE, RtpToneType == NoTonePkt);

}
void CGpakConfigDlg::OnChangeFldToneGenInstances()  {
   ADT_Bool ValidInt;   // flag indicating valid integer
   int Value;      // integer value

   Value = (int) GetDlgItemInt (FldToneGenInstances, &ValidInt, UNSIGNED);
   if (!ValidInt)   {
   AfxMessageBox ("The Number Of tone generation instances is invalid!");
      ToneGenInstances = 0;
   }   else
      ToneGenInstances = Value;
   
}
void CGpakConfigDlg::OnCheckFlgToneDetectB() {
   ToneDetectBEnabled = IsDlgButtonChecked (FlgToneDetectB);
}
void CGpakConfigDlg::OnCheckTonerlygenenab() {

   ToneRlyGenEnabled = IsDlgButtonChecked (IDC_CHECK_TONERLYGENENAB);
}
void CGpakConfigDlg::OnUpdateEditMaxdettones() {
   UpdateToneDetSelection ();   
}
void CGpakConfigDlg::UpdateToneDetSelection () {
   CWnd *pObj;         // pointer to Window object
   ADT_Bool State;   // enable state
   ADT_Bool ValidInt;
   
   State = GetDlgItemInt(IDC_EDIT_MAXDETTONES, &ValidInt, UNSIGNED) != 0;

   // Enable/Disable Tone detection
   DisableObj (IDC_CHECK_DTMFENABLE, State);
   DisableObj (IDC_CHECK_MFR1ENABLE, State);
   DisableObj (IDC_CHECK_MFR2FWDENABLE, State);
   DisableObj (IDC_CHECK_MFR2REVENABLE, State);
   DisableObj (IDC_CHECK_MFR2REVENABLE, State);
   DisableObj (IDC_CHECK_CALLPROGENABLE, State);
   DisableObj (FlgCED, State);
   DisableObj (FlgCNG, State);
   DisableObj (FlgARB, State);
   DisableObj (FlgToneDetectB, State);
}

//---------------------------------------------------------
// Vocoders 
void CGpakConfigDlg::OnCheckG72616k () {

   G726Rate16Enabled = IsDlgButtonChecked (IDC_CHECK_G72616K);
}
void CGpakConfigDlg::OnCheckG72624k () {

   G726Rate24Enabled = IsDlgButtonChecked (IDC_CHECK_G72624K);
}
void CGpakConfigDlg::OnCheckG72632k () {

   G726Rate32Enabled = IsDlgButtonChecked (IDC_CHECK_G72632K);
}
void CGpakConfigDlg::OnCheckG72640k () {

   G726Rate40Enabled = IsDlgButtonChecked (IDC_CHECK_G72640K);
}
void CGpakConfigDlg::OnCheckG723 ()  {
   CWnd *pObj;         // pointer to Window object

   G723Enabled = IsDlgButtonChecked (IDC_CHECK_G723);
   if (G723Enabled && DspTypeInfo.IsC64xPlus) {
        if ((pObj = GetDlgItem(IDC_CHECK_TI_G7231A)) != NULL) {
            pObj->EnableWindow(1);
        }
        OnCheckTiG7231a ();
   } else {
        if ((pObj = GetDlgItem(IDC_CHECK_TI_G7231A)) != NULL)       
            pObj->EnableWindow(0);
            TI_G7231AEnabled = 0;
   }

}
void CGpakConfigDlg::OnCheckTiG7231a() {
    TI_G7231AEnabled = IsDlgButtonChecked (IDC_CHECK_TI_G7231A);   
	
}
void CGpakConfigDlg::OnCheckG728 ()  {
   G728Enabled = IsDlgButtonChecked (IDC_CHECK_G728);
}
void CGpakConfigDlg::OnCheckG722 ()  {
   G722Enabled = IsDlgButtonChecked (IDC_CHECK_G722);
}
void CGpakConfigDlg::OnCheckG729() {
   CWnd *pObj;         // pointer to Window object

   G729ABEnabled = IsDlgButtonChecked (IDC_CHECK_G729AB);   

   if (G729ABEnabled && DspTypeInfo.IsC64xPlus) {
        if ((pObj = GetDlgItem(IDC_CHECK_TI_G729AB)) != NULL) {
            pObj->EnableWindow(1);
        }
        OnCheckTiG729ab ();
   } else {
        if ((pObj = GetDlgItem(IDC_CHECK_TI_G729AB)) != NULL)       
            pObj->EnableWindow(0);
            TI_G729ABEnabled = 0;
   }


}
void CGpakConfigDlg::OnCheckTiG729ab() {
    TI_G729ABEnabled = IsDlgButtonChecked (IDC_CHECK_TI_G729AB);   
	
}
void CGpakConfigDlg::OnCheckG726LowMem () {

   UpdateG726LowMemSelection ();
}
void CGpakConfigDlg::OnCheckG726LowMips () {

   UpdateG726LowMipsSelection ();
}
void CGpakConfigDlg::UpdateG726LowMemSelection () {
   CWnd *pObj;         // pointer to Window object
   ADT_Bool State;   // enable state

   // Disable G.726 Low MIPS selection if G.726 Low Memory selected.
   G726LowMemEnabled = IsDlgButtonChecked (IDC_CHECK_G726LOWMEM);
   
   State = !G726LowMemEnabled;
   
   if ((pObj = GetDlgItem (IDC_CHECK_G726LOWMIPS)) != NULL)   pObj->EnableWindow(State);
}
void CGpakConfigDlg::UpdateG726LowMipsSelection () {
   CWnd *pObj;         // pointer to Window object
   ADT_Bool State;   // enable state

   // Disable G.726 Low Memory selection if G.726 Low MIPS selected.
   G726LowMipsEnabled = IsDlgButtonChecked(IDC_CHECK_G726LOWMIPS);
   
   State = !G726LowMipsEnabled;
   
   if ((pObj = GetDlgItem (IDC_CHECK_G726LOWMEM)) != NULL) pObj->EnableWindow (State);

   // Enable/Disable G.726 Low MIPS Rates selections.
   if ((pObj = GetDlgItem (IDC_CHECK_G72616K)) != NULL)  pObj->EnableWindow (G726LowMipsEnabled);
   if ((pObj = GetDlgItem (IDC_CHECK_G72624K)) != NULL)  pObj->EnableWindow (G726LowMipsEnabled);
   if ((pObj = GetDlgItem (IDC_CHECK_G72632K)) != NULL)  pObj->EnableWindow (G726LowMipsEnabled);
   if ((pObj = GetDlgItem (IDC_CHECK_G72640K)) != NULL)  pObj->EnableWindow (G726LowMipsEnabled);
}
void CGpakConfigDlg::OnCheckMelp() {
   melpEnabled = IsDlgButtonChecked (IDC_CHECK_MELP);
   
}
void CGpakConfigDlg::OnCheckMelpe() {
   melpEEnabled = IsDlgButtonChecked (IDC_CHECK_MELPE);
}

void CGpakConfigDlg::OnCheckGSMEFR () {
   gsmEFREnabled = IsDlgButtonChecked (IDC_CHECK_GSM_EFR);
   
}
void CGpakConfigDlg::OnCheckAdt4800()  {
   Adt4800Enabled = IsDlgButtonChecked (IDC_CHECK_ADT4800);
}
void CGpakConfigDlg::OnCheckG711() {
   G711Enabled = IsDlgButtonChecked (IDC_CHECK_G711);
}
void CGpakConfigDlg::OnCheckG711Wb() {
   G711WBEnabled = IsDlgButtonChecked (IDC_CHECK_G711_WB);
}
void CGpakConfigDlg::OnCheckL16() {
   L16Enabled = IsDlgButtonChecked (IDC_CHECK_L16);
}
void CGpakConfigDlg::OnCheckL16Wb() {
   L16WBEnabled = IsDlgButtonChecked (IDC_CHECK_L16_WB);
}
void CGpakConfigDlg::OnCheckSpeex() {
   SpeexEnabled = IsDlgButtonChecked (IDC_CHECK_SPEEX);
}
void CGpakConfigDlg::OnCheckSpeexWb() {
   SpeexWBEnabled = IsDlgButtonChecked (IDC_CHECK_SPEEX_WB);
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TDM ports
void CGpakConfigDlg::OnChangeEditP1NumSlots () {

   UpdatePort1SlotsSelection ();
}
void CGpakConfigDlg::OnButtonP1Config () {
   
   // Use the Port Configuration dialog to configure Serial Port 1.
   m_SerialPort.m_PortNum = 1;
   PortConfig = Port1Config;
   if (m_SerialPort.DoModal() == IDOK)   {
   Port1Config = PortConfig;
   }
}
void CGpakConfigDlg::UpdatePort1SlotsSelection () {
   CWnd *pObj;         // pointer to Window object
   ADT_Bool State;   // enable state

   // Enable/Disable the Port 1 Config button based on non zero slots
   // specified for Port 1.
   State = ((GetDlgItemInt(IDC_EDIT_P1NUMSLOTS, NULL, UNSIGNED) != 0) &&
   (GetDlgItemInt(IDC_EDIT_P1SLOTSUSED, NULL, UNSIGNED) != 0));

   if ((pObj = GetDlgItem(IDC_BUTTON_P1CONFIG)) != NULL)   pObj->EnableWindow(State);
}

void CGpakConfigDlg::OnChangeEditP2NumSlots () {

   UpdatePort2SlotsSelection();
}
void CGpakConfigDlg::OnButtonP2Config () {
   
   // Use the Port Configuration dialog to configure Serial Port 2.
   m_SerialPort.m_PortNum = 2;
   PortConfig = Port2Config;
   if (m_SerialPort.DoModal() == IDOK)   {
   Port2Config = PortConfig;
   }
}
void CGpakConfigDlg::UpdatePort2SlotsSelection () {
   CWnd *pObj;         // pointer to Window object
   ADT_Bool State;   // enable state

   // Enable/Disable the Port 2 Config button based on non zero slots
   // specified for Port 2.
   State = ((GetDlgItemInt(IDC_EDIT_P2NUMSLOTS, NULL, UNSIGNED) != 0) &&
   (GetDlgItemInt(IDC_EDIT_P2SLOTSUSED, NULL, UNSIGNED) != 0));

   if ((pObj = GetDlgItem(IDC_BUTTON_P2CONFIG)) != NULL)  pObj->EnableWindow(State);
}

void CGpakConfigDlg::OnChangeEditP3NumSlots () {

   UpdatePort3SlotsSelection();
}
void CGpakConfigDlg::OnButtonP3Config () {
   
   // Use the Port Configuration dialog to configure Serial Port 3.
   m_SerialPort.m_PortNum = 3;
   PortConfig = Port3Config;
   if (m_SerialPort.DoModal() == IDOK)   {
   Port3Config = PortConfig;
   }
}
void CGpakConfigDlg::UpdatePort3SlotsSelection () {
   CWnd *pObj;         // pointer to Window object
   ADT_Bool State;   // enable state

   // Enable/Disable the Port 3 Config button based on non zero slots
   // specified for Port 3.
   State = ((DspTypeInfo.NumSerialPorts >= 3) &&
   (GetDlgItemInt(IDC_EDIT_P3NUMSLOTS, NULL, UNSIGNED) != 0) &&
   (GetDlgItemInt(IDC_EDIT_P3SLOTSUSED, NULL, UNSIGNED) != 0));

   if ((pObj = GetDlgItem(IDC_BUTTON_P3CONFIG)) != NULL)  pObj->EnableWindow(State);
}

ADT_Bool CheckCmpd (int idx, int CmpdMode) {

   ADT_Bool cmpTypeErr;

   if (DspTypeInfo.SwCmpd) return 0;

   cmpTypeErr = (CmpdMode == cmpNone8) || (CmpdMode == cmpL8PcmU) || (CmpdMode == cmpL8PcmA);
   if (!cmpTypeErr) return 0;

   AfxMessageBox ("Invalid companding option on serial port");
   return 1;

}
void SerialPortConfig (sysConfig_t *SysCfg, SerialPortInfo_t *Port, int TotalSlots, int SlotsUsed, int idx) {

   SysCfg->numSlotsOnStream[idx]  = TotalSlots;
   SysCfg->maxSlotsSupported[idx] = SlotsUsed;

   SysCfg->compandingMode[idx] = Port->DataFormat;
   SysCfg->txDataDelay[idx]   = Port->XmtDataDelay;
   SysCfg->rxDataDelay[idx]   = Port->RcvDataDelay;

   if (Port->DataFormat == cmpNone16)   SysCfg->serialWordSize[idx] = 16;
   else                                 SysCfg->serialWordSize[idx] = 8;

   if (Port->XmtSyncHigh)   SysCfg->txFrameSyncPolarity[idx] = 0;
   else                     SysCfg->txFrameSyncPolarity[idx] = 1;

   if (Port->RcvSyncHigh)   SysCfg->rxFrameSyncPolarity[idx] = 0;
   else                     SysCfg->rxFrameSyncPolarity[idx] = 1;

   if (Port->XmtClockRise)  SysCfg->txClockPolarity[idx] = 0;
   else                     SysCfg->txClockPolarity[idx] = 1;

   if (Port->RcvClockRise)   SysCfg->rxClockPolarity[idx] = 1;
   else                      SysCfg->rxClockPolarity[idx] = 0;

   if (Port->DxDelayEnable)   SysCfg->dxDelay[idx] = 1;
   else                       SysCfg->dxDelay[idx] = 0;

   if (Port->TsipEnable)   TSIPEnable[idx] = 1;
   else                    TSIPEnable[idx] = 0;

   
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Conferencing 
void CGpakConfigDlg::OnUpdateEditNumconferences() {
   ADT_Bool ValidInt;      // flag indicating valid integer

   // Verify VAD and AGC are selected if the entered Number of Conferences is
   // not zero.
   if ((GetDlgItemInt(IDC_EDIT_NUMCONFERENCES, &ValidInt, UNSIGNED) != 0) &&
   ValidInt && (!VADEnabled || AgcInstances == 0))
   AfxMessageBox("VAD and AGC are required for conferencing!");

   UpdateConferenceSelection ();
}
void CGpakConfigDlg::UpdateConferenceSelection () {
   CWnd *pObj;         // pointer to Window object
   ADT_Bool State;   // enable state
   ADT_Bool ValidInt;      // flag indicating valid integer

   // Enable / Disable VAD parameter fields.
   State =    GetDlgItemInt(IDC_EDIT_NUMCONFERENCES, &ValidInt, UNSIGNED) != 0;

   if ((pObj = GetDlgItem (IDC_EDIT_CUSTOM_CONFERENCE_CNT)) != NULL)   pObj->EnableWindow(State);
   if ((pObj = GetDlgItem (IDC_EDIT_CHANSPERCONF)) != NULL)   pObj->EnableWindow(State);
   if ((pObj = GetDlgItem (IDC_EDIT_NUMDOMINANT)) != NULL)   pObj->EnableWindow(State);
   if ((pObj = GetDlgItem (IDC_EDIT_MAXNOISESUP)) != NULL)   pObj->EnableWindow(State);
   
}

//----------------------------------------------------
//  Noise Suppression
void CGpakConfigDlg::OnCheckNoiseSuppressionEnable() {
   UpdateNoiseSuppressionSelection();
}
void CGpakConfigDlg::UpdateNoiseSuppressionSelection () {
   CWnd *pObj;         // pointer to Window object
   ADT_Bool State;   // enable state

   // Enable / Disable VAD parameter fields.
   State = (IsDlgButtonChecked (FLG_NoiseSuppressionEnabled));

   if ((pObj = GetDlgItem (FLD_SuppressMaxAttenuation)) != NULL)   pObj->EnableWindow(State);
   if ((pObj = GetDlgItem (FLD_SuppressVadLo)) != NULL)      pObj->EnableWindow(State);
   if ((pObj = GetDlgItem (FLD_SuppressVadHi)) != NULL)         pObj->EnableWindow(State);
   if ((pObj = GetDlgItem (FLD_SuppressVadHang)) != NULL)   pObj->EnableWindow(State);
   if ((pObj = GetDlgItem (FLD_SuppressVadWindow)) != NULL)   pObj->EnableWindow(State);
   
   NoiseSuppressionEnabled = State;
}



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Functions to build G.PAK Configuration files based on selected parameters.
//
//  1) Moves local parameters into sysconfig and ecconfig structures.
//  2) Calls routine to create G.PAK configuration files
ADT_Bool CGpakConfigDlg::ParmFound (char *pString, char *pToken, int *pValue) {
   size_t Length;  // length of token
   char *pChar;   // pointer to character in sring

   Length = strlen(pToken);
   if (strncmp(pString, pToken, Length) != 0)
   return (FALSE);

   pChar = &(pString[Length]);
   while (*pChar == ' ')   pChar++;
   if (*pChar != '=')   return (FALSE);

   pChar++;
   while (*pChar == ' ')   pChar++;
   if (*pChar == '\0')   return (FALSE);

   if (sscanf(pChar, "%d", pValue) == 0)  return (FALSE);
   return (TRUE);
}
ADT_Bool CGpakConfigDlg::StringFound (char *pString, char *pToken, char **pStr, size_t *Len) {
   size_t Length;  // length of token
   char *pChar;   // pointer to character in sring

   Length = strlen(pToken);
   if (strncmp(pString, pToken, Length) != 0)
   return (FALSE);

   pChar = &(pString[Length]);
   while (*pChar == ' ')   pChar++;
   if (*pChar != '=')   return (FALSE);

   pChar++;
   while (*pChar == ' ')   pChar++;
   if (*pChar == '\0')   return (FALSE);

   *Len = strlen(pChar);
   if (*Len == 0) return(FALSE);

   // remove carriage return and linefeed from string
   if ((pChar[*Len-1] == 0x0A) || (pChar[*Len-1] == 0x0D))  *Len -= 1;
   if ((pChar[*Len-1] == 0x0A) || (pChar[*Len-1] == 0x0D))  *Len -= 1;

   *pStr = pChar;
   return (TRUE);
}
void CGpakConfigDlg::WriteConfigFile() {

   FILE *CfgFile;   // Config Parms file pointer
   GpakVarDef* var;
   int ParmValue;

   // Open the Config Parms file.
   if ((CfgFile = fopen(CfgFileName, "w")) == NULL) {
      AfxMessageBox("Failed to open Config Parms file!");
   return;
   }

   fprintf (CfgFile, "StubPath = %s\n", StubPath);
   fprintf (CfgFile, "CompPath = %s\n", CompPath);
   fprintf (CfgFile, "GpakPath = %s\n", GpakPath);

   var = gpakVars;
   while (var->name != NULL) {
      
      switch (var->varI8) {
      case 2: ParmValue = * ((ADT_Int16 *) var->pointer); break;
      case 4: ParmValue = * ((ADT_Int32 *) var->pointer); break;
      }
      fprintf (CfgFile, "%s = %d\n", var->name, ParmValue);
      var++;
   }

   // Close the Config Parms file.
   if (fclose(CfgFile))
      AfxMessageBox ("Failed to close Config Parms file!");
}
void CGpakConfigDlg::ReadConfigFile() {
   FILE *CfgFile;   // Config Parms file pointer
   char LineBufr[201];   // line buffer
   int ParmValue;   // parameter value
   char* pString;
   size_t len;
   GpakVarDef* var;

   // Attempt to open the Config Parms file.
   if ((CfgFile = fopen(CfgFileName, "r")) == NULL)  return;

   GpakPath[0] = 0;
   CompPath[0] = 0;

   // Parse and extract any Config Parm values.
   while (fgets(LineBufr, 100, CfgFile) != NULL)  {
   
      // Path names
      if (StringFound(LineBufr, "StubPath", &pString, &len)) {
         memcpy(StubPath, pString, len);
         continue;
      }
      if (StringFound(LineBufr, "CompPath", &pString, &len)) {
        memcpy(CompPath, pString, len);
        continue;
      }
      if (StringFound(LineBufr, "GpakPath", &pString, &len)) {
        memcpy(GpakPath, pString, len);
        continue;
      }

      // Search through gpakVars for matching name.
      var = gpakVars;
      while (var->name != NULL) {
         if (!ParmFound (LineBufr, var->name, &ParmValue)) {
            var++;
            continue;
         }
         // Name matches, store value in variable
         switch (var->varI8) {
         case 2: * ((ADT_Int16 *) var->pointer) = ParmValue; break;
         case 4: * ((ADT_Int32 *) var->pointer) = ParmValue; break;
         }
         break;
      }

   }

   PcmEcanConfig.AdaptEnable = TRUE;
   PktEcanConfig.AdaptEnable = TRUE;

   UpdateDspSelection (DspType);

   // Close the Config Parms file.
   fclose (CfgFile);
   return;
}
ADT_Bool CGpakConfigDlg::BuildGpakConfigFiles (char **pErrText) {
   sysConfig_t SysCfg;   // G.PAK System configuration info
   ecConfig_t EcCfg;   // G.PAK Echo Canceller configuration info
   blockStatus_t MemBlk;   // G.PAK Memory Block configuration info
   int i;   // loop index / counter
   int jbFactor;
   unsigned int CfgFrameChanFlags;   // configured frame and channel type flags
   int YesNo;            // Yes / No response status
   int NumToneTypes;      // number of tone types selected
   ADT_Bool PortsUnused;

   // Default the error text to unknown.
   *pErrText = "?";

   // Establish the content of the G.PAK System configuration structure.


   // Determine the Enable flag bits.
   //-----------------------------------------
   // Set codecs bits
   SysCfg.Codecs.LongWord = 0;
   if (G726LowMemEnabled)
      cdcBits.g726LowMemEnable = 1;
   else if (G726LowMipsEnabled) {
      if (G726Rate16Enabled)   {
         cdcBits.g726Rate16Enable = 1;
         cdcBits.g726LowMipsEnable = 1;
      }
      if (G726Rate24Enabled)   {
         cdcBits.g726Rate24Enable = 1;
         cdcBits.g726LowMipsEnable = 1;
      }
      if (G726Rate32Enabled)   {
         cdcBits.g726Rate32Enable = 1;
         cdcBits.g726LowMipsEnable = 1;
      }
      if (G726Rate40Enabled)   {
         cdcBits.g726Rate40Enable = 1;
         cdcBits.g726LowMipsEnable = 1;
      }
   }
   if (G723Enabled) {
	   if (TI_G7231AEnabled)
	      cdcBits.TI_g7231AEnable = 1;
	   else
	      cdcBits.g723Enable = 1;
   }
   if (G728Enabled)         cdcBits.g728Enable = 1;
   if (Adt4800Enabled)      cdcBits.adt4800Enable = 1;
   
   // NOTE: Temporary until new release of AMR is available.
   if (gsmEFREnabled)       cdcBits.gsmEFREnable = 1;
   //if (gsmEFREnabled)       
   //   AfxMessageBox ("AMR temporarily unavailalbe until handling of scratch is corrected.");

   if (G729ABEnabled) {
       if (TI_G729ABEnabled)
            cdcBits.TI_g729ABEnable = 1;
       else
            cdcBits.g729ABEnable = 1;
   }
   if (melpEnabled)         cdcBits.melpEnable = 1;
   if (melpEEnabled)        cdcBits.melpEEnable = 1;
   if ((SpeexEnabled) ||
       (SpeexWBEnabled))    cdcBits.speexEnable = 1;
   if (G722Enabled)         cdcBits.g722Enable = 1;

   WBCodecEnabled = G711WBEnabled | L16WBEnabled | SpeexWBEnabled | G722Enabled;
   if (WBCodecEnabled)  SrcInstances = NumChannels;

   if (SRTP_Enable)     SRTPInstances = NumChannels;


   //-----------------------------------------
   // Set algorithm bits
   SysCfg.enab.LongWord = 0;
   if (NumPcmEcans != 0)    algBits.g168PcmEnable = 1;
   if (NumPktEcans != 0)    algBits.g168PktEnable = 1;
   if (EcShortTailEnabled)  algBits.g168VarAEnable = 1;

    if (HostIF & RMII_IF) algBits.networkStack = 1;
    else                  algBits.networkStack = 0;
 
 
   if (JBMaxMS)  {
      if (HostIF & RMII_IF) {
         algBits.rtpHostDelivery = 0;
         algBits.rtpNetDelivery  = 1; 
      } else {
         algBits.rtpHostDelivery = 1; 
         algBits.rtpNetDelivery  = 0; 
      }
   }

   if (VADEnabled)         algBits.vadCngEnable = 1;
   else                    VadReportEvents = FALSE;

   if (VadReportEvents)    SysCfg.VadReport = Enabled;
   else SysCfg.VadReport = Disabled;

   if (ToneGenInstances != 0) algBits.toneGenEnable = 1;

   if (DtmfDialEnabled)     algBits.dtmfDialEnable = 1;
   else                     algBits.dtmfDialEnable = 0;
   
   if (ToneRlyGenEnabled)        algBits.toneRlyGenEnable = 1;
   if (RtpToneType != NoTonePkt) algBits.toneRlyDetEnable = 1;
   if (ToneDetectBEnabled && MaxConcurrentTones)  algBits.toneDetEnable_B = 1;

   // Determine selected tone detect types and max concurrent.
   NumToneTypes = 0;
   if (DtmfEnabled)      { algBits.dtmfEnable = 1;     NumToneTypes++;  }
   if (Mfr1Enabled)      { algBits.mfr1Enable = 1;     NumToneTypes++;  }
   if (Mfr2FwdEnabled)   { algBits.mfr2FwdEnable = 1;  NumToneTypes++;  }
   if (Mfr2RevEnabled)   { algBits.mfr2BackEnable = 1; NumToneTypes++;  }
   if (CallProgEnabled)  { algBits.cprgEnable = 1;     NumToneTypes++;  }


   if (NumToneTypes == 0)                      SysCfg.maxToneDetTypes = 0;
   else if (MaxConcurrentTones > NumToneTypes) SysCfg.maxToneDetTypes = NumToneTypes;
   else                                        SysCfg.maxToneDetTypes = MaxConcurrentTones;

   algBits.wbEnable = WBCodecEnabled | TDM_16kHz;

   //-------------------------------------------------------------
   // Determine Port related parameters.
   SerialPortConfig (&SysCfg, &Port1Config, Port1NumSlots, Port1SlotsUsed, 0);
   SerialPortConfig (&SysCfg, &Port2Config, Port2NumSlots, Port2SlotsUsed, 1);
   SerialPortConfig (&SysCfg, &Port3Config, Port3NumSlots, Port3SlotsUsed, 2);

   // Determine other parameters.
   SysCfg.DspType   = DspTypeInfo.DspType;
   SysCfg.DspMips   = DspSpeed;
   SysCfg.hpiBlockStartAddress = (void *) HostInterfaceAddress;
   SysCfg.packetProfile = PktProfile;
   SysCfg.rtpPktType = RtpToneType;
   SysCfg.maxJitterms = JBMaxMS;
   SysCfg.rtpMaxBuffsPerChan = RtpMaxBuffsPerChan;

   SysCfg.HostIF = PCI_IF | HostIF;
   SysCfg.SRTP_Enable = SRTP_Enable;

   if (NumAECEcans) {
      SysCfg.micEqLength = MicEqLength;
      SysCfg.spkrEqLength = MicEqLength;
      AfxMessageBox ("The GpakProj.stackSize field of the .tcf must be"
                     " increased by 0x1000 when AEC is selected");
   } else {
      SysCfg.micEqLength = 0;
      SysCfg.spkrEqLength = 0;
   }

   // Determine the configured Frame Sizes and Channel Types.
   CfgFrameChanFlags = 0;
   if (FrameMS_1)    CfgFrameChanFlags |= FRAME_8_CFG_BIT;
   if (FrameMS_2_5)  CfgFrameChanFlags |= FRAME_20_CFG_BIT;
   if (FrameMS_5)    CfgFrameChanFlags |= FRAME_40_CFG_BIT;
   if (FrameMS_10)   CfgFrameChanFlags |= FRAME_80_CFG_BIT;
   if (FrameMS_20)   CfgFrameChanFlags |= FRAME_160_CFG_BIT;
#ifdef MELP
   if (FrameMS_22_5) CfgFrameChanFlags |= FRAME_180_CFG_BIT;
#endif
   if (FrameMS_30)   CfgFrameChanFlags |= FRAME_240_CFG_BIT;

   // Verify a PCM To Packet type channel is wanted.
   PortsUnused = (Port1SlotsUsed == 0) && (Port2SlotsUsed == 0) &&   (Port3SlotsUsed == 0);

   
   //--------------------------------------------------------------------
   //  Check for inconsistencies
   if (Pcm2PktChan)   {
      if (PortsUnused) YesNo = WarnMsg ("serial ports", "PCM To Packet");
      else           YesNo = IDYES;

      if (YesNo == IDYES)   CfgFrameChanFlags |= PCM2PKT_CFG_BIT;
   }

    // Verify a PCM To PCM type channel is wanted.
   if (Pcm2PcmChan)   {
      if (PortsUnused)  YesNo = WarnMsg ("serial ports", "PCM To PCM");
      else                YesNo = IDYES;

      if (YesNo == IDYES) CfgFrameChanFlags |= PCM2PCM_CFG_BIT;
   }

   if (Pkt2PktChan)
      CfgFrameChanFlags |= PKT2PKT_CFG_BIT;

   // Verify a Circuit Data type channel is wanted.
   if (CircDataChan)   {
      if (PortsUnused)   YesNo = WarnMsg ("serial ports", "Circuit Data");
      else         YesNo = IDYES;

      if (YesNo == IDYES)   CfgFrameChanFlags |= CIRCDATA_CFG_BIT;
   }

   // Verify a Conference PCM type channel is wanted.
   if (ConfPcmChan)   {
      YesNo = IDYES;
      if (NumGpakConferences == 0) YesNo = WarnMsg ("conferences", "Conference PCM");
      if ((YesNo == IDYES) && PortsUnused) YesNo = WarnMsg ("serial ports", "Conference PCM");

      if (YesNo == IDYES)   CfgFrameChanFlags |= CONFPCM_CFG_BIT;
   }

   // Verify a Conference Packet type channel is wanted.
   if (ConfPktChan)   {
      if (NumGpakConferences == 0) YesNo = WarnMsg ("conferences", "Conference Packet");
      else                         YesNo = IDYES;

      if (YesNo == IDYES)      CfgFrameChanFlags |= CONFPKT_CFG_BIT;
   }

   // Verify a Conference Composite type channel is wanted.
   if (ConfCompChan)   {
      if (NumGpakConferences == 0) YesNo = WarnMsg ("conferences", "Conference Composite");
      else                          YesNo = IDYES;

      if (YesNo == IDYES)         CfgFrameChanFlags |= CONFCOMP_CFG_BIT;
   }

   SysCfg.cfgFramesChans = CfgFrameChanFlags;

   SysCfg.numAGCChans   = AgcInstances;
   SysCfg.agcTargetPower = AgcTargetPowerA;
   SysCfg.agcMinGain   = AgcLossLimitA;
   SysCfg.agcMaxGain   = AgcGainLimitA;
   SysCfg.agcLowSigThreshDBM =  AgcLowSigThreshA;

   SysCfg.agcTargetPowerB = AgcTargetPowerB;
   SysCfg.agcMinGainB   = AgcLossLimitB;
   SysCfg.agcMaxGainB   = AgcGainLimitB;
   SysCfg.agcLowSigThreshDBMB =  AgcLowSigThreshB;

   SysCfg.vadNoiseFloorZThresh = VadNoiseFloor;
   SysCfg.vadHangOverTime   = VadHangTime;
   SysCfg.vadWindowMs       = VadWindowMs;

   SysCfg.maxNumChannels = NumChannels;

   SysCfg.maxNumConferences = NumConferences;
   SysCfg.maxNumConfChans   = ChansPerConference;
   SysCfg.numConfDominant   = NumConfDominant;
   SysCfg.maxConfNoiseSuppress = MaxConfNoiseSuppress;

   if (NumT38Instances != 0) SysCfg.numFaxChans = NumT38Instances;
   else                      SysCfg.numFaxChans = NumTIT38Instances;
   SysCfg.numTGChans =  ToneGenInstances;
   SysCfg.numSrcChans =  SrcInstances;

   SysCfg.numPcmEcans = NumPcmEcans;
   SysCfg.numPktEcans = NumPktEcans;
   SysCfg.AECInstances = NumAECEcans;
   SysCfg.AECTailLen   = AECTailLen;

   if (TDM_16kHz) {
      SysCfg.samplesPerMs = 16;
   } else {
      SysCfg.samplesPerMs = 8;
   }


   jbFactor = 1;
   if (L16Enabled || WBCodecEnabled) jbFactor = 2;
   if (L16WBEnabled)   jbFactor = 4;

   SysCfg.maxNumLbCoders = LbCoderInstances;

   if      (FrameMS_5)   SysCfg.smallJBBlockI32  = (40 * jbFactor + JB_HEAD_ROOM) / 4;
   else if (FrameMS_2_5) SysCfg.smallJBBlockI32  = (20 * jbFactor + JB_HEAD_ROOM) / 4;
   else if (FrameMS_1)   SysCfg.smallJBBlockI32  = (8  * jbFactor + JB_HEAD_ROOM) / 4;
   else                  SysCfg.smallJBBlockI32  = 0;

   if      (FrameMS_30) SysCfg.largeJBBlockI32   = (240 * jbFactor + JB_HEAD_ROOM) / 4;
#ifdef MELP
   else if (FrameMS_22_5) SysCfg.largeJBBlockI32 = (180 * jbFactor + JB_HEAD_ROOM) / 4;
#endif
   else if (FrameMS_20) SysCfg.largeJBBlockI32   = (160 * jbFactor + JB_HEAD_ROOM) / 4;
   else if (FrameMS_10) SysCfg.largeJBBlockI32   = (80  * jbFactor + JB_HEAD_ROOM) / 4;
   else                 SysCfg.largeJBBlockI32   = 0;

   if (MaxCustomFrameRateMs > 30) SysCfg.hugeJBBlockI32  = (8 * MaxCustomFrameRateMs * jbFactor + JB_HEAD_ROOM) / 4;
   else                           SysCfg.hugeJBBlockI32  = 0;

   if (HostIF & RMII_IF) {
      // make sure huge JB is big enough for network socket allocation
      if (SysCfg.hugeJBBlockI32 < JB_NET_SOCKET_HUGE_I32)
            SysCfg.hugeJBBlockI32 = JB_NET_SOCKET_HUGE_I32;
      SysCfg.smallJBBlockI32 += FAST_RTP_HDR_I32;
      SysCfg.largeJBBlockI32 += FAST_RTP_HDR_I32;
      SysCfg.hugeJBBlockI32  += FAST_RTP_HDR_I32;
   }

//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------
    SysCfg.numTxCidChans = NumTxCidInstances;
    SysCfg.numRxCidChans = NumRxCidInstances;

	SysCfg.type2CID             = type2CID;
    SysCfg.numSeizureBytes      = CIDConfigLcl.numSeizureBytes;  
    SysCfg.numMarkBytes         = CIDConfigLcl.numMarkBytes;     
    SysCfg.numPostambleBytes    = CIDConfigLcl.numPostambleBytes;
    SysCfg.fskType              = (GpakFskType) CIDConfigLcl.fskType;          
    SysCfg.fskLevel             = CIDConfigLcl.fskLevel;
    SysCfg.msgType              = CIDConfigLcl.msgType;          
    if ((NumTxCidInstances + NumRxCidInstances) == 0) SysCfg.fskType = fskNone;
	if (SysCfg.type2CID) {
       algBits.toneGenEnable = 1; // Type 2, TAS(CID TX) and ACK(RX CID) tone generator
	   //ToneGenInstances += (NumTxCidInstances + NumRxCidInstances);
	   //SysCfg.numTGChans = ToneGenInstances +(NumTxCidInstances + NumRxCidInstances);
	   algBits.tasDetEnable = 1;  // Type 2 CID RX needs TAS tone detector
	   if (!DtmfEnabled)      { 
          algBits.dtmfEnable = 1; 
	   } // Type 2 CID TX needs ACK tone detector
	   TxCidDTMFIntances = 0; //NumTxCidInstances; //0 ,moved in CID super channnel
	} else {
	   TxCidDTMFIntances = 0;
	}
 //-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------
   
   // Determine Echo Canceller parameters.

   // PCM Echo canceller
   EcCfg.g168Pcm.TapLength = PcmEcanConfig.TapLength;
   EcCfg.g168Pcm.NLPType   = PcmEcanConfig.NlpType;
   EcCfg.g168Pcm.AdaptEnable = 1;

   EcCfg.g168Pcm.BypassEnable = 0;
   if (PcmEcanConfig.G165DetectEnable)  EcCfg.g168Pcm.G165DetectEnable = 1;
   else                                 EcCfg.g168Pcm.G165DetectEnable = 0;

   EcCfg.g168Pcm.DoubleTalkThres     = PcmEcanConfig.DblTalkThreshold;
   EcCfg.g168Pcm.MaxDoubleTalkThres  = PcmEcanConfig.MaxDblTalkThreshold;
   EcCfg.g168Pcm.NLPThres            = PcmEcanConfig.NlpThrehold;
   EcCfg.g168Pcm.NlpUpperThresConv   = PcmEcanConfig.NlpUpperThresConv;
   EcCfg.g168Pcm.NlpUpperThresUnConv = PcmEcanConfig.NlpUpperThresUnConv;
   EcCfg.g168Pcm.NlpMaxSupp          = PcmEcanConfig.NlpMaxSupp;
   EcCfg.g168Pcm.CNGNoiseThreshold   = PcmEcanConfig.CngThreshold;
   EcCfg.g168Pcm.AdaptLimit          = PcmEcanConfig.AdaptLimit;
   EcCfg.g168Pcm.CrossCorrLimit      = PcmEcanConfig.CrossCorrLimit;
   EcCfg.g168Pcm.NFIRSegments        = PcmEcanConfig.NumFirSegments;
   EcCfg.g168Pcm.FIRSegmentLength    = PcmEcanConfig.FirSegmentLength;
   EcCfg.g168Pcm.FirTapCheckPeriod   = PcmEcanConfig.FirTapCheckPeriod;
   EcCfg.g168Pcm.TandemOperation     = PcmEcanConfig.TandemOperationEnable;
   EcCfg.g168Pcm.MixedFourWireMode   = !PcmEcanConfig.EchoSourceAttached;
   if (PcmEcanConfig.ReconvergenceCheck)
      EcCfg.g168Pcm.ReconvergenceChk  = 2;
   else
      EcCfg.g168Pcm.ReconvergenceChk  = 0;


   //  Packet Echo Canceller
   EcCfg.g168Pkt.TapLength   = PktEcanConfig.TapLength;
   EcCfg.g168Pkt.NLPType   = PktEcanConfig.NlpType;
   EcCfg.g168Pkt.AdaptEnable = 1;

   EcCfg.g168Pkt.BypassEnable = 0;
   if (PktEcanConfig.G165DetectEnable)  EcCfg.g168Pkt.G165DetectEnable = 1;
   else                                 EcCfg.g168Pkt.G165DetectEnable = 0;

   EcCfg.g168Pkt.DoubleTalkThres     = PktEcanConfig.DblTalkThreshold;
   EcCfg.g168Pkt.MaxDoubleTalkThres  = PktEcanConfig.MaxDblTalkThreshold;
   EcCfg.g168Pkt.NLPThres            = PktEcanConfig.NlpThrehold;
   EcCfg.g168Pkt.NlpUpperThresConv   = PktEcanConfig.NlpUpperThresConv;
   EcCfg.g168Pkt.NlpUpperThresUnConv = PktEcanConfig.NlpUpperThresUnConv;
   EcCfg.g168Pkt.NlpMaxSupp          = PktEcanConfig.NlpMaxSupp;
   EcCfg.g168Pkt.CNGNoiseThreshold   = PktEcanConfig.CngThreshold;
   EcCfg.g168Pkt.AdaptLimit          = PktEcanConfig.AdaptLimit;
   EcCfg.g168Pkt.CrossCorrLimit      = PktEcanConfig.CrossCorrLimit;
   EcCfg.g168Pkt.NFIRSegments        = PktEcanConfig.NumFirSegments;
   EcCfg.g168Pkt.FIRSegmentLength    = PktEcanConfig.FirSegmentLength;
   EcCfg.g168Pkt.FirTapCheckPeriod   = PktEcanConfig.FirTapCheckPeriod;

   EcCfg.g168Pkt.TandemOperation     = PktEcanConfig.TandemOperationEnable;
   EcCfg.g168Pkt.MixedFourWireMode   = !PktEcanConfig.EchoSourceAttached;
   if (PktEcanConfig.ReconvergenceCheck)
      EcCfg.g168Pkt.ReconvergenceChk  = 2;
   else
      EcCfg.g168Pkt.ReconvergenceChk  = 0;

   
   SysCfg.maxG168PcmTapLength        = EcCfg.g168Pcm.TapLength;
   SysCfg.maxG168PcmNFIRSegments     = EcCfg.g168Pcm.NFIRSegments;
   SysCfg.maxG168PcmFIRSegmentLength = EcCfg.g168Pcm.FIRSegmentLength;

   SysCfg.maxG168PktTapLength        = EcCfg.g168Pkt.TapLength;
   SysCfg.maxG168PktNFIRSegments     = EcCfg.g168Pkt.NFIRSegments;
   SysCfg.maxG168PktFIRSegmentLength = EcCfg.g168Pkt.FIRSegmentLength;

   // Noise Suppression
   if (NoiseSuppressionEnabled)  algBits.noiseSuppressEnable = 1;
   else                          algBits.noiseSuppressEnable = 0;
   NoiseSuppression = NseSpr;

   // Determine DMA buffer utilization.
   for (i = 0; i < MAX_NUM_SERIAL_PORTS; i++)   {
   if (SysCfg.maxSlotsSupported[i] != 0)   {
      SysCfg.dmaBufferSize[i] =  SysCfg.maxSlotsSupported[i] * SysCfg.samplesPerMs * 2;  // time 2 for ping/pong
   }  else
      SysCfg.dmaBufferSize[i] = 1;
   }

   // Determine the channel data block size.
   computeBlockSize (&SysCfg, &MemBlk, &EcCfg, &DspTypeInfo);

   // For now, cause both Tone Relay Generate and Tone Relay Detect to be
   // included if either one is selected by the user. There are currently
   // library dependencies that do not allow seperate inclusion.
   if ((algBits.toneRlyDetEnable) ||   (algBits.toneRlyGenEnable)) {
      algBits.toneRlyDetEnable = 1;
      algBits.toneRlyGenEnable = 1;
   }

   // Identify echo canceller library
   shortTailEC = longTailEC = 0;
   if (algBits.g168PktEnable) {
      longTailEC = 1;
      algBits.g168VarAEnable = 0;
   } else if (algBits.g168PcmEnable) {
      if (algBits.g168VarAEnable)
         shortTailEC = 1;
      else 
         longTailEC = 1;
   } 


   // Determine available libraries (production, demo or stub)
   LibErrors = IdentifyActiveFiles (&SysCfg);
   if (LibErrors != NULL) {
      CAboutDlg displayBox;

      displayBox.DoModal ();
   }

   if (!ToneRlyGenEnabled)       algBits.toneRlyGenEnable = 0;
   if (RtpToneType == NoTonePkt) algBits.toneRlyDetEnable = 0;
 

   // Create the Configuration source files.
   return createDspSrcFiles (&SysCfg, &MemBlk, NumChannels, &EcCfg, pErrText, 
                             &ExtMemConfig, RootFileName, &DspTypeInfo);
}


void CGpakConfigDlg::GetProjectInfo () {

   char *loc;
   CWnd *pObj;         // pointer to Window object

   CFileDialog CurrentDirectory (TRUE, NULL, "",
   OFN_ENABLESIZING | OFN_NONETWORKBUTTON | OFN_HIDEREADONLY |
   OFN_PATHMUSTEXIST, "G.PAK tcf or pjt|*.tcf;*.pjt|G.PAK tcf file|*.tcf|G.PAK pjt file|*.pjt||", this); //);


   CurrentDirectory.m_ofn.lpstrInitialDir = ".";
   sprintf (Title, "ADT G.PAK V%x_%x Project Selection", VERS>>8, VERS & 0xFF);
   CurrentDirectory.m_ofn.lpstrTitle = Title;

   if (CurrentDirectory.DoModal () != IDOK) {     
      EndDialog (IDABORT);      
      return ;       
   }     

   sprintf (Title, "ADT G.PAK V%x_%x Configuration:  %s", VERS>>8, VERS & 0xFF, CurrentDirectory.m_ofn.lpstrFile);
   SetWindowText (Title);

   strncpy (RootFileName, CurrentDirectory.m_ofn.lpstrFile, sizeof (RootFileName));   
   loc = strrchr (RootFileName, '.');     
   if (loc != NULL) *loc = 0;      

   sprintf (CfgFileName, "%s.cfg", RootFileName);    
   strncpy (RootPath, RootFileName, sizeof (RootPath));     
   loc = strrchr (RootPath, '\\');     
   if (loc != NULL) *(loc + 1) = 0;      

   // Attempt to read all selectable parameter values from an existing Config
   // Parms file.
   ReadConfigFile();

   InitDSPComboBox (this, DspType);
   UpdateDspSelection (DspType);

   SetDlgItemInt (FldDspSpeed,     DspSpeed, UNSIGNED);

   // Initialize the Frame Size selection display.
   CheckDlgButton (IDC_CHECK_FRAME8,  FrameMS_1);
   CheckDlgButton (IDC_CHECK_FRAME20,  FrameMS_2_5);
   CheckDlgButton (IDC_CHECK_FRAME40,  FrameMS_5);
   CheckDlgButton (IDC_CHECK_FRAME80,  FrameMS_10);
   CheckDlgButton (IDC_CHECK_FRAME160, FrameMS_20);
#ifdef MELP
   CheckDlgButton (IDC_CHECK_FRAME180, FrameMS_22_5);
#else
   CheckDlgButton (IDC_CHECK_FRAME180, 0);
#endif

   CheckDlgButton (IDC_CHECK_FRAME240, FrameMS_30);

   CheckDlgButton (IDC_CHECK_HINT8,   Hint8);
   CheckDlgButton (IDC_CHECK_HINT20,  Hint20);
   CheckDlgButton (IDC_CHECK_HINT40,  Hint40);
   CheckDlgButton (IDC_CHECK_HINT80,  Hint80);
   CheckDlgButton (IDC_CHECK_HINT160, Hint160);
#ifdef MELP
   CheckDlgButton (IDC_CHECK_HINT180, Hint180);
#else
   CheckDlgButton (IDC_CHECK_HINT180, 0);
#endif
   CheckDlgButton (IDC_CHECK_HINT240, Hint240);
 
   UpdateMaxFrameSizeSelection();
   SetDlgItemInt (FldJBMaxMS, JBMaxMS, UNSIGNED);
   SetDlgItemInt (FldJBNBuffs, RtpMaxBuffsPerChan, UNSIGNED);
   SetDlgItemInt (FldMicEqLen, MicEqLength, UNSIGNED);
   SetDlgItemInt (FldSpkrEqLen, SpkrEqLength, UNSIGNED);

   CheckDlgButton (DSPStackCheckBox, HostIF & RMII_IF);
   CheckDlgButton (SRTP_Enabled, SRTP_Enable);

   // Initialize the Channel Type selection display.
   CheckDlgButton (IDC_CHECK_PCM2PKT,  Pcm2PktChan);
   CheckDlgButton (IDC_CHECK_PCM2PCM,  Pcm2PcmChan);
   CheckDlgButton (IDC_CHECK_PKT2PKT,  Pkt2PktChan);
   CheckDlgButton (IDC_CHECK_CIRCDATA, CircDataChan);
   CheckDlgButton (IDC_CHECK_CONFPCM,  ConfPcmChan);
   CheckDlgButton (IDC_CHECK_CONFPKT,  ConfPktChan);
   CheckDlgButton (IDC_CHECK_CONFCOMP, ConfCompChan);

   // Initialize the Packet Profile display.
   CheckDlgButton (IDC_RADIO_AAL2PROFILE, PktProfile == AAL2Trunking);
   CheckDlgButton (IDC_RADIO_RTPPROFILE,  PktProfile == RTPAVP);
   UpdatePktTypeSelection ();

   SetDlgItemInt (FldAGCInstances,     AgcInstances , UNSIGNED);
   
   
   // Initialize the AGC display.
   SetDlgItemInt (FldAGCInstances,     AgcInstances , UNSIGNED);
   SetDlgItemInt (FldAgcTargetPowerA,  AgcTargetPowerA, SIGNED);
   SetDlgItemInt (FldAgcLossLimitA,    AgcLossLimitA, SIGNED);
   SetDlgItemInt (FldAgcGainLimitA,    AgcGainLimitA, SIGNED);
   SetDlgItemInt (FldAgcLowSigThreshA, AgcLowSigThreshA, SIGNED);

   SetDlgItemInt (FldAgcTargetPowerB,  AgcTargetPowerB, SIGNED);
   SetDlgItemInt (FldAgcLossLimitB,    AgcLossLimitB, SIGNED);
   SetDlgItemInt (FldAgcGainLimitB,    AgcGainLimitB, SIGNED);
   SetDlgItemInt (FldAgcLowSigThreshB, AgcLowSigThreshB, SIGNED);
   
   UpdateAgcSelection ();

   // Initialize the Short Tail Echo Canceller Enabled display.
   CheckDlgButton (IDC_CHECK_ECSHORTTAIL, EcShortTailEnabled);
   CheckDlgButton (IDC_CHECK_16KHZ_TDM,   TDM_16kHz);

   // Initialize the Number of PCM Echo Cancellers display.
   SetDlgItemInt (IDC_EDIT_PCMECNUM, NumPcmEcans, UNSIGNED);
   UpdatePcmEcSelection ();

   // Initialize the Packet Echo Cancel Enabled display.
   SetDlgItemInt (IDC_EDIT_PKTECNUM, NumPktEcans, UNSIGNED);
   UpdatePcktEcSelection ();

   // Initialize the Packet Echo Cancel Enabled display.
   SetDlgItemInt (FldNumAEC, NumAECEcans, UNSIGNED);
   UpdateAECSelection ();

   // Initialize the VAD display.
   CheckDlgButton (IDC_CHECK_VADENABLE, VADEnabled);

   if (VADEnabled) {
        if ((pObj = GetDlgItem(IDC_CHECK_VADREPORT)) != NULL) {
            pObj->EnableWindow(1);
            CheckDlgButton (IDC_CHECK_VADREPORT,  VadReportEvents);
        }
   } else {
        CheckDlgButton (IDC_CHECK_VADREPORT,  VadReportEvents);
        if ((pObj = GetDlgItem(IDC_CHECK_VADREPORT)) != NULL)       
            pObj->EnableWindow(0);
   }

   SetDlgItemInt (IDC_EDIT_VADNOISEFLOOR, VadNoiseFloor, SIGNED);
   SetDlgItemInt (IDC_EDIT_VADHANGTIME,   VadHangTime, UNSIGNED);
   SetDlgItemInt (IDC_EDIT_VADWINSIZE,    VadWindowMs, UNSIGNED);
   UpdateVadSelection ();

   // Initialize the Tone Type Enables display.
   CheckDlgButton (IDC_CHECK_DTMFENABLE,   DtmfEnabled);
   CheckDlgButton (IDC_CHECK_MFR1ENABLE,   Mfr1Enabled);
   CheckDlgButton (IDC_CHECK_MFR2FWDENABLE,  Mfr2FwdEnabled);
   CheckDlgButton (IDC_CHECK_MFR2REVENABLE,  Mfr2RevEnabled);
   CheckDlgButton (IDC_CHECK_CALLPROGENABLE, CallProgEnabled);
   CheckDlgButton (FlgCED,   CEDEnabled);
   CheckDlgButton (FlgCNG,   CNGEnabled);
   CheckDlgButton (FlgARB,   ARBEnabled);

   if (ARBEnabled) {
        if ((pObj = GetDlgItem(IDC_EDIT_NARBCFGS)) != NULL) {
            pObj->EnableWindow(1);
        }
   } else {
        if ((pObj = GetDlgItem(IDC_EDIT_NARBCFGS)) != NULL)       
            pObj->EnableWindow(0);
   }
   SetDlgItemInt (IDC_EDIT_NARBCFGS, ARBCfgCount, UNSIGNED);

   SetDlgItemInt  (IDC_EDIT_MAXDETTONES,   MaxConcurrentTones, UNSIGNED);
   SetDlgItemInt  (FldToneGenInstances, ToneGenInstances, UNSIGNED);
   CheckDlgButton (IDC_CHECK_DTMF_DIAL, DtmfDialEnabled);
   CheckDlgButton (IDC_CHECK_TONERLYGENENAB, ToneRlyGenEnabled);
   CheckDlgButton (FlgToneDetectB,   ToneDetectBEnabled);

   // Initialize the RTP Tone Type display.
   CheckDlgButton (IDC_RADIO_RTPTONETONE,  RtpToneType == TonePkt);
   CheckDlgButton (IDC_RADIO_RTPTONEEVENT, (RtpToneType == EventPkt) || (RtpToneType == DialedDigitsPkt));
   CheckDlgButton (IDC_RADIO_RTPTONE_NONE, RtpToneType == NoTonePkt);

   SetDlgItemInt  (FldLbCoderInstances, LbCoderInstances, UNSIGNED);

   // Initialize the G.726 Low Memory and Low MIPS displays.
   CheckDlgButton (IDC_CHECK_G726LOWMEM,  G726LowMemEnabled);
   CheckDlgButton (IDC_CHECK_G726LOWMIPS, G726LowMipsEnabled);
   CheckDlgButton (IDC_CHECK_G72616K,   G726Rate16Enabled);
   CheckDlgButton (IDC_CHECK_G72624K,   G726Rate24Enabled);
   CheckDlgButton (IDC_CHECK_G72632K,   G726Rate32Enabled);
   CheckDlgButton (IDC_CHECK_G72640K,   G726Rate40Enabled);
   UpdateG726LowMemSelection ();
   UpdateG726LowMipsSelection ();

   // Initialize other vocoder displays.
   CheckDlgButton (IDC_CHECK_G723,   G723Enabled);
   if (G723Enabled && DspTypeInfo.IsC64xPlus) {
        if ((pObj = GetDlgItem(IDC_CHECK_TI_G7231A)) != NULL) {
            pObj->EnableWindow(1);
        }
   } else {
        if ((pObj = GetDlgItem(IDC_CHECK_TI_G7231A)) != NULL)       
            pObj->EnableWindow(0);
        TI_G7231AEnabled = 0;
   }
   CheckDlgButton (IDC_CHECK_TI_G7231A,  TI_G7231AEnabled);

   CheckDlgButton (IDC_CHECK_G728,   G728Enabled);
   CheckDlgButton (IDC_CHECK_G722,   G722Enabled);
   CheckDlgButton (IDC_CHECK_G729AB,  G729ABEnabled);
   if (G729ABEnabled && DspTypeInfo.IsC64xPlus) {
        if ((pObj = GetDlgItem(IDC_CHECK_TI_G729AB)) != NULL) {
            pObj->EnableWindow(1);
        }
   } else {
        if ((pObj = GetDlgItem(IDC_CHECK_TI_G729AB)) != NULL)       
            pObj->EnableWindow(0);
        TI_G729ABEnabled = 0;
   }
   CheckDlgButton (IDC_CHECK_TI_G729AB,  TI_G729ABEnabled);

   if ((pObj = GetDlgItem(FldLbCoderInstances)) != NULL)       
        pObj->EnableWindow(TRUE);

   CheckDlgButton (IDC_CHECK_MELP,    melpEnabled);
   CheckDlgButton (IDC_CHECK_MELPE,   melpEEnabled);
   CheckDlgButton (IDC_CHECK_GSM_EFR, gsmEFREnabled);
   CheckDlgButton (IDC_CHECK_ADT4800, Adt4800Enabled);

   CheckDlgButton (IDC_CHECK_G711,     G711Enabled);
   CheckDlgButton (IDC_CHECK_G711_WB,  G711WBEnabled);
   CheckDlgButton (IDC_CHECK_L16,      L16Enabled);
   CheckDlgButton (IDC_CHECK_L16_WB,   L16WBEnabled);
   CheckDlgButton (IDC_CHECK_SPEEX,    SpeexEnabled);
   CheckDlgButton (IDC_CHECK_SPEEX_WB, SpeexWBEnabled);

   // Initialize the Port Configuration displays.
   SetDlgItemInt (IDC_EDIT_P1NUMSLOTS,  Port1NumSlots,  UNSIGNED);
   SetDlgItemInt (IDC_EDIT_P1SLOTSUSED, Port1SlotsUsed, UNSIGNED);
   SetDlgItemInt (IDC_EDIT_P2NUMSLOTS,  Port2NumSlots,  UNSIGNED);
   SetDlgItemInt (IDC_EDIT_P2SLOTSUSED, Port2SlotsUsed, UNSIGNED);
   SetDlgItemInt (IDC_EDIT_P3NUMSLOTS,  Port3NumSlots,  UNSIGNED);
   SetDlgItemInt (IDC_EDIT_P3SLOTSUSED, Port3SlotsUsed, UNSIGNED);
   UpdatePort1SlotsSelection ();
   UpdatePort2SlotsSelection ();
   UpdatePort3SlotsSelection ();

   // Initialize the Host Interface Address display.
   UpdateHostIFSelection (HostInterfaceAddress);

   SetDlgItemInt (IDC_EDIT_NUMCHANNELS,   NumChannels, UNSIGNED);
   SetDlgItemInt (IDC_EDIT_NUMCONFERENCES,        NumGpakConferences, UNSIGNED);
   SetDlgItemInt (IDC_EDIT_CUSTOM_CONFERENCE_CNT, NumCustomConferences, UNSIGNED);

   SetDlgItemInt (IDC_EDIT_CHANSPERCONF,   ChansPerConference, UNSIGNED);
   SetDlgItemInt (IDC_EDIT_NUMDOMINANT,   NumConfDominant,   UNSIGNED);
   SetDlgItemInt (IDC_EDIT_MAXNOISESUP,   MaxConfNoiseSuppress,UNSIGNED);
   SetDlgItemHex (this, IDC_EDIT_EXTRAPROGMEM,   DataMemory);


   CheckDlgButton (TI_T38_CHECKBOX, NumTIT38Instances != 0);
   if (NumTIT38Instances != 0)
      SetDlgItemInt (IDC_EDIT_NUMT38_INSTANCES, NumTIT38Instances, UNSIGNED);
   else if (NumT38Instances != 0) 
      SetDlgItemInt (IDC_EDIT_NUMT38_INSTANCES, NumT38Instances, UNSIGNED);
   else 
      SetDlgItemInt (IDC_EDIT_NUMT38_INSTANCES, 0, UNSIGNED);

   SetDlgItemInt (FldToneDetInstances, ToneDetInstances, UNSIGNED);
   SetDlgItemInt (FldToneDetInstances, ToneDetInstances, UNSIGNED);

   // CID
   SetDlgItemInt (IDC_EDIT_RXCIDNUM, NumRxCidInstances, UNSIGNED);
   SetDlgItemInt (IDC_EDIT_TXCIDNUM, NumTxCidInstances, UNSIGNED);


   // Initialize Noise Suppression display
   CheckDlgButton (FLG_NoiseSuppressionEnabled, NoiseSuppressionEnabled);
   SetDlgItemInt (FLD_SuppressMaxAttenuation, NseSpr.MaxAttendB, UNSIGNED);
   SetDlgItemInt (FLD_SuppressVadLo,   NseSpr.VadLowThreshdB, SIGNED);
   SetDlgItemInt (FLD_SuppressVadHi,   NseSpr.VadHighThreshdB, SIGNED);
   SetDlgItemInt (FLD_SuppressVadHang,   NseSpr.VadHangMSec,   UNSIGNED);
   SetDlgItemInt (FLD_SuppressVadWindow,   NseSpr.VadWindowSize, UNSIGNED);
   UpdateNoiseSuppressionSelection ();

   InitAECComboBox (this, AECTailLen);

}
void CGpakConfigDlg::OnSelchangeCmbDSP() {
   
   CComboBox *pCombo;
   int idx, DspValue;

   pCombo = (CComboBox *) GetDlgItem (CmbDSP);
   if (pCombo == NULL) return;
   
   idx = pCombo->GetCurSel ();
   DspValue = pCombo->GetItemData (idx);

   UpdateDspSelection(DspValue);
   UpdateHostIFSelection (DspTypeInfo.HostInterface);

}
void CGpakConfigDlg::UpdateDspSelection (int DspValue) {
   CWnd *pObj;         // pointer to Window object
   ADT_Bool State;   // enable state

   switch (DspValue) {
   default:
   case Ti5410:  DspTypeInfo = Dsp5410Info;  break;
   case Ti5416:  DspTypeInfo = Dsp5416Info;  break;
   case Ti5420:  DspTypeInfo = Dsp5420Info;  break;
   case Ti5421:  DspTypeInfo = Dsp5421Info;  break;
   case Ti5441:  DspTypeInfo = Dsp5441Info;  break;

   case Ti5510:  DspTypeInfo = Dsp5510Info;  break;

   case Ti6416:   DspTypeInfo = Dsp6416Info;  break;
   case Ti6424:   DspTypeInfo = Dsp6424Info;  break;
   case Ti6452:   DspTypeInfo = Dsp6452Info;  break;
   case Ti647:    DspTypeInfo = Dsp647Info;   break;
   case Ti674:    DspTypeInfo = Dsp674Info;   break;
   case Ti665:    DspTypeInfo = Dsp665xInfo;  break;
   case Ti667:    DspTypeInfo = Dsp667xInfo;  break;
   
   case TiDM642:  DspTypeInfo = Dsp64xxInfo;  break;
   case TiDM6437: DspTypeInfo = Dsp6437Info;  break;
   case TiDM648:  DspTypeInfo = Dsp648Info;   break;
   case TiOM3530: DspTypeInfo = Dsp3530Info;  break;
   }
   
   if (DspType != DspValue) {
      DspSpeed = DspTypeInfo.Mips;
      SetDlgItemInt (FldDspSpeed, DspSpeed, UNSIGNED);
   }
   DspType = DspValue;


   // Enable / Disable Serial Port 3 fields.
   State = (DspTypeInfo.NumSerialPorts >= 3);

   if ((pObj = GetDlgItem(IDC_BUTTON_P3CONFIG)) != NULL)       pObj->EnableWindow(State);
   SetItemState (P3NUMSLOTS, State);
   SetItemState (P3SLOTSUSED, State);
   UpdatePort3SlotsSelection ();

   // Disable fields that are restricted to C54xx builds
   State = DspTypeInfo.IsC64x | DspTypeInfo.IsC55x;
   if ((pObj = GetDlgItem(IDC_BUTTON_EXTMEM_CONFIG)) != NULL)    pObj->EnableWindow(State);
   if ((pObj = GetDlgItem (IDC_EDIT_EXTRAPROGMEM)) != NULL)       pObj->EnableWindow(State);

   UpdateAECSelection ();
}
void CGpakConfigDlg::UpdateHostIFSelection (int HostIFValue) {

   if (HostIFValue == 0)
        HostInterfaceAddress = DspTypeInfo.HostInterface;
   else
       HostInterfaceAddress = HostIFValue;

   SetDlgItemLongHex (this, IDC_EDIT_HPIADDRESS, HostInterfaceAddress);
}
void CGpakConfigDlg::UpdateT38Instances () {
   ADT_Bool ValidInt;   // flag indicating valid integer
   int Value;      // integer value

   // Get and validate the entered Number Of PCM Echo Cancellers value.
   Value = (int) GetDlgItemInt (IDC_EDIT_NUMT38_INSTANCES, &ValidInt, UNSIGNED);
   if (!ValidInt)   {
      AfxMessageBox ("The Number Of T.38 channels is invalid!");
      NumT38Instances = 0;
      NumTIT38Instances = 0;
   } else if (IsDlgButtonChecked (TI_T38_CHECKBOX)) {
      NumT38Instances = 0;
      NumTIT38Instances = Value;
   } else {
      NumT38Instances = Value;
      NumTIT38Instances = 0;
   }
}
void CGpakConfigDlg::OnChangeFldLbCoderInstances() {
   ADT_Bool ValidInt;   // flag indicating valid integer
   int Value;      // integer value

   Value = (int) GetDlgItemInt (FldLbCoderInstances, &ValidInt, UNSIGNED);
   if (!ValidInt)   {
      AfxMessageBox ("The number of Loopback Coder instances is invalid!");
      LbCoderInstances = 0;
   } else {
      LbCoderInstances = Value;
  }
	
}
void CGpakConfigDlg::OnCheckedArb() {
   CWnd *pObj;         // pointer to Window object
   ARBEnabled = IsDlgButtonChecked (FlgARB);   

   if (ARBEnabled) {
        if ((pObj = GetDlgItem(IDC_EDIT_NARBCFGS)) != NULL) {
            pObj->EnableWindow(1);
        }
   } else {
        if ((pObj = GetDlgItem(IDC_EDIT_NARBCFGS)) != NULL)       
            pObj->EnableWindow(0);
   }
}
void CGpakConfigDlg::OnCheck16khzTdm() {
   TDM_16kHz = IsDlgButtonChecked(IDC_CHECK_16KHZ_TDM);
}
void CGpakConfigDlg::OnChangeEditNumt38Instances()  {
	UpdateT38Instances ();
}
void CGpakConfigDlg::OnT38Checkbox() {
	UpdateT38Instances ();
}
void CGpakConfigDlg::SRTP_checkbox() {
   if (IsDlgButtonChecked (SRTP_Enabled))
      SRTP_Enable = TRUE;
   else
      SRTP_Enable = FALSE;
}
void CGpakConfigDlg::OnDSPStackCheckBox() {
   if (IsDlgButtonChecked (DSPStackCheckBox))
      HostIF |= RMII_IF;
   else
      HostIF &= ~RMII_IF;
}

// =====================================================================
// Build buttons.
//
//  1) Reads fields from dialog window.   
//  2) Call routine to create G.PAK configuration files
//  3) Writes configuration parameters to .cfg file
void CGpakConfigDlg::OnButtonBuild() {
   ADT_Bool ValidInt;      // flag indicating valid integer
   int Value;         // converted integer value
   char *pErrText;   // pointer to error text
   int NumToneTypes;   // number of tone types selected



   // Verify a frame size was selected.
   if (MaxFrameSize == 0)   {
      AfxMessageBox ("At least one frame size must be selected!");
      return;
   }

   // Validate AAL2 profile dependent parameters.
   if (PktProfile == AAL2Trunking && !FrameMS_5)   {
      AfxMessageBox ("AAL2 requires a Frame Size of 40!");
      return;
   }

   
   // -------------------------------------------------------------
   //  AGC parameters.
   if (AgcInstances != 0)   {

      // Get and validate the entered AGC Target Power value.
     AgcTargetPowerA = (int) GetDlgItemInt(FldAgcTargetPowerA, &ValidInt, SIGNED);
      ItemRangeCheck (MIN_AGC_TARGET_PWR, AgcTargetPowerA, MAX_AGC_TARGET_PWR);

      // Get and validate the entered AGC Max Negative Gain value.
     AgcLossLimitA = (int) GetDlgItemInt(FldAgcLossLimitA, &ValidInt, SIGNED);
      ItemRangeCheck (MIN_AGC_LOSS_LIMIT, AgcLossLimitA, MAX_AGC_LOSS_LIMIT);
 
      // Get and validate the entered AGC Max Positive Gain value.
     AgcGainLimitA = (int) GetDlgItemInt(FldAgcGainLimitA, &ValidInt, SIGNED);
      ItemRangeCheck (MIN_AGC_GAIN_LIMIT, AgcGainLimitA, MAX_AGC_GAIN_LIMIT);

      // Get and validate the entered AGC Low Signal threshold value.
     AgcLowSigThreshA = (int) GetDlgItemInt(FldAgcLowSigThreshA, &ValidInt, SIGNED);
      ItemRangeCheck (MIN_AGC_LOW_SIG_LIMIT, AgcLowSigThreshA, MAX_AGC_LOW_SIG_LIMIT);


      // Get and validate the entered AGC Target Power value.
     AgcTargetPowerB = (int) GetDlgItemInt(FldAgcTargetPowerB, &ValidInt, SIGNED);
      ItemRangeCheck (MIN_AGC_TARGET_PWR, AgcTargetPowerB, MAX_AGC_TARGET_PWR);

      // Get and validate the entered AGC Max Negative Gain value.
     AgcLossLimitB = (int) GetDlgItemInt(FldAgcLossLimitB, &ValidInt, SIGNED);
      ItemRangeCheck (MIN_AGC_LOSS_LIMIT, AgcLossLimitB, MAX_AGC_LOSS_LIMIT);
 
      // Get and validate the entered AGC Max Positive Gain value.
     AgcGainLimitB = (int) GetDlgItemInt(FldAgcGainLimitB, &ValidInt, SIGNED);
      ItemRangeCheck (MIN_AGC_GAIN_LIMIT, AgcGainLimitB, MAX_AGC_GAIN_LIMIT);

      // Get and validate the entered AGC Low Signal threshold value.
     AgcLowSigThreshB = (int) GetDlgItemInt(FldAgcLowSigThreshB, &ValidInt, SIGNED);
      ItemRangeCheck (MIN_AGC_LOW_SIG_LIMIT, AgcLowSigThreshB, MAX_AGC_LOW_SIG_LIMIT);
   
   }

   // -------------------------------------------------------------
   // VAD parameters.
   if (VADEnabled)  {

      // Get and validate the entered VAD Noise Floor value.
      VadNoiseFloor = (int) GetDlgItemInt(IDC_EDIT_VADNOISEFLOOR, &ValidInt, SIGNED);
      ItemRangeCheck (MIN_VAD_NOISE_FLOOR, VadNoiseFloor, MAX_VAD_NOISE_FLOOR);

      // Get and validate the entered VAD Hangover Time value.
     VadHangTime = (int) GetDlgItemInt(IDC_EDIT_VADHANGTIME, &ValidInt, UNSIGNED);
      ItemRangeCheck (MIN_VAD_HANG_TIME, VadHangTime, MAX_VAD_HANG_TIME);
 
      // Get and validate the entered VAD Window Size value.
     VadWindowMs = (int) GetDlgItemInt(IDC_EDIT_VADWINSIZE, &ValidInt, UNSIGNED);
      ItemRangeCheck (MIN_VAD_WIN_MS, VadWindowMs, MAX_VAD_WIN_MS);

   }
 
   // -------------------------------------------------------------
   // Serial Port parameters
   if (Port1Config.TsipEnable) {
        Port1NumSlots = (int) GetDlgItemInt(IDC_EDIT_P1NUMSLOTS, &ValidInt, UNSIGNED);
        ItemRangeCheck (0, Port1NumSlots, MAX_TSIP_SLOTS)
    } else {
        Port1NumSlots = (int) GetDlgItemInt(IDC_EDIT_P1NUMSLOTS, &ValidInt, UNSIGNED);
        ItemRangeCheck (0, Port1NumSlots, MAX_PORT_SLOTS)
    }
    Port1SlotsUsed = (int) GetDlgItemInt(IDC_EDIT_P1SLOTSUSED, &ValidInt, UNSIGNED);
    ItemRangeCheck (0, Port1SlotsUsed, Port1NumSlots);
    if   (Port1SlotsUsed == 0) Port1NumSlots = 0;
    else if (CheckCmpd (1, Port1Config.DataFormat)) return;


   if (Port2Config.TsipEnable) {
        Port2NumSlots = (int) GetDlgItemInt(IDC_EDIT_P2NUMSLOTS, &ValidInt, UNSIGNED);
        ItemRangeCheck (0, Port2NumSlots, MAX_TSIP_SLOTS)
   } else {
        Port2NumSlots = (int) GetDlgItemInt(IDC_EDIT_P2NUMSLOTS, &ValidInt, UNSIGNED);
        ItemRangeCheck (0, Port2NumSlots, MAX_PORT_SLOTS)
   }
   Port2SlotsUsed = (int) GetDlgItemInt(IDC_EDIT_P2SLOTSUSED, &ValidInt, UNSIGNED);
   ItemRangeCheck (0, Port2SlotsUsed, Port2NumSlots);
   if   (Port2SlotsUsed == 0) Port2NumSlots = 0;
   else if (CheckCmpd (2, Port2Config.DataFormat)) return;

   if (Port3Config.TsipEnable) {
        Port3NumSlots = (int) GetDlgItemInt(IDC_EDIT_P3NUMSLOTS, &ValidInt, UNSIGNED);
        ItemRangeCheck (0, Port3NumSlots, MAX_TSIP_SLOTS)
   } else {
        Port3NumSlots = (int) GetDlgItemInt(IDC_EDIT_P3NUMSLOTS, &ValidInt, UNSIGNED);
        ItemRangeCheck (0, Port3NumSlots, MAX_PORT_SLOTS)
   }
   Port3SlotsUsed = (int) GetDlgItemInt(IDC_EDIT_P3SLOTSUSED, &ValidInt, UNSIGNED);
   ItemRangeCheck (0, Port3SlotsUsed, Port3NumSlots);
   if   (Port3SlotsUsed == 0)  Port3NumSlots = 0;
   else if (CheckCmpd (3, Port3Config.DataFormat)) return;


   // -------------------------------------------------------------
   // Instance counts

   DspSpeed = (int) GetDlgItemInt (FldDspSpeed, &ValidInt, UNSIGNED);

   HostInterfaceAddress = (int) GetDlgItemHex (this, IDC_EDIT_HPIADDRESS);

 
   NumChannels = (int) GetDlgItemInt (IDC_EDIT_NUMCHANNELS, &ValidInt, UNSIGNED);
   ItemRangeCheck (1, NumChannels, MAX_CONFIGURABLE_CHANNELS);

   NumGpakConferences = (int) GetDlgItemInt (IDC_EDIT_NUMCONFERENCES, &ValidInt, UNSIGNED);
   ItemRangeCheck (0, NumGpakConferences, MAX_CONFIGURABLE_CONFERENCES);

   NumCustomConferences = (int) GetDlgItemInt (IDC_EDIT_CUSTOM_CONFERENCE_CNT, &ValidInt, UNSIGNED);
   ItemRangeCheck (0, NumCustomConferences, MAX_CONFIGURABLE_CONFERENCES);

   NumConferences = NumGpakConferences + NumCustomConferences;
   ItemRangeCheck (0, NumConferences, MAX_CONFIGURABLE_CONFERENCES);

   if (0 < NumConferences) {
      ChansPerConference = (int) GetDlgItemInt (IDC_EDIT_CHANSPERCONF, &ValidInt, UNSIGNED);
      ItemRangeCheck (1, ChansPerConference, NumChannels);

      NumConfDominant = (int) GetDlgItemInt (IDC_EDIT_NUMDOMINANT, &ValidInt, UNSIGNED);
      ItemRangeCheck (1, NumConfDominant, ChansPerConference);
  
      MaxConfNoiseSuppress  = (int) GetDlgItemInt (IDC_EDIT_MAXNOISESUP, &ValidInt, UNSIGNED);
      ItemRangeCheck (0, MaxConfNoiseSuppress, MAX_MAX_CONF_NOISE_SUPPRESS);

      if ((!VADEnabled || AgcInstances == 0)) {
         AfxMessageBox ("VAD and AGC must be selected for conferencing!");
         return;
      }

   } else {
      ChansPerConference = 0;
      NumConfDominant = 0;
      MaxConfNoiseSuppress = 0;
   }

   NumRxCidInstances = (int) GetDlgItemInt (IDC_EDIT_RXCIDNUM, &ValidInt, UNSIGNED);
   NumTxCidInstances = (int) GetDlgItemInt (IDC_EDIT_TXCIDNUM, &ValidInt, UNSIGNED);

   
   JBMaxMS = (unsigned int) GetDlgItemInt (FldJBMaxMS, &ValidInt, UNSIGNED);
   RtpMaxBuffsPerChan = (unsigned int) GetDlgItemInt (FldJBNBuffs, &ValidInt, UNSIGNED);

   // -------------------------------------------------------------
   // Noise suppression parameters
   if (NoiseSuppressionEnabled) {
      NseSpr.MaxAttendB   = (int)  GetDlgItemInt (FLD_SuppressMaxAttenuation, &ValidInt, SIGNED);
      ItemRangeCheck (0, NseSpr.MaxAttendB, 35);
   
      NseSpr.VadLowThreshdB = (int) GetDlgItemInt (FLD_SuppressVadLo,   &ValidInt, SIGNED);
      ItemRangeCheck (-96, NseSpr.VadLowThreshdB, -1);

      NseSpr.VadHighThreshdB = (int) GetDlgItemInt (FLD_SuppressVadHi,   &ValidInt, SIGNED);
      ItemRangeCheck (NseSpr.VadLowThreshdB+1, NseSpr.VadHighThreshdB, 0);

      NseSpr.VadHangMSec   =  (int) GetDlgItemInt (FLD_SuppressVadHang,   &ValidInt, UNSIGNED);
      ItemRangeCheck (0, NseSpr.VadHangMSec, 32767);

      NseSpr.VadWindowSize  = (int) GetDlgItemInt (FLD_SuppressVadWindow, &ValidInt, UNSIGNED);
      ItemRangeCheck (1, NseSpr.VadWindowSize, 10);
   }

   // -------------------------------------------------------------
   // Get and validate the entered data Memory value.
   DataMemory = (int) GetDlgItemHex (this, IDC_EDIT_EXTRAPROGMEM);
   ItemRangeCheck (0, DataMemory, 0x8000);

   
   // -------------------------------------------------------------
   // Echo Cancellation with short tail must not exceed tap length of 32 ms
   if (NumPcmEcans != 0) {
      if (EcShortTailEnabled && 256 < PcmEcanConfig.TapLength) {
           AfxMessageBox ("Short tail option requires PCM tap length <= 32 ms!");
         return;
      }
      if (!EcShortTailEnabled && PcmEcanConfig.TapLength < 256) {
          AfxMessageBox ("Long tail option requires 32ms <= PCM tap length!");
         return;
      }
   }

   if (NumPktEcans != 0) { 
      if (EcShortTailEnabled && 256 < PktEcanConfig.TapLength) {
          AfxMessageBox ("Short tail option requires PKT tap length <= 32 ms!");
         return;
      }
      if (!EcShortTailEnabled && PktEcanConfig.TapLength < 256) {
          AfxMessageBox ("Long tail option requires 32ms <= PKT tap length!");
         return;
      }
   }


  GetAECInfo (this);
  MicEqLength = (unsigned int) GetDlgItemInt (FldMicEqLen, &ValidInt, UNSIGNED);
  SpkrEqLength = (unsigned int) GetDlgItemInt (FldSpkrEqLen, &ValidInt, UNSIGNED);
  if ((NumAECEcans == 0) && (MicEqLength || SpkrEqLength)) {
      AfxMessageBox ("Non-zero equalizer length requires an AEC Instance!");
      return;
  }


   // Verify the frame sizes for fixed frame codecs.
   if (G723Enabled && !FrameMS_30) {
      AfxMessageBox ("G.723 requires a Frame Size of 240!");
      return;
   }

   if (G729ABEnabled && !(FrameMS_10 || FrameMS_20 || FrameMS_30)) {
      AfxMessageBox ("G.729 requires a  multiple of 80 Frame Size!");
      return;
   }

   if (gsmEFREnabled && !FrameMS_20) {
      AfxMessageBox ("AMR requires a Frame Size of 160!");
      return;
   }

   if (melpEnabled && !FrameMS_22_5) {
      AfxMessageBox ("MELP requires a Frame Size of 180!");
      return;
   }

   if (melpEEnabled && !FrameMS_22_5) {
      AfxMessageBox ("MELPE requires a Frame Size of 180!");
      return;
   }

   if (Adt4800Enabled && !FrameMS_30)   {
     AfxMessageBox ("ADT-4800 requires a Frame Size of 240!");
      return;
   }


   // Get the Host Port Interrupt selections.
   Hint8   = (FrameMS_1   && IsDlgButtonChecked (IDC_CHECK_HINT8));
   Hint20  = (FrameMS_2_5 && IsDlgButtonChecked (IDC_CHECK_HINT20));
   Hint40  = (FrameMS_5   && IsDlgButtonChecked (IDC_CHECK_HINT40));
   Hint80  = (FrameMS_10  && IsDlgButtonChecked (IDC_CHECK_HINT80));
   Hint160 = (FrameMS_20  && IsDlgButtonChecked (IDC_CHECK_HINT160));
#ifdef MELP
   Hint180 = (FrameMS_22_5 && IsDlgButtonChecked (IDC_CHECK_HINT180));
#else
   Hint180 = FALSE;
#endif
   Hint240 = (FrameMS_30 && IsDlgButtonChecked (IDC_CHECK_HINT240));

   if (IsDlgButtonChecked (DSPStackCheckBox))
      HostIF |= RMII_IF;
   else
      HostIF &= ~RMII_IF;

   // Determine the selected Channel types.
   Pcm2PktChan = IsDlgButtonChecked (IDC_CHECK_PCM2PKT);
   Pcm2PcmChan = IsDlgButtonChecked (IDC_CHECK_PCM2PCM);
   Pkt2PktChan = IsDlgButtonChecked (IDC_CHECK_PKT2PKT);
   CircDataChan = IsDlgButtonChecked (IDC_CHECK_CIRCDATA);
   ConfPcmChan =  IsDlgButtonChecked (IDC_CHECK_CONFPCM);
   ConfPktChan =  IsDlgButtonChecked (IDC_CHECK_CONFPKT);
   ConfCompChan = IsDlgButtonChecked (IDC_CHECK_CONFCOMP);

   // Verify tone selection parameters.
   NumToneTypes = 0;

   DtmfEnabled = IsDlgButtonChecked (IDC_CHECK_DTMFENABLE);
   Mfr1Enabled = IsDlgButtonChecked (IDC_CHECK_MFR1ENABLE);
   Mfr2FwdEnabled = IsDlgButtonChecked (IDC_CHECK_MFR2FWDENABLE);
   Mfr2RevEnabled = IsDlgButtonChecked(IDC_CHECK_MFR2REVENABLE);
   CallProgEnabled = IsDlgButtonChecked (IDC_CHECK_CALLPROGENABLE);
   CEDEnabled   = IsDlgButtonChecked (FlgCED);
   CNGEnabled   = IsDlgButtonChecked (FlgCNG);
   ARBEnabled   = IsDlgButtonChecked (FlgARB);

   Value = (int) GetDlgItemInt (FldToneDetInstances, &ValidInt, UNSIGNED);
   if (!ValidInt || (Value < 1)) ToneDetInstances = 0;
   else                          ToneDetInstances = Value;

   if (CEDEnabled)   CEDDetInstances = ToneDetInstances;
   if (CNGEnabled)   CNGDetInstances = ToneDetInstances;

   if (ARBEnabled) {
        ARBDetInstances = ToneDetInstances;
        Value = (int) GetDlgItemInt (IDC_EDIT_NARBCFGS, &ValidInt, UNSIGNED);
        if (!ValidInt || (Value < 1)) {
            AfxMessageBox ("Arbitrary Tone Detector needs at least 1 configuration !");
            return;
         } else  {
            ARBCfgCount = Value;
        }
    } else {
        ARBCfgCount = 0;
    }

   if (DtmfEnabled)      NumToneTypes++;
   if (Mfr1Enabled)      NumToneTypes++;
   if (Mfr2FwdEnabled)   NumToneTypes++;
   if (Mfr2RevEnabled)   NumToneTypes++;
   if (CallProgEnabled)  NumToneTypes++;

   if (0 < NumToneTypes)   {
      Value = (int) GetDlgItemInt(IDC_EDIT_MAXDETTONES, &ValidInt, UNSIGNED);
      if (!ValidInt || (Value < 1))   {
         AfxMessageBox ("The Max Concurrent Tone Types is invalid!");
         AfxMessageBox ("The Max Concurrent Tone Types is invalid!");
        AfxMessageBox ("The Max Concurrent Tone Types is invalid!");
        return;
     }
      MaxConcurrentTones = Value;
   } else
      MaxConcurrentTones = 0;


   if (0 < NumToneTypes && ToneDetInstances == 0) {
      AfxMessageBox ("Tone detection requires at least 1 tone detect instance");
      return;
   }

   if (CEDEnabled && CEDDetInstances == 0) {
      AfxMessageBox ("CED detect requires at least 1 tone detect instance");
      return;
   }

   if (CNGEnabled && CNGDetInstances == 0) {
      AfxMessageBox ("CNG detect requires at least 1 tone detect instance");
      return;
   }

   if (ARBEnabled && ARBDetInstances == 0) {
      AfxMessageBox ("Arbitrary detect requires at least 1 tone detect instance");
      return;
   }

   Value = (int) GetDlgItemInt(FldToneGenInstances, &ValidInt, UNSIGNED);
   if (!ValidInt || (Value < 1)) ToneGenInstances = 0;
   else                          ToneGenInstances = Value;

   DtmfDialEnabled   = IsDlgButtonChecked (IDC_CHECK_DTMF_DIAL);

   if (ToneGenInstances == 0 && DtmfDialEnabled) {
      AfxMessageBox ("DtmfDialing requires at least 1 generation instance");
      return;
   }

   Value = (int) GetDlgItemInt(FldLbCoderInstances, &ValidInt, UNSIGNED);
   if (!ValidInt || (Value < 1)) LbCoderInstances = 0;
   else                          LbCoderInstances = Value;



   if ((RtpToneType != NoTonePkt) && (NumToneTypes == 0))   {
      AfxMessageBox ("Tone Relay Detect requires at least 1 tone type!");
      return;
   }

   if (ToneGenInstances == 0 && ToneRlyGenEnabled) {
      AfxMessageBox ("Tone relay generation requires at least 1 generation instance");
      return;
   }

   if (ToneGenInstances < NumTIT38Instances) {
      AfxMessageBox ("TI's t.38 relay requires at least 1 tone generation instance per t.38 instance");
      return;
   }

   NumExtMemObjs  = 0;
   if ((!DspTypeInfo.IsC64x) && (!DspTypeInfo.IsC55x)){
      ExtMemConfig.type = EXT_NONE;
   }


   if (ExtMemConfig.type == EXT_DMA) {
      NumExtMemObjs = NumPktEcans + NumPcmEcans;
      
      ADT_Bool G726LowMIPS = (G726LowMipsEnabled    &&
         (G726Rate16Enabled     || G726Rate24Enabled     ||
          G726Rate32Enabled     || G726Rate40Enabled)  );

      if (G726LowMemEnabled     || G726LowMIPS     ||
          G723Enabled           || G728Enabled     ||
          G729ABEnabled         || melpEnabled     || melpEEnabled || 
          gsmEFREnabled         ||  Adt4800Enabled || G722Enabled)
         NumExtMemObjs += NumChannels;

      // no objects have been configured that can be stored in external mem.
      // Disable the flags indicating that external memory is used for DMA.
      // Overflow external memory can still be used
      if (NumExtMemObjs == 0)  {
         ExtMemConfig.type = EXT_CACHE;
      }

      if (!FrameMS_10 && !FrameMS_20 && !FrameMS_30)   {
         ExtMemConfig.type = EXT_CACHE;
      }
   }

   if (ExtMemConfig.type == EXT_DMA) {
      numAtccBlocks = 8;
   } else 
      numAtccBlocks = 0;

   // Attempt to build the G.PAK Configuration files.
   if (!BuildGpakConfigFiles(&pErrText))   {
      AfxMessageBox (pErrText);
      return;
   }

   // Indicate the build was successful.
   Value = AfxMessageBox("Configuration files created successfully!\nYes to exit. No to change parameters.",
                         MB_YESNO | MB_ICONQUESTION);

   // Save the current values of all selectable parameters.
   WriteConfigFile();

   if (Value == IDYES) EndDialog (0);
}
void CGpakConfigDlg::OnButtonBuild2() {
   ForceRebuild = TRUE;
   OnButtonBuild ();   
}
void CGpakConfigDlg::OnButtonPath() {

/////////////////////// NEW from JDC to access the path for components and stubs /////////////////////
//   

   CDirDialog cfdlg;
   int len;

#ifdef ADD_STUB
   cfdlg.m_ofn.lpstrTitle = _T ("Select Stub Path");
   cfdlg.m_ofn.lpstrInitialDir = StubPath;
   cfdlg.m_strFilter = _T("Stub Files|*Stub*o*|Folders Only|.|All Files|*.*||");
   cfdlg.m_strFilter.Replace('|', '\0');
   if (IDOK==cfdlg.DoModal()) strcpy (StubPath, (LPCTSTR) cfdlg.m_rawPath);
#endif

   cfdlg.m_ofn.lpstrTitle = _T ("Select Component Path");
   cfdlg.m_ofn.lpstrInitialDir = CompPath;
   cfdlg.m_strFilter = _T("Component Files|*.l*|Folders Only|.|All Files|*.*||");
   cfdlg.m_strFilter.Replace('|', '\0');
   if (IDOK==cfdlg.DoModal())  strcpy (CompPath, (LPCTSTR) cfdlg.m_rawPath);

#ifdef ADD_STUB
   len = strlen (StubPath);
   if (StubPath[len-1] == '\\') StubPath[len-1] = 0;
#endif

   len = strlen (CompPath);
   if (CompPath[len-1] == '\\') CompPath[len-1] = 0;

//////////////////////////////////////////////////////////////////////////////////////////////////////

}
void CGpakConfigDlg::OnButtonCidcfg() {
    CIDConfigDlg = CIDConfigLcl;
    if (m_CallerId.DoModal() == IDOK)   {
        CIDConfigLcl = CIDConfigDlg;
   }
}
void CGpakConfigDlg::OnButtonExtmemConfig() {
   // Use the External Memory dialog to configure external memory
   if (m_ExternalMem.DoModal() == IDOK)   {
      ;
   }

}
