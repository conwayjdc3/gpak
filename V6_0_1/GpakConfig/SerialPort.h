#if !defined(AFX_SERIALPORT_H__A989A16D_7CA4_4E72_9567_043E9715C13C__INCLUDED_)
#define AFX_SERIALPORT_H__A989A16D_7CA4_4E72_9567_043E9715C13C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SerialPort.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// SerialPort dialog

class SerialPort : public CDialog
{
// Construction
public:
	int m_PortNum;
	SerialPort(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(SerialPort)
	enum { IDD = IDD_SERIALPORT_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(SerialPort)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(SerialPort)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SERIALPORT_H__A989A16D_7CA4_4E72_9567_043E9715C13C__INCLUDED_)
