//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by GpakConfig.rc
//
#define IDDEFAULTS                      3
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_GPAKCONFIG_DIALOG           102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDD_ECHOCANCEL_DIALOG           105
#define IDD_SERIALPORT_DIALOG           106
#define IDR_MAINFRAME                   128
#define IDI_ICON1                       128
#define IDB_BITMAP1                     133
#define IDD_EXTMEM_DIALOG               135
#define IDC_CURSOR1                     145
#define IDD_DIALOG1                     146
#define IDD_DIALOG_CID                  147
#define IDC_RADIO_5410                  1001
#define IDC_RADIO_5416                  1002
#define IDC_RADIO_5420                  1003
#define IDC_CHECK_AGCENABLE             1004
#define IDC_EDIT_AGCTARGPWR             1005
#define FldAgcTargetPowerA              1005
#define IDC_EDIT_AGCMAXNEGGAIN          1006
#define IDC_EDIT_TAPLENGTH              1006
#define FldAgcLossLimitA                1006
#define IDC_EDIT_AGCMAXPOSGAIN          1007
#define IDC_EDIT_NLPTHRESHOLD           1007
#define FldAgcGainLimitA                1007
#define IDC_CHECK_VADENABLE             1008
#define IDC_EDIT_CNGTHRESHOLD           1008
#define IDC_EDIT_VADNOISEFLOOR          1009
#define IDC_EDIT_DBLTALKTHRESH          1009
#define IDC_EDIT_VADHANGTIME            1010
#define IDC_EDIT_CROSSCORRLIMIT         1010
#define IDC_EDIT_VADWINSIZE             1011
#define IDC_EDIT_NUMFIRSEGS             1011
#define IDC_CHECK_DTMFENABLE            1012
#define IDC_EDIT_FIRSEGLENGTH           1012
#define IDC_CHECK_MFR1ENABLE            1013
#define IDC_CHECK_165DETENABLE          1013
#define IDC_CHECK_MFR2FWDENABLE         1014
#define IDC_EDIT_ADAPTLIMIT             1014
#define IDC_CHECK_MFR2REVENABLE         1015
#define IDC_CHECK_ADAPTENABLE           1015
#define IDC_EDIT_MAX_DBLTALKTHRESH      1015
#define IDC_RADIO_MAXFRAME8             1016
#define IDC_EDIT_PCMECNUM               1016
#define IDC_EDIT_NLPCONVERGE            1016
#define IDC_CHECK_G726LOWMEM            1017
#define IDC_EDIT_NLPNONCONVERGE         1017
#define IDC_CHECK_G726LOWMIPS           1018
#define IDC_EDIT_NLPMAXSUPPRESS         1018
#define IDC_CHECK_G72616K               1019
#define IDC_EDIT_FIRTAPCHECK            1019
#define IDC_CHECK_G72624K               1020
#define ChkTandemOperation              1020
#define IDC_CHECK_G72632K               1021
#define ChkEchoSource                   1021
#define IDC_CHECK_G72640K               1022
#define ChkReconvergence                1022
#define IDC_CHECK_PCMECENABLE           1023
#define IDC_EDIT_PKTECNUM               1023
#define IDC_BUTTON_PCMECCONFIG          1024
#define IDC_RADIO_MAXFRAME20            1025
#define IDC_CHECK_CALLPROGENABLE        1025
#define IDC_BUTTON_PKTECCONFIG          1026
#define IDC_BUTTON_P1CONFIG             1027
#define IDC_RADIO_5421                  1028
#define IDC_BUTTON_P2CONFIG             1029
#define IDC_BUTTON_P3CONFIG             1030
#define IDC_BUTTON_BUILD                1031
#define IDC_STATIC_PORT3                1032
#define IDC_STATIC_P3NUMSLOTS           1033
#define IDC_STATIC_P3SLOTSUSED          1034
#define IDC_STATIC_RTPTONEPKTTYPE       1035
#define IDC_RADIO_MAXFRAME40            1036
#define FLD_SuppressMaxAttenuation      1036
#define IDC_RADIO_MAXFRAME80            1037
#define FLD_SuppressVadLo               1037
#define IDC_CHECK_PKTECENABLE           1038
#define FLD_SuppressVadHi               1038
#define IDC_STATIC_AGCTARGPWR           1039
#define IDC_STATIC_AGCMAXNEGGAIN        1040
#define IDC_STATIC_AGCMAXPOSGAIN        1041
#define IDC_RADIO_5441                  1042
#define IDC_STATIC_CROSSCORRLIMIT       1042
#define IDC_BUTTON_PATH                 1042
#define IDC_STATIC_NUMFIRSEGS           1043
#define IDC_CHECK_HINT8                 1043
#define IDC_STATIC_FIRSEGLENGTH         1044
#define IDC_CHECK_HINT240               1044
#define IDC_STATIC_VADNOISEFLOOR        1045
#define IDC_STATIC_VADHANGTIME          1046
#define IDC_STATIC_VADWINSIZE           1047
#define IDC_CHECK_HINT20                1048
#define IDC_CHECK_HINT40                1049
#define IDC_CHECK_ECSHORTTAIL           1050
#define IDC_RADIO_MAXFRAME160           1051
#define IDC_STATIC_AGCMAXPOSGAIN2       1051
#define IDC_RADIO_MAXFRAME240           1052
#define IDC_STATIC_AGCMAXPOSGAIN3       1052
#define IDC_RADIO_RTPPROFILE            1053
#define IDC_RADIO_AAL2PROFILE           1054
#define IDC_RADIO_RTPTONETONE           1055
#define IDC_RADIO_RTPTONEEVENT          1056
#define IDC_EDIT_HPIADDRESS             1057
#define IDC_CHECK_TONERLYDETENAB        1058
#define LBL_MaxAttenuation              1058
#define IDC_CHECK_HINT80                1059
#define IDC_CHECK_HINT160               1060
#define IDC_CHECK_FRAME8                1061
#define IDC_CHECK_FRAME20               1062
#define IDC_CHECK_FRAME40               1063
#define IDC_RADIO_NLPOFF                1064
#define IDC_CHECK_FRAME80               1064
#define IDC_RADIO_NLPMUTE               1065
#define IDC_CHECK_FRAME160              1065
#define IDC_RADIO_NLPRAND               1066
#define IDC_CHECK_FRAME240              1066
#define IDC_RADIO_NLPHOTH               1067
#define IDC_CHECK_PCM2PKT               1067
#define IDC_CHECK_PCM2PCM               1068
#define IDC_RADIO_NLPSUPP               1068
#define IDC_EDIT_P1NUMSLOTS             1069
#define IDC_RADIO_NLPSUPPAUTO           1069
#define IDC_EDIT_P1SLOTSUSED            1070
#define IDC_RADIO_MULAW                 1071
#define IDC_CHECK_PKT2PKT               1071
#define IDC_RADIO_ALAW                  1072
#define IDC_CHECK_CIRCDATA              1072
#define IDC_RADIO_LINEAR8               1073
#define IDC_CHECK_CONFPCM               1073
#define IDC_RADIO_LINEAR16              1074
#define IDC_CHECK_CONFPKT               1074
#define IDC_RADIO_XSYNCHIGH             1075
#define IDC_CHECK_CONFCOMP              1075
#define IDC_RADIO_XSYNCLOW              1076
#define IDC_CHECK_TONEGENENABLE         1076
#define FlgCNG                          1076
#define IDC_CHECK_TONERLYGENENAB        1077
#define IDC_RADIO_RTPTONE_NONE          1078
#define IDC_RADIO_XCLKRISE              1079
#define LBL_VadLo                       1079
#define IDC_RADIO_XCLKFALL              1080
#define IDC_NoiseSuppression            1080
#define LBL_VadHi                       1081
#define IDC_STATIC_FIRTAPCHECK          1081
#define LBL_VadHang                     1082
#define IDC_EDIT_XDATDELAY              1083
#define LBL_VadWindow                   1083
#define IDC_EDIT_RDATDELAY              1084
#define FLD_SuppressVadHang             1084
#define IDC_CHECK_DXDELAY               1085
#define FLD_SuppressVadWindow           1085
#define IDC_EDIT_P2NUMSLOTS             1086
#define IDC_RADIO_RSYNCHIGH             1086
#define IDC_EDIT_P2SLOTSUSED            1087
#define IDC_RADIO_RSYNCLOW              1087
#define FLG_NoiseSuppressionEnabled     1088
#define IDC_RADIO_64xx                  1089
#define IDC_CHECK_FRAME180              1089
#define IDC_RADIO_RCLKRISE              1090
#define IDC_STATIC_AGCLOWSIGTHRESH      1090
#define IDC_RADIO_RCLKFALL              1091
#define IDC_EDIT_AGCLOWSIGTHRESH        1091
#define FldAgcLowSigThreshA             1091
#define IDC_STATIC_AGCMAXPOSGAIN4       1092
#define IDC_EXTERNAL_MEM                1093
#define FldNumAEC                       1094
#define FlgToneDetectB                  1095
#define FlgCED                          1096
#define IDC_STATIC_AGCMAXPOSGAIN5       1097
#define FldAgcTargetPowerB              1098
#define IDC_CHECK_HINT180               1099
#define IDC_BUTTON_BUILD2               1100
#define DSPStackCheckBox                1101
#define FldAgcLossLimitB                1102
#define IDC_EDIT_P3NUMSLOTS             1103
#define IDC_EDIT_P3SLOTSUSED            1104
#define FldAgcGainLimitB                1105
#define FldAgcLowSigThreshB             1106
#define SRTP_Enabled                    1107
#define IDC_CHECK_16KHZ_TDM             1108
#define IDC_EMIFA                       1109
#define IDC_EMIFB                       1110
#define IDC_CHECK_EMIFA_CE0             1111
#define IDC_CHECK_EMIFA_CE1             1112
#define IDC_CHECK_EMIFA_CE2             1113
#define IDC_CHECK_EMIFA_CE3             1114
#define IDC_CHECK_EMIFB_CE0             1115
#define IDC_BUTTON_EXTMEM_CONFIG        1115
#define IDC_CHECK_EMIFB_CE1             1116
#define IDC_CHECK_EMIFB_CE2             1117
#define IDC_CHECK_EMIFB_CE3             1118
#define IDC_CHECK_EXTMEM_OVFL           1119
#define IDC_CHECK_G723                  1120
#define IDC_CHECK_G728                  1121
#define IDC_CHECK_G729AB                1122
#define IDC_EDIT_NUMCHANNELS            1123
#define IDC_CHECK_ADT4800               1124
#define IDC_EDIT_EXTRAPROGMEM           1125
#define IDC_EDIT_NUMCONFERENCES         1126
#define IDC_EDIT_CHANSPERCONF           1127
#define IDC_EDIT_NUMDOMINANT            1128
#define IDC_EDIT_MAXNOISESUP            1129
#define IDC_EDIT_MAXDETTONES            1130
#define IDC_CHECK_GSM_EFR               1131
#define CmbAECTailLen                   1132
#define FldLibInfo                      1133
#define IDC_EDIT_NUMT38_INSTANCES       1133
#define FldToneGenInstances             1134
#define FldAGCInstances                 1135
#define IDC_CHECK_MELP                  1136
#define FldToneDetInstances             1137
#define FldDspSpeed                     1138
#define FldJBMaxMS                      1139
#define IDC_EDIT_CUSTOM_CONFERENCE_CNT  1140
#define IDC_CHECK_G711                  1141
#define IDC_CHECK_L16_WB                1142
#define CmbCompand                      1143
#define IDC_CHECK_L16                   1143
#define CmbDSP                          1144
#define AlgorithmDMA                    1145
#define IDC_CHECK_G711_WB               1145
#define Cached                          1146
#define IDC_CHECK_MELPE                 1146
#define NoExtMem                        1147
#define IDC_EDIT_RXCIDNUM               1148
#define IDC_EDIT_TXCIDNUM               1149
#define IDC_BUTTON_CIDCFG               1150
#define IDC_CHECK_FMTTXCID              1151
#define IDC_EDIT_TXCID_NUMSIEZ          1152
#define IDC_EDIT_TXCID_NUMMARK          1153
#define IDC_EDIT_TXCID_NUMPOST          1154
#define IDC_EDIT_TXCID_LEVEL            1155
#define IDC_EDIT_TXCID_MSG              1156
#define IDC_RADIO_CID_V23               1157
#define IDC_RADIO_CID_BELL202           1158
#define IDC_BUTTON_CID_DEF              1164
#define IDC_CHECK_TI_G729AB             1165
#define IDC_CHECK_VADREPORT             1166
#define TI_T38_CHECKBOX                 1167
#define FldLbCoderInstances             1168
#define FldJBNBuffs                     1169
#define FldMicEqLen                     1170
#define FldSpkrEqLen                    1171
#define FlgARB                          1172
#define IDC_EDIT_NARBCFGS               1173
#define IDC_CHECK_TSIP                  1174
#define IDC_CHECK_G722                  1175
#define IDC_CHECK_SPEEX                 1176
#define IDC_CHECK_SPEEX_WB              1177
#define IDC_CHECK_DTMF_DIAL             1178
#define IDC_CHECK_TI_G7231A             1179

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        148
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1179
#define _APS_NEXT_SYMED_VALUE           107
#endif
#endif
