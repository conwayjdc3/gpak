// AECG4DLL.h : main header file for the AECG4DLL DLL
//

#pragma once

#ifndef __AFXWIN_H__
//	#error "include 'stdafx.h' before including this file for PCH"
#endif

//#include "resource.h"		// main symbols


#include "std1.h"
#include "alg.h"
#include "xdas.h"
#include "aecg4.h"
#if 0
#include "aecg4_private.h"
#endif

// CAECG4DLLApp
// See AECG4DLL.cpp for the implementation of this class
//
#ifdef _USRDLL
   #define MY_APP_API __declspec(dllexport) 
 #else
   #define MY_APP_API __declspec(dllimport)
 #endif

class MY_APP_API CAECG4
{
	
public:
	CAECG4();
	~CAECG4();
	CAECG4(AECG4_Params *pParams);
	//CAECG4(AECG4Params_t *pParams);
	void Apply(short int *pRxIn, short int *pRxOut, short int *pTxIn, short int *pTxOut);
	void BackgroundHandler();
	void GetVersion(char *VersionString);
	void GetStatus(AECG4_Status *pStatus);
	void Control(AECG4_Cmd Cmd, AECG4_Status *pStatus);
#ifdef DUMP_ENABLE
	void dumpInit(DumpParams_t *pDumpParams);
	void dump();
#endif
	static void StaticAllocHelper(const IALG_Params *AECG4Params, IALG_Fxns **fxns, IALG_MemRec memTab[]);
	int CAECG4::alloc(const IALG_Params *AECG4Params, IALG_Fxns **fxns, IALG_MemRec memTab[]);
	AECG4_Handle  AECG4Handle;
};

