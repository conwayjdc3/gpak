# Microsoft Developer Studio Project File - Name="GpakConfig" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=GpakConfig - Win32 V6_0
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "GpakConfig.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "GpakConfig.mak" CFG="GpakConfig - Win32 V6_0"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "GpakConfig - Win32 V5_0" (based on "Win32 (x86) Application")
!MESSAGE "GpakConfig - Win32 V6_0" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "GpakConfig - Win32 V5_0"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "GpakConfig___Win32_V5_0"
# PROP BASE Intermediate_Dir "GpakConfig___Win32_V5_0"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "GpakConfig___Win32_V5_0"
# PROP Intermediate_Dir "GpakConfig___Win32_V5_0"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I "..\Gpak64xDsp" /I "w:\Utilities" /I "..\Includes" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D VERS=0x402 /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I "..\Gpak64xDsp" /I "w:\Utilities" /I "..\Includes" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D VERS=0x601 /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /out:"GpakConfigV4_2.exe" /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386 /out:"GpakConfigV5_0.exe" /pdbtype:sept

!ELSEIF  "$(CFG)" == "GpakConfig - Win32 V6_0"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "GpakConfig___Win32_V6_0"
# PROP BASE Intermediate_Dir "GpakConfig___Win32_V6_0"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "GpakConfig___Win32_V6_0"
# PROP Intermediate_Dir "GpakConfig___Win32_V6_0"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I "..\Gpak64xDsp" /I "w:\Utilities" /I "..\Includes" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D VERS=0x500 /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I "..\Gpak64xDsp" /I "w:\adt_algorithms" /I "w:\Utilities" /I "..\Includes" /I "." /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D VERS=0x600 /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /out:"GpakConfigV5_0.exe" /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386 /out:"GpakConfigV6_0.exe" /pdbtype:sept

!ENDIF 

# Begin Target

# Name "GpakConfig - Win32 V5_0"
# Name "GpakConfig - Win32 V6_0"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\DirDialog.cpp

!IF  "$(CFG)" == "GpakConfig - Win32 V5_0"

!ELSEIF  "$(CFG)" == "GpakConfig - Win32 V6_0"

# ADD CPP /I "T:\adt_algorithms"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\EchoCancel.cpp

!IF  "$(CFG)" == "GpakConfig - Win32 V5_0"

!ELSEIF  "$(CFG)" == "GpakConfig - Win32 V6_0"

# ADD CPP /I "T:\adt_algorithms"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\ExternalMem.cpp

!IF  "$(CFG)" == "GpakConfig - Win32 V5_0"

!ELSEIF  "$(CFG)" == "GpakConfig - Win32 V6_0"

# ADD CPP /I "T:\adt_algorithms"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\GpakCidDlg.cpp

!IF  "$(CFG)" == "GpakConfig - Win32 V5_0"

!ELSEIF  "$(CFG)" == "GpakConfig - Win32 V6_0"

# ADD CPP /I "T:\adt_algorithms"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\GpakConfig.cpp

!IF  "$(CFG)" == "GpakConfig - Win32 V5_0"

!ELSEIF  "$(CFG)" == "GpakConfig - Win32 V6_0"

# ADD CPP /I "T:\adt_algorithms"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\GpakConfigDlg.cpp

!IF  "$(CFG)" == "GpakConfig - Win32 V5_0"

!ELSEIF  "$(CFG)" == "GpakConfig - Win32 V6_0"

# ADD CPP /I "T:\adt_algorithms"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\GpakConfigUtl.cpp

!IF  "$(CFG)" == "GpakConfig - Win32 V5_0"

!ELSEIF  "$(CFG)" == "GpakConfig - Win32 V6_0"

# ADD CPP /I "T:\adt_algorithms"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\SerialPort.cpp

!IF  "$(CFG)" == "GpakConfig - Win32 V5_0"

!ELSEIF  "$(CFG)" == "GpakConfig - Win32 V6_0"

# ADD CPP /I "T:\adt_algorithms"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp

!IF  "$(CFG)" == "GpakConfig - Win32 V5_0"

# ADD BASE CPP /Yc"stdafx.h"
# ADD CPP /Yc"stdafx.h"

!ELSEIF  "$(CFG)" == "GpakConfig - Win32 V6_0"

# ADD BASE CPP /Yc"stdafx.h"
# ADD CPP /I "T:\adt_algorithms" /Yc"stdafx.h"

!ENDIF 

# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\DirDialog.h
# End Source File
# Begin Source File

SOURCE=.\EchoCancel.h
# End Source File
# Begin Source File

SOURCE=.\ExternalMem.h
# End Source File
# Begin Source File

SOURCE=.\GpakCidDlg.h
# End Source File
# Begin Source File

SOURCE=.\GpakConfig.h
# End Source File
# Begin Source File

SOURCE=.\GpakConfigDlg.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\SerialPort.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\theFileDlg.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\bitmap1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\cursor1.cur
# End Source File
# Begin Source File

SOURCE=.\res\GpakConfig.ico
# End Source File
# Begin Source File

SOURCE=.\GpakConfig.rc
# End Source File
# Begin Source File

SOURCE=.\res\GpakConfig.rc2
# End Source File
# End Group
# Begin Source File

SOURCE=.\aecg4dll.lib

!IF  "$(CFG)" == "GpakConfig - Win32 V5_0"

!ELSEIF  "$(CFG)" == "GpakConfig - Win32 V6_0"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\ADT_AECDLL_V1_50_CGTVS2008.lib
# End Source File
# End Target
# End Project
