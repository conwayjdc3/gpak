#include "GpakDefs.h"
#include "GpakErrs.h"
#include "GpakExts.h"
#include "GpakHpi.h"
#include "sysconfig.h"
#include "GpakDma.h"
#define JDC_CAPTURE
extern void  jdc_capture(int chanId, void *pcmBuff, int SampsPerFrame, int code);   

extern ADT_Bool CheckAecMemory(ADT_UInt16 FrameSize, ADT_UInt32 SampleRate);
extern void play_vector (ADT_Int16 *buff, ADT_UInt32 num);
extern int vect_play;
extern void processFaxRelay (chanInfo_t *pChan, void *pcmInBuff, void *pcmOut, int FrameSize);
extern void waveFilePlayback (wavefileInfo_t *pInfo, ADT_Int16 *buff,  int sampsPerFrame); 



#pragma CODE_SECTION (customProcChanCfgMsg, "SLOW_PROG_SECT")
GPAK_ChannelConfigStat_t customProcChanCfgMsg (ADT_UInt16 *pCmd, chanInfo_t *chan) {
   // Called by messaging to parse a custom channel configuration message


   pcm2pkt_t *PcmPkt;   // pointer to channel's PCM to Packet data
   pkt2pcm_t *PktPcm;   // pointer to channel's Packet to PCM data

   GPAK_ChannelConfigStat_t toneStatus;
   GpakFaxMode_t faxMode;
   GpakToneTypes toneTypes, toneTypesPktPcm;
   int mfTones, cedTones, cngTones, arbTones;

   PcmPkt = &(chan->pcmToPkt);
   PktPcm = &(chan->pktToPcm);

   channelAllocsNull (chan, PcmPkt, PktPcm);


   // Frame rate in samples before api version 603
   // Frame rate in 1/2 ms after api version 603
   if (ApiBlock.ApiVersionId < 0x0603u) {
      PcmPkt->SamplesPerFrame = Byte0 (pCmd[5]);
      PktPcm->SamplesPerFrame = Byte0 (pCmd[4]);
   } else {
      PcmPkt->SamplesPerFrame = halfMSToSamps (Byte0 (pCmd[5]));
      PktPcm->SamplesPerFrame = halfMSToSamps (Byte0 (pCmd[4]));
   }

   //
   //  Get TDM and vocoding parameters
   //
   PcmPkt->InSerialPortId  = (GpakSerialPort_t) UnpackPort (pCmd[2]);
   PcmPkt->InSlotId =                           UnpackSlot (pCmd[2]);
   PcmPkt->Coding =                (GpakCodecs) Byte1 (pCmd[5]);
#if 0
   if (PcmPkt->InSerialPortId == SerialPortNull) PcmPkt->Coding = NullCodec;
   if (PcmPkt->Coding == NullCodec) PcmPkt->InSerialPortId = SerialPortNull;
#endif
   
   if (!ValidFrameSize (PcmPkt->SamplesPerFrame))
      return Cc_InvalidPktOutSizeB;
   
   if (!ValidCoding (PcmPkt->Coding, sampsToHalfMS (PcmPkt->SamplesPerFrame)))
      return Cc_InvalidPktOutCodingB;
      
   PcmPkt->OutSerialPortId = SerialPortNull;
   PcmPkt->OutSlotId_MuxFact = 0;

   PktPcm->OutSerialPortId = (GpakSerialPort_t) UnpackPort (pCmd[3]);
   PktPcm->OutSlotId =                          UnpackSlot (pCmd[3]);
   PktPcm->Coding    =             (GpakCodecs) Byte1 (pCmd[4]);
#if 0
   if (PktPcm->OutSerialPortId == SerialPortNull) PktPcm->Coding = NullCodec;
   if (PktPcm->Coding == NullCodec) PktPcm->OutSerialPortId = SerialPortNull;
#endif

   PktPcm->InSerialPortId = SerialPortNull;
   PktPcm->InSlotId_MuxFact = 0;
   
   if (!ValidFrameSize (PktPcm->SamplesPerFrame))
      return Cc_InvalidPktInSizeB;

   if (!ValidCoding (PktPcm->Coding, sampsToHalfMS (PktPcm->SamplesPerFrame)))
      return Cc_InvalidPktInCodingB;

   //
   // Data channel. All algorithm processing is ignored.
   //
   if ((pCmd[6] & 0x0400) == 0) {
      chan->VoiceChannel = Disabled;
      if ((PktPcm->Coding != L8) && (PktPcm->Coding != L16))
         return Cc_InvalidPktInCodingB;

      if ((PcmPkt->Coding != L8) && (PcmPkt->Coding != L16))
         return Cc_InvalidPktOutCodingB;

      return Cc_Success;
   } 
   chan->VoiceChannel = Enabled;

   //
   // Select acoustic or PCM echo canceller giving priority to acoustic echo canceller
   //
   if ((pCmd[6] & 0x0800) == 0) {                // Acoustic Echo cancellation
      PcmPkt->AECEchoCancel = Disabled;
   } else if (NumAECEcansUsed < sysConfig.AECInstances) {
      // Acoustic echo canceller selected. 
      pCmd[6] &= 0x7fff;                 // Turn off request for PCM canceller.
      if (!CheckAecMemory(PcmPkt->SamplesPerFrame, chan->ProcessRate))
         return Cc_InsuffBuffsAvail;
      PcmPkt->AECEchoCancel = Enabled;

      // Ensure that the TDM in and TDM out rates are the same when AEC is turned on
      if (PktPcm->SamplesPerFrame != PcmPkt->SamplesPerFrame)
           return Cc_InvalidPktOutSizeB;

   } else if ((pCmd[6] & 0x8000) == 0) 
      return Cc_AECEchoCancelNotCfg;     // Acoustic and PCM echo cancellers not available


   if ((pCmd[6] & 0x8000) == 0)   {             // PCM Echo cancellation
      PcmPkt->EchoCancel = Disabled;
   } else if (NumPcmEcansUsed < sysConfig.numPcmEcans) {
      PcmPkt->EchoCancel = Enabled;
   } else
      return Cc_PcmEchoCancelNotCfg;

   PktPcm->AECEchoCancel = Disabled;
   if ((pCmd[6] & 0x4000) == 0)   {             // Pkt Echo cancellation
      PktPcm->EchoCancel = Disabled;
   } else if (NumPktEcansUsed < sysConfig.numPktEcans) {
      PktPcm->EchoCancel = Enabled;
   } else
      return Cc_PktEchoCancelNotCfg;

   //
   // VAD / Comfort noise
   //
   if ((pCmd[6] & 0x2000) != 0)   {
      PcmPkt->VAD = SetVADMode (PcmPkt->Coding);
      if (PcmPkt->VAD == VadDisabled)
         return Cc_VadNotConfigured;
      if (sysConfig.VadReport == Enabled)
        PcmPkt->VAD |= VadNotifyEnabled;
   } else
      PcmPkt->VAD = VadDisabled;

   PktPcm->ForwardCNGPkts = Disabled;
   PktPcm->CNG = SetVADMode (PktPcm->Coding);

   if ((pCmd[6] & 0x0080) != 0)   {
      PktPcm->VAD = (GpakVADMode)VadCngEnabled; //SetVADMode (PcmPkt->Coding);
      //if (PktPcm->VAD == VadDisabled)
      //   return Cc_VadNotConfigured;
      if (sysConfig.VadReport == Enabled)
        PktPcm->VAD |= VadNotifyEnabled;
      PktPcm->PrevVadState = VAD_INIT_RELEASE;
   } else
      PktPcm->VAD = VadDisabled;
   //
   //  AGC
   //
   if ((pCmd[6] & 0x1000) == 0)
        PcmPkt->AGC = Disabled;
   else if (numAGCChansAvail == 0)
        return Cc_InsuffAgcResourcesAvail;
   else
        PcmPkt->AGC = Enabled;

   if ((pCmd[6] & 0x0100) == 0)
        PktPcm->AGC = Disabled;
   else if (numAGCChansAvail == 0)
        return Cc_InsuffAgcResourcesAvail;
   else
        PktPcm->AGC = Enabled;
   //
   // DTX
   //
   if ((pCmd[6] & 0x200) != 0) {
        if ((PcmPkt->VAD & VadMask) != VadCngEnabled)
            return Cc_VadNotConfigured;
        else {
            PcmPkt->DtxEnable = Enabled;
            PcmPkt->PrevVadState = VAD_INIT_RELEASE;
        }
   } else {
        PcmPkt->DtxEnable = Disabled;
   }

   //
   // Tone detection/generation paramters
   //
   // detection only on tdm to packet side.
   // Regular Gpak generation only on packet to tdm side.
   // Trilogy asks for tone gen on the tdm to packet side.
   // Cornet asks for Tone Det packet to tdm side for C66x build
   PcmPkt->tonePktGet = 0;
   PktPcm->tonePktPut = 0;
   PktPcm->ForwardTonePkts = Disabled;

   toneTypes = (GpakToneTypes) (pCmd[8] & 0xFFF);
   PcmPkt->toneTypes = toneTypes & (~(Tone_Regen));

   mfTones = cedTones = cngTones = arbTones = 0;
   toneStatus = ValidToneTypes (toneTypes, &mfTones, &cedTones, &cngTones, &arbTones);
   if (toneStatus != Cc_Success) return toneStatus;

   if ((toneTypes & Tone_Generate) && (numToneGenChansAvail == 0))
        return Cc_InsuffTGResourcesAvail;

   if ((toneTypes & ToneDetect) && (ToneDetectorsAvail == 0))
        return Cc_InsuffTDResourcesAvail;

   toneTypesPktPcm = (GpakToneTypes) (pCmd[13] & 0xFFF);
   PktPcm->toneTypes = toneTypesPktPcm & (Tone_Generate | Tone_Regen | Notify_Host | ToneDetect);
   //mfTones = cedTones = cngTones = arbTones = 0;
   toneStatus = ValidToneTypes (toneTypes, &mfTones, &cedTones, &cngTones, &arbTones);
   if (toneStatus != Cc_Success) return toneStatus;

   if ((toneTypes & Tone_Generate) && (numToneGenChansAvail == 0))
        return Cc_InsuffTGResourcesAvail;

   if ((toneTypes & ToneDetect) && (ToneDetectorsAvail == 0))
        return Cc_InsuffTDResourcesAvail;
   //
   //  T.38 FAX parameters
   //
   chan->fax.faxMode = disabled;
   faxMode = (GpakFaxMode_t)((pCmd[8] >> 14) & 0x0003);
   chan->fax.faxTransport = (GpakFaxTransport_t)((pCmd[8] >> 12) & 3);
   if (faxMode != disabled)   { 
      if (sysConfig.numFaxChans <= numFaxChansUsed)
         return Cc_FaxRelayNotConfigured;

      if ((faxMode != faxOnly) && (faxMode != faxVoice))
         return Cc_FaxRelayInvalidMode;

      if (chan->fax.faxTransport != faxRtp) 
         return Cc_FaxRelayInvalidTransport;

      chan->fax.faxMode = faxMode;

   }
   if (CheckIfBuffsAvail (PktPcm->EchoCancel) == 0)
        return Cc_InsuffBuffsAvail;

   if (ApiBlock.ApiVersionId <= 0x0401u) return Cc_Success;

   //----------------------------------------------------------
   // API Version 402 and better.
   //   CID and gains
   if ((pCmd[9] & CidReceive) && (numRxCidChansAvail == 0))  return Cc_RxCIDNotAvail;
   if ((pCmd[9] & CidTransmit) && (numTxCidChansAvail == 0)) return Cc_TxCIDNotAvail;

   PcmPkt->CIDMode = CidDisabled;
   PktPcm->CIDMode = CidDisabled;
   if (pCmd[9] & CidReceive)  PcmPkt->CIDMode |= CidReceive;

   if (pCmd[9] & CidTransmit) PktPcm->CIDMode |= CidTransmit;

   PcmPkt->Gain.ToneGenGainG1  = pCmd[10];
   PcmPkt->Gain.OutputGainG2   = pCmd[11];
   PcmPkt->Gain.InputGainG3    = pCmd[12];
   ClampScaleGain(&PcmPkt->Gain);

   PktPcm->Gain.ToneGenGainG1  = PcmPkt->Gain.ToneGenGainG1;
   PktPcm->Gain.OutputGainG2   = PcmPkt->Gain.OutputGainG2; 
   PktPcm->Gain.InputGainG3    = PcmPkt->Gain.InputGainG3;         

   // Return with an indication of success.
   return Cc_Success;

}

#pragma CODE_SECTION (customProcChanStatMsg, "SLOW_PROG_SECT")
ADT_UInt16 customProcChanStatMsg (ADT_UInt16 *pReply, chanInfo_t *chan) { 
   // Called by messaging to obtain a custom channel status reply
   pkt2pcm_t *PktPcm;
   pcm2pkt_t *PcmPkt;
   ADT_UInt16 ATone;
   int VAD;
   

   if (chan->pcmToPkt.VAD & VadEnabled)
      VAD = 1;
   else 
      VAD = 0;

   PcmPkt = &(chan->pcmToPkt);
   PktPcm = &(chan->pktToPcm);

   pReply[7] = PackPortandSlot (PcmPkt->InSerialPortId, PcmPkt->InSlotId);
   pReply[8] = PackPortandSlot (PktPcm->OutSerialPortId, PktPcm->OutSlotId);
   if (ApiBlock.ApiVersionId < 0x0603u) {
      pReply[9] = (ADT_UInt16) PackBytes (PktPcm->Coding,  PktPcm->SamplesPerFrame);
      pReply[10] = (ADT_UInt16) PackBytes (PcmPkt->Coding, PcmPkt->SamplesPerFrame);
   } else {
      pReply[9] = (ADT_UInt16) PackBytes (PktPcm->Coding,  sampsToHalfMS (PktPcm->SamplesPerFrame));
      pReply[10] = (ADT_UInt16) PackBytes (PcmPkt->Coding, sampsToHalfMS (PcmPkt->SamplesPerFrame));
   }

   ATone    = PcmPkt->toneTypes | PktPcm->toneTypes;
   pReply[11] = (ADT_UInt16)
      ((PcmPkt->EchoCancel << 15)           | ((PktPcm->EchoCancel << 14) & 0x4000) |
       ((VAD               << 13) & 0x2000) | ((PcmPkt->AGC        << 12) & 0x1000) |
       ((PcmPkt->AECEchoCancel << 11) & 0x0800) | 
       ((chan->VoiceChannel << 10) & 0x0400) | ((PcmPkt->DtxEnable << 9) & 0x0200));

   pReply[12] = 0;  // Obsolete pin IDs
   pReply[13] = (ADT_UInt16) ((chan->fax.faxMode << 14) | 
                           ((chan->fax.faxTransport << 12) & 0x3000) | (ATone & 0x0FFF));

   if (ApiBlock.ApiVersionId <= 0x0401u) return 14;

   //----------------------------------------------------------
   // API Version 402 and better.
   pReply[14] = (ADT_UInt16) ((PcmPkt->CIDMode & CidReceive) | (PktPcm->CIDMode & CidTransmit)) ;
   if (PktPcm->Gain.ToneGenGainG1 != (ADT_UInt16)ADT_GAIN_MUTE)
        pReply[15] = (ADT_Int16) PktPcm->Gain.ToneGenGainG1/10;
   else
        pReply[15] = (ADT_Int16) PktPcm->Gain.ToneGenGainG1;

   if (PktPcm->Gain.OutputGainG2 != (ADT_UInt16)ADT_GAIN_MUTE)
        pReply[16] = (ADT_Int16) PktPcm->Gain.OutputGainG2/10; 
   else 
        pReply[16] = (ADT_Int16) PktPcm->Gain.OutputGainG2; 

   if (PcmPkt->Gain.InputGainG3 != (ADT_UInt16)ADT_GAIN_MUTE)
        pReply[17] = (ADT_Int16) PcmPkt->Gain.InputGainG3/10;  
   else
        pReply[17] = (ADT_Int16) PcmPkt->Gain.InputGainG3;  
   pReply[18] = (ADT_UInt16) chan->fromHostPktCount;
   pReply[19] = (ADT_UInt16) chan->toHostPktCount;

   return 20;
}

#pragma CODE_SECTION (customSetupChan, "SLOW_PROG_SECT")
void customSetupChan (chanInfo_t *chan) {
   // Called by messaging to allocate and initialize channel structures
   pcm2pkt_t *PcmPkt;   // pointer to channel's PCM to Packet data
   pkt2pcm_t *PktPcm;   // pointer to channel's Packet to PCM data

   // Get pointers to the channel's A and B Sides
   PcmPkt = &chan->pcmToPkt;
   PktPcm = &chan->pktToPcm;

   // Modes that force narrow band processing
   if (PcmPkt->CIDMode || PcmPkt->EchoCancel ||
       PktPcm->CIDMode || PktPcm->EchoCancel ||
      chan->fax.faxMode)
      chan->ProcessRate = 8000;


   // Allocate Pcm, Pkt and BulkDelay Buffers
   AllocBuffs (chan);

   // Initialize PCM buffers
   PcmPkt->activeInBuffer = &PcmPkt->inbuffer;
   memset (PcmPkt->inbuffer.pBufrBase,  0, (PcmPkt->inbuffer.BufrSize)*sizeof(ADT_PCM16));

   // tonePkt put/get pointers not used in pcm/pkt mode
   PcmPkt->tonePktGet = 0;
   PktPcm->tonePktPut = 0;
   PktPcm->ForwardTonePkts = Disabled;
   PktPcm->ForwardCNGPkts = Disabled;

   memset (PktPcm->outbuffer.pBufrBase, 0, (PktPcm->outbuffer.BufrSize)*sizeof(ADT_PCM16));

   // Initialize the PCM Echo Canceller if enabled.
   PcmPkt->ecFarPtr.SlipSamps = 0;
   PcmPkt->ecBulkDelay.SlipSamps = 0;
   if (PcmPkt->AECEchoCancel == Enabled) {
      PcmPkt->pG168Chan =
         AllocEchoCanceller (ACOUSTIC , PcmPkt->SamplesPerFrame, &PcmPkt->EcIndex, chan->ProcessRate);
      if (PcmPkt->pG168Chan == NULL)
      {
	     PcmPkt->AECEchoCancel = Disabled;
      }
      else
      {
         PcmPkt->ecFarPtr.pBufrBase = pAECFarPool[PcmPkt->EcIndex & 0xFF];
         PcmPkt->ecFarPtr.BufrSize  = PcmPkt->SamplesPerFrame;
         memset (PcmPkt->ecFarPtr.pBufrBase, 0,  PcmPkt->SamplesPerFrame * sizeof (ADT_PCM16));
      }

   } else if (PcmPkt->EchoCancel == Enabled) {
      PcmPkt->ecFarPtr.pBufrBase = PktPcm->outbuffer.pBufrBase;
      PcmPkt->ecFarPtr.BufrSize  = PktPcm->outbuffer.BufrSize;
      PcmPkt->EcIndex = chan->CoreID;
      PcmPkt->pG168Chan =
         AllocEchoCanceller (G168_PCM, PcmPkt->SamplesPerFrame, &PcmPkt->EcIndex, chan->ProcessRate);
   }

   // Initialize the Packet Echo Canceller if enabled.
   if (PktPcm->EchoCancel == Enabled) {
      memset (PcmPkt->ecBulkDelay.pBufrBase, 0,
             (PcmPkt->ecBulkDelay.BufrSize)*sizeof(ADT_UInt16));
      
      PktPcm->ecFarPtr.pBufrBase = PcmPkt->ecBulkDelay.pBufrBase;
      PktPcm->ecFarPtr.BufrSize  = PcmPkt->ecBulkDelay.BufrSize;
      PktPcm->EcIndex = chan->CoreID;
      PktPcm->pG168Chan =
         AllocEchoCanceller (G168_PKT, PktPcm->SamplesPerFrame, &PktPcm->EcIndex, chan->ProcessRate);
   }


   // Initialize tone detection and tone relay instances
   if ((PcmPkt->toneTypes & ToneDetectAll) != Null_tone) {
      InitToneDetection (PcmPkt->toneTypes,   &PcmPkt->TDInstances, 
                         PcmPkt->toneRelayPtr, PcmPkt->SamplesPerFrame);
   }
   if ((PktPcm->toneTypes & ToneDetectAll) != Null_tone) {
      InitToneDetection (PktPcm->toneTypes,   &PktPcm->TDInstances,
                         0, PktPcm->SamplesPerFrame);
   }

   // Initialize VAD and Comfort noise generation
   if ((PcmPkt->VAD & VadCng) || (PktPcm->CNG & VadCng)) {
      InitVadCng (PcmPkt->vadPtr, &PcmPkt->VadParms, PcmPkt->SamplesPerFrame, PktPcm->SamplesPerFrame, chan->ProcessRate);
   }
   // Initialize VAD
   if (PktPcm->VAD & VadCng) {
      InitVadCng (PktPcm->vadPtr, &PktPcm->VadParms, PcmPkt->SamplesPerFrame, PktPcm->SamplesPerFrame, chan->ProcessRate);
   }

   // Allocate AGC and initialize instances
   AllocAGC (chan, ADevice, PcmPkt->AGC);
   AllocAGC (chan, BDevice, PktPcm->AGC);

   // Allocate tone generation instances
   // Trilogy's request for tone gen on Pkt side
   AllocToneGen (&PcmPkt->TGInfo, PcmPkt->toneTypes); //AllocToneGen (&PcmPkt->TGInfo, 0);
   AllocToneGen (&PktPcm->TGInfo, PktPcm->toneTypes);

   // Initialize tone relay generation instance.
   if (PktPcm->TGInfo.toneRlyGenPtr) {
      InitToneRelayGen (&PktPcm->TGInfo);
   }

   // Allocate and initialize T.38 fax relay
   AllocFaxRelay (chan);

   // Initialize vocoders
   initEncoder (PcmPkt, 0, chan->ProcessRate);
   initDecoder (PktPcm, 0, chan->ProcessRate);

   // Initialize Caller ID
   NullCID (&(PcmPkt->cidInfo));
   NullCID (&(PktPcm->cidInfo));
   AllocCID (chan, ADevice, PcmPkt->CIDMode, PktPcm->CIDMode);
   
   // Initialize DMA info structures
   dmaInitInfoStruct (chan, PcmPkt->EcIndex, PktPcm->EcIndex);

   // Allocate and initialize sampling rate converter instances
   AllocEncoderSampleRateConv (chan);
   AllocDecoderSampleRateConv (chan);

   return;
}

#pragma CODE_SECTION (customFrameDecode, "SLOW_PROG_SECT")
void customFrameDecode (chanInfo_t *chan, void  *pktBuff, ADT_PCM16 *pcmBuff,
                      ADT_PCM16 *ecFarWork, int SampsPerFrame, int pcmBuffI8) {
   // Called by framing to decode a vocoder payload

   GpakPayloadClass pyldclass;      // packet's class
   pkt2pcm_t *PktPcm = &chan->pktToPcm;
   pcm2pkt_t *PcmPkt = &chan->pcmToPkt;
   //CircBufInfo_t pcmAInfo;
   //ADT_PCM16 * pktBuffST;
   short *DTMFBuff;     // pointer to PCM data for DTMF processing

   int sampsPerFrame, paramsI8;

   logTime (0x400B0000ul | chan->ChannelId);
   if (PktPcm->Coding != NullCodec) {
      // Read packet from host interface into pktBuff using pcmBuff as scratch for unpacking
      paramsI8 = pcmBuffI8;
      pyldclass = parsePacket (chan, pktBuff, &paramsI8, pcmBuff, pcmBuffI8);
   } else {
      memset (pcmBuff, 0, SampsPerFrame * sizeof(ADT_UInt16));
      pyldclass = PayClassNone;
   }

   // When T38 Fax relay is in progess PCM output will be generated by the PCM to packet processing thread.
   if (chan->fax.t38InProgress)  {
      logTime (0x400B0F00ul | chan->ChannelId);

     if(chan->FrameCount <= 1)
     {
         // Determine sample count for this channel
         if (chan->ProcessRate < TDMRate)
             sampsPerFrame = PktPcm->SamplesPerFrame / (TDMRate / chan->ProcessRate);
         else
             sampsPerFrame = PktPcm->SamplesPerFrame * (chan->ProcessRate / TDMRate);

         memset (pcmBuff, 0, sampsPerFrame * sizeof (ADT_UInt16));
         copyLinearToCirc (pcmBuff, chan->pcmToPkt.activeInBuffer, sampsPerFrame);
     }

      return;
   }

   // Decode packets (pktBuff) by playload class and type into PCM (pcmBuff) samples.
   sampsPerFrame = DecodePayload (chan, PktPcm, pyldclass, pktBuff, paramsI8, pcmBuff, pcmBuffI8);
   AppErr ("Buffer size error", SampsPerFrame < sampsPerFrame);

   DTMFBuff = (short *) pktBuff;
   i16cpy (DTMFBuff, pcmBuff, SampsPerFrame);
   ProcessVadToneOnDecodePath(chan, pcmBuff, DTMFBuff, SampsPerFrame);
   
   ToneGenerate (&PktPcm->TGInfo, pcmBuff, sampsPerFrame);

   // Apply Tonegen Gain G1 when generator is active
   if (PktPcm->TGInfo.active && PktPcm->Gain.ToneGenGainG1)
      ADT_GainBlockWrapper (PktPcm->Gain.ToneGenGainG1, pcmBuff, pcmBuff, sampsPerFrame);

   // Apply Pcm Output Gain G2
   if (PktPcm->Gain.OutputGainG2)
      ADT_GainBlockWrapper (PktPcm->Gain.OutputGainG2, pcmBuff, pcmBuff, sampsPerFrame);

   // Apply voice play back
   if (PktPcm->playA.state & PlayBack)
      voicePlayback (&PktPcm->playA, pcmBuff, ecFarWork, sampsPerFrame, ADevice, chan->ChannelId);

   waveFilePlayback (&(PktPcm->waveInfo), pcmBuff, sampsPerFrame); 

   // playback B-buffer capture or vector to PCM output
   play_record(chan->ChannelId, pcmBuff, SampsPerFrame, (PLAYREC_B | PLAYREC_PLAY));

   if (PktPcm->AGC == Enabled) {
      // Perform AGC on pcm buffer
      PktPcm->AGCPower = AGC_ADT_run (PktPcm->agcPtr, (short *) pcmBuff, SampsPerFrame, 0, 0, (short *) pcmBuff, NULL);
   }

   // copy the linear pcm buffer into the circular pcm buffer
   if (Enabled == PcmPkt->AECEchoCancel) {
      // Store pcm for output by PCM to PKT side
      i16cpy (PcmPkt->ecFarPtr.pBufrBase, pcmBuff, sampsPerFrame);
   } else {
      ecSigTrace (PktPcm->OutSerialPortId, PktPcm->OutSlotId, 1, (ADT_Int16 *)pcmBuff, NULL, sampsPerFrame);
#ifdef TDM_INTERFACE_AVAIL
      sampsPerFrame = toTDMCirc (chan, BDevice, pcmBuff, sampsPerFrame, &PktPcm->rateCvt);
#else
     // copy directly into pcmout buffer so samples can be accessed by ECFar 
     copyLinearToCirc (pcmBuff,  &chan->pktToPcm.outbuffer, sampsPerFrame);

     // read samples to simulate DMACopy read
     copyCircToLinear (&chan->pktToPcm.outbuffer, ecFarWork, sampsPerFrame);

     // write directly into encode pcm input circ buffer for tdm loopback
     copyLinearToCirc (pcmBuff, chan->pcmToPkt.activeInBuffer, sampsPerFrame);
#endif
   }

   if (PktPcm->recB.state & Record) {
      voiceRecord (&PktPcm->recB, pcmBuff, ecFarWork, sampsPerFrame, BDevice, chan->ChannelId);
   }

   logTime (0x400B0F00ul | chan->ChannelId);
   return;

}

#pragma CODE_SECTION (customFrameEncode, "SLOW_PROG_SECT")
void customFrameEncode (chanInfo_t *chan, ADT_PCM16 *pcmBuff, void *pktBuff,
                     ADT_PCM16 *ecFarWork, int SampsPerFrameTDM, int pktBuffBytes) {
    // Called by framing to encode a vocoder payload
   short *DTMFBuff;     // pointer to PCM data for DTMF processing

   pcm2pkt_t *PcmPkt = &chan->pcmToPkt;
   pkt2pcm_t *PktPcm = &chan->pktToPcm;
#ifdef TDM_INTERFACE_AVAIL
   int slipCnt;
#endif
   int SampsPerFrame;

   SampsPerFrame = SampsPerFrameTDM;

  logTime (0x400A0000ul | chan->ChannelId);

#ifdef TDM_INTERFACE_AVAIL
   // copy the input pcm data from circular buffer to linear work buffer
   slipCnt = fromTDMCirc (chan, ADevice, pcmBuff, &SampsPerFrame, &PcmPkt->rateCvt);
#else
     // read directly from encode pcm input circ buffer
   copyCircToLinear (chan->pcmToPkt.activeInBuffer, pcmBuff, SampsPerFrame);
#endif

   // playback A-buffer capture or vector to PCM input
   play_record(chan->ChannelId, pcmBuff, SampsPerFrame, (PLAYREC_A | PLAYREC_PLAY));

   if (Enabled == PcmPkt->AECEchoCancel) {
      DTMFBuff = (short *) pcmBuff;
      runAEC (chan, pcmBuff, ecFarWork, SampsPerFrame, &PktPcm->rateCvt);
   } else if (Enabled == PcmPkt->EchoCancel) {

      // Perform pcm echo cancellation; DTMF contains pre-NLP for DTMF detection.
      DTMFBuff = (short *) pktBuff;

      // Perform echo cancellation on pcmBuff, Obtain far data from far end's TDM output buffer.   Todo.  DownConversion
      SampsPerFrame = SampsPerFrameTDM;
#ifdef TDM_INTERFACE_AVAIL
      fromTDMCirc (chan, ASideECFar, ecFarWork, &SampsPerFrame, NULL);
#else
      copyCircToLinear (&(chan->pcmToPkt.ecFarPtr), ecFarWork, SampsPerFrame);
#endif
      ecSigTrace (PcmPkt->InSerialPortId, PcmPkt->InSlotId, 4, (ADT_Int16 *)pcmBuff, (ADT_Int16  *)ecFarWork, SampsPerFrame);
      g168Capture (PcmPkt->EcIndex, pcmBuff, ecFarWork, SampsPerFrame);
      LEC_ADT_g168Cancel (PcmPkt->pG168Chan, (short *) pcmBuff, (short *) ecFarWork, DTMFBuff);
      g168PostCapture (PcmPkt->EcIndex, pcmBuff, ecFarWork, SampsPerFrame);

   } else { 
      DTMFBuff = (short *) pktBuff;
      i16cpy (DTMFBuff, pcmBuff, SampsPerFrame);
   }

   if (chan->fax.faxMode != disabled) {
      // call the relay PCM interface
      logTime (0x400A1200ul | chan->ChannelId);
      processFaxRelay (chan, pcmBuff, ecFarWork, SampsPerFrame);

      // skip the remaining processing if fax relay is active
      if (chan->fax.t38InProgress)
         return;
   }


   // Apply Pcm Input Gain G3
   if (PcmPkt->Gain.InputGainG3)
      ADT_GainBlockWrapper (PcmPkt->Gain.InputGainG3, pcmBuff, pcmBuff, SampsPerFrame);

   if (PcmPkt->AGC == Enabled) {
      // Perform AGC on pcm buffer
      logTime (0x400A0500ul | chan->ChannelId);
      PcmPkt->AGCPower = AGC_ADT_run (PcmPkt->agcPtr, (short *) pcmBuff, SampsPerFrame, 0, 0, (short *) pcmBuff, NULL);
   }
   
   // copy pcm samples into scratch buffer ecFarWork for voice recording
   if (PcmPkt->recA.state & Record) {
      i16cpy (ecFarWork, pcmBuff, SampsPerFrame);
      voiceRecord (&PcmPkt->recA, ecFarWork, pktBuff, SampsPerFrame, ADevice, chan->ChannelId);
   }

   if (PcmPkt->playB.state & PlayBack)
      voicePlayback (&PcmPkt->playB, pcmBuff, ecFarWork, SampsPerFrame, BDevice, chan->ChannelId);

   waveFilePlayback (&(PcmPkt->waveInfo), pcmBuff, SampsPerFrame); 

   // Store pcm data for far end echo canceller in bulk delay buffer.
   if (Enabled == chan->pktToPcm.EchoCancel) {
      logTime (0x400A0B00ul | chan->ChannelId);
#ifdef TDM_INTERFACE_AVAIL
      copyLinearToCirc (pcmBuff, &chan->pktToPcm.ecFarPtr, SampsPerFrame);
#endif
   }

   // generate tones if enabled
   ToneGenerate (&PcmPkt->TGInfo, pcmBuff, SampsPerFrame);
   
   // Perform VAD, Tone Detection, and Encode functions.
   ProcessVadToneEncode (chan, (ADT_PCM16 *) pcmBuff, DTMFBuff, pktBuff, SampsPerFrame);
   
   logTime (0x400A0F00ul | chan->ChannelId);
   return;
}
extern void setPcmPktPointers (PhaseData *Near, PhaseData *Far);
extern void setPktPcmPointers (PhaseData *Near, PhaseData *Far);

#pragma CODE_SECTION (customSetDevicePointers, "SLOW_PROG_SECT")
void customSetDevicePointers (chanInfo_t *chan, 
                      pcm2pkt_t *PcmPkt,  pkt2pcm_t *PktPcm, 
                      ADT_UInt16 APhase,    ADT_UInt16 BPhase) {

   PhaseData DevA, DevB;
//   ADT_UInt16 rxDelay, txDelay;
//   ConferenceInfo_t *gpakCnfr;

   DevA.Phase   = APhase;
   DevA.sampsPerFrame = PcmPkt->SamplesPerFrame;
   DevA.inBuff  = PcmPkt->activeInBuffer;
   DevA.outBuff = &PktPcm->outbuffer;
   DevA.rxDelay = 0;
   DevA.txDelay = 0;
   DevA.txCompand = PktPcm->OutCompandingMode;
   DevA.rxCompand = PcmPkt->InCompandingMode;
   DevA.ecBuff  = &PcmPkt->ecFarPtr;

   DevB.Phase   = BPhase;
   DevB.sampsPerFrame = PktPcm->SamplesPerFrame;
   DevB.inBuff  = PktPcm->inbuffer;
   DevB.outBuff = PcmPkt->outbuffer;
   DevB.ecBuff  = &PktPcm->ecFarPtr;
   DevB.rxDelay = 0;
   DevB.txDelay = 0;
   DevB.txCompand = PcmPkt->OutCompandingMode;
   DevB.rxCompand = PktPcm->InCompandingMode;

   setPcmPktPointers (&DevA, &DevB);
   setPktPcmPointers (&DevB, &DevA);

}

#pragma CODE_SECTION (customGetPlaybackInstances, "SLOW_PROG_SECT")
void customGetPlaybackInstances (chanInfo_t *chan, GpakDeviceSide_t deviceSide,
                                 playRecInfo_t **playInfo, playRecInfo_t **recInfo,
                                 ADT_UInt16 *samplesPerFrame) {
   // Called by framing obtain a custom channel's playback and record instances
   if (deviceSide == ADevice) {
      *playInfo = &chan->pktToPcm.playA;
      *recInfo  = &chan->pcmToPkt.recA;
   } else if (deviceSide == BDevice) {
      *playInfo = &chan->pcmToPkt.playB;
      *recInfo  = &chan->pktToPcm.recB;
   } else {
       *playInfo = NULL;
       *recInfo = NULL;
   }
   *samplesPerFrame = 0;
}

#pragma CODE_SECTION (customProcessConference, "SLOW_PROG_SECT")
void customProcessConference (int cnfrID, ConferenceInfo_t *Cnfr, ConfInstance_t *CnfrInst,
                       ADT_Int16 *pWork1, void *scratch) {
   // Called by framing to process a custom conference
}

#pragma CODE_SECTION (customTeardown, "SLOW_PROG_SECT")
void customTeardown  (chanInfo_t *chan) { 
   // Called by messaging to deallocate channel instance memory
   return; 
}

#pragma CODE_SECTION (customInvalidateChannelCache, "MEDIUM_PROG_SECT")
void customInvalidateChannelCache  (chanInfo_t *chan) { 
   // Called by framing to invalidate custom instance data allocated to the channel
   return; 
}
