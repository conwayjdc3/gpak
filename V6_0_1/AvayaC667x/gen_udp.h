#ifndef _GEN_UDP_H
#define _GEN_UDP_H

//#define USE_GENERAL_TX_SOCKET	// enable use of general UDP send socket

// ----------- debug for general udp socket transmit
typedef struct gen_udp_t {
    int enabled;   // if == -1, transmit forever, else if > == transmit x packets
    int send;
    int pktlenI8;
    int periodMs;
    int countdown;
    int numPorts;
    ADT_UInt16 dest_port; 
    ADT_UInt32 dest_ip;
    ADT_UInt8 data;
} gen_udp_t;
extern gen_udp_t gen_udp;

#ifdef  USE_GENERAL_TX_SOCKET	 
extern void gen_udp_timer();
#else
#define gen_udp_timer() 
#endif


#endif

