/******************************************************************************
 * Copyright (c) 2011-2012 Texas Instruments Incorporated - http://www.ti.com
 *
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *****************************************************************************/

//#include <cerrno>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <ti\platform\platform.h>
#include <ti\platform\resource_mgr.h>
#include <ti\csl\csl_bootcfgAux.h>
#include "avayac6678.h"
#include <ti\platform\evmc6678l\platform_lib\include\evmc66x_gpio.h>
#include <ti\platform\evmc6678l\platform_lib\include\evmc66x_uart.h>
#include <ipStackUser.h>
//#include <ti/ndk/inc/ipStackUser.h>
#include <ti/ndk/inc/usertype.h>
#ifdef _INCLUDE_IPv6_CODE
extern int IPv6StringToIPAddress (char* StringIP, IP6N *address);
#endif

extern int GpakReadLinkReports(uint32_t *LinkEvent, uint32_t *LinkStatus);


extern IPv6Info_t IPv6Info;

#define DEF_INIT_CONFIG_UART_BAUDRATE  115200
#define MAX_MESSAGE_LEN 64
#define SCAN_IS_SPACE(c)		((c)==' ' || (c)=='\t')
#define IS_A_NUMBER(c) ((c) >= 48 && (c) <= 57)

uint8_t ip6_addr_string[48];
IP6N    ip6_address;
IPN     ip_address;
IPN     gwip_address;
IPN     netmask;
int network_prefix;
uint8_t ip_addr[16];
uint8_t gwip_addr[16];
uint8_t net_mask[16];
uint8_t compandingMode[2];
uint16_t numSlotsOnStream[2];
uint16_t maxSlotsSupported[2];
uint16_t serialWordSize[2];
uint16_t dmaBufferSize[2];
uint8_t local_mac[6];
int mcbsp_loopback = 0;
extern const char *GPAK_BUILD_VERSION;
IPN *pIPAddr = (IPN *)&IPConfig.IPAddr;
IPN *pGWIPAddr = (IPN *)&IPConfig.GateWayIP;
IPN *pNetMask = (IPN *)&IPConfig.IPMask;
IPStackCfg *ipConfig = (IPStackCfg *) &IPConfig;
static int key_was_hit = 0;

extern uint32_t    inet_addr(char * );

static void Platform_uart_set_baudrate(uint32_t baudrate, uint32_t uart_input_clock_freq) {

	uint16_t brate;

    brate = ((Uint16) (uart_input_clock_freq/(baudrate * 16)));

	UartSetBaudRate(brate);

}


static int parse_cmd(uint8_t *Cmd) {
int i;
uint8_t c;
uint8_t *cmd = Cmd;
uint8_t port;
int dot_count, prev_dot, delta_dot;
#ifdef _INCLUDE_IPv6_CODE
int v6 = 0;
#endif


    while (SCAN_IS_SPACE(*cmd)) {
	    ++cmd;
    }
    c = toupper(*cmd);
    if (c == 'I') 
    {   
        // read IP address
        cmd++;
        c = toupper(*cmd);
        if (c != 'P') return -1; 

        cmd++;
#ifdef _INCLUDE_IPv6_CODE
        if (*cmd == '6') { 
            v6 = 1;
            cmd++;
        }
#endif
        while (SCAN_IS_SPACE(*cmd)) {
	        ++cmd;
        }
        if (*cmd != '=') return -1;

        cmd++;
        while (SCAN_IS_SPACE(*cmd)) {
	        ++cmd;
        }
#ifdef _INCLUDE_IPv6_CODE
        if (v6) {
            if (IPv6StringToIPAddress ((char *)cmd, &ip6_address) < 0) {
                return -1;
            }
            memset (ip6_addr_string, 0, sizeof(ip6_addr_string));
            sprintf((char *)ip6_addr_string, "%s",cmd);
        } else 
#endif
        {
            // IPv4
            dot_count = 0;
            prev_dot = -1;     
            for (i=0; i<16; i++) {
                if (cmd[i] == '.') {
                    delta_dot = i-prev_dot;
                    prev_dot = i;
                    dot_count++;
                    if ((delta_dot < 2) || (delta_dot > 4) || (dot_count > 3)) {
                        return -1;
                    } 
                }
                else if ((cmd[i] == ';') || (cmd[i] == 0)) {
                  ip_addr[i] = 0;
                  if (dot_count != 3)
                      return -1;
                  break;
                } else if (IS_A_NUMBER(cmd[i]) == 0) {
                    return -1;
                }
                ip_addr[i] = cmd[i];
            }
            ip_address = inet_addr((char *)ip_addr);
        }
        return 0;
    } 

    if (c == 'G') 
    {   
        // read Gateway IP address
        cmd++;
        c = toupper(*cmd);
        if (c != 'W') return -1; 
        cmd++;
        c = toupper(*cmd);
        if (c != 'I') return -1; 
        cmd++;
        c = toupper(*cmd);
        if (c != 'P') return -1; 

        cmd++;
        while (SCAN_IS_SPACE(*cmd)) {
	        ++cmd;
        }
        if (*cmd != '=') return -1;

        cmd++;
        while (SCAN_IS_SPACE(*cmd)) {
	        ++cmd;
        }

        dot_count = 0;
        prev_dot = -1;     
        for (i=0; i<16; i++) {
            if (cmd[i] == '.') {
                delta_dot = i-prev_dot;
                prev_dot = i;
                dot_count++;
                if ((delta_dot < 2) || (delta_dot > 4) || (dot_count > 3)) {
                    return -1;
                } 
            }
            else if ((cmd[i] == ';') || (cmd[i] == 0)) {
              gwip_addr[i] = 0;
              if (dot_count != 3)
                  return -1;
              break;
            } else if (IS_A_NUMBER(cmd[i]) == 0) {
                return -1;
            }
            gwip_addr[i] = cmd[i];
        }
        gwip_address = inet_addr((char *)gwip_addr);
        return 0;
    } 

    if (c == 'N') 
    {   
        // read netmask
        cmd++;
        c = toupper(*cmd);
        if (c == 'P') {
            // IPv6 network prefix
            cmd++;
            if (*cmd != '6') return -1;
            cmd++;
            while (SCAN_IS_SPACE(*cmd)) {
	            ++cmd;
            }
            if (*cmd != '=') return -1;

            cmd++;
            while (SCAN_IS_SPACE(*cmd)) {
	            ++cmd;
            }
            network_prefix = atoi((char *)cmd);
            if ((network_prefix <= 0) || (network_prefix > 64)) return -1;
            return 0;
        }
        if (c != 'M') return -1; 
        cmd++;
        c = toupper(*cmd);
        if (c != 'A') return -1; 
        cmd++;
        c = toupper(*cmd);
        if (c != 'S') return -1; 
        cmd++;
        c = toupper(*cmd);
        if (c != 'K') return -1; 

        cmd++;
        while (SCAN_IS_SPACE(*cmd)) {
	        ++cmd;
        }
        if (*cmd != '=') return -1;

        cmd++;
        while (SCAN_IS_SPACE(*cmd)) {
	        ++cmd;
        }

        dot_count = 0;
        prev_dot = -1;     
        for (i=0; i<16; i++) {
            if (cmd[i] == '.') {
                delta_dot = i-prev_dot;
                prev_dot = i;
                dot_count++;
                if ((delta_dot < 2) || (delta_dot > 4) || (dot_count > 3)) {
                    return -1;
                } 
            }
            else if ((cmd[i] == ';') || (cmd[i] == 0))  {
              net_mask[i] = 0;
              if (dot_count != 3)
                  return -1;
              break;
            } else if (IS_A_NUMBER(cmd[i]) == 0) {
                return -1;
            }
            net_mask[i] = cmd[i];
        }
        netmask = inet_addr((char *)net_mask);
        return 0;
    } 

    else if (c == 'C') 
    {

        // read companding
        cmd++;
        c = toupper(*cmd);
        if (c != 'M') return -1; 

        cmd++;
        c = toupper(*cmd);
        if (c != 'P') return -1; 

        cmd++;
        if ((*cmd != '0') && (*cmd != '1')) return -1;
        port = *cmd - '0';

        cmd++;
        while (SCAN_IS_SPACE(*cmd)) {
	        ++cmd;
        }
        if (*cmd != '=') return -1;

        cmd++;
        while (SCAN_IS_SPACE(*cmd)) {
	        ++cmd;
        }

        c = toupper(*cmd);
        if (c == 'U') {
            compandingMode[port] = 0;
            numSlotsOnStream[port] = 32;
            maxSlotsSupported[port] = 32;
            serialWordSize[port] = 8;
            dmaBufferSize[port] = 512; 
        } else if (c == 'A') {
            compandingMode[port] = 1;
            numSlotsOnStream[port] = 32;
            maxSlotsSupported[port] = 32;
            serialWordSize[port] = 8;
            dmaBufferSize[port] = 512; 
        } else if (c == 'L') {
            compandingMode[port] = 2;
            numSlotsOnStream[port] = 16;
            maxSlotsSupported[port] = 16;
            serialWordSize[port] = 16;
            dmaBufferSize[port] = 256; 
        } else
            return -1;

        return 0;
    } 

    else if (c == 'M') 
    {
        // report MAC address
        cmd++;
        c = toupper(*cmd);
        if (c != 'A') return -1; 

        cmd++;
        c = toupper(*cmd);
        if (c != 'C') return -1; 

        return 2;
    }
    else if (c == 'E') 
    {
        // exit the parameter update
        cmd++;
        c = toupper(*cmd);
        if (c != 'X') return -1; 

        cmd++;
        c = toupper(*cmd);
        if (c != 'I') return -1; 

        cmd++;
        c = toupper(*cmd);
        if (c != 'T') return -1;

        return 1;    
    }

    else if (c == 'L') {
        cmd++;
        c = toupper(*cmd);
        if (c != 'O') return -1; 

        cmd++;
        c = toupper(*cmd);
        if (c != 'O') return -1; 

        cmd++;
        c = toupper(*cmd);
        if (c != 'P') return -1;

        cmd++;
        while (SCAN_IS_SPACE(*cmd)) {
	        ++cmd;
        }
        if (*cmd != '=') return -1;

        cmd++;
        while (SCAN_IS_SPACE(*cmd)) {
	        ++cmd;
        }

        if (cmd[0] == '0')
            mcbsp_loopback = 0;
        else
            mcbsp_loopback = 1;

        return 0;
    }
    else {
        return -1;
    }
}
static uint8_t hexToAscii(uint8_t c) {
    if (c<10) 
        return (c+'0');
    else if (c<16) 
        return (c+'a'-10);
    else
        return('x');
}
 
static void get_host_params_uart(uint32_t timeout_secs)
{
    uint8_t 	*message;
    int 		length;
    uint8_t 	val;
    int 		i;
    WRITE_info 	setting;
    uint8_t     reply_message[MAX_MESSAGE_LEN];
    uint8_t     cmd[MAX_MESSAGE_LEN];
    int         idx;
    int         status;
    Uint32 macId1, macId2;
    Uint8 buf[128];
    Uint8 buf1[128];
    uint32_t timeout_count = 0;

    memset(ip_addr, 0, sizeof(ip_addr));
    ip_addr[0] = '0';    
    ip_addr[1] = '.';    
    ip_addr[2] = '0';    
    ip_addr[3] = '.';    
    ip_addr[4] = '0';    
    ip_addr[5] = '.';    
    ip_addr[6] = '0';    
    ip_address = 0;

    memset(gwip_addr, 0, sizeof(gwip_addr));
    gwip_addr[0] = '0';    
    gwip_addr[1] = '.';    
    gwip_addr[2] = '0';    
    gwip_addr[3] = '.';    
    gwip_addr[4] = '0';    
    gwip_addr[5] = '.';    
    gwip_addr[6] = '0';    
    gwip_address = 0;

    memset(net_mask, 0, sizeof(net_mask));
    net_mask[0] = '0';    
    net_mask[1] = '.';    
    net_mask[2] = '0';    
    net_mask[3] = '.';    
    net_mask[4] = '0';    
    net_mask[5] = '.';    
    net_mask[6] = '0';    
    netmask = 0;

    memset (ip6_addr_string, 0, sizeof(ip6_addr_string));
    memset (&ip6_address, 0, sizeof(IP6N));
    network_prefix = 64;

    for (i=0; i<2; i++) {
        compandingMode[i]    = sysConfig.compandingMode[i];
        numSlotsOnStream[i]  = sysConfig.numSlotsOnStream[i];
        maxSlotsSupported[i] = sysConfig.maxSlotsSupported[i];
        serialWordSize[i]    = sysConfig.serialWordSize[i];
        dmaBufferSize[i]     = sysConfig.dmaBufferSize[i];
    }

    CSL_BootCfgGetMacIdentifier (&macId1, &macId2);
    local_mac[5] = macId1 & 0xff;
    local_mac[4] = (macId1 >> 8)  & 0xff;
    local_mac[3] = (macId1 >> 16) & 0xff;
    local_mac[2] = (macId1 >> 24) & 0xff;
    local_mac[1] = (macId2) & 0xff;
    local_mac[0] = (macId2 >> 8) & 0xff;

    /* Don't echo to the uart since we are testing on it */
    setting = platform_write_configure (PLATFORM_WRITE_PRINTF);


    sprintf((char *)buf,"\r\nGPAK build version %s, %s, %s \r\n",GPAK_BUILD_VERSION,__DATE__,__TIME__);
    length = strlen((char *)buf);
    for (i = 0; i < length; i++) 
        platform_uart_write(buf[i]);
    platform_write("%s",buf);

    sprintf((char *)buf, "MAC %02x.%02x.%02x.%02x.%02x.%02x\r\n",local_mac[0], local_mac[1], local_mac[2], local_mac[3], local_mac[4], local_mac[5]);
    length = strlen((char *)buf);
    for (i = 0; i < length; i++) 
        platform_uart_write(buf[i]);
    platform_write("%s",buf);

    if (timeout_secs == 0)
        goto timeout_expired;

    platform_write("Open a serial port console in a PC connected to\n");
    platform_write("the board using UART and set its baudrate to %d\n", DEF_INIT_CONFIG_UART_BAUDRATE);

    sprintf((char *)buf,"\r\nparameter configuration syntax: IP=xxx.xxx.xxx.xxx; CMP0=<U,A,L>; CMP1=<U,A,L>; MAC; EXIT; LOOP=<0,1>;\r\n");
    sprintf((char *)buf1,"GWIP=xxx.xxx.xxx.xxx;  NMASK=xxx.xxx.xxx.xxx; IP6=xxxx:xxxx:xxxx:xxxx:xxxx:xxxx:xxxx:xxxx; NP6=x; \r\n\r\n");
    platform_write("You should see following message --- %s", buf);
    platform_write("                                     %s", buf1);

    length = strlen((char *)buf);
    for (i = 0; i < length; i++) 
        platform_uart_write(buf[i]);

    length = strlen((char *)buf1);
    for (i = 0; i < length; i++) 
        platform_uart_write(buf1[i]);

    message = "example to set static IP: IP=192.168.0.32;\r\n";
    length = strlen((char *)message);
    for (i = 0; i < length; i++) 
        platform_uart_write(message[i]);

    message = "example to set DHCP IP: IP=0.0.0.0;\r\n";
    length = strlen((char *)message);
    for (i = 0; i < length; i++) 
        platform_uart_write(message[i]);

    message = "example to set default gateway IP: GWIP=192.168.0.1;\r\n";
    length = strlen((char *)message);
    for (i = 0; i < length; i++) 
        platform_uart_write(message[i]);

    message = "example to set network mask: NMASK=255.255.255.0;\r\n";
    length = strlen((char *)message);
    for (i = 0; i < length; i++) 
        platform_uart_write(message[i]);

    message = "example to set port0 companding to Linear 16-bit: CMP0=L;\r\n";
    length = strlen((char *)message);
    for (i = 0; i < length; i++) 
        platform_uart_write(message[i]);

    message = "example to set port1 companding to U-law: CMP1=U;\r\n";
    length = strlen((char *)message);
    for (i = 0; i < length; i++) 
        platform_uart_write(message[i]);

    message = "example to display MAC address as xx.xx.xx.xx.xx.xx:  MAC;\r\n";
    length = strlen((char *)message);
    for (i = 0; i < length; i++) 
        platform_uart_write(message[i]);

    message = "example to disable McBSP TDM Loopback: LOOP=0;\r\n";
    length = strlen((char *)message);
    for (i = 0; i < length; i++) 
        platform_uart_write(message[i]);

    message = "example to set global IPv6: IP6=2001:db8:1234::2;\r\n";
    length = strlen((char *)message);
    for (i = 0; i < length; i++) 
        platform_uart_write(message[i]);

    message = "example to set IPv6 network prefix: NP6=64;\r\n";
    length = strlen((char *)message);
    for (i = 0; i < length; i++) 
        platform_uart_write(message[i]);

    idx = 0;

#ifdef CONFIG_GPIO
    gpioInit();
    gpioSetDirection(GPIO_26, GPIO_OUT);
    gpioSetDirection(GPIO_27, GPIO_OUT);
    gpioSetOutput(GPIO_26);
#endif

    while(1) 
    {
        platform_errno = PLATFORM_ERRNO_RESET;
        //if (platform_uart_read(&val, 30 * 10000000) == Platform_EOK) 
        if (platform_uart_read(&val, 1000000) == Platform_EOK) 
        {
            key_was_hit = 1;
            cmd[idx] = val;
            if (cmd[idx] == ';') {
                cmd[idx] = 0;
                status = parse_cmd(cmd);
                cmd[idx] = ';';
                if (status == 0) {
                    idx++;
                    cmd[idx] = '\r';
                    idx++;
                    cmd[idx] = '\n';
                    // echo the message as the reply
                    for (i=0; i<=idx; i++)
                        platform_uart_write(cmd[i]);
                }
                else if (status == 1) {
                    reply_message[0] = 'B';
                    reply_message[1] = 'Y';
                    reply_message[2] = 'E';
                    reply_message[3] = '\r';
                    reply_message[4] = '\n';
                    for (i=0; i<5; i++)
                        platform_uart_write(reply_message[i]);
                    break;
                } 
                else if (status == 2) {
                    // Send the MAC as the reply
                    reply_message[0] = 'M';
                    reply_message[1] = 'A';
                    reply_message[2] = 'C';
                    reply_message[3] = '=';
                    reply_message[4] = hexToAscii((local_mac[0]>>4) & 0xf);
                    reply_message[5] = hexToAscii(local_mac[0] & 0xf);
                    reply_message[6] = '.';
                    reply_message[7] = hexToAscii((local_mac[1]>>4) & 0xf);
                    reply_message[8] = hexToAscii(local_mac[1] & 0xf);
                    reply_message[9] = '.';
                    reply_message[10] = hexToAscii((local_mac[2]>>4) & 0xf);
                    reply_message[11] = hexToAscii(local_mac[2] & 0xf);
                    reply_message[12] = '.';
                    reply_message[13] = hexToAscii((local_mac[3]>>4) & 0xf);
                    reply_message[14] = hexToAscii(local_mac[3] & 0xf);
                    reply_message[15] = '.';
                    reply_message[16] = hexToAscii((local_mac[4]>>4) & 0xf);
                    reply_message[17] = hexToAscii(local_mac[4] & 0xf);
                    reply_message[18] = '.';
                    reply_message[19] = hexToAscii((local_mac[5]>>4) & 0xf);
                    reply_message[20] = hexToAscii(local_mac[5] & 0xf);
                    reply_message[21] = '\r';
                    reply_message[22] = '\n';
                    for (i=0; i<23; i++)
                        platform_uart_write(reply_message[i]);
                }
                else {
                    reply_message[0] = 'E';
                    reply_message[1] = 'R';
                    reply_message[2] = 'R';
                    reply_message[3] = 'O';
                    reply_message[4] = 'R';
                    reply_message[5] = '\r';
                    reply_message[6] = '\n';
                    for (i=0; i<7; i++)
                        platform_uart_write(reply_message[i]);
                }
                idx = 0;
            } else {
                idx++;
                if (idx >= MAX_MESSAGE_LEN)
                    idx = 0; 
            }
        } else 
        {
            //platform_write("Char %c (errno: 0x%x)\n", val, platform_errno);
            if ((platform_errno == 0x40) && !key_was_hit) {
                timeout_count++;
                if (timeout_count >= timeout_secs)
                    break;
            }
        }
    };

timeout_expired:

    // save the mcbsp settings
    for (i=0; i<2; i++) { 
        sysConfig.compandingMode[i]    = (GpakCompandModes)compandingMode[i];
        sysConfig.numSlotsOnStream[i]  = numSlotsOnStream[i];
        sysConfig.maxSlotsSupported[i] = maxSlotsSupported[i];
        sysConfig.serialWordSize[i]    = serialWordSize[i];
        sysConfig.dmaBufferSize[i]     = dmaBufferSize[i]; 
    }

    // save the IP settings
    *pIPAddr = ip_address;

    // gw IP and subnet mask only valid for static IP
    if (ip_address != 0) {
        *pGWIPAddr = gwip_address;
        *pNetMask = netmask;
    }

    if (ip6_addr_string[0] != 0) {
        memcpy(&IPv6Info.globalAddress, &ip6_address, sizeof(IP6N));
        IPv6Info.add_address = 2;
        IPv6Info.network_prefix = network_prefix;
    } 

    ipConfig->RxMac[0] = local_mac[0];
    ipConfig->RxMac[1] = local_mac[1];
    ipConfig->RxMac[2] = local_mac[2];
    ipConfig->RxMac[3] = local_mac[3];
    ipConfig->RxMac[4] = local_mac[4];
    ipConfig->RxMac[5] = local_mac[5];

    platform_write_configure (setting);
}


/******************************************************************************
 * Function:    main  
 ******************************************************************************/

void gpak_uart(uint32_t cpu_freq, uint32_t timeout_secs)
{
	platform_init_flags  init_flags;
	platform_init_config init_config;

	/* Set default values */
	memset(&init_flags, 0, sizeof(platform_init_flags));
    memset(&init_config, 0, sizeof(platform_init_config));

    //init_flags.pll = 1;
	//init_config.pllm    = DEF_INIT_CONFIG_PLL1_PLLM;
    //init_flags.ddr = 1;
    init_flags.phy = 0; //1;
    init_flags.ecc = 1;


	if (platform_init(&init_flags, &init_config) != Platform_EOK) {
	    printf("Platform failed to initialize, errno = 0x%x \n", platform_errno);
	}

	platform_write_configure(PLATFORM_WRITE_PRINTF);
	platform_uart_init();
	Platform_uart_set_baudrate(DEF_INIT_CONFIG_UART_BAUDRATE,  cpu_freq/6);

	get_host_params_uart(timeout_secs);

}
char *mdioLinkStatus[6] = {
    "NOLINK",
    "HD10",
    "FD10",
    "HD100",
    "FD100",
    "FD1000"
};
char *mdioLinkEvent[4] = {
    "MDIOINIT",
    "LINKDOWN",
    "LINKUP",  
    "PHYERROR"
};
 
void uart_reportLinkStatus() {
uint8_t msg[MAX_MESSAGE_LEN];
int i, length, avail;
uint32_t LinkEvent, LinkStatus; 

    do {

        avail = GpakReadLinkReports(&LinkEvent, &LinkStatus);
        if (!avail) return;

        if ((LinkEvent > 3) || (LinkStatus > 5))
            sprintf((char *)msg,"\r\nLinkReportError: Event %d, Status %d\r\n",LinkEvent, LinkStatus);
        else 
            sprintf((char *)msg,"\r\nLinkReport: %s, %s\r\n",mdioLinkEvent[LinkEvent], mdioLinkStatus[LinkStatus]);

        length = strlen((char *)msg);
        for (i = 0; i < length; i++)
            platform_uart_write(msg[i]);

    } while (avail);
}


Uint32 uart_count = 0;
int uart_sym_idx = 0;
char uart_sym[2] = {'+', '-'};
void uart_keepalive() {
Uint8 buf[4];

    uart_reportLinkStatus();
    uart_count++;
    if (uart_count < 2000) return;
    uart_count = 0;
    buf[0] = uart_sym[uart_sym_idx];
    uart_sym_idx ^= 1;
    platform_uart_write(buf[0]);
}

void uart_printIPv6Address(char *addr) {
uint8_t msg[MAX_MESSAGE_LEN];
int i, length;

    sprintf((char *)msg,"\r\nIPV6=%s\r\n",addr);
    length = strlen((char *)msg);
    for (i = 0; i < length; i++) { 
        platform_uart_write(msg[i]);
    }
}
void uart_printIPv4Address(IPN LocalIPAddr) {
uint8_t msg[MAX_MESSAGE_LEN];
int i, length;

    sprintf((char *)msg,"\r\nIPV4=%d.%d.%d.%d\r\n",
        (uint8_t) (LocalIPAddr >> 24) & 0xFF, (uint8_t) (LocalIPAddr >> 16) & 0xFF,
        (uint8_t) (LocalIPAddr >> 8) & 0xFF, (uint8_t) LocalIPAddr & 0xFF);
    length = strlen((char *)msg);
    for (i = 0; i < length; i++) { 
        platform_uart_write(msg[i]);
    }
}
