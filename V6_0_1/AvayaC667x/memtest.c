#define _CSL_H_         // Avoid csl definitions
#include <std.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "GpakExts.h"
#include "GpakHpi.h"

#define BASE_ADDR       0x98000000ul // start memtest at upper half of non-cached DDR3 (just playrecord buffer)
#define BLOCK_SIZE      0x1000ul
#define MEM_RANGE       0x01000000ul // 0x08000000ul
#define NUM_BLOCKS      (MEM_RANGE/BLOCK_SIZE)

typedef struct MemoryTestStats_t {
    GPAK_MemoryTestResults_t MemoryTestResult;
	ADT_UInt32 MemoryTestAddress;
	ADT_UInt16 MemoryTestData;
} MemoryTestStats_t;

static MemoryTestStats_t memoryTestStats;
#pragma DATA_SECTION (memoryTestStats, "FAST_DATA_SECT:memoryTestStats")

ADT_UInt16 ProcMemoryTestMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {
   pReply[0] |= (MSG_READ_MEMTEST_STATUS_REPLY << 8);
   pReply[1] =  memoryTestStats.MemoryTestResult;
   pReply[2] =  memoryTestStats.MemoryTestData;
   pReply[3] =  memoryTestStats.MemoryTestAddress>>16;
   pReply[4] =  memoryTestStats.MemoryTestAddress;
   return 5;
}

/**********************************************************************
 *
 * Function:    memTestDataBus()
 *
 * Description: Test the data bus wiring in a memory region by
 *              performing a walking 1's test at a fixed address
 *              within that region.  The address (and hence the
 *              memory region) is selected by the caller.
 *
 * Notes:
 *
 * Returns:     0 if the test succeeds.
 *              A non-zero result is the first pattern that failed.
 *
 **********************************************************************/
#pragma CODE_SECTION (memTestDataBus, "INIT_PROG_SECT:memTestDataBus")
static Uint16 memTestDataBus(MemoryTestStats_t *memoryTestStats)
{
    Uint16 pattern;
    Uint16 saverestore;

    volatile Uint16 *address;
    address = (Uint16*)memoryTestStats->MemoryTestAddress;
    saverestore = *address;
    /*
     * Perform a walking 1's test at the given address.
     */
    for (pattern = 1; pattern != 0; pattern <<= 1)
    {
        /*
         * Write the test pattern.
         */
        *address = pattern;

        /*
         * Read it back (immediately is okay for this test).
         */
        if (*address != pattern)
        {
        	memoryTestStats->MemoryTestAddress = (Uint32) address;
        	memoryTestStats->MemoryTestData = pattern;
        	*address = saverestore;
            return (pattern);

        }
    }
    *address = saverestore;

    return (0);

}   /* memTestDataBus() */


/**********************************************************************
 *
 * Function:    memTestAddressBus()
 *
 * Description: Test the address bus wiring in a memory region by
 *              performing a walking 1's test on the relevant bits
 *              of the address and checking for aliasing. This test
 *              will find single-bit address failures such as stuck
 *              -high, stuck-low, and shorted pins.  The base address
 *              and size of the region are selected by the caller.
 *
 * Notes:       For best results, the selected base address should
 *              have enough LSB 0's to guarantee single address bit
 *              changes.  For example, to test a 64-Kbyte region,
 *              select a base address on a 64-Kbyte boundary.  Also,
 *              select the region size as a power-of-two--if at all
 *              possible.
 *
 * Returns:     NULL if the test succeeds.
 *              A non-zero result is the first address at which an
 *              aliasing problem was uncovered.  By examining the
 *              contents of memory, it may be possible to gather
 *              additional information about the problem.
 *
 **********************************************************************/
#pragma CODE_SECTION (memTestAddressBus, "INIT_PROG_SECT:memTestAddressBus")
static Uint16 * memTestAddressBus(MemoryTestStats_t *memoryTestStats, unsigned long nBytes)
{
    unsigned long addressMask = (nBytes/sizeof(Uint16) - 1);
    unsigned long offset, restore_offset;
    unsigned long testOffset;

    volatile Uint16 * baseAddress;
    Uint16 pattern     = (Uint16) 0xAAAA;
    Uint16 antipattern = (Uint16) 0x5555;
    Uint16 saverestore[BLOCK_SIZE/sizeof(Uint16)];

    baseAddress = (Uint16*)memoryTestStats->MemoryTestAddress;

    for(restore_offset = 0; restore_offset != BLOCK_SIZE/sizeof(Uint16); restore_offset++)
    {
        saverestore[restore_offset] = baseAddress[restore_offset];
    }
    /*
     * Write the default pattern at each of the power-of-two offsets.
     */
    for (offset = 1; (offset & addressMask) != 0; offset <<= 1)
    {
        baseAddress[offset] = pattern;
    }
    FLUSH_wait ();
    /*
     * Check for address bits stuck high.
     */
    testOffset = 0;
    baseAddress[testOffset] = antipattern;
    FLUSH_wait ();
    for (offset = 1; (offset & addressMask) != 0; offset <<= 1)
    {
        if (baseAddress[offset] != pattern)
        {
        	memoryTestStats->MemoryTestAddress = (Uint32) &baseAddress[offset];
        	memoryTestStats->MemoryTestData = pattern;
        	for(restore_offset = 0; restore_offset != BLOCK_SIZE/sizeof(Uint16); restore_offset++)
			{
				baseAddress[restore_offset] = saverestore[restore_offset];
			}
			FLUSH_wait ();
            return ((Uint16 *) &baseAddress[offset]);
        }
    }
    baseAddress[testOffset] = pattern;
    FLUSH_wait ();
    /*
     * Check for address bits stuck low or shorted.
     */
    for (testOffset = 1; (testOffset & addressMask) != 0; testOffset <<= 1)
    {
        baseAddress[testOffset] = antipattern;
        FLUSH_wait ();
		if (baseAddress[0] != pattern)
		{
			memoryTestStats->MemoryTestAddress = (Uint32) &baseAddress[testOffset];
			memoryTestStats->MemoryTestData = pattern;
			for(restore_offset = 0; restore_offset != BLOCK_SIZE/sizeof(Uint16); restore_offset++)
			{
				baseAddress[restore_offset] = saverestore[restore_offset];
			}
			FLUSH_wait ();
			return ((Uint16 *) &baseAddress[testOffset]);
		}

        for (offset = 1; (offset & addressMask) != 0; offset <<= 1)
        {
            if ((baseAddress[offset] != pattern) && (offset != testOffset))
            {
            	memoryTestStats->MemoryTestAddress = (Uint32) &baseAddress[testOffset];
            	memoryTestStats->MemoryTestData = pattern;
            	for(restore_offset = 0; restore_offset != BLOCK_SIZE/sizeof(Uint16); restore_offset++)
				{
					baseAddress[restore_offset] = saverestore[restore_offset];
				}
				FLUSH_wait ();
                return ((Uint16 *) &baseAddress[testOffset]);
            }
        }

        baseAddress[testOffset] = pattern;
        FLUSH_wait ();
    }
    for(restore_offset = 0; restore_offset != BLOCK_SIZE/sizeof(Uint16); restore_offset++)
	{
    	baseAddress[restore_offset] = saverestore[restore_offset];
	}
    FLUSH_wait ();
    return (NULL);

}   /* memTestAddressBus() */

/**********************************************************************
 *
 * Function:    memTestDevice()
 *
 * Description: Test the integrity of a physical memory device by
 *              performing an increment/decrement test over the
 *              entire region.  In the process every storage bit
 *              in the device is tested as a zero and a one.  The
 *              base address and the size of the region are
 *              selected by the caller.
 *
 * Notes:
 *
 * Returns:     NULL if the test succeeds.
 *
 *              A non-zero result is the first address at which an
 *              incorrect value was read back.  By examining the
 *              contents of memory, it may be possible to gather
 *              additional information about the problem.
 *
 **********************************************************************/
#pragma CODE_SECTION (memTestDevice, "INIT_PROG_SECT:memTestDevice")
static Uint16 * memTestDevice(MemoryTestStats_t *memoryTestStats, unsigned long nBytes)

{
    unsigned long offset, restore_offset;
    unsigned long nWords = nBytes / sizeof(Uint16);

    volatile Uint16 * baseAddress;
    Uint16 pattern;
    Uint16 antipattern;
    Uint16 saverestore[BLOCK_SIZE/sizeof(Uint16)];

    baseAddress = (Uint16*)memoryTestStats->MemoryTestAddress;

	for(restore_offset = 0; restore_offset != BLOCK_SIZE/sizeof(Uint16); restore_offset++)
	{
	   saverestore[restore_offset] = baseAddress[restore_offset];
	}
    /*
     * Fill memory with a known pattern.
     */
    for (pattern = 1, offset = 0; offset < nWords; pattern++, offset++)
    {
        baseAddress[offset] = pattern;
    }
    FLUSH_wait ();
    /*
     * Check each location and invert it for the second pass.
     */
    for (pattern = 1, offset = 0; offset < nWords; pattern++, offset++)
    {
        if (baseAddress[offset] != pattern)
        {
        	memoryTestStats->MemoryTestAddress = (Uint32) &baseAddress[offset];
			memoryTestStats->MemoryTestData = pattern;
			for(restore_offset = 0; restore_offset != BLOCK_SIZE/sizeof(Uint16); restore_offset++)
			{
				baseAddress[restore_offset] = saverestore[restore_offset];
			}
			FLUSH_wait ();
            return ((Uint16 *) &baseAddress[offset]);
        }

        antipattern = ~pattern;
        baseAddress[offset] = antipattern;
        FLUSH_wait ();
    }

    /*
     * Check each location for the inverted pattern and zero it.
     */
    for (pattern = 1, offset = 0; offset < nWords; pattern++, offset++)
    {
        antipattern = ~pattern;
        if (baseAddress[offset] != antipattern)
        {
        	memoryTestStats->MemoryTestAddress = (Uint32) &baseAddress[offset];
			memoryTestStats->MemoryTestData = pattern;
			for(restore_offset = 0; restore_offset != BLOCK_SIZE/sizeof(Uint16); restore_offset++)
			{
				baseAddress[restore_offset] = saverestore[restore_offset];
			}
        	FLUSH_wait ();
            return ((Uint16 *) &baseAddress[offset]);
        }
        baseAddress[offset] = 0;
        FLUSH_wait ();
    }

    for(restore_offset = 0; restore_offset != BLOCK_SIZE/sizeof(Uint16); restore_offset++)
   	{
       	baseAddress[restore_offset] = saverestore[restore_offset];
   	}
    FLUSH_wait ();
    return (NULL);

}  


#pragma CODE_SECTION (memory_test, "INIT_PROG_SECT:memory_test")
void memory_test(MemoryTestStats_t *memoryTestStats) {
Uint16 i;

	memoryTestStats->MemoryTestResult = MT_Success;
	memoryTestStats->MemoryTestAddress = BASE_ADDR;
	memoryTestStats->MemoryTestData = 0;
    if (memTestDataBus(memoryTestStats) != 0) {
    	memoryTestStats->MemoryTestResult = MT_DataBusFail;
        goto memError;
    }

    memoryTestStats->MemoryTestAddress = BASE_ADDR;
    memoryTestStats->MemoryTestData = 0;
    for (i=0; i< NUM_BLOCKS ; i++) {
        if (memTestAddressBus(memoryTestStats, BLOCK_SIZE) != NULL) {
        	memoryTestStats->MemoryTestResult = MT_AddressBusFail;
            goto memError;
        }
        memoryTestStats->MemoryTestAddress += BLOCK_SIZE;
    }

    memoryTestStats->MemoryTestAddress = BASE_ADDR;
    memoryTestStats->MemoryTestData = 0;
    for (i=0; i< NUM_BLOCKS ; i++) {
        if (memTestDevice(memoryTestStats, BLOCK_SIZE) != NULL) {
        	memoryTestStats->MemoryTestResult = MT_DeviceFail;
            goto memError;
        }
        memoryTestStats->MemoryTestAddress += BLOCK_SIZE;
    }

memError:

    return;
}
void run_memory_test() {
    memset(&memoryTestStats, 0, sizeof(memoryTestStats));
    memory_test(&memoryTestStats);
}

