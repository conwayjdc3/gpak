#include "AvayaC6678.h"
#include "GpakDefs.h"
#include "GpakExts.h"
#include "sysconfig.h"
#include "GpakPcm.h"
#include "stdio.h"
#include <string.h>
#include "g711a1a2_user.h"
#define G711_muLawDecode(a,b,c) G711_ADT_muLawExpand(a,b,c)
#define G711_muLawEncode(a,b,c) G711_ADT_muLawCompress(a,b,c)
#define G711_aLawDecode(a,b,c)  G711_ADT_aLawExpand(a,b,c)
#define G711_aLawEncode(a,b,c)  G711_ADT_aLawCompress(a,b,c)

//#define G726_TEST
//#define G711_TEST
//#define SRTP_TEST
//#define VAD_TEST
//#define  TG_TEST
//#define T38_RELAY_TEST


#ifdef G726_TEST
#define G726_INVECT  "I:\\adt_projects\\avaya\\GPAK\\VectGel\\f2_f1_m1_raw_8k_mono.pcm"                
#define G726_OUTVECT "I:\\adt_projects\\avaya\\GPAK\\VectGel\\G726Out.pcm"                
#define G726_FRAMESIZE 160
typedef struct g726Test_t {
    int rate;
    int useCanned;
    int law;       // 0==ULAW, 1==ALAW, 2==LINEAR
    G726ChannelInstance_t *enc;   
    G726ChannelInstance_t *dec;
    FILE *fpIn;
    FILE *fpOut;
    short pcm[G726_FRAMESIZE];
    unsigned char mu[G726_FRAMESIZE];
    unsigned short params[G726_FRAMESIZE];
    unsigned short payload[G726_FRAMESIZE];
} g726Test_t;
g726Test_t g726Test = {32, 1, ULAW};
static const ADT_UInt16 cannedPcm[8] = {303, 672, 1322, 2409, 5110, 9738, 19202, 34770};
extern void G726_Encode (void *inst, unsigned char *Ulaw, ADT_UInt16 *Pyld, int smpCnt);
extern void G726_Decode (void *inst, unsigned char *Ulaw, ADT_UInt16 *Pyld, int smpCnt);

void G726Test() {
int num, width, i;
ADT_UInt16 *pDst;
unsigned char *pPay;

    g726Test.enc = (G726ChannelInstance_t *)encoderPool[0];
    g726Test.dec = (G726ChannelInstance_t *)decoderPool[0];
    G726_ADT_Init( g726Test.enc, g726Test.rate, g726Test.law);
    G726_ADT_Init( g726Test.dec, g726Test.rate, g726Test.law);
    if (g726Test.useCanned == 0) { 
        g726Test.fpIn  = fopen(G726_INVECT, "rb");
        g726Test.fpOut = fopen(G726_OUTVECT, "wb");
        if ((g726Test.fpIn == 0) || (g726Test.fpOut == 0))  {
            goto g726_done;
        }
    }
    if      (g726Test.rate == 40) width = 5;
    else if (g726Test.rate == 32) width = 4;
    else if (g726Test.rate == 24) width = 3;
    else if (g726Test.rate == 16) width = 2;
            

    while(1) {

        // Encode, pack
        memset(g726Test.pcm,     0, G726_FRAMESIZE*sizeof(short));
        memset(g726Test.mu,      0, G726_FRAMESIZE);
        memset(g726Test.params,  0, G726_FRAMESIZE*sizeof(short));
        memset(g726Test.payload, 0, G726_FRAMESIZE*sizeof(short));

        if (g726Test.useCanned == 0) { 
            num = fread(g726Test.pcm, sizeof(short), G726_FRAMESIZE, g726Test.fpIn);
            if (num != G726_FRAMESIZE)  break;
        } else {
            pDst = (ADT_UInt16 *)g726Test.pcm;
            num = G726_FRAMESIZE/8;
            for (i=0; i<num; i++) {
                memcpy(pDst, cannedPcm, sizeof(cannedPcm));
                pDst += 8;
            }
        }

        if (g726Test.law != LINEAR) {
            G711_muLawEncode (g726Test.pcm, g726Test.mu, G726_FRAMESIZE);
            G726_Encode (g726Test.enc, g726Test.mu, g726Test.params, G726_FRAMESIZE);
        }

        packLSB (g726Test.params, g726Test.payload, G726_FRAMESIZE, width);
        if (g726Test.useCanned != 0) { 
            pPay = (unsigned char *)g726Test.payload; 
            for(i = 0; i <G726_FRAMESIZE/2; i++) {
                printf("ENC[%d] %x, ENC[%d] %x, PKT[%d] %x\n",2*i, g726Test.params[2*i], 2*i+1, g726Test.params[2*i+1], i, pPay[i]);
            }
        }

        // unpack, Decode
        memset(g726Test.pcm,     0, G726_FRAMESIZE*sizeof(short));
        memset(g726Test.mu,      0, G726_FRAMESIZE);
        memset(g726Test.params,  0, G726_FRAMESIZE*sizeof(short));
        unpackLSB (g726Test.payload, g726Test.params, G726_FRAMESIZE, width);
        if (g726Test.law != LINEAR) {
            G726_Decode (g726Test.dec, g726Test.mu, g726Test.params, G726_FRAMESIZE);
            G711_muLawDecode (g726Test.mu, g726Test.pcm, G726_FRAMESIZE);
        }


        if (g726Test.useCanned == 0) { 
            num = fwrite(g726Test.pcm, sizeof(short), G726_FRAMESIZE, g726Test.fpOut);
            if (num != G726_FRAMESIZE)  break;
        } else {
            pPay = (unsigned char *)g726Test.payload; 
            for(i = 0; i <G726_FRAMESIZE/2; i++) {
                printf("DEC[%d] %4d, DEC[%d] %4d\n",2*i, g726Test.pcm[2*i], 2*i+1, g726Test.pcm[2*i+1]);
            }

            g726Test.useCanned -= 1;
            if (g726Test.useCanned == 0) return;
        }
    }
g726_done:
    if (g726Test.fpIn) fclose(g726Test.fpIn);     
    if (g726Test.fpOut) fclose(g726Test.fpOut);     

}
#endif


#ifdef G711_TEST
#define G711_INVECT "I:\\adt_projects\\avaya\\GPAK\\VectGel\\f2_f1_m1_raw_8k_mono.pcm"                
#define G711A_OUTVECT "I:\\adt_projects\\avaya\\GPAK\\VectGel\\ALAWOut.pcm"                
#define G711U_OUTVECT "I:\\adt_projects\\avaya\\GPAK\\VectGel\\ULAWOut.pcm"                
#define G711_FRAMESIZE 160

typedef struct g711Test_t {
    FILE *fpIn;
    FILE *fpAOut;
    FILE *fpUOut;
    short pcmU[G711_FRAMESIZE];
    unsigned char u[G711_FRAMESIZE];
    short pcmA[G711_FRAMESIZE];
    unsigned char a[G711_FRAMESIZE];
} g711Test_t;
g711Test_t g711Test;

void G711Test() {
int num, i;

    g711Test.fpIn   = fopen(G711_INVECT, "rb");
    g711Test.fpAOut = fopen(G711A_OUTVECT, "wb");
    g711Test.fpUOut = fopen(G711U_OUTVECT, "wb");
    if ((g711Test.fpIn == 0) || (g711Test.fpAOut == 0) || (g711Test.fpUOut == 0))  {
        goto g711_done;
    }

    while(1) {

        memset(g711Test.pcmU, 0, G711_FRAMESIZE*sizeof(short));
        memset(g711Test.pcmA, 0, G711_FRAMESIZE*sizeof(short));
        memset(g711Test.u,    0, G711_FRAMESIZE);
        memset(g711Test.a,    0, G711_FRAMESIZE);

        num = fread(g711Test.pcmU, sizeof(short), G711_FRAMESIZE, g711Test.fpIn);
        if (num != G711_FRAMESIZE)  break;

        for (i=0; i<G711_FRAMESIZE; i++)
            g711Test.pcmA[i] = g711Test.pcmU[i];

        G711_muLawEncode (g711Test.pcmU,  g711Test.u,    G711_FRAMESIZE);
        G711_muLawDecode (g711Test.u,     g711Test.pcmU, G711_FRAMESIZE);
        num = fwrite(g711Test.pcmU, sizeof(short), G711_FRAMESIZE, g711Test.fpUOut);
        if (num != G711_FRAMESIZE)  break;

        G711_aLawEncode (g711Test.pcmA,  g711Test.a,    G711_FRAMESIZE);
        G711_aLawDecode (g711Test.a,     g711Test.pcmA, G711_FRAMESIZE);
        num = fwrite(g711Test.pcmA, sizeof(short), G711_FRAMESIZE, g711Test.fpAOut);
        if (num != G711_FRAMESIZE)  break;

    }
g711_done:
    if (g711Test.fpIn)   fclose(g711Test.fpIn);     
    if (g711Test.fpAOut) fclose(g711Test.fpAOut);     
    if (g711Test.fpUOut) fclose(g711Test.fpUOut);

}
#endif

#ifdef SRTP_TEST
typedef ADT_UInt8 word;

#define bytesToWords(a) a

   #define HEX(a,b)   0x##a,  0x##b
   #define byteSize(a)     sizeof(a)

void printhex (void *Buff, int BuffI8) {
   ADT_UInt8 *buff = Buff;
   int cnt = 0;
   while (BuffI8--) {
      if (!(cnt   & 0x3)) printf (" ");
      if (!(cnt++ & 0xf)) printf ("\n\t");
      printf ("%02X", *buff++); 

   }
   printf ("\n");
}
void printreg (ADT_UInt32 *regs, int BuffI8) {
    
   int cnt = 0, wordCnt;
   wordCnt = BuffI8 / 4;;
   while (wordCnt--) {
      if (!(cnt++ & 0x3)) printf ("\n\t");
      printf ("%04x%04x ", (*regs>>16) & 0xffff, (*regs)&0xffff); 
      regs++;
   }
   printf ("\n");
}

typedef struct DEBUG_FLAGS {
   unsigned int hmac:1;
   unsigned int encr:1;
   unsigned int keys:1;
   unsigned int srtp:1;
   unsigned int keyExchange:1;
} DEBUG_FLAGS;
DEBUG_FLAGS debug = { 0, 0, 0, 0, 0 }  ;

//----------------------------------
//  interOp SRTP test vectors
//  https://tools.ietf.org/html/rfc6904#page-14
//      Appendix A.1
static word interOp_key[] = {
   HEX(e1, f9), HEX(7a, 0d), HEX(3e, 01), HEX(8b, e0),
   HEX(d6, 4f), HEX(a3, 2c), HEX(06, de), HEX(41, 39)
};
static word interOp_salt [] = {
   HEX(0e, c6), HEX(75, ad), HEX(49, 8a), HEX(fe, eb),
   HEX(b6, 96), HEX(0b, 3a), HEX(ab, e6)
};
static word interOp_plaintext_ref[] = {
   HEX(80, 0f), HEX(12, 34), 
   HEX(de, ca), HEX(fb, ad), 
   HEX(ca, fe), HEX(ba, be), 

   HEX(ab, ab), HEX(ab, ab),
   HEX(ab, ab), HEX(ab, ab), 
   HEX(ab, ab), HEX(ab, ab), 
   HEX(ab, ab), HEX(ab, ab)
};
static word interOp_ciphertext[] = {
   HEX(80, 0f), HEX(12, 34),
   HEX(de, ca), HEX(fb, ad), 
   HEX(ca, fe), HEX(ba, be),
    
   HEX(4e, 55), HEX(dc, 4c),
   HEX(e7, 99), HEX(78, d8),
   HEX(8c, a4), HEX(d2, 15), 
   HEX(94, 9d), HEX(24, 02),

   HEX(b7, 8d), HEX(6a, cc),  // Authentication tag
   HEX(99, ea), HEX(17, 9b), HEX(8d, bb)
};
  
ADT_UInt32 *ScratchMemory;    // scratch memory
ADT_UInt32 *encryptInst;       // encrypt context memory
ADT_UInt32 *decryptInst;       // decrypt context memory
ADT_UInt32 encSize = byteSize(SrtpTxInstance_t);
ADT_UInt32 decSize = byteSize(SrtpRxInstance_t);

word inBufr[100];
word outBufr[100];

#if defined(BIG_ENDIAN)
  #define byteMask 0xff00
#else
  #define byteMask 0x00ff
#endif

static int compareBuffers (char *test, void *BuffA, void *BuffB, int BuffI8) {
   int wordCnt, byteCnt;
   ADT_UInt8 *buffA = BuffA;
   ADT_UInt8 *buffB = BuffB;

   if (bytesToWords (2) == 2) {
      byteCnt = 0;
   } else {
      byteCnt = BuffI8 & 1;
      BuffI8 &=0xfffe;
   }
   wordCnt = bytesToWords (BuffI8);
   while (wordCnt--) {
      if (*buffA++ != *buffB++) goto fail;
   }

   if (byteCnt) {
      if ((*buffA++ & byteMask) != (*buffB++ & byteMask)) goto fail;
   }
   printf (" .. %s passed\n", test);
   return 0;

fail:
   printf ("\n%s failed. Buffer mismatch\n", test);
   return 1;
}

static ADT_Bool interOp_ProvideKey (void *pAppHandle,  SrtpKeyInfo_t *pKeyInfo) {

   if (pAppHandle == ((void *) 1))  {
      memcpy (pKeyInfo->pKeyValue,  interOp_key,  sizeof (interOp_key));
      memcpy (pKeyInfo->pSaltValue, interOp_salt, sizeof (interOp_salt));
      printf ("Encrypt key callback.\n");
   } else if (pAppHandle == ((void *) 2)) {
      memcpy (pKeyInfo->pKeyValue,  interOp_key,  sizeof (interOp_key));
      memcpy (pKeyInfo->pSaltValue, interOp_salt, sizeof (interOp_salt));
      printf ("Decrypt key callback.\n");
   } else {
      printf ("Unknown callback.\n");
     return 0;
   }

   return 1;
}

int SRTPTest () {

   SrtpInstanceCfg_V1_t srtpConfig;   // SRTP configuration
   SrtpStatus_t      srtpStatus;   // SRTP status

   ADT_UInt32 contextI8;              // size of context memory
   ADT_UInt16 scratchMemI8;           // size of scratch memory needed
   ADT_UInt16 appendFieldsI8;         // number of extra packet bytes needed
   ADT_UInt16 encryptPktI8;           // encrypted packet length
   ADT_UInt16 decryptPktI8;           // decrypted packet length

   int failureCnt = 0;

   // Define an SRTP Tx configuration and determine the context memory size.
   srtpConfig.APIVersion    = SRTP_API_VERSION;
   srtpConfig.KeyScheme     = KEY_PSK;
   srtpConfig.EncryptType   = ENCRYPT_CM;
   srtpConfig.AuthType      = AUTH_HMAC_SHA;
   srtpConfig.AuthKeySizeU8 = 20;
   srtpConfig.AuthTagLenU8  = 10;
   srtpConfig.KeySizeU8     = 16;
   srtpConfig.SaltSizeU8    = 14;
   srtpConfig.MkiLengthU8   = 0;

   encryptInst = (ADT_UInt32 *)&SrtpTxChanData[0]->Instance;
   decryptInst = (ADT_UInt32 *)&SrtpRxChanData[0]->Instance;
   ScratchMemory = &SrtpScratchPtr[0];


   printf ("\n\nSRTP interOp Test\n");    
   srtpConfig.InstType = INST_SRTP_TX;
   contextI8 = SrtpEvalContextSizeU8 (&srtpConfig, &scratchMemI8, &appendFieldsI8);
   if (contextI8 == 0) {
      printf("Invalid Tx configuration!\n");
      return 0;
   }
   if (encSize < contextI8) {
      printf ("Tx instance structure too small. Requires %ld bytes\n", contextI8);
      return 0;
   }
  

   srtpStatus =  SrtpInitializeInstance (encryptInst, ScratchMemory, &srtpConfig,
                                &interOp_ProvideKey, (void *) 1);
   if (srtpStatus != SRTP_STAT_SUCCESS)  {
      printf ("Tx initialization failed!\n");
      return 0;
   }

   // Define an SRTP Rx configuration and determine the context memory size.
   srtpConfig.InstType = INST_SRTP_RX;
   contextI8 = SrtpEvalContextSizeU8 (&srtpConfig, &scratchMemI8, &appendFieldsI8);
   if (contextI8 == 0) {
      printf ("Invalid Rx configuration!\n");
      return 0;
   }
   if (decSize < contextI8){
     printf ("Rx instance structure too small. Requires %d bytes", contextI8);
     return 0;
  }

   srtpStatus = SrtpInitializeInstance (decryptInst, ScratchMemory, &srtpConfig,
                                        &interOp_ProvideKey, (void *) 2);
   if (srtpStatus != SRTP_STAT_SUCCESS)  {
        printf ("Rx initialization failed!\n");
        return 0;
    }

    // Encrypt an RTP packet.
   encryptPktI8 = SrtpEncryptU8 (encryptInst, interOp_plaintext_ref, 
                               byteSize (interOp_plaintext_ref), outBufr);
   if (encryptPktI8 != byteSize (interOp_ciphertext)) {
      printf("Encrypt failed!\n");
      return 0;
   }
   failureCnt += compareBuffers ("interop RTP AES-CM encrypt", outBufr, interOp_ciphertext, byteSize (interOp_ciphertext));

   // Decrypt the SRTP packet.
   decryptPktI8 = SrtpDecryptU8 (decryptInst, outBufr, encryptPktI8, inBufr);
   if (decryptPktI8 == 0)  {
      printf ("Decrypt failed!\n");
      return failureCnt;
   }

   // Verify the decrypted packet matches the original packet.
   if (decryptPktI8 + appendFieldsI8 == encryptPktI8) {
     failureCnt += compareBuffers ("interop RTP AES-CM decrypt", inBufr, interOp_plaintext_ref, decryptPktI8);
   } else {
      printf ("Packet lengths are unequal!\n");
   }
   printf ("\n----------------------------------");

   return failureCnt;
}
#endif

#ifdef VAD_TEST
#define VAD_INVECT   "I:\\adt_projects\\avaya\\GPAK\\VectGel\\leaves_noise_8k_mono.pcm"                
#define VAD_OUTVECT  "I:\\adt_projects\\avaya\\GPAK\\VectGel\\VadOutStereo.pcm"                

#define VAD_FRAMESIZE 160

typedef struct vadTest_t {
    FILE *fpIn;
    FILE *fpOut;
    short pcm[VAD_FRAMESIZE];
} vadTest_t;
vadTest_t vadTest;

void VadTest () {
VADCNG_Instance_t *pVad = VadCngInstance[0];
VADCNG_ADT_Param_t VadParams;
int vadStat, num, i;
ADT_Int16   NoiseLevel;
short *Src;

    vadTest.fpIn   = fopen(VAD_INVECT, "rb");
    vadTest.fpOut  = fopen(VAD_OUTVECT, "wb");
    if ((vadTest.fpIn == 0) || (vadTest.fpOut == 0))  {
        goto vad_done;
    }
   VadParams.Thres           = -40;  // level above is voice, level below is noise
   VadParams.HangMSec        = 500;
   VadParams.WindowSizeMSec  = 5;
   VadParams.VadFrameSize    = VAD_FRAMESIZE;
   VadParams.CngFrameSize    = VAD_FRAMESIZE;
   VadParams.SamplingRate    = 8000;

   VADCNG_ADT_init (pVad, &VadParams);

    while(1) {

        memset(vadTest.pcm,  0, VAD_FRAMESIZE*sizeof(short));
        num = fread(vadTest.pcm, sizeof(short), VAD_FRAMESIZE, vadTest.fpIn);
        if (num != VAD_FRAMESIZE)  break;

        vadStat = 10000 * VADCNG_ADT_vad (pVad, vadTest.pcm, &NoiseLevel);

        if (vadStat == 0)
            VADCNG_ADT_cng(pVad, 0, NoiseLevel, vadTest.pcm);

        // write vadstat and input signal as stereo output file        
        Src = vadTest.pcm;
        for (i=0; i<VAD_FRAMESIZE; i++) {
            fwrite(&vadStat, sizeof(short), 1, vadTest.fpOut);
            fwrite(Src++,    sizeof(short), 1, vadTest.fpOut);
        }
    }

vad_done:
    if (vadTest.fpIn)  fclose(vadTest.fpIn);     
    if (vadTest.fpOut) fclose(vadTest.fpOut);
}
#endif

#ifdef T38_RELAY_TEST
#define T38_TEST_USE_GPAK_API
#ifdef T38_TEST_USE_GPAK_API
extern  int T38RelayTest_GpakApi ();
#else
extern  int T38RelayTest ();
#endif
#endif


#ifdef  TG_TEST
#define TG_OUTVECT "I:\\adt_projects\\avaya\\GPAK\\VectGel\\ToneGenOut.pcm"                
#define DD_OUTVECT "I:\\adt_projects\\avaya\\GPAK\\VectGel\\DtmfDialOut.pcm"                
#define TG_FRAMESIZE 160
typedef struct tgTest_t {
    int numFrames;
    FILE *fpOut;
    short pcm[TG_FRAMESIZE];
} tgTest_t;
tgTest_t tgTest;

void ToneGenTest() {
int num;
TGInfo_t TGInfo;
TGParams_1_t *tgParams;

    tgTest.numFrames = 0;
    tgTest.fpOut = fopen(TG_OUTVECT, "wb");
    if (tgTest.fpOut == 0)  {
        goto tg_done;
    }


    memset(&TGInfo, 0, sizeof(TGInfo_t));
    AllocToneGen (&TGInfo, Tone_Generate);
    TGInfo.cmd    = ToneGenStart;
    TGInfo.update = Enabled;


    tgParams = TGInfo.toneGenParmPtr;
    CLEAR_INST_CACHE (tgParams, sizeof (TGParams_1_t));
    tgParams->ToneType    = TgContinuous; 
    tgParams->Level[0]    = -10;
    tgParams->Level[1]    = 0;
    tgParams->Level[2]    = 0;
    tgParams->Level[3]    = 0;
    tgParams->NOnOffs     = 0;
    tgParams->NFreqs      = 1;  
    tgParams->OnTime[0]   = 200;
    tgParams->OnTime[1]   = 0;
    tgParams->OnTime[2]   = 0;
    tgParams->OnTime[3]   = 0;
    tgParams->OffTime[0]  = 0;
    tgParams->OffTime[1]  = 0;
    tgParams->OffTime[2]  = 0;
    tgParams->OffTime[3]  = 0;
    tgParams->Freq[0]     = 1000*10;
    tgParams->Freq[1]     = 0;
    tgParams->Freq[2]     = 0;
    tgParams->Freq[3]     = 0;
    FLUSH_INST_CACHE (tgParams, sizeof (TGParams_1_t));

    while(1) {
        ToneGenerate (&TGInfo, tgTest.pcm, TG_FRAMESIZE);
        num = fwrite(tgTest.pcm, sizeof(short), TG_FRAMESIZE, tgTest.fpOut);
        if (num != TG_FRAMESIZE)  break;

        tgTest.numFrames++;
        if (tgTest.numFrames >= 50)
            break;
    }

    printf("TG %d frames generated\n",tgTest.numFrames);
    toneGenChanInUse[TGInfo.index] = 0;
    numToneGenChansAvail++;

tg_done:
    if (tgTest.fpOut) fclose(tgTest.fpOut);     
}

void DtmfDialTest() {
int num;
TGInfo_t TGInfo;
DtmfDialInfo_t  *dial;   
ADT_Int16 NumDigits, i;

    NumDigits = 16;

    tgTest.numFrames = 0;
    tgTest.fpOut = fopen(DD_OUTVECT, "wb");
    if (tgTest.fpOut == 0)  {
        goto dd_done;
    }


    memset(&TGInfo, 0, sizeof(TGInfo_t));
    AllocToneGen (&TGInfo, Tone_Generate);
    TGInfo.update   = Enabled;
    TGInfo.dtmfDial = Enabled;

    dial = TGInfo.dtmfDialParmPtr;
    memset(dial->Digits, 0, sizeof(ADT_UInt16)*16);

    for (i=0; i<NumDigits; i++)
       dial->Digits[i] = i;
    dial->NumDigits = NumDigits;
    dial->CurrentDigit = 0;
    dial->Rate = 8000;
    dial->NSampsOn  = 8000;
    dial->NSampsOff = 12000;
    dial->NSampsGen = 0;
    dial->Level[0] = -10;
    dial->Level[1] = -10;
    dial->State = DialToneOn;

    while(1) {
        ToneGenerate (&TGInfo, tgTest.pcm, TG_FRAMESIZE);
        num = fwrite(tgTest.pcm, sizeof(short), TG_FRAMESIZE, tgTest.fpOut);
        if (num != TG_FRAMESIZE)  break;

        tgTest.numFrames++;
        if (TGInfo.dtmfDial == Disabled)
            break;
    }

    printf("TG %d frames generated\n",tgTest.numFrames);
    toneGenChanInUse[TGInfo.index] = 0;
    numToneGenChansAvail++;

dd_done:
    if (tgTest.fpOut) fclose(tgTest.fpOut);     

}

#endif

void AlgTest() {

#ifdef SRTP_TEST
    SRTPTest ();
#endif

#ifdef G726_TEST
    G726Test ();
#endif

#ifdef G711_TEST
    G711Test ();
#endif

#ifdef VAD_TEST
    VadTest ();
#endif

#ifdef  TG_TEST
    ToneGenTest();
    DtmfDialTest();
#endif
#ifdef T38_RELAY_TEST
	#ifdef T38_TEST_USE_GPAK_API
		T38RelayTest_GpakApi ();
	#else
		T38RelayTest ();
    #endif
#endif
}

