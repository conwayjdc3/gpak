//{ Avaya C667xINit.c  (C667x)
//
// G.PAK initialization for Avaya C667x platform 
//
//---------------------------------------------------------------
//  Custom stubs.
//
//  customDSPInit      Customized configuration before GPAK initialization
//  customDSPStartUp   Customized configuration after GPAK initialization
//  customEncodeInit   Initialization of custom encoder
//  customDecodeInit   Initialization of custom decoder
//  customEncode       Converts pcm data to payload data.
//  customDecode       Converts payload data to pcm data
//  customMsg          Processes a custom G.PAK message
//  customIdle         User hook for idle processing
//}
#define _CSL_H_         // Avoid csl definitions
#include <std.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
//#include <soc.h>
//#include <csl_emac.h>
//#include <gbl.h>
//#include <clk.h>
//#include <mpc.h>
#include "avayaC6678.h"
#include "GpakExts.h"
#include "toneLog.h"
#include "framestats.h"

#ifdef IPC_SUPPORT
#include "gpak_ipc.h"
#endif

#include <ti/ndk/inc/netmain.h>
#include <ipStackUser.h>
//#include <ti/ndk/inc/ipStackUser.h>
#define CPU_FREQ 1000000000ul
#define UART_POLL_TIMEOUT_SECS 0 // 5 // jdc skip uart config for now...

volatile ADT_UInt32 *pin_control0 = (volatile ADT_UInt32 *)0x02620580;
extern int mcbsp_loopback;
#ifdef OPTIMIZED_CFG
const char *GPAK_BUILD_VERSION = "01.14 Opt";
#else
const char *GPAK_BUILD_VERSION = "01.14 Dbg";
#endif

typedef struct versionInfo_t {
    char buf[256];
    int length;
} versionInfo_t;
versionInfo_t gpakVersion;

#ifdef TG_TEST
typedef struct tg_test_t {
    int         cmd;
    ADT_UInt16  chan;
    int         dir;
    int         init;
    int         enab;
    int         copy;
    TGInstance_t  Inst;
    TGParams_1_t  Params;
    ADT_UInt16  pcm[160];
} tg_test_t;
tg_test_t tg = {0,0,1,1,0,0};
#pragma DATA_SECTION (tg, "NON_CACHED_DATA")
void toneGen(ADT_PCM16 *pcmBuff, int FrameSize);
#endif

//#define CACHE_TEST
#ifdef CACHE_TEST
void cacheTest();
#else
#define cacheTest();
#endif

#ifdef AVAYA_INTEGRATION
far const int numTestCores = 6;
#pragma DATA_SECTION (numTestCores, "NON_CACHED_DATA")
#endif


#include "gen_udp.h"   
extern void init_custom_frames ();
extern void Gen_Host_Interrupt ();
extern void initWaveHeap ();
extern void InitTDMMode ();
extern far int DSPCore;
far uint32_t phy_addr_cfg = 0x18; // default PHY address for c6657 EVM

g722RtpDebugStats_t g722RTPStats[1]; //needed for 64xRtpSupport.c
Uint32 delta_1ms, prev_1ms; //, max_wrap = 0;
volatile int custom_cmd = 0;
FrameStats_t FrameStats;
unsigned MAC_HI = 0, MAC_LO = 0;

#pragma DATA_SECTION (FrameStats, "PER_CORE_DATA:STATS")

far ADT_UInt16 PlayRecStartAddr[1];
#pragma DATA_SECTION (PlayRecStartAddr, "PLAYRECSTARTADDR")

far ADT_UInt16 PlayRecEndAddr[1];
#pragma DATA_SECTION (PlayRecEndAddr, "PLAYRECENDADDR")

far ADT_UInt32 PlayRecLengthI8=0;

extern void custom_start_port_evm ();
extern void custom_start_port ();
extern void custom_stop_port ();
extern int captureSignal (CircBufInfo_t *cBuff, void *data, int samples);
void run_memory_test();
extern void init_pll(uint32_t cpu_freq);
extern void* (*customNetIn) (ADT_UInt32 ip, ADT_UInt16 port, ADT_UInt8 *payload);
extern void uart_keepalive();
extern void gpak_uart(uint32_t cpu_freq, uint32_t timeout_secs);

#ifdef _INCLUDE_IPv6_CODE
extern void* (*customNetIn6) (ADT_UInt8 *ip, ADT_UInt16 port, ADT_UInt8 *payload);
extern void* ipToChanLookup6 (ADT_UInt8 *rmtIP, ADT_UInt16 rmtPort, int *chanId);
#endif

static char *LocalIPAddr = "127.1.1.205";
static char *LocalIPMask = "255.255.255.0";  // Not used when using DHCP
static char *GatewayIP   = "192.168.0.1";    // Not used when using DHCP
static int static_ip_address = 0;            // set to 1 to use static IP address
extern IPStackCfg *ipConfig;

// Capture buffers
#define DBG_CAPTI16 80000ul
extern CircBufInfo_t captureA;
extern CircBufInfo_t captureB;
extern CircBufInfo_t captureC;
extern CircBufInfo_t captureD;

ADT_UInt16 lclCaptureBuffA [DBG_CAPTI16];  // 3 minutes of data
ADT_UInt16 lclCaptureBuffB [DBG_CAPTI16];  // 3 minutes of data
ADT_UInt16 lclCaptureBuffC [DBG_CAPTI16];  // 3 minutes of data
ADT_UInt16 lclCaptureBuffD [DBG_CAPTI16];  // 3 minutes of data
#pragma DATA_SECTION (lclCaptureBuffA, "NON_CACHED_DATA")
#pragma DATA_SECTION (lclCaptureBuffB, "NON_CACHED_DATA")
#pragma DATA_SECTION (lclCaptureBuffC, "NON_CACHED_DATA")
#pragma DATA_SECTION (lclCaptureBuffD, "NON_CACHED_DATA")
ADT_UInt32 dbg_captI16 = DBG_CAPTI16;

#pragma DATA_SECTION (DataLogBuff,  "NON_CACHED_DATA:logging")
#ifdef _DEBUG
ADT_UInt64 DataLogBuff[2000];
#else
ADT_UInt64 DataLogBuff[200];

#define Tx1_TCC   4
#define Rx1_TCC   5
#define Tx0_TCC   2
#define Rx0_TCC   3
#endif

typedef struct tdmSignal_t {
    int         mode;
    ADT_UInt32  slotmask;
    ADT_UInt16  pattern;
    ADT_UInt32 playA_index;
    ADT_UInt32 playB_index;
    ADT_UInt32 playC_index;
    ADT_UInt32 playD_index;
} tdmSignal_t;

tdmSignal_t tdm_out_signal[3];
tdmSignal_t tdm_in_signal[3];
#pragma DATA_SECTION (tdm_out_signal, "NON_CACHED_DATA")
#pragma DATA_SECTION (tdm_in_signal, "NON_CACHED_DATA")
const ADT_Int16 sine_1k[8] = { (-13), (23160),  (32767),  (23178), (12), (-23160), (-32767), (-23179)};
										  
//*****************************************************
// EDMA3 definitions
#define EDMA3_TPCC_BASE		0x02740000
#define EDMA3_IERH                (EDMA3_TPCC_BASE + 0x1054) // IERH Control
#define EDMA3_EERH                (EDMA3_TPCC_BASE + 0x1024) // EERH Control
#define EDMA3_ICRH                (EDMA3_TPCC_BASE + 0x1074) // ICRH Control
#define EDMA3_ECRH                (EDMA3_TPCC_BASE + 0x100C) // ECRH Control
#define EDMA3_IER                 (EDMA3_TPCC_BASE + 0x1050) // IER Control
#define EDMA3_EER                 (EDMA3_TPCC_BASE + 0x1020) // EER Control
#define EDMA3_ICR                 (EDMA3_TPCC_BASE + 0x1070) // ICR Control
#define EDMA3_ECR                 (EDMA3_TPCC_BASE + 0x1008) // ECR Control
#define EDMA3_IECRH               (EDMA3_TPCC_BASE + 0x105C) // IECRH Control
#define EDMA3_IECR                (EDMA3_TPCC_BASE + 0x1058) // IECR Control
#define EDMA3_EECRH               (EDMA3_TPCC_BASE + 0x102C) // EECRH Control
#define EDMA3_EECR                (EDMA3_TPCC_BASE + 0x1028) // EECR Control




                    

static void disable_edmaints() {

    *(int*)EDMA3_IECRH = 0xFFFFFFFF;  // IECRH (disable high interrupts enable)
    *(int*)EDMA3_EECRH = 0xFFFFFFFF;  // EECRH (disable high events enable)
    *(int*)EDMA3_ICRH  = 0xFFFFFFFF;  // ICRH  (clear high interrupts pending)
    *(int*)EDMA3_ECRH  = 0xFFFFFFFF;  // ECRH  (clear high events pending)

    *(int*)EDMA3_IECR  = 0xFFFFFFFF;  // IECR  (disable low interrupts enable)
    *(int*)EDMA3_EECR  = 0xFFFFFFFF;  // EECR  (disable low events enable)
    *(int*)EDMA3_ICR   = 0xFFFFFFFF;  // ICR   (clear low interrupts pending)
    *(int*)EDMA3_ECR   = 0xFFFFFFFF;  // ECR   (clear low events pending)

    // Disable interrupts
    //IER = 0;
}
void* CustomNetIn (ADT_UInt32 remoteIp, ADT_UInt16 remotePort, ADT_UInt8 *payload) {
    return (void *)0;
}

#ifdef _INCLUDE_IPv6_CODE
void* CustomNetIn6 (ADT_UInt8 *remoteIp, ADT_UInt16 remotePort, ADT_UInt8 *payload) {
    return (void *)0;
}
#endif

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// customDSPInit - Perform board initialization.
//
// FUNCTION
//   This function is called by main before G.PAK framework initialization.
//
// RETURNS
//   nothing
//
#pragma CODE_SECTION (customDSPInit, "SLOW_PROG_SECT")
void customDSPInit () {
    volatile ADT_UInt32 intstat0;


#ifdef CONFIG_GPIO
// DSP startup procedure using UART:
// 1) Configure GPIO pins 26, 27 as outputs. GPIO26 indicates to host that the UART is ready for input.
// 2) Set GPIO pin 26 high. After the host detects GPIO pin 26 has been set, it may send UART commands to the DSP.
// 3) Enter a UART polling loop waiting for commands from host. 
// 4) If no UART characters are received within 5 seconds of setting GPIO pin 26 high, the UART loop is exited, and execution branches to the G.PAK application.

    // configure pin mux to use gpio 26, 27 
    *pin_control0   |= 0x0C000000;
#endif

    PlayRecLengthI8 = PlayRecEndAddr - PlayRecStartAddr; 

    disable_edmaints();
    // clear pending EDMA interrupts
    //REG_RD (0x02600280, intstat0);
    //REG_WR (0x02600280, intstat0);


    if (DSPCore == 0) {
        init_pll(CPU_FREQ);

    // run_memory_test();
        initIPv6Info();

        #ifdef UART_ENABLED
        // print splash screen to uart 
        // allow host to configure startup IP parameters
        // load local MAC address
        gpak_uart(CPU_FREQ, UART_POLL_TIMEOUT_SECS);
        #endif

        if (static_ip_address) {
            ipConfig->IPAddr = inet_addr(LocalIPAddr);
            ipConfig->IPMask = inet_addr(LocalIPMask);
            ipConfig->GateWayIP = inet_addr(GatewayIP);
        }
        customNetIn = CustomNetIn;

        #ifdef _INCLUDE_IPv6_CODE
        customNetIn6 = CustomNetIn6;
        #endif

    }

    memset(tdm_out_signal, 0, sizeof(tdm_out_signal));
    memset(tdm_in_signal, 0, sizeof(tdm_in_signal));

    init_custom_frames ();

    InitTDMMode ();
    printf ("\n\nTMS320C6678 TI_EVM\n");

    setToneLogMemory (DataLogBuff, sizeof(DataLogBuff));

} 

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// customDSPStartUp - Perform G.PAK initialization.
//
// FUNCTION
//   This function is called by main after G.PAK framework initialization.
//
// ***********  Beware: This function is called more than once!  ***********
//
// RETURNS
//   nothing
//
#pragma CODE_SECTION (customDSPStartUp, "INIT_PROG_SECT:customDSPStartUp")
ADT_Bool dspStartupWasCalled = FALSE;	// flag: customDSPStartUp was called
void customDSPStartUp() {

   if (DSPCore == 0) {
    captureA.pBufrBase = lclCaptureBuffA;
    captureB.pBufrBase = lclCaptureBuffB;
    captureC.pBufrBase = lclCaptureBuffC;
    captureD.pBufrBase = lclCaptureBuffD;

    captureA.BufrSize = dbg_captI16;
    captureB.BufrSize = dbg_captI16;
    captureC.BufrSize = dbg_captI16;
    captureD.BufrSize = dbg_captI16;
    } 
	// Exit if already called once.
	if (dspStartupWasCalled)
	{
		return;
	}
	dspStartupWasCalled = TRUE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// customTimer - Process timer.
//
// FUNCTION
//   This function is called every millisecond by the G.PAK timer thread.
//
// RETURNS
//   nothing
//


#pragma CODE_SECTION(customTimer, "FAST_PROG_SECT")
void customTimer() {
Uint32 curr_time;
    curr_time = CLK_gethtime();
    delta_1ms = curr_time - prev_1ms;
/*	
    if (curr_time < prev_1ms) {
        if (max_wrap < prev_1ms) {
            max_wrap = prev_1ms;
        }
    }
*/	
    prev_1ms = curr_time;   
    rtcp_debug_ntptime(); 

#ifdef UART_ENABLED
   uart_keepalive();
#endif

#ifndef AVAYA_INTEGRATION
   gen_udp_timer();
#endif

#ifdef TG_TEST
    if ((DSPCore == 1) && tg.cmd) {
        custom_cmd = tg.cmd;
        tg.cmd = 0;
        tg.init = 1;
        ApiBlock.CmdMsgLength = 1;
    }
#endif

}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// customPktIn - Process custom in-bound packet.
//
// FUNCTION
//   This function is called when an in-bound packet is received.
//
// RETURNS
//   Pointer to packet data.
//
#pragma CODE_SECTION(customPktIn, "FAST_PROG_SECT")
void *customPktIn (void *Hdr, ADT_UInt16 *Payload) {
    return Payload; 
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// customPktOut - Process custom out-bound packet.
//
// FUNCTION
//   This function is called by the Frame thread when an out-bound packet is
//   built prior to sending.
//
// RETURNS
//   Pointer to packet data.
//
#pragma CODE_SECTION(customPktOut, "MEDIUM_PROG_SECT")
void *customPktOut (void *Hdr, ADT_UInt16 *Payload) { return Payload; }

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// customTDMIn - Process custom in-bound TDM streams.
//
// FUNCTION
//   This function is called by the DMA thread before writing DMA input buffers
//   to channel data input buffers.
//
// RETURNS
//   Zero.
//
#pragma CODE_SECTION(customTDMIn, "FAST_PROG_SECT")
int customTDMIn (ADT_PCM16* DataIn,  int port, int slotCnt) {
int i, j;
ADT_PCM16 *pSlot;
tdmSignal_t *pSig = &tdm_in_signal[port];
ADT_PCM16 *pVect;

    if (pSig->slotmask && pSig->mode) {
      switch (pSig->mode) {

        case 1:  // fixed pattern
            for (i=0; i<slotCnt; i++) {
                if (((1<<i) & pSig->slotmask) == 0)
                    continue;
                pSlot = &DataIn[i*sysConfig.samplesPerMs];
                for (j=0; j<sysConfig.samplesPerMs; j++)
                    pSlot[j] = pSig->pattern;
            }
        break;

        case 2:  // tonegen
            for (i=0; i<slotCnt; i++) {
                if (((1<<i) & pSig->slotmask) == 0)
                    continue;
                pSlot = &DataIn[i*sysConfig.samplesPerMs];
                for (j=0; j<sysConfig.samplesPerMs; j++)
                    pSlot[j] = sine_1k[j];
            }
        break;

       case 3: // play captA vect
            pVect = (ADT_PCM16 *)&lclCaptureBuffA[pSig->playA_index];
            pSig->playA_index += 8;
            if (pSig->playA_index >= dbg_captI16)
                pSig->playA_index -= dbg_captI16;

            for (i=0; i<slotCnt; i++) {
                if (((1<<i) & pSig->slotmask) == 0)
                    continue;
                pSlot = &DataIn[i*sysConfig.samplesPerMs];
                for (j=0; j<sysConfig.samplesPerMs; j++)
                    pSlot[j] = pVect[j];
            }
       break;

       case 4: // play captB vect
            pVect = (ADT_PCM16 *)&lclCaptureBuffB[pSig->playB_index];
            pSig->playB_index += 8;
            if (pSig->playB_index >= dbg_captI16)
                pSig->playB_index -= dbg_captI16;

            for (i=0; i<slotCnt; i++) {
                if (((1<<i) & pSig->slotmask) == 0)
                    continue;
                pSlot = &DataIn[i*sysConfig.samplesPerMs];
                for (j=0; j<sysConfig.samplesPerMs; j++)
                    pSlot[j] = pVect[j];
            }
       break;
     }
    }

    return 0; 
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// customTDMOut - Process custom out-bound TDM streams.
//
// FUNCTION
//   This function is called by the DMA thread after writing channel data to DMA
//   output buffers.
//
// RETURNS
//   Zero.
//
#pragma CODE_SECTION(customTDMOut, "FAST_PROG_SECT")
int customTDMOut (ADT_PCM16* DataOut, int port, int slotCnt) {
int i, j;
ADT_PCM16 *pSlot;
tdmSignal_t *pSig = &tdm_out_signal[port];
ADT_PCM16 *pVect;

    if (pSig->slotmask && pSig->mode) {
      switch (pSig->mode) {

        case 1:  // fixed pattern
            for (i=0; i<slotCnt; i++) {
                if (((1<<i) & pSig->slotmask) == 0)
                    continue;
                pSlot = &DataOut[i*sysConfig.samplesPerMs];
                for (j=0; j<sysConfig.samplesPerMs; j++)
                    pSlot[j] = pSig->pattern;
            }
        break;

        case 2:  // tonegen 
            for (i=0; i<slotCnt; i++) {
                if (((1<<i) & pSig->slotmask) == 0)
                    continue;
                pSlot = &DataOut[i*sysConfig.samplesPerMs];
                for (j=0; j<sysConfig.samplesPerMs; j++)
                    pSlot[j] = sine_1k[j];
            }
        break;

       case 3: // play captA vect
            pVect = (ADT_PCM16 *)&lclCaptureBuffA[pSig->playA_index];
            pSig->playA_index += 8;
            if (pSig->playA_index >= dbg_captI16)
                pSig->playA_index -= dbg_captI16;

            for (i=0; i<slotCnt; i++) {
                if (((1<<i) & pSig->slotmask) == 0)
                    continue;
                pSlot = &DataOut[i*sysConfig.samplesPerMs];
                for (j=0; j<sysConfig.samplesPerMs; j++)
                    pSlot[j] = pVect[j];
            }
       break;

       case 4: // play captB vect
            pVect = (ADT_PCM16 *)&lclCaptureBuffB[pSig->playB_index];
            pSig->playB_index += 8;
            if (pSig->playB_index >= dbg_captI16)
                pSig->playB_index -= dbg_captI16;

            for (i=0; i<slotCnt; i++) {
                if (((1<<i) & pSig->slotmask) == 0)
                    continue;
                pSlot = &DataOut[i*sysConfig.samplesPerMs];
                for (j=0; j<sysConfig.samplesPerMs; j++)
                    pSlot[j] = pVect[j];
            }
       break;

      }
    }

   return 0; 
}


//}----------------------------------------------------------------
//  VAD Processing - Allows custom voice activity detectin
//
//  NOTE: Setting *vad = 1 allows overriding silence periods
//{----------------------------------------------------------------
#pragma CODE_SECTION (customVad, "MEDIUM_PROG_SECT")
void customVad (int *vad) { return; }

//}----------------------------------------------------------------
//  Codec/Frame Processing - Allows custom encoding and data packing
//
//  NOTE: These routines must return the payload size in octets to statisfy
//        G.PAK requirements for packet management
//{----------------------------------------------------------------
#pragma CODE_SECTION (customEncodeInit,  "MEDIUM_PROG_SECT")
#pragma CODE_SECTION (customDecodeInit,  "MEDIUM_PROG_SECT")
#pragma CODE_SECTION (customEncode,      "MEDIUM_PROG_SECT")
#pragma CODE_SECTION (customDecode,      "MEDIUM_PROG_SECT")
#pragma CODE_SECTION (customPack,        "MEDIUM_PROG_SECT")
#pragma CODE_SECTION (customUnpack,      "MEDIUM_PROG_SECT")

int customEncodeInit (pcm2pkt_t *PcmPkt) { return 0; }
int customDecodeInit (pkt2pcm_t *PktPcm) { return 0; }
int customEncode     (chanInfo_t *pChan, void *pcm, void *params) { return 0; }
int customDecode     (chanInfo_t *pChan, void *params, void *pcm) { return 0; }
int customPack       (void *params, void *payload) { return 0; }
int customUnpack     (PktHdr_t *PktHdr, void *payload, void *params) { return 0; }

//}-------------------------------------------------------------------
// SetHostPortInterrupt - Notify host with HPI or PCI interrupt
//                        Allows processing on a frame boundary
//  FrameSize = 0 is an indication that an event message is available.
//            = non-0 is an indication that the corresponding framing task
//              has completed
//{----------------------------------------------------------------
far int generateWarningInterrupts = TRUE;
#pragma CODE_SECTION (SetHostPortInterrupt, "MEDIUM_PROG_SECT")
void SetHostPortInterrupt (int frameSamples) { 

   // Generate host interrupts (both PCI and HPI) 
   // Turn off warning interrupts when framing interrupts are encountered
   if (frameSamples != 0)
      generateWarningInterrupts = FALSE;

   if (generateWarningInterrupts || frameSamples != 0)
       Gen_Host_Interrupt ();

   return;
}

//}----------------------------------------------------------------
//  Message Processing - Allows custom host<->DSP messaging
//
//  ProcessUserTest - testID = UserDefined, param = 200 ==> Configure TDM for TI EVM.
//  customMsg       - testMode = 1 ==> read dsp memory via gpakTestMode:CustomVarLen messaging
//                    testMode = MSG_PRE_RTP_HDR ==> gpakPreRTPHeader messaging
//  customIdleFunc  - set IP stack debug level
//{----------------------------------------------------------------

#pragma CODE_SECTION (ProcessUserTest, "SLOW_PROG_SECT")
int ProcessUserTest (ADT_UInt16 testID, ADT_UInt16 param, ADT_UInt16 *rply) {

   *rply = Tm_InvalidTest;
   if (testID != UserDefined) return 0;
   switch (param) {
   default:   
       break;
   };

   return 1;
}

void getVersionInfo(versionInfo_t *ver) {
    sprintf((char *)ver->buf,"\r\nGpak C6678 version %s, %s, %s \r\n",GPAK_BUILD_VERSION,__DATE__,__TIME__);
    ver->length = strlen((char *)ver->buf);
    //printf("%s",ver->buf);
}

//----  customMsg
//
//    Process non-standard G.PAK messages received from host
//
//   Return value is number of bytes in reply message
// The Cmd buffer holds the following upon entering:
//     Cmd[0] == test mode parameter value...used to identify the custom message
//     Cmd[1] == number of I16 elements in the custom message (includes the parameter element at Cmd[0]
//     Cmd[2]... start of custom message data.
#pragma CODE_SECTION (customMsg,       "SLOW_PROG_SECT")
int customMsg (ADT_UInt16 *Cmd, ADT_UInt16 *Reply) { 
   ADT_UInt16 cmdID;
   ADT_UInt16 replyI16;

   cmdID = (Cmd[0] >> 8) & 0xff;
   switch (cmdID) {
    case 2:  // param == 2, read version string
        replyI16   = Cmd[2];    // custom message expected reply length I16
        getVersionInfo(&gpakVersion);
        if (gpakVersion.length > replyI16*sizeof(ADT_UInt16)) return -1;

        memset(Reply, 0, replyI16*sizeof(ADT_UInt16)); 
        memcpy(Reply, &gpakVersion.buf[0], gpakVersion.length); 
        return (replyI16*sizeof(ADT_UInt16));
        default:
        break;
   };
   return 0;
}

//}----------------------------------------------------------------
//  Idle Processing
//
// ToggleFrameTaskLED   - Toggles LED from framing task every 30 milliseconds
//{----------------------------------------------------------------
static ADT_Bool firstIdle = TRUE;

#ifdef IPC_SUPPORT
volatile int send_ipc_message = 0;
uint8_t ipcMessage[GPAK_IPC_MAX_MSGLENI8];
#endif

#pragma CODE_SECTION (customIdleFunc,     "SLOW_PROG_SECT")
void customIdleFunc ()
{

	if (firstIdle)
	{
		if (DSPCore == 0)
		{
#if 0
#ifdef EVM
			setIPStackDebugLevel (2);
#else
			setIPStackDebugLevel (3);
#endif
#endif
		}
		firstIdle = FALSE;
        initWaveHeap ();
	}

#ifdef IPC_SUPPORT
    if ((DSPCore == 0) && (send_ipc_message)) {
        ipcMessage[0] = 101;
        ipcMessage[1] = 102;
        ipcMessage[2] = 103;
        ipcMessage[3] = 104;
        ipcMessage[4] = 105;
        gpak_msgQ_send (1, ipcMessage, 5, DSPTotalCores,1);
        ipcMessage[0] = 0;
        ipcMessage[1] = 0;
        ipcMessage[2] = 0;
        ipcMessage[3] = 0;
        ipcMessage[4] = 0;
        send_ipc_message = 0;
    }
#endif

    cacheTest();

	return; 
}

#pragma CODE_SECTION (ToggleFrameTaskLED, "MEDIUM_PROG_SECT")
void ToggleFrameTaskLED () {  return; }

#ifdef TG_TEST
void toneGenControl (ADT_UInt16 ChannelId, GpakToneGenCmd_t  ToneCmd, int toNet) {
   chanInfo_t  *pChan; 
   TGInfo_t     *tgInfo;
   TGParams_1_t *tgParams;

   pChan = chanTable[ChannelId];

   if (toNet)
      tgInfo = &(pChan->pcmToPkt.TGInfo);
   else
      tgInfo = &(pChan->pktToPcm.TGInfo);

   tgInfo->update = Disabled;
   if (ToneCmd == ToneGenStart) {
        tgParams = tgInfo->toneGenParmPtr;
        CLEAR_INST_CACHE (tgParams, sizeof (TGParams_1_t));
        tgParams->ToneType    = TgContinuous; 
        tgParams->Level[0]    = -100;
        tgParams->Level[1]    = 0;
        tgParams->Level[2]    = 0;
        tgParams->Level[3]    = 0;
        tgParams->NOnOffs     = 0;
        tgParams->NFreqs      = 1;  
        tgParams->OnTime[0]   = 0;
        tgParams->OnTime[1]   = 0;
        tgParams->OnTime[2]   = 0;
        tgParams->OnTime[3]   = 0;
        tgParams->OffTime[0]  = 0;
        tgParams->OffTime[1]  = 0;
        tgParams->OffTime[2]  = 0;
        tgParams->OffTime[3]  = 0;
        tgParams->Freq[0]  = 10000;
        tgParams->Freq[1]  = 0;
        tgParams->Freq[2]  = 0;
        tgParams->Freq[3]  = 0;
        FLUSH_INST_CACHE (tgParams, sizeof (TGParams_1_t));
   }
   tgInfo->cmd = ToneCmd;
   FLUSH_wait ();
   tgInfo->update = Enabled;
}




void tg_sin1k(short *pcmBuff, int FrameSize) {
int i,j,n;
short *ptr = pcmBuff;
short sin1k[8] = { (-13), (23160),  (32767),  (23178), (12),  (-23160), (-32767), (-23179)};
    n=FrameSize/8;
    for (i=0; i<n; i++) {
        for(j=0; j<8; j++) {
            ptr[j] = sin1k[j];
        }
        ptr += 8;
    }
}

void toneGen(ADT_PCM16 *pcmBuff, int FrameSize) {

    if (tg.enab == 0)    
        return;

    if (tg.init) {
        tg.init = 0;
        tg.Params.ToneType    = TgContinuous; 
        tg.Params.Level[0]    = -100;
        tg.Params.Level[1]    = 0;
        tg.Params.Level[2]    = 0;
        tg.Params.Level[3]    = 0;
        tg.Params.NOnOffs     = 0;
        tg.Params.NFreqs      = 1;  
        tg.Params.OnTime[0]   = 0;
        tg.Params.OnTime[1]   = 0;
        tg.Params.OnTime[2]   = 0;
        tg.Params.OnTime[3]   = 0;
        tg.Params.OffTime[0]  = 0;
        tg.Params.OffTime[1]  = 0;
        tg.Params.OffTime[2]  = 0;
        tg.Params.OffTime[3]  = 0;
        tg.Params.Freq[0]  = 10000;
        tg.Params.Freq[1]  = 0;
        tg.Params.Freq[2]  = 0;
        tg.Params.Freq[3]  = 0;
        TG_ADT_init_highres (&tg.Inst, &tg.Params);
    }
    TG_ADT_generate (&tg.Inst, FrameSize, tg.pcm);

    if (tg.enab == 2)
        tg_sin1k(tg.pcm, FrameSize);

    if (tg.copy) 
        memcpy(pcmBuff, tg.pcm, sizeof(short)*FrameSize);
}
#endif


void process_custom_cmd() {
    switch (custom_cmd) {
#ifdef TG_TEST
            case 1:
            toneGenControl(tg.chan,ToneGenStart,tg.dir); // start tone
            break;

            case 2:
            toneGenControl(tg.chan,ToneGenStop,tg.dir); // start tone
            break;
#endif
            case 3:
      //      custom_pcm_pcm (1);
            break;

            case 4:
      //      custom_pcm_pcm (0);
            break;

            case 5:
       //     custom_chan_status ();
            break;

            case 6:
     //       custom_fixed_value ();
            break;

            case 7:
      //      custom_start_port_evm ();
            break;

            case 8:
      //      process_led ();
            break;

            default:
            break;
        }
}

#define TEARDOWN_STATE_BUFLEN 32
typedef struct tdState_t {
    ADT_UInt32 chanId;
    ADT_UInt32 td_active;
    ADT_UInt32 idx;
    ADT_UInt32 code[TEARDOWN_STATE_BUFLEN];
} tdState_t;
tdState_t tdState = {0, 0, 0, {(ADT_UInt32)-1}};


#pragma CODE_SECTION (teardownLog, "MEDIUM_PROG_SECT")
void teardownLog(ADT_UInt32 chanId, ADT_UInt16 code) { 
    ADT_UInt16 i;
    tdState_t *state;

    state = &tdState;

    if (chanId != state->chanId)
        return;

    // reset teardown log on channel setup
    if (code == 0xffff) {
        state->td_active = 0;
        state->idx = 0;
        for (i=0; i<TEARDOWN_STATE_BUFLEN; i++) {
            state->code[i] = (ADT_UInt32)-1;
        }
        return;
    }

    // teardown log begins
    if ((code == 0) && (state->td_active == 0)){
        state->td_active = 1;
    }

    if ((state->td_active) && (state->idx < TEARDOWN_STATE_BUFLEN)) {
        state->code[state->idx] = code;
        state->idx += 1;
    }
}

typedef struct playrec_t {
    char cmd;           // S==stop,P==play,R==record
    char bufname;       // A,B,C,D
    int  chan;
    int  active;
    ADT_UInt32 playA;  // ABuff playback take index   
    ADT_UInt32 playB;  // BBuff playback take index  
    ADT_UInt32 playC;  // CBuff playback take index  
    ADT_UInt32 playD;  // DBuff playback take index  
} playrec_t;
far playrec_t playrec = {0,0,0,0,0,0,0,0};
#pragma DATA_SECTION (playrec, "NON_CACHED_DATA")

#pragma CODE_SECTION (play_record, "MEDIUM_PROG_SECT")
void  play_record(int chanId, void *pcmBuff, int SampsPerFrame, int code) {
ADT_Int16 *pSrc, *pDst;
ADT_UInt32 *pRecIndex;
ADT_UInt32 *pPlayIndex;
int active = 0;
int j;

    if (playrec.chan != chanId) return; 

    if (playrec.cmd) {
        switch (playrec.bufname) {
            case 'a': case 'A':
            pRecIndex = &captureA.PutIndex;
            pPlayIndex = &playrec.playA;
            active =  PLAYREC_A;
            break;                                              
    
            case 'b': case 'B':
            pRecIndex = &captureB.PutIndex;
            pPlayIndex = &playrec.playB;
            active =  PLAYREC_B;
            break;                                              
    
            case 'c': case 'C':
            pRecIndex = &captureC.PutIndex;
            pPlayIndex = &playrec.playC;
            active =  PLAYREC_C;
            break;                                              
    
            case 'd': case 'D':
            pRecIndex = &captureD.PutIndex;
            pPlayIndex = &playrec.playD;
            active =  PLAYREC_D;
            break;                                              
    
            default:
            break;
        }

        if ((playrec.cmd == 's') || (playrec.cmd == 'S')) {
            active = 0;
        } 
        else if ((playrec.cmd == 'r') || (playrec.cmd == 'R')) {
            *pRecIndex = 0;
            active |= PLAYREC_REC;
        } else if ((playrec.cmd == 'p') || (playrec.cmd == 'P')) {
            *pPlayIndex = 0;
            active |= PLAYREC_PLAY;
        }
        playrec.active = active;
        playrec.cmd = 0;
    }

    if ((code & playrec.active) != code) return;

    switch (code)
    {
        case (PLAYREC_A | PLAYREC_REC):
        if (captureSignal (&captureA, pcmBuff, SampsPerFrame))
            playrec.active &= ~code;
        break;

        case (PLAYREC_B | PLAYREC_REC):
        if (captureSignal (&captureB, pcmBuff, SampsPerFrame))
            playrec.active &= ~code;
        break;

        case (PLAYREC_C | PLAYREC_REC):
        if (captureSignal (&captureC, pcmBuff, SampsPerFrame))
            playrec.active &= ~code;
        break;

        case (PLAYREC_D | PLAYREC_REC):
        if (captureSignal (&captureD, pcmBuff, SampsPerFrame))
            playrec.active &= ~code;
        break;

        case (PLAYREC_A | PLAYREC_PLAY):
        pDst = (ADT_Int16 *)pcmBuff;
        pSrc = (ADT_Int16 *)&captureA.pBufrBase[playrec.playA];
        playrec.playA += (ADT_UInt32)SampsPerFrame;
        if (playrec.playA >=  dbg_captI16)
            playrec.playA -= dbg_captI16;
        for (j=0; j<SampsPerFrame; j++)
            pDst[j] = pSrc[j];
        break;

        case (PLAYREC_B | PLAYREC_PLAY):
        pDst = (ADT_Int16 *)pcmBuff;
        pSrc = (ADT_Int16 *)&captureB.pBufrBase[playrec.playB];
        playrec.playB += (ADT_UInt32)SampsPerFrame;
        if (playrec.playB >=  dbg_captI16)
            playrec.playB -= dbg_captI16;
        for (j=0; j<SampsPerFrame; j++)
            pDst[j] = pSrc[j];
        break;

        case (PLAYREC_C | PLAYREC_PLAY):
        pDst = (ADT_Int16 *)pcmBuff;
        pSrc = (ADT_Int16 *)&captureC.pBufrBase[playrec.playC];
        playrec.playC += (ADT_UInt32)SampsPerFrame;
        if (playrec.playC >=  dbg_captI16)
            playrec.playC -= dbg_captI16;
        for (j=0; j<SampsPerFrame; j++)
            pDst[j] = pSrc[j];
        break;

        case (PLAYREC_D | PLAYREC_PLAY):
        pDst = (ADT_Int16 *)pcmBuff;
        pSrc = (ADT_Int16 *)&captureD.pBufrBase[playrec.playD];
        playrec.playD += (ADT_UInt32)SampsPerFrame;
        if (playrec.playD >=  dbg_captI16)
            playrec.playD -= dbg_captI16;
        for (j=0; j<SampsPerFrame; j++)
            pDst[j] = pSrc[j];
        break;
    }
    
}

//void wordUnlock (int *lockField) {return;}
//int wordLock (int DspCore, int *lockField) {return 0;}
   
#ifdef CACHE_TEST //-----------------------
typedef struct cache_data_t {
int cmd;
ADT_UInt32 *addr;
ADT_UInt32 lenI8;
ADT_UInt32 rd_val[2];
} cache_data_t;
cache_data_t cache_data = {-1};

ADT_UInt32 cache_buf[128/sizeof(ADT_UInt32)];
#pragma DATA_SECTION (cache_buf,  "SLOW_DATA_SECT")
#pragma DATA_ALIGN   (cache_buf, 128)

void cacheTest() {
    if (cache_data.cmd < 0) {
        cache_data.addr =  cache_buf;
        cache_data.lenI8 = sizeof(cache_buf);
        cache_data.rd_val[0] = 0xffffffff;
        cache_data.rd_val[1] = 0xffffffff;
        cache_data.cmd = 0;
    }

    switch (cache_data.cmd) {
        case 0:
        return;

        case 1:
        // cache invalidate
        CLEAR_INST_CACHE(cache_data.addr, cache_data.lenI8);
        FLUSH_wait();
        break;

        case 2:
        // cache writeback, invalidate
        FLUSH_INST_CACHE(cache_data.addr, cache_data.lenI8);
        FLUSH_wait();
        break;

        case 3:
        // fill with non-zero
        cache_data.addr[0] =  0x12345678;
        cache_data.addr[1] =  0xabcdef07;
        break;

        case 4:
        // fill with zero
        cache_data.addr[0] = 0;         
        cache_data.addr[1] = 0;         
        break;

        case 5:
        // read
        cache_data.rd_val[0] = cache_data.addr[0];         
        cache_data.rd_val[1] = cache_data.addr[1];         
        break;

    }
    cache_data.cmd = 0;
    
}


#endif

#if 0
extern int CSL_chipReadReg (int reg);
extern int boot_reset0;
extern int boot_reset1;
extern int boot_reset2;
extern int boot_reset3;
extern int boot_reset4;
extern int boot_reset5;
extern int boot_reset6;
extern int boot_reset7;
extern int first_reset0;
extern int first_reset1;
extern int first_reset2;
extern int first_reset3;
extern int first_reset4;
extern int first_reset5;
extern int first_reset6;
extern int first_reset7;
extern int last_reset0;
extern int last_reset1;
extern int last_reset2;
extern int last_reset3;
extern int last_reset4;
extern int last_reset5;
extern int last_reset6;
extern int last_reset7;
extern int boot_enter;
extern int first_enter;
extern int last_enter;

void gpak_boot_reset() {
int Core;

   boot_enter = 0xdeadbeef;
   Core = CSL_chipReadReg (17);  // Read which core this system is running on.
   if (Core == 0)
        boot_reset0 = 1;
   else if (Core == 1)
        boot_reset1 = 1;
   else if (Core == 2)
        boot_reset2 = 1;
   else if (Core == 3)
        boot_reset3 = 1;
   else if (Core == 4)
        boot_reset4 = 1;
   else if (Core == 5)
        boot_reset5 = 1;
   else if (Core == 6)
        boot_reset6 = 1;
   else if (Core == 7)
        boot_reset7 = 1;
 }

void gpak_boot_first() { 
int   Core;

   first_enter = 0xdeadbeef;
   Core = CSL_chipReadReg (17);
   if (Core == 0)
        first_reset0 = 1;
   else if (Core == 1)
        first_reset1 = 1;
   else if (Core == 2)
        first_reset2 = 1;
   else if (Core == 3)
        first_reset3 = 1;
   else if (Core == 4)
        first_reset4 = 1;
   else if (Core == 5)
        first_reset5 = 1;
   else if (Core == 6)
        first_reset6 = 1;
   else if (Core == 7)
        first_reset7 = 1;
}


void gpak_boot_last() { 
int Core;

   last_enter = 0xdeadbeef;
   Core = CSL_chipReadReg (17);
   if (Core == 0)
        last_reset0 = 1;
   else if (Core == 1)
        last_reset1 = 1;
   else if (Core == 2)
        last_reset2 = 1;
   else if (Core == 3)
        last_reset3 = 1;
   else if (Core == 4)
        last_reset4 = 1;
   else if (Core == 5)
        last_reset5 = 1;
   else if (Core == 6)
        last_reset6 = 1;
   else if (Core == 7)
        last_reset7 = 1;
}
#pragma CODE_SECTION (gpak_boot_reset, "SLOW_PROG_SECT")
#pragma CODE_SECTION (gpak_boot_first, "SLOW_PROG_SECT")
#pragma CODE_SECTION (gpak_boot_last, "SLOW_PROG_SECT")

#endif
