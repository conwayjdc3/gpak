#define _CSL_H_         // Avoid csl definitions
#include <std.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "avayaC6678.h"
#include "GpakExts.h"
#include "framestats.h"

extern ADT_Bool InitCustomFrameTask (int halfMSPerFrame, int generateInt,
                             void *workBuff,  int workBuffI16,
                             void *saScratch, int saScratchI16,
                             void *daScratch, int daScratchI16);

//  Custom Frame Data Structures
typedef TDScratch_t GPAK_TD_Scratch;
typedef int GPAK_Cnfr_Scratch;

ADT_UInt32 workBuff40ms[3*40*8];
ADT_UInt16 G168DAscratch_40ms [1040];
union {
   ADT_UInt64        alignment;
   GPAK_TD_Scratch   TDScratch;
   GPAK_Cnfr_Scratch CnfrScratch;
   ADT_UInt16        SAScratch [1818];
   ADT_UInt32        SRTPScratch [ 80];
} G168SAscratch_40ms;
#pragma DATA_SECTION (workBuff40ms,    "IN_WORK")
#pragma DATA_SECTION (G168DAscratch_40ms, "SLOW_SCRATCH")
#pragma DATA_SECTION (G168SAscratch_40ms, "FAST_SCRATCH")


ADT_UInt32 workBuff50ms[3*50*8];
ADT_UInt16 G168DAscratch_50ms [1040];
union {
   ADT_UInt64        alignment;
   GPAK_TD_Scratch   TDScratch;
   GPAK_Cnfr_Scratch CnfrScratch;
   ADT_UInt16        SAScratch [2178];
   ADT_UInt32        SRTPScratch [ 80];
} G168SAscratch_50ms;
#pragma DATA_SECTION (workBuff50ms,    "IN_WORK")
#pragma DATA_SECTION (G168DAscratch_50ms, "SLOW_SCRATCH")
#pragma DATA_SECTION (G168SAscratch_50ms, "FAST_SCRATCH")


ADT_UInt32 workBuff60ms[3*60*8];
ADT_UInt16 G168DAscratch_60ms [1040];
union {
   ADT_UInt64        alignment;
   GPAK_TD_Scratch   TDScratch;
   GPAK_Cnfr_Scratch CnfrScratch;
   ADT_UInt16        SAScratch [2538];
   ADT_UInt32        SRTPScratch [ 80];
} G168SAscratch_60ms;
#pragma DATA_SECTION (workBuff60ms,    "IN_WORK")
#pragma DATA_SECTION (G168DAscratch_60ms, "SLOW_SCRATCH")
#pragma DATA_SECTION (G168SAscratch_60ms, "FAST_SCRATCH")


// Configure custom frame rates of: 40 ms, 50 ms, and 60 ms.
void init_custom_frames () {

   InitCustomFrameTask (40*2, 0,          
        &workBuff40ms,  sizeof (workBuff40ms) / sizeof (ADT_UInt16),
        &G168SAscratch_40ms, sizeof (G168SAscratch_40ms) / sizeof (ADT_UInt16),
        &G168DAscratch_40ms, sizeof (G168DAscratch_40ms) / sizeof (ADT_UInt16));

   InitCustomFrameTask (50*2, 0,          
        &workBuff50ms,  sizeof (workBuff50ms) / sizeof (ADT_UInt16),
        &G168SAscratch_50ms, sizeof (G168SAscratch_50ms) / sizeof (ADT_UInt16),
        &G168DAscratch_50ms, sizeof (G168DAscratch_50ms) / sizeof (ADT_UInt16));

   InitCustomFrameTask (60*2, 0,          
        &workBuff60ms,  sizeof (workBuff60ms) / sizeof (ADT_UInt16),
        &G168SAscratch_60ms, sizeof (G168SAscratch_60ms) / sizeof (ADT_UInt16),
        &G168DAscratch_60ms, sizeof (G168DAscratch_60ms) / sizeof (ADT_UInt16));
}
