#include <stdio.h>


#define PLL1_M_1000 19  // 1000 MHz operation: 
#define PLL1_M_850 16   // 850 MHz operation
#define PLL1_D 0

// Global Register and constant definitions
#define REF_CLOCK_KHZ 100000

// Global timeout value
#define GTIMEOUT 1000

//*****************************************************
// Power definitions
#define PSC_BASE            0x02350000
#define PSC_PTCMD           *( unsigned int* )( PSC_BASE+0x120 )
#define PSC_PTSTAT          *( unsigned int* )( PSC_BASE+0x128 )
#define PSC_PDCTL_BASE      ( PSC_BASE+0x300 )
#define PSC_MDSTAT_BASE     ( PSC_BASE+0x800 )
#define PSC_MDCTL_BASE      ( PSC_BASE+0xA00 )

//#define LPSC_EMIF25_SPI 3 // EMIF16
#define LPSC_EMAC		3
//PD 12
#define LPSC_VCP20 		4

#define LPSC_DEBUGSS_TRC 	5
// PD 1
#define LPSC_TETB_TRC 		6
// PD 3
#define LPSC_PCIEX 		10
// PD 4
#define LPSC_SRIO 		11
// PD 5
#define LPSC_Hyperbridge 	12
// PD 7
#define LPSC_MSMCSRAM 		14
// PD 11
#define LPSC_TCP3d		19
// PD 12
#define LPSC_VCP21		20
// PD 13
#define LPSC_TMR0		23
// PD 14
#define LPSC_TMR1		24

// Power domains definitions
#define PD0         (0)     // Power Domain-0
#define PD1         (1)     // Power Domain-1
#define PD2         (2)     // Power Domain-2
#define PD3         (3)     // Power Domain-3
#define PD4         (4)     // Power Domain-4
#define PD5         (5)     // Power Domain-5
#define PD6         (6)     // Power Domain-6
#define PD7         (7)     // Power Domain-7
#define PD8         (8)     // Power Domain-8
#define PD9         (9)     // Power Domain-9
#define PD10        (10)    // Power Domain-10
#define PD11        (11)    // Power Domain-11
#define PD12        (12)    // Power Domain-12
#define PD13        (13)    // Power Domain-13
#define PD14        (14)    // Power Domain-14
#define PD15        (15)    // Power Domain-15
#define PD16        (16)    // Power Domain-16
#define PD17        (17)    // Power Domain-17

#define PSC_SYNCRESET (0x1)
#define PSC_DISABLE   (0x2)
#define PSC_ENABLE    (0x3)

//*****************************************************
// BOOT and CONFIG dsp system modules Definitions
#define CHIP_LEVEL_REG  0x02620000
#define DEVSTAT                     (*(unsigned int*)(CHIP_LEVEL_REG + 0x0020))
#define KICK0                       (*(unsigned int*)(CHIP_LEVEL_REG + 0x0038))
#define KICK1                       (*(unsigned int*)(CHIP_LEVEL_REG + 0x003C))
#define TINPSEL                     (*(unsigned int*)(CHIP_LEVEL_REG + 0x0300))
#define TOUTPSEL                    (*(unsigned int*)(CHIP_LEVEL_REG + 0x0304))
#define MAINPLLCTL0                 (*(unsigned int*)(CHIP_LEVEL_REG + 0x0328))
#define MAINPLLCTL1                 (*(unsigned int*)(CHIP_LEVEL_REG + 0x032C))
#define DDR3PLLCTL0                 (*(unsigned int*)(CHIP_LEVEL_REG + 0x0330))
#define DDR3PLLCTL1                 (*(unsigned int*)(CHIP_LEVEL_REG + 0x0334))
#define OBSCLKCTL                   (*(unsigned int*)(CHIP_LEVEL_REG + 0x03AC)) //TODO:find out obsc, it may not require. as it useful for PA

// DDR3 tuning registers
#define DATA0_GTLVL_INIT_RATIO      (*(unsigned int*)(CHIP_LEVEL_REG + 0x043C))
#define DATA1_GTLVL_INIT_RATIO      (*(unsigned int*)(CHIP_LEVEL_REG + 0x0440))
#define DATA2_GTLVL_INIT_RATIO      (*(unsigned int*)(CHIP_LEVEL_REG + 0x0444))
#define DATA3_GTLVL_INIT_RATIO      (*(unsigned int*)(CHIP_LEVEL_REG + 0x0448))
#define DATA4_GTLVL_INIT_RATIO      (*(unsigned int*)(CHIP_LEVEL_REG + 0x044C))
#define DATA5_GTLVL_INIT_RATIO      (*(unsigned int*)(CHIP_LEVEL_REG + 0x0450))
#define DATA6_GTLVL_INIT_RATIO      (*(unsigned int*)(CHIP_LEVEL_REG + 0x0454))
#define DATA7_GTLVL_INIT_RATIO      (*(unsigned int*)(CHIP_LEVEL_REG + 0x0458))
#define DATA8_GTLVL_INIT_RATIO      (*(unsigned int*)(CHIP_LEVEL_REG + 0x045C))

#define DATA0_WRLVL_INIT_RATIO      (*(unsigned int*)(CHIP_LEVEL_REG + 0x040C))
#define DATA1_WRLVL_INIT_RATIO      (*(unsigned int*)(CHIP_LEVEL_REG + 0x0410))
#define DATA2_WRLVL_INIT_RATIO      (*(unsigned int*)(CHIP_LEVEL_REG + 0x0414))
#define DATA3_WRLVL_INIT_RATIO      (*(unsigned int*)(CHIP_LEVEL_REG + 0x0418))
#define DATA4_WRLVL_INIT_RATIO      (*(unsigned int*)(CHIP_LEVEL_REG + 0x041C))
#define DATA5_WRLVL_INIT_RATIO      (*(unsigned int*)(CHIP_LEVEL_REG + 0x0420))
#define DATA6_WRLVL_INIT_RATIO      (*(unsigned int*)(CHIP_LEVEL_REG + 0x0424))
#define DATA7_WRLVL_INIT_RATIO      (*(unsigned int*)(CHIP_LEVEL_REG + 0x0428))
#define DATA8_WRLVL_INIT_RATIO      (*(unsigned int*)(CHIP_LEVEL_REG + 0x042C))

#define DDR3_CONFIG_REG_0           (*(unsigned int*)(CHIP_LEVEL_REG + 0x0404))
#define DDR3_CONFIG_REG_1           (*(unsigned int*)(CHIP_LEVEL_REG + 0x0408))
#define DDR3_CONFIG_REG_12          (*(unsigned int*)(CHIP_LEVEL_REG + 0x0434))
#define DDR3_CONFIG_REG_23          (*(unsigned int*)(CHIP_LEVEL_REG + 0x0460))
#define DDR3_CONFIG_REG_24          (*(unsigned int*)(CHIP_LEVEL_REG + 0x0464))

#define DDR3_CONFIG_REG_52 		(*(unsigned int*)(CHIP_LEVEL_REG + 0x04D4)) 
#define DDR3_CONFIG_REG_53 		(*(unsigned int*)(CHIP_LEVEL_REG + 0x04D8)) 
#define DDR3_CONFIG_REG_54 		(*(unsigned int*)(CHIP_LEVEL_REG + 0x04DC)) 
#define DDR3_CONFIG_REG_55 		(*(unsigned int*)(CHIP_LEVEL_REG + 0x04E0)) 
#define DDR3_CONFIG_REG_56 		(*(unsigned int*)(CHIP_LEVEL_REG + 0x04E4)) 
#define DDR3_CONFIG_REG_57 		(*(unsigned int*)(CHIP_LEVEL_REG + 0x04E8)) 
#define DDR3_CONFIG_REG_58 		(*(unsigned int*)(CHIP_LEVEL_REG + 0x04EC)) 
#define DDR3_CONFIG_REG_59 		(*(unsigned int*)(CHIP_LEVEL_REG + 0x04F0)) 
#define DDR3_CONFIG_REG_60 		(*(unsigned int*)(CHIP_LEVEL_REG + 0x04F4)) 

#define SGMII_SERDES_STS			(*(unsigned int*)(CHIP_LEVEL_REG + 0x158))
#define SGMII_SERDES_CFGPLL         (*(unsigned int*)(CHIP_LEVEL_REG + 0x340))
#define SGMII_SERDES_CFGRX0         (*(unsigned int*)(CHIP_LEVEL_REG + 0x344))
#define SGMII_SERDES_CFGTX0         (*(unsigned int*)(CHIP_LEVEL_REG + 0x348))

#define KICK0_UNLOCK (0x83E70B13)
#define KICK1_UNLOCK (0x95A4F1E0)
#define KICK_LOCK    0

/* PLL Registers */
#define BYPASS_BIT_SHIFT 23
#define CLKF_BIT_SHIFT   6
#define CLKR_BIT_SHIFT   0
//#define DEVSTAT    (*((unsigned int *) 0x02620020))

//*****************************************************
// Timeout definitions
int _GEL_Global_Timeout1 = 0;
#define TIMEOUT_ID 10

//********************************************************
int iRESULT = 0;

//*****************************************************
// Extended Memory Controller (XMC) Configuration
#define XMC_BASE_ADDR (0x08000000)
#define XMPAX2_L     (*(unsigned int*)(XMC_BASE_ADDR + 0x00000010))
#define XMPAX2_H     (*(unsigned int*)(XMC_BASE_ADDR + 0x00000014))

// DDR3 definitions
#define DDR_BASE_ADDR          0x21000000

#define DDR_MIDR                    (*(unsigned int*)(DDR_BASE_ADDR + 0x00000000))
#define DDR_STATUS					(*(unsigned int*)(DDR_BASE_ADDR + 0x00000004))
#define DDR_SDCFG                   (*(unsigned int*)(DDR_BASE_ADDR + 0x00000008))
#define DDR_SDRFC                   (*(unsigned int*)(DDR_BASE_ADDR + 0x00000010))
#define DDR_SDTIM1                  (*(unsigned int*)(DDR_BASE_ADDR + 0x00000018))
#define DDR_SDTIM2                  (*(unsigned int*)(DDR_BASE_ADDR + 0x00000020))
#define DDR_SDTIM3                  (*(unsigned int*)(DDR_BASE_ADDR + 0x00000028))
#define DDR_PMCTL                   (*(unsigned int*)(DDR_BASE_ADDR + 0x00000038))
#define DDR_ZQCFG                   (*(unsigned int*)(DDR_BASE_ADDR + 0x000000C8))
#define DDR_RDWR_LVL_RMP_WIN	    (*(unsigned int*)(DDR_BASE_ADDR + 0x000000D4))
#define DDR_RDWR_LVL_RMP_CTRL       (*(unsigned int*)(DDR_BASE_ADDR + 0x000000D8))
#define DDR_TMPALRT                 (*(unsigned int*)(DDR_BASE_ADDR + 0x000000CC))
#define DDR_RDWR_LVL_CTRL           (*(unsigned int*)(DDR_BASE_ADDR + 0x000000DC))
#define DDR_DDRPHYC                 (*(unsigned int*)(DDR_BASE_ADDR + 0x000000E4))

#define RD_DQS_SLAVE_RATIO_1600 0x34
#define WR_DQS_SLAVE_RATIO_1600 0xA3
#define WR_DATA_SLAVE_RATIO_1600 0xE3
#define FIFO_WE_SLAVE_RATIO_1600 0x103

#define RD_DQS_SLAVE_RATIO_1333 0x34
#define WR_DQS_SLAVE_RATIO_1333 0x9D
#define WR_DATA_SLAVE_RATIO_1333 0xDD
#define FIFO_WE_SLAVE_RATIO_1333 0xF5

#define RD_DQS_SLAVE_RATIO_1066 0x34
#define WR_DQS_SLAVE_RATIO_1066 0x97
#define WR_DATA_SLAVE_RATIO_1066 0xD7
#define FIFO_WE_SLAVE_RATIO_1066 0xE8

//*****************************************************
// PLL 1 definitions (DSP clk and subsystems)
#define PLL1_BASE           	 0x02310000
#define PLL1_PLLCTL              (*(unsigned int*)(PLL1_BASE + 0x100))   // PLL1 Control
#define PLL1_SECCTL              (*(unsigned int*)(PLL1_BASE + 0x108))   // PLL1 Sec Control
#define PLL1_PLLM                (*(unsigned int*)(PLL1_BASE + 0x110))   // PLL1 Multiplier
#define PLL1_DIV1                (*(unsigned int*)(PLL1_BASE + 0x118))   // DIV1 divider
#define PLL1_DIV2                (*(unsigned int*)(PLL1_BASE + 0x11C))   // DIV2 divider
#define PLL1_DIV3                (*(unsigned int*)(PLL1_BASE + 0x120))   // DIV3 divider
#define PLL1_CMD                 (*(unsigned int*)(PLL1_BASE + 0x138))   // CMD control
#define PLL1_STAT                (*(unsigned int*)(PLL1_BASE + 0x13C))   // STAT control
#define PLL1_ALNCTL              (*(unsigned int*)(PLL1_BASE + 0x140))   // ALNCTL control
#define PLL1_DCHANGE             (*(unsigned int*)(PLL1_BASE + 0x144))   // DCHANGE status
#define PLL1_CKEN                (*(unsigned int*)(PLL1_BASE + 0x148))   // CKEN control
#define PLL1_CKSTAT              (*(unsigned int*)(PLL1_BASE + 0x14C))   // CKSTAT status
#define PLL1_SYSTAT              (*(unsigned int*)(PLL1_BASE + 0x150))   // SYSTAT status
#define PLL1_DIV4                (*(unsigned int*)(PLL1_BASE + 0x160))   // DIV4 divider
#define PLL1_DIV5                (*(unsigned int*)(PLL1_BASE + 0x164))   // DIV5 divider
#define PLL1_DIV6                (*(unsigned int*)(PLL1_BASE + 0x168))   // DIV6 divider
#define PLL1_DIV7                (*(unsigned int*)(PLL1_BASE + 0x16C))   // DIV7 divider
#define PLL1_DIV8                (*(unsigned int*)(PLL1_BASE + 0x170))   // DIV8 divider
#define PLL1_DIV9                (*(unsigned int*)(PLL1_BASE + 0x174))   // DIV9 divider
#define PLL1_DIV10               (*(unsigned int*)(PLL1_BASE + 0x178))   // DIV10 divider
#define PLL1_DIV11               (*(unsigned int*)(PLL1_BASE + 0x17C))   // DIV11 divider
#define PLL1_DIV12               (*(unsigned int*)(PLL1_BASE + 0x180))   // DIV12 divider
#define PLL1_DIV13               (*(unsigned int*)(PLL1_BASE + 0x184))   // DIV13 divider
#define PLL1_DIV14               (*(unsigned int*)(PLL1_BASE + 0x188))   // DIV14 divider
#define PLL1_DIV15               (*(unsigned int*)(PLL1_BASE + 0x18C))   // DIV15 divider
#define PLL1_DIV16               (*(unsigned int*)(PLL1_BASE + 0x190))   // DIV16 divider

#define PLL_REG_RSCTL_VALUE_KEY                  (0x5A69)
#define PLL_REG_RSCFG_FIELD_POWER_ON_RESET       (1<<29)


//*****************************************************
// CACHE definitions
#define CACHE_BASE         		0x01840000
#define CACHE_L2CFG             (*( unsigned int* )( CACHE_BASE ))
#define CACHE_L1PCFG            (*( unsigned int* )( CACHE_BASE + 0x0020 ))
#define CACHE_L1DCFG            (*( unsigned int* )( CACHE_BASE + 0x0040 ))
#define L2WBINV                 (CACHE_BASE + 0x5004) // L2WBINV Control
#define L2INV                   (CACHE_BASE + 0x5008) // L2INV Control
#define L1PINV                  (CACHE_BASE + 0x5028) // L1PINV Control
#define L1DWBINV                (CACHE_BASE + 0x5044) // L1DWBINV Control
#define L1DINV                  (CACHE_BASE + 0x5048) // L1DINV Control

//*****************************************************
// EDMA3 definitions
#define EDMA3_TPCC_BASE		0x02740000
#define EDMA3_IERH                (EDMA3_TPCC_BASE + 0x1054) // IERH Control
#define EDMA3_EERH                (EDMA3_TPCC_BASE + 0x1024) // EERH Control
#define EDMA3_ICRH                (EDMA3_TPCC_BASE + 0x1074) // ICRH Control
#define EDMA3_ECRH                (EDMA3_TPCC_BASE + 0x100C) // ECRH Control
#define EDMA3_IER                 (EDMA3_TPCC_BASE + 0x1050) // IER Control
#define EDMA3_EER                 (EDMA3_TPCC_BASE + 0x1020) // EER Control
#define EDMA3_ICR                 (EDMA3_TPCC_BASE + 0x1070) // ICR Control
#define EDMA3_ECR                 (EDMA3_TPCC_BASE + 0x1008) // ECR Control
#define EDMA3_IECRH               (EDMA3_TPCC_BASE + 0x105C) // IECRH Control
#define EDMA3_IECR                (EDMA3_TPCC_BASE + 0x1058) // IECR Control
#define EDMA3_EECRH               (EDMA3_TPCC_BASE + 0x102C) // EECRH Control
#define EDMA3_EECR                (EDMA3_TPCC_BASE + 0x1028) // EECR Control

//*****************************************************
// GPIO definitions
#define GPIO_BASE           0x02320000
#define GPIO_BITEN          (*(unsigned int*)(GPIO_BASE + 0x0008)) // BITEN Control
#define GPIO_DIR            (*(unsigned int*)(GPIO_BASE + 0x0010)) // DIR Control
#define GPIO_OUT_DATA       (*(unsigned int*)(GPIO_BASE + 0x0014)) // OUT_DATA Control
#define GPIO_IN_DATA        (*(unsigned int*)(GPIO_BASE + 0x0020)) // IN_DATA Register
#define GPIO_CLR_RIS_TRIG   (*(unsigned int*)(GPIO_BASE + 0x0028)) // CLR_RIS_TRIG Control
#define GPIO_CLR_FAL_TRIG   (*(unsigned int*)(GPIO_BASE + 0x0030)) // CLR_FAL_TRIG Control

#define GPIO_DEFAULT_DIR    0xFFFF2CFF  // GP08,GP09,GP12,GP14,GP15 (Nand) are outputs
#define GPIO_DEFAULT_OUT    0x0000D000  // GP15, GP14, GP12 default to HIGH

// Used for eeprom programming
#define LITTLE_END	1
#define BIG_END 	2

//TIMER 0 definitions
#define TIMER0_CNTLO                     (*(unsigned int*)(0x02200010))
#define TIMER0_CNTHI                     (*(unsigned int*)(0x02200014))
#define TIMER0_PRDLO                     (*(unsigned int*)(0x02200018))
#define TIMER0_PRDHI                     (*(unsigned int*)(0x0220001C))
#define TIMER0_TCR                       (*(unsigned int*)(0x02200020))
#define TIMER0_TGCR                      (*(unsigned int*)(0x02200024))

#define L2EDSTAT 0x01846004
#define L2EDCMD  0x01846008
#define L2EDADDR 0x0184600C
#define L2EDCPEC 0x01846018
#define L2EDCNEC 0x0184601C
#define L2EDCEN  0x01846030

#define L1PEDSTAT  0x01846404
#define L1PEDCMD 0x01846408
#define L1PEDADDR  0x0184640C

#define SMCERRAR 0x0BC00008
#define SMCERRXR 0x0BC0000C
#define SMEDCC   0x0BC00010
#define SMCEA    0x0BC00014
#define SMSECC   0x0BC00018

#define PLL_REINIT_MAX_COUNT			(10)


#if 0
static void Wait_Soft( unsigned int nloop )
{
    unsigned int i;

    // 1 sec ~ 40000 loop on P4 3.4GHz
    for( i = 0 ; i < nloop ; i++ )
    {
    }
}
#endif

static void pll_delay(unsigned int ix)
{
    while (ix--) {
        asm("   NOP");
    }
}
#if 0
static void Delay_milli_seconds(unsigned int numMs) {
    pll_delay(numMs*140056);
}
#endif


static void Prog_pll1_values(unsigned int pll_multiplier, unsigned int pll_divider, unsigned int odiv)
{
    int TEMP;

    /* Check the Range for the parameters */

    if (odiv > 15)
    {
        printf ( " invalid output divide range, should be less than 15... \n");
    }

    if (pll_multiplier > 4095)
    {
        printf ( " invalid pll multiplier range, should be less than 4095... \n");
    }

    if (pll_divider > 63)
    {
        printf ( " invalid pll multiplier range, should be less than 63... \n");
    }

    /* 4. Set the PLL Multiplier, Divider, BWADJ                                 *
     * The PLLM[5:0] bits of the multiplier are controlled by the PLLM Register  *
     * inside the PLL Controller and the PLLM[12:6] bits are controlled by the   *
     * chip-level MAINPLLCTL0 Register.                                          *
     * PLL Control Register (PLLM)  Layout                                       *
     * |31...6   |5...0        |                                                 *
     * |Reserved |PLLM         |                                                 *
     *                                                                           *
     * Main PLL Control Register (MAINPLLCTL0)                                   *
     * |31...24   |23...19   |18...12    | 11...6   |5...0 |                     *
     * |BWADJ[7:0]| Reserved |PLLM[12:6] | Reserved | PLLD |                     */

    /* Set pll multipler (13 bit field) */
    PLL1_PLLM     = (pll_multiplier & 0x0000003F); /* bits[5:0]  */
    TEMP          = (pll_multiplier & 0x1FC0) >> 6;/* bits[12:6] */
    MAINPLLCTL0  &=~(0x0007F000);                /*Clear PLLM field */
    MAINPLLCTL0  |=((TEMP << 12) & 0x0007F000);

   /* 5. Set the BWADJ     (12 bit field)                                        *
     * BWADJ[11:8] and BWADJ[7:0] are located in MAINPLLCTL0 and MAINPLLCTL1     *
     * registers. BWADJ[11:0] should be programmed to a value equal to half of   *
     * PLLM[12:0] value (round down if PLLM has an odd value)                    *
     * Example: If PLLM = 15, then BWADJ = 7                                     */
    TEMP = ((pll_multiplier + 1) >> 1) - 1; /* Divide the pllm by 2 */
    MAINPLLCTL0 &=~(0xFF000000);  /* Clear the BWADJ Field */
    MAINPLLCTL0 |=  ((TEMP << 24) & 0xFF000000);
    MAINPLLCTL1 &=~(0x0000000F);   /* Clear the BWADJ field */
    MAINPLLCTL1 |= ((TEMP >> 8) & 0x0000000F);

    /* 6. Set the pll divider (6 bit field)                                         *
     * PLLD[5:0] is located in MAINPLLCTL0                                       */
    MAINPLLCTL0   &= ~(0x0000003F);    /* Clear the Field */
    MAINPLLCTL0   |= (pll_divider & 0x0000003F);

    /* 7. Set the OUTPUT DIVIDE (4 bit field) in SECCTL */
    PLL1_SECCTL    &= ~(0x00780000);     /* Clear the field       */
    PLL1_SECCTL   |= ((odiv << 19) & 0x00780000) ;
 
}

int Init_PLL(int pll_mult, int pll_div )
{
    int i, TEMP;
    /* Default dividers */
    unsigned int div2=3, div5=5, div8=64;
    int dsp_freq;
    int dsp_freM,dsp_freD;

    /*Unlock Boot Config*/
    KICK0 = 0x83E70B13;
    KICK1 = 0x95A4F1E0;

    
    /* 1. Wait for Stabilization time (min 100 us)                             *
     * The below loop is good enough for the Gel file to get minimum of        *
     * 100 micro seconds, this should be appropriately modified for port       *
     * to a C function                                                         *
     * Minimum delay in GEL can be 1 milli seconds, so program to 1ms=1000us,  *
     * more than required, but should be Okay                                  */
     //Delay_milli_seconds(1);
     pll_delay(140056);
                                                                               
    /* 2. Check the status of BYPASS bit in SECCTL register,                   *
     *    execute following steps if                                           *
     *    BYPASS == 1 (if bypass enabled), if BYPASS==0 then Jump to Step 3    */
                                                                               
    TEMP = PLL1_SECCTL &  0x00800000; /* Check the Bit 23 value */

    if (TEMP != 0) /* PLL BYPASS is enabled, we assume if not in Bypass ENSAT = 1 */
    {
        printf ( "PLL in Bypass ... \n");

        /* 2a. Usage Note 9: For optimal PLL operation, the ENSAT bit in the PLL control *
         * registers for the Main PLL, DDR3 PLL, and PA PLL should be set to 1.      *
         * The PLL initialization sequence in the boot ROM sets this bit to 0 and    *
         * could lead to non-optimal PLL operation. Software can set the bit to the  *
         * optimal value of 1 after boot                                             *
         * Ref: http://www.ti.com/lit/er/sprz334b/sprz334b.pdf                       *
         * |31...7   |6     |5 4       |3...0      |                                 *
         * |Reserved |ENSAT |Reserved  |BWADJ[11:8]|                                 */

        MAINPLLCTL1 = MAINPLLCTL1 | 0x00000040;

        /* 2b. Clear PLLEN bit (bypass enabled in PLL controller mux) */
        PLL1_PLLCTL &= ~(1 << 0);

        /* 2c. Clear PLLENSRC bit (enable PLLEN to control PLL controller mux) */
        PLL1_PLLCTL &= ~(1 << 5);

        /* 2d. Wait for 4 RefClks (to make sure the PLL controller *
         * mux switches properly to the bypass)                    *
         * Assuming slowest Ref clock of 25MHz, min: 160 ns delay  */
        //Delay_milli_seconds(1);
        pll_delay(225);

        /* 2e. Bypass needed to perform PWRDN cycle for C6670 and C6657                  *
         * Needed on all devices when in NOBOOT, I2C or SPI boot modes               *
         * Ref: Figure 4-2 of http://www.ti.com/lit/ug/sprugv2a/sprugv2a.pdf         *
         * PLL Secondary Control Register (SECCTL)  Layout                           *
         * |31...24  |23     |22...19       |18...0   |                              *
         * |Reserved |BYPASS |OUTPUT DIVIDE |Reserved |                              */
        PLL1_SECCTL |= 0x00800000; /* Set the Bit 23 */

        /* 2f. Advisory 8: Multiple PLLs May Not Lock After Power-on Reset Issue         *
         * In order to ensure proper PLL startup, the PLL power_down pin needs to be *
         * toggled. This is accomplished by toggling the PLLPWRDN bit in the PLLCTL  *
         * register. This needs to be done before the main PLL initialization        *
         * sequence                                                                  *
         * Ref: Figure 4-1 of http://www.ti.com/lit/ug/sprugv2a/sprugv2a.pdf         *
         * PLL Control Register (PLLCTL)  Layout                                     *
         * |31...4   |3      |2        |1        |0        |                         *
         * |Reserved |PLLRST |Reserved |PLLPWRDN |Reserved |                         */

        PLL1_PLLCTL   |= 0x00000002; /*Power Down the PLL */

        /* 2g. Stay in a loop such that the bit is set for 5 �s (minimum) and           *
         * then clear the bit.                                                      */

        //Delay_milli_seconds(1); /* This is more than required delay */  
        pll_delay(14005);

        /* 2h. Power up the PLL */  
        PLL1_PLLCTL   &= ~(0x00000002); 
    }
    else
    {
       /* 3. Enable BYPASS in the PLL controller */
        printf ( "PLL not in Bypass, Enable BYPASS in the PLL Controller... \n");

        /* 3a. Clear PLLEN bit (bypass enabled in PLL controller mux) */
        PLL1_PLLCTL &= ~(1 << 0);

        /* 3b. Clear PLLENSRC bit (enable PLLEN to control PLL controller mux) */
        PLL1_PLLCTL &= ~(1 << 5);

        /* 3c. Wait for 4 RefClks (to make sure the PLL controller *
         * mux switches properly to the bypass)                    *
         * Assuming slowest Ref clock of 25MHz, min: 160 ns delay  */
        //Delay_milli_seconds(1);
        pll_delay(225);
    }

    /* 4, 5, 6 and 7 are done here: 
     * Program the necessary multipliers/dividers and BW adjustments             */
    Prog_pll1_values(pll_mult, pll_div, 1);

#if 1// jdc added
    /* part of 8, go stat bit needs to be zero here    */
    /* Read the GOSTAT bit in PLLSTAT to make sure the bit returns to 0 to      *  
     * indicate that the GO operation has completed                             */
    /* wait for the GOSTAT, but don't trap if lock is never read */
    for (i = 0; i < 100; i++)
    {
        pll_delay(300);
        if ( (PLL1_STAT & 0x00000001) == 0 ) {
            break;
        }
    }
    if ( i == 100 ) {
        printf( "Error while waiting for GOSTAT bit returning to 0 ... \n");
        return(-1);
    }
#endif

    /* 8. Set PLL dividers if needed */
    PLL1_DIV2 = (0x8000) | (div2 - 1);
    PLL1_DIV5 = (0x8000) | (div5 - 1);
    PLL1_DIV8 = (0x8000) | (div8 - 1);

    /* part of 8, Program ALNCTLn */
    /* Set bit 1, 4 and 7 */
    PLL1_ALNCTL |= ( (1 << 1) | (1 << 4) | (1 << 7));

    /* part of 8, Set GOSET bit in PLLCMD to initiate the GO operation to change the divide *
     * values and align the SYSCLKs as programmed                                */
    PLL1_CMD     |= 0x00000001;

    /* part of 8, go stat bit needs to be zero here    */
    /* Read the GOSTAT bit in PLLSTAT to make sure the bit returns to 0 to      *  
     * indicate that the GO operation has completed                             */
    /* wait for the GOSTAT, but don't trap if lock is never read */
    for (i = 0; i < 100; i++)
    {
        pll_delay(300);
        if ( (PLL1_STAT & 0x00000001) == 0 ) {
            break;
        }
    }
    if ( i == 100 ) {
        printf( "Error while waiting for GOSTAT bit returning to 0 ... \n");
        return(-1);
    }

    /* 9. Place PLL in Reset, In PLLCTL, write PLLRST = 1 (PLL is reset)         */
    PLL1_PLLCTL |= 0x00000008;

    /* 10. Wait for PLL Reset assertion Time (min: 7 us)                          *
     * Minimum delay in GEL can be 1 milli seconds, so program to 1ms=1000us,  *
     * more than required, but should be Okay                                  */
    //Delay_milli_seconds(1);
    pll_delay (14006);

    /* 11. In PLLCTL, write PLLRST = 0 (PLL reset is de-asserted) */
    PLL1_PLLCTL &= ~(0x00000008);

    /* 
     * 12. PLL Lock Delay needs to be 500 RefClk periods * (PLLD + 1)
     * i.e., Wait for at least 500 * CLKIN cycles * (PLLD + 1) (PLL lock time)
     * Using 2000 25ns RefClk periods per DM
     * Wait for PLL to lock min 50 micro seconds
     * 
     * */
    //Delay_milli_seconds(1);
    pll_delay (140056 >> 1);

    /* 13. In SECCTL, write BYPASS = 0 (enable PLL mux to switch to PLL mode) */
    PLL1_SECCTL &= ~(0x00800000); /* Release Bypass */

    /* 14. In PLLCTL, write PLLEN = 1 (enable PLL controller mux to switch to PLL mode) */
    PLL1_PLLCTL |= (1 << 0);
    
    /* 15. The PLL and PLL Controller are now initialized in PLL mode - Completed. */

#if 1 // informational only
    // Compute the real dsp freq (*100)
	dsp_freq = (((REF_CLOCK_KHZ/10) * (pll_mult+1)/(pll_div+1)/2));

    // Displayed frequency setup
    // dsp freq in MHz
    dsp_freM = dsp_freq / 100;

    // dsp freq first decimal if freq expressed in MHz
    dsp_freD = ((dsp_freq - dsp_freM * 100) + 5) / 10;

    // Add roundup unit to MHz displayed and reajust decimal value if necessary...
    if (dsp_freD > 9)
    {
        dsp_freD = dsp_freD - 10;
        dsp_freM = dsp_freM + 1;
    }

    // Print freq info...
    printf( "PLL1 Setup for DSP @ %d.%d MHz.\n", dsp_freM, dsp_freD );
    printf( "SYSCLK2 = %f MHz, SYSCLK5 = %f MHz.\n", ((float)(dsp_freq/100)/div2), ((float)(dsp_freq/100)/div5));
    printf( "SYSCLK8 = %f MHz.\n", ((float)(dsp_freq/100)/div8));
#endif

    return (0);
}

void init_pll(unsigned int cpu_freq) {

    if (cpu_freq == 850000000ul)
        Init_PLL(PLL1_M_850, PLL1_D);
    else
        Init_PLL(PLL1_M_1000, PLL1_D);
}
