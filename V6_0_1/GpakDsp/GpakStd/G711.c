
#include "G711.H"
#include <math.h>

// Expand (A,Mu) defines
#define MASKFF  0x0ff

// MuLawCompress defines
#define BIAS1 0xff-0x180
#define BIAS2 0xff80
#define BIAS3 0x21
#define BIAS4 0x10
#define MCMASK 0xff
#define MAX 8158

// ALawCompress defines
#define MASK 0x80 
#define ACBIAS 0x1f0 
#define SHIFT0 x10 
#define INVERT 0xD5 
#define CONST 0x1f 

extern  const short G711DEC_ADT_muLawTable[];
extern  const short G711DEC_ADT_aLawTable[];

//---------------------------------------------
#ifdef _TMS320C6X
#define norm_l(a) (_norm(a))
#else
inline short norm_l (int L_var1) {
   short var_out;

   if (L_var1 == 0)  return 0;
   if (L_var1 == (int)0xffffffffL) return 31;

   if (L_var1 < 0) L_var1 = ~L_var1;

   for (var_out = 0; L_var1 < (int)0x40000000L; var_out++) {
      L_var1 <<= 1;
   }

   return var_out;
}
#endif
///////////////////////////////////////////////////////////////////
void G711DEC_ADT_aLawExpand(short *Input, short *Output, short Length) {
   short i=0;

   for (i=0; i<Length; i++)
       *Output++ = G711DEC_ADT_aLawTable[*Input++ & MASKFF];

}

/////////////////////////////////////////////////////////////////
void G711DEC_ADT_muLawExpand(short *Input, short *Output, short Length) {
   short i=0;

   for (i=0; i<Length; i++)
	   *Output++ = G711DEC_ADT_muLawTable[*Input++ & MASKFF];
}

////////////////////////////////////////////////////////////////
void G711ENC_ADT_aLawCompress(short *Input, short *Output, short Length) {
   short i=0;
   int accA, accB;
   short shift;

   for (i=0; i<Length; i++) {
      accB = (*Input++)>>3;
      accA =  accB >>6;
      accB = abs(accB);

      shift = norm_l(accA);
      if (accA == 0) shift = 31;
      accA = (accA & MASK) + ACBIAS - shift*16;

      if (shift > 0)
         accA += ((accB >> (32-shift)) & 0xff);

      *Output++ = (short) (accA ^ INVERT);

	}
}

/////////////////////////////////////////////////////////////////
void G711ENC_ADT_muLawCompress(short *Input, short *Output, short Length) {
   short i=0;
   int accA, accB;
   short shift;

   for (i=0; i<Length; i++) {
      accA = (*Input++)>>2;
      if (accA > 8158)  accA = 8158;
      if (accA < -8158) accA = -8158;
      accB = BIAS1 - (accA>>16)*BIAS2;
      accA = abs(accA)+BIAS3;
      shift = norm_l (accA);
      accB += shift*16;
      accB = (accB-((accA<<shift)>>26));
      
      *Output++ = (short)accB; 
	}
}







