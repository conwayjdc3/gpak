#include "adt_typedef.h"
#include "GpakExts.h"
#include "GpakDefs.h"
#include "GpakHpi.h"

const ADT_UInt16 tdmCore = 0;

int ClkDiv, FrameWidth, PulseWidth, genClockEnable;
ADT_UInt32 RxEvents [] = { 0, 0, 0 };     // Identifies rx events that are reported to dma SWI
ADT_UInt32 TxEvents [] = { 0, 0, 0 };     // Identifies tx events that are reported to dma SWI
//int mcbsp_loopback = 0;

#pragma CODE_SECTION (TogglePCMOutSignal, "SLOW_PROG_SECT")
int TogglePCMOutSignal (unsigned int port)    { return 0xff; }
#pragma CODE_SECTION (TogglePCMInSignal, "SLOW_PROG_SECT")
int TogglePCMInSignal (unsigned int port)     { return 0xff; }

#pragma CODE_SECTION (storeMcBSPConfiguration, "SLOW_PROG_SECT")
void storeMcBSPConfiguration (int port)                 { }
#pragma CODE_SECTION (setupMcBSP, "SLOW_PROG_SECT")
GpakActivation setupMcBSP (int port)                    { return Disabled; }
#pragma CODE_SECTION (startMcBSP, "SLOW_PROG_SECT")
void startMcBSP (int port)                              { }
#pragma CODE_SECTION (stopMcBsp, "SLOW_PROG_SECT")
GpakActivation stopMcBsp (int port)                     { return Disabled; }
#pragma CODE_SECTION (SetTransmitEnables, "SLOW_PROG_SECT")
void SetTransmitEnables (int port, ADT_UInt32 Mask[])   { }
#pragma CODE_SECTION (ClearTransmitEnables, "SLOW_PROG_SECT")
void ClearTransmitEnables (int port, ADT_UInt16 Mask[]) { }
#pragma CODE_SECTION (ConfigureMcBSP, "SLOW_PROG_SECT")
void ConfigureMcBSP (ADT_UInt32 McBspEnable[3], ADT_UInt32 McBspSlotMask[3][4]) { }
#pragma CODE_SECTION (GetMasks, "SLOW_PROG_SECT")
void GetMasks (ADT_Word McBspId, int Slot, int SlotCnt, ADT_Word *TxMask) { }
#pragma CODE_SECTION (updateTdmLoopback, "SLOW_PROG_SECT")
int updateTdmLoopback(GpakSerialPort_t port, GpakActivation state) { return 0; }



ADT_Bool CopyDmaTxBuffers (int port, int slotCnt, CircBufInfo_t **TxCircBufrList, int slip) { 
    return 0;
}
ADT_Bool CopyDmaRxBuffers (int port, int slotCnt, CircBufInfo_t **RxCircBufrList, int slip) { 
    return 0;
}

void DMAInitialize () { }

int DmaLbRxToTxBuffers (int port, int slotCnt) {   return 0;  }

GPAK_PortConfigStat_t ProcConfigSerialPortsMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {
   pReply[0] |= (MSG_CONFIG_PORTS_REPLY << 8);
   return Pc_NoInterrupts;
}

ADT_UInt16 disableDMA (int port) { return 0; }
ADT_UInt16 enableDMA (int port) { return 0; }
ADT_Bool McBSPFrameError (int port)   { return 0;}
void zeroTxBuffer (int port, int dmaSlot) { }

void InitTDMMode () { 
    sysConfig.samplesPerMs = 8;
    TDMRate = 8000;
}
