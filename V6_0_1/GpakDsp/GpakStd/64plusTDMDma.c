/*
 * Copyright (c) 2008, Adaptive Digital Technologies, Inc.
 *
 * File Name: G4plusTDMDma.c
 *
 * Description:
 *    This file contains G.PAK DMA functions to transfer between McBSPs and memory.
 *
 * Version: 1.0
 *
 * Revision History:
 *   01/23/2007 - Initial release.
 *
 *
 */

/*
   The DMA configuration details for each McBSP port are defined below. 

                  Parameter block assignments
   
                   Tx0  Rx0      Tx1  Rx1
  McBSP Start Chn    2    3        4    5
    DMA Ping Lnk    64   66       68   70
    DMA Pong Lnk    65   67       69   71

    TCC Event        2    3        4    5
    Transfer queue   2    2        2    2
    Queue priority   0    0        0    0


    Shadow region 0 (interrupt signal 35) used to access the EDMA registers

    TDM DMA completion HW IRQ  8  (for ALL DMA channels)

                   ---------------------------
                   
   The DMA is setup to transfer 1ms of data between the McBSP and a DMA buffer. 
   Two linear buffers (ping and pong) are used so that the DMA software interrupt 
   can work with one set of buffers (e.g. ping) during the same 1 ms interval that
   the DMA is transfering data between the McBSPs and the other set of buffers (e.g. pong).

   The channel/frame (2 bytes/element) data is placed into the DSP's ping and pong buffers 
   as shown below:

 
      C1F1   C1F2   C1F3   C1F4   C1F5   C1F6   C1F7   C1F8
      C2F1   C2F2   C2F3   C2F4   C2F5   C2F6   C2F7   C2F8
      C3F1   C3F2   C3F3   C3F4   C3F5   C3F6   C3F7   C3F8
      C4F1   C4F2   C4F3   C4F4   C4F5   C4F6   C4F7   C4F8
      C5F1   C5F2   C5F3   C5F4   C5F5   C5F6   C5F7   C5F8
                                . 
                                . 
                                . 
      CNF1   CNF2   CNF3   CNF4   CNF5   CNF6   CNF7   CNF8
 

  The order of the data arriving at McBSP is as follows:

       C1F1 C2F1 C3F1 ... CNF1 | C1F2 C2F2 C3F2 ... CNF2 | C1F3 ... CNF8

  Where the | character symbolizes the beginning of a new frame of N channels.
   
  After 8 frames (1ms) of data, the DMA automatically switches between the ping and pong
  buffers and generates a hardware interrupt so that the CPU can begin processing the next
  1ms of data.
  
  
   Each buffer consists of 2 bytes per element, N channels per frame and 8 frames.
   The first element of the pong buffer is located immediately after the last element 
   in the ping buffer.  
   
   NOTE:  Separating the ping and pong buffers (as opposed to interleaving the 
          buffers as in earlier versions) reduces the need for L1 cache updates
          since the CPU and DMA will always be accessing different cache lines.


                   -------------------------
                   
   DMA transfers are linked together, using the DMA parameter blocks, such that 
   at the completion of a ping buffer transfer, the pong buffer transfer is 
   automatically started and vice versa.
   
   The CPU is notified that a transfer is complete via a common interrupt.
   The interrupt handler can determine which transfer(s) is complete by reading
   and clearing the CIPR register

                   -------------------------

   INTERFACES

   void DMAInitialize ()
   ADT_Events enableDMA  (port) 
   ADT_Events disableDMA (port)

   ADT_Bool DmaLostTxSync (int port, ADT_PCM16 *TxSwi);
   ADT_Bool DmaLostRxSync (int port, ADT_PCM16 *RxSwi);

   void CopyDmaTxBuffers (port, CircBufInfo_t **pCircTxBase,  ADT_UInt16 *pDmaTxBufr);
   void CopyDmaRxBuffers (port, ADT_UInt16 *pDmaRxBufr, CircBufInfo_t **pCircRxBase);

                      -------------------------

     DEBUGGING AIDES
     
   TxPatternSlot1    = (non-zer0) Pattern placed only in first slot.
   TxPatternAllSlots = (non-zero) Pattern placed in all slots.  TxPatternSlot1 takes precedence on first slot.

   RxPatternSlot1    = (non-zer0) Pattern placed only in first slot.
   RxPatternAllSlots = (non-zer0) Pattern placed in all slots.  RxPatternSlot1 takes precedence on first slot.

   FrameStamp = (true) 1st byte of each frame contains current frame count   
*/


   
// Application related header files.

#define SHADOW_REGION 0
#include <std.h>
#include <hwi.h>
#include <exc.h>
#include <c64.h>

#include "GpakDefs.h"
#include "GpakExts.h"
#include "sysmem.h"
#include "sysconfig.h"
#include "GpakPcm.h"
#include "string.h"
#include "64plus.h"
#include "GpakPragma.h"

extern int customTDMOut (ADT_PCM16 *dataOut, int port, int slotCnt);
extern int customTDMIn  (ADT_PCM16 *dataIn,  int port, int slotCnt);



#define HWI_TDM_ERR        7    // HW Interrupt vector for TDM/EDMA errors
#define HWI_TDM_COMPLETE   8    // HW Interrupt vector for EDMA completions

#define TDM_INTR_CLR   INTR_CLR // Interrupt signal clear register

struct errCnts {
   ADT_UInt32 cnt;

   ADT_Int64  MissedEvents;
   ADT_UInt32 WaterMarkErr;

   ADT_UInt32 TC0Stat;
   ADT_UInt32 TC1Stat;
   ADT_UInt32 TC2Stat;
   ADT_UInt32 TC0Error;
   ADT_UInt32 TC1Error;
   ADT_UInt32 TC2Error;

   ADT_UInt32 IntrStat;
   
   ADT_UInt32 InternalExc;
} err = { 0 };

extern __cregister volatile unsigned int ECR;
extern __cregister volatile unsigned int IERR;


PortAddr DMACfg [NUM_TDM_PORTS];
PortAddr DefaultDMACfg [NUM_TDM_PORTS];

#pragma DATA_SECTION (DMACfg,    "FAST_DATA_SECT:TDM")

static ADT_PCM16 * const RxDMABuffers[NUM_TDM_PORTS] = {
   BSP0DMA_RxBuffer, BSP1DMA_RxBuffer, BSP2DMA_RxBuffer
};

static ADT_PCM16 * const TxDMABuffers[NUM_TDM_PORTS] = {
   BSP0DMA_TxBuffer, BSP1DMA_TxBuffer, BSP2DMA_TxBuffer
};

//---------------------------------------
// Offset (bytes) from ping buffer to pong buffer
//
static ADT_UInt16 DmaPongOffset [] = { 0, 0, 0 };  
#pragma DATA_SECTION (DmaPongOffset,    "FAST_DATA_SECT:TDM")

// -------------------------------------------
//  DMA event bits to port assignments for PCM thread to determine port and direction of event
//
ADT_Events RxEvents [] = { 0, 0, 0 };
ADT_Events TxEvents [] = { 0, 0, 0 };
#pragma DATA_SECTION (RxEvents,    "FAST_DATA_SECT:TDM")
#pragma DATA_SECTION (TxEvents,    "FAST_DATA_SECT:TDM")

//---------------------------------------------------------
//   Runtime structures
//
ADT_PCM16 *DmaRxBuff   [NUM_TDM_PORTS];   // DMA Rcv buffer address for McBSP
ADT_PCM16 *DmaTxBuff   [NUM_TDM_PORTS];   // DMA Xmt buffer address for McBSP
ADT_UInt16 DmaTxOffset [NUM_TDM_PORTS];   // current Tx DMA buffer offset (ping or pong) for McBSP
ADT_UInt16 DmaRxOffset [NUM_TDM_PORTS];   // current Rx DMA buffer offset (ping or pong) for McBSP
#pragma DATA_SECTION (DmaRxBuff,    "FAST_DATA_SECT:TDM")
#pragma DATA_SECTION (DmaTxBuff,    "FAST_DATA_SECT:TDM")
#pragma DATA_SECTION (DmaRxOffset,  "FAST_DATA_SECT:TDM")
#pragma DATA_SECTION (DmaTxOffset,  "FAST_DATA_SECT:TDM")


static void GpakDmaIsr (void);
static void GpakErrIsr (void);
static void DMAClearErrors (void);

static void initRxTCCParams (int port);
static void initTxTCCParams (int port);
static void nullTCCParams   (int port);


//  Debugging aides
ADT_Bool  FrameStamp = FALSE;         // Time stamp is to be placed on every 8th sample
struct dmaDebug {
   ADT_Bool   firstTime;
   ADT_UInt16 TxPatternSlot1;       // Debug pattern to be placed on Tx timeslot 1
   ADT_UInt16 TxPatternAllSlots;    // Debug pattern to be placed on all Tx timeslots
   ADT_UInt16 RxPatternSlot1;       // Debug pattern to be placed on Rx timeslot 1
   ADT_UInt16 RxPatternAllSlots;    // Debug pattern to be placed on all Rx timeslots

   ADT_UInt16 TxSineSlot;           // Slot to transmit sine pattern
   ADT_UInt16 BroadcastPort;
   ADT_UInt16 BroadcastSlot;
   ADT_UInt16 captureTx;
   ADT_UInt16 captureRx;
} dma = { TRUE, 0, 0, 0, 0,
          0xffff, 0, 0xffff, 0xffff, 0xffff };
//--------------------------------------------------------------------------------
//
//   TEST_SWCOMPAND = Testing S/W compand

#ifdef TEST_SWCOMPAND // convert linear sampled to compressed data to test sw comapnding
   #define muLawExpand(inout,cnt)    G711_ADT_muLawExpand (inout,inout,cnt)
   #define muLawCompress(inout,cnt)  G711_ADT_muLawCompress(inout,inout,cnt)
#else 
   #define muLawExpand(inout, cnt)
   #define muLawCompress(inout,cnt)
#endif

//--------------------------------------------------------------------------------
//
//   Debug = Enable fixed pattern testing and out of sync buffers

#ifndef _DEBUG
   ADT_UInt16 TogglePCMInSignal (int port)  { return 0xff; }
   ADT_UInt16 TogglePCMOutSignal (int port) { return 0xff; }
   #define CheckTxPointers(circ,dma) 
   #define CheckRxPointers(circ,dma) 
   #define FixedPatternTx(TxDmaBufr) 
   #define FixedPatternRx(RxDmaBufr) 
#else
struct {
  void *Param;
  void *CC;
  void *TC;
} EdmaAddr = { (void *) EDMA_PARAM_ADDR, (void *) EDMA_CFG_BASE, (void *) TC0_BASE };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ToggleTestSignal
//
// FUNCTION
//   Toggles receive port between DMA buffer (normal) and constant tone (test)
//
// RETURNS
//  nothing
//
int isrCnt=0;
int isrFlags=0;

static ADT_PCM16 const SineTableM12[] = {
  -3, 2828, 4000, 2828, -2, -2828, -4000, -2828,
  -1, 2829, 4000, 2828,  0, -2828, -4000, -2828,
};
static ADT_PCM16 Drain [MAX_SAMPLES_PER_MS*2][MAX_CHANS_PER_FRAME];     // Dummy receive buffer
#pragma DATA_SECTION (Drain, "FAST_DATA_SECT:TDM")
#pragma DATA_ALIGN   (Drain, CACHE_L2_LINE_SIZE)

static ADT_PCM16 Dummy [MAX_SAMPLES_PER_MS*2][MAX_CHANS_PER_FRAME];     // Dummy transmit buffer
#pragma DATA_SECTION (Dummy, "FAST_DATA_SECT:TDM")
#pragma DATA_ALIGN   (Dummy, CACHE_L2_LINE_SIZE)

static ADT_PCM16 BroadcastBufr [MAX_SAMPLES_PER_MS];
#pragma DATA_SECTION (BroadcastBufr, "FAST_DATA_SECT:TDM")
#pragma DATA_ALIGN   (BroadcastBufr,  CACHE_L2_LINE_SIZE)

ADT_UInt16 inCaptureBuff [900];
CircBufInfo_t captureTDMIn = {
  inCaptureBuff, sizeof (inCaptureBuff)/2, 0, 0, 0 
};
#pragma DATA_SECTION (inCaptureBuff,  "FAST_DATA_SECT:TDM")
#pragma DATA_ALIGN   (inCaptureBuff,  CACHE_L2_LINE_SIZE)

ADT_UInt16 outCaptureBuff [900];
CircBufInfo_t captureTDMOut = {
  outCaptureBuff, sizeof (outCaptureBuff)/2, 0, 0, 0
};
#pragma DATA_SECTION (outCaptureBuff, "FAST_DATA_SECT:TDM")
#pragma DATA_ALIGN   (outCaptureBuff,  CACHE_L2_LINE_SIZE)



static void sineBuffer (unsigned int port, ADT_PCM16 *Buffer) {
   int i, dmaSltCnt;

   // Populate dummy buffer with 1 kHz SINE wave on all channels
   if (NUM_TDM_PORTS <= port) return;

   dmaSltCnt = sysConfig.maxSlotsSupported[port];

   for (i=0; i<dmaSltCnt; i++) {
      memcpy (Buffer, SineTableM12, sizeof (SineTableM12));
      Buffer += (SAMPLES_PER_INTERRUPT * 2);
   }

}

int TogglePCMInSignal (unsigned int port) {

   if (NUM_TDM_PORTS <= port) return 0;

   sineBuffer (port, (ADT_PCM16 *) &Dummy);

   if (DmaRxBuff[port] == RxDMABuffers[port]) {
       DmaRxBuff[port] = (ADT_PCM16 *) &Dummy;
       return 1;
   }
   // Restore
   DmaRxBuff[port] = RxDMABuffers[port];
   return 0;
}

int TogglePCMOutSignal (unsigned int port) {

   if (NUM_TDM_PORTS <= port) return 0;

   if (DmaTxBuff[port] == TxDMABuffers[port]) {
       sineBuffer (port, TxDMABuffers[port]);
       DmaTxBuff[port] = (ADT_PCM16 *) &Drain;
       return 1;
   }
   // Restore with initial zeroes
   DmaTxBuff[port] = TxDMABuffers[port];
   memset (TxDMABuffers[port], 0, sysConfig.dmaBufferSize[port] * 2);
   return 0;
}

static void FixedPatternTx (ADT_PCM16 *TxDmaBufr) {
   ADT_PCM16 *DmaBufr;
   int i;

   if (dma.TxPatternSlot1) {
      DmaBufr = TxDmaBufr;
      for (i=0; i<SAMPLES_PER_INTERRUPT; i++) *DmaBufr++ = dma.TxPatternSlot1;
   }

   if (dma.TxSineSlot == 0) {
      memcpy (TxDmaBufr, SineTableM12, SAMPLES_PER_INTERRUPT * 2);
   }
   if (dma.captureTx == 0 && dma.captureRx != 0xffff) {
      copyLinearToCirc (&ApiBlock.DmaSwiCnt, &captureTDMOut, 1);
      copyLinearToCirc (TxDmaBufr, &captureTDMOut, SAMPLES_PER_INTERRUPT);
      captureTDMOut.TakeIndex = captureTDMOut.PutIndex;
   }

}
static void FixedPatternRx (ADT_PCM16 *RxDmaBufr) {
   ADT_PCM16 *DmaBufr;
   int i;

   // Overwrite channel(s) output with a fixed (non-zero) value
   if (dma.BroadcastSlot == 0)
      memcpy (BroadcastBufr, RxDmaBufr, sizeof (BroadcastBufr));
   else if (dma.BroadcastSlot != 0xffff)
      memcpy (RxDmaBufr, BroadcastBufr, sizeof (BroadcastBufr));

   if (dma.RxPatternSlot1) {
      DmaBufr = RxDmaBufr;
      for (i=0; i<SAMPLES_PER_INTERRUPT; i++) *DmaBufr++ = dma.RxPatternSlot1;
   }
   if (dma.captureRx == 0 && dma.captureTx != 0xffff) {
      copyLinearToCirc (&ApiBlock.DmaSwiCnt, &captureTDMIn, 1);
      copyLinearToCirc (RxDmaBufr, &captureTDMIn, SAMPLES_PER_INTERRUPT);
      captureTDMIn.TakeIndex = captureTDMIn.PutIndex;
   }
   if (FrameStamp) {
      *RxDmaBufr = (ADT_PCM16) ApiBlock.DmaSwiCnt;
   }     
}


inline void CheckTxPointers (CircBufInfo_t *circ, ADT_PCM16 *dma) {
   LogTransfer (0xC1, circ, &circ->pBufrBase[circ->TakeIndex], 0, dma, SAMPLES_PER_INTERRUPT);
   AppErr ("TxPointerError", (SAMPLES_PER_INTERRUPT < circ->BufrSize) && (getAvailable (circ) < SAMPLES_PER_INTERRUPT));
}
inline void CheckRxPointers (CircBufInfo_t *circ, ADT_PCM16 *dma) {
   LogTransfer (0x1C, 0, dma, circ, &circ->pBufrBase[circ->PutIndex], SAMPLES_PER_INTERRUPT);
   AppErr ("RxPointerError", (SAMPLES_PER_INTERRUPT < circ->BufrSize) && (getFreeSpace (circ) < SAMPLES_PER_INTERRUPT));
}
#endif

void DMAClearAll () {

   // Clear all DMA error conditions
   DMAClearErrors ();

   // clear all indications of edma events and event interrupts
   REG64_WR (EDMA_EMCR, 0xffffffff);        // Clear missed event notifications
   REG64_WR (EDMA_GLB_INCR, 0xffffffff);    // Clear interrupt notifications
   REG64_WR (EDMA_GLB_ENCR, 0xffffffff);    // Clear event notifications
   REG64_WR (EDMA_GLB_SNCR, 0xffffffff);    // Clear secondary event notifications

   // disable edma events and transfer completion interrupts for port
   REG64_WR (EDMA_GLB_IECR, 0xffffffff);    // Disable completion interrupts
   REG64_WR (EDMA_GLB_EECR, 0xffffffff);    // Disable events

   // remove events from shadow regions
   REG64_WR (EDMA_SHAD0, 0);
   REG64_WR (EDMA_SHAD1, 0);

   // Disable all dma access
   memset ((void *)EDMA_PARAM_ADDR, 0, 128 * PARAM_SIZE);

}
 
static void PowerOnDMA () {

   int intrMask;

   volatile ADT_UInt32 *PscCmd  = PSC_CMD;
   volatile ADT_UInt32 *PscStat = PSC_STAT;
   volatile ADT_UInt32 *PscReg  = PSC_REG;

   intrMask = HWI_disable ();
   //---------------------------------------------------
   // 1. Wait for previous power transition to complete
   while (*PscStat & 1);

   //---------------------------------------------------
   //  2. Enable clock domains for EDMA  (channel control and 3 transfer controllers)
    PscReg[PSC_EDMA_CC]  = PSC_ON;
    PscReg[PSC_EDMA_TC0] = PSC_ON;
    PscReg[PSC_EDMA_TC1] = PSC_ON;
    PscReg[PSC_EDMA_TC2] = PSC_ON;

   //---------------------------------------------------
   //  3. Transition power
   *PscCmd = 1;

   //---------------------------------------------------
   //  4. Wait for power transition to complete
   while (*PscStat & 1);

   HWI_restore (intrMask);

}

//---------------------------------------------------
//{  DMAIntialize 
//
// FUNCTION
//   - Disables all DMA; initializes interrupt variables
//}
void DMAInitialize () {
   int port;

   C64_disableIER (1 << HWI_TDM_COMPLETE);
   C64_disableIER (1 << HWI_TDM_ERR);
   if (dma.firstTime) {
      // Remove all interrupts from combiner
      REG_WR (INTR_CMB,      0xFFFFFFFF);
      REG_WR (INTR_CMB + 4,  0xFFFFFFFF);
      REG_WR (INTR_CMB + 8,  0xFFFFFFFF);
      REG_WR (INTR_CMB + 12, 0xFFFFFFFF);

      // Map TDM DMA interrupt completion signal to GpakDmaIsr
      HWI_dispatchPlug (HWI_TDM_COMPLETE, (Fxn) &GpakDmaIsr, -1, NULL);
      HWI_eventMap     (HWI_TDM_COMPLETE, TDM_CompleteSgnl);

      // Map TDM Error signals to GpakDmaIsr
      HWI_dispatchPlug (HWI_TDM_ERR, (Fxn) &GpakErrIsr, -1, NULL);
      HWI_eventMap     (HWI_TDM_ERR, ErrSgnl);

      PowerOnDMA ();
      dma.firstTime = FALSE;

      for (port=0; port<NUM_TDM_PORTS; port++)
      	memcpy (&DMACfg[port], &DefaultDMACfg[port], sizeof (PortAddr));

   }

   // Set the multiplexed interrupt control for all DMA interrupts.
   for (port=0; port<NUM_TDM_PORTS; port++) {
      DmaRxBuff[port] = RxDMABuffers[port];
      DmaTxBuff[port] = TxDMABuffers[port];
      disableDMA (port);
   }

   // Assign a priority for the TDM queue
   assignPriority (TDM_TC, TDM_Priority);

   // Clear pending exceptions
   DMAClearErrors ();

   // Assign TDM error interrupt signals as exceptions
   assignErrorInterrupt (TDM_CCErrSgnl);
   assignErrorInterrupt (TDM_Que0ErrSgnl);   REG_WR (TC0_ERR_STAT_ENBL, 0x5);
   assignErrorInterrupt (TDM_Que1ErrSgnl);   REG_WR (TC1_ERR_STAT_ENBL, 0x5);
   assignErrorInterrupt (TDM_Que2ErrSgnl);   REG_WR (TC2_ERR_STAT_ENBL, 0x5);

//   assignErrorInterrupt (DroppedIntrSgnl);

   REG_WR (INTR_XSTAT_CLR, 1);     // Clear pending exceptions
   C64_enableIER (1 << HWI_TDM_COMPLETE);
   C64_enableIER (1 << HWI_TDM_ERR);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ enableDMA
//
// FUNCTION
//   Enables Tx and Rx DMA for specified McBSP
//
// RETURNS
//    Bit mask of events that are generated at interrupt
//}
ADT_Events enableDMA (int port) {

   ADT_Events chnBits, shadowBits;

   if (NUM_TDM_PORTS <= port) return 0;


   // Convert Tx and Rx DMA channels to their corresponding event register bits
   chnBits = (1 << DMACfg[port].RxChn) | (1 << DMACfg[port].TxChn);

   // disable edma events and transfer completion interrupts for port
   REG64_WR (EDMA3_IECR, chnBits);    // disable completion interrupts
   REG64_WR (EDMA3_EECR, chnBits);    // disable events

   // Allow PcmProcess to determine if event is transmit or receive
   RxEvents[port] = 1 << DMACfg[port].RxChn;
   TxEvents[port] = 1 << DMACfg[port].TxChn;

   DmaTxOffset[port] = 0;  // Point DMA copy to ping buffer
   DmaRxOffset[port] = 0; 
   DmaPongOffset[port] = SAMPLES_PER_INTERRUPT * MaxDmaSlots[port];

   // Setup DMA transfer parameters
   initRxTCCParams (port);
   initTxTCCParams (port);

   // Assign TDM channels to the designated transfer controller
   assignToTC (DMACfg[port].RxChn, TDM_TC+1);
   assignToTC (DMACfg[port].TxChn, TDM_TC);
   
   // Assign events to shadow region
   REG64_RD (EDMA_SHAD0, shadowBits);
   REG64_WR (EDMA_SHAD0, (shadowBits | chnBits));

   // clear all indications of edma events and event interrupts
   REG64_WR (EDMA3_INCR, chnBits);    // Clear interrupt notifications
   REG64_WR (EDMA3_ENCR, chnBits);    // Clear event notifications
   REG64_WR (EDMA3_SNCR, chnBits);    // Clear secondary event notifications
   REG64_WR (EDMA_EMCR, chnBits);     // Clear missed event notifications
   DMAClearErrors ();

   // enable edma events and transfer completion interrupts for port
   REG64_WR (EDMA3_IESR, chnBits);    // Enable completion interrupts
   REG64_WR (EDMA3_EESR, chnBits);    // Enable events
   return chnBits;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ disableDMA
//
// FUNCTION
//   This function resets the EDMA controller
//
// RETURNS
//   
//   Disabled transfer control bits 
//}
ADT_Events disableDMA (int port) {

   ADT_Events chnBits, shadowBits;

   if (NUM_TDM_PORTS <= port) return 0;

   // Only execute disableDMA if the structure for the logical 
   // channel has been initialized as indicated by a non-zero TxAddr member.
   if (DMACfg[port].TxAddr == 0) return 0;

   // Disable DMA parameters   
   nullTCCParams (port);
 

   chnBits = (1 << DMACfg[port].RxChn) | (1 << DMACfg[port].TxChn);

   RxEvents[port] = 0;
   TxEvents[port] = 0;

   // disable all edma events and interrupts for port
   REG64_WR (EDMA3_IECR, chnBits);    // Disable event interrupts
   REG64_WR (EDMA3_EECR, chnBits);    // Disable event notifications

   // clear all indications of edma events and event interrupts
   REG64_WR (EDMA3_INCR, chnBits);    // Clear event notifications
   REG64_WR (EDMA3_ENCR, chnBits);    // Clear event notifications
   REG64_WR (EDMA3_SNCR, chnBits);    // Clear secondary event notifications
   REG64_WR (EDMA_EMCR, chnBits);    // clear missed event notifications 

   // Remove events from shadow region
   REG64_RD (EDMA_SHAD0, shadowBits);
   REG64_WR (EDMA_SHAD0, (~shadowBits & chnBits));


   // Zero transmit buffer to avoid noise on line
   memset (TxDMABuffers[port], 0, sysConfig.dmaBufferSize[port] * 2); 
 
   
   return chnBits;
}



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ initRxTCCParams - Initialize the EDMA parameter ram for receive channels
//
// FUNCTION
//  Initializes EDMA parameter ram for EDMA channels that are driven by receive events
//
// Inputs
//    port        - McBSPn
//
//  Outputs
//    EDMA_PARAM registers for port
//
// RETURNS
//  none
//}
static void initRxTCCParams (int port) {

   ADT_UInt32 Acnt, Bcnt, Ccnt;   // Count of elements in A, B, and C dimensions
   ADT_UInt32 BOffset, COffset;   // Offset of to next element in B and C dimensions
   ADT_UInt32 slotsPerFrame, tcc;
   
   dma3Opts_t dmaOpts;
   ADT_UInt32 srcAddr, dstAddr;
   ADT_UInt32 BcntAcnt, BcntLink;

   ADT_UInt32 startOffset, pingOffset, pongOffset;  // Register offsets to parameter blocks
   ADT_UInt32 startAddr,   pingAddr,   pongAddr;    // Parameter block addresses

   if (NUM_TDM_PORTS <= port) return;

   // Get slotsPerFrame from configuration data
   slotsPerFrame = MaxDmaSlots[port];

   // Get event codes and transfer addresses from static tables
   tcc     = DMACfg[port].RxChn;
   srcAddr = DMACfg[port].RxAddr;
   dstAddr = (ADT_UInt32) RxDMABuffers[port];

   // Calculate offsets into parameter tables
   startOffset = DMACfg[port].RxChn  * PARAM_SIZE;
   pingOffset  = DMACfg[port].RxPing * PARAM_SIZE;
   pongOffset  = pingOffset + PARAM_SIZE;

   startAddr = EDMA_PARAM_ADDR + startOffset;
   pingAddr  = EDMA_PARAM_ADDR + pingOffset;
   pongAddr  = EDMA_PARAM_ADDR + pongOffset;

   //------------- Set up parameter table entries
   // Options register
   dmaOpts.TransferOptions = 0;
   dmaOpts.Bits.SAM        = 0;     // src address not a FIFO
   dmaOpts.Bits.DAM        = 0;     // dst address not a FIFO
   dmaOpts.Bits.SYNCDIM    = 0;     // transfer A bytes per event
   dmaOpts.Bits.STATIC     = 0;     // dynamic parameter entry
   dmaOpts.Bits.FWID       = 0;     // FIFO width = N/A
   dmaOpts.Bits.TCCMOD     = 0;     // notify transfer complete upon full transfer
   dmaOpts.Bits.TCC        = tcc;   // transfer complete code
   dmaOpts.Bits.TCINTEN    = 1;     // interrupt upon transfer complete only
   dmaOpts.Bits.ITCINTEN   = 0;
   dmaOpts.Bits.TCCHEN     = 0;     // no chaining
   dmaOpts.Bits.ITCCHEN    = 0;

   // Number of elements in each dimension
   Acnt = 2;                      // A dimension is bytes within slot
   Bcnt = slotsPerFrame;          // B dimension is slots within frame
   Ccnt = SAMPLES_PER_INTERRUPT;  // C dimension is frame

   // Offset between C1F1 and C2F1 (see diagram at top)
   BOffset = (Acnt * Ccnt);

   // Offset between CNF1 and C1F2 (see diagram at top)
   COffset = Acnt - ((Bcnt-1) * BOffset);


   // Shift B and C offsets into destination field
   BOffset  = BOffset << 16;
   COffset  = COffset << 16;
   BcntAcnt = (Bcnt << 16) | Acnt;

   BcntLink = (Bcnt<<16) | (pongOffset & 0xffff);


   REG_WR ( startAddr,     dmaOpts.TransferOptions);
   REG_WR ((startAddr+4),  srcAddr);
   REG_WR ((startAddr+8),  BcntAcnt);
   REG_WR ((startAddr+12), dstAddr);
   REG_WR ((startAddr+16), BOffset);
   REG_WR ((startAddr+20), BcntLink);
   REG_WR ((startAddr+24), COffset);
   REG_WR ((startAddr+28), Ccnt);

   REG_WR ( pingAddr,     dmaOpts.TransferOptions);
   REG_WR ((pingAddr+4),  srcAddr);
   REG_WR ((pingAddr+8),  BcntAcnt);
   REG_WR ((pingAddr+12), dstAddr);
   REG_WR ((pingAddr+16), BOffset);
   REG_WR ((pingAddr+20), BcntLink);
   REG_WR ((pingAddr+24), COffset);
   REG_WR ((pingAddr+28), Ccnt);

   dstAddr += DmaPongOffset[port] * 2;      // Point to pong offset
   BcntLink = (Bcnt<<16) | (pingOffset & 0xffff);

   REG_WR ( pongAddr,     dmaOpts.TransferOptions);
   REG_WR ((pongAddr+4),  srcAddr);
   REG_WR ((pongAddr+8),  BcntAcnt);
   REG_WR ((pongAddr+12), dstAddr);
   REG_WR ((pongAddr+16), BOffset);
   REG_WR ((pongAddr+20), BcntLink);
   REG_WR ((pongAddr+24), COffset);
   REG_WR ((pongAddr+28), Ccnt);

   // Store address for DMA slippage checking
   DMACfg[port].RxBuf = (ADT_PCM16 **) (startAddr + 12);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ initTxTCCParams - Initialize the EDMA parameter ram for McBSP transmit channels
//
// FUNCTION
//  Initializes EDMA parameter ram for EDMA channels that are driven by transmit events
//
// Inputs
//    port        - McBSPn
//
//  Outputs
//    EDMA_PARAM registers for port
//
// RETURNS
//  none
//}
static void initTxTCCParams (int port) {

   ADT_UInt32 Acnt, Bcnt, Ccnt;   // Count of elements in A, B, and C dimensions
   ADT_UInt32 BOffset, COffset;   // Offset of to next element in B and C dimensions
   ADT_UInt32 slotsPerFrame, tcc;
   
   dma3Opts_t dmaOpts;
   ADT_UInt32 srcAddr, dstAddr;
   ADT_UInt32 BcntAcnt, BcntLink;

   ADT_UInt32 startOffset, pingOffset, pongOffset;  // Register offsets to parameter blocks
   ADT_UInt32 startAddr,   pingAddr,   pongAddr;    // Parameter block addresses

   if (NUM_TDM_PORTS <= port) return;

   // Get slotsPerFrame from configuration data
   slotsPerFrame = MaxDmaSlots[port];

   // Get event codes and transfer addresses from static tables
   tcc     = DMACfg[port].TxChn;
   dstAddr = DMACfg[port].TxAddr;
   srcAddr = (ADT_UInt32) TxDMABuffers[port];

   // Calculate offsets into parameter tables
   startOffset = DMACfg[port].TxChn  * PARAM_SIZE;
   pingOffset  = DMACfg[port].TxPing * PARAM_SIZE;
   pongOffset  = pingOffset + PARAM_SIZE;

   startAddr = EDMA_PARAM_ADDR + startOffset;
   pingAddr  = EDMA_PARAM_ADDR + pingOffset;
   pongAddr  = EDMA_PARAM_ADDR + pongOffset;

   //------------- Set up parameter table entries
   // Options register
   dmaOpts.TransferOptions = 0;
   dmaOpts.Bits.SAM        = 0;     // src address not a FIFO
   dmaOpts.Bits.DAM        = 0;     // dst address not a FIFO
   dmaOpts.Bits.SYNCDIM    = 0;     // transfer A bytes per event
   dmaOpts.Bits.STATIC     = 0;     // dynamic parameter entry
   dmaOpts.Bits.FWID       = 0;     // FIFO width = N/A
   dmaOpts.Bits.TCCMOD     = 0;     // notify transfer complete upon full transfer
   dmaOpts.Bits.TCC        = tcc;   // transfer complete code
   dmaOpts.Bits.TCINTEN    = 1;     // interrupt upon transfer complete only
   dmaOpts.Bits.ITCINTEN   = 0;
   dmaOpts.Bits.TCCHEN     = 0;     // no chaining
   dmaOpts.Bits.ITCCHEN    = 0;

   // Number of elements in each dimension
   Acnt = 2;                      // A dimension is bytes within slot
   Bcnt = slotsPerFrame;          // B dimension is slots within frame
   Ccnt = SAMPLES_PER_INTERRUPT;  // C dimension is frame

   // Offset between C1F1 and C2F1 (see diagram at top)
   BOffset = (Acnt * Ccnt);

   // Offset between CNF1 and C1F2 (see diagram at top)
   COffset = Acnt - ((Bcnt-1) * BOffset);

   // Mask out B and C offsets froms source fields
   BOffset  = BOffset & 0xffff;
   COffset  = COffset & 0xffff;
   BcntAcnt = (Bcnt << 16) | Acnt;

   BcntLink = (Bcnt<<16) | (pongOffset & 0xffff);


   REG_WR ( startAddr,     dmaOpts.TransferOptions);
   REG_WR ((startAddr+4),  srcAddr);
   REG_WR ((startAddr+8),  BcntAcnt);
   REG_WR ((startAddr+12), dstAddr);
   REG_WR ((startAddr+16), BOffset);
   REG_WR ((startAddr+20), BcntLink);
   REG_WR ((startAddr+24), COffset);
   REG_WR ((startAddr+28), Ccnt);

   REG_WR ( pingAddr,     dmaOpts.TransferOptions);
   REG_WR ((pingAddr+4),  srcAddr);
   REG_WR ((pingAddr+8),  BcntAcnt);
   REG_WR ((pingAddr+12), dstAddr);
   REG_WR ((pingAddr+16), BOffset);
   REG_WR ((pingAddr+20), BcntLink);
   REG_WR ((pingAddr+24), COffset);
   REG_WR ((pingAddr+28), Ccnt);

   srcAddr += DmaPongOffset[port] * 2;    // Point to pong buffer
   BcntLink = (Bcnt<<16) | (pingOffset & 0xffff);

   REG_WR ( pongAddr,     dmaOpts.TransferOptions);
   REG_WR ((pongAddr+4),  srcAddr);
   REG_WR ((pongAddr+8),  BcntAcnt);
   REG_WR ((pongAddr+12), dstAddr);
   REG_WR ((pongAddr+16), BOffset);
   REG_WR ((pongAddr+20), BcntLink);
   REG_WR ((pongAddr+24), COffset);
   REG_WR ((pongAddr+28), Ccnt);

   // Store address for DMA slippage checking
   DMACfg[port].TxBuf = (ADT_PCM16 **) (startAddr + 4);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ nullTCCParams - Initialize the EDMA parameter ram for receive channels
//
// FUNCTION
//  Initializes EDMA parameter ram for EDMA channels that are driven by receive events
//
// Inputs
//    port        - McBSPn or McASPn
//    bitwidth        - width of serial word (8, 16, or 32)
//    slotsPerFrame    - Timeslots per tdm frame
//    numSampsPerChan - Samples per channel per DMA interrupt
//
//  Outputs
//    eerl            - Event enable low
//    eerh            - Event enable high
//    cierl           - Channel interrupt enable low
//
// RETURNS
//  none
//}
static void nullTCCParams (int port) {

   ADT_UInt32 startOffset, pingOffset, pongOffset;  // Register offsets to parameter blocks
   ADT_UInt32 *startAddr,  *pingAddr,  *pongAddr;   // Parameter block addresses
   int i;
   
   if (NUM_TDM_PORTS <= port) return;

   // Calculate offsets into Tx parameter tables
   startOffset = DMACfg[port].TxChn  * PARAM_SIZE;
   pingOffset  = DMACfg[port].TxPing * PARAM_SIZE;
   pongOffset  = pingOffset + PARAM_SIZE;

   startAddr = (ADT_UInt32 *) (EDMA_PARAM_ADDR + startOffset);
   pingAddr  = (ADT_UInt32 *) (EDMA_PARAM_ADDR + pingOffset);
   pongAddr  = (ADT_UInt32 *) (EDMA_PARAM_ADDR + pongOffset);

   for (i=0; i<8; i++) {
      REG_WR (startAddr++, 0);
      REG_WR (pingAddr++,  0);
      REG_WR (pongAddr++,  0);
   }

   // Calculate offsets into Rx parameter tables
   startOffset = DMACfg[port].RxChn  * PARAM_SIZE;
   pingOffset  = DMACfg[port].RxPing * PARAM_SIZE;
   pongOffset  = pingOffset + PARAM_SIZE;

   startAddr = (ADT_UInt32 *) (EDMA_PARAM_ADDR + startOffset);
   pingAddr  = (ADT_UInt32 *) (EDMA_PARAM_ADDR + pingOffset);
   pongAddr  = (ADT_UInt32 *) (EDMA_PARAM_ADDR + pongOffset);

   for (i=0; i<8; i++) {
      REG_WR (startAddr++, 0);
      REG_WR (pingAddr++,  0);
      REG_WR (pongAddr++,  0);
   }

}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ DmaLostSync
//
// FUNCTION
//    checks that dma transfer and SWI copy are working in opposite buffers
//
// Inputs
//
//    DmaSlots    - Number of dma slot for port
//    DmsStart    - Start of DMA buffer
//    SWIOffset   - Offset of ping/pong bvffer used by SWI copy
//    DmaPos      - DMA register address pointing to current address of DMA transfer
//
// RETURNS
//    Lost sync indication
//}
static inline ADT_Bool DmaLostSync (int DmaSlots,         ADT_PCM16  *DmaStart,
                                    ADT_UInt16 SWIOffset, ADT_PCM16 **DmaPos) {
   ADT_PCM16 *DmaCur;
   ADT_Bool outOfRange, wrongBuffer;

   DmaCur = *DmaPos;

   // Verify within range of DMA buffer
   outOfRange = (DmaCur < DmaStart) || ((DmaStart + (DmaSlots * 2 * SAMPLES_PER_INTERRUPT)) <= DmaCur);
 
   // Verify within range of current (ping/pong) buffer
   if (SWIOffset == 0) DmaStart += (DmaSlots * SAMPLES_PER_INTERRUPT);
 
   wrongBuffer = (DmaCur < DmaStart) || ((DmaStart + (DmaSlots * SAMPLES_PER_INTERRUPT)) <= DmaCur);
 
   return (outOfRange || wrongBuffer);
}

//  Switch copy between ping and pong buffers
inline switchBuffers (int port, ADT_UInt16 *offset) {
   if (*offset == 0) *offset = DmaPongOffset[port]; 
   else              *offset = 0;
   return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ zeroTxBuffer
//
// FUNCTION
//  Zeroes slot on Tx Buffer when channel goes inactive
//
// Inputs
//    port            - McBSPn
//    dmaSlot         - dma index of slot to zero
//
// RETURNS
//  none
//}
void zeroTxBuffer (int port, int dmaSlot) {
   ADT_PCM16 *txSlot;

   if (NUM_TDM_PORTS <= port) return;

   txSlot = TxDMABuffers[port];

   // Ping buffer fill memory
   txSlot += dmaSlot * SAMPLES_PER_INTERRUPT; 
   memset (txSlot, 0, SAMPLES_PER_INTERRUPT * 2);

   // Pong buffer fill memory
   txSlot +=  DmaPongOffset [port]; 
   memset (txSlot, 0, SAMPLES_PER_INTERRUPT * 2);
}

#ifdef RMF
void AdjustDmaBuffers (ADT_UInt16 numSlots, ADT_PCM16 *DmaBase, int Offset) {

   ADT_PCM16 *NxtBufr, *PrvBufr, LastPcm;
   int i, k;

   NxtBufr = DmaBase + Offset;
   AdjustDmaOffset (Offset);
   PrvBufr = DmaBase + Offset  + SAMPLES_PER_INTERRUPT - 1;

   // Loop through all configured slots zeroing out buffer corresponding ping or pong buffer.
   for (k = 0; k < numSlots; k++)  {
      LastPcm = *PrvBufr;
      LogTransfer (0xAAD, PrvBufr, NxtBufr, SAMPLES_PER_INTERRUPT);
      for (i = 0; i < SAMPLES_PER_INTERRUPT; i++, NxtBufr++)  {
          *NxtBufr = LastPcm;
      }
      NxtBufr += SAMPLES_PER_INTERRUPT;
      PrvBufr += 2 * SAMPLES_PER_INTERRUPT;
   }
   return;
}
#endif

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ CopyTxDmaBuffers - Copy channel PCM buffers to serial port DMA buffers.
//
// FUNCTION
//   Multiplexs PCM data from active channel PCM buffers onto a serial 
//   port's DMA buffers.
//
// Inputs
//     slotCnt       -  Count of TDM slots
//     TxCircBufrList -  List of transmit circular PCM buffer pointers
//     TxDmaBufr     -  Linear dma transmit buffer
//
// RETURNS
//     lostSync indication
//}
ADT_Bool CopyDmaTxBuffers (int port, int slotCnt, CircBufInfo_t **TxCircBufrList, int slip) {
   
   int k, buffCnt, lostSync;
   CircBufInfo_t *TxCirc, **TxCircBufr;
   ADT_PCM16     *TxDmaBufr;
   ADT_UInt16    *TxOffset;
   ADT_PCM16 *DmaBufr0;
   ADT_Word SlipSamps;

   TxOffset = &DmaTxOffset[port];

   // Adjust the SWI copy buffer offset (TxOffset) if the DMA and SWI buffers are conflicting
   lostSync = DmaLostSync (slotCnt, TxDMABuffers[port], *TxOffset, DMACfg[port].TxBuf);
   if (lostSync) {
      switchBuffers (port, TxOffset);
      SendWarningEvent (port + 0x200, WarnFrameSyncError);
   }

   // Set the circular buffer and linear DMA pointers to the first slot.
   TxCircBufr = TxCircBufrList;
   TxDmaBufr  = DmaTxBuff[port] + *TxOffset;
   DmaBufr0   = TxDmaBufr;

   SlipSamps = 0;
   if (slip)
       SlipSamps = SAMPLES_PER_INTERRUPT;

   // Loop through all configured slots checking for active slots.
   for (k = 0; k < slotCnt; k++, TxCircBufr++)  {
      TxCirc = *TxCircBufr;
   
      // Copy from active outbound channels' circular buffer to linear DMA buffer
      TxCirc->SlipSamps += SlipSamps;
      CheckTxPointers (TxCirc, TxDmaBufr);   // Make sure there are enough samples for transfers
      buffCnt = TxCirc->BufrSize - TxCirc->TakeIndex;

      if (buffCnt <= SAMPLES_PER_INTERRUPT) {
         memcpy (TxDmaBufr, &TxCirc->pBufrBase[TxCirc->TakeIndex], buffCnt << 1);
         TxDmaBufr += buffCnt;
         buffCnt = SAMPLES_PER_INTERRUPT - buffCnt;
         TxCirc->TakeIndex = 0;
      } else {
         buffCnt = SAMPLES_PER_INTERRUPT;   
      }
      if (buffCnt) {
         memcpy (TxDmaBufr, &TxCirc->pBufrBase[TxCirc->TakeIndex], buffCnt << 1);
         TxCirc->TakeIndex += buffCnt;
         TxDmaBufr += buffCnt;
      }
      // NOTE: muLawExpand is for testing only
      muLawExpand ((void *)(TxDmaBufr-SAMPLES_PER_INTERRUPT), SAMPLES_PER_INTERRUPT);


#ifdef FRAMESTAMP  // Verify that frame markers are correct
      if (FrameStamp)
         AppErr ("FrameStampError",
                      *(TxDmaBufr-(SAMPLES_PER_INTERRUPT*2)) != 0 &&
                      *(TxDmaBufr-(SAMPLES_PER_INTERRUPT*2)) != (short) (ApiBlock.DmaSwiCnt & 0xFFFF));
#endif


   }
   FixedPatternTx (DmaBufr0);   // Allow overwrite of slot 0 with fixed pattern for debug
   customTDMOut (DmaBufr0, port, slotCnt);

   switchBuffers (port, TxOffset);

   return lostSync;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ CopyDmaRxBuffers - Copy serial port DMA buffers to channel PCM buffers.
//
// FUNCTION
//   Demultiplexs PCM data between a serial port's DMA buffers and the
//   PCM circular buffers of any active channels using the serial port.
//
// Inputs
//     port          -  Count of TDM slots
//     RxCircBufrList -  List of receive circular PCM buffer pointers
//     RxDmaBufr     -  Linear dma receive buffer
//
// RETURNS
//     lostSync indication
//}
ADT_Bool CopyDmaRxBuffers (int port, int slotCnt, CircBufInfo_t **RxCircBufrList, int slip) {
   
   int k, buffCnt, lostSync;
   CircBufInfo_t *RxCirc, **RxCircBufr;
   ADT_PCM16     *RxDmaBufr;
   ADT_UInt16    *RxOffset;
   ADT_Word      SlipSamps;
              
   RxOffset = &DmaRxOffset[port];

   // Adjust the SWI copy buffer offset (RxOffset) if the DMA and SWI buffers are conflicting
   lostSync = DmaLostSync (slotCnt, RxDMABuffers[port], *RxOffset, DMACfg[port].RxBuf);
   if (lostSync) {
      switchBuffers (port, RxOffset);
      SendWarningEvent (port + 0x100, WarnFrameSyncError);
   }

   // Set the circular buffer and linear DMA pointers to the first slot.
   RxCircBufr = RxCircBufrList;
   RxDmaBufr  = DmaRxBuff[port] + *RxOffset;
   
   // Loop through all configured slots checking for active slots.
   FixedPatternRx  (RxDmaBufr);  // Allow overwrite of slot 0 with fixed pattern for debug
   customTDMIn (RxDmaBufr, port, slotCnt);
   
   SlipSamps = 0;
   if (slip)
       SlipSamps = SAMPLES_PER_INTERRUPT;

   for (k = 0; k < slotCnt; k++, RxCircBufr++)  {

      RxCirc = *RxCircBufr; 

      // Copy from active inbound DMA buffer to circular buffer
      RxCirc->SlipSamps += SlipSamps;
      CheckRxPointers (RxCirc, RxDmaBufr);   // Make sure there is enough samples/room for transfers

      // NOTE: muLawCompress for testing only
      muLawCompress ((void *)RxDmaBufr, SAMPLES_PER_INTERRUPT);

      buffCnt = RxCirc->BufrSize - RxCirc->PutIndex;
      if (buffCnt <= SAMPLES_PER_INTERRUPT) {
         memcpy (&RxCirc->pBufrBase[RxCirc->PutIndex], RxDmaBufr, buffCnt << 1);
         RxDmaBufr += buffCnt;
         buffCnt = SAMPLES_PER_INTERRUPT - buffCnt;
         RxCirc->PutIndex = 0;
         if (buffCnt == 0) continue;
      } else {
         buffCnt = SAMPLES_PER_INTERRUPT;   
      }
      memcpy (&RxCirc->pBufrBase[RxCirc->PutIndex], RxDmaBufr, buffCnt << 1);
      RxCirc->PutIndex += buffCnt;
      RxDmaBufr += buffCnt;
   }   

   switchBuffers (port, RxOffset);
   return lostSync;
}

int DmaLbRxToTxBuffers (int port, int slotCnt) {
   int k, buffCnt, txlostSync, rxlostSync, rv;
   ADT_PCM16     *RxDmaBufr;
   ADT_UInt16    *RxOffset;
   ADT_PCM16     *TxDmaBufr;
   ADT_UInt16    *TxOffset;


   rv = 0;              
   RxOffset = &DmaRxOffset[port];
   TxOffset = &DmaTxOffset[port];

   // Adjust the SWI copy buffer offset (RxOffset) if the DMA and SWI buffers are conflicting
   rxlostSync = DmaLostSync (slotCnt, RxDMABuffers[port], *RxOffset, DMACfg[port].RxBuf);
   if (rxlostSync) {
      rv |= 1;
      switchBuffers (port, RxOffset);
      SendWarningEvent (port + 0x100, WarnFrameSyncError);
   }

   // Adjust the SWI copy buffer offset (TxOffset) if the DMA and SWI buffers are conflicting
   txlostSync = DmaLostSync (slotCnt, TxDMABuffers[port], *TxOffset, DMACfg[port].TxBuf);
   if (txlostSync) {
      rv |= 2;
      switchBuffers (port, TxOffset);
      SendWarningEvent (port + 0x200, WarnFrameSyncError);
   }

   // Set the linear DMA pointers to the first slot.
   RxDmaBufr  = DmaRxBuff[port] + *RxOffset;
   TxDmaBufr  = DmaTxBuff[port] + *TxOffset;
   buffCnt = SAMPLES_PER_INTERRUPT;   

   for (k = 0; k < slotCnt; k++)  {
        memcpy (TxDmaBufr, RxDmaBufr, buffCnt << 1);
        RxDmaBufr += buffCnt;
        TxDmaBufr += buffCnt;
   }   

   switchBuffers (port, RxOffset);
   switchBuffers (port, TxOffset);

   return rv;
}



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ GpakEdmaIsr
//
// FUNCTION
//  G.PAK Hardware Interrupt handler for all EDMA Channels.
//  It's triggered by the occurrence of tx and rx mcbsp edma events. The 
//  CIPR register is checked to determine which events have occurred and
//  the SWI_Dma interrupt is issued to start the circular buffer copies.
//
// RETURNS
//  nothing
//}
static void GpakDmaIsr (void) {

   ADT_Events cipr;
   
   // Determine which interrupt has occurred
   REG64_RD (EDMA3_INR, cipr);

#ifdef _DEBUG
   isrCnt++;
   isrFlags |=cipr;
   if (NumActiveChannels) logTime (0x00100000ul | cipr);
#endif

   SWI_or (SWI_Dma, cipr);

   // clear the pending edma interrupts by writing back the register contents
   REG64_WR (EDMA3_INCR, cipr);
   REG_WR (EDMA3_IEVL, 1);
   return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ GpakErrIsr
//
// FUNCTION
//  G.PAK Hardware Interrupt handler for error interrupts
//
//  It's triggered by the occurrence of edma errors.  Status registers
//  are read at the time of the error and stored in global memory for 
//  later review.
//
// RETURNS
//  nothing
//}
static void DMAClearErrors (void) {

   // Channel controller errors
   REG64_RD (EDMA_EMR,    err.MissedEvents);
   REG_RD   (EDMA_CCERR,  err.WaterMarkErr);

   // Transfer controller errors
   REG_RD (TC0_ERR, err.TC0Error);
   REG_RD (TC1_ERR, err.TC1Error);
   REG_RD (TC2_ERR, err.TC2Error);
   REG_RD (TC0_ERR_STAT, err.TC0Stat);
   REG_RD (TC1_ERR_STAT, err.TC1Stat);
   REG_RD (TC2_ERR_STAT, err.TC2Stat);

   // Interrupt status
   REG_RD (INTR_XSTAT, err.IntrStat);

   // Clear error registers
   REG64_WR (EDMA_EMCR,   ALL_EVENTS);
   REG_WR (EDMA_CCERRCLR, 0xffffffff);

   REG_WR (TC0_ERR_STAT_CLR, 0xffffffff);
   REG_WR (TC1_ERR_STAT_CLR, 0xffffffff);
   REG_WR (TC2_ERR_STAT_CLR, 0xffffffff);

   REG_WR (INTR_XSTAT_CLR, 0xffffffff);

   // Generate error interrupt signals if pending
   REG_WR (TC0_EVAL, 1);
   REG_WR (TC1_EVAL, 1);
   REG_WR (TC2_EVAL, 1);
   REG_WR (EDMA_ERREVAL, 1);
   
   // Generate completion interrupt if pending
   REG_WR (EDMA3_IEVL, 1);
   
   err.InternalExc = IERR;
   IERR = 0;
   return;
}

static void GpakErrIsr (void) {
   err.cnt++;
   DMAClearErrors ();

}

