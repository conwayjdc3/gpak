/*
 * Copyright (c) 2014, Adaptive Digital Technologies, Inc.
 *
 * File Name: cust674xMcBSP.c
 *
 * Description:
 *   This file contains Trilogy's custom G.PAK 6748 McBSP driver.
 *
 * Version: 1.0
 *
 * Revision History:
 *   4/2014 - Initial release.
 *      
 */
 
//==============================================
//
//  Build-time configuration values
//
//  sysConfig.dxDelay[port] - Sets channels as 'tri-state' or driven high
//  sysConfig.rxClockPolarity[port]      - Sets signal polarity and source
//  sysConfig.txClockPolarity[port]      - Sets signal polarity and source
//  sysConfig.rxFrameSyncPolarity[port]  - Sets signal polarity and source
//  sysConfig.txFrameSyncPolarity[port]  - Sets signal polarity and source
//

//=====================================================
//  Run-time configuration
//
//  Word     Bits
//Msg[1]     0x0001    = McBSP0 Enable
//Msg[1]     0x0002    = McBSP1 Enable
//Msg[1]     0x0004    = McBSP2 Enable
//Msg[1]     0x0008    = McASP0 Enable
//Msg[1]     0x0010    = McASP1 Enable
//Msg[2-9]   0xffff    = McBSP0 Slot Mask
//Msg[10-17] 0xffff    = McBSP1 Slot Mask
//Msg[18-25] 0xffff    = McBSP2 Slot Mask
//Msg[26-27] 0xffff    = McASP0 Tx Pins
//Msg[28-29] 0xffff    = McASP0 Rx Pins
//Msg[30-31] 0xffff    = McASP0 Slot Mask
//Msg[32-33] 0xffff    = McASP1 Tx Pins
//Msg[34-35] 0xffff    = McASP1 Rx Pins
//Msg[36-37] 0xffff    = McASP2 Slot Mask
//Msg[38]    0x00ff    = CLKDIV0      - Sets generated clock divisor
//Msg[38]    0xff00    = CLKDIV1      - Sets generated clock divisor
//Msg[39]    0x00ff    = CLKDIV2      - Sets generated clock divisor
//Msg[39]    0xff00    = FWID0        - Sets generated frame sync pulse width
//Msg[40]    0x00ff    = FWID1        - Sets generated frame sync pulse width
//Msg[40]    0xff00    = FWID2        - Sets generated frame sync pulse width
//Msg[41]    0xfff0    = FPER0        - Sets generated frame width
//Msg[41]    0x000E    = COMPAND0     - Sets compandingMode and wordsize
//Msg[41]    0x0001    = GRST0        - Enables generated signals
//Msg[42]    0xfff0    = FPER1        - Sets generated frame width
//Msg[42]    0x000E    = COMPAND1     - Sets compandingMode and wordsize
//Msg[42]    0x0001    = GRST0        - Enables generated signals
//Msg[43]    0xfff0    = FPER2        - Sets generated frame width
//Msg[43]    0x000E    = COMPAND2     - Sets compandingMode and wordsize
//Msg[43]    0x0001    = GRST0        - Enables generated signals
//Msg[44]    0x0003    = txDataDelay0 - Sets bit delay from frame sync
//Msg[44]    0x000c    = rxDataDelay0 - Sets bit delay from frame sync
//Msg[44]    0x0030    = txDataDelay1 - Sets bit delay from frame sync
//Msg[44]    0x00c0    = rxDataDelay1 - Sets bit delay from frame sync
//Msg[44]    0x0300    = txDataDelay2 - Sets bit delay from frame sync
//Msg[44]    0x0c00    = rxDataDelay2 - Sets bit delay from frame sync
//
//
//  sysConfig.rxDataDelay[port] = rxDataDelayx      - Delay between FS and first Rx bit
//  sysConfig.txDataDelay[port] = txDataDelayx      - Delay between FS and first Tx bit
//  sysConfig.compandingMode[port] = COMPANDx       - u-Law, A-law, none, ...
//  sysConfig.serialWordSize[port] (from COMPANDx   - bits per sample
//
//  genClockEnable[port] = GRSTx                    - Sample clock generates internal signals
//  ClkDiv[port]         = CLKDIVx                  - System clock to bit clock divisor
//  PulseWidth[port]     = FWIDx                    - Frame sync pulse width (bits)
//  FrameWidth[port]     = FPERx                    - Frame width (bits)



// Application related header files.
#include <std.h>
#include <hwi.h>
#include <clk.h>
#include <string.h>

#include "adt_typedef.h"
#include "GpakExts.h"
#include "GpakHpi.h"
#include "GpakPcm.h"
//#include "GpakDefs.h"

//#define ENABLE_BFIFO


ADT_UInt32 ppsMcBspTest1 = 0;


#if 0

// McBSP Base Addresses
#ifdef _TMS320C6400_PLUS
   #include "64plus.h"
#else
   #include "64x.h"
#endif

#endif

// McBSP Register offsets (32-bits per unit)
#define SPCR   0x08
#define RCR    0x0C
#define XCR    0x10
#define SRGR   0x14
#define MCR    0x18
#define RCERE0 0x1C
#define XCERE0 0x20
#define PCR    0x24
#define RCERE1 0x28
#define XCERE1 0x2C
#define RCERE2 0x30
#define XCERE2 0x34
#define RCERE3 0x38
#define XCERE3 0x3C

//  SPCR masks
#define RRST    0x00000001   // Rx reset
#define DXENA   0x00000080   // Dx delay enable
#define RJZF    0x00000000   // Right justify zero fill
#define RJSE    0x00002000   // Right justify sign extend
#define LJZF    0x00004000   // Left justify zero fill
#define RSYNERR 0x00000008
#define DLB     0x00008000   // Digital Loopback 
#define XRST    0x00010000   // Tx reset
#define XRDY    0x00020000
#define XEMPTY  0x00040000
#define XSYNERR 0x00080000
#define GRST    0x00400000   // Clock generation reset
#define FRST    0x00800000   // Frame sync reset

// RCR/XCR masks
#define CMPDMSB 0x0000   // linear, msb first
#define NOFS    0x0004
#define CMPDU   0x0010   // u-law
#define CMPDA   0x0018   // a-law
#define WD8     0x0000   // 8-bit word
#define WD16    0x0040   // 16-bit word

//  PCR masks
#define CLKRP 0x0001   // Rx clk polarity
#define CLKXP 0x0002   // Tx clk polarity
#define FSRP  0x0004   // Rx frame sync polarity
#define FSXP  0x0008   // Tx frame sync polarity
#define CLKRM 0x0100   // Rx clk source
#define CLKXM 0x0200   // Tx clk source
#define FSRM  0x0400   // Rx frame sync source
#define FSXM  0x0800   // Tx frame sync source

// FSGR masks
#define FSGM  0x1000   // Frame sync generated by frame boundary
#define CLKSM 0x2000   // Sample generator clock source is CPU

// FIFO control register offsets
#define TXFIFO      0x10   // Write FIFO Control Register
#define RXFIFO      0x18   // Read FIFO Control Register
#define TXFIFOSTS   0x14   // Write FIFO Status Register
#define RXFIFOSTS   0x1C   // Read FIFO Status Register
#define FIFO_ENABLE   0x00010000
#define BFIFO_WFIFOSTS_WLVL (0x000000FFu)
// McBSP Addressing

#define MCBSP_READ(addr, reg)         (*(volatile ADT_UInt32 *) (addr+reg))
#define MCBSP_WRITE(addr, reg, value) {*((volatile ADT_UInt32 *) (addr+reg)) = (ADT_UInt32) value;}
#define MCBSP_AND(addr, reg, value)   {*((volatile ADT_UInt32 *) (addr+reg)) &= (ADT_UInt32) value;}
#define MCBSP_OR(addr, reg, value)    {*((volatile ADT_UInt32 *) (addr+reg)) |= (ADT_UInt32) value;}
volatile int DELAY_MICROSEC_i;
#define DELAY_MICROSEC(x)  for(DELAY_MICROSEC_i=0; DELAY_MICROSEC_i<300*x; DELAY_MICROSEC_i++) 



// Serial port (stream) configuration related variables.
typedef struct {
   GpakActivation Enabled;
   
   // Clock/Frame sync register values
   ADT_UInt32 McSPCR;   //  Serial Port Control
                   //      TxSyncErr:19,  RxSyncErr:3
                   //      TxReset:15, Justify:14-13; Dx Delay enable:7; RxReset:0
   ADT_UInt32 McPCR;    //  Pin Control
                   //      TxFrameSyncPolarity:3, RxFrameSyncPolarity:2,
                   //      TxClockPolarity:1, RxClockPolarity:0
   ADT_UInt32 McRCR;    //  Rcv Control
                   //      Compand:20-19, Delay:17-16, Slots/Frame:14-8, WordSize:7-5
   ADT_UInt32 McXCR;    //  Xmt Control
                   //      Compand:20-19, Delay:17-16, Slots/Frame:14-8, WordSize:7-5

   // Multi-channel register values
   ADT_UInt32 McMCR;     // Multi Chan Control
                    //    128 channel xmit mode:25, XmtMode:17-16,
                    //    128 channel rcv mode:9,   RcvMode: 0
   ADT_UInt32 McRCER[4]; // Bit masks identifying which of the 128 channels are received
                    //    These values are updated when McBsps are started
   ADT_UInt32 McXCER[4]; // Bit masks identifying which of the 128 channels are transmitted
                    //    These values are updated when channels are activated/deactivated.
   
   ADT_UInt32 McSRGR;   // sample rate generator
   ADT_UInt32 TxFifoCtrl;
   ADT_UInt32 RxFifoCtrl;
} McBspInfo;

static McBspInfo McBsp[NUM_TDM_PORTS] = { 
  { Disabled }, 
  { Disabled },
  { Disabled }
};

/* IMPORTANT: McBSP_base[] physical base addresses assigned to the logical ports 
    MUST be initialized by the custom Gpak startup function in the project 
    Initialization source C file. */

int mcbsp_loopback = 0;
ADT_UInt32 McBSP_Base[] = { 0, 0, 0 };
ADT_UInt32 McBSP_Fifo[NUM_TDM_PORTS] = { 0, 0, 0 };

ADT_UInt16 mcBSPSlotsNeeded[NUM_TDM_PORTS];
ADT_UInt16 TxMcBSPSlotsNeeded[NUM_TDM_PORTS];
ADT_Bool setupMcBSP (int port);
ADT_Bool stopMcBSP  (int port);
ADT_Bool startMcBSP (int port);

TDM_Port McBSPPort = {
   startMcBSP, stopMcBSP, setupMcBSP
};


//  Stored McBSP configuration message values
ADT_UInt16 ClkDiv[NUM_TDM_PORTS]     = { 0, 0, 0 }; // Clock divisor (0-255)
ADT_UInt16 PulseWidth[NUM_TDM_PORTS] = { 0, 0, 0 }; // Width (0-255 bits) of frame sync pulse
ADT_UInt16 FrameWidth[NUM_TDM_PORTS] = { 0, 0, 0 }; // Width (0-4095 bits) of frame
GpakActivation genClockEnable[NUM_TDM_PORTS] = { Disabled, Disabled, Disabled };

extern ADT_UInt16 MaxDmaSlots [NUM_TDM_PORTS];  // Max slots available for DMA [fixed at build]
extern volatile ADT_UInt16 MatchDmaFlags;    // DMA flag bits needed to post SWI
extern int custom_cmd;

void ConfigureMcBSP (ADT_UInt32 McBspEnable[NUM_TDM_PORTS], ADT_UInt32 McBspSlotMask[NUM_TDM_PORTS][4]);
ADT_UInt16 determineLastSlot (ADT_UInt32 Mask, ADT_UInt16 offset);


// - - - - - - - - - -  Stubs for McASP related functions.  - - - - - - - - - -

#pragma CODE_SECTION (PowerOnMcASPs, "SLOW_PROG_SECT")
void PowerOnMcASPs()
{

	return;
}

#pragma CODE_SECTION (clearTDMErrors, "FAST_PROG_SECT")
ADT_UInt32 clearTDMErrors(int port)
{

   return 1;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// validCompanding
//
// FUNCTION
//    Verification of companding mode parameter
//
// PARAMETERS
//
//     mode - Companding mode
//
// RETURNS
//    TRUE - companding mode supported
//
ADT_Bool validCompanding(GpakCompandModes mode) {
   return (cmpPCMU <= mode && mode <= cmpNone8);
}

static void bitDelay (int count, int port) { 
   int bitsPerFrame;
   unsigned long ticksPerFrame, startTime, delayTime;
   
   startTime = CLK_gethtime ();

   ticksPerFrame = CLK_countspms () / sysConfig.samplesPerMs;
   bitsPerFrame = sysConfig.serialWordSize[port] * sysConfig.numSlotsOnStream[port];
   delayTime = ((ticksPerFrame / bitsPerFrame) + 1) * count;

   while (CLK_gethtime () - startTime < delayTime);
}

static void bitDelayDiv (int count, int port) { 
   int bitsPerFrame;
   unsigned long ticksPerFrame, startTime, delayTime;
   
   startTime = CLK_gethtime ();

   ticksPerFrame = CLK_countspms () / sysConfig.samplesPerMs;
   bitsPerFrame = sysConfig.serialWordSize[port] * sysConfig.numSlotsOnStream[port];
   delayTime = ((ticksPerFrame / bitsPerFrame) + 1) / count;

   while (CLK_gethtime () - startTime < delayTime);
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// PowerOnMcBSPs
//
// FUNCTION
//  
//     Power on McBSPs and setup p
//
// PARAMETERS
//
//     None
//

static void PowerOnMcBSPs () {

   int intrMask;

   volatile ADT_UInt32 *PscCmd  = PSC1_CMD;
   volatile ADT_UInt32 *PscStat = PSC1_STAT;
   volatile ADT_UInt32 *PscReg  = PSC1_REG;
   volatile ADT_UInt32 *PinMux1 = CFG_PINMUX1;

   intrMask = HWI_disable ();
   //---------------------------------------------------
   // 1. Wait for previous power transition to complete
   while (*PscStat & 1);

   //---------------------------------------------------
   //  2. Enable clock domains for McBSPs (McBSP0 and McBSP1)
//   PscReg[PSC_McBSP0] = PSC_ON;
   PscReg[PSC_McBSP1] = PSC_ON;

   //---------------------------------------------------
   //  3. Transition power
   *PscCmd = 1;

   //---------------------------------------------------
   //  4. Wait for power transition to complete
   while (*PscStat & 1);

   //---------------------------------------------------
   //  5. Set pin mux to use McBSPs
   UnlockSysRegs ();
   *PinMux1 &= MUX1_McBSPMask;
   *PinMux1 |= MUX1_McBSPActive;
   HWI_restore (intrMask);

}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// storeMcBSPConfiguration
//
// FUNCTION
//  
//     Stores configured values for McBSP registers.  
//     Called at start up and after configuration request verified
//
// PARAMETERS
//
//     port - McBSP port index
//
int DEBUG_McBsp = 0;
void storeMcBSPConfiguration (int port) {
   McBspInfo *Bsp;
   ADT_Word compand, wordSize, pcr, bitsPerSample, cmpdMode;

   // Configuration dependent serial I/O parameters.
   if (NUM_TDM_PORTS <= port) return;
   Bsp = &McBsp[port];

   //  -- SPCR --
   Bsp->McSPCR = RJSE;
   if (sysConfig.dxDelay[port] != 0) Bsp->McSPCR |= DXENA;
   if (DEBUG_McBsp) Bsp->McSPCR |= DLB;
   if (mcbsp_loopback) Bsp->McSPCR |= DLB;
   //-- RCR and XCR --
   compand  = CMPDMSB;    // msb first
   wordSize = WD8;        // 8-bit
   bitsPerSample = 8;
   cmpdMode = sysConfig.compandingMode[port];
   if (cmpdMode == cmpPCMU)  compand = CMPDU;      // u law
   if (cmpdMode == cmpPCMA)  compand = CMPDA;      // A law
   else if (cmpdMode == cmpNone16) {
      wordSize = WD16;     // 16-bit
      bitsPerSample = 16;
   }
   sysConfig.serialWordSize[port] = bitsPerSample;

   Bsp->McRCR = compand | (sysConfig.rxDataDelay[port] & 0x0003); // NOFS;
   Bsp->McRCR = (Bsp->McRCR << 16) | ((SltsPerFrame[port] - 1) << 8) | wordSize;

   Bsp->McXCR = compand | (sysConfig.txDataDelay[port] & 0x0003); // NOFS;
   Bsp->McXCR = (Bsp->McXCR << 16) | ((TxSltsPerFrame[port] - 1) << 8) | wordSize;

   //  -- PCR --
   if ((SltsPerFrame[port] == 0) && (TxSltsPerFrame[port] == 0)) {
      pcr = 0x3000;     // Configure for GPIO
   } else {
      pcr = 0;

      // Signal polarities
      if (sysConfig.rxClockPolarity[port] & 1)      pcr |= CLKRP;
      if (sysConfig.txClockPolarity[port] & 1)      pcr |= CLKXP;
      if (sysConfig.rxFrameSyncPolarity[port] & 1)  pcr |= FSRP;
      if (sysConfig.txFrameSyncPolarity[port] & 1)  pcr |= FSXP;

      // Signal sources 2 = internal(1)/external(0)
      if (genClockEnable[port]) {
         if (sysConfig.rxClockPolarity[port] & 2)      pcr |= CLKRM;
         if (sysConfig.txClockPolarity[port] & 2)      pcr |= CLKXM;
         if (sysConfig.rxFrameSyncPolarity[port] & 2)  pcr |= FSRM;
         if (sysConfig.txFrameSyncPolarity[port] & 2)  pcr |= FSXM;

         if(custom_cmd == 7) //EVM_LOOPBACK
         {
        	 // Set up McBsp to generate xmit and rcv clocks
        	 pcr |= CLKRM;
        	 pcr |= CLKXM;
        	 pcr |= FSRM;
        	 pcr |= FSXM;
         }
      }
   }
   Bsp->McPCR = pcr; 

   // enhanced multi-channel mode:   Rx channels enabled via RCERX.
   Bsp->McMCR = 0x02030201;    //    Tx channels enabled via XCERX. Disabled float.

   // -- SRGR --
   Bsp->McSRGR = FSGM | CLKSM | FrameWidth[port];
   Bsp->McSRGR = (Bsp->McSRGR << 16) | (((PulseWidth[port]) & 0x00ff) << 8) | ClkDiv[port];

}

#define MUX1_GPIO0_3 0x00080000   // McBSP data pins (8-15)
#define MUX1_NONFSX1 0xFFFDFFFF   // McBSP data pins (8-15)
#define MUX1_NONGPIO0_3 0xFFF7FFFF   // McBSP data pins (8-15)
#define GPIO0_DIR    0x01E26010 
#define GPIO0_IN_DATA    0x01E26020 
#define GPIO_3_SHIFT      3   /* pin as an input */
#define GPIO_DIR_INPUT      1   /* pin as an input */
#define GPIO_DIR_OUTPUT     0   /* pin as an output */
#define PSC_GPIO 3
void multiplexFSX_GPIO(int port) {
   int intrMask;

   volatile ADT_UInt32 *PscCmd  = PSC1_CMD;
   volatile ADT_UInt32 *PscStat = PSC1_STAT;
   volatile ADT_UInt32 *PscReg  = PSC1_REG;
   volatile ADT_UInt32 *PinMux1 = CFG_PINMUX1;

   volatile ADT_UInt32 *PinReg;
   ADT_UInt32 val;

   intrMask = HWI_disable ();
   // 1. Wait for previous power transition to complete
   while (*PscStat & 1);

   //---------------------------------------------------
   //  2. Enable clock domains for McBSPs (McBSP0 and McBSP1)
//   PscReg[PSC_McBSP0] = PSC_ON;
   PscReg[PSC_GPIO] = PSC_ON;

   //---------------------------------------------------
   //  3. Transition power
   *PscCmd = 1;

   //---------------------------------------------------
   //  4. Wait for power transition to complete
   while (*PscStat & 1);

   //1. Using a GPIO pin multiplexed with the McBSP FSX signal
   //---------------------------------------------------
   //  Set pin mux to use GPIO[0]:3
   *PinMux1 &= MUX1_NONFSX1; //turn off FSX
   *PinMux1 |= MUX1_GPIO0_3; // turn GPIO0[3]

   // Configure GPIO[0] pin 3 as an input
   val = MCBSP_READ(GPIO0_DIR, 0);
   val = val | ( 1 << GPIO_3_SHIFT);
   MCBSP_WRITE(GPIO0_DIR, 0, val);

   HWI_restore (intrMask);
}

//#define CAPT_FSX
#ifdef CAPT_FSX
extern CircBufInfo_t captureA;

typedef struct fsx_hist_t {
    ADT_UInt32 low;
    ADT_UInt32 high;
} fsx_hist_t;
fsx_hist_t *fsx_hist;
ADT_UInt32 fsx_hist_len;
ADT_UInt32 fsx_hist_idx;
#endif

ADT_UInt32 fsx_low_count;
ADT_UInt32 fsx_high_count;

void waitForExternalFrameSync( int port) {
   //int intrMask;
   ADT_UInt32 BspBase;
   volatile ADT_UInt32 val;

   BspBase = McBSP_Base[port];
   if (BspBase == NULL) return;

#ifdef CAPT_FSX
    fsx_hist_idx = 0;
    fsx_hist =  (fsx_hist_t *)captureA.pBufrBase;
    fsx_hist_len = sizeof(ADT_UInt16)*captureA.BufrSize/sizeof(fsx_hist_t);
    //do {
#endif

   fsx_low_count = 0; 
   fsx_high_count = 0; 
   val = (MCBSP_READ(GPIO0_IN_DATA, 0) & ( 1 << GPIO_3_SHIFT));

   // wait for rising edge of FS signal
   while (val == 0) {
      val = (MCBSP_READ(GPIO0_IN_DATA, 0) & ( 1 << GPIO_3_SHIFT));
      fsx_low_count++;
   }

   // wait for falling edge of FS signal
   val = (MCBSP_READ(GPIO0_IN_DATA, 0) & ( 1 << GPIO_3_SHIFT));
   while (val != 0) {
      val = (MCBSP_READ(GPIO0_IN_DATA, 0) & ( 1 << GPIO_3_SHIFT));
      fsx_high_count++;
   }

#ifdef CAPT_FSX
    fsx_hist[fsx_hist_idx].low  = fsx_low_count;
    fsx_hist[fsx_hist_idx++].high = fsx_high_count;
  //} while(fsx_hist_idx < fsx_hist_len);
#endif

}

void multiplexGPIO_FSX(int port) {
   //int intrMask;

   volatile ADT_UInt32 *PscCmd  = PSC1_CMD;
   volatile ADT_UInt32 *PscStat = PSC1_STAT;
   volatile ADT_UInt32 *PscReg  = PSC1_REG;
   volatile ADT_UInt32 *PinMux1 = CFG_PINMUX1;

   //intrMask = HWI_disable ();

   // 1. Wait for previous power transition to complete
   //while (*PscStat & 1);

   //---------------------------------------------------
   //  2. Enable clock domains for McBSPs (McBSP0 and McBSP1)
   //   PscReg[PSC_McBSP0] = PSC_ON;
   //PscReg[PSC_GPIO] = PSC_OFF;

   //---------------------------------------------------
   //  3. Transition power
   //*PscCmd = 1;

   //---------------------------------------------------
   //  4. Wait for power transition to complete
   //while (*PscStat & 1);

   //1. Using a GPIO pin multiplexed with the McBSP FSX signal
   //---------------------------------------------------
   //  Set pin mux to use McBSPs
   //*PinMux1 &= MUX1_McBSPMask; //MUX1_McBSPMask;
   *PinMux1 &= MUX1_NONGPIO0_3;  // turn GPIO
   *PinMux1 |= MUX1_McBSPActive; // turn on FSX

   //HWI_restore (intrMask);

}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// setupMcBsp
//
// FUNCTION
//  
//     Writes McBSP registers in preparation for startup
//
//    NOTE: if DX is disabled, we will force transmission of zeroes on inactive slots
//          if DX is enabled, inactive slots will float
//
// PARAMETERS
//
//     port - McBSP port index
//

ADT_Bool setupMcBSP (int port) {
   int intrMask;
   ADT_UInt32 BspBase, Fifo;
   McBspInfo *Bsp;

   if (NUM_TDM_PORTS <= port) return FALSE;
   Bsp = &McBsp[port];

   if (Bsp->Enabled == Disabled) return FALSE;
  
   BspBase = McBSP_Base[port];
   Fifo = McBSP_Fifo [port];
   
   if (BspBase == NULL) return FALSE;

   //intrMask = HWI_disable ();

   // Initialization Procedure When External Device is Frame Sync Master
   if (genClockEnable[port] == Disabled) {
      multiplexFSX_GPIO(port);
   
   }
   MCBSP_WRITE(BspBase, SPCR, 0x00000000); // reset mcbsp


   // Step 2) Program control registers
#ifdef ENABLE_BFIFO
   //----------- 2) Enable  Audio FIFOs
   MCBSP_WRITE (Fifo, TXFIFO, 0);
   MCBSP_WRITE (Fifo, RXFIFO, 0);

   DELAY_MICROSEC(5);
#endif
   // Pin Control registers
   MCBSP_WRITE (BspBase, PCR, Bsp->McPCR);
   MCBSP_WRITE (BspBase, RCR, Bsp->McRCR);
   MCBSP_WRITE (BspBase, XCR, Bsp->McXCR);


   // Multi Channel Control registers.
   MCBSP_WRITE (BspBase, RCERE0, Bsp->McRCER[0]);
   MCBSP_WRITE (BspBase, RCERE1, Bsp->McRCER[1]);
   MCBSP_WRITE (BspBase, RCERE2, Bsp->McRCER[2]);
   MCBSP_WRITE (BspBase, RCERE3, Bsp->McRCER[3]);

   if (sysConfig.dxDelay[port] == 0) {
      MCBSP_WRITE (BspBase, XCERE0, Bsp->McRCER[0]);
      MCBSP_WRITE (BspBase, XCERE1, Bsp->McRCER[1]);
      MCBSP_WRITE (BspBase, XCERE2, Bsp->McRCER[2]);
      MCBSP_WRITE (BspBase, XCERE3, Bsp->McRCER[3]);
   } else {
      MCBSP_WRITE (BspBase, XCERE0, Bsp->McXCER[0]);
      MCBSP_WRITE (BspBase, XCERE1, Bsp->McXCER[1]);
      MCBSP_WRITE (BspBase, XCERE2, Bsp->McXCER[2]);
      MCBSP_WRITE (BspBase, XCERE3, Bsp->McXCER[3]);
   }
   MCBSP_WRITE (BspBase, MCR,    Bsp->McMCR);

   // Sample rate generation registers
   MCBSP_WRITE (BspBase, SRGR, Bsp->McSRGR);

   // Serial port command registers
   MCBSP_WRITE (BspBase, SPCR, Bsp->McSPCR);

   // Step 3) wait for internal synchronization
   //bitDelay (2, port);
   DELAY_MICROSEC(1);	// delay for internal synchronization
   //HWI_restore (intrMask);

   if (genClockEnable[port] == Enabled) {
   
      // Step 4) Start sample rate generator and waitfor 2 clock cycles
      MCBSP_OR (BspBase, SPCR, GRST);

      // wait for two CLKG cycles: (CLKDIV * 4 CPUclocks) * 2 
      bitDelay (2, port);
   }

#if 0 // RC debug ... skipping this step fixes the 1-slot TX alignment issue. 
   // Step 5) Ignore initial frame sync error
   // a) release transmit from reset
   MCBSP_OR (BspBase, SPCR, XRST);
      
   // b) wait for unexpected sync error
   //bitDelay (2, port);
    DELAY_MICROSEC(1);	// delay for internal synchronization

   // c) clear XRST to clear any sync errors
   MCBSP_AND (BspBase, SPCR, ~XRST);
#endif
   
   // NOTE:  Step 6) Setup DMA is started by GpakPCM.c
   return TRUE;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// startMcBSP
//
// FUNCTION
//  
//     Write McBSP registers to activate transmit and receive
//
// PARAMETERS
//
//     port - McBSP port index
//


ADT_UInt32 startSPCR;
ADT_Bool startMcBSP (int port) {
   int i, intrMask, var;
   int xempty, timeout = 5000;
   ADT_UInt32 BspBase, Fifo;
   McBspInfo *Bsp;

   if (NUM_TDM_PORTS <= port) return FALSE;

   BspBase = McBSP_Base[port];
   Fifo = McBSP_Fifo [port];
   if (BspBase == NULL) return FALSE;
   Bsp = &McBsp[port];

   intrMask = HWI_disable ();

#ifdef ENABLE_BFIFO
   //----------- 2) Enable  Audio FIFOs
   MCBSP_WRITE (Fifo, TXFIFO, Bsp->TxFifoCtrl);
   MCBSP_WRITE (Fifo, RXFIFO, Bsp->RxFifoCtrl);

   MCBSP_WRITE (Fifo, TXFIFO, Bsp->TxFifoCtrl | FIFO_ENABLE);
   MCBSP_WRITE (Fifo, RXFIFO, Bsp->RxFifoCtrl | FIFO_ENABLE);
   //DELAY_MICROSEC(5);
	// wait for FIFO to be serviced by EDMA
   while(( (MCBSP_READ(Fifo, TXFIFOSTS) & BFIFO_WFIFOSTS_WLVL) == 0 ) &&  (timeout--));
#endif
   // Step 7) Enable McBSP transmit and receive
   //set RRST to release recv from reset

      // Initialization Procedure When External Device is Frame Sync Master
      if (genClockEnable[port] == Disabled)
            waitForExternalFrameSync(port);

      MCBSP_OR (BspBase, SPCR, RRST);

      //set XRST to release transmit from reset
      MCBSP_OR (BspBase, SPCR, XRST);

      // Initialization Procedure When External Device is Frame Sync Master
	  if (genClockEnable[port] == Disabled)
        multiplexGPIO_FSX(port);
   
   // Step 8) Is not a step. It identifies where the frame sync is generated
   startSPCR = MCBSP_READ (BspBase, SPCR);

   if (!genClockEnable[port]) {
      HWI_restore (intrMask);
      return TRUE;
   }
   // Step 9) Wait for DXR to fill
   // poll XEMPTY until it equals 1
   xempty = startSPCR & XEMPTY;
   timeout = 5000;
   while (xempty == 0 && timeout--) {
      xempty = MCBSP_READ (BspBase, SPCR) & XEMPTY;
   };

   
   // Step 10) Start Frame sync generator
   MCBSP_OR (BspBase, SPCR, FRST);
   HWI_restore (intrMask);

   return TRUE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// stopMcBSP
//
// FUNCTION
//  
//     Write McBSP registers to deactivate: tx, rx
//     frame sync and sample clk
//
// PARAMETERS
//
//     port - McBSP port index
//
ADT_Bool stopMcBSP (int port) {

   ADT_UInt32 BspBase, Fifo;
   McBspInfo *Bsp;

   if (NUM_TDM_PORTS <= port) return FALSE;

   BspBase = McBSP_Base[port];
   Fifo = McBSP_Fifo [port];
   if (BspBase == NULL) return FALSE;

   Bsp = &McBsp[port];

   if (Bsp->Enabled == Disabled) return FALSE;

   MCBSP_AND (BspBase, SPCR, (~(XRST | RRST | GRST | FRST)));  
   MCBSP_WRITE (Fifo, TXFIFO, 0);
   MCBSP_WRITE (Fifo, RXFIFO, 0);

   if (McBsp[port].Enabled)     return TRUE;
   else                         return FALSE;

}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// SetTransmitEnables
//
// FUNCTION
//   Writes McBSP registers to enable individual channels for transmission
//
//   NOTE: if DX is disabled, we force transmission of zeroes on inactive slots
//         if DX is enabled, inactive slots will float
//
// PARAMETERS
//
//     port - McBSP port index
//     Mask - Mask of individual time slots to enable
//
void SetTransmitEnables (int port, ADT_UInt32 Mask[]) {

   ADT_UInt32 BspBase;
   int i;
   McBspInfo *Bsp;

   if (NUM_TDM_PORTS <= port) return;
   
   Bsp = &McBsp[port];
   BspBase = McBSP_Base[port];
   
   if (sysConfig.dxDelay[port] == 0) return;

   for (i = 0; i < 4; i++) {
      Bsp->McXCER[i]  |= Mask[i];
   }
   MCBSP_WRITE (BspBase, XCERE0, Bsp->McXCER[0]);
   MCBSP_WRITE (BspBase, XCERE1, Bsp->McXCER[1]);
   MCBSP_WRITE (BspBase, XCERE2, Bsp->McXCER[2]);
   MCBSP_WRITE (BspBase, XCERE3, Bsp->McXCER[3]);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ClearTransmitEnables
//
// FUNCTION
//   Writes McBSP registers to disable individual channels for transmission 
//
//   NOTE: if DX is disabled, we force transmission of zeroes on inactive slots
//         if DX is enabled, inactive slots will float
//
// PARAMETERS
//
//     port - McBSP port index
//     Mask - Mask of individual time slots to enable
//
void ClearTransmitEnables (int port, ADT_UInt32 Mask[]) {

   ADT_UInt32 BspBase;
   int i;
   McBspInfo *Bsp;

   if (NUM_TDM_PORTS <= port) return;
   if (sysConfig.dxDelay[port] == 0) return;

   Bsp = &McBsp[port];
   BspBase = McBSP_Base[port];
   
   for (i = 0; i < 4; i++) {
      Bsp->McXCER[i] &= ~Mask[i];
   }
   MCBSP_WRITE (BspBase, XCERE0, Bsp->McXCER[0]);
   MCBSP_WRITE (BspBase, XCERE1, Bsp->McXCER[1]);
   MCBSP_WRITE (BspBase, XCERE2, Bsp->McXCER[2]);
   MCBSP_WRITE (BspBase, XCERE3, Bsp->McXCER[3]);
   return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// McBSPFrameError
//
// FUNCTION
//    Determine if frame sync error occurred on McBSP
//
// RETURNS
//    TRUE - frame sync error detected
//
int lastSPCR;
ADT_Bool McBSPFrameError (int port) {

   ADT_UInt32 BspBase;
   McBspInfo *Bsp;

   Bsp = &McBsp[port];
   if (Bsp->Enabled == Disabled) return 0;

   BspBase = McBSP_Base[port];

   lastSPCR = MCBSP_READ (BspBase, SPCR);
   MCBSP_WRITE (BspBase, SPCR, (lastSPCR & ~(XSYNERR | RSYNERR)));
   return  ((lastSPCR & (XSYNERR | RSYNERR)) != 0);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ProcConfigSerialPortsMsg - Process a Configure Serial Ports message.
//
// FUNCTION
//   Performs processing for a host's Configure Serial Ports message
//   Includes: parsing, parameter validation and serial port register store
//
// PARAMETERS
//
//     pCmd   - Command buffer
//     pReply - Reply buffer
//
// RETURNS
//   Parameter validation status
//
GPAK_PortConfigStat_t ProcConfigSerialPortsMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {

   ADT_UInt32 startDmaCnt;
   ADT_UInt32 McBspEnable[3];
   ADT_UInt32 SlotMask[3][4];   
   ADT_UInt32 txDataDelay[3];
   ADT_UInt32 rxDataDelay[3];
   GpakCompandModes compMode[3];
   ADT_UInt32 Mask;
   ADT_UInt16 *SlotMap;

   int i, j, k, LastSlot;
   unsigned long startTime;

   // Prepare the reply message.
   pReply[0] |= (MSG_CONFIG_PORTS_REPLY << 8);

   // Make sure there are no channels currently active.
   if (NumActiveChannels != 0)
      return Pc_ChannelsActive;

   //--------------------------------------------------------------------------
   // Parse message for McBSP configuration parameters
   //--------------------------------------------------------------------------
   McBspEnable[0] = (ADT_UInt32)(pCmd[1] & 0x0001);
   McBspEnable[1] = (ADT_UInt32)(pCmd[1] & 0x0002);
   McBspEnable[2] = (ADT_UInt32)(pCmd[1] & 0x0004);


   SlotMask[0][0] = (((ADT_UInt32)pCmd[2])<<16) | (ADT_UInt32)pCmd[3];
   SlotMask[0][1] = (((ADT_UInt32)pCmd[4])<<16) | (ADT_UInt32)pCmd[5];
   SlotMask[0][2] = (((ADT_UInt32)pCmd[6])<<16) | (ADT_UInt32)pCmd[7];
   SlotMask[0][3] = (((ADT_UInt32)pCmd[8])<<16) | (ADT_UInt32)pCmd[9];

   SlotMask[1][0] = (((ADT_UInt32)pCmd[10])<<16) | (ADT_UInt32)pCmd[11];
   SlotMask[1][1] = (((ADT_UInt32)pCmd[12])<<16) | (ADT_UInt32)pCmd[13];
   SlotMask[1][2] = (((ADT_UInt32)pCmd[14])<<16) | (ADT_UInt32)pCmd[15];
   SlotMask[1][3] = (((ADT_UInt32)pCmd[16])<<16) | (ADT_UInt32)pCmd[17];

   SlotMask[2][0] = (((ADT_UInt32)pCmd[18])<<16) | (ADT_UInt32)pCmd[19];
   SlotMask[2][1] = (((ADT_UInt32)pCmd[20])<<16) | (ADT_UInt32)pCmd[21];
   SlotMask[2][2] = (((ADT_UInt32)pCmd[22])<<16) | (ADT_UInt32)pCmd[23];
   SlotMask[2][3] = (((ADT_UInt32)pCmd[24])<<16) | (ADT_UInt32)pCmd[25];

   ClkDiv[0] = (ADT_UInt32)  (pCmd[38] & 0x00ff);
   ClkDiv[1] = (ADT_UInt32) ((pCmd[38] & 0x00ff) >> 8);
   ClkDiv[2] = (ADT_UInt32)  (pCmd[39] & 0x00ff);

   PulseWidth [0] =  (ADT_UInt32) ((pCmd[39] & 0xff00) >> 8);
   PulseWidth [1] =  (ADT_UInt32)  (pCmd[40] & 0x00ff);
   PulseWidth [2] =  (ADT_UInt32) ((pCmd[40] & 0xff00) >> 8);

   FrameWidth [0] =  (ADT_UInt32) ((pCmd[41] & 0xfff0) >> 4);
   FrameWidth [1] =  (ADT_UInt32) ((pCmd[42] & 0xfff0) >> 4);
   FrameWidth [2] =  (ADT_UInt32) ((pCmd[43] & 0xfff0) >> 4);

   genClockEnable[0] = (GpakActivation)(pCmd[41] & 0x0001);
   genClockEnable[1] = (GpakActivation)(pCmd[42] & 0x0001);
   genClockEnable[2] = (GpakActivation)(pCmd[43] & 0x0001);

   compMode[0] = (GpakCompandModes)((pCmd[41] & 0x000E)>>1);
   compMode[1] = (GpakCompandModes)((pCmd[42] & 0x000E)>>1);
   compMode[2] = (GpakCompandModes)((pCmd[43] & 0x000E)>>1);


   //--------------------------------------------------------------------------
   // Validate configuration parameters
   //--------------------------------------------------------------------------
   if      (!validCompanding(compMode[0])) return Pc_InvalidCompandMode0;
   else if (!validCompanding(compMode[1])) return Pc_InvalidCompandMode1;
   else if (!validCompanding(compMode[2])) return Pc_InvalidCompandMode2;

   txDataDelay[0] = (ADT_UInt32)(pCmd[44] & 0x0003);
   rxDataDelay[0] = (ADT_UInt32)((pCmd[44] >> 2) & 0x0003);
   txDataDelay[1] = (ADT_UInt32)((pCmd[44] >> 4) & 0x0003);
   rxDataDelay[1] = (ADT_UInt32)((pCmd[44] >> 6) & 0x0003);
   txDataDelay[2] = (ADT_UInt32)((pCmd[44] >> 8) & 0x0003);
   rxDataDelay[2] = (ADT_UInt32)((pCmd[44] >> 10) & 0x0003);

   if      ((2 < txDataDelay[0]) || (2 < rxDataDelay[0])) return Pc_InvalidDataDelay0;
   else if ((2 < txDataDelay[1]) || (2 < rxDataDelay[1])) return Pc_InvalidDataDelay1;
   else if ((2 < txDataDelay[2]) || (2 < rxDataDelay[2])) return Pc_InvalidDataDelay2;

   //--------------------------------------------------------------------------
   // For each enabled McBsp. Verify that
   //     The last selected slot by bit mask is within configured slots (SlotsPerFrame).
   //     The total selected slots is supported for DMA
   //--------------------------------------------------------------------------

   for (i = 0; i < NUM_TDM_PORTS; i++) {
      mcBSPSlotsNeeded[i] = 0;
      
      if (!McBspEnable[i]) continue;
      
      // Determine the highest numbered selected slot.
      LastSlot = determineLastSlot (SlotMask[i][3],96);
      if (!LastSlot) LastSlot = determineLastSlot (SlotMask[i][2],64);
      if (!LastSlot) LastSlot = determineLastSlot (SlotMask[i][1],32);
      if (!LastSlot) LastSlot = determineLastSlot (SlotMask[i][0],0);
     
      // Count the number of selected slots.
      for (k = 0; k < 4; k++) {
         Mask = SlotMask[i][k];
         for (j = 0; j < 32; j++) {
            if ((Mask & 1) != 0) mcBSPSlotsNeeded[i]++;
         
            Mask >>= 1;
         }
      }
      TxMcBSPSlotsNeeded[i] = mcBSPSlotsNeeded[i];
      if (MaxDmaSlots[i] < mcBSPSlotsNeeded[i])
         return (GPAK_PortConfigStat_t) (Pc_TooManySlots0 + i);        // Number active more than supported by build

      if (mcBSPSlotsNeeded[i] == 0)
         return (GPAK_PortConfigStat_t) (Pc_NoSlots0 + i);             // No slots active
   }

   sysConfig.rxDataDelay[0] = rxDataDelay[0];
   sysConfig.txDataDelay[0] = txDataDelay[0];
   sysConfig.rxDataDelay[1] = rxDataDelay[1];
   sysConfig.txDataDelay[1] = txDataDelay[1];
   sysConfig.rxDataDelay[2] = rxDataDelay[2];
   sysConfig.txDataDelay[2] = txDataDelay[2];

   sysConfig.compandingMode[0] = compMode[0];
   sysConfig.compandingMode[1] = compMode[1];
   sysConfig.compandingMode[2] = compMode[2];

   // Configure the McBSP registers.
   ConfigureMcBSP (McBspEnable, SlotMask);
   StartGpakPcm   (&mcBSPSlotsNeeded[0]);

   // Wait up to 10 ms for verification that the DMA has started.
   startTime = CLK_gethtime ();
   startDmaCnt = ApiBlock.DmaSwiCnt;

   if ((MatchDmaFlags == 0) && 
       (McBspEnable[0] == 0) && (McBspEnable[1] == 0) && (McBspEnable[2] == 0))
      return Pc_Success;

   do {
      if (2 <= (ApiBlock.DmaSwiCnt - startDmaCnt)) return Pc_Success;
   } while ((CLK_gethtime() - startTime) < (CLK_countspms() * 10));
   
   for (i=0; i<NUM_TDM_PORTS; i++) {
       SlotMap = pSlotMap[i];
       for (j=0; j<SltsPerFrame[i] ; j++)
            SlotMap[j] = UNCONFIGURED_SLOT;
       SlotMap = pTxSlotMap[i];
       for (j=0; j<TxSltsPerFrame[i] ; j++)
            SlotMap[j] = UNCONFIGURED_SLOT;
   }
   return Pc_NoInterrupts;
}



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ConfigureMcBSP - Configure G.PAK McBSP.
//
// FUNCTION
//   Configures PCM serial port I/O.  Called in response to host message
//
//  Inputs
//     McBspEnable - Array indicating which McBsp ports are to be configured.
//     McBspSlotMask - Array[port][word] of bit masks (four elements per port
//                     indicating which time slots are to be activated
//                     Each word represents 32 time slots. 
//
//
void ConfigureMcBSP (ADT_UInt32 McBspEnable[3], ADT_UInt32 McBspSlotMask[3][4]) { 

   int port, slot, idx;
   ADT_UInt32 Mask;                // mask value for testing
   ADT_UInt32 DmaSlots;
   ADT_UInt16 DmaIndx;             // DMA buffer index (also used for tx Pin)
   ADT_UInt16 *SlotMap;
   
   PowerOnMcBSPs ();
   //--------------------------------------------------------------------------
   // Map each active rx time slot to its DMA slot and establish register values
   //--------------------------------------------------------------------------
   for (port = 0; port < NUM_TDM_PORTS; port++) {
      McBspInfo *Bsp = &McBsp [port];


      if (McBspEnable[port]) {   
          storeMcBSPConfiguration (port);
      } else {
          Bsp->Enabled = Disabled;
          SlotMap = pSlotMap[port];
          for (slot = 0; slot < SltsPerFrame[port]; slot++) 
              SlotMap[slot] = UNCONFIGURED_SLOT;
          SlotMap = pTxSlotMap[port];
          for (slot = 0; slot < TxSltsPerFrame[port]; slot++) 
              SlotMap[slot] = UNCONFIGURED_SLOT;
          continue;
      }

      Bsp->Enabled = Enabled;

      tdmPortFuncs = &McBSPPort;

      // Configure for DMA channels
      slot = 0;
      DmaIndx = 0;

      // Determine DMA index of active slots
      for (idx = 0; idx < 4; idx++) {
         Mask = 1;
         DmaSlots = McBspSlotMask[port][idx];
         SlotMap = pSlotMap[port];
         while (Mask != 0 && (DmaIndx < SltsPerFrame[port])) {
            if ((Mask & DmaSlots) != 0) SlotMap[slot++] = DmaIndx++;
            else                        SlotMap[slot++] = UNCONFIGURED_SLOT;
            Mask = Mask << 1;
         }        
         Bsp->McRCER [idx] = DmaSlots;
      }

      slot = 0;
      DmaIndx = 0;
      // Determine DMA index of active slots
      for (idx = 0; idx < 4; idx++) {
         Mask = 1;
         DmaSlots = McBspSlotMask[port][idx];
         SlotMap = pTxSlotMap[port];
         while (Mask != 0 && (DmaIndx < TxSltsPerFrame[port])) {
            if ((Mask & DmaSlots) != 0) SlotMap[slot++] = DmaIndx++;
            else                        SlotMap[slot++] = UNCONFIGURED_SLOT;
            Mask = Mask << 1;
         }        
         Bsp->McXCER [idx] = 0;
      }
#if 1 // jdc try
      // 1 word per pin for DMA event and DMA transfer
      Bsp->TxFifoCtrl = (1 << 8) | 1; //(TxMcBSPSlotsNeeded[port] << 8) | TxMcBSPSlotsNeeded[port];
      Bsp->RxFifoCtrl = (1 << 8) | 1; //(mcBSPSlotsNeeded[port] << 8) | mcBSPSlotsNeeded[port];
#endif	  

   SltsPerFrame[port] = mcBSPSlotsNeeded[port];
   DmaSlotCnt  [port] = mcBSPSlotsNeeded[port];
   TxSltsPerFrame[port] = mcBSPSlotsNeeded[port];
   TxDmaSlotCnt  [port] = mcBSPSlotsNeeded[port];
   }
   return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ConfigureMcBSPOnePort - Configure G.PAK McBSP.
//
// FUNCTION
//   Configures PCM serial port I/O.  Called in response to host message
//
//  Inputs
//     McBspEnable - Array indicating which McBsp ports are to be configured.
//     McBspSlotMask - Array[port][word] of bit masks (four elements per port
//                     indicating which time slots are to be activated
//                     Each word represents 32 time slots. 
//
//
void ConfigureMcBSPOnePort (ADT_UInt32 port, ADT_Bool McBspEnable, ADT_UInt32 McBspSlotMask[4], ADT_UInt32 TxMcBspSlotMask[4]) { 

   int slot, idx;
   ADT_UInt32 Mask;                // mask value for testing
   ADT_UInt32 DmaSlots;
   ADT_UInt16 DmaIndx;             // DMA buffer index (also used for tx Pin)
   ADT_UInt16 *SlotMap;
   
   PowerOnMcBSPs ();
   //--------------------------------------------------------------------------
   // Map each active rx time slot to its DMA slot and establish register values
   //--------------------------------------------------------------------------
   //for (port = 0; port < NUM_TDM_PORTS; port++) 
   {
      McBspInfo *Bsp = &McBsp [port];


      if (McBspEnable) {   
          //tdmPortFuncs[port] = &McBSPPort;
          storeMcBSPConfiguration (port);
      } else {
          Bsp->Enabled = Disabled;
          SlotMap = pSlotMap[port];
          for (slot = 0; slot < SltsPerFrame[port]; slot++) 
              SlotMap[slot] = UNCONFIGURED_SLOT;
          SlotMap = pTxSlotMap[port];
          for (slot = 0; slot < TxSltsPerFrame[port]; slot++) 
              SlotMap[slot] = UNCONFIGURED_SLOT;
          return;
      }

      Bsp->Enabled = Enabled;

      tdmPortFuncs = &McBSPPort;

      // Configure for DMA channels
      slot = 0;
      DmaIndx = 0;
      // Determine DMA index of active slots
      for (idx = 0; idx < 4; idx++) {
         Mask = 1;
         DmaSlots = McBspSlotMask[idx];
         SlotMap = pSlotMap[port];
         while (Mask != 0 && (DmaIndx < SltsPerFrame[port])) {
            if ((Mask & DmaSlots) != 0) SlotMap[slot++] = DmaIndx++;
            else                        SlotMap[slot++] = UNCONFIGURED_SLOT;
            Mask = Mask << 1;
         }        
         Bsp->McRCER [idx] = DmaSlots;
      }

      slot = 0;
      DmaIndx = 0;
      // Determine DMA index of active slots
      for (idx = 0; idx < 4; idx++) {
         Mask = 1;
         DmaSlots = TxMcBspSlotMask[idx];
         SlotMap = pTxSlotMap[port];
         while (Mask != 0 && (DmaIndx < TxSltsPerFrame[port])) {
            if ((Mask & DmaSlots) != 0) SlotMap[slot++] = DmaIndx++;
            else                        SlotMap[slot++] = UNCONFIGURED_SLOT;
            Mask = Mask << 1;
         }        
         Bsp->McXCER [idx] = 0;
      }


      // 1 word per pin for DMA event and DMA transfer
      Bsp->TxFifoCtrl = (1 << 8) | 1; //(TxMcBSPSlotsNeeded[port] << 8) | TxMcBSPSlotsNeeded[port];
      Bsp->RxFifoCtrl = (1 << 8) | 1; //(mcBSPSlotsNeeded[port] << 8) | mcBSPSlotsNeeded[port];
   }

   SltsPerFrame[port] = mcBSPSlotsNeeded[port];
   DmaSlotCnt  [port] = mcBSPSlotsNeeded[port];
   TxSltsPerFrame[port] = TxMcBSPSlotsNeeded[port];
   TxDmaSlotCnt  [port] = TxMcBSPSlotsNeeded[port];

   return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// GetMasks
//
// FUNCTION
//    Get bit mask corresponding to port, pin, slot and 
//
//
// PARAMETERS
//    McBspID - 
// RETURNS
//  highest slot number if any bits in Mask are set. Otherwise returns 0.
//
void GetMasks (ADT_Word McBspId, int Slot, int SlotCnt, ADT_Word *TxMask) {

   ADT_Word MaskBit; 
   int group;
   ADT_UInt16 *SlotMap;
   
   memset (TxMask, 0, MULTI_REG * sizeof (ADT_Word));
   if (NUM_TDM_PORTS <= McBspId) return;

   SlotMap = pTxSlotMap[McBspId];
   MaskBit = 1 << (Slot % 32);
   group = (Slot / 32);
   
   // Add additional slots (circuit data) to slot masks
   while (0 < SlotCnt)  {
      if (SlotMap[group] != UNCONFIGURED_SLOT) {
         TxMask[group] |= MaskBit;
         SlotCnt--;
      }
      if (MaskBit != 0x8000000) {
         MaskBit <<= 1;
      } else {
         MaskBit = 1;
         group++;
      }
   }
}





// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// determineLastSlot - determine the highest slot number
//
// FUNCTION
// Determines the highest configured slot number based upon
// the 32-bit slot mask and offset
//
// RETURNS
//  highest slot number if any bits in Mask are set. Otherwise returns 0.
//
ADT_UInt16 determineLastSlot (ADT_UInt32 Mask, ADT_UInt16 offset) {

   ADT_UInt16 j;

   for (j = 31; j != 0; j--)    {
      if ((Mask & 0x80000000) != 0)       {
         return (j + offset);
      }
      Mask = Mask << 1;
   }
   return(0);
}

// ===========================================================================
extern GpakActivation tdmLoopback[NUM_TDM_PORTS];
extern ADT_UInt16 RxEvents [];             // Rx DMA Flags (configured by DMA)
extern ADT_UInt16 TxEvents [];             // Tx DMA Flags (configured by DMA)
extern ADT_UInt16 lbReady[NUM_TDM_PORTS];
ADT_UInt32 prevTxMask[NUM_TDM_PORTS][4];
void StopSerialPortIo (void);
void StartSerialPortIo (void);

int updateTdmLoopback(GpakSerialPort_t port, GpakActivation state) {
   McBspInfo *Bsp;


  
    if (port == SerialPortNull)
        return 0;

    Bsp = &McBsp[port];
    if (Bsp->Enabled == Disabled)
//    if ((TxEvents [port] == 0) || (RxEvents [port] == 0))
        return 0;


    SWI_disable();
    if ((state == Enabled) && (tdmLoopback[port] == Disabled)) {
		// Activate the loopback
        // Save off current state of txMask registers
        prevTxMask[port][0] = Bsp->McXCER[0];
        prevTxMask[port][1] = Bsp->McXCER[1];
        prevTxMask[port][2] = Bsp->McXCER[2];
        prevTxMask[port][3] = Bsp->McXCER[3];

        // Turn on all transmit enables
        Bsp->McXCER[0]  = Bsp->McRCER[0];
        Bsp->McXCER[1]  = Bsp->McRCER[1];
        Bsp->McXCER[2]  = Bsp->McRCER[2];
        Bsp->McXCER[3]  = Bsp->McRCER[3];

        // restart serial port activity
        StopSerialPortIo();

        lbReady[port] = 0;
        tdmLoopback[port] = Enabled;                

        StartSerialPortIo();


    } else if ((state == Disabled) && (tdmLoopback[port] == Enabled)) {
		// Deactivate the loopback

        // Restore transmit enables
        Bsp->McXCER[0]  = prevTxMask[port][0];
        Bsp->McXCER[1]  = prevTxMask[port][1];
        Bsp->McXCER[2]  = prevTxMask[port][2];
        Bsp->McXCER[3]  = prevTxMask[port][3];

        // restart serial port activity
        StopSerialPortIo();

        lbReady[port] = 0;
        tdmLoopback[port] = Disabled;                

        StartSerialPortIo();
    }

    SWI_enable();
    return 1;    
}

typedef struct TDMModeCfg_t {
    ADT_UInt16 sampleRatekHz;     // Samples per kHz on TDM bus
} TDMModeCfg_t;
#if 0
const TDMModeCfg_t TDMCfg = {
   16,          // Samples per kHz on TDM bus 
};
#pragma DATA_SECTION (TDMCfg, "TDMCFG")
#endif
extern const TDMModeCfg_t TDMCfg;
#if 0 //ss
TDMModeCfg_t* const tdmCfg = (TDMModeCfg_t *) &TDMCfg;  // Allow access to modifiable TSIP structure
#endif
extern TDMModeCfg_t* const tdmCfg;  // Allow access to modifiable TSIP structure
#if 0 //ss
#pragma CODE_SECTION (InitTDMMode, "SLOW_PROG_SECT")
void InitTDMMode () { 
   if (tdmCfg->sampleRatekHz == 8) {
      sysConfig.samplesPerMs = 8;
      TDMRate = 8000;
   } else if ((tdmCfg->sampleRatekHz == 16)  && (SysAlgs.wbEnable)) {
      sysConfig.samplesPerMs = 16;
      TDMRate = 16000;
   }
}
#endif
extern void InitTDMMode ();
