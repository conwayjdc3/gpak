
#include <stdio.h>
#include "adt_typedef.h"
#undef  DSP_TYPE
#define DSP_TYPE 647
#include "GpakHpi.h"
#include "GpakApi.h"
#include "GpakEnum.h"


#if defined (__BIOS__) || defined (__TMS470__)
   #include <string.h>
#endif

#include "GpakCustWin32.h"

unsigned int MaxDsps          = MAX_DSP_CORES;
unsigned int MaxChannelAlloc  = MAX_CHANNELS;
unsigned int MaxWaitLoops     = MAX_WAIT_LOOPS;

/* DSP Related arrays */
DSP_Address  ifBlkAddress = DSP_IFBLK_ADDRESS;                // Interface block address

GPAK_DspCommStat_t DSPError[MAX_DSP_CORES];

DSP_Word    MaxChannels[MAX_DSP_CORES];  /* max num channels */
DSP_Word    MaxCmdMsgLen[MAX_DSP_CORES];  /* max Cmd msg length (octets) */

DSP_Address pDspIfBlk[MAX_DSP_CORES];          /* DSP address of interface blocks */


#define HighWord(a)            ((ADT_UInt16) ((a >> 16) & 0xffff))
#define LowWord(a)             ((ADT_UInt16) ( a        & 0xffff))
#define PackWords(hi,lo)       ((ADT_UInt32) ((((ADT_UInt32) (hi)) << 16) | (((ADT_UInt32) (lo)) & 0xffff)))
#define Pack(Hi,Lo)            ((ADT_UInt16) ((Hi)<<8) | ((Lo) & 0x00ff))
#define PackWordsSigned(hi,lo) ((ADT_Int32) ((((ADT_UInt32) (hi)) << 16) | (((ADT_UInt32) (lo)) & 0xffff)))


// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
// Macro to reconstruct a 32-bit value from two 16-bit values.
// Parameter p32: 32-bit-wide destination
// Parameter p16: 16-bit-wide source array of length 2 words

#define CORE_ID(chan) ((chan >> 8) & 0xFF)
#define CHAN_ID(chan) (chan & 0xFF)
#define CNFR_ID(cnfr) (cnfr & 0xFF)


#define NO_ID_CHK 0
#define BYTE_ID 1
#define WORD_ID 2

extern GPAK_PortConfigStat_t ProcConfigSerialPortsMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply);

// ----------------------   TDM ports

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakConfigurePorts - Configure a DSP's TDM ports.
 *
 * FUNCTION
 *  Configures a DSP's serial ports.
 *
 * Inputs
 *   DspId    - Dsp identifier 
 *   PortCfg  - Port configuration data structure pointer
 *
 * Outputs
 *    pStatus  - port configuration status from DSP
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
ADT_UInt32 gpakFormatPortConfig (ADT_UInt16 *Msg, GpakPortConfig_t *PortCfg) {
   int i, idx;

    /* Build the Configure Serial Ports message. */
    Msg[0] = MSG_CONFIGURE_PORTS << 8;

    // Enable bits
    Msg[1] = 0;
    if (PortCfg->TsipEnable1)        Msg[1]  = 0x0001;
    if (PortCfg->TsipEnable2)        Msg[1] |= 0x0002;
    if (PortCfg->TsipEnable3)        Msg[1] |= 0x0004;

    //--------------------------------------------------------------------
    // TSIP 0
    // Slot masks
    for (i=0, idx=2; i<32; i++, idx+=2) {
        Msg[idx]    = (ADT_UInt16)((PortCfg->slotMask1[i]) >> 16);  
        Msg[idx+1]  = (ADT_UInt16)((PortCfg->slotMask1[i]) & 0xffff);  
    }

    // Control parameters
    Msg[66] = (ADT_UInt16) ( (PortCfg->singleClk1             & 0x0001) |
                            ((PortCfg->txFsPolarity1   << 1)  & 0x0002) |
                            ((PortCfg->txDataClkEdge1  << 2)  & 0x0004) |
                            ((PortCfg->txFsClkEdge1    << 3)  & 0x0008) |
                            ((PortCfg->txClkMode1      << 4)  & 0x0010) |
                            ((PortCfg->txClkFsSrc1     << 5)  & 0x0020) |
                            ((PortCfg->txOutputDly1    << 6)  & 0x0040) |
                            ((PortCfg->rxFsPolarity1   << 7)  & 0x0080) |
                            ((PortCfg->rxDataClkEdge1  << 8)  & 0x0100) |
                            ((PortCfg->rxFsClkEdge1    << 9)  & 0x0200) |
                            ((PortCfg->rxClkMode1      << 10) & 0x0400) |
                            ((PortCfg->rxClkFsSrc1     << 11) & 0x0800));

    Msg[67] = (ADT_UInt16) ( (PortCfg->txDriveState1      & 0x0003) |
                            ((PortCfg->txDataRate1  << 2) & 0x000C) |
                            ((PortCfg->rxDataRate1  << 4) & 0x0030) |
                            ((PortCfg->loopBack1    << 6) & 0x00C0));

    Msg[68] = (ADT_UInt16) PortCfg->txDatDly1;
    Msg[69] = (ADT_UInt16) PortCfg->rxDatDly1;

    //--------------------------------------------------------------------
    // TSIP 1
    // Slot masks
    for (i=0, idx=70; i<32; i++, idx+=2) {
        Msg[idx]    = (ADT_UInt16)((PortCfg->slotMask2[i]) >> 16);  
        Msg[idx+1]  = (ADT_UInt16)((PortCfg->slotMask2[i]) & 0xffff);  
    }

    // Control parameters
    Msg[134] = (ADT_UInt16) ( (PortCfg->singleClk2            & 0x0001) |
                            ((PortCfg->txFsPolarity2   << 1)  & 0x0002) |
                            ((PortCfg->txDataClkEdge2  << 2)  & 0x0004) |
                            ((PortCfg->txFsClkEdge2    << 3)  & 0x0008) |
                            ((PortCfg->txClkMode2      << 4)  & 0x0010) |
                            ((PortCfg->txClkFsSrc2     << 5)  & 0x0020) |
                            ((PortCfg->txOutputDly2    << 6)  & 0x0040) |
                            ((PortCfg->rxFsPolarity2   << 7)  & 0x0080) |
                            ((PortCfg->rxDataClkEdge2  << 8)  & 0x0100) |
                            ((PortCfg->rxFsClkEdge2    << 9)  & 0x0200) |
                            ((PortCfg->rxClkMode2      << 10) & 0x0400) |
                            ((PortCfg->rxClkFsSrc2     << 11) & 0x0800));

    Msg[135] = (ADT_UInt16) ( (PortCfg->txDriveState2     & 0x0003) |
                            ((PortCfg->txDataRate2  << 2) & 0x000C) |
                            ((PortCfg->rxDataRate2  << 4) & 0x0030) |
                            ((PortCfg->loopBack2    << 6) & 0x00C0));

    Msg[136] = (ADT_UInt16) PortCfg->txDatDly2;
    Msg[137] = (ADT_UInt16) PortCfg->rxDatDly2;

    //--------------------------------------------------------------------
    // TSIP 2
    // Slot masks
    for (i=0, idx=138; i<32; i++, idx+=2) {
        Msg[idx]    = (ADT_UInt16)((PortCfg->slotMask3[i]) >> 16);  
        Msg[idx+1]  = (ADT_UInt16)((PortCfg->slotMask3[i]) & 0xffff);  
    }

    // Control parameters
    Msg[202] = (ADT_UInt16) ( (PortCfg->singleClk3            & 0x0001) |
                            ((PortCfg->txFsPolarity3   << 1)  & 0x0002) |
                            ((PortCfg->txDataClkEdge3  << 2)  & 0x0004) |
                            ((PortCfg->txFsClkEdge3    << 3)  & 0x0008) |
                            ((PortCfg->txClkMode3      << 4)  & 0x0010) |
                            ((PortCfg->txClkFsSrc3     << 5)  & 0x0020) |
                            ((PortCfg->txOutputDly3    << 6)  & 0x0040) |
                            ((PortCfg->rxFsPolarity3   << 7)  & 0x0080) |
                            ((PortCfg->rxDataClkEdge3  << 8)  & 0x0100) |
                            ((PortCfg->rxFsClkEdge3    << 9)  & 0x0200) |
                            ((PortCfg->rxClkMode3      << 10) & 0x0400) |
                            ((PortCfg->rxClkFsSrc3     << 11) & 0x0800));

    Msg[203] = (ADT_UInt16) ( (PortCfg->txDriveState3     & 0x0003) |
                            ((PortCfg->txDataRate3  << 2) & 0x000C) |
                            ((PortCfg->rxDataRate3  << 4) & 0x0030) |
                            ((PortCfg->loopBack3    << 6) & 0x00C0));

    Msg[204] = (ADT_UInt16) PortCfg->txDatDly3;
    Msg[205] = (ADT_UInt16) PortCfg->rxDatDly3;

    //--------------------------------------------------------------------
    // TSIP 0
    // Companding modes
    for (i=0, idx=206; i<4; i++, idx+=2) {
        Msg[idx]    = (ADT_UInt16)((PortCfg->slotCompand1[i]) >> 16);  
        Msg[idx+1]  = (ADT_UInt16)((PortCfg->slotCompand1[i]) & 0xffff);  
    }

    //--------------------------------------------------------------------
    // TSIP 1
    // Companding modes
    for (i=0, idx=214; i<4; i++, idx+=2) {
        Msg[idx]    = (ADT_UInt16)((PortCfg->slotCompand2[i]) >> 16);  
        Msg[idx+1]  = (ADT_UInt16)((PortCfg->slotCompand2[i]) & 0xffff);  
    }

    //--------------------------------------------------------------------
    // TSIP 2
    // Companding modes
    for (i=0, idx=222; i<4; i++, idx+=2) {
        Msg[idx]    = (ADT_UInt16)((PortCfg->slotCompand3[i]) >> 16);  
        Msg[idx+1]  = (ADT_UInt16)((PortCfg->slotCompand3[i]) & 0xffff);  
    }

    return 230;
}


GpakApiStatus_t gpakConfigurePorts (ADT_UInt32 DspId, GpakPortConfig_t *pPortConfig, 
                                                     GPAK_PortConfigStat_t *pStatus) {

    ADT_UInt16 Msg[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 Rply[MSG_BUFFER_SIZE];    /* message buffer */

    gpakFormatPortConfig (Msg, pPortConfig);
    *pStatus = ProcConfigSerialPortsMsg (Msg, Rply);
    return GpakApiSuccess;
}


#define MAX_SLOTS 1024
void PopulateSlotMask (ADT_UInt32 *slotMask, int slotCnt, int slotsPerSample) {
   ADT_UInt32 mask;
   int  slotsPerMask, bitsPerSlot;

   // Space out slots across the TDM bus.
   if (slotsPerSample == 8) {
      slotsPerMask = 4;
      mask = 0x01010101;
   } else if (slotsPerSample == 4) {
      slotsPerMask = 8;
      mask = 0x11111111;
   } else if (slotsPerSample == 2) {
      slotsPerMask = 16;
      mask = 0x55555555;
   } else {
      slotsPerMask = 32;
      mask = 0xffffffff;
      slotsPerSample = 1;
   }
   bitsPerSlot = 32 / slotsPerMask;

   // Populate slot mask array
   while (slotsPerMask <= slotCnt) {
      *slotMask++ = mask;
      slotCnt -= slotsPerMask;
   }
   *slotMask = (1 << (slotCnt * bitsPerSlot)) - 1;
   *slotMask &= mask;
}
void PopulateCompandingMask (ADT_UInt32 *slotMask, int slotCnt, int slotsPerSample, int compand_a) {
   ADT_UInt32 mask;
   int slotsPerMask;

   if (compand_a) mask = (ADT_UInt32) -1;
   else           mask =  0;
   slotsPerMask = 32 * 8;

   slotCnt *= slotsPerSample;

   // Populate slot mask array
   while (slotsPerMask <= slotCnt) {
      *slotMask++ = mask;
      slotCnt -= slotsPerMask;
   }
   *slotMask = (1 << (slotCnt / 8)) - 1;
   *slotMask &= mask;
}
void dspTDMInit (ADT_UInt32 DspId, int portBits, int slotCnt, int slotsPerSample) {
   // TSIP configuration variables
   GpakPortConfig_t       PortConfig;
   gpakConfigPortStatus_t PrtCfgStat = (gpakConfigPortStatus_t) 0;
   GPAK_PortConfigStat_t  PrtDspStat = (GPAK_PortConfigStat_t) 0;


   memset (&PortConfig, 0, sizeof (PortConfig));

   PortConfig.TsipEnable1 = Disabled;
   PortConfig.TsipEnable2 = Disabled;
   PortConfig.TsipEnable3 = Disabled;
   if (portBits & 1) PortConfig.TsipEnable1 = Enabled;
   if (portBits & 2) PortConfig.TsipEnable2 = Enabled;
   if (portBits & 4) PortConfig.TsipEnable3 = Enabled;

   PopulateSlotMask (PortConfig.slotMask1, slotCnt, slotsPerSample);
   PortConfig.singleClk1       = Enabled;
   PortConfig.txFsPolarity1    = fsActLow;
   PortConfig.txDataClkEdge1   = clkRising;
   PortConfig.txFsClkEdge1     = clkRising;
   PortConfig.txClkMode1       = clkSingleRate;
   PortConfig.txDataRate1      = rate_8Mbps;
   PortConfig.txClkFsSrc1      = clkFsA;
   PortConfig.txDatDly1        = 1023;
   PortConfig.txDriveState1    = highz;
   PortConfig.txOutputDly1     = Disabled;
   PortConfig.rxFsPolarity1    = fsActLow;
   PortConfig.rxDataClkEdge1   = clkFalling;
   PortConfig.rxFsClkEdge1     = clkRising;
   PortConfig.rxClkMode1       = clkSingleRate;
   PortConfig.rxDataRate1      = rate_8Mbps;
   PortConfig.rxClkFsSrc1      = clkFsA;
   PortConfig.rxDatDly1        = 1023;
   PortConfig.loopBack1        = internalLoopback;
   PopulateCompandingMask (PortConfig.slotCompand1, slotCnt, slotsPerSample, 0);
   
   PopulateSlotMask (PortConfig.slotMask2, slotCnt, slotsPerSample);
   PortConfig.singleClk2       = Enabled;
   PortConfig.txFsPolarity2    = fsActLow;
   PortConfig.txDataClkEdge2   = clkRising;
   PortConfig.txFsClkEdge2     = clkRising;
   PortConfig.txClkMode2       = clkSingleRate;
   PortConfig.txDataRate2      = rate_8Mbps;
   PortConfig.txClkFsSrc2      = clkFsA;
   PortConfig.txDatDly2        = 1023;
   PortConfig.txDriveState2    = highz;
   PortConfig.txOutputDly2     = Disabled;
   PortConfig.rxFsPolarity2    = fsActLow;
   PortConfig.rxDataClkEdge2   = clkFalling;
   PortConfig.rxFsClkEdge2     = clkRising;
   PortConfig.rxClkMode2       = clkSingleRate;
   PortConfig.rxDataRate2      = rate_8Mbps;
   PortConfig.rxClkFsSrc2      = clkFsA;
   PortConfig.rxDatDly2        = 1023;
   PortConfig.loopBack2        = internalLoopback;
   PopulateCompandingMask (PortConfig.slotCompand2, slotCnt, slotsPerSample, 0);
   
   PopulateSlotMask (PortConfig.slotMask3, slotCnt, slotsPerSample);
   PortConfig.singleClk3       = Enabled;
   PortConfig.txFsPolarity3    = fsActLow;
   PortConfig.txDataClkEdge3   = clkRising;
   PortConfig.txFsClkEdge3     = clkRising;
   PortConfig.txClkMode3       = clkSingleRate;
   PortConfig.txDataRate3      = rate_8Mbps;
   PortConfig.txClkFsSrc3      = clkFsA;
   PortConfig.txDatDly3        = 1023;
   PortConfig.txDriveState3    = highz;
   PortConfig.txOutputDly3     = Disabled;
   PortConfig.rxFsPolarity3    = fsActLow;
   PortConfig.rxDataClkEdge3   = clkFalling;
   PortConfig.rxFsClkEdge3     = clkRising;
   PortConfig.rxClkMode3       = clkSingleRate;
   PortConfig.rxDataRate3      = rate_8Mbps;
   PortConfig.rxClkFsSrc3      = clkFsA;
   PortConfig.rxDatDly3        = 1023;
   PortConfig.loopBack3        = internalLoopback;
   PopulateCompandingMask (PortConfig.slotCompand3, slotCnt, slotsPerSample, 0);

   PrtCfgStat = gpakConfigurePorts (DspId, &PortConfig, &PrtDspStat);
   if (PrtCfgStat != GpakApiSuccess || PrtDspStat != Pc_Success) {
      printf ("\n TSIP setup failure %d:%d\n", PrtCfgStat, PrtDspStat);
   } else {
      printf (" \n TSIP configuration success\n");
   }
}

