#include <stdio.h>
#include <std.h>
#include <swi.h>
#include <gbl.h>
#include <adt_typedef.h>
#include <common/include/mips.h>
#include "GpakDefs.h"
#include "GpakExts.h"
#include "GpakPcm.h"
#include <cslr_pllc.h>

#define Port0 0
#define Port1 1
#define Port2 2

#define BITS8 1
#define BITS16 2

#define PING 0
#define PONG 1


#define MAX_GPAK_SLOTS 512
#define SLOT_I8          4
#define FRAMES_PER_MS    8
#define BUFFER_CNT       2  // Ping + Pong
#define CID_I8           8  // TSIP configuration ID

#define FRAME_I8     ((MAX_GPAK_SLOTS * SLOT_I8) + CID_I8)
#define TSIPBuffI16 ( (FRAME_I8 * FRAMES_PER_MS * BUFFER_CNT) / sizeof (ADT_UInt16) )
#define DMABuffI16  ((MAX_GPAK_SLOTS * 16) * 2)
#define DRAINBuffI16 1024
#define LclBuffI16     48

extern void logDMA (char* type, int port, ADT_UInt64 event);
extern void dspTDMInit (ADT_UInt32 DspId, int portBits, int slotCnt, int slotsPerSample);
extern volatile ADT_UInt64 DmaActive;
extern volatile ADT_UInt64 DmaBuffActive;
extern ADT_UInt16 RxEvents [];
extern ADT_UInt16 TxEvents [];
extern void* getTxBuff (int port);
extern void* getRxBuff (int port);

extern void GpakDmaSwi (void);
extern int tdmCore;
__cregister volatile unsigned int DNUM;

int DSPCore;
int DSPTotalCores = 2;

int IdX = 0;
int intCnt;
int skipErrors = ADT_FALSE;

ADT_UInt16 SlotMap0[1024];
#pragma DATA_SECTION (SlotMap0, "FAST_DATA_SECT:TDM")
ADT_UInt16 SlotMap1[1024];
#pragma DATA_SECTION (SlotMap1, "FAST_DATA_SECT:TDM")
ADT_UInt16 SlotMap2[1024];
#pragma DATA_SECTION (SlotMap2, "FAST_DATA_SECT:TDM")
ADT_UInt16* pSlotMap[3] = {SlotMap0, SlotMap1, SlotMap2};


GpakIfBlock_t ApiBlock;
sysConfig_t sysConfig;
volatile ADT_UInt16 NumActiveChannels = 0;

ADT_UInt32 MultiCoreCpuUsage[6], MultiCoreCpuPeakUsage[6];

int TDMRate = 8000;
ADT_UInt32 SlotCompandMode[3][4];
int UseSlotCmpMask[3];

typedef struct dmaLog {
   char* type;
   ADT_Int32 port;
   ADT_UInt32 event;
   ADT_UInt32 DmaActive;
   ADT_UInt32 DmaBuffActive;
   ADT_UInt32 time;
} dmaLog_t;
extern dmaLog_t dmaLog [257];
extern int dmaIdx;

struct VARS {
   ADT_UInt32* intrCtrl;
   ADT_UInt32* edmaCtrl;
   ADT_UInt32* edmaParams;

   ADT_UInt32* edmaGlbl;
   ADT_UInt32* shadow0;
   ADT_UInt32* shadow1;

   ADT_UInt32* tsip0;
   ADT_UInt32* tsip1;
   ADT_UInt32* tsip2;
} regs = {
  (ADT_UInt32*) INTR_CTL_BASE, (ADT_UInt32*) EDMA_CFG_BASE, (ADT_UInt32*) EDMA_PARAM_ADDR,
  (ADT_UInt32*) (CSL_EDMA3CC_0_REGS + 0x1000), (ADT_UInt32*) (CSL_EDMA3CC_0_REGS + 0x2000), (ADT_UInt32*) (CSL_EDMA3CC_0_REGS + 0x2200),
  (ADT_UInt32*) CSL_TSIP_0_REGS, (ADT_UInt32*) CSL_TSIP_1_REGS, (ADT_UInt32*) CSL_TSIP_2_REGS
};
 
typedef void (DMA_TRANSFER) (void* restrict tdmBuff, void* restrict circBuff);
extern DMA_TRANSFER* dmaToCirc;
extern DMA_TRANSFER* circToDma;

// TSIP Buffers
ADT_PCM16 BSP0DMA_RxBuffer[TSIPBuffI16], BSP0DMA_TxBuffer[TSIPBuffI16];
ADT_PCM16 BSP1DMA_RxBuffer[TSIPBuffI16], BSP1DMA_TxBuffer[TSIPBuffI16];
ADT_PCM16 BSP2DMA_RxBuffer[TSIPBuffI16], BSP2DMA_TxBuffer[TSIPBuffI16];
#pragma DATA_ALIGN (BSP0DMA_RxBuffer, 8)
#pragma DATA_ALIGN (BSP1DMA_RxBuffer, 8)
#pragma DATA_ALIGN (BSP2DMA_RxBuffer, 8)
#pragma DATA_ALIGN (BSP0DMA_TxBuffer, 8)
#pragma DATA_ALIGN (BSP1DMA_TxBuffer, 8)
#pragma DATA_ALIGN (BSP2DMA_TxBuffer, 8)

ADT_PCM16 RxTransBuff0[DMABuffI16], TxTransBuff0[DMABuffI16];
ADT_PCM16 RxTransBuff1[DMABuffI16], TxTransBuff1[DMABuffI16];
ADT_PCM16 RxTransBuff2[DMABuffI16], TxTransBuff2[DMABuffI16];
#pragma DATA_ALIGN (RxTransBuff0, 16)
#pragma DATA_ALIGN (RxTransBuff1, 16)
#pragma DATA_ALIGN (RxTransBuff2, 16)
#pragma DATA_ALIGN (TxTransBuff0, 16)
#pragma DATA_ALIGN (TxTransBuff1, 16)
#pragma DATA_ALIGN (TxTransBuff2, 16)

ADT_PCM16* const RxTransBuffers[] = { RxTransBuff0, RxTransBuff1, RxTransBuff2 };
ADT_PCM16* const TxTransBuffers[] = { TxTransBuff0, TxTransBuff1, TxTransBuff2 };
       
volatile ADT_UInt16 AccumDmaFlags;
volatile ADT_UInt16 MatchDmaFlags; 
int gblPkLoadingRst;

ADT_UInt16 TsipBuflenI8[3] = { sizeof (BSP0DMA_RxBuffer),   sizeof (BSP1DMA_RxBuffer),   sizeof (BSP2DMA_RxBuffer) };
ADT_UInt16 TsipFallocI8[3] = { sizeof (BSP0DMA_RxBuffer)/8, sizeof (BSP1DMA_RxBuffer)/8, sizeof (BSP2DMA_RxBuffer)/8 };

TDM_Port *tdmPortFuncs = NULL;    // TDM functions for starting and stopping the TDM device

typedef struct TSIPModeCfg_t {
    ADT_UInt16 tdmSampleSizeI8;   // Bytes per sample on tdm bus (and dma buffer)
    ADT_UInt16 sampleRatekHz;     // Samples per kHz on TDM bus
    ADT_UInt16 compandedData;     // Data on TDM bus is companded
    ADT_UInt16 dmaChannelSizeI8;  // Bytes per channel in dma buffer
    TSIPMode_t mode;
} TSIPModeCfg_t;

ADT_Word MaxFrameCnt = 48;

extern TSIPModeCfg_t *tsipCfg;  // Allow access to modifiable TSIP structure

extern const ADT_UInt8* RxTsipBuffers[];
extern const ADT_UInt8* TxTsipBuffers[];
extern ADT_UInt16 DmaPingBufferI8 [NUM_TDM_PORTS];
extern ADT_UInt16 TDMSampsPerMs;

ALLOC_MIPS_TIMER (mips)

ADT_UInt16 drainBuff[DRAINBuffI16];
CircBufInfo_t circDrain;
CircBufInfo_t* pDrain = &circDrain;

ADT_UInt16 sinkBuff[DRAINBuffI16];
CircBufInfo_t circSink;
CircBufInfo_t* pSink = &circSink;

ADT_UInt16 lclBuff[LclBuffI16];
CircBufInfo_t circLcl;

CircBufInfo_t* extCrcRxBuff[MAX_GPAK_SLOTS];
CircBufInfo_t* extCrcTxBuff[MAX_GPAK_SLOTS];
CircBufInfo_t* circBuffs[MAX_GPAK_SLOTS];

CircBufInfo_t** pCrcRxBuff[3] = { extCrcRxBuff, extCrcRxBuff, extCrcRxBuff};
CircBufInfo_t** pCrcTxBuff[3] = { extCrcTxBuff, extCrcTxBuff, extCrcTxBuff};

ADT_UInt16 DmaSlotCnt[3];
ADT_UInt16 MaxDmaSlots[3]  = {  MAX_GPAK_SLOTS,  MAX_GPAK_SLOTS,  MAX_GPAK_SLOTS };
ADT_UInt16 SltsPerFrame[3] = { 1024, 1024, 1024 };

SWI_Handle SWI_Dma = NULL;
static SWI_Attrs swi_attr = { (SWI_Fxn)  GpakDmaSwi, 0, 0, 0, 0};

ADT_UInt32 nextCID;

ADT_UInt16 circBuff[16];
#pragma DATA_ALIGN (circBuff, 8)
int portFilter = -1;

void evtToChr (ADT_UInt64 evt) {
   char buff[28];
   memset (buff, ' ', 27);
   buff[27] = 0;
   
   // Rx Buffers
   if (evt & 0x00000002) buff[0]  = 'R';
   if (evt & 0x00010000) buff[1]  = 'A';
   if (evt & 0x00020000) buff[2]  = 'a';

   if (evt & 0x00000020) buff[4]  = 'R';
   if (evt & 0x00100000) buff[5]  = 'B';
   if (evt & 0x00200000) buff[6]  = 'b';

   if (evt & 0x00000200) buff[8]  = 'R';
   if (evt & 0x01000000) buff[9]  = 'C';
   if (evt & 0x02000000) buff[10] = 'c';

   // Tx Buffers
   if (evt & 0x00000008) buff[12] = 'T';
   if (evt & 0x00040000) buff[13] = 'A';
   if (evt & 0x00080000) buff[14] = 'a';

   if (evt & 0x00000080) buff[16] = 'T';
   if (evt & 0x00400000) buff[17] = 'B';
   if (evt & 0x00800000) buff[18] = 'b';

   if (evt & 0x00000800) buff[20] = 'T';
   if (evt & 0x04000000) buff[21] = 'C';
   if (evt & 0x08000000) buff[21] = 'c';
   
   printf ("%4x ", evt);
   return;
}
void dumpDMAlog () {
   struct dmaLog *log;
   float delta;
   int i;

   printf ("\nDMA log\n");

   log = &dmaLog[0];
   for (i=0; i<dmaIdx; i++, log++) {
      delta = log->time;
      delta /= CLK_countspms();
      if ((portFilter != -1) && (0 <= log->port) && (log->port != portFilter)) continue;
      evtToChr (log->event);
      evtToChr (log->DmaBuffActive);
      evtToChr (log->DmaActive);
      printf ("%8x %6.3f  %s.%x\n", log->event, delta, log->type, log->port);
   }
   dmaIdx = 0;
}

// STUB OUT G.PAK FUNCTIONS
int customTDMOut (ADT_PCM16 *dataOut, int port, int slotCnt) { return 0; }
int customTDMIn  (ADT_PCM16 *dataIn,  int port, int slotCnt) { return 0; }

void StartSerialPortIo (void) {

   int port, mask;
   ADT_UInt32 flags = 0;

   // Protect critical code from interruption.
   mask = HWI_disable();

   for (port = 0; port < NUM_TDM_PORTS; port++)  {
      if (tdmPortFuncs->SetupIO (port) == FALSE) continue;  // Write to Port configuration registers (port disabled)
      flags |= enableDMA  (port);  // Configure DMA registers and enable transfer events
      if (tdmPortFuncs->StartIO (port) == FALSE) continue;
      MatchDmaFlags = flags;
   }
   
   // Unprotect critical code.
   HWI_restore(mask);
   return;
}
void StopSerialPortIo (void) {

   int port, mask;

   // Protect critical code from interruption.
   mask = HWI_disable();

   MatchDmaFlags = 0;

   for (port = 0; port < NUM_TDM_PORTS; port++)  {
      if (tdmPortFuncs->StopIO (port) == FALSE) continue;
      disableDMA (port);
   }

   // Unprotect critical code.
   HWI_restore(mask);

   return;
}
void StartGpakPcm (ADT_UInt16 *DmaChanCnt) { 
   int port, chan, chanCnt;

   StopSerialPortIo();

   // Configure each serial port.
   for (port = 0; port < NUM_TDM_PORTS; port++) {
      chanCnt = *DmaChanCnt++;
      DmaSlotCnt[port] = chanCnt;
   }
   // Initialize the port's active slot buffer pointers to sink/drain.
   for (chan = 0; chan < MAX_GPAK_SLOTS; chan++) {
      extCrcRxBuff[chan] = pSink;
      extCrcTxBuff[chan] = pDrain;
   }
   DMAInitialize ();

   AccumDmaFlags = 0;

   StartSerialPortIo();
   return; 
}
void SendWarningEvent (ADT_UInt16 port, ADT_UInt16 event) { 
//   StopSerialPortIo ();
//   ApiBlock.DmaSwiCnt += 1000;
   logDMA ("Warning\n", port, event);
//   printf ("Warning %d on port: %d\n", event, port); 
}

int logTime (ADT_UInt32 event) { return 0; }

void GpakDmaSwi (void) {
   int events, TxEvent, RxEvent;
   int port, slotCnt;
   int frameErr;
   CircBufInfo_t **CrcRxBuff;   // act Rcv bufrs
   CircBufInfo_t **CrcTxBuff;   // act Tx bufrs
   ApiBlock.DmaSwiCnt++;

   events = SWI_getmbox ();

   if (MatchDmaFlags == 0) {
      printf ("DMA FLAGS not enabled\n");
      return;
   }
   logDMA ("SWI", events, (ADT_UInt64) events);

   // Copy PCM between channel circular buffers and DMA linear buffers
   for (port = 0; port < NUM_TDM_PORTS; port++)  {
      slotCnt = DmaSlotCnt[port];
      if (slotCnt == 0) continue;

      TxEvent = events & TxEvents [port];
      RxEvent = events & RxEvents [port];

      CrcRxBuff = pCrcRxBuff[port];
      CrcTxBuff = pCrcTxBuff[port];

      //=============================================================
      // Tdm Loopback is disabled... process Tx followed by Rx
      if (TxEvent) {
         if (TxEvent & AccumDmaFlags) SendWarningEvent (port, WarnTxDmaSlip);
         CopyDmaTxBuffers (port, slotCnt, &CrcTxBuff[0], 0);
      }
      if (RxEvent) {
         if (RxEvent & AccumDmaFlags) SendWarningEvent (port, WarnRxDmaSlip);
         CopyDmaRxBuffers (port, slotCnt, &CrcRxBuff[0], 0);
      }

      //=============================================================
      AccumDmaFlags |= (TxEvent | RxEvent);
   }

   if (AccumDmaFlags != MatchDmaFlags) return;

   // Determine if a framing error was detected on any active serial port.
   for (port = 0; port < NUM_TDM_PORTS; port++)  {
      if (DmaSlotCnt[port] == 0) continue;

      frameErr = McBSPFrameError (port);
      if (frameErr == 0) continue;
      
      SendWarningEvent (port, WarnFrameSyncError);

      StopSerialPortIo();
      StartSerialPortIo();
      break;
   }
   logDMA ("Schedule\n", intCnt++, 0);
   AccumDmaFlags = 0;
   return;
}

void AppError (char* file, int line, char* msg, int err) {
   if (!err) return;
   logDMA (file, 0, line);
   if (1000 < ApiBlock.DmaSwiCnt) return;

   printf ("Error %s at %s line %d\n", msg, file, line);
   ApiBlock.DmaSwiCnt += 1000;
}

ADT_Word getAvailable (CircBufInfo_t* circ) { return 1000; }
ADT_Word getFreeSpace (CircBufInfo_t* circ) { return 1000; }
int logTransfer (ADT_Int32 code, void* a, void* b, void* c, void* d, int e) { return 0; }

void core0init (void) { return; }
void postRtpHostSWI (void) { return; }

//  TESTS
void dumpMem (char* labl, void* buff_v, int lenI8, int crI8) {
   ADT_UInt8* buff = buff_v;
   int i;
   printf ("\n%s\n", labl);
   for (i=0; i<lenI8; i++) {
      printf ("%4x ", *buff++);
      if (i%crI8 == (crI8-1)) printf ("\n");
   }
   printf ("\n");
}

void fillDrainBuff () {
   int i;
   for (i=0; i<DRAINBuffI16; i++) drainBuff[i] = i;
}
void fillTsipBuff (ADT_UInt32 tsipBuff, int sampHz, int sampI8, int chnCnt, int port, int offset) {
   int i, j, k, o;

   memset (tsipBuff, 0xff, TsipBuflenI8[port]);
   // Emulate Rx tsip buffer.
   for (j = 0, o = offset; j < 8; j++, o += sampHz/8000) {
      *((ADT_UInt32*) tsipBuff) = nextCID;   tsipBuff+=4;
      for (i = 0, k = 0; i < chnCnt; i++, k++) {
          if (sampI8 == 1) *(((ADT_UInt8*) tsipBuff) + k)  = (i <<4) + o;
          else             *(((ADT_UInt16*) tsipBuff) + k) = (i <<4) + o;

          if (sampHz == 8000) continue;
          k++;
          if (sampI8 == 1) *(((ADT_UInt8*) tsipBuff) + k)  = (i <<4) + o + 1;
          else             *(((ADT_UInt16*) tsipBuff) + k) = (i <<4) + o + 1;
      }
      tsipBuff += TsipFallocI8[port] - 8;
      *((ADT_UInt32*) tsipBuff) = nextCID++;   tsipBuff+=4;     
   }
}
void circInit () {
   circDrain.PutIndex = circDrain.TakeIndex = 0;
   circDrain.pBufrBase = drainBuff;
   circDrain.BufrSize  = sizeof (drainBuff)/2;
   circDrain.SlipSamps = 0;

   circSink.PutIndex = circSink.TakeIndex = 0;
   circSink.pBufrBase = sinkBuff;
   circSink.BufrSize  = sizeof (sinkBuff)/2;
   circSink.SlipSamps = 0;

   circLcl.PutIndex = circLcl.TakeIndex = 0;
   circLcl.pBufrBase = lclBuff;
   circLcl.BufrSize  = sizeof (lclBuff)/2;
   circLcl.SlipSamps = 0;
}

void tsipTest (int sampHz, int sampI8, int chnCnt, int portBits) {

   int sampCnt;
   int startTime, msec;
   int port;

   dmaIdx = 0;
   memset (dmaLog, 0, sizeof (dmaLog));

   ApiBlock.DmaSwiCnt = 0;
   printf ("\n\ntsip ports %d test.  %d Hz.  %d Bits.  %d Channels\n", portBits, sampHz, sampI8 * 8, chnCnt);

   // Initialize TSIP variables
   tsipCfg->sampleRatekHz   = sampHz;
   tsipCfg->tdmSampleSizeI8 = sampI8;

   sampCnt = sampHz/1000;
   sysConfig.samplesPerMs = sampCnt;

   intCnt = 0;
   InitTSIPMode ();

   for (port = 0; port < 3; port++) {

      MaxDmaSlots[port] = chnCnt;
      TsipFallocI8[port] = 8 + (sampI8 * (sampCnt/8) * chnCnt) + 3;
      TsipFallocI8[port] &= ~3;

      zeroTxBufferFull (port);
   }

   //-------------------------------------------------------------
   // Set circular drain buffer
   fillDrainBuff ();
   logDMA ("Start", 0, 1);
   startTime = CLK_gethtime ();
   dspTDMInit (0, portBits, chnCnt, sampI8);
   while (((CLK_gethtime() - startTime) < (CLK_countspms() * 50)) && (ApiBlock.DmaSwiCnt < 1000));
   msec = (CLK_gethtime() - startTime) / CLK_countspms ();

   printf ("%d.%d SWIs in 50 milliseconds\n", intCnt, msec);
   if (intCnt < 50 || 52 < intCnt) dumpDMAlog ();  
   printf ("%d.%d SWIs in 50 milliseconds\n", intCnt, msec);
   getchar ();
   StopSerialPortIo ();

}

void txCopyTest (int sampHz, int sampI8, int chnCnt, int port) {

   int sampCnt;


   dmaIdx = 0;
   memset (dmaLog, 0, sizeof (dmaLog));

   printf ("\n\nTx copy %d test.  %d Hz.  %d Bits.  %d Channels\n", port, sampHz, sampI8 * 8, chnCnt);
   // Initialize TSIP variables
   tsipCfg->sampleRatekHz   = sampHz;
   tsipCfg->tdmSampleSizeI8 = sampI8;

   sampCnt = sampHz/1000;
   sysConfig.samplesPerMs = sampCnt;

   DMAInitialize ();
   zeroTxBufferFull (port);

   MaxDmaSlots[port] = chnCnt;
   TsipFallocI8[port] = 8 + (sampI8 * (sampCnt/8) * chnCnt) + 3;
   TsipFallocI8[port] &= ~3;

   //-------------------------------------------------------------
   // Set circular drain buffer
   fillDrainBuff ();
   enableDMA (port);

   //-----------------------------------   
   ApiBlock.DmaSwiCnt += 1000;
   CopyDmaTxBuffers (port, chnCnt, circBuffs, 0);
   while (DmaBuffActive | DmaActive);
   logDMA ("Complete\n", port, 0);

   ApiBlock.DmaSwiCnt += 1000;
   CopyDmaTxBuffers (port, chnCnt, circBuffs, 0);
   while (DmaBuffActive | DmaActive);
   logDMA ("Complete\n", port, 0);

   CopyDmaTxBuffers (port, chnCnt, circBuffs, 0);
   while (DmaBuffActive | DmaActive);
   logDMA ("Complete\n", port, 0);
   dumpDMAlog ();

   dumpMem ("TSIP Ping", (void*) TxTsipBuffers[port],                          TsipFallocI8[port]*2, TsipFallocI8[port]);
   dumpMem ("TSIP Pong", (void*) (TxTsipBuffers[port] + TsipFallocI8[port]*8), TsipFallocI8[port]*2, TsipFallocI8[port]);

   printf ("\n\nTx copy %d test.  %d Hz.  %d Bits.  %d Channels\n", port, sampHz, sampI8 * 8, chnCnt);
   printf ("verify Tx TSIP buffers are correct\n");

   getchar ();
}
void rxCopyTest (int sampHz, int sampI8, int chnCnt, int port) {

   int sampCnt;
   ADT_UInt32  tsipBuffPing, tsipBuffPong;

   dmaIdx = 0;
   memset (dmaLog, 0, sizeof (dmaLog));

   printf ("\n\nRx copy %d test.  %d Hz.  %d Bits.  %d Channels\n", port, sampHz, sampI8 * 8, chnCnt);
   // Initialize TSIP variables
   tsipCfg->sampleRatekHz   = sampHz;
   tsipCfg->tdmSampleSizeI8 = sampI8;

   sampCnt = sampHz/1000;
   sysConfig.samplesPerMs = sampCnt;

   DMAInitialize ();
   zeroTxBufferFull (port);

   MaxDmaSlots[port] = chnCnt;
   TsipFallocI8[port] = 8 + (sampI8 * (sampCnt/8) * chnCnt) + 3;
   TsipFallocI8[port] &= ~3;

   tsipBuffPing = (ADT_UInt32) (RxTsipBuffers[port]);
   tsipBuffPong = tsipBuffPing + TsipFallocI8[port]*8;

   memset (drainBuff, 0, sizeof (drainBuff));
   memset (RxTransBuffers[port], 0, DmaPingBufferI8[port]*2);
   circDrain.PutIndex = circDrain.TakeIndex = 0;

   //-------------------------------------------------------------
   // Enable dma
   enableDMA (port);

   //-----------------------------------   
   ApiBlock.DmaSwiCnt += 1000;
   fillTsipBuff (tsipBuffPing, sampHz, sampI8, chnCnt, port, 0);
   issueRxDMA (port);
   while (DmaActive);
   CopyDmaRxBuffers (port, chnCnt, circBuffs, 0);
   logDMA ("Complete\n", port, 0);

   ApiBlock.DmaSwiCnt += 1000;
   fillTsipBuff (tsipBuffPong, sampHz, sampI8, chnCnt, port, sampCnt);
   issueRxDMA (port);
   while (DmaActive);
   CopyDmaRxBuffers (port, chnCnt, circBuffs, 0);
   logDMA ("Complete\n", port, 0);

   ApiBlock.DmaSwiCnt += 1000;
   fillTsipBuff (tsipBuffPing, sampHz, sampI8, chnCnt, port, sampCnt*2);
   issueRxDMA (port);
   while (DmaActive);
   CopyDmaRxBuffers (port, chnCnt, circBuffs, 0);
   logDMA ("Complete\n", port, 0);
   dumpDMAlog ();

   dumpMem ("Circular buffers", (void*) drainBuff, sampCnt*6*chnCnt, sampCnt*2);

   printf ("\n\nRx copy %d test complete.  %d Hz.  %d Bits.  %d Channels\n", port, sampHz, sampI8 * 8, chnCnt);
   printf ("Please verify that the circular buffers are correct\n");

   getchar ();
}

void dmaTxTest (int sampHz, int sampI8, int chnCnt, int port) {

   int i, j, sampCnt, transI8;
   ADT_UInt16  sampValue;
   ADT_UInt16* lclCrcBuff;
   ADT_UInt32  transBuff, transBuffPing, transBuffPong;
   void* buffInfo;

   dmaIdx = 0;
   memset (dmaLog, 0, sizeof (dmaLog));

   printf ("\n\nDMA Tx port %d test.  %d Hz.  %d Bits.  %d Channels\n", port, sampHz, sampI8 * 8, chnCnt);
   // Initialize TSIP variables
   tsipCfg->sampleRatekHz   = sampHz;
   tsipCfg->tdmSampleSizeI8 = sampI8;

   sampCnt = sampHz/1000;
   sysConfig.samplesPerMs = sampCnt;

   if (sampHz == 16000 && sampI8 == 1) transI8 = 1;
   else                                transI8 = 2;


   DMAInitialize ();
   zeroTxBufferFull (port);

   MaxDmaSlots[port] = chnCnt;
   TsipFallocI8[port] = 8 + (sampI8 * (sampCnt/8) * chnCnt) + 3;
   TsipFallocI8[port] &= ~3;

   //-------------------------------------------------------------
   // Set transfers buffers
   transBuffPing = (ADT_UInt32) (TxTransBuffers[port]);
   transBuffPong = transBuffPing + DmaPingBufferI8[port];
   memset (transBuffPing, 0, DmaPingBufferI8[port]*2);
   circDrain.PutIndex = circDrain.TakeIndex = 0;

   INIT_MIPS_TIMER (mips, sampCnt, sampHz);

   enableDMA (port);

   //-----------------------------------   
   // Mark DMABuff active
   // Setup ping dma transfer buffer
   // Issue DMA
   // Wait for completion
   buffInfo = getTxBuff (port);
   transBuff =  transBuffPing;
   for (i = 0; i < chnCnt; i++) {
      lclCrcBuff = circBuff;
      sampValue = i << 4;
      for (j = 0; j < sampCnt; j++) *lclCrcBuff++ = sampValue++;

      START_MIPS_TIMER (mips);
      (*circToDma) ((void *) transBuff, (void*) circBuff);
      STOP_MIPS_TIMER (mips);

      transBuff += (sampCnt * transI8);
   }
   issueTxDMA (port, buffInfo);  // ping
   while (DmaBuffActive | DmaActive);
   logDMA ("Complete\n", port, 0);

   //-----------------------------------   
   // Mark DMABuff active
   // Setup pong dma transfer buffer
   // Issue DMA
   // Wait for completion
   buffInfo = getTxBuff (port);
   transBuff =  transBuffPong;
   for (i = 0; i < chnCnt; i++) {
      lclCrcBuff = circBuff;
      sampValue = (i << 4) + sampCnt;
      for (j = 0; j < sampCnt; j++) *lclCrcBuff++ = sampValue++;
   
      START_MIPS_TIMER (mips);
      (*circToDma) ((void *) transBuff, (void*) circBuff);
      STOP_MIPS_TIMER (mips);

      transBuff += (sampCnt * transI8);
   }
   issueTxDMA (port, buffInfo);  // pong
   while (DmaBuffActive | DmaActive);
   logDMA ("Complete\n", port, 0);

   dumpDMAlog ();

   dumpMem ("TSIP Ping", (void*) TxTsipBuffers[port], TsipFallocI8[port]*2, TsipFallocI8[port]);
   dumpMem ("TSIP Pong", (void*) (TxTsipBuffers[port] + TsipFallocI8[port]*8), TsipFallocI8[port]*2, TsipFallocI8[port]);
   printf ("verify Tx TSIP buffers are correct\n");

   MIPS_REPORT (mips, "TxTSIP");
   getchar ();
}
void dmaRxTest (int sampHz, int sampI8, int chnCnt, int port) {

   int i, sampCnt, transI8;
   ADT_UInt32  tsipBuffPing, tsipBuffPong;
   ADT_UInt32  transBuff, transBuffPing, transBuffPong;
   void* buffInfo;

   dmaIdx = 0;
   memset (dmaLog, 0, sizeof (dmaLog));

   printf ("\n\nDMA Rx port %d test.  %d Hz.  %d Bits.  %d Channels\n", port, sampHz, sampI8 * 8, chnCnt);
   // Initialize TSIP variables
   tsipCfg->sampleRatekHz   = sampHz;
   tsipCfg->tdmSampleSizeI8 = sampI8;

   sampCnt = sampHz/1000;
   sysConfig.samplesPerMs = sampCnt;

   if (sampHz == 16000 && sampI8 == 1) transI8 = 1;
   else                                transI8 = 2;

   DMAInitialize ();

   MaxDmaSlots[port] = chnCnt;
   TsipFallocI8[port] = 8 + (sampI8 * (sampCnt/8) * chnCnt) + 3;
   TsipFallocI8[port] &= ~3;

   //-------------------------------------------------------------
   // Setup TSIP buffer with data.
   tsipBuffPing = (ADT_UInt32) (RxTsipBuffers[port]);
   tsipBuffPong = tsipBuffPing + TsipFallocI8[port]*8;
   memset (RxTsipBuffers[port], 0xFF, TsipBuflenI8[port]);

   transBuffPing = (ADT_UInt32) (RxTransBuffers[port]);
   transBuffPong = transBuffPing + DmaPingBufferI8[port];
   memset (transBuffPing, 0, DmaPingBufferI8[port]*2);

   INIT_MIPS_TIMER (mips, sampCnt, sampHz);

   enableDMA (port);

   fillTsipBuff (tsipBuffPing, sampHz, sampI8, chnCnt, port, 0);
   fillTsipBuff (tsipBuffPong, sampHz, sampI8, chnCnt, port, sampCnt);
   circDrain.PutIndex = circDrain.TakeIndex = 0;

   //--------------------------------------------
   // Start DMA ping transfer and wait for completion
   printf ("Rx DMA ping started\n");
   issueRxDMA (port);
   while (DmaActive);

   // Copy from DMA transfer buffers to circBuff
   transBuff = transBuffPing;
   buffInfo = getRxBuff (port);
   for (i = 0; i < chnCnt; i++) {
      START_MIPS_TIMER (mips);
      (*dmaToCirc) ((void *) transBuff, (void*) circBuff);
      STOP_MIPS_TIMER (mips);

      transBuff += (sampCnt * transI8);
   }
   freeRxBuff (port, buffInfo);
   while (DmaActive | DmaBuffActive);
   logDMA ("Complete\n", port, 0);

   //--------------------------------------------
   // Start DMA ping transfer and wait for completion
   issueRxDMA (port);
   while (DmaActive);

   // Copy from DMA transfer buffers to 
   transBuff = transBuffPong;
   buffInfo = getRxBuff (port);
   for (i = 0; i < chnCnt; i++) {
      START_MIPS_TIMER (mips);
      (*dmaToCirc) ((void *) transBuff, (void*) circBuff);
      STOP_MIPS_TIMER (mips);

      transBuff += (sampCnt * transI8);
   }
   freeRxBuff (port, buffInfo);
   while (DmaBuffActive | DmaActive);
   logDMA ("Complete\n", port, 0);

   dumpDMAlog ();
   printf ("Rx DMA pong completed\n");
   MIPS_REPORT (mips, "RxTSIP");
   getchar();

}
void regressionTests () {
  
   // Test TxDMA
   dmaTxTest (16000, BITS8, MAX_GPAK_SLOTS, Port1);

}


void NB_8bit () {
   ADT_UInt64* transBuff;
   ADT_UInt16* transBuff16;
   int i;

   printf ("\n\nNarrowband 8-bit\n");

   extCrcRxBuff[0] = &circSink;
   extCrcRxBuff[1] = &circLcl;
   extCrcRxBuff[2] = &circDrain;
   extCrcRxBuff[3] = NULL;

   circInit ();
   circDrain.PutIndex  = circDrain.BufrSize - 8;
   circDrain.TakeIndex = circDrain.BufrSize - 8;

   memset (sinkBuff,  0, sizeof (sinkBuff));
   memset (lclBuff,   0, sizeof (lclBuff));
   memset (drainBuff, 0, sizeof (drainBuff));

   transBuff = (ADT_UInt64*) RxTransBuff0;
   transBuff16 = (ADT_UInt16*) RxTransBuff0;
   for (i=1; i<=120; i++)  *transBuff16++ = i;

   transBuff = (void*) dmaToCirc8BitNB (RxTransBuff0, extCrcRxBuff, 3);
   transBuff = (void*) dmaToCirc8BitNB (transBuff,    extCrcRxBuff, 3);
   transBuff = (void*) dmaToCirc8BitNB (transBuff,    extCrcRxBuff, 0);

   dumpMem ("CircSink", sinkBuff, 40, 16);
   dumpMem ("CircLcl ", lclBuff,  40, 16);
   dumpMem ("CircDrain", &drainBuff[circDrain.TakeIndex], 16, 16);
   dumpMem ("         ", &drainBuff[0], 24, 16);
   
   memset (TxTransBuff0, 0, sizeof (TxTransBuff0));
   transBuff = (ADT_UInt64*) TxTransBuff0;
   transBuff = (void*) circToDma8BitNB (TxTransBuff0, extCrcRxBuff, 3);
   transBuff = (void*) circToDma8BitNB (transBuff,    extCrcRxBuff, 3);
   transBuff = (void*) circToDma8BitNB (transBuff,    extCrcRxBuff, 0);
   dumpMem ("TxTrans", TxTransBuff0, 128, 16);
   
}
void NB_16bit () {
   ADT_UInt64* transBuff;
   ADT_UInt16* transBuff16;
   int i;

   printf ("\n\nNarrowband 16-bit\n");
   extCrcRxBuff[0] = &circSink;
   extCrcRxBuff[1] = &circLcl;
   extCrcRxBuff[2] = &circDrain;
   extCrcRxBuff[3] = NULL;

   circInit ();
   circDrain.PutIndex  = circDrain.BufrSize - 8;
   circDrain.TakeIndex = circDrain.BufrSize - 8;

   memset (sinkBuff,  0, sizeof (sinkBuff));
   memset (lclBuff,   0, sizeof (lclBuff));
   memset (drainBuff, 0, sizeof (drainBuff));

   transBuff = (ADT_UInt64*) RxTransBuff0;
   transBuff16 = (ADT_UInt16*) RxTransBuff0;
   for (i=1; i<=120; i++)  *transBuff16++ = i;

   transBuff = (void*) dmaToCirc16BitNB (RxTransBuff0, extCrcRxBuff, 3);
   transBuff = (void*) dmaToCirc16BitNB (transBuff,    extCrcRxBuff, 3);
   transBuff = (void*) dmaToCirc16BitNB (transBuff,    extCrcRxBuff, 0);

   dumpMem ("CircSink", sinkBuff, 40, 16);
   dumpMem ("CircLcl ", lclBuff,  40, 16);
   dumpMem ("CircDrain", &drainBuff[circDrain.TakeIndex], 16, 16);
   dumpMem ("         ", &drainBuff[0], 24, 16);
   
   memset (TxTransBuff0, 0, sizeof (TxTransBuff0));
   transBuff = (ADT_UInt64*) TxTransBuff0;
   transBuff = (void*) circToDma16BitNB (TxTransBuff0, extCrcRxBuff, 3);
   transBuff = (void*) circToDma16BitNB (transBuff,    extCrcRxBuff, 3);
   transBuff = (void*) circToDma16BitNB (transBuff,    extCrcRxBuff, 0);
   dumpMem ("TxTrans", TxTransBuff0, 128, 16);
}
void WB_16bit () {
   ADT_UInt64* transBuff;
   ADT_UInt16* transBuff16;
   int i;

   printf ("\n\nWideband 16-bit\n");
   extCrcRxBuff[0] = &circSink;
   extCrcRxBuff[1] = &circLcl;
   extCrcRxBuff[2] = &circDrain;
   extCrcRxBuff[3] = NULL;
   
   circInit ();
   circDrain.PutIndex  = circDrain.BufrSize - 16;
   circDrain.TakeIndex = circDrain.BufrSize - 16;

   memset (sinkBuff,  0, sizeof (sinkBuff));
   memset (lclBuff,   0, sizeof (lclBuff));
   memset (drainBuff, 0, sizeof (drainBuff));

   transBuff = (ADT_UInt64*) RxTransBuff0;
   transBuff16 = (ADT_UInt16*) RxTransBuff0;
   for (i=1; i<=120; i++)  *transBuff16++ = i;

   transBuff = (void*) dmaToCirc16BitWB (RxTransBuff0, extCrcRxBuff, 3);
   transBuff = (void*) dmaToCirc16BitWB (transBuff,    extCrcRxBuff, 3);
   transBuff = (void*) dmaToCirc16BitWB (transBuff,    extCrcRxBuff, 0);

   dumpMem ("CircSink", sinkBuff, 72, 16);
   dumpMem ("CircLcl ", lclBuff,  72, 16);
   dumpMem ("CircDrain", &drainBuff[circDrain.TakeIndex], 32, 16);
   dumpMem ("         ", &drainBuff[0], 40, 16);
   
   memset (TxTransBuff0, 0, sizeof (TxTransBuff0));
   transBuff = (ADT_UInt64*) TxTransBuff0;
   transBuff = (void*) circToDma16BitWB (TxTransBuff0, extCrcRxBuff, 3);
   transBuff = (void*) circToDma16BitWB (transBuff,    extCrcRxBuff, 3);
   transBuff = (void*) circToDma16BitWB (transBuff,    extCrcRxBuff, 0);
   dumpMem ("TxTrans", TxTransBuff0, 192, 16);
}

void copyTests () {
   int i;
   NB_8bit ();
   NB_16bit ();
   WB_16bit ();

   printf ("\nCopy tests completed\n");
   getchar ();

   for (i=0; i<MAX_GPAK_SLOTS; i++) {
      circBuffs[i] = &circDrain;
      extCrcRxBuff[i] = &circSink;
      extCrcTxBuff[i] = &circDrain;
   }
}

void timingTests () {

   copyTests ();

   // Turn on power and setup interrupt handlers.  
   DMAInitialize ();

   regressionTests ();

   tsipTest (8000,  BITS8,  250, 1);
   tsipTest (8000,  BITS8,  250, 4);
   tsipTest (8000,  BITS8,  250, 5);

   rxCopyTest (8000,  BITS8,  10, Port1);
   rxCopyTest (8000,  BITS16, 10, Port1);
   rxCopyTest (16000, BITS8,  10, Port1);
   rxCopyTest (16000, BITS16, 10, Port1);

   txCopyTest (8000,  BITS8,  10, Port1);
   txCopyTest (8000,  BITS16, 10, Port1);
   txCopyTest (16000, BITS8,  10, Port1);
   txCopyTest (16000, BITS16, 10, Port1);

   // Test TxDMA
   dmaTxTest (8000,  BITS8,   2, Port0);  
   dmaTxTest (8000,  BITS16,  3, Port1);
   dmaTxTest (16000, BITS8,   5, Port1);
   dmaTxTest (16000, BITS16, 10, Port2);
   dmaTxTest (8000,  BITS8, MAX_GPAK_SLOTS, Port1);
   dmaTxTest (16000, BITS8, MAX_GPAK_SLOTS, Port1);

   dmaRxTest (8000,  BITS8,   2, Port0);  
   dmaRxTest (8000,  BITS16,  3, Port0);
   dmaRxTest (16000, BITS8,   5, Port0);
   dmaRxTest (16000, BITS16, 10, Port0);
   dmaRxTest ( 8000, BITS8, MAX_GPAK_SLOTS, Port1);
   dmaRxTest (16000, BITS8, MAX_GPAK_SLOTS, Port1);
   
   printf ("All tests completed\n");
   while (ADT_TRUE);
}

inline void WaitClks (int clkCnt) {

   clkCnt = 4;
   while (clkCnt--);
}
// PLL Initialization
#pragma  CODE_SECTION (EVM6472_pllc_init, "INIT_PROG_SECT")
void EVM6472_pllc_init () {

   CSL_PllcRegsOvly Addr;

   int SlowClkX4, ResetClks, LockClks;

   ADT_UInt32 mult, mult_val;
   ADT_UInt32 clkInKHz, targetKHz, bestKHz, testKHz;

   targetKHz = GBL_getFrequency ();
   clkInKHz  = GBL_getClkIn (); 

   // Find multiplier that comes closest to target
   // frequency without exceeding it
   bestKHz = 0;
   mult_val = 1;

   for (mult = 1; mult < 64; mult++) {
      testKHz = clkInKHz * mult;
      if (targetKHz < testKHz) break;
      bestKHz = testKHz;
      mult_val = mult;
   }

   printf ("PLL set to provide DSP clock at %d kHz\n", bestKHz);
   printf ("\tclkIn: %d  target: %d  multiplier: %d.\n",
           clkInKHz, targetKHz, mult_val);

   // NOTES:  SlowClk4 derived from slowest generated CLKIN     (CLKIN1 = 15 MHz = 67 ns)
   //         ResetClks and LockClks derived from slowest CLKIN (CLKIN1 = 15 MHz = 67 ns)
   //         Values obtained from 6472 spec sheet
   SlowClkX4 =    4 *  67 * CLK_countspms() / 1000;
   ResetClks =  256 *  67 * CLK_countspms() / 1000;
   LockClks  = 2000 *  67 * CLK_countspms() / 1000;

   /** PLL 1 configuration *****************************************/
   Addr = (CSL_PllcRegsOvly) CSL_PLLC_0_REGS;
   Addr->PLLCTL &= ~(0x00000020);   /* PLLCTL.PLLENSRC = 0 (enable PLLEN bit).*/
   Addr->PLLCTL &= ~(0x00000001);   /* PLLCTL.PLLEN = 0 (bypass mode).*/
   WaitClks (SlowClkX4);            /* Wait 4 cycles of the slowest of PLLOUT or CLKIN */

   Addr->PLLCTL |= 0x00000008;           /* PLLCTL.PLLRST = 1 (reset PLL).*/
   Addr->PLLM = mult_val - 1;            /* PLL Multiplier */
   while (Addr->PLLSTAT & 0x00000001);   /* Wait for previous operations to complete */

   Addr->PLLCMD |= 0x00000001;          /* Initiate operation to change divide values and align SYSCLKs */
   while (Addr->PLLSTAT & 0x00000001);  /* Wait until operation is complete */
   WaitClks (ResetClks);                /* Wait minimum of 256 * CLKIN1 */
   Addr->PLLCTL &= ~(0x00000008);       /* PLLCTL.PLLRST = 0  (remove from reset.) */

   WaitClks (LockClks);
   Addr->PLLCTL |= (0x00000001);        /* PLLCTL.PLLEN = 1 (enable PLL mode) */

      /** PLL 2 configuration (EMAC) **********************************/
   Addr = (CSL_PllcRegsOvly) CSL_PLLC_1_REGS;
   Addr->PLLCTL &= ~(0x00000020);   /* PLLCTL.PLLENSRC = 0 (enable PLLEN bit).*/
   Addr->PLLCTL &= ~(0x00000001);   /* PLLCTL.PLLEN = 0 (bypass mode).*/
   WaitClks (SlowClkX4);            /* Wait 4 cycles of the slowest of PLLOUT or CLKIN */

   Addr->PLLCMD |= 0x00000001;          /* Initiate operation to change divide values and align SYSCLKs */
   while (Addr->PLLSTAT & 0x00000001);  /* Wait until operation is complete */
   WaitClks (ResetClks);                /* Wait minimum of 256 * CLKIN1 */
   Addr->PLLCTL &= ~(0x00000008);       /* PLLCTL.PLLRST = 0  (remove from reset.) */

   WaitClks (LockClks);
   Addr->PLLCTL |= (0x00000001);        /* PLLCTL.PLLEN = 1 (enable PLL mode) */

}


void main () {
   int i;

   swi_attr.mailbox = 0;
   swi_attr.priority = DMAPriority;
   SWI_Dma = SWI_create (&swi_attr);


// EVM6472_pllc_init ();

   tdmCore = DSPCore = DNUM;

   circInit ();
   for (i=0; i<MAX_GPAK_SLOTS; i++) {
      circBuffs[i] = &circDrain;
      extCrcRxBuff[i] = &circSink;
      extCrcTxBuff[i] = &circDrain;
   }
   for (i=0; i<2; i++) sysConfig.dmaBufferSize[i] = TSIPBuffI16;
   return;
}
