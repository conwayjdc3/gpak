#
#  CONFERENCE LIBRARY MAKE FILE
#
#  Make flags
#     GEN=y     - build vector generation images
#     MIPS=y    - build mips test images
#
UTIL_DIR ?= W:/Utilities
include $(UTIL_DIR)/PlatformDefs.Make
CGT?=6_1_15

OPTS=GPAKVERSION_H="\"V6_0.h64p"\" TEST

override SRCDIR:=..$_
override SRCDIR_:=..$_

INCDIRS+=..$_..$_..$_Includes ..$_..$_Includes64 ..$_..$_..$_GpakApi

LIBS    =tsip
LIBBLDS =rls dbg

FLGS_dbg:=-g -D _DEBUG
tsip_SRC = 6472TsipDma.c 6472Tsip.c 647xPowerCtrl.c TsipApi.c 6472DmaCopy.s64

EXECS   =test
EXEBLDS =rls dbg
test_SRC = 6472TsipTest.c


rls_LIB = rls
dbg_LIB = dbg

define test_lib_rules # <bld>
   LIBNAMES_$1+= $$(call lib_name,tsip,_${$1_LIB})
endef

${foreach bld,$(EXEBLDS),                  \
   ${eval ${call test_lib_rules,$(bld)}}  \
}

#{
#  PlatformIncludes.mak sets the following variables:
#
#      ROOTDIR, SRCDIR, SUBDIRS, INCDIRS, CMPFLAGS, OPTS,
#      ProjDir, ExampleDir, USER_HDR_DIR, TESTCODEDIR,
#      TESTVECTORDIR, DOCDIR, DELIVDIR, TCF_OPT, CMDS_ti
#
# to their default values based upon PLATFORM, CPU_TYPE, and the common algorithm directory structure
#
# This include file should be modified when new platforms are added.  This will allow new platforms to
# be easily added for all existing algorithms.
#}
include $(UTIL_DIR)/Make.PlatformIncludes

# Setup BIOS for timing executables
CMDS_test := $(CMDS_ti)
${call timingExec,test}


#============================================
#{ Standard build procedures.
#
#  dependencies -> <os>-<bld>.dep
#  objects      -> <objdir>/<basename>-<platform>-<version>.<object suffix>_<bld>
#  libraries    -> <libdir>/<libray>-<platform>-<version>.<lib suffix>_<bld>
#  executables  -> <exedir>/<exebase>-<platform>-<version>.<exe suffix>_<bld>
#							  
#
#   targets = clean        libs         execs
#             clean_<bld>  <lib>_<bld>  exec_<bld>
#}
clean_execs::
	@echo clean_execs
	
include $(UTIL_DIR)$_Make.Procs

