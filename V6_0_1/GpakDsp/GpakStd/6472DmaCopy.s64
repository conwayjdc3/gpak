;
;  6742DmaCopy.s62  
   .if 0
void dmaToCirc8BitNB (ADT_UInt64* dmaBuff, CircBufInfo_t **CircBufr, int slotCnt) {
   register CircBufInfo_t *Circ;

   Circ = *CircBufr++;
   while (slotCnt--) {
      memcpy (&Circ->pBufrBase[Circ->PutIndex], dmaBuff, 16);

      Circ->PutIndex += 8;
      if (Circ->BufrSize <= Circ->PutIndex) Circ->PutIndex = 0;
      Circ = *CircBufr++;

      dmaBuff += 2;
   }
}
  .endif

    .global     _dmaToCirc8BitNB
    .global     _dmaToCirc16BitNB
    .sect    "FAST_PROG_SECT"

;{ void dmaToCirc8BitNB (ADT_UInt64* dmaBuff (A4), CircBufInfo_t **CircBufr (B4), int slotCnt (A6)) 
; A0  tmp      (Circ->BufrSize < frmSamps + Circ->PutIndex)
; B0  slotCnt

; A4,B5    dmaBuff,  dmaBuff+8
; B4       CircBufr

; B2  0
; A9:A8, B9:B8  (TEMPS)
; A16,B16  Circ
; B17 Circ->PutIndex + 8 (Circular buffer increased by 8 samples)
; A18 Circ->BufrBase
; A19 Circ->BufrSize
; A20 Circ->PutIndex
; A21,B21  CircAddr, CircAddr+8;
;}

        .asmfunc

_dmaToCirc8BitNB:
_dmaToCirc16BitNB:
;{
          LDW   *B4++[1],A16     ; A16 (Circ) = *CircBufr++
          MV    A6,B0            ; B0 = slotCnt
          ADD   8,A4,B5          ; A4 = dmaBuff; B5 = dmaBuff+1
 || [ B0] MVK   0,B2             ; B2 = 0
 || [!B0] B     Return           ; return if slotCnt == 0

          NOP         2
        ; Load Circular buffer variables
          LDW   *+A16[0],A18   ; A18 = Circ->BufrBase, A19 = Circ->BufrSize
          LDW   *+A16[2],A20   ; A20 = Circ->PutIndex
          LDW   *+A16[1],A19   ; A19 = Circ->BufrSize
          MV    A16,B16        ; B16 (Circ) = *CircBufr
          LDW   *B4++[1],A16   ; A16 (Circ) = *CircBufr++
          NOP         1

NextBuff:
          LDDW    *A4++[2],B9:B8 ; B9:B8 = *dmaBuff     (bytes 0-7)
          LDDW    *B5++[2],A9:A8 ; A9:A8 = *(dmaBuff+8) (bytes 8-15)

          ADD     8,A20,B17      ; B17 (NewPut)   = Circ->PutIndex + 8 (NB ==> add 8 samples to circular buffer)
 ||       ADDAH   A18,A20,A21    ; A21 (CircAddr) = Circ->BufrBase + Circ->PutIndex 

          ADD     8,A21,B21      ; B21 = CircAddr+8
 ||       CMPGTU  A19,B17,A0     ; A0 (tmp) = (Circ->BufrSize < NewPut)
 
          ADD     B0,-1,B0       ; B0 = sltCnt - 1
 ||       LDNDW   *+A16[0],A19:A18 ; A18 = Circ->BufrBase, A19 = Circ->BufrSize
          LDW     *+A16[2],A20     ; A20 = Circ->PutIndex

    [ B0] B       NextBuff
    [!A0] STW     B2,*+B16[2]     ; if (tmp) Circ->PutIndx = 0
    [ A0] STW     B17,*+B16[2]    ; else     Circ->PutIndex += frmSamps

          MV      A16,B16         ; B16 (Circ) = *CircBufr
          LDW     *B4++[1],A16    ; A16 = *CircBufr++
          STDW    B9:B8,*A21(0)   ; *(CircAddr)    (bytes 0-7)
 ||       STDW    A9:A8,*B21(0)   ; *(CircAddr+8)  (bytes 8-15)
    
Return:
          BNOP    B3,5            ; return
;}
;        .endasmfunc

;-------------------------------------------------------------------------
;------------------------------------------------------------------------
    .global     _circToDma8BitNB
    .global     _circToDma16BitNB
;        .asmfunc

_circToDma8BitNB:
_circToDma16BitNB:
;{
          LDW   *B4++[1],A16     ; A16 (Circ) = *CircBufr++
          MV    A6,B0            ; B0 = slotCnt
          ADD   8,A4,B5          ; A4 = dmaBuff; B5 = dmaBuff+1
 || [ B0] MVK   0,B2             ; B2 = 0
 || [!B0] B     Return1          ; return if slotCnt == 0

         ; Load Circular buffer variables
          NOP         2
          LDW   *+A16[0],A18   ; A18 = Circ->BufrBase, A19 = Circ->BufrSize
          LDW   *+A16[3],A20   ; A20 = Circ->TakeIndex
          LDW   *+A16[1],A19   ; A19 = Circ->BufrSize
          MV    A16,B16        ; B16 (Circ) = *CircBufr
          LDW   *B4++[1],A16   ; A16 (Circ) = *CircBufr++
          NOP   1

NextBuff1:
          ADD     8,A20,B17      ; B17 (NewTake)  = Circ->TakeIndex + 8 (NB ==> remove 8 samples from circular buffer)
 ||       ADDAH   A18,A20,A21    ; A21 (CircAddr) = Circ->BufrBase + Circ->TakeIndex 

          ADD     8,A21,B21      ; B21 = CircAddr+8
 ||       CMPGTU  A19,B17,A0     ; A0 (tmp) = (Circ->BufrSize < NewTake)
 
          LDDW    *A21(0),B9:B8 ; B9:B8 = *CircAddr     (bytes 0-7)
          LDDW    *B21(0),A9:A8 ; A9:A8 = *(CircAddr+8) (bytes 8-15)

          ADD     B0,-1,B0         ; B0 = sltCnt - 1
 ||       LDNDW   *+A16[0],A19:A18 ; A18 = Circ->BufrBase, A19 = Circ->BufrSize
          LDW     *+A16[3],A20     ; A20 = Circ->TakeIndex

    [ B0] B       NextBuff1
    [!A0] STW     B2,*+B16[3]     ; if (tmp) Circ->TakeIndex = 0
    [ A0] STW     B17,*+B16[3]    ; else     Circ->TakeIndex += frmSamps

          MV      A16,B16         ; B16 (Circ) = *CircBufr
          LDW     *B4++[1],A16    ; A16 = *CircBufr++
          STDW    B9:B8,*A4++[2]  ; *(dmaBuff)   (bytes 0-7)
 ||       STDW    A9:A8,*B5++[2]  ; *(dmaBuff+8) (bytes 8-15)
    
Return1:
          BNOP    B3,5            ; return
          .endasmfunc
;}


;
;  
   .if 0
void dmaToCirc16BitWB (ADT_UInt64* dmaBuff, CircBufInfo_t **CircBufr, int slotCnt) {
   register CircBufInfo_t *Circ;

   Circ = *CircBufr++;
   while (slotCnt--) {
      memcpy (&Circ->pBufrBase[Circ->PutIndex], dmaBuff, 32);

      Circ->PutIndex += 16;
      if (Circ->BufrSize <= Circ->PutIndex) Circ->PutIndex = 0;
      Circ = *CircBufr++;

      dmaBuff += 4;
   }
}
  .endif

    .global     _dmaToCirc16BitWB
    .sect    "FAST_PROG_SECT"

;{   void dmaToCirc16BitWB (ADT_UInt64* dmaBuff (A4), CircBufInfo_t **CircBufr (B4), int slotCnt (A6)) 
; A0  tmp      (Circ->BufrSize < frmSamps + Circ->PutIndex)
; B0  slotCnt

; B4       CircBufr
; A4,B5    dmaBuff,  dmaBuff+8
; A21,B21  CircAddr, CircAddr+8  

; B2   0
; B31 16
; A9:A8, B9:B8, A23:A22, B23:B22  (TEMPS)

; A16,B16  Circ
; B17 Circ->PutIndex + 16 (Circular buffer increased by 16 samples)
; A18 Circ->BufrBase
; A19 Circ->BufrSize
; A20 Circ->PutIndex
;}

        .asmfunc

_dmaToCirc16BitWB:
;{
          LDW   *B4++[1],A16     ; A16 (Circ) = *CircBufr++
          MV    A6,B0            ; B0 = slotCnt
          ADD   8,A4,B5          ; A4 = dmaBuff; B5 = dmaBuff+8
 || [ B0] MVK   0,B2             ; B2 = 0
 || [!B0] B     Return16WB       ; return if slotCnt == 0

          MVK   16,B31
          NOP         1
        ; Load Circular buffer variables
          LDW   *+A16[0],A18   ; A18 = Circ->BufrBase, A19 = Circ->BufrSize
          LDW   *+A16[2],A20   ; A20 = Circ->PutIndex
          LDW   *+A16[1],A19   ; A19 = Circ->BufrSize
          MV    A16,B16        ; B16 (Circ) = *CircBufr
          LDW   *B4++[1],A16   ; A16 (Circ) = *CircBufr++

NextBuff16WB:
          LDDW    *+A4[0],B9:B8    ;  B9:B8  = *dmaBuff      (bytes 0-7)
          LDDW    *+B5[0],A9:A8    ;  A9:A8  = *(dmaBuff+8)  (bytes 8-15)
          LDDW    *+A4[2],B23:B22  ; B23:B22 = *(dmaBuff+16) (bytes 16-23)
          LDDW    *+B5[2],A23:A22  ; A23:A22 = *(dmaBuff+24) (bytes 24-31)

          ADD     A20,B31,B17    ; B17 (NewPut)   = Circ->PutIndex + 16 (WB ==> add 16 samples to circular buffer)

          ADDAH   A18,A20,A21    ; A21 (CircAddr) = Circ->BufrBase + Circ->PutIndex 
          ADD     A21,8,B21      ; B21 = CircAddr+8
 ||       CMPGTU  A19,B17,A0     ; A0 (tmp) = (Circ->BufrSize < NewPut)
 
          ADD     B0,-1,B0       ; B0 = sltCnt - 1
 ||       LDNDW   *+A16[0],A19:A18 ; A18 = Circ->BufrBase, A19 = Circ->BufrSize
          LDW     *+A16[2],A20     ; A20 = Circ->PutIndex

          ADDK    32,A4           ; dmaBuff += 32
  ||      ADDK    32,B5   
  ||      STDW    B9:B8,*A21(0)    ; *(CircAddr)    (bytes 0-7)
  ||      STDW    A9:A8,*B21(0)    ; *(CircAddr+8)  (bytes 8-15)

    [ B0] B       NextBuff16WB
          STDW    B23:B22,*A21(16) ; *(CircAddr+16) (bytes 16-23)
  ||      STDW    A23:A22,*B21(16) ; *(CircAddr+24) (bytes 24-31)
    [!A0] STW     B2,*+B16[2]     ; if (tmp) Circ->PutIndx = 0
    [ A0] STW     B17,*+B16[2]    ; else     Circ->PutIndex += frmSamps

          MV      A16,B16          ; B16 (Circ) = *CircBufr
          LDW     *B4++[1],A16     ; A16 = *CircBufr++
    
Return16WB:
          BNOP    B3,5            ; return
;}
;        .endasmfunc

;-------------------------------------------------------------------------
;------------------------------------------------------------------------

    .global     _circToDma16BitWB
;        .asmfunc

_circToDma16BitWB:
;{
          LDW   *B4++[1],A16     ; A16 (Circ) = *CircBufr++
          MV    A6,B0            ; B0 = slotCnt
          ADD   8,A4,B5          ; A4 = dmaBuff; B5 = dmaBuff+1
 || [ B0] MVK   0,B2             ; B2 = 0
 || [!B0] B     Return_CD16WB     ; return if slotCnt == 0

         ; Load Circular buffer variables
          NOP     1
          MVK   16,B31
          LDW   *+A16[0],A18   ; A18 = Circ->BufrBase, A19 = Circ->BufrSize
          LDW   *+A16[3],A20   ; A20 = Circ->TakeIndex
          LDW   *+A16[1],A19   ; A19 = Circ->BufrSize
          MV    A16,B16        ; B16 (Circ) = *CircBufr
          LDW   *B4++[1],A16   ; A16 (Circ) = *CircBufr++
          NOP     1

NextBuff_CD16WB:
          ADD     B31,A20,B17    ; B17 (NewTake)  = Circ->TakeIndex + 16 (WB ==> remove 8 samples from circular buffer)
 ||       ADDAH   A18,A20,A21    ; A21 (CircAddr) = Circ->BufrBase + Circ->TakeIndex 

          ADD     8,A21,B21      ; B21 = CircAddr+8
 ||       CMPGTU  A19,B17,A0     ; A0 (tmp) = (Circ->BufrSize < NewTake)
 
          LDDW    *A21(16),B23:B22 ; B23:B22 = *(CircAddr+16) (bytes 16-23)
          LDDW    *B21(16),A23:A22 ; A23:A22 = *(CircAddr+24) (bytes 24-31)
          LDDW    *A21(0),B9:B8    ; B9:B8   = *CircAddr      (bytes 0-7)
          LDDW    *B21(0),A9:A8    ; A9:A8   = *(CircAddr+8)  (bytes 8-15)
 
          ADD     B0,-1,B0         ; B0 = sltCnt - 1
 ||       LDNDW   *+A16[0],A19:A18 ; A18 = Circ->BufrBase, A19 = Circ->BufrSize
          LDW     *+A16[3],A20     ; A20 = Circ->TakeIndex

    [!A0] STW     B2,*+B16[3]     ; if (tmp) Circ->TakeIndex = 0

    [ B0] B       NextBuff_CD16WB

    [ A0] STW     B17,*+B16[3]    ; else     Circ->TakeIndex += frmSamps

          STDW    B23:B22,*+A4[2] ; *(dmaBuff+16) (bytes 16-23)
 ||       STDW    A23:A22,*+B5[2] ; *(dmaBuff+24) (bytes 24-31)
          STDW    B9:B8,*A4++[4]  ; *(dmaBuff)    (bytes 0-7)
 ||       STDW    A9:A8,*B5++[4]  ; *(dmaBuff+8)  (bytes 8-15)

          MV      A16,B16         ; B16 (Circ) = *CircBufr
          LDW     *B4++[1],A16    ; A16 = *CircBufr++
    
Return_CD16WB:
          BNOP    B3,5            ; return
          .endasmfunc
;}

          .end

