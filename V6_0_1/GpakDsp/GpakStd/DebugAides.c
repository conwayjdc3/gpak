/*
 * Copyright (c) 2007, Adaptive Digital Technologies, Inc.
 *
 * File Name: DebugAides.c
 *
 * Description:
 *   This file contains routine to aid in testing G.PAK.
 *
 *
 *  
 * Version: 1.0
 *
 * Revision History:
 *   10/2007 - Initial release.
 *
 */
// diag.codec = non-NullCodec will force codec on PKT channels
// diag.loopbackMode = Sets mode of packet loopback operation
//
// diag.pcmPcmChannels = non-zero will automatically allocate PCM to PCM channels
// diag.pcmPktChannels = non-zero will automatically allocate PCM to PKT channels
// diag.pcmCnfChannels = non-zero will automatically allocate PCM to CNFR channels
// diag.pktCnfChannels = non-zero will automatically allocate PKT to CNFR channels
//
#include <std.h>
//#include <atm.h>
//#include <hwi.h>
//#include <clk.h>
//#ifndef BIOS_V_6
//#include <exc.h>
//#endif

#include <stdio.h>
#include <string.h>
#include "GpakDefs.h"
#include "GpakExts.h"
#include "sysconfig.h"
#include "sysmem.h"
#include "GpakPcm.h"

//============================================================================
// ---------------------- Setup Aides ----------------------------------------
//
// Channel, Conference, and Serial port setup aides
//
#ifndef _DEBUG 
   // Satisfy references when using debugging components with non-debugging library
   void InitDiagnostics (GpakCodecs defaultCodec, int pcmPcmCnt, int pcmPktCnt, 
                      int pktPktCnt, int pcmCnfCnt, int pktCnfCnt) {
      return;
   }
   void inPktGenerate (int frameSize) { return; }
   void SetLoopbackMode (GpakTestMode mode) { return; }
   void outPktDistribute (int frameSize) { return; }
   void generateMcBSPSignals (int mcBSPMHz, int port, int slotsPerFrame, int SamplesPerSec) {
      return;
   }
   ADT_UInt8 appendCheckSum (void *buff, int buffI8) { return 0; }
   void verifyCheckSum (void *buff, int buffI8) { return; }
#else
//  McBsp external Stored McBSP configuration message values
extern ADT_UInt16 ClkDiv[NUM_TDM_PORTS]; // Clock divisor (0-255)
extern ADT_UInt16 PulseWidth[NUM_TDM_PORTS]; // Width (0-255 bits) of frame sync pulse
extern ADT_UInt16 FrameWidth[NUM_TDM_PORTS]; // Width (0-4095 bits) of frame
extern GpakActivation genClockEnable[NUM_TDM_PORTS];

void generateMcBSPSignals (int mcBSPMHz, int port, int slotsPerFrame, int SamplesPerSec) {

   int bitsPerFrame;
   ADT_UInt32 mcBSPClocksPerSec, mcBSPClocksPerFrame, clocksPerBit;

   sysConfig.rxClockPolarity[port] |= 2;;
   sysConfig.txClockPolarity[port] |= 2;;
   sysConfig.rxFrameSyncPolarity[port] |= 2;;
   sysConfig.txFrameSyncPolarity[port] |= 2;
   
   mcBSPClocksPerSec = mcBSPMHz * 1000000U;

   mcBSPClocksPerFrame = mcBSPClocksPerSec / SamplesPerSec;
  
   bitsPerFrame = slotsPerFrame * sysConfig.serialWordSize[port];

   if (bitsPerFrame == 0) {
      genClockEnable [port] = Disabled;
      return;
   }

   clocksPerBit = mcBSPClocksPerFrame / bitsPerFrame;


   PulseWidth [port] = 1;
   FrameWidth [port] = bitsPerFrame - 1;
   ClkDiv [port] = clocksPerBit - 1;

  genClockEnable [port] = Enabled;
}
static int codecToSamplesPerFrame (GpakCodecs codec, int FrameSize);

extern ADT_UInt16 pcmPktLoopBack (int chanId);
extern ADT_UInt16 pcmPktCrossover(int chanId);
extern ADT_UInt32 RTPTime (int instID);
extern NETFUNCTION netTransmit;

static ADT_UInt32 dataBuff[256];   // Buffer used to replicate packet data

rtpConfig_APIV5_t cfg;
ProfileTypes profile;

ADT_UInt8 appendCheckSum (void *Buff, int BuffI8) { 
   ADT_UInt8 chksum, *buff = Buff;

   chksum = 0;
   while (BuffI8--) chksum ^= *buff++;
   *buff = chksum;
   return chksum; 
}
void verifyCheckSum (void *Buff, int BuffI8) { 
   ADT_UInt8 chksum, *buff = Buff;

   chksum = 0;
   while (BuffI8--) chksum ^= *buff++;

   AppErr ("Check sum error", chksum != *buff);
}

struct diag_control {
   int countDown;
   int stress;

   GpakCodecs codec;            // Codec to be used for Pkt channels
   GpakTestMode loopbackMode;   // Loopback mode used for PKT channels

   // Set each of the following variables to desired values to automatically setup 
   // new G.PAK channels matching their respective channel types.  
   int pcmPcmChannels;
   int pcmPktChannels;
   int pcmCnfChannels;
   int pktCnfChannels;
   int pktPktChannels;

   int cnfrCnt;
   int cnfrFrameSize;
   int cnfrID;

   int disableAll;
} diag = { 10 };

static void stdDiagnostics ();

extern void (*rtdxDiagnostics) (void);

#define Vocoders sysConfig.Codecs.Bits
#define relay    sysConfig.packetProfile


//========================================================================
//{-------------------------  CHANNEL SETUP -------------------------------
static GpakVADMode SelectVADMode (GpakCodecs codec) {
   GpakVADMode VAD;
   
   VAD = SetVADMode (codec);
   if (diag.stress && (VAD & VadCodec))
      VAD = VadDisabled;
      
   return VAD;
}

//-----------------------------------------------------------------------
//  frameSizes
// 
//  returns largest (frameA) and smallest (frameB) configured frame sizes
static void frameSizes (int *frameA, int *frameB) {
   if      ((sysConfig.cfgFramesChans & FRAME_240_CFG_BIT) != 0) *frameA = 240;
   else if ((sysConfig.cfgFramesChans & FRAME_180_CFG_BIT) != 0) *frameA = 180;
   else if ((sysConfig.cfgFramesChans & FRAME_160_CFG_BIT) != 0) *frameA = 160;
   else if ((sysConfig.cfgFramesChans & FRAME_80_CFG_BIT) != 0)  *frameA = 80;
   else if ((sysConfig.cfgFramesChans & FRAME_40_CFG_BIT) != 0)  *frameA = 40;
   else if ((sysConfig.cfgFramesChans & FRAME_20_CFG_BIT) != 0)  *frameA = 20;
   else                                                          *frameA =  8;

   if      ((sysConfig.cfgFramesChans & FRAME_8_CFG_BIT) != 0)   *frameB =   8;
   else if ((sysConfig.cfgFramesChans & FRAME_20_CFG_BIT) != 0)  *frameB =  20;
   else if ((sysConfig.cfgFramesChans & FRAME_40_CFG_BIT) != 0)  *frameB =  40;
   else if ((sysConfig.cfgFramesChans & FRAME_80_CFG_BIT) != 0)  *frameB =  80;
   else if ((sysConfig.cfgFramesChans & FRAME_160_CFG_BIT) != 0) *frameB = 160;
   else if ((sysConfig.cfgFramesChans & FRAME_180_CFG_BIT) != 0) *frameB = 180;
   else                                                          *frameB = 240;
}

static void dfltRTP ();
//-----------------------------------------------------------------------
//  Called from within main to identify initial channel setup.
void InitDiagnostics (GpakCodecs defaultCodec, int pcmPcmCnt, int pcmPktCnt, 
                      int pktPktCnt, int pcmCnfCnt, int pktCnfCnt) {

  int frameA, frameB;

  memset (&diag, 0, sizeof (diag));

  rtdxDiagnostics = stdDiagnostics;

  frameSizes (&frameA, &frameB);

  diag.cnfrCnt = sysConfig.maxNumConferences;
  diag.cnfrFrameSize = codecToSamplesPerFrame (defaultCodec, frameA);

  diag.pcmPcmChannels = pcmPcmCnt;
  diag.pcmPktChannels = pcmPktCnt;
  diag.pktPktChannels = pktPktCnt;
  diag.pcmCnfChannels = pcmCnfCnt;
  diag.pktCnfChannels = pktCnfCnt;
  
  diag.stress       = TRUE;
  diag.codec        = defaultCodec;
  diag.loopbackMode = PacketCrossOver;
  diag.countDown   = 10; 
  dfltRTP ();
}


//========================================================================
void SetLoopbackMode (GpakTestMode mode) {
   diag.loopbackMode = mode;
}

//-----------------------------------------------------------------------
//
//  codecToSamplesPerFrame
//
//  Returns frame size for fixed frame codecs.
//
static int codecToSamplesPerFrame (GpakCodecs codec, int FrameSamps) {
   switch (codec) {
   default:
       return FrameSamps;

   case G728_16:  if (FrameSamps == 8) return 20;
                  else                return FrameSamps;
   case G729:
   case G729AB:   if ((FrameSamps % 80) == 0) return FrameSamps;
                  else                       return 80;

   case AMR_BASE:               return 160;

   case MELP:                   return 180;

   case ADT_4800:  
   case G723_53:  case G723_53A:  case G723_63:  case G723_63A: return 240;
   }
}

//-----------------------------------------------------------------------
//
//  nextCodec
//
//  Finds next available codec type
//
static GpakCodecs nextCodec (GpakCodecs oldCodec, int FrameSamps) {
   if (diag.codec != NullCodec) return diag.codec;

   switch (oldCodec) {
top:
   case PCMU_64: if ((oldCodec != PCMU_64) && ((relay != AAL2Trunking) || (FrameSamps == 40))) return PCMU_64;
   case PCMA_64: if ((oldCodec != PCMA_64) && ((relay != AAL2Trunking) || (FrameSamps == 40))) return PCMA_64;

   case PCMU_48: if ((oldCodec != PCMU_48) && (relay == AAL2Trunking) && (FrameSamps == 40)) return PCMU_48;
   case PCMA_48: if ((oldCodec != PCMA_48) && (relay == AAL2Trunking) && (FrameSamps == 40)) return PCMA_48;
   case PCMU_56: if ((oldCodec != PCMU_56) && (relay == AAL2Trunking) && (FrameSamps == 40)) return PCMU_56;
   case PCMA_56: if ((oldCodec != PCMA_56) && (relay == AAL2Trunking) && (FrameSamps == 40)) return PCMA_56;

   case L8:      if ((oldCodec != L8)  && (relay == RTPAVP)) return L8;
   case L16:     if ((oldCodec != L16) && (relay == RTPAVP)) return L16;

   case G723_53:  if ((oldCodec != G723_53)  && Vocoders.g723Enable && (FrameSamps == 240)) return G723_53;
   case G723_63:  if ((oldCodec != G723_63)  && Vocoders.g723Enable && (FrameSamps == 240)) return G723_63;
   case G723_53A: if ((oldCodec != G723_53A) && Vocoders.g723Enable && (FrameSamps == 240)) return G723_53A;
   case G723_63A: if ((oldCodec != G723_63A) && Vocoders.g723Enable && (FrameSamps == 240)) return G723_63A;

   case G726_16:  if ((oldCodec != G726_16) && Vocoders.g726Rate16Enable && (FrameSamps != 40))  return G726_16;
   case G726_24:  if ((oldCodec != G726_24) && Vocoders.g726Rate24Enable && (FrameSamps != 40))  return G726_24;
   case G726_32:  if ((oldCodec != G726_32) && Vocoders.g726Rate32Enable && (FrameSamps != 40))  return G726_32;
   case G726_40:  if ((oldCodec != G726_40) && Vocoders.g726Rate40Enable && (FrameSamps != 40))  return G726_40;

   case G728_16:  if ((oldCodec != G728_16) && Vocoders.g728Enable && (FrameSamps != 8))  return G728_16;

   case G729:    if ((oldCodec != G729)   && (Vocoders.g729ABEnable || Vocoders.TI_g729ABEnable) && (FrameSamps == 80)) return G729;
   case G729AB:  if ((oldCodec != G729AB) && (Vocoders.g729ABEnable || Vocoders.TI_g729ABEnable) && (FrameSamps == 80)) return G729AB;

   case AMR_BASE:  if ((oldCodec != AMR_BASE) && Vocoders.AMREnable && (FrameSamps == 160)) return AMR_BASE;

   case ADT_4800:  if ((oldCodec != ADT_4800) && Vocoders.adt4800Enable && (FrameSamps == 240)) return ADT_4800;

   case MELP:       if ((oldCodec != MELP) && Vocoders.melpEnable && (FrameSamps == 180)) return MELP;
   case CustomCodec:  if (oldCodec != CustomCodec) return CustomCodec;
 goto top;
   }

   // Return with an indication the Coding is invalid.
   return CustomCodec;
}
//-----------------------------------------------------------------------
//
//  nextChannel
//
//  Finds next available channel instance structure
//
static int nextChannel (int channel) {
   while (channel < sysConfig.maxNumChannels) {
      if (chanTable[channel]->channelType == inactive)
         return channel;
      channel++;
   }
   return -1;
}


static void dfltRTP () {

   int         chanId;

   if (sysConfig.maxJitterms  == 0) return;

   memset (&cfg, 0, sizeof (cfg));

   // Parse message for configuration parameters
   cfg.JitterMode       = 12;
   cfg.SSRC             = 0;
   cfg.StartSequence    = 0;

   cfg.DelayTargetMin   = 160;
   cfg.DelayTarget      = 320;
   cfg.DelayTargetMax   = 960;

   // Fill in GPAK constrained variables
   cfg.ClockFunction     = RTPTime;
   cfg.NetTransmit       = netTransmit;
   cfg.ProbationCount    = 3;
   cfg.SampleRate        = sysConfig.samplesPerMs * 1000;
   cfg.JB_Max            = sysConfig.maxJitterms * sysConfig.samplesPerMs;
   cfg.Pad32             = TRUE;
   cfg.APIVersion        = ADT_RTP_API_VERSION;

   profile.Voice        = 0;
   profile.CNG          = 1;
   profile.Tone         = 2;
   profile.T38_PROFILE  = 3;

   for (chanId = 0; chanId < sysConfig.maxNumChannels; chanId++) {
     cfg.ChanId            = chanId;

     // Use channel structure as network "handle" for callback
     memcpy  (&RtpChanData[chanId]->Profile, &profile, sizeof (ProfileTypes));
     RTP_ADT_Init ((RTPCONNECT *) &RtpChanData[chanId]->Instance, (void *) chanId, &cfg);
   }

   return ;
}

//----------------------------------------------------------------------
// Get hdr and payload that has been placed as packet output for the 
// specified channel.
int getOutBoundPkt (chanInfo_t *chan, PktHdr_t *Hdr, ADT_UInt32* buff, int buffI8) {
   int takeIdx;

   takeIdx = GetPktFromCircBuffer (chan->pcmToPkt.outbuffer, Hdr, sizeof (PktHdr_t), (ADT_UInt16 *) buff, buffI8);
   if (takeIdx < 0) return FALSE;

   RemovePktFromCircBuffer (chan->pcmToPkt.outbuffer, Hdr, buff, takeIdx);
   return TRUE;
}


//----------------------------------------------------------------------
// Generate inbound pkt<->pkt and pkt<->cnfr packets for even and odd channels
// from tdm->pkt channels 0 and 1 respectively.
// 
void inPktGenerate (int frameSize)  {
   chanInfo_t *chanOut, *chanIn;
   PktHdr_t    Hdr;
   int         i;

   chanOut = chanTable[0];
   if (chanOut->channelType != pcmToPacket)            chanOut = NULL;
   else if (!getOutBoundPkt (chanOut, &Hdr, dataBuff, byteSize (dataBuff))) chanOut = NULL;

   // Replicate channel 0's outbound packets as inbound packets to
   // even numbered packet channels
   for (i=0; i <= sysConfig.maxNumChannels && chanOut != NULL; i+=2) {
      chanIn = chanTable[i];
      if (chanIn->pktToPcm.SamplesPerFrame != chanOut->pcmToPkt.SamplesPerFrame) continue;
      if (chanIn->channelType != packetToPacket &&
          chanIn->channelType != conferencePacket) continue;
      if (Hdr.PayldType != chanIn->pktToPcm.Coding) continue;

      Hdr.ChanId = chanIn->ChannelId;
      PayloadToCirc (chanIn->pktToPcm.inbuffer, &Hdr, PKT_HDR_I16LEN, (ADT_UInt8 *) dataBuff, Hdr.PayldBytes);
   }

   // Replicate channel 1's outbound packets as inbound packets to
   // odd numbered packet channels
   chanOut = chanTable[1];
   if (chanOut->channelType != pcmToPacket)            chanOut = NULL;
   else if (!getOutBoundPkt (chanOut, &Hdr, dataBuff, byteSize (dataBuff))) chanOut = NULL;

   for (i=1; i <= sysConfig.maxNumChannels && chanOut != NULL; i+=2) {
      chanIn = chanTable[i];
      if (chanIn->pktToPcm.SamplesPerFrame != chanOut->pcmToPkt.SamplesPerFrame) continue;
      if (chanIn->channelType != packetToPacket &&
          chanIn->channelType != conferencePacket) continue;
      if (Hdr.PayldType != chanIn->pktToPcm.Coding) continue;

      Hdr.ChanId = chanIn->ChannelId;
      PayloadToCirc (chanIn->pktToPcm.inbuffer, &Hdr, PKT_HDR_I16LEN, (ADT_UInt8 *) dataBuff, Hdr.PayldBytes);
   }
}

//----------------------------------------------------------------------
// Distribute outbound packets from pkt<->pkt and pkt<->cnfr channels 2 and 3
// to inbound packets for tdm->pkt channels 0 and 1 respectively.  Discard
// outbound packets for channels 4 and above.
// 

static void chanOutToIn (chanInfo_t *chanOut, chanInfo_t *chanIn, int frameSize) {
   PktHdr_t Hdr; 

   if (chanIn->pcmToPkt.SamplesPerFrame != frameSize) return;
   if (chanOut->pktToPcm.SamplesPerFrame != frameSize) return;

   if (chanIn->channelType != pcmToPacket) return;

   if (chanOut->channelType != packetToPacket &&
       chanOut->channelType != conferencePacket) return;

   if (!getOutBoundPkt (chanOut, &Hdr, dataBuff, byteSize (dataBuff))) return;

   if (Hdr.PayldType != chanIn->pcmToPkt.Coding) return;

   Hdr.ChanId = chanIn->ChannelId;
   PayloadToCirc (chanIn->pktToPcm.inbuffer, &Hdr, PKT_HDR_I16LEN, (ADT_UInt8 *) dataBuff, Hdr.PayldBytes);
}

void outPktDistribute (int frameSize) {
   chanInfo_t *chan;
   PktHdr_t    Hdr;
   int         i;

   // Use outbound packets from channels 2 and 3 
   // as inbound packets for channels 0 and 1
   chanOutToIn (chanTable[2], chanTable[0], frameSize);
   chanOutToIn (chanTable[3], chanTable[1], frameSize);

   // Discard outbound packets from channels 4 on up
   for (i=4; i <= sysConfig.maxNumChannels; i++) {
      chan = chanTable[i];
      if (chan->pktToPcm.SamplesPerFrame != frameSize) continue;
      if (chan->channelType != packetToPacket &&
          chan->channelType != conferencePacket) continue;
      getOutBoundPkt (chan, &Hdr, dataBuff, byteSize (dataBuff));
   }

}
#define A chan->pcmToPkt
#define B chan->pktToPcm
static char *chanTypes[] = { "inv", "T2P", "T2T", "P2P", "CD ", "C2T", "C2P", "C2C" };

void printChannels () {
   int i;
   chanInfo_t *chan;

   printf ("\nID  Typ  CdC_Siz TDM:Slt     In       Out       Ec   \n");
   for (i=0; i<sysConfig.maxNumChannels; i++) {
      chan = chanTable [i];
      if (chan->channelType == inactive) continue;
      printf ("%3d %s ", chan->ChannelId, chanTypes[chan->channelType]);

      switch (chan->channelType) {
      case pcmToPcm:
      case pcmToPacket:

         printf ("%3d_%3d  %3d:%3d  %8x %8x %8x\n", 
                  A.Coding, A.SamplesPerFrame,
                  A.InSerialPortId, A.InSlotId,
                   A.activeInBuffer->pBufrBase, 
                  A.outbuffer->pBufrBase, A.ecFarPtr.pBufrBase);
         printf ("        ");
         printf ("%3d_%3d  %3d:%3d  %8x %8x %8x\n\n", 
                  B.Coding, B.SamplesPerFrame, 
                  B.OutSerialPortId, B.OutSlotId,
                  B.inbuffer->pBufrBase, 
                  B.outbuffer.pBufrBase, B.ecFarPtr.pBufrBase);
         break;

      case packetToPacket:
         printf ("%3d_%3d           %8x \n", 
                  B.Coding, B.SamplesPerFrame,  B.inbuffer->pBufrBase);
         printf ("        ");
         printf ("%3d_%3d                    %8x\n\n", 
                  A.Coding, A.SamplesPerFrame, A.outbuffer->pBufrBase);
         break;
 
      default:
         printf ("\n");
         break;
      }
   }
   return;
}

//}-----------------------------------------------------------------------
//
//{ ---- PCM to PCM channel setup
//
//  startPcmPcmChan
//
//    starts a channel PCM to PCM channel with all resources
//    (echo canceller, AGC, ...) that are available at run time on
//    the specified time slot and the next time slot
//
static GPAK_ChannelConfigStat_t startPcmPcmChan (chanInfo_t *pChan, GpakSerialPort_t port, int slot, int frameA, int frameB) {

   pcm2pkt_t *ASide;
   pkt2pcm_t *BSide;
   int PcmAvailable, AECAvailable;      // number of Echo Cancellers available
   int tones;

   if (!(sysConfig.cfgFramesChans & PCM2PCM_CFG_BIT))
      return Cc_InvalidChannelType;
   
   PcmAvailable = (int) (sysConfig.numPcmEcans  - NumPcmEcansUsed);
   if (frameA == frameB)
      AECAvailable = (int) (sysConfig.AECInstances - NumAECEcansUsed);
   else
      AECAvailable = 0;

   if (pChan->channelType != inactive) {
      DeactivateChannel (pChan);
      tearDownChannel   (pChan);
   }

   ASide = &(pChan->pcmToPkt);
   BSide = &(pChan->pktToPcm);

   pChan->channelType = pcmToPcm;
   pChan->VoiceChannel = Enabled;

   ASide->Coding = NullCodec;       BSide->Coding = NullCodec;
   ASide->InSerialPortId  = port;   ASide->InSlotId  = slot;
   ASide->OutSerialPortId = port;   ASide->OutSlotId_MuxFact = slot + 1;

   BSide->InSerialPortId  = port;   BSide->InSlotId_MuxFact  = slot + 1;
   BSide->OutSerialPortId = port;   BSide->OutSlotId = slot;

   ASide->SamplesPerFrame = frameA;
   BSide->SamplesPerFrame = frameB;

   ASide->EchoCancel    = 1 <= PcmAvailable ? Enabled : Disabled;
   BSide->EchoCancel    = 2 <= PcmAvailable ? Enabled : Disabled;
   ASide->AECEchoCancel = 1 <= AECAvailable ? Enabled : Disabled;
   BSide->AECEchoCancel = 2 <= AECAvailable ? Enabled : Disabled;
   ASide->AGC           = 1 <= numAGCChansAvail ? Enabled : Disabled;
   BSide->AGC           = 2 <= numAGCChansAvail ? Enabled : Disabled;

   tones =  (SysAlgs.dtmfEnable     ? DTMF_tone : 0)     |
            (SysAlgs.cprgEnable     ? CallProg_tone : 0) |
            (SysAlgs.mfr1Enable     ? MFR1_tone : 0)     |
            (SysAlgs.mfr2FwdEnable  ? MFR2Fwd_tone : 0)  |
            (SysAlgs.mfr2BackEnable ? MFR2Back_tone : 0) |
            (CedDetAvail != 0          ? CED_tone : 0)   |
            (CngDetAvail != 0          ? CNG_tone : 0);
   
   ASide->toneTypes = 1 <= numToneGenChansAvail ? Tone_Generate : 0;
   BSide->toneTypes = 2 <= numToneGenChansAvail ? Tone_Generate : 0;
   ASide->toneTypes |= tones;
   BSide->toneTypes |= tones;
   
   ASide->Noise.Bufr =  SysAlgs.noiseSuppressEnable ?  NoiseSuppressInstance [pChan->ChannelId][1] : NULL;
   BSide->Noise.Bufr =  SysAlgs.noiseSuppressEnable ?  NoiseSuppressInstance [pChan->ChannelId][1] : NULL;

   return setupChannel (pChan);
}
//-----------------------------------------------------------------------
//
// startPcmPcmChannels
//
//  This routine starts 'chnCnt' PCM to PCM channels on the first available
//  McBSP timeslots.  Channels are allocated with all features (echo 
//  cancellation, tone detection, etc.) that are available at run time.
//
static void startPcmPcmChannels (int chnCnt) {
   int frameA, frameB, channel, slots, i;
   GpakSerialPort_t port;
   GPAK_ChannelConfigStat_t stat;
   channel = 0;

   if (diag.countDown) return;
   if (chnCnt <= 0) return;

   frameSizes (&frameA, &frameB);
   
   for (port=SerialPort1; port <= SerialPort3; port++) {
      slots = sysConfig.maxSlotsSupported[port];
      if (slots <= 0) continue;
      for (i = 0; i < slots; i = i+2) {
          if (chnCnt <= 0) return;

          channel = nextChannel (channel);
          if (channel == -1) return;   // No more channels available - exit

          stat = startPcmPcmChan (chanTable[channel], port, i, frameA, frameB);
          if (stat != Cc_Success) continue;  // Channel failure, try next slot
          
          chnCnt--;
      }     
   }  
}



//}-----------------------------------------------------------------------
//
//{ ---- PCM to PKT channel setup
//
// pcmPktLoopBack.   Loops pcm to packet channel back pnto itself.

ADT_UInt16 pcmPktLoopBack (int chanId) {

   chanInfo_t *pChan;
   unsigned int mask;

   pChan  = chanTable[chanId];
   if ((pChan->channelType != pcmToPacket) && (pChan->channelType != circuitData))
      return Tm_InvalidTest;

   printf ("Packet loopback on channel %d\n", chanId);
   LogTransfer (0x40000000,  pChan->pktToPcm.inbuffer, 0, pChan->pcmToPkt.outbuffer, 0, 
                            pChan->pktToPcm.inbuffer->BufrSize);

   mask = HWI_disable ();
   pChan->pktToPcm.inbuffer = pChan->pcmToPkt.outbuffer;
   pChan->pktToPcm.inbuffer->PutIndex = pChan->pktToPcm.inbuffer->TakeIndex;
   pChan->PairedChannelId = PKT_LOOP_BACK;
   HWI_restore (mask);

   return Tm_Success;
}

//-----------------------------------------------------------------------
//
// pcmPktcrossover.  Pairs even and odd channels for internal crossover
//
ADT_UInt16 pcmPktCrossover (int chanId) {

   chanInfo_t *pChan1, *pChan2;
   int chan1, chan2;
   unsigned int mask;
   
   chan1 = chanId & 0xfe;
   chan2 = chan1 + 1;

   pChan1 = chanTable[chan1];
   if (pChan1->channelType == inactive) 
      return Tm_Success;
      
   pChan2 = chanTable[chan2];
   if (pChan2->channelType == inactive) 
      return Tm_Success;

   if ((pChan1->channelType != pcmToPacket) || (pChan2->channelType != pcmToPacket))
      return Tm_InvalidTest;

   if (pChan1->PairedChannelId == PKT_LOOP_BACK)
      return Tm_Success;

   pChan1->PairedChannelId = PKT_LOOP_BACK;
   pChan2->PairedChannelId = PKT_LOOP_BACK;

   printf ("Pcm to pkt crossover on channels %d %d\n", pChan1->ChannelId, pChan2->ChannelId);
   LogTransfer (0x40000000,  pChan1->pktToPcm.inbuffer, 0, pChan2->pcmToPkt.outbuffer, 0, 
                         pChan1->pktToPcm.inbuffer->BufrSize);
   LogTransfer (0x40000000,  pChan2->pktToPcm.inbuffer, 0, pChan1->pcmToPkt.outbuffer, 0, 
                         pChan2->pktToPcm.inbuffer->BufrSize);

   mask = HWI_disable ();
   pChan1->pktToPcm.inbuffer = pChan2->pcmToPkt.outbuffer;
   pChan2->pktToPcm.inbuffer = pChan1->pcmToPkt.outbuffer;

   pChan1->pktToPcm.inbuffer->PutIndex = pChan1->pktToPcm.inbuffer->TakeIndex;
   pChan2->pktToPcm.inbuffer->PutIndex = pChan2->pktToPcm.inbuffer->TakeIndex;
   pChan1->PairedChannelId = PKT_LOOP_BACK;
   pChan2->PairedChannelId = PKT_LOOP_BACK;
   HWI_restore (mask);
   return Tm_Success;
}

//-----------------------------------------------------------------------
//
//  startPcmPktChan
//
//    starts a channel PCM to PKT channel with all resources
//    (echo canceller, AGC, ...) that are available at run time on
//    the specified time slot.  The channel is placed in packet loop back mode.
//    according to diag.loopbackMode
//
static GPAK_ChannelConfigStat_t startPcmPktChan (chanInfo_t *pChan, GpakCodecs Codec,
                     GpakSerialPort_t port, int slot, int frameSize) {

   pcm2pkt_t *ASide;
   pkt2pcm_t *BSide;
   GPAK_ChannelConfigStat_t status;
   int PcmAvailable, PktAvailable, AECAvailable;      // number of Echo Cancellers available

   if (!(sysConfig.cfgFramesChans & PCM2PKT_CFG_BIT))
      return Cc_InvalidChannelType;

   PcmAvailable = (int) (sysConfig.numPcmEcans  - NumPcmEcansUsed);
   PktAvailable = (int) (sysConfig.numPktEcans  - NumPktEcansUsed);
   AECAvailable = (int) (sysConfig.AECInstances - NumAECEcansUsed);

   ASide = &(pChan->pcmToPkt);
   BSide = &(pChan->pktToPcm);

   if (pChan->channelType != inactive) {
      DeactivateChannel (pChan);
      tearDownChannel   (pChan);
   }

   pChan->channelType = pcmToPacket;
   pChan->VoiceChannel = Enabled;
   pChan->fax.faxMode  = (numFaxChansUsed < sysConfig.numFaxChans) ? faxVoice : disabled;
   pChan->fax.faxTransport = faxRtp;

   ASide->Coding = Codec;
   ASide->InSerialPortId  = port;            ASide->InSlotId = slot;
   ASide->OutSerialPortId = SerialPortNull;  ASide->OutSlotId_MuxFact = 0;

   BSide->Coding = Codec;
   BSide->InSerialPortId  = SerialPortNull;  BSide->InSlotId_MuxFact = 0;
   BSide->OutSerialPortId = port;            BSide->OutSlotId = slot;

   ASide->SamplesPerFrame = codecToSamplesPerFrame (Codec, frameSize);
   BSide->SamplesPerFrame = codecToSamplesPerFrame (Codec, frameSize);

   ASide->EchoCancel    = 1 <= PcmAvailable ? Enabled : Disabled;
   BSide->EchoCancel    = 1 <= PktAvailable ? Enabled : Disabled;
   ASide->AECEchoCancel = 1 <= AECAvailable ? Enabled : Disabled;
   BSide->AECEchoCancel = 2 <= AECAvailable ? Enabled : Disabled;
   ASide->AGC           = 1 <= numAGCChansAvail ? Enabled : Disabled;
   BSide->AGC           = Disabled;
   ASide->VAD           = SelectVADMode (Codec);
   BSide->CNG           = SetVADMode (Codec);

   ASide->tonePktGet = 0;
   BSide->tonePktPut = 0;
   BSide->ForwardTonePkts = Disabled;
   BSide->ForwardCNGPkts = Disabled;

   ASide->toneTypes = (SysAlgs.dtmfEnable     ? DTMF_tone : 0)     |
                      (SysAlgs.cprgEnable     ? CallProg_tone : 0) |
                      (SysAlgs.mfr1Enable     ? MFR1_tone : 0)     |
                      (SysAlgs.mfr2FwdEnable  ? MFR2Fwd_tone : 0)  |
                      (SysAlgs.mfr2BackEnable ? MFR2Back_tone : 0) |
                      (CedDetAvail != 0          ? CED_tone : 0)   |
                      (CngDetAvail != 0          ? CNG_tone : 0);

   if (sysConfig.maxToneDetTypes)
      ASide->toneTypes &= ((1 << sysConfig.maxToneDetTypes) - 1);
   
   ASide->toneTypes |= (SysAlgs.toneRlyDetEnable ? Tone_Relay : 0);
      
   ASide->toneTypes |= (SysAlgs.toneRlyGenEnable ? Tone_Regen : 0);

   BSide->toneTypes = 1 <= numToneGenChansAvail ? Tone_Generate : 0;
   ASide->Noise.Bufr =  SysAlgs.noiseSuppressEnable ?  NoiseSuppressInstance [pChan->ChannelId][1] : NULL;
   BSide->Noise.Bufr =  SysAlgs.noiseSuppressEnable ?  NoiseSuppressInstance [pChan->ChannelId][1] : NULL;

   status = setupChannel (pChan);
   if (diag.loopbackMode == PacketCrossOver)
     pcmPktCrossover (pChan->ChannelId);
   else if (diag.loopbackMode ==  PacketLoopBack)
     pcmPktLoopBack (pChan->ChannelId);

   return status;
}

//-----------------------------------------------------------------------
//
//  startPcmPktChannels
//
//  This routine starts 'chnCnt' PCM to PKT channels on the first available
//  McBSP timeslots.  Channels are allocated with all features (echo 
//  cancellation, tone detection, etc.) that are available at run time.
//
static void startPcmPktChannels (int chnCnt, GpakCodecs selectedCodec) {
   int frameA, frameB, channel, slots, slot;
   GpakSerialPort_t port;
   GPAK_ChannelConfigStat_t stat;
   GpakCodecs codec;
   channel = 0;
   codec = CustomCodec;

   if (diag.countDown) return;
   if (chnCnt <= 0) return;

   frameSizes (&frameA, &frameB);
   
   for (port=SerialPort1; port <= SerialPort3; port++) {
      slots = sysConfig.numSlotsOnStream[port];
      if (slots <= 0) continue;
      for (slot = 0; slot < slots; slot++) {
          if (chnCnt <= 0) return;

          if (selectedCodec == NullCodec) 
             codec = nextCodec (codec, frameB);
          else {
             codec = selectedCodec;
             frameA = codecToSamplesPerFrame(codec, frameB);
          }
          channel = nextChannel (channel);
          if (channel == -1) return;   // No more channels available - exit

          stat = startPcmPktChan (chanTable[channel], codec, port, slot, frameB);
          if (stat != Cc_Success) continue;  // Channel failure, try next slot

          chnCnt--;
      }     
   }  
}


//}-----------------------------------------------------------------------
//
//{ ---  PKT to PKT channel setup
//
// --- pktPktLoopBack.  Performs internal crossover on two pcmPkt channels
//
ADT_UInt16 pktPktLoopBack (int chan1, int chan2) {
   chanInfo_t *pChan1, *pChan2;
   unsigned int mask;

   pChan1 = chanTable[chan1];
   pChan2 = chanTable[chan2];
   if ((pChan1->channelType != pcmToPacket) || (pChan2->channelType != pcmToPacket))
      return Tm_InvalidTest;

   printf ("Pkt to pkt crossover on channels %d %d\n", pChan1->ChannelId, pChan2->ChannelId);
   LogTransfer (0x40000000,  pChan1->pktToPcm.inbuffer, 0, pChan2->pcmToPkt.outbuffer, 0, 
                         pChan1->pktToPcm.inbuffer->BufrSize);
   LogTransfer (0x40000000,  pChan2->pktToPcm.inbuffer, 0, pChan1->pcmToPkt.outbuffer, 0, 
                         pChan2->pktToPcm.inbuffer->BufrSize);

   mask = HWI_disable ();
   pChan1->pktToPcm.inbuffer = pChan2->pcmToPkt.outbuffer;
   pChan2->pktToPcm.inbuffer = pChan1->pcmToPkt.outbuffer;

   pChan1->pktToPcm.inbuffer->PutIndex = pChan1->pktToPcm.inbuffer->TakeIndex;
   pChan2->pktToPcm.inbuffer->PutIndex = pChan2->pktToPcm.inbuffer->TakeIndex;
   pChan1->PairedChannelId = PKT_LOOP_BACK;
   pChan2->PairedChannelId = PKT_LOOP_BACK;
   HWI_restore (mask);

   return Tm_Success;
}


//=============================================================================
//
// --- pktPktChain.  Adds pktPkt channel as last link on chain of pkt channels
//
static ADT_UInt16 pktPktChain (int chan) {
   chanInfo_t *aChan, *bChan, *prvChan;
   unsigned int mask;

   prvChan = chanTable[chan-1];
   aChan = chanTable[chan];
   bChan = chanTable[chan+1];

   if (aChan->channelType != packetToPacket)
      return Tm_InvalidTest;

   if ((prvChan->channelType != pcmToPacket) && (prvChan->channelType != packetToPacket))
      return Tm_InvalidTest;


   printf ("Pkt to pkt chain on channel %d\n", aChan->ChannelId);
   LogTransfer (0x40000000,  aChan->pktToPcm.inbuffer, 0, prvChan->pcmToPkt.outbuffer, 0, 
                         aChan->pktToPcm.inbuffer->BufrSize);
   LogTransfer (0x40000000,  prvChan->pktToPcm.inbuffer, 0, aChan->pcmToPkt.outbuffer, 0, 
                         prvChan->pktToPcm.inbuffer->BufrSize);


   mask = HWI_disable ();
   aChan->pktToPcm.inbuffer   = prvChan->pcmToPkt.outbuffer;
   prvChan->pktToPcm.inbuffer = aChan->pcmToPkt.outbuffer;

   aChan->pktToPcm.inbuffer->PutIndex   = aChan->pktToPcm.inbuffer->TakeIndex;
   prvChan->pktToPcm.inbuffer->PutIndex = prvChan->pktToPcm.inbuffer->TakeIndex;
   aChan->PairedChannelId = PKT_LOOP_BACK;
   bChan->PairedChannelId = PKT_LOOP_BACK;
   HWI_restore (mask);

   return Tm_Success;
}


//{-----------------------------------------------------------------------
//
//  startPktPktChan
//
//    starts a channel PKT to PKT channel with all resources
//    (echo canceller, AGC, ...) that are available at run time on
//    the specified time slot.  The channel is placed in packet loop back mode.
//    according to diag.loopbackMode
//
//    NOTE: This sets up two half duplex channels that are chained together with the
//          previous channel.
//
//     Thus prev->out --> a->in   a->out --> b->in   b->out --> host
//}
static GPAK_ChannelConfigStat_t startPktPktChan (chanInfo_t *chanA, GpakCodecs ACodec,
                                  GpakCodecs BCodec, int AFrameSize, int BFrameSize) {

   chanInfo_t *chanB;
   pcm2pkt_t *ASideOut, *BSideOut;
   pkt2pcm_t *ASideIn,  *BSideIn;
   GPAK_ChannelConfigStat_t status;
   int PktAvailable;      // number of Echo Cancellers available

   if (!(sysConfig.cfgFramesChans & PCM2PKT_CFG_BIT))
      return Cc_InvalidChannelType;

   PktAvailable = (int) (sysConfig.numPktEcans  - NumPktEcansUsed);

   if (chanA->channelType != inactive) {
      DeactivateChannel (chanA);
      tearDownChannel   (chanA);
   }

   chanB = chanTable[chanA->ChannelId+1];
   if (chanB->channelType != inactive) {
      DeactivateChannel (chanB);
      tearDownChannel   (chanB);
   }
   ASideIn  = &(chanA->pktToPcm);   BSideOut = &(chanA->pcmToPkt);
   BSideIn  = &(chanB->pktToPcm);   ASideOut = &(chanB->pcmToPkt);

   channelAllocsNull (chanA, ASideOut, BSideIn);
   channelAllocsNull (chanB, BSideOut, ASideIn);

   chanA->channelType  = chanB->channelType  = packetToPacket;
   chanA->VoiceChannel = chanB->VoiceChannel = Enabled;
   chanA->fax.faxMode  = chanB->fax.faxMode  = disabled;

   chanA->PairedChannelId = chanB->ChannelId; 
   chanB->PairedChannelId = chanA->ChannelId;


   ASideIn->Coding = ASideOut->Coding = ACodec;
   BSideIn->Coding = BSideOut->Coding = BCodec;
   ASideIn->SamplesPerFrame = ASideOut->SamplesPerFrame = codecToSamplesPerFrame (ACodec, AFrameSize);
   BSideIn->SamplesPerFrame = BSideOut->SamplesPerFrame = codecToSamplesPerFrame (BCodec, BFrameSize);

   // Setup parameters for A side of call.
   ASideIn->InSerialPortId    = ASideOut->InSerialPortId   = SerialPortNull;
   ASideIn->InSlotId_MuxFact  = ASideOut->InSlotId = 0;
   ASideIn->OutSerialPortId   = ASideOut->OutSerialPortId  = SerialPortNull;
   ASideIn->OutSlotId         = ASideOut->OutSlotId_MuxFact = 0;

   ASideOut->EchoCancel   = 1 <= PktAvailable ? Enabled : Disabled;
   ASideOut->VAD          = SelectVADMode (ACodec);
   ASideIn->CNG           = SetVADMode (ACodec);

   ASideOut->tonePktGet = 0;
   ASideIn->tonePktPut = 0;

   ASideOut->toneTypes = (SysAlgs.dtmfEnable     ? DTMF_tone : 0)     |
                         (SysAlgs.cprgEnable     ? CallProg_tone : 0) |
                         (SysAlgs.mfr1Enable     ? MFR1_tone : 0)     |
                         (SysAlgs.mfr2FwdEnable  ? MFR2Fwd_tone : 0)  |
                         (SysAlgs.mfr2BackEnable ? MFR2Back_tone : 0) |
                         (CedDetAvail != 0          ? CED_tone : 0)   |
                         (CngDetAvail != 0          ? CNG_tone : 0);

   if (sysConfig.maxToneDetTypes)
      ASideOut->toneTypes &= ((1 << sysConfig.maxToneDetTypes) - 1);
   
   ASideOut->toneTypes |= (SysAlgs.toneRlyDetEnable ? Tone_Relay : 0);
   ASideIn->toneTypes  |= (SysAlgs.toneRlyGenEnable ? Tone_Regen : 0);

   ASideIn->toneTypes = 1 <= numToneGenChansAvail ? Tone_Generate : 0;
   ASideIn->Noise.Bufr  =  SysAlgs.noiseSuppressEnable ?  NoiseSuppressInstance [chanA->ChannelId][1] : NULL;
   ASideOut->Noise.Bufr =  SysAlgs.noiseSuppressEnable ?  NoiseSuppressInstance [chanA->ChannelId][1] : NULL;

   // Setup parameters for B side of call.
   BSideIn->InSerialPortId    = BSideOut->InSerialPortId   = SerialPortNull;
   BSideIn->InSlotId_MuxFact  = BSideOut->InSlotId = 0;
   BSideIn->OutSerialPortId   = BSideOut->OutSerialPortId  = SerialPortNull;
   BSideIn->OutSlotId         = BSideOut->OutSlotId_MuxFact = 0;

   BSideOut->EchoCancel     = 2 <= PktAvailable ? Enabled : Disabled;
   BSideOut->VAD            = SelectVADMode (BCodec);
   BSideIn->CNG             = SetVADMode (BCodec);

   BSideOut->tonePktGet = 0;
   BSideIn->tonePktPut = 0;

   BSideOut->toneTypes = (SysAlgs.dtmfEnable     ? DTMF_tone : 0)     |
                         (SysAlgs.cprgEnable     ? CallProg_tone : 0) |
                         (SysAlgs.mfr1Enable     ? MFR1_tone : 0)     |
                         (SysAlgs.mfr2FwdEnable  ? MFR2Fwd_tone : 0)  |
                         (SysAlgs.mfr2BackEnable ? MFR2Back_tone : 0) |
                         (CedDetAvail != 0          ? CED_tone : 0)   |
                         (CngDetAvail != 0          ? CNG_tone : 0);

   if (sysConfig.maxToneDetTypes)
      BSideOut->toneTypes &= ((1 << sysConfig.maxToneDetTypes) - 1);
   
   BSideOut->toneTypes |= (SysAlgs.toneRlyDetEnable ? Tone_Relay : 0);
   BSideIn ->toneTypes |= (SysAlgs.toneRlyGenEnable ? Tone_Regen : 0);

   BSideIn->toneTypes = 2 <= numToneGenChansAvail ? Tone_Generate : 0;
   BSideIn->Noise.Bufr  =  SysAlgs.noiseSuppressEnable ?  NoiseSuppressInstance [chanB->ChannelId][1] : NULL;
   BSideOut->Noise.Bufr =  SysAlgs.noiseSuppressEnable ?  NoiseSuppressInstance [chanB->ChannelId][1] : NULL;


   status = setupChannel (chanA);

   if (diag.loopbackMode == PacketChain)
      pktPktChain (chanA->ChannelId);
   else if (diag.loopbackMode ==  PacketLoopBack)
      pktPktLoopBack (chanA->ChannelId, chanB->ChannelId);


   return status;
}



//{------------------------------------------------------
// Full duplex channels are set up in pairs.  The first of
// the paired channels converts between PMU_64 and the selected
// codec (diag.codec).  The second channel converts the selected
// codec back to PMU_64.
//
// Each full duplex channel is then chained to its preceeding
// channel.  The last channel in the chain is loop backed onto itself.
//
// It is expected that a single TDM-PKT(PCMU_64) channel has already
// been set up to act as the first channel in the chain.
//
//    TDM                  TDM    | 
//     |                    ^     | Initial link of TDM-PKT channel
//     v       chan 0       |     |
//    G.711                G.711    Linkage point
//     |                    ^     |
//     v       chan 1       |     |
//    codec                codec  |  First pair of G.711 - codec channel
//     |                    ^     |
//     v       chan 2       |     |
//    G.711                G.711     Linkage point
//     |                    ^     |
//     v       chan 3       |     |
//    codec                codec  |  Second pair of G.711 - codec channel
//     |                    ^     |
//     v       chan 4       |     |
//    G.711                G.711  |
//     |                    ^
//}     |____________________|        Loopback point

static void startPktPktChain (int pairCnt) {

   int i;
   chanInfo_t *chan;

   SetLoopbackMode (PacketChain);
   for (i=0; i<sysConfig.maxNumChannels; i++)
      if (chanTable[i]->channelType == inactive) break;

   while (pairCnt--) {
      chan = chanTable[i];
      startPktPktChan (chan, PCMU_64, diag.codec, 80, 80);
      i+=2;

      chan = chanTable[i];
      startPktPktChan (chan, diag.codec, PCMU_64, 80, 80);
      i+=2;
   }

   chan = chanTable[i-1];
   chan->pktToPcm.inbuffer = chan->pcmToPkt.outbuffer;
}

//------------------------------------------------------
// Sets up full duplex packet channels that converts from G.711 
// to selected codec (diag.codec).
//
void startPktPktChannels (int pktCnt) {

   int i;
   chanInfo_t *chan;

   SetLoopbackMode ((GpakTestMode) 0);
   for (i=0; i<sysConfig.maxNumChannels; i++)
      if (chanTable[i]->channelType == inactive) break;

   while (pktCnt--) {
      chan = chanTable[i];
      startPktPktChan (chan, PCMU_64, diag.codec, 80, 80);
      i+=2;
   }
}

//}-----------------------------------------------------------------------
//
//{ ---  PCM to CNFR channel setup
//
//  startPcmCnfrChan
//
//    starts a channel PCM to conference channel with all resources
//    (echo canceller, AGC, ...) that are available at run time on
//    the specified time slot
//
static GPAK_ChannelConfigStat_t startPcmCnfrChan (int channel, 
                         GpakSerialPort_t port, int slot, int CnfrId) {

   int ConfFrameSize;
   chanInfo_t *pChan = chanTable [channel];
   pcm2pkt_t *PcmPkt = &(pChan->pcmToPkt);
   pkt2pcm_t *PktPcm = &(pChan->pktToPcm);

   if (sysConfig.maxNumConferences <= CnfrId || sysConfig.maxNumConferences == 0)
      return (Cc_InvalidConference);
      
   ConfFrameSize = ConferenceInfo[CnfrId]->FrameSize;
   if (ConfFrameSize == 0)
      return Cc_ConfNotConfigured;
      
   if (sysConfig.maxNumConfChans <= ConferenceInfo[CnfrId]->NumMembers || sysConfig.maxNumConfChans == 0)
      return Cc_ConferenceFull;

   PktPcm->EchoCancel = Disabled;
   
   pChan->PairedChannelId = CnfrId;
   pChan->channelType = conferencePcm;
   PcmPkt->SamplesPerFrame = ConferenceInfo[CnfrId]->FrameSize;
   PktPcm->SamplesPerFrame = ConferenceInfo[CnfrId]->FrameSize;

   PcmPkt->Coding = NullCodec;
   PcmPkt->InSerialPortId  = port;
   PcmPkt->InSlotId = slot;
   PcmPkt->OutSerialPortId = SerialPortNull;

   PktPcm->Coding = NullCodec;
   PktPcm->InSerialPortId = SerialPortNull;
   PktPcm->OutSerialPortId = port;
   PktPcm->OutSlotId =  slot;

   if (numAGCChansAvail < 2) {
      PcmPkt->AGC = Disabled;
      PktPcm->AGC = Disabled;
   } else {
       PcmPkt->AGC = Enabled;
       PktPcm->AGC = Enabled;
   }


   PktPcm->AECEchoCancel = Disabled;
   PktPcm->EchoCancel    = Disabled;

   // Determine Echo Canceller needs and availability.
   if (sysConfig.numPcmEcans <= NumPcmEcansUsed)
      PcmPkt->EchoCancel = Disabled;
   else
      PcmPkt->EchoCancel = Enabled;

   pChan->VoiceChannel = Enabled;

   PcmPkt->toneTypes = Null_tone;
   PktPcm->toneTypes = Null_tone;

   // Return with an indication of success or failure.
   return setupChannel (pChan);
}


//------------------------------------------------------
//
// start pcm to conference channels. Distributes across conferences.
//
static void startPcmCnfrChannels (int chnCnt) {
   int channel, slots, i;
   GpakSerialPort_t port;
   GPAK_ChannelConfigStat_t stat;
   channel = 0;

   if (diag.countDown) return;
   if (chnCnt <= 0) return;

   
   for (port=SerialPort1; port <= SerialPort3; port++) {
      slots = sysConfig.maxSlotsSupported[port];
      if (slots <= 0) continue;


      for (i = 0; i < slots; i = i+2) {
          if (chnCnt <= 0) return;

          channel = nextChannel (channel);
          if (channel == -1) return;   // No more channels available - exit

          stat = startPcmCnfrChan (channel, port, i, diag.cnfrID);
          if (stat != Cc_Success) continue;  // Channel failure, try next slot
          
          chnCnt--;
      }     
   }  
}


//}-----------------------------------------------------------------------
//
//{ ---  PKT to CNFR channel setup
//
//  startPktCnfrChan
//
//    starts a channel Packet to conference channel with all resources
//    (echo canceller, AGC, ...) that are available at run time on
//    the specified time slot
//
static GPAK_ChannelConfigStat_t startPktCnfrChan (int channel, GpakCodecs selectedCodec, int CnfrId) {

   int ConfFrameSize;
   chanInfo_t *pChan = chanTable [channel];
   pcm2pkt_t *PcmPkt = &(pChan->pcmToPkt);
   pkt2pcm_t *PktPcm = &(pChan->pktToPcm);

   if (sysConfig.maxNumConferences <= CnfrId || sysConfig.maxNumConferences == 0)
      return (Cc_InvalidConference);
      
   ConfFrameSize = ConferenceInfo[CnfrId]->FrameSize;
   if (ConfFrameSize == 0)
      return Cc_ConfNotConfigured;
      
   if (sysConfig.maxNumConfChans <= ConferenceInfo[CnfrId]->NumMembers || sysConfig.maxNumConfChans == 0)
      return Cc_ConferenceFull;

   channelAllocsNull (pChan, PcmPkt, PktPcm);

   
   pChan->PairedChannelId = CnfrId;
   pChan->channelType = conferencePacket;
   PcmPkt->SamplesPerFrame = ConferenceInfo[CnfrId]->FrameSize;
   PktPcm->SamplesPerFrame = ConferenceInfo[CnfrId]->FrameSize;

   PcmPkt->Coding = selectedCodec;
   PcmPkt->InSerialPortId  = PcmPkt->OutSerialPortId = SerialPortNull;
   PcmPkt->InSlotId = 0;

   PktPcm->Coding = selectedCodec;
   PktPcm->InSerialPortId =  PktPcm->OutSerialPortId = SerialPortNull;
   PktPcm->OutSlotId = 0;

   PktPcm->ForwardTonePkts = Disabled;
   PktPcm->ForwardCNGPkts = Disabled;

   PcmPkt->VAD = SelectVADMode (PcmPkt->Coding);

   if (numAGCChansAvail < 2) {
      PcmPkt->AGC = Disabled;
      PktPcm->AGC = Disabled;
   } else {
       PcmPkt->AGC = Enabled;
       PktPcm->AGC = Enabled;
   }


   PktPcm->AECEchoCancel = Disabled;
   PktPcm->EchoCancel    = Disabled;

   // Determine Echo Canceller needs and availability.
   if (sysConfig.numPcmEcans <= NumPcmEcansUsed)
      PcmPkt->EchoCancel = Disabled;
   else
      PcmPkt->EchoCancel = Enabled;

   pChan->VoiceChannel = Enabled;

   PcmPkt->toneTypes = ConferenceInfo[pChan->PairedChannelId]->InputToneTypes;
   if (0 < numToneGenChansAvail) PcmPkt->toneTypes |= Tone_Generate;

   PktPcm->toneTypes = Null_tone;

   // Return with an indication of success or failure.
   return setupChannel (pChan);
}


//------------------------------------------------------
// Sets up packet to conference channels for selected codec (diag.codec).
// Distribute across all conferences.
static void startPktCnfrChannels (int chnCnt) {

   int i, j, frameSize, cnfrId;
   chanInfo_t *chan;

   cnfrId = 0;
   
   frameSize = codecToSamplesPerFrame (diag.codec, diag.cnfrFrameSize);
 
   SetLoopbackMode ((GpakTestMode) (0));
   for (i=0; i<sysConfig.maxNumChannels && chnCnt; i++) {
      chan = chanTable[i];
      if (chan->channelType != inactive) continue;

      // Attempt to place channels in specified conference
      cnfrId = diag.cnfrID;
      for (j=0; j < sysConfig.maxNumConferences; j++, cnfrId++) {
         if (sysConfig.maxNumConferences <= cnfrId)
            cnfrId = 0;

         if (ConferenceInfo[cnfrId]->FrameSize == frameSize) break;
      }
      if (startPktCnfrChan (i, diag.codec, cnfrId) != Cc_Success) continue;
      chnCnt--;
   }
}
//}-----------------------------------------------------------------------
//
//{ ---  Channel tear down
void disableLoopBack (chanInfo_t *pChan) {
   int chanId;
   unsigned int mask;

   chanId = pChan->ChannelId;
   
   if ((pChan->channelType != pcmToPacket) && (pChan->channelType != packetToPacket)) 
      return;

   if (pChan->PairedChannelId == PKT_LOOP_BACK)
      printf ("Loopback disabled on %d\n", chanId); 
   mask = HWI_disable ();
   pChan->pktToPcm.inbuffer = &(pktBuffStructTable[chanId*2]);
   pChan->pktToPcm.inbuffer->PutIndex = pChan->pktToPcm.inbuffer->TakeIndex;
   HWI_restore (mask);
   return;
}

//-----------------------------------------------------------------------
//
//  disableAllChannels
//
//    Disables all active channels.
//
static void disableAllChannels () {
   int channel;
   chanInfo_t *pChan;

   if (diag.countDown) return;

   for (channel = 0; channel < sysConfig.maxNumChannels; channel++) {
      pChan = chanTable[channel];
      if (pChan->channelType == inactive) continue;
      DeactivateChannel (pChan);   // Remove from TDM 
      tearDownChannel   (pChan);   // Release instance data
   }
}


//}========================================================================
//
//{ ---  Conference setup
//========================================================================
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// startConference - Configure Conference
//
//
static GPAK_ConferConfigStat_t SetupConferences (int CnfrCnt, int FrameSize) {

   ConfParams_t ConfParms;     // conference parameters
   ConfScratch_t *lclConfScratch; 
   ConferenceInfo_t *cnfrInfo;
   int i;
   TGInfo_t *pInfo;

   // Verify the frame size is valid.
   if (!ValidFrameSize(FrameSize))
      return (Cf_InvalidFrameSize);

   // Verify the Conference Id is valid.
   for (i=0; i<sysConfig.maxNumConferences && CnfrCnt != 0; i++) {
      // Verify the conference is inactive.
      if ((ConferenceInfo[i]->MemberList != NULL) ||
         (ConferenceInfo[i]->CompositeList != NULL))
          continue;

      // Setup and initialize the conference.
      ConferenceInfo[i]->InputToneTypes = Null_tone;
      ConferenceInfo[i]->FrameSize = (GpakFrameSizes) FrameSize;

      ConfParms.ConfType = CONF_TYPE_8K_ONLY; 
      ConfParms.MaxConferenceChannels = sysConfig.maxNumConfChans;
      ConfParms.FrameSizeMSecQ4 = ((FrameSize + sysConfig.samplesPerMs - 1) * 16)/sysConfig.samplesPerMs;
      ConfParms.DominantN = sysConfig.numConfDominant;

      ConfParms.MaxNoiseSuppression = sysConfig.maxConfNoiseSuppress;
#if (DSP_TYPE == 64)
      ConfParms.RunVADExternally    = 0;
#endif
      ConfParms.VADNoiseFloorThresh = sysConfig.vadNoiseFloorZThresh;
      ConfParms.VADHangMSec         = sysConfig.vadHangOverTime;
      ConfParms.VADWindowSizeMSec   = sysConfig.vadWindowMs;
   
      ConfParms.AGCTargetPowerIn    = sysConfig.agcTargetPower;
      ConfParms.AGCMaxLossLimitIn   = sysConfig.agcMinGain;
      ConfParms.AGCMaxGainLimitIn   = sysConfig.agcMaxGain;
      ConfParms.AGCLowSigThreshdBm  = sysConfig.agcLowSigThreshDBM;

      // Initialize the Conference Tone Generator Info Structure
      pInfo = &(ConferenceInfo[i]->TG_NB);
      pInfo->index    = i;
      pInfo->cmd      = ToneGenStop; 
      pInfo->active   = Disabled;    
      pInfo->update   = Disabled;    
      pInfo->toneGenPtr     = ConfToneGenInstance[i];
      pInfo->toneGenParmPtr = ConfToneGenParams[i];

      pInfo = &(ConferenceInfo[i]->TG_WB);
      pInfo->toneGenPtr = NULL;
     
      // Determine the scratch memory to use based on how scratch memory was
      // allocated, either by Frame Rate or by Conference.
      lclConfScratch = (ConfScratch_t *) getConfScratch (FrameSize);
      AppErr ("Conference scratch", lclConfScratch == NULL);

      cnfrInfo = (ConferenceInfo_t *) ConferenceInfo[i];

      CONF_ADT_init (&ConfParms, cnfrInfo->Inst, lclConfScratch, cnfrInfo->AgcInstance);
      CnfrCnt--;
   }
   // Return with an indication of success.
   return (Cf_Success);
}


//}========================================================================
//
//{  --- Serial port setup 
//
//========================================================================





//}========================================================================
//
//{  --- Main diagnostics loop
//
// called within RTDX software interrupt to perform TDM port and channel setup
//
static void stdDiagnostics () {
  
   if (diag.countDown) {
      if (--diag.countDown == 0) {
         StartGpakPcm  (&sysConfig.maxSlotsSupported[0]);
      }
      return;
   }
   
   if (diag.cnfrCnt) {
      SetupConferences (diag.cnfrCnt, diag.cnfrFrameSize);
      diag.cnfrCnt = 0;
   }

   if (diag.pcmPcmChannels) {
      startPcmPcmChannels (diag.pcmPcmChannels);
      diag.pcmPcmChannels = 0;
   }

   if (diag.pcmPktChannels) {
      startPcmPktChannels (diag.pcmPktChannels, diag.codec);
      diag.pcmPktChannels = 0;
   }

   if (diag.pcmCnfChannels) {
      startPcmCnfrChannels (diag.pcmCnfChannels);  
      diag.pcmCnfChannels = 0;
   }

   if (diag.pktCnfChannels) {
      startPktCnfrChannels (diag.pktCnfChannels);  
      diag.pktCnfChannels = 0;
   }

   if (diag.pktPktChannels) {
      startPktPktChain (diag.pktPktChannels / 2);  
      diag.pktPktChannels = 0;
   }

   if (diag.disableAll) {
      disableAllChannels ();
      diag.disableAll = 0;
   }

}
//}
#endif // end of RTDX



//============================================================================
//--------------------------- DATA CAPTURING -----------------------------
//  echo canceller, PCM data, and debug text captures
//========================================================================
struct gpakDbg_t {
   int errorCnt;
} dbg = {0};

#ifndef _DEBUG
   void CaptureText (char *s) { return; }
#else

struct textLog {
   int logLen;
   char *capBuff;
} textLog = { 0, (void *) NULL };
char *startCapBuff = NULL;
int capBuffI8 = 0;

void CaptureText (char *s) {
   int len; 
   if(textLog.capBuff == NULL) return;
   len = strlen (s); 
   if (capBuffI8 < textLog.logLen + len) return;   //if (0x400000 < textLog.logLen) return;

   textLog.logLen += len;

   memcpy (textLog.capBuff, s, len);
   textLog.capBuff+=len;
   *textLog.capBuff = 0;

}

void *TextLogReset () {
   void *end;

   end = (void *)startCapBuff;
   textLog.capBuff = (void *)startCapBuff;
   textLog.logLen = 0;

   memset (textLog.capBuff, ' ', capBuffI8);
   return end;
}

int TextCaptureI8 () {
   return textLog.logLen;
}
#endif



//============================================================================
// ---------------------- Debugging Aides ------------------------------------

//---------------------------  LOGGING ----------------
//   Data, time and echo canceller
//========================================================================
extern int DSPCore;
extern void updateBootProg (ADT_UInt32 type);
extern void dataLogInit (void *dataBuff, int dataBuffI8);

#undef logTime
#undef logTransfer
#undef searchDataBuffer

volatile int stopOnError = TRUE;
volatile int skipErrors = FALSE;
#pragma DATA_SECTION (stopOnError, "SHARED_DATA_SECT:logging")
#pragma DATA_SECTION (skipErrors,  "SHARED_DATA_SECT:logging")

extern coreLock dataLock;
coreLock *pdataLock = &dataLock;
//#pragma DATA_SECTION (dataLock, "SHARED_LOCK_SECT") 
//#pragma DATA_ALIGN   (dataLock, 64) 

int dataFilter = 0x800f0000;
#pragma DATA_SECTION (dataFilter,  "SHARED_DATA_SECT:logging")
//{ Data logging filters
//  0x8000 0000  Errors
//  0x4000 0000  Buffer loopbacks
//  0x2000 0000  Start/end frame info
//  0x1000 0000  Enqueue/dequeue channels

//  0x0010 0000  MIPs overrun adjustment
//  0x0008 0000  Buffer initialization
//  0x0004 0000  Packet transfers
//  0x0002 0000  Framing buffer transfers
//} 0x0001 0000  TDM buffer transfers

#ifndef _DEBUG  // Data logging
   // Satisfy references when using debugging components with non-debugging library
   #pragma CODE_SECTION (searchDataBuffer, "SLOW_PROG_SECT")
   void searchDataBuffer () { return; }
   #pragma CODE_SECTION (logTransfer, "FAST_PROG_SECT")
   int logTransfer (ADT_Int32 type, void *srcC, void *src, void *destC, void *dest, int length) { return 0; }
   #pragma CODE_SECTION (dataLogInit, "SLOW_PROG_SECT")
   void dataLogInit (void *buff, int buffI8) { return; }
#else

#ifndef DBG_LOGDATA_LEN
   #define DBG_LOGDATA_LEN (2<<6)
#endif

typedef struct dlStruct {
  ADT_UInt32 RTPTime;
  ADT_Int32  type;
  void *srcC, *src, *destC, *dest;
  ADT_UInt16 length;
  ADT_UInt16 fill;
  ADT_UInt16 value1, value2;
  ADT_UInt16 srcSlips, destSlips;
} dlDetails;
dlDetails DataDetails[DBG_LOGDATA_LEN + 3];
#pragma DATA_SECTION (DataDetails,  "NON_CACHED_DATA:logging")

typedef struct dataLogCtrl {
   ADT_UInt32   magic;
   int          Core;
   int          MaxDump;
   void         *dumpBuffer;
   void         *logBuffer;
   unsigned int Idx;
   int          Instances;
   int          Entries;
   ADT_Bool     Disable;
   ADT_Bool     StopAtEnd;
   dlDetails *Data;
} dataLogCtrl;
volatile dataLogCtrl dl = { 0 };
#pragma DATA_SECTION (dl,  "SHARED_DATA_SECT:logging")


// Dump the data buffer entries that match the last entry
#pragma CODE_SECTION (searchDataBuffer, "SLOW_PROG_SECT")
void searchDataBuffer () {
   int idx, cnt, dmpCnt;
   dlDetails *entry;

   CircBufInfo_t *circ;

   if (dl.dumpBuffer == NULL || DSPCore != dl.Core || dl.magic != 0x234432) return;

   // Start dump at wrap around position 
   if (dl.Idx < dl.Instances) idx = 0;
   else                       idx = (dl.Idx + 1) - dl.Instances;
   cnt = 0;
   dmpCnt=0;
   printf ("\n                   ------- Src ------  --------  Dst -------\n");
   printf ("   Time     Filter    Circ      Buff      Circ/Free  Buff/Used Size\n");
   
   do {
      entry = &dl.Data[idx % dl.Instances];
      idx++;
      cnt++;
      if ( (entry->destC != dl.dumpBuffer) && (entry->srcC != dl.dumpBuffer) && 
           !(entry->type & 0x8000000) ) continue;
      printf ("%8d  %8x  %8x  %8x  %8x   %8x   %4x (%d)\n",
              entry->RTPTime & 0xffffff, entry->type, entry->srcC,   entry->src,
              entry->destC,   entry->dest, entry->length, entry->length);

      if (dl.MaxDump < dmpCnt++) break;
   } while (idx < dl.Entries && cnt < dl.Instances);
   
   circ = (CircBufInfo_t *) dl.dumpBuffer;
   printf ("\nBuff: %x   Free: %u   Pending: %u\n", circ, getFreeSpace (circ), getAvailable (circ));
   dl.dumpBuffer = NULL;
}
//-------------------------------------------------------
//{ Data logging
// 
//   Type value     Event
//   111C           DMA to circular buffer
//   21Cl           Circular buffer to DMA
//
//   321C           Framing work task to circular buffer
//   32C1           Circular buffer to framing task work
//
//   24C1           Packet buffer to framing task
//   141C           Framing task to packet buffer
//
//   
//   1xxx  = TDM to packet direction
//   2xxx  = Packet to TDM direction
//   8xxx  = Auxilliary info
//
//   x1xx  = DMA transfers
//   x2xx  = DMA <-> Framing task transfers
//   x4xx  = Packet <-> Framing task transfers
//}
#pragma CODE_SECTION (dataLogInit, "SLOW_PROG_SECT")
void dataLogInit (void *dataBuff, int dataBuffI8) {
   //dataLock.Inst = 0;
   pdataLock->Inst = 0;
   dl.magic = 0x234432;
   dl.Idx   = 0;
   dl.Instances = dataBuffI8 / sizeof (dlDetails);
   dl.Entries = 0;
   dl.MaxDump = 40;
   dl.Disable = ADT_FALSE;
   dl.dumpBuffer = NULL;
   dl.logBuffer  = NULL;
   dl.StopAtEnd = ADT_FALSE;
   dl.Data = dataBuff;
}

#pragma CODE_SECTION (logTransfer, "MEDIUM_PROG_SECT")
int logTransfer (ADT_Int32 type, void *srcC, void *src, void *destC, void *dest, int length) {
   dlDetails *log;
   ADT_Bool   circXfer;
   ADT_UInt16 *data;
   int mask, swicnt;
   int idx;

   if (((dataFilter & type) == 0) || dl.Disable == ADT_TRUE) 
      return 0;

   // Track entries to a single circular buffer
   if ((dl.logBuffer != 0) && (dl.logBuffer != srcC) && (dl.logBuffer != destC))
      return 0;

   // Stop logging when near end of buffer and address is being tracked
   if ((dl.logBuffer || dl.StopAtEnd) && (dl.Instances < (dl.Entries + 4))) {
      dl.Disable = TRUE;
      return 0;
   }
   //mask = ADDRESS_lock (&dataLock.Inst);
   mask = ADDRESS_lock (&pdataLock->Inst);
   idx = dl.Idx++;
   dl.Entries++;
   swicnt = ApiBlock.DmaSwiCnt & 0xffffff;
   //ADDRESS_unlock (&dataLock.Inst, mask);
   ADDRESS_unlock (&pdataLock->Inst, mask);
   
   idx = idx%dl.Instances;
   log  = &dl.Data[idx];
   data = src;

   log->type = type;
   log->srcC  = srcC;
   log->src   = src;
   log->destC = destC;
   log->dest  = dest;
   log->length = length;
   log->fill   = 0;

   // Check for circular buffer transfer
   circXfer  = ((type & 0x00FF) == 0x00C1);
   circXfer |= ((type & 0x00FF) == 0x001C);
 
   if (circXfer && data) {
      log->value1  = *data++;
      log->value2  = *data++;
   }

   log->RTPTime = swicnt | (DSPCore << 28);
   if (circXfer && srcC != 0) {
      log->srcSlips = ((CircBufInfo_t *)(srcC))->SlipSamps;
   } else
      log->srcSlips = 0xFFFF;

   if (circXfer && destC != 0) {
      log->destSlips = ((CircBufInfo_t *)(destC))->SlipSamps;
   } else 
      log->destSlips = 0xFFFF;

   return 0;
}

#endif

ADT_UInt32 timeFilter = 0x80000000ul;
#pragma DATA_SECTION (timeFilter,  "SHARED_DATA_SECT:logging")
ADT_UInt32 tlDisable = ADT_FALSE;
#pragma DATA_SECTION (tlDisable,   "SHARED_DATA_SECT:logging")

//{  Time logging filters
// 
//   Type value     Event
//   0010 intr      DMA interrupt
//
//   0020 fsth      SWI Threads (fs=frame size, th=thread type)
//
//   010x xxxx      DMA SWI
//   0100 eeee           Entry (evnt)
//   010F aaaa           Exit  (accum)
//   810D eeee           DMA Sync  (evnt)
//   810E 00AD           rx DMA sync error
//   810E 00DA           tx DMA sync error
//   810E 00FE           frame sync error
//   
//   020x xxxx     TIMER SWI
//   020A 0000           Scheduler activation
//   020C ccnt           Missed interrupts count
//   020E ccnt           Serial port restart (count)
//   020F 0000           Timer Exit
//   820E mcnt           Missing interrupt error
//
//   040x xxxx     MESSAGING 
//   0400 msgs           Message received
//   0401 rply           Reply sent
//
//   100x xxxx     FRAME TASK SWI
//   100A 0000           Scheduler activation
//   1000 size           Frame Entry
//   1001 size           Decode begin
//   1002 size           Encode begin
//   1003 size           Conference begin
//   100F size           Frame Exit
//   900E size           Circular buffer overrun

//   200x xxxx     CHANNEL PROCESSING
//   200A chsz           Algorithm DMA retreive
//   200A ch00           Algorithn DNA store
//   200B chsz           Algorithm DMA retreive
//   200B ch00           Algorithn DNA store
//   200C cfsz           Conference start
//   200C cf00           Conference end
//   200E 00ch           Channel enqueued
//   200D 00ch           Channel dequeued
//   A000 wwch           Warning message
//
//   400s xxch     CHANNEL DETAILS (s = A or B side; ch = channel #)
//        00            Pkt buffer retreive
//        01            SW expansion
//        02            Echo cancellation
//        03            Tone detection
//        04            Ced/Cng detection
//        05            AGC
//        06            Noise supression
//        07            Voice recording
//        08            Tone generation
//        09            Voice playback
//        0A            SW compression
//        0B            To PCM copy
//        0C            Bulk copy
//        0F            Channel Type Exit
//        10            Pkt buffer parsing
//        11            Decode
//        12            FAX relay PCM and pkt generation
//        13            VAD detection
//        14            Encoding
//        15            Pkt buffer store
//}
#ifndef _DEBUG  // Time logging
#ifndef _LOG
   int lastLogValue;
    #pragma CODE_SECTION (logTime, "FAST_PROG_SECT")
   void logTime (ADT_UInt32 type) { lastLogValue = type; return; }
#else
   // ------------- Time logging structures
   #ifndef DBG_TIMEDATA_LEN
      #define DBG_TIMEDATA_LEN (2<<6)
   #endif
   #define timeDataSize DBG_TIMEDATA_LEN

   typedef struct tlStruct {
      ADT_UInt32 RTPtime;
      ADT_UInt32 type;
   } tlStruct;

   typedef struct timeLog {
      ADT_UInt32   magic;
      unsigned int Idx;
      tlStruct     Data[timeDataSize + 3];
   } timeLogData;
   timeLogData tl;
   #pragma DATA_SECTION (tl,  "PER_CORE_DATA:logging")

    #pragma CODE_SECTION (logTime, "FAST_PROG_SECT")
   int logTime (ADT_UInt32 type) {
      tlStruct  *log;
      int idx, mask;


      if (type == 0x234432) {
         memset (&tl, 0, sizeof (tl));
         tl.magic = 0x13572468;
         return 0;
      }

      mask = HWI_disable ();
      idx = tl.Idx++ % timeDataSize;
      log = &tl.Data[idx];
      (log+1)->type = 0xFFFFFFFF;
      HWI_restore (mask);
      log->type    = type;
      log->RTPtime = ApiBlock.DmaSwiCnt;

      return 0;
   }
#endif
#else

// NOTE: TIMEDATA_LEN must be power of 2
#ifndef DBG_TIMEDATA_LEN
   #define DBG_TIMEDATA_LEN (2<<6)
#endif
#define timeDataSize DBG_TIMEDATA_LEN
// ------------- Time logging structures
typedef struct tlStruct {
   ADT_UInt32 delta;
   ADT_UInt32 RTPtime;
   ADT_UInt32 type;
} tlStruct;

typedef struct timeLog {
   ADT_UInt32   magic;
   ADT_UInt32   lastLogTime; 
   ADT_UInt32   pattern[4];
   ADT_UInt32   patternFilter[4];
   unsigned int Idx;
   ADT_Bool     Disable;

   tlStruct     Data[timeDataSize + 3];
} timeLogData;
timeLogData tl = { 0 };
#pragma DATA_SECTION (tl,  "PER_CORE_DATA:logging")


#pragma CODE_SECTION (logTime, "FAST_PROG_SECT")
int logTime (ADT_UInt32 type) {
   tlStruct  *log;
   ADT_UInt32 time;
   int i, patternActive, nMatches, mask, idx;

   if (tlDisable) return 0;
   if (DSPCore == 1)   updateBootProg (type);

   if (type == 0x234432) {
      memset (&tl, 0, sizeof (tl));
      tl.magic = 0x13572468;

      if (dl.magic != 0x234432)
          dataLogInit (DataDetails, sizeof (DataDetails));
      return 0;
   }
   AppErr ("TimeLog", tl.magic != 0x13572468);

   for (i=0, nMatches=0, patternActive=0; i<4; i++) {
      if (tl.pattern[i] == 0) break;
      patternActive = 1;
      if ((type & tl.patternFilter[i]) == tl.pattern[i])  nMatches++;
   }

   if (patternActive && (nMatches == 0)) return 0;
    
   if (!patternActive) {
      if ((type & timeFilter) == 0) return 0;
   }

   mask = HWI_disable ();
   time = CLK_gethtime();
   idx = (tl.Idx++) % timeDataSize;
   log = &tl.Data[idx];
   memset (&tl.Data[idx+1], 0xffff, sizeof (tlStruct));
   log->delta   = time - tl.lastLogTime;
   tl.lastLogTime = time;
   log->type    = type;
   HWI_restore (mask);

   log->RTPtime = (ApiBlock.DmaSwiCnt & 0xffffff) | (DSPCore << 28);
   return 0;
}

#endif

#undef ecSigTrace
#ifndef _DEBUG  // Echo canceller pulse tracing
   // Satisfy references when using debugging components with non-debugging library
   void ecSigTrace (int port, int slot, int code, ADT_Int16 *pBuf1, ADT_Int16 *pBuf2, int len) { return; }
#else

// Set up debug buffers to default sizes
#define EC_TRACE_LEN 256
// ------------- EC pulse tracing structures
int ecTraceIdx = 0;

typedef struct ecTrace {
    int active;             // 1 == active
    int sendRate;           // rate to send impulses (samples)
    int sendCount;          // count till next impulse is sent (samples)
    int chanId;             // channel being traced
    int inPort;             // input tdm port being traced
    int inSlot;             // input tdm slot being traced
    int dmaInSlot;          // input dma buffer slot
    int outPort;            // output tdm port being traced
    int outSlot;            // output tdm slot being traced
    int dmaOutSlot;         // output dma buffer slot
    int rxImpulseThresh;    // minimum rx level of impulse
} ecTraceCtrl_t;

ecTraceCtrl_t ecTrace = {
    0,      // 1 == active
    800,    // rate to send impulses (samples)
    0,      // count till next impulse is sent (samples)
    1,      // ID of channel being traced
    0,      // input tdm port being traced
    0,      // input tdm slot being traced
    0,      // input dma buffer slot
    0,      // output tdm port being traced
    0,      // output tdm slot being traced
    0,      // output dma buffer slot
    1000    // minimum rx level of impulseSE_THRESH};
};

typedef struct ecTraceData {
    ADT_UInt32 code;
    ADT_UInt32 ts;
    ADT_UInt32 offset;
} ecTraceData_t;
ecTraceData_t *pEcTr;
ecTraceData_t ecTrBuf[EC_TRACE_LEN];
#pragma DATA_SECTION (ecTrBuf,  "PER_CORE_DATA:logging")
#pragma DATA_ALIGN   (ecTrBuf,  CACHE_L2_LINE_SIZE)

#define INC_TRACE_IDX if (++ecTraceIdx >= EC_TRACE_LEN) ecTraceIdx = 0
#define LD_TRACE_PTR pEcTr = &ecTrBuf[ecTraceIdx]

#pragma CODE_SECTION (UpdateEcTraceLog, "SLOW_PROG_SECT")
static void UpdateEcTraceLog (int code, ADT_UInt32 offset) {
    SWI_disable();       
    LD_TRACE_PTR;          
    pEcTr->code = code;   
    pEcTr->ts = ApiBlock.DmaSwiCnt;
    pEcTr->offset = offset;
    INC_TRACE_IDX; 
    SWI_enable();
}


// EcTrace codes
// 0 start a trace  (Called externally)
// 1 add an impulse to circ output buffer (Called by framing)
// 2 Search for impulse in tx dma copy buffer.
// 3 Search for impulse location in current rx dma buffer
// 4 search the ec near and ec far buffers for the impulse (Called by framing)
// 5 disable the trace  (Called externally)

#pragma CODE_SECTION (ecSigTrace, "SLOW_PROG_SECT")
void ecSigTrace (int port, int slot, int code, ADT_Int16 *pBuf1, ADT_Int16 *pBuf2, int len) {
    chanInfo_t *pChan;
    pcm2pkt_t *PcmPkt;   // pointer to channel's PCM to Packet data
    pkt2pcm_t *PktPcm;   // pointer to channel's Packet to PCM data
    int i;
    ADT_Int16 *pBuf;
    ADT_UInt16 *SlotMap;

    if ((code != 0) && (ecTrace.active == 0))   return;

    switch (code) {
     case 0:
         // start a trace
         pChan = chanTable[ecTrace.chanId];
         PcmPkt = &(pChan->pcmToPkt);
         PktPcm = &(pChan->pktToPcm);

         ecTrace.sendCount  = 0;
         ecTrace.inPort     = PcmPkt->InSerialPortId;
         ecTrace.inSlot     = PcmPkt->InSlotId;

         SlotMap = pSlotMap[PcmPkt->InSerialPortId];
         ecTrace.dmaInSlot  = SlotMap[PcmPkt->InSlotId];
         ecTrace.outPort    = PktPcm->OutSerialPortId;
         ecTrace.outSlot    = PktPcm->OutSlotId;
         SlotMap = pSlotMap[PktPcm->OutSerialPortId];
         ecTrace.dmaOutSlot = SlotMap[PktPcm->OutSlotId];
         ecTraceIdx = 0;
         LD_TRACE_PTR;
         ecTrace.active  = 1;
         break;            

     case 1:  // add an impulse to circ output buffer
         if ((port != ecTrace.outPort) || (slot != ecTrace.outSlot)) return;
         memset(pBuf1, 0, sizeof(ADT_UInt16) * len);
         if (ecTrace.sendCount >= ecTrace.sendRate) {
             pBuf1[0] = 32767;
             ecTrace.sendCount = 0;
             UpdateEcTraceLog(code, 0); 
         } else {
             ecTrace.sendCount += len;
         }
         break;

     case 2:   // Search for impulse in tx dma copy buffer.
         if (port != ecTrace.outPort) return; 

         // index to correct slot location in dma buffer
         pBuf = &pBuf1[len * ecTrace.dmaOutSlot];
         for (i=0; i<len; i++) {
             if (pBuf[i] == 32767) {
                 UpdateEcTraceLog(code, i); 
                 break;
             }
         }
         break;

     case 3:  // Search for impulse location in current rx dma buffer
         if (port != ecTrace.inPort) return; 

         // index to correct slot location in dma buffer
         pBuf = &pBuf1[len * ecTrace.dmaInSlot];
         for (i=0; i<len; i++) {
             if (pBuf[i] > ecTrace.rxImpulseThresh) {
                 UpdateEcTraceLog(code, i); 
                 break;
             }
         }
         break;        

     case 4:     // search the ec near and ec far buffers for the impulse
         if ((port != ecTrace.inPort) || (slot != ecTrace.inSlot)) return; 

         // search near buffer
         for (i=0; i<len; i++) {
             if (pBuf1[i] > ecTrace.rxImpulseThresh) {
                 UpdateEcTraceLog(4, i); 
                 break;
             }
         }
                 
         // search far buffer
         for (i=0; i<len; i++) {
             if (pBuf2[i] == 32767) {
                 UpdateEcTraceLog(5, i);
                 break;
             } 
         }
         break;

     case 5:
         // disable the trace
         ecTrace.active = 0;
         break;

     default:
         break;
    };
}
#endif


#undef ptrTrace
#ifndef _DEBUG  // Echo canceller circular pointer tracing
   // Satisfy references when using debugging components with non-debugging library
   void ptrTrace (CircBufInfo_t *pTdmOut, CircBufInfo_t *pEcFar, int chan) { return; } 
#else

#ifndef DBG_CIRC_TRACELEN
   #define DBG_CIRC_TRACELEN 256
#endif

int circ0Idx = 0;
int circ1Idx = 0;
typedef struct ecCircTrace_t {
   ADT_Word     farSlip;
   ADT_Word     tdmoSlip;
   ADT_Word     farTake;
   ADT_Word     tdmoPut;
   ADT_Word     tdmoTake;
   ADT_Word     delta;
} ecCircTrace_t;

ecCircTrace_t circTrace0[DBG_CIRC_TRACELEN + 127];
ecCircTrace_t circTrace1[DBG_CIRC_TRACELEN + 127];
#pragma DATA_SECTION (circ0Idx,    "SHARED_DATA_SECT")
#pragma DATA_SECTION (circ1Idx,    "SHARED_DATA_SECT")
#pragma DATA_SECTION (circTrace0,  "SLOW_DATA_SECT:logging")
#pragma DATA_ALIGN   (circTrace0,  CACHE_L2_LINE_SIZE)
#pragma DATA_SECTION (circTrace1,  "SLOW_DATA_SECT:logging")
#pragma DATA_ALIGN   (circTrace1,  CACHE_L2_LINE_SIZE)


//-------------------------------------------------------
//  Echo canceller data trace
#pragma CODE_SECTION (ptrTrace, "SLOW_PROG_SECT")
void ptrTrace (CircBufInfo_t *pTdmOut, CircBufInfo_t *pEcFar, int chan) {
   ecCircTrace_t *p, *pNext;
   ADT_Int32 delta;

    if (chan == 0) {       
        p = &circTrace0[circ0Idx];
        circ0Idx++;
        if (circ0Idx >= DBG_CIRC_TRACELEN)   circ0Idx = 0;
        pNext = &circTrace0[circ0Idx];
    } else if (chan == 1)  {
        p = &circTrace1[circ1Idx];
        circ1Idx++;
        if (circ1Idx >= DBG_CIRC_TRACELEN)  circ1Idx = 0;
        pNext = &circTrace1[circ1Idx];
    } else
      return;

    p->farSlip  = pEcFar->SlipSamps;
    p->tdmoSlip = pTdmOut->SlipSamps;
    p->farTake  = pEcFar->TakeIndex;
    p->tdmoPut  = pTdmOut->PutIndex;
    p->tdmoTake = pTdmOut->TakeIndex;
    delta       = (ADT_Int32)(p->tdmoTake - p->farTake);
   if (delta < 0)  delta += pTdmOut->BufrSize;
   p->delta = delta;

    pNext->farSlip  = (ADT_Word)(-1);
    pNext->tdmoSlip = (ADT_Word)(-1);
    pNext->farTake  = (ADT_Word)(-1);
    pNext->tdmoPut  = (ADT_Word)(-1);
    pNext->tdmoTake = (ADT_Word)(-1);
    pNext->delta    = (ADT_Word)(-1);
} 

#endif



//============================================================================
// ---------------------- System Error Handling ------------------------------------
//========================================================================
// Project must set ErrPrintBuff and errPrintI8 to enable error text buffering
char *ErrPos = NULL;
char *ErrBuffEndPos = NULL;
char *ErrPrintBuff = NULL;
int  errorPrintI8 = 0;

#undef SystemError
#undef AppError
#ifndef _DEBUG   // SystemError and AppError
   // Satisfy references when using debugging components with non-debugging library
   void SystemError (char *Msg) { HWI_disable (); while (TRUE);  }
   void AppError (char *FileName, int Line, char *Msg, int Error) { return; }

#else


#ifndef BIOS_V_6
EXC_Status xStat;
#endif

#pragma CODE_SECTION (SystemError, "SLOW_PROG_SECT")
void SystemError (char *msg) { 
   int idx, mask;
   int tempTimeFilter, tempDataFilter;
   dbg.errorCnt++;

   if (skipErrors) return;

   skipErrors = TRUE;

   tempTimeFilter = timeFilter;
   tlDisable = ADT_TRUE;

   tempDataFilter = dataFilter;
   dl.Disable = ADT_TRUE;
   dl.Core    = DSPCore;

   dataFilter = 0;
   timeFilter = 0;

   SWI_disable ();
   //mask = ADDRESS_lock (&dataLock.Inst);
   mask = ADDRESS_lock (&pdataLock->Inst);

#ifndef SKIP_ERR_PRINT
#ifdef _TMS320C6400_PLUS
#ifndef BIOS_V_6
   xStat = EXC_getLastStatus ();
   EXC_clearLastStatus ();
   printf ("EXC. IERR=%x  Efr= %x Nrp=%x\n", xStat.ierr, xStat.efr, xStat.nrp);
#endif
#endif
   printf ("... Sys Error %s\n", msg);
#endif

   // Find buffer that corresponds to the last entry before the 
   // system error.  Entries in the data log that refer to that
   // buffer will automatically be printed
   idx = dl.Idx - 1;
   idx = idx % dl.Instances;
   dl.dumpBuffer = dl.Data[idx].srcC;

   if (dl.dumpBuffer == NULL) 
      dl.dumpBuffer = dl.Data[idx].destC;

   //  Loop on error
   while (stopOnError) {
      searchDataBuffer ();
   } 
   //ADDRESS_unlock (&dataLock.Inst, mask);
   ADDRESS_unlock (&pdataLock->Inst, mask);
   SWI_enable (); 

   // Error manually cleared.  Restore filters and restart data buffer index
   dl.Idx = 0;
   dl.Disable = ADT_FALSE;
   dataFilter = tempDataFilter;

   tlDisable  = ADT_FALSE;
   timeFilter = tempTimeFilter;

}

#pragma CODE_SECTION (SendAppErrEvent, "MEDIUM_PROG_SECT")
void SendAppErrEvent (char *Warning) {

   typedef struct ErrEvmtMsg_t {
      EventFifoHdr_t  header;    
      char            errorTxt[80];
   } ErrEvntMsg_t;
   ErrEvntMsg_t eventMsg;

   strncpy (eventMsg.errorTxt, Warning, 79);
   eventMsg.header.channelId = DSPCore;
   eventMsg.header.eventCode = EventWarning;
   eventMsg.header.eventLength = strlen (eventMsg.errorTxt) + 1; 
   eventMsg.header.deviceSide = WarnApplicationError;
   writeEventIntoFifo ((EventFifoMsg_t *) &eventMsg);
}


#pragma CODE_SECTION (AppError, "SLOW_PROG_SECT")
void AppError (char *FileName, int Line, char *Msg, int Error) {
   char *EndPos, *StartPos;
   char *fileName;

   if (!Error) return;
   if (skipErrors) return;

   dl.Disable = ADT_TRUE;
   tlDisable  = ADT_TRUE;

   if (ErrPos == NULL) {
      ErrPos = ErrPrintBuff;
      ErrBuffEndPos = ErrPrintBuff + errorPrintI8;
   }
   StartPos = ErrPos;
   EndPos = ErrPos + strlen (Msg) + 80;

   // Strip directory name
   fileName = strrchr (FileName, '/');
   if (fileName == NULL) fileName = FileName;
   else fileName++;

   if ((ErrPos != NULL) && (EndPos <= ErrBuffEndPos))
      ErrPos += sprintf (ErrPos, "%s:%d %s %d\n", fileName, Line, Msg, Error) + 1;


   logTime (0x08000000ul |  DSPCore);
   SendAppErrEvent (StartPos);
   printf ("%s:%d %s %d\n", fileName, Line, Msg, Error);
   SWI_disable ();
   SystemError (Msg);
   SWI_enable ();
}

#endif // end _DEBUG
