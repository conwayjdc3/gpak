//---------------------------------------------------------------
// Power control initialization
//
#define _CSL_H_         // Avoid csl definitions
#include <std.h>
#include <stdio.h>
#include <hwi.h>
#include <c64.h>
#include <clk.h>
#include <bcache.h>
#include "GpakExts.h"
#include "adt_typedef.h"

#include "64plus.h"
#include "sysconfig.h"

int hostToDspIntrCnt;
int dspToHostIntrCnt;

#pragma CODE_SECTION (updateBootProg, "FAST_PROG_SECT")
void updateBootProg (ADT_UInt32 type) { 
#ifdef CHIP_6472   // Intercore interrupts
   *((volatile ADT_UInt32 *)0x02AB0008) = type; // Boot progress register
#endif
}

#ifdef CHIP_6472   // Intercore interrupts
extern Fxn frameReadyISR;


#pragma CODE_SECTION (Gen_Core_Interrupt, "MEDIUM_PROG_SECT")
void Gen_Core_Interrupt (int frameBits, int coreID) {
   ADT_UInt32 *CoreIntrRegister;
      
   CoreIntrRegister = (ADT_UInt32 *) 0x02A80540;
   CoreIntrRegister += coreID;
   *CoreIntrRegister |= frameBits | 1;
}

#pragma CODE_SECTION (Read_Core_Interrupt, "FAST_PROG_SECT")
ADT_UInt32 Read_Core_Interrupt (int coreID) {
   ADT_UInt32 *CoreIntrRegister;
      
   CoreIntrRegister = (ADT_UInt32 *) 0x02A80540;
   CoreIntrRegister += coreID;
   return *CoreIntrRegister;
}

#pragma CODE_SECTION (ACK_Core_Interrupt, "FAST_PROG_SECT")
void ACK_Core_Interrupt (int flags, int coreID) {
   ADT_UInt32 *CoreACKRegister;
      
   CoreACKRegister = (ADT_UInt32 *) 0x02A80580;
   CoreACKRegister += coreID;
   *CoreACKRegister |= flags;
   
}
#else
#pragma CODE_SECTION (Gen_Core_Interrupt, "SLOW_PROG_SECT")
void Gen_Core_Interrupt (int frameIdx, int coreID) { return; }

#pragma CODE_SECTION (Read_Core_Interrupt, "SLOW_PROG_SECT")
ADT_UInt32 Read_Core_Interrupt (int coreID) {  return 0; }

#pragma CODE_SECTION (ACK_Core_Interrupt, "SLOW_PROG_SECT")
void ACK_Core_Interrupt (int flags, int coreID) { return; }
#endif

#pragma CODE_SECTION (Gen_Host_Interrupt, "MEDIUM_PROG_SECT")
void Gen_Host_Interrupt () {
   if (sysConfig.HostIF & HPI_IF)
      *HPIC |= HPI_HOST_INT; 
#ifdef PCI_BASE
   else if (sysConfig.HostIF & PCI_IF)
      *PCI_INT_GEN = PCI_HOST_INTS;
#endif
   dspToHostIntrCnt++;
}

#pragma CODE_SECTION (Enable_Interrupt_From_Host, "SLOW_PROG_SECT")
void Enable_Interrupt_From_Host (Fxn HostIsr) {
   int hostIntEvent;    // Event generating interrupt

   hostToDspIntrCnt = 0;
   dspToHostIntrCnt = 0;

   if (sysConfig.HostIF & PCI_IF) {
#ifdef PCI_BASE
      *PCI_DSP_INT_DISABLE = PCI_ALL_INTS;
      *PCI_DSP_INT_ENABLE = PCI_DSP_INTS;  // Enable PCI interrupts at DSP
      hostIntEvent = PCI_INT_EVENT;        // Identify event causing interrupt
#endif
   } else if (sysConfig.HostIF & HPI_IF) {
      hostIntEvent = HPI_INT_EVENT;
   }

   // Identify ISR for HOST generated interrupts
   HWI_dispatchPlug (HWI_HOST, HostIsr, -1, NULL);
   HWI_eventMap     (HWI_HOST, hostIntEvent);
   C64_enableIER (1 << HWI_HOST);
}

#pragma CODE_SECTION (Enable_Interrupt_From_Core0, "SLOW_PROG_SECT")
void Enable_Interrupt_From_Core0 (Fxn frameReadyISR) {

#ifdef CHIP_6472
   ACK_Core_Interrupt (0xffffffff, DSPCore);

   // Identify ISR for CORE 0 generated interrupts
   HWI_dispatchPlug (HWI_FRAME_READY, frameReadyISR, -1, NULL);
   HWI_eventMap     (HWI_FRAME_READY, CSL_INTC_EVENTID_IPC_LOCAL);
   C64_enableIER    (1 << HWI_FRAME_READY);
#endif
}

#pragma CODE_SECTION (PowerOnDevice, "SLOW_PROG_SECT")
void PowerOnDevice (int DeviceCode) {
    // 1. Wait for previous power transition to complete
    while (*PSC_STAT & 1);

    // 2. Enable clock domains for device code
    PSC_REG[DeviceCode]  = PSC_ON;

    // 3. Transition power
    *PSC_CMD = 1;

    // 4. Wait for power transition to complete
    while (*PSC_STAT & 1);
}

#pragma CODE_SECTION (PowerOnPeripherals, "SLOW_PROG_SECT")
void PowerOnPeripherals () {

    volatile ADT_UInt32 *PscCmd  = PSC_CMD;
    volatile ADT_UInt32 *PscStat = PSC_STAT;
    volatile ADT_UInt32 *PscReg  = PSC_REG;
    int NetworkState = PSC_OFF;

#ifdef DEV_CFG_BASE
    volatile ADT_UInt32 *PinMux0 = CFG_PINMUX0;
    volatile ADT_UInt32 *PinMux1 = CFG_PINMUX1;
    int PWR_bits;

    //---------------------------------------------------
    //  Enable/disable power to muxed pins.  0 = power on
    PWR_bits = MUX_BLK_TIMER1;
   *PinMux1 &= MUX1_Timer1Mask;
   *PinMux1 |= MUX1_Timer1Active;
    if (sysConfig.numSlotsOnStream[0] + sysConfig.numSlotsOnStream[1] != 0) {
       PWR_bits |= MUX_BLK_McBSPs;
    }

    if (sysConfig.HostIF & RMII_IF) {
       PWR_bits |= MUX_BLK_EMAC_RMII;
       *PinMux0 &= MUX0_RMIIMask;
       *PinMux0 |= MUX0_RMIIActive;
   }

   // NOTE:  PCI/HPI and DDR2 must already be powered
    //        on for download to complete
    if (sysConfig.HostIF & PCI_IF) {
       PWR_bits |= MUX_BLK_PCI; 
       *PinMux1 &= MUX1_PCIMask;
       *PinMux1 |= MUX1_PCIActive;
    }
    if (sysConfig.HostIF & HPI_IF) {
       PWR_bits |= MUX_BLK_HPI; 
       *PinMux1 &= MUX1_HPIMask;
       *PinMux1 |= MUX1_HPIActive;
    }

    *CFG_VDD3P3V_PWDN = ~PWR_bits;
#endif

//#ifdef CHIP_6472
#if 0
   {
   		ADT_UInt32 *PRI_ALLOC_Register;
   		// increase HPI priority      
   		PRI_ALLOC_Register = (ADT_UInt32 *) 0x02A80004;
   		*PRI_ALLOC_Register &= ~0x0000038;
   }
#endif

   if (sysConfig.HostIF & RMII_IF) {
      NetworkState = PSC_ON;
   }


    //---------------------------------------------------
    //  Enable power to peripherals.
    
    //  Power to HPI, PCI, and DDR2 must be turned on by host
    //  during download.

    //---------------------------------------------------
    // 1. Wait for previous power transition to complete
    while (*PscStat & 1);


    //---------------------------------------------------
    //  2. Enable clock domains for only those peripherals
    //     used by G.PAK 
    //  a. Timer1
    PscReg[PSC_TIMER1] = PSC_ON;

    //  b. Ethernet peripherals
    PscReg[PSC_EMAC_CTRL] = NetworkState;
    PscReg[PSC_MDIO]      = NetworkState;
    PscReg[PSC_EMAC]      = NetworkState;


    //---------------------------------------------------
    //  3. Transition power
    *PscCmd = 1;

    //---------------------------------------------------
    //  4. Wait for power transition to complete
    while (*PscStat & 1);

   // Disable timer on frame processing cores
   if (DSPCore != 0) {
      CLK_stop ();
   }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ HostHWI
//
// FUNCTION
//   Hardware interrupt generated by host
//
// Inputs
//   Nothing
//
// RETURNS
//   Nothing
//
//}
#pragma CODE_SECTION (HostIsr, "FAST_PROG_SECT")
extern void postRtpHostSWI();
void ProcessHostIsr () {

    postRtpHostSWI();

   // Clear interrupt events (PCI or HPI)
#ifdef PCI_BASE
   *PCI_INT_CLR = PCI_DSP_INTS;
#endif
   *HPIC |= HPI_DSP_INT;

   hostToDspIntrCnt++;

   // Clear 
#ifdef PCI_BASE
   clearInterrupt (PCI_INT_EVENT);
#endif
   clearInterrupt (HPI_INT_EVENT);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ CACHE_wbInvAllL2 
//
// FUNCTION
//   Trigger write back of 
//
// Inputs
//   CACHE_NO_WAIT or CACHE_WAIT
//
// RETURNS
//   Nothing
//
//}
#pragma CODE_SECTION (CACHE_wbInvAllL2, "SLOW_PROG_SECT")
void CACHE_wbAll () {
   BCACHE_wbAll();
}
