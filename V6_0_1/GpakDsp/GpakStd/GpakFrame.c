/*
 * Copyright (c) 2004-2007, Adaptive Digital Technologies, Inc.
 *
 * File Name: GpakFrame.c
 *
 * Description:
 *   This file contains G.PAK Framing Task functions.
 *
 * Version: 1.0
 *
 * Revision History:
 *   11/13/01 - Initial release.
 *   04/2005  - Added GSM EFR
 *   2/2007   - Merged C54 and C64
 *
 */
//#define CPU_TEST

// Application related header files.
#include "GpakDefs.h"
#include "GpakExts.h"
#include "sysconfig.h"
#include "GpakPcm.h"
#include "stdio.h"
//#include <clk.h>
//#include <swi.h>
#include <string.h>
#include "toneLog.h"
#include "framestats.h"

#ifdef RTCP_ENABLED
extern far int rtcp_cached;
#endif

int gen_core_int = 1; // jdc test
extern ADT_UInt16 minChanCore;
//#define G726_DEBUG
#ifdef G726_DEBUG
static int useCannedPcm=0;
static int g726_enc_print=0;
static const ADT_UInt16 cannedPcm[8] = {303, 672, 1322, 2409, 5110, 9738, 19202, 34770};
#endif

#define CORE_HEALTH
#ifdef CORE_HEALTH

#define NUM_HEALTH_FRAMES 6
#define NUM_CONSECUTIVE_INACTIVE_FRAMES 20

int core_health_count[8][NUM_HEALTH_FRAMES];  
int core_health_prev_count[8][NUM_HEALTH_FRAMES];
int core_health_inactive[8][NUM_HEALTH_FRAMES];
int core_dead[8];
int core_check_count =0;
#pragma DATA_SECTION (core_health_count,  "NON_CACHED_DATA")
#pragma DATA_SECTION (core_health_prev_count,  "NON_CACHED_DATA")
#pragma DATA_SECTION (core_health_inactive,  "NON_CACHED_DATA")
#pragma DATA_SECTION (core_dead,  "NON_CACHED_DATA")
#pragma DATA_SECTION (core_check_count,  "NON_CACHED_DATA")


#pragma CODE_SECTION (InitFrameHealth, "SLOW_PROG_SECT")
void InitFrameHealth() {
    memset(core_health_count, 0, sizeof(core_health_count));
    memset(core_health_prev_count, 0, sizeof(core_health_prev_count));
    memset(core_health_inactive, 0, sizeof(core_health_inactive));
    memset(core_dead, 0, sizeof(core_dead));
}

#pragma CODE_SECTION (UpdateFrameHealth, "SLOW_PROG_SECT")
void UpdateFrameHealth( ADT_UInt16 SampsPerFrame) {

    switch (SampsPerFrame) {
        case 80:
        core_health_count[DSPCore][0]++;
        break;

        case 160:
        core_health_count[DSPCore][1]++;
        break;

        case 240:
        core_health_count[DSPCore][2]++;
        break;

        case 320:
        core_health_count[DSPCore][3]++;
        break;

        case 400:
        core_health_count[DSPCore][4]++;
        break;

        case 480:
        core_health_count[DSPCore][5]++;
        break;
    }
}

#pragma CODE_SECTION (CoreHealthtoHost, "SLOW_PROG_SECT")
void CoreHealthtoHost  (int DSPCore) {
   EventFifoMsg_t msg;

   msg.header.channelId   = DSPCore;
   msg.header.deviceSide  = ADevice;
   msg.header.eventLength = EVENT_FIFO_MSGLEN_ZERO;
   msg.header.eventCode   = EventCoreHealth;

   writeEventIntoFifo (&msg);
}

// called by message core at 1 msec interval 
// to check the health of framing cores
#pragma CODE_SECTION (CheckFrameHealth, "SLOW_PROG_SECT")
void CheckFrameHealth() {
    int frame;
    int *count, *prev, *inactive;
    int Core_dead = 0;
    int core;

    // check the cores every 100 msec
    core_check_count++;
    if (core_check_count < 100) return;
    core_check_count = 0;

    for (core=minChanCore; core<DSPTotalCores; core++) { 
        Core_dead = 0;
        count = (int *)&core_health_count[core];
        prev  = (int *)&core_health_prev_count[core];
        inactive = (int *)&core_health_inactive[core];
    
        for (frame=0; frame<NUM_HEALTH_FRAMES; frame++) {
            if (count[frame] != prev[frame]) {
                prev[frame] = count[frame];
                inactive[frame] = 0;
            } else {
                if (inactive[frame] < NUM_CONSECUTIVE_INACTIVE_FRAMES) {
                    inactive[frame]++;
                    if (inactive[frame] == NUM_CONSECUTIVE_INACTIVE_FRAMES) {
                        // flag this core as inactive
                        Core_dead++;
                    }
                }
            }
        }
    
        if (Core_dead && (core_dead[core] == 0)) {
            core_dead[core] = 1;
            // send report to host that core is dead
            CoreHealthtoHost(core);
        }
    }
}
#else
void CheckFrameHealth() { }
void UpdateFrameHealth( ADT_UInt16 SampsPerFrame) { }
void InitFrameHealth() { }
void CoreHealthtoHost  (int CoreID) { }
#endif

//#define EVENT_TEST   // jdc event priority test
#ifdef EVENT_TEST   // jdc event priority test
int sendTestEvent = 0;
char appErrBuff[16];
extern void SendAppErrEvent (char *Warning);
#endif

#define SendToneTimeStamp  PcmPkt->TDInstances.ToneTimeStamp
#define RegenToneTimeStamp PktPcm->TDInstances.ToneTimeStamp

extern CircBufInfo_t captureA, captureB, captureC, captureD;
extern GpakTestMode RTPLoopBack;

#define TSIP_debug  0
#if (DSP_TYPE == 64)
#define G711_muLawDecode(a,b,c) G711_ADT_muLawExpand(a,b,c)
#define G711_muLawEncode(a,b,c) G711_ADT_muLawCompress(a,b,c)
#define G711_aLawDecode(a,b,c)  G711_ADT_aLawExpand(a,b,c)
#define G711_aLawEncode(a,b,c)  G711_ADT_aLawCompress(a,b,c)
#else
#define G711_muLawDecode muLawDecode
#define G711_muLawEncode muLawEncode
#define G711_aLawDecode  aLawDecode
#define G711_aLawEncode  aLawEncode
#endif
extern int g729EncInit(void *chan, void *scratch, short vadEnable);
extern int g729DecInit(void *chan, void *scrtch);
extern int g729Decode (void *chan, void *pyld, void *pcm, short FrameErase, short VadFlag);
extern int g729Encode (void *chan, void *pcm,  void *data, short Vad, short *FrameType);
extern void lbCoderInit();
extern void customVad();
extern void postRtpHostSWI(ADT_Bool overloaded, ADT_UInt32 ticksPerFrame);

extern ADT_UInt32 HostSWIMaxTicks;
extern ADT_Word encoderInstI16;
extern ADT_Word decoderInstI16;

extern ADT_Word NoMcBSPCnt;
extern int gblPkLoadingRst;

/* Host Port Interrupt flags. */
extern const ADT_UInt16 Hint_1ms,  Hint_2_5ms, Hint_5ms;
extern const ADT_UInt16 Hint_10ms, Hint_20ms,  Hint_22_5ms;
extern const ADT_UInt16 Hint_30ms;

static void EnqueuePendingChannels (int SamplesPerFrame);
static void EnqueuePendingConferences (int cnfID);
static void GpakFrameTask (Arg FrameRate);
ADT_Bool GpakAdjustChannelPointers (FrameTaskData_t *TskInf);

#define EncoderSilence   0xDEAD
#define EncoderSpeech    0xAAAA
#define VocoderUnprimed  0xFFFF

#define ntohl(a) ((long) (((long)a>>16) & 0xffff) | ((long)a<<16))

// Task timing related definitions.
#define SUM_SHIFT     5   // number of bits to shift out of measurement
#define TIME_AVG_CNT 32   // number of time measurements in average 2 ^ SUM_SHIFT

#ifdef _TMS320C6X
#define CYCLESPERTICK 8
#else
#define CYCLESPERTICK 1
#endif


#if ((DSP_TYPE == 54)||(DSP_TYPE == 55))
   #define muLawDecode(pyld, pcm, frmSize)   MuLawExpand (pyld, pcm, frmSize)
   #define muLawEncode(pcm, pyld, frmSize)   MuLawCompress (pcm, pyld, frmSize)
   #define aLawDecode(pyld, pcm, frmSize)    ALawExpand (pyld, pcm, frmSize)
   #define aLawEncode(pcm, pyld, frmSize)    ALawCompress  (pcm, pyld, frmSize)
   #define FLUSH_ALL_CACHE() ;
   
   #define Enable_Interrupt_From_Core0(fnc)  ;
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// PackG711 - Pack G.711 compressed data into RTP packet payload format.
//
// FUNCTION
//    This function packs the specified G.711 compressed data into RTP packet
//    payload format and stores in the specified buffer.
//
// RETURNS
//  nothing
//
static void PackG711 (
   ADT_UInt16 *pG711Data,  // in, pointer to G.711 compressed data (1 item / word)
   ADT_UInt16 *pPktBufr,   // out, pointer to RTP payload buffer (2 octets / word)
   ADT_UInt16 DataLength   // length of compressed data (must be even)
   ) {
   ADT_UInt16 Count;      // loop counter

   // Move pairs of compressed octets into words of the payload buffer.
   Count = DataLength / 2;
   while (Count-- > 0) {
      *pPktBufr = *pG711Data++ & 0x00FF;
      *pPktBufr++ |= ((*pG711Data++ << 8) & 0xFF00);
   }
   return;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// UnpackG711 - Unpack G.711 RTP packet payload data to compressed data format.
//
// FUNCTION
//    This function unpacks the specified G.711 RTP packet payload data into 
//    compressed data format and stores in the specified buffer.
//
// RETURNS
//  nothing
//
static void UnpackG711 (
   ADT_UInt16 *pPktData,   // in, pointer to RTP payload (2 octets / word)
   ADT_UInt16 *pG711Bufr,  // out, pointer to G.711 compressed data bufr (1 item / word)
   ADT_UInt16 DataLength   // length of payload data (must be even)
   ) {
   ADT_UInt16 Count;        // loop counter

   // Move octets from the payload buffer into the compressed data buffer.
   Count = DataLength / 2;
   while (Count-- > 0) {
      *pG711Bufr++ = *pPktData & 0x00FF;
      *pG711Bufr++ = (*pPktData++  >> 8) & 0x00FF;
   }

   return;
}
#elif (DSP_TYPE == 64)
#include "g711a1a2_user.h"
   #define muLawDecode(pyld, pcm, frmSize)   G711DEC_ADT_muLawExpand (pyld, pcm, frmSize)
   #define muLawEncode(pcm, pyld, frmSize)   G711ENC_ADT_muLawCompress (pcm, pyld, frmSize)
   #define aLawDecode(pyld, pcm, frmSize)    G711DEC_ADT_aLawExpand (pyld, pcm, frmSize)
   #define aLawEncode(pcm, pyld, frmSize)    G711ENC_ADT_aLawCompress  (pcm, pyld, frmSize)
   #define FLUSH_ALL_CACHE() if (1 < DSPTotalCores) BCACHE_wbInvAll ();

   void frameReadyISR (void);
#if 0  
void G726_Encode (void *inst, ADT_UInt16 *Pcm, ADT_UInt16 *Pyld, int smpCnt) { 
   while (smpCnt--) {
      *Pyld++ = (ADT_UInt16) G726_ADT_Encode (inst, *Pcm++); 
   }
}
void G726_Decode (void *inst, ADT_UInt16 *Pcm, ADT_UInt16 *Pyld, int smpCnt) {
   while (smpCnt--) {
      *Pcm++  = G726_ADT_Decode (inst, (unsigned char) *Pyld++); 
   }
}
#else
// jdc mods. to support new g711 ulaw with unsigned char compressed buffer
void G726_Encode (void *inst, unsigned char *Ulaw, ADT_UInt16 *Pyld, int smpCnt) {
ADT_UInt16 temp;

 
   while (smpCnt--) {
      temp = (ADT_UInt16)*Ulaw++;
      *Pyld++ = G726_ADT_Encode (inst, (unsigned char)temp);
   }
}
void G726_Decode (void *inst, unsigned char *Ulaw, ADT_UInt16 *Pyld, int smpCnt) {
ADT_UInt16 temp;
   while (smpCnt--) {
      temp  = G726_ADT_Decode (inst, *Pyld++); 
      *Ulaw++ = (unsigned char)temp;
   }
}

#endif
#endif

#ifdef TONE_LOGGING // Tone logging
toneLog_t toneLog;
#pragma DATA_SECTION (toneLog,  "PER_CORE_DATA:logging")

#pragma CODE_SECTION (setToneLogMemory, "SLOW_PROG_SECT")
void setToneLogMemory (void *memory, int memoryI8) {
   toneLog.data = memory;
   toneLog.dataEntries = memoryI8 / sizeof (toneData_t);
   toneLog.filter = TONET | FRMNET | TONEDETECT;
}

#pragma CODE_SECTION (logTone, "SLOW_PROG_SECT")
void logTone (ADT_UInt16 Type, ADT_UInt16 ChanId, ADT_UInt16 PayldClass, ADT_UInt16 ToneIdx,
              ADT_UInt16 ToneDur, ADT_UInt32 PktTimeStamp, ADT_UInt32 ToneTimeStamp) {

   toneData_t *log;

   if ((toneLog.chan1 == -1) && (toneLog.chan2 == -1)) return;
   if ((toneLog.chan1 != ChanId) && (toneLog.chan2 != ChanId)) return;
   if ((toneLog.filter != -1) && ((toneLog.filter & Type) == 0)) return;
   if (toneLog.data == NULL) return;

   log = &toneLog.data[toneLog.idx++];
   toneLog.idx = toneLog.idx % toneLog.dataEntries;
   log->Type   = Type;
   log->ChanId = ChanId;
   log->PayldClass = PayldClass;
   log->ToneIdx = ntohs (ToneIdx);
   log->ToneDur = ntohs (ToneDur);
   log->PktTimeStamp  = PktTimeStamp;
   log->ToneTimeStamp = ToneTimeStamp;
   log->DmaCnt = ApiBlock.DmaSwiCnt;
}

#else
#define logTone(Type, ChanId, PayldClass, ToneIdx, ToneDur, PktTimeStamp, ToneTimeStamp);
#define logPkt(ChanId, Direction, PayldType, PktSeq, PktTimeStamp);
void setToneLogMemory (void *memory, int memoryI8) { }
#endif

// GPAK Defaults for Speex.
Gpak_SpeexCfg SpeexDflt = {
    4,        // Quality
    3,        // Complexity
   ADT_TRUE,  // DecodeEnhancementEnable

   ADT_FALSE,  // VBREnable
   4.0,        // VBRQuality
};
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//++++++++++ Framing task data.
//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#define FRAME_TIME_FLAG 1

far int frameTaskCnt = 0;
#pragma DATA_SECTION (frameTaskCnt,  "PER_CORE_DATA:FrameTaskData")

ADT_Word conserveDeActivateThresh = 400;   // deactivate mips conserve when when cpu utilization falls below
ADT_Word conserveActivateThresh   = 600;   // activate mips conserve when cpu utilization exceeds

int frameInitComplete = ADT_FALSE;
#pragma DATA_SECTION (frameInitComplete,  "PER_CORE_DATA:FrameTaskData")

#if (DSP_TYPE == 54)
typedef struct CONTROLDEF {
   Word16 WrkMode ;
   Word16 WrkRate ;
   Flag   UseHp ;   // Hi bypass
   Flag   UsePf ;   // Post filter
   Flag   UseVx ;   // VAD
} CONTROLDEF;
#endif

// NOTE: A single instance is used because G723 runs only at a fixed rate
static CONTROLDEF G723Aux = { 0, 0, TRUE, TRUE, TRUE }; 

// AMR Codec to encoder mode table
const ADT_Int16 amrMode [] = {
     MR475,  MR515,   MR59,    MR67,
     MR74,   MR795,   MR102,   MR122,
};


#define RX_SPEECH_GOOD 0


//-------------------------------------------------------------------
//           Channel Queueing
extern chanInfo_t *PendingDecodeQueue [];
extern chanInfo_t *PendingEncodeQueue [];

extern chanInfo_t *PendingMemberQueue    [];
extern chanInfo_t *PendingCompositeQueue [];

extern chanInfo_t *PPendingDecodeQueue [];
extern chanInfo_t *PPendingEncodeQueue [];

extern chanInfo_t *PPendingMemberQueue    [];
extern chanInfo_t *PPendingCompositeQueue [];

extern coreLock queueLock;
coreLock *pqueueLock = &queueLock;
//#pragma DATA_SECTION (queueLock, "SHARED_LOCK_SECT:queueLock") 
//#pragma DATA_ALIGN   (queueLock, 64) 


//-------------------------------------------------------------------
//           Scratch buffers
extern ADT_UInt16 G168SAscratch_1msLenI16;
extern ADT_UInt16 G168SAscratch_2_5msLenI16;
extern ADT_UInt16 G168SAscratch_5msLenI16;
extern ADT_UInt16 G168SAscratch_10msLenI16;
extern ADT_UInt16 G168SAscratch_20msLenI16;
extern ADT_UInt16 G168SAscratch_22_5msLenI16;
extern ADT_UInt16 G168SAscratch_30msLenI16;

extern ADT_UInt16 G168DAscratch_1msLenI16;
extern ADT_UInt16 G168DAscratch_2_5msLenI16;
extern ADT_UInt16 G168DAscratch_5msLenI16;
extern ADT_UInt16 G168DAscratch_10msLenI16;
extern ADT_UInt16 G168DAscratch_20msLenI16;
extern ADT_UInt16 G168DAscratch_22_5msLenI16;
extern ADT_UInt16 G168DAscratch_30msLenI16;



//-----------------------------------------------------------------------
// DTMF event translation tables
//---- ADT tone relay to rtp event
const ADT_UInt16 relayToPacketDTMFTbl[] = {1,2,3,12,              4,5,6,13,        7,8,9,14,   10,0,11,15};
const ADT_UInt16 relayToPacketMFR1Tbl[] = {129,130,132,135, 142,131,133,136, 139,134,137,138, 128,140,141};

//---- rtp event to ADT tone relay
const ADT_UInt16 packetToRelayDTMFTbl[] = {13,0,1,2,        4,5,6,8,  9,10,12,14, 3,7,11,15};
const ADT_UInt16 packetToRelayMFR1Tbl[] = {32,20,21,25, 22,26,29,23, 27,30,31,28, 33,34};
const ADT_UInt16 packetToRelayFaxTbl[]  = {90,0xffff,0xffff,0xffff, };

//---- aal2 event to ADT tone relay
const ADT_UInt16 rlyToPktAAL2MFR1Tbl[] = { 1,2,4,7, 12,3,5,8, 13,6,9,10,  0,14,11};
const ADT_UInt16 pktToRlyAAL2MFR1Tbl[] = {12,0,1,5,  2,6,9,3, 7,10,11,14, 4,8,13};// TR entry - 20

extern ADT_Int16 TR_ADT_toneTable[100][2];  // ADT tone relay to frequency table

//-------------------------------------------------------------------
//           T38 related definitions
#define END_OF_FAX_TIMEOUT  7000    // 7 seconds of bi-directional silence to declare fax over
#define MEMCPY_ENABLE       1
#define MEMCPY_DISABLE      0
#define TAKE_UPDATE_ENABLE  1
#define TAKE_UPDATE_DISABLE 0

#define FAX_TONE_EVENT ((tone == 32) || (tone == 36) || (tone == 39) || (tone == 40))

//#define _HFZ_DEBUG
#ifdef _HFZ_DEBUG
ADT_UInt32 lostPacketCnt = 0, totalPacketCnt = 0;
#endif
#if 1  // Variable maps
typedef struct FRMVars {

   int *frameTaskCnt;
   ADT_Word *MaxFrameCnt;
   FrameTaskData_t (*FrameTaskData)[];
   struct FrameStats_t *FrameStats;

   ADT_Word *conserveDeActivateThresh;
   ADT_Word *conserveActivateThresh;

   ADT_Word *encoderInstI16;
   ADT_Word *decoderInstI16;

   ADT_Word *NoMcBSPCnt;

   const ADT_UInt16 *Hint_1ms,  *Hint_2_5ms, *Hint_5ms;
   const ADT_UInt16 *Hint_10ms, *Hint_20ms,  *Hint_22_5ms;
   const ADT_UInt16 *Hint_30ms;

   chanInfo_t *(*PendingDecodeQueue)[],  *(*PendingEncodeQueue)[];
   chanInfo_t *(*PPendingDecodeQueue)[], *(*PPendingEncodeQueue)[];
   chanInfo_t *(*PendingMemberQueue)[],  *(*PendingCompositeQueue)[];
   chanInfo_t *(*PPendingMemberQueue)[], *(*PPendingCompositeQueue)[];

   coreLock *queueLock;

   ADT_UInt16 *(*encoderPool)[], *(*decoderPool)[];

} FRMVars_t;

FRMVars_t FRMVars = {
   &frameTaskCnt, &MaxFrameCnt, &FrameTaskData,
   &FrameStats,

   &conserveDeActivateThresh, &conserveActivateThresh,

   &encoderInstI16, &decoderInstI16,

   &NoMcBSPCnt,

   &Hint_1ms,  &Hint_2_5ms, &Hint_5ms,
   &Hint_10ms, &Hint_20ms,  &Hint_22_5ms,
   &Hint_30ms,

   (chanInfo_t *(*)[]) &PendingDecodeQueue,  (chanInfo_t *(*)[]) &PendingEncodeQueue,
   (chanInfo_t *(*)[]) &PPendingDecodeQueue, (chanInfo_t *(*)[]) &PPendingEncodeQueue,

   (chanInfo_t *(*)[]) &PendingMemberQueue,  (chanInfo_t *(*)[]) &PendingCompositeQueue,
   (chanInfo_t *(*)[]) &PPendingMemberQueue, (chanInfo_t *(*)[]) &PPendingCompositeQueue,

   &queueLock,

   (ADT_UInt16 *(*)[]) &encoderPool, (ADT_UInt16 *(*)[]) &decoderPool
};

typedef struct CNFVars_t {
   ConferenceInfo_t  *(*ConferenceInfo)[];
   ConfInstance_t    *(*ConferenceInst)[];
   AGCInstance_t     *(*ConfAgcInstance)[];
   TGInstance_t      *(*ConfToneGenInstance)[];
   TGParams_1_t      *(*ConfToneGenParams)[];
   conf_Member_t     *(*ConfMemberData)[];
   VADCNG_Instance_t *(*ConfVadData)[];
} CNFVars_t;

CNFVars_t CNFVars = {
   (ConferenceInfo_t  *(*)[]) &ConferenceInfo,  
   (ConfInstance_t    *(*)[]) &ConferenceInst, 
   (AGCInstance_t     *(*)[]) &ConfAgcInstance, 
   (TGInstance_t      *(*)[]) &ConfToneGenInstance,
   (TGParams_1_t      *(*)[]) &ConfToneGenParams,
   (conf_Member_t     *(*)[]) &ConfMemberData,
   (VADCNG_Instance_t *(*)[]) &ConfVadData
};

#endif


//} - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//++++++++++ Channel Framing
//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// InitFrameTasks - Initialize G.PAK Framing task data.
//
// FUNCTION
//  This function initializes constant G.PAK Framing task data.
//
// RETURNS
//  nothing
//}
#define FrameDataInit(fd,MS)                 \
   memset (fd, 0, sizeof (FrameTaskData_t)); \
   fd->SampsPerFrame = Frame_##MS##ms * sysConfig.samplesPerMs / 2;   \
   fd->WorkBytes   = fd->SampsPerFrame * 2;                     \
   if ((sysConfig.samplesPerMs == 8) && (SysAlgs.wbEnable))     \
     fd->WorkBytes = fd->SampsPerFrame * 4;                     \
   fd->pInWork     =        inWork_##MS##msec;                  \
   fd->pOutWork    =       outWork_##MS##msec;                  \
   fd->pFarWork    =     ECFarWork_##MS##msec;                  \
   fd->SAScratch   = (void *) &G168SAscratch_##MS##ms;          \
   fd->SAScratchI16=           G168SAscratch_##MS##msLenI16;     \
   fd->DAScratch   = (void *) &G168DAscratch_##MS##ms;          \
   fd->DAScratchI16=           G168DAscratch_##MS##msLenI16;     \
   fd->Hint        =          Hint_##MS##ms;                    \
   fd->TicksPerFrame = TicksPerSample * fd->SampsPerFrame;      \
   fd->pG168MipsConserve    = (void *) &G168MipsConserve_##MS##ms;

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// InitFrameTasks
//
// FUNCTION
//    Call by main to initialize framing trask structures
//
//}
#pragma CODE_SECTION (InitFrameTasks, "SLOW_PROG_SECT")
void InitFrameTasks (void) {
   SWI_Attrs swi_attr = { (SWI_Fxn) GpakFrameTask, 0, 0, 0, 0};

   FrameTaskData_t *frame_data, *Frame_data;

   ADT_UInt32 TicksPerSample;  // number of high res clock ticks per sample
   unsigned int lastFrameSize, nextFrameSize, i;
   int mask;

   ADT_Word *cpuUsage;
   ADT_Word *cpuPkUsage;

#if (DSP_TYPE == 54)
   swi_attr.iscfxn = TRUE;
#endif


   if (DSPCore == 0) {
      InitFrameHealth();

      memset (&PendingDecodeQueue,    0, DSPTotalCores * FRAME_TASK_CNT * sizeof (void *));
      memset (&PendingEncodeQueue,    0, DSPTotalCores * FRAME_TASK_CNT * sizeof (void *));
      memset (&PendingMemberQueue,    0, DSPTotalCores * sysConfig.maxNumConferences * sizeof (void *));
      memset (&PendingCompositeQueue, 0, DSPTotalCores * sysConfig.maxNumConferences * sizeof (void *));

      memset (&PPendingDecodeQueue,    0, DSPTotalCores * FRAME_TASK_CNT * sizeof (void *));
      memset (&PPendingEncodeQueue,    0, DSPTotalCores * FRAME_TASK_CNT * sizeof (void *));
      memset (&PPendingMemberQueue,    0, DSPTotalCores * sysConfig.maxNumConferences * sizeof (void *));
      memset (&PPendingCompositeQueue, 0, DSPTotalCores * sysConfig.maxNumConferences * sizeof (void *));

      if (0 == frameTaskCnt)
         memset (FrameTaskData, 0, MaxFrameCnt * sizeof (FrameTaskData_t));
      //queueLock.Inst = 0;
      pqueueLock->Inst = 0;
   } else {
      // Wait for core zero to finish initialization.
      //mask = ADDRESS_lock (&queueLock.Inst);
      //ADDRESS_unlock (&queueLock.Inst, mask);
      mask = ADDRESS_lock (&pqueueLock->Inst);
      ADDRESS_unlock (&pqueueLock->Inst, mask);
   }

   // Index into core's frame data structures
   frame_data = &FrameTaskData [(DSPCore * FRAME_TASK_CNT) + frameTaskCnt];
   TicksPerSample = CLK_countspms() / sysConfig.samplesPerMs;

   // Initialize Framing task data.
   if (sysConfig.cfgFramesChans & FRAME_8_CFG_BIT) {
      FrameDataInit (frame_data, 1);
      frame_data++;
      frameTaskCnt++;
   }
   if (sysConfig.cfgFramesChans & FRAME_20_CFG_BIT) {
      FrameDataInit (frame_data, 2_5);
      frame_data++;
      frameTaskCnt++;
   }
   if (sysConfig.cfgFramesChans & FRAME_40_CFG_BIT) {
      FrameDataInit (frame_data, 5);
      frame_data++;
      frameTaskCnt++;
   }
   if (sysConfig.cfgFramesChans & FRAME_80_CFG_BIT) {
      FrameDataInit (frame_data, 10);
      frame_data++;
      frameTaskCnt++;
   }
   if (sysConfig.cfgFramesChans & FRAME_160_CFG_BIT) {
      FrameDataInit (frame_data, 20);
      frame_data++;
      frameTaskCnt++;
   }
   if (sysConfig.cfgFramesChans & FRAME_180_CFG_BIT) {
      FrameDataInit (frame_data, 22_5);
      frame_data++;
      frameTaskCnt++;
   }
   if (sysConfig.cfgFramesChans & FRAME_240_CFG_BIT) {
      FrameDataInit (frame_data, 30);
      frame_data++;
      frameTaskCnt++;
   }

   lbCoderInit();
   // Assign priorities to framing tasks. Smaller frame sizes get higher priority.
   swi_attr.priority = FramePriority;
   lastFrameSize = 0;

   cpuUsage   = CpuUsage;
   cpuPkUsage = CpuPkUsage;

   do {
      // Search for next highest unassigned frame size.
      nextFrameSize = (unsigned int) -1;
      frame_data = NULL;
      for (i=0, Frame_data = &FrameTaskData [DSPCore * FRAME_TASK_CNT]; i<frameTaskCnt; i++, Frame_data++) {
         if (Frame_data->SampsPerFrame <= lastFrameSize) continue;
         if (nextFrameSize < Frame_data->SampsPerFrame) continue;
         nextFrameSize = Frame_data->SampsPerFrame;
         frame_data = Frame_data;
      }

      if (frame_data == NULL) break;

      // Assign the selected frame task to the next lower priority
      swi_attr.arg0  = (Arg) frame_data;
      frame_data->pCpuUsage   = cpuUsage++;
      frame_data->pCpuPkUsage = cpuPkUsage++;
      frame_data->SWI = SWI_create (&swi_attr);
      swi_attr.priority--;
      lastFrameSize = nextFrameSize;

   } while (TRUE);

   for (i=0; i<(sysConfig.maxNumConferences + sysConfig.customCnfrCnt) * DSPTotalCores; i++) {
      memset (ConferenceInfo [i], 0, sizeof (ConferenceInfo_t));
   }

   STATS (memset (&FrameStats, 0, sizeof (FrameStats));)
   STATS (FrameStats.StatID = 0x3333; )
   AppErr ("No frame tasks", (frameTaskCnt == 0));
   dmaAllocateChains ();

   Enable_Interrupt_From_Core0 ((Fxn) frameReadyISR);
   frameInitComplete = ADT_TRUE;
   return;
}

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// InitCustomFrameTask
//
// FUNCTION
//    Called by main to initialize a customized framing trask structure
//    Must be called within customDSPInit before InitFrameTasks is called
//
// INPUTS
//    samplesPerFrame - frame size in PCM samples
//    workBuffer      - pointer to ADT_UInt16 array with 6 * samplesPerFrame elements
//   g168saScratch    - algorithm fast memory scratch buffer
//   g168daScratch    - algorithm slow memory scratch buffer
//      tdScratch     - tone detection scratch buffer
//}
#pragma CODE_SECTION (InitCustomFrameTask, "SLOW_PROG_SECT")
ADT_Bool InitCustomFrameTask (int halfMSPerFrame, int generateInt,
                             void *workBuff,  int workBuffI16,
                             void *saScratch, int saScratchI16,
                             void *daScratch, int daScratchI16) {
   ADT_PCM16 *workBuffer;
   ADT_UInt32 TicksPerSample;  // number of high res clock ticks per sample
   FrameTaskData_t *FrameData;
   int samplesPerFrame, workI16;

   AppErr ("Custom Framing", frameInitComplete);
   if (FRAME_TASK_CNT <= frameTaskCnt) {
      AppErr ("Too many frames", ADT_TRUE);
      return 0;
   }

   //  Work bytes must be adjusted if wideband is enabled on 8 kHz TDM channels
   samplesPerFrame = halfMSToSamps (halfMSPerFrame);

   workI16 = samplesPerFrame;
   if ((sysConfig.samplesPerMs == 8) && (SysAlgs.wbEnable))
     workI16 = samplesPerFrame * 2;

   if (workBuffI16 < workI16*3) {
      AppErr ("Buffer too small", ADT_TRUE);
      return 0;
   }


   workBuffer = workBuff;

   TicksPerSample = CLK_countspms() / sysConfig.samplesPerMs;
   FrameData = &FrameTaskData[(DSPCore * FRAME_TASK_CNT) + frameTaskCnt];
   if (0 == frameTaskCnt++)
      memset (FrameData, 0, FRAME_TASK_CNT * sizeof (FrameTaskData_t));

   FrameData->PcmToPktHead = NULL;
   FrameData->PktToPcmHead = NULL;
   FrameData->SamplesPending = 0;
   FrameData->FramesScheduled = 0;
   FrameData->FramesProcessed = 0;
   FrameData->SampsPerFrame  = samplesPerFrame;
   FrameData->WorkBytes     =  workI16 * 2;
   FrameData->pInWork       =  &workBuffer[0];
   FrameData->pOutWork      =  &workBuffer[workI16];
   FrameData->pFarWork      =  &workBuffer[workI16*2];
   FrameData->SAScratch     =  saScratch;
   FrameData->SAScratchI16  =  saScratchI16;
   FrameData->DAScratch     =  daScratch;
   FrameData->DAScratchI16  =  daScratchI16;

   FrameData->Hint           = generateInt;
   FrameData->TicksPerFrame  = TicksPerSample * samplesPerFrame;
   FrameData->ProcTimeSum    = 0;
   FrameData->ScheduledStart = 0;
   FrameData->TimeAvgCnt     = 0;
   FrameData->pkUsageTicks   = 0;    
   return 1;
}

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// GetPcmToPktQueueHead - Get the address of PCM to Pkt framing rate queue head.
//
// FUNCTION
//   This function determines the address of the PCM to Packet framing rate queue
//   head associated with the specified framing rate.
//
// RETURNS
//  Pointer to framing rate queue head.
//}
#pragma CODE_SECTION (GetPcmToPktQueueHead, "SLOW_PROG_SECT")
chanInfo_t **GetPcmToPktQueueHead (int CoreID, int SamplesPerFrame) {
   int i;
   FrameTaskData_t *FrameData;

   // Return with the head of the PCM to Packet framing rate queue associated
   // with the specified number of samples per frame.
   FrameData = &FrameTaskData[CoreID * FRAME_TASK_CNT];
   for (i=0; i<frameTaskCnt; i++, FrameData++) {
      if (SamplesPerFrame == FrameData->SampsPerFrame) 
         return &FrameData->PcmToPktHead;
   }
   AppErr ("Frame Size", SamplesPerFrame);
   return NULL;
}

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// GetPktToPcmQueueHead - Get the address of Pkt to PCM framing rate queue head.
//
// FUNCTION
//   This function determines the address of the Packet to PCM framing rate queue
//   head associated with the specified framing rate.
//
// RETURNS
//  Pointer to framing rate queue head.
//}
#pragma CODE_SECTION (GetPktToPcmQueueHead, "SLOW_PROG_SECT")
chanInfo_t **GetPktToPcmQueueHead (int CoreID, int SamplesPerFrame) {
   int i;
   FrameTaskData_t *FrameData;

   // Return with the head of the Packet to PCM framing rate queue associated
   // with the specified number of samples per frame.
   FrameData = &FrameTaskData[CoreID * FRAME_TASK_CNT];
   for (i=0; i<frameTaskCnt; i++, FrameData++) {
      if (SamplesPerFrame == FrameData->SampsPerFrame)
         return &FrameData->PktToPcmHead;
   }
   AppErr ("Frame Size", SamplesPerFrame);
   return NULL;
}

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ValidFrameSize - Determine if the frame size is valid.
//}
#pragma CODE_SECTION (ValidFrameSize, "SLOW_PROG_SECT")
int ValidFrameSize (int SamplesPerFrame) {
   int i;
   FrameTaskData_t *FrameData;

   FrameData = &FrameTaskData[DSPCore * FRAME_TASK_CNT];
   for (i=0; i<frameTaskCnt; i++, FrameData++) {
      if (SamplesPerFrame == FrameData->SampsPerFrame) return TRUE;
   }
   return FALSE;
}

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// getFrameIdx - Frame size to frame index lookup.
//}
#pragma CODE_SECTION (getFrameIdx, "SLOW_PROG_SECT")
int getFrameIdx (int SamplesPerFrame) {
   int i;
   FrameTaskData_t *FrameData;

   FrameData = &FrameTaskData[DSPCore * FRAME_TASK_CNT];
   for (i=0; i<frameTaskCnt; i++, FrameData++) {
      if (SamplesPerFrame == FrameData->SampsPerFrame) return i;
   }
   return -1;
}
#pragma CODE_SECTION (updateEcChanCount, "SLOW_PROG_SECT")
ADT_Int16 updateEcChanCount (ADT_UInt16 DspCore, ADT_UInt16 SamplesPerFrame, ADT_UInt16 EcanType, ADT_Int16 val) {

   int i;
   ADT_Int16 rv = -1;
   FrameTaskData_t *FrameData;

   if (EcanType == ACOUSTIC) return 0;

   // Return with the current Ec channel number for the core/frame, and update the
   // frame's channel number as indicated by val.
   FrameData = &FrameTaskData[DspCore * FRAME_TASK_CNT];
   for (i=0; i<frameTaskCnt; i++, FrameData++) {
      if (SamplesPerFrame != FrameData->SampsPerFrame) continue;

      if (EcanType == G168_PCM) {
         rv = FrameData->PcmEcInUseCnt;
         FrameData->PcmEcInUseCnt += val;
      } else if (EcanType == G168_PKT) {
         rv = FrameData->PktEcInUseCnt;
         FrameData->PktEcInUseCnt += val;
      }
      AppErr ("Ec Channel Cnt", (rv < 0));
      return rv;
   }
   AppErr ("Frame Size", SamplesPerFrame);
   return NULL;
}

#pragma CODE_SECTION (getMipsConservePtr, "SLOW_PROG_SECT")
MipsConserve_t *getMipsConservePtr (ADT_UInt16 SamplesPerFrame) {
   int i;
   FrameTaskData_t *FrameData;

   // Return with the pointer to the MipsConserve structure for the frame
   FrameData = &FrameTaskData[0];
   for (i=0; i<frameTaskCnt; i++, FrameData++) {
      if (SamplesPerFrame == FrameData->SampsPerFrame) {
         return  FrameData->pG168MipsConserve;
      }
   }
   AppErr ("Frame Size", SamplesPerFrame);
   return NULL;
}


//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// getSAScratch - Gets pointer to frame's scratch memory (accessed by frame size)
//}
#pragma CODE_SECTION (getSAScratch, "SLOW_PROG_SECT")
far void *getSAScratch (int SamplesPerFrame, ADT_UInt16 *lenI16) {
   int i;
   FrameTaskData_t *FrameData;

   FrameData = &FrameTaskData[DSPCore * FRAME_TASK_CNT];
   for (i=0; i< frameTaskCnt; i++, FrameData++) {
      if (SamplesPerFrame == FrameData->SampsPerFrame) {
         *lenI16 = FrameData->SAScratchI16;
         return FrameData->SAScratch;
      }
   }
   AppErr ("Frame Size", SamplesPerFrame);
   return NULL;
}

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// getDAScratch - Gets pointer to frame's scratch memory (accessed by frame size)
//}
#pragma CODE_SECTION (getDAScratch, "SLOW_PROG_SECT")
far void *getDAScratch (int SamplesPerFrame, ADT_UInt16 *lenI16) {
   int i;
   FrameTaskData_t *FrameData;

   FrameData = &FrameTaskData[DSPCore * FRAME_TASK_CNT];
   for (i=0; i< frameTaskCnt; i++, FrameData++) {
      if (SamplesPerFrame == FrameData->SampsPerFrame) {
         *lenI16 = FrameData->DAScratchI16;
         return FrameData->DAScratch;
      }
   }
   AppErr ("Frame Size", SamplesPerFrame);
   return NULL;
}

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// getConfScratch - Gets pointer to frame's scratch memory (accessed by frame size)
//}
#pragma CODE_SECTION (getConfScratch, "SLOW_PROG_SECT")
far void *getConfScratch (int SamplesPerFrame) {
   ADT_UInt16 len;   
   return getSAScratch (SamplesPerFrame, &len);
}

// tone detection scratch buffers
#pragma CODE_SECTION (getToneDetScratchPtr, "SLOW_PROG_SECT")
TDScratch_t *getToneDetScratchPtr (int SamplesPerFrame) {
   ADT_UInt16 len;
   return getSAScratch (SamplesPerFrame, &len);
}

#pragma CODE_SECTION (getCidScratchPtr, "SLOW_PROG_SECT")
far ADT_Int32 *getCidScratchPtr (int SamplesPerFrame) {
   ADT_UInt16 len;
   return getSAScratch (SamplesPerFrame, &len);
}

#pragma CODE_SECTION (updateMipsConserve, "MEDIUM_PROG_SECT")
void updateMipsConserve (ADT_UInt16 SamplesPerFrame, ADT_Word cpuUsage, ADT_Int16 level) {
   MipsConserve_t *pG168MipsConserve;
    pG168MipsConserve = getMipsConservePtr (SamplesPerFrame);
    if (pG168MipsConserve == 0) return;
    // activated based on CPU utilization. Use global variable
    // for  the time being.

    if (level == 0) {
        pG168MipsConserve->ReductionAmount = 0;
        return;
    }
    pG168MipsConserve->FrameCount += 1;
    if (cpuUsage >= conserveActivateThresh)   pG168MipsConserve->ReductionAmount = level;
    if (cpuUsage <= conserveDeActivateThresh) pG168MipsConserve->ReductionAmount = 0;
    return ;
}
//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// GpakFrameTask - G.PAK Framing task.
//
// FUNCTION
//  This function is the G.PAK Framing Task's BIOS SWI function. The Framing
//  Task performs all channel framing functions at a particular rate.
//
//     Note: There are multiple instances of this task (one per frame rate).
//
// RETURNS
//  nothing
//}
#pragma CODE_SECTION (GpakFrameTask, "MEDIUM_PROG_SECT")
static void GpakFrameTask (Arg TskData) {
   FrameTaskData_t  *Tsk, *FrameData;
   ConferenceInfo_t *Cnfr;
   chanInfo_t       *Chan; 
   ADT_UInt32       ScheduledStart;
   int i;

   ADT_UInt32 TicksToComplete;

   // Get a pointer to the task's data.
   Tsk = (FrameTaskData_t *) TskData;
   ScheduledStart = Tsk->ScheduledStart;
   Tsk->ScheduledStart = 0;

   // reset peak cpu loading statistics
   if (gblPkLoadingRst & (1 << DSPCore)) {
      FrameData = &FrameTaskData[DSPCore * FRAME_TASK_CNT];

      // Zero peak usage for all framing tasks on this core
      for (i=0; i<frameTaskCnt; i++, FrameData++) {
         FrameData->pkUsageTicks = 0;
#ifndef CPU_TEST
         *(FrameData->pCpuPkUsage) = 0;
#endif
      }

      AppErr ("Deprecated", SysAlgs.apiCacheEnable);
      gblPkLoadingRst &= ~(1 << DSPCore);
   }

   STATS (FrameStats.Frames++;)

   LogTransfer (0x2000F000, 0, 0, 0, 0, Tsk->SampsPerFrame/8);

   logTime (0x80000003ul | ((Tsk->SampsPerFrame/8)<< 8));

   // Transfer any pending channels to the task queue
   EnqueuePendingChannels (Tsk->SampsPerFrame);

   // Check for circular buffer overrun
   if (GpakAdjustChannelPointers (Tsk)) {
      if (Tsk->FramesScheduled == Tsk->FramesProcessed) return;
      STATS (FrameStats.LateErrs++;)

      //------------- Notify host of error
      if ((Tsk->Flags & FRAME_TIME_FLAG) == 0) {
         SendWarningEvent ((DSPCore << 8) | Tsk->SampsPerFrame/sysConfig.samplesPerMs, WarnFrameBuffers);
         Tsk->Flags |= FRAME_TIME_FLAG;
      }
      return;
   }

   updateMipsConserve (Tsk->SampsPerFrame, *(Tsk->pCpuUsage), sysConfig.g168MipsConserveLev);

   //-------------------------------------------------
   //           Decode processing
   logTime (0x10010000ul | ((Tsk->SampsPerFrame/8) << 8));

   // Process decode framing functions for each non conference channel.
   for (Chan = Tsk->PktToPcmHead; Chan != NULL; Chan = Chan->nextPktToPcm) {
      Chan->FrameCount++;
      logTime (0x20010000ul | ((Tsk->SampsPerFrame/8) << 8) | Chan->ChannelId);

      while ((Chan != NULL) && (AlgorithmControl (&Chan, FrmDecode) == ALG_CTRL_DEQUEUE_CMD)) ;
      if (Chan == NULL) break;

      // Process frame of channel data
      switch (Chan->channelType) {
      case conferenceMultiRate:
         // NOTE: Both encode and decode operations are performed under the decode chain because
         //       the frame's encode chain is used for conferencing.
         MultiRateCnfrFrame (Chan, Tsk->pInWork, Tsk->pOutWork, Tsk->pFarWork, Tsk->SampsPerFrame, Tsk->WorkBytes);
         Chan->pcmToPkt.FrameTimeStamp += Tsk->SampsPerFrame;
         break;

      case pcmToPcm:
         FramePcmB2PcmA  (Chan, Tsk->pInWork, Tsk->pOutWork, Tsk->pFarWork, Tsk->SampsPerFrame);
         break;

      case pcmToPacket:
         FramePktB2PcmA  (Chan, Tsk->pInWork, Tsk->pOutWork, Tsk->pFarWork, Tsk->SampsPerFrame, Tsk->WorkBytes);
         break;

      case packetToPacket:
         PktPktDecode  (Chan, Tsk->pInWork, Tsk->pOutWork, Tsk->pFarWork, Tsk->SampsPerFrame, Tsk->WorkBytes);
         break;

      case circuitData:
         FrameOutCircData (Chan, Tsk->pInWork, Tsk->pOutWork, Tsk->pFarWork, Tsk->SampsPerFrame, Tsk->WorkBytes);
         break;

      case customChannel:
         customFrameDecode (Chan, Tsk->pInWork, Tsk->pOutWork, Tsk->pFarWork, Tsk->SampsPerFrame, Tsk->WorkBytes);
         break;

      case inactive:
         break;

      default:
         AppErr ("Unknown channel type", Chan->channelType);
         break;
      }

      // Increment the channel's frame stamp
      Chan->pktToPcm.FrameTimeStamp += Tsk->SampsPerFrame;

      logTime (0x20020000ul | ((Tsk->SampsPerFrame/8) << 8) | Chan->ChannelId);

   }

   //-------------------------------------------------
   //           Custom conference processing
   for (i = 0; i < sysConfig.customCnfrCnt; i++)   {

      Cnfr = GetGpakCnfr (DSPCore, i + sysConfig.maxNumConferences);
      if (Cnfr->FrameSize != Tsk->SampsPerFrame)
         continue;

      if ((Cnfr->MemberList == NULL) && (Cnfr->CompositeList == NULL))
         continue;

      logTime (0x20050000ul | ((Tsk->SampsPerFrame/8) << 8) | (i + sysConfig.maxNumConferences));
      customProcessConference (i, Cnfr, Cnfr->Inst, Tsk->pInWork, Tsk->pOutWork);
      logTime (0x20060000ul | ((Tsk->SampsPerFrame/8) << 8) | (i + sysConfig.maxNumConferences));

   }

   //-------------------------------------------------
   //           Encode processing
   logTime (0x10020000ul | ((Tsk->SampsPerFrame/8) << 8));

   // Perform encoding framing functions for each non conference channel.
   for (Chan = Tsk->PcmToPktHead; Chan != NULL; Chan = Chan->nextPcmToPkt) {

      logTime (0x20030000ul | ((Tsk->SampsPerFrame/8) << 8) | Chan->ChannelId);

      while ((Chan != NULL) && (AlgorithmControl (&Chan, FrmEncode) == ALG_CTRL_DEQUEUE_CMD)) ;
      if (Chan == NULL) break;

      // Process frame of channel data
      switch (Chan->channelType) {
      case pcmToPcm:
         FramePcmA2PcmB (Chan, Tsk->pInWork, Tsk->pOutWork, Tsk->pFarWork, Tsk->SampsPerFrame);
         break;

      case pcmToPacket:
         FramePcmA2PktB (Chan, Tsk->pInWork, Tsk->pOutWork, Tsk->pFarWork, Tsk->SampsPerFrame);
		 #ifdef RTCP_ENABLED
         rtcp_execute_scheduled_callbacks(Chan->ChannelId);
		 #endif
         break;

      case packetToPacket:
         PktPktEncode (Chan, Tsk->pInWork, Tsk->pOutWork, Tsk->pFarWork, Tsk->SampsPerFrame);
         break;

      case circuitData:
         FrameInCircData (Chan, Tsk->pInWork, Tsk->pOutWork, Tsk->pFarWork, Tsk->SampsPerFrame);
         break;

      case customChannel:
         customFrameEncode (Chan, Tsk->pInWork, Tsk->pOutWork, Tsk->pFarWork, Tsk->SampsPerFrame, Tsk->WorkBytes);
         #ifdef RTCP_ENABLED
         rtcp_execute_scheduled_callbacks(Chan->ChannelId);
        #endif

         break;

      case inactive:
         break;

      default:
         AppErr ("Unknown channel type", Chan->channelType);
         break;
      }

      Chan->pcmToPkt.FrameTimeStamp += Tsk->SampsPerFrame;

      logTime (0x20040000ul | (Tsk->SampsPerFrame << 8) | Chan->ChannelId);
   }


   //-------------------------------------------------
   //           Conference processing
   logTime (0x10030000ul | ((Tsk->SampsPerFrame/8) << 8));

   // Service all conferences using this task's frame size.
   for (i = 0; i < sysConfig.maxNumConferences; i++)   {

      Cnfr = GetGpakCnfr (DSPCore, i);
      if (Cnfr->FrameSize != Tsk->SampsPerFrame)
         continue;
         
      logTime (0x20050000ul | ((Tsk->SampsPerFrame/8) << 8) | i);
      EnqueuePendingConferences (i);

      if ((Cnfr->MemberList == NULL) && (Cnfr->CompositeList == NULL))
         continue;


      ProcessConference (Cnfr, Cnfr->Inst, Tsk->pInWork, Tsk->pOutWork, Tsk->pFarWork, Tsk->WorkBytes);
      logTime (0x20060000ul | ((Tsk->SampsPerFrame/8) << 8) | i);

   }

   logTime (0x10040000ul | ((Tsk->SampsPerFrame/8) << 8));
  
   // Set the Host Port Interrupt if configured for the rate.
   if (Tsk->Hint != 0)
      SetHostPortInterrupt (Tsk->SampsPerFrame);

   logTime (0x10050000ul | ((Tsk->SampsPerFrame/8) << 8));

   // Measure elapsed processing time and periodically compute the CPU usage
   // estimate in units of .1 percent.
   TicksToComplete = CLK_gethtime() - ScheduledStart;

   // compute Max loading
   if (Tsk->pkUsageTicks < TicksToComplete) {
        Tsk->pkUsageTicks = TicksToComplete;
      // Report Peak usage as 1/10 percent of total CPU MIPs
#ifndef CPU_TEST
      *(Tsk->pCpuPkUsage) = (((ADT_UInt32)Tsk->pkUsageTicks * 10L) / (Tsk->TicksPerFrame/100L)) + 1;
#endif
   }

   Tsk->ProcTimeSum += (TicksToComplete >> SUM_SHIFT);
   if (TIME_AVG_CNT <= ++Tsk->TimeAvgCnt) {
      // Report CPU usage as 1/10 percent
#ifndef CPU_TEST
      *(Tsk->pCpuUsage) = ((ADT_UInt32)Tsk->ProcTimeSum * 10L) / (Tsk->TicksPerFrame/100L);
#endif
      Tsk->ProcTimeSum = 0;
      Tsk->TimeAvgCnt  = 0;
      Tsk->Flags       = 0;
   }

   // Report frame exceeded warnings
   if (Tsk->TicksPerFrame < TicksToComplete && ((Tsk->Flags & FRAME_TIME_FLAG) == 0)) {
      SendWarningEvent ((DSPCore << 8) | (Tsk->SampsPerFrame/sysConfig.samplesPerMs), WarnFrameTiming);
      Tsk->Flags |= FRAME_TIME_FLAG;
   }

#ifdef EVENT_TEST   // jdc event priority test
    if ((Tsk->SampsPerFrame == 160) && (sendTestEvent)) {
        memset(appErrBuff, 0, sizeof(appErrBuff));
        sprintf(appErrBuff,"%d", sendTestEvent);
        SendAppErrEvent (appErrBuff);
        sendTestEvent--;
    }
#endif
   STATS (
      unsigned int MaxMIPS;
      MaxMIPS = (TicksToComplete * CYCLESPERTICK * sysConfig.samplesPerMs) / (Tsk->SampsPerFrame * 1000);
      if (FrameStats.MaxMIPS < MaxMIPS) FrameStats.MaxMIPS = MaxMIPS;
   )

   logTime (0x10060000ul | ((Tsk->SampsPerFrame/8) << 8));

   LogTransfer (0x2000F100, 0, 0, 0, 0, Tsk->SampsPerFrame/8);

   if (Tsk->TicksPerFrame <= HostSWIMaxTicks * 2) {
      postRtpHostSWI (995 < *(Tsk->pCpuUsage), Tsk->TicksPerFrame);
      logTime (0x80010003ul | ((Tsk->SampsPerFrame/8) << 8));
   }

   UpdateFrameHealth(Tsk->SampsPerFrame);

   return;
}

#pragma CODE_SECTION (getCoreOffset, "SLOW_PROG_SECT")
ADT_Int32 getCoreOffset (ADT_UInt16 CoreID) {
   if (DSPTotalCores <= CoreID) SystemHalt ("Core ID %d error", CoreID);
   if (CoreID == 0 && DSPTotalCores == 1) return 0;

   return CoreOffsets [CoreID];  
}

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// GetEncodeQueue - Enqueue a channel to it's framing rate or conference queues.
//}
#define PrvEnc  chan->prevPcmToPkt
#define NxtEnc  chan->nextPcmToPkt
#define PrvDec  chan->prevPktToPcm
#define NxtDec  chan->nextPktToPcm

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// GetEncodeQueue - Get the address of PCM to Pkt framing rate queue head.
//
// FUNCTION
//   This function determines the address of the PCM to Packet framing rate queue
//   head associated with the specified framing rate.
//
// RETURNS
//  Pointer to framing rate queue head.
//}
#pragma CODE_SECTION (GetEncodeQueue, "MEDIUM_PROG_SECT")
chanInfo_t **GetEncodeQueue (int CoreID, int SamplesPerFrame) {
   int i;
   FrameTaskData_t *FrameData;

   // Return with the head of the PCM to Packet framing rate queue associated
   // with the specified number of samples per frame.
   FrameData = &FrameTaskData[CoreID * FRAME_TASK_CNT];
   for (i=0; i<frameTaskCnt; i++, FrameData++) {
      if (SamplesPerFrame == FrameData->SampsPerFrame) 
         return &PendingEncodeQueue [(CoreID * FRAME_TASK_CNT) + i];
   }
   AppErr ("Frame Size", SamplesPerFrame);
   return NULL;
}

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// GetDecodeQueue - Get the address of Pkt to PCM framing rate queue head.
//
// FUNCTION
//   This function determines the address of the Packet to PCM framing rate queue
//   head associated with the specified framing rate.
//
// RETURNS
//  Pointer to framing rate queue head.
//}
#pragma CODE_SECTION (GetDecodeQueue, "MEDIUM_PROG_SECT")
chanInfo_t **GetDecodeQueue (int CoreID, int SamplesPerFrame) {
   int i;
   FrameTaskData_t *FrameData;

   // Return with the head of the Packet to PCM framing rate queue associated
   // with the specified number of samples per frame.
   FrameData = &FrameTaskData[CoreID * FRAME_TASK_CNT];
   for (i=0; i<frameTaskCnt; i++, FrameData++) {
      if (SamplesPerFrame == FrameData->SampsPerFrame)
         return &PendingDecodeQueue [(CoreID * FRAME_TASK_CNT) + i];
   }
   AppErr ("Frame Size", SamplesPerFrame);
   return NULL;
}

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// GetPPendingEncodeQueue - Get the address of PCM to Pkt framing rate queue head.
//
// FUNCTION
//   This function determines the address of the PCM to Packet framing rate queue
//   head associated with the specified framing rate.
//
// RETURNS
//  Pointer to framing rate queue head.
//}
#pragma CODE_SECTION (GetPPendingEncodeQueue, "MEDIUM_PROG_SECT")
chanInfo_t **GetPPendingEncodeQueue (int CoreID, int SamplesPerFrame) {
   int i;
   FrameTaskData_t *FrameData;

   // Return with the head of the PCM to Packet framing rate queue associated
   // with the specified number of samples per frame.
   FrameData = &FrameTaskData[CoreID * FRAME_TASK_CNT];
   for (i=0; i<frameTaskCnt; i++, FrameData++) {
      if (SamplesPerFrame == FrameData->SampsPerFrame) 
         return &PPendingEncodeQueue [(CoreID * FRAME_TASK_CNT) + i];
   }
   AppErr ("Frame Size", SamplesPerFrame);
   return NULL;
}

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// GetPPendingDecodeQueue - Get the address of Pkt to PCM framing rate queue head.
//
// FUNCTION
//   This function determines the address of the Packet to PCM framing rate queue
//   head associated with the specified framing rate.
//
// RETURNS
//  Pointer to framing rate queue head.
//}
#pragma CODE_SECTION (GetPPendingDecodeQueue, "MEDIUM_PROG_SECT")
chanInfo_t **GetPPendingDecodeQueue (int CoreID, int SamplesPerFrame) {
   int i;
   FrameTaskData_t *FrameData;

   // Return with the head of the Packet to PCM framing rate queue associated
   // with the specified number of samples per frame.
   FrameData = &FrameTaskData[CoreID * FRAME_TASK_CNT];
   for (i=0; i<frameTaskCnt; i++, FrameData++) {
      if (SamplesPerFrame == FrameData->SampsPerFrame)
         return &PPendingDecodeQueue [(CoreID * FRAME_TASK_CNT) + i];
   }
   AppErr ("Frame Size", SamplesPerFrame);
   return NULL;
}


#undef Enc
#undef Dec

#define Enc     chan->pcmToPkt
#define EncPrv  chan->prevPcmToPkt
#define EncNxt  chan->nextPcmToPkt

#define Dec     chan->pktToPcm
#define DecPrv  chan->prevPktToPcm
#define DecNxt  chan->nextPktToPcm

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// AddtoPrePendQueue 
//
// FUNCTION
//   This function adds a channel to its corresponding PPendingEncode and PPendingDecode queues
//   It is called by the messaging task
//
//}
#pragma CODE_SECTION (AddtoPrePendQueue, "MEDIUM_PROG_SECT")
void AddtoPrePendQueue (chanInfo_t *chan) {
   chanInfo_t *First, *Last;
   chanInfo_t **EncPend, **DecPend;      // Framing task pending queues
   ConferenceInfo_t *Cnfr = NULL;
   int cnfrIdx;


   switch (chan->channelType) {
   case conferenceMultiRate:
      chan->Flags |= CF_CNFR_ADD;
      Cnfr = GetGpakCnfr (chan->CoreID, chan->PairedChannelId);
      cnfrIdx = (chan->CoreID * sysConfig.maxNumConferences) + chan->PairedChannelId;
      EncPend = &PPendingMemberQueue[cnfrIdx];
      DecPend = GetPPendingDecodeQueue (chan->CoreID, Dec.SamplesPerFrame);
      break;

   case conferencePcm:
   case conferencePacket:
      chan->Flags |= CF_CNFR_ADD;
      Cnfr = GetGpakCnfr (chan->CoreID, chan->PairedChannelId);
      cnfrIdx = (chan->CoreID * sysConfig.maxNumConferences) + chan->PairedChannelId;
      EncPend = &PPendingMemberQueue[cnfrIdx];
      DecPend = NULL;
      break;
  
   case conferenceComposite:
      cnfrIdx = (chan->CoreID * sysConfig.maxNumConferences) + chan->PairedChannelId;
      EncPend = &PPendingCompositeQueue[cnfrIdx];
      DecPend = NULL;
      break;

   default:
      EncPend = GetPPendingEncodeQueue (chan->CoreID, Enc.SamplesPerFrame);
      DecPend = GetPPendingDecodeQueue (chan->CoreID, Dec.SamplesPerFrame);
      break;
   }

   // Add channel to it's encode pre-pending queue
   if (EncPend != NULL) {
       if (*EncPend == 0) {
           EncPrv = chan;
           EncNxt = chan;
           *EncPend = chan;
       } else {
           First = *EncPend;
           Last  = (*EncPend)->prevPcmToPkt;
           EncPrv = Last;
           EncNxt = First;
           First->prevPcmToPkt = chan;
           Last->nextPcmToPkt  = chan;
           if (chan->Flags & CF_ENCODE_PRIORITY) *EncPend = chan;
       }
   }

   // Add channel to it's decode pre-pending queue
   if (DecPend != NULL) {
      if (*DecPend == 0) {
         DecPrv = chan;
         DecNxt = chan;
         *DecPend = chan;
      } else {
         First = *DecPend;
         Last  = (*DecPend)->prevPktToPcm;
         DecPrv = Last;
         DecNxt = First;
         First->prevPktToPcm = chan;
         Last->nextPktToPcm  = chan;
         if (chan->Flags & CF_DECODE_PRIORITY) *DecPend = chan;
      }
   }

   if (Cnfr) Cnfr->NumMembers++;
}

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// MovePPendToPendingQueue
//
// FUNCTION
//   This function is called by the Scheduler at the DMA-level to move channels
//   from the pre-pending queues to the pending queues.
//
//}
#pragma CODE_SECTION (transferQueue, "MEDIUM_PROG_SECT")
static inline void transferQueue (chanInfo_t **PrePending, chanInfo_t **Pending, int SamplesPerFrame) {
  chanInfo_t *FirstChan;

   if (PrePending != NULL) {
      FirstChan = *PrePending;
   }
   if ((Pending == NULL) || (FirstChan == NULL)) return;

   if (*Pending == NULL) {
      *Pending    = FirstChan;
      *PrePending = NULL;
   } else {
      AppErr ("DecPend not empty", SamplesPerFrame);
   }
}
#pragma CODE_SECTION (MovePPendToPendingQueue, "MEDIUM_PROG_SECT")
static void MovePPendToPendingQueue (int CoreID, int SamplesPerFrame) {

   chanInfo_t **EncPend,  **DecPend;     // Framing task pending queues
   chanInfo_t **EncPPend, **DecPPend;    // Framing task pre-pending queues
   int i, cnfrIdx;

   // Get pointers to encoder pre-pending and pending queues
   // to transfer pre-pending queue to pending queue
   EncPPend = GetPPendingEncodeQueue (CoreID, SamplesPerFrame);
   EncPend  = GetEncodeQueue (CoreID, SamplesPerFrame);

   transferQueue (EncPPend, EncPend, SamplesPerFrame);

   // Get pointers to decoder pre-pending and pending queues
   // to transfer pre-pending queue to pending queue
   DecPPend = GetPPendingDecodeQueue (CoreID, SamplesPerFrame);
   DecPend  = GetDecodeQueue (CoreID, SamplesPerFrame);
   transferQueue (DecPPend, DecPend, SamplesPerFrame);

   // Transfer Conference Member and Composite queues from
   // pre-pending to pending queue.
   cnfrIdx = CoreID * sysConfig.maxNumConferences;
   for (i=0; i<sysConfig.maxNumConferences; i++, cnfrIdx++) {

      // Move Conference Members from pre-pending to pending queue
      EncPPend = &PPendingMemberQueue[cnfrIdx];
      EncPend  = &PendingMemberQueue[cnfrIdx];
      transferQueue (EncPPend, EncPend, SamplesPerFrame);

      // Move Conference Composite from pre-pending to pending queue
      EncPPend = &PPendingCompositeQueue[cnfrIdx];
      EncPend  = &PendingCompositeQueue[cnfrIdx];

      transferQueue (EncPPend, EncPend, SamplesPerFrame);
   }

}

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// EnqueuePendingChannels 
//
// FUNCTION
//   This function is called by the framing tasks to move channels from 
//   their pending queues to the live framing task queues
//
//   Pending channel queues are resource protected.  Active channel queues
//   are maintaining entirely within the framing task thread.
//
//}
#pragma CODE_SECTION (EnqueuePendingChannels, "MEDIUM_PROG_SECT")
static void EnqueuePendingChannels (int SamplesPerFrame) {

   int mask;
   chanInfo_t *FirstPendEnc, *FirstPendDec, *LastPending, *LastPriority, *FirstNonPriority;

   chanInfo_t **EncPendQue, **DecPendQue;      // Framing task pending queues
   chanInfo_t **EncLiveQue, **DecLiveQue;      // Framing task active queues

   EncLiveQue = GetPcmToPktQueueHead (DSPCore, SamplesPerFrame);
   DecLiveQue = GetPktToPcmQueueHead (DSPCore, SamplesPerFrame);

   EncPendQue = GetEncodeQueue (DSPCore, SamplesPerFrame);
   DecPendQue = GetDecodeQueue (DSPCore, SamplesPerFrame);

   logTime (0x200E1000 | (SamplesPerFrame / 8));
   // Get pointers to pending channels and NULL pending list to allow
   // messaging to add more channels
   //mask = ADDRESS_lock (&queueLock.Inst);
   mask = ADDRESS_lock (&pqueueLock->Inst);
   FirstPendEnc = *EncPendQue;
   *EncPendQue = NULL;

   FirstPendDec = *DecPendQue;
   *DecPendQue = NULL;
   //ADDRESS_unlock (&queueLock.Inst, mask);
   ADDRESS_unlock (&pqueueLock->Inst, mask);


   // Add pending channels after last priority channel in active queue.
   if (FirstPendEnc != NULL) {

      LastPending = FirstPendEnc->prevPcmToPkt;
      if (*EncLiveQue == NULL) {
         LastPriority     = NULL;
         FirstNonPriority = NULL;
      } else {
         // Find last priority channel and first non-priority channel in active queue.
         while ( ((*EncLiveQue)->nextPcmToPkt != NULL) &&
                 ((*EncLiveQue)->nextPcmToPkt->Flags & CF_ENCODE_PRIORITY) )
               EncLiveQue = &((*EncLiveQue)->nextPcmToPkt);

         if ((*EncLiveQue)->Flags & CF_ENCODE_PRIORITY) {
            // Add pending channels after last non-priority entry
            LastPriority     = (*EncLiveQue);
            FirstNonPriority = (*EncLiveQue)->nextPcmToPkt;
         } else {
            LastPriority     = NULL;
            FirstNonPriority = (*EncLiveQue);
         }
         
      }
      if (LastPriority == NULL) {
         (*EncLiveQue) = FirstPendEnc;
         FirstPendEnc->prevPcmToPkt = NULL;
      } else {
         LastPriority->nextPcmToPkt = FirstPendEnc;
         FirstPendEnc->prevPcmToPkt = LastPriority;
      }

      if (FirstNonPriority == NULL) {
         LastPending->nextPcmToPkt = NULL;
      } else {
         LastPending->nextPcmToPkt      = FirstNonPriority;
         FirstNonPriority->prevPcmToPkt = LastPending;
         
      }
   }

   // Add pending channels after last priority channel in active queue.
   if (FirstPendDec != NULL) {

      LastPending = FirstPendDec->prevPktToPcm;
      if (*DecLiveQue == NULL) {
         LastPriority     = NULL;
         FirstNonPriority = NULL;
      } else {
         // Find last priority channel and first non-priority channel in active queue.
         while ( ((*DecLiveQue)->nextPktToPcm != NULL) &&
                 ((*DecLiveQue)->nextPktToPcm->Flags & CF_DECODE_PRIORITY) )
               DecLiveQue = &((*DecLiveQue)->nextPktToPcm);

         if ((*DecLiveQue)->Flags & CF_DECODE_PRIORITY) {
            // Add pending channels after last non-priority entry
            LastPriority     = (*DecLiveQue);
            FirstNonPriority = (*DecLiveQue)->nextPktToPcm;
         } else {
            LastPriority     = NULL;
            FirstNonPriority = (*DecLiveQue);
         }
         
      }
      if (LastPriority == NULL) {
         (*DecLiveQue) = FirstPendDec;
         FirstPendDec->prevPktToPcm = NULL;
      } else {
         LastPriority->nextPktToPcm = FirstPendDec;
         FirstPendDec->prevPktToPcm = LastPriority;
      }

      if (FirstNonPriority == NULL) {
         LastPending->nextPktToPcm = NULL;
      } else {
         LastPending->nextPktToPcm      = FirstNonPriority;
         FirstNonPriority->prevPktToPcm = LastPending;
         
      }
   }
   logTime (0x200E2000 | (SamplesPerFrame / 8));
}

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// EnqueuePendingConferences
//
// FUNCTION
//   This function is called by the framing tasks to move channels from 
//   their pending queues to the live framing task queues
//
//   Pending channel queues are resource protected.  Active channel queues
//   are maintaining entirely within the framing task thread.
//
//}
#pragma CODE_SECTION (EnqueuePendingConferences, "MEDIUM_PROG_SECT")
static void EnqueuePendingConferences (int cnfID) {
   int mask;
   chanInfo_t *FirstMem, *FirstCmp, *Last;

   chanInfo_t **MemPend, **CmpPend;      // Framing task pending queues
   chanInfo_t **MemLive, **CmpLive;      // Framing task active queues

   MemLive = &GetGpakCnfr (DSPCore, cnfID)->MemberList;
   MemPend = &PendingMemberQueue[(DSPCore * sysConfig.maxNumConferences) + cnfID];

   CmpLive = &GetGpakCnfr (DSPCore, cnfID)->CompositeList;
   CmpPend = &PendingCompositeQueue[(DSPCore *sysConfig.maxNumConferences) + cnfID];

   // Get pointers to pending channels and NULL pending list to allow
   // messaging to add more channels
   //mask = ADDRESS_lock (&queueLock.Inst);
   mask = ADDRESS_lock (&pqueueLock->Inst);
   FirstMem = *MemPend;
   *MemPend = NULL;

   FirstCmp = *CmpPend;
   *CmpPend = NULL;
   //ADDRESS_unlock (&queueLock.Inst, mask);
   ADDRESS_unlock (&pqueueLock->Inst, mask);

   // Transfer channels to active member queue
   if (FirstMem != NULL) {
      Last = FirstMem->prevPcmToPkt;
      FirstMem->prevPcmToPkt = NULL;
      Last->nextPcmToPkt = *MemLive;
      if (*MemLive != NULL) {
         (*MemLive)->prevPcmToPkt = Last;
      }
      *MemLive = FirstMem;
   }

   // Transfer channels to active composite queue
   if (FirstCmp != NULL) {
      Last = FirstCmp->prevPcmToPkt;
      FirstCmp->prevPcmToPkt = NULL;
      Last->nextPcmToPkt = *CmpLive;
      if (*CmpLive != NULL) {
         (*CmpLive)->prevPcmToPkt = Last;
      }
      *CmpLive = FirstCmp;
   }

}


//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// DequeueChannel - Dequeue a channel from it's framing rate/conference queues.
//}
#pragma CODE_SECTION (DequeueChannel, "MEDIUM_PROG_SECT")
void DequeueChannel (chanInfo_t *chan, chanInfo_t **AHead, chanInfo_t **BHead) {
   LogTransfer (0x1000DA00, AHead, 0, 0, 0, chan->ChannelId);
   LogTransfer (0x1000DB00, BHead, 0, 0, 0, chan->ChannelId);

   chan->algCtrl.ControlCode = ALG_CTRL_DEQUEUE_ENCD;
   if (BHead != NULL) chan->algCtrl.ControlCode |= ALG_CTRL_DEQUEUE_DECD;
   return;
}

#undef Enc
#undef Dec
#define Enc chan->pcmToPkt
#define Dec chan->pktToPcm
#pragma CODE_SECTION (DequeueChannelFromFrame, "MEDIUM_PROG_SECT")
static void DequeueChannelFromFrame (chanInfo_t **Chan, frmType_t frmType) {

   int coreID;
   chanInfo_t **QueueHead;
   chanInfo_t *chan = *Chan;
   ConferenceInfo_t *gpakCnfr;

   coreID = chan->CoreID;
   switch (frmType) {
   case FrmDecode:      QueueHead = GetPktToPcmQueueHead (coreID, Dec.SamplesPerFrame);       break;
   case FrmEncode:      QueueHead = GetPcmToPktQueueHead (coreID, Enc.SamplesPerFrame);       break;
   case CnfrMembers:    QueueHead = &GetGpakCnfr (coreID, chan->PairedChannelId)->MemberList;  break;
   case CnfrComposites: QueueHead = &GetGpakCnfr (coreID, chan->PairedChannelId)->CompositeList;  break;
   default:  break;
   }

   if (frmType != FrmDecode) {
      // Unlink channel from encoding or conference queues.
      if (PrvEnc == NULL) {
         *QueueHead = NxtEnc;             LogTransfer (0x1000D100, QueueHead, QueueHead, NxtEnc, 0, chan->ChannelId);
      } else {
         PrvEnc->nextPcmToPkt = NxtEnc;   LogTransfer (0x1000D200, QueueHead, &PrvEnc->nextPcmToPkt, NxtEnc, 0, chan->ChannelId);
      }
      if (NxtEnc != NULL) {
         NxtEnc->prevPcmToPkt = PrvEnc;   LogTransfer (0x1000D300, QueueHead, &NxtEnc->prevPcmToPkt, PrvEnc, 0, chan->ChannelId);
      }
      // Update framing task to continue with next channel after the dequeued channel
      *Chan = NxtEnc;

      PrvEnc = NULL;
      NxtEnc = NULL;
   } else {
      // Unlink the channel from decoding queues.
      if (PrvDec == NULL) {
         *QueueHead = NxtDec;             LogTransfer (0x1000D400, QueueHead, QueueHead, NxtDec, 0, chan->ChannelId);
      } else {
         PrvDec->nextPktToPcm = NxtDec;   LogTransfer (0x1000D500, QueueHead, &PrvDec->nextPktToPcm, NxtDec, 0, chan->ChannelId);
      }
 
      if (NxtDec != NULL) {
         NxtDec->prevPktToPcm = PrvDec;   LogTransfer (0x1000D600, QueueHead, &NxtDec->prevPktToPcm, PrvDec, 0, chan->ChannelId);
      }
      // Update framing task to continue with next channel after the dequeued channel
      *Chan = NxtDec;

      PrvDec = NULL;
      NxtDec = NULL;
   }

   // Remove conference member channels from the conference
   if (frmType == CnfrMembers) {
      gpakCnfr = GetGpakCnfr (coreID, chan->PairedChannelId);
      CONF_ADT_removeMember (gpakCnfr->Inst, ConfMemberData[chan->ChannelId]);
      CLEAR_INST_CACHE (ConfMemberData[chan->ChannelId], sizeof (conf_Member_t));
      CLEAR_INST_CACHE (ConfVadData[chan->ChannelId],    sizeof (VADCNG_Instance_t));
      chan->PairedChannelId = NULL_CHANNEL;

      // Clear cache of conference instance data so that core0 may re-initialize
      if ((gpakCnfr->MemberList == NULL) &&  (gpakCnfr->CompositeList == NULL)) {
         CLEAR_INST_CACHE (gpakCnfr->Inst,        sizeof (ConfInstance_t));
         CLEAR_INST_CACHE (gpakCnfr->AgcInstance, sizeof (AGCInstance_t));
         CLEAR_INST_CACHE (gpakCnfr->TG_NB.toneGenPtr,  sizeof (TGInstance_t));
         CLEAR_INST_CACHE (gpakCnfr->TG_WB.toneGenPtr,  sizeof (TGInstance_t));
      }
      FLUSH_wait ();
      gpakCnfr->NumMembers--;
   }
   return;
}
#undef Enc
#undef Dec



//=============================================================================
// 
//   C54 compatibility routines
//    
//=============================================================================
#if (DSP_TYPE == 54)
int G723_ADT_getSize4Decoder (ADT_UInt16 *Payload) {
  int Info;

  Info = Payload[0] & 0x0003;

  /* Get rate information */
  switch (Info) {
  case 0: return 23;    /* Active frame, high rate */
  case 1: return 19;    /* Active frame, low rate */
  case 2: return  3;    /* Sid Frame */
  default: return 0;    /* untransmitted */
  }
}
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void G729_Encoder_Init (ADT_Int16 *mem) {
   CODG729_ADT_Channel_t *Channel; 
   CODG729_ADT_I_t *I;
   CODG729_ADT_E_t *E;
   CODG729_ADT_H_t *H;
   CODG729_ADT_B_t *B;
   
   Channel = (CODG729_ADT_Channel_t *) mem;
   I = (CODG729_ADT_I_t *) (Channel + 1);
   E = (CODG729_ADT_E_t *) (I + 1);
   H = (CODG729_ADT_H_t *) (E + 1);
   B = (CODG729_ADT_B_t *) (H + 1);

   codInitializeG729 (Channel, I, E, H, B);
}
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void G729_Decoder_Init (ADT_Int16 *mem) {
   DECG729_ADT_Channel_t *Channel; 
   DECG729_ADT_I_t *I;
   DECG729_ADT_E_t *E;
   DECG729_ADT_H_t *H;
   DECG729_ADT_B_t *B;
   
   Channel = (DECG729_ADT_Channel_t *) mem;
   I = (DECG729_ADT_I_t *) (Channel + 1);
   E = (DECG729_ADT_E_t *) (I + 1);
   H = (DECG729_ADT_H_t *) (E + 1);
   B = (DECG729_ADT_B_t *) (H + 1);

   decInitializeG729 (Channel, I, E, H, B);
}
#endif

//}=============================================================================
//++++++++++ Packet building
//{==============================================================================
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// PayloadToCirc - Places a payload with header in a circular buffer.
//
#pragma CODE_SECTION (PayloadToCirc, "MEDIUM_PROG_SECT")
int PayloadToCirc (CircBufInfo_t *circ, void *Hdr, int HdrI16, ADT_UInt8 *Payload, int PyldI8) {

   // Circular buffer info
   unsigned int PutIndex, TakeIndex, BufrSize;

   int WdsFree, WdsRemaining, WdsTillEnd;  
   int PayloadWds, HdrWds, PacketWds;   

   ADT_Word *pBufr, *pHdr, *pPayld;

   pPayld     = (ADT_Word *) Payload;
   PutIndex   = SzInt16ToHost (circ->PutIndex);
   TakeIndex  = SzInt16ToHost (circ->TakeIndex);
   BufrSize   = SzInt16ToHost (circ->BufrSize);

   pBufr      = (ADT_Word *) circ->pBufrBase;
   pBufr     += PutIndex;

   // NOTE: Circular buffers are normally sized and indexed as 16-bits per word
   //       We Convert bufrSize from count of 16-bit words to count of DSP words
   //       Casting pBufr as ADT_Word changes indexing to conform to DSP word sizing.
   if (TakeIndex <= PutIndex)
      WdsFree = BufrSize - 1 - (PutIndex - TakeIndex);
   else
      WdsFree = TakeIndex - PutIndex - 1;

   // Convert payload length in bytes to host access word size
   PayloadWds = SzInt16ToHost ((PyldI8 + 1) / 2);
   HdrWds     = SzInt16ToHost (HdrI16);
   PacketWds  = HdrWds + PayloadWds;

   if (WdsFree < PacketWds) {
      LogTransfer (0x4FF1C, 0, Hdr, circ, &circ->pBufrBase [circ->PutIndex],
                SzHostToInt16 (PacketWds));
      return (0);
   }

   WdsTillEnd = BufrSize - PutIndex;

   if (WdsTillEnd < HdrWds)   {
      // Copy header into circular buffer
      pHdr = (ADT_Word *) Hdr;
      wdcpy (pBufr, pHdr, WdsTillEnd);
      pHdr += WdsTillEnd;
      WdsRemaining = HdrWds - WdsTillEnd;

      pBufr = (ADT_Word *) circ->pBufrBase;      // Wrap circular buffer
      wdcpy (pBufr, pHdr, WdsRemaining);
      pBufr += WdsRemaining;

   } else {
      // Copy header into circular buffer
      wdcpy (pBufr, Hdr, HdrWds);
      WdsTillEnd -= HdrWds;
      
      if (WdsTillEnd == 0)      {
         WdsTillEnd = BufrSize;
         pBufr = (ADT_Word *) circ->pBufrBase;   // Wrap circular buffer
      } else
         pBufr += HdrWds;

      // Copy start of payload into circular buffer
      if (WdsTillEnd < PayloadWds)   {
         wdcpy (pBufr, pPayld, WdsTillEnd);

         pBufr   = (ADT_Word *) circ->pBufrBase;    // Wrap circular buffer
         pPayld     += WdsTillEnd;
         PayloadWds -= WdsTillEnd;
      }
   }
   // Copy remainder of payload into circular buffer
   wdcpy (pBufr, pPayld, PayloadWds);

   LogTransfer (0x04001C, 0, Hdr, circ, &circ->pBufrBase [SzHostToInt16 (PutIndex)],
                SzHostToInt16 (PacketWds));

   PutIndex += PacketWds;
   if (BufrSize <= PutIndex)
      PutIndex -= BufrSize;

   circ->PutIndex = SzHostToInt16 (PutIndex);

   return 1;
}

#pragma CODE_SECTION (PayloadToCircCache, "MEDIUM_PROG_SECT")
int PayloadToCircCache (CircBufInfo_t *circ, void *Hdr, int HdrI16, ADT_UInt8 *Payload, int PyldI8) {

   // Circular buffer info
   unsigned int PutIndex, TakeIndex, BufrSize;

   int WdsFree, WdsRemaining, WdsTillEnd;  
   int PayloadWds, HdrWds, PacketWds;   

   ADT_Word *pBufr, *pHdr, *pPayld;

   if (SysAlgs.apiCacheEnable) {
      AppErr ("Deprecated", ADT_TRUE);
   }

   pPayld     = (ADT_Word *) Payload;
   PutIndex   = SzInt16ToHost (circ->PutIndex);
   TakeIndex  = SzInt16ToHost (circ->TakeIndex);
   BufrSize   = SzInt16ToHost (circ->BufrSize);

   pBufr      = (ADT_Word *) circ->pBufrBase;
   pBufr     += PutIndex;

   // NOTE: Circular buffers are normally sized and indexed as 16-bits per word
   //       We Convert bufrSize from count of 16-bit words to count of DSP words
   //       Casting pBufr as ADT_Word changes indexing to conform to DSP word sizing.
   if (TakeIndex <= PutIndex)
      WdsFree = BufrSize - 1 - (PutIndex - TakeIndex);
   else
      WdsFree = TakeIndex - PutIndex - 1;

   // Convert payload length in bytes to host access word size
   PayloadWds = SzInt16ToHost ((PyldI8 + 1) / 2);
   HdrWds     = SzInt16ToHost (HdrI16);
   PacketWds  = HdrWds + PayloadWds;

   if (WdsFree < PacketWds) {
      LogTransfer (0x4FF1C, 0, Hdr, circ, &circ->pBufrBase [circ->PutIndex], SzHostToInt16 (PacketWds));
      return 0;
   }

   WdsTillEnd = BufrSize - PutIndex;

   if (WdsTillEnd < HdrWds)   {
      // Copy header into circular buffer
      pHdr = (ADT_Word *) Hdr;
      CACHE_INV   (pBufr, SzHostToBytes (WdsTillEnd));
      wdcpy (pBufr, pHdr, WdsTillEnd);
      CACHE_WBINV (pBufr, SzHostToBytes (WdsTillEnd));
      pHdr += WdsTillEnd;
      WdsRemaining = HdrWds - WdsTillEnd;

      pBufr = (ADT_Word *) circ->pBufrBase;      // Wrap circular buffer
      CACHE_INV   (pBufr, SzHostToBytes (WdsRemaining));
      wdcpy (pBufr, pHdr, WdsRemaining);
      CACHE_WBINV (pBufr, SzHostToBytes (WdsRemaining));
      pBufr += WdsRemaining;

   } else {
      // Copy header into circular buffer
      CACHE_INV   (pBufr, SzHostToBytes (HdrWds));
      wdcpy (pBufr, Hdr, HdrWds);
      CACHE_WBINV (pBufr, SzHostToBytes (HdrWds));
      WdsTillEnd -= HdrWds;
      
      if (WdsTillEnd == 0)      {
         WdsTillEnd = BufrSize;
         pBufr = (ADT_Word *) circ->pBufrBase;   // Wrap circular buffer
      } else
         pBufr += HdrWds;

      // Copy start of payload into circular buffer
      if (WdsTillEnd < PayloadWds)   {
         CACHE_INV   (pBufr, SzHostToBytes (WdsTillEnd));
         wdcpy (pBufr, pPayld, WdsTillEnd);
         CACHE_WBINV (pBufr, SzHostToBytes (WdsTillEnd));

         pBufr   = (ADT_Word *) circ->pBufrBase;    // Wrap circular buffer
         pPayld     += WdsTillEnd;
         PayloadWds -= WdsTillEnd;
      }
   }
   // Copy remainder of payload into circular buffer
   if (PayloadWds != 0) {
      CACHE_INV   (pBufr, SzHostToBytes (PayloadWds));
      wdcpy (pBufr, pPayld, PayloadWds);
      CACHE_WBINV (pBufr, SzHostToBytes (PayloadWds));
   }

   LogTransfer (0x4001C, 0, Hdr, circ, &circ->pBufrBase [SzHostToInt16 (PutIndex)],
                SzHostToInt16 (PacketWds));

   PutIndex += PacketWds;
   if (BufrSize <= PutIndex)
      PutIndex -= BufrSize;

   circ->PutIndex = SzHostToInt16 (PutIndex);
   if (SysAlgs.apiCacheEnable) {
      AppErr ("Deprecated", ADT_TRUE);
   }

   return 1;
}

#pragma CODE_SECTION (getLoopbackBufr, "SLOW_PROG_SECT")
CircBufInfo_t *getLoopbackBufr (chanInfo_t *chan, PktHdr_t *Hdr) {
   ADT_UInt16 chID;
   // This routine changes the channel to which the packet is directed according to the loopback mode

   //  PacketLoopback retains channel number to return packet to same channel
   //  PacketCrossover pairs even and odd channels
   //  PacketDaisyChain forms channel link as follows.  x.y are paired decode.encode channels
   //  
   //  PcmPkt                                                          PcmPkt
   //    0  <->  1:2  <->  3:4  <->  5:6  <->  ...  <->  n-2:n-1  <->    n
   // PCM to Pkt channels are at ends to terminate chains
   chID = Hdr->ChanId;
   if (RTPLoopBack == PacketCrossOver) {
      Hdr->ChanId = chID ^ 1;   // Perform odd/even channel crossover
   } else if (RTPLoopBack == PacketDaisyChain) {
      switch (chanTable[chID]->channelType) {
      case pcmToPacket:
      case packetToPacket: 
         if (chID & 1) chID -= 1;
         else          chID += 1;
         break;

      default:
         break;
      }
      Hdr->ChanId = chID;
   }
   chan = chanTable[Hdr->ChanId];
   return chan->pktToPcm.inbuffer;
}
//-----------------------------------------------
//  SendPktToHost Adjust timestamp for the following scenarios
//
//  Tone relay    - tone to packet
//  Pkt fowarding - tone or cng packet to 
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma CODE_SECTION (SendPktToHost, "MEDIUM_PROG_SECT")
int SendPktToHost (chanInfo_t *chan, PktHdr_t *Hdr, ADT_UInt16 *Payload) {
   int delivered, rtpSamplingRate;
   ADT_UInt40 timeStamp40;
   void *PktCirc;

   pcm2pkt_t *PcmPkt;
   PcmPkt = &chan->pcmToPkt;

   // -  Deterime time stamp
   //
   // Event or tone packet
   //     For tone relay     - ToneTimeStamp set at first tone detect to frameTimeStamp - duration.
   //     For packet relay   - ToneTimeStamp set to tone's playout time (from jitter buffer).
   //     For host insertion - ToneTimeStamp set to frameTimeStamp at time the packet was generated.
   //
   // Non-tone packets cause ToneTimeStamp to return to zero.
   //
   // All others
   //     Use channel's frame time stamp.
   //     

   if (Hdr->PayldType == EventPkt) {
      timeStamp40 = SendToneTimeStamp;
   } else {
      timeStamp40 = PcmPkt->FrameTimeStamp;
   }

   // Samplng rate adjustment
   rtpSamplingRate = chan->EncoderRate;
   if (PcmPkt->Coding == G722_64) {
      if (TDMRate == 16000) timeStamp40 /= 2;
   } else if (TDMRate < rtpSamplingRate) {  // Narrowband scheduling, wideband codec
      timeStamp40 *= (rtpSamplingRate / TDMRate);
   } else if (rtpSamplingRate < TDMRate) { // Narrowband codec, wideband scheduling
      timeStamp40 /= (TDMRate / rtpSamplingRate);
   }
   Hdr->TimeStamp = (ADT_UInt32) timeStamp40;

   STATS (if (Hdr->PayldClass == PayClassT38) FrameStats.pktOutT38++;)

   //  If RTP is to be delivered -> format RTP packet and place in RTP buffer
   //  otherwise -> place payload in channel's circular buffer
   if (SysAlgs.rtpHostDelivery || SysAlgs.rtpNetDelivery) {
      if (chan->Flags & CF_RTP_TX_DISABLED)
         return 1;

      delivered = sendPacketToRTP (chan, Hdr, Payload);
   } else {
      ADT_UInt8 *payload;
      PktCirc = PcmPkt->outbuffer;
      if (RTPLoopBack != DisableTest) {
         // Loopback packets
         PktCirc = getLoopbackBufr (chan, Hdr);
      }
      payload = customPktOut (&Hdr, Payload);
      delivered = PayloadToCircCache (PktCirc, Hdr, PKT_HDR_I16LEN, payload, Hdr->PayldBytes);
   }

   logTime (0x04090000 | chan->ChannelId);

   if (delivered == 0) {
      chan->status.Bits.packetOverflow = 1;
      chan->overflowCount++;
   } else {
      chan->toHostPktCount++;
   }

   return delivered;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma CODE_SECTION (buildAudioPacket, "MEDIUM_PROG_SECT")
static void buildAudioPacket (chanInfo_t *chan, ADT_UInt16 *params, ADT_UInt16 *payload,
                          int shift, ADT_UInt16 width, packFormat packing, int paramCnt) {
   PktHdr_t  PktHdr;      // packet header
   int i;               // loop index / counter
   pcm2pkt_t *PcmPkt;
   PcmPkt = &chan->pcmToPkt;


   // Build the packet header.
   PktHdr.ChanId     = chan->ChannelId;
   PktHdr.PayldClass = PayClassAudio;
   PktHdr.PayldType  = PcmPkt->Coding;

   // Mask off LSBs of parameters if shift > 0.
   if (packing == PK_None) {
      PktHdr.PayldBytes = paramCnt;
      SendPktToHost (chan, &PktHdr, params);
      STATS (FrameStats.pktOutVoice++;)
      return;
   }

   if (0 < shift) {
      for (i = 0; i < paramCnt; i++)
         params[i] >>= shift;
   }


   // Pack 16-bit parameters into bytes.
   if (packing == PK_MSB) {
      packMSB (params, payload, paramCnt, width);
      PktHdr.PayldBytes = ((paramCnt * width) + 7) / 8;
   } else if (packing == PK_LSB) {
#if (DSP_TYPE == 55)
      PackG711(params, payload, paramCnt); // simple code to speed up packing
#else
      packLSB (params, payload, paramCnt, width);
#endif
      PktHdr.PayldBytes = ((paramCnt * width) + 7) / 8;
#ifdef G726_DEBUG
      if (g726_enc_print){
      unsigned char *pPay = (unsigned char *)payload; 
          g726_enc_print--;
          for(i = 0; i <PktHdr.PayldBytes; i++) {
              printf("ENC[%d] %x, ENC[%d] %x, PKT[%d] %x\n",2*i, params[2*i], 2*i+1, params[2*i+1], i, pPay[i]);
          }
      }
#endif
   } else if (packing == PK_AMR) {
      PktHdr.PayldBytes = formatAMRPayload (params, payload);
      PktHdr.PayldType  = AMR_BASE;
   } else if (packing == PK_CUSTOM)
      PktHdr.PayldBytes = customPack (params, payload);

      
   // Send the packet to the host.
   SendPktToHost (chan, &PktHdr, payload);
   
   STATS (FrameStats.pktOutVoice++;)
   return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma CODE_SECTION (buildAAL2TonePacket, "MEDIUM_PROG_SECT")
static void buildAAL2TonePacket (chanInfo_t *pChan, TRDetectResults_t* tone, GpakPayloadClass pyClass) {

   PktHdr_t PktHdr;                   // packet header
   ADT_UInt32 Payload[(AAL2_DIGITS_PAYLOAD_WORDS+1)/2];   // packet payload

   ADT_Int16 ToneIndex;
   ADT_UInt32 time, deltaTime;

   ADT_UInt16 *pPayload;


   ADT_UInt16 amp;
   ADT_UInt16 DigitType;      // AAL2 digit type
   ADT_UInt16 DigitCode;      // AAL2 digit code
   pcm2pkt_t *PcmPkt;
   PcmPkt = &pChan->pcmToPkt;

   pPayload = (ADT_UInt16 *)Payload;
    
   // Build the packet header.
   PktHdr.ChanId     = pChan->ChannelId;
   PktHdr.PayldClass = pyClass;
   PktHdr.PayldType  = DialedDigitsPkt;
   PktHdr.PayldBytes   = AAL2_DIGITS_PAYLOAD_LEN;

   // Build the packet payload.
    ToneIndex = tone->ToneIndex;
    if (ToneIndex == tdsNoToneDetected)   {
        // negate the status bit setting due to a invalid ToneIndex 
        pChan->status.Bits.invalidToneDetected = 0;  

        //set Digit code to indicate end of tone
        DigitCode = 31; //"11111"
        DigitType = 0;  // DTMF, not to conflict with R1 code 31

        time = SendToneTimeStamp + (tone->TimeElapsed >> 2);
        SendToneTimeStamp = 0;
    } else { 
        time = PcmPkt->FrameTimeStamp;
        deltaTime = time - SendToneTimeStamp;
        
        // Throttle to 3 every 500 milliseconds
        if ((SendToneTimeStamp != 0) && 
             PcmPkt->SamplesPerFrame * 3 < deltaTime && deltaTime < (sysConfig.samplesPerMs * 500)) return;
        if ((sysConfig.samplesPerMs * 500) < deltaTime) SendToneTimeStamp = time;

        if ((0 <= ToneIndex) && (ToneIndex <= 15))  {       // DTMF Digit type = 0
          DigitType = 0;
          DigitCode = relayToPacketDTMFTbl[ToneIndex];
           
       }  else if ((20 <= ToneIndex) && (ToneIndex <= 34)) {  // MFR1 Digit type = 1
          DigitType = 1;
          DigitCode = rlyToPktAAL2MFR1Tbl[ToneIndex - 20];
          
       } else if ((40 <= ToneIndex) && (ToneIndex <= 54))  {  // MFR2 Fwd Digit type = 2
          DigitType = 2;
          DigitCode = ToneIndex - 39;
       } else if ((60 <= ToneIndex) && (ToneIndex <= 74))  {  // MFR2 Rev Digit type = 3
          DigitType = 3;
          DigitCode = ToneIndex - 59;
       } else   {  // Invalid Tone Index detected
          pChan->status.Bits.invalidToneDetected = 1;
          return;
       }
    }

    time = ((time >> 1) & 0x3FFF);
    // swap high and low bytes
    pPayload[0] = ntohs (time);

    // Signal Power: bits 4..0 of 1st octet
    amp = tone->Amplitude ; 
    if (amp > 31)
        amp = 31;
    pPayload[1] = (amp & 0x001F) | (DigitType << 13) | ((DigitCode & 0x001F) << 8);

   // Attempt to send the packet to the host.
   SendPktToHost (pChan, &PktHdr, pPayload);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma CODE_SECTION (buildRTPL16Packet, "MEDIUM_PROG_SECT")
static void buildRTPL16Packet (chanInfo_t *pChan, ADT_UInt16 *payload, ADT_UInt16 *pkt, GpakCodecs Codec) {
   PktHdr_t PktHdr; 
   int i;           

   // Build the packet header.
   PktHdr.ChanId     = pChan->ChannelId;
   PktHdr.PayldClass = PayClassAudio;
   PktHdr.PayldType  = Codec;
   PktHdr.PayldBytes = pChan->pcmToPkt.OctetsPerPacket;

   // Move linear 16 data words into words of the payload buffer with most
   // significant byte first.
   for (i = 0; i < (PktHdr.PayldBytes / 2); i++)
      pkt[i] = ntohs (payload[i]);

   // Attempt to send the packet to the host.
   SendPktToHost (pChan, &PktHdr, pkt);
   return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma CODE_SECTION (buildCngPacket, "MEDIUM_PROG_SECT")
static void buildCngPacket (chanInfo_t *pChan) {
   PktHdr_t PktHdr;             // packet header
   ADT_UInt32 Payload[(SID_PAYLOAD_WORDS+1)/2];   // packet payload

   ADT_UInt16 *pPayload;

    pPayload = (ADT_UInt16 *)Payload;   

   // Build the packet header.
   PktHdr.ChanId     = pChan->ChannelId;
   PktHdr.PayldClass = PayClassSilence;
   PktHdr.PayldType  = 0;
   PktHdr.PayldBytes   = SID_PAYLOAD_LEN;

   // Build the payload.
   pPayload[0] = (pChan->pcmToPkt.SIDPower & 0x007F);

   // Attempt to send the packet to the host.
   SendPktToHost (pChan, &PktHdr, pPayload);

   STATS (FrameStats.pktOutCng++;)
   return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma CODE_SECTION (buildRTPEventPacket, "MEDIUM_PROG_SECT")
static void buildRTPEventPacket (chanInfo_t *chan, TRDetectResults_t* tone, GpakPayloadClass pyClass) {

   PktHdr_t PktHdr;            // packet header
   ADT_UInt32 Payload[(RTP_EVT_PAYLOAD_WORDS+1)/2];   // packet payload

   ADT_UInt16 timeElapsed;
   ADT_Int16 ToneIndex;
   ADT_UInt16 *pPayload;
#ifdef TONE_LOGGING // Tone logging
   pcm2pkt_t *PcmPkt;
   PcmPkt = &chan->pcmToPkt;
#endif
   pPayload = (ADT_UInt16 *)Payload;   

   // Build the packet header.
   PktHdr.ChanId     = chan->ChannelId;
   PktHdr.PayldClass = pyClass;
   PktHdr.PayldType  = EventPkt;
   PktHdr.PayldBytes = RTP_EVT_PAYLOAD_LEN;

   timeElapsed = tone->TimeElapsed;   // Time elapsed is kept in the Codec's units.

   PktHdr.TimeStamp  = timeElapsed;
 
   // Build the payload.
   ToneIndex = tone->ToneIndex;

   // Signal Power: bits 0..5 of 2nd octet
   pPayload[0] = (tone->Amplitude & 0x003F) << 8;

   // Bit 6 of 2nd octet: reserved: 0
   // Bit 7 of 2nd octet: EndOfEvent
   if (pyClass == PayClassEndTone) {
      pPayload[0] |= 0x8000;
   }

   // Bits 0..7 of 1st octet: Event Code
   if ((0 <= ToneIndex) && (ToneIndex <= 15))
      pPayload[0] |= relayToPacketDTMFTbl[ToneIndex];
   else  if ((20 <= ToneIndex) && (ToneIndex <= 34))      //TR:mf0:14
      pPayload[0] |= relayToPacketMFR1Tbl[ToneIndex - 20];
   else  {
     // Invalid Tone Index detected
     chan->status.Bits.invalidToneDetected = 1;
     return;
   }

   pPayload[1] = ntohs (timeElapsed);

#ifdef TONE_LOGGING // Tone logging
   logTone (TONET, chan->ChannelId, pyClass, pPayload[0], pPayload[1], PcmPkt->FrameTimeStamp, SendToneTimeStamp);
#endif

   // Attempt to send the packet to the host.
   SendPktToHost (chan, &PktHdr, pPayload);

   // send repeat packets at end of tone
   if (pyClass == PayClassEndTone) {
       SendPktToHost (chan, &PktHdr, pPayload);
       SendPktToHost (chan, &PktHdr, pPayload);
   }

}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma CODE_SECTION (buildRTPTonePacket, "MEDIUM_PROG_SECT")
static void buildRTPTonePacket (chanInfo_t *chan, TRDetectResults_t* tone, GpakPayloadClass pyClass) {
   PktHdr_t PktHdr;                   // packet header
   ADT_UInt32 Payload[(RTP_TONE_PAYLOAD_WORDS+1)/2];   // packet payload
   ADT_UInt16 timeElapsed;

   ADT_Int16 ToneIndex;
   ADT_UInt16 Freq1;   // tone frequency 1
   ADT_UInt16 Freq2;   // tone frequency 2
   ADT_UInt16 *pPayload;
   pcm2pkt_t *PcmPkt;
   PcmPkt = &chan->pcmToPkt;

   pPayload = (ADT_UInt16 *)Payload;   

   if (chan->EncoderRate == 16000) 
      timeElapsed = tone->TimeElapsed * 2;
   else
      timeElapsed = tone->TimeElapsed;


   // Build the packet header.
   PktHdr.ChanId = chan->ChannelId;
   PktHdr.PayldClass = pyClass;
   PktHdr.PayldType = TonePkt;
   PktHdr.PayldBytes = RTP_TONE_PAYLOAD_LEN;
   PktHdr.TimeStamp  = timeElapsed - (PcmPkt->FrameTimeStamp - SendToneTimeStamp);


   // Build the payload.
   ToneIndex = tone->ToneIndex;

   // indicate usage of tone freqs for regeration instead of tone index
   if ( !((( 0 <= ToneIndex) && (ToneIndex <= 15)) ||
          ((20 <= ToneIndex) && (ToneIndex <= 34)) ) ) {
      // Invalid Tone Index detected
      chan->status.Bits.invalidToneDetected = 1;
      return;
    }

   // Signal Power: bits 0..5 of 2nd octet (first 10 bits are 0)
   pPayload[0] =  (tone->Amplitude & 0x003F) << 8;
   pPayload[1] = ntohs (PktHdr.TimeStamp);

   Freq1 = TR_ADT_toneTable[ToneIndex][0];
   Freq2 = TR_ADT_toneTable[ToneIndex][1];
   pPayload[2] = ntohs (Freq1);
   pPayload[3] = ntohs (Freq2);

   // Attempt to send the packet to the host.
   SendPktToHost (chan, &PktHdr, pPayload);

}
static inline toneToHost (int chanId, GpakDeviceSide_t deviceSide,
                         ADT_Int16 toneIndex, ADT_UInt16 durationMs) {
    EventFifoMsg_t eventMsg;
 
    eventMsg.header.eventCode =  EventToneDetect;
    eventMsg.header.eventLength = EVENT_FIFO_TONE_MSGLEN_BYTES; 
    eventMsg.header.channelId = chanId;
    eventMsg.header.deviceSide = deviceSide;
    eventMsg.payload[0] = (ADT_UInt16) toneIndex;
    eventMsg.payload[1] = durationMs;
    writeEventIntoFifo (&eventMsg);
    STATS (FrameStats.tonesToHost++;)
}
//}========================================================================
//++++++++++ Packet parsing
//{==============================================================================
#pragma CODE_SECTION (parse2833Tone, "MEDIUM_PROG_SECT")
ADT_Bool parse2833Tone (TGInfo_t* tgInfo, TRGenerateEvent_t* toneEvt, ADT_UInt16 *pyld) {
   // test the modulation field. If it's non-zero then we can't
   // process this packet since we don't support modulated tones.
    if (pyld[0] & 0xC0FF) return 0;

//   if (tgInfo->regenActive == 0)   toneEvt->TimeElapsed = 0;
   toneEvt->StartBit = 0;
   toneEvt->SampleRate = 8000;
    
   toneEvt->Amplitude = (pyld[0] >> 8) & 0x3f;
   toneEvt->TimeElapsed      = ntohs (pyld[1]);
   toneEvt->ToneFrequency[0] = ntohs (pyld[2]);
   toneEvt->ToneFrequency[1] = ntohs (pyld[3]);
   toneEvt->UseIndex = 0;
   return 1;
}

#pragma CODE_SECTION (parse2833Event, "MEDIUM_PROG_SECT")
ADT_Bool parse2833Event (TRGenerateEvent_t* toneEvt, ADT_UInt16 *pyld,
                        int SamplesPerFrame) {
   ADT_UInt16 tone;

   tone = (pyld[0] & 0xff);

   toneEvt->UseIndex = 1;

   toneEvt->StartBit = 0;
   toneEvt->SampleRate = 8000;

   // Convert RFC2833 tone id to ADT tone index
   if       (                 tone <= 15)   toneEvt->ToneIndex = packetToRelayDTMFTbl[tone];
   else if ((128 <= tone) && (tone <= 141)) toneEvt->ToneIndex = packetToRelayMFR1Tbl[tone-128];
   else {
      // Set frequencies for tone types not supported by ADT's tone relay generator
      if      (tone == 32) toneEvt->ToneIndex = 90;  // fax CED tone
      else if (tone == 36) toneEvt->ToneIndex = 91;  // fax CNG tone
      else if (tone == 39) toneEvt->ToneIndex = 92;  // V.21 Channel 2 "0" bit
      else if (tone == 40) toneEvt->ToneIndex = 93;  // V.21 Channel 2 "1" bit
      else return 0;
   }

   toneEvt->Amplitude   =  (pyld[0] >> 8) & 0x3f;
   toneEvt->TimeElapsed =  ntohs (pyld[1]);
   return 1;
}

#pragma CODE_SECTION (parseAAL2Tone, "MEDIUM_PROG_SECT")
ADT_Bool parseAAL2Tone (TRGenerateEvent_t* toneEvt, ADT_UInt16 *pyld,
                        ADT_UInt40 *pRegenToneTimeStamp, int SamplesPerFrame) {

   ADT_UInt16 digitType, digitCode;
   ADT_Int16 toneIndex;
   ADT_Int32 TimeLong;
   ADT_UInt16 tmp;

   digitType = (pyld[1]>>13) &  0x7;
   digitCode = (pyld[1]>>8) & 0x1f;
   toneEvt->Amplitude =  pyld[1] & 0x1f;

   // Convert AAL2 digit type and code to ADT tone index
   if ((0 == digitType) && (digitCode <= 15))      // dtmf
      toneIndex = (ADT_Int16) packetToRelayDTMFTbl[digitCode]; 
   else if ((1 == digitType) && (digitCode <= 14)) // mfr1
      toneIndex = (ADT_Int16) pktToRlyAAL2MFR1Tbl[digitCode];
   else if (((2 == digitType) || (3 == digitType)) && (1 <= digitCode) && (digitCode <= 15)) // mfr2 fwd/rev
      toneIndex = (20 * digitType) + digitCode-1;
   else {
      toneIndex = tdsNoToneDetected;
   }

   // swap low and high bytes of time stamp
   tmp = ntohs (pyld[0]);

   if (digitCode == 31) {
      // put the delta(TimeStamp) in the time elapsed field
      TimeLong = (long)tmp  & 0x3FFF;
      toneEvt->TimeElapsed = (TimeLong - *pRegenToneTimeStamp)<<3;
      toneIndex = tdsNoToneDetected;
   } else {  // 1st tone packet 
      toneEvt->ToneIndex   = toneIndex;
      toneEvt->TimeElapsed = SamplesPerFrame;

      *pRegenToneTimeStamp = tmp & 0x3FFF;; // lower 14 bits 
   }
   toneEvt->UseIndex = 1;
   return (toneIndex != tdsNoToneDetected);
}

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// GetPktFromCircBuffer - Get a packet from the host and return in Hdr and Payload buffers
//
//} Note:  Requires that that first I16 element in header defines the payload size in bytes
#pragma CODE_SECTION (GetPktFromCircBuffer, "MEDIUM_PROG_SECT")
int GetPktFromCircBuffer (CircBufInfo_t *circ, void *Hdr, int HdrI16, 
                          ADT_UInt16 *Payload, ADT_UInt16 maxPayloadI8) {

   // Circular buffer info                   
   int PutIndex, TakeIndex, BufrSize;
   
   // Sizes in 32-bit integer units
   unsigned int WdsReady, WdsTillEnd, WdsRemaining;
   unsigned int PayloadWds, HdrWds, PacketWds;

   ADT_Word *pBufr, *pHdr, *pPayld; 
   ADT_UInt16 *hdrPayloadI8;

   hdrPayloadI8 = (ADT_UInt16 *) Hdr;
   pPayld       = (ADT_Word *) Payload;
   
   PutIndex    = SzInt16ToHost (circ->PutIndex);
   TakeIndex   = SzInt16ToHost (circ->TakeIndex);
   BufrSize    = SzInt16ToHost (circ->BufrSize);

   HdrWds = SzInt16ToHost (HdrI16);

   if (TakeIndex <= PutIndex)
      WdsReady = PutIndex - TakeIndex;
   else
      WdsReady = BufrSize - (TakeIndex - PutIndex);

   if (WdsReady < HdrWds)  {
      return -1;
   }
   
   WdsTillEnd = BufrSize - TakeIndex;
   
   // Set pBufr to current read position
   pBufr  = (ADT_Word *) circ->pBufrBase;
   pBufr += TakeIndex;

   // Copy header from circular buffer
   if (WdsTillEnd < HdrWds)   {
      pHdr = (ADT_Word *) Hdr;
      wdcpy (pHdr, pBufr, WdsTillEnd);
      pHdr += WdsTillEnd;
      WdsRemaining = HdrWds - WdsTillEnd;

      pBufr = (ADT_Word *) circ->pBufrBase;  // Wrap circular buffer to start
      wdcpy (pHdr, pBufr, WdsRemaining);
      pBufr += WdsRemaining;
      WdsTillEnd = BufrSize - WdsRemaining;

   } else {
      wdcpy (Hdr, pBufr, HdrWds);
      
      if (WdsTillEnd == 0)   {
         WdsTillEnd = BufrSize;
         pBufr = (ADT_Word *) circ->pBufrBase;
      } else {
         WdsTillEnd -= HdrWds;
         pBufr += HdrWds;
      }
   }

   // Convert payload length in bytes to Wds
   PayloadWds =  SzInt16ToHost ((*hdrPayloadI8 + 1) / 2);
   PacketWds  = HdrWds + PayloadWds;

   // Make sure payload fits in application buffer.
   if (SzInt16ToHost (maxPayloadI8 / 2) < PayloadWds) {
      // Payload too large for application buffer - flush circular buffer
      circ->TakeIndex = circ->PutIndex;
      return -1;
   }

   if (WdsReady < PacketWds) {
      return -1;
   }

   // Copy payload from circluar buffer
   if (WdsTillEnd < PayloadWds)   {
      wdcpy (pPayld, pBufr, WdsTillEnd);
      pBufr    = (ADT_Word *) circ->pBufrBase;   // Wrap circular buffer
      pPayld     += WdsTillEnd;   
      PayloadWds -= WdsTillEnd;
   }
   wdcpy (pPayld, pBufr, PayloadWds);

   TakeIndex += PacketWds;
   if (BufrSize <= TakeIndex)
      TakeIndex -= BufrSize;

   return SzHostToInt16 (TakeIndex);
}

#pragma CODE_SECTION (RemovePktFromCircBuffer, "MEDIUM_PROG_SECT")
void RemovePktFromCircBuffer (CircBufInfo_t *circ, void *Hdr, void *Payload, int TakeIndex) {
   // Circular buffer info

#if _DEBUG    // Data logging
   unsigned int PacketWds;

   if (circ->TakeIndex <= TakeIndex) 
      PacketWds = TakeIndex - circ->TakeIndex;
   else
      PacketWds = (circ->BufrSize - circ->TakeIndex) + TakeIndex;

   LogTransfer (0x400C1, circ, &circ->pBufrBase [circ->TakeIndex], 
                0, Hdr, SzHostToInt16 (PacketWds));
#endif
   circ->TakeIndex = TakeIndex;

}

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// GetPktFromCircBufferCache - Get a packet from the host and return in Hdr and Payload buffers
//   ... uses the extended API interface to maintain cache coherancy with host processor
//
//} Note:  Requires that that first I16 element in header defines the payload size in bytes
#pragma CODE_SECTION (GetPktFromCircBufferCache, "MEDIUM_PROG_SECT")
int GetPktFromCircBufferCache (CircBufInfo_t *circ, void *Hdr, int HdrI16, 
                          ADT_UInt16 *Payload, ADT_UInt16 maxPayloadI8) {

   // Circular buffer info                   
   int PutIndex, TakeIndex, BufrSize;
   
   // Sizes in 32-bit integer units
   unsigned int WdsReady, WdsTillEnd, WdsRemaining;
   unsigned int PayloadWds, HdrWds, PacketWds;

   ADT_Word *pBufr, *pHdr, *pPayld; 
   ADT_UInt16 *hdrPayloadI8;

   hdrPayloadI8 = (ADT_UInt16 *) Hdr;
   pPayld       = (ADT_Word *) Payload;

 
   if (SysAlgs.apiCacheEnable) {
      AppErr ("Deprecated", ADT_TRUE);
   }

   PutIndex    = SzInt16ToHost (circ->PutIndex);
   TakeIndex   = SzInt16ToHost (circ->TakeIndex);
   BufrSize    = SzInt16ToHost (circ->BufrSize);

   HdrWds = SzInt16ToHost (HdrI16);

   if (TakeIndex <= PutIndex)
      WdsReady = PutIndex - TakeIndex;
   else
      WdsReady = BufrSize - (TakeIndex - PutIndex);

   if (WdsReady < HdrWds)  {
      return -1;
   }
   
   WdsTillEnd = BufrSize - TakeIndex;
   
   // Set pBufr to current read position
   pBufr  = (ADT_Word *) circ->pBufrBase;
   pBufr += TakeIndex;

   // Copy header from circular buffer
   if (WdsTillEnd < HdrWds)   {
      pHdr = (ADT_Word *) Hdr;

      CACHE_INV (pBufr, SzHostToBytes(WdsTillEnd));
      wdcpy (pHdr, pBufr, WdsTillEnd);
      pHdr += WdsTillEnd;
      WdsRemaining = HdrWds - WdsTillEnd;

      pBufr = (ADT_Word *) circ->pBufrBase;  // Wrap circular buffer to start
      CACHE_INV (pBufr, SzHostToBytes(WdsRemaining));
      wdcpy (pHdr, pBufr, WdsRemaining);
      pBufr += WdsRemaining;
      WdsTillEnd = BufrSize - WdsRemaining;

   } else {
      CACHE_INV (pBufr, SzHostToBytes(HdrWds));
      wdcpy (Hdr, pBufr, HdrWds);
      
      if (WdsTillEnd == 0)   {
         WdsTillEnd = BufrSize;
         pBufr = (ADT_Word *) circ->pBufrBase;
      } else {
         WdsTillEnd -= HdrWds;
         pBufr += HdrWds;
      }

   }

   // Convert payload length in bytes to Wds
   PayloadWds =  SzInt16ToHost ((*hdrPayloadI8 + 1) / 2);
   PacketWds  = HdrWds + PayloadWds;

   // Make sure payload fits in application buffer.
   if (SzInt16ToHost (maxPayloadI8 / 2) < PayloadWds) {
      // Payload too large for application buffer - flush circular buffer
      circ->TakeIndex = circ->PutIndex;
      if (SysAlgs.apiCacheEnable) {
         AppErr ("Deprecated", ADT_TRUE);
      }
      return -1;
   }

   if (WdsReady < PacketWds) {
      return -1;
   }

   // Copy payload from circluar buffer
   if (WdsTillEnd < PayloadWds)   {
      CACHE_INV (pBufr, SzHostToBytes(WdsTillEnd));
      wdcpy (pPayld, pBufr, WdsTillEnd);
      pBufr    = (ADT_Word *) circ->pBufrBase;   // Wrap circular buffer
      pPayld     += WdsTillEnd;   
      PayloadWds -= WdsTillEnd;
   }

   if (PayloadWds != 0) {
      CACHE_INV (pBufr, SzHostToBytes(PayloadWds));
      wdcpy (pPayld, pBufr, PayloadWds);
   }

   TakeIndex += PacketWds;
   if (BufrSize <= TakeIndex)
      TakeIndex -= BufrSize;

   return SzHostToInt16 (TakeIndex);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Validates incoming packet headers. Returns 1 if valid, 0 if invalid
#pragma CODE_SECTION (verifyPacketHeader, "MEDIUM_PROG_SECT")
ADT_UInt16 verifyPacketHeader (PktHdr_t *PktHdr, chanInfo_t *chan) {

    // Verify the ChannelID
    if (chan->PairedChannelId != PKT_LOOP_BACK && PktHdr->ChanId != chan->ChannelId)
        return 0;

    switch (PktHdr->PayldClass)  {
    case PayClassAudio:
       if (PktHdr->PayldType != chan->pktToPcm.Coding) {
          // Change of audio codec. Verify that it is acceptable for the frame rate.
          // Force decode reinitialization with new codec
          if (!ValidCoding ((GpakCodecs) PktHdr->PayldType, sampsToHalfMS (chan->pktToPcm.SamplesPerFrame)))
             return 0;

          if ((PktHdr->PayldType == G729AB) && (chan->pktToPcm.Coding == G729)) {
             chan->pktToPcm.Coding = G729AB;
          } else if ((PktHdr->PayldType == G723_53) &&
                    ((chan->pktToPcm.Coding == G723_53A) || (chan->pktToPcm.Coding == G723_63) || (chan->pktToPcm.Coding == G723_63A) )) {
             chan->pktToPcm.Coding = G723_53;
          } else {
             chan->pktToPcm.Coding = chan->pktToPcm.PreferredCoding;
             chan->Flags |= CF_REINIT_DECODE;
          }
       }
       return 1;

    case PayClassStartTone:
    case PayClassEndTone:
       PktHdr->PayldClass = PayClassTone;
       // Intentional fall through

    case PayClassTone:
       if ((EventPkt != PktHdr->PayldType) &&  (TonePkt  != PktHdr->PayldType) && 
           (RTPAVP == sysConfig.packetProfile))
          break;

       if ((DialedDigitsPkt != PktHdr->PayldType) && (AAL2Trunking == sysConfig.packetProfile))
          break;

       if ((EventPkt == PktHdr->PayldType) && (RTP_EVT_PAYLOAD_LEN != PktHdr->PayldBytes))
          break;

       if ((TonePkt == PktHdr->PayldType)  && (RTP_TONE_PAYLOAD_LEN != PktHdr->PayldBytes))
         break;

       if ((DialedDigitsPkt == PktHdr->PayldType) && (AAL2_DIGITS_PAYLOAD_LEN != PktHdr->PayldBytes))
         break;

       return 1;;

    case PayClassSilence:
       if ((SID_PAYLOAD_LEN == PktHdr->PayldBytes) || (SIDA2_PAYLOAD_LEN == PktHdr->PayldBytes)) return 1;
       break;

    case PayClassT38: return 1;

    default:
       break;
   }
   // Invalid header
   chan->status.Bits.invalidPacketHeader = 1;
   chan->invalidHdrCount++;
   return 0;
}


#pragma CODE_SECTION (getPacketBuffer, "MEDIUM_PROG_SECT")
packetStatus_t getPacketBuffer (chanInfo_t *pChan, PktHdr_t *PktHdr, ADT_UInt16 *pyld, ADT_UInt16 maxPayI8) {

   int  statUpdate;
   GpakPayloadClass pyldclass;
   CircBufInfo_t *circ;
   int takeIdx;

    circ   = pChan->pktToPcm.inbuffer;

    // set flag to skip updating packet statistics if a t38 fax is in progress
    statUpdate = !pChan->fax.t38InProgress;

    // -------------------------------------------------------
    //   Read packet(s) from host.  
    //
    //   No packet => exit with no packet
    //   Non-t38 packet => exit with packet
    //   t38 packet and t38 full => exit with no packet
    //   t38 packet and t38 not full => get next packet
    do {
        // Read packet without removal. This will allow
        // the packet to remain in the buffer in the event that the relay input queue
        // is full and cannot accept the packet. If the relay does accept the packet,
        // or it's not a T38 packet, the circular buffer take index is updated to remove the packet.
        if ((takeIdx = GetPktFromCircBuffer (circ, PktHdr, PKT_HDR_I16LEN, pyld, maxPayI8)) == -1) {
           if (statUpdate) {
              pChan->status.Bits.packetUnderflow = 1;
              pChan->underflowCount++;
           }
           return noPacketAvailable;
        }
        if (!verifyPacketHeader (PktHdr, pChan)) {
           // error...invalid packet header. Remove packet from buffer
           RemovePktFromCircBuffer (circ, PktHdr, pyld, takeIdx);
           return invalidPktHeader;
        }

        pyldclass = (GpakPayloadClass) PktHdr->PayldClass;
        if (pyldclass != PayClassT38) {
           // non-T38 payload class available. Remove packet from buffer
           RemovePktFromCircBuffer (circ, PktHdr, pyld, takeIdx);
           pChan->fromHostPktCount++;
           logTime (0x04080000 | pChan->ChannelId);
           return nonT38PktAvailable;
        }

        // Move packet to Fax Relay buffer.  
        if (T38_Add_Packet (pChan, pyld, PktHdr->PayldBytes, PktHdr->TimeStamp)) {
           STATS (FrameStats.pktInT38++;)
           RemovePktFromCircBuffer (circ, PktHdr, pyld, takeIdx);
           pChan->fromHostPktCount++;
           logTime (0x04080000 | pChan->ChannelId);

        } else
           break;           
    } while (TRUE);
    

    return relayFull;
}

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// parsePacket - Read and parse a packet from the host.
//
//  Returns parsed (unpacked) payload data in vparams and parameter length in vparamsI8.
//          vplyd is temporary work buffer to hold packed payload for parsing
//}
#pragma CODE_SECTION (parsePacket, "MEDIUM_PROG_SECT")
GpakPayloadClass parsePacket (chanInfo_t *chan, void *vparams, int *vparamsI8, void *vpyld, int vpyldI8) {
   PktHdr_t PktHdr;
   packetStatus_t    pktStat;
   GpakPayloadClass  pyldclass;

   ADT_UInt16 i, port, timeElapsedMs;
   ADT_Int16 CngLevel;  
   pkt2pcm_t *PktPcm;
   ADT_Bool nonTone;

   TGInfo_t*             tgInfo;
   TRGenerateInstance_t* relayGen;
   TRGenerateEvent_t     toneEvtScratch;
   TRGenerateEvent_t*    toneEvt;

   ADT_UInt16 bits, shift, pyldI8, paramCnt;
   ADT_Int16 regenActive, ToneActive;
   ADT_UInt16 *params, *pyld;
   FwdBuf_t   *fwdBuff;
   ADT_UInt32 lastRTPToneStamp;

   params = vparams;
   pyld   = vpyld;
   PktPcm = &chan->pktToPcm;

   // Get current status of tone generator
   tgInfo = &PktPcm->TGInfo;
   relayGen  = tgInfo->toneRlyGenPtr;
   fwdBuff  = (FwdBuf_t*) PktPcm->tonePktPut;
   toneEvt = &toneEvtScratch;  // Need scratch tone data buffer for tone packet notifications
   if (relayGen == NULL) regenActive = ADT_FALSE;
   else                  regenActive = tgInfo->regenActive;

   ToneActive = (RegenToneTimeStamp != 0);

   // Read a packet from the host or RTP jitter buffer
   logTime (0x400B1600ul | chan->ChannelId);
   if (SysAlgs.rtpHostDelivery || SysAlgs.rtpNetDelivery) {
      pktStat = getPacketFromRTP (chan, &PktHdr, pyld, vpyldI8);
      if (chan->Flags & CF_RTP_RX_DISABLED)
         pktStat = noPacketAvailable;
   } else {
      pktStat = getPacketBuffer (chan, &PktHdr, pyld, vpyldI8);
      pyld = customPktIn (&PktHdr, pyld);
   }
   *vparamsI8 = PktHdr.PayldBytes;

   // START SWITCH pktStat -----------------------------------------------------
   switch (pktStat) {
   case invalidPktHeader:
      STATS (FrameStats.Invalid++;)
   case noPacketAvailable:
      STATS (FrameStats.None++;)
      // When bi-directinal silence is detected over the timeout period, fax is considered
      // to be inactive and the DSP can transition back to voice mode
      if (chan->fax.t38InProgress) {
         T38_Check_Timeout (chan, PktPcm->SamplesPerFrame);
         return PayClassNone;
      }

      // Bad or no packet,
      //    Tone gen active -> return PayClassTone
      //    Tone gen is inactive
      //      Tone forward inactive -> return PayClassNone
      //      else                  -> signal PayClassEndTone
      if (regenActive) {
         // tone gen is active: return payclass tone
         if (AAL2Trunking == sysConfig.packetProfile)
            toneEvt->TimeElapsed += PktPcm->SamplesPerFrame;
         return PayClassTone;
      }

      if (!ToneActive) return PayClassNone;

      pyldclass = PayClassNone;
      break;

   case nonT38PktAvailable:
      // Restart voice mode when an non-T38 packet is received
      if (chan->fax.t38InProgress) {
         T38_Check_Timeout (chan, PktPcm->SamplesPerFrame);
         T38_Restart_Voice (chan);
      }
      pyldclass = (GpakPayloadClass) PktHdr.PayldClass;
      break;

   case relayFull:  // T38 relay full.
      pyldclass = PayClassNone;
      break;
   } // END SWITCH pktStat -------------------------------------------------------
   

   logTime (0x400B1000ul | chan->ChannelId);

   // Terminate tone relay when non-tone packet is received or tone gen is no longer active
   // Terminate CNG when non-tone packet is received
   nonTone = (pyldclass != PayClassTone) && (pyldclass != PayClassStartTone) && (pyldclass != PayClassEndTone);
    if (nonTone) {
#if 1 // jdc original
      if (ToneActive) {
         if ((Enabled != PktPcm->ForwardTonePkts) || (fwdBuff->fwdType == FWD_PACKET_TONE))
            pyldclass = PayClassEndTone;
         logTone (FRMNET, chan->ChannelId, pyldclass, 0, 0, 0, RegenToneTimeStamp);
      }
#else
      //jdc try for regen issue when event packets arrive with voice packets: drop non-tone packets when tone regen is active
      if (regenActive) {
         // tone gen is active: return payclass tone
         return PayClassTone;
      }
#endif
      if (pyldclass == PayClassNone) return PayClassNone;
      if ((Enabled == PktPcm->ForwardCNGPkts) && (fwdBuff->fwdType == FWD_PACKET_CNG)) {
         fwdBuff->fwdType = FWD_PACKET_NONE;
      }
   }

   // Parse packet data
   // START SWITCH pyldClass ---------------------------------------------------
   switch (pyldclass) {
   default:
      if (!regenActive) return PayClassNone;
      pyldclass = PayClassTone;
      break;

   case PayClassT38:
   case PayClassEndTone:
      break;

   case PayClassSilence:
   {
        STATS (FrameStats.pktInCng++;)
        if (Enabled == PktPcm->ForwardCNGPkts)  {
               
            // Forward CNG packet to the other half of the channel
            memcpy (&fwdBuff->pktHdr, &PktHdr, sizeof (PktHdr));

            fwdBuff->pktData[0] = pyld[0];
            fwdBuff->fwdType = FWD_PACKET_CNG; 
            break;
        }

        if (!SysAlgs.vadCngEnable) 
            return PayClassNone;

        CngLevel = pyld[0] & 0x7f;
        if (sysConfig.packetProfile == RTPAVP)
            PktPcm->SIDPower = -CngLevel;
        else {
            if (CngLevel < 30)          PktPcm->SIDPower = -30;
            else if (78 < CngLevel)     PktPcm->SIDPower = -78;
            else                        PktPcm->SIDPower = -CngLevel;
        }
        break;
   } // END case PayClassSilence

   case PayClassAudio:
   {
      STATS (FrameStats.pktInVoice++;)
      bits    = 8;
      shift   = 0;
      pyldI8 = PktHdr.PayldBytes;

      // START SWITCH PayClassAudio: PktHdr.PayldType --------------------------
      switch (PktHdr.PayldType) {
      case PCMU_WB:   case PCMA_WB:
      case PCMU_64:
      case PCMA_64:   //  bits = 8  shift = 0
         if ((sysConfig.DspType == Ti665) || (sysConfig.DspType == Ti667)) {
            i8cpy (params, pyld, pyldI8);
            pyldI8 = 0;  // Do not use standard unpack
	     }
         break;
      case G722_64:   break; //  bits = 8  shift = 0
 
      case PCMU_56:
      case PCMA_56:  bits =  7; shift = 1; break;

      case PCMU_48:
      case PCMA_48:  bits =  6; shift = 2; break;

      case L8:        // bits = 8    shift = 8 (16-bit TDM) or 0 (8-bit TDM)
      case L8_WB:
         port = PktPcm->OutSerialPortId;
         if (port != SerialPortNull && sysConfig.serialWordSize[port] == 16) shift = 8;
         break;

      case L16:
      case L16_WB:
         // Move octets from the payload buffer into the linear 16 data buffer.
         for (i = 0; i < pyldI8 / 2; i++)  params[i] = ntohs (pyld[i]);
         pyldI8 = 0; // Do not use standard unpack
         break;


      case CircuitPkt:  break; // bits = 8  shift = 0

      case ADT_4800:    break; // 8-bit samples, 0-bit left shift

      case G723_63: case G723_63A:
      case G723_53: case G723_53A:  //  Unpacking performed by decoder
         i8cpy (params, pyld, pyldI8);
         pyldI8 = 0;  // Do not use standard unpack
         break;

      case G726_40:  bits =  5; shift = 0; break;
      case G726_32:  bits =  4; shift = 0; break;
      case G726_24:  bits =  3; shift = 0; break;
      case G726_16:  bits =  2; shift = 0; break;

      case G728_16:  bits = 10; shift = 0; break; 

      case G729:    // Store payload byte count as first element.
      case G729AB:  //  Unpacking performed by decoder
         *params++ = pyldI8; 
#if ((DSP_TYPE == 54)||(DSP_TYPE == 55))
         *params++;
         UnpackG711 (pyld, params, pyldI8); // Align payload on 32-bit boundary
#else
         i8cpy (params, pyld, pyldI8);
#endif
         pyldI8 = 0;  // Do not use standard unpack
         break;
                                                   
      case MELP: bits = 6; shift = 0; break;

      case Speex:      case SpeexWB:    case MELP_E:
         i8cpy (params, pyld, pyldI8);
         pyldI8 = 0;
         break;

      case AMR_475:    case AMR_515:    case AMR_590:
      case AMR_670:    case AMR_740:    case AMR_795:
      case AMR_1020:   case AMR_1220:
         parseAMRPayload (pyld, params);
         pyldI8 = 0;
         break;

      case CustomCodec:
         customUnpack (&PktHdr, pyld, params);
         pyldI8 = 0;
         break;

      default:
         AppErr ("Unknown Codec", TRUE);
         pyldI8 = 0; // Do not use standard unpack
         break;
      }; // END SWITCH PayClassAudio: PktHdr.PayldType ----------------------------

      if (pyldI8 == 0) break;
      


      paramCnt = (pyldI8 * 8) / bits;  // Convert payload byte count to parameter count
      if ((AAL2Trunking == sysConfig.packetProfile) || (PktHdr.PayldType == G728_16))
         unpackMSB (pyld, params, paramCnt, bits, shift);    // AAL2 Format
      else
         unpackLSB (pyld, params, paramCnt, bits);    // RFC1890
      *vparamsI8 = paramCnt;
      break;
   } // END case PayClassAudio

   case PayClassStartTone:
   case PayClassTone:
   {
        STATS (FrameStats.pktInTone++;)
        lastRTPToneStamp = chan->LastRTPToneStamp;
        chan->LastRTPToneStamp = RtpChanData[chan->ChannelId]->Instance.LastDelivered;
        if ((lastRTPToneStamp == RtpChanData[chan->ChannelId]->Instance.LastDelivered) && (RegenToneTimeStamp == 0)) {
            STATS (FrameStats.duplicateEndTone++;)
            return PayClassNone;
        }

        // Forward tone packets
        if (Enabled == PktPcm->ForwardTonePkts) {
            // First tone packet - PayClassStartTone.  PktHdr and payload written to tone buffer
            // Remaining packets - PayClassTone.       PktHdr (minus timestamp) and payload written to tone buffer
            if (fwdBuff->fwdType == FWD_PACKET_NONE) {

                pyldclass = PayClassStartTone;
                PktHdr.PayldClass = pyldclass;
                fwdBuff->fwdType = FWD_PACKET_TONE;
                memcpy (&fwdBuff->pktHdr, &PktHdr, sizeof (PktHdr));
            } else {
                PktHdr.PayldClass = pyldclass;
                memcpy (&fwdBuff->pktHdr, &PktHdr, (sizeof (PktHdr) - sizeof (ADT_UInt32)));
            }
            logTone (TOFWD, chan->ChannelId, pyldclass, pyld[0], pyld[1], PktHdr.TimeStamp, RegenToneTimeStamp);
            i8cpy  (fwdBuff->pktData, pyld, PktHdr.PayldBytes);

            // Packet forwarding without host notification.  Skip any further processing.
            if ((PktPcm->toneTypes & Notify_Host) == 0)
                break;
        }

        // Tone regeneration and/or host notification
        // Parse tone packets for frequencies and durations: RFC2833 Event, RFC2833 Tone or AAL2.

        // START SWITCH PayClassTone,StartTone: PktHdr.PayldType -----------------
        switch (PktHdr.PayldType) {
            default:
                return PayClassNone;
        
            case DialedDigitsPkt:
                if (parseAAL2Tone (toneEvt, pyld, &RegenToneTimeStamp, PktPcm->SamplesPerFrame) == 0)
                    return PayClassNone;
                break;
        
            case TonePkt:
                if (parse2833Tone (tgInfo, toneEvt, pyld) == 0)
                    return PayClassNone;
        
                if (RegenToneTimeStamp == 0) {
                    pyldclass = PayClassStartTone;
                } else {
                    pyldclass = PayClassTone;
                }
                logTone (FRMNET, chan->ChannelId, pyldclass, pyld[0], pyld[1], PktHdr.TimeStamp, RegenToneTimeStamp);
                break;
        
            case EventPkt:
                if (parse2833Event (toneEvt, pyld, PktPcm->SamplesPerFrame) == 0)
                    return PayClassNone;
        
                if (RegenToneTimeStamp == 0) {
                    pyldclass = PayClassStartTone;
                } else {
                    pyldclass = PayClassTone;
                }
                if (pyld[0] & 0x8000) {
                    // Notify host of start tone when first tone is also marked as last
                    if (WB_START <= PktPcm->Coding && PktPcm->Coding <= WB_END) {
                        timeElapsedMs = toneEvt->TimeElapsed / 16;
                    } else {
                        timeElapsedMs = toneEvt->TimeElapsed / 8;
                    }
                    if ((PktPcm->toneTypes & Notify_Host) && (pyldclass == PayClassStartTone))
                        toneToHost (chan->ChannelId, BDevice, toneEvt->ToneIndex, timeElapsedMs);
                    pyldclass = PayClassEndTone;
                }
        
                logTone (FRMNET, chan->ChannelId, pyldclass, pyld[0], pyld[1], PktHdr.TimeStamp, RegenToneTimeStamp);
                break;
        }  // END SWITCH PayClassTone,StartTone: PktHdr.PayldType -------------------
   

        if (pyldclass == PayClassNone) break;

        // Tone regeneration update
        toneEvt->TimeStamp = PktHdr.TimeStamp;

        if (toneEvt->UseIndex) {
            if (pyldclass == PayClassStartTone)
                toneEvt->TimeStamp = PktHdr.TimeStamp;
            else 
                toneEvt->TimeStamp = RegenToneTimeStamp;
        }

        // Adjust frequency and elapsed time values to account for 16 kHz sampling.
        if ((WB_START <= PktPcm->Coding) && (PktPcm->Coding <= WB_END)) {
            timeElapsedMs = toneEvt->TimeElapsed / 16;
        } else {
            timeElapsedMs = toneEvt->TimeElapsed / 8;
        }

        if (PktPcm->Coding == G722_64)
            toneEvt->TimeElapsed *= 2;

        toneEvt->SampleRate = chan->ProcessRate;

        tgInfo->regenActive = Enabled;
        toneEvt->LastFrameFlag = (pyldclass == PayClassEndTone);
        toneEvt->StartBit = (RegenToneTimeStamp == 0);

        TR_ADT_sendNewEventToGenerator (relayGen, toneEvt);
        break;
   } // END case PayClassTone, PayClassStartTone
   } // END SWITCH pyldClass -----------------------------------------------------
   

   if ((chan->channelType == conferencePacket) || (chan->channelType == conferenceMultiRate))
     return pyldclass;

   // Host notification of start and end of tones
   if (pyldclass == PayClassStartTone) {
      RegenToneTimeStamp = PktHdr.TimeStamp;     // Use start time returned from jitter buffer
      if (Enabled == PktPcm->ForwardTonePkts) fwdBuff->pktHdr.PayldClass = pyldclass;   // Modify packet header to indicate start of tone
      if (PktPcm->toneTypes & Notify_Host) 
         toneToHost (chan->ChannelId, BDevice, toneEvt->ToneIndex, timeElapsedMs);
   } else if (pyldclass == PayClassEndTone) {
      RegenToneTimeStamp = 0;
      if (Enabled == PktPcm->ForwardTonePkts) fwdBuff->pktHdr.PayldClass = pyldclass;    // Modify packet header to indicate end of tone
      if (PktPcm->toneTypes & Notify_Host) 
         toneToHost (chan->ChannelId, BDevice, tdsNoToneDetected, timeElapsedMs);
   }

   return pyldclass;
}


//}=============================================================================
//++++++++++ Encoding
//{==============================================================================
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma CODE_SECTION (initSpeexCfg, "MEDIUM_PROG_SECT")
void initSpeexCfg (SpeexControlAndStatus_t *speexCfg, GpakCodecs Codec, int FrameSize, int ProcessRate, short VADEnabled) {

   // Channel configured fields
   if (Codec == SpeexWB) {
      speexCfg->ModeID = SPEEX_MODEID_WB;
      speexCfg->FrameSize = 320;
   } else {
      speexCfg->ModeID = SPEEX_MODEID_NB;
      speexCfg->FrameSize = 160;
   }
   speexCfg->SamplingRate = ProcessRate;

   speexCfg->VADEnable = VADEnabled;
   speexCfg->DTXEnable = VADEnabled;

   // SpeexDefault configured fields
   speexCfg->Complexity    = SpeexDflt.Complexity;
   speexCfg->Quality       = SpeexDflt.Quality;
   speexCfg->DecodeEnhancementEnable = SpeexDflt.DecodeEnhancementEnable;
   speexCfg->EncodeBitRate =     0;   // Set by Quality

   speexCfg->VBREnable  = SpeexDflt.VBREnable;
   speexCfg->VBRQuality = SpeexDflt.VBRQuality;
   speexCfg->MaxVBRBitRate = 10000;   // Set by VBRQuality


   // Gpak unsupported fields
   speexCfg->ABREnable      = ADT_FALSE;
   speexCfg->AverageBitRate = 10000;

   // Fields unsupported by V1_00 API
   speexCfg->DecodeBitRate  = 0;
   speexCfg->LBSubMode = 0;
   speexCfg->HBSubMode = 0;
   speexCfg->HPFEnable = ADT_TRUE;
   speexCfg->ExpectedPacketLossRate = 1;
   speexCfg->pfSpeexInBandHandler = NULL;
   speexCfg->pfUserInBandHandler  = NULL;
   return;
}

#pragma CODE_SECTION (initEncoder, "MEDIUM_PROG_SECT")
void initEncoder (pcm2pkt_t *PcmPkt, int scratchFlag, int ProcessRate) {
   SpeexControlAndStatus_t speexCfg;
   void *encodeInst;
   int FrameSize;
   void *pScratch;
   short VADEnabled;
   ADT_UInt16 scratchI16;
   ADT_UInt8  speexStatus;

   encodeInst = PcmPkt->EncodePtr;
   CLEAR_INST_CACHE (encodeInst, encoderInstI16 * sizeof (ADT_UInt16));

   if (PcmPkt->VAD & VadEnabled) VADEnabled = 1;
   else                          VADEnabled = 0;

   FrameSize = PcmPkt->SamplesPerFrame;
   if (scratchFlag == 0)
      pScratch = getSAScratch (FrameSize, &scratchI16);    // Frame-dependent scratch
   else
      pScratch = getBGScratch (&scratchI16);             // Loopback coder scratch

   PcmPkt->OctetsPerPacket = getPacketSize (PcmPkt->Coding, sampsToHalfMS (FrameSize));
   FLUSH_wait ();

   switch (PcmPkt->Coding) {
   case G722_64:  G722_ADT_reset (encodeInst, 1);   break;      
   case G723_63:  case G723_63A:
      G723_Encode_Init (encodeInst, pScratch);
      break;      

   case G723_53:  case G723_53A:
      G723_Encode_Init (encodeInst, pScratch);
      break;      

   case G726_40: G726_Encode_Init (encodeInst, 40, ULAW);   break;
   case G726_32: G726_Encode_Init (encodeInst, 32, ULAW);   break;
   case G726_24: G726_Encode_Init (encodeInst, 24, ULAW);   break;
   case G726_16: G726_Encode_Init (encodeInst, 16, ULAW);   break;

   case G728_16: G728_Encode_Init (encodeInst, pScratch, 16000);   break;

   case G729:
   case G729AB:  g729EncInit (encodeInst, pScratch, VADEnabled);     break;

   case MELP:   MELP_ADT_initEnc  (encodeInst, pScratch); break;
   case MELP_E: MELPE_ADT_initEncode (encodeInst, 2400, FrameSize, pScratch); break;

   case ADT_4800: codInitialize4800Channel (encodeInst); break;      

   case Speex:
   case SpeexWB:
      initSpeexCfg (&speexCfg, PcmPkt->Coding, FrameSize, ProcessRate, VADEnabled);
      SPEEX_ADT_initEncoder (encodeInst, encoderInstI16 * 2, pScratch, scratchI16 * 2, 
                             &speexCfg, &speexStatus);
      AppErr ("Speex Init", speexStatus);
      break;

   case AMR_475:    case AMR_515:    case AMR_590:
   case AMR_670:    case AMR_740:    case AMR_795:
   case AMR_1020:   case AMR_1220:
      AMR_Encode_Init (encodeInst,  VADEnabled, pScratch);
      break;

   case CustomCodec: 
      PcmPkt->OctetsPerPacket = customEncodeInit (PcmPkt);
      break;
   }

   FLUSH_INST_CACHE (encodeInst, encoderInstI16 * sizeof (ADT_UInt16));
}


#pragma CODE_SECTION (vadNotify, "MEDIUM_PROG_SECT")
void vadNotify (chanInfo_t *chan, int vadState, GpakDeviceSide_t deviceSide) {
   EventFifoMsg_t msg;
   pcm2pkt_t *PcmPkt;            // pntr to PCM to Packet chan structure
   pkt2pcm_t *PktPcm;            // pntr to PCM to Packet chan structure
   ADT_Int16       PrevVadState;

   PcmPkt = &(chan->pcmToPkt);
   PktPcm = &(chan->pktToPcm);
   if(deviceSide == ADevice)
	   PrevVadState = PcmPkt->PrevVadState;
   else
	   PrevVadState = PktPcm->PrevVadState;
   // jdc -----------------------------------------------------------------
   if (PrevVadState == VAD_INIT_RELEASE) return;

   // now perform this check at channel setup to enable on per-channel basis
   #if 0
   if (sysConfig.VadReport == Disabled) return;
   #endif
   // --------------------------------------------------------------------------

   if ((PrevVadState == VOICE_VADCNG) && (vadState != VOICE_VADCNG)) {
      // send vad silence transition message to host
      msg.header.eventCode    = EventVadStateSilence;
   } else if ((PrevVadState != VOICE_VADCNG) && (vadState == VOICE_VADCNG)) {
      msg.header.eventCode    = EventVadStateVoice;
   } else 
      return;

   msg.header.channelId    = chan->ChannelId;
   msg.header.eventLength  = EVENT_FIFO_MSGLEN_ZERO;
   msg.header.deviceSide   = deviceSide;
   writeEventIntoFifo (&msg);

   if(deviceSide == ADevice)
      PcmPkt->PrevVadState = vadState;
   else
      PktPcm->PrevVadState = vadState;
	   
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma CODE_SECTION (voiceEncode, "MEDIUM_PROG_SECT")
void voiceEncode (chanInfo_t *chan, void *pcm, void *param, short sampleCnt) {
   int j, port, Shift, vadState, dtx;
   short VADEnabled;
   ADT_UInt16 Width, ParamBits;
   packFormat Packing;
   ADT_Int16 Rate;
   ADT_Int16 actualRate, TxFrType, PktType;
   ADT_Int16 *data;
   ADT_PCM16 *pcmFrame;

   int ParamCnt;

   pcm2pkt_t *PcmPkt;            // pntr to PCM to Packet chan structure
   void *encodeInst;

   PcmPkt = &chan->pcmToPkt;
   encodeInst = PcmPkt->EncodePtr;

   // Default packing parameters
   Width = 8;
   Shift = 0;
   Packing = PK_LSB;

   VADEnabled = (PcmPkt->VAD & VadEnabled);

   if (chan->ProcessRate < chan->EncoderRate) {
      logTime (0x400A1900ul | chan->ChannelId);
      AppErr ("Rate err", (sampleCnt*2) != PcmPkt->rateCvt.UpFrame);
      sampleCnt = upConvert (PcmPkt->rateCvt.UpInst, pcm, sampleCnt, param);

      // Swap pcm and param working pointers since data has been transfered to opposite buffers
      data = pcm; pcm = param; param = data;
   }
   logTime (0x400A1400ul | chan->ChannelId);
   ParamCnt = sampleCnt;

   switch (PcmPkt->Coding)   {
   case CircuitPkt:
      port = PcmPkt->InSerialPortId;

      if (cmpPCMU == sysConfig.compandingMode[port])
         G711_muLawEncode (pcm, param, sampleCnt);
      else if (cmpPCMA == sysConfig.compandingMode[port])
         G711_aLawEncode  (pcm, param, sampleCnt);
      else if (16 == sysConfig.serialWordSize[port])
         for (j=0; j<(int) sampleCnt; j++)
            ((ADT_PCM16 *) pcm)[j] >>= 8;
      return;

   case PCMU_64:
   case PCMU_WB:
      G711_muLawEncode (pcm, param, sampleCnt);

      if ((sysConfig.DspType == Ti665) || (sysConfig.DspType == Ti667)) {
         Packing = PK_None;
		 break;
	  }
      if (AAL2Trunking == sysConfig.packetProfile) {
         Packing = PK_MSB;
      } if (RTPAVP != sysConfig.packetProfile)
         return;
      break;

   case PCMU_56:
      G711_muLawEncode (pcm, param, sampleCnt);

      Shift = 1; Width = 7; Packing = PK_MSB;
      break;

    case PCMU_48:
      G711_muLawEncode (pcm, param, sampleCnt);

      Shift = 2; Width = 6; Packing = PK_MSB;
      break;

   case PCMA_64:
   case PCMA_WB:
      G711_aLawEncode (pcm, param, (short) sampleCnt);
      if ((sysConfig.DspType == Ti665) || (sysConfig.DspType == Ti667)) {
         Packing = PK_None;
		 break;
	  }
      // format and copy the compressed data into packet buffer
      if (AAL2Trunking == sysConfig.packetProfile) {
         Packing = PK_MSB;
      } else if (RTPAVP != sysConfig.packetProfile)
         return;       
      break;

   case PCMA_56:
      G711_aLawEncode (pcm, param, sampleCnt);
      Shift = 1; Width = 7; Packing = PK_MSB;
      break;

   case PCMA_48:
      G711_aLawEncode (pcm,  param, sampleCnt);
      Shift = 2; Width = 6; Packing = PK_MSB;
      break;

   case L8:  // Rshift 8 bits, 8-bits, lsb packing
   case L8_WB:
      port = PcmPkt->InSerialPortId;
      if (sysConfig.serialWordSize[port] == 16) Shift = 8;
      i16cpy (param, pcm, sampleCnt);
      break;
            
   case L16:
   case L16_WB:
      // format and copy the compressed data into packet buffer
      // Only RTP supports linear data
      buildRTPL16Packet (chan, pcm, param, PcmPkt->Coding);
      return;

    case G722_64:
      {
         ADT_Int16 *g722Pyld = (ADT_Int16 *)param;
         ADT_Int16 *g722Pcm  = (ADT_Int16 *)pcm;
         ParamCnt = sampleCnt / 2;
         for (j=0; j < ParamCnt; j++) {
           G722_ADT_encode ((G722ChannelInstance_t *)encodeInst, &g722Pcm[j*2], &g722Pyld[j]);
         }
      }
      break;

    case G723_53A:  case G723_63A:
       VADEnabled = TRUE;
       // Intentional fall through

    case G723_53:   case G723_63:
       Packing = PK_None;
       if (VADEnabled) 
          G723Aux.UseVx = 1;
       else
          G723Aux.UseVx = 0;

       if ((PcmPkt->Coding == G723_63) || (PcmPkt->Coding == G723_63A))
          G723Aux.WrkRate = Rate63;
       else
          G723Aux.WrkRate = Rate53;

       G723_Encode (encodeInst, &G723Aux, pcm, param);
       ParamCnt = G723_ADT_getSize4Decoder (param) + 1;
       if (PcmPkt->VAD & VadNotifyEnabled) {
          if (20 <= ParamCnt) vadState = VOICE_VADCNG;
          else                vadState = SILENCE;
          vadNotify (chan, vadState, ADevice);
       }

       if (ParamCnt <= 1) return;  // Silence without SID packet
       break;

    case G726_40:
    case G726_32:
    case G726_24:
    case G726_16:

#ifdef G726_DEBUG // use canned data
    if (useCannedPcm) {
        int num, i;
        ADT_UInt16 *pDst = pcm;

        num = sampleCnt/8;
        for (i=0; i<num; i++) {
            memcpy(pDst, cannedPcm, sizeof(cannedPcm));
            pDst += 8;
        }
    }
#endif

       G711_muLawEncode (pcm, pcm, (short) sampleCnt);

       G726_Encode (encodeInst, pcm, param, (int) sampleCnt);

       if (PcmPkt->Coding == G726_40)      Width = 5;
       else if (PcmPkt->Coding == G726_32) Width = 4;
       else if (PcmPkt->Coding == G726_24) Width = 3;
       else                                Width = 2;

       // format and copy the compressed data into packet buffer
       if (AAL2Trunking == sysConfig.packetProfile) {
         Packing = PK_MSB;
       } else if (RTPAVP != sysConfig.packetProfile) 
         return;
       break;

   case G728_16:
      ParamCnt = 0;
      data = param;
      pcmFrame = pcm;

      while (sampleCnt) {
         G728_Encode (encodeInst, pcmFrame, data, 16000);
         pcmFrame += 20;
         data = ((ADT_Int16 *) data) + 4;
         sampleCnt -= 20;
         ParamCnt  += 4;
      }
      Width = 10; Packing = PK_MSB;
      break;
       
   case G729AB:  // no packing
       VADEnabled = TRUE;
       // Intentional fall through

   case G729:
#if (DSP_TYPE != 55)
      Packing = PK_None;
#endif
      ParamCnt = 0;
      data = param;
      pcmFrame = pcm;
      dtx = ADT_FALSE;
      do {
         g729Encode (encodeInst, pcmFrame, (char *) data, VADEnabled, &PktType);
         pcmFrame  += 80;   // Skip to next PCM frame
         sampleCnt -= 80;

         if (PktType == 1)      Rate = 10;  // Voice payload
         else if (PktType == 2) Rate = 2;   // Silence payload
         else                   Rate = 0;   // Null payload

         if (PcmPkt->VAD & VadNotifyEnabled) {
            if (Rate == 10) vadState = VOICE_VADCNG;
            else            vadState = SILENCE;
            vadNotify (chan, vadState, ADevice);
         }

#if (DSP_TYPE == 55)
         data = ((ADT_Int16 *) data) + Rate; // 16bit word parameter
#else
         data = ((ADT_Int16 *) data) + (Rate / 2);
#endif
         if (dtx) continue;
         dtx = (Rate < 10);
         ParamCnt  += Rate;

      } while (80 <= sampleCnt);    // Exit on frame complete, SID, or null payload
       
      if (ParamCnt == 0) return;            // No packet
      break;

   case MELP_E:
      Packing = PK_None;
      MELPE_ADT_encode (pcm, param, encodeInst);
      ParamCnt = 7;
      break;

   case MELP:
      MELP_ADT_encode (encodeInst, pcm, param);
      Width = 6;  ParamCnt = 9;
      break;

   case Speex:
   case SpeexWB:
      Packing = PK_None;
      SPEEX_ADT_encode (encodeInst, pcm, sampleCnt, param, &ParamBits);
      AppErr ("Speex bits", ParamBits & 7);
      ParamCnt = ParamBits / 8;
      break;

   case ADT_4800:
      Encode_ADT_4800 (encodeInst, pcm, param);
      break;

   case AMR_475:    case AMR_515:    case AMR_590:
   case AMR_670:    case AMR_740:    case AMR_795:
   case AMR_1020:   case AMR_1220:
      data = param;

      Rate = amrMode [PcmPkt->Coding - AMR_BASE];
      if (VADEnabled) VADEnabled = 1;
      AMR_Encode (encodeInst, Rate, VADEnabled, pcm,
                  &data[2], &actualRate, &TxFrType, 2);

      if (PcmPkt->VAD & VadNotifyEnabled) {
         if (TxFrType == AMR_TX_SPEECH) vadState = VOICE_VADCNG;
         else                           vadState = SILENCE;
         vadNotify (chan, vadState, ADevice);
      }

      if (TxFrType == AMR_TX_NO_DATA) return;    // No packet
      Packing = PK_AMR;
      data[0] = chan->pktToPcm.Coding - AMR_BASE;
      data[1] = actualRate;
      break;

   case CustomCodec:
      Packing = PK_None;
      ParamCnt = customEncode (chan, pcm, param);
      break;

   default:
      return;
   };
   
   // Build Packet
   logTime (0x400A1500ul | chan->ChannelId);
   buildAudioPacket (chan, param, pcm, Shift, Width, Packing, ParamCnt); 

}

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ProcessVadToneEncode - Perform VAD, Tone Detect, and Encode functions.
//   SampCnt is count at channel's process rate.
//}
#define MAX_NUM_RELAY_10MS_SUBFRAMES 10 // supports maximum custom frame size of 100 ms

#pragma CODE_SECTION (sendTonePkt, "MEDIUM_PROG_SECT")
void sendTonePkt(chanInfo_t *chan, TRDetectResults_t  *pToneData, GpakPayloadClass pyClass) {
    //  Tone Relay - build tone packet
    STATS (FrameStats.toneRelayCnt++;)
    STATS (FrameStats.pktOutTone++;)
    if (sysConfig.rtpPktType == EventPkt)               buildRTPEventPacket (chan, pToneData, pyClass);   // Active tone packet
    else if (sysConfig.rtpPktType == TonePkt)           buildRTPTonePacket  (chan, pToneData, pyClass);
    else if (sysConfig.rtpPktType == DialedDigitsPkt)   buildAAL2TonePacket (chan, pToneData, pyClass);
}                


#pragma CODE_SECTION (ProcessVadToneEncode, "MEDIUM_PROG_SECT")
void ProcessVadToneEncode (chanInfo_t *chan,    ADT_PCM16 *pcmBuff, 
                           ADT_Int16 *DTMFBuff, ADT_UInt16 *pktBuff, 
                           short SampCnt) {

   TRDetectInstance_t  *toneRelayInst;   // Tone relay detection instance
   TRDetectResults_t   toneData[MAX_NUM_RELAY_10MS_SUBFRAMES], oldToneData[MAX_NUM_RELAY_10MS_SUBFRAMES];

   toneDetInfo_t    toneInf;
   GpakToneTypes    toneTypes;

   //GpakPayloadClass pyClass;

   pcm2pkt_t  *PcmPkt;            // pntr to PCM to Packet chan structure
   PktHdr_t    PktHdr;
   ADT_UInt16 *Pyld;
   FwdBuf_t   *fwdBuff;
   RtpChanData_t *RTPChan;

   ADT_UInt16  Frame_Period;
   ADT_Int16   NoiseLevel;
   ADT_UInt16  ElapsedMs;
   int SampCnt8k;
   int relayFrameSize,numRelayFrames, i;
   ADT_Int16 *pDTMFBuff;   
   ADT_Int16 turnOn  = -1;
   ADT_Int16 turnOff = -1;
   ADT_Int16 wrapFrame = -1;
   ADT_Int16 tonePktSent = 0;

   // Get a pointer to the PCM to Packet part of the channel structure.
   PcmPkt = &chan->pcmToPkt;
   fwdBuff = (FwdBuf_t *) PcmPkt->tonePktGet;

   RTPChan = RtpChanData [chan->ChannelId];
   Frame_Period = PcmPkt->SamplesPerFrame/(RTPChan->Instance.SampleRate/1000);

   captureEncoderOutput  (chan, pcmBuff, SampCnt);

   //------------ Perform packet forwarding or tone insertion
   memset (&PktHdr, 0, sizeof (PktHdr));
   if (fwdBuff && (fwdBuff->fwdType != FWD_PACKET_NONE)) {
      memcpy (&PktHdr, &fwdBuff->pktHdr, sizeof (PktHdr));
      Pyld   = (ADT_UInt16 *) &fwdBuff->pktData;
      PktHdr.ChanId = chan->ChannelId;

      // Send forwarded packets to host for delivery to network
      if ((fwdBuff->fwdType == FWD_PACKET_TONE) || (fwdBuff->fwdType == FWD_PACKET_TONE_MSG)) {
         if (SendToneTimeStamp == 0) SendToneTimeStamp = PktHdr.TimeStamp;
         if ((PktHdr.PayldType == EventPkt) && (PktHdr.PayldClass == PayClassEndTone))
            Pyld[0] |= 0x8000;
      }
      SendPktToHost (chan, &PktHdr, Pyld);
      // Terminate playout of buffered tone packet
      if ((PktHdr.PayldClass == PayClassEndTone)  || (fwdBuff->fwdType == FWD_PACKET_TONE_MSG)) {
         SendPktToHost (chan, &PktHdr, Pyld);
         SendPktToHost (chan, &PktHdr, Pyld);
         SendToneTimeStamp = 0;
         if (fwdBuff->fwdType == FWD_PACKET_TONE)
            fwdBuff->fwdType = FWD_PACKET_TONE_MSG;
         else 
            fwdBuff->fwdType = FWD_PACKET_NONE;
      }
      logTone (FRMFWD, chan->ChannelId, PktHdr.PayldClass, Pyld[0], Pyld[1], PktHdr.TimeStamp, SendToneTimeStamp);
      return;
   }

   //------------ Perform VAD detection, notification and CNG packet generation
   if ((PcmPkt->VAD & VadMask) == VadCngEnabled) {
      int vad;
      logTime (0x400A1300ul | chan->ChannelId);
      vad = VADCNG_ADT_vad (PcmPkt->vadPtr, (ADT_Int16 *) pcmBuff, &NoiseLevel);

      // force the vad state to be silence coming out of reset until Vad algorithm
      // actually declares silence.
      if (PcmPkt->PrevVadState == VAD_INIT_RELEASE) {
        if (vad == SILENCE) {
            PcmPkt->PrevVadState = SILENCE;
        }
        vad = SILENCE;
      } else {
        if (PcmPkt->VAD & VadNotifyEnabled)
             vadNotify (chan, vad, ADevice);
        customVad (&vad);
      }
      if((chan->ED137BcfgParms.rtp_ext_en) && (chan->ED137TxState.TxVAD_DelayCount <= sysConfig.vadHangOverTime/Frame_Period))
      {
          chan->ED137TxState.TxVAD_DelayCount++;
          vad = SILENCE;
      }

      if (vad == SILENCE) {

         if(chan->ED137BcfgParms.rtp_ext_en)
         {
             if(chan->ED137TxState.TxState & TX_KEEP_ALIVE)
             {
                 if(++chan->ED137TxState.KeepAliveCount < (chan->ED137BcfgParms.R2SKeepAlivePeriod/Frame_Period))
                 {
                     if( (chan->ED137TxState.TxState & TX_UPDATE_PF_MSG) || (chan->ED137TxState.TxState & TX_UPDATE_PTT) || (chan->ED137TxState.TxState & TX_NON_PF_MSG))
                     {
                         chan->ED137TxState.KeepAliveCount = 0;
                     }
                     else
                     {
                         chan->Flags |= CF_REINIT_ENCODE;
                         return;
                     }
                 }
                 else
                 {
                     chan->ED137TxState.KeepAliveCount = 0;
                 }
             }
             else if(chan->ED137TxState.TxState & TX_PTT_OFF)
             {
                 if(chan->ED137TxState.PTT_OffCount < chan->ED137BcfgParms.ptt_rep)
                  {
                     chan->ED137TxState.PTT_OffCount++;
                  }
                  else
                  {
                      chan->ED137TxState.TxState &= (TX_PTT_OFF ^ 0xFFFF);
                      chan->ED137TxState.TxState |= TX_KEEP_ALIVE;
                      chan->ED137TxState.KeepAliveCount = 0;
                      chan->ED137TxState.PTT_OffCount = 0;
                  }
             }
             else
             {
                 chan->ED137TxState.TxState |= TX_PTT_OFF;
                 chan->ED137TxState.KeepAliveCount = 0;
                 chan->ED137TxState.PTT_OffCount = 0;
             }
             chan->Flags |= CF_REINIT_ENCODE;
         }
         else
         {
             // Convert and clamp the VAD Level to packet profile's standard range.
             PcmPkt->SIDPower = -NoiseLevel;
             if (sysConfig.packetProfile == RTPAVP)   {
                if (PcmPkt->SIDPower < 0)    PcmPkt->SIDPower = 0;
                if (PcmPkt->SIDPower > 127)  PcmPkt->SIDPower = 127;
             } else {
                if (PcmPkt->SIDPower < 30)   PcmPkt->SIDPower = 30;
                if (PcmPkt->SIDPower > 78)   PcmPkt->SIDPower = 78;
             }

             // Build and send a Silence packet.
             if (PcmPkt->DtxEnable == Disabled) {
                buildCngPacket (chan);
             }
             chan->Flags |= CF_REINIT_ENCODE;
             return;

         }

      }
      else if(chan->ED137BcfgParms.rtp_ext_en)
      {
          chan->ED137TxState.TxState &= (TX_PTT_OFF ^ 0xFFFF);
          chan->ED137TxState.TxState &= (TX_KEEP_ALIVE ^ 0xFFFF);
      }
   }


   //---------- Perform DTMF, MFR, and CPRG Tone Detection
   //             for either host notification or tone relay
   toneTypes = PcmPkt->toneTypes;

   SampCnt8k = SampCnt;
   if ((chan->ProcessRate == 16000) && (PcmPkt->rateCvt.DwnInst != NULL) && (toneTypes & ToneDetectAll)) {
      //Tone Detection needs 8KHz signal, 
      //if the DTMF buffer has the 16K signal, downsampling is needed
      logTime (0x400A1900ul | chan->ChannelId);
      AppErr ("Rate err", PcmPkt->rateCvt.DwnFrame != SampCnt);
      SampCnt8k = downConvert (PcmPkt->rateCvt.DwnInst, DTMFBuff, SampCnt, DTMFBuff);
   }

   if (toneTypes & ToneDetectAll) {

      toneRelayInst = PcmPkt->toneRelayPtr;
      if ((toneTypes & Tone_Relay) && (toneRelayInst != NULL)) {
         toneTypes &= ~(Tone_Relay | ToneDetect);

         // relay-only processing

         relayFrameSize = SampCnt8k;
         numRelayFrames = 1;
         pDTMFBuff = DTMFBuff;

         // force the relay to operate on 10 ms subframes
         if (SampCnt8k > 80) {
            if ((SampCnt8k % 80) == 0) {
                relayFrameSize = 80;
                numRelayFrames = SampCnt8k/80;
            }       
         }
         if (numRelayFrames > MAX_NUM_RELAY_10MS_SUBFRAMES) {
            // framesize too large, can't do subframes
            relayFrameSize = SampCnt8k;
            numRelayFrames = 1;
         }

         // process all subframes in 10 ms intervals
         for (i=0; i<numRelayFrames; i++, pDTMFBuff += relayFrameSize) {
            TR_ADT_getLastDetectResults (toneRelayInst, &oldToneData[i]);
            TR_ADT_detect (toneRelayInst, pDTMFBuff, relayFrameSize, pDTMFBuff, &toneData[i]);
         }

         turnOn  = -1;
         turnOff = -1;
         wrapFrame = -1;
         tonePktSent = 0;
         for (i=0; i<numRelayFrames; i++) {

            // tone start transition
            if ((oldToneData[i].ToneIndex == tdsNoToneDetected) && (toneData[i].ToneIndex != tdsNoToneDetected)) {
                chan->Flags |= CF_REINIT_ENCODE;
                turnOn  = i;

                ElapsedMs = toneData[i].TimeElapsed / 8;
                if ((chan->EncoderRate == 16000) && (PcmPkt->Coding != G722_64) && (toneData[i].TimeElapsed != 0xffff)) 
                    toneData[i].TimeElapsed *= 2;

                if (toneTypes & Notify_Host) 
                    toneToHost (chan->ChannelId, ADevice, toneData[i].ToneIndex, ElapsedMs);
                SendToneTimeStamp = PcmPkt->FrameTimeStamp +  (relayFrameSize * i) - (ElapsedMs * sysConfig.samplesPerMs);
                
                sendTonePkt(chan, &toneData[i], PayClassStartTone);
                tonePktSent++;
            }

            // tone off transition
            if ((oldToneData[i].ToneIndex != tdsNoToneDetected) && (toneData[i].ToneIndex == tdsNoToneDetected)) {
                chan->Flags |= CF_REINIT_ENCODE;
                turnOff  = i;

                ElapsedMs = oldToneData[i].TimeElapsed / 8;
                if ((chan->EncoderRate == 16000) && (PcmPkt->Coding != G722_64) && (oldToneData[i].TimeElapsed != 0xffff)) 
                    oldToneData[i].TimeElapsed *= 2;

                if (toneTypes & Notify_Host) 
                    toneToHost (chan->ChannelId, ADevice, oldToneData[i].ToneIndex, ElapsedMs);

                sendTonePkt(chan, &oldToneData[i],  PayClassEndTone);
                tonePktSent++;
                SendToneTimeStamp = 0;
            }

            if ((i == turnOn) || (i == turnOff)) continue;

            // tone is on, check for duration wrap
            if (toneData[i].ToneIndex != tdsNoToneDetected) {
                chan->Flags |= CF_REINIT_ENCODE;
                ElapsedMs = toneData[i].TimeElapsed / 8;
                if (SendToneTimeStamp == 0)
                    SendToneTimeStamp = PcmPkt->FrameTimeStamp +  (relayFrameSize * i) - (ElapsedMs * sysConfig.samplesPerMs);
                // send a packet at each subframe
                sendTonePkt(chan, &toneData[i], PayClassTone);
                tonePktSent++;
                if ((wrapFrame == -1) && (toneData[i].TimeElapsed == 0xffff)) {
                    wrapFrame = i;
                    SendToneTimeStamp = 0;
                } 
            }
         } // end of subframe for-loop

        if (tonePktSent)
            return;

      } // end of tone relay processing

      if (toneTypes & (Notify_Host | Tone_Squelch)) {

         // Tone detection (for host notification or DTMF squelching)
         toneInf.ChannelId   = chan->ChannelId;
         toneInf.deviceSide  = ADevice;
         toneInf.FrameSize8k = SampCnt8k;
         toneInf.toneTypes   = PcmPkt->toneTypes;

         // Prevent detectors from running twice when tone relay is enabled
        if (toneInf.toneTypes & Tone_Relay )
            toneInf.toneTypes &= ~(Tone_Relay | ToneDetect);

         toneInf.TDInstances  = &PcmPkt->TDInstances; 
         toneInf.DTMFBuff  = DTMFBuff;
         if (chan->ProcessRate == 8000)   toneInf.pcmBuff = pcmBuff;
         else                             toneInf.pcmBuff = DTMFBuff;
         toneInf.Rec = &(PcmPkt->recA);
         logTime (0x400A0300ul | chan->ChannelId);
         toneDetect (&toneInf, PcmPkt->FrameTimeStamp, chan->ProcessRate, SampCnt);
      } 
   }


   //--------- Perform encode
   if (PcmPkt->Coding == NullCodec) return;

   if ((chan->Flags & CF_REINIT_ENCODE) != 0)   {
      logTime (0x400A1B00ul | chan->ChannelId);
      initEncoder (PcmPkt, chan->channelType == loopbackCoder, chan->ProcessRate);
      if (chan->ProcessRate < chan->EncoderRate) {
         AppErr ("Rate err", PcmPkt->rateCvt.UpFrame != (SampCnt * 2));
         logTime (0x400A1C00ul | chan->ChannelId);
         InitUpSamplingInst (PcmPkt->rateCvt.UpInst, PcmPkt->rateCvt.UpFrame);
      }
      chan->Flags ^= CF_REINIT_ENCODE;
   }       
   voiceEncode (chan, pcmBuff, pktBuff, SampCnt);
   return;
}

#pragma CODE_SECTION (ProcessVadToneOnDecodePath, "MEDIUM_PROG_SECT")
void ProcessVadToneOnDecodePath (chanInfo_t *chan,    ADT_PCM16 *pcmBuff, 
                           ADT_Int16 *DTMFBuff, short SampCnt) {

   toneDetInfo_t    toneInf;
   GpakToneTypes    toneTypes;
   //pcm2pkt_t  *PcmPkt;            // pntr to PCM to Packet chan structure
   pkt2pcm_t  *PktPcm;            // pntr to PCM to Packet chan structure
  //GpakPayloadClass pyClass;
   ADT_Int16   NoiseLevel;
   //ADT_UInt16  ElapsedMs;
   int SampCnt8k;
   //TDInst_t          *pTDInstances;
   // Get a pointer to the PCM to Packet part of the channel structure.
   //PcmPkt = &chan->pcmToPkt;
   PktPcm = &chan->pktToPcm;
   //pTDInstances = &PktPcm->TDInstances;
   //------------ Perform VAD detection, notification and CNG packet generation
   if ((PktPcm->VAD & VadMask) == VadCngEnabled) {
      int vad;
      logTime (0x400A1300ul | chan->ChannelId);
      vad = VADCNG_ADT_vad (PktPcm->vadPtr, (ADT_Int16 *) pcmBuff, &NoiseLevel);

      // force the vad state to be silence coming out of reset until Vad algorithm
      // actually declares silence.
      if (PktPcm->PrevVadState == VAD_INIT_RELEASE) {
        if (vad == SILENCE) {
            PktPcm->PrevVadState = SILENCE;
        }
        vad = SILENCE;
      } else {
        if (PktPcm->VAD & VadNotifyEnabled)
             vadNotify (chan, vad, BDevice);
        customVad (&vad);
      }

   }

   //---------- Perform DTMF, MFR, and CPRG Tone Detection
   //             for either host notification
   toneTypes = PktPcm->toneTypes;

   SampCnt8k = SampCnt;
   if ((chan->ProcessRate == 16000) && (PktPcm->rateCvt.DwnInst != NULL) && (toneTypes & ToneDetectAll)) {
      //Tone Detection needs 8KHz signal, 
      //if the DTMF buffer has the 16K signal, downsampling is needed
      logTime (0x400A1900ul | chan->ChannelId);
      AppErr ("Rate err", PktPcm->rateCvt.DwnFrame != SampCnt);
      SampCnt8k = downConvert (PktPcm->rateCvt.DwnInst, DTMFBuff, SampCnt, DTMFBuff);
   }

   if (toneTypes & ToneDetectAll) {

      if (toneTypes & (Notify_Host | Tone_Squelch)) {

         // Tone detection (for host notification or DTMF squelching)
         toneInf.ChannelId   = chan->ChannelId;
         toneInf.deviceSide  = BDevice;
         toneInf.FrameSize8k = SampCnt8k;
         toneInf.toneTypes   = toneTypes;

         // Prevent detectors from running twice when tone relay is enabled
        if (toneInf.toneTypes & Tone_Relay )
            toneInf.toneTypes &= ~(Tone_Relay | ToneDetect);

         toneInf.TDInstances  = (TDInst_t *)&PktPcm->TDInstances;
         toneInf.DTMFBuff  = DTMFBuff;
         if (chan->ProcessRate == 8000)   toneInf.pcmBuff = pcmBuff;
         else                             toneInf.pcmBuff = DTMFBuff;
         toneInf.Rec = &(PktPcm->recB);
         logTime (0x400A0300ul | chan->ChannelId);
         toneDetect (&toneInf, PktPcm->FrameTimeStamp, chan->ProcessRate, SampCnt);
      } 
   }

   return;
}

//}=============================================================================
//++++++++++ Decoding
//{==============================================================================
#pragma CODE_SECTION (initDecoder, "MEDIUM_PROG_SECT")
void initDecoder (pkt2pcm_t *PktPcm, int bgScratchFlag, int ProcessRate) {
   void *decodeInst;
   int FrameSize;
   void *pScratch;
   ADT_UInt16 scratchI16;
   ADT_UInt8  speexStatus;
   SpeexControlAndStatus_t speexCfg;

   decodeInst = PktPcm->DecodePtr;
   CLEAR_INST_CACHE (decodeInst, decoderInstI16 * sizeof (ADT_UInt16));

   FrameSize = PktPcm->SamplesPerFrame;
   if (bgScratchFlag == 0)
      pScratch = getSAScratch (FrameSize, &scratchI16);    // Frame-dependent scratch
   else
      pScratch = getBGScratch (&scratchI16);               // Background transcoder scratch

   PktPcm->OctetsPerPacket = getPacketSize (PktPcm->Coding, sampsToHalfMS (FrameSize));
   FLUSH_wait ();

   switch (PktPcm->Coding) {
   case PCMU_64:     /* 64 kbps u-Law PCM */
   case PCMA_64:     /* 64 kbps A-Law PCM */
      {
         G711DecParams_t DecParams;

         DecParams.APIVersion = G711A1A2_API_VERSION;
         DecParams.EnableVADCNG = 0;
         DecParams.EnablePLC = 1; //enablePLC; //Huafeng cofigurable?

         DecParams.InstanceSizeInBytes = sizeof(G711DEC_ADT_Instance_t); //Not needed if using create
         DecParams.ScratchSizeInBytes  = sizeof(G711DEC_ADT_Scratch_t);  //Not needed if using create
         DecParams.pScratch = pScratch;
         G711_ADT_initDecode(decodeInst, &DecParams);
      }
      break;
   case G722_64: G722_ADT_reset (decodeInst, 1);   break;

   case G723_63:   case G723_63A:
      G723_Decode_Init (decodeInst, pScratch);
      break;      

   case G723_53:   case G723_53A:
      G723_Decode_Init (decodeInst, pScratch);
      break;

   case G726_40: G726_Decode_Init (decodeInst, 40, ULAW); break;
   case G726_32: G726_Decode_Init (decodeInst, 32, ULAW); break;
   case G726_24: G726_Decode_Init (decodeInst, 24, ULAW); break;
   case G726_16: G726_Decode_Init (decodeInst, 16, ULAW); break;

   case G728_16: G728_Decode_Init (decodeInst, pScratch, 16000); break;

   case G729:
   case G729AB:  g729DecInit (decodeInst, pScratch); break;

   case MELP:   MELP_ADT_initDec (decodeInst, pScratch); break;
   case MELP_E: MELPE_ADT_initDecode (decodeInst, 2400, 0, FrameSize, pScratch); break;


   case Speex:
   case SpeexWB:
      initSpeexCfg (&speexCfg, PktPcm->Coding, FrameSize, ProcessRate, FALSE);
      SPEEX_ADT_initDecoder (decodeInst, decoderInstI16 * 2, pScratch, scratchI16 * 2, 
                             &speexCfg, &speexStatus);
      AppErr ("Speex Init", speexStatus);
      break;

   case ADT_4800: decInitialize4800Channel (decodeInst); break;

   case AMR_475:    case AMR_515:    case AMR_590:
   case AMR_670:    case AMR_740:    case AMR_795:
   case AMR_1020:   case AMR_1220:
      AMR_Decode_Init (decodeInst, pScratch);
      break;

   case CustomCodec:
      PktPcm->OctetsPerPacket = customDecodeInit (PktPcm);
      break;
   }
   FLUSH_INST_CACHE (decodeInst, decoderInstI16 * sizeof (ADT_UInt16));
}


//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//  voiceDecode
//
// FUNCTION
//  Decodes payload into pcm
//
// Inputs
//   pChan    - Channel data structure
//   pyld     -  payload buffer to be decoded,  
//   paramsI8 - 
//
// Outputs
//   pcm  - Unencoded PCM data
//}
#pragma CODE_SECTION (voiceDecode, "MEDIUM_PROG_SECT")
static void voiceDecode (chanInfo_t *chan, void *pyld, int pyldI8, void *pcm, int pcmI8) {

   unsigned int j, rqstRate, frmRate, frmType, SerialPort;
   pkt2pcm_t *PktPcm;
   short FrameErase;
   ADT_UInt16 PktType;
   void *decodeInst;
   ADT_UInt16 *param, *data, dataI8;

   PktPcm = &chan->pktToPcm;
   decodeInst = PktPcm->DecodePtr;

   // Reinitial decoder and down sampler after voice
   if (chan->Flags & CF_REINIT_DECODE) {
      logTime (0x400B1B00ul | chan->ChannelId);
      chan->Flags ^= CF_REINIT_DECODE;
      initDecoder (PktPcm, chan->channelType == loopbackCoder, chan->ProcessRate);
      PktPcm->SIDPower = 0xffff;
      if (chan->ProcessRate < chan->DecoderRate) {
         logTime (0x400B1C00ul | chan->ChannelId);
         InitDownSamplingInst (PktPcm->rateCvt.DwnInst, PktPcm->rateCvt.DwnFrame);
      }
   }

   switch (PktPcm->Coding) {
   case CircuitPkt:
      // Only 8-bit linear data can be sent for circuit data mode - if the serial port
      // is Ulaw or Alaw, we must compress the data before sending.
      SerialPort = PktPcm->OutSerialPortId;
      if (cmpPCMU == sysConfig.compandingMode[SerialPort])
         G711_muLawDecode (pyld, pcm, (short) pyldI8);
      else if (cmpPCMA == sysConfig.compandingMode[SerialPort])
         G711_aLawDecode  (pyld, pcm, (short) pyldI8);
      else if (16 == sysConfig.serialWordSize[SerialPort])
         for (j=0; j<pyldI8; j++) ((ADT_PCM16 *) pyld)[j] <<= 8;
      break;

   case PCMU_64:   case PCMU_WB:
   case PCMU_56:
   case PCMU_48:   // 2 unpacked bytes per sample //66x library use one byte
#ifdef _HFZ_DEBUG
      totalPacketCnt ++;
      if(totalPacketCnt % 20 == 0)
          pyldI8 = 0;
      if(pyldI8 == 0)
         lostPacketCnt ++;
#endif
      if ((sysConfig.DspType == Ti665) || (sysConfig.DspType == Ti667)) {
          G711_ADT_decode(decodeInst,
                          pyld,
                          pyldI8,
                          1, // 1: Mu-law
                          pcmI8,
                          pcm);
          break;
      }
      AppErr ("PCMU error", pcmI8 < pyldI8);
      if (pcmI8 < pyldI8) pyldI8 = pcmI8;
      G711_muLawDecode (pyld, pcm, (short) pyldI8);
      break;

   case PCMA_64:   case PCMA_WB:
   case PCMA_56:
   case PCMA_48:   // 2 unpacked bytes per sample
       if ((sysConfig.DspType == Ti665) || (sysConfig.DspType == Ti667)) {
           G711_ADT_decode(decodeInst,
                           pyld,
                           pyldI8,
                           0,   // 0:A-law
                           pcmI8,
                           pcm);
           break;
       }
      AppErr ("PCMA error", pcmI8 < pyldI8);
      if (pcmI8 < pyldI8) pyldI8 = pcmI8;
      G711_aLawDecode (pyld, pcm, (short) pyldI8);
      break;

   case L8:   case L8_WB:
   case L16:  case L16_WB:   // 2 unpacked bytes per sample
      AppErr ("L8/L16 error", pcmI8 < pyldI8);
      if (pcmI8 < pyldI8) pyldI8 = pcmI8;
      i16cpy (pcm, pyld, pyldI8/2);
      break;

   case G723_63:   case G723_63A:   // 24 bytes per 30 millisecond frame
   case G723_53:   case G723_53A:   // 20 bytes per 30 millisecond frame

      if (pyldI8 != 0) pyldI8 = G723_ADT_getSize4Decoder (pyld) + 1;

      if (pyldI8 == 0)
         FrameErase = (PktPcm->SIDPower == EncoderSpeech);
      else {
         FrameErase = ADT_FALSE;
      }
      AppErr ("G723 error", (24 < pyldI8) || (pcmI8 < 480));
      if (pcmI8 < 480) break;

      // Only call the G723 decoder after at least one packet has been received
      if (chan->fromHostPktCount)
         G723_Decode (decodeInst, &G723Aux, pyld, pcm, FrameErase);
      else 
         memset (pcm, 0, 240*sizeof(ADT_PCM16)); 

      // Store last received packet type
      if (20 <= pyldI8)      PktPcm->SIDPower = EncoderSpeech;
      else if (pyldI8 == 4)  PktPcm->SIDPower = EncoderSilence;
      break;

   case G726_40:   case G726_32:
   case G726_24:   case G726_16:  // 2 unpacked bytes per sample
      AppErr ("G726 error", pcmI8 < pyldI8);
      if (pcmI8 < pyldI8) pyldI8 = pcmI8;

      G726_Decode (decodeInst, pyld, pyld, pyldI8);
      G711_muLawDecode (pyld, pcm, (short) pyldI8);
      break;

   case G728_16:   // 4 bytes / 20 samples
      while (pyldI8) {
         AppErr ("G728 error", pcmI8 < 40);
         if (pcmI8 < 40) break;

         G728_Decode (decodeInst, pcm, pyld, 16000);
         pcm  = ((ADT_PCM16 *) pcm) + 20;
         pyld = ((ADT_UInt16 *) pyld) + 4;
         pyldI8 -= 4;
         pcmI8 -= 40;
      }
      break;

   case G729:     // 10 millisecond frames.  
   case G729AB:   // Packet Type: 0 = nopacket, 1 = voice packet,  2 = SID packet.
      // Retrieve payload size from first parameter word.
      param = pyld;
#if ((DSP_TYPE == 54)||(DSP_TYPE == 55))
      if (pyldI8 != 0) { pyldI8 = *param++; *param++; }
#else
      if (pyldI8 != 0) { pyldI8 = *param++; }
#endif
      // Decode parameters into pcm data one 10 millisecond frame at a time
      data = pcm;
      PktType = 1;
      for (j = 0; j < sampsToHalfMS (PktPcm->SamplesPerFrame) / 20; j++) {
         AppErr ("G729 error", pcmI8 < 160);
         if (pcmI8 < 160) break;

         // Determine packet type from remaining frame size
         if      (pyldI8 == 2) PktType = 2;  // SID Packet
         else if (pyldI8 < 10) PktType = 0;  // No packet

         // Frame Erasure when no packet (FrameSize == 0) and
         // last processed was not a silence (SIDPower != EncoderSilence)
         FrameErase = ((pyldI8 == 0) && (PktPcm->SIDPower != EncoderSilence));

         // Only call the G729 decoder after at least one packet has been received
         if (chan->fromHostPktCount)
            g729Decode (decodeInst, param, data, FrameErase, PktType); 
         else 
            memset (data, 0, 80*sizeof(ADT_PCM16)); 

         // Store last received packet type for frameErase determination
         if (PktType == 2)      PktPcm->SIDPower = EncoderSilence;
         else if (PktType == 1) PktPcm->SIDPower = EncoderSpeech;

         // Prepare for next decode frame
#if ((DSP_TYPE == 55) || (DSP_TYPE == 55))
         param += 10;
#else
         param += 5;
#endif
         data   += 80;
         pcmI8  -= 160;
         pyldI8 -= 10;
         if (pyldI8 < 0) pyldI8 = 0;
      }
      break;

   case G722_64: {
      ADT_Int16 *g722Pyld = (ADT_Int16 *)pyld;
      ADT_Int16 *g722Pcm  = (ADT_Int16 *)pcm;
      AppErr ("G722 error", pcmI8 < pyldI8 * 2);
      if ((pcmI8 / 2) < pyldI8) pyldI8 = pcmI8 / 2;
      for (j=0; j < pyldI8; j++) {
         G722_ADT_decode ((G722ChannelInstance_t *)decodeInst, g722Pyld[j], &g722Pcm[j*2]);
         pcmI8 -= (pyldI8 * 2);
      }
      }
      break;

   case MELP_E:   // 7 bytes per 22.5 millisecond frame
      AppErr ("MELPE error", (7 < pyldI8) || (pcmI8 < 360));
      if (pcmI8 < 360) break;
      FrameErase = (pyldI8 != 7);
      MELPE_ADT_decode (pyld, pcm, decodeInst, FrameErase);
      break;

   case MELP:   // 9 bytes per 22.5 millisecond frame
      AppErr ("MELP error", (9 < pyldI8) || (pcmI8 < 360));
      if (pcmI8 < 360) break;
      MELP_ADT_decode (decodeInst, pyld, pcm);
      break;

   case SpeexWB:  // 20 millisecond frame
      AppErr ("Speex error", pcmI8 < 640);
      if (pcmI8 < 640) break;
   case Speex:
      AppErr ("Speex error", pcmI8 < 320);
      if (pcmI8 < 320) break;
      SPEEX_ADT_decode (decodeInst, pyld, pyldI8 * 8, pcm, &dataI8);
      break;

   case ADT_4800:  // 30 millecond frame
      AppErr ("ADT_4800 error", pcmI8 < 480);
      if (pcmI8 < 480) break;
      Decode_ADT_4800 (decodeInst, pyld, pcm, 0);
      break;

   case AMR_475:    case AMR_515:    case AMR_590:
   case AMR_670:    case AMR_740:    case AMR_795:
   case AMR_1020:   case AMR_1220:   //  20 millisecond frame
      AppErr ("AMR error", pcmI8 < 320);
      if (pcmI8 < 320) break;
      data = pyld;
      if (pyldI8 == 0) {
         frmType = RX_NO_DATA;
         frmRate = MRDTX;
      } else {
         rqstRate = data[0];
         if (rqstRate < MRDTX) chan->pcmToPkt.Coding = (GpakCodecs) (rqstRate + AMR_BASE);

         frmRate = data[1];
         if (MRDTX < frmRate) {
            frmType = RX_NO_DATA;
            frmRate = MRDTX;
         } else if (frmRate != MRDTX) {
             frmType = RX_SPEECH_GOOD;
             PktPcm->SIDPower = EncoderSpeech;
         } else {
             PktPcm->SIDPower = EncoderSilence;
             if (PktPcm->SIDPower == EncoderSpeech)
                frmType = RX_SID_FIRST;
             else
                frmType = RX_SID_UPDATE;
         }
      }
      AMR_Decode (decodeInst, frmRate, (ADT_Int16*) &data[2], frmType, (ADT_Int16 *) pcm, 1, 2);
      break;

   case CustomCodec: 
   case CustomCodec_WB: 
      customDecode (chan, pyld, pcm);
      break;

   default:
      break;
   }

}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Decode Payload - Decode a payload into PCM data.
//
//  Converts encoded params into decoded pcm data at the channel's internal processing rate
//
//   Returns internal processing sample count
#pragma CODE_SECTION (DecodePayload, "MEDIUM_PROG_SECT")
int DecodePayload (chanInfo_t *chan, pkt2pcm_t *PktPcm, 
                   GpakPayloadClass PayClass, ADT_UInt16 *params, int paramsI8,
                                              ADT_PCM16 *pcm,     int pcmI8) {

   int decodeSampleCnt, processSampleCnt;

   // Determine inbound packet's sample count for this channel
   if (chan->DecoderRate < TDMRate)
      decodeSampleCnt = PktPcm->SamplesPerFrame / (TDMRate / chan->DecoderRate);
   else
      decodeSampleCnt = PktPcm->SamplesPerFrame * (chan->DecoderRate / TDMRate);

   // Determine internal processing sample count for this channel
   if (chan->ProcessRate < TDMRate)
      processSampleCnt = PktPcm->SamplesPerFrame / (TDMRate / chan->ProcessRate);
   else
      processSampleCnt = PktPcm->SamplesPerFrame * (chan->ProcessRate / TDMRate);
   AppErr ("SampleCnt", (pcmI8 / 2) < processSampleCnt);


   // Call tone generator each frame to keep track of how many samples to output for the current tone.
   if ((PktPcm->toneTypes & Tone_Regen) && (Enabled != PktPcm->ForwardTonePkts)) {
      logTime (0x400B0800ul | chan->ChannelId);
      // Override payload class if tone is still playing out

      if (ToneRelayGenerate (&PktPcm->TGInfo, processSampleCnt, pcm)) {
        PayClass = PayClassTone;
      }
   } else {
      memset (pcm, 0, processSampleCnt*sizeof(ADT_PCM16)); // zero for echo cancellation
   }

   logTime (0x400B1100ul | chan->ChannelId);

   switch (PayClass)   {
   case PayClassAudio:
      // Decode parameter data (pWork1) into pcm data (pcmCnfrBuf)
      voiceDecode (chan, params, paramsI8, pcm, pcmI8);
      break;

   case PayClassNone:  // Missing packet. Perform concealment
      if (PktPcm->CNG & VadCodec) {
          voiceDecode (chan, params, 0, pcm, pcmI8);
          break;
      }
      chan->Flags |= CF_REINIT_DECODE;
      memset (pcm, 0, processSampleCnt*sizeof(ADT_PCM16)); 
      captureDecoderInput  (chan, pcm, processSampleCnt);
      return processSampleCnt;

   case PayClassSilence:
      chan->Flags |= CF_REINIT_DECODE;
      if (PktPcm->CNG & VadCngEnabled) 
         // Generate comfort noise pcm data
         VADCNG_ADT_cng (PktPcm->cngPtr, 0, (ADT_Int16) PktPcm->SIDPower, pcm);
      else if (Enabled == PktPcm->ForwardCNGPkts)
         memset (pcm, 0, processSampleCnt*sizeof(ADT_PCM16)); 
      break;

   case PayClassTone:
   case PayClassStartTone:
   case PayClassEndTone:
      chan->Flags |= CF_REINIT_DECODE;
      
      if (Enabled == PktPcm->ForwardTonePkts)
         memset (pcm, 0, processSampleCnt*sizeof(ADT_PCM16)); // zero for echo cancellation
      captureDecoderInput  (chan, pcm, processSampleCnt);
      return processSampleCnt;

   default:
      break;
   }

   captureDecoderInput  (chan, pcm, decodeSampleCnt);
   if (chan->ProcessRate < chan->DecoderRate) {
      logTime (0x400B1900ul | chan->ChannelId);
      AppErr ("Rate err", decodeSampleCnt != PktPcm->rateCvt.DwnFrame);
      processSampleCnt = downConvert (PktPcm->rateCvt.DwnInst, pcm, PktPcm->rateCvt.DwnFrame, pcm);
   }
   return processSampleCnt;
}

//}=============================================================================
//++++++++++ Voice enhancements
//{==============================================================================
//    S/W companding, TDM rate conversions
//    Playback / record
//    Tone detection
//    Tone generation
//    Acoustic echo cancellation
//------------------------    S/W companding (support voice and data channels)
#pragma CODE_SECTION (tdmFixedValue, "MEDIUM_PROG_SECT")
void tdmFixedValue (chanInfo_t *chan, GpakDeviceSide_t deviceSide, ADT_PCM16 *pcmBuff, int FrameSize) {
   int i;

   if (chan->tdmPattern.deviceSide != deviceSide) return;

   for (i=0; i<FrameSize; i++)
      pcmBuff[i] = chan->tdmPattern.value;
}

#if TSIP_debug
static  int errcnt = 0;
void pcmbuffertest(ADT_PCM16 *pcm, int Samps)
{
   ADT_PCM16 testdata;
   int testcnt;

   testdata = pcm[0];
   for(testcnt = 0; testcnt< Samps;testcnt++)
   {
       if(testdata != pcm[testcnt])
       {
           errcnt++;
       }
       testdata++;
       if(!(testdata < Samps))
       {
           testdata = 0;
       }
   }
}
static ADT_UInt16 testpcm[160];
static int testinit = 0;
#endif
//  fromTMDCirc returns count of extra samples added by TDM transfer to adjust for TDM slippage
#pragma CODE_SECTION (fromTDMCirc, "MEDIUM_PROG_SECT")
int fromTDMCirc (chanInfo_t *chan, GpakDeviceSide_t deviceSide, void *pcm, int *SampsPerFrame, GpakRateCvt *cvt) {
   int slipCnt;
   GpakSerialPort_t port;
   GpakCompandModes compand;
   CircBufInfo_t *circBuf;
   int numSubFrames, i,j;
   ADT_UInt8 temp[8];
   ADT_Int16 *pPcm;

   if (deviceSide == ADevice) {
      port    = chan->pcmToPkt.InSerialPortId;
      compand = chan->pcmToPkt.InCompandingMode;
      circBuf = chan->pcmToPkt.activeInBuffer;
      logTime (0x400A0100ul | chan->ChannelId);
   } else if (deviceSide == BDevice) {
      port    =  chan->pktToPcm.InSerialPortId;
      compand =  chan->pktToPcm.InCompandingMode;
      circBuf =  chan->pktToPcm.inbuffer;
      logTime (0x400B0100ul | chan->ChannelId);
   } else if (deviceSide == ASideECFar) {
      port    =  chan->pktToPcm.OutSerialPortId;
      compand =  chan->pktToPcm.OutCompandingMode;
      circBuf = &chan->pcmToPkt.ecFarPtr;
      logTime (0x400A0200ul | chan->ChannelId);
   } else {
      AppErr ("Invalid device", TRUE);
      return 0;
   }


   if (SerialPortNull == port) {
      if (chan->ProcessRate < TDMRate) *SampsPerFrame /= 2;
      memset (pcm, 0, *SampsPerFrame * sizeof (ADT_PCM16));
      return 0;
   }

   // Copy data from TDM circular buffer (input or echo canceller far end)
   // Report any samples added to adjust for slips
   if (!circBuf)
      return 0;

   slipCnt = copyCircToLinear (circBuf, pcm, *SampsPerFrame);
#if TSIP_debug
   pcmbuffertest(pcm, *SampsPerFrame);
#endif
   // Convert from PCM to linear
   if (chan->VoiceChannel == Enabled) {
      switch (compand) {
      case cmpL8PcmU:  
            numSubFrames = *SampsPerFrame/8;
            pPcm = (ADT_Int16 *)pcm;
            for (i=0; i<numSubFrames; i++) {
                for (j=0; j<8; j++) {
                    temp[j] = (ADT_UInt8)pPcm[j];
                }
                G711_muLawDecode (temp, pPcm, (short) 8); 
                pPcm += 8;
            }
            break;
      case cmpL8PcmA:  
            numSubFrames = *SampsPerFrame/8;
            pPcm = (ADT_Int16 *)pcm;
            for (i=0; i<numSubFrames; i++) {
                for (j=0; j<8; j++) {
                    temp[j] = (ADT_UInt8)pPcm[j];
                }
                G711_aLawDecode (temp, pPcm, (short) 8); 
                pPcm += 8;
            }
            break;
      default:
         break;
      }
   }

   if (deviceSide == ADevice)
      captureEncoderInput (chan, pcm, *SampsPerFrame);
   else if (deviceSide == BDevice)
      captureDecoderInput (chan, pcm, *SampsPerFrame);

   // Down convert TDM as necessary to match processing rate
   if (chan->ProcessRate < TDMRate) {
      AppErr ("Downconvert failure", (cvt->DwnInst == NULL) || (cvt->DwnFrame != *SampsPerFrame));
      *SampsPerFrame = downConvert (cvt->DwnInst, pcm, *SampsPerFrame, pcm);
   }

   return slipCnt;
}

// toTDMCirc returns actual count of samples to TDM
#pragma CODE_SECTION (toTDMCirc, "MEDIUM_PROG_SECT")
ADT_UInt16 toTDMCirc (chanInfo_t *chan, GpakDeviceSide_t deviceSide, void *pcm, int SampsPerFrame, GpakRateCvt *cvt) {
   GpakSerialPort_t port;
   GpakCompandModes compand;
   CircBufInfo_t *circBuf;
   int numSubFrames, i,j;
   ADT_UInt8 temp[8];
   ADT_Int16 *pPcm;

#if TSIP_debug
   int tstcnt;
    if(testinit == 0)
    {
        for(tstcnt = 0; tstcnt <160; tstcnt++)
        {
            testpcm[tstcnt]=tstcnt;
        }
        testinit =1;
    }
#endif
   if (deviceSide == ADevice) {
      port    = chan->pcmToPkt.OutSerialPortId;
      compand = chan->pcmToPkt.OutCompandingMode;
      circBuf = chan->pcmToPkt.outbuffer;
      logTime (0x400A0A00ul | chan->ChannelId);
   } else if (deviceSide == BDevice) {
      port    =  chan->pktToPcm.OutSerialPortId;
      compand =  chan->pktToPcm.OutCompandingMode;
      circBuf = &chan->pktToPcm.outbuffer;
      logTime (0x400B0A00ul | chan->ChannelId);
   } else {
      AppErr ("Invalid device", TRUE);
      return 0;
   }

   if (SerialPortNull == port) {
       if (chan->ProcessRate < TDMRate) SampsPerFrame *= 2;
       return SampsPerFrame;
   }

   // Up convert to TDM Rate
   if (chan->ProcessRate < TDMRate) {
      AppErr ("Upconvert failure", (cvt->UpInst == NULL) || (cvt->UpFrame != (SampsPerFrame * 2)));
      SampsPerFrame = upConvert (cvt->UpInst, pcm, SampsPerFrame, pcm);
   }

   if (deviceSide == ADevice)
      captureEncoderOutput (chan, pcm, SampsPerFrame);
   else
      captureDecoderOutput (chan, pcm, SampsPerFrame);

   // Convert to PCM for TDM bus
   if (chan->VoiceChannel == Enabled) {
      switch (compand) {
      case cmpL8PcmU:
            numSubFrames = SampsPerFrame/8;
            pPcm = (ADT_Int16 *)pcm;
            for (i=0; i<numSubFrames; i++) {
                G711_muLawEncode (pPcm, temp, (short) 8);
                for (j=0; j<8; j++) {
                    pPcm[j] = (ADT_Int16)temp[j];
                }
                pPcm += 8;
            }
            break;
      case cmpL8PcmA: 
            numSubFrames = SampsPerFrame/8;
            pPcm = (ADT_Int16 *)pcm;
            for (i=0; i<numSubFrames; i++) {
                G711_aLawEncode (pPcm, temp, (short) 8);
                for (j=0; j<8; j++) {
                    pPcm[j] = (ADT_Int16)temp[j];
                }
                pPcm += 8;
            }
            break;
      default:
          break;
      }
   }

   // Transfer to TMD circular out buffer
   tdmFixedValue (chan, deviceSide, pcm, SampsPerFrame);
#if TSIP_debug
   copyLinearToCirc ((ADT_UInt16 *) testpcm, circBuf, SampsPerFrame);
#else
   copyLinearToCirc ((ADT_UInt16 *) pcm, circBuf, SampsPerFrame);
#endif

   return SampsPerFrame;

}
//---------------------    Playback / record 
#pragma CODE_SECTION (sendRecPlaybackEvent, "MEDIUM_PROG_SECT")
void sendRecPlaybackEvent (int ChannelId, GpakAsyncEventCode_t event, playRecInfo_t *pInfo, GpakDeviceSide_t deviceSide) {    
   EventFifoMsg_t msg;
   ADT_UInt32 recordI8, recordAddr;
   ADT_UInt32 *pRecordI16;

   msg.header.channelId = ChannelId;
   msg.header.deviceSide = deviceSide;
   msg.header.eventCode = event;

   recordAddr = (ADT_UInt32) pInfo->Circ.pBufrBase;
   if (event == EventRecordingBufferFull) {
      recordI8 =  pInfo->Circ.PutIndex * 2;

      // save final recording length in the block header
      pRecordI16  = pInfo->blockStart;
      *pRecordI16 = pInfo->Circ.PutIndex;
      pInfo->state = Inactive;
   } else if (event == EventRecordingStopped)  {
      // save final recording length in the block header
      pRecordI16  = pInfo->blockStart;
      *pRecordI16 = pInfo->Circ.PutIndex;
      recordI8 = (*pRecordI16) * 2;

      // Adjust reported address and length to current (ping or pong) half buffer
      if ((pInfo->state & FileRecording) && (pInfo->Circ.BufrSize  < recordI8)) {
         recordAddr += pInfo->Circ.BufrSize;
         recordI8   -= pInfo->Circ.BufrSize;
      }

   } else {
      recordI8 = pInfo->Circ.BufrSize * 2;
   }
   msg.payload[0] = (ADT_UInt16) (recordI8 >> 16);
   msg.payload[1] = (ADT_UInt16) (recordI8);
   msg.payload[2] = (ADT_UInt16) (recordAddr >> 16);
   msg.payload[3] = (ADT_UInt16) (recordAddr);
   msg.header.eventLength = 8;   // 8 bytes
   writeEventIntoFifo (&msg);
}

#pragma CODE_SECTION (playRecordEnd, "MEDIUM_PROG_SECT")
void playRecordEnd (int ChanId, GpakAsyncEventCode_t event, playRecInfo_t *inst, GpakDeviceSide_t deviceSide) {
   // Tear down play/record instance data
   inst->state = Inactive;
   if (inst->CvtIndex != VOID_INDEX) {
      if (inst->CvtIndex & PlayRecordUpConvertBit)
         DeallocUpConverter (inst->CvtIndex & 0x7FFF);
      else
         DeallocDownConverter (inst->CvtIndex);
      inst->CvtIndex = VOID_INDEX;
      inst->CvtInst  = NULL;
   }
   sendRecPlaybackEvent (ChanId, event, inst, deviceSide);
}


//  Three modes of voice playback.
//    1) single buffer  - notify hoat at end of buffer and stop playback
//    2) continuous     - continuously loop to top of buffer
//    3) file           - notify host at middle an end points of buffer; wrap back to top when end is reached.
#pragma CODE_SECTION (voicePlayback, "MEDIUM_PROG_SECT")
void voicePlayback (playRecInfo_t *play, void *pcm, void *scratch, int FrameSize, GpakDeviceSide_t deviceSide, int chanId) {
   chanInfo_t *chan;
   int  bufferSwaps;
   void *PCM, *PLAY, *temp;   // Pointer to buffers that will contain converted PCM and PLAY data

   ADT_UInt32 availI16;  // Length of recorded data in 16-bits
   ADT_UInt32 *buffHdr;  // B
   int FrameRateTDM;     // Number of samples stored in record buffer
   int CircI16;          // Number of I16 byte of storage for frame

   if (!(play->state & PlayBack)) return;

   logTime (0x40000900ul | ((deviceSide + 'A') << 16) | chanId);

   // Convert process frame size to TDM rate.
   chan = chanTable[chanId];
   FrameRateTDM = (FrameSize * TDMRate) / chan->ProcessRate;
   CircI16 = FrameRateTDM;


   // change the dest pointer and length if storage format is 8-bit packed
   if (play->mode != RecordingL16) {
      CircI16 >>= 1;  // div len by 2 since data is packed (A or mu-law)
   }

   // Determine the amount of available data.  Signal host on halfway at end or complete.
   availI16 = getAvailable (&play->Circ);
   if (play->state & FileRecording) {
      if (((play->Circ.BufrSize/2 - CircI16) <= play->Circ.TakeIndex) && (play->Circ.TakeIndex < (play->Circ.BufrSize / 2))) {
         sendRecPlaybackEvent (chanId, EventPlaybackHalfWay, play, deviceSide);
         play->Circ.PutIndex = 0;
      } else if ((play->Circ.BufrSize - CircI16) <= play->Circ.TakeIndex) {
         buffHdr = (ADT_UInt32 *) play->Circ.pBufrBase;
         if (buffHdr [-2] == 0) {  // Read out length from buffer
            playRecordEnd (chanId, EventPlaybackComplete, play, deviceSide);
            return;
         }

         sendRecPlaybackEvent (chanId, EventPlaybackAtEnd, play, deviceSide);
         play->Circ.PutIndex = play->Circ.BufrSize / 2;
      }
   } else if (play->state & PlayContinuous) {   // Refresh putindex for continuous playback
      play->Circ.PutIndex = play->Circ.TakeIndex - CircI16;
      if (((ADT_Int16) play->Circ.PutIndex) < 0) play->Circ.PutIndex += play->Circ.BufrSize;
   } else if (availI16 < CircI16) { // End of one-time playback.
      playRecordEnd (chanId, EventPlaybackComplete, play, deviceSide);
      return;
   }

   
   // Set pointers so that pcm buffer is last buffer written to after all conversions
   bufferSwaps = 0; 
   if (play->mode != RecordingL16) bufferSwaps++;
   if (play->CvtIndex != VOID_INDEX) bufferSwaps++;

   if (bufferSwaps & 1) {
      PCM  = scratch;
      PLAY = pcm;
   } else {
      PCM  = pcm;
      PLAY = scratch;
   }

   // read playback data from circular buffer into playback or scratch buffer
   copyCircToLinear (&play->Circ, PCM, CircI16);
   
   // unpack and expand data
   if (play->mode != RecordingL16) {
      temp = PCM;  
      PCM = PLAY;  
      PLAY = temp;
      // jdc.. skip unpack, G711 decode expects compressed input in char buffer
      // unpackLSB (PLAY, PCM, FrameRateTDM, 8);
      if (play->mode == RecordingUlaw)
         G711_muLawDecode (PLAY, PCM, (short) FrameRateTDM);
      else
         G711_aLawDecode  (PLAY, PCM, (short) FrameRateTDM);
   }

   // Convert playback buffer (stored at TDMRate) to process rate
   if (play->CvtIndex == VOID_INDEX) return;

   temp = PCM;  PCM = PLAY;  PLAY = temp;
   if (play->CvtIndex & PlayRecordUpConvertBit) {
      AppErr ("Cvt err", play->CvtFrame != (FrameRateTDM * 2));
      upConvert (play->CvtInst, PLAY, FrameRateTDM, PCM);
   } else {
      AppErr ("Cvt err", play->CvtFrame != FrameRateTDM);
      downConvert (play->CvtInst, PLAY, play->CvtFrame, PCM);
   }
   return;
}

//  Two modes of voice record.
//    1) single buffer  - notify hoat at end of buffer and stop recording
//    2) file           - notify host at middle an end points of buffer; wrap back to top.
#pragma CODE_SECTION (voiceRecord, "MEDIUM_PROG_SECT")
void voiceRecord (playRecInfo_t *rec, void *pcm, void *scratch, int FrameSize, GpakDeviceSide_t deviceSide, int chanId) {
   chanInfo_t *chan;
   void *PCM, *REC;           // Pointer to buffers that will contain converted PCM and RECORD data
   ADT_UInt32 freeI16;

   int FrameRateTDM;  // Number of samples stored in record buffer
   int CircI16;       // Number of I16 byte of storage for frame
   PCM = pcm;
   REC = pcm;

   if (!(rec->state & Record)) return;

   logTime (0x40000700ul | ((deviceSide + 'A') << 16) | chanId);

   chan = chanTable[chanId];
   FrameRateTDM = (FrameSize * TDMRate) / chan->ProcessRate;
   CircI16 = FrameRateTDM;

   // change the src pointer and length if storage format is 8-bit packed
   if (rec->mode != RecordingL16) {
      CircI16 >>= 1;  // div len by 2 since data is packed
   }

   // attempt to store the next frame of the recording
   // Signal host of recording at middle, at end or complete
   freeI16 = getFreeSpace (&rec->Circ);
   if (rec->state & FileRecording) {
      if (((rec->Circ.BufrSize/2 - CircI16) <= rec->Circ.PutIndex) && (rec->Circ.PutIndex < (rec->Circ.BufrSize / 2))) {
         sendRecPlaybackEvent (chanId, EventRecordingHalfWay, rec, deviceSide);
         rec->Circ.TakeIndex = 0;
      } else if ((rec->Circ.BufrSize - CircI16) <= rec->Circ.PutIndex) {
         sendRecPlaybackEvent (chanId, EventRecordingAtEnd, rec, deviceSide);
         rec->Circ.TakeIndex = rec->Circ.BufrSize / 2;
      }
   } else if (freeI16 < CircI16) { // End of one-time record
      playRecordEnd (chanId, EventRecordingBufferFull, rec, deviceSide);
      return;
   }

   // Convert process rate to TDMRate rate for storage
   if (rec->CvtIndex != VOID_INDEX) {
      if (rec->CvtIndex & PlayRecordUpConvertBit) {
         AppErr ("Cvt error", rec->CvtFrame != FrameRateTDM);
         upConvert   (rec->CvtInst, PCM, rec->CvtFrame / 2, scratch);
         PCM = scratch;
         REC = scratch;
      } else {
         AppErr ("Cvt error", rec->CvtFrame != (FrameRateTDM * 2));
         downConvert (rec->CvtInst, PCM, rec->CvtFrame, scratch);
         PCM = scratch;
         REC = scratch;
      }
   }

   // compress and pack data for storage
   if (rec->mode != RecordingL16) {
      REC = scratch;
      if (rec->mode == RecordingUlaw)
         G711_muLawEncode (PCM, REC, (short) FrameRateTDM);
      else
         G711_aLawEncode  (PCM, REC, (short) FrameRateTDM);
      // jdc skip pack: g711 encoder puts compressed output in char buffer 
      //  packLSB (PCM, REC, FrameRateTDM, 8);
   }
   
   // copy recorded data to circular buffer
   copyLinearToCirc (REC, &rec->Circ, CircI16);
}

#pragma CODE_SECTION (runAEC, "MEDIUM_PROG_SECT")
void runAEC (chanInfo_t *chan, void *pcmBuff, void *farWork, int SampsPerFrame, GpakRateCvt *rateCvt) {
#ifdef AEC_SUPPORTED
   int idx;
   pcm2pkt_t *PcmPkt;

   PcmPkt = &chan->pcmToPkt;
   idx = (PcmPkt->EcIndex & 0xFF);

   logTime (0x400B0200ul | chan->ChannelId);

   // Copy stored far end data to far work
   i16cpy (farWork, PcmPkt->ecFarPtr.pBufrBase, SampsPerFrame);

   // Apply equalization filter on input
   if (sysConfig.micEqLength != 0)
      eqApply (&micEqChan[idx], pcmBuff, pcmBuff, SampsPerFrame);

   // Perform echo cancellation and capture
   custAecInput (chan, pcmBuff, farWork, SampsPerFrame);
   g168Capture (PcmPkt->EcIndex, pcmBuff, farWork, SampsPerFrame);
   AECG4_ADT_apply ((IAECG4_Handle) PcmPkt->pG168Chan, farWork, farWork, pcmBuff, pcmBuff);
   g168PostCapture (PcmPkt->EcIndex, pcmBuff, farWork, SampsPerFrame);
   custAecOutput (chan, pcmBuff, farWork, SampsPerFrame);

   // Apply equalization filter on far end output
   if (sysConfig.spkrEqLength != 0)
     eqApply (&spkrEqChan[idx], farWork, farWork, SampsPerFrame);

   // Copy far end output to TDM buffer
   toTDMCirc (chan, BDevice, farWork, SampsPerFrame, rateCvt);
#endif
} 

//-------------------------    Tone detection (reporting to host)
//
//  reportToneStatus - send tone event to host
//  toneDetect       - performs detection for DTMF, MFR1, MFR2F, MFR2R, CPRG, Generic and T.38 tones
//
#pragma CODE_SECTION (reportToneStatus, "MEDIUM_PROG_SECT")
static ADT_Int16 reportToneStatus (toneDetInfo_t* tone, TDStatus_t* toneStatus,   TDStatus_t* prevStat,   GpakToneTypes   toneType,
      int        FrameSize8k,  ADT_UInt40 FrameTimeStamp8k,  ADT_UInt40 *ToneTimeStamp8k) {

   ADT_UInt16 durationMs;
   ADT_Int16  currResult, prevResult, toneIndex, prevTone;
   
   currResult = toneStatus->Result;
   toneIndex  = toneStatus->ToneIndex;
   prevResult = prevStat->Result;
   prevTone   = prevStat->ToneIndex;

   if ((currResult == prevResult) && (toneIndex == prevTone)) return tdsNoChange;

   // Set start time of tone.
   switch (currResult) {
   case TD_EARLY:
   case TD_LEAD_EDGE:
   case TD_DIGIT:    
      if (*ToneTimeStamp8k != 0)
          break;
      *ToneTimeStamp8k = FrameTimeStamp8k - (ADT_UInt40)FrameSize8k;
      break;

   default:
      break;
   }

   switch (prevResult) {
   case TD_NULL:
   case TD_FALSE_EARLY:
   case TD_VERY_EARLY:
   case TD_EARLY:
   case TD_LEAD_EDGE:
      // Previous result reported as inactive.   Transition only on DIGIT.
      switch (currResult) {
      case TD_DIGIT:  // Tone-On transition detected 
         if (toneType & MFR1_tone)          toneIndex += 20;
         else if (toneType & MFR2Fwd_tone)  toneIndex += 40;
         else if (toneType & MFR2Back_tone) toneIndex += 60;
         else if (toneType & CallProg_tone) toneIndex += 80;
         else if (toneType & Arbit_tone)    toneIndex += 101;
         break;

      default:  // No change in state
         toneIndex = tdsNoChange;
         break;
      }
      break;

   case TD_DIGIT:    
      // Previous result reported as tone.  
      //    Transition to new tone on new digit.
      //    No change on trailing edge.
      //    Change to no tone on any other state. 
      switch (currResult) {
      case TD_DIGIT:
      case TD_TRAIL_EDGE:      // Stay in tone
         toneIndex = tdsNoChange;
         break;

      default:
         toneIndex = tdsNoToneDetected;
         break;

      }
      break;

   case TD_TRAIL_EDGE:
      // Previous result reported as tone
      //    Transition to new tone if new digit.
      //    Stay in tone on same digit or continuing trailing edge
      //    Otherwise end of tone.
      switch (currResult) {
      case TD_DIGIT:
         if (toneIndex == prevTone) {
            toneIndex = tdsNoChange;
            break;
         }
         if (toneType & Notify_Host) { // Notify end of tone.
            durationMs = (ADT_UInt16) ((FrameTimeStamp8k - *ToneTimeStamp8k) >> 3);
            toneToHost (tone->ChannelId, tone->deviceSide, 100, durationMs);
            logTone (TONEDETECT, tone->ChannelId, -1 * currResult, ntohs(100), ntohs(durationMs), FrameTimeStamp8k, *ToneTimeStamp8k);
         }
         // Start new tone.
      *ToneTimeStamp8k = FrameTimeStamp8k - (ADT_UInt40)FrameSize8k;
      if (toneType & MFR1_tone)          toneIndex += 20;
      else if (toneType & MFR2Fwd_tone)  toneIndex += 40;
      else if (toneType & MFR2Back_tone) toneIndex += 60;
      else if (toneType & CallProg_tone) toneIndex += 80;
      else if (toneType & Arbit_tone)    toneIndex += 101;
         break;

      case TD_TRAIL_EDGE: 
         toneIndex = tdsNoChange;
         break;

      default:
      toneIndex = tdsNoToneDetected;
         break;
      }
      break;  
   }

   durationMs = (ADT_UInt16) ((FrameTimeStamp8k - *ToneTimeStamp8k) >> 3);
   if (*ToneTimeStamp8k != 0) { // Log only active tones
       if (toneIndex == tdsNoToneDetected)  prevTone = toneIndex;
       else                                 prevTone = toneStatus->ToneIndex;
       logTone (TONEDETECT, tone->ChannelId, -1 * currResult, ntohs(prevTone), ntohs(durationMs), FrameTimeStamp8k, *ToneTimeStamp8k);
   }
   
   if (toneIndex == tdsNoChange) return toneIndex;
   if (toneType & Notify_Host) {
      toneToHost (tone->ChannelId, tone->deviceSide, toneIndex, durationMs);
   }
   if (toneIndex == tdsNoToneDetected)
       *ToneTimeStamp8k = 0;
   return toneIndex;
}


#pragma CODE_SECTION (toneDetect, "MEDIUM_PROG_SECT")
void toneDetect (toneDetInfo_t *tone, ADT_UInt40 FrameTimeStamp, ADT_UInt32 processRate, ADT_UInt16 SampsPerFrame) {

   TDInst_t      *tdInstances;
   TDInstance_t  *toneInst;
   TDStatus_t     prevStat;

   ADT_UInt40    *pSendToneTimeStamp;
   ADT_Int16      toneIdx;
   int            toneTypeCnt, sampsPerFrame8k, sampsPerCall;
   void          *Scratch;

   if (processRate < TDMRate)  SampsPerFrame *= (TDMRate/processRate);
   Scratch = getToneDetScratchPtr (SampsPerFrame);

   // NOTE: DTFM_ADT_toneSuppress requires 105 or fewer samples
   //       Also, to avoid multiple tone transitions from occuring within a 
   //       single frame, tone detection is processed in spurts of 10 ms or less
   tdInstances = tone->TDInstances;

   if(tone->deviceSide == ADevice)
      pSendToneTimeStamp = &tdInstances->ToneTimeStamp;
   else // PktPcm side can't use same ToneTimeStampTone that might be used by Toneregen
	  pSendToneTimeStamp = &tdInstances->ToneTimeStampToneDet;
   if (processRate == 16000) {
      *pSendToneTimeStamp /= 2;
      FrameTimeStamp /= 2;
   }

   sampsPerFrame8k = tone->FrameSize8k;
   while (sampsPerFrame8k) {
      if (80 < sampsPerFrame8k) sampsPerCall = 80;
      else                      sampsPerCall = sampsPerFrame8k;
      sampsPerFrame8k -= sampsPerCall;
      toneTypeCnt = 0;

      // Run DTMF if selected.
      if ((toneTypeCnt < sysConfig.maxToneDetTypes) && (tone->toneTypes & DTMF_tone))   {
         toneInst = &(tdInstances->ToneDetInst[toneTypeCnt++]);
         prevStat = toneInst->Status;
           logTime (0x40001300ul);

         TD_ADT_toneDetect (toneInst, tone->DTMFBuff, sampsPerCall);
         if ((tone->toneTypes & Tone_Squelch) && (tone->pcmBuff != NULL))
            DTMF_ADT_toneSuppress (NULL, toneInst, tone->DTMFBuff, sampsPerCall, DEFAULT_SUPPRESSION, (ADT_PCM16 *) tone->pcmBuff);

         toneIdx = reportToneStatus (tone, &toneInst->Status, &prevStat, DTMF_tone | (tone->toneTypes & Notify_Host), 
                                        sampsPerCall, FrameTimeStamp, pSendToneTimeStamp);
                                       
         // Stop recording when recording stop tone is detected.
         if ((tone->Rec->stopTone == toneIdx) && (toneIdx != tdsNoToneDetected) && (tone->Rec->state == Record)) {
           playRecordEnd (tone->ChannelId, EventRecordingStopped, tone->Rec, tone->deviceSide);
         }
      }

      // Run MFR1 if selected.
      if ((toneTypeCnt < sysConfig.maxToneDetTypes) && (tone->toneTypes & MFR1_tone))   {
         toneInst = &(tdInstances->ToneDetInst[toneTypeCnt++]);
         prevStat = toneInst->Status;
         logTime (0x40002300ul);
         TD_ADT_toneDetect (toneInst, tone->DTMFBuff, sampsPerCall);
         reportToneStatus  (tone, &toneInst->Status, &prevStat, MFR1_tone | (tone->toneTypes & Notify_Host),
                            sampsPerCall, FrameTimeStamp, pSendToneTimeStamp);
      }

      // Run MFR2 Forward if selected.
      if ((toneTypeCnt < sysConfig.maxToneDetTypes) && (tone->toneTypes & MFR2Fwd_tone))   {
         toneInst = &(tdInstances->ToneDetInst[toneTypeCnt++]);
         prevStat = toneInst->Status;
         logTime (0x40003300ul);
         TD_ADT_toneDetect (toneInst, tone->DTMFBuff, sampsPerCall);
         if ((tone->toneTypes & Tone_Squelch) && (tone->pcmBuff != NULL))
            MFR2F_ADT_toneSuppress (NULL, toneInst, tone->DTMFBuff, sampsPerCall, DEFAULT_SUPPRESSION, (ADT_PCM16 *) tone->pcmBuff);

         reportToneStatus  (tone, &toneInst->Status, &prevStat, MFR2Fwd_tone | (tone->toneTypes & Notify_Host),
                            sampsPerCall, FrameTimeStamp, pSendToneTimeStamp);
      }

      // Run MFR2 Back if selected.
      if ((toneTypeCnt < sysConfig.maxToneDetTypes) && (tone->toneTypes & MFR2Back_tone))   {
         toneInst = &(tdInstances->ToneDetInst[toneTypeCnt++]);
         prevStat = toneInst->Status;
         logTime (0x40004300ul);
         TD_ADT_toneDetect (toneInst, tone->DTMFBuff, sampsPerCall);
         if ((tone->toneTypes & Tone_Squelch) && (tone->pcmBuff != NULL))
            MFR2R_ADT_toneSuppress (NULL, toneInst, tone->DTMFBuff, sampsPerCall, DEFAULT_SUPPRESSION, (ADT_PCM16 *) tone->pcmBuff);
         reportToneStatus  (tone, &toneInst->Status, &prevStat, MFR2Back_tone | (tone->toneTypes & Notify_Host),
                            sampsPerCall, FrameTimeStamp, pSendToneTimeStamp);
      }

      // Run Call Progress if selected.
      if ((toneTypeCnt < sysConfig.maxToneDetTypes) && (tone->toneTypes & CallProg_tone))   {
         toneInst = &(tdInstances->ToneDetInst[toneTypeCnt++]);
         prevStat = toneInst->Status;
         logTime (0x40005300ul);
         TD_ADT_toneDetect (toneInst, tone->DTMFBuff, sampsPerCall);
         if ((tone->toneTypes & Tone_Squelch) && (tone->pcmBuff != NULL))
            CPRG_ADT_toneSuppress (NULL, toneInst, tone->DTMFBuff, sampsPerCall, DEFAULT_SUPPRESSION, (ADT_PCM16 *) tone->pcmBuff);
         reportToneStatus  (tone, &toneInst->Status, &prevStat, CallProg_tone | (tone->toneTypes & Notify_Host),
                            sampsPerCall, FrameTimeStamp, pSendToneTimeStamp);
      }

      // Arbitrary Tone Detection (host notification)
      if (tone->toneTypes & Arbit_tone) {
         prevStat.Result    = (tdInstances->ArbToneInst)->Status.Result;
         prevStat.ToneIndex = (tdInstances->ArbToneInst)->Status.ToneIndex;
         logTime (0x40006300ul);
         ARBIT_ADT_toneDetect (tdInstances->ArbToneInst, tone->DTMFBuff, sampsPerCall);
         if ((tone->toneTypes & Tone_Squelch) && (tone->pcmBuff != NULL))
             TDLOWMEM_ADT_toneSuppress ((ARBIT_TDInstance_t *)toneInst, tone->DTMFBuff, sampsPerCall, DEFAULT_SUPPRESSION, (ADT_PCM16 *) tone->pcmBuff);
         logTime (0x40006300ul | ((TDStatus_t*) &(tdInstances->ArbToneInst)->Status)->Result);
         reportToneStatus  (tone, (TDStatus_t *) &(tdInstances->ArbToneInst)->Status, 
                            &prevStat, Arbit_tone | (tone->toneTypes & Notify_Host),
                            sampsPerCall, FrameTimeStamp, pSendToneTimeStamp);
      }

      // Fax Tone Detection (host notification)
      if (tone->toneTypes & FAX_tone) {
         logTime (0x40007300ul);
         CedCngDetect (tone, tdInstances, Scratch, tone->DTMFBuff, sampsPerCall, FrameTimeStamp, pSendToneTimeStamp);
      }
      FrameTimeStamp += sampsPerCall;
      tone->pcmBuff  += sampsPerCall;
      tone->DTMFBuff += sampsPerCall;
   }
   if (processRate == 16000) 
      *pSendToneTimeStamp *= 2;
}

#pragma CODE_SECTION (CedCngDetect, "MEDIUM_PROG_SECT")
void CedCngDetect (toneDetInfo_t *tone, TDInst_t *tdInstances, void *Scratch, ADT_Int16 *pcmBuff, 
                  int FrameSize8k, ADT_UInt40 FrameTimeStamp8k, ADT_UInt40 *ToneTimeStamp8k) {

#if (DSP_TYPE == 54 || DSP_TYPE == 55)
   return;
#elif (DSP_TYPE == 64)
   ADT_Int16  prevG164Flag;
   ADT_Int16  prevG165Flag;
   ADT_Int16  prevCngFlag;
   ADT_Int16  newCngFlag;
   ADT_UInt16 durationMs;
   ADT_Int16  toneIndex;
   FaxCEDChannel_t  *pCedChan;
   FAXCNGInstance_t *pCngChan; 

   pCedChan = tdInstances->CedDetInst;
   pCngChan = tdInstances->CngDetInst;
   
   toneIndex = tdsNoChange;
   if (pCedChan != NULL) {
       // call the faxCed detector
       prevG164Flag = pCedChan->G164Flag;
       prevG165Flag = pCedChan->G165Flag;
       FAXCED_ADT_detect (pCedChan, pCedChan, pcmBuff, pcmBuff, FrameSize8k);

       if (!prevG164Flag && pCedChan->G164Flag) {
           *ToneTimeStamp8k = FrameTimeStamp8k - (ADT_UInt32)FrameSize8k;
           toneIndex = tdsG164;             // Start of Ced G.164 tone
       } else if (prevG164Flag && !(pCedChan->G164Flag))  {
           toneIndex = tdsNoToneDetected;   // End of Ced G.164 tone
       }

       if (!prevG165Flag && pCedChan->G165Flag && pCedChan->G164Flag) {
           *ToneTimeStamp8k = FrameTimeStamp8k - (ADT_UInt32)FrameSize8k;
           toneIndex = tdsG165;             // Start of Ced G.165 tone
       } else if (prevG165Flag && !pCedChan->G165Flag) {
           toneIndex = tdsNoToneDetected;   // End of Ced G.165 tone
       }
   }

   if (pCngChan != NULL) {   
      prevCngFlag = ((FAXCNGChannel_t *)pCngChan)->Result;
      newCngFlag = FAXCNG_ADT_toneDetect (pCngChan, pcmBuff, FrameSize8k, Scratch);

      if (prevCngFlag != newCngFlag) {
         if ((prevCngFlag != FAXCNG_TRAIL_EDGE) && (newCngFlag == FAXCNG_DIGIT)) {
           *ToneTimeStamp8k = FrameTimeStamp8k - (ADT_UInt32)FrameSize8k;
            toneIndex = tdsFaxCng;          //  Start of CNG tone
         } else if ((newCngFlag == FAXCNG_NULL) && ((prevCngFlag == FAXCNG_TRAIL_EDGE) || (prevCngFlag == FAXCNG_DIGIT))) {
            toneIndex = tdsNoToneDetected;  // End of CNG tone
         }
      }
   }

   if (toneIndex == tdsNoChange) 
      return;

   durationMs = (ADT_UInt16) ((FrameTimeStamp8k - *ToneTimeStamp8k) >> 3);
   toneToHost (tone->ChannelId, tone->deviceSide, toneIndex, durationMs);
   if (toneIndex == tdsNoToneDetected) *ToneTimeStamp8k = 0;
#endif
}



//-----------  Tone relay generation (generate tones from packets)
// Return 1 to indicate tone is still playing out
#pragma CODE_SECTION (ToneRelayGenerate, "MEDIUM_PROG_SECT")
int ToneRelayGenerate (TGInfo_t* tgInfo, int FrameSamps, ADT_PCM16 *pOutBufr) {

   if (!SysAlgs.toneRlyGenEnable) return 0;
   if (tgInfo->toneRlyGenPtr == NULL) return 0;
   if (!tgInfo->regenActive) return 0;

   
   STATS (FrameStats.toneRelayGenCnt++;)
   tgInfo->regenActive = (GpakActivation) TR_ADT_generator (tgInfo->toneRlyGenPtr, (ADT_Int16) FrameSamps, (ADT_Int16 *) pOutBufr);
   return tgInfo->regenActive;
}


//-----------------    Tone generation (generate tone from host messages)
const ADT_Int16 dtmfTones[16][2] = {
    697, 1209,    /* DTMF Digit 1 */
    697, 1336,    /* DTMF Digit 2 */
    697, 1477,    /* DTMF Digit 3 */
    697, 1633,    /* DTMF Digit A */
    770, 1209,    /* DTMF Digit 4 */
    770, 1336,    /* DTMF Digit 5 */
    770, 1477,    /* DTMF Digit 6 */
    770, 1633,    /* DTMF Digit B */
    852, 1209,    /* DTMF Digit 7 */
    852, 1336,    /* DTMF Digit 8 */
    852, 1477,    /* DTMF Digit 9 */
    852, 1633,    /* DTMF Digit C */
    941, 1209,    /* DTMF Digit * */
    941, 1336,    /* DTMF Digit 0 */
    941, 1477,    /* DTMF Digit # */
    941, 1633};   /* DTMF Digit D */

void DtmfDialInit (TGInfo_t *tgInfo) {
   TGParams_1_t tg;
   DtmfDialInfo_t  *dial;   
   ADT_UInt16 digit;

   dial = tgInfo->dtmfDialParmPtr;

   memset(&tg, 0, sizeof(TGParams_1_t));
   digit = dial->Digits[dial->CurrentDigit];
   tg.Level[0] = (dial->Level[0]*10);
   tg.Level[1] = (dial->Level[1]*10);
   tg.ToneType = TgContinuous;
   tg.NFreqs = 2;
   if (dial->Rate == 8000) {
      tg.Freq[0] = (dtmfTones[digit][0]*10);
      tg.Freq[1] = (dtmfTones[digit][1]*10);
   } else if (dial->Rate == 16000) {
      tg.Freq[0] = (dtmfTones[digit][0]*10) >> 1;
      tg.Freq[1] = (dtmfTones[digit][1]*10) >> 1;

   }
   
   TG_ADT_init_highres (tgInfo->toneGenPtr, &tg);//TG_ADT_init_1
   dial->CurrentDigit += 1;
   STATS (FrameStats.toneGenCnt++;)

}


#pragma CODE_SECTION (DtmfDialGenerate, "MEDIUM_PROG_SECT")
void DtmfDialGenerate (TGInfo_t *tgInfo, ADT_PCM16 *pcmBuff, int FrameSize) {
   DtmfDialInfo_t  *dial;   
   ADT_UInt16 nsilence, ntone; 

   // If the dtmf dial parameters were updated, check if initialization is needed.
   dial = tgInfo->dtmfDialParmPtr;
   if (tgInfo->update) {
        tgInfo->update = Disabled;
         CLEAR_INST_CACHE (dial, sizeof (DtmfDialInfo_t));
         FLUSH_wait ();
         DtmfDialInit (tgInfo);
   }

   if (dial->State == DialToneOn) {
      nsilence = 0;
      ntone = FrameSize;
      
      if (dial->NSampsGen + FrameSize > dial->NSampsOn) {
            ntone = dial->NSampsOn - dial->NSampsGen;
            nsilence = FrameSize - ntone;
      }

      TG_ADT_generate (tgInfo->toneGenPtr, ntone, pcmBuff);

      dial->NSampsGen += ntone;

      if (dial->NSampsGen == dial->NSampsOn) {
            if (nsilence)
                memset(&pcmBuff[ntone], 0, sizeof(ADT_PCM16) * nsilence);
            dial->State = DialToneOff;
            dial->NSampsGen = nsilence;
      }

   } else if (dial->State == DialToneOff) {
      nsilence = FrameSize;
      ntone = 0;
      
      if (dial->NSampsGen + FrameSize > dial->NSampsOff) {
            nsilence = dial->NSampsOff - dial->NSampsGen;
            ntone = FrameSize - nsilence;
      }

      memset(pcmBuff, 0, sizeof(ADT_PCM16) * nsilence);

      dial->NSampsGen += nsilence;

      if (dial->NSampsGen == dial->NSampsOff) {

            if (dial->CurrentDigit < dial->NumDigits) {
                // re-init the tone generator with next digit
                DtmfDialInit (tgInfo);

                if (ntone) 
                    TG_ADT_generate (tgInfo->toneGenPtr, ntone, &pcmBuff[nsilence]);
                dial->State = DialToneOn;
                dial->NSampsGen = ntone;
            } else {
                // No more digits to generate.
                if (ntone)
                    memset(&pcmBuff[nsilence], 0, sizeof(ADT_PCM16) * ntone);

                CLEAR_INST_CACHE (dial, sizeof (DtmfDialInfo_t));
                tgInfo->dtmfDial = Disabled;

                // todo, send message to host
            }
      }

   }

}

#pragma CODE_SECTION (ToneGenerate, "MEDIUM_PROG_SECT")
void ToneGenerate (TGInfo_t *tgInfo, ADT_PCM16 *pcmBuff, int FrameSize) {

   if (tgInfo->dtmfDial == Enabled) {
        DtmfDialGenerate (tgInfo, pcmBuff, FrameSize);
        return;    
   }

   // If the tone parameters were updated, check if initialization is needed.
   if (tgInfo->update) {
        tgInfo->update = Disabled;

      // New tone is detected, initialize the tone generator, 
      if (tgInfo->cmd == ToneGenStart) {
         CLEAR_INST_CACHE (tgInfo->toneGenParmPtr, sizeof (TGParams_1_t));
         FLUSH_wait ();
         TG_ADT_init_highres (tgInfo->toneGenPtr, tgInfo->toneGenParmPtr);//TG_ADT_init_1
         CLEAR_INST_CACHE (tgInfo->toneGenParmPtr, sizeof (TGParams_1_t));
         tgInfo->active = Enabled;
         STATS (FrameStats.toneGenCnt++;)
      } else
        tgInfo->active = Disabled;
   }

   // Generate the tone data for this frame and update active state
   if (tgInfo->active) {
      tgInfo->active = (GpakActivation) TG_ADT_generate (tgInfo->toneGenPtr, FrameSize, pcmBuff);
   }
   return;
}

//} - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//++++++++++  Scheduling
//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// initDMAPhaseCounters
//
// FUNCTION
//   Phase counters moved to frame task structure and initialized once at start up.
//
//
// RETURNS
//  nothing
//}
#pragma CODE_SECTION (initDMAPhaseCounters, "SLOW_PROG_SECT")
void initDMAPhaseCounters () {
   return;
}

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// FrameRatePhaseCount
//
// FUNCTION
//  Determines the number of samples that have been collected and not yet
//  processed by the framing task specified by frame size
//
//  INPUTS
//     FrameSize - Identifies framing task
//
// RETURNS
//    Samples DMA'd into the circular buffers since the last time the
//    framing task was run.
//
// NOTES
//    Frame sizes of 2.5 ms are handled by alternating the interval between
//    the framing task between 2 and 3 milliseconds to produce an average of
//    2.5 ms. An adjustment of 4 samples (1/2 ms) is made during the 2 ms
//    interval to account for the extra 4 sample obtained during the 3 ms
//    interval.
//}
#pragma CODE_SECTION (FrameRatePhaseCount, "SLOW_PROG_SECT")
ADT_Word FrameRatePhaseCount (int coreID, int SamplesPerFrame) {
   int i;
   unsigned int phase, fs;
   FrameTaskData_t *FrameData;

   // Return current phase (number of samples collected) of frame task
   FrameData = &FrameTaskData[coreID * FRAME_TASK_CNT];
   for (i=0; i<frameTaskCnt; i++, FrameData++) {
      fs = FrameData->SampsPerFrame;
      if (SamplesPerFrame != fs) continue;

      phase = FrameData->SamplesPending;
      if (phase < sysConfig.samplesPerMs) phase += fs;

      return phase;
   }
   AppErr ("Frame Size", SamplesPerFrame);
   return 0;
}

const int adjustMarker = 0x7f7f8080;
inline adjustTake (CircBufInfo_t *buf, int adjustSampCnt) {
   adjustSampCnt = adjustSampCnt % buf->BufrSize;
   buf->TakeIndex += adjustSampCnt;
   if (buf->BufrSize <= buf->TakeIndex) buf->TakeIndex -= buf->BufrSize;
   LogTransfer (0x12AD00, buf, buf->pBufrBase, NULL, NULL, adjustSampCnt);
}

inline adjustPut (CircBufInfo_t *buf, int adjustSampCnt) {
   int zeroI16;
   ADT_UInt16  *adjustBuf;

   adjustSampCnt = adjustSampCnt % buf->BufrSize;
   adjustBuf = buf->pBufrBase + buf->PutIndex;
   if (buf->BufrSize < buf->PutIndex + adjustSampCnt) {
      zeroI16 = buf->BufrSize - buf->PutIndex;
      memset (adjustBuf, 0, zeroI16 * sizeof(ADT_PCM16));
      buf->PutIndex = 0;
      adjustSampCnt -= zeroI16;
      adjustBuf = buf->pBufrBase;
   }
   memset (adjustBuf, 0, adjustSampCnt * sizeof(ADT_PCM16));
   buf->PutIndex += adjustSampCnt;
   LogTransfer (0x12AA00, buf, buf->pBufrBase, NULL, NULL, adjustSampCnt);
}

#pragma CODE_SECTION (fromTDMCircAdjust, "MEDIUM_PROG_SECT")
void fromTDMCircAdjust (chanInfo_t *chan, GpakDeviceSide_t deviceSide, int *SampsPerFrame) {
   GpakSerialPort_t port;
   CircBufInfo_t *circBuf;

   if (deviceSide == ADevice) {
      port    = chan->pcmToPkt.InSerialPortId;
      circBuf = chan->pcmToPkt.activeInBuffer;
   } else if (deviceSide == BDevice) {
      port    =  chan->pktToPcm.InSerialPortId;
      circBuf =  chan->pktToPcm.inbuffer;
   } else if (deviceSide == ASideECFar) {
      port    =  chan->pktToPcm.OutSerialPortId;
      circBuf = &chan->pcmToPkt.ecFarPtr;
   } else {
      AppErr ("Invalid device", TRUE);
      return;
   }

   if (chan->ProcessRate < TDMRate) *SampsPerFrame /= 2;
   if ((SerialPortNull == port) || (circBuf == NULL))    return;

   adjustTake (circBuf, *SampsPerFrame);

   if (deviceSide == ADevice)
      captureEncoderInput (chan, (void *)&adjustMarker, sizeof (adjustMarker));
   else if (deviceSide == BDevice)
      captureDecoderInput (chan, (void *)&adjustMarker, sizeof (adjustMarker));
   return;
}

#pragma CODE_SECTION (toTDMCircAdjust, "MEDIUM_PROG_SECT")
void toTDMCircAdjust (chanInfo_t *chan, GpakDeviceSide_t deviceSide, int SampsPerFrame) {
   GpakSerialPort_t port;
   CircBufInfo_t *circBuf;

   if (deviceSide == ADevice) {
      port    = chan->pcmToPkt.OutSerialPortId;
      circBuf = chan->pcmToPkt.outbuffer;
   } else if (deviceSide == BDevice) {
      port    =  chan->pktToPcm.OutSerialPortId;
      circBuf = &chan->pktToPcm.outbuffer;
   } else {
      AppErr ("Invalid device", TRUE);
      return;
   }

   if (SerialPortNull == port)
       return;

   if (chan->ProcessRate < TDMRate) SampsPerFrame *= 2;

   adjustPut (circBuf, SampsPerFrame);

   if (deviceSide == ADevice)
      captureEncoderOutput (chan, (void *)&adjustMarker, sizeof (adjustMarker));
   else
      captureDecoderOutput (chan, (void *)&adjustMarker, sizeof (adjustMarker));
   return;

}

#pragma CODE_SECTION (MultiRateCnfrFrameAdjust, "MEDIUM_PROG_SECT")
void MultiRateCnfrFrameAdjust (chanInfo_t *chan, int adjustSampCnt) {
   adjustPut  (chan->pktToPcm.inbuffer,  adjustSampCnt);
   adjustTake (chan->pcmToPkt.outbuffer, adjustSampCnt);
   captureDecoderInput   (chan, (void *)&adjustMarker, sizeof (adjustMarker));
   captureEncoderOutput  (chan, (void *)&adjustMarker, sizeof (adjustMarker));
}

#pragma CODE_SECTION (PcmA2PcmBFrameAdjust, "MEDIUM_PROG_SECT")
void PcmA2PcmBFrameAdjust  (chanInfo_t *chan, int adjustSampCnt) {
   int SampsPerFrame;

   SampsPerFrame = adjustSampCnt;
   fromTDMCircAdjust (chan, ADevice, &SampsPerFrame);
   if (Enabled == chan->pcmToPkt.EchoCancel) {
      SampsPerFrame = (int) adjustSampCnt;
      fromTDMCircAdjust (chan, ASideECFar, &SampsPerFrame);
   } else if (Enabled == chan->pcmToPkt.AECEchoCancel) {
      toTDMCircAdjust (chan, BDevice, SampsPerFrame);
   }
   if (Enabled == chan->pktToPcm.EchoCancel)
      adjustPut (&chan->pktToPcm.ecFarPtr, SampsPerFrame);

   toTDMCircAdjust (chan, ADevice, SampsPerFrame);
}
#pragma CODE_SECTION (PcmB2PcmAFrameAdjust, "MEDIUM_PROG_SECT")
void PcmB2PcmAFrameAdjust  (chanInfo_t *chan, int adjustSampCnt) {
   int SampsPerFrame;

   SampsPerFrame = adjustSampCnt;
   fromTDMCircAdjust (chan, BDevice, &SampsPerFrame);
   if (Enabled == chan->pktToPcm.EchoCancel)
      adjustTake (&chan->pktToPcm.ecFarPtr, SampsPerFrame);

   if (Enabled == chan->pcmToPkt.AECEchoCancel) {
      if (chan->pcmToPkt.ecFarPtr.BufrSize < SampsPerFrame)
        SampsPerFrame =  chan->pcmToPkt.ecFarPtr.BufrSize;
      memset (chan->pcmToPkt.ecFarPtr.pBufrBase, 0, SampsPerFrame * sizeof (ADT_PCM16));
   } else
      toTDMCircAdjust (chan, BDevice, SampsPerFrame);
}

#pragma CODE_SECTION (Pkt2PcmFrameAdjust, "MEDIUM_PROG_SECT")
void Pkt2PcmFrameAdjust (chanInfo_t *chan, int adjustSampCnt) {
   int SampsPerFrame;

   SampsPerFrame = adjustSampCnt;

   if (chan->pktToPcm.OutSerialPortId == SerialPortNull) 
      return;

   captureDecoderInput (chan, (void *)&adjustMarker, sizeof (adjustMarker));
   if (Enabled == chan->pktToPcm.EchoCancel) 
     adjustTake (&chan->pktToPcm.ecFarPtr, SampsPerFrame);

   if (Enabled != chan->pcmToPkt.AECEchoCancel)
      toTDMCircAdjust (chan, BDevice, SampsPerFrame);
   return;

}
#pragma CODE_SECTION (Pcm2PktFrameAdjust, "MEDIUM_PROG_SECT")
void Pcm2PktFrameAdjust (chanInfo_t *chan, int adjustSampCnt) {
   int SampsPerFrame;

   SampsPerFrame = adjustSampCnt;
   if (chan->pcmToPkt.InSerialPortId == SerialPortNull)
      return;

   fromTDMCircAdjust (chan, ADevice, &SampsPerFrame);

   if (Enabled == chan->pcmToPkt.AECEchoCancel) {
      toTDMCircAdjust (chan, BDevice, SampsPerFrame);
   } else if (Enabled == chan->pcmToPkt.EchoCancel) {
      SampsPerFrame = adjustSampCnt;
      fromTDMCircAdjust (chan, ASideECFar, &SampsPerFrame);
   }

   if ((chan->fax.faxMode != disabled) && (chan->fax.t38InProgress))
      return;

   if (Enabled == chan->pktToPcm.EchoCancel)
      adjustPut (&chan->pktToPcm.ecFarPtr, SampsPerFrame);

   captureEncoderOutput  (chan, (void *)&adjustMarker, sizeof (adjustMarker));
   return;
}

#pragma CODE_SECTION (PktPktDecodeFrameAdjust, "MEDIUM_PROG_SECT")
void PktPktDecodeFrameAdjust (chanInfo_t *chan, int adjustSampCnt) {
   ADT_Bool   BActive;

   captureDecoderInput  (chan, (void *)&adjustMarker, sizeof (adjustMarker));
   captureDecoderOutput (chan, (void *)&adjustMarker, sizeof (adjustMarker));

   BActive = ((chanTable[chan->PairedChannelId]->Flags & CF_ACTIVE) && 
              (chanTable[chan->PairedChannelId]->pcmToPkt.Coding != NullCodec));

   if (BActive)
      adjustPut (&chan->pktToPcm.outbuffer, adjustSampCnt);
}
#pragma CODE_SECTION (PktPktEncodeFrameAdjust, "MEDIUM_PROG_SECT")
void PktPktEncodeFrameAdjust (chanInfo_t *chan, int adjustSampCnt) {
   chanInfo_t *chanB = chanTable[chan->PairedChannelId];
   ADT_Bool   BActive;

   if (chan->pcmToPkt.Coding == NullCodec) return;

   if (chan->ProcessRate < TDMRate)
      adjustSampCnt /= (TDMRate / chan->ProcessRate);
   else if (TDMRate < chan->ProcessRate)
      adjustSampCnt *= (chan->ProcessRate / TDMRate);

   BActive = ((chanB->Flags & CF_ACTIVE) && (chanB->pktToPcm.Coding != NullCodec));
   if (BActive)
      adjustTake (chan->pcmToPkt.activeInBuffer, adjustSampCnt);
   
   captureEncoderInput (chan, (void *)&adjustMarker, sizeof (adjustMarker));
   if ((Enabled == chan->pktToPcm.EchoCancel) && BActive)
      adjustTake (&chan->pktToPcm.ecFarPtr, adjustSampCnt);

   if ((Enabled == chanB->pktToPcm.EchoCancel) && BActive) 
      adjustPut (&chanB->pktToPcm.ecFarPtr, adjustSampCnt);

   captureEncoderOutput (chan, (void *)&adjustMarker, sizeof (adjustMarker));
}

#pragma CODE_SECTION (ProcessConferenceFrameAdjust, "MEDIUM_PROG_SECT")
void  ProcessConferenceFrameAdjust (ConferenceInfo_t *Cnfr, int adjustSampCnt) {

   chanInfo_t *chan;    // pointer to Channel Info
   pcm2pkt_t *PcmPkt;   // Channel's PCM to Packet control info
   pkt2pcm_t *PktPcm;   // Channel's Packet to PCM control info

   int SampsPerFrame;

   // Adjust decode side buffer pointers
   for (chan = Cnfr->MemberList; chan != NULL; chan = chan->nextPcmToPkt) {
      if ((chan->Flags & CF_ACTIVE) == 0) continue;

      PcmPkt = &chan->pcmToPkt;
      PktPcm = &chan->pktToPcm;
      SampsPerFrame = adjustSampCnt;
      chan->underflowCount++;

      // Adjust input buffers
      if (chan->channelType == conferencePcm) {
         fromTDMCircAdjust (chan, ADevice, &SampsPerFrame);
      } else if (chan->channelType == conferenceMultiRate) {
         if (chan->ProcessRate < TDMRate)
            SampsPerFrame /= (TDMRate / chan->ProcessRate);
         else
            SampsPerFrame *= (chan->ProcessRate / TDMRate);
         adjustTake (&PcmPkt->inbuffer, SampsPerFrame);
      } else if (chan->channelType == conferencePacket)  {
         captureDecoderInput  (chan, (void *)&adjustMarker, sizeof (adjustMarker));
      }

      // Adjust EC buffers
      if (Enabled == PcmPkt->AECEchoCancel) {
          toTDMCircAdjust (chan, BDevice, SampsPerFrame);
      } else if (Enabled == PcmPkt->EchoCancel) {
         if (chan->channelType == conferencePcm) {
            SampsPerFrame = adjustSampCnt;
            fromTDMCircAdjust (chan, ASideECFar, &SampsPerFrame);
         } else
            adjustTake (&PcmPkt->ecFarPtr, SampsPerFrame);
      }
      if (chan->channelType != conferenceMultiRate)
         PktPcm->FrameTimeStamp += SampsPerFrame;

      if ((chan->channelType == conferenceMultiRate) || (chan->channelType == conferencePacket))
         captureDecoderOutput (chan, (void *)&adjustMarker, sizeof (adjustMarker));
   }

   // Adjust encode side buffer pointers
   for (chan = Cnfr->MemberList; chan != NULL; chan = chan->nextPcmToPkt)   {
      if ((chan->Flags & CF_ACTIVE) == 0) continue;

      PcmPkt = &chan->pcmToPkt;
      PktPcm = &chan->pktToPcm;
      SampsPerFrame = adjustSampCnt;
      chan->overflowCount++;

      if (TDMRate < chan->ProcessRate)
         SampsPerFrame *= (chan->ProcessRate / TDMRate);
      else
         SampsPerFrame /= (TDMRate / chan->ProcessRate);

      if ((chan->channelType == conferenceMultiRate) || (chan->channelType == conferencePacket))
         captureEncoderInput (chan, (void *)&adjustMarker, sizeof (adjustMarker));

      if (chan->channelType == conferencePcm) {
         if (PktPcm->OutSerialPortId != SerialPortNull)  {
            if (Enabled == PcmPkt->AECEchoCancel) {
               if (PcmPkt->ecFarPtr.BufrSize < SampsPerFrame)
                  SampsPerFrame = PcmPkt->ecFarPtr.BufrSize;
               memset (PcmPkt->ecFarPtr.pBufrBase, 0, SampsPerFrame * sizeof (ADT_PCM16));
            } else {
               toTDMCircAdjust (chan, BDevice, SampsPerFrame);
            }
         }  
      } else if (chan->channelType == conferenceMultiRate) {
         if (PcmPkt->EchoCancel == Enabled)
            adjustPut (&PcmPkt->ecFarPtr, SampsPerFrame);

         adjustPut (&PktPcm->outbuffer, SampsPerFrame);
      } else if (chan->channelType == conferencePacket) {

         if (PcmPkt->EchoCancel == Enabled)
            adjustPut (&PcmPkt->ecFarPtr, SampsPerFrame);

         captureEncoderOutput (chan, (void *)&adjustMarker, sizeof (adjustMarker));
         PcmPkt->FrameTimeStamp += SampsPerFrame;
      }
   }

   // Adjust composite encode side buffer pointers
   for (chan = Cnfr->CompositeList; chan != NULL;  chan = chan->nextPcmToPkt) {
      if ((chan->Flags & CF_ACTIVE) == 0) continue;

      PcmPkt = &chan->pcmToPkt;
      PktPcm = &chan->pktToPcm;
      SampsPerFrame = adjustSampCnt;
      chan->overflowCount++;

      if (TDMRate < chan->ProcessRate)
         SampsPerFrame *= (chan->ProcessRate / TDMRate);
      else
         SampsPerFrame /= (TDMRate / chan->ProcessRate);

      if (PktPcm->OutSerialPortId != SerialPortNull)
         toTDMCircAdjust (chan, BDevice, SampsPerFrame);
      if (PcmPkt->Coding != NullCodec)
         captureEncoderOutput (chan, (void *)&adjustMarker, sizeof (adjustMarker));

      PktPcm->FrameTimeStamp += Cnfr->FrameSize;
      PcmPkt->FrameTimeStamp += Cnfr->FrameSize;
   }
}

#pragma CODE_SECTION (customDecodeFrameAdjust, "SLOW_PROG_SECT")
void customDecodeFrameAdjust (chanInfo_t *chan, int adjustSampCnt) {
}
#pragma CODE_SECTION (customEncodeFrameAdjust, "SLOW_PROG_SECT")
void customEncodeFrameAdjust (chanInfo_t *chan, int adjustSampCnt) {
}

#pragma CODE_SECTION (customProcessConferenceAdjust, "SLOW_PROG_SECT")
int customProcessConferenceAdjust (unsigned int i, ConferenceInfo_t *Cnfr, 
                                   ConfInstance_t *CnfrInst, int adjustSampCnt) {
   return 0;
}

#pragma CODE_SECTION (GpakAdjustChannelPointers, "MEDIUM_PROG_SECT")
ADT_Bool GpakAdjustChannelPointers (FrameTaskData_t *Tsk) {
   ConferenceInfo_t *Cnfr;
   chanInfo_t       *chan;
   int i, adjustSampCnt, adjustFramesCnt, adjustedChannels;

   // Check for framing task MIPs overrun.
   adjustFramesCnt = Tsk->FramesScheduled - Tsk->FramesProcessed;
   Tsk->FramesProcessed = Tsk->FramesScheduled;

   if (NumActiveChannels == 0) return ADT_FALSE;

   if (adjustFramesCnt <= 1) return (adjustFramesCnt != 1);

   adjustSampCnt = adjustFramesCnt * Tsk->SampsPerFrame;
   adjustedChannels = 0;

   //-------------------------------------------------
   // Adjust decode framing pointers for each non conference channel.
   for (chan = Tsk->PktToPcmHead; chan != NULL; chan = chan->nextPktToPcm)   {
      if ((chan->Flags & CF_ACTIVE) == 0) continue;

      switch (chan->channelType) {
      case conferenceMultiRate:   MultiRateCnfrFrameAdjust (chan, adjustSampCnt);    break;
      case pcmToPcm:              PcmB2PcmAFrameAdjust    (chan, adjustSampCnt);     break;
      case pcmToPacket:           Pkt2PcmFrameAdjust      (chan, adjustSampCnt);     break;
      case packetToPacket:        PktPktDecodeFrameAdjust (chan, adjustSampCnt);     break;
      case customChannel:         customDecodeFrameAdjust (chan, adjustSampCnt);     break;
      case inactive:              continue;
      default:                    AppErr ("Unknown channel type", chan->channelType);   break;
      }

      // Increment the channel's frame stamp
      chan->pktToPcm.FrameTimeStamp += adjustSampCnt;
      chan->underflowCount++;
      adjustedChannels++;
   }

   //-------------------------------------------------
   // Adjust framing pointers for custom conference channels
   for (i = 0; i < sysConfig.customCnfrCnt; i++)   {

      Cnfr = GetGpakCnfr (DSPCore, i + sysConfig.maxNumConferences);
      if (Cnfr->FrameSize != Tsk->SampsPerFrame) continue;

       adjustedChannels = customProcessConferenceAdjust (i, Cnfr, Cnfr->Inst, adjustSampCnt);
   }

   //-------------------------------------------------
   // Adjust encode framing pointers for each non conference channel.
   for (chan = Tsk->PcmToPktHead; chan != NULL; chan = chan->nextPcmToPkt) {
      if ((chan->Flags & CF_ACTIVE) == 0) continue;

      switch (chan->channelType) {
      case pcmToPcm:         PcmA2PcmBFrameAdjust (chan, adjustSampCnt);         break;
      case pcmToPacket:      Pcm2PktFrameAdjust   (chan, adjustSampCnt);         break;
      case packetToPacket:   PktPktEncodeFrameAdjust (chan, adjustSampCnt);      break;
      case customChannel:    customEncodeFrameAdjust (chan, adjustSampCnt);      break;
      case inactive:         break;
      default:         AppErr ("Unknown channel type", chan->channelType);     break;
      }
      chan->pcmToPkt.FrameTimeStamp += adjustSampCnt;
      chan->overflowCount++;
      adjustedChannels++;
   }

   //-------------------------------------------------
   // Adjust framing pointers for conference channels
   for (i = 0; i < sysConfig.maxNumConferences; i++)   {

      Cnfr = GetGpakCnfr (DSPCore, i);
      if (Cnfr->FrameSize != Tsk->SampsPerFrame)  continue;

      if ((Cnfr->MemberList == NULL) && (Cnfr->CompositeList == NULL))    continue;
      ProcessConferenceFrameAdjust (Cnfr, adjustSampCnt);
      adjustedChannels++;
   }
   if (!adjustedChannels) return ADT_FALSE;

   AppErr ("MIPs Overrun Adjustment",  adjustSampCnt);
   return ADT_TRUE;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ScheduleFramingTasks - Schedule G.PAK Framing tasks.
//{
// FUNCTION
//   This function schedules the framing tasks at each task's particular rate.
//  It should be called after every 8 PCM samples occur or every 1 msec if PCM
//   I/O is not configured.
//
// RETURNS
//  nothing
//}
extern volatile chanInfo_t *PendingChannel;
extern void pendingChannelSetup ();

#pragma CODE_SECTION (ScheduleFramingTasks, "FAST_PROG_SECT")
void ScheduleFramingTasks (void) {

   FrameTaskData_t *frameTask;
   ADT_UInt32  frameStartTime;
   int i, coreID, frameBit;
   int frameBits[8];

   frameStartTime = CLK_gethtime();

   // Schedule framing tasks when sufficient samples are pending
   for (coreID = 0; coreID < DSPTotalCores; coreID++) {
      frameTask = &FrameTaskData[coreID * FRAME_TASK_CNT];
      frameBits[coreID] = 0;
      for (i=0, frameBit = 0x10; i< frameTaskCnt & i< FRAME_TASK_CNT; i++, frameTask++, frameBit = frameBit << 1) {

         frameTask->SamplesPending += sysConfig.samplesPerMs;
         if (frameTask->SamplesPending < frameTask->SampsPerFrame) continue;

         frameTask->ScheduledStart = frameStartTime;
         frameTask->SamplesPending -= frameTask->SampsPerFrame;
         frameTask->FramesScheduled++;

         // Start frame task running. If multicore system, just determine
         // which cores/frames are due to run.
         if (DSPTotalCores == 1) {
            SWI_post (frameTask->SWI);
            logTime (0x100A0000 | frameTask->SampsPerFrame);
         } else {
            frameBits[coreID] |= frameBit;
         }
         // Move pre-pending queue to pending queue for core/frames that are
         // scheduled to run.  
         MovePPendToPendingQueue (coreID, frameTask->SampsPerFrame);
      }
   }

   // add pending channels to the TDM DMA list
   if (PendingChannel != NULL) {
      pendingChannelSetup ();
      PendingChannel = NULL;
   }

   // Generate the interrupt to cores that have framing SWIs due to run.
   if (1 < DSPTotalCores) {
       for (coreID = minChanCore; coreID < DSPTotalCores; coreID++) {
           if (!frameBits[coreID]) continue;
           logTime (0x100A0000 | ((frameBits[coreID] << 8) | coreID));
           Gen_Core_Interrupt (frameBits[coreID], coreID);
       }
   }

   if ((ApiBlock.DmaSwiCnt & 0x1f) == 0) {
      ToggleFrameTaskLED ();
   }

   // Increment a count of DMA Software interrupts.
   ApiBlock.DmaSwiCnt++;
   return;
}


#define ctrl pChan->algCtrl
#pragma CODE_SECTION (AlgorithmControl, "MEDIUM_PROG_SECT")
GpakAlgCtrl_t AlgorithmControl (chanInfo_t **chan, frmType_t frmType) {
   G168Params_t *pG168Parms;
   G168ChannelInstance_t *pEcChan;
   ADT_UInt16 FrameSize, EcID;
   chanInfo_t *pChan = *chan;
   ADT_Bool   correctSide;

   if (ctrl.ControlCode == ALG_CTRL_VOID_CMD) return ALG_CTRL_VOID_CMD;
   logTime (0x0080000ul | (ctrl.ControlCode<< 8) | pChan->ChannelId);

   // Detemine if algorhithm control is directed to current frame type.
   if (frmType == FrmDecode)         // FrmDecode = BDevice
      correctSide = (ctrl.AorB == BDevice);
   else if (frmType == FrmEncode)    // FrmEncode = ADevice
      correctSide = (ctrl.AorB == ADevice);
   else if (frmType == CnfrMembers)  // Conference = AorB ,  Multirate = ADevice only
      correctSide = (ctrl.AorB == ADevice) || (pChan->channelType != conferenceMultiRate);
   else                              // Composite conferences = AorB
      correctSide = ADT_TRUE;

   switch (ctrl.ControlCode) {
   case ALG_CTRL_DEQUEUE_DECD:  // Process both stages of dequeue before completing channel teardown
   case ALG_CTRL_DEQUEUE_ENCD:
   case ALG_CTRL_DEQUEUE_CMD:
      pChan->Flags &= ~CF_ACTIVE;
      pChan->channelType = inactive;
      DequeueChannelFromFrame (chan, frmType);
      if (frmType == FrmDecode) ctrl.ControlCode &= ~ALG_CTRL_DEQUEUE_DECD;
      else                      ctrl.ControlCode &= ~ALG_CTRL_DEQUEUE_ENCD;

      if (ctrl.ControlCode == 0) {  // Channel removed from both queues
          if (SysAlgs.rtpHostDelivery || SysAlgs.rtpNetDelivery) {
            // Shut down channel's RTP instance and remove the channel from it's frame
            rtcp_channel_null (pChan->ChannelId);
            if (rtcp_cached) {
                FLUSH_INST_CACHE (rtcpData[pChan->ChannelId], sizeof (rtcpdata_t));
            }
            RTP_ADT_Close ((RTPCONNECT *) &RtpChanData[pChan->ChannelId]->Instance);
            RtpChanData[pChan->ChannelId]->Instance.State = 0;
            FLUSH_INST_CACHE (&RtpChanData[pChan->ChannelId]->Instance.State, 4);
         }
         invalidateChannelCache (pChan);
         teardownLog(pChan->ChannelId, 6);
         ctrl.ControlCode = ALG_CTRL_DEQUEUE_CMPLT;
      }
      return ALG_CTRL_DEQUEUE_CMD;

   case BypassEcan:
   case EnableEcan:
      if (!correctSide) return ALG_CTRL_VOID_CMD;

      if (ctrl.AorB == ADevice)
         pG168Parms = (G168Params_t *)pChan->pcmToPkt.pG168Chan;
      else
         pG168Parms = (G168Params_t *)pChan->pktToPcm.pG168Chan;
      pG168Parms->BypassEnable = (ctrl.ControlCode == BypassEcan);
      break;

   case EnableEcanAdapt:
   case BypassEcanAdapt:
      if (!correctSide) return ALG_CTRL_VOID_CMD;

      if (ctrl.AorB == ADevice)
         pG168Parms = (G168Params_t *)pChan->pcmToPkt.pG168Chan;
      else
         pG168Parms = (G168Params_t *)pChan->pktToPcm.pG168Chan;
      pG168Parms->AdaptEnable = (ctrl.ControlCode == EnableEcanAdapt);
      break;

   case ResetEcan:
      if (!correctSide) return ALG_CTRL_VOID_CMD;

      if (ctrl.AorB == ADevice) {
         FrameSize = pChan->pcmToPkt.SamplesPerFrame;
         EcID = pChan->pcmToPkt.EcIndex;
         pEcChan = pChan->pcmToPkt.pG168Chan;
      } else {
         FrameSize = pChan->pktToPcm.SamplesPerFrame;
         EcID = pChan->pktToPcm.EcIndex;
         pEcChan = pChan->pktToPcm.pG168Chan;
      }
      EchoCancellerReInit (FrameSize, EcID, pEcChan, pChan);
      break;

   case UpdateEcanNLP:
      if (!correctSide) return ALG_CTRL_VOID_CMD;

      if (ctrl.AorB == ADevice)
          pG168Parms = (G168Params_t *) pChan->pcmToPkt.pG168Chan;
      else
         pG168Parms = (G168Params_t *) pChan->pktToPcm.pG168Chan;
      pG168Parms->NLPType = ctrl.NLP;
      break;

   case EnableVad:
      if (!correctSide) return ALG_CTRL_VOID_CMD;

      if (ctrl.AorB == ADevice)
         pChan->pcmToPkt.VAD |= (GpakVADMode) VadEnabled;
         if (pChan->pcmToPkt.VAD & VadCng) {
            VADCNG_ADT_init (pChan->pcmToPkt.vadPtr, &(pChan->pcmToPkt.VadParms));
            pChan->pcmToPkt.PrevVadState = SILENCE;
            if (sysConfig.VadReport == Enabled)
               pChan->pcmToPkt.VAD |= VadNotifyEnabled;
         }
      else {
         pChan->pktToPcm.VAD |= (GpakVADMode) VadEnabled;
         if (pChan->pktToPcm.VAD & VadCng) {
            VADCNG_ADT_init (pChan->pktToPcm.vadPtr, &(pChan->pktToPcm.VadParms));
            pChan->pktToPcm.PrevVadState = SILENCE;
            if (sysConfig.VadReport == Enabled)
               pChan->pktToPcm.VAD |= VadNotifyEnabled;
         }
		  
	  }
      break;

   case BypassVad:
      pChan->pcmToPkt.VAD &= (GpakVADMode) ~VadEnabled;
      break;

   case EnableVAGC:
      if (!correctSide) return ALG_CTRL_VOID_CMD;
      if (ctrl.AorB == ADevice) {
         pChan->pcmToPkt.AGC &= (GpakActivation) ~ALG_CTRL_BYPASS;
         if (pChan->pcmToPkt.AGC == Enabled)
             AGC_ADT_agcinit (pChan->pcmToPkt.agcPtr, &(pChan->pcmToPkt.AGCParms));
      } else {
         pChan->pktToPcm.AGC &= (GpakActivation) ~ALG_CTRL_BYPASS;
         if (pChan->pktToPcm.AGC == Enabled)
            AGC_ADT_agcinit (pChan->pktToPcm.agcPtr, &(pChan->pktToPcm.AGCParms));
      }
      break;

  case BypassVAGC:
      if (ctrl.AorB == ADevice)
         pChan->pcmToPkt.AGC |= (GpakActivation) ALG_CTRL_BYPASS;
      else
         pChan->pktToPcm.AGC |= (GpakActivation) ALG_CTRL_BYPASS;
      break;

   case ModifyToneDetector:
      if (!correctSide) return ALG_CTRL_VOID_CMD;
      if (ctrl.AorB == ADevice)
         reInitASideToneDetect (&pChan->pcmToPkt, ctrl.DeactTonetype, ctrl.ActTonetype);
      else
         reInitBSideToneDetect (&pChan->pktToPcm, ctrl.DeactTonetype, ctrl.ActTonetype);
      break;
         
   case EnableToneSuppress:
      if (ctrl.AorB == ADevice) {
         if (pChan->pcmToPkt.toneTypes & (DTMF_tone | CallProg_tone | MFR2Fwd_tone | MFR2Back_tone | Arbit_tone))
            pChan->pcmToPkt.toneTypes |= Tone_Squelch;
      } else {
         if (pChan->pktToPcm.toneTypes & (DTMF_tone | CallProg_tone | MFR2Fwd_tone | MFR2Back_tone | Arbit_tone))
            pChan->pktToPcm.toneTypes |= Tone_Squelch;
      }
      break;
       
   case BypassToneSuppress:
      if (ctrl.AorB == ADevice) 
         pChan->pcmToPkt.toneTypes &= ~Tone_Squelch;
      else
         pChan->pktToPcm.toneTypes &= ~Tone_Squelch;
      break;

   case UpdateTdmOutGain:
      if (ctrl.AorB == ADevice)
         pChan->pktToPcm.Gain.OutputGainG2 = ctrl.Gain;
      else
         pChan->pcmToPkt.Gain.OutputGainG2 = ctrl.Gain;
      break;

   case UpdateTdmInGain:
      if (ctrl.AorB == ADevice)
         pChan->pcmToPkt.Gain.InputGainG3 = ctrl.Gain;
      else
         pChan->pktToPcm.Gain.InputGainG3 = ctrl.Gain;
      break;

   case ResetChanStats:
      pChan->status.Word      = 0;
      pChan->invalidHdrCount  = 0;
      pChan->overflowCount    = 0;
      pChan->underflowCount   = 0;
      pChan->fromHostPktCount = 0;
      pChan->toHostPktCount   = 0;
      break;

   case RTPTransmitDisable:
      pChan->Flags |= CF_RTP_TX_DISABLED;
      break;

   case RTPTransmitEnable:
      pChan->Flags &= ~CF_RTP_TX_DISABLED;
      break;

   case RTPReceiveDisable:
      pChan->Flags |= CF_RTP_RX_DISABLED;
      break;

   case RTPReceiveEnable:
      pChan->Flags &= ~CF_RTP_RX_DISABLED;
      break;

   case CID2RXRestart:
      if (pChan->pcmToPkt.cidInfo.pRxCIDInst == NULL) break;
      pChan->pcmToPkt.cidInfo.type2CID = 1;
      rxCidInit (&pChan->pcmToPkt.cidInfo, pChan->pcmToPkt.SamplesPerFrame);
      break;

   case CID1RXRestart:
      if (pChan->pcmToPkt.cidInfo.pRxCIDInst == NULL) break;
      pChan->pcmToPkt.cidInfo.type2CID = 0;
      rxCidInit (&pChan->pcmToPkt.cidInfo, pChan->pcmToPkt.SamplesPerFrame);
      break;

   case SetCodecType:
      if (ctrl.AorB == ADevice) { // FrmEncode = ADevice 
         pChan->pcmToPkt.Coding = ctrl.CodecType;
		 pChan->Flags |= CF_REINIT_ENCODE;
      } else { // FrmDecode = BDevice
         pChan->pktToPcm.Coding = ctrl.CodecType;
		 pChan->Flags |= CF_REINIT_DECODE;
	  }
      break;

   default:
      break;
   };

   ctrl.ControlCode = ALG_CTRL_VOID_CMD;
   logTime (0x00810000ul | pChan->ChannelId);
   return ALG_CTRL_VOID_CMD;
}


#if (DSP_TYPE == 64)
// InterCore hardware interrupt handler
#pragma CODE_SECTION (frameReadyISR, "FAST_PROG_SECT")
void frameReadyISR (void) {
   ADT_UInt32  frameStartTime;
   int i, frameBits;
   FrameTaskData_t *FrameData;

   frameStartTime = CLK_gethtime();

   logTime (0x01020000 | DSPCore);
   frameBits = Read_Core_Interrupt (DSPCore);
   ACK_Core_Interrupt (frameBits, DSPCore);

   frameBits = frameBits >> 4;
   logTime (0x01030000 | frameBits);

   FrameData = &FrameTaskData[DSPCore * FRAME_TASK_CNT];
   for (i=0; i<frameTaskCnt && frameBits;
             i++, frameBits = frameBits >> 1, FrameData++) {
      if ((frameBits & 1) == 0) continue;
      FrameData->ScheduledStart = frameStartTime;
      
      SWI_post (FrameData->SWI);
   }
   logTime (0x01040000 | DSPCore);
}
#endif

void ADT_GainBlockWrapper(
	ADT_Int16 GainValue,	// gain value (units of .1 dBm)
	ADT_Int16 *pInput,		// pointer to Input buffer
	ADT_Int16 *pOutput,		// pointer to Output buffer
	ADT_Int16 Length		// Length of Array to be processed
	) 
{

    if (GainValue == (ADT_Int16)ADT_GAIN_MUTE) {
        memset(pOutput, 0, sizeof(ADT_Int16)*Length);
        return;
    } 
    ADT_GainBlock(GainValue, pInput, pOutput, Length);

}
