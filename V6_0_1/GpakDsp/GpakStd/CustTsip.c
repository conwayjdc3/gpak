//{ TrilogyInit.c  (C6748)
//
// G.PAK initialization for Trilogy 
//
//---------------------------------------------------------------
//  Custom stubs.
//
//  customDSPInit      Customized configuration before GPAK initialization
//  customDSPStartUp   Customized configuration after GPAK initialization
//  customEncodeInit   Initialization of custom encoder
//  customDecodeInit   Initialization of custom decoder
//  customEncode       Converts pcm data to payload data.
//  customDecode       Converts payload data to pcm data
//  customMsg          Processes a custom G.PAK message
//  customIdle         User hook for idle processing
//}
#define _CSL_H_         // Avoid csl definitions
#include <stdio.h>
#include <std.h>
#include <stdio.h>
#include <stdint.h>

#include <string.h>
#include <soc.h>

#include "adt_typedef.h"
#include "GpakDefs.h"
#include "GpakExts.h"
#include "GpakErrs.h"
#include "sysconfig.h"
#include "GpakHpi.h"


// Each slotMask entry is a 32-bit mask that corresponds to 32 consective time slots on a given TSIP link.
// The number of links (serial port pins) connected to the TSIP is configurable.
//
// One bit for each corresponding time slot that the DSP must process must be enabled in order for the DSP
// to transfer data on that wire.
//
// The following table can be used to determine the starting offset into the slotMask array that corresponds to
// the first 32 slots (0-31) on that link.
//
//                      -----------  Link -----------
//  DataRate (Mbps)     0   1   2   3   4   5   6   7
//        8             0   4   8  12  16  20  24  28   0 at 8 mBits/sec
//       16             0   8  16  24   x   x   x   x
//       32             0  16   x   x   x   x   x   x

//
// Thus for an 8 Mbps clocking rate: link 3's slot map starts at &slotMaskx[12]
// for a 32 Mbps clocking rate: link 1's slot map starts at &slotMaskx[16]
//
// The timeslot companding is selected in 8-slot chunks via the slotCompand[]
// array.
//      A 0-value specifies U-law companding, a 1-value specifies A-law
//
//  slotCompand[0].Bit[0] sets companding for slots 0..7
//  slotCompand[0].Bit[1] sets companding for slots 8..15    ...
//  slotCompand[1].Bit[0] sets companding for slots 256..263 ...
//  slotCompand[3].Bit[0] sets companding for slots 768..775 ...
//
typedef struct GpakPortConfig_t {

    // Tsip 1 configuration
    GpakActivation  TsipEnable1;     // Port Enable/Disable Flag
    ADT_UInt32      slotMask1[32]; // timeslot enable/disable bit masks...see above
    GpakActivation  singleClk1;      // when Enabled: frame sync and serial clock signals are shared by the transmit and receive interfaces.
                                     // when Disabled: one framesync and serial clock for transmit, and a second framesync and clock for receive
    fsPolarity      txFsPolarity1;  // tx framesync polarity
    clkEdge         txDataClkEdge1; // clock edge used to clock tx data
    clkEdge         txFsClkEdge1;   // clock edge used to sample tx framesync
    clkMode         txClkMode1;     // tx clocking mode               (specifies tx and rx when redundant mode is active)
    dataRate        txDataRate1;    // tx data rate                   (specifies tx and rx when redundant mode is active)
    clkFsSrc        txClkFsSrc1;    // tx clock and frame sync source (specifies tx and rx when redundant mode is active)
    ADT_Int16       txDatDly1;      // Transmit Data Delay:
                                    // TXDLY = txDatDly + 0.5 When clock polarity for data and frame sync differs
                                    // TXDLY = txDatDly + 1 When clock polarity for data and frame sync is identical
                                    // The transmit data delay (TXDLY) determines the delay,
                                    // in sample clock periods, from the clock edge sampling the frame
                                    // synchronization signal to the clock edge that starts driving the first data bit of the first timeslot. A
                                    // txDatDly value of 0 represents a delay of one clock period from the clock edge sampling the frame
                                    // synchronization signal to the clock edge that starts driving the first data bit of the first timeslot when the
                                    // same clock polarity is used for both events. If opposite clock edges are used for these two events, an
                                    // txDatDly value of 0 represents a delay of half a clock period from the clock edge sampling the frame
                                    // synchronization signal to the clock edge that starts driving the first data bit.
    txDisState      txDriveState1;  // tx data line output drive state for disabled timeslots
    GpakActivation  txOutputDly1;   // when Enabled: the tx output of an enabled timeslot (that follows a high-z timeslot) is delayed by 0.5 serial clock period
                                    // when Disabled: output delay is disabled
    fsPolarity      rxFsPolarity1;  // rx framesync polarity
    clkEdge         rxDataClkEdge1; // clock edge used to clock rx data
    clkEdge         rxFsClkEdge1;   // clock edge used to sample rx framesync
    clkMode         rxClkMode1;     // rx clocking mode (single or double) (only valid when redundant mode is inactive)
    dataRate        rxDataRate1;    // rx data rate                        (only valid when redundant mode is inactive)
    clkFsSrc        rxClkFsSrc1;    // tx clock and frame sync source      (only valid when redundant mode is inactive)
    ADT_Int16       rxDatDly1;      // Receive Data Delay:
                                    // RXDLY = rxDatDly + 1.5 When clock polarity for data and frame sync differs
                                    // RXDLY = rxDatDly + 2 When clock polarity for data and frame sync is identical
                                    // The receive data delay (RXDLY) determines the delay, in sample clock periods, from the
                                    // clock edge sampling the frame synchronization signal to the clock edge that samples the first data bit of
                                    // the first timeslot. A rxDatDly  value of 0 represents a delay of two clock periods from the clock edge
                                    // sampling the frame synchronization signal to the clock edge that samples the first data bit of the first
                                    // timeslot when the same clock polarity is used for both events. If opposite clock edges are used for these
                                    // two events, a rxDatDly value of 0 represents a delay of one and a half clock periods from the clock
                                    // edge sampling the frame synchronization signal to the clock edge that samples the first data bit.
    loopbackMode    loopBack1;      // Loopback  mode
    ADT_UInt32      slotCompand1[4]; // Timeslot Companding mask: 0==U-law, 1==A-law    See above.

    // Tsip 2 configuration
    GpakActivation  TsipEnable2;
    ADT_UInt32      slotMask2[32];
    GpakActivation  singleClk2;
    fsPolarity      txFsPolarity2;
    clkEdge         txDataClkEdge2;
    clkEdge         txFsClkEdge2;
    clkMode         txClkMode2;
    dataRate        txDataRate2;
    clkFsSrc        txClkFsSrc2;
    ADT_Int16       txDatDly2;
    txDisState      txDriveState2;
    GpakActivation  txOutputDly2;
    fsPolarity      rxFsPolarity2;
    clkEdge         rxDataClkEdge2;
    clkEdge         rxFsClkEdge2;
    clkMode         rxClkMode2;
    dataRate        rxDataRate2;
    clkFsSrc        rxClkFsSrc2;
    ADT_Int16       rxDatDly2;
    loopbackMode    loopBack2;
    ADT_UInt32      slotCompand2[4]; // Timeslot Companding mask: 0==U-law, 1==A-law    See above.

    // Tsip 3 configuration
    GpakActivation  TsipEnable3;
    ADT_UInt32      slotMask3[32];
    GpakActivation  singleClk3;
    fsPolarity      txFsPolarity3;
    clkEdge         txDataClkEdge3;
    clkEdge         txFsClkEdge3;
    clkMode         txClkMode3;
    dataRate        txDataRate3;
    clkFsSrc        txClkFsSrc3;
    ADT_Int16       txDatDly3;
    txDisState      txDriveState3;
    GpakActivation  txOutputDly3;
    fsPolarity      rxFsPolarity3;
    clkEdge         rxDataClkEdge3;
    clkEdge         rxFsClkEdge3;
    clkMode         rxClkMode3;
    dataRate        rxDataRate3;
    clkFsSrc        rxClkFsSrc3;
    ADT_Int16       rxDatDly3;
    loopbackMode    loopBack3;
    ADT_UInt32      slotCompand3[4]; // Timeslot Companding mask: 0==U-law, 1==A-law    See above.
} GpakPortConfig_t;

#define MAX_SLOTS 1024
void PopulateSlotMask (ADT_UInt32 *slotMask, int slotCnt, int slotsPerSample) {
   ADT_UInt32 mask;
   int  slotsPerMask, bitsPerSlot;

   // Space out slots across the TDM bus.
   if (slotsPerSample == 8) {
      slotsPerMask = 4;
      mask = 0x01010101;
   } else if (slotsPerSample == 4) {
      slotsPerMask = 8;
      mask = 0x11111111;
   } else if (slotsPerSample == 2) {
      slotsPerMask = 16;
      mask = 0x55555555;
   } else {
      slotsPerMask = 32;
      mask = 0xffffffff;
      slotsPerSample = 1;
   }
   bitsPerSlot = 32 / slotsPerMask;

   // Populate slot mask array
   while (slotsPerMask <= slotCnt) {
      *slotMask++ = mask;
      slotCnt -= slotsPerMask;
   }
   *slotMask = (1 << (slotCnt * bitsPerSlot)) - 1;
   *slotMask &= mask;
}
void PopulateCompandingMask (ADT_UInt32 *slotMask, int slotCnt, int slotsPerSample, int compand_a) {
   ADT_UInt32 mask;
   int slotsPerMask;

   if (compand_a) mask = (ADT_UInt32) -1;
   else           mask =  0;
   slotsPerMask = 32 * 8;

   slotCnt *= slotsPerSample;

   // Populate slot mask array
   while (slotsPerMask <= slotCnt) {
      *slotMask++ = mask;
      slotCnt -= slotsPerMask;
   }
   *slotMask = (1 << (slotCnt / 8)) - 1;
   *slotMask &= mask;
}
//}===============================================================
//  Common type definitions and structures
//{===============================================================
typedef enum GpakApiStatus_t {
   GpakApiSuccess = 0,   GpakApiParmError,          GpakApiInvalidChannel,
   GpakApiInvalidDsp,    GpakApiCommFailure,        GpakApiBufferTooSmall,
   GpakApiNoPayload,     GpakApiBufferFull,         GpakApiReadError,
   GpakApiInvalidFile,   GpakApiUnknownReturnData,  GpakApiNoQue,
   GpakApiInvalidPaylen, GpakApiNotConfigured
} GpakApiStatus_t;
typedef GpakApiStatus_t gpakConfigPortStatus_t;
#define MAX_DSP_CORES           1       /* maximum number of DSP cores */
#define MAX_EC_TAP_LEN 1024  /* max tap length in sample units */
#define MAX_EC_FIR_SEGS 3    /* max number of FIR segments */
#define MAX_EC_SEG_LEN 64    /* mac FIR segment length in sample units */

#define EC_STATE_DATA_LEN ((MAX_EC_FIR_SEGS * (2 + MAX_EC_SEG_LEN)) + (MAX_EC_TAP_LEN / 4))
#define ECAN_STATE_LEN_I16 (EC_STATE_DATA_LEN + 6)
/* Miscellaneous definitions. */
#define MAX_MSGLEN_I32       (5 + (ECAN_STATE_LEN_I16+1)/2)
#define MSG_BUFFER_SIZE      MAX_MSGLEN_I32         /* size of Host msg buffers (32-bit words) */
ADT_UInt32 gpakFormatPortConfig (ADT_UInt16 *Msg, GpakPortConfig_t *PortCfg) {
   int i, idx;

    /* Build the Configure Serial Ports message. */
    Msg[0] = MSG_CONFIGURE_PORTS << 8;

    // Enable bits
    Msg[1] = 0;
    if (PortCfg->TsipEnable1)        Msg[1]  = 0x0001;
    if (PortCfg->TsipEnable2)        Msg[1] |= 0x0002;
    if (PortCfg->TsipEnable3)        Msg[1] |= 0x0004;

    //--------------------------------------------------------------------
    // TSIP 0
    // Slot masks
    for (i=0, idx=2; i<32; i++, idx+=2) {
        Msg[idx]    = (ADT_UInt16)((PortCfg->slotMask1[i]) >> 16);
        Msg[idx+1]  = (ADT_UInt16)((PortCfg->slotMask1[i]) & 0xffff);
    }

    // Control parameters
    Msg[66] = (ADT_UInt16) ( (PortCfg->singleClk1             & 0x0001) |
                            ((PortCfg->txFsPolarity1   << 1)  & 0x0002) |
                            ((PortCfg->txDataClkEdge1  << 2)  & 0x0004) |
                            ((PortCfg->txFsClkEdge1    << 3)  & 0x0008) |
                            ((PortCfg->txClkMode1      << 4)  & 0x0010) |
                            ((PortCfg->txClkFsSrc1     << 5)  & 0x0020) |
                            ((PortCfg->txOutputDly1    << 6)  & 0x0040) |
                            ((PortCfg->rxFsPolarity1   << 7)  & 0x0080) |
                            ((PortCfg->rxDataClkEdge1  << 8)  & 0x0100) |
                            ((PortCfg->rxFsClkEdge1    << 9)  & 0x0200) |
                            ((PortCfg->rxClkMode1      << 10) & 0x0400) |
                            ((PortCfg->rxClkFsSrc1     << 11) & 0x0800));

    Msg[67] = (ADT_UInt16) ( (PortCfg->txDriveState1      & 0x0003) |
                            ((PortCfg->txDataRate1  << 2) & 0x000C) |
                            ((PortCfg->rxDataRate1  << 4) & 0x0030) |
                            ((PortCfg->loopBack1    << 6) & 0x00C0));

    Msg[68] = (ADT_UInt16) PortCfg->txDatDly1;
    Msg[69] = (ADT_UInt16) PortCfg->rxDatDly1;

    //--------------------------------------------------------------------
    // TSIP 1
    // Slot masks
    for (i=0, idx=70; i<32; i++, idx+=2) {
        Msg[idx]    = (ADT_UInt16)((PortCfg->slotMask2[i]) >> 16);
        Msg[idx+1]  = (ADT_UInt16)((PortCfg->slotMask2[i]) & 0xffff);
    }

    // Control parameters
    Msg[134] = (ADT_UInt16) ( (PortCfg->singleClk2            & 0x0001) |
                            ((PortCfg->txFsPolarity2   << 1)  & 0x0002) |
                            ((PortCfg->txDataClkEdge2  << 2)  & 0x0004) |
                            ((PortCfg->txFsClkEdge2    << 3)  & 0x0008) |
                            ((PortCfg->txClkMode2      << 4)  & 0x0010) |
                            ((PortCfg->txClkFsSrc2     << 5)  & 0x0020) |
                            ((PortCfg->txOutputDly2    << 6)  & 0x0040) |
                            ((PortCfg->rxFsPolarity2   << 7)  & 0x0080) |
                            ((PortCfg->rxDataClkEdge2  << 8)  & 0x0100) |
                            ((PortCfg->rxFsClkEdge2    << 9)  & 0x0200) |
                            ((PortCfg->rxClkMode2      << 10) & 0x0400) |
                            ((PortCfg->rxClkFsSrc2     << 11) & 0x0800));

    Msg[135] = (ADT_UInt16) ( (PortCfg->txDriveState2     & 0x0003) |
                            ((PortCfg->txDataRate2  << 2) & 0x000C) |
                            ((PortCfg->rxDataRate2  << 4) & 0x0030) |
                            ((PortCfg->loopBack2    << 6) & 0x00C0));

    Msg[136] = (ADT_UInt16) PortCfg->txDatDly2;
    Msg[137] = (ADT_UInt16) PortCfg->rxDatDly2;

    //--------------------------------------------------------------------
    // TSIP 2
    // Slot masks
    for (i=0, idx=138; i<32; i++, idx+=2) {
        Msg[idx]    = (ADT_UInt16)((PortCfg->slotMask3[i]) >> 16);
        Msg[idx+1]  = (ADT_UInt16)((PortCfg->slotMask3[i]) & 0xffff);
    }

    // Control parameters
    Msg[202] = (ADT_UInt16) ( (PortCfg->singleClk3            & 0x0001) |
                            ((PortCfg->txFsPolarity3   << 1)  & 0x0002) |
                            ((PortCfg->txDataClkEdge3  << 2)  & 0x0004) |
                            ((PortCfg->txFsClkEdge3    << 3)  & 0x0008) |
                            ((PortCfg->txClkMode3      << 4)  & 0x0010) |
                            ((PortCfg->txClkFsSrc3     << 5)  & 0x0020) |
                            ((PortCfg->txOutputDly3    << 6)  & 0x0040) |
                            ((PortCfg->rxFsPolarity3   << 7)  & 0x0080) |
                            ((PortCfg->rxDataClkEdge3  << 8)  & 0x0100) |
                            ((PortCfg->rxFsClkEdge3    << 9)  & 0x0200) |
                            ((PortCfg->rxClkMode3      << 10) & 0x0400) |
                            ((PortCfg->rxClkFsSrc3     << 11) & 0x0800));

    Msg[203] = (ADT_UInt16) ( (PortCfg->txDriveState3     & 0x0003) |
                            ((PortCfg->txDataRate3  << 2) & 0x000C) |
                            ((PortCfg->rxDataRate3  << 4) & 0x0030) |
                            ((PortCfg->loopBack3    << 6) & 0x00C0));

    Msg[204] = (ADT_UInt16) PortCfg->txDatDly3;
    Msg[205] = (ADT_UInt16) PortCfg->rxDatDly3;

    //--------------------------------------------------------------------
    // TSIP 0
    // Companding modes
    for (i=0, idx=206; i<4; i++, idx+=2) {
        Msg[idx]    = (ADT_UInt16)((PortCfg->slotCompand1[i]) >> 16);
        Msg[idx+1]  = (ADT_UInt16)((PortCfg->slotCompand1[i]) & 0xffff);
    }

    //--------------------------------------------------------------------
    // TSIP 1
    // Companding modes
    for (i=0, idx=214; i<4; i++, idx+=2) {
        Msg[idx]    = (ADT_UInt16)((PortCfg->slotCompand2[i]) >> 16);
        Msg[idx+1]  = (ADT_UInt16)((PortCfg->slotCompand2[i]) & 0xffff);
    }

    //--------------------------------------------------------------------
    // TSIP 2
    // Companding modes
    for (i=0, idx=222; i<4; i++, idx+=2) {
        Msg[idx]    = (ADT_UInt16)((PortCfg->slotCompand3[i]) >> 16);
        Msg[idx+1]  = (ADT_UInt16)((PortCfg->slotCompand3[i]) & 0xffff);
    }

    return 230;
}
GpakApiStatus_t gpakConfigurePorts (ADT_UInt32 DspId, GpakPortConfig_t *pPortConfig,
                                                     GPAK_PortConfigStat_t *pStatus) {

    ADT_UInt16 Msg[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 Rply[MSG_BUFFER_SIZE];    /* message buffer */

    gpakFormatPortConfig (Msg, pPortConfig);
    *pStatus = ProcConfigSerialPortsMsg (Msg, Rply);
    return GpakApiSuccess;
}
void dspTsipTDMInit (void) {

// TSIP configuration variables
   GpakPortConfig_t       PortConfig;
   gpakConfigPortStatus_t PrtCfgStat = (gpakConfigPortStatus_t) 0;
   GPAK_PortConfigStat_t  PrtDspStat = (GPAK_PortConfigStat_t) 0;
   int portBits = 1;
   int slotCnt = 16;//SSTSIP 1024 128 enabled     8 links 8 bit linear samples
   int slotsPerSample = 1;
   ADT_UInt32 DspId = 0;

   memset (&PortConfig, 0, sizeof (PortConfig));

   PortConfig.TsipEnable1 = Disabled;
   PortConfig.TsipEnable2 = Disabled;
   PortConfig.TsipEnable3 = Disabled;
   if (portBits & 1) PortConfig.TsipEnable1 = Enabled;
   if (portBits & 2) PortConfig.TsipEnable2 = Enabled;
   if (portBits & 4) PortConfig.TsipEnable3 = Enabled;

   PopulateSlotMask (PortConfig.slotMask1, slotCnt, slotsPerSample);
   PortConfig.singleClk1       = Enabled;
   PortConfig.txFsPolarity1    = fsActLow;
   PortConfig.txDataClkEdge1   = clkRising;
   PortConfig.txFsClkEdge1     = clkRising;
   PortConfig.txClkMode1       = clkSingleRate;
   PortConfig.txDataRate1      = rate_8Mbps;
   PortConfig.txClkFsSrc1      = clkFsA;
   PortConfig.txDatDly1        = 1023;
   PortConfig.txDriveState1    = highz;
   PortConfig.txOutputDly1     = Disabled;
   PortConfig.rxFsPolarity1    = fsActLow;
   PortConfig.rxDataClkEdge1   = clkFalling;
   PortConfig.rxFsClkEdge1     = clkRising;
   PortConfig.rxClkMode1       = clkSingleRate;
   PortConfig.rxDataRate1      = rate_8Mbps;
   PortConfig.rxClkFsSrc1      = clkFsA;
   PortConfig.rxDatDly1        = 1023;
   PortConfig.loopBack1        = internalLoopback;
   PopulateCompandingMask (PortConfig.slotCompand1, slotCnt, slotsPerSample, 0);

   PopulateSlotMask (PortConfig.slotMask2, slotCnt, slotsPerSample);
   PortConfig.singleClk2       = Enabled;
   PortConfig.txFsPolarity2    = fsActLow;
   PortConfig.txDataClkEdge2   = clkRising;
   PortConfig.txFsClkEdge2     = clkRising;
   PortConfig.txClkMode2       = clkSingleRate;
   PortConfig.txDataRate2      = rate_8Mbps;
   PortConfig.txClkFsSrc2      = clkFsA;
   PortConfig.txDatDly2        = 1023;
   PortConfig.txDriveState2    = highz;
   PortConfig.txOutputDly2     = Disabled;
   PortConfig.rxFsPolarity2    = fsActLow;
   PortConfig.rxDataClkEdge2   = clkFalling;
   PortConfig.rxFsClkEdge2     = clkRising;
   PortConfig.rxClkMode2       = clkSingleRate;
   PortConfig.rxDataRate2      = rate_8Mbps;
   PortConfig.rxClkFsSrc2      = clkFsA;
   PortConfig.rxDatDly2        = 1023;
   PortConfig.loopBack2        = internalLoopback;
   PopulateCompandingMask (PortConfig.slotCompand2, slotCnt, slotsPerSample, 0);

   PopulateSlotMask (PortConfig.slotMask3, slotCnt, slotsPerSample);
   PortConfig.singleClk3       = Enabled;
   PortConfig.txFsPolarity3    = fsActLow;
   PortConfig.txDataClkEdge3   = clkRising;
   PortConfig.txFsClkEdge3     = clkRising;
   PortConfig.txClkMode3       = clkSingleRate;
   PortConfig.txDataRate3      = rate_8Mbps;
   PortConfig.txClkFsSrc3      = clkFsA;
   PortConfig.txDatDly3        = 1023;
   PortConfig.txDriveState3    = highz;
   PortConfig.txOutputDly3     = Disabled;
   PortConfig.rxFsPolarity3    = fsActLow;
   PortConfig.rxDataClkEdge3   = clkFalling;
   PortConfig.rxFsClkEdge3     = clkRising;
   PortConfig.rxClkMode3       = clkSingleRate;
   PortConfig.rxDataRate3      = rate_8Mbps;
   PortConfig.rxClkFsSrc3      = clkFsA;
   PortConfig.rxDatDly3        = 1023;
   PortConfig.loopBack3        = internalLoopback;
   PopulateCompandingMask (PortConfig.slotCompand3, slotCnt, slotsPerSample, 0);

   PrtCfgStat = gpakConfigurePorts (DspId, &PortConfig, &PrtDspStat);
   if (PrtCfgStat != GpakApiSuccess || PrtDspStat != Pc_Success) {
      printf ("\n TSIP setup failure %d:%d\n", PrtCfgStat, PrtDspStat);
   } else {
      printf (" \n TSIP configuration success\n");
   }

   return;
}


// configures Mcbsp for 8148EVM
void custom_start_port_evm () { 

    dspTsipTDMInit();
}


