//*******************************************************************
//{
// Copyright(c) 2001-2007 Adaptive Digital Technologies Inc. 
//              All Rights Reserved
//
//
// File Name:   ChanCreate.c
// 
//
// Description: System and channel startup software to allocate and
//              initialize gpak instance structures.
//
//
// Initial Version: 11/15/2001
//
//
// Modified:
//  2/2007 - Combined C54 and C64 Version
//
//}*****************************************************************
#include "GpakDefs.h"
#include "GpakExts.h"
#include "GpakDma.h"
#include "sysconfig.h"
#include "sysmem.h"
#include "stdio.h"

#define MIN(a,b) (((a) < (b)) ? (a) : (b))

#define CLAMP_GAIN(a) if (40 < a) a = 40;\
                      else if (a < -40) a = -40;

#ifndef AEC_LIB_VERSION
   #define AEC_LIB_VERSION 0
#endif
                      
ADT_UInt16 TxDelayMs = 0;
ADT_UInt16 RxDelayMs = 0;

// Echo canceller instance data
extern ADT_UInt16 *pPcmEcDaState[],  *pPktEcDaState[];
extern ADT_UInt16 *pPcmEcSaState[],  *pPktEcSaState[];
extern ADT_UInt16 *pPcmEcEchoPath[], *pPktEcEchoPath[];
extern ADT_UInt16 *pPcmEcBackEp[],   *pPktEcBackEp[];
extern ADT_UInt16 *pPcmEcBackFar[],  *pPktEcBackFar[];
extern ADT_UInt16 *pPcmEcBackNear[], *pPktEcBackNear[];

extern ADT_Word encoderInstI16;
extern ADT_Word decoderInstI16;
extern ecInstanceInfo_t pcmEcInfo;
extern ecInstanceInfo_t pktEcInfo;

extern ADT_UInt16 pcmInAlignI16;
extern ADT_UInt16 pcmOutAlignI16;
extern ADT_UInt16 bulkDelayAlignI16;
extern ADT_UInt16 pktBuffAlignI16;

extern int AEC_Inst_I8;     // Configuration size of AEC instance structure
extern IAECG4_Params aecChanParms[];	// AEC parameters at channel create

ADT_Word NumPcmEcansUsed;   // number of PCM echo eancellers in use
ADT_Word NumPktEcansUsed;   // number of packet echo cancellers in use
ADT_Word NumAECEcansUsed;   // number of acoustic echo cancellers in use
#pragma DATA_SECTION (NumPcmEcansUsed,  "POOL_ALLOC")
#pragma DATA_SECTION (NumPktEcansUsed,  "POOL_ALLOC")
#pragma DATA_SECTION (NumAECEcansUsed,  "POOL_ALLOC")

ADT_UInt16 ToneDetectorsTotal = VOID_INDEX;
ADT_UInt16 ArbToneDetectorsTotal = VOID_INDEX;
ADT_UInt16 CedDetTotal = VOID_INDEX;
ADT_UInt16 CngDetTotal = VOID_INDEX;
#pragma DATA_SECTION (ToneDetectorsTotal,    "POOL_ALLOC")
#pragma DATA_SECTION (ArbToneDetectorsTotal, "POOL_ALLOC")
#pragma DATA_SECTION (CedDetTotal,  "POOL_ALLOC")
#pragma DATA_SECTION (CngDetTotal,  "POOL_ALLOC")

extern GPAK_ChannelConfigStat_t setChannelRates (chanInfo_t *chan);
extern void invalidateFaxRelay (chanInfo_t *pChan);

// Data structure sizes and logging buffers
#ifndef _DEBUG
   #define LogBuff(a,b)    // Disable logging on release builds
#else
const struct structSizes {
   int GpakChanInst;
   
   struct Confr {
      int Inst,         Member;
 
   } Confr;

   struct CodecSizes {
      int G723EncInst,    G723DecInst;
      int G726DecInst,    G726EncInst;
   
      int G728DecInst,    G728EncInst;
      int Adt4800EncInst, Adt4800DecInst;
      int melpEncInst,    melpDecInst;
      int FaxInst,        FaxBuff;

      int GpakInst;
   } Codecs;
   
   struct G168 {
      int Inst,     Saram,     Daram;
      int Scratch,  DAScratch, BgState;
   } G168;
    
   struct Tones {
      int FaxCEDInst,      FaxCNGInst;
   
      int DTMFCfg,         ToneDetInst;
      int ToneDetScrt,     ToneGenInst;

      int TrDetInst,       TrGenInst;
   } Tones;
   
   struct Enhance {
      int AgcInst,         VadInst;
      int NCANInst,        CIDRxInst;
      int CIDTxInst,       RTPInst;
   } Enhance;
   
} InstanceSize = {
   sizeof (chanInfo_t),
   {  // Conferences
      sizeof (ConfInstance_t),                sizeof (conf_Member_t)
   },

   {  // Codecs
      sizeof (G723ENC_ChannelInst_t),         sizeof (G723DEC_ChannelInst_t),
      sizeof (G726DecInstance_t),             sizeof (G726EncInstance_t),
   
      sizeof (G728EncodeChannelInstance_t),   sizeof (G728DecodeChannelInstance_t),
      sizeof (MELP_AnaChannel_t),             sizeof (MELP_SynChannel_t),
      sizeof (Enc48Chan_t),                   sizeof (Dec48Chan_t),

      (RLY_CHANNEL_SIZE * 2),                 sizeof (t38Packet_t),
      sizeof (struct chanInfo)
   },

   {  // G168 echo canceller
      sizeof (G168ChannelInstance_t),   sizeof (G168_SA_State_t),   sizeof (G168_DA_State_t),
      sizeof (G168_Scratch_t),          sizeof (G168_DAScratch_t),  sizeof (G168_BG_SA_State_t)
   },
   
   {  // Tones
      sizeof (FaxCEDChannel_t),              sizeof (FAXCNGInstance_t),

      sizeof (DTMF_Config_Param_t),           sizeof (TDInstance_t),
      sizeof (TDScratch_t),                   sizeof (TGInstance_t),
   
      sizeof (TRDetectInstance_t),            sizeof (TRGenerateInstance_t)
   },
   
   {  // Enhancement
      sizeof (AGCInstance_t),                sizeof (VADCNG_Instance_t),
      sizeof (NCAN_Channel_t),               sizeof (CIDRX_Inst_t),
      sizeof (CIDTX_Inst_t),                 sizeof (RtpChanData_t)
   }
};


#pragma DATA_SECTION (phaseInfo, "PER_CORE_DATA:logging")
#pragma DATA_ALIGN   (phaseInfo, CACHE_L2_LINE_SIZE)

typedef struct {
  int CnfrSize, CnfrPhase, CnfrPhaseB, CnfrPut;
  int PktSize,  PktPhase,  PktPhaseB,  PktPut;
} phase;
phase phaseInfo[0x40];
int phaseIdx = 0;
#endif

static const TGInfo_t TG_Null = TGNULL;

//===================================================================================
//
//{ Channel setup parameter validation
//
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ValidCoding - Determine if the Coding is valid.
//
#pragma CODE_SECTION (getPacketSize, "SLOW_PROG_SECT")
int getPacketSize (GpakCodecs Coding, int FrameHalfMS) {

   switch (Coding) {
   case L8:
   case PCMU_64:
   case PCMA_64:  return FrameHalfMS * 4;

   case L8_WB:
   case PCMU_WB:
   case PCMA_WB: 
   case L16:      return FrameHalfMS * 8;

   case L16_WB:   return FrameHalfMS * 16;

   case PCMU_56:
   case PCMA_56:  return 35;

   case PCMU_48:
   case PCMA_48:  return 30;

   case G722_64:  return FrameHalfMS * 4;


   case G726_40:  return  ((20*FrameHalfMS) + 7) >>3; // 5 bits/samp
   case G726_32:  return  ((16*FrameHalfMS) + 7) >>3; // 4 bits/samp
   case G726_24:  return  ((12*FrameHalfMS) + 7) >>3; // 3 bits/samp
   case G726_16:  return  ( (8*FrameHalfMS) + 7) >>3; // 2 bits/samp

   case G728_16:  return  ( (8*FrameHalfMS) + 7) >>3; // 2 bits/samp

   case MELP_E:   return  7;
   case MELP:     return  9;

   case ADT_4800: return 18;

   default: 
   case Speex:      case SpeexWB:
   case G729:       case G729AB: 
   case G723_63:    case G723_63A: 
   case G723_53:    case G723_53A:
   case AMR_475:    case AMR_515:    case AMR_590:
   case AMR_670:    case AMR_740:    case AMR_795:
   case AMR_1020:   case AMR_1220:
      return -1;   // Packet size is variable

   }
}
#pragma CODE_SECTION (ValidCoding, "SLOW_PROG_SECT")
int ValidCoding (GpakCodecs Coding, int FrameHalfMS) {

#define Vocoders sysConfig.Codecs.Bits
#define relay    sysConfig.packetProfile

   // Verify the specified Coding is valid based on the configured build.
   switch (Coding) {
   case NullCodec: return TRUE;

   case PCMU_WB:
   case PCMA_WB:
   case PCMU_64:
   case PCMA_64: return (relay != AAL2Trunking) || (FrameHalfMS == Frame_5ms);

   case PCMU_48:
   case PCMA_48:
   case PCMU_56:
   case PCMA_56: return (relay == AAL2Trunking) && (FrameHalfMS == Frame_5ms);

   case CircuitPkt:  return FALSE;  // Deprecated

   case L8:  case L8_WB:
   case L16:      return relay == RTPAVP;

   case L16_WB:   return SysAlgs.wbEnable;

   case G726_16:   case G726_24:   case G726_32:   case G726_40:
      if ((relay == AAL2Trunking) && (FrameHalfMS != Frame_5ms)) return FALSE;

      if (Vocoders.g726LowMemEnable)   return TRUE;
      if (!Vocoders.g726LowMipsEnable) return FALSE;

      if ((Coding == G726_16) && (Vocoders.g726Rate16Enable))  return TRUE;
      if ((Coding == G726_24) && (Vocoders.g726Rate24Enable))  return TRUE;
      if ((Coding == G726_32) && (Vocoders.g726Rate32Enable))  return TRUE;
      if ((Coding == G726_40) && (Vocoders.g726Rate40Enable))  return TRUE;
      return FALSE;

   case G728_16:  return ((Vocoders.g728Enable) && ((FrameHalfMS % Frame_2_5ms) == 0));
   
   case G729:
   case G729AB:   return (Vocoders.g729ABEnable || Vocoders.TI_g729ABEnable) &&  ((FrameHalfMS % Frame_10ms) == 0);

   case AMR_475:    case AMR_515:    case AMR_590:
   case AMR_670:    case AMR_740:    case AMR_795:
   case AMR_1020:   case AMR_1220:
      return Vocoders.AMREnable &&  (FrameHalfMS == Frame_20ms);

   case Speex:    return Vocoders.speexEnable   && (FrameHalfMS == Frame_20ms);
   case SpeexWB:  return Vocoders.speexWBEnable && (FrameHalfMS == Frame_20ms);

   case MELP:     return Vocoders.melpEnable  && (FrameHalfMS == Frame_22_5ms);
   case MELP_E:   return Vocoders.melpEEnable && (FrameHalfMS == Frame_22_5ms);
   
   case G723_63: case G723_63A:
   case G723_53: case G723_53A:  return (Vocoders.g723Enable || Vocoders.TI_g7231AEnable) && (FrameHalfMS == Frame_30ms);

   case ADT_4800: return Vocoders.adt4800Enable && (FrameHalfMS == Frame_30ms);
   case G722_64:  return Vocoders.g722Enable;

   case CustomCodec:  return TRUE;
   }

   // Return with an indication the Coding is invalid.
   return FALSE;

#undef Vocoders
}
//----------------------------------------------------------
// Verify specified tone types are valid for build.

#pragma CODE_SECTION (ValidToneTypes, "SLOW_PROG_SECT")
GPAK_ChannelConfigStat_t ValidToneTypes (GpakToneTypes ToneTypes, int *mfTones,
                    int *cedTones, int *cngTones, int *arbTones) {

   int NumTones;
   NumTones = 0;

   ToneTypes &=  ~(Tone_Generate);
   if (ToneTypes == Null_tone)
      return Cc_Success;

   // Validate FAX tone detection request
   if ((ToneTypes & CED_tone) != 0) {
      *cedTones += 1;
      if (CedDetAvail < *cedTones) return Cc_InsuffTDResourcesAvail;
   }
   if ((ToneTypes & CNG_tone) != 0) {
      *cngTones += 1;
      if (CngDetAvail < *cngTones) return Cc_InsuffTDResourcesAvail;
   }

   if ((ToneTypes & Arbit_tone) != 0) {
        if (activeArbToneCfg == VOID_INDEX) return Cc_InvalidToneTypesA;

        *arbTones += 1;
        if (ArbToneDetectorsAvail < *arbTones)  return Cc_InsuffTDResourcesAvail;
   }

   // Validate MFR tone detection request
   if ((ToneTypes & MFR1_tone) != 0) {
      if (!SysAlgs.mfr1Enable) return Cc_InvalidToneTypesA;
      NumTones += 1;
   }
   
   if ((ToneTypes & MFR2Fwd_tone) != 0) {
      if (!SysAlgs.mfr2FwdEnable) return Cc_InvalidToneTypesA;
      NumTones += 1;
   }
   
   if ((ToneTypes & MFR2Back_tone) != 0) {
      if (!SysAlgs.mfr2BackEnable) return Cc_InvalidToneTypesA;
      NumTones += 1;
   }
   
   if ((ToneTypes & CallProg_tone) != 0) {
      if (!SysAlgs.cprgEnable) return Cc_InvalidToneTypesA;
      NumTones += 1;
   }

   if ((ToneTypes & DTMF_tone) != 0) {
      if (!SysAlgs.dtmfEnable) return Cc_InvalidToneTypesA;
      NumTones += 1;
   } 
   //else {
      // Squelch must be accompanied by DTMF detect
   //   if ((ToneTypes & Tone_Squelch) != 0) return Cc_InvalidToneTypesA;
   //}

   if (sysConfig.maxToneDetTypes < NumTones)
      return Cc_InsuffTDResourcesAvail;

   // Validate tone relay request
   if (((ToneTypes & Tone_Relay) != 0) && !SysAlgs.toneRlyDetEnable)
      return Cc_InvalidToneTypesA;

   // Validate tone regeneration request
   if ((ToneTypes & Tone_Regen) != 0 && !SysAlgs.toneRlyGenEnable)
      return Cc_InvalidToneTypesA;
 
   *mfTones += NumTones;

   // Return with an indication the request is valid.
   return Cc_Success;
#undef Components
}


#pragma CODE_SECTION (SetVADMode, "SLOW_PROG_SECT")
GpakVADMode SetVADMode (GpakCodecs Codec) {

   switch (Codec) {
   case AMR_475:   case AMR_515:   case AMR_590:
   case AMR_670:   case AMR_740:   case AMR_795:
   case AMR_1020:  case AMR_1220:  
   case G723_53A:  case G723_63A:
   case G729AB:
   case Speex:     case SpeexWB:
       return (GpakVADMode) VadCodecEnabled;
   }
   
   if (SysAlgs.vadCngEnable)
      return (GpakVADMode) VadCngEnabled;
   return VadDisabled;

}

/* returns 1 if NLP is valid, 0 if invalid */
#pragma CODE_SECTION (ValidNLP, "SLOW_PROG_SECT")
int ValidNLP (int NLP) {

   switch (NLP) {
   case NLP_OFF:       case NLP_MUTE: case NLP_RAND_CNG:
   case NLP_HOTH_CNG:  case NLP_SUPP: case NLP_CNG1:
   case NLP_SUPP_AUTO:
      return 1;    // valid

   default:
      return 0;    // invalid
   }
}

#pragma CODE_SECTION (ClampScaleGain, "SLOW_PROG_SECT")                 
void ClampScaleGain (gains_t *g) {
    if (g->ToneGenGainG1 != (ADT_Int16)ADT_GAIN_MUTE) {
        CLAMP_GAIN(g->ToneGenGainG1);
        g->ToneGenGainG1 *= 10;
    }

    if (g->OutputGainG2 != (ADT_Int16)ADT_GAIN_MUTE) {
        CLAMP_GAIN(g->OutputGainG2);
        g->OutputGainG2  *= 10;
    }

    if (g->InputGainG3 != (ADT_Int16)ADT_GAIN_MUTE) {
        CLAMP_GAIN(g->InputGainG3);
        g->InputGainG3   *= 10;
    }
}
//}===================================================================================


// One-time allocation of status instance structures for channels
#pragma CODE_SECTION (AllocateInstanceStructures, "SLOW_PROG_SECT")
struct chanInfo *AllocateInstanceStructures (ADT_UInt16  chId) {

   chanInfo_t *chan;
   pcm2pkt_t  *PcmPkt;   
   pkt2pcm_t  *PktPcm;   

   int i;               

   // Get a pointer to the channel's G.PAK information structure.
   chan = &(GpakChanInstance[chId]);
   memset (chan, 0, sizeof (chanInfo_t));

   chan->ChannelId = chId;
   
   PcmPkt = &chan->pcmToPkt;
   PktPcm = &chan->pktToPcm;

   // Allocate static instance structures
   if (SysAlgs.vadCngEnable) {
      PcmPkt->vadPtr = VadCngInstance[2*chId];
      PktPcm->vadPtr = VadCngInstance[2*chId+1];
      PktPcm->cngPtr = VadCngInstance[2*chId];
   }

   if (SysAlgs.toneRlyDetEnable)
      PcmPkt->toneRelayPtr = ToneRlyDetInstance[chId];

   if (SysAlgs.toneRlyGenEnable) {
      PktPcm->TGInfo.toneRlyGenPtr = ToneRlyGenInstance[chId];
      PktPcm->TGInfo.toneRlyEvt    = ToneRlyGenEvent[chId];
   }

   //  Initialize Noise suppression instance pointers
   if (SysAlgs.noiseSuppressEnable != 0) {
      PktPcm->Noise.Remnant = &(NoiseRemnantPool [chId * NCAN_FRAME_SIZE]);
      PcmPkt->Noise.Remnant = &(NoiseRemnantPool [(chId + sysConfig.maxNumChannels)* NCAN_FRAME_SIZE]);
   }

   // Point channel to circular buffer structures for host interface
   i = chId * 2;
   PktPcm->inbuffer  = (CircBufInfo_t *) &(pktBuffStructTable[i]);
   i++;
   PcmPkt->outbuffer = (CircBufInfo_t *) &(pktBuffStructTable[i]);

   // Null out channel's circular buffer structures
   memset (PktPcm->inbuffer,     0, sizeof(CircBufInfo_t));
   memset (PcmPkt->outbuffer,    0, sizeof(CircBufInfo_t));
   memset (&PcmPkt->inbuffer,    0, sizeof(CircBufInfo_t));
   memset (&PktPcm->outbuffer,   0, sizeof(CircBufInfo_t));  
   memset (&PcmPkt->ecBulkDelay, 0, sizeof(CircBufInfo_t));

   // Return a pointer to the Channel structure.
   return chan;
#undef Components
}


//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// channelAllocsNull
//
// FUNCTION
//     Initializes channel structures to null resource allocations.
//
// RETURNS
//   None
//}
#pragma CODE_SECTION (channelAllocsNull, "SLOW_PROG_SECT")
void channelAllocsNull (chanInfo_t *chan, pcm2pkt_t *PcmPkt, pkt2pcm_t *PktPcm) {

   // This routine is called by channel configuration to re-initializes all of the channel's non-static variables to
   // the null state.
   //
   // Static variables are assigned once at DSP startup by the AllocateInstanceStructures routine and are mainly
   // pointers to non-pooled instance structures and data buffers.

   chan->status.Word = 0;
   chan->invalidHdrCount = 0;
   chan->overflowCount = 0;
   chan->underflowCount = 0;
   chan->fromHostPktCount = 0;
   chan->toHostPktCount = 0;
   chan->Flags = 0;

   chan->fax.faxMode = disabled;
   chan->fax.t38InProgress = FALSE;

   memset (&chan->algCtrl, 0, sizeof(algCtrl_t));
   chan->algCtrl.ControlCode = (GpakAlgCtrl_t) ALG_CTRL_VOID_CMD;
   chan->tdmPattern.deviceSide = NullDevice;
   chan->LastRTPToneStamp = 0;

   chan->FrameCount = 0;
   chan->VoiceChannel = Enabled;

   chan->pcmPoolIndex  = VOID_INDEX;
   chan->pktPoolIndex  = VOID_INDEX;

   chan->ED137RxState.RxState = RX_IDLE;
   chan->ED137RxState.LAST_PTT_VALUE = 0;
   chan->ED137RxState.LAST_PF_VALUE = 0;
   chan->ED137RxState.KeepAliveCount = 0;
   chan->ED137RxState.KeepAliveErrorCount = 0;

   chan->ED137TxState.TxState &= (TX_UPDATE_PTT | TX_UPDATE_PF_MSG | TX_NON_PF_MSG);
   chan->ED137TxState.PTT_OffCount = 0;
   chan->ED137TxState.KeepAliveCount = 0;
   chan->ED137TxState.KeepAliveErrorCount = 0;
   chan->ED137TxState.TxVAD_DelayCount = 0;

   chan->RtpRxTimeoutValue = 0;
   chan->RtpRxTimeoutCount = 0;

   chan->customData = NULL;

   // Pcm to Pkt
   PcmPkt->EchoCancel    = Disabled;
   PcmPkt->AECEchoCancel = Disabled;
   PcmPkt->pG168Chan     = NULL;
   PcmPkt->EcIndex       = VOID_INDEX;
   PcmPkt->bulkDelayIndex= VOID_INDEX;
   PcmPkt->slipReset     = 0;
   PcmPkt->SIDPower      = 0xffff;
   PcmPkt->PrevVadState  = SILENCE;


   PcmPkt->AGC           = Disabled;
   PcmPkt->agcPtr        = NULL;
   PcmPkt->AGCIndex      = VOID_INDEX;
   PcmPkt->AGCPower      = 0;

   PcmPkt->toneTypes     = Null_tone;
   PcmPkt->TGInfo        = TG_Null;
   PcmPkt->TDInstances.ToneDetInst   = NULL;
   PcmPkt->TDInstances.CedDetInst    = NULL;
   PcmPkt->TDInstances.CngDetInst    = NULL;
   PcmPkt->TDInstances.ArbToneInst   = NULL;
   PcmPkt->TDInstances.ArbToneCfgId  = VOID_INDEX;
   PcmPkt->tonePktGet    = NULL;

   PcmPkt->NoiseSuppress = Disabled;

   PcmPkt->recA.CvtIndex  = VOID_INDEX;
   PcmPkt->recA.Circ.SlipSamps = 0;
   PcmPkt->recA.state  = Inactive;

   PcmPkt->playB.CvtIndex = VOID_INDEX;
   PcmPkt->playB.Circ.SlipSamps = 0;
   PcmPkt->playB.state = Inactive;

   PcmPkt->Gain.ToneGenGainG1 = 0;
   PcmPkt->Gain.OutputGainG2  = 0;
   PcmPkt->Gain.InputGainG3   = 0;

   PcmPkt->CIDMode  = CidDisabled;
   NullCID (&PcmPkt->cidInfo);
   PcmPkt->rateCvt.UpInst   = NULL;
   PcmPkt->rateCvt.UpIndex  = VOID_INDEX;
   PcmPkt->rateCvt.DwnInst  = NULL;
   PcmPkt->rateCvt.DwnIndex = VOID_INDEX;


   //  Pkt to Pcm
   PktPcm->EchoCancel    = Disabled;
   PktPcm->AECEchoCancel = Disabled;
   PktPcm->pG168Chan     = NULL;
   PktPcm->EcIndex       = VOID_INDEX;
   PktPcm->slipReset     = 0;

   PktPcm->CNG           = VadDisabled;
   PktPcm->ForwardCNGPkts  = Disabled;
   PktPcm->SIDPower      = 0xffff;

   PktPcm->AGC           = Disabled;
   PktPcm->agcPtr        = NULL;
   PktPcm->AGCIndex      = VOID_INDEX;
   PktPcm->AGCPower      = 0;

   PktPcm->toneTypes     = Null_tone;
   PktPcm->TGInfo        = TG_Null;
   if (SysAlgs.toneRlyGenEnable) {
      PktPcm->TGInfo.toneRlyGenPtr = ToneRlyGenInstance[chan->ChannelId];
      PktPcm->TGInfo.toneRlyEvt    = ToneRlyGenEvent[chan->ChannelId];
   }

   PktPcm->TDInstances.ToneDetInst   = NULL;
   PktPcm->TDInstances.CedDetInst    = NULL;
   PktPcm->TDInstances.CngDetInst    = NULL;
   PktPcm->TDInstances.ArbToneInst   = NULL;
   PktPcm->TDInstances.ArbToneCfgId  = VOID_INDEX;

   PktPcm->ForwardTonePkts = Disabled;
   PktPcm->tonePktPut    = NULL;

   PktPcm->NoiseSuppress = Disabled;

   PktPcm->playA.state = Inactive;
   PktPcm->playA.CvtIndex = VOID_INDEX;
   PktPcm->playA.Circ.SlipSamps = 0;

   PktPcm->recB.state  = Inactive;
   PktPcm->recB.CvtIndex  = VOID_INDEX;
   PktPcm->recB.Circ.SlipSamps = 0;

   PktPcm->Gain.ToneGenGainG1 = 0;
   PktPcm->Gain.OutputGainG2  = 0;
   PktPcm->Gain.InputGainG3   = 0;

   PktPcm->CIDMode       = CidDisabled;
   NullCID (&PktPcm->cidInfo);

   PktPcm->rateCvt.UpInst   = NULL;
   PktPcm->rateCvt.UpIndex  = VOID_INDEX;
   PktPcm->rateCvt.DwnInst  = NULL;
   PktPcm->rateCvt.DwnIndex = VOID_INDEX;

}

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Setup channel instances and add channel to TDM and frame processing tasks
//
//}
#define RTP_ADT_ADJUST_16K_SAMPLING
#pragma CODE_SECTION (channelInstanceInit, "SLOW_PROG_SECT")
void channelInstanceInit (chanInfo_t *chan) {

   pcm2pkt_t *PcmPkt;
   pkt2pcm_t *PktPcm;
   int chanId;

   chanId = chan->ChannelId;

   // Perform common channel type initialization.
   PcmPkt = &chan->pcmToPkt;
   PktPcm = &chan->pktToPcm;

   // Assign the encode/decode instances
   PcmPkt->EncodePtr = (GpakEncodeInst *) encoderPool[chanId];
   PktPcm->DecodePtr = (GpakDecodeInst *) decoderPool[chanId];

#ifdef _DEBUG
   memset (PcmPkt->EncodePtr, 0xFF, encoderInstI16 * sizeof(short int));
   memset (PktPcm->DecodePtr, 0xFF, decoderInstI16 * sizeof(short int));
#endif

   PcmPkt->PreferredCoding = PcmPkt->Coding;   // Store preferred coding for modem processing
   PktPcm->PreferredCoding = PktPcm->Coding;   // Store preferred coding for modem processing

   // Undo channel loop back
   PktPcm->inbuffer = &(pktBuffStructTable[chanId * 2]);
#ifdef RTP_ADT_ADJUST_16K_SAMPLING
    chan->G722RtpSamplingRate = 0; // checking incoming RTP rate every time setup channel
#endif
}

#pragma CODE_SECTION (parseChannelMsg,             "SLOW_PROG_SECT")
static GPAK_ChannelConfigStat_t parseChannelMsg (chanInfo_t *chan, ADT_UInt16 *pCmd) {
   GPAK_ChannelConfigStat_t status;


   // Extract and validate message parameters based on channel type.
   switch (chan->channelType) {
   case pcmToPacket:    status = ProcCfgPcm2PktMsg  (pCmd, chan); break;
   case pcmToPcm:       status = ProcCfgPcm2PcmMsg  (pCmd, chan); break;
   case packetToPacket: status = ProcCfgPkt2PktMsg  (pCmd, chan); break;
   case circuitData:    status = ProcCfgCircDataMsg (pCmd, chan); break;
 
   case conferencePcm:       status = ProcCfgConfPcmMsg  (pCmd, chan); break;
   case conferencePacket:    status = ProcCfgConfPktMsg  (pCmd, chan);  break;
   case conferenceComposite: status = ProcCfgConfCompMsg (pCmd, chan);  break;
   case loopbackCoder:       status = ProcCfgLpbkMsg     (pCmd, chan);  break;

   case customChannel:       status = customProcChanCfgMsg (pCmd, chan);  break;
   default:      return Cc_InvalidChannelType;
   }
   // Return failure code if parameters are invalid
   return status;

}

#pragma CODE_SECTION (codecSamplingRate, "SLOW_PROG_SECT")
ADT_UInt32 codecSamplingRate (GpakCodecs codec) {
   if (WB_START <= codec && codec <= WB_END)  return 16000;
   if (G722_64  <= codec && codec <= G722_48) return 16000;
   if (codec == NullCodec) return (TDMRate);
   return 8000;
}

#pragma CODE_SECTION (setupChannel, "SLOW_PROG_SECT")
GPAK_ChannelConfigStat_t setupChannel (chanInfo_t *chan) {
   GPAK_ChannelConfigStat_t status;

   chanInfo_t *chanB = NULL;

   status = setChannelRates (chan);
   if (status != Cc_Success) return status;

   //--------------------------------------
   // Perform channel type dependent initialization.
   //   Channel instance structure initialization
   channelInstanceInit (chan);
   if (chan->channelType == packetToPacket) {
      chanB = chanTable[chan->PairedChannelId];
      chanB->CoreID = chan->CoreID;
      channelInstanceInit (chanB);
   }

   // Allocate and initialize component instance structures
   // Process rate may be set to 8 kHz for specific algorithms (CID, FAX, EC, and NoiseReduction)
   switch (chan->channelType) {
   case pcmToPacket:         SetupPcm2PktChan  (chan); break;
   case pcmToPcm:            SetupPcm2PcmChan  (chan); break;
   case packetToPacket:      SetupPkt2PktChan  (chan); break;
   case circuitData:         SetupCircDataChan (chan); break;
   case conferencePcm:       SetupConfPcmChan  (chan); break;
   case conferenceComposite: SetupConfCompChan (chan); break;

   case conferenceMultiRate: // Intentional pass through
   case conferencePacket:    SetupConfPktChan  (chan); break;

   case loopbackCoder:       SetupLoopbackCoder (chan);  return Cc_Success;
   case customChannel:       customSetupChan (chan);  break;
   }
   FLUSH_wait ();    // Wait for all cached instance data to be written back before enqueueing channel

   //  Activate channel by adding to TDM processing and framing tasks
   if (chan->channelType == packetToPacket) {
        // NOTE: ActivateChannel always success on pkt-pkt channels since
        //       there is no TDM parameter validation and therefore
        //       there is no need to teardown chanB
        status = ActivateChannelPktPkt (chanB);
        return status;
   }

   // NOTE: ActivateChannel always success on pkt-pkt channels since
   //       there is no TDM parameter validation and therefore
   //       there is no need to teardown chanB
   //
   status = ActivateChannel (chan);
   if (status != Cc_Success) {
      logTime (0x400E0000 | (status << 4) | chan->ChannelId);
      tearDownChannel (chan);
   }
   return status;
}

#pragma CODE_SECTION (processChannelMsg,            "SLOW_PROG_SECT")
GPAK_ChannelConfigStat_t processChannelMsg (chanInfo_t *chan, ADT_UInt16 *pCmd) {

   GPAK_ChannelConfigStat_t status;
   ConferenceInfo_t *gpakCnfr;

   status = parseChannelMsg (chan, pCmd);
   if (status != Cc_Success) 
      return status;

   // Change request for conferencePacket to conferenceMultiRate when frame size mismatched      
   if (chan->channelType == conferencePacket) {
      gpakCnfr = GetGpakCnfr (chan->CoreID, chan->PairedChannelId);
      if (chan->pcmToPkt.SamplesPerFrame != gpakCnfr->FrameSize)
         chan->channelType = conferenceMultiRate;
   }
   return setupChannel (chan);

}

//===================================================================================
//
//{ Non-pooled resource initialization
//===================================================================================

//=============================================================================

//  Tone Relay

//==============================================================================
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// InitToneRelayGen - Initialize Tone Relay Generation.
//
#pragma CODE_SECTION (InitToneRelayGen, "SLOW_PROG_SECT")
void InitToneRelayGen (TGInfo_t* TGInfo) {
   TRGenerateInstance_t* pToneRlyGen;
   TRGenerateEvent_t*    EvtBuff;

   TRGenerateParamsV701_t relayGenParams;
   
   // Ignore if Tone Relay Generation isn't configured in the build.
   if (!SysAlgs.toneRlyGenEnable)  return;

   pToneRlyGen = TGInfo->toneRlyGenPtr;
   EvtBuff     = TGInfo->toneRlyEvt;

   CLEAR_INST_CACHE (pToneRlyGen, sizeof (TRGenerateInstance_t));
   FLUSH_wait ();

   relayGenParams.APIVersion = TR_API_VERSION;
   relayGenParams.TRGenInstanceSizeInBytes = sizeof (TRGenerateInstance_t);
   relayGenParams.pPastEventStorage = EvtBuff;
// jdc   relayGenParams.NPastGenEventsToStore = 0;
   relayGenParams.NPastGenEventsToStore = numPastToneRlyGenEvents; // jdc
   relayGenParams.UseEnforceDefaults = 1;

   TR_ADT_generateInit (pToneRlyGen, &relayGenParams);

   FLUSH_INST_CACHE (pToneRlyGen, sizeof (TRGenerateInstance_t));
   return;
}



#pragma CODE_SECTION (InitVadCng, "SLOW_PROG_SECT")
void InitVadCng (VADCNG_Instance_t *vadInst, VADCNG_ADT_Param_t *vadParams, int vadSamplesPerFrame, int cngSamplesPerFrame, int ProcessRate) {

   CLEAR_INST_CACHE (vadInst, sizeof (VADCNG_Instance_t));

   // A VAD and B sides CNG point to the same vad channel
   vadParams->Thres           = sysConfig.vadNoiseFloorZThresh;
   vadParams->HangMSec        = sysConfig.vadHangOverTime;
   vadParams->WindowSizeMSec  = sysConfig.vadWindowMs;
   if (ProcessRate < TDMRate) {
      vadParams->VadFrameSize = vadSamplesPerFrame / (TDMRate / ProcessRate);
      vadParams->CngFrameSize = cngSamplesPerFrame / (TDMRate / ProcessRate);
   } else {
      vadParams->VadFrameSize = vadSamplesPerFrame * (ProcessRate / TDMRate);
      vadParams->CngFrameSize = cngSamplesPerFrame * (ProcessRate / TDMRate);
   }

   vadParams->SamplingRate    = ProcessRate; //TDMRate; 

   FLUSH_wait ();
   VADCNG_ADT_init (vadInst, vadParams);
   FLUSH_INST_CACHE (vadInst, sizeof (VADCNG_Instance_t));
}


#pragma CODE_SECTION (InitNoiseSuppression, "SLOW_PROG_SECT")
void InitNoiseSuppression (NCAN_Channel_t *NoiseInst, NCAN_Params_t *NCAN_Params) {

   CLEAR_INST_CACHE (NoiseInst, sizeof (NCAN_Channel_t));
   FLUSH_wait ();

   NCAN_ADT_init    (NoiseInst, NCAN_Params);
   FLUSH_INST_CACHE (NoiseInst, sizeof (NCAN_Channel_t));
}

//}===================================================================================

//===================================================================================
//
//{ Pool management allocation and initialization
//
//===================================================================================

//{ Channel buffers
//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// AllocateInstanceStructures
//
// FUNCTION
//     One-time initialization of channel structure.
//
// RETURNS
//   A pointer to the channel's Channel structure.
//}
#pragma CODE_SECTION (CircBuffAllocate, "SLOW_PROG_SECT")
static void CircBuffAllocate (CircBufInfo_t *buff, ADT_UInt16 *pool, 
                                   ADT_UInt16 idx, ADT_UInt16 buffI16,
                                   ADT_UInt16 cacheI16) {
 
   buff->pBufrBase = &(pool[idx * cacheI16]);
   buff->BufrSize  = buffI16;
   buff->PutIndex  = 0;
   buff->TakeIndex = 0;
   buff->SlipSamps = 0;
}


#pragma CODE_SECTION (AllocPcmBuffs, "SLOW_PROG_SECT")
static ADT_UInt16 AllocPcmBuffs (CircBufInfo_t *in, CircBufInfo_t *out) {
   int i;

   if (PcmBuffAvail == 0) return VOID_INDEX;

   for (i=0; i<sysConfig.maxNumPcmBuffs; i++) {
      if (PcmBuffInUse[i] != 0) continue;
      CircBuffAllocate (in,  PcmInBufferPool,  i, 
                       sysConfig.pcmInBufferSize,  pcmInAlignI16);
      CircBuffAllocate (out, PcmOutBufferPool, i, 
                       sysConfig.pcmOutBufferSize, pcmOutAlignI16);
      PcmBuffInUse[i] = 1;
      PcmBuffAvail--;

      // Tone forwarding buffer
      memset (in->pBufrBase, 0, in->BufrSize * sizeof (ADT_UInt16));
      return i;
   }
   return VOID_INDEX;
}

#pragma CODE_SECTION (DeAllocPcmBuffs, "SLOW_PROG_SECT")
static void DeAllocPcmBuffs (ADT_UInt16 idx) {
    if (idx >= sysConfig.maxNumPcmBuffs) return;
    PcmBuffInUse[idx] = 0;
    PcmBuffAvail++;
}

#pragma CODE_SECTION (AllocBulkDelayBuff, "SLOW_PROG_SECT")
static ADT_UInt16 AllocBulkDelayBuff (CircBufInfo_t *buff) {
   int i;

   if (BulkDelayBuffAvail == 0) return VOID_INDEX;

   for (i=0; i<sysConfig.maxNumBulkDelayBuffs; i++) {
      if (BulkDelayBuffInUse[i] != 0) continue;
      CircBuffAllocate (buff, BulkDelayBufferPool, i, 
                    sysConfig.bulkDelayBufferSize, bulkDelayAlignI16);
      BulkDelayBuffInUse[i] = 1;
      BulkDelayBuffAvail--;
      return i;
   }

   return VOID_INDEX;
}

#pragma CODE_SECTION (DeAllocBulkDelayBuff, "SLOW_PROG_SECT")
static void DeAllocBulkDelayBuff (ADT_UInt16 idx) {
    if (idx >= sysConfig.maxNumBulkDelayBuffs) return;
    BulkDelayBuffInUse[idx] = 0;
    BulkDelayBuffAvail++;
}

#pragma CODE_SECTION (AllocPktBuffs, "SLOW_PROG_SECT")
static ADT_UInt16 AllocPktBuffs (CircBufInfo_t *in, CircBufInfo_t *out) {
   int i;

   if (PktBuffAvail == 0) return VOID_INDEX;

   for (i=0; i<sysConfig.maxNumPktBuffs; i++) {
      if (PktBuffInUse[i] != 0) continue;
      CircBuffAllocate (in,   PktInBufferPool, i, 
                   sysConfig.pktInOutBufferSize, pktBuffAlignI16);

      CircBuffAllocate (out,  PktOutBufferPool, i, 
                   sysConfig.pktInOutBufferSize, pktBuffAlignI16);
      PktBuffInUse[i] = 1;
      PktBuffAvail--;
      return i;
   }

   return VOID_INDEX;
}

#pragma CODE_SECTION (DeAllocPktBuffs, "SLOW_PROG_SECT")
static void DeAllocPktBuffs (ADT_UInt16 idx) {
    if (idx >= sysConfig.maxNumPktBuffs) return;
    PktBuffInUse[idx] = 0;
    PktBuffAvail++;
}

#pragma CODE_SECTION (CheckIfBuffsAvail, "SLOW_PROG_SECT")
int CheckIfBuffsAvail (GpakActivation pktEc) {
    if (PcmBuffAvail == 0) return 0;
    if (PktBuffAvail == 0) return 0;
    if (pktEc && (BulkDelayBuffAvail == 0)) return 0;

    return 1;
}
    
#pragma CODE_SECTION (AllocBuffs, "SLOW_PROG_SECT")
void AllocBuffs (chanInfo_t *chan) {
   pcm2pkt_t *PcmPkt;   // pointer to channel's PCM to Packet data
   pkt2pcm_t *PktPcm;   // pointer to channel's Packet to PCM data

   // Get pointers to the channel's A and B Sides
   PcmPkt = &chan->pcmToPkt;
   PktPcm = &chan->pktToPcm;

   // Allocate Pcm Buffers
   chan->pcmPoolIndex =  AllocPcmBuffs (&PcmPkt->inbuffer, &PktPcm->outbuffer);

   // Allocate Pkt Buffers
   chan->pktPoolIndex =  AllocPktBuffs (PktPcm->inbuffer, PcmPkt->outbuffer);

   // Alloc a BulkDelay buffer if packet Ec is enabled
   if (PktPcm->EchoCancel == Enabled)
      PcmPkt->bulkDelayIndex = AllocBulkDelayBuff (&PcmPkt->ecBulkDelay);
   else
      PcmPkt->bulkDelayIndex = VOID_INDEX;
}

#pragma CODE_SECTION (DeAllocBuffs, "SLOW_PROG_SECT")
void DeAllocBuffs (chanInfo_t *chan) {
   DeAllocPcmBuffs (chan->pcmPoolIndex);
   DeAllocPktBuffs (chan->pktPoolIndex);
   DeAllocBulkDelayBuff (chan->pcmToPkt.bulkDelayIndex);

   chan->pcmPoolIndex = VOID_INDEX;
   chan->pktPoolIndex = VOID_INDEX;
   chan->pcmToPkt.bulkDelayIndex = VOID_INDEX;

}
//}

//{ Sample rate conversion buffers
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ Sample rate converter routines
// 
//
//  Conversion instance allocations.
//
//  Encode (PcmPkt) Conversion requirements
//
//  Channels       Down                           Up
//  PcmPcm       ProcessRate < TDMRate    ProcessRate < TDMRate
//               Tone Detect Enabled
//
//  PcmPkt       ProcessRate < TDMRate    ProcessRate < Encode
//               Tone Detect Enabled
//
//  PktPkt       Tone Detect Enabled      ProcessRate < Encode
//
//  PcmCnfr      EC Enabled               ProcessRate < TDMRate
//
//  PktCnfr      Tone Relay Enabled       ProcessRate < Encode
//
//  MultiCnfr    Tone Relay Enabled       ProcessRate < Encode
//
//  Composite    Tone Relay Enabled       ProcessRate < Encode
//
//  Custom       TDM:16000 or WB Enabled  TDM:16000 or WB Enabled
//


//
//  Decode (PktPcm) Conversion requirements
//
//  Channels       Down                           Up
//  PcmPcm       ProcessRate < TDMRate    ProcessRate < TDMRate
//               Tone Detect Enabled
//
//  PcmPkt       ProcessRate < Decode     ProcessRate < TDMRate
//
//  PktPkt       ProcessRate < Decode          None
//
//  PcmCnfr      ProcessRate < TDMRate         None
//               Tone Detect Enabled
//
//  PktCnfr      ProcessRate < Decode          None
//               Tone Detect Enabled
//
//  MultiCnfr    ProcessRate < Decode          None
//               Tone Detect Enabled
//
//  Composite        None
//
//  Custom       TDM:16000 or WB Enabled  TDM:16000 or WB Enabled
//
//}
#define DecodeUp  1
#define DecodeDwn 2
#define EncodeUp  4
#define EncodeDwn 8
#define AllRates  (DecodeUp | DecodeDwn | EncodeUp | EncodeDwn)

#pragma CODE_SECTION (identifySampleRateConv, "SLOW_PROG_SECT")
static int identifySampleRateConv  (chanInfo_t *chan) {
   int   required;

   pcm2pkt_t *PcmPkt;
   pkt2pcm_t *PktPcm;

   PcmPkt = &chan->pcmToPkt;
   PktPcm = &chan->pktToPcm;

   required = 0;

   switch (chan->channelType) {
   case customChannel:
      if ((TDMRate == 16000) || (SysAlgs.wbEnable)) required = AllRates;
      break;

   case pcmToPcm:
      if (chan->ProcessRate < TDMRate)       required = AllRates;
      if (chan->ProcessRate == 8000) break;
      if (PcmPkt->toneTypes & ToneDetectAll) required |= EncodeDwn;
      if (PktPcm->toneTypes & ToneDetectAll) required |= DecodeDwn;
      break;

   case pcmToPacket:
      if (chan->ProcessRate < TDMRate)           required |= (EncodeDwn | DecodeUp);
      if (chan->ProcessRate < chan->EncoderRate) required |= EncodeUp;
      if (chan->ProcessRate < chan->DecoderRate) required |= DecodeDwn;
      if (chan->ProcessRate == 8000) break;
      if (PcmPkt->toneTypes & ToneDetectAll)     required |= EncodeDwn;
      if (PktPcm->toneTypes & ToneDetectAll)     required |= DecodeDwn;
      break;

   case packetToPacket:
      if (chan->ProcessRate < chan->EncoderRate) required |= EncodeUp;
      if (chan->ProcessRate < chan->DecoderRate) required |= DecodeDwn;
      if (chan->ProcessRate == 8000) break;
      if (PcmPkt->toneTypes & ToneDetectAll)     required |= EncodeDwn;
      break;

   case conferencePcm:
      if (chan->ProcessRate < TDMRate)       required |= (EncodeUp | DecodeDwn);
      if (PcmPkt->EchoCancel)                required |= EncodeDwn;
      if (chan->ProcessRate == 8000) break;
      if (PktPcm->toneTypes & ToneDetectAll) required |= DecodeDwn;
      break;

   case conferencePacket:
   case conferenceMultiRate:
      if (chan->ProcessRate < chan->EncoderRate) required |= EncodeUp;
      if (chan->ProcessRate < chan->DecoderRate) required |= DecodeDwn;
      if (PcmPkt->toneTypes & Tone_Relay)        required |= EncodeDwn;
      if (chan->ProcessRate == 8000) break;
      if (PktPcm->toneTypes & ToneDetectAll)     required |= DecodeDwn;
      break;

   case conferenceComposite:
      if (chan->ProcessRate < chan->EncoderRate) required |= EncodeUp;
      if (chan->ProcessRate == 8000) break;
      if (PcmPkt->toneTypes & Tone_Relay)        required |= EncodeDwn;
      break;

   case backgroundTranscoder:
      if (chan->ProcessRate < chan->EncoderRate) required |= EncodeUp;
      if (chan->ProcessRate < chan->DecoderRate) required |= DecodeDwn;
      break;

   default:
      break;
   }
   return required;
}

#pragma CODE_SECTION (AllocUpSamplingInst, "SLOW_PROG_SECT")
void *AllocUpSamplingInst (ADT_UInt16 *index) { 
   void  *Inst = NULL;
   ADT_UInt16 i;

   if (numSrcUpBy2ChansAvail == 0) 
      return NULL;

   *index = VOID_INDEX;
   for (i=0; i<sysConfig.numSrcChans; i++) {
      if (srcUpBy2ChanInUse[i] != 0) continue;

      Inst = SrcUpBy2Instance[i];
      srcUpBy2ChanInUse[i] = 1;
      numSrcUpBy2ChansAvail--;
      *index = i;
      break;
   }
   AppErr ("SRC Up Alloc", *index == VOID_INDEX);
   if (*index == VOID_INDEX) return NULL;
   return Inst;
}

#pragma CODE_SECTION (AllocDownSamplingInst, "SLOW_PROG_SECT")
void *AllocDownSamplingInst (ADT_UInt16 *index) {
   void  *Inst = NULL;
   ADT_UInt16 i;

   if (numSrcDownBy2ChansAvail == 0) 
      return NULL;

   *index = VOID_INDEX;
   for (i=0; i<sysConfig.numSrcChans; i++) {
        if (srcDownBy2ChanInUse[i] != 0) continue;

        Inst = SrcDownBy2Instance[i];
        srcDownBy2ChanInUse[i] = 1;
        numSrcDownBy2ChansAvail--;
        *index = i;
        break;
   }
   AppErr ("SRC Down Alloc", *index == VOID_INDEX);
   if (*index == VOID_INDEX) return NULL;
   return Inst;
}

#pragma CODE_SECTION (InitUpSamplingInst, "SLOW_PROG_SECT")
void InitUpSamplingInst   (void *Inst, int SamplesPerFrame) {
   AppErr ("Up sampling", (Inst == NULL));
   if (Inst == NULL) return;
   CLEAR_INST_CACHE (Inst, sizeof (RateUpBy2_Channel_t));
   FLUSH_wait ();
   SRC_ADT_InitInterpolateBy2 ((RateUpBy2_Channel_t *)Inst, SamplesPerFrame+INT_COEF_SIZE);
   FLUSH_INST_CACHE (Inst, sizeof (RateUpBy2_Channel_t));
   return;
}

#pragma CODE_SECTION (InitDownSamplingInst, "SLOW_PROG_SECT")
void InitDownSamplingInst (void *Inst, int SamplesPerFrame) {
   AppErr ("Down sampling", (Inst == NULL));
   if (Inst == NULL) return;
   CLEAR_INST_CACHE (Inst, sizeof (RateDownBy2_Channel_t));
   FLUSH_wait ();
   SRC_ADT_InitdecimateBy2 ((RateDownBy2_Channel_t *)Inst, SamplesPerFrame+DEC_COEF_SIZE);
   FLUSH_INST_CACHE (Inst, sizeof (RateDownBy2_Channel_t));
   return;
}

#pragma CODE_SECTION (AllocEncoderSampleRateConv, "SLOW_PROG_SECT")
void AllocEncoderSampleRateConv (chanInfo_t *chan) {
   ADT_UInt16 index;
   ADT_UInt16 samplesPerFrame;
   void *Inst;
   pcm2pkt_t *PcmPkt;
   int rqd = identifySampleRateConv (chan);

   PcmPkt = &chan->pcmToPkt;

   samplesPerFrame = PcmPkt->SamplesPerFrame  * (16000 / TDMRate);

   // Upsampling required
   if (rqd & EncodeUp) {
      Inst = AllocUpSamplingInst (&index);
      PcmPkt->rateCvt.UpInst  = Inst;
      PcmPkt->rateCvt.UpIndex = index;
      PcmPkt->rateCvt.UpFrame = samplesPerFrame;
      InitUpSamplingInst (Inst, samplesPerFrame);
   } else {
      PcmPkt->rateCvt.UpInst  = NULL;
      PcmPkt->rateCvt.UpIndex = VOID_INDEX;
      PcmPkt->rateCvt.UpFrame = 0;
   }

   // Downsampling required
   if (rqd & EncodeDwn) {
      Inst = AllocDownSamplingInst (&index);
      PcmPkt->rateCvt.DwnInst  = Inst;
      PcmPkt->rateCvt.DwnIndex = index;
      PcmPkt->rateCvt.DwnFrame = samplesPerFrame;
      InitDownSamplingInst (Inst, samplesPerFrame);
   } else {
      PcmPkt->rateCvt.DwnInst  = NULL;
      PcmPkt->rateCvt.DwnIndex = VOID_INDEX;
      PcmPkt->rateCvt.DwnFrame = 0;
   }
}

#pragma CODE_SECTION (AllocDecoderSampleRateConv, "SLOW_PROG_SECT")
void AllocDecoderSampleRateConv (chanInfo_t *chan) {
   ADT_UInt16 index;
   ADT_UInt16 samplesPerFrame;
   void *Inst;
   pkt2pcm_t *PktPcm;

   int rqd = identifySampleRateConv (chan);

   PktPcm = &chan->pktToPcm;
   samplesPerFrame = PktPcm->SamplesPerFrame * (16000 / TDMRate);

   // Upsampling required when ProcessRate < Encoder Rate
   if (rqd & DecodeUp) {
      Inst = AllocUpSamplingInst (&index);
      PktPcm->rateCvt.UpInst  = Inst;
      PktPcm->rateCvt.UpIndex = index;
      PktPcm->rateCvt.UpFrame = samplesPerFrame;
      InitUpSamplingInst (Inst, samplesPerFrame);
   } else {
      PktPcm->rateCvt.UpInst  = NULL;
      PktPcm->rateCvt.UpIndex = VOID_INDEX;
      PktPcm->rateCvt.UpFrame = 0;
   }

   // On multi-rate channels decode down conversions take place at packet rate
   // tone detect down conversions take place at conference rate
   if ((chan->channelType == conferenceMultiRate) && (chan->ProcessRate == chan->DecoderRate)) {
      samplesPerFrame = GetGpakCnfr (chan->CoreID, chan->PairedChannelId)->FrameSize;
      samplesPerFrame *= chan->ProcessRate / TDMRate;
   }

   // Downsampling required when ProcessRate < Decoder Rate or
   // Tone Detection is enabled
   if (rqd & DecodeDwn) {
      Inst = AllocDownSamplingInst (&index);
      PktPcm->rateCvt.DwnInst  = Inst;
      PktPcm->rateCvt.DwnIndex = index;
      PktPcm->rateCvt.DwnFrame = samplesPerFrame;
      InitDownSamplingInst (Inst, samplesPerFrame);
   } else {
      PktPcm->rateCvt.DwnInst  = NULL;
      PktPcm->rateCvt.DwnIndex = VOID_INDEX;
      PktPcm->rateCvt.DwnFrame = 0;
   }
}

#pragma CODE_SECTION (sampleRateConversionAvail, "SLOW_PROG_SECT")
ADT_Bool sampleRateConversionAvail (chanInfo_t *chan) {

   int rateConversions = identifySampleRateConv (chan);
   int conversions;

   conversions = rateConversions & (EncodeUp | DecodeUp);
   if (conversions) {
      if (conversions == (EncodeUp | DecodeUp))
         conversions = 2;
      else
         conversions = 1;
   }
   if (numSrcUpBy2ChansAvail < conversions) return FALSE; 

   conversions = rateConversions & (EncodeDwn | DecodeDwn);
   if (conversions) {
      if (conversions == (EncodeDwn | DecodeDwn))
         conversions = 2;
      else
         conversions = 1;
   }
   if (numSrcDownBy2ChansAvail < conversions) return FALSE; 
   return TRUE;
}

#pragma CODE_SECTION (setChannelRates, "SLOW_PROG_SECT")
GPAK_ChannelConfigStat_t setChannelRates (chanInfo_t *chan) {
   chanInfo_t *chanB;

   // Setup encode, decode and process sample rates
   // 
   // Process rate is the lowest rate of the encode, decode, and TDM bus rates.
   // This provides the minimum CPU overhead while maintaining the highest possible voice quality
   //
   chan->EncoderRate = codecSamplingRate (chan->pcmToPkt.Coding);
   chan->DecoderRate = codecSamplingRate (chan->pktToPcm.Coding);
   chan->ProcessRate = MIN (chan->DecoderRate, chan->EncoderRate);
   if ((chan->pktToPcm.EchoCancel) || (chan->pcmToPkt.EchoCancel)) {
      chan->ProcessRate = 8000;
   }

   switch (chan->channelType) {
   case packetToPacket:
      chanB = chanTable[chan->PairedChannelId];
      chanB->EncoderRate = codecSamplingRate (chanB->pcmToPkt.Coding);
      chanB->DecoderRate = codecSamplingRate (chanB->pktToPcm.Coding);
      chanB->ProcessRate = MIN (chanB->DecoderRate, chanB->EncoderRate);

      chan->ProcessRate  = MIN (chan->ProcessRate,  chanB->ProcessRate);
      chanB->ProcessRate = chan->ProcessRate;
      if ((chanB->pktToPcm.EchoCancel == Enabled) || (chanB->pcmToPkt.EchoCancel == Enabled)) {
         chanB->ProcessRate = 8000;
      }
      if (sampleRateConversionAvail (chanB) != TRUE)
         return Cc_InsuffRateConverters;
      break;

   case pcmToPacket:
      chan->ProcessRate = MIN (chan->ProcessRate, TDMRate);
      break;

   case pcmToPcm:   
      chan->ProcessRate = MIN (chan->ProcessRate, TDMRate);
      chan->EncoderRate = chan->DecoderRate = chan->ProcessRate;
      break;

   case conferencePcm:
      chan->EncoderRate = chan->DecoderRate = chan->ProcessRate = TDMRate;
      break;

   case conferencePacket:
   case conferenceComposite:
   case conferenceMultiRate:
      chan->ProcessRate = MIN (chan->ProcessRate, TDMRate);
      break;
   case loopbackCoder:
      chan->ProcessRate = MIN (chan->EncoderRate, chan->DecoderRate);
      break;

   case customChannel:
      chan->ProcessRate = MIN (chan->EncoderRate, chan->DecoderRate);
      chan->ProcessRate = MIN (chan->ProcessRate, TDMRate);
      break;
   }
   if ((chan->pktToPcm.EchoCancel) || (chan->pktToPcm.CIDMode != CidDisabled) ||
       (chan->pcmToPkt.EchoCancel) || (chan->pcmToPkt.CIDMode != CidDisabled)) {
      chan->ProcessRate = 8000;
   }

   if (sampleRateConversionAvail (chan) != TRUE)
      return Cc_InsuffRateConverters;

   // EC forces narrowband processing
   return Cc_Success;
}
// -------------------------------------------------------------------
#pragma CODE_SECTION (DeallocUpConverter, "MEDIUM_PROG_SECT")
void DeallocUpConverter (ADT_UInt16 index) {

   if ((index < sysConfig.numSrcChans) && (srcUpBy2ChanInUse[index] != 0)) {
      CLEAR_INST_CACHE (SrcUpBy2Instance[index], sizeof (RateUpBy2_Channel_t));
      srcUpBy2ChanInUse[index] = 0;
      numSrcUpBy2ChansAvail++;
   } else if (index != VOID_INDEX) {
      AppErr ("Src Up Dealloc", index);
   }
}
#pragma CODE_SECTION (DeallocDownConverter, "MEDIUM_PROG_SECT")
void DeallocDownConverter (int index) {
   if ((index < sysConfig.numSrcChans) && (srcDownBy2ChanInUse[index] != 0)) {
      CLEAR_INST_CACHE (SrcDownBy2Instance[index], sizeof (RateDownBy2_Channel_t));
      srcDownBy2ChanInUse[index] = 0;
      numSrcDownBy2ChansAvail++;
   } else if (index != VOID_INDEX) {
      AppErr ("Src Down Dealloc", index);
   }
}

#pragma CODE_SECTION (DeallocEncoderSampleRateConv, "SLOW_PROG_SECT")
void DeallocEncoderSampleRateConv (chanInfo_t *chan) {
   pcm2pkt_t *PcmPkt;
   PcmPkt = &chan->pcmToPkt;

   // Encoder conversion teardown
   DeallocUpConverter   (PcmPkt->rateCvt.UpIndex);
   DeallocDownConverter (PcmPkt->rateCvt.DwnIndex);

   // Playback/recording teardown
   if (PcmPkt->recA.state != Inactive)
      playRecordEnd (chan->ChannelId, EventRecordingStopped, &PcmPkt->recA, ADevice);

   if (PcmPkt->playB.state != Inactive)
      playRecordEnd (chan->ChannelId, EventPlaybackComplete, &PcmPkt->playB, BDevice);
}
#pragma CODE_SECTION (DeallocDecoderSampleRateConv, "SLOW_PROG_SECT")
void DeallocDecoderSampleRateConv (chanInfo_t *chan) {
   pkt2pcm_t *PktPcm;
   PktPcm = &chan->pktToPcm;

   // Decoder conversion teardown
   DeallocUpConverter   (PktPcm->rateCvt.UpIndex);
   DeallocDownConverter (PktPcm->rateCvt.DwnIndex);

   // Playback/recording teardown
   if (PktPcm->playA.state != Inactive)
      playRecordEnd (chan->ChannelId, EventPlaybackComplete, &PktPcm->playA, ADevice);

   if (PktPcm->recB.state != Inactive)
      playRecordEnd (chan->ChannelId, EventRecordingStopped, &PktPcm->recB, BDevice);
}
//}

//{  Echo canceller buffers
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Echo cancellers
// AllocEchoCanceller      - Allocate single echo canceller instance
// invalidateEchoCanceller - Invalidates channel's echo canceller cached memory
// DeallocEcans            - Deallocate all of a channel's echo canceller instances
// DeallocateEchoCanceller - Deallocate a single echo canceller instance
// EchoCancellerReInit     - Reinitializes the canceller
// 

// Echo canceller

#if (AEC_LIB_VERSION >= 0x0420)

ADT_UInt32 hAecLock;		// AEC lock handle

#pragma CODE_SECTION (aecLock, "MEDIUM_PROG_SECT")
ADT_UInt32 aecLock(
	void *LockHandle,		//If LOCK, UNLOCK, DELETE_LOCK: Handle to lock. Null otherwise
	char *Name,				//If CREATE_LOCK, name of lock. If LOCK or UNLOCK, name of calling function
	LockAction_e Action,	
	void **CreatedLock		//Used only if action is CREATE_LOCK
	)
{

	ADT_UInt32 curTime;			// current time

	if (Action == LOCK)
	{
	}
	else if (Action == UNLOCK)
	{
	}
	else if (Action == CREATE_LOCK)
	{
		*CreatedLock = &hAecLock;
	}
	return 0;
}
#endif

#pragma CODE_SECTION (EchoCancellerReInit, "MEDIUM_PROG_SECT")
void EchoCancellerReInit (ADT_UInt16 FrameSize, ADT_UInt16 EcID, G168ChannelInstance_t *pInst, chanInfo_t *chan) {

   ADT_UInt16  ecIdx, EcanType;
   short *pEchoPath;     
   short *pBgEchoPath; 

   G168Params_t EcanParms;           
   G168ChannelInstance_t *ecInst;     
   G168_Scratch_t     *pSAscratch;   
   G168_DAScratch_t   *pDAscratch;   
   G168_SA_State_t    *pSaState;     
   G168_DA_State_t    *pDaState;     
   ADT_UInt16         SAScratchLen, DAScratchLen;

#if (DSP_TYPE == 64)
   ecInstanceInfo_t *ecInfo;
   ADT_UInt8 initStatus;
#endif


#if (DSP_TYPE == 54)
   G168_BG_SA_State_t *pBackFarState;
   G168_BG_SA_State_t *pBackNearState;
#endif
   
   // Allocate scratch memory by frame size
   pSAscratch = (G168_Scratch_t *)   getSAScratch (FrameSize, &SAScratchLen);
   pDAscratch = (G168_DAScratch_t *) getDAScratch (FrameSize, &DAScratchLen);
   AppErr ("Scratch Allocation", (pSAscratch == NULL) || (pDAscratch == NULL));

   EcanType = (EcID >> 8) & 0xFF;
   ecIdx  =  EcID & 0xFF;
#ifdef AEC_SUPPORTED
   if (EcanType == ACOUSTIC) {
        AECG4_ADT_reset ((IAECG4_Handle) pInst, &(aecChanParms[ecIdx]));
        return;
   } 
#endif
   if (EcanType == G168_PCM)   {
      ecInst         = (G168ChannelInstance_t *) pPcmEcChan[ecIdx];
      pEchoPath     = (short int *)             pPcmEcEchoPath[ecIdx];
      pDaState      = (G168_DA_State_t *)       pPcmEcDaState[ecIdx];
      pSaState      = (G168_SA_State_t *)       pPcmEcSaState[ecIdx];
      pBgEchoPath = (short int *)               pPcmEcBackEp[ecIdx];
      LEC_ADT_g168GetConfig (ecInst, &EcanParms);
   }

   if (SysAlgs.g168VarAEnable)   {
      // Short tail dependent values.
      pDAscratch     = (G168_DAScratch_t *) 0;
      pBgEchoPath  = (short int *) 0;
#if (DSP_TYPE == 54)
      pBackFarState  = (G168_BG_SA_State_t *) 0;
      pBackNearState = (G168_BG_SA_State_t *) 0;
#endif
   } else {

      // Standard tail dependent values.
      if (EcanType == G168_PCM)   {
         pBgEchoPath  = (short int *)          pPcmEcBackEp[ecIdx];
#if (DSP_TYPE == 54)
         pBackFarState  = (G168_BG_SA_State_t *) pPcmEcBackFar[ecIdx];
         pBackNearState = (G168_BG_SA_State_t *) pPcmEcBackNear[ecIdx];
#endif         
      } else {
         pBgEchoPath  = (short int *)          pPktEcBackEp[ecIdx];
#if (DSP_TYPE == 54)
         pBackFarState  = (G168_BG_SA_State_t *) pPktEcBackFar[ecIdx];
         pBackNearState = (G168_BG_SA_State_t *) pPktEcBackNear[ecIdx];
#endif         
      }
   }

#if (DSP_TYPE == 64)
   if (EcanType == G168_PCM) 
        ecInfo = &pcmEcInfo;
   else
        ecInfo = &pktEcInfo;   // should never hit this code

   EcanParms.ChannelSizeI8    = ecInfo->EcChanMemSize       * sizeof(ADT_UInt16);
   EcanParms.EchoPathSizeI8   = ecInfo->EcEchoPathMemSize   * sizeof(ADT_UInt16);
   EcanParms.DAStateSizeI8    = ecInfo->EcDAStateMemSize    * sizeof(ADT_UInt16);
   EcanParms.SAStateSizeI8    = ecInfo->EcSAStateMemSize    * sizeof(ADT_UInt16);
   EcanParms.SAScratchSizeI8  = SAScratchLen                * sizeof(ADT_UInt16);
   EcanParms.BGEchoPathSizeI8 = ecInfo->EcBgEchoPathMemSize * sizeof(ADT_UInt16);
   EcanParms.DAScratchSizeI8  = DAScratchLen                * sizeof(ADT_UInt16);
#endif

#if (DSP_TYPE == 64)
   initStatus = 
#endif

   // Initialize the Echo Canceller.
   LEC_ADT_g168Init (ecInst, &EcanParms, pEchoPath,  pDaState, pSaState,
                     pSAscratch,    pBgEchoPath,  pDAscratch
#if (DSP_TYPE == 54)
                    , pBackFarState, pBackNearState, pEchoPath
#elif (DSP_TYPE == 55)
                , pEchoPath
#endif
   );

#if (DSP_TYPE == 64)
   AppErr ("EC init Version Error", initStatus == 1);
   AppErr ("EC init StructSize Error", initStatus > 1);
   (void) initStatus;
#endif

   return ;
#undef Components
}

#ifdef AEC_SUPPORTED
#pragma CODE_SECTION (createAecHandle, "SLOW_PROG_SECT")
IAECG4_Handle createAecHandle(
	ADT_UInt16 FrameSize,
	ADT_UInt16 ecIdx,
	IALG_MemRec *pMemTab
	)
{

   EqParams_t   eqParms;

   // NOTE:  AEC is not supported on multicore CPUs.

   //- Initialize
   AECHandles[ecIdx] = AECG4_ADT_createStatic (NULL, &(aecChanParms[ecIdx]), pMemTab);
   AppErr ("AEC Alloc", AECHandles[ecIdx] == NULL);

   //-------------------------
   // Initialize equalizers
   if (sysConfig.micEqLength != 0) {
      eqParms.coefLength  = sysConfig.micEqLength;
      eqParms.pCoef   = micEqCoef;
      eqParms.pState  = pMicEqState[ecIdx];
      eqParms.frameSize = FrameSize;
      eqInit(&micEqChan[ecIdx], &eqParms);
   }

   if (sysConfig.spkrEqLength != 0) {
      eqParms.coefLength  = sysConfig.spkrEqLength;
      eqParms.pCoef   = spkrEqCoef;
      eqParms.pState  = pSpkrEqState[ecIdx];
      eqParms.frameSize = FrameSize;
      eqInit(&spkrEqChan[ecIdx], &eqParms);
   }
   return AECHandles[ecIdx];
}
#endif

#pragma CODE_SECTION (CheckAecMemory, "SLOW_PROG_SECT")
ADT_Bool CheckAecMemory(ADT_UInt16 FrameSize, ADT_UInt32 SampleRate)
{
#ifdef AEC_SUPPORTED

	IAECG4_Params tempAecParms;			// AEC parameters
	IALG_MemRec memTab[MTAB_NRECS];		// AEC memory needs
	ADT_Int32 totalSize;				// total size of memory needed
	int i;								// loop index / counter
	ADT_UInt16 sampsPerMs;

	sampsPerMs = sysConfig.samplesPerMs;
	if (SampleRate < TDMRate)
	{
		FrameSize /= (TDMRate / SampleRate);
		sampsPerMs /= (TDMRate / SampleRate);
	}

	tempAecParms = AECConfig;
	tempAecParms.frameSize = FrameSize;
#if (AEC_LIB_VERSION >= 0x0420)
	tempAecParms.lockCallback = aecLock;
	tempAecParms.fixedBulkDelayMSec = (FrameSize*2) / sampsPerMs; 
#else
	tempAecParms.bulkDelaySamples = FrameSize*2; 
#endif
	tempAecParms.samplingRate = SampleRate;
	tempAecParms.maxAudioFreq = SampleRate/2;
	AECG4_ADT_alloc((const IALG_Params *) &tempAecParms, NULL, memTab);
	totalSize = 0;
	for (i = 0; i < MTAB_NRECS; i++)
	{
		memTab[i].size = (memTab[i].size + 7) & ~7;
		totalSize += memTab[i].size;
	}
	if (totalSize > AEC_Inst_I8)
	{
		return FALSE;
	}
	else
	{
		return TRUE;
	}
#else
	    return FALSE;
#endif
}


#pragma CODE_SECTION (AllocEchoCanceller, "SLOW_PROG_SECT")
G168ChannelInstance_t *AllocEchoCanceller (ADT_UInt16 EcanType, 
                            ADT_UInt16 FrameSize, ADT_UInt16 *EcID,
                            ADT_UInt32 SampleRate) {
#ifdef AEC_SUPPORTED
   IALG_MemRec memTab[MTAB_NRECS];
   ADT_Int32 totalSize;
   ADT_UInt32 memAddrs;
   ADT_UInt16 sampsPerMs;
#endif
   ecInstanceInfo_t *ecInfo;
   ADT_UInt16 i, nbFrameSamples;  // narrow band frame size
   ADT_UInt16  ecIdx;
   short *pEchoPath;     
   short *pBgEchoPath;  // Background echo path

   G168Params_t EcanParms;           
   G168ChannelInstance_t *ecInst;     
   G168_Scratch_t     *pSAscratch;   
   G168_DAScratch_t   *pDAscratch;   
   G168_SA_State_t    *pSaState;     
   G168_DA_State_t    *pDaState;     
   ADT_UInt16         SAScratchI16, DAScratchI16;
   ADT_UInt8 initStatus;

#if (DSP_TYPE == 54)
   G168_BG_SA_State_t *pBackFarState;
   G168_BG_SA_State_t *pBackNearState;
#endif


   // Allocate scratch memory by frame size
   pSAscratch = (G168_Scratch_t *)   getSAScratch (FrameSize, &SAScratchI16);
   if (0x3fff < SAScratchI16)  SAScratchI16 = 0x3fff;

   pDAscratch = (G168_DAScratch_t *) getDAScratch (FrameSize, &DAScratchI16);
   AppErr ("Scratch Allocation", (pSAscratch == NULL) || (pDAscratch == NULL));

   // Allocate echo canceller instance data
   ecIdx = 0xFFFF;
#ifdef AEC_SUPPORTED
   if (EcanType == ACOUSTIC) {

      // Find an unused acoustic Echo Canceller.
      for (i = 0; i < sysConfig.AECInstances; i++) {
         if (AECInUse[i] == 0)   {
            AECInUse[i] = 3;    // 3 = In use,  2 = Waiting for deletion by idle task
            ecIdx = i;
            NumAECEcansUsed++;
            break;
         }
      }
      AppErr ("EC allocation error", ecIdx == 0xFFFF);

      sampsPerMs = sysConfig.samplesPerMs;
      if (SampleRate < TDMRate)
      {
         FrameSize /= (TDMRate / SampleRate);
         sampsPerMs /= (TDMRate / SampleRate);
      }

      //- Setup parameters
      aecChanParms[ecIdx] = AECConfig;
      aecChanParms[ecIdx].frameSize = FrameSize;
#if (AEC_LIB_VERSION >= 0x0420)
      aecChanParms[ecIdx].lockCallback = aecLock;
      aecChanParms[ecIdx].fixedBulkDelayMSec = (FrameSize*2) / sampsPerMs;
#else
      aecChanParms[ecIdx].bulkDelaySamples = FrameSize*2; 
#endif
      aecChanParms[ecIdx].samplingRate = SampleRate;
      aecChanParms[ecIdx].maxAudioFreq = SampleRate/2;

      // Verify AEC instance size and assign context memory addresses.
      AECG4_ADT_alloc((const IALG_Params *) &(aecChanParms[ecIdx]), NULL, memTab);
      totalSize = 0;
      memAddrs = (ADT_UInt32) AECChan[ecIdx];
      for (i = 0; i < MTAB_NRECS; i++)
      {
         memTab[i].size = (memTab[i].size + 7) & ~7;
         totalSize += memTab[i].size;
         memTab[i].base = (void *) memAddrs;
         memset(memTab[i].base, 0, memTab[i].size);
         memAddrs += memTab[i].size;
      }
      if (totalSize > AEC_Inst_I8)
      {
         AECInUse[ecIdx] = 0;
         NumAECEcansUsed--;
         return NULL;
      }

      ecInst = (G168ChannelInstance_t *) createAecHandle(FrameSize, ecIdx, memTab);

      *EcID = EcanType << 8 | ecIdx;
      return  ecInst;

   }
#endif
   if (EcanType == G168_PCM)   {

      // Find an unused PCM Echo Canceller.
      for (i = 0; i < sysConfig.numPcmEcans; i++) {
         if (PcmEcInUse[i] == 0)   {
            PcmEcInUse[i] = 1;
            ecIdx = i;
            NumPcmEcansUsed++;
            break;
         }
      }

      EcanParms     = g168_PCM_EcParams;

      ecInst         = (G168ChannelInstance_t *) pPcmEcChan[ecIdx];
      pEchoPath     = (short int *)             pPcmEcEchoPath[ecIdx];
      pDaState      = (G168_DA_State_t *)       pPcmEcDaState[ecIdx];
      pSaState      = (G168_SA_State_t *)       pPcmEcSaState[ecIdx];
      pBgEchoPath = (short int *)               pPcmEcBackEp[ecIdx];

      ecInfo = &pcmEcInfo;

   } else if (EcanType == G168_PKT) {

      // Find an unused Packet Echo Canceller.
      for (i = 0; i < sysConfig.numPktEcans; i++) {
         if (PktEcInUse[i] == 0)   {
            PktEcInUse[i] = 1;
            ecIdx = i;
            NumPktEcansUsed++;
            break;
         }
      }

      EcanParms     = g168_PKT_EcParams;
      ecInst         = (G168ChannelInstance_t *) pPktEcChan[ecIdx];
      pEchoPath     = (short int *)             pPktEcEchoPath[ecIdx];
      pDaState      = (G168_DA_State_t *)       pPktEcDaState[ecIdx];
      pSaState      = (G168_SA_State_t *)       pPktEcSaState[ecIdx];
      pBgEchoPath = (short int *)               pPktEcBackEp[ecIdx];

      ecInfo = &pktEcInfo;
   }
   if (sysConfig.samplesPerMs == 16)
      nbFrameSamples = FrameSize / 2;
   else
      nbFrameSamples = FrameSize;

   AppErr ("EC allocation error", ecIdx == 0xFFFF);
   CLEAR_INST_CACHE (ecInst,      ecInfo->EcChanMemSize * sizeof (ADT_UInt16));  
   CLEAR_INST_CACHE (pDaState,    ecInfo->EcDAStateMemSize * sizeof (ADT_UInt16));
   CLEAR_INST_CACHE (pSaState,    ecInfo->EcSAStateMemSize * sizeof (ADT_UInt16));
   CLEAR_INST_CACHE (pEchoPath,   ecInfo->EcEchoPathMemSize * sizeof (ADT_UInt16));

   if (SysAlgs.g168VarAEnable)   {
      // Short tail dependent values.
      pDAscratch     = (G168_DAScratch_t *) 0;
      pBgEchoPath  = (short int *) 0;
#if (DSP_TYPE == 54)
      pBackFarState  = (G168_BG_SA_State_t *) 0;
      pBackNearState = (G168_BG_SA_State_t *) 0;
#endif
   } else {

      // Standard tail dependent values.
      if (EcanType == G168_PCM)   {
         pBgEchoPath  = (short int *)          pPcmEcBackEp[ecIdx];
#if (DSP_TYPE == 54)
         pBackFarState  = (G168_BG_SA_State_t *) pPcmEcBackFar[ecIdx];
         pBackNearState = (G168_BG_SA_State_t *) pPcmEcBackNear[ecIdx];
#endif         
      } else {
         pBgEchoPath  = (short int *)          pPktEcBackEp[ecIdx];
#if (DSP_TYPE == 54)
         pBackFarState  = (G168_BG_SA_State_t *) pPktEcBackFar[ecIdx];
         pBackNearState = (G168_BG_SA_State_t *) pPktEcBackNear[ecIdx];
#endif         
      }
   }
   if (pBgEchoPath) CLEAR_INST_CACHE (pBgEchoPath, ecInfo->EcBgEchoPathMemSize * sizeof (ADT_UInt16));

   // Determine the values of the frame size dependent parameters.
   EcanParms.FrameSize = nbFrameSamples;
   EcanParms.AdaptLimit = ((nbFrameSamples * EcanParms.AdaptLimit) + 50) / 100;
   if (EcanParms.AdaptLimit < 1)
      EcanParms.AdaptLimit = 1;
       
   EcanParms.CrossCorrLimit =  (nbFrameSamples / 7) + 1;
   if (EcanParms.CrossCorrLimit < 2)
      EcanParms.CrossCorrLimit = 2;

   EcanParms.FIRTapCheckPeriod = 80;

#if (DSP_TYPE == 64)
      EcanParms.ChannelSizeI8    = ecInfo->EcChanMemSize       * sizeof(ADT_UInt16);
      EcanParms.EchoPathSizeI8   = ecInfo->EcEchoPathMemSize   * sizeof(ADT_UInt16);
      EcanParms.DAStateSizeI8    = ecInfo->EcDAStateMemSize    * sizeof(ADT_UInt16);
      EcanParms.SAStateSizeI8    = ecInfo->EcSAStateMemSize    * sizeof(ADT_UInt16);
      EcanParms.SAScratchSizeI8  = SAScratchI16                * sizeof(ADT_UInt16);
      EcanParms.BGEchoPathSizeI8 = ecInfo->EcBgEchoPathMemSize * sizeof(ADT_UInt16);
      EcanParms.DAScratchSizeI8  = DAScratchI16                * sizeof(ADT_UInt16);

      // EcID holds target core on entry   
      EcanParms.ChannelNumber = updateEcChanCount (*EcID, FrameSize, EcanType, 1);
      EcanParms.pMIPSConserve = (MIPSConserve_t *)getMipsConservePtr (FrameSize);
#endif

   FLUSH_wait ();
   // Initialize the Echo Canceller.
   initStatus = LEC_ADT_g168Init (ecInst, &EcanParms, pEchoPath,  pDaState, pSaState,
                     pSAscratch,    pBgEchoPath,  pDAscratch
#if (DSP_TYPE == 54)
                    , pBackFarState, pBackNearState, pEchoPath
#elif (DSP_TYPE == 55)
                , pEchoPath
#endif
   );

#if (DSP_TYPE == 64)
   AppErr ("EC init Version Error", initStatus == 1);
   AppErr ("EC init StructSize Error", 1 < initStatus);
#endif
   (void) initStatus;

   *EcID = EcanType << 8 | ecIdx;

   // Flush cache after initialization to allow usage on another core
   FLUSH_INST_CACHE (ecInst,      ecInfo->EcChanMemSize * sizeof (ADT_UInt16));  
   FLUSH_INST_CACHE (pDaState,    ecInfo->EcDAStateMemSize * sizeof (ADT_UInt16));
   FLUSH_INST_CACHE (pSaState,    ecInfo->EcSAStateMemSize * sizeof (ADT_UInt16));
   FLUSH_INST_CACHE (pEchoPath,   ecInfo->EcEchoPathMemSize * sizeof (ADT_UInt16));
   if (pBgEchoPath) FLUSH_INST_CACHE (pBgEchoPath, ecInfo->EcBgEchoPathMemSize * sizeof (ADT_UInt16));
   // Return a pointer to the Echo Canceller channel structure.
   return ecInst;
#undef Components
}
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma CODE_SECTION (invalidateEchoCanceller, "SLOW_PROG_SECT")
static void invalidateEchoCanceller (ADT_UInt16 EcanType, ADT_UInt16 ecIdx) {
   G168ChannelInstance_t *ecInst;     
   G168_SA_State_t    *pSaState;     
   G168_DA_State_t    *pDaState;     
   short              *pEchoPath;     
   short              *pBgEchoPath;  // Background echo path

   ecInstanceInfo_t   *ecInfo;


   if (EcanType == G168_PCM)   {
      ecInst        = (G168ChannelInstance_t *) pPcmEcChan[ecIdx];
      pEchoPath     = (short int *)             pPcmEcEchoPath[ecIdx];
      pDaState      = (G168_DA_State_t *)       pPcmEcDaState[ecIdx];
      pSaState      = (G168_SA_State_t *)       pPcmEcSaState[ecIdx];
      pBgEchoPath   = (short int *)             pPcmEcBackEp[ecIdx];
      ecInfo        = &pcmEcInfo;
   } else if (EcanType == G168_PKT) {
      ecInst        = (G168ChannelInstance_t *) pPktEcChan[ecIdx];
      pEchoPath     = (short int *)             pPktEcEchoPath[ecIdx];
      pDaState      = (G168_DA_State_t *)       pPktEcDaState[ecIdx];
      pSaState      = (G168_SA_State_t *)       pPktEcSaState[ecIdx];
      pBgEchoPath   = (short int *)             pPktEcBackEp[ecIdx];
      ecInfo        = &pktEcInfo;
   } else
     return;

   // Clear instance cache to allow re-use
   CLEAR_INST_CACHE (ecInst,      ecInfo->EcChanMemSize     * sizeof (ADT_UInt16));  
   CLEAR_INST_CACHE (pDaState,    ecInfo->EcDAStateMemSize  * sizeof (ADT_UInt16));
   CLEAR_INST_CACHE (pSaState,    ecInfo->EcSAStateMemSize  * sizeof (ADT_UInt16));
   CLEAR_INST_CACHE (pEchoPath,   ecInfo->EcEchoPathMemSize * sizeof (ADT_UInt16));
   CLEAR_INST_CACHE (pBgEchoPath, ecInfo->EcBgEchoPathMemSize * sizeof (ADT_UInt16));
   return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma CODE_SECTION (DeallocateEchoCanceller, "SLOW_PROG_SECT")
static void DeallocateEchoCanceller (ADT_UInt16 EcanType, ADT_UInt16 ecIdx) {
   invalidateEchoCanceller (EcanType, ecIdx);

   // Deallocate the Echo Canceller's data structures
   if (EcanType == G168_PCM)   {
      if (sysConfig.numPcmEcans <= ecIdx) return;
      if (PcmEcInUse[ecIdx] != 0)  {
         PcmEcInUse[ecIdx] = 0;
         NumPcmEcansUsed--;
      }
      return;
   }

   if (EcanType == G168_PKT)   {
      if (sysConfig.numPktEcans <= ecIdx) return;
      if (PktEcInUse[ecIdx] != 0)  {
         PktEcInUse[ecIdx] = 0;
         NumPktEcansUsed--;
      }
      return;
   }

   // Mark AEC instance for deletion by idle task.
   if (EcanType == ACOUSTIC)   {
      if (sysConfig.AECInstances <= ecIdx) return;
      if (AECInUse[ecIdx] == 3)  {
         AECInUse[ecIdx] = 2;       // Mark AEC for deletion
      }
      return;
   }
   AppErr ("EC Delloc", TRUE);
   return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma CODE_SECTION (DeallocEcans, "SLOW_PROG_SECT")
void DeallocEcans (chanInfo_t *chan) {

   ADT_UInt16 EcanType, EcanIdx, EcanID;

   pcm2pkt_t *PcmPkt;
   pkt2pcm_t *PktPcm;

   PcmPkt = &chan->pcmToPkt;
   PktPcm = &chan->pktToPcm;
   
   // Determine if an Echo Canceller needs to be deallocated.
   if (PcmPkt->EchoCancel == Enabled || PcmPkt->AECEchoCancel == Enabled) {
      EcanID   = PcmPkt->EcIndex;
      EcanType = (EcanID >> 8) & 0xFF;
      EcanIdx  =  EcanID & 0xFF;
      updateEcChanCount(chan->CoreID, PcmPkt->SamplesPerFrame, EcanType, -1);
      DeallocateEchoCanceller (EcanType, EcanIdx);
   }
        
   if (PktPcm->EchoCancel == Enabled || PktPcm->AECEchoCancel == Enabled) {
      EcanID   = PktPcm->EcIndex;
      EcanType = (EcanID >> 8) & 0xFF;
      EcanIdx  =  EcanID & 0xFF;
      updateEcChanCount(chan->CoreID, PktPcm->SamplesPerFrame, EcanType, -1);
      DeallocateEchoCanceller (EcanType, EcanIdx);
   }
   return;
}

//}

//{  AGC buffers
//===================================================================================
// AGC
// -------------------------------------------------------------------
// AllocAGC ( ) allocate and initialize an AGC instance
// DeallocAGC ( ) deallocate all AGC instance of a channel
#pragma CODE_SECTION (AllocAGC, "SLOW_PROG_SECT")
void AllocAGC (chanInfo_t *chan, GpakDeviceSide_t deviceSide, GpakActivation enabled) {

   ADT_UInt16 index = VOID_INDEX;
   AGCInstance_t  *pAGC;
   ADT_UInt16 i;

   pcm2pkt_t *PcmPkt;
   pkt2pcm_t *PktPcm;
   AGC_ADT_Param_t *agcParm;

   PcmPkt = &(chan->pcmToPkt);
   PktPcm = &(chan->pktToPcm);

   if (!enabled)   {
      if (deviceSide == ADevice) {
         PcmPkt->agcPtr = NULL;
         PcmPkt->AGCIndex = VOID_INDEX;
      } else {
         PktPcm->agcPtr = NULL;
         PktPcm->AGCIndex = VOID_INDEX;
      }
      return;
   }

   for (i=0; i<sysConfig.numAGCChans; i++) {
      if (agcChanInUse[i] != 0) continue;

      pAGC = AgcInstance[i];
      agcChanInUse[i] = 1;
      numAGCChansAvail--;
      index = i;
      break;
   }
   AppErr ("AGC Alloc", index == VOID_INDEX);
   CLEAR_INST_CACHE (pAGC, sizeof (AGCInstance_t));

   
   if (deviceSide == ADevice) {
      PcmPkt->agcPtr = pAGC;
      PcmPkt->AGCIndex = index;
      agcParm = &PcmPkt->AGCParms;

      agcParm->targetPowerIn       = sysConfig.agcTargetPower;
      agcParm->maxLossLimitIn      = sysConfig.agcMinGain;
      agcParm->maxGainLimitIn      = sysConfig.agcMaxGain;
      agcParm->lowSigThreshDBM     = sysConfig.agcLowSigThreshDBM;
   } else {
      PktPcm->agcPtr = pAGC;
      PktPcm->AGCIndex = index;
      agcParm = &PktPcm->AGCParms;

      agcParm->targetPowerIn       = sysConfig.agcTargetPowerB;
      agcParm->maxLossLimitIn      = sysConfig.agcMinGainB;
      agcParm->maxGainLimitIn      = sysConfig.agcMaxGainB;
      agcParm->lowSigThreshDBM     = sysConfig.agcLowSigThreshDBMB;
   }
   agcParm->StickyControl       = 0;
   agcParm->StickyInitialGain   = 0;
   agcParm->SamplingRate        = chan->ProcessRate; // TDMRate;
   FLUSH_wait ();
   AGC_ADT_agcinit (pAGC, agcParm);
   FLUSH_INST_CACHE (pAGC, sizeof (AGCInstance_t));
}

// -------------------------------------------------------------------
#pragma CODE_SECTION (DeallocAGC, "SLOW_PROG_SECT")
void DeallocAGC (chanInfo_t *chan) {
   pcm2pkt_t *PcmPkt;
   pkt2pcm_t *PktPcm;

   PcmPkt = &chan->pcmToPkt;
   PktPcm = &chan->pktToPcm;

   PcmPkt->AGC = Disabled;
   PktPcm->AGC = Disabled;

   if (PcmPkt->AGCIndex < sysConfig.numAGCChans) {
      CLEAR_INST_CACHE (AgcInstance[PcmPkt->AGCIndex], sizeof (AGCInstance_t));
      agcChanInUse[PcmPkt->AGCIndex] = 0;
      PcmPkt->AGCIndex = VOID_INDEX;
      numAGCChansAvail++;
   } else {
      if(PcmPkt->AGCIndex != VOID_INDEX)
         AppErr ("AGC Dealloc", PcmPkt->AGCIndex);
   }

   if (PktPcm->AGCIndex < sysConfig.numAGCChans) {
      agcChanInUse[PktPcm->AGCIndex] = 0;
      CLEAR_INST_CACHE (AgcInstance[PktPcm->AGCIndex], sizeof (AGCInstance_t));
      PktPcm->AGCIndex = VOID_INDEX;
      numAGCChansAvail++;
   } else {
      if(PcmPkt->AGCIndex != VOID_INDEX)
         AppErr ("AGC Dealloc", PktPcm->AGCIndex);
   }

}
//}

//{ Tone generation buffers
//===================================================================================
// Tone generation 
//===================================================================================
#pragma CODE_SECTION (AllocToneGen, "SLOW_PROG_SECT")
void AllocToneGen (TGInfo_t *pTGInfo, int tones) {
   ADT_UInt16  index, i;

   index = VOID_INDEX;

   pTGInfo->index = VOID_INDEX;
   pTGInfo->cmd = ToneGenStop;
   pTGInfo->active = Disabled;
   pTGInfo->update = Disabled;
   pTGInfo->toneGenPtr = NULL;
   pTGInfo->toneGenParmPtr = NULL;

   if (!(tones & Tone_Generate))
      return;

   // allocate the next available instance from the pool
   for (i=0; i<sysConfig.numTGChans; i++)  {
      if (toneGenChanInUse[i] != 0) continue;

      toneGenChanInUse[i] = 1;
      numToneGenChansAvail--;
      index = i;
      break;
   }
   if (index == VOID_INDEX) 
      return;
   
   pTGInfo->index = index;           
   pTGInfo->toneGenPtr     = ToneGenInstance[index];
   pTGInfo->toneGenParmPtr = ToneGenParams[index];

   pTGInfo->dtmfDial = Disabled;
   if (SysAlgs.dtmfDialEnable)
      pTGInfo->dtmfDialParmPtr = DtmfDialInstance[index];
   else
      pTGInfo->dtmfDialParmPtr = NULL;

}
#pragma CODE_SECTION (DeallocToneGen, "SLOW_PROG_SECT")
void DeallocToneGen (chanInfo_t *chan) {
   pcm2pkt_t *PcmPkt;
   pkt2pcm_t *PktPcm;
   int index;
   
   PcmPkt = &chan->pcmToPkt;
   PktPcm = &chan->pktToPcm;

   if (PcmPkt->toneTypes & Tone_Generate) {
      index = PcmPkt->TGInfo.index;
      PcmPkt->TGInfo.index = VOID_INDEX;
      
      if (index < sysConfig.numTGChans) {
         toneGenChanInUse[index] = 0;
         numToneGenChansAvail++;
      } else {
         AppErr ("ToneGen Delloc", index);
      }
   }

   if (PktPcm->toneTypes & Tone_Generate) {
      index = PktPcm->TGInfo.index;
      PktPcm->TGInfo.index = VOID_INDEX;
      
      if (index < sysConfig.numTGChans) {
         toneGenChanInUse[index] = 0;
         numToneGenChansAvail++;
      } else {
         AppErr ("ToneGen Delloc", index);
      }
   }
}
//}

//{ Tone detection buffers
//===================================================================================
// Tone detection 
//===================================================================================
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// AllocToneDetectors - Allocate tone detection instances for one side of channel
// AllocArbToneDetectors - Allocate arbitrary tone detection instances for one side of channel
// AllocFAXToneDetect - Allocate CED and CNG detect instance from their pools
#pragma CODE_SECTION (AllocToneDetectors, "SLOW_PROG_SECT")
TDInstance_t *AllocToneDetectors () {
   ADT_UInt16     index, i;
   TDInstance_t  *Inst;
   int  mask;

   mask = FRAME_lock (TONE_DETECT_LOCK);
   index = VOID_INDEX;
   if (ToneDetectorsTotal == VOID_INDEX) 
      ToneDetectorsTotal = ToneDetectorsAvail;
      
   // allocate the next available instance from the pool
   for (i=0; i<ToneDetectorsTotal; i++)  {
      if (ToneDetectorInUse[i] != 0) continue;

      ToneDetectorInUse[i] = 1;
      ToneDetectorsAvail--;
      index = i;
      break;
   }

   if (index == VOID_INDEX) 
      Inst = NULL;
   else
      Inst = ToneDetInstance[index * sysConfig.maxToneDetTypes];

   FRAME_unlock (TONE_DETECT_LOCK, mask);
   return Inst;
}
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma CODE_SECTION (AllocArbToneDetectors, "SLOW_PROG_SECT")
ARBIT_TDInstance_t *AllocArbToneDetectors () {
   ADT_UInt16     index, i;
   ARBIT_TDInstance_t  *Inst;
   int mask;

   mask = FRAME_lock (ARB_DETECT_LOCK);
   index = VOID_INDEX;
   if (ArbToneDetectorsTotal == VOID_INDEX) 
      ArbToneDetectorsTotal = ArbToneDetectorsAvail;
      
   // allocate the next available instance from the pool
   for (i=0; i<ArbToneDetectorsTotal; i++)  {
      if (ArbToneDetectorInUse[i] != 0) continue;

      ArbToneDetectorInUse[i] = 1;
      ArbToneDetectorsAvail--;
      index = i;
      break;
   }

   if (index == VOID_INDEX) 
      Inst = NULL;
   else
      Inst = ArbToneDetInstance[index];

   FRAME_unlock (ARB_DETECT_LOCK, mask);
   return Inst;
}
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma CODE_SECTION (AllocFAXToneDetect, "SLOW_PROG_SECT")
void AllocFAXToneDetect (GpakToneTypes toneTypes, FaxCEDChannel_t **pCedInst,
                         FAXCNGInstance_t **pCngInst) {
   ADT_UInt16 i;
   int mask;

   mask = FRAME_lock (FAX_DETECT_LOCK);
   if (CedDetTotal == VOID_INDEX) {
      CedDetTotal = CedDetAvail;
      CngDetTotal = CngDetAvail;
   }

   *pCedInst = NULL;
   if (toneTypes & CED_tone) {

      // allocate the next available instance from the pool
      for (i=0; i<CedDetTotal; i++)  {
         if (CedDetInUse[i] != 0) continue;

         CedDetInUse[i] = 1;
         CedDetAvail--;
         *pCedInst = faxCedInstance[i];
         break;
      }
   }
   
   *pCngInst = NULL;
   if (toneTypes & CNG_tone) {

      // allocate the next available instance from the pool
      for (i=0; i<CngDetTotal; i++)  {
         if (CngDetInUse[i] != 0) continue;

         CngDetInUse[i] = 1;
         CngDetAvail--;
         *pCngInst = faxCngInstance[i];
         break;
      }
   }
   FRAME_unlock (FAX_DETECT_LOCK, mask);
}

// DeallocToneDetectInstances   - Restore tone detection instances to pool
// DeallocArbToneDetectInstance - Restore arbitrary tone detect instance to pool
// DeallocCngDetectInstance     - Restore CED detect instance to pool
// DeallocCedDetectInstance     - Restore CNG detect instance to pool
// DeallocToneDetectors         - Deallocate all tone detection instances from both sides of channel
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma CODE_SECTION (DeallocToneDetectInstances, "SLOW_PROG_SECT")
static void DeallocToneDetectInstances (TDInstance_t *Inst) {
   int i, mask;

   if (ToneDetectorsTotal == VOID_INDEX) return;
   

   mask = FRAME_lock (TONE_DETECT_LOCK);
   // Find instance and deallocate
   for (i=0; i<ToneDetectorsTotal; i++)  {
      if (ToneDetectorInUse[i] == 0) continue;
      if (ToneDetInstance[i * sysConfig.maxToneDetTypes] != Inst) continue;
      CLEAR_INST_CACHE (Inst, sizeof (TDInstance_t) * sysConfig.maxToneDetTypes);

      ToneDetectorInUse[i] = 0;
      ToneDetectorsAvail++;
      FRAME_unlock (TONE_DETECT_LOCK, mask);   
      return;
   }
   FRAME_unlock (TONE_DETECT_LOCK, mask);
   AppErr ("ToneDet Dealloc", TRUE);
}
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma CODE_SECTION (DeallocArbDetectInstance, "SLOW_PROG_SECT")
static void DeallocArbDetectInstance (ARBIT_TDInstance_t *Inst, ADT_UInt32 CfgID) {
   int i, mask;

   if (ArbToneDetectorsTotal == VOID_INDEX) return;
   if (numArbToneCfgs <= CfgID) return;

   CLEAR_INST_CACHE (ArbToneCfgInst[CfgID], sizeof (ArbitDtInstance_t));
   CLEAR_INST_CACHE (Inst, sizeof (ARBIT_TDInstance_t));
   mask = FRAME_lock (ARB_DETECT_LOCK);

   // Find instance and deallocate
   for (i=0; i<ArbToneDetectorsTotal; i++)  {
      if (ArbToneDetectorInUse[i] == 0) continue;
      if (ArbToneDetInstance[i] != Inst) continue;

      ArbToneDetectorInUse[i] = 0;
      ArbToneDetectorsAvail++;
      FRAME_unlock (ARB_DETECT_LOCK, mask);
      FLUSH_wait ();
      return;
   }
   FRAME_unlock (ARB_DETECT_LOCK, mask);
   AppErr ("ArbToneDet Dealloc", TRUE);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma CODE_SECTION (DeallocCedDetectInstance, "SLOW_PROG_SECT")
static void DeallocCedDetectInstance (FaxCEDChannel_t *pCedInst) {
   int i, mask;

   if (CedDetTotal == VOID_INDEX) return;
   
   mask = FRAME_lock (FAX_DETECT_LOCK);
   // Find instance and deallocate
   for (i=0; i<CedDetTotal; i++)  {
      if (CedDetInUse[i] == 0) continue;
      if (faxCedInstance[i] != pCedInst) continue;
      CedDetInUse[i] = 0;
      CedDetAvail++;
      FRAME_unlock (FAX_DETECT_LOCK, mask);
      return;
   }
   FRAME_unlock (FAX_DETECT_LOCK, mask);
   AppErr ("CEDDet Dealloc", TRUE);
}
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma CODE_SECTION (DeallocCngDetectInstance, "SLOW_PROG_SECT")
static void DeallocCngDetectInstance (FAXCNGInstance_t *pCngInst) {
   int i, mask;

   if (CngDetTotal == VOID_INDEX) return;
   
   // Find instance and deallocate
   mask = FRAME_lock (FAX_DETECT_LOCK);
   for (i=0; i<CngDetTotal; i++)  {
      if (CngDetInUse[i] == 0) continue;
      if (faxCngInstance[i] != pCngInst) continue;
      CngDetInUse[i] = 0;
      CngDetAvail++;
      FRAME_unlock (FAX_DETECT_LOCK, mask);
      return;
   }
   FRAME_unlock (FAX_DETECT_LOCK, mask);
   AppErr ("CngDet Dealloc", TRUE);
}
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#pragma CODE_SECTION (DeallocToneDetectorInstances, "SLOW_PROG_SECT")
void DeallocToneDetectorInstances (TDInst_t *TDInst) {

   if (TDInst->ToneDetInst != NULL) {
      DeallocToneDetectInstances (TDInst->ToneDetInst);
      TDInst->ToneDetInst = NULL;
   }
   if (TDInst->CngDetInst != NULL) {
      DeallocCngDetectInstance (TDInst->CngDetInst);
      TDInst->CngDetInst = NULL;
   }
   if (TDInst->CedDetInst != NULL) {
      DeallocCedDetectInstance (TDInst->CedDetInst);
      TDInst->CedDetInst = NULL;
   }
   if (TDInst->ArbToneInst != NULL) {
      DeallocArbDetectInstance (TDInst->ArbToneInst, TDInst->ArbToneCfgId);
      TDInst->ArbToneInst = NULL;
   }
}
#pragma CODE_SECTION (DeallocASideToneDetectors, "SLOW_PROG_SECT")
void DeallocASideToneDetectors (pcm2pkt_t *PcmPkt) {
   DeallocToneDetectorInstances (&PcmPkt->TDInstances);
   PcmPkt->toneTypes &= ~(ToneDetectAll);
}
#pragma CODE_SECTION (DeallocBSideToneDetectors, "SLOW_PROG_SECT")
void DeallocBSideToneDetectors (pkt2pcm_t *PktPcm) {
   DeallocToneDetectorInstances (&PktPcm->TDInstances);
   PktPcm->toneTypes &= ~(ToneDetectAll);
}
#pragma CODE_SECTION (DeallocToneDetectors, "SLOW_PROG_SECT")
void DeallocToneDetectors (chanInfo_t *chan) {
   DeallocASideToneDetectors (&chan->pcmToPkt);
   DeallocBSideToneDetectors (&chan->pktToPcm);
}

#if (DSP_TYPE == 64)
#define TDInit(func,inst,scratch)  func (inst, scratch); inst->Status.ToneIndex = tdsNoToneDetected;
#else
#define TDInit(func,inst,scratch)  func (inst); inst->Status.ToneIndex = tdsNoToneDetected;
#endif

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// InitToneDetection - Initialize tone detection and relay.
#pragma CODE_SECTION (InitArbToneDetection, "SLOW_PROG_SECT")
void InitArbToneDetection (ARBIT_TDInstance_t **instance, ADT_UInt32 *cfgId) {

   *instance = AllocArbToneDetectors ();
   if (*instance == NULL) return;

   CLEAR_INST_CACHE (ArbToneCfgInst[activeArbToneCfg], sizeof (ArbitDtInstance_t));
   CLEAR_INST_CACHE (*instance, sizeof (ARBIT_TDInstance_t));
   *cfgId  = activeArbToneCfg;
   FLUSH_wait ();

   ARBIT_ADT_init (ArbToneCfgInst[activeArbToneCfg], *instance);

   FLUSH_INST_CACHE (ArbToneCfgInst[activeArbToneCfg], sizeof (ArbitDtInstance_t));
   FLUSH_INST_CACHE (*instance, sizeof (ARBIT_TDInstance_t));
   FLUSH_wait ();
}
   
#pragma CODE_SECTION (InitCedCngToneDetection, "SLOW_PROG_SECT")
void InitCedCngToneDetection (GpakToneTypes toneType,
                              FaxCEDChannel_t **pCedDet,
                             FAXCNGInstance_t **pCngDet) {

   if ((toneType & FAX_tone) == 0) return;

   AllocFAXToneDetect (toneType, pCedDet, pCngDet);
    
   if (*pCedDet != NULL) {
      CLEAR_INST_CACHE (*pCedDet, sizeof (FaxCEDChannel_t));
      FLUSH_wait ();

      FAXCED_ADT_detectInit (*pCedDet);
      FLUSH_INST_CACHE (*pCedDet, sizeof (FaxCEDChannel_t));
   }

   if (*pCngDet != NULL) {
      CLEAR_INST_CACHE (*pCngDet, sizeof (FAXCNGInstance_t));
      FLUSH_wait ();

      FAXCNG_ADT_init (*pCngDet);
      FLUSH_INST_CACHE (*pCngDet, sizeof (FAXCNGInstance_t));
   }
}

// Called by messaging via channel setup
// Called by framing via algorithm control
#pragma CODE_SECTION (InitToneDetection, "SLOW_PROG_SECT")
void InitToneDetection   (GpakToneTypes ToneTypes, TDInst_t *TDInstances,
                          TRDetectInstance_t *pToneRly, int SamplesPerFrame) {

   int ToneTypeCnt;         // count of tone types initialized
   TDInstance_t *pDTMF, *pMFR1, *pMFR2Fwd, *pMFR2Back, *pCPRG;
   TDInstance_t *tdInst;
   TRDetectParamsV701_t toneRelayParams;

   void *pScratch;

   //------------------------------------------------
   // Allocate and initialize FAX tone detectors
   if ((ToneTypes & FaxDetect) != Null_tone) {
      InitCedCngToneDetection (ToneTypes, &TDInstances->CedDetInst, &TDInstances->CngDetInst);
   }

   //------------------------------------------------
   // Allocate and initialize generic tone detectors
   if ((ToneTypes & Arbit_tone) != Null_tone) {
      InitArbToneDetection (&TDInstances->ArbToneInst, &TDInstances->ArbToneCfgId);
   }

   //------------------------------------------------
   // Allocate standard (tone relay supported) tone detectors
   if ((ToneTypes & ToneDetect) == Null_tone) 
      return;

   pScratch = getToneDetScratchPtr (SamplesPerFrame);
   tdInst = AllocToneDetectors ();
   if (tdInst == NULL) return;

   TDInstances->ToneDetInst = tdInst;
   
   ToneTypeCnt = 0;

   CLEAR_INST_CACHE (tdInst, sizeof (TDInstance_t) * sysConfig.maxToneDetTypes);


   // Initialize DTMF if selected.
   if ((ToneTypeCnt < sysConfig.maxToneDetTypes) && ((ToneTypes & DTMF_tone) != 0)) {
      pDTMF = &(tdInst[ToneTypeCnt++]);

      TDInit (DTMF_ADT_initialize, pDTMF, pScratch);
   } else
      pDTMF = NULL;

   // Initialize MFR1 if selected.
   if ((ToneTypeCnt < sysConfig.maxToneDetTypes) && ((ToneTypes & MFR1_tone) != 0))  {
      pMFR1 = &(tdInst[ToneTypeCnt++]);

      TDInit (MFR1_ADT_initialize, pMFR1, pScratch);
   } else
      pMFR1 = NULL;

   // Initialize MFR2 Forward if selected.
   if ((ToneTypeCnt < sysConfig.maxToneDetTypes) && ((ToneTypes & MFR2Fwd_tone) != 0))  {
      pMFR2Fwd = &(tdInst[ToneTypeCnt++]);

      TDInit (MFR2F_ADT_initialize, pMFR2Fwd, pScratch);
   } else
      pMFR2Fwd = NULL;

   // Initialize MFR2 Back if selected.
   if ((ToneTypeCnt < sysConfig.maxToneDetTypes) && ((ToneTypes & MFR2Back_tone) != 0))  {
      pMFR2Back = &(tdInst[ToneTypeCnt++]);
      TDInit (MFR2R_ADT_initialize, pMFR2Back, pScratch);
   } else
      pMFR2Back = NULL;

   // Initialize Call Progress if selected.
   if ((ToneTypeCnt < sysConfig.maxToneDetTypes) && ((ToneTypes & CallProg_tone) != 0)) {
      pCPRG = &(tdInst[ToneTypeCnt++]);
      TDInit (CPRG_ADT_initialize, pCPRG, pScratch);

   } else
      pCPRG = NULL;


   FLUSH_wait ();

   // Initialize Tone Relay if enabled.
   if ((0 < ToneTypeCnt) && ((ToneTypes & Tone_Relay) != 0) && (pToneRly != NULL)) {
      AppErr ("Tone Relay Init", pToneRly == NULL);
      CLEAR_INST_CACHE (pToneRly, sizeof (TRDetectInstance_t));
      
      toneRelayParams.APIVersion = TR_API_VERSION;
      toneRelayParams.TRDetInstanceSizeInBytes = sizeof (TRDetectInstance_t);
      toneRelayParams.pDTMFDetInstance = pDTMF;
      toneRelayParams.pMFR1DetInstance = pMFR1;
      toneRelayParams.pMFR2FDetInstance = pMFR2Fwd;
      toneRelayParams.pMFR2RDetInstance = pMFR2Back;
      toneRelayParams.pCPRGDetInstance = pCPRG;
      toneRelayParams.pSuppressDelayBuffer = NULL;
      toneRelayParams.SuppressDelaySizeInSamples = 0;
      toneRelayParams.pDTMFConfigNominal = NULL;
      toneRelayParams.pDTMFConfigWaiver = NULL;
      
      TR_ADT_detectInit (pToneRly, &toneRelayParams);
      FLUSH_INST_CACHE (pToneRly, sizeof (TRDetectInstance_t));
   }
   if (ToneTypeCnt) {
      FLUSH_INST_CACHE (tdInst, sizeof (TDInstance_t) * ToneTypeCnt);
   }

   return;
}


#pragma CODE_SECTION (reInitASideToneDetect, "MEDIUM_PROG_SECT")
void reInitASideToneDetect (pcm2pkt_t *PcmPkt, int disableTones, int enableTones) {
   GpakToneTypes NewTones;

   // Establish new tone detectors
   NewTones = (PcmPkt->toneTypes & ~disableTones);
   NewTones |= (enableTones & (ToneDetectAll | Notify_Host));
   if (NewTones == Notify_Host) NewTones = Null_tone;

   // Teardown old A-Side detectors
   DeallocASideToneDetectors (PcmPkt);

   //  Allocate and init new A-Side detectors
   if (NewTones != Null_tone) {
      PcmPkt->toneTypes |= NewTones;
      InitToneDetection (PcmPkt->toneTypes,   &PcmPkt->TDInstances,  PcmPkt->toneRelayPtr, PcmPkt->SamplesPerFrame);
   }
}

#pragma CODE_SECTION (reInitBSideToneDetect, "MEDIUM_PROG_SECT")
void reInitBSideToneDetect (pkt2pcm_t *PktPcm, int disableTones, int enableTones) {
   GpakToneTypes NewTones;

   // Establish new tone detectors
   NewTones = (PktPcm->toneTypes & ~disableTones);
   NewTones |= (enableTones & (ToneDetectAll | Notify_Host));
   if (NewTones == Notify_Host) NewTones = Null_tone;

   // Teardown old B-Side detectors
   DeallocBSideToneDetectors (PktPcm);

   //  Allocate and init new B-Side detectors
   if (NewTones != Null_tone) {
      PktPcm->toneTypes |= NewTones;
      InitToneDetection (PktPcm->toneTypes,   &PktPcm->TDInstances, 0, PktPcm->SamplesPerFrame);
   }
}


//}

//{ Caller ID pool management
//===================================================================================
// AllocCID ( )   Allocate all CID instances of a channel
// DeallocCID ( ) deallocate all CID instances of a channel
#pragma CODE_SECTION (AllocCID, "SLOW_PROG_SECT")
void AllocCID (chanInfo_t *chan, GpakDeviceSide_t deviceSide, GpakCidMode_t rxEnab, GpakCidMode_t txEnab) {

   ADT_UInt16 index = VOID_INDEX;
   CIDRX_Inst_t  *pRxCid = NULL;
   CIDTX_Inst_t  *pTxCid = NULL;
   ADT_UInt8     *pRxCidBuf = NULL;
   ADT_UInt8     *pTxCidMsgBuf = NULL;
   ADT_UInt8     *pTxCidBrstBuf = NULL;
   ADT_UInt16 i;
   int FrameSize;

   pcm2pkt_t *PcmPkt;
   pkt2pcm_t *PktPcm;
   cidInfo_t *pInfo;

   PcmPkt = &chan->pcmToPkt;
   PktPcm = &chan->pktToPcm;

   memset (&(PcmPkt->cidInfo), 0, sizeof(cidInfo_t));
   memset (&(PktPcm->cidInfo), 0, sizeof(cidInfo_t));

   if ((rxEnab == CidDisabled) && (txEnab == CidDisabled))
      return;
   // allocate RxCid
   if (rxEnab & CidReceive) {
      for (i=0; i<sysConfig.numRxCidChans; i++) {
         if (rxCidInUse[i] != 0) continue;
         getRxCidPtrs (i, &pRxCid, &pRxCidBuf);
         rxCidInUse[i] = 1;
         numRxCidChansAvail--;
         index = i;
         break;
      }
      AppErr ("Rx CID Alloc", index == VOID_INDEX);

      if (index == VOID_INDEX) return;
      if (sysConfig.type2CID) {
         CLEAR_INST_CACHE (pRxCid, sizeof (CIDRX2_Inst_t));
      } else {
         CLEAR_INST_CACHE (pRxCid, sizeof (CIDRX_Inst_t));
      }
      if (deviceSide == ADevice) {
         pInfo = &(PcmPkt->cidInfo);
         FrameSize = PcmPkt->SamplesPerFrame;
      } else {
         pInfo = &(PktPcm->cidInfo);
         FrameSize = PktPcm->SamplesPerFrame;
      }

      pInfo->pRxCIDInst = pRxCid;
      pInfo->pRxCidBuff = pRxCidBuf;
      pInfo->RxCIDIndex = index;
      FLUSH_wait ();
      rxCidInit (pInfo, FrameSize);
      FLUSH_INST_CACHE (pRxCid, sizeof (CIDRX_Inst_t));
   }

   // allocate TxCid
   if (txEnab & CidTransmit) {
      for (i=0; i<sysConfig.numTxCidChans; i++) {
         if (txCidInUse[i] != 0) continue;
         getTxCidPtrs (i, &pTxCid, &pTxCidMsgBuf, &pTxCidBrstBuf);
         txCidInUse[i] = 1;
         numTxCidChansAvail--;
         index = i;
         break;
      }
      AppErr ("Tx CID Alloc", index == VOID_INDEX);
   
      if (index == VOID_INDEX) return;
        
      if (deviceSide == ADevice)
         pInfo = &(PktPcm->cidInfo);
      else 
         pInfo = &(PcmPkt->cidInfo);

      pInfo->pTxCIDInst = pTxCid;
      pInfo->pTxCidMsgBuff  = pTxCidMsgBuf;
      pInfo->pTxCidBrstBuff = pTxCidBrstBuf;
      pInfo->TxCIDIndex = index;
   }
} 
// -------------------------------------------------------------------
#pragma CODE_SECTION (DeallocCID, "SLOW_PROG_SECT")
void DeallocCID (chanInfo_t *chan) {
   pcm2pkt_t *PcmPkt;
   pkt2pcm_t *PktPcm;

   CIDRX_Inst_t  *pRxCid;
   ADT_UInt8     *pRxCidBuf;

   PcmPkt = &chan->pcmToPkt;
   PktPcm = &chan->pktToPcm;


   // A-side Rx
   if (PcmPkt->CIDMode & CidReceive) {
      if (PcmPkt->cidInfo.RxCIDIndex < sysConfig.numRxCidChans) {
         getRxCidPtrs (PcmPkt->cidInfo.RxCIDIndex, &pRxCid, &pRxCidBuf);
         rxCidInUse[PcmPkt->cidInfo.RxCIDIndex] = 0;
         CLEAR_INST_CACHE (pRxCid, sizeof (CIDRX_Inst_t));
         
         PcmPkt->cidInfo.RxCIDIndex = VOID_INDEX;
         numRxCidChansAvail++;
      } else {
         AppErr ("CID Dealloc", PcmPkt->cidInfo.RxCIDIndex);
      }
   }

   // A-side Tx 
   if (PktPcm->CIDMode & CidTransmit) {
      if (PktPcm->cidInfo.TxCIDIndex < sysConfig.numTxCidChans) {
         txCidInUse[PktPcm->cidInfo.TxCIDIndex] = 0;
         PktPcm->cidInfo.TxCIDIndex = VOID_INDEX;
         numTxCidChansAvail++;
      } else {
         AppErr ("CID Dealloc", PktPcm->cidInfo.TxCIDIndex);
      }
   } 


   // B-side Rx
   if (PktPcm->CIDMode & CidReceive) {
      if (PktPcm->cidInfo.RxCIDIndex < sysConfig.numRxCidChans) {
         getRxCidPtrs (PktPcm->cidInfo.RxCIDIndex, &pRxCid, &pRxCidBuf);
         CLEAR_INST_CACHE (pRxCid, sizeof (CIDRX_Inst_t));
         rxCidInUse[PktPcm->cidInfo.RxCIDIndex] = 0;
         PktPcm->cidInfo.RxCIDIndex = VOID_INDEX;
         numRxCidChansAvail++;
      } else {
         AppErr ("CID Dealloc", PktPcm->cidInfo.RxCIDIndex);
      }
   }

   // B-side Tx
   if (PcmPkt->CIDMode & CidTransmit) {
      if (PcmPkt->cidInfo.TxCIDIndex < sysConfig.numTxCidChans) {
         txCidInUse[PcmPkt->cidInfo.TxCIDIndex] = 0;
         PcmPkt->cidInfo.TxCIDIndex = VOID_INDEX;
         numTxCidChansAvail++;
      } else {
         AppErr ("CID Dealloc", PcmPkt->cidInfo.TxCIDIndex);
      }
   }
}
//}
//}===================================================================================

//===================================================================================
//
//{ Channel teardown
// Channel teardown invalidation of all allocated instance structures located in cached external memory
// This is called from the framing task to allow the memory to be re-allocated to channels running
// on other cores. If the cache is not cleared, then a future cache line eviction may cause memory corruption
#pragma CODE_SECTION (invalidateChannelCache, "SLOW_PROG_SECT")
void invalidateChannelCache (chanInfo_t *chan) {
   ADT_UInt16 EcanType, EcanIdx, EcanID;
   
   CIDRX_Inst_t  *pRxCid;
   ADT_UInt8     *pRxCidBuf;

   CIDTX_Inst_t  *pTxCid;
   ADT_UInt8     *pTxCidBuf;
   ADT_UInt8     *pTxCidBurst;

   pcm2pkt_t *PcmPkt;
   pkt2pcm_t *PktPcm;

   PcmPkt = &chan->pcmToPkt;
   PktPcm = &chan->pktToPcm;

   if (chan->customData) {
      customInvalidateChannelCache  (chan);
   }

   // Vocoders - One time alloc
   CLEAR_INST_CACHE (PcmPkt->EncodePtr, encoderInstI16 * sizeof (ADT_UInt16));
   CLEAR_INST_CACHE (PktPcm->DecodePtr, decoderInstI16 * sizeof (ADT_UInt16));

   // Echo canceller instance memory.
   if (PcmPkt->EchoCancel == Enabled) {
      EcanID   = PcmPkt->EcIndex;
      EcanType = (EcanID >> 8) & 0xFF;
      EcanIdx  =  EcanID & 0xFF;
      invalidateEchoCanceller (EcanType, EcanIdx);
   }
   if (PktPcm->EchoCancel == Enabled) {
      EcanID   = PktPcm->EcIndex;
      EcanType = (EcanID >> 8) & 0xFF;
      EcanIdx  =  EcanID & 0xFF;
      invalidateEchoCanceller (EcanType, EcanIdx);
   }

   // Echo canceller bulk delay buffer
   if (PcmPkt->ecBulkDelay.BufrSize) {
      CLEAR_INST_CACHE (PcmPkt->ecBulkDelay.pBufrBase, PcmPkt->ecBulkDelay.BufrSize * 2);
   }

   // Noise suppression - One time alloc
   if (PcmPkt->Noise.Bufr) {
      CLEAR_INST_CACHE (PcmPkt->Noise.Bufr, sizeof (NCAN_Channel_t));
   }
   if (PktPcm->Noise.Bufr) {
      CLEAR_INST_CACHE (PktPcm->Noise.Bufr, sizeof (NCAN_Channel_t));
   }

   // CID instance memory.
   if ((PcmPkt->CIDMode & CidReceive) && (PcmPkt->cidInfo.RxCIDIndex < sysConfig.numRxCidChans)) {
      getRxCidPtrs (PcmPkt->cidInfo.RxCIDIndex, &pRxCid, &pRxCidBuf);
      if (sysConfig.type2CID) {
         CLEAR_INST_CACHE (pRxCid,    sizeof (CIDRX2_Inst_t));
      } else {
         CLEAR_INST_CACHE (pRxCid,    sizeof (CIDRX_Inst_t));
      }
      CLEAR_INST_CACHE (pRxCidBuf, rxCidBufferI8);
   }
   if ((PktPcm->CIDMode & CidReceive) && (PktPcm->cidInfo.RxCIDIndex < sysConfig.numRxCidChans)) {
      getRxCidPtrs (PktPcm->cidInfo.RxCIDIndex, &pRxCid, &pRxCidBuf);
      if (sysConfig.type2CID) {
         CLEAR_INST_CACHE (pRxCid,    sizeof (CIDRX2_Inst_t));
      } else {
         CLEAR_INST_CACHE (pRxCid, sizeof (CIDRX_Inst_t));
      }
      CLEAR_INST_CACHE (pRxCidBuf, rxCidBufferI8);
   }
   if ((PcmPkt->CIDMode & CidTransmit) && (PcmPkt->cidInfo.TxCIDIndex < sysConfig.numTxCidChans)) {
      getTxCidPtrs (PcmPkt->cidInfo.TxCIDIndex, &pTxCid, &pTxCidBuf, &pTxCidBurst);
      if(sysConfig.type2CID) {
         CLEAR_INST_CACHE (pTxCid, sizeof (CIDTX2_Inst_t));
      } else {
         CLEAR_INST_CACHE (pTxCid, sizeof (CIDTX_Inst_t));
      }
      CLEAR_INST_CACHE (pTxCidBuf,   txCidBufferI8);
      CLEAR_INST_CACHE (pTxCidBurst, txCidBurstI8);
   }
   if ((PktPcm->CIDMode & CidTransmit) && (PktPcm->cidInfo.TxCIDIndex < sysConfig.numTxCidChans)) {
      getTxCidPtrs (PktPcm->cidInfo.TxCIDIndex, &pTxCid, &pTxCidBuf, &pTxCidBurst);
      if (sysConfig.type2CID) {
         CLEAR_INST_CACHE (pTxCid, sizeof (CIDTX2_Inst_t));
      } else {
         CLEAR_INST_CACHE (pTxCid, sizeof (CIDTX_Inst_t));
       }
      CLEAR_INST_CACHE (pTxCidBuf,   txCidBufferI8);
      CLEAR_INST_CACHE (pTxCidBurst, txCidBurstI8);
   }

   // Tone detect instance memory.
   if (PcmPkt->TDInstances.ToneDetInst != NULL) {
      CLEAR_INST_CACHE (PcmPkt->TDInstances.ToneDetInst, sizeof (TDInstance_t) * sysConfig.maxToneDetTypes);
   }
   if (PktPcm->TDInstances.ToneDetInst != NULL) {
      CLEAR_INST_CACHE (PktPcm->TDInstances.ToneDetInst, sizeof (TDInstance_t) * sysConfig.maxToneDetTypes);
   }

   // Tone generation
   if (PcmPkt->TGInfo.toneGenPtr != NULL) {
      CLEAR_INST_CACHE (PcmPkt->TGInfo.toneGenPtr, sizeof (TGInstance_t));
   }
   if (PktPcm->TGInfo.toneGenPtr != NULL) {
      CLEAR_INST_CACHE (PktPcm->TGInfo.toneGenPtr, sizeof (TGInstance_t));
   }

   // Arbitrary tone detect instance memory.
   if (PcmPkt->TDInstances.ArbToneInst != NULL) {
      CLEAR_INST_CACHE (PcmPkt->TDInstances.ArbToneInst, sizeof (ARBIT_TDInstance_t));
   }
   if (PktPcm->TDInstances.ArbToneInst != NULL) {
      CLEAR_INST_CACHE (PktPcm->TDInstances.ArbToneInst, sizeof (ARBIT_TDInstance_t));
   }

   // CED / CNG tone detection
   if (PcmPkt->TDInstances.CedDetInst != NULL) {
      CLEAR_INST_CACHE (PcmPkt->TDInstances.CedDetInst, sizeof (FaxCEDChannel_t));
   }
   if (PcmPkt->TDInstances.CngDetInst != NULL) {
      CLEAR_INST_CACHE (PcmPkt->TDInstances.CngDetInst, sizeof (FAXCNGInstance_t));
   }

   if (PktPcm->TDInstances.CedDetInst != NULL) {
      CLEAR_INST_CACHE (PktPcm->TDInstances.CedDetInst, sizeof (FaxCEDChannel_t));
   }
   if (PktPcm->TDInstances.CngDetInst != NULL) {
      CLEAR_INST_CACHE (PktPcm->TDInstances.CngDetInst, sizeof (FAXCNGInstance_t));
   }

   // Tone relay
   if (PktPcm->TGInfo.toneRlyGenPtr) {
      CLEAR_INST_CACHE (PktPcm->TGInfo.toneRlyGenPtr, sizeof (TRGenerateInstance_t));
      CLEAR_INST_CACHE (PktPcm->TGInfo.toneRlyEvt,    sizeof (TRGenerateEvent_t)*numPastToneRlyGenEvents); // jdc
   }
   if (PcmPkt->toneRelayPtr) {
      CLEAR_INST_CACHE (PcmPkt->toneRelayPtr, sizeof (TRDetectInstance_t));
   }

   // t.38 instance memory.
   invalidateFaxRelay (chan);

   // RTP / SRTP
   if (sysConfig.maxJitterms != 0) {
      CLEAR_INST_CACHE (RtpChanData[chan->ChannelId], sizeof (RtpChanData_t));
#ifdef RTCP_ENABLED
      CLEAR_INST_CACHE (rtcpData[chan->ChannelId], sizeof (rtcpdata_t));
#endif
   }

   if (sysConfig.SRTP_Enable) {
      // Mark SRTP instances inactive.
      if (SrtpTxKeyData[chan->ChannelId]->Configured) {
         SrtpTxKeyData[chan->ChannelId]->Configured = ADT_FALSE;
         FLUSH_INST_CACHE (SrtpTxKeyData[chan->ChannelId],  sizeof (GpakSrtpKeyInfo_t));
         CLEAR_INST_CACHE (SrtpTxChanData[chan->ChannelId], srtpTxI8);
      }
      if (SrtpRxKeyData[chan->ChannelId]->Configured) {
         SrtpRxKeyData[chan->ChannelId]->Configured = ADT_FALSE;
         FLUSH_INST_CACHE (SrtpRxKeyData[chan->ChannelId],  sizeof (GpakSrtpKeyInfo_t));
         CLEAR_INST_CACHE (SrtpRxChanData[chan->ChannelId], srtpRxI8);
      }
   }

   // AGC instance memory.
   if ((PcmPkt->AGC == Enabled) && (PcmPkt->AGCIndex < sysConfig.numAGCChans)) {
      CLEAR_INST_CACHE (AgcInstance[PcmPkt->AGCIndex], sizeof (AGCInstance_t));
   }
   if ((PktPcm->AGC == Enabled) && (PktPcm->AGCIndex < sysConfig.numAGCChans)) {
      CLEAR_INST_CACHE (AgcInstance[PktPcm->AGCIndex], sizeof (AGCInstance_t));
   }

   // VAD
   if (PcmPkt->vadPtr) {
      CLEAR_INST_CACHE (PcmPkt->vadPtr, sizeof (VADCNG_Instance_t));
   }
   if (PktPcm->vadPtr) {
      CLEAR_INST_CACHE (PktPcm->vadPtr, sizeof (VADCNG_Instance_t));
   }

   FLUSH_wait ();
}

#pragma CODE_SECTION (tearDownChannel, "SLOW_PROG_SECT")
void tearDownChannel (chanInfo_t *chan) {
   // Tears down instance memory allocated by channel type setup routines
   // Called by scheduling swi after channel has been marked for removal from framing task queues.
   chan->Flags |= (CF_RTP_TX_DISABLED | CF_RTP_RX_DISABLED);
   DeallocEcans    (chan);
   DeallocAGC      (chan);
   DeallocToneGen  (chan);
   DeallocToneDetectors (chan);
   DeallocFaxRelay (chan);
   DeallocCID (chan);

#if RTDX
   disableLoopBack (chan);
#endif
   DeAllocBuffs (chan);
   DeallocEncoderSampleRateConv (chan);
   DeallocDecoderSampleRateConv (chan);
   if (chan->customData)
      customTeardown (chan);
   chan->G722RtpSamplingRate  = 0; // jdc added for g.722 trilogy: experimental
   teardownLog(chan->ChannelId, 8);

}
//}

//=============================================================================
//
//{  Adjust circular buffer pointer to current DMA phase
//
//=============================================================================

#ifndef LogBuff
#pragma CODE_SECTION (LogBuff, "SLOW_PROG_SECT")
void LogBuff (int flag, CircBufInfo_t *Buff) {
   if (Buff == NULL) return;
   if (Buff->pBufrBase == NULL) return;
   LogTransfer (flag, Buff, Buff->pBufrBase, getFreeSpace (Buff), getAvailable (Buff), Buff->BufrSize);
}
#endif

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// SetCircPointers
//
// FUNCTION
//   This function sets the put and take pointers of the channel's circular buffers.
//
//   Three buffers inBuff, outBuff, and ecBuff are identified as either PCM or PKT 
//   buffers.  The put and take index of each buffer is calculated according to the
//   buffer type, current phases (near and far) and frame sizes (near and far).
//
// INPUTS   
//    Near   - Near-end buffers and framing information
//    Far    - Far-end buffers and framing information
//
//  OUTPUTS
//     Take and Put pointers for all of the channel's circular buffers
//}
#pragma CODE_SECTION (preFillWithSilence, "SLOW_PROG_SECT")
void preFillWithSilence (CircBufInfo_t *circ, GpakCompandModes compand) {
   ADT_UInt16* buff;
   ADT_UInt16  silence;
   ADT_Int16   lenI16;

   if      (compand == cmpL8PcmU) silence = 0xFF;
   else if (compand == cmpL8PcmA) silence = 0x55;
   else                           silence = 0;

   lenI16 = circ->PutIndex - circ->TakeIndex;
   buff = circ->pBufrBase + circ->TakeIndex;
   while (0 < lenI16--) *buff++ = silence;
}

// Set pcm pointers NEAR device is PCM, FAR device is PKT
#pragma CODE_SECTION (setPcmPktPointers, "SLOW_PROG_SECT")
void setPcmPktPointers (PhaseData *Near, PhaseData *Far) {

   CircBufInfo_t *PcmIn, *PcmOut, *Ec;
   ADT_Int16      ECTake, frameAdjust;

   // Establish take and put pointers for DMA (PcmIn and PcmOut) transfers
   PcmIn = Near->inBuff;
   if (PcmIn != NULL) {
      PcmIn->PutIndex  = Near->Phase + (Near->sampsPerFrame % sysConfig.samplesPerMs);
      PcmIn->TakeIndex = 0;
      preFillWithSilence (PcmIn, Near->rxCompand);
   }
   LogBuff (0x8A100u, PcmIn);  

   PcmOut = Near->outBuff;
   if (PcmOut != NULL) {
      if (Near->sampsPerFrame != Far->sampsPerFrame) frameAdjust = sysConfig.samplesPerMs;
      else                                           frameAdjust = 0;
      PcmOut->PutIndex  = Far->sampsPerFrame * 2 + (Far->sampsPerFrame % sysConfig.samplesPerMs) + frameAdjust;
      PcmOut->TakeIndex = Far->Phase;
      preFillWithSilence (PcmOut, Near->txCompand);    
   }
   LogBuff (0x8A000u, PcmOut);

   // Establish take and put pointers for echo cancellation
   //  NOTE: ecBuff is 'take only' when outbuffer is used both for output and
   //        echo cancellation.  The put index is never used, but is set to
   //        fool the system into believing data is always available to satisfy
   //        debug over/under run checking.
   Ec = Near->ecBuff;
   if (Ec != NULL) {
      Ec->PutIndex = 2 * Ec->BufrSize;   // SEE NOTE
      if (Far->sampsPerFrame == Near->sampsPerFrame) {
         Ec->TakeIndex = Far->sampsPerFrame * 2;
      } else {
         ECTake = Far->Phase - PcmIn->PutIndex;
         Ec->TakeIndex = ECTake;
         if (ECTake < 0) {
            Ec->TakeIndex += Ec->BufrSize;
         }
      }
   }
   LogBuff (0x8AE00u, Ec);
}

// Set pkt pointers NEAR device is PKT, FAR device is PCM
#pragma CODE_SECTION (setPktPcmPointers, "SLOW_PROG_SECT")
void setPktPcmPointers (PhaseData *Near, PhaseData *Far) {

   CircBufInfo_t *PktOut, *PktIn, *Ec;

   // Establish take and put pointers for host (PktIn and PktOut) transfers
   PktIn = Near->inBuff;
   if (PktIn != NULL) {
      PktIn->TakeIndex  = PktIn->PutIndex;   
   }
   LogBuff (0x8B100u, PktIn);  

   PktOut = Near->outBuff;
   if (PktOut != NULL) {
      PktOut->PutIndex  = PktOut->TakeIndex;
   }
   LogBuff (0x8B000u, PktOut);

   // Establish take and put pointers for echo cancellation (bulk delay buffer)
   Ec = Near->ecBuff;
   if (Ec != NULL) {
      Ec->TakeIndex = 0;
      Ec->PutIndex = Near->sampsPerFrame + Far->Phase - Near->Phase + sysConfig.samplesPerMs;
   }
   LogBuff (0x8BE00u, Ec);
}

#pragma CODE_SECTION (setDevicePointers, "SLOW_PROG_SECT")
void setDevicePointers (BufferStyle BuffStyle, PhaseData *Near, PhaseData *Far) {

   ADT_Int16 MaxFrame, FarInFrameStart, FarOutFrameStart, CurrentPosition, ECTake;
   CircBufInfo_t *NearOut, *NearIn, *Ec;

   if (Near->sampsPerFrame < Far->sampsPerFrame) MaxFrame = Far->sampsPerFrame;
   else                                          MaxFrame = Near->sampsPerFrame;

   // Establish current position for PCM output and Echo cancel buffers
   FarInFrameStart  = 2 * MaxFrame;
   FarOutFrameStart = FarInFrameStart - (2 * Far->sampsPerFrame);
   CurrentPosition  = FarOutFrameStart + Far->Phase;

   // Establish take and put pointers for DMA (Pcm) or host (Pkt) transfers
   NearIn = Near->inBuff;
   if (NearIn != NULL) {
      if (BuffStyle == PcmBuffers) {
         NearIn->PutIndex   = Near->Phase + Near->rxDelay;
         NearIn->TakeIndex  = 0;
         preFillWithSilence (NearIn, Near->rxCompand);
      } else {
         NearIn->TakeIndex  = NearIn->PutIndex;   
      }
   }
   AppErr ("Rx delay setting", (NearIn->BufrSize < NearIn->PutIndex));

   NearOut = Near->outBuff;
   if (NearOut != NULL) {
      if (BuffStyle == PcmBuffers) {
         NearOut->PutIndex  = FarInFrameStart + Near->txDelay;
         NearOut->TakeIndex = CurrentPosition;
         preFillWithSilence (NearOut, Near->txCompand);
      } else {
         NearOut->PutIndex  = NearOut->TakeIndex;
      }
   }
   AppErr ("Tx delay setting", (NearOut->BufrSize < NearOut->PutIndex));

   // Establish take and put pointers for echo cancellation
   //  NOTE: ecBuff is 'take only' when outbuffer is used both for output and
   //        echo cancellation.  The put index is never used, but is set to
   //        fool the system into believing data is always available to satisfy
   //        debug over/under run checking.
   Ec = Near->ecBuff;
   if (Ec != NULL) {
      ECTake = CurrentPosition - Near->Phase;
      Ec->TakeIndex = ECTake;
      if (NearOut->pBufrBase != Ec->pBufrBase)
         Ec->PutIndex = FarInFrameStart;
      else
         Ec->PutIndex = 2 * NearOut->BufrSize;   // SEE NOTE

      if (ECTake < 0) {
         Ec->PutIndex  += MaxFrame;
         Ec->TakeIndex += MaxFrame;
         if ((NearOut->pBufrBase == Ec->pBufrBase) &&
             (BuffStyle == PcmBuffers)) {
            NearOut->PutIndex  += MaxFrame;
            NearOut->TakeIndex += MaxFrame;
         }
      }
   }
   LogBuff (0x2A100u, NearIn);  
   LogBuff (0x2A000u, NearOut);
   LogBuff (0x2AE00u, Ec);
}
//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// SetCircPointersPktPkt
//
// FUNCTION
//   This function sets the put and take pointers of a pktpkt channel's circular buffers.
//
// INPUTS   
//    EncodeA, DecodeA - X channel's structure pointers
//    EncodeB, DecodeB - Y channel's structure pointers
//    EncodeAPhase, DecodeAPhase - X channel's A and B side phase
//    EncodeBPhase, DecodeBPhase - Y channel's A and B side phase
//
//  OUTPUTS
//     Take and Put pointers for all of the channel's circular buffers
//}

#pragma CODE_SECTION (setCircPointersPktPkt, "SLOW_PROG_SECT")
void setCircPointersPktPkt (chanInfo_t *chan,
          pcm2pkt_t *EncodeA,      pkt2pcm_t *DecodeA,         // A channel buffers
          pcm2pkt_t *EncodeB,      pkt2pcm_t *DecodeB,         // B channel buffers
          ADT_UInt16 EncodeAPhase, ADT_UInt16 DecodeAPhase,    // A channel phases
          ADT_UInt16 EncodeBPhase, ADT_UInt16 DecodeBPhase) {  // B channel phases

   ADT_UInt16 APut, BPut, EcAPut, EcBPut;
   // The Pkt-Pkt Channel data flow through the circular buffers is:
   //
   //
   //                    Channel A                                  |          Channel B
   //               ................. Dec A ......................     ....................... Enc B ....................
   // INPKT  A ---> DecodeA->inbuffer  ---> DecodeA->outbuffer      |  EncodeB->activeInBuffer ---> EncodeB->outbuffer ---> OUTPKT B
   //                                                               |
   //                                                               |
   // OUTPKT A <--- EncodeA->outbuffer <--- EncodeA->activeInBuffer |  DecodeB->outbuffer      <--- DecodeB->inbuffer  <--- INPKT  B
   //               ................. Enc A ......................     ....................... Dec B ....................
   //                    Channel A                                  |          Channel B

   // Empty packet buffers.
   DecodeA->inbuffer->TakeIndex = DecodeA->inbuffer->PutIndex;
   DecodeB->inbuffer->TakeIndex = DecodeB->inbuffer->PutIndex;

   EncodeA->outbuffer->PutIndex = EncodeA->outbuffer->TakeIndex;
   EncodeB->outbuffer->PutIndex = EncodeB->outbuffer->TakeIndex;


   // Set put and take positions (adjusted to process rate) in PCM holding buffers. 
   APut = EncodeBPhase - DecodeAPhase;
   if (DecodeA->SamplesPerFrame != EncodeB->SamplesPerFrame) 
      APut += DecodeA->SamplesPerFrame;

   BPut = EncodeAPhase - DecodeBPhase;
   if (DecodeB->SamplesPerFrame != EncodeA->SamplesPerFrame) 
      BPut += DecodeB->SamplesPerFrame;

   EcBPut = EncodeA->SamplesPerFrame + EncodeBPhase - EncodeAPhase;
   if (EncodeA->SamplesPerFrame != EncodeB->SamplesPerFrame)
      EcBPut += EncodeA->SamplesPerFrame;

   EcAPut = EncodeB->SamplesPerFrame + EncodeAPhase - EncodeBPhase;
   if (EncodeB->SamplesPerFrame != EncodeA->SamplesPerFrame)
      EcAPut += EncodeB->SamplesPerFrame;

   if (TDMRate < chan->ProcessRate) {
      APut   *= (chan->ProcessRate / TDMRate);
      BPut   *= (chan->ProcessRate / TDMRate);
      EcAPut *= (chan->ProcessRate / TDMRate);
      EcBPut *= (chan->ProcessRate / TDMRate);
   } else if (chan->ProcessRate < TDMRate) {
      APut   /= (TDMRate / chan->ProcessRate);
      BPut   /= (TDMRate / chan->ProcessRate);
      EcAPut /= (TDMRate / chan->ProcessRate);
      EcBPut /= (TDMRate / chan->ProcessRate);
   }

   DecodeA->outbuffer.TakeIndex = 0;
   DecodeA->outbuffer.PutIndex = APut + sysConfig.samplesPerMs;

   DecodeB->outbuffer.TakeIndex = 0;
   DecodeB->outbuffer.PutIndex = BPut + sysConfig.samplesPerMs;

   if (DecodeA->EchoCancel == Enabled) {
      DecodeA->ecFarPtr.TakeIndex = 0;
      DecodeA->ecFarPtr.PutIndex  = EcAPut + sysConfig.samplesPerMs;
   }
   if (DecodeB->EchoCancel == Enabled) {
      DecodeB->ecFarPtr.TakeIndex = 0;
      DecodeB->ecFarPtr.PutIndex  = EcBPut + sysConfig.samplesPerMs;
   }

   LogBuff (0x8D000u, &DecodeA->outbuffer);  
   LogBuff (0x8D100u, &DecodeB->outbuffer);
   LogBuff (0x8D200u, &DecodeA->ecFarPtr);  
   LogBuff (0x8D300u, &DecodeB->ecFarPtr);

#ifdef _DEBUG
   phaseInfo[phaseIdx].CnfrSize  = DecodeA->SamplesPerFrame;
   phaseInfo[phaseIdx].CnfrPhase = DecodeAPhase;
   phaseInfo[phaseIdx].CnfrPhaseB= EncodeBPhase;
   phaseInfo[phaseIdx].CnfrPut   = DecodeA->outbuffer.PutIndex;
   phaseInfo[phaseIdx].PktSize   = DecodeB->SamplesPerFrame;
   phaseInfo[phaseIdx].PktPhase  = DecodeBPhase;
   phaseInfo[phaseIdx].PktPhaseB = EncodeAPhase;
   phaseInfo[phaseIdx].PktPut    = DecodeB->outbuffer.PutIndex;
   phaseIdx++;
   phaseIdx &=0x3f;   
#endif
}

#pragma CODE_SECTION (setCircPointersCnfrMultiRate, "SLOW_PROG_SECT")
static void setCircPointersCnfrMultiRate (chanInfo_t *chan, PhaseData *CnfrSide, PhaseData *PktSide) {

   ADT_UInt16 inIdx, outIdx, ecIdx;

   AppErr ("Phase", ( PktSide->Phase < 8) || ( PktSide->sampsPerFrame + 8) <=  PktSide->Phase);
   AppErr ("Phase", (CnfrSide->Phase < 8) || (CnfrSide->sampsPerFrame + 8) <= CnfrSide->Phase);

   // Empty conference's packet buffers.
   PktSide->outBuff->TakeIndex = PktSide->outBuff->PutIndex;
   PktSide->inBuff->PutIndex   = PktSide->inBuff->TakeIndex;

   // Set put and take positions (adjusted to process rate) in PCM holding buffers and ec bulk buffers
   ecIdx  = CnfrSide->outBuff->PutIndex - CnfrSide->sampsPerFrame;
   inIdx  = PktSide->sampsPerFrame - PktSide->Phase + CnfrSide->Phase;
   outIdx = CnfrSide->sampsPerFrame - CnfrSide->Phase + PktSide->Phase;
   if (TDMRate < chan->ProcessRate) {
      ecIdx  *= (chan->ProcessRate / TDMRate);
      inIdx  *= (chan->ProcessRate / TDMRate);
      outIdx *= (chan->ProcessRate / TDMRate);
   } else if (chan->ProcessRate < TDMRate) {
      ecIdx  /= (TDMRate / chan->ProcessRate);
      inIdx  /= (TDMRate / chan->ProcessRate);
      outIdx /= (TDMRate / chan->ProcessRate);
   }

   CnfrSide->inBuff->TakeIndex  = 0;
   CnfrSide->inBuff->PutIndex   = inIdx + sysConfig.samplesPerMs;
   memset (CnfrSide->inBuff->pBufrBase, 0, CnfrSide->inBuff->PutIndex*sizeof(short int));
   
   CnfrSide->outBuff->TakeIndex = 0;
   CnfrSide->outBuff->PutIndex  = outIdx + sysConfig.samplesPerMs;
   memset (CnfrSide->outBuff->pBufrBase, 0, CnfrSide->outBuff->PutIndex*sizeof(short int));
   
   CnfrSide->ecBuff->PutIndex = 2 * CnfrSide->ecBuff->BufrSize;
   CnfrSide->ecBuff->TakeIndex = ecIdx;
   if (CnfrSide->ecBuff->BufrSize < CnfrSide->ecBuff->TakeIndex) 
      CnfrSide->ecBuff->TakeIndex += CnfrSide->ecBuff->BufrSize;

   LogBuff (0x8C100u, CnfrSide->outBuff);  
   LogBuff (0x8C000u, CnfrSide->inBuff);
   LogBuff (0x8CE00u, CnfrSide->ecBuff);

#ifdef _DEBUG      
   phaseInfo[phaseIdx].CnfrSize  = PktSide->sampsPerFrame;
   phaseInfo[phaseIdx].CnfrPhase = PktSide->Phase;
   phaseInfo[phaseIdx].CnfrPhaseB= CnfrSide->Phase;
   phaseInfo[phaseIdx].CnfrPut   = CnfrSide->inBuff->PutIndex;

   phaseInfo[phaseIdx].PktSize   = CnfrSide->sampsPerFrame;
   phaseInfo[phaseIdx].PktPhase  = CnfrSide->Phase;
   phaseInfo[phaseIdx].PktPhaseB = PktSide->Phase;
   phaseInfo[phaseIdx].PktPut    = CnfrSide->outBuff->PutIndex;
   phaseIdx++;
   phaseIdx &=0x3f;   
#endif

}

#pragma CODE_SECTION (setCircPointers, "SLOW_PROG_SECT")
void setCircPointers (chanInfo_t *chan,
                      pcm2pkt_t *PcmPkt,  pkt2pcm_t *PktPcm, 
                      ADT_UInt16 APhase,  ADT_UInt16 BPhase) {
   PhaseData DevA, DevB;
   ADT_UInt16 rxDelay, txDelay;
   ConferenceInfo_t *gpakCnfr;

   DevA.Phase   = APhase;
   DevA.sampsPerFrame = PcmPkt->SamplesPerFrame;
   DevA.inBuff  = PcmPkt->activeInBuffer;
   DevA.outBuff = &PktPcm->outbuffer;
   DevA.rxDelay = 0;
   DevA.txDelay = 0;
   DevA.txCompand = PktPcm->OutCompandingMode;
   DevA.rxCompand = PcmPkt->InCompandingMode;

   // aecg4: Skip ecFar circ buff ptr setup for aec.
   // The aec maintains bulk delay internally
   if (PcmPkt->AECEchoCancel == Enabled) 
        DevA.ecBuff = NULL;
   else
        DevA.ecBuff  = &PcmPkt->ecFarPtr;

   DevB.Phase   = BPhase;
   DevB.sampsPerFrame = PktPcm->SamplesPerFrame;
   DevB.inBuff  = PktPcm->inbuffer;
   DevB.outBuff = PcmPkt->outbuffer;
   DevB.ecBuff  = &PktPcm->ecFarPtr;
   DevB.rxDelay = 0;
   DevB.txDelay = 0;
   DevB.txCompand = PcmPkt->OutCompandingMode;
   DevB.rxCompand = PktPcm->InCompandingMode;

   switch (chan->channelType) {
   case customChannel:
      customSetDevicePointers (chan, PcmPkt, PktPcm, APhase, BPhase);
      break;

   case packetToPacket:
      break;

   default: // conferencePCM
      setDevicePointers (PcmBuffers, &DevA, &DevB);
      setDevicePointers (PktBuffers, &DevB, &DevA);
      break;

   case pcmToPacket:
      setPcmPktPointers (&DevA, &DevB);
      setPktPcmPointers (&DevB, &DevA);
      break;

   case circuitData:
      DevA.Phase *= PcmPkt->OutSlotId_MuxFact;
      DevA.sampsPerFrame  *= PcmPkt->OutSlotId_MuxFact;
      DevB.Phase *= PktPcm->InSlotId_MuxFact;
      DevB.sampsPerFrame  *= PktPcm->InSlotId_MuxFact;
      // Intentional fall-through

   case pcmToPcm:
      setDevicePointers (PcmBuffers, &DevA, &DevB);

      // Any additional delay is applied to device B (packet buffers) by design.
      rxDelay = RxDelayMs * sysConfig.samplesPerMs;
      txDelay = TxDelayMs * sysConfig.samplesPerMs;
      // subtract out the built-in buffering delay from the additional delay
      if (DevB.sampsPerFrame <= rxDelay) rxDelay -= DevB.sampsPerFrame;
      if (DevB.sampsPerFrame <= txDelay) txDelay -= DevB.sampsPerFrame;
      DevB.rxDelay = rxDelay;
      DevB.txDelay = txDelay;
      setDevicePointers (PcmBuffers, &DevB, &DevA);
      break;

   case conferencePacket:
      DevB.ecBuff = &PcmPkt->ecFarPtr;
      setDevicePointers (PktBuffers, &DevB, &DevA);
      break;

   case conferenceMultiRate:
      gpakCnfr = GetGpakCnfr (chan->CoreID, chan->PairedChannelId);
      DevA.sampsPerFrame  = gpakCnfr->FrameSize;
      DevA.Phase = FrameRatePhaseCount (chan->CoreID, DevA.sampsPerFrame);
      setCircPointersCnfrMultiRate (chan, &DevA, &DevB);
      break;
   }
   return;
}
//}===================================================================================

