/*
 * Copyright (c) 2008, Adaptive Digital Technologies, Inc.
 *
 * File Name: 64plusMcASP.c
 *
 * Description:
 *   This file contains G.PAK McASP I/O functions.
 *
 * Version: 1.0
 *
 * Revision History:
 *   6/08 - Initial release.
 *
 *
 * NOTE: 
 *    This version only supports a single, two timeslot McASP.
 */
   
// Application related header files.
typedef int chanInfo_t;   // Statisfy header files

#include "sysconfig.h"
#include "64plus.h"
#include "GpakPcm.h"
extern ADT_UInt16       *pSlotMap[];      // pointers to the DMA SlotMap buffers

//-------------------------------------------------------------------
//  McASP register addresses
//-------------------------------------------------------------------
//  Global control registier
#define PID_OFFSET         0x000 // Peripheral Identification register [Register value: 0x0010 0101]
#define PFUNC_OFFSET       0x010 // Pin function register
#define PDIR_OFFSET        0x014 // Pin direction register
#define GBLCTL_OFFSET      0x044 // Global control register
#define AMUTE_OFFSET       0x048 // Mute control register
#define DLBCTL_OFFSET      0x04C // Digital Loop-back control register
#define DITCTL_OFFSET      0x050 // DIT mode control register

// Receiver control registers
#define RGBLCTL_OFFSET     0x060 //RGBLCTL Alias of GBLCTL containing only Receiver Reset bits, allows transmit to be reset
                                           //independently from receive.
#define RMASK_OFFSET       0x064 // Format unit bit mask register
#define RFMT_OFFSET        0x068 // Bit stream format register
#define RAFSCTL_OFFSET     0x06C // Frame sync control register
#define RACLKCTL_OFFSET    0x070 // Clock control register
#define RAHCLKCTL_OFFSET   0x074 // High-frequency clock control register
#define RTDM_OFFSET        0x078 // TDM slot
#define RINTCTL_OFFSET     0x07C // Interrupt control register
#define RSTAT_OFFSET       0x080 // Status register
#define RSLOT_OFFSET       0x084 // Current TDM slot register
#define RCLKCHK_OFFSET     0x088 // Clock check control register
#define REVTCTL_OFFSET     0x08C // DMA control register

// Transmitter control registers
#define XGBLCTL_OFFSET     0x0A0 // Alias of GBLCTL containing only Transmitter Reset bits, allows transmit to be reset
                                           //independently from receive.
#define XMASK_OFFSET       0x0A4 // Format unit bit mask register
#define XFMT_OFFSET        0x0A8 // Bit stream format register
#define XAFSCTL_OFFSET     0x0AC // Frame sync control register
#define XACLKCTL_OFFSET    0x0B0 // Clock control register
#define XAHCLKCTL_OFFSET   0x0B4 // High-frequency clock control register
#define XTDM_OFFSET        0x0B8 // TDM slot register
#define XINTCTL_OFFSET     0x0BC // Interrupt control register
#define XSTAT_OFFSET       0x0C0 // Status register
#define XSLOT_OFFSET       0x0C4 // Current TDM slot
#define XCLKCHK_OFFSET     0x0C8 // Clock check control register
#define XEVTCTL_OFFSET     0x0CC // DMA control register

#define SRCTL0_OFFSET      0x180 //SRCTL0 Serializer 0 control register
#define SRCTL1_OFFSET      0x184 //SRCTL1 Serializer 1 control register
#define SRCTL2_OFFSET      0x188 //SRCTL2 Serializer 2 control register
#define SRCTL3_OFFSET      0x18C //SRCTL3 Serializer 3 control register
#define SRCTL4_OFFSET      0x190 //SRCTL4 Serializer 4 control register
#define SRCTL5_OFFSET      0x194 //SRCTL5 Serializer 5 control register
#define SRCTL6_OFFSET      0x198 //SRCTL6 Serializer 6 control register
#define SRCTL7_OFFSET      0x19C //SRCTL7 Serializer 7 control register

#define XBUF0_OFFSET       0x200 //XBUF0 Transmit Buffer for Pin 0
#define XBUF1_OFFSET       0x204 //XBUF1 Transmit Buffer for Pin 1
#define XBUF2_OFFSET       0x208 //XBUF2 Transmit Buffer for Pin 2
#define XBUF3_OFFSET       0x20C //XBUF3 Transmit Buffer for Pin 3
#define XBUF4_OFFSET       0x210 //XBUF4 Transmit Buffer for Pin 4
#define XBUF5_OFFSET       0x214 //XBUF5 Transmit Buffer for Pin 5
#define XBUF6_OFFSET       0x218 //XBUF6 Transmit Buffer for Pin 6
#define XBUF7_OFFSET       0x21C //XBUF7 Transmit Buffer for Pin 7

#define RBUF0_OFFSET       0x280 //RBUF0 Receive Buffer for Pin 0
#define RBUF1_OFFSET       0x284 //RBUF1 Receive Buffer for Pin 1
#define RBUF2_OFFSET       0x288 //RBUF2 Receive Buffer for Pin 2
#define RBUF3_OFFSET       0x28C //RBUF3 Receive Buffer for Pin 3
#define RBUF4_OFFSET       0x290 //RBUF4 Receive Buffer for Pin 4
#define RBUF5_OFFSET       0x294 //RBUF5 Receive Buffer for Pin 5
#define RBUF6_OFFSET       0x298 //RBUF6 Receive Buffer for Pin 6
#define RBUF7_OFFSET       0x29C //RBUF7 Receive Buffer for Pin 7


//-------------------------------------------------------------------
//  McASP register configuration parameters
//-------------------------------------------------------------------
mcASPCfgParms_t McASPDefParms = {
0x0000UL,        // McASPPinTypes     
0x8070UL,        // McASPFormat       
0x0100UL,        // McASPTxFrameSync    
0x0100UL,        // McASPRxFrameSync    
0x0000UL,        // McASPClk          
0x003FUL,        // McASPHFClk        
0UL,             // McASPInactivePin  
1UL,             // McASPTransmitPin  
2UL,             // McASPReceivePin   
16UL,            // McASPSlotWidth    
3UL,             // McASPSlotMask     
4UL,             // McAspPinDir      
0xFFFFFFFFUL,    // McAspMask        
0x0002UL,        // McAspGblCtrlRx   
0x0200UL         // McAspGblCtrlTx   
};


#define MCASP_READ(addr, reg)         (*(volatile ADT_UInt32 *) (addr+reg))
#define MCASP_WRITE(addr, reg, value) {*((volatile ADT_UInt32 *) (addr+reg)) = (ADT_UInt32) value;}

//  -----------------------  IMPORTANT ---------------------------------------
//  McASP_base[] and McASP_Data[] physical addresses assigned to the logical
//  ports MUST be initialized by the custom Gpak startup function in the project 
//  Initialization source C file. 
//  ----------------------------------------------------------------------------
ADT_UInt32 McASP_Base[NUM_TDM_PORTS] = { 0, 0, 0 };
ADT_UInt32 McASP_Data[NUM_TDM_PORTS] = { 0, 0, 0 };
ADT_UInt16 McASP_Configured[NUM_TDM_PORTS] = { FALSE, FALSE, FALSE  };

//-------------------------------------------------------------------
//  Interface data for DMA processing
extern ADT_UInt16 SltsPerFrame[];                   // Slots on TDM frame [fixed at build]
extern ADT_UInt16 MaxDmaSlots [];                   // Max slots available for DMA [fixed at build]

extern volatile ADT_UInt16 MatchDmaFlags;

ADT_Bool startMcASP (int port);
ADT_Bool stopMcASP  (int port);
ADT_Bool setupMcASP (int port );

TDM_Port McASPPort = {
   startMcASP, stopMcASP, setupMcASP
};

static int pollRegister (ADT_UInt32 Base, ADT_UInt32 regAddr, ADT_UInt32 expectedVal, ADT_UInt32 mask) {
   volatile ADT_Int32 timeout;
   volatile ADT_UInt32 regVal;
    timeout = 0x7fffffff;
    do {
        regVal = MCASP_READ (Base, regAddr);
    } while (((regVal & mask) != expectedVal) && (0 < timeout--));
    return (timeout <= 0);
}

#ifndef _TMS320C6400_PLUS
#define PowerOnMcASPs()  //
#else
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// PowerOnMcBSPs
//
// FUNCTION
//  
//     Power on McBSPs and setup p
//
// PARAMETERS
//
//     None
//

static void PowerOnMcASPs () {

   int intrMask;

   volatile ADT_UInt32 *PscCmd  = PSC_CMD;
   volatile ADT_UInt32 *PscStat = PSC_STAT;
   volatile ADT_UInt32 *PscReg  = PSC_REG;
   volatile ADT_UInt32 *PinMux1 = CFG_PINMUX1;

   intrMask = HWI_disable ();

   *CFG_VDD3P3V_PWDN &= ~MUX_BLK_McASPs;
 
   //---------------------------------------------------
   // 1. Wait for previous power transition to complete
   while (*PscStat & 1);

   //---------------------------------------------------
   //  2. Enable clock domains for McASPs (McASP0 and McBSP1)
   PscReg[PSC_McASP0] = PSC_ON;


   //---------------------------------------------------
   //  3. Transition power
   *PscCmd = 1;

   //---------------------------------------------------
   //  4. Wait for power transition to complete
   while (*PscStat & 1);

   //---------------------------------------------------
   //  5. Set pin mux to use McBSPs
   *PinMux1 &= MUX1_McASPMask;
   *PinMux1 |= MUX1_McASPActive;
   HWI_restore (intrMask);

}
#endif


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ConfigureMcASP 
//
// FUNCTION
//   Configures McASP serial port I/O.
//
//  Inputs
//     McAspEnable - Array indicating which McAsp ports are to be configured.
//
// RETURNS
//  Status code indicating success or a specific error.
//
void ConfigureMcASP (ADT_UInt32 McAspEnable[2], mcASPCfgParms_t *pMcASPParms) { 

   ADT_UInt32 j, port;
   ADT_UInt16 *SlotMap;

   PowerOnMcASPs ();
   
    // over-ride default mcASP parameters 
    if (pMcASPParms)
        memcpy (&McASPDefParms, pMcASPParms, sizeof(mcASPCfgParms_t));

    //--------------------------------------------------------------------------
    // For each enabled McAsp. Verify that
    //       configured tx and rx mask values are within build's range
    //       configured tx and rx bits are equal (same number transmit and receive slots)
    //       the quantity of slots requested is supported by the build
   //--------------------------------------------------------------------------
   tdmPortFuncs = &McASPPort;

   for (port=0; port < NUM_TDM_PORTS; port++)   {  

      McASP_Configured[port] = McAspEnable[port];
      
      if (!McAspEnable[port]) continue;

       SlotMap = pSlotMap[port];

      sysConfig.serialWordSize[port] = 16;
      SltsPerFrame[port] = 2;
      DmaSlotCnt[port]   = 2;
      MaxDmaSlots[port]  = 2;
      for (j = 0; j < SltsPerFrame[port]; j++)
         SlotMap[j] = j;

   }
   return;
}

ADT_Bool setupMcASP (int port) {
   // Steps used for initialization are defined in McAsp Reference Guide
   ADT_UInt32 rgblctrl, xgblctrl;           // local copy of mcasp global control register
   ADT_UInt32 Base;
   int rv;

   if (NUM_TDM_PORTS <= port) return FALSE;

   Base = McASP_Base [port];

   //----------- 1) Place McAsp in reset
   rgblctrl = MCASP_READ (Base, GBLCTL_OFFSET);
   if (rgblctrl)  {
       rgblctrl = 0;
       MCASP_WRITE (Base, GBLCTL_OFFSET, 0);
       rv = pollRegister (Base, GBLCTL_OFFSET, rgblctrl, 0xFFFFFEFF);
       if (rv) return FALSE;
   }

   // Setup McASP0 and EDMA if any slots are configured on first McASP port
   if (McASP_Configured[port] == 0) return FALSE;


   //----------- 2) Configure McAsp registers
   // b) Receive registers
   MCASP_WRITE (Base, RMASK_OFFSET,       McASPDefParms.McAspMask);         // No padding data bits
   MCASP_WRITE (Base, RFMT_OFFSET,        McASPDefParms.McASPFormat);       // Data format
   MCASP_WRITE (Base, RAFSCTL_OFFSET,     McASPDefParms.McASPRxFrameSync);  // Frame sync format
   MCASP_WRITE (Base, RTDM_OFFSET,        McASPDefParms.McASPSlotMask);     // Mask of active slots
   MCASP_WRITE (Base, RINTCTL_OFFSET,     0UL);             // No McAsp interrupts

   MCASP_WRITE (Base, REVTCTL_OFFSET,     0UL);             // DMA Enable
   MCASP_WRITE (Base, RCLKCHK_OFFSET,     0xFF0000UL);      // No clock checking
   MCASP_WRITE (Base, RACLKCTL_OFFSET,    McASPDefParms.McASPClk);        // Clock format
   MCASP_WRITE (Base, RAHCLKCTL_OFFSET,   McASPDefParms.McASPHFClk);      // High freq clock format

   // c) Transmit registers
   MCASP_WRITE (Base, XMASK_OFFSET,       McASPDefParms.McAspMask);         // No padding data bits
   MCASP_WRITE (Base, XFMT_OFFSET,        (McASPDefParms.McASPFormat));     // Data format
   MCASP_WRITE (Base, XAFSCTL_OFFSET,     McASPDefParms.McASPTxFrameSync);  // Frame sync format
   MCASP_WRITE (Base, XTDM_OFFSET,        McASPDefParms.McASPSlotMask);     // Mask of active slots
   MCASP_WRITE (Base, XINTCTL_OFFSET,     0UL);             // No McAsp interrupts

   MCASP_WRITE (Base, XEVTCTL_OFFSET,     0UL);             // DMA Enable
   MCASP_WRITE (Base, XCLKCHK_OFFSET,     0xFF0000UL);      // No clock checking
   MCASP_WRITE (Base, XACLKCTL_OFFSET,    McASPDefParms.McASPClk);        // Clock format
   MCASP_WRITE (Base, XAHCLKCTL_OFFSET,   McASPDefParms.McASPHFClk);      // High freq clock format

   // d) Pin state registers
   MCASP_WRITE (Base, SRCTL0_OFFSET,      McASPDefParms.McASPReceivePin);  // Rx
   MCASP_WRITE (Base, SRCTL1_OFFSET,      McASPDefParms.McASPInactivePin); // Inactive
   MCASP_WRITE (Base, SRCTL2_OFFSET,      McASPDefParms.McASPTransmitPin); // Tx
   MCASP_WRITE (Base, SRCTL3_OFFSET,      McASPDefParms.McASPInactivePin);
   MCASP_WRITE (Base, SRCTL4_OFFSET,      McASPDefParms.McASPInactivePin);
   MCASP_WRITE (Base, SRCTL5_OFFSET,      McASPDefParms.McASPInactivePin);
   MCASP_WRITE (Base, SRCTL6_OFFSET,      McASPDefParms.McASPInactivePin);
   MCASP_WRITE (Base, SRCTL7_OFFSET,      McASPDefParms.McASPInactivePin);

   // e) Global registers
   MCASP_WRITE (Base, PFUNC_OFFSET,       McASPDefParms.McASPPinTypes);   // McAsp vs GPIO pins
   MCASP_WRITE (Base, PDIR_OFFSET,        McASPDefParms.McAspPinDir);
   MCASP_WRITE (Base, DITCTL_OFFSET,      0UL);             // DIT mode not used
   MCASP_WRITE (Base, DLBCTL_OFFSET,      0UL);             // Loopback not used
   MCASP_WRITE (Base, AMUTE_OFFSET,       0UL);             // Mute not used

   rv = 1;
   //----------- 3) Start respective high-frequency clocks
   if (McASPDefParms.McASPHFClk & 0x00008000UL) {
        // a) Take internal high-frequency dividers and clock divider out of reset 
        // b) Read back to ensure bits are latched
        rgblctrl = McASPDefParms.McAspGblCtrlRx;
        MCASP_WRITE (Base, RGBLCTL_OFFSET, rgblctrl);
        rv = pollRegister (Base, RGBLCTL_OFFSET, rgblctrl, rgblctrl); 

        xgblctrl = McASPDefParms.McAspGblCtrlTx;
        MCASP_WRITE (Base, XGBLCTL_OFFSET, xgblctrl);
        rv |= pollRegister (Base, XGBLCTL_OFFSET, xgblctrl, xgblctrl); 

        //----------- 4) Start serial clocks
        //  Can be skipped if external
        xgblctrl |= 0x00000200;
        MCASP_WRITE (Base, XGBLCTL_OFFSET, xgblctrl);
        rv |= pollRegister (Base, XGBLCTL_OFFSET, xgblctrl, xgblctrl); 
   }

   //----------- 3) Start respective low-frequency clocks
    if (McASPDefParms.McASPClk & 0x00000020UL) {
        // a) Take internal low-frequency dividers and clock divider out of reset 
        // b) Read back to ensure bits are latched
        rgblctrl = McASPDefParms.McAspGblCtrlRx;
        MCASP_WRITE (Base, RGBLCTL_OFFSET, rgblctrl);
        rv = pollRegister (Base, RGBLCTL_OFFSET, rgblctrl, rgblctrl); 

        xgblctrl = McASPDefParms.McAspGblCtrlTx;
        MCASP_WRITE (Base, XGBLCTL_OFFSET, xgblctrl);
        rv |= pollRegister (Base, XGBLCTL_OFFSET, xgblctrl, xgblctrl); 

        //----------- 4) Start serial clocks
        //  Can be skipped if external
        xgblctrl |= 0x00000100;
        MCASP_WRITE (Base, XGBLCTL_OFFSET, xgblctrl);
        rv |= pollRegister (Base, XGBLCTL_OFFSET, xgblctrl, xgblctrl); 
   }
   
   //----------- 5) Activate DMA
   //  Done as separate function
   if (rv)      return FALSE;
   else         return TRUE;    
}        

//static 
ADT_Bool startMcASP (int port) {
   int rgblctrl, xgblctrl, rv;
   ADT_UInt32 Base, Data;

   Base = McASP_Base[port];
   Data = McASP_Data[port];
   
   //----------- 6) Activate Serializers
   // a) Clear status
   MCASP_WRITE (Base, RSTAT_OFFSET, 0x1FFUL);
   MCASP_WRITE (Base, XSTAT_OFFSET, 0x1FFUL);

   // b) Serializer enable
   rgblctrl = MCASP_READ (Base, RGBLCTL_OFFSET) | 0x00000004;
   MCASP_WRITE (Base, RGBLCTL_OFFSET, rgblctrl);
   rv = pollRegister (Base, RGBLCTL_OFFSET, rgblctrl, rgblctrl); 

   xgblctrl = MCASP_READ (Base, XGBLCTL_OFFSET) | 0x00000400;
   MCASP_WRITE (Base, XGBLCTL_OFFSET, xgblctrl);
   rv = pollRegister (Base, XGBLCTL_OFFSET, xgblctrl, xgblctrl); 

   //----------- 7) Verify transmit buffers clear by XDATA flag of XSTAT register 
   // a) if DMA -> verify XDATA Bit cleared
   MCASP_WRITE (Data, 0, 0);
   rv |= pollRegister (Base, XSTAT_OFFSET, 0, 0x20); 

   //----------- 8) State machine release
   rgblctrl |= 0x00000008;
   MCASP_WRITE (Base, RGBLCTL_OFFSET, rgblctrl);
   rv |= pollRegister (Base, RGBLCTL_OFFSET, rgblctrl, rgblctrl); 

   xgblctrl |= 0x00000800;   
   MCASP_WRITE (Base, XGBLCTL_OFFSET, xgblctrl);
   rv |= pollRegister (Base, XGBLCTL_OFFSET, xgblctrl, xgblctrl); 

   //----------- 9) Frame sync release
   rgblctrl |= 0x00000010;
   MCASP_WRITE (Base, RGBLCTL_OFFSET, rgblctrl);
   rv |= pollRegister (Base, RGBLCTL_OFFSET, rgblctrl, rgblctrl); 

   xgblctrl |= 0x00001000;
   MCASP_WRITE (Base, XGBLCTL_OFFSET, xgblctrl);
   rv |= pollRegister (Base, XGBLCTL_OFFSET, xgblctrl, xgblctrl); 

   if (rv) {
      // Report setup failure and disable McAsp
      MCASP_WRITE (Base, GBLCTL_OFFSET, 0);
      return FALSE;
   }

   return TRUE;

}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// stopMcASP - Stop all McAsp input/output.
//
// FUNCTION
//   This function stops a McAsp port
//
// RETURNS
//  nothing
//
ADT_Bool stopMcASP (int port) {

   ADT_UInt32 Base;

   if (McASP_Configured[port] == 0) return FALSE;

   Base = McASP_Base [port];

   MCASP_WRITE (Base, GBLCTL_OFFSET, 0); // reset mcasp
   pollRegister (Base, GBLCTL_OFFSET, 0, 0xFFFFFFFF);  

   return TRUE;
}


