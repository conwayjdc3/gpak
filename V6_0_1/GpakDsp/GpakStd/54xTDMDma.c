/*
 * Copyright (c) 2007, Adaptive Digital Technologies, Inc.
 *
 * File Name: 54xTDMDma.c
 *
 * Description:
 *   This file contains Dma configuration for the PCM data collection
 *
 * Version: 1.0
 *
 * Revision History:
 *   1/2007 - Initial release.
 *
 */
#include "adt_typedef.h"
#include "GpakDefs.h"
#include "GpakExts.h"
#include "gpakpcm.h"

// NOTE: The multiplexed interrupt assignments uses mode 2
//       which maps all 6 DMA channels to interrupts.
//
// DMA Channels for RX and TX McBSP operations

//  DMA Channel / Interrupt table 
//   McBSP     Rx     Tx  
//     0      0/6    1/7
//     1      2/10   3/11
//     2      4/12   5/13

/*
                      SFC Values

  Bit Name
15-12 DSYN[3:0] DMA sync event used to initiate transfer.
   11 DBLW      0 = 16-bit elements. 1 = 32-bit elements.
  7-0 FCNT      Total number of frames to transfer.
  

----------------------------------------------------
  
                     MCR Values


Bits Name
 15    AUTOINIT  1 = 
 14    DINM      1 = DMA interrupts generated.
 13    IMOD      DMA interrupt generation mode bit.
                   0 Interrupt at buffer full or block transfer
                   1 Interrupt at half buffer full and buffer full
 12    CTMOD       0 = Multiframe,  1 = Autobuffer
 10-8  SIND     Source address increment mode.
 7-6   DMS      Source address space mode.
 4-2   DIND     Destination address increment mode.
 1-0   DMD      Destination address space mode.


 Increment modes:
     0 = No modification,  1 = Postincrement,
     2 = Post-decrement,   3 = Postinc w/DMIDX0
     4 = Postinc w/DMIDX1, 5 = Postinc w/DMIDX0 and DMFRI0
     6 = Postinc w/DMIDX1 and DMFRI1

  Address space modes:
     0 =  Program space,   1 = Data space,     2 = I/O space
*/




//-------------------------------------------------------
// DMA sub-register addresses 



#define IMR    *(volatile ADT_UInt16 *) 0x00   // Interrupt Mask register
#define DMPREC *(volatile ADT_UInt16 *) 0x54     // DMA Priority & Ctrl register
#define DMSA   *(volatile ADT_UInt16 *) 0x55     // DMA Sub Address register
#define DMSDI  *(volatile ADT_UInt16 *) 0x56     // DMA Sub Data (+Inc) register

// NOTE:  DMSRCx is first of five reqister set:  
//   
//  Offset  Name    Description
//    0     DMSRCx  DMA source address
//    1     DMDSTx  DMA destination address
//    2     DMCTRx  Elment count
//    3     DMSFCx  Sync select and frame count
//    4     DMMCRx  Mode control register

#define DMSRC0 0x00
#define DMSRC1 0x05
#define DMSRC2 0x0A
#define DMSRC3 0x0F
#define DMSRC4 0x14
#define DMSRC5 0x19

// McBSP addresses
#define DRR0_ADDRS 0x21  // McBSP0 Data Rcv 
#define DXR0_ADDRS 0x23  // McBSP0 Data Xmt
#define DRR1_ADDRS 0x41  // McBSP1 Data Rcv
#define DXR1_ADDRS 0x43  // McBSP1 Data Xmt
#define DRR2_ADDRS 0x31  // McBSP2 Data Rcv
#define DXR2_ADDRS 0x33  // McBSP2 Data Xmt

#define DMA_RX_0 0x0001
#define DMA_TX_0 0x0002
#define DMA_RX_1 0x0004
#define DMA_TX_1 0x0008
#define DMA_RX_2 0x0010
#define DMA_TX_2 0x0020

// -------------------------------------------
//  DMA to McBSP channel assignments
//
ADT_UInt16 RxEvents [NUM_MCBSPS] = { DMA_RX_0, DMA_RX_1, DMA_RX_2 };
ADT_UInt16 TxEvents [NUM_MCBSPS] = { DMA_TX_0, DMA_TX_1, DMA_TX_2 };

// -------------------------------------------
//  DMA to McBSP control setup
//
typedef struct {
   int BuffSize;
   struct {
      ADT_UInt16 DmaReg, McBSP, SFC, MCR;
   } Rx;
   struct {
      ADT_UInt16 DmaReg, McBSP, SFC, MCR;
   } Tx;
} DMAAddress;


DMAAddress DMAAddr[] = { 
       // ------------ Rx -------------------    --------------- Tx ---------------
       //  Reg,    McBSP,       SFC,   MCR          Reg,   McBSP,        SFC,  MCR
  {0,  { DMSRC0, DRR0_ADDRS, 0x1000, 0x7045 }, { DMSRC1, DXR0_ADDRS, 0x2000, 0x7141 } },
  {0,  { DMSRC2, DRR1_ADDRS, 0x5000, 0x7045 }, { DMSRC3, DXR1_ADDRS, 0x6000, 0x7141 } },
  {0,  { DMSRC4, DRR2_ADDRS, 0x3000, 0x7045 }, { DMSRC5, DXR2_ADDRS, 0x4000, 0x7141 } }
};




//------------------------------------------
// McBSP-DMA to interrupt assignment
#define IMR_RX_0 0x0040
#define IMR_TX_0 0x0080
#define IMR_RX_1 0x0400
#define IMR_TX_1 0x0800
#define IMR_RX_2 0x1000
#define IMR_TX_2 0x2000

ADT_UInt16 DMAIntMasks [] = {
   IMR_RX_0 | IMR_TX_0,
   IMR_RX_1 | IMR_TX_1,
   IMR_RX_2 | IMR_TX_2
};

static ADT_PCM16 *McBspRxBuffers[] = {
   BSP0DMA_RxBuffer, BSP1DMA_RxBuffer, BSP2DMA_RxBuffer
};

static ADT_PCM16 *McBspTxBuffers[] = {
   BSP0DMA_TxBuffer, BSP1DMA_TxBuffer, BSP2DMA_TxBuffer 
};

ADT_PCM16 *DmaRxBuff   [NUM_MCBSPS];   // DMA Rcv buffer address for McBSP
ADT_PCM16 *DmaTxBuff   [NUM_MCBSPS];   // DMA Xmt buffer address for McBSP
ADT_UInt16 DmaPongOffset[NUM_MCBSPS] = { 0, 0, 0 };   // Offset from ping buffer to pong buffer
ADT_UInt16 DmaTxOffset [NUM_MCBSPS];   // current Tx DMA buffer offset (ping or pong) for McBSP
ADT_UInt16 DmaRxOffset [NUM_MCBSPS];   // current Rx DMA buffer offset (ping or pong) for McBSP

extern ADT_UInt16 DmaSlotCnt[];


#ifdef _DEBUG
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ToggleTestSignal
//
// FUNCTION
//   Toggles receive port between DMA buffer (normal) and constant tone (test)
//
// RETURNS
//  nothing
//
static ADT_PCM16 SinTableM12[] = {
  -3, 2828, 4000, 2828, -2, -2828, -4000, -2828,
  -1, 2829, 4000, 2828,  0, -2828, -4000, -2828,
};
static ADT_PCM16 Drain [MAX_SAMPLES_PER_MS*2][MAX_CHANS_PER_FRAME];     // Dummy receive buffer
static ADT_PCM16 Dummy [MAX_SAMPLES_PER_MS*2][MAX_CHANS_PER_FRAME];     // Dummy transmit buffer
static int dummyPort = 0xff;

static void sineBuffer (int port, ADT_PCM16 *Buffer) {
   int i, dmaSltCnt;

   // Populate dummy buffer with 1 kHz SINE wave on all channels
   if (dummyPort == port) return;
   if (NUM_MCBSPS <= port) return;
   dmaSltCnt = DmaSlotCnt[port];

   for (i=0; i<SAMPLES_PER_INTERRUPT*2; i++) {
      memset (Buffer, SinTableM12[i], dmaSltCnt);
      Buffer += dmaSltCnt;
   }

}

ADT_UInt16 TogglePCMInSignal (int port) {

   port &= 0xff;
   port--;
   if (NUM_MCBSPS <= port) return 0;

   sineBuffer (port, (ADT_PCM16 *) &Dummy);

   if (DmaRxBuff[port] == McBspRxBuffers[port]) {
       DmaRxBuff[port] = (ADT_PCM16 *) &Dummy;
       return 1;
   }
   // Restore
   DmaRxBuff[port] = McBspRxBuffers[port];
   return 0;
}


ADT_UInt16 TogglePCMOutSignal (int port) {

   port &= 0xff;
   port--;
   if (NUM_MCBSPS <= port) return 0;


   if (DmaTxBuff[port] == McBspTxBuffers[port]) {
       sineBuffer (port, McBspTxBuffers[port]);
       DmaTxBuff[port] = (ADT_PCM16 *) &Drain;
       return 1;
   }
   // Restore
   DmaTxBuff[port] = McBspTxBuffers[port];
   memset (DmaTxBuff[port], 0, DMAAddr[port].BuffSize);
   return 0;
}
#else
ADT_UInt16 TogglePCMOutSignal (int port) { return 0xff; }
ADT_UInt16 TogglePCMInSignal (int port)  { return 0xff; }
#endif


void zeroTxBuffer (int port, int dmaSlot) {

   int i, dmaSltCnt;
   ADT_PCM16 *TxBuffer;

   // Populate transmission channel with zero
   if (NUM_MCBSPS <= port) return;

   dmaSltCnt = DmaSlotCnt[port];
   TxBuffer  = McBspTxBuffers[port] + dmaSlot;

   LogTransfer (0x8000, dmaSlot, TxBuffer, 0, 0, dmaSltCnt);

   for (i=0; i<SAMPLES_PER_INTERRUPT*2; i++) {
     *TxBuffer = 0;
      TxBuffer += dmaSltCnt;
   }
}

//---------------------------------------------------
//
//  DMAIntialize - Disables all DMA; initializes interrupt variables
//
void DMAInitialize () {
   int port;
   
   // Set the multiplexed interrupt control for all DMA interrupts.
   DMPREC = 0x0080;

   for (port = 0; port < 3; port++) {      
      DmaRxBuff[port] = McBspRxBuffers[port];
      DmaTxBuff[port] = McBspTxBuffers[port];
      disableDMA (port);
  }

}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{enableDMA
//
// FUNCTION
//   Enables Tx and Rx DMA for specified McBSP
//
// RETURNS
//   Bit mask for event interrupts
//}
ADT_UInt16 enableDMA (int port) {
   int intrMask;
   ADT_UInt16 buffSize;
    
   DMAAddress *Addr;

   if (NUM_MCBSPS <= port) return 0;

   Addr = &DMAAddr[port];

   buffSize = DmaSlotCnt[port] * SAMPLES_PER_INTERRUPT * 2;
   Addr->BuffSize = buffSize;

   intrMask = HWI_disable ();

   // Configure Rx DMA
   DMSA  = Addr->Rx.DmaReg;
   DMSDI = Addr->Rx.McBSP;       // Src
   DMSDI = (ADT_UInt16) DmaRxBuff[port];  // Dest
   DMSDI = buffSize;             // Size
   DMSDI = Addr->Rx.SFC;         // SFC Value
   DMSDI = Addr->Rx.MCR;         // MCR Value

   // Configure Tx DMA
   DMSA  = Addr->Tx.DmaReg;
   DMSDI = (ADT_UInt16) DmaTxBuff[port];  // Src
   DMSDI = Addr->Tx.McBSP;       // Dest
   DMSDI = buffSize;             // Size
   DMSDI = Addr->Tx.SFC;         // SFC Value
   DMSDI = Addr->Tx.MCR;         // MCR Value

   IMR     |= DMAIntMasks [port];  // Enable interrupts
   DMPREC  |= (RxEvents [port] | TxEvents [port]);  // Enable DMA

   DmaPongOffset[port] = buffSize / 2;
   HWI_restore (intrMask);
   return (RxEvents [port] | TxEvents [port]);
   
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{disableDMA
//
// FUNCTION
//   This function resets the EDMA controller
//
// RETURNS
//   Bit mask for event interrupts
/}
ADT_UInt16 disableDMA (int port) {

   if (NUM_MCBSPS <= port) return 0;

   IMR    &= ~DMAIntMasks [port];
   DMPREC &= ~(RxEvents [port] | TxEvents [port]);

   memset (McBspTxBuffers[port], 0, sysConfig.dmaBufferSize[port]); 
   return (RxEvents [port] | TxEvents [port]);

}


static void signalDmaInt (ADT_UInt16 DMAFlag) {

   if (NumActiveChannels) logTime (0x00100000ul | DMAFlag);

   SWI_or (SWI_Dma, DMAFlag);
   return;
}


ADT_Bool DmaLostTxSync (int port) {

   ADT_PCM16   *TxDma, *TxPong;
   int          BuffSize;
   DMAAddress  *Addr;
   ADT_Bool     LostSync;
   
   TxPong = McBspTxBuffers[port] + DmaTxOffset[port];

   Addr = &DMAAddr[port];
   BuffSize = Addr->BuffSize;   // Buffer size

   // Read DMA Tx address
   DMSA  = Addr->Tx.DmaReg;  // Address Tx source register
   TxDma  = (ADT_PCM16 *) DMSDI;       // Current Tx source

   LostSync = (TxPong <= TxDma && TxDma < TxPong + BuffSize/2);
#if _DEBUG
   if (LostSync) LogTransfer (0x8ADA, TxDma, TxPong, 0, 0, BuffSize);
#endif
   return LostSync;
}

ADT_Bool DmaLostRxSync (int port) {

   ADT_PCM16   *RxDma, *RxPong;
   int          BuffSize;
   DMAAddress  *Addr;
   ADT_Bool     LostSync;

   RxPong = McBspRxBuffers[port] + DmaRxOffset[port];

   Addr = &DMAAddr[port];
   BuffSize = Addr->BuffSize;   // Buffer size

   // Read DMA Tx address
   DMSA  = Addr->Rx.DmaReg + 1;  // Address Tx source register
   RxDma  = (ADT_PCM16 *) DMSDI;       // Current Tx source

   LostSync = (RxPong <= RxDma && RxDma < (RxPong + BuffSize/2));

#ifdef _DEBUG
   if (LostSync) LogTransfer (0x8AAD, RxDma, RxPong, 0, 0, BuffSize);
#endif

   return LostSync;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{GpakDmaxIsr
//
// FUNCTION
//  G.PAK Hardware Interrupt handlers for all DMA Channels.
//  Triggered by occurrance tx and rx mcbsp edma events.
//  The SWI_Dma interrupt is issued to start the circular buffer copies.
//
// RETURNS
//  nothing
//}

void GpakDma0Isr(void) { signalDmaInt (DMA_TX_0); return;  }
void GpakDma1Isr(void) { signalDmaInt (DMA_RX_0); return;  }
void GpakDma2Isr(void) { signalDmaInt (DMA_TX_1); return;  }
void GpakDma3Isr(void) { signalDmaInt (DMA_RX_1); return;  }
void GpakDma4Isr(void) { signalDmaInt (DMA_TX_2); return;  }
void GpakDma5Isr(void) { signalDmaInt (DMA_RX_2); return;  }



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{CopyDmaBuffers - Copy serial port DMA buffers to/from channel PCM buffers.
//
// FUNCTION
//   This function copies PCM data between a serial port's DMA buffers and the
//   PCM circular buffers of any active channels using the serial port.  
//
//   The copy performs the transfers in the order in which they appear in memory.
//
//   DMA buffers are organized as follows:
//
//      T1-S1   T2-S1   T3-S1  ...  Tn-S1  \
//      T1-S2   T2-S2   T3-S2  ...  Tn-S2   \
//               .       .       .             Ping        
//      T1-S8   T2-S8   T3-S8  ...  Tn-S8   /
//
//      T1-S9   T2-S9   T3-S9  ...  Tn-S9   \
//               .       .       .             Pong        
//      T1-S16  T2-S16  T3-S16 ...  Tn-S16  /
//
//   where: Ti = i th time-slot in TDM frame and
//          Sj = j th sample
//
// RETURNS
//  nothing
//}
void CopyDmaTxBuffers (int slotCnt, CircBufInfo_t **pCircTxBase,
                       ADT_PCM16 *pDmaTxBufr) {

   int sample, timeSlot;
   CircBufInfo_t *pCirc;        // pointer to Circular Buffer structure
   CircBufInfo_t **pCircTx;     // current pointer to circ xmt bufr pointer

   
   for (sample = 0; sample < SAMPLES_PER_INTERRUPT; sample++) {

      pCircTx = pCircTxBase;

      for (timeSlot = 0; timeSlot < slotCnt; timeSlot++) {

         if ((pCirc = *pCircTx) != NULL) {

            // Make sure there is enough samples/room for transfers
            if (sample == 0) LogTransfer (0x21C1, pCirc, &pCirc->pBufrBase[pCirc->TakeIndex], 0, pDmaTxBufr, SAMPLES_PER_INTERRUPT);
            SystemError ((sample==0 && getAvailable(pCirc) < SAMPLES_PER_INTERRUPT));

            *pDmaTxBufr = pCirc->pBufrBase[pCirc->TakeIndex];
            if (++pCirc->TakeIndex >= pCirc->BufrSize) pCirc->TakeIndex = 0;
         }

         // Locate next channel locations
         pCircTx++;         pDmaTxBufr++;
      }
   }
   return;
}




void CopyDmaRxBuffers (int slotCnt,  ADT_PCM16 *pDmaRxBufr,
                       CircBufInfo_t **pCircRxBase) {

   int sample, timeSlot;
   CircBufInfo_t *pCirc;        // pointer to Circular Buffer structure
   CircBufInfo_t **pCircRx;     // current pointer to circ Rcv bufr pointer


   for (sample = 0; sample < SAMPLES_PER_INTERRUPT; sample++) {

      pCircRx = pCircRxBase;

      for (timeSlot = 0; timeSlot < slotCnt; timeSlot++) {

         // Copy Rx sample from DMA to circular buffer
         if ((pCirc = *pCircRx) != NULL)  {
 
             // Make sure there is enough samples/room for transfers
            if (sample == 0) LogTransfer (0x111C, 0, pDmaRxBufr, pCirc, &pCirc->pBufrBase[pCirc->PutIndex], SAMPLES_PER_INTERRUPT);
            SystemError ((sample==0 && getFreeSpace (pCirc) < SAMPLES_PER_INTERRUPT));

            pCirc->pBufrBase[pCirc->PutIndex] = *pDmaRxBufr;
            if (++pCirc->PutIndex >= pCirc->BufrSize) pCirc->PutIndex = 0;
         }

         // Locate next channel locations
         pCircRx++;         pDmaRxBufr++;
      }
   }
   return;
}
