/* Copyright (c) 2022, Adaptive Digital Technologies, Inc.7
 *
 * File Name: 6472TSIPDma.c
 *
 * Description:
 *    This file contains G.PAK DMA functions to perform transfer between TSIPs and channel buffers.
 *
 * Version: 1.0
 *
 * Revision History:
 *   01/23/2007 - Initial release.
 *   06/07/2022 - 667x support
 *
 */
/*   DMA configuration details for each TSIP port 

    DMA completion is mapped directly to interrupt 4

    DMA errors, TSIP errors, and TSIP superframe completions are
    combined in event 1 which is mapped to interrupt 5

   --- DMA channels and synchronization events.  
   --- Unmapped channels are used for DMA buffer to TSIP buffer transfers.

   ---------------------------------------------------------------------------
   ----   DMA Completion Interrupt
    DMA completion event  8 (TDM_CompleteSgnl CSL_INTC_EVENTID_EDMA3CCINT_LOCAL)
    DMA completion IRQ     4 (HWI_DMA_COMPLETE)

                   Rx0  Tx0      Rx1  Tx1     Rx2  Tx2  
        Ping        48   50       52   54      56   58
        Pong        49   51       53   55      57   59
       TCC Queue     0    1        0    1       0    1

   ---------------------------------------------------------------------------
   ----   TSIP Interrupts and Error Interrupts
    TSIP/DMA error combined event  1 (CombinedEventIntr)
    TSIP/DMA error IRQ             5 (HWI_COMBINED_INT)

   --- TSIP super frame interrupts (mapped to interrupt combiner)

                      Rx0  Tx0      Rx1  Tx1
        SuperFrame     53   55       57   59
           Error          18            19

    --- DMA error interrupts (mapped to interrupt combiner) 
    0 TDM_CCErrSgnl     CSL_INTC_EVENTID_EDMA3CC_ERRINT
    2 TDM_Que0ErrSgnl   CSL_INTC_EVENTID_EDMA3TC0_ERRINT
    3 TDM_Que1ErrSgnl   CSL_INTC_EVENTID_EDMA3TC1_ERRINT
    4 TDM_Que2ErrSgnl   CSL_INTC_EVENTID_EDMA3TC2_ERRINT




 Data flow:
         circular buffers <-> GpakDMASwi <-> DMA transfer buffer <-> DMA <-> TSIP transfer buffer <-> TSIP

*/
/*   Description of processing 

              
   The DMA is setup to transfer 1ms (8 TSIP frames) of data between: channel circular buffers, DMA transfer buffers,
   TSIP transfer buffers, and the TSIP device. DMA transfers are chained to themselves to allow the entire 1 milliseconds
   of data to be reformatted without CPU intervention.

   DMA transfer buffers, specified by TxTransBuffers[port] and RxTransBuffers[port], are each divided into 
   two sections (ping and pong). Data in these buffers is arranged 8 samples per channel and is N channels 
   deep to allow efficient transfers to and from the channel circular buffers.  
   
   TSIP transfer buffers, specified by TxTsipBuffers[port] and RxTsipBuffers[port] are also divided into 
   ping and pong sections.  Data in these buffers is arranged N channels per frane and 8 frames
   deep to allow efficient transfers between the circular buffers and the.  

   For data going to the TDM bus, when the TSIP transmit interrupt occurs, the TSIP interrupt
   handler issues a SWI to run the GpakDMASwi routine.  This routine invokes the CopyDmaTxBuffers 
   routine which copies data from the circular channel buffers into a linear DMA transfer buffer.   
   After the CopyDmaTxBuffer moves the data into the DMA transfer buffer, it issues a DMA request to 
   reformat the data into the TSIP transfer buffers.
   
   For data coming from the TDM bus, the TSIP interrupt handler issues a DMA request to reformat
   the data from the TSIP transfer buffers into the linear DMA transfer buffer.  The interrupt generated
   by the completion of the DMA request, issues a SWI to run the GpakDMASwi routine.  This routine invokes 
   the CopyDmaRxBuffers routine which copies data from the DMA transfer buffer into the circular channel buffers.
   
   After 8 frames (1ms) of data, the processing automatically switches between the ping and pong buffers.
  
   The CPU is notified that a transfer is complete via a common interrupt.
   The interrupt handler can determine which transfer(s) is complete by reading
   and clearing the CIPR register

*/
/*   Transfer buffer formats
//---------------------------------------------------------------
// Each element of format ScbFn in the tables below indicates a single byte
// where c = channel number, b = byte number within the sample, and n = frame number.


//----------------------------------------------------------------
//   1 frame in 8-bit NB mode requires one 8-bit transfer to one 16-bit sample for each channel.
// DMA tranfer buffer format
      S11F1 0 | S11F2 0 | S11F3 0 | S11F4 0 | S11F5 0 | S11F6 0 | S11F7 0 | S11F8 0
      S21F1 0 | S21F2 0 | S21F3 0 | S21F4 0 | S21F5 0 | S21F6 0 | S21F7 0 | S21F8 0 
      S31F1 0 | S31F2 0 | S31F3 0 | S31F4 0 | S31F5 0 | S31F6 0 | S31F7 0 | S31F8 0
                                . 
                                . 
      Sn1F1 0 | Sn1F2 0 | Sn1F3 0 | Sn1F4 0 | Sn1F5 0 | Sn1F6 0 | Sn1F7 0 | Sn1F8 0
 

// TSIP buffer format
       FID1 | S11F1 | S21F1 | S31F1 | ... | Sn1F1 | FID1 | padding
       FID2 | S11F2 | S21F2 | S31F2 | ... | Sn1F2 | FID2 | padding
       FID3 | S11F3 | S21F3 | S31F3 | ... | Sn1F3 | FID3 | padding
                       . 
                       . 
       FID8 | S11F8 | S21F8 | S31F8 | ... | Sn1F8 | FID8 | padding


//----------------------------------------------------------------
//   1 frame in 8-bit WB mode requires two 8-bit transfers to two 16-bit samples for each channel.
// DMA tranfer buffer format
      S11F1 S11F2 | S11F3 S11F4 | S11F5 S11F6 |         | S11Ff S11F0
      S21F1 S21F2 | S21F3 S21F4 | S21F5 S21F6 | .  .  . | S21Ff S21F0
      S31F1 S31F2 | S31F3 S31F4 | S31F5 S31F6 |         | S31Ff S31F0
                                         .                                           
                                         .                                          
      Sn1F1 Sn1F2 | Sn1F3 Sn1F4 | Sn1F5 Sn1F6 | .  .  . | Sn1Ff Sn2F0

// TSIP buffer format
     FID1 | S11F1 S11F2 | S21F1 S21F2 | S31F1 S31F2 | .  .  . | Sn1F1  Sn1F2  | FID1 | padding 
     FID2 | S11F3 S11F4 | S2134 S21F2 | S31F3 S31F4 | .  .  . | Sn1F3  Sn1F4  | FID2 | padding 
     FID3 | S11F5 S11F6 | S21F5 S21F6 | S31F5 S31F6 | .  .  . | Sn1F5  Sn1F6  | FID3 | padding 
                                         .                                           
                                         .                                          
     FID8 | S11Ff S11F0 | S21Ff S21F0 | S31Ff S31F0 | .  .  . | Sn1Ff  Sn1F0  | FID8 | padding 

     NOTE: Transfer from DMA transfer buffer to circular buffer requires 
           8-bit to 16-bit transfers.
     
//----------------------------------------------------------------
//   1 frame in 16-bit NB mode requires one 16-bit transfer to one 16-bit sample for each channel.
// DMA tranfer buffer format
      S11F1 S12F1 | S11F2 S12F2 | S11F3 S12F3 |         | S11F8 S12F8
      S21F1 S22F1 | S21F2 S22F2 | S21F3 S22F3 | .  .  . | S21F8 S22F8
      S31F1 S32F1 | S31F2 S32F2 | S31F3 S32F3 |         | S31F8 S32F8
                                . 
                                . 
      Sn1F1 Sn2F1 | Sn1F2 Sn2F2 | Sn1F3 Sn2F3 | .  .  . | Sn1F8 Sn2F8

// TSIP buffer format
     FID1 | S11F1 S12F1 | S21F1 S22F1 | S31F1 S32F1 | .  .  . | Sn1F1  Sn2F1  | FID1 | padding
     FID2 | S11F2 S12F2 | S21F2 S22F1 | S31F1 S32F2 | .  .  . | Sn1F2  Sn2F2  | FID2 | padding
     FID3 | S11F3 S12F3 | S21F3 S22F1 | S31F1 S32F3 | .  .  . | Sn1F3  Sn2F3  | FID3 | padding
                       . 
                       . 
     FID8 | S11F8 S12F8 | S21F8 S22F8 | S31F8 S32F8 | .  .  . | Sn1F8  Sn2F8  | FID8 | padding



//----------------------------------------------------------------
//   1 frame in 16-bit WB mode requires two 16-bit transfers to two 16-bit sample for each channel.
// DMA tranfer buffer format
      S11F1 S12F1  S11F2 S12F2 | S11F3 S12F3  S11F4 S12F4 |          | S11Ff S12Ff S11Ff S12F0
      S21F1 S22F1  S21F2 S22F2 | S21F3 S22F3  S21F4 S22F4 |  .  .  . | S21Ff S22Ff S21Ff S22F0
      S31F1 S32F1  S31F2 S32F2 | S31F3 S32F3  S31F4 S32F4 |          | S31Ff S32Ff S31Ff S32F0
                                                . 
                                                . 
      Sn1F1 Sn2F1  Sn1F2 Sn2F2 | Sn1F3 Sn2F3  Sn3F4 Sn4F4 |  .  .  . | Sn1F8 Sn2F8 Sn3F8 Sn4F8

// TSIP buffer format
     FID1 | S11F1 S12F1  S11F2 S12F2 | S21F1 S22F1 S21F2 S22F2 | S31F1 S32F1 S31F2 S32F2 | .  .  . | Sn1F1 Sn2F1 Sn1F2 Sn2F2 | FID1 | padding
     FID2 | S11F3 S12F3  S11F4 S12F4 | S21F3 S22F3 S21F4 S22F4 | S31F3 S32F3 S31F4 S32F4 | .  .  . | Sn1F3 Sn2F3 Sn1F4 Sn2F4 | FID2 | padding
     FID3 | S11F5 S12F5  S11F6 S12F6 | S21F5 S22F5 S21F6 S22F6 | S31F5 S32F5 S31F6 S32F6 | .  .  . | Sn1F5 Sn2F5 Sn1F6 Sn2F5 | FID3 | padding
                                                                   .                                                             
                                                                   .                                                            
     FID8 | S11Ff S12Ff  S11F0 S12F0 | S21Ff S22Ff S21F0 S22F0 | S31Ff S32Ff S31F0 S32F0 | .  .  . | Sn1Ff Sn2Ff Sn1F0 Sn2F0 | FID8 | padding

*/
 
/* INTERFACES

   void DMAInitialize ()
   ADT_UInt16 enableDMA  (port) 
   ADT_UInt16 disableDMA (port)

   ADT_Bool DmaLostTxSync (int port, ADT_PCM16 *TxSwi);
   ADT_Bool DmaLostRxSync (int port, ADT_PCM16 *RxSwi);

   void CopyDmaTxBuffers (port, CircBufInfo_t **pCircTxBase,  ADT_UInt16 *pDmaTxBufr);
   void CopyDmaRxBuffers (port, ADT_UInt16 *pDmaRxBufr, CircBufInfo_t **pCircRxBase);

                      -------------------------

     DEBUGGING AIDES
     
   TxPatternSlot1    = (non-zer0) Pattern placed only in first slot.
   TxPatternAllSlots = (non-zero) Pattern placed in all slots.  TxPatternSlot1 takes precedence on first slot.

   RxPatternSlot1    = (non-zer0) Pattern placed only in first slot.
   RxPatternAllSlots = (non-zer0) Pattern placed in all slots.  RxPatternSlot1 takes precedence on first slot.

   FrameStamp = (true) 1st byte of each frame contains current frame count   
*/


// Application related header files.
#ifndef SHADOW_REGION
   #define SHADOW_REGION 1  // TSIP core.
#endif


#include <stdint.h>
#include <std.h>
//#include <hwi.h>
//#include <swi.h>
//#include <exc.h>
//#include <c64.h>
#include <string.h>

//#include "GpakDefs.h"
#include "adt_typedef.h"
#include "GpakExts.h"
#include "sysconfig.h"
#include "GpakPcm.h"
//#include "64plus.h"
#include "667x.h"

//#define HWI_DMA_COMPLETE   4    // HW Interrupt vector for EDMA completions
#define TSIP_SYNC_I8       4    // Size of TSIP Frame ID


#ifdef TEST     // static / inline removal
   #undef _DEBUG
   #define _DEBUG
   #define static
   #define inline
#endif
extern __cregister volatile unsigned int IERR;

typedef void (DMA_TRANSFER) (ADT_UInt64* dmaBuff, CircBufInfo_t **CircBufr, int slotCnt);

ADT_UInt64 dmaError;


//------------------------------------------
//{ Frameworks defined externals
extern int customTDMOut (ADT_PCM16 *dataOut, int port, int slotCnt);
extern int customTDMIn  (ADT_PCM16 *dataIn,  int port, int slotCnt);
extern void *addCoreOffset (void *addr);
extern TSIPMode_t getTSIPMode ();
extern int gblPkLoadingRst;

// Pointers to DMA transfer buffers
extern ADT_PCM16* const RxTransBuffers[];
extern ADT_PCM16* const TxTransBuffers[];

// Size of TSIP buffers
extern ADT_UInt16 TsipBuflenI8[];
extern ADT_UInt16 TsipFallocI8[];
//}

//------------------------------------------
//{ TSIP defined externals
extern void InitTSIPMode ();
extern void GpakTSIPIsr    (Hwi_FuncPtr event);
extern void GpakTSIPErrIsr (Hwi_FuncPtr event);
extern int validateRxFrameCnt (int port, int pong);

// Pointers to TSIP data buffers
extern const ADT_UInt8* RxTsipBuffers[];
extern const ADT_UInt8* TxTsipBuffers[];

extern ADT_UInt16 tdmCore;      
extern ADT_UInt16 TDM_frames_per_interrupt;
//}

// -------------------------------------------
//{  TSIP DMA defined externals
//
//  DMA event bits to port assignments for PCM thread to relate each port and direction to a specific event
//SS DEBUG TSIP ADT_UInt16 RxEvents [] = { 0, 0, 0 };     // Identifies rx events that are reported to dma SWI
//SS DEBUG TSIP ADT_UInt16 TxEvents [] = { 0, 0, 0 };     // Identifies tx events that are reported to dma SWI

ADT_UInt32 RxEvents [] = { 0, 0, 0 };     // Identifies rx events that are reported to dma SWI
ADT_UInt32 TxEvents [] = { 0, 0, 0 };     // Identifies tx events that are reported to dma SWI
#pragma DATA_SECTION (RxEvents,    "FAST_DATA_SECT:TDM")
#pragma DATA_SECTION (TxEvents,    "FAST_DATA_SECT:TDM")

DMA_TRANSFER* dmaToCirc;
DMA_TRANSFER* circToDma;
#pragma DATA_SECTION (dmaToCirc, "NON_CACHED_DATA")
#pragma DATA_SECTION (circToDma, "NON_CACHED_DATA")

//DMA_TRANSFER dmaToCirc16BitNB;
//DMA_TRANSFER circToDma16BitNB;

DMA_TRANSFER dmaToCirc8BitNB;
DMA_TRANSFER circToDma8BitNB;

//}

// -------------------------------------------
//{  TSIP DMA defined internals
//
void DMAClearErrors (ADT_UInt32 pendingInterrupts);
void DMAClearAll (void);
static void GpakDmaIsr      (Hwi_FuncPtr event);
static void GpakDMAErrIsr   (void);
static void GpakTSIP_EventIsr (Hwi_FuncPtr errIsr);

static ADT_Bool dmaFirstTime = ADT_TRUE;
#pragma DATA_SECTION (dmaFirstTime, "NON_CACHED_DATA")

// NOTE: Pong channel = Ping channel + 1
static const ADT_UInt16 DmaRxPingCh [] = { 52, 56 , 0};   // Ping dma event IDs
static const ADT_UInt16 DmaTxPingCh [] = { 54, 58 , 0 };

static ADT_UInt64 DmaRxPingMask [NUM_TDM_PORTS];  // Bit mask for ping dma events
static ADT_UInt64 DmaTxPingMask [NUM_TDM_PORTS];
#pragma DATA_SECTION (DmaRxPingMask, "FAST_DATA_SECT:TDM")
#pragma DATA_SECTION (DmaTxPingMask, "FAST_DATA_SECT:TDM")

static ADT_UInt64 DmaRxEvents [NUM_TDM_PORTS];   // Bit mask for ping and pong dma events
static ADT_UInt64 DmaTxEvents [NUM_TDM_PORTS];
#pragma DATA_SECTION (DmaRxEvents, "FAST_DATA_SECT:TDM")
#pragma DATA_SECTION (DmaTxEvents, "FAST_DATA_SECT:TDM")

static ADT_UInt64 ShadowRegionBits = 0;      // Bit mask for all events assigned to shadow region
#pragma DATA_SECTION (ShadowRegionBits, "SLOW_DATA_SECT:TDM")

static ADT_UInt64 enabledDmaEvts = 0;        // Bit mask for active dma events
#pragma DATA_SECTION (enabledDmaEvts, "FAST_DATA_SECT:TDM")

static ADT_UInt64 DmaActive = 0;             // Bit mask identifying active dma transfers
static ADT_UInt64 DmaBuffActive = 0;         // Bit mask identifying active dma buffers
#pragma DATA_SECTION (DmaActive,     "FAST_DATA_SECT:TDM")
#pragma DATA_SECTION (DmaBuffActive, "FAST_DATA_SECT:TDM")

static ADT_UInt16 DmaPingBufferI8 [NUM_TDM_PORTS];  // Size of DMA transfer ping buffer
#pragma DATA_SECTION (DmaPingBufferI8, "FAST_DATA_SECT:TDM")

// Buffer allocation FIFOs
typedef struct FIFO {
   void* first;
   void* last;
} FIFO;

typedef enum BuffState {
   idle,
   circToLinear, linearToTsipDma, waitingForTSIPTx,    // Tx states
   tsipToLinearDma, linearToCirc, waitingForTSIPRx
} BuffState;

typedef struct DMABuffInfo {
   void*      next;
   void*      buffAddr;
   BuffState  state;
   int        pong;   // pong offset
   ADT_UInt64 evt;    // DMA event bit
} DMABuffInfo;

static FIFO RxDMAQ[NUM_TDM_PORTS];
static FIFO TxDMAQ[NUM_TDM_PORTS];
static FIFO RxTSIPQ[NUM_TDM_PORTS];
static FIFO TxTSIPQ[NUM_TDM_PORTS];
#pragma DATA_SECTION (RxDMAQ, "FAST_DATA_SECT:TDM")
#pragma DATA_SECTION (TxDMAQ, "FAST_DATA_SECT:TDM")
#pragma DATA_SECTION (RxTSIPQ, "FAST_DATA_SECT:TDM")
#pragma DATA_SECTION (TxTSIPQ, "FAST_DATA_SECT:TDM")

static DMABuffInfo RxDMAPing[NUM_TDM_PORTS];
#pragma DATA_SECTION (RxDMAPing, "FAST_DATA_SECT:TDM")
static DMABuffInfo RxDMAPong[NUM_TDM_PORTS];
#pragma DATA_SECTION (RxDMAPong, "FAST_DATA_SECT:TDM")
static DMABuffInfo TxDMAPing[NUM_TDM_PORTS];
#pragma DATA_SECTION (TxDMAPing, "FAST_DATA_SECT:TDM")
static DMABuffInfo TxDMAPong[NUM_TDM_PORTS];
#pragma DATA_SECTION (TxDMAPong, "FAST_DATA_SECT:TDM")

typedef struct errCnts {
   ADT_UInt32 cnt;

   ADT_Int64  MissedEvents;
   ADT_UInt32 WaterMarkErr;

   ADT_UInt32 TC0Stat;
   ADT_UInt32 TC1Stat;
   ADT_UInt32 TC2Stat;
   ADT_UInt32 TC0Error;
   ADT_UInt32 TC1Error;
   ADT_UInt32 TC2Error;

   ADT_UInt32 IntrStat;
   
   ADT_UInt32 InternalExc;
} errCnts;
static errCnts dmaErrs = { 0 };

typedef struct cpuUsage_t {
   ADT_UInt8 cnt;
   ADT_UInt32 accum;
} cpuUsage_t;
cpuUsage_t TDMUsage [6];   // 0-2 rx ports.  3-5 tx ports.

#define DMA_STAT_ID  0x55550002

typedef struct dmaStats {
   int statID;
   int initCnt;
   int errors;
   int Intrs, RxSwis, TxSwis;
   int Rqsts, Cmplts;
} dmaStats_t;
dmaStats_t dmaStats;
//}
extern int skipErrors;

#if !defined (_DEBUG) && !defined (_LOG)   // DMA Logging
   #define logDMA(type,port,evt)
   #define clearDmaLog()
#else
   typedef struct dmaLog {
      char* type;
      ADT_UInt32 port;
      ADT_UInt32 event;
      ADT_UInt32 DmaActive;
      ADT_UInt32 DmaBuffActive;
      ADT_UInt32 time;
   } dmaLog_t;
   dmaLog_t dmaLog [129];
   int dmaIdx = 0;
   int lastDmaTime = 0;
   int dmaLogWrap = ADT_TRUE;

   #pragma DATA_SECTION (dmaLog, "STATS:dma")
   #pragma CODE_SECTION (logDMA, "FAST_PROG_SECT")
   void logDMA (char* type, int port, ADT_UInt64 event) {
      struct dmaLog *log;
      int time;

      if (skipErrors) return;
      if (!dmaLogWrap && 120 < dmaIdx) return;

      time  = CLK_gethtime ();
      log = &dmaLog[dmaIdx++];
      dmaIdx &= 0x7f;

      log->type = type;
      log->port = port;

      if (event & 0xffffffff) log->event = event;
      else                    log->event = event>>48;

      log->DmaActive     = DmaActive>>48;
      log->DmaBuffActive = DmaBuffActive>>48;
      log->time = time - lastDmaTime;
      if (lastDmaTime == 0) log->time = 0;
      log++;
      log->type = NULL;
      if (DmaBuffActive == 0) lastDmaTime = time;
   }

   #define clearDmaLog()  memset (&dmaLog, 0, sizeof (dmaLog));
#endif
   ADT_Bool  FrameStamp = FALSE;         // Time stamp is to be placed on every 8th sample
   struct dmaDebug {
      ADT_Bool   firstTime;
      ADT_UInt16 TxPatternSlot1;       // Debug pattern to be placed on Tx timeslot 1
      ADT_UInt16 TxPatternAllSlots;    // Debug pattern to be placed on all Tx timeslots
      ADT_UInt16 RxPatternSlot1;       // Debug pattern to be placed on Rx timeslot 1
      ADT_UInt16 RxPatternAllSlots;    // Debug pattern to be placed on all Rx timeslots

      ADT_UInt16 TxSineSlot;           // Slot to transmit sine pattern
      ADT_UInt16 BroadcastPort;
      ADT_UInt16 BroadcastSlot;
      ADT_UInt16 captureTx;
      ADT_UInt16 captureRx;
   } dma = { TRUE, 0x1234, 0x35, 0x17, 0x48,
             0xffff, 0, 0xffff, 0xffff, 0xffff };

#ifndef _DEBUG
//--------------------------------------------------------------------------------
//
//   Debug = Enable fixed pattern testing and out of sync buffers
//   ADT_UInt16 TogglePCMInSignal (int port)  { return 0xff; }
//   ADT_UInt16 TogglePCMOutSignal (int port) { return 0xff; }

   #define FixedPatternTx(TxDmaBufr)
   #define FixedPatternRx(RxDmaBufr)
#else

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ToggleTestSignal
//
// FUNCTION
//   Toggles receive port between DMA buffer (normal) and constant tone (test)
//
// RETURNS
//  nothing
//
ADT_PCM16 const SineTableM12[] = {
  -3, 2828, 4000, 2828, -2, -2828, -4000, -2828,
  -1, 2829, 4000, 2828,  0, -2828, -4000, -2828,
};
static ADT_PCM16 Drain [MAX_SAMPLES_PER_MS*2][MAX_CHANS_PER_FRAME];     // Dummy receive buffer
#pragma DATA_SECTION (Drain, "FAST_DATA_SECT:TDM")
#pragma DATA_ALIGN   (Drain, CACHE_L2_LINE_SIZE)

static ADT_PCM16 Dummy [MAX_SAMPLES_PER_MS*2][MAX_CHANS_PER_FRAME];     // Dummy transmit buffer
#pragma DATA_SECTION (Dummy, "FAST_DATA_SECT:TDM")
#pragma DATA_ALIGN   (Dummy, CACHE_L2_LINE_SIZE)

static ADT_PCM16 BroadcastBufr [MAX_SAMPLES_PER_MS];
#pragma DATA_SECTION (BroadcastBufr, "FAST_DATA_SECT:TDM")
#pragma DATA_ALIGN   (BroadcastBufr,  CACHE_L2_LINE_SIZE)

ADT_UInt16 inCaptureBuff [900];
CircBufInfo_t captureTDMIn = {
  inCaptureBuff, sizeof (inCaptureBuff)/2, 0, 0, 0
};
#pragma DATA_SECTION (inCaptureBuff,  "FAST_DATA_SECT:TDM")
#pragma DATA_ALIGN   (inCaptureBuff,  CACHE_L2_LINE_SIZE)

ADT_UInt16 outCaptureBuff [900];
CircBufInfo_t captureTDMOut = {
  outCaptureBuff, sizeof (outCaptureBuff)/2, 0, 0, 0
};
#pragma DATA_SECTION (outCaptureBuff, "FAST_DATA_SECT:TDM")
#pragma DATA_ALIGN   (outCaptureBuff,  CACHE_L2_LINE_SIZE)


static void sineBuffer (unsigned int port, ADT_PCM16 *Buffer) {
   int i, dmaSltCnt;

   // Populate dummy buffer with 1 kHz SINE wave on all channels
   if (NUM_TDM_PORTS <= port) return;

   dmaSltCnt = sysConfig.maxSlotsSupported[port];

   for (i=0; i<dmaSltCnt; i++) {
      memcpy (Buffer, SineTableM12, sizeof (SineTableM12));
      Buffer += (sysConfig.samplesPerMs * 2);
   }

}
#if 0
int TogglePCMInSignal (unsigned int port) {

   if (NUM_TDM_PORTS <= port) return 0;

   sineBuffer (port, (ADT_PCM16 *) &Dummy);

   if (DmaRxBuff[port] == RxDMABuffers[port]) {
       DmaRxBuff[port] = (ADT_PCM16 *) &Dummy;
       return 1;
   }
   // Restore
   DmaRxBuff[port] = RxDMABuffers[port];
   return 0;
}

int TogglePCMOutSignal (unsigned int port) {

   if (NUM_TDM_PORTS <= port) return 0;

   if (DmaTxBuff[port] == TxDMABuffers[port]) {
       sineBuffer (port, TxDMABuffers[port]);
       DmaTxBuff[port] = (ADT_PCM16 *) &Drain;
       return 1;
   }
   // Restore with initial zeroes
   DmaTxBuff[port] = TxDMABuffers[port];
   memset (TxDMABuffers[port], 0, sysConfig.dmaBufferSize[port] * 2);
   return 0;
}
#endif
static void FixedPatternTx (ADT_PCM16 *TxDmaBufr) {
   ADT_PCM16 *DmaBufr;
   int i;

   if (dma.TxPatternSlot1) {
      DmaBufr = TxDmaBufr;
      for (i=0; i<sysConfig.samplesPerMs; i++) *DmaBufr++ = i;//dma.TxPatternSlot1;
   }

   if (dma.TxSineSlot == 0) {
      memcpy (TxDmaBufr, SineTableM12, sysConfig.samplesPerMs * 2);
   }
   if (dma.captureTx == 0 && dma.captureRx != 0xffff) {
      copyLinearToCirc (&ApiBlock.DmaSwiCnt, &captureTDMOut, 1);
      copyLinearToCirc (TxDmaBufr, &captureTDMOut, sysConfig.samplesPerMs);
      captureTDMOut.TakeIndex = captureTDMOut.PutIndex;
   }

}
static void FixedPatternRx (ADT_PCM16 *RxDmaBufr) {
   ADT_PCM16 *DmaBufr;
   int i;

   // Overwrite channel(s) output with a fixed (non-zero) value
   if (dma.BroadcastSlot == 0)
      memcpy (BroadcastBufr, RxDmaBufr, sizeof (BroadcastBufr));
   else if (dma.BroadcastSlot != 0xffff)
      memcpy (RxDmaBufr, BroadcastBufr, sizeof (BroadcastBufr));

   if (dma.RxPatternSlot1) {
      DmaBufr = RxDmaBufr;
      for (i=0; i<sysConfig.samplesPerMs; i++) *DmaBufr++ = dma.RxPatternSlot1;
   }
   if (dma.captureRx == 0 && dma.captureTx != 0xffff) {
      copyLinearToCirc (&ApiBlock.DmaSwiCnt, &captureTDMIn, 1);
      copyLinearToCirc (RxDmaBufr, &captureTDMIn, sysConfig.samplesPerMs);
      captureTDMIn.TakeIndex = captureTDMIn.PutIndex;
   }
   if (FrameStamp) {
      *RxDmaBufr = (ADT_PCM16) ApiBlock.DmaSwiCnt;
   }
}

inline void CheckTxPointers (CircBufInfo_t *circ, ADT_PCM16 *dma) {
   LogTransfer (0x100C1, circ, &circ->pBufrBase[circ->TakeIndex], 0, dma, sysConfig.samplesPerMs);
   AppErr ("TxPointerError", (sysConfig.samplesPerMs < circ->BufrSize) &&
                             (getAvailable (circ) < sysConfig.samplesPerMs));
}
inline void CheckRxPointers (CircBufInfo_t *circ, ADT_PCM16 *dma) {
   LogTransfer (0x1001C, 0, dma, circ, &circ->pBufrBase[circ->PutIndex], sysConfig.samplesPerMs);
   AppErr ("RxPointerError", (sysConfig.samplesPerMs < circ->BufrSize) &&
                             (getFreeSpace (circ) < sysConfig.samplesPerMs));
}
#endif
//
#undef  _LOG
#define _LOG
#if defined (_DEBUG) || defined (_LOG)    // Event logging
int evtLogIdx = 0;
int lastEvtTime = 0;
struct evtLog {
   int type;
   int time;
   ADT_UInt64 evt;
   void* que;
   void* ele;
} evtLog[258];
#if 0  // logEvt
void logEvt (int type, ADT_UInt64 evt) {
  int mask, time;
  if (2 < dmaStats.errors) return;
  time = CLK_gethtime ();
  mask = HWI_disable ();
  evtLog[evtLogIdx].type = (type + 1);
  evtLog[evtLogIdx].time = time - lastEvtTime;
  lastEvtTime = time;
  evtLog[evtLogIdx].evt  = evt;
  evtLog[evtLogIdx].que  = NULL;
  evtLog[evtLogIdx].ele  = NULL;
  evtLogIdx++;
  evtLogIdx &= 0xff;
  evtLog[evtLogIdx].evt  = 0xffffffff;
  HWI_restore (mask);

}
#else
#define logEvt(type,evt) 
#endif
#if 1  // logQue
void logQue (int type, void* que, DMABuffInfo*  ele) {
  int mask, time;
  if (2 < dmaStats.errors) return;
  time = CLK_gethtime ();
  mask = HWI_disable ();
  evtLog[evtLogIdx].type = (type + 1) << 8;
  evtLog[evtLogIdx].time = time - lastEvtTime;
  lastEvtTime = time;
  if (ele == NULL) evtLog[evtLogIdx].evt  = 0;
  else             evtLog[evtLogIdx].evt  = ele->evt;
  evtLog[evtLogIdx].que  = que;
  evtLog[evtLogIdx].ele  = ele;
  evtLogIdx++;
  evtLogIdx &= 0xff;
  evtLog[evtLogIdx].type  = 0xffffffff;
  HWI_restore (mask);
}
void logDMARqst (int type, int edmaChn) {
  ADT_UInt64 evt;
  evt = 1;
  evt <<= edmaChn;
  logEvt (type, evt);
}
#else
#define logQue(type,que,ele) 
#define logDMARqst(type,edmaChn)
#endif
#else
#define logEvt(type,evt) 
#define logQue(type,que,ele) 
#define logDMARqst(type,edmaChn)
#endif

#ifdef _DEBUG  // Check pointers
   void CheckTxPointers (ADT_UInt64* dma, CircBufInfo_t **CircBufr, int slotCnt) {
      register CircBufInfo_t *circ;
      int   avail;
      circ = *CircBufr++;
      while (slotCnt--) {
         if (circ != pDrain) {
            avail = getAvailable(circ);
            LogTransfer (0x100C1, circ, &circ->pBufrBase[circ->TakeIndex], 0, dma, sysConfig.samplesPerMs);
            AppErr ("TxPointerError", (avail < sysConfig.samplesPerMs)?avail : ADT_FALSE);
            AppErr ("TxAlignError", circ->BufrSize % sysConfig.samplesPerMs);
         }
         dma += (sysConfig.samplesPerMs / 4);
         circ = *CircBufr++;
      }
   }
   void CheckRxPointers (ADT_UInt64* dma, CircBufInfo_t **CircBufr, int slotCnt) {
      register CircBufInfo_t *circ;
      int free;
      circ = *CircBufr++;
      while (slotCnt--) {
         if (circ != pSink) {
            LogTransfer (0x1001C, 0, dma, circ, &circ->pBufrBase[circ->PutIndex], sysConfig.samplesPerMs);
            free = getFreeSpace (circ);
            AppErr ("RxPointerError", (free < sysConfig.samplesPerMs)?free : ADT_FALSE);
            AppErr ("RxAlignError", circ->BufrSize % sysConfig.samplesPerMs);
         }
         dma += (sysConfig.samplesPerMs / 4);
         circ = *CircBufr++;
      }
   }

#else
   #define CheckRxPointers(dma, circ, sltCnt)
   #define CheckTxPointers(dma, circ, sltCnt)
#endif

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{  DMA queuing routines  (FIFO)
#pragma CODE_SECTION (initFIFO, "SLOW_PROG_SECT")
static void initFIFO (FIFO* fifo) {
   fifo->first = fifo->last = NULL;
   logQue (0, fifo, NULL);
}
#pragma CODE_SECTION (pushFIFO, "FAST_PROG_SECT")
static void pushFIFO (FIFO* fifo, DMABuffInfo* element) {
   DMABuffInfo* last;

   element->next = NULL;

   last = fifo->last;
   if (last != NULL)        last->next = element;

   fifo->last = element;
   if (fifo->first == NULL) fifo->first = element;
   logQue (1, fifo, element);

}
#pragma CODE_SECTION (popFIFO, "FAST_PROG_SECT")
static DMABuffInfo* popFIFO (FIFO* fifo) {
   DMABuffInfo* first;
   first = fifo->first;

   if (first != NULL) {
      fifo->first = first->next;
      if (fifo->first == NULL) fifo->last = NULL;
   } else {
      logDMA ("FIFO empty...", -1, (ADT_UInt64) fifo);
      AppErr ("FIFO empty", ADT_TRUE);
   }
   logQue (2, fifo, first);
   return first;
}

#pragma CODE_SECTION (initDMABuff, "SLOW_PROG_SECT")
static void initDMABuff (DMABuffInfo* element, ADT_UInt64 evt, void* buffAddr, int pongI8) {
   ADT_UInt8* dmaAddr;

   dmaAddr = buffAddr;

   element->next  = NULL;
   element->buffAddr = dmaAddr + pongI8;
   element->state = idle;
   element->pong  = pongI8;
   element->evt   = evt;
   DmaActive     &= ~evt;
   DmaBuffActive &= ~evt;


}
#pragma CODE_SECTION (initDMABuffInfo, "SLOW_PROG_SECT")
static void initDMABuffInfo (int port) {
   logDMA ("Port Init....", port,0);

   initFIFO (&RxDMAQ[port]);
   initFIFO (&RxTSIPQ[port]);
   initFIFO (&TxDMAQ[port]);
   initFIFO (&TxTSIPQ[port]);

   DmaPingBufferI8 [port] =  MaxDmaSlots[port] * 16 * sizeof (ADT_UInt16);

   initDMABuff (&RxDMAPing[port], DmaRxPingMask[port],    RxTransBuffers[port], 0);
   initDMABuff (&RxDMAPong[port], DmaRxPingMask[port]<<1, RxTransBuffers[port], DmaPingBufferI8[port]);
   initDMABuff (&TxDMAPing[port], DmaTxPingMask[port],    TxTransBuffers[port], 0);
   initDMABuff (&TxDMAPong[port], DmaTxPingMask[port]<<1, TxTransBuffers[port], DmaPingBufferI8[port]);
   
   pushFIFO (&RxTSIPQ[port], &RxDMAPing[port]);
   pushFIFO (&RxTSIPQ[port], &RxDMAPong[port]);

   pushFIFO (&TxTSIPQ[port], &TxDMAPing[port]);
   pushFIFO (&TxTSIPQ[port], &TxDMAPong[port]);
}

#pragma CODE_SECTION (clearDMABuffInfo, "SLOW_PROG_SECT")
static void clearDMABuffInfo (int port) {

   initFIFO (&RxDMAQ[port]);
   initFIFO (&RxTSIPQ[port]);
   initFIFO (&TxDMAQ[port]);
   initFIFO (&TxTSIPQ[port]);

   initDMABuff (&RxDMAPing[port], 0, 0, 0);
   initDMABuff (&RxDMAPong[port], 0, 0, 0);
   initDMABuff (&TxDMAPing[port], 0, 0, 0);
   initDMABuff (&TxDMAPong[port], 0, 0, 0);
   
}

//}
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{  DMA setup  parameter init / enable / disable
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ initRxTCCParams - Initialize the EDMA parameter ram for receive channels
//
// FUNCTION
//     Initializes EDMA parameter ram for EDMA channels that are driven by receive events
//     This routine links two EDMA parameter blocks to process two TDM frames of data.
//
// Inputs
//    port  - TDM port ID
//    pong  - ADT_TRUE = initialize pong buffer.  ADT_FALSE = initial ping buffer
//
//  Outputs
//    EDMA_PARAM registers for port
//
// RETURNS
//  none
//}
#pragma CODE_SECTION (initRxTCCParams, "SLOW_PROG_SECT")
static void initRxTCCParams (int port, int pong) {

   dma3Opts_t dmaOpts;
   ADT_UInt32 BcntAcnt, BcntLink;
   ADT_UInt32 BOffsets, COffsets;
   ADT_UInt32 srcAddr, dstAddr;

   ADT_UInt32 Acnt, ADstCnt;            // Count of bytes in A dimension
   ADT_UInt32 Bcnt, Ccnt;               // Count of elements in B and C dimensions
   ADT_UInt32 BSrcOffset, CSrcOffset;   // Offset of to next element in B and C dimensions
   ADT_UInt32 BDstOffset, CDstOffset;   // Offset of to next element in B and C dimensions

   ADT_UInt32* paramAddr;    // Parameter block addresses
   ADT_UInt16 edmaChn;
   
   if (NUM_TDM_PORTS <= port) return;

   // Get event channel from static table
   edmaChn = DmaRxPingCh[port];
   if (pong) edmaChn++;

   // Calculate offsets into parameter tables
   paramAddr = (ADT_UInt32*) (EDMA_PARAM_ADDR + (edmaChn * PARAM_SIZE));

   //------------- Set up parameter table entries
   // Options register
   dmaOpts.TransferOptions = 0;
   dmaOpts.Bits.SAM        = 0;     // src address not a FIFO
   dmaOpts.Bits.DAM        = 0;     // dst address not a FIFO
   dmaOpts.Bits.SYNCDIM    = 1;     // transfer A bytes * B frames per request
   dmaOpts.Bits.STATIC     = 0;     // allow parameter field update
   dmaOpts.Bits.FWID       = 0;     // FIFO width = N/A
   dmaOpts.Bits.TCCMOD     = 0;     // notify transfer complete upon full transfer
   dmaOpts.Bits.TCC        = edmaChn;   // chain to self to allow continuation 
                                        // after intermediate transfer completions
   dmaOpts.Bits.TCINTEN    = 1;     // Interrupt enabled on final transfer
   dmaOpts.Bits.ITCINTEN   = 0;     // Interrupt disabled on intermediate transfers
   dmaOpts.Bits.TCCHEN     = 0;     // Chaining disabled on final transfer
   dmaOpts.Bits.ITCCHEN    = 1;     // Chaining enabled on intermediate transfers

   // Number of elements in each dimension
   //   Acnt    = is the size of the element in the TSIP buffer (source).
   //   ADstCnt = is the size of the element in the DMA transfer buffer.
   //   Bcnt    = is the number of elements to transfer per TDM frame.

   //  NOTE: In tsip8BitWB mode, the DMA transfer buffer is 8-bits per sample; in
   //        all other modes the DMA transer buffer is 16-bits per sample.

   switch (getTSIPMode ()) {
   case tsip8BitNB:  Acnt = 1; ADstCnt = 2;  break;
   case tsip8BitWB:  Acnt = 2; ADstCnt = 2;  break;
   case tsip16BitNB: Acnt = 2; ADstCnt = 2;  break;
   case tsip16BitWB: Acnt = 4; ADstCnt = 4;  break;
   default: AppErr ("Unknown TSIP Mode", ADT_TRUE); break;
   }
   Bcnt = TDM_frames_per_interrupt;  // B dimension is frames per millisecond
   Ccnt = MaxDmaSlots[port];         // C dimension is number of 'primary' slots

   srcAddr = ((ADT_UInt32) RxTsipBuffers[port]) + TSIP_SYNC_I8;  // Arranged N slots per 8 TDM frames
   dstAddr = (ADT_UInt32) addCoreOffset (RxTransBuffers[port]);  // Arranged 1 or 2 bytes per 8 or 16 samples per n channels

   // Offset from S11F1 to S11F2 (see diagram at top)
   BSrcOffset = TsipFallocI8[port];
   BDstOffset = ADstCnt;

   if (pong) {
      srcAddr += Bcnt * BSrcOffset;
      dstAddr += DmaPingBufferI8 [port];
   }
   srcAddr |= 0x10000000;
   dstAddr |= 0x10000000;
   // Offset from S11F1 and S21F1 (see diagram at top)
   CSrcOffset = Acnt;
   CDstOffset = (ADstCnt * Bcnt);

   // Shift B and C offsets into destination field
   BOffsets  = (BDstOffset << 16) | BSrcOffset;
   COffsets  = (CDstOffset << 16) | CSrcOffset;
   BcntAcnt = (Bcnt << 16) | Acnt;

   BcntLink = 0xffff;

   REG_WR ( paramAddr,    dmaOpts.TransferOptions);
   REG_WR ((paramAddr+1), srcAddr);
   REG_WR ((paramAddr+2), BcntAcnt);
   REG_WR ((paramAddr+3), dstAddr);
   REG_WR ((paramAddr+4), BOffsets);
   REG_WR ((paramAddr+5), BcntLink);
   REG_WR ((paramAddr+6), COffsets);
   REG_WR ((paramAddr+7), Ccnt);
   logDMARqst (2, edmaChn);

}
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ initTxTCCParams - Initialize the EDMA parameter ram for TDM bus transmit channels
//
// FUNCTION
//  Initializes EDMA parameter ram for EDMA channels that are driven by transmit events
//
// Inputs
//    port  - TDM port ID
//    pong  - ADT_TRUE = initialize pong buffer.  ADT_FALSE = initial ping buffer
//
//  Outputs
//    EDMA_PARAM registers for port
//
// RETURNS
//  none
//}
#pragma CODE_SECTION (initTxTCCParams, "SLOW_PROG_SECT")
static void initTxTCCParams (int port, int pong) {

   dma3Opts_t dmaOpts;
   ADT_UInt32 BcntAcnt, BcntLink;
   ADT_UInt32 BOffsets, COffsets;
   ADT_UInt32 srcAddr, dstAddr;


   ADT_UInt32 Acnt, ASrcCnt;            // Count of bytes in A dimension
   ADT_UInt32 Bcnt, Ccnt;               // Count of elements in B and C dimensions
   ADT_UInt32 BSrcOffset, CSrcOffset;   // Offset of to next element in B and C dimensions
   ADT_UInt32 BDstOffset, CDstOffset;   // Offset of to next element in B and C dimensions

   ADT_UInt32* paramAddr;    // Parameter block addresses
   ADT_UInt16  edmaChn;

   if (NUM_TDM_PORTS <= port) return;

   // Get event channel from static table
   edmaChn = DmaTxPingCh[port];
   if (pong) edmaChn++;

   // Calculate offsets into parameter tables
   paramAddr = (ADT_UInt32*) (EDMA_PARAM_ADDR + (edmaChn * PARAM_SIZE));

   //------------- Set up parameter table entries
   // Options register
   dmaOpts.TransferOptions = 0;
   dmaOpts.Bits.SAM        = 0;     // src address not a FIFO
   dmaOpts.Bits.DAM        = 0;     // dst address not a FIFO
   dmaOpts.Bits.SYNCDIM    = 1;     // transfer A bytes * B frames per request
   dmaOpts.Bits.STATIC     = 0;     // allow parameter field update
   dmaOpts.Bits.FWID       = 0;     // FIFO width = N/A
   dmaOpts.Bits.TCCMOD     = 0;     // notify transfer complete upon full transfer
   dmaOpts.Bits.TCC        = edmaChn;   // chain to self to allow continuation 
                                        // after intermediate transfer completions
   dmaOpts.Bits.TCINTEN    = 1;     // Interrupt enabled on final transfer
   dmaOpts.Bits.ITCINTEN   = 0;     // Interrupt disabled on intermediate transfers
   dmaOpts.Bits.TCCHEN     = 0;     // Chaining disabled on final transfer
   dmaOpts.Bits.ITCCHEN    = 1;     // Chaining enabled on intermediate transfers

   // Number of elements in each dimension
   //   Acnt    = is the size of the element in the TSIP buffer (destination).
   //   ASrcCnt = is the size of the element in the DMA transfer buffer.
   //   Bcnt    = is the number of elements to transfer per TDM frame.
   
   //  NOTE: In tsip8BitWB mode, the DMA transfer buffer is 8-bits per sample; in
   //        all other modes the DMA transer buffer is 16-bits per sample.
   switch (getTSIPMode ()) {
   case tsip8BitNB:  Acnt = 1; ASrcCnt = 2; break;
   case tsip8BitWB:  Acnt = 2; ASrcCnt = 2; break;
   case tsip16BitNB: Acnt = 2; ASrcCnt = 2; break;
   case tsip16BitWB: Acnt = 4; ASrcCnt = 4; break;
   default: AppErr ("Unknown TSIP Mode", ADT_TRUE); break;
   }

   Bcnt = TDM_frames_per_interrupt;  // B dimension is frames per millisecond
   Ccnt = MaxDmaSlots[port];         // C dimension is number of 'primary' slots

   srcAddr = (ADT_UInt32) addCoreOffset (TxTransBuffers[port]);  // Arranged 1 or 2 bytes per 8 or 16 samples per n channels
   dstAddr = ((ADT_UInt32) TxTsipBuffers[port]) + TSIP_SYNC_I8;  // Arranged 1 byte per N slots per 8 frames

   // Offset from S11F1 to S11F2 (see diagram at top)
   BSrcOffset = ASrcCnt;
   BDstOffset = TsipFallocI8[port];

   if (pong) {
      srcAddr += DmaPingBufferI8 [port];
      dstAddr += Bcnt * BDstOffset;
   }

   // Offset from S11F1 and S21F1 (see diagram at top)
   CSrcOffset = (ASrcCnt * Bcnt);
   CDstOffset = Acnt;

   // Shift B and C offsets into destination field
   BOffsets  = (BDstOffset << 16) | BSrcOffset;
   COffsets  = (CDstOffset << 16) | CSrcOffset;
   BcntAcnt = (Bcnt << 16) | Acnt;

   BcntLink = 0xffff;
   
   srcAddr |= 0x10000000;
   dstAddr |= 0x10000000;

   REG_WR ( paramAddr,    dmaOpts.TransferOptions);
   REG_WR ((paramAddr+1), srcAddr);
   REG_WR ((paramAddr+2), BcntAcnt);
   REG_WR ((paramAddr+3), dstAddr);
   REG_WR ((paramAddr+4), BOffsets);
   REG_WR ((paramAddr+5), BcntLink);
   REG_WR ((paramAddr+6), COffsets);
   REG_WR ((paramAddr+7), Ccnt);

   logDMARqst (2, edmaChn);
}
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ nullTCCParams - Initialize the EDMA parameter ram for receive channels
//
// FUNCTION
//  Initializes EDMA parameter ram for EDMA channels that are driven by receive events
//
// Inputs
//    port        - McBSPn or McASPn
//    bitwidth        - width of serial word (8, 16, or 32)
//    slotsPerFrame    - Timeslots per tdm frame
//    numSampsPerChan - Samples per channel per DMA interrupt
//
//  Outputs
//    eerl            - Event enable low
//    eerh            - Event enable high
//    cierl           - Channel interrupt enable low
//
// RETURNS
//  none
//}
#pragma CODE_SECTION (nullTCCParams, "SLOW_PROG_SECT")
static void nullTCCParams (int edmaChn) {

   ADT_UInt32* paramAddr;    // Parameter block addresses
   int port;

   // Calculate offsets into ping parameter tables. 
   // Zero both ping and following pong buffers
   paramAddr = (ADT_UInt32*) (EDMA_PARAM_ADDR + (edmaChn * PARAM_SIZE));
   for (port=0; port<16; port++) REG_WR (paramAddr++, 0);
   
   logDMARqst (3, edmaChn);

}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ enableDMA
//
// FUNCTION
//   Enables Tx and Rx DMA for specified TDM bus
//
// RETURNS
//    Bit mask of events that are generated at interrupt
//}
#pragma CODE_SECTION (enableDMA, "FAST_PROG_SECT")
ADT_UInt32 enableDMA (int port) {

   ADT_UInt64 chnBits;

   if (NUM_TDM_PORTS <= port) return 0;

   logDMA ("Dma Enable...", port, 0);
   AppErr ("TSIP alignment", TsipFallocI8[port] & 3);

   // Identify the DMA events used by the port.
   chnBits = (DmaRxEvents[port] | DmaTxEvents[port]);
   enabledDmaEvts |= chnBits;

   AppErr ("ShadowBits", ((ShadowRegionBits & chnBits) != chnBits));
   // disable edma events and transfer completion interrupts for port
   REG64_WR (EDMA_SHADOW_IECR, chnBits);    // disable completion interrupts
   REG64_WR (EDMA_SHADOW_EECR, chnBits);    // disable events

   // Assign DMA channels to their designated transfer controller
   assignToTC (DmaRxPingCh[port],   TDM_TC+1);
   assignToTC (DmaRxPingCh[port]+1, TDM_TC+1);

   assignToTC (DmaTxPingCh[port],   TDM_TC);
   assignToTC (DmaTxPingCh[port]+1, TDM_TC);

   // clear all indications of edma events and event interrupts
   REG64_WR (EDMA_SHADOW_SPCR, chnBits);    // Clear secondary event notifications
   REG64_WR (EDMA_EMCR,  chnBits);    // Clear missed event notifications
   REG64_WR (EDMA_SHADOW_EPCR, chnBits);    // Clear event notifications
   REG64_WR (EDMA_SHADOW_IPCR, chnBits);    // Clear interrupt notifications
   logEvt (0, chnBits);

   DMAClearErrors (0);

   // enable edma events and transfer completion interrupts for port
   REG64_WR (EDMA_SHADOW_IESR, chnBits);    // Enable completion interrupts
   REG64_WR (EDMA_SHADOW_EESR, chnBits);    // Enable events

   initDMABuffInfo (port);

   // NOTE: EnableDMA requires events that are reported to PCM processing.
   //       In the case of TSIP, the TSIP interrupt events are reported instead of the DMA events.
   return (RxEvents[port] | TxEvents[port]);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ disableDMA
//
// FUNCTION
//   This function resets the EDMA controller
//
// RETURNS
//   
//   Disabled transfer control bits 
//}
#pragma CODE_SECTION (disableDMA, "FAST_PROG_SECT")
ADT_UInt32 disableDMA (int port) {

   ADT_UInt64 chnBits;

   if (NUM_TDM_PORTS <= port) return 0;

   logDMA ("Dma Disable..", port, 0);

   MultiCoreCpuUsage[port] = 0;
   MultiCoreCpuPeakUsage[port] = 0;

   MultiCoreCpuUsage[port + 3] = 0;
   MultiCoreCpuPeakUsage[port + 3] = 0;

   // Disable DMA parameters   
   nullTCCParams (DmaRxPingCh[port]);
   nullTCCParams (DmaTxPingCh[port]);

   chnBits = (DmaRxEvents[port] | DmaTxEvents[port]);
   enabledDmaEvts &= ~chnBits;

   AppErr ("ShadowBits", ((ShadowRegionBits & chnBits) != chnBits));

   RxEvents[port] = 0;
   TxEvents[port] = 0;

   // disable all edma events and interrupts for port
   REG64_WR (EDMA_SHADOW_EECR, chnBits);   // Disable event notifications
   REG64_WR (EDMA_SHADOW_IECR, chnBits);   // Disable event interrupts

   // clear all indications of edma events and event interrupts
   REG64_WR (EDMA_SHADOW_SPCR, chnBits);   // Clear secondary event notifications
   REG64_WR (EDMA_EMCR, chnBits);    // Clear missed event notifications 
   REG64_WR (EDMA_SHADOW_EPCR, chnBits);   // Clear event notifications
   REG64_WR (EDMA_SHADOW_IPCR, chnBits);   // Clear interrupt notifications
   logEvt (0, chnBits);

   return 0;
}

//---------------------------------------------------
//{  DMAIntialize 
//
// FUNCTION
//   - Disables all DMA; initializes interrupt variables
//}
#pragma CODE_SECTION (DMAInitialize, "SLOW_PROG_SECT")
void DMAInitialize () {
   int port;

   InitTSIPMode ();

   C64_disableIER (1 << HWI_DMA_COMPLETE);
   C64_disableIER (1 << HWI_DMA_ERROR);
//SS DEBUG TSIP   C64_disableIER (1 << HWI_DMA_COMPLETE);
//SS DEBUG TSIP   C64_disableIER (1 << HWI_COMBINED_INT);
//   C64_clearIFR   (1 << HWI_DMA_COMPLETE);
//   C64_clearIFR   (1 << HWI_COMBINED_INT);

   if (DSPCore != tdmCore) return;

   memset (&TDMUsage, 0, sizeof (TDMUsage));
   if (!skipErrors) clearDmaLog();

   if (dmaFirstTime) {
#if 0
      // Remove all interrupts from combiner
      REG_WR (INTR_CMB,      0xFFFFFFFF);
      REG_WR (INTR_CMB + 4,  0xFFFFFFFF);
      REG_WR (INTR_CMB + 8,  0xFFFFFFFF);
      REG_WR (INTR_CMB + 12, 0xFFFFFFFF);

      // Map TDM DMA interrupt completion signal to GpakDmaIsr
      HWI_dispatchPlug (HWI_DMA_COMPLETE, (Fxn) &GpakDmaIsr, -1, NULL);
      HWI_eventMap     (HWI_DMA_COMPLETE, TDM_CompleteSgnl);
      clearInterrupt   (TDM_CompleteSgnl);

      // Map combined interrupt signals to GpakCombinedIsr
      HWI_dispatchPlug (HWI_COMBINED_INT, (Fxn) &GpakCombinedIsr, -1, NULL);
      HWI_eventMap     (HWI_COMBINED_INT, CombinedEventIntr);
      clearInterrupt   (CombinedEventIntr);
#endif
      setup_edma_interrupts((Hwi_FuncPtr)GpakDmaIsr, DSPCore);
      setup_edmaErr_interrupts((Hwi_FuncPtr)GpakDMAErrIsr, DSPCore);
      setup_Combined_TSIP_interrupts((Hwi_FuncPtr)GpakTSIP_EventIsr, DSPCore);
#if 1
      DMAClearAll ();
      memset (&dmaStats, 0, sizeof (dmaStats));

      ShadowRegionBits = 0;

      // max of 16 samples per 1 millisecond.
      for (port=0; port<NUM_TDM_PORTS; port++) {
         DmaRxEvents   [port] = (ADT_UInt64) 3 << DmaRxPingCh [port];
         DmaRxPingMask [port] = (ADT_UInt64) 1 << DmaRxPingCh [port];
         DmaTxEvents   [port] = (ADT_UInt64) 3 << DmaTxPingCh [port];
         DmaTxPingMask [port] = (ADT_UInt64) 1 << DmaTxPingCh [port];
         ShadowRegionBits |= DmaRxEvents[port] | DmaTxEvents[port];
      }
      dmaFirstTime = ADT_FALSE;

      // Assign TSIP dma events to shadow region
      REG64_WR (EDMA_SHADOW_ASGN, ShadowRegionBits);
   }
   dmaStats.initCnt++;

   for (port=0; port<NUM_TDM_PORTS; port++)
      disableDMA (port);

   DmaActive     = 0;
   DmaBuffActive = 0;

   // Assign a priority to the TDM DMA queues
   assignPriority (TDM_TC,     TDM_Priority);
   assignPriority (TDM_TC + 1, TDM_Priority);

   // Clear pending exceptions
   DMAClearAll ();
   for (port = 0; port < NUM_TDM_PORTS; port++) clearDMABuffInfo (port);

   // Enable TDM errors as combined events
 //  assignErrorInterrupt (TDM_CCErrSgnl);
 //  assignErrorInterrupt (TDM_Que0ErrSgnl);   REG_WR (TC0_ERR_STAT_ENBL, 0x5);
  // assignErrorInterrupt (TDM_Que1ErrSgnl);   REG_WR (TC1_ERR_STAT_ENBL, 0x5);
  // assignErrorInterrupt (TDM_Que2ErrSgnl);   REG_WR (TC2_ERR_STAT_ENBL, 0x5);

//   C64_clearIFR  (1 << HWI_DMA_COMPLETE);
//   C64_clearIFR  (1 << HWI_COMBINED_INT);
//SS DEBUG TSP   C64_enableIER (1 << HWI_DMA_COMPLETE);
//SS DEBUG TSP   C64_enableIER (1 << HWI_COMBINED_INT);
   REG_WR (TC0_ERR_STAT_ENBL, 0x5);
   REG_WR (TC1_ERR_STAT_ENBL, 0x5);
   REG_WR (INTR_XSTAT_CLR, 1);     // Clear pending exceptions
   C64_enableIER (1 << HWI_DMA_COMPLETE);
   C64_enableIER (1 << HWI_DMA_ERROR);
#endif
}
//}
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ DmaUtils: issueDMA, getBuff, freeBuff
static inline markDMAInactive (ADT_UInt64 evt) {
   int mask;

   dmaStats.Cmplts++;

   mask = HWI_disable ();
   DmaActive &= ~evt;
   HWI_restore (mask);
}

static inline DMABuffInfo* getTxBuff (int port) {
   DMABuffInfo* buff;
   ADT_UInt64 evt;
   int mask;

   // SWI.CopyDmaTxBuffers started.  Start circ to linear copy.
   mask = HWI_disable ();
   buff = popFIFO (&TxTSIPQ[port]);
   if (buff != NULL) {
      evt = buff->evt;
      dmaError = (evt & DmaBuffActive);
      DmaBuffActive |= evt;
      logDMA ("Tx GetBuff...", port, evt);
      AppErr ("TxDMA buffer", dmaError);

      buff->state = circToLinear;
   }
   HWI_restore (mask);
   return buff;
}

#pragma CODE_SECTION (issueTxDMA, "FAST_PROG_SECT")
static void issueTxDMA (int port, DMABuffInfo* buff) {
   ADT_UInt64 evt;
   int mask;

   if (buff == NULL) return;

   dmaStats.Rqsts++;

   // SWI.CopyDmaTxBuffers completed. Start DMA channel mux.
   mask = HWI_disable ();
   evt = buff->evt;

   dmaError = (evt & DmaBuffActive);
   DmaActive |= evt;
   logDMA ("Tx DmaIssue..", port, evt);
   AppErr ("TxDMA buffer", dmaError);

   initTxTCCParams (port, buff->pong);
   REG64_WR (EDMA_SHADOW_EPSR, evt);  // Start DMA

   buff->state = linearToTsipDma;
   pushFIFO (&TxDMAQ[port], buff);

   HWI_restore (mask);
   return;
}
static inline freeTxBuff (int port) {
   // DMA Tx interrupt complete.  Free buffer.
   DMABuffInfo* buff;
   ADT_UInt64   evt;
   int mask;

   mask = HWI_disable ();
   buff = popFIFO (&TxDMAQ[port]);
   if (buff != NULL) {
      evt = buff->evt;

      dmaError = (DmaActive & evt) || (!(DmaBuffActive & evt));
      DmaBuffActive &= ~evt;
      logDMA ("Tx FreeBuff..", port, evt);
      AppErr ("TxDma buffer", dmaError);

      buff->state = waitingForTSIPTx;
      pushFIFO (&TxTSIPQ[port], buff);
   }
   HWI_restore (mask);
   return;
}


#pragma CODE_SECTION (issueRxDMA, "FAST_PROG_SECT")
void issueRxDMA (int port) {
   DMABuffInfo* buff;
   ADT_UInt64 evt;
   int mask;

   dmaStats.Rqsts++;

   // TSIP Rx interrput. Start DMA channel demux.
   mask = HWI_disable ();
   buff = popFIFO (&RxTSIPQ[port]);
   if (buff != NULL) {
      evt = buff->evt;

      dmaError = (evt & DmaActive) || (evt & DmaBuffActive);
      DmaActive     |= evt;
      DmaBuffActive |= evt;
      logDMA ("Rx DmaIssue..", port, evt);
      AppErr ("TxDma buffer", dmaError);
      initRxTCCParams (port, buff->pong);
      REG64_WR (EDMA_SHADOW_EPSR, evt);   // Activate DMA.

      // Start DMA channel demux.
      buff->state = tsipToLinearDma;
      pushFIFO (&RxDMAQ[port], buff);
   } else {
      disableDMA (port);
   }
   HWI_restore (mask);
   return;
}

#pragma CODE_SECTION (getRxBuff, "FAST_PROG_SECT")
static DMABuffInfo* getRxBuff (int port) {
   DMABuffInfo* buff;
   int mask;

   // SWI.CopyDmaRxBuffers started.  Start circ to linear copy.
   mask = HWI_disable ();
   buff = popFIFO (&RxDMAQ[port]);
   if (buff != NULL) {

      logDMA ("Rx GetBuff...", port, buff->evt);
      AppErr ("RxDMA buffer",  (buff->evt & DmaActive));
      AppErr ("RxDMA buffer", !(buff->evt & DmaBuffActive));

      buff->state = linearToCirc;
   }
   HWI_restore (mask);
   return buff;
}

static inline freeRxBuff (int port, DMABuffInfo* buff) {
   // SWI.CopyDmaRxBuffers completed.  Free buffer.
   int mask;

   ADT_UInt64 evt;

   if (buff == NULL) return;

   mask = HWI_disable ();
   evt = buff->evt;

   dmaError = !(DmaBuffActive & evt);
   DmaBuffActive &= ~evt;
   logDMA ("Rx FreeBuff..", port, evt);
   AppErr ("RxDma buffer", dmaError);

   buff->state = waitingForTSIPRx;
   pushFIFO (&RxTSIPQ[port], buff);

   HWI_restore (mask);
   return;
}
//}
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ TxCopy. Circular buffer to DMA buffer transfers

void inline updateUsage (int port, ADT_UInt32 startTime) { 
   cpuUsage_t* cpuUsage;
   ADT_UInt32 TicksToComplete, Usage;
   cpuUsage = &TDMUsage[port];

   if (DSPTotalCores == 1) return;

   port += FRAME_TASK_CNT;    // Report CPU usage in core1's slot
   TicksToComplete = CLK_gethtime() - startTime;
   Usage = (TicksToComplete * 10)  / (CLK_countspms() / 100L);
   if (MultiCoreCpuPeakUsage[port] < Usage) MultiCoreCpuPeakUsage[port] = Usage;

   cpuUsage->accum += TicksToComplete;
   if (16 <= ++cpuUsage->cnt) {
      Usage = ((cpuUsage->accum / 16) * 10)  / (CLK_countspms() / 100L);
      MultiCoreCpuUsage[port] = Usage;
      cpuUsage->cnt = 0;
      cpuUsage->accum = 0;
   }
}

#pragma CODE_SECTION (adjustForSlips, "FAST_PROG_SECT")
void adjustForSlips (CircBufInfo_t **CircBufr, int slotCnt, int SlipSamps) {
   CircBufInfo_t *Circ;

   Circ = *CircBufr++;
   while (slotCnt--) {
      Circ->SlipSamps += SlipSamps;
      Circ = *CircBufr++;
   }
}

// channel buffer to 8-bit wideband DMA.  Samples in DMA buffer are 8-bits wide.
#pragma CODE_SECTION (circToDma8x16, "FAST_PROG_SECT")
inline void* circToDma8x16 (ADT_UInt8* restrict dmaBuff, ADT_UInt64* restrict chBuff) {
   // Read channel data into 64 bit register.  Store low byte of each successive 16-bit value into 8-bit dmabuffer.
   register ADT_UInt64 chData;
   chData = *chBuff++;
   *dmaBuff++ = (ADT_UInt8)  (chData & 0xff);
   *dmaBuff++ = (ADT_UInt8) ((chData >> 16) & 0xff);
   *dmaBuff++ = (ADT_UInt8) ((chData >> 32) & 0xff);
   *dmaBuff++ = (ADT_UInt8) ((chData >> 48) & 0xff);

   chData = *chBuff++;
   *dmaBuff++ = (ADT_UInt8)  (chData & 0xff);
   *dmaBuff++ = (ADT_UInt8) ((chData >> 16) & 0xff);
   *dmaBuff++ = (ADT_UInt8) ((chData >> 32) & 0xff);
   *dmaBuff++ = (ADT_UInt8) ((chData >> 48) & 0xff);

   chData = *chBuff++;
   *dmaBuff++ = (ADT_UInt8)  (chData & 0xff);
   *dmaBuff++ = (ADT_UInt8) ((chData >> 16) & 0xff);
   *dmaBuff++ = (ADT_UInt8) ((chData >> 32) & 0xff);
   *dmaBuff++ = (ADT_UInt8) ((chData >> 48) & 0xff);

   chData = *chBuff;
   *dmaBuff++ = (ADT_UInt8)  (chData & 0xff);
   *dmaBuff++ = (ADT_UInt8) ((chData >> 16) & 0xff);
   *dmaBuff++ = (ADT_UInt8) ((chData >> 32) & 0xff);
   *dmaBuff++ = (ADT_UInt8) ((chData >> 48) & 0xff);
   return (ADT_UInt64*) dmaBuff; 

}

//channel buffer to 8-bit wideband DMA.  16  8-bit samples on 16-bit (zero padded) alignment.
#pragma CODE_SECTION (circToDma8BitWB, "FAST_PROG_SECT")
void circToDma8BitWB (ADT_UInt64* dmaBuff, CircBufInfo_t **CircBufr, int slotCnt) {
   register CircBufInfo_t *Circ;

   Circ = *CircBufr++;
   while (slotCnt--)  {
      dmaBuff = circToDma8x16 ((ADT_UInt8*) dmaBuff, (ADT_UInt64*) &Circ->pBufrBase[Circ->TakeIndex]);

      Circ->TakeIndex += 16;
      if (Circ->BufrSize <= Circ->TakeIndex) Circ->TakeIndex = 0;
      Circ = *CircBufr++;
   }
}

#pragma CODE_SECTION (adjustTxCircBuffs, "MEDIUM_PROG_SECT")
void adjustTxCircBuffs (int slip, int slotCnt, CircBufInfo_t **TxCircBufrList) {
   CircBufInfo_t *Circ, **CircBufr;
   ADT_Word      SlipSamps;
   int k;

   SlipSamps = 0;
   if (slip) SlipSamps = sysConfig.samplesPerMs;

   // Adjust circular buffers to account for frame
   CircBufr = TxCircBufrList;
   Circ     = *CircBufr++;
   for (k = 0; k < slotCnt; k++, Circ = *CircBufr++)  {
      Circ->SlipSamps += SlipSamps;
      Circ->TakeIndex += sysConfig.samplesPerMs;
      if (Circ->BufrSize <= Circ->TakeIndex) Circ->TakeIndex = 0;
   }
}
#pragma CODE_SECTION (CopyDmaTxBuffers, "FAST_PROG_SECT")

ADT_Bool CopyDmaTxBuffers (int port, int slotCnt, CircBufInfo_t **TxCircBufrList, int slip) {

   DMABuffInfo* buffInfo;  
   ADT_UInt64*  dmaBuff;
   ADT_UInt32   startTime;

   // Get transfer buffer
   buffInfo = getTxBuff (port);
   if ((buffInfo == NULL) || (buffInfo->buffAddr == NULL)) {
      adjustTxCircBuffs (slip, slotCnt, TxCircBufrList);
      logDMA ("Tx AdjustBuff", port, 0);
      return 1;
   }

   startTime = CLK_gethtime();
   dmaStats.TxSwis++;
   dmaBuff = buffInfo->buffAddr;

   if (slip) adjustForSlips (TxCircBufrList, slotCnt, sysConfig.samplesPerMs);


   // copy from circ buffers into dma transfer buffers
   (*circToDma) (dmaBuff, TxCircBufrList, slotCnt);
   CheckTxPointers (dmaBuff, TxCircBufrList, slotCnt);

   // Allow modification of DMA transfer buffer before DMA occurs
   FixedPatternTx ((void *) dmaBuff);   // Allow overwrite of slot 0 with fixed pattern for debug
   customTDMOut ((void *) dmaBuff, port, slotCnt);
   issueTxDMA (port, buffInfo);

   // Update CPU utilization
   if (gblPkLoadingRst & 1) {
      MultiCoreCpuPeakUsage[0] = 0;
      MultiCoreCpuPeakUsage[1] = 0;
      MultiCoreCpuPeakUsage[2] = 0;
      MultiCoreCpuPeakUsage[3] = 0;
      MultiCoreCpuPeakUsage[4] = 0;
      MultiCoreCpuPeakUsage[5] = 0;
      gblPkLoadingRst &= ~1;
   }

   updateUsage (port + 3, startTime);
   return 0;
}
//}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ RxCopy. DMA buffer to circular transfers
#pragma CODE_SECTION (adjustTxCircBuffs, "MEDIUM_PROG_SECT")
void adjustRxCircBuffs (int slip, int slotCnt, CircBufInfo_t **RxCircBufrList) {

   CircBufInfo_t *Circ, **CircBufr;
   ADT_Word      SlipSamps;
   int k;  

   SlipSamps = 0;
   if (slip) SlipSamps = sysConfig.samplesPerMs;

   // Adjust circular buffers with silence
   CircBufr = RxCircBufrList;
   Circ     = *CircBufr++;
   for (k = 0; k < slotCnt; k++, Circ = *CircBufr++)  {
      Circ->SlipSamps += SlipSamps;
      Circ->PutIndex += sysConfig.samplesPerMs;
      if (Circ->BufrSize <= Circ->PutIndex)
      {
          Circ->PutIndex = 0;
      }
   }
   return;
}

#pragma CODE_SECTION (dmaToCirc8BitWB, "FAST_PROG_SECT")

#pragma CODE_SECTION (dmaToCirc8x16, "FAST_PROG_SECT")
inline void* dmaToCirc8x16 (ADT_UInt8* restrict dmaBuff, ADT_UInt64* restrict chBuff) {
   // Read one byte from tdm buffer.  Expand to 16 bits and append to 64 bit register.   Store as four 64 bit values.
   register ADT_UInt64 chData;

   chData =   (ADT_UInt64) *dmaBuff++;
   chData |= ((ADT_UInt64) *dmaBuff++) << 16;
   chData |= ((ADT_UInt64) *dmaBuff++) << 32;
   *chBuff++ = chData | ((ADT_UInt64) *dmaBuff++) << 48;

   chData =   (ADT_UInt64) *dmaBuff++;
   chData |= ((ADT_UInt64) *dmaBuff++) << 16;
   chData |= ((ADT_UInt64) *dmaBuff++) << 32;
   *chBuff++ = chData | ((ADT_UInt64) *dmaBuff++) << 48;

   chData =   (ADT_UInt64) *dmaBuff++;
   chData |= ((ADT_UInt64) *dmaBuff++) << 16;
   chData |= ((ADT_UInt64) *dmaBuff++) << 32;
   *chBuff++ = chData | ((ADT_UInt64) *dmaBuff++) << 48;

   chData =   (ADT_UInt64) *dmaBuff++;
   chData |= ((ADT_UInt64) *dmaBuff++) << 16;
   chData |= ((ADT_UInt64) *dmaBuff++) << 32;
   *chBuff = chData | ((ADT_UInt64) *dmaBuff++) << 48;
   return (ADT_UInt64*) dmaBuff;
}

// 8-bit wideband DMA to channel buffer.  16  8-bit samples on 8-bit alignment.
#pragma CODE_SECTION (dmaToCirc8BitWB, "FAST_PROG_SECT")
void dmaToCirc8BitWB (ADT_UInt64* dmaBuff, CircBufInfo_t **CircBufr, int slotCnt) {
   register CircBufInfo_t *Circ;

   Circ = *CircBufr++;
   while (slotCnt--) {
      dmaBuff = dmaToCirc8x16 ((ADT_UInt8*) dmaBuff, (ADT_UInt64*) &Circ->pBufrBase[Circ->PutIndex]);

      Circ->PutIndex  += 16;
      if (Circ->BufrSize <= Circ->PutIndex) Circ->PutIndex = 0;
      Circ = *CircBufr++;
   }
}
#pragma CODE_SECTION (dmaToCirc8x8, "FAST_PROG_SECT")
inline void* dmaToCirc8x8 (ADT_UInt16* restrict dmaBuff, ADT_UInt64* restrict chBuff) {
   // Read one byte from tdm buffer.  Expand to 16 bits and append to 64 bit register.   Store as four 64 bit values.
   register ADT_UInt64 chData;

   chData =   (ADT_UInt64) *dmaBuff++;
   chData |= ((ADT_UInt64) *dmaBuff++) << 16;
   chData |= ((ADT_UInt64) *dmaBuff++) << 32;
   *chBuff++ = chData | ((ADT_UInt64) *dmaBuff++) << 48;

   chData =   (ADT_UInt64) *dmaBuff++;
   chData |= ((ADT_UInt64) *dmaBuff++) << 16;
   chData |= ((ADT_UInt64) *dmaBuff++) << 32;
   *chBuff++ = chData | ((ADT_UInt64) *dmaBuff++) << 48;

   return (ADT_UInt64*) dmaBuff;
}

#ifdef C_VERSION
#pragma CODE_SECTION (dmaToCirc8BitNB, "FAST_PROG_SECT")
void dmaToCirc8BitNB (ADT_UInt64* dmaBuff, CircBufInfo_t **CircBufr, int slotCnt) {
    register CircBufInfo_t *Circ;

      Circ = *CircBufr++;
      while (slotCnt--) {
         dmaBuff = dmaToCirc8x8 ((ADT_UInt8*) dmaBuff, (ADT_UInt64*) &Circ->pBufrBase[Circ->PutIndex]);

         Circ->PutIndex  += 8;
         if (Circ->BufrSize <= Circ->PutIndex)
         {
             Circ->PutIndex = 0;
         }
         Circ = *CircBufr++;
      }
}
#endif

#pragma CODE_SECTION (circToDma8x8, "FAST_PROG_SECT")
inline void* circToDma8x8 (ADT_UInt16* restrict dmaBuff, ADT_UInt64* restrict chBuff) {
   // Read channel data into 64 bit register.  Store low byte of each successive 16-bit value into 8-bit dmabuffer.
   register ADT_UInt64 chData;
   chData = *chBuff++;
   *dmaBuff++ = (ADT_UInt16)  (chData & 0xff);
   *dmaBuff++ = (ADT_UInt16) ((chData >> 16) & 0xff);
   *dmaBuff++ = (ADT_UInt16) ((chData >> 32) & 0xff);
   *dmaBuff++ = (ADT_UInt16) ((chData >> 48) & 0xff);

   chData = *chBuff++;
   *dmaBuff++ = (ADT_UInt16)  (chData & 0xff);
   *dmaBuff++ = (ADT_UInt16) ((chData >> 16) & 0xff);
   *dmaBuff++ = (ADT_UInt16) ((chData >> 32) & 0xff);
   *dmaBuff++ = (ADT_UInt16) ((chData >> 48) & 0xff);

   return (ADT_UInt64*) dmaBuff;

}

#ifdef C_VERSION
#pragma CODE_SECTION (circToDma8BitNB, "FAST_PROG_SECT")
void circToDma8BitNB (ADT_UInt64* dmaBuff, CircBufInfo_t **CircBufr, int slotCnt) {
    register CircBufInfo_t *Circ;

    Circ = *CircBufr++;
    while (slotCnt--)  {
        dmaBuff = circToDma8x8 ((ADT_UInt8*) dmaBuff, (ADT_UInt64*) &Circ->pBufrBase[Circ->TakeIndex]);

        Circ->TakeIndex += 8;
        if (Circ->BufrSize <= Circ->TakeIndex)
        {
            Circ->TakeIndex = 0;
        }
        Circ = *CircBufr++;
    }
}
#endif


#pragma CODE_SECTION (circToDma16x8, "FAST_PROG_SECT")
inline void* circToDma16x8 (ADT_UInt16* restrict dmaBuff, ADT_UInt64* restrict chBuff) {
   // Read channel data into 64 bit register.  Store low byte of each successive 16-bit value into 8-bit dmabuffer.
   register ADT_UInt64 chData;
   chData = *chBuff++;
   *dmaBuff++ = (ADT_UInt16)  (chData & 0xffff);
   *dmaBuff++ = (ADT_UInt16) ((chData >> 16) & 0xffff);
   *dmaBuff++ = (ADT_UInt16) ((chData >> 32) & 0xffff);
   *dmaBuff++ = (ADT_UInt16) ((chData >> 48) & 0xffff);

   chData = *chBuff++;
   *dmaBuff++ = (ADT_UInt16)  (chData & 0xffff);
   *dmaBuff++ = (ADT_UInt16) ((chData >> 16) & 0xffff);
   *dmaBuff++ = (ADT_UInt16) ((chData >> 32) & 0xffff);
   *dmaBuff++ = (ADT_UInt16) ((chData >> 48) & 0xffff);

   return (ADT_UInt64*) dmaBuff;

}
#ifdef C_VERSION
#pragma CODE_SECTION (circToDma16BitNB, "FAST_PROG_SECT")
void circToDma16BitNB (ADT_UInt64* dmaBuff, CircBufInfo_t **CircBufr, int slotCnt) {
    register CircBufInfo_t *Circ;

     Circ = *CircBufr++;
     while (slotCnt--)  {
        dmaBuff = circToDma16x8 ((ADT_UInt16*) dmaBuff, (ADT_UInt64*) &Circ->pBufrBase[Circ->TakeIndex]);

        Circ->TakeIndex += 8;
        if (Circ->BufrSize <= Circ->TakeIndex) Circ->TakeIndex = 0;
        Circ = *CircBufr++;
     }
}
#endif

#pragma CODE_SECTION (dmaToCirc16x8, "FAST_PROG_SECT")
inline void* dmaToCirc16x8 (ADT_UInt16* restrict dmaBuff, ADT_UInt64* restrict chBuff) {
   // Read one byte from tdm buffer.  Expand to 16 bits and append to 64 bit register.   Store as four 64 bit values.
   register ADT_UInt64 chData;

   chData =   (ADT_UInt64) *dmaBuff++;
   chData |= ((ADT_UInt64) *dmaBuff++) << 16;
   chData |= ((ADT_UInt64) *dmaBuff++) << 32;
   *chBuff++ = chData | ((ADT_UInt64) *dmaBuff++) << 48;

   chData =   (ADT_UInt64) *dmaBuff++;
   chData |= ((ADT_UInt64) *dmaBuff++) << 16;
   chData |= ((ADT_UInt64) *dmaBuff++) << 32;
   *chBuff++ = chData | ((ADT_UInt64) *dmaBuff++) << 48;

   return (ADT_UInt64*) dmaBuff;
}

#ifdef C_VERSION
#pragma CODE_SECTION (dmaToCirc16BitNB, "FAST_PROG_SECT")
void dmaToCirc16BitNB (ADT_UInt64* dmaBuff, CircBufInfo_t **CircBufr, int slotCnt) {
    register CircBufInfo_t *Circ;

    Circ = *CircBufr++;
    while (slotCnt--) {
         dmaBuff = dmaToCirc16x8 ((ADT_UInt16*) dmaBuff, (ADT_UInt64*) &Circ->pBufrBase[Circ->PutIndex]);

         Circ->PutIndex  += 8;
         if (Circ->BufrSize <= Circ->PutIndex) Circ->PutIndex = 0;
         Circ = *CircBufr++;
    }
}

#pragma CODE_SECTION (circToDma16x16, "FAST_PROG_SECT")
inline void* circToDma16x16 (ADT_UInt16* restrict dmaBuff, ADT_UInt64* restrict chBuff) {
   // Read channel data into 64 bit register.  Store low byte of each successive 16-bit value into 8-bit dmabuffer.
   register ADT_UInt64 chData;
   chData = *chBuff++;
   *dmaBuff++ = (ADT_UInt16)  (chData & 0xffff);
   *dmaBuff++ = (ADT_UInt16) ((chData >> 16) & 0xffff);
   *dmaBuff++ = (ADT_UInt16) ((chData >> 32) & 0xffff);
   *dmaBuff++ = (ADT_UInt16) ((chData >> 48) & 0xffff);

   chData = *chBuff++;
   *dmaBuff++ = (ADT_UInt16)  (chData & 0xffff);
   *dmaBuff++ = (ADT_UInt16) ((chData >> 16) & 0xffff);
   *dmaBuff++ = (ADT_UInt16) ((chData >> 32) & 0xffff);
   *dmaBuff++ = (ADT_UInt16) ((chData >> 48) & 0xffff);

   *dmaBuff++ = (ADT_UInt16)  (chData & 0xffff);
   *dmaBuff++ = (ADT_UInt16) ((chData >> 16) & 0xffff);
   *dmaBuff++ = (ADT_UInt16) ((chData >> 32) & 0xffff);
   *dmaBuff++ = (ADT_UInt16) ((chData >> 48) & 0xffff);

   chData = *chBuff++;
   *dmaBuff++ = (ADT_UInt16)  (chData & 0xffff);
   *dmaBuff++ = (ADT_UInt16) ((chData >> 16) & 0xffff);
   *dmaBuff++ = (ADT_UInt16) ((chData >> 32) & 0xffff);
   *dmaBuff++ = (ADT_UInt16) ((chData >> 48) & 0xffff);

   return (ADT_UInt64*) dmaBuff;

}
#pragma CODE_SECTION (circToDma16BitWB, "FAST_PROG_SECT")
void circToDma16BitWB (ADT_UInt64* dmaBuff, CircBufInfo_t **CircBufr, int slotCnt) {

    register CircBufInfo_t *Circ;

    Circ = *CircBufr++;
    while (slotCnt--)  {
       dmaBuff = circToDma16x16 ((ADT_UInt16*) dmaBuff, (ADT_UInt64*) &Circ->pBufrBase[Circ->TakeIndex]);

       Circ->TakeIndex += 16;
       if (Circ->BufrSize <= Circ->TakeIndex) Circ->TakeIndex = 0;
       Circ = *CircBufr++;
    }
}

#pragma CODE_SECTION (dmaToCirc16x16, "FAST_PROG_SECT")
inline void* dmaToCirc16x16 (ADT_UInt16* restrict dmaBuff, ADT_UInt64* restrict chBuff) {
   // Read one byte from tdm buffer.  Expand to 16 bits and append to 64 bit register.   Store as four 64 bit values.
   register ADT_UInt64 chData;

   chData =   (ADT_UInt64) *dmaBuff++;
   chData |= ((ADT_UInt64) *dmaBuff++) << 16;
   chData |= ((ADT_UInt64) *dmaBuff++) << 32;
   *chBuff++ = chData | ((ADT_UInt64) *dmaBuff++) << 48;

   chData =   (ADT_UInt64) *dmaBuff++;
   chData |= ((ADT_UInt64) *dmaBuff++) << 16;
   chData |= ((ADT_UInt64) *dmaBuff++) << 32;
   *chBuff++ = chData | ((ADT_UInt64) *dmaBuff++) << 48;

   chData =   (ADT_UInt64) *dmaBuff++;
   chData |= ((ADT_UInt64) *dmaBuff++) << 16;
   chData |= ((ADT_UInt64) *dmaBuff++) << 32;
   *chBuff++ = chData | ((ADT_UInt64) *dmaBuff++) << 48;

   chData =   (ADT_UInt64) *dmaBuff++;
   chData |= ((ADT_UInt64) *dmaBuff++) << 16;
   chData |= ((ADT_UInt64) *dmaBuff++) << 32;
   *chBuff++ = chData | ((ADT_UInt64) *dmaBuff++) << 48;

   return (ADT_UInt64*) dmaBuff;
}
#pragma CODE_SECTION (dmaToCirc16BitWB, "FAST_PROG_SECT")
void dmaToCirc16BitWB (ADT_UInt64* dmaBuff, CircBufInfo_t **CircBufr, int slotCnt) {
    register CircBufInfo_t *Circ;

    Circ = *CircBufr++;
    while (slotCnt--) {
        dmaBuff = dmaToCirc16x16 ((ADT_UInt16*) dmaBuff, (ADT_UInt64*) &Circ->pBufrBase[Circ->PutIndex]);

        Circ->PutIndex  += 16;
        if (Circ->BufrSize <= Circ->PutIndex) Circ->PutIndex = 0;
        Circ = *CircBufr++;
    }

}
#endif

#pragma CODE_SECTION (CopyDmaRxBuffers, "FAST_PROG_SECT")
ADT_Bool CopyDmaRxBuffers (int port, int slotCnt, CircBufInfo_t **RxCircBufrList, int slip) {

   DMABuffInfo* buffInfo;
   ADT_UInt64*  dmaBuff;
   ADT_UInt32   startTime;

   int badFrame;

   // Get transfer buffer
   buffInfo = getRxBuff (port);
   if (buffInfo == NULL) {
      adjustRxCircBuffs (slip, slotCnt, RxCircBufrList);
      logDMA ("Rx AdjustBuff", port, 0);
      return 1;
   }

   startTime = CLK_gethtime();
   dmaStats.RxSwis++;
   dmaBuff = buffInfo->buffAddr;


   // Check that the TSIP Rx frame counters are valid
   badFrame = validateRxFrameCnt (port, buffInfo->pong);
   if (badFrame) {
      adjustRxCircBuffs (slip, slotCnt, RxCircBufrList);
      logDMA ("Rx AdjustBuff", port, 0);
      freeRxBuff (port, buffInfo);
      return 1;
   }

   if (slip) adjustForSlips (RxCircBufrList, slotCnt, sysConfig.samplesPerMs);

   // Allow modification of DMA transfer buffer before copy to circular buffers
   customTDMIn ((void *) dmaBuff, port, slotCnt);

   CheckRxPointers (dmaBuff, RxCircBufrList, slotCnt);

   // copy into circ buffers from dma buffer transfer buffers
   (*dmaToCirc) (dmaBuff, RxCircBufrList, slotCnt);
   freeRxBuff (port, buffInfo);

   // Update utilization
   updateUsage (port, startTime);
   return badFrame;
}

#pragma CODE_SECTION (DmaLbRxToTxBuffers, "SLOW_PROG_SECT")
int DmaLbRxToTxBuffers (int port, int slotCnt) {   return 0;  }

//}
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ GpakDmaIsr
//
// FUNCTION
//  G.PAK Hardware Interrupt handler for all EDMA Channels.
//  It's triggered by the completion of a EDMA event. The 
//  CIPR register is checked to determine which events have occurred and
//  the SWI_Dma interrupt is issued to start the circular buffer copies.
//
// RETURNS
//  nothing
//}
#pragma CODE_SECTION (GpakDmaIsr, "FAST_PROG_SECT")
static void GpakDmaIsr (Hwi_FuncPtr event) {

   ADT_UInt64 dmaEvt, pendingEvts;

   ADT_UInt32 rxTsipIntr;
   dmaStats.Intrs++;

   // Determine which DMA event has occurred
   REG64_RD (EDMA_SHADOW_IPR, pendingEvts);
//   logEvt (1, pendingEvts);

   dmaEvt = pendingEvts & enabledDmaEvts;

   logTime (0x00100000ul | dmaEvt >> 48);

   logDMA ("DMAIntr......", pendingEvts >> 48, pendingEvts);
 
   markDMAInactive (dmaEvt);    // Mark DMA event no longer active

   //-------------------------------------------------------------------------
   // Rx DMA complete --> Issue SWI to copy DMA buffers to channel buffers
   // Transform RxDMA events into matching RxTSIP interrupts.
   rxTsipIntr = 0;
   if (dmaEvt & DmaRxEvents[0]) rxTsipIntr |= RxEvents[0];
   if (dmaEvt & DmaRxEvents[1]) rxTsipIntr |= RxEvents[1];
   if (dmaEvt & DmaRxEvents[2]) rxTsipIntr |= RxEvents[2];
   if (rxTsipIntr) SWI_or (SWI_Dma, rxTsipIntr);

   //-------------------------------------------------------------------------
   // Tx DMA complete --> Mark DMA buffers no longer active
   if (dmaEvt & DmaTxEvents[0]) freeTxBuff (0);
   if (dmaEvt & DmaTxEvents[1]) freeTxBuff (1);
   if (dmaEvt & DmaTxEvents[2]) freeTxBuff (2);

   REG64_WR (EDMA_SHADOW_IPCR, dmaEvt);   // clear pending edma interrupts
//   logEvt (0, dmaEvt);

   REG64_RD (EDMA_SHADOW_IPR, pendingEvts);
   if (pendingEvts) {
//      logEvt (1, pendingEvts);
      REG_WR (EDMA_SHADOW_IEVL, 1);    // generate new edma interrupt if any events are still pending
   }
   return;
}

#pragma CODE_SECTION (GpakDMAErrIsr, "FAST_PROG_SECT")
static void GpakDMAErrIsr (void) {

    ADT_UInt32 pendingInterrupts;

    REG_RD (CMB_EVT_PND, pendingInterrupts);
    // Combined interrupt processes:
    //  DMA error
    logTime (0x00110000ul | pendingInterrupts);

    // Disable DMA interrupts while clearing any DMA errors.
    if (pendingInterrupts & DMA_ERROR_EVTS) {
        C64_disableIER (1 << HWI_DMA_COMPLETE);
        DMAClearErrors (pendingInterrupts & DMA_ERROR_EVTS);
        C64_enableIER  (1 << HWI_DMA_COMPLETE);
    }
    return;
}

//--------------------------------------------------------------------------------
//{ DMA error handling
#pragma CODE_SECTION (DMAClearErrors, "FAST_PROG_SECT")
void DMAClearErrors (ADT_UInt32 pendingInterrupts) {
   ADT_UInt32 tmp32;
   ADT_UInt64 missedEvts;

   if (pendingInterrupts) {
      dmaStats.errors++;
      dmaErrs.cnt++;
      logDMA ("DMAErr.......", -1, (ADT_UInt64) pendingInterrupts);

      // Channel controller errors
      REG64_RD (EDMA_EMR, missedEvts); 
      if (missedEvts) {
         logEvt (0, missedEvts);
         dmaErrs.MissedEvents = missedEvts;
         REG64_WR (EDMA_SHADOW_SPCR,  missedEvts);  // Clear secondary events
         REG64_WR (EDMA_EMCR,   missedEvts);  // Clear missed events
         logDMA ("MissedEvts...", -1, missedEvts);
         if (missedEvts & DmaTxEvents[0]) freeTxBuff (0);
         if (missedEvts & DmaTxEvents[1]) freeTxBuff (1);
         if (missedEvts & DmaTxEvents[2]) freeTxBuff (2);

         if (missedEvts & DmaRxEvents[0]) freeRxBuff (0, getRxBuff (0));
         if (missedEvts & DmaRxEvents[1]) freeRxBuff (1, getRxBuff (1));
         if (missedEvts & DmaRxEvents[2]) freeRxBuff (2, getRxBuff (2));
      }
      REG_RD (EDMA_CCERR, tmp32);      if (tmp32) dmaErrs.WaterMarkErr = tmp32;

      // Transfer controller error status (Memory, bus, param)
      REG_RD (TC0_ERR_STAT, tmp32); if (tmp32) dmaErrs.TC0Stat = tmp32;
      REG_RD (TC1_ERR_STAT, tmp32); if (tmp32) dmaErrs.TC1Stat = tmp32;
  //    REG_RD (TC2_ERR_STAT, tmp32); if (tmp32) dmaErrs.TC2Stat = tmp32;

      // Transfer controller error details
      REG_RD (TC0_ERR, tmp32);  if (tmp32) dmaErrs.TC0Error = tmp32; 
      REG_RD (TC1_ERR, tmp32);  if (tmp32) dmaErrs.TC1Error = tmp32; 
 //     REG_RD (TC2_ERR, tmp32);  if (tmp32) dmaErrs.TC2Error = tmp32;

      // Dropped interrupt info
      REG_RD (INTR_XSTAT, tmp32); if (tmp32) dmaErrs.IntrStat = tmp32;

      if (IERR) dmaErrs.InternalExc = IERR;

   }

   REG_WR (EDMA_CCERRCLR, 0x1003F);          // Clear TCC and watermark errors

   REG_WR (TC0_ERR_STAT_CLR, 0xd);           // Clear TCC errors
   REG_WR (TC1_ERR_STAT_CLR, 0xd);
 //  REG_WR (TC2_ERR_STAT_CLR, 0xd);

   REG_WR (INTR_XSTAT_CLR,   1);             // Clear dropped interrupts

   REG_WR (CMB_EVT_CLR, DMA_ERROR_EVTS);     // Clear interrupt events


   IERR = 0;

   // Regenerate error interrupt for pending events
   REG_WR (TC0_EVAL, 1);
   REG_WR (TC1_EVAL, 1);
 //  REG_WR (TC2_EVAL, 1);
   REG_WR (EDMA_ERREVAL, 1);

   return;
}

#pragma CODE_SECTION (DMAClearAll, "FAST_PROG_SECT")
void DMAClearAll (void) {

   // Clear all DMA error conditions
   DMAClearErrors (0);
   memset (&dmaErrs, 0, sizeof (dmaErrs));
   dmaStats.statID = DMA_STAT_ID;

   // disable edma transfer completion interrupts and events
   REG64_WR (EDMA_SHADOW_EECR, ALL_EVENTS);  // Disable events
   REG64_WR (EDMA_SHADOW_IECR, ALL_EVENTS);  // Disable completion interrupts

   // Disable all dma access
   memset ((void *)EDMA_PARAM_ADDR, 0, 128 * PARAM_SIZE);

   // clear all indications of edma event notifications
   REG64_WR (EDMA_SHADOW_SPCR, ALL_EVENTS);  // Clear secondary event notifications
   REG64_WR (EDMA_EMCR,  ALL_EVENTS);  // Clear missed event notifications
   REG64_WR (EDMA_SHADOW_EPCR, ALL_EVENTS);  // Clear event notifications
   REG64_WR (EDMA_SHADOW_IPCR, ALL_EVENTS);  // Clear interrupt notifications
   logEvt (0, ALL_EVENTS);

}
//}

#pragma CODE_SECTION (GpakTSIP_EventIsr, "FAST_PROG_SECT")
static void GpakTSIP_EventIsr (Hwi_FuncPtr event) {

    // Combined interrupt processes:
    //  TSIP completion,
    //  TSIP error interrupt events, and
    //  DMA error
//    logTime (0x00110000ul | event);

    if((event == (Hwi_FuncPtr)CSL_GEM_TSIP0_RSFINT_N) || (event == (Hwi_FuncPtr)CSL_GEM_TSIP0_XSFINT_N) ||
      (event == (Hwi_FuncPtr)CSL_GEM_TSIP1_RSFINT_N) || (event == (Hwi_FuncPtr)CSL_GEM_TSIP1_XSFINT_N))
    {
        GpakTSIPIsr (event);
    }
    if((event == (Hwi_FuncPtr)CSL_GEM_TSIP0_ERRINT_N) || (event == (Hwi_FuncPtr)CSL_GEM_TSIP1_ERRINT_N))
    {
        GpakTSIPErrIsr (event);
    }
    return;
}
