/*
 * Copyright (c) 2011, Adaptive Digital Technologies, Inc.
 *
 * File Name: 81xxMcASP.c
 *
 * Description:
 *   This file contains G.PAK McASP I/O functions.
 *
 * Version: 1.0
 *
 */
   
//{
// NOTE:  A single DMA transfer request (i.e. an 'A' level transfer) is generated 
//        for each time slot.  If multiple pins are configured, the data must
//        be grouped by time instead of channels.  DMA processing would
//        need be modified accordingly to support multiple pins.
//
//        Currently DMA is set up as:
//           'A' level = one word
//           'B' level = 'b' active time slots
//           'C' level = 'c' frames per millisecond (8 or 16)
//
//       This results in data arranged in linear memory as follows:
//            S1F1  S1F2  S1F3 ... S1Fc
//            S2F1  S2F2  S2F3 ... S2Fc
//
//            SbF1  SbF2  SbF3 ... SbFc
//
//      Where SxFy is the sample (word) from slot x frame y.
//}

#include <std.h>

#include <clk.h>
#include <hwi.h>
#include <string.h>
#include "adt_typedef.h"
#include "81xx.h"


#include "GpakDefs.h"

int jdcStartHClk = 0;
int jdcStartLClk = 1;

ADT_UInt16 mcASPSlotsNeeded[NUM_TDM_PORTS] = { 0, 0, 0 };

#define CM_ALWON_MCASP2_CLKCTRL   0x48181548  // mcasp2 clock management control
#define CM_AUDIOCLK_MCASP2_CLKSEL 0x48180384  // mcasp2 audio clock mux select line

#define PINCTRL148  (volatile ADT_UInt32*)(0x48140A4C)  // MCA[2]_ACLKR    McASP2 Receive Bit Clock                         muxmode[2:0]b == 000
#define PINCTRL149  (volatile ADT_UInt32*)(0x48140A50)  // MCA[2]_AHCLKR   McASP2 Receive High-Frequency Master Clock       muxmode[2:0]b == 000
#define PINCTRL150  (volatile ADT_UInt32*)(0x48140A54)  // MCA[2]_AFSR     McASP2 Receive Frame Sync                        muxmode[2:0]b == 000
#define PINCTRL151  (volatile ADT_UInt32*)(0x48140A58)  // MCA[2]_ACLKX    McASP2 Transmit Bit Clock                        muxmode[2:0]b == 000
#define PINCTRL152  (volatile ADT_UInt32*)(0x48140A5C)  // MCA[2]_AHCLKX   McASP2 Transmit High-Frequency Master Clock      muxmode[2:0]b == 000
#define PINCTRL153  (volatile ADT_UInt32*)(0x48140A60)  // MCA[2]_AFSX     McASP2 Transmit Frame Sync                       muxmode[2:0]b == 000
#define PINCTRL154  (volatile ADT_UInt32*)(0x48140A64)  // MCA[2]_AMUTE    McASP2 Mute Output                               muxmode[2:0]b == 000
#define PINCTRL155  (volatile ADT_UInt32*)(0x48140A68)  // MCA[2]_AXR[0]   McASP2 Transmit/Receive0 Data I/Os               muxmode[2:0]b == 000
#define PINCTRL156  (volatile ADT_UInt32*)(0x48140A6C)  // MCA[2]_AXR[1]   McASP2 Transmit/Receive1 Data I/Os               muxmode[2:0]b == 000
#define PINCTRL296  (volatile ADT_UInt32*)(0x48140C9C)  // MCA[2]_AMUTEIN  McASP2 Mute Input                                muxmode[2:0]b == 001                        

#define muxModeMask 7

struct mcASPErrors_t {
   int errCnt;
   ADT_UInt32 rxErr;
   ADT_UInt32 txErr;
} mcASPErrors[NUM_TDM_PORTS];

McASP_t *McASP2 = (McASP_t *) McASP2_BASE;
McASP_Fifo_t *McASP2FIFO = (McASP_Fifo_t *) McASP2_FIFO;

//-------------------------------
//  Configurable Parameters
typedef struct mcASPParams_t {

   int inputPinMap;   // PFUNC[15:0] = Pins
   int outputPinMap;  // PDIR[15:0]  = Pins
   int PinCnt;        // Derived from PinMap

   int DataDelay;     // xFMT[17:16]
   int BitsPerSlot;   //     [7:4,2:0]
   int BitsPerSample; //         [2:0]

   int SlotsPerFrame; // FSxCTL[15:7]
   int HclkDiv;       // AHCLKxCTL[11:0]
   int ClkDiv;        //  ACLKxCTL[ 4:0]

   int ActiveSlotMap; // xTDM[31:0]
   int ActiveSlotCnt; // Devired from ActiveSlotMap

   unsigned int FsWord:1;       // FSxCTL[4]
   unsigned int FsFalling:1;    // FSxCTL[0]
   unsigned int SharedClk:1;    // ACLKxCTL[6]
   unsigned int ClkRxRising:1;  // ACLKxCTL[7]
   unsigned int HClkInvert:1;   // AHCLKxCTL[14]
   unsigned int GenClocks:1;    // PFUNC/PDIR [31:26]
                                //    FSRCTL[1]
                                //  ACLKxCTL[5]
                                // AHCLKxCTL[15]
   int burstMode;
} mcASPParams_t;

//-------------------------------
//  Configured register settings
typedef struct mcASPCfgRegs_t {
   ADT_UInt32 PinTypes;     // Active Tx and Rx pins
   ADT_UInt32 PinDir;       // Tx pins
   ADT_UInt32 TxFormat;
   ADT_UInt32 RxFormat;
   ADT_UInt32 FrameSync;
   ADT_UInt32 Clk;
   ADT_UInt32 HFClk;
   ADT_UInt32 SlotMask;
   ADT_UInt32 FifoCtrl;
} mcASPCfgRegs_t;


ADT_Bool startMcASP (int port);
ADT_Bool stopMcASP  (int port);
ADT_Bool setupMcASP (int port );

#ifdef STANDALONE
#define EVM 1
volatile ADT_UInt16 MatchDmaFlags;
#else
// G.PAK Constructs
#include "GpakDefs.h"
#include "GpakHpi.h"
#include "GpakExts.h"
#include "GpakPcm.h"

TDM_Port McASPPort = {
   startMcASP, stopMcASP, setupMcASP
};

//-------------------------------------------------------------------
//  Interface data for DMA processing
extern PortAddr DMACfg [];

extern volatile ADT_UInt16 MatchDmaFlags;
#endif
extern ADT_UInt32 getTccFlags (int port);


extern ADT_UInt16 SltsPerFrame[];   // Total slots on TDM frame [fixed at build]
extern ADT_UInt16 MaxDmaSlots [];   // Max slots available for DMA [fixed at build]. Used by DMA to perform DMA to TDM transfers
extern ADT_UInt16 DmaSlotCnt  [];   // Set by StartGpakPcm. Used by GpakPcm to perform DMA to circular transfers

extern ADT_UInt16 *pSlotMap   [];   // pointers to the DMA SlotMap buffers

#define MCASP_READ(addr, reg)         (*(volatile ADT_UInt32 *) (addr+reg))
#define MCASP_WRITE(addr, reg, value) {*((volatile ADT_UInt32 *) (addr+reg)) = (ADT_UInt32) value;}

//-------------------------------------------------------------------
//  McASP control register address offsets
//-------------------------------------------------------------------
//{  Global control register offsets
#define PID         0x000 // Peripheral Identification register [Register value: 0x0010 0101]
#define PFUNC       0x010 // Pin function register
#define PDIR        0x014 // Pin direction register
#define GBLCTL      0x044 // Global control register
#define AMUTE       0x048 // Mute control register
#define DLBCTL      0x04C // Digital Loop-back control register
#define DITCTL      0x050 // DIT mode control register

// Receiver control register offsets
#define RGBLCTL     0x060 //RGBLCTL Alias of GBLCTL containing only Receiver Reset bits, allows transmit to be reset
                                           //independently from receive.
#define RMASK       0x064 // Format unit bit mask register
#define RFMT        0x068 // Bit stream format register
#define RAFSCTL     0x06C // Frame sync control register
#define RACLKCTL    0x070 // Clock control register
#define RAHCLKCTL   0x074 // High-frequency clock control register
#define RTDM        0x078 // TDM slot
#define RINTCTL     0x07C // Interrupt control register
#define RSTAT       0x080 // Status register
#define RSLOT       0x084 // Current TDM slot register
#define RCLKCHK     0x088 // Clock check control register
#define REVTCTL     0x08C // DMA control register

// Transmitter control register offsets
#define XGBLCTL     0x0A0 // Alias of GBLCTL containing only Transmitter Reset bits, allows transmit to be reset
                                           //independently from receive.
#define XMASK       0x0A4 // Format unit bit mask register
#define XFMT        0x0A8 // Bit stream format register
#define XAFSCTL     0x0AC // Frame sync control register
#define XACLKCTL    0x0B0 // Clock control register
#define XAHCLKCTL   0x0B4 // High-frequency clock control register
#define XTDM        0x0B8 // TDM slot register
#define XINTCTL     0x0BC // Interrupt control register
#define XSTAT       0x0C0 // Status register
#define XSLOT       0x0C4 // Current TDM slot
#define XCLKCHK     0x0C8 // Clock check control register
#define XEVTCTL     0x0CC // DMA control register

#define SRCTL0      0x180 //SRCTL0 Serializer 0 control register

// FIFO control register offsets
#define TXFIFO      0x10
#define RXFIFO      0x18
//}

// Fixed Values / Restrictions
//{   PFUNC   (Frame Sync, High Res Clk, and CLK: McASP, Mute: GPIO)
//    RXFMT   (MSB first [15], DMA xfer [3])
//    INTCTL  (No interrupts from McASP,  comes from DMA instead)
//    EVTCTL  (DMA enabled)
//    MASK    (No padding)
//    AMUTE   (No errors mute audio)
//    DLBCTRL (Loopback disabled)
//    DITCTL  (DIT mode disabled)
//
//    Slot size and data size are same

#define PFUNC_FIXED  0x01000000
#define FMT_FIXED    0x00008000   // MSB first
#define RINTR_FIXED  0x00000003   // Overrun and framesync errors
#define XINTR_FIXED  0x00000003   // Underrun and framesync errors
#define EVTCTL_FIXED 0x00000000
#define MASK_FIXED   0xFFFFFFFF
#define AMUTE_FIXED  0x00000000
#define DLBCTL_FIXED 0x00000000
#define DITCTL_FIXED 0x00000000
#define CLKCHK_FIXED 0x00FF0000
#define DMA_EVENT    0x00000000

// GLB RESET FIELDS
#define RGBL_LFCLK   0x00000001
#define RGBL_HFCLK   0x00000002
#define RGBL_SER     0x00000004
#define RGBL_SM      0x00000008
#define RGBL_FSYNC   0x00000010
#define RGBL_MASK    0x0000001F

#define XGBL_LFCLK   0x00000100
#define XGBL_HFCLK   0x00000200
#define XGBL_SER     0x00000400
#define XGBL_SM      0x00000800
#define XGBL_FSYNC   0x00001000
#define XGBL_MASK    0x00001F00
#define CLK_ASYNC    0x00000040

#define DATAREADY    0x00000020 // DATA READY
// Configurable values
// PFUNC  (AXR[n])   ~InputPins + ~OutputPins
// PDIR   (AXR[n])   clksInternal + OutputPins

#define FS_CLK_INTERNAL  0xFC000000   // Frame sync and clocks are internally generated
#define FS_INTERNAL      0x00000002
#define INACTIVE_PIN     0x00000000
#define TX_PIN           0x00000001
#define RX_PIN           0x00000002

#define FIFO_ENABLE   0x00010000
//}
static mcASPCfgRegs_t McASPRegs[NUM_TDM_PORTS];


ADT_UInt32 McASP_Base[NUM_TDM_PORTS] = { 0, 0, 0 };
ADT_UInt32 McASP_Fifo[NUM_TDM_PORTS] = { 0, 0, 0 };
ADT_UInt16 McASP_Configured[NUM_TDM_PORTS] = { FALSE,       FALSE,       FALSE       };
PortAddr ASPDMACfg [NUM_TDM_PORTS];

//{  // For use with debug aides to establish McBSP setup for internally generated clocks
ADT_UInt16 ClkDiv[NUM_TDM_PORTS]     = { 0, 0, 0 }; // Clock divisor (0-255)
ADT_UInt16 PulseWidth[NUM_TDM_PORTS] = { 0, 0, 0 }; // Width (0-255 bits) of frame sync pulse
ADT_UInt16 FrameWidth[NUM_TDM_PORTS] = { 0, 0, 0 }; // Width (0-4095 bits) of frame
GpakActivation genClockEnable[NUM_TDM_PORTS] = { Disabled, Disabled, Disabled };
//}

#pragma CODE_SECTION (pollRegister, "SLOW_PROG_SECT")
static ADT_Bool pollRegister (ADT_UInt32 Base, ADT_UInt32 regAddr, ADT_UInt32 expectedVal, ADT_UInt32 mask) {
   volatile ADT_UInt32 regVal;
   ADT_UInt32 startTime;
   
   startTime = CLK_gethtime ();
   do {
       regVal = MCASP_READ (Base, regAddr);
      if ((regVal & mask) == expectedVal) return TRUE;
   } while ((startTime - CLK_gethtime()) < CLK_countspms());
   return FALSE;
}

#if 0 
void init816xPinCtrl() {

    // McASP2 functional clock mode is enabled - sleep transition cannot occur
    REG_WR (CM_ALWON_MCASP2_CLKCTRL, 2);
    REG_WR (CM_AUDIOCLK_MCASP2_CLKSEL, 0); // audio clock for mcasp2: 0==SYSCLK20, 1==SYSYCLK21, 2==SYSYCLK22

    // Set pin mux to use McASP pins
    *PINCTRL148  &= (~muxModeMask);
    *PINCTRL149  &= (~muxModeMask);
    *PINCTRL150  &= (~muxModeMask);
    *PINCTRL151  &= (~muxModeMask);
    *PINCTRL152  &= (~muxModeMask);
    *PINCTRL153  &= (~muxModeMask);
    *PINCTRL154  &= (~muxModeMask);
    *PINCTRL155  &= (~muxModeMask);
    *PINCTRL156  &= (~muxModeMask);
    *PINCTRL296  &= (~muxModeMask);
    *PINCTRL296  |= 1;
}
' must update void init814xPinCtrl() {
    // McASP2 functional clock mode is enabled - sleep transition cannot occur
    REG_WR (CM_ALWON_MCASP2_CLKCTRL, 2);
    REG_WR (CM_AUDIOCLK_MCASP2_CLKSEL, 0); // audio clock for mcasp2: 0==SYSCLK20, 1==SYSYCLK21, 2==SYSYCLK22

    // Set pin mux to use McASP pins
    *PINCTRL148  &= (~muxModeMask);
    *PINCTRL149  &= (~muxModeMask);
    *PINCTRL150  &= (~muxModeMask);
    *PINCTRL151  &= (~muxModeMask);
    *PINCTRL152  &= (~muxModeMask);
    *PINCTRL153  &= (~muxModeMask);
    *PINCTRL154  &= (~muxModeMask);
    *PINCTRL155  &= (~muxModeMask);
    *PINCTRL156  &= (~muxModeMask);
    *PINCTRL296  &= (~muxModeMask);
    *PINCTRL296  |= 1;
}
#endif

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{PowerOnMcASPs
//
// FUNCTION
//  
//     Power on McASPs and setup pin mux
//
// PARAMETERS
//
//     None
//}
#pragma CODE_SECTION (PowerOnMcASPs, "SLOW_PROG_SECT")
void PowerOnMcASPs () {
   int intrMask, port;

   intrMask = HWI_disable ();
   memset (&mcASPErrors, 0, sizeof (mcASPErrors));

#if 0 // JDC skip mux and clock control for now...host takes care of it
#ifdef TARGET_816X
   init816xPinCtrl();
#else
    init814xPinCtrl();
#endif
#endif

   for (port = 0; port < NUM_TDM_PORTS; port++)
        stopMcASP (port);

   HWI_restore (intrMask);

}

#pragma CODE_SECTION (clearTDMErrors, "FAST_PROG_SECT")
ADT_UInt32 clearTDMErrors (int port) {
   int Base;
   ADT_UInt32 rxErr;
   ADT_UInt32 txErr;

   Base  = McASP_Base [port];
   rxErr = MCASP_READ  (Base, RSTAT);
   txErr = MCASP_READ  (Base, XSTAT);
   if (((rxErr | txErr) & 0x100) == 0) return 0;

   mcASPErrors[port].errCnt++;
   mcASPErrors[port].rxErr = rxErr;
   mcASPErrors[port].txErr = txErr;

   MCASP_WRITE (Base, RSTAT, 0xFFFFUL);
   MCASP_WRITE (Base, XSTAT, 0xFFFFUL);
   return 1;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ StartMcASPPortIo - Start serial port input/output.
//      bool startMcASP (int port);
//      bool setupMcASP (int port);
//
// FUNCTION
//   This function sets up the serial ports and DMA controllers to start PCM I/O
//  after serial port configuration has occuured. It is also used to restart
//  I/O after a serial port error occurs.
//
// RETURNS
//  nothing
//
// 
//}static 
#pragma CODE_SECTION (setupMcASP, "SLOW_PROG_SECT")
ADT_Bool setupMcASP (int port) {
   // Steps used for initialization are defined in McAsp Reference Guide
   ADT_UInt32 rgblctrl, xgblctrl;           // local copy of mcasp global control register
   ADT_UInt32 Base, Fifo;
   int i, reg;
   ADT_Bool rv;
   
   mcASPCfgRegs_t *Regs;

   if (NUM_TDM_PORTS <= port) return FALSE;

   if (McASP_Configured[port] == 0) return FALSE;

   Base = McASP_Base [port];
   Fifo = McASP_Fifo [port];

   //----------- 1) Place McAsp in reset; Wait until reset is complete
   rgblctrl = (MCASP_READ (Base, RGBLCTL) & RGBL_MASK);
   xgblctrl = (MCASP_READ (Base, XGBLCTL) & XGBL_MASK);
   if (rgblctrl | xgblctrl)  {
      rgblctrl = xgblctrl = 0;
      MCASP_WRITE (Base, GBLCTL, 0);
      rv = pollRegister (Base, GBLCTL, rgblctrl, 0xFFFFFFFF);
      if (!rv) return FALSE;
   }


   Regs = &McASPRegs[port];
   clearTDMErrors (port);

   //----------- 2) Enable  Audio FIFOs
   MCASP_WRITE (Fifo, TXFIFO, Regs->FifoCtrl);
   MCASP_WRITE (Fifo, RXFIFO, Regs->FifoCtrl);

   MCASP_WRITE (Fifo, TXFIFO, Regs->FifoCtrl | FIFO_ENABLE);
   MCASP_WRITE (Fifo, RXFIFO, Regs->FifoCtrl | FIFO_ENABLE);

   //----------- 3) Configure McAsp registers
   // b) Receive registers
   MCASP_WRITE (Base, RMASK,    MASK_FIXED);        // Padding mask - no padding
   MCASP_WRITE (Base, RFMT,     Regs->RxFormat);    // Data format
   MCASP_WRITE (Base, RAFSCTL,  Regs->FrameSync);   // Frame sync format
   MCASP_WRITE (Base, RACLKCTL, Regs->Clk);         // Clock polarity, divisor
   MCASP_WRITE (Base, RAHCLKCTL,Regs->HFClk);       // High freq clock format
   MCASP_WRITE (Base, RTDM,     Regs->SlotMask);    // Mask of active slots
   MCASP_WRITE (Base, RINTCTL,  RINTR_FIXED);       // Receiver overrun
   MCASP_WRITE (Base, RCLKCHK,  CLKCHK_FIXED);      // No clock checking

   // c) Transmit registers
   MCASP_WRITE (Base, XMASK,    MASK_FIXED);        // Padding mask - no padding
   MCASP_WRITE (Base, XFMT,     Regs->TxFormat);    // Data format
   MCASP_WRITE (Base, XAFSCTL,  Regs->FrameSync);   // Frame sync format
   MCASP_WRITE (Base, XACLKCTL, Regs->Clk);         // Clock  polarity, divisor, sameTxRxClk
   MCASP_WRITE (Base, XAHCLKCTL,Regs->HFClk);       // High freq clock format
   MCASP_WRITE (Base, XTDM,     Regs->SlotMask);    // Mask of active slots
   MCASP_WRITE (Base, XINTCTL,  XINTR_FIXED);       // Transmitter underrun, clk failure
   MCASP_WRITE (Base, XCLKCHK,  CLKCHK_FIXED);      // No clock checking


   // d) Setup pin state registers
   // Identify pins as: inputs, outputs or inactive.

// jdc look at this...maybe accessing too many serializers!
   for (i=0, reg=SRCTL0; i < 6; i++, reg+=4) {
      if (Regs->PinDir & 1<<i) {
         MCASP_WRITE (Base, reg, TX_PIN);
      } else if ((Regs->PinTypes & 1<<i) == 0) {
         MCASP_WRITE (Base, reg, RX_PIN);
      } else {
         MCASP_WRITE (Base, reg, INACTIVE_PIN);
      }
   }

   // e) Global registers
   MCASP_WRITE (Base, PFUNC,    Regs->PinTypes);   // McAsp vs GPIO pins
   MCASP_WRITE (Base, PDIR,     Regs->PinDir);     // Input vs output pins
   MCASP_WRITE (Base, DITCTL,   DITCTL_FIXED);     // DIT mode not used
   MCASP_WRITE (Base, DLBCTL,   DLBCTL_FIXED);     // Loopback not used
   MCASP_WRITE (Base, AMUTE,    AMUTE_FIXED);      // Mute not used


   //----------- 4) Start respective high-frequency clocks
   // a) Take high-frequency clock out of reset 
   // b) Read back to ensure bits are latched
  if (!jdcStartHClk)  rv = 1;

  if (jdcStartHClk) {
   rgblctrl |= RGBL_HFCLK;
   MCASP_WRITE (Base, RGBLCTL, rgblctrl);
   rv = pollRegister (Base, RGBLCTL, rgblctrl, rgblctrl); 

   xgblctrl |= XGBL_HFCLK;
   MCASP_WRITE (Base, XGBLCTL, xgblctrl);
   rv &= pollRegister (Base, XGBLCTL, xgblctrl, xgblctrl); 
  }

   //----------- 5) Start respective low-frequency clocks
   // jdc removed for debug only: if (Regs->Clk & 0x00000020UL) {
   if (jdcStartLClk) { // jdc debug

      // a) Take internal low-frequency dividers and clock divider out of reset 
      // b) Read back to ensure bits are latched
      rgblctrl |= RGBL_LFCLK;
      MCASP_WRITE (Base, RGBLCTL, rgblctrl);
      rv &= pollRegister (Base, RGBLCTL, rgblctrl, rgblctrl); 

      xgblctrl |= XGBL_LFCLK;
      MCASP_WRITE (Base, XGBLCTL, xgblctrl);
      rv &= pollRegister (Base, XGBLCTL, xgblctrl, xgblctrl); 
   }



   //----------- 6) Activate DMA
   //  Done as separate function
   return rv;
}        

// FALSE indicates failure
#pragma CODE_SECTION (startMcASP, "SLOW_PROG_SECT")
ADT_Bool startMcASP (int port) {
   int rgblctrl, xgblctrl;
   ADT_Bool rv;
   ADT_UInt32 Base;

   if (NUM_TDM_PORTS <= port) return FALSE;

   Base = McASP_Base[port];

   MCASP_WRITE (Base, REVTCTL,  DMA_EVENT);         // DMA event 
   MCASP_WRITE (Base, XEVTCTL,  DMA_EVENT);         // DMA event 
   
   //----------- 7) Activate Serializers
   // a) Clear status
   MCASP_WRITE (Base, RSTAT, 0xFFFFUL);
   MCASP_WRITE (Base, XSTAT, 0xFFFFUL);

   // b) Serializer enable
   xgblctrl = (MCASP_READ (Base, XGBLCTL) & XGBL_MASK) | XGBL_SER;
   MCASP_WRITE (Base, XGBLCTL, xgblctrl);
   rv = pollRegister (Base, XGBLCTL, xgblctrl, xgblctrl); 

   rgblctrl = (MCASP_READ (Base, RGBLCTL) & RGBL_MASK) | RGBL_SER;
   MCASP_WRITE (Base, RGBLCTL, rgblctrl);
   rv &= pollRegister (Base, RGBLCTL, rgblctrl, rgblctrl); 


   //----------- 8) Verify transmit buffers clear by XDATA flag of XSTAT register 
   // a) if DMA -> verify XDATA Bit cleared
   #if 0 // jdc find out why this is here
   if (!firstTime) 
      rv &= pollRegister (Base, XSTAT, 0, DATAREADY);
   #endif 

   //----------- 9) State machine release
   rgblctrl |= RGBL_SM;
   MCASP_WRITE (Base, RGBLCTL, rgblctrl);
   rv &= pollRegister (Base, RGBLCTL, rgblctrl, rgblctrl); 

   xgblctrl |= XGBL_SM;   
   MCASP_WRITE (Base, XGBLCTL, xgblctrl);
   rv &= pollRegister (Base, XGBLCTL, xgblctrl, xgblctrl); 

   //----------- 10) Frame sync release
   rgblctrl |= RGBL_FSYNC;
   MCASP_WRITE (Base, RGBLCTL, rgblctrl);
   rv &= pollRegister (Base, RGBLCTL, rgblctrl, rgblctrl); 

   xgblctrl |= XGBL_FSYNC;
   MCASP_WRITE (Base, XGBLCTL, xgblctrl);
   rv &= pollRegister (Base, XGBLCTL, xgblctrl, xgblctrl); 

   if (!rv) {
      // Report setup failure and disable McAsp
      MCASP_WRITE (Base, GBLCTL, 0);
   }
   return rv;

}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ StopSerialPortIo - Stop all serial port and McAsp input/output.
//
// FUNCTION
//   This function stops all serial ports, McAsp ports, and DMA controllers.
//
// RETURNS
//  nothing
//}
#pragma CODE_SECTION (stopMcASP, "SLOW_PROG_SECT")
ADT_Bool stopMcASP (int port) {

    ADT_UInt32 Base;

    if (McASP_Configured[port] == 0) return FALSE;

    Base = McASP_Base [port];

    MCASP_WRITE  (Base, GBLCTL, 0); // reset mcasp
    pollRegister (Base, GBLCTL, 0, 0xFFFFFFFF);  

    clearTDMErrors (port);
    return TRUE;
}



#pragma CODE_SECTION (findHighestBit, "SLOW_PROG_SECT")
static int findHighestBit (ADT_UInt32 Mask) {
   int j;
   for (j = 31; j != 0; j--)    {
      if ((Mask & 0x80000000) != 0)       {
         return j;
      }
      Mask = Mask << 1;
   }
   return 0;
}

#pragma CODE_SECTION (findBitCount, "SLOW_PROG_SECT")
static int findBitCount (ADT_UInt32 Mask) {
   int bitCnt = 0;
   int j;

   for (j = 0; j < 32; j++) {
      if ((Mask & 1) != 0) bitCnt++;
   
      Mask >>= 1;
   }
   return bitCnt;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ ConfigureMcASP 
//
// FUNCTION
//   Configures McASP serial port I/O.
//
//  Inputs
//     McAspEnable - Array indicating which McAsp ports are to be configured.
//
// RETURNS
//  Status code indicating success or a specific error.
//}
#pragma CODE_SECTION (ConfigureMcASP, "SLOW_PROG_SECT")
void ConfigureMcASP (ADT_UInt32 port, ADT_Bool McAspEnable, mcASPParams_t *params) { 

#ifndef STANDALONE
   ADT_UInt32 j;
   ADT_UInt16 *SlotMap;
   ADT_UInt16 framemode;
#endif

   mcASPCfgRegs_t *Regs;

   if (NUM_TDM_PORTS <= port) return;

   Regs = &McASPRegs[port];

   Regs->PinTypes    = PFUNC_FIXED | (~params->inputPinMap & ~params->outputPinMap) & 0xffff;
   Regs->PinDir      = params->outputPinMap;
   if (params->GenClocks) Regs->PinDir |= FS_CLK_INTERNAL;

   Regs->SlotMask = params->ActiveSlotMap;

   Regs->TxFormat      = FMT_FIXED                 |  // MBS first | Pad zeros
                  (params->DataDelay << 16)        |  // Data delay
                  (((params->BitsPerSlot/2)-1) << 4);   // Slot size.

   if (params->BitsPerSample != 32)
      Regs->TxFormat |= (params->BitsPerSample/4);          // Rotate word to msb


   Regs->RxFormat      = FMT_FIXED                 |  // MBS first | Use padding bit
                  (params->DataDelay << 16)        |  // Data delay
                (((params->BitsPerSlot/2)-1) << 4) |  // Slot size.
       ((params->BitsPerSlot - params->BitsPerSample)/4);   // Rotate word to msb

   framemode = params->SlotsPerFrame;
   if (params->burstMode)
        framemode = 0;   

   Regs->FrameSync = framemode << 7 | params->FsWord << 4 |
                     params->GenClocks << 1     | params->FsFalling;

   Regs->Clk   =     params->ClkRxRising << 7   |
                     params->GenClocks << 5     | params->ClkDiv - 1;

   if (params->SharedClk == 0) 
        Regs->Clk |= (1 << 6); // async mode

   Regs->HFClk = params->GenClocks << 15        | params->HClkInvert << 14  |
                 params->HclkDiv - 1;

   // 1 word per pin for DMA event and DMA transfer
   Regs->FifoCtrl = params->PinCnt << 8 | params->PinCnt;
   McASP_Configured[port] = McAspEnable;
   if (!McAspEnable) return;


   tdmPortFuncs = &McASPPort;

   // All slots are DMA slots
#ifndef STANDALONE

   SlotMap = pSlotMap[port];
   for (j = 0; j < params->PinCnt * params->SlotsPerFrame; j++)
      SlotMap[j] = j;
#endif


   // Identify McASP port parameters to DMA driver
   memcpy (&DMACfg[port], &ASPDMACfg[port], sizeof (PortAddr));

   SltsPerFrame[port] = params->PinCnt * params->SlotsPerFrame;
   DmaSlotCnt  [port] = params->PinCnt * params->ActiveSlotCnt;
   return;
}

//=====================================================
//  Run-time configuration
//{ ProcConfigSerialPortsMsg
//  Word     Bits
//Msg[1]     0x00ff    = Port
//Msg[2]     0xffff    = Input pin map
//Msg[3]     0xffff    = Output pin map
//Msg[4]     0xc000    = Delay bits
//Msg[4]     0x1fff    = Hi-res clock divisor
//Msg[5]     0xff00    = Bits per slot
//Msg[5]     0x00ff    = Bits per sample
//Msg[6]     0xff00    = Slots per frame
//Msg[6]     0x00ff    = Clock divisor
//Msg[7]     0xffff    = Active slot map (high 16 bits)
//Msg[8]     0xffff    = Active slot map (low 16 bits)
//Msg[9]     0x8000    = Frame sync is word
//Msg[9]     0x4000    = Frame starts on falling frame sync edge
//Msg[9]     0x2000    = Rx starts on rising clock edge
//Msg[9]     0x1000    = Hi-res clock signal is inverted
//Msg[9]     0x0800    = Internally generate clock and frame sync signals
//}
int skipPortApi = 0;
#pragma CODE_SECTION (ProcConfigSerialPortsMsg, "SLOW_PROG_SECT")
GPAK_PortConfigStat_t ProcConfigSerialPortsMsg (ADT_UInt16 pCmd[], ADT_UInt16 pReply[]) {

   mcASPParams_t params;
   ADT_Bool McAspEnable;
   ADT_UInt32 startDmaCnt;
   ADT_UInt16 *SlotMap;
   ADT_UInt32 matchflags;

   int port, lastSlot, j;
   unsigned long startTime;

   // Prepare the reply message.
   pReply[0] |= (MSG_CONFIG_PORTS_REPLY << 8);

   if (skipPortApi)   return Pc_Success;

   // Make sure there are no channels currently active.
   if (NumActiveChannels != 0)
      return Pc_ChannelsActive;

   //--------------------------------------------------------------------------
   // Parse message for McASP configuration parameters
   //--------------------------------------------------------------------------
   port                  = Byte0 (pCmd[1]);

   params.inputPinMap    = pCmd[2];   // 16 bits
   params.outputPinMap   = pCmd[3];   // 16 bits

   params.DataDelay      = (pCmd[4] >> 14) & 0x3;  //  2 bits (0-2)
   params.HclkDiv        = (pCmd[4] & 0x1fff);     // 13 bits (1 to 4096)

   params.BitsPerSlot    = Byte1(pCmd[5]);   //  8 bits (8, 16, or 32)
   params.BitsPerSample  = Byte0(pCmd[5]);   //  8 bits (8, 16, or 32)

   params.SlotsPerFrame  = Byte1(pCmd[6]);   //  8 bits (1 to 32)
   params.ClkDiv         = Byte0(pCmd[6]);   //  8 bits (1 to 32)
   params.ActiveSlotMap  = (pCmd[7] << 16) | pCmd[8];  // 32 bits

   params.FsWord         = (pCmd[9] >> 15) & 1;   // 1 bit
   params.FsFalling      = (pCmd[9] >> 14) & 1;   // 1 bit
   params.ClkRxRising    = (pCmd[9] >> 13) & 1;   // 1 bit
   params.HClkInvert     = (pCmd[9] >> 12) & 1;   // 1 bit
   params.GenClocks      = (pCmd[9] >> 11) & 1;   // 1 bit
   params.SharedClk      = (pCmd[9] >> 10) & 1;   // 1 bit

   // If slots per frame equals 0xff, it means use burst-mode
   params.burstMode = 0;
   if (params.SlotsPerFrame == 0xFF) {
        params.SlotsPerFrame = 1;
        params.ActiveSlotMap = 1;
        params.burstMode = 1;
   }
   
   //--------------------------------------------------------------------------
   // Validate configuration parameters
   //--------------------------------------------------------------------------
   if (NUM_TDM_PORTS <= port) return Pc_UnconfiguredAPort1;

   McAspEnable = (params.SlotsPerFrame != 0);
   if (McAspEnable) {
      if (2 < params.DataDelay)      return Pc_InvalidDataDelay0;
      if (32 < params.SlotsPerFrame) return Pc_TooManySlots0;

      if (4096 < params.HclkDiv) return Pc_ClockDivideErr;
      if (  32 < params.ClkDiv)  return Pc_ClockDivideErr;

      if (params.BitsPerSlot != 8  &&
          params.BitsPerSlot != 16 &&
          params.BitsPerSlot != 32) return Pc_SlotSizeErr;

      if (params.BitsPerSample != 8  &&
          params.BitsPerSample != 16) return Pc_SampleSizeErr;

      if (params.BitsPerSlot < params.BitsPerSample) return Pc_SizeInconsistency;

      //--------------------------------------------------------------------------
      // Verify that
      //     The last selected slot by bit mask is within configured slots (SlotsPerFrame).
      //     The total selected slots is supported for DMA
      //--------------------------------------------------------------------------

      // Determine the highest numbered selected slot.
      lastSlot = findHighestBit (params.ActiveSlotMap);
      params.ActiveSlotCnt = findBitCount (params.ActiveSlotMap);

      if (params.SlotsPerFrame < lastSlot) 
         return Pc_TooManySlots0;        // Number active more than supported by build

      // Count the number of pins
      params.PinCnt = findBitCount (params.inputPinMap);
      if (params.PinCnt != findBitCount (params.outputPinMap)) return Pc_UnequalTxRxASerPort1;


      mcASPSlotsNeeded[port] =  params.PinCnt * params.ActiveSlotCnt;

      if (MaxDmaSlots[port] < mcASPSlotsNeeded[port])
         return Pc_TooManySlots0;        // Number active more than supported by build

      if (mcASPSlotsNeeded[port] == 0)
         return Pc_NoSlots0;             // No slots active

   }

   // Configure the McASP registers.
   ConfigureMcASP (port, McAspEnable, &params);
   StartGpakPcm   (&mcASPSlotsNeeded[0]);
   matchflags = getTccFlags (port);

   // success turning-off the port
   if (!McAspEnable && ((matchflags & MatchDmaFlags) == 0))
      return Pc_Success;

   // try to turn on the port, but MatchDmaFlags are incorrect.
   if (McAspEnable  && ((matchflags & MatchDmaFlags) != matchflags)) {
        SlotMap = pSlotMap[port];
        for (j=0; j<SltsPerFrame[port]; j++)
            SlotMap[j] = UNCONFIGURED_SLOT;
        return Pc_NoInterrupts;
   }

   // Wait up to 10 ms for verification that the DMA has started.
   startTime = CLK_gethtime ();
   startDmaCnt = ApiBlock.DmaSwiCnt;
   do {
      if (2 <= (ApiBlock.DmaSwiCnt - startDmaCnt)) return Pc_Success;
   } while ((CLK_gethtime() - startTime) < (CLK_countspms() * 10));

   SlotMap = pSlotMap[port];
   for (j=0; j<SltsPerFrame[port]; j++)
       SlotMap[j] = UNCONFIGURED_SLOT;

   return Pc_NoInterrupts;
}


#pragma CODE_SECTION (SetTransmitEnables, "SLOW_PROG_SECT")
void SetTransmitEnables (int port, ADT_UInt32 Mask[]) { }

#pragma CODE_SECTION (ClearTransmitEnables, "SLOW_PROG_SECT")
void ClearTransmitEnables (int port, ADT_UInt16 Mask[]) {}

#pragma CODE_SECTION (GetMasks, "SLOW_PROG_SECT")
void GetMasks (ADT_Word McAspId, int Slot, int SlotCnt, ADT_Word *TxMask) {}

#pragma CODE_SECTION (updateTdmLoopback, "SLOW_PROG_SECT")
int updateTdmLoopback (GpakSerialPort_t port, GpakActivation state) { return 0; }

#pragma CODE_SECTION (McBSPFrameError, "SLOW_PROG_SECT")
ADT_Bool McBSPFrameError (int port)  { return FALSE; }

#pragma CODE_SECTION (storeMcBSPConfiguration, "SLOW_PROG_SECT")
void storeMcBSPConfiguration (int port) {}

