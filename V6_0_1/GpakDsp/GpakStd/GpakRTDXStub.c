#include <std.h>
#include <swi.h>
#include "GpakExts.h"
// 
static void GpakRtdxSwi (void);
far SWI_Handle SWI_RTDX = NULL;
void (*rtdxDiagnostics) (void) = NULL;
static SWI_Attrs RtdxAttrs = {  (SWI_Fxn) GpakRtdxSwi, 0, 0, 0, 0 };



#pragma CODE_SECTION (GpakRtdxSetup, "SLOW_PROG_SECT")
void GpakRtdxSetup (void) {

   RtdxAttrs.priority = RtdxPriority;

#if (DSP_TYPE == 54)
   RtdxAttrs.iscfxn = TRUE;
#endif
   
   SWI_RTDX = SWI_create (&RtdxAttrs);
   AppErr ("RTDXSemaphore", SWI_RTDX == NULL);

}

#pragma CODE_SECTION (GpakRtdxSwi, "SLOW_PROG_SECT")
static void GpakRtdxSwi (void) {
   if (rtdxDiagnostics != NULL) (*rtdxDiagnostics) ();
}

#pragma CODE_SECTION (PostRTDX, "FAST_PROG_SECT")
void PostRTDX (void) {
   if (SWI_RTDX != NULL) SWI_post (SWI_RTDX);
}

