#define RS_AVAYA_INIT
#ifdef RS_AVAYA_INIT
/******************************************************************************
* Copyright (c) 2011-2012 Texas Instruments Incorporated - http://www.ti.com
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated emac the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*****************************************************************************/

/**************************************************************************************
* FILE PURPOSE:  Core 0 wakes up the other cores
**************************************************************************************
* FILE NAME: was formerly pcieboot_helloworld.c,  now wakeup_cores.c
*
* DESCRIPTION:  Core 0 wakes up the other cores
*
***************************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "../../AvayaC667x/AvayaC6678.h"

//#include "device.h"
//#include "chip.h"
//#include "device_map.h"
//#include "intc_api.h"
/* #include "platform.h" */
/* #include <ti\platform\platform.h> */

//#define NUMBER_AVAYA_CORES 8	/* for now let them all run just in case Gpak expects it */
extern void Wait_Soft(int nloop);
#define DEVICE_REG32_W(x,y)   *(volatile uint32_t *)(x)=(y)
#define DEVICE_REG32_R(x)    (*(volatile uint32_t *)(x))

#define CHIP_LEVEL_REG  0x02620000
#define KICK0           (CHIP_LEVEL_REG + 0x0038)
#define KICK1           (CHIP_LEVEL_REG + 0x003C)

/* Magic address RBL is polling */

//#ifdef _EVMC6678L_
#define MAGIC_ADDR          0x87fffc
//#endif

#define BOOT_MAGIC_ADDR(x)  (MAGIC_ADDR + (1<<28) + (x<<24))
#define IPCGR(x)            (0x02620240 + x*4)
#define DSP_BOOT_ADDR_REG(x)            (0x02620040 + x*4)

#define BOOT_MAGIC_NUMBER   0xBABEFACE

#define BOOT_UART_BAUDRATE         115200


extern cregister volatile unsigned int DNUM;
extern far uint32_t _c_int00;
extern int DSPTotalCores;
#define NUMBER_AVAYA_CORES DSPTotalCores

#if 0
/* OSAL functions for Platform Library */
uint8_t *Osal_platformMalloc (uint32_t num_bytes, uint32_t alignment)
{
	return malloc(num_bytes);
}

void Osal_platformFree (uint8_t *dataPtr, uint32_t num_bytes)
{
    /* Free up the memory */
    if (dataPtr)
    {
        free(dataPtr);
    }
}

void Osal_platformSpiCsEnter(void)
{
    return;
}

void Osal_platformSpiCsExit (void)
{
    return;
}
#endif

void write_boot_magic_number(uint32_t core)
{
    //DEVICE_REG32_W(BOOT_MAGIC_ADDR(core), BOOT_MAGIC_NUMBER);
    DEVICE_REG32_W(MAGIC_ADDR, BOOT_MAGIC_NUMBER);
}

int read_boot_magic_number(uint32_t core)
{
 uint32_t magic;

    magic = DEVICE_REG32_R(BOOT_MAGIC_ADDR(core));
    Wait_Soft(1000);
    if (magic == BOOT_MAGIC_NUMBER) return 0;
    else return -1;
}



/******************************************************************************
* Function:    main
******************************************************************************/
//#define REVERSE_CORE_BOOT_ORDER
void wakeup_cores()
{
    uint32_t                core;

    if (DNUM != 0) return;

    /* Unlock the chip registers */
    DEVICE_REG32_W(KICK0, 0x83e70b13);
    DEVICE_REG32_W(KICK1, 0x95a4f1e0);
    /* Writing the entry address to other cores */
    for (core = 1; core < NUMBER_AVAYA_CORES; core++)
    {
        Wait_Soft(1000);
	    DEVICE_REG32_W(BOOT_MAGIC_ADDR(core), (uint32_t)&_c_int00);
        Wait_Soft(100);
    }

	BCACHE_wbInvAll ();

    for (core = 1; core < NUMBER_AVAYA_CORES; core++)
    {
        Wait_Soft(1000);
        /* IPC interrupt other cores */
        DEVICE_REG32_W(IPCGR(core), 1);
        Wait_Soft(1000);
    }
}
void wakeup_next_core(uint32_t core)
{
    //platform_info           pform_info;
    //uint32_t                core;
    if (core == 0)
    {

	/* IBL may do this eventually */
        /* Unlock the chip registers */
        DEVICE_REG32_W(KICK0, 0x83e70b13);
        DEVICE_REG32_W(KICK1, 0x95a4f1e0);
    }

    if (core <= 4)
    {
        //platform_get_info(&pform_info);

	/* IBL may do this eventually */
        /* Unlock the chip registers */
        //DEVICE_REG32_W(KICK0, 0x83e70b13);
        //DEVICE_REG32_W(KICK1, 0x95a4f1e0);

        /* Writing the entry address to other cores */
        //for (core = 1; core < pform_info.cpu.core_count; core++)
        //for (core = 1; core < NUMBER_AVAYA_CORES; core++)
        //{
	    #if 0
            sprintf(boot_msg, "\r\n\r\nBooting Hello World image on Core %d from Core 0 ...", core);
            write_uart(boot_msg);
	    #endif

	    DEVICE_REG32_W(BOOT_MAGIC_ADDR(core+1), (uint32_t)&_c_int00);

	    /* Delay 1us sec */
            //platform_delay(1);
            Wait_Soft(1000);
        //}

        //for (core = 1; core < pform_info.cpu.core_count; core++)
        //for (core = 1; core < NUMBER_AVAYA_CORES; core++)
        //{
            /* IPC interrupt other cores */
            DEVICE_REG32_W(IPCGR(core+1), 1);
            //platform_delay(1000);
            Wait_Soft(1000);
        //}
    }
    write_boot_magic_number(core);
}


void wakeup_core(uint32_t core)
{
    DEVICE_REG32_W(BOOT_MAGIC_ADDR(core), (uint32_t)&_c_int00);

	/* Delay 1us sec */
    Wait_Soft(1000);

	BCACHE_wbInvAll ();

    /* IPC interrupt other core */
    DEVICE_REG32_W(IPCGR(core), 1);
    Wait_Soft(1000);
}

void unlock_chip_regs()
{
        /* Unlock the chip registers */
        DEVICE_REG32_W(KICK0, 0x83e70b13);
        DEVICE_REG32_W(KICK1, 0x95a4f1e0);
}


#endif
