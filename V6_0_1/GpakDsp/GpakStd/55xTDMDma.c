#define ENDPROG  // Jim's fix for the DMS working register copy

/*
 * Copyright (c) 2007, Adaptive Digital Technologies, Inc.
 *
 * File Name: 55xTDMDma.c
 *
 * Description:
 *   This file contains Dma configuration for the PCM data collection
 *
 * Version: 1.0
 *
 * Revision History:
 *   1/2007 - Initial release.
 */
/*
   The DMA configuration details for each McBSP port are defined below. 
// Note: McBSP vs DMA Controller usage.
//	McBSP0 Receive  - DMA Channel 4 INT 14
//	McBSP0 Transmit - DMA Channel 5 INT 15
// 	McBSP1 Receive  - DMA Channel 2 INT 20
// 	McBSP1 Transmit - DMA Channel 3 INT 21
//	McBSP2 Receive  - DMA Channel 0 INT 18
//	McBSP2 Transmit - DMA Channel 1 INT 9

// The DMA is configured to use ping/pong DMA buffers. The DMA switches between
// ping and pong every 8 samples. Samples are stored in the DMA buffer in the 
// following format:
//
//  PING: [slot0, samples 0-7]
//        [slot1, samples 0-7]
//        [slot2, samples 0-7]
//        [slot3, samples 0-7]
//        [slot4, samples 0-7]
//          ...
//        [slotN-1, samples 0-7]
//  PONG: [slot0, samples 0-7]
//        [slot1, samples 0-7]
//        [slot2, samples 0-7]
//        [slot3, samples 0-7]
//        [slot4, samples 0-7]
//          ...
//        [slotN-1, samples 0-7]
//
// During each RX DMA frame, the DMA controller copies each timeslot(n) current 
// sample(i) from the MCBSP's receive data register to the correct location in
// the rx DMA buffer. The DMA element indexing is configured such that the 
// write address into the buffer increments by 8 words after each copy. The
// number of transfers in a DMA frame equals the number of configured slots. At 
// the end of each DMA frame, the destination write address is decremented by 
// frame index (actually incremented, but the frame index is configured to be 
// a negative number). The frame index is configured such that the starting
// address into the DMA buffer points to the next sample of timeslot 0. After
// 8 DMA frames have been received, the DMA controller will set a RX DMA event
// and the DMA controller will be reset to begin another DMA cycle. The
// element and frame counters are reset and the DMA buffer destination address 
// is reloaded with the contents of the DMA's channel destination address 
// register. When the DSP software services the DMA software interrupt, it
// loads the DMA's channels destination address register with the PING or PONG
// buffer address depending upon the current phase.
//
// The transmit DMA works the same way except that the DMA is reading data from
// DMA buffers and writing to the McBSP's transmit data register.
//
// As an example of a 4-slot frame: the DMA copies will occur in the following
// sequence:
//----------------------------------------------
// DMA Frame       DMA buffer       contents
//  Number          location        transfered
//----------------------------------------------
//  1:              Ping[0]        slot 0, sample0
//                  Ping[8]        slot 1, sample0
//                  Ping[16]       slot 2, sample0
//                  Ping[24]       slot 3, sample0
//  2:              Ping[1]        slot 0, sample1
//                  Ping[9]        slot 1, sample1
//                  Ping[17]       slot 2, sample1
//                  Ping[25]       slot 3, sample1
//  3:              Ping[2]        slot 0, sample2
//                  Ping[10]       slot 1, sample2
//                  Ping[18]       slot 2, sample2
//                  Ping[26]       slot 3, sample2
//  .
//  .
//  .
//
//  8:              Ping[7]        slot 0, sample7
//                  Ping[15]       slot 1, sample7
//                  Ping[23]       slot 2, sample7
//                  Ping[31]       slot 3, sample7 
//
//   -----> DMA triggers receive interrupt, DMA parameters are reset.
//
//  1:              Pong[0]        slot 0, sample0
//                  Pong[8]        slot 1, sample0
//                  Pong[16]       slot 2, sample0
//                  Pong[24]       slot 3, sample0
//  2:              Pong[1]        slot 0, sample1
//                  Pong[9]        slot 1, sample1
//                  Pong[17]       slot 2, sample1
//                  Pong[25]       slot 3, sample1
//  3:              Pong[2]        slot 0, sample2
//                  Pong[10]       slot 1, sample2
//                  Pong[18]       slot 2, sample2
//                  Pong[26]       slot 3, sample2
//  .
//  .
//  .
//
//  8:              Pong[7]        slot 0, sample7
//                  Pong[15]       slot 1, sample7
//                  Pong[23]       slot 2, sample7
//                  Pong[31]       slot 3, sample7 
//

    INTERFACES

   void DMAInitialize ()
   ADT_UInt16 enableDMA  (port) 
   ADT_UInt16 disableDMA (port)

   ADT_Bool DmaLostTxSync (int port, ADT_PCM16 *TxSwi);
   ADT_Bool DmaLostRxSync (int port, ADT_PCM16 *RxSwi);

   void CopyDmaTxBuffers (port, CircBufInfo_t **pCircTxBase,  ADT_UInt16 *pDmaTxBufr);
   void CopyDmaRxBuffers (port, ADT_UInt16 *pDmaRxBufr, CircBufInfo_t **pCircRxBase);

                      -------------------------

     DEBUGGING AIDES
     
   TxPatternSlot1    = (non-zer0) Pattern placed only in first slot.
   TxPatternAllSlots = (non-zero) Pattern placed in all slots.  TxPatternSlot1 takes precedence on first slot.

   RxPatternSlot1    = (non-zer0) Pattern placed only in first slot.
   RxPatternAllSlots = (non-zer0) Pattern placed in all slots.  RxPatternSlot1 takes precedence on first slot.

   FrameStamp = (true) 1st byte of each frame contains current frame count   
*/

#include <std.h>
#include <hwi.h>
#include <c55.h>

#include "GpakDefs.h"
#include "GpakExts.h"
#include "GpakPcm.h"
#include "GpakPragma.h"

extern int customTDMOut (ADT_PCM16 *dataOut, int port, int slotCnt);
extern int customTDMIn  (ADT_PCM16 *dataIn,  int port, int slotCnt);

#define NUM_TDM_PORTS 3
int TdmRxPriority = 0;  // Urgent
int TdmTxPriority = 1;  // High

static void initRxDMAParams (ADT_UInt32 deviceIdx);
static void initTxDMAParams (ADT_UInt32 deviceIdx);
static void GpakDma0Isr(void);
static void GpakDma1Isr(void);
static void GpakDma2Isr(void);
static void GpakDma3Isr(void);
static void GpakDma4Isr(void);
static void GpakDma5Isr(void);
static void updateNextDmaAdress(ADT_UInt16 dmaChanAddr, ADT_UInt16 dstFlag, ADT_UInt16 lower, ADT_UInt16 upper, ADT_UInt16 wait_flag);


#define IER0 *(volatile unsigned int *) 0x00    // Interrupt Enable register 0
#define IER1 *(volatile unsigned int *) 0x45    // Interrupt Enable register 1
#define IFR0 *(volatile unsigned int *) 0x01    // Interrupt Flag register 0
#define IFR1 *(volatile unsigned int *) 0x46    // Interrupt Flag register 1
#define IER0_DMA1 0x0200						// IER0 bit for DMA1
#define IER0_DMA4 0x4000						// IER0 bit for DMA4
#define IER0_DMA5 0x8000						// IER0 bit for DMA5
#define IER1_DMA0 0x0004						// IER1 bit for DMA0
#define IER1_DMA2 0x0010						// IER1 bit for DMA2
#define IER1_DMA3 0x0020						// IER1 bit for DMA3




// DMA global register base addresses in IO space
#define DMA_GCR_BASE_ADDR   0x0E00  // control register base address
#define DMA_GSCR_BASE_ADDR  0x0E02  // software compatability reg. base address
#define DMA_GTCR_BASE_ADDR  0x0E03  // timeout control register base address

// DMA channel register base addresses in IO space
#define DMA_CHAN0_BASE_ADDR 0x0C00  // dma channel 0 base address
#define DMA_CHAN1_BASE_ADDR 0x0C20  // dma channel 1 base address
#define DMA_CHAN2_BASE_ADDR 0x0C40  // dma channel 2 base address
#define DMA_CHAN3_BASE_ADDR 0x0C60  // dma channel 3 base address
#define DMA_CHAN4_BASE_ADDR 0x0C80  // dma channel 4 base address
#define DMA_CHAN5_BASE_ADDR 0x0CA0  // dma channel 5 base address

// DMA register offsets from base address
#define CSDP_OFFSET   0x00 // channel source/dest parameters
#define CCR_OFFSET    0x01 // channel control 
#define CICR_OFFSET   0x02 // channel interrupt control
#define CSR_OFFSET    0x03 // channel status
#define CSSA_L_OFFSET 0x04 // channel source start address lower bits
#define CSSA_U_OFFSET 0x05 // channel source start address upper bits
#define CDSA_L_OFFSET 0x06 // channel destination start address lower bits
#define CDSA_U_OFFSET 0x07 // channel detination start address upper bits
#define CEN_OFFSET    0x08 // channel element number
#define CFN_OFFSET    0x09 // channel frame number
#define CFI_OFFSET    0x0A // channel frame index
#define CEI_OFFSET    0x0B // channel element index
#define CSAC_OFFSET   0x0C // src count
#define CDAC_OFFSET   0x0D // dst count

#define DMA_CSR_BLOCK 0x20
#define DMA_CSR_DROP  0x02
#define DMA_CSR_TIMEOUT 0x01
//volatile ADT_UInt16 dmaDropCount = 0;
//volatile ADT_UInt16 dmaTimeoutCount[3][2] = {0,0,0,0,0,0};
// macros to access IO-mapped registers
#define IO_REG_WRITE(base, offset, value) *(ioport volatile ADT_UInt16 *) (base + offset) = value
#define IO_REG_OR(base, offset, value)    *(ioport volatile ADT_UInt16 *) (base + offset) |= value
#define IO_REG_AND(base, offset, value)   *(ioport volatile ADT_UInt16 *) (base + offset) &= value
#define IO_REG_READ(base, offset, value)  value = *(ioport volatile ADT_UInt16 *) (base + offset)

//
//

// DMA interrupt related definitions.
#define DMA_CHAN_RDY_0 0x0001		// DMA Channel 0 completed flag bit //	McBSP2 Receive
#define DMA_CHAN_RDY_1 0x0002		// DMA Channel 1 completed flag bit //	McBSP2 Transmit
#define DMA_CHAN_RDY_2 0x0004		// DMA Channel 2 completed flag bit //  McBSP1 Receive
#define DMA_CHAN_RDY_3 0x0008		// DMA Channel 3 completed flag bit // 	McBSP1 Transmit
#define DMA_CHAN_RDY_4 0x0010		// DMA Channel 4 completed flag bit //	McBSP0 Receive
#define DMA_CHAN_RDY_5 0x0020		// DMA Channel 5 completed flag bit // 	McBSP0 Transmit

// Miscellaneous definitions.
//#define NUM_SERIAL_PORTS 3			// number of serial ports supported
//#define MAX_STREAM_SLOTS 32			// max number of time slots could be configured
//#define MAX_SLOTS_ON_STREAM 128     // max number of slots on the stream
//#define UNCONFIGURED_SLOT 65535		// reserved slot index for Unconfigured
//#define UNCONFIGURED_PORT 65535		// reserved port index for Unconfigured


// Serial port (stream) configuration related variables.
//volatile ADT_UInt16 StreamSlots[NUM_SERIAL_PORTS]; //SltsPerFrame // number of stream time slots on McBSP
// volatile ADT_UInt16 MaxSlots[NUM_SERIAL_PORTS];// MaxDmaSlots // max slots available for use on McBSP
//ADT_UInt16 NumCfgSlots[NUM_SERIAL_PORTS];	// DmaSlotCnt // num time slots configured on McBSP
//volatile ADT_UInt16 SlotMap[NUM_SERIAL_PORTS][MAX_SLOTS_ON_STREAM];	// slot num to DMA bufr map
//volatile ADT_UInt16 CfgSlotCount = 0;				// count of all configured time slots


ADT_UInt16 DMAChan0DstStartL[2]; // low 16 bits of DMA 0 Destination start address
ADT_UInt16 DMAChan0DstStartU[2]; // upper 8 bits of DMA 0 Destination start address
ADT_UInt16 DMAChan1SrcStartL[2]; // low 16 bits of DMA 1 Source start address
ADT_UInt16 DMAChan1SrcStartU[2]; // upper 8 bits of DMA 1 Source start address
ADT_UInt16 DMAChan2DstStartL[2]; // low 16 bits of DMA 2 Destination start address
ADT_UInt16 DMAChan2DstStartU[2]; // upper 8 bits of DMA 2 Destination start address
ADT_UInt16 DMAChan3SrcStartL[2]; // low 16 bits of DMA 3 Source start address
ADT_UInt16 DMAChan3SrcStartU[2]; // upper 8 bits of DMA 3 Source start address
ADT_UInt16 DMAChan4DstStartL[2]; // low 16 bits of DMA 4 Destination start address
ADT_UInt16 DMAChan4DstStartU[2]; // upper 8 bits of DMA 4 Destination start address
ADT_UInt16 DMAChan5SrcStartL[2]; // low 16 bits of DMA 5 Source start address
ADT_UInt16 DMAChan5SrcStartU[2]; // upper 8 bits of DMA 5 Source start address


#define DMA_PRIO 0x40
typedef struct {
  ADT_UInt16 Base;     // Base address of McBSP control registers
  ADT_UInt16 RxEdma;   // Rx Interrupt flag
  ADT_UInt16 TxEdma;   // Tx Interrupt flag
  ADT_UInt16 RxEdmaBaseAddr;   // Rx addr
  ADT_UInt16 TxEdmaBaseAddr;   // Tx addr

  ADT_UInt16 RxEdmaCCR; // RX CCR config
  ADT_UInt16 TxEdmaCCR; // TX CCR config
  
  ADT_UInt16 *DstStartL; // DMA RX buffer Surce L
  ADT_UInt16 *DstStartU; // DMA RX buffer Surce U
  ADT_UInt16 *SrcStartL; // DMA TX buffer Surce L
  ADT_UInt16 *SrcStartU; // DMA TX buffer Surce U
  ADT_UInt16 RxDMAInt;   // Rx Interrupt Number
  ADT_UInt16 TxDMAInt;   // Tx Interrupt Number
  
} PortAddr; 

// Mcbsp register base word-addresses in IO space
#define MCBSP0_BASE_ADDR 0x2800  // mcbsp0 base address
#define MCBSP1_BASE_ADDR 0x2C00  // mcbsp1 base address
#define MCBSP2_BASE_ADDR 0x3000  // mcbsp2 base address
// Mcbsp register offsets from base address
#define DRR2_OFFSET  0x00        // data recv. reg 2 offset
#define DRR1_OFFSET  0x01        // data recv. reg 1 offset
#define DXR2_OFFSET  0x02        // data transmit reg 2 offset
#define DXR1_OFFSET  0x03        // data transmit reg 1 offset

#pragma DATA_SECTION (DMACfg,    "FAST_DATA_SECT")
PortAddr DMACfg [] = {
 { MCBSP0_BASE_ADDR, DMA_CHAN_RDY_4, DMA_CHAN_RDY_5, 
   DMA_CHAN4_BASE_ADDR, DMA_CHAN5_BASE_ADDR,
#ifdef ENDPROG
   0xC101 | DMA_PRIO, 0x3102 | DMA_PRIO,
#else
   0xCB01 | DMA_PRIO, 0x3B02 | DMA_PRIO,
#endif
   DMAChan4DstStartL,
   DMAChan4DstStartU,
   DMAChan5SrcStartL,
   DMAChan5SrcStartU,
   14, 15
 },

 { MCBSP1_BASE_ADDR, DMA_CHAN_RDY_2, DMA_CHAN_RDY_3, 
   DMA_CHAN2_BASE_ADDR, DMA_CHAN3_BASE_ADDR,
#ifdef ENDPROG
   0xC105 | DMA_PRIO, 0x3106 | DMA_PRIO,
#else
   0xCB05 | DMA_PRIO, 0x3B06 | DMA_PRIO,
#endif
   DMAChan2DstStartL,
   DMAChan2DstStartU,
   DMAChan3SrcStartL,
   DMAChan3SrcStartU,
   20, 21
  },

 { MCBSP2_BASE_ADDR, DMA_CHAN_RDY_0, DMA_CHAN_RDY_1, 
   DMA_CHAN0_BASE_ADDR, DMA_CHAN1_BASE_ADDR, 
#ifdef ENDPROG
   0xC109 | DMA_PRIO, 0x310A | DMA_PRIO,
#else
   0xCB09 | DMA_PRIO, 0x3B0A | DMA_PRIO,
#endif
   DMAChan0DstStartL,
   DMAChan0DstStartU,
   DMAChan1SrcStartL,
   DMAChan1SrcStartU,
   18, 9
 }
};

// DMA buffer related variables.

#pragma DATA_SECTION (RxDMABuffers,    "FAST_DATA_SECT")
#pragma DATA_SECTION (TxDMABuffers,    "FAST_DATA_SECT")
   
static ADT_PCM16 *RxDMABuffers[] = {
   BSP0DMA_RxBuffer, BSP1DMA_RxBuffer, BSP2DMA_RxBuffer
};

static ADT_PCM16 *TxDMABuffers[] = {
   BSP0DMA_TxBuffer, BSP1DMA_TxBuffer, BSP2DMA_TxBuffer 
};
#pragma DATA_SECTION (DmaPongOffset,    "FAST_DATA_SECT")
ADT_UInt16 DmaPongOffset [] = { 0, 0, 0 };  // Offset from ping buffer to pong buffer

#pragma DATA_SECTION (LocalAccumDmaFlags,    "FAST_DATA_SECT")
static ADT_UInt16 LocalAccumDmaFlags = 0;      // accumulated DMA flag bits
// -------------------------------------------
//  DMA TCC events to port assignments
//#pragma DATA_SECTION (RxEvents,    "FAST_DATA_SECT")
#pragma DATA_SECTION (RxEvents,    "FAST_DATA_SECT")
#pragma DATA_SECTION (TxEvents,    "FAST_DATA_SECT")
ADT_UInt16 RxEvents [] = { 0, 0, 0 };
ADT_UInt16 TxEvents [] = { 0, 0, 0 };


//---------------------------------------------------------
//   Runtime structures
//
#pragma DATA_SECTION (DmaRxBuff,    "FAST_DATA_SECT")
#pragma DATA_SECTION (DmaTxBuff,    "FAST_DATA_SECT")
#pragma DATA_SECTION (DmaRxOffset,  "FAST_DATA_SECT")
#pragma DATA_SECTION (DmaTxOffset,  "FAST_DATA_SECT")

ADT_PCM16 *DmaRxBuff [NUM_TDM_PORTS];   // DMA Rcv buffer address for McBSP
ADT_PCM16 *DmaTxBuff [NUM_TDM_PORTS];   // DMA Xmt buffer address for McBSP
ADT_UInt16 DmaRxOffset [NUM_TDM_PORTS];   // current Rx DMA buffer offset (ping or pong) for McBSP
ADT_UInt16 DmaTxOffset [NUM_TDM_PORTS];   // current Tx DMA buffer offset (ping or pong) for McBSP
ADT_UInt16 DmaEventMask = 0;

//  Debugging aides
ADT_Bool  FrameStamp = FALSE;         // Time stamp is to be placed on every 8th sample
struct {
   ADT_Bool   firstTime;
   ADT_UInt16 TxPatternSlot1;       // Debug pattern to be placed on Tx timeslot 1
   ADT_UInt16 TxPatternAllSlots;    // Debug pattern to be placed on all Tx timeslots
   ADT_UInt16 RxPatternSlot1;       // Debug pattern to be placed on Rx timeslot 1
   ADT_UInt16 RxPatternAllSlots;    // Debug pattern to be placed on all Rx timeslots

   ADT_UInt16 TxSineSlot;           // Slot to transmit sine pattern
   ADT_UInt16 BroadcastPort;
   ADT_UInt16 BroadcastSlot;
   ADT_UInt16 captureTx;
   ADT_UInt16 captureRx;
} dma = { TRUE, 0, 0, 0, 0,
          0xffff, 0, 0xffff, 0xffff, 0xffff };

//--------------------------------------------------------------------------------
//
//   TEST_SWCOMPAND = Testing S/W compand

#ifdef TEST_SWCOMPAND // convert linear sampled to compressed data to test sw comapnding
   #define muLawExpand(inout,cnt)    G711_ADT_muLawExpand (inout,inout,cnt)
   #define muLawCompress(inout,cnt)  G711_ADT_muLawCompress(inout,inout,cnt)
#else 
   #define muLawExpand(inout, cnt)
   #define muLawCompress(inout,cnt)
#endif

//--------------------------------------------------------------------------------
//
//   Debug = Enable fixed pattern testing and out of sync buffers

#ifndef _DEBUG
   ADT_UInt16 TogglePCMInSignal (int port)  { return 0xff; }
   ADT_UInt16 TogglePCMOutSignal (int port) { return 0xff; }
   #define CheckTxPointers(circ,dma)  
   #define CheckRxPointers(circ,dma)    
   #define FixedPatternTx(TxDmaBufr) 
   #define FixedPatternRx(RxDmaBufr) 
#else

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ToggleTestSignal
//
// FUNCTION
//   Toggles receive port between DMA buffer (normal) and constant tone (test)
//
// RETURNS
//  nothing
//
static ADT_PCM16 SineTableM12[] = {
  -3, 2828, 4000, 2828, -2, -2828, -4000, -2828,
  -1, 2829, 4000, 2828,  0, -2828, -4000, -2828,
};
static ADT_PCM16 Drain [MAX_SAMPLES_PER_MS*2][MAX_CHANS_PER_FRAME];     // Dummy receive buffer
static ADT_PCM16 Dummy [MAX_SAMPLES_PER_MS*2][MAX_CHANS_PER_FRAME];     // Dummy transmit buffer
static ADT_PCM16 BroadcastBufr [MAX_SAMPLES_PER_MS];

ADT_UInt16 inCaptureBuff [900];
CircBufInfo_t captureIn = {
  inCaptureBuff, sizeof (inCaptureBuff)/2, 0, 0 
};
ADT_UInt16 outCaptureBuff [900];
CircBufInfo_t captureOut = {
  outCaptureBuff, sizeof (outCaptureBuff)/2, 0, 0 
};
#pragma DATA_SECTION (inCaptureBuff, "SLOW_DATA_SECT")
#pragma DATA_SECTION (outCaptureBuff, "SLOW_DATA_SECT")

static void sineBuffer (unsigned int port, ADT_PCM16 *Buffer) {
   int i, dmaSltCnt;

   // Populate dummy buffer with 1 kHz SINE wave on all channels
   dmaSltCnt = sysConfig.maxSlotsSupported[port];

   for (i=0; i<dmaSltCnt; i++) {
      memcpy (Buffer, SineTableM12, sizeof (SineTableM12));
      Buffer += (SAMPLES_PER_INTERRUPT * 2);
   }

}

ADT_UInt16 TogglePCMInSignal (unsigned int port) {

   if (NUM_TDM_PORTS <= port) return 0;

   sineBuffer (port, (ADT_PCM16 *) &Dummy);

   if (DmaRxBuff[port] == RxDMABuffers[port]) {
       DmaRxBuff[port] = (ADT_PCM16 *) &Dummy;
       return 1;
   }
   // Restore
   DmaRxBuff[port] = RxDMABuffers[port];
   return 0;
}

ADT_UInt16 TogglePCMOutSignal (unsigned int port) {

   if (NUM_TDM_PORTS <= port) return 0;

   if (DmaTxBuff[port] == TxDMABuffers[port]) {
       sineBuffer (port, TxDMABuffers[port]);
       DmaTxBuff[port] = (ADT_PCM16 *) &Drain;
       return 1;
   }
   // Restore with initial zeroes
   DmaTxBuff[port] = TxDMABuffers[port];
   memset (TxDMABuffers[port], 0, sysConfig.dmaBufferSize[port] *sizeof(ADT_PCM16));
   return 0;
}

static void FixedPatternTx (ADT_PCM16 *TxDmaBufr) {
   ADT_PCM16 *DmaBufr;
   int i;

   if (dma.TxPatternSlot1) {
      DmaBufr = TxDmaBufr;
      for (i=0; i<SAMPLES_PER_INTERRUPT; i++) *DmaBufr++ = dma.TxPatternSlot1;
   }

   if (dma.TxSineSlot == 0) {
      memcpy (TxDmaBufr, SineTableM12, SAMPLES_PER_INTERRUPT *sizeof(ADT_PCM16));
   }
   if (dma.captureTx == 0 && dma.captureRx != 0xffff) {
      copyLinearToCirc (&ApiBlock.DmaSwiCnt, &captureOut, 1);
      copyLinearToCirc (TxDmaBufr, &captureOut, SAMPLES_PER_INTERRUPT);
      captureOut.TakeIndex = captureOut.PutIndex;
   }

}
static void FixedPatternRx (ADT_PCM16 *RxDmaBufr) {
   ADT_PCM16 *DmaBufr;
   int i;

   // Overwrite channel(s) output with a fixed (non-zero) value
   if (dma.BroadcastSlot == 0)
      memcpy (BroadcastBufr, RxDmaBufr, sizeof (BroadcastBufr));
   else if (dma.BroadcastSlot != 0xffff)
      memcpy (RxDmaBufr, BroadcastBufr, sizeof (BroadcastBufr));

   if (dma.RxPatternSlot1) {
      DmaBufr = RxDmaBufr;
      for (i=0; i<SAMPLES_PER_INTERRUPT; i++) *DmaBufr++ = dma.RxPatternSlot1;
   }
   if (dma.captureRx == 0 && dma.captureTx != 0xffff) {
      copyLinearToCirc (&ApiBlock.DmaSwiCnt, &captureIn, 1);
      copyLinearToCirc (RxDmaBufr, &captureIn, SAMPLES_PER_INTERRUPT);
      captureIn.TakeIndex = captureIn.PutIndex;
   }
   if (FrameStamp) {
      *RxDmaBufr = (ADT_PCM16) ApiBlock.DmaSwiCnt;
   }     
}


inline void CheckTxPointers (CircBufInfo_t *circ, ADT_PCM16 *dma) {
   if (circ == pDrain) return;
   LogTransfer (0xC1, circ, &circ->pBufrBase[circ->TakeIndex], 0, dma, SAMPLES_PER_INTERRUPT);
   AppErr ("TxPointerError", getAvailable (circ) < SAMPLES_PER_INTERRUPT);
}
inline void CheckRxPointers (CircBufInfo_t *circ, ADT_PCM16 *dma) {
   if (circ == pSink) return;
   LogTransfer (0x1C, 0, dma, circ, &circ->pBufrBase[circ->PutIndex], SAMPLES_PER_INTERRUPT);
   AppErr ("RxPointerError", getFreeSpace (circ) < SAMPLES_PER_INTERRUPT);
}
#endif

//---------------------------------------------------
//
//  DMAIntialize 
//
// FUNCTION
//   - Disables all DMA; initializes interrupt variables
//
void DMAInitialize () {
   int port;

   // GLOBAL CONTROL REGISTER:
   //  1) emulation breakpoint halts DMA: FREE=0, 
   //  2) HPI shares SARAM/DARAM with DMA: EHPIEXCL=0
   //  3) HPI has lower priority than other DMA accesses: EHPIPRIO=0
   IO_REG_WRITE(DMA_GCR_BASE_ADDR, 0, 0x0008);

   // GLOBAL SOFTWARE COMPATABILITY REGISTER
   //  compatability mode: DINDXMD=0
   IO_REG_WRITE(DMA_GSCR_BASE_ADDR, 0, 0x0000);

   // GLOBAL TIMEOUT CONTROL REGISTER 
   //  1) daram timeout counter disabled: DTCE=0
   //  2) saram timeout counter disabled: STCE=0
   IO_REG_WRITE(DMA_GTCR_BASE_ADDR, 0, 0x0000);

   // Prevent all DMA interrupts.
   IER0 &= ~(IER0_DMA1 | IER0_DMA4 | IER0_DMA5);
   IER1 &= ~(IER1_DMA0 | IER1_DMA2 | IER1_DMA3);
   if (dma.firstTime) {
      HWI_dispatchPlug (DMACfg[0].RxDMAInt, (Fxn) &GpakDma4Isr, NULL);
      HWI_dispatchPlug (DMACfg[0].TxDMAInt, (Fxn) &GpakDma5Isr, NULL);
      HWI_dispatchPlug (DMACfg[1].RxDMAInt, (Fxn) &GpakDma2Isr, NULL);
      HWI_dispatchPlug (DMACfg[1].TxDMAInt, (Fxn) &GpakDma3Isr, NULL);
      HWI_dispatchPlug (DMACfg[2].RxDMAInt, (Fxn) &GpakDma0Isr, NULL);
      HWI_dispatchPlug (DMACfg[2].TxDMAInt, (Fxn) &GpakDma1Isr, NULL);
      dma.firstTime = FALSE;
   }
   DmaEventMask = 0;

   // Set the multiplexed interrupt control for all DMA interrupts.
   for (port=0; port<NUM_TDM_PORTS; port++) {
      DmaRxBuff[port] = RxDMABuffers[port];
      DmaTxBuff[port] = TxDMABuffers[port];

      disableDMA (port);
   }
   LocalAccumDmaFlags = 0;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// enableDMA
//
// FUNCTION
//   Enables Tx and Rx DMA for specified McBSP
//
// RETURNS
//   Bit mask for event interrupts
//

ADT_UInt16 enableDMA (int port) {
   ADT_UInt16 tccBits; // mask

   if (NUM_MCBSPS <= port) return 0;

   RxEvents[port] = DMACfg[port].RxEdma;
   TxEvents[port] = DMACfg[port].TxEdma;

   tccBits = (DMACfg[port].RxEdma) | (DMACfg[port].TxEdma);
   DmaEventMask |= tccBits;

   // clear all pending dma events and transfer completion interrupts for port
   // disable all edma events and interrupts for port
   if(port == 0){
      IER0 &= ~(IER0_DMA4 | IER0_DMA5);
      IFR0 &= ~(IER0_DMA4 | IER0_DMA5);  // Clear any pending DMA interrupts
   }
   else if(port == 1) {
      IFR1 &= ~(IER1_DMA2 | IER1_DMA3);  // Clear any pending DMA interrupts
      IER1 &= ~(IER1_DMA2 | IER1_DMA3);
   }
   else {
      IFR1 &= ~IER1_DMA0;  // Clear any pending chan0 DMA interrupts
      IFR0 &= ~IER0_DMA1;  // Clear any pending chan1 DMA interrupts
      IER0 &= ~(IER0_DMA1);
      IER1 &= ~(IER1_DMA0);
	}

   // Configure Rx DMA
   initRxDMAParams(port);
   // Configure Tx DMA
   initTxDMAParams(port);

   DmaRxOffset[port] = 0; 
   DmaTxOffset[port] = 0; 
   DmaPongOffset[port] = SAMPLES_PER_INTERRUPT * MaxDmaSlots[port];

   // Zero receive buffer
   memset (RxDMABuffers[port], 0, sysConfig.dmaBufferSize[port] *sizeof(ADT_PCM16)); 

   // enable edma events and transfer completion interrupts for port
   if(port == 0) {
      IER0 |= (IER0_DMA4 | IER0_DMA5);   // Enable chan4,5 DMA interrupts
   } else if(port == 1){
      IER1 |= (IER1_DMA2 | IER1_DMA3);   // Enable chan2,3 DMA interrupts
   } else if(port == 2) {
      IER1 |= IER1_DMA0;   // Enable chan0 DMA interrupt
      IER0 |= IER0_DMA1;   // Enable chan1 DMA interrupt
   }

   // Enable DMA channels 4 and 5. Set endprog to 1, instructing DMA to 
   // copy the configuration regs to the working regs.
   IO_REG_OR(DMACfg[port].RxEdmaBaseAddr, CCR_OFFSET, 0x0880);
   IO_REG_OR(DMACfg[port].TxEdmaBaseAddr, CCR_OFFSET, 0x0880);

   // initRxDMAParams() set the current Rx DMA buffer to Ping, 
   // once the DMA start, set the buffer to Pong for the next transfer
   // program the configuration for the next RX DMA block transfer
   updateNextDmaAdress(DMACfg[port].RxEdmaBaseAddr, 1,
                DMACfg[port].DstStartL[1], 
                DMACfg[port].DstStartU[1],
                1);

   // initTxDMAParams() set the current Tx DMA buffer to Ping, 
   // once the DMA start, set the buffer to Pong for the next transfer
   // program the configuration for the next TX DMA block transfer
   updateNextDmaAdress(DMACfg[port].TxEdmaBaseAddr, 0,
                DMACfg[port].SrcStartL[1], 
                DMACfg[port].SrcStartU[1],
                1);

   //next step should enable McBSP, move the McBSP out of reset
   // which will be done in the GpakPcm.c following this function
   LocalAccumDmaFlags = 0;

   return tccBits;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// disableDMA
//
// FUNCTION
//   This function resets the EDMA controller
//
// RETURNS
//   
//   Disabled transfer control bits 
ADT_UInt16 disableDMA (int port) {

   ADT_UInt16 tccBits = 0;

   if (NUM_TDM_PORTS <= port) return 0;

   // Zero transmit buffer
   memset (TxDMABuffers[port], 0, sysConfig.dmaBufferSize[port] *sizeof(ADT_PCM16)); 

   tccBits = (DMACfg[port].RxEdma) | (DMACfg[port].TxEdma);
   DmaEventMask &= ~tccBits;

   RxEvents[port] = 0;
   TxEvents[port] = 0;

    // disable all edma events and interrupts for port
   if(port == 0){
      IER0 &= ~(IER0_DMA4 | IER0_DMA5);
      IFR0 &= ~(IER0_DMA4 | IER0_DMA5);  // Clear any pending DMA interrupts
   }
   else if(port == 1) {
      IFR1 &= ~(IER1_DMA2 | IER1_DMA3);  // Clear any pending DMA interrupts
      IER1 &= ~(IER1_DMA2 | IER1_DMA3);
   }
   else {
      IFR1 &= ~IER1_DMA0;  // Clear any pending DMA interrupts
      IFR0 &= ~IER0_DMA1;  // Clear any pending DMA interrupts
      IER0 &= ~(IER0_DMA1);
      IER1 &= ~(IER1_DMA0);
	}
    // clear all pending dma events and interrupts for port
	// Disable DMA channels
    IO_REG_AND(DMACfg[port].RxEdmaBaseAddr, CCR_OFFSET, ~0x0080);
    IO_REG_AND(DMACfg[port].TxEdmaBaseAddr, CCR_OFFSET, ~0x0080);

	return tccBits;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// initRxDMAParams
//
// FUNCTION
//  Initializes EDMA parameter ram for EDMA channels that are driven by receive events
//
// Inputs
//    port            - McBSPn
//
//  Outputs
//    DMA Parameters - DMA transfer parameters
//    eer            - Event enable register
//    cier           - Channel interrupt enable register
//
// RETURNS
//  none
//
static void initRxDMAParams (ADT_UInt32 port) {

   ADT_UInt32  addr;
   ADT_UInt16 addrL;
   ADT_UInt16 addrU;
   ADT_Int16 elementIndex;
   ADT_Int16 frameIndex, i;
   ADT_UInt16 temp;

   if (NUM_TDM_PORTS <= port) return;

        // init Rx DMA to read from MCBSP(port)
        IO_REG_WRITE(DMACfg[port].RxEdmaBaseAddr, CSDP_OFFSET, 0x020D); 
        IO_REG_WRITE(DMACfg[port].RxEdmaBaseAddr, CCR_OFFSET, DMACfg[port].RxEdmaCCR);
        IO_REG_WRITE(DMACfg[port].RxEdmaBaseAddr, CICR_OFFSET, 0x0020); // BLOCKIE
        // init the source start byte-address registers: DMACSSAU, DMACSSAL
        addr = (ADT_UInt32)(DMACfg[port].Base+DRR1_OFFSET)<<1; 
        addrL = (ADT_UInt16)addr;
        addr >>= 16;
        addrU = (ADT_UInt16)(addr & 0xFF);
        IO_REG_WRITE(DMACfg[port].RxEdmaBaseAddr, CSSA_L_OFFSET, addrL);
        IO_REG_WRITE(DMACfg[port].RxEdmaBaseAddr, CSSA_U_OFFSET, addrU);

        // init the destination start byte-address registers: DMACDSAU, DMACDSAL
        addr = ((ADT_UInt32)(ADT_UInt32) RxDMABuffers[port])<<1;
        DMACfg[port].DstStartL[0] = (ADT_UInt16)addr;
        DMACfg[port].DstStartU[0] = (ADT_UInt16)((addr>>16) & 0xFF);
        IO_REG_WRITE(DMACfg[port].RxEdmaBaseAddr, CDSA_L_OFFSET, DMACfg[port].DstStartL[0]);
        IO_REG_WRITE(DMACfg[port].RxEdmaBaseAddr, CDSA_U_OFFSET, DMACfg[port].DstStartU[0]);
                     
        // Compute Rcv DMA buffer mid-point address, PONG
        addr = ((ADT_UInt32)&RxDMABuffers[port][SAMPLES_PER_INTERRUPT*DmaSlotCnt[port]])<<1;
        DMACfg[port].DstStartL[1] = (ADT_UInt16)addr;
        addr >>= 16;
        DMACfg[port].DstStartU[1] = (ADT_UInt16)(addr & 0xFF);

        // init the element number and frame number registers: DMACEN, DMACFN
        IO_REG_WRITE(DMACfg[port].RxEdmaBaseAddr, CEN_OFFSET, DmaSlotCnt[port]);
        IO_REG_WRITE(DMACfg[port].RxEdmaBaseAddr, CFN_OFFSET, SAMPLES_PER_INTERRUPT);

        elementIndex = (SAMPLES_PER_INTERRUPT*2) - 1;
        frameIndex   = -(((DmaSlotCnt[port]-1) * SAMPLES_PER_INTERRUPT*2) - 1); 
        
        IO_REG_WRITE(DMACfg[port].RxEdmaBaseAddr, CEI_OFFSET, elementIndex);
        IO_REG_WRITE(DMACfg[port].RxEdmaBaseAddr, CFI_OFFSET, frameIndex);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// initTxDMAParams - Initialize the EDMA parameter ram for transmit channels
//
// FUNCTION
//  Initializes EDMA parameter ram for EDMA channels that are driven by transmit events
//
// Inputs
//    port            - McBSPn
//
//  Outputs
//    DMA Parameters - DMA transfer parameters
//    eer            - Event enable register
//    cier           - Channel interrupt enable register
//
// RETURNS
//  none
//
static void initTxDMAParams (ADT_UInt32 port) {
                
   ADT_UInt32  addr;
   ADT_UInt16 addrL;
   ADT_UInt16 addrU;
   ADT_Int16 elementIndex;
   ADT_Int16 frameIndex;
   ADT_UInt16 temp;

   if (NUM_TDM_PORTS <= port) return;

		// init Tx DMA Channel to write to MCBSP0[port].
        IO_REG_WRITE(DMACfg[port].TxEdmaBaseAddr, CSDP_OFFSET, 0x0605); 
        IO_REG_WRITE(DMACfg[port].TxEdmaBaseAddr, CCR_OFFSET, DMACfg[port].TxEdmaCCR);
        IO_REG_WRITE(DMACfg[port].TxEdmaBaseAddr, CICR_OFFSET, 0x0020); // BLOCKIE

        // init the destination start byte-address registers: DMACDSAU, DMACDSAL
        addr = (ADT_UInt32)(DMACfg[port].Base+DXR1_OFFSET)<<1; 
        addrL = (ADT_UInt16)addr;
        addrU = (ADT_UInt16)((addr>>16) & 0xFF);
        IO_REG_WRITE(DMACfg[port].TxEdmaBaseAddr, CDSA_L_OFFSET, addrL);
        IO_REG_WRITE(DMACfg[port].TxEdmaBaseAddr, CDSA_U_OFFSET, addrU);

        // init the source start byte-address registers: DMACSSAU, DMACSSAL
        addr = ((ADT_UInt32)TxDMABuffers[port])<<1;
        DMACfg[port].SrcStartL[0] = (ADT_UInt16)addr;
        addr >>= 16;
        DMACfg[port].SrcStartU[0] = (ADT_UInt16)(addr & 0xFF);
        IO_REG_WRITE(DMACfg[port].TxEdmaBaseAddr, CSSA_L_OFFSET, DMACfg[port].SrcStartL[0]);
        IO_REG_WRITE(DMACfg[port].TxEdmaBaseAddr, CSSA_U_OFFSET, DMACfg[port].SrcStartU[0]);
                     
        // Compute Xmt DMA 5 buffer mid-point address
        addr = ((ADT_UInt32)&TxDMABuffers[port][SAMPLES_PER_INTERRUPT*DmaSlotCnt[port]])<<1;
        DMACfg[port].SrcStartL[1] = (ADT_UInt16)addr;
        addr >>= 16;
        DMACfg[port].SrcStartU[1] = (ADT_UInt16)(addr & 0xFF);

        // init the element number and frame number registers: DMACEN, DMACFN
        IO_REG_WRITE(DMACfg[port].TxEdmaBaseAddr, CEN_OFFSET, DmaSlotCnt[port]);
        IO_REG_WRITE(DMACfg[port].TxEdmaBaseAddr, CFN_OFFSET, SAMPLES_PER_INTERRUPT);

        elementIndex = (SAMPLES_PER_INTERRUPT*2) - 1;
        frameIndex   = -(((DmaSlotCnt[port]-1) * SAMPLES_PER_INTERRUPT*2) - 1); 
       // init the element index and frame index registers: DMACEN, DMACFN
        IO_REG_WRITE(DMACfg[port].TxEdmaBaseAddr, CEI_OFFSET, elementIndex);
        IO_REG_WRITE(DMACfg[port].TxEdmaBaseAddr, CFI_OFFSET, frameIndex);

        // read the DMA status registers so new interrupts can be serviced
        IO_REG_READ(DMACfg[port].RxEdmaBaseAddr, CSR_OFFSET, temp);
        IO_REG_READ(DMACfg[port].TxEdmaBaseAddr, CSR_OFFSET, temp);
 
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// DmaLostSync
//
// FUNCTION
//    checks that dma transfer and SWI copy are working in opposite buffers
//
// Inputs
//
//    DmaSlots    - Number of dma slot for port
//    DmsStart    - Start of DMA buffer
//    SWIOffset   - Offset of ping/pong bvffer used by SWI copy
//    DmaPos      - DMA register address pointing to current address of DMA transfer
//
// RETURNS
//    Lost sync indication
//
static inline ADT_Bool DmaLostSync (int DmaSlots,         ADT_UInt16  *DmaStart_p,
                                    ADT_UInt16 SWIOffset, ADT_UInt16 DmaCurLo16) {
   ADT_UInt16 DmaCur, DmaStart;
   ADT_Bool outOfRange, wrongBuffer;

   DmaCur = (ADT_UInt16 )DmaCurLo16;
   DmaStart= (ADT_UInt16 )DmaStart_p;


   // Verify within range of DMA buffer
   outOfRange = (DmaCur < DmaStart) || ((DmaStart + (DmaSlots * 2 * SAMPLES_PER_INTERRUPT)) <= DmaCur);
 
   // Verify within range of current (ping/pong) buffer
   if (SWIOffset == 0) DmaStart += (DmaSlots * SAMPLES_PER_INTERRUPT);
 
   wrongBuffer = (DmaCur < DmaStart) || ((DmaStart + (DmaSlots * SAMPLES_PER_INTERRUPT)) <= DmaCur);
 
   return (outOfRange || wrongBuffer);
}

//
//  Switch copy between ping and pong buffers
//
inline switchBuffers (int port, ADT_UInt16 *offset) {
   if (*offset == 0) *offset = DmaPongOffset[port]; 
   else              *offset = 0;
   return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// zeroTxBuffer
//
// FUNCTION
//  Zeroes slot on Tx Buffer when channel goes inactive
//
// Inputs
//    port            - McBSPn
//    dmaSlot         - dma index of slot to zero
//
// RETURNS
//  none
//
void zeroTxBuffer (int port, int dmaSlot) {
   ADT_PCM16 *txSlot;

   if (NUM_TDM_PORTS <= port) return;

   txSlot = TxDMABuffers[port];

   // Ping buffer fill memory
   txSlot += dmaSlot * SAMPLES_PER_INTERRUPT; 
   memset (txSlot, 0, SAMPLES_PER_INTERRUPT *sizeof(ADT_PCM16));

   // Pong buffer fill memory
   txSlot +=  DmaPongOffset [port]; 
   memset (txSlot, 0, SAMPLES_PER_INTERRUPT *sizeof(ADT_PCM16));
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// CopyTxDmaBuffers - Copy channel PCM buffers to serial port DMA buffers.
//
// FUNCTION
//   Multiplexs PCM data from active channel PCM buffers onto a serial 
//   port's DMA buffers.
//
// Inputs
//     slotCnt       -  Count of TDM slots
//     TxCircBufrList -  List of transmit circular PCM buffer pointers
//     TxDmaBufr     -  Linear dma transmit buffer
//
// RETURNS
//     lostSync indication
//
ADT_Bool CopyDmaTxBuffers (int port, int slotCnt, CircBufInfo_t **TxCircBufrList, int slip) {
   
   int k, buffCnt, lostSync, pongIndex = 0;
   CircBufInfo_t *TxCirc, **TxCircBufr;
   ADT_PCM16     *TxDmaBufr;
   ADT_UInt16    *TxOffset;
   ADT_UInt16    temp;
   ADT_PCM16     *DmaBufr0;
   ADT_WOrd      SlipSamps;

   TxOffset = &DmaTxOffset[port];

   // program the configuration for the next TX DMA block transfer
   if(*TxOffset == 0)
	   pongIndex = 0;
   else
	   pongIndex = 1;
   updateNextDmaAdress(DMACfg[port].TxEdmaBaseAddr, 0,
                DMACfg[port].SrcStartL[pongIndex], 
                DMACfg[port].SrcStartU[pongIndex],
                0);
#ifndef CHIP_5509 
   // Adjust the SWI copy buffer offset (TxOffset) if the DMA and SWI buffers are conflicting
   IO_REG_READ(DMACfg[port].TxEdmaBaseAddr, CSAC_OFFSET, temp); // read current copying address
   temp = (temp >> 1); // byte addresss to word address conversion
   lostSync = DmaLostSync (slotCnt, TxDMABuffers[port], *TxOffset, temp);
   if (lostSync) {
      SendWarningEvent (port + 0x200, WarnFrameSyncError);
      switchBuffers (port, TxOffset);
   }
#endif
   // Set the circular buffer and linear DMA pointers to the first slot.
   TxCircBufr = TxCircBufrList;
   TxDmaBufr  = DmaTxBuff[port] + *TxOffset;
   DmaBufr0   = TxDmaBufr;

   SlipSamps = 0;
   if (slip)
       SlipSamps = SAMPLES_PER_INTERRUPT;
   
   // Loop through all configured slots checking for active slots.
   for (k = 0; k < slotCnt; k++, TxCircBufr++)  {
      TxCirc = *TxCircBufr;
   
      // Copy from active outbound channels' circular buffer to linear DMA buffer
      TxCirc->SlipSamps += SlipSamps;
      CheckTxPointers (TxCirc, TxDmaBufr);   // Make sure there are enough samples for transfers
      buffCnt = TxCirc->BufrSize - TxCirc->TakeIndex;

      if (buffCnt <= SAMPLES_PER_INTERRUPT) {
         memcpy (TxDmaBufr, &TxCirc->pBufrBase[TxCirc->TakeIndex], buffCnt*sizeof(ADT_PCM16));
         TxDmaBufr += buffCnt;
         buffCnt = SAMPLES_PER_INTERRUPT - buffCnt;
         TxCirc->TakeIndex = 0;
      } else {
         buffCnt = SAMPLES_PER_INTERRUPT;   
      }
      memcpy (TxDmaBufr, &TxCirc->pBufrBase[TxCirc->TakeIndex], buffCnt *sizeof(ADT_PCM16));
      TxCirc->TakeIndex += buffCnt;
      TxDmaBufr += buffCnt;

      // NOTE: muLawExpand is for testing only
      muLawExpand ((void *)(TxDmaBufr-SAMPLES_PER_INTERRUPT), SAMPLES_PER_INTERRUPT);


#ifdef _DEBUG  // Verify that frame markers are correct
      if (FrameStamp)
         AppErr ("FrameStampError",
                      *(TxDmaBufr-(SAMPLES_PER_INTERRUPT*2)) != 0 &&
                      *(TxDmaBufr-(SAMPLES_PER_INTERRUPT*2)) != (short) (ApiBlock.DmaSwiCnt & 0xFFFF));
#endif


   }
   FixedPatternTx (DmaBufr0);   // Allow overwrite of slot 0 with fixed pattern for debug
   customTDMOut (DmaBufr0, port, slotCnt);
   switchBuffers (port, TxOffset);


   return lostSync;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// CopyDmaRxBuffers - Copy serial port DMA buffers to channel PCM buffers.
//
// FUNCTION
//   Demultiplexs PCM data between a serial port's DMA buffers and the
//   PCM circular buffers of any active channels using the serial port.
//
// Inputs
//     port          -  Count of TDM slots
//     RxCircBufrList -  List of receive circular PCM buffer pointers
//     RxDmaBufr     -  Linear dma receive buffer
//
// RETURNS
//     lostSync indication
//
ADT_Bool CopyDmaRxBuffers (int port, int slotCnt, CircBufInfo_t **RxCircBufrList, int slip) {
   
   int k, buffCnt, lostSync, pongIndex = 0;
   CircBufInfo_t *RxCirc, **RxCircBufr;
   ADT_PCM16     *RxDmaBufr;
   ADT_UInt16    *RxOffset;
   ADT_UInt16    temp;
   ADT_Word      SlipSamps;

   RxOffset = &DmaRxOffset[port];

   // program the configuration for the next RX DMA block transfer
   if(*RxOffset == 0)
	   pongIndex = 0;
   else
	   pongIndex = 1;
   updateNextDmaAdress(DMACfg[port].RxEdmaBaseAddr, 1,
                DMACfg[port].DstStartL[pongIndex], 
                DMACfg[port].DstStartU[pongIndex],
                0);

#ifndef CHIP_5509 
   // Adjust the SWI copy buffer offset (RxOffset) if the DMA and SWI buffers are conflicting
   IO_REG_READ(DMACfg[port].RxEdmaBaseAddr, CDAC_OFFSET, temp); // read current copying address
   temp = (temp >> 1); // byte addresss to word address conversion
   lostSync = DmaLostSync (slotCnt, RxDMABuffers[port], *RxOffset, temp);
   if (lostSync) {
      SendWarningEvent (port + 0x100, WarnFrameSyncError);
      switchBuffers (port, RxOffset);
   }
#endif
   // Set the circular buffer and linear DMA pointers to the first slot.
   RxCircBufr = RxCircBufrList;
   RxDmaBufr  = DmaRxBuff[port] + *RxOffset;
   
   // Loop through all configured slots checking for active slots.
   FixedPatternRx  (RxDmaBufr);  // Allow overwrite of slot 0 with fixed pattern for debug
   customTDMIn (RxDmaBufr, port, slotCnt);

   SlipSamps = 0;
   if (slip)
       SlipSamps = SAMPLES_PER_INTERRUPT;
   for (k = 0; k < slotCnt; k++, RxCircBufr++)  {

      RxCirc = *RxCircBufr; 

      // Copy from active inbound DMA buffer to circular buffer
      RxCirc->SlipSamps += SlipSamps;
      CheckRxPointers (RxCirc, RxDmaBufr);   // Make sure there is enough samples/room for transfers

      // NOTE: muLawCompress for testing only
      muLawCompress ((void *)RxDmaBufr, SAMPLES_PER_INTERRUPT);

      buffCnt = RxCirc->BufrSize - RxCirc->PutIndex;
      if (buffCnt <= SAMPLES_PER_INTERRUPT) {
         memcpy (&RxCirc->pBufrBase[RxCirc->PutIndex], RxDmaBufr, buffCnt*sizeof(ADT_PCM16) );
         RxDmaBufr += buffCnt;
         buffCnt = SAMPLES_PER_INTERRUPT - buffCnt;
         RxCirc->PutIndex = 0;
      } else {
         buffCnt = SAMPLES_PER_INTERRUPT;   
      }
      memcpy (&RxCirc->pBufrBase[RxCirc->PutIndex], RxDmaBufr, buffCnt*sizeof(ADT_PCM16));
      RxCirc->PutIndex += buffCnt;
      RxDmaBufr += buffCnt;
   }   

   switchBuffers (port, RxOffset);
   return lostSync;
}
#if 1
int DmaLbRxToTxBuffers (int port, int slotCnt) {
   int k, buffCnt, txlostSync, rxlostSync, rv, pongIndex = 0;
   ADT_PCM16     *RxDmaBufr;
   ADT_UInt16    *RxOffset;
   ADT_PCM16     *TxDmaBufr;
   ADT_UInt16    *TxOffset, temp;
#ifdef PIKACROSSOVER
   ADT_UInt16    *TxOffsetCross;
#endif
   rv = 0;              
   RxOffset = &DmaRxOffset[port];
   TxOffset = &DmaTxOffset[port];
#ifdef PIKACROSSOVER
   TxOffsetCross = &DmaTxOffset[(port ^ 1)]; // cross between McBsp0 and McBsp1
#endif
   // Adjust the SWI copy buffer offset (RxOffset) if the DMA and SWI buffers are conflicting
   if(*RxOffset == 0)
	   pongIndex = 0;
   else
	   pongIndex = 1;
   updateNextDmaAdress(DMACfg[port].RxEdmaBaseAddr, 1,
                DMACfg[port].DstStartL[pongIndex], 
                DMACfg[port].DstStartU[pongIndex],
                0);
#ifndef CHIP_5509 
   // Adjust the SWI copy buffer offset (RxOffset) if the DMA and SWI buffers are conflicting
   IO_REG_READ(DMACfg[port].RxEdmaBaseAddr, CDAC_OFFSET, temp); // read current copying address
   temp = (temp >> 1); // byte addresss to word address conversion
   rxlostSync = DmaLostSync (slotCnt, RxDMABuffers[port], *RxOffset, temp);
   if (rxlostSync) {
      rv |= 1;
      switchBuffers (port, RxOffset);
      SendWarningEvent (port + 0x100, WarnFrameSyncError);
   }
#endif
   // Adjust the SWI copy buffer offset (TxOffset) if the DMA and SWI buffers are conflicting
   if(*TxOffset == 0)
	   pongIndex = 0;
   else
	   pongIndex = 1;
   updateNextDmaAdress(DMACfg[port].TxEdmaBaseAddr, 0,
                DMACfg[port].SrcStartL[pongIndex], 
                DMACfg[port].SrcStartU[pongIndex],
                0);

#ifndef CHIP_5509 
   // Adjust the SWI copy buffer offset (TxOffset) if the DMA and SWI buffers are conflicting
   IO_REG_READ(DMACfg[port].TxEdmaBaseAddr, CSAC_OFFSET, temp); // read current copying address
   temp = (temp >> 1); // byte addresss to word address conversion
   txlostSync = DmaLostSync (slotCnt, TxDMABuffers[port], *TxOffset, temp);
   if (txlostSync) {
      rv |= 2;
      switchBuffers (port, TxOffset);
      SendWarningEvent (port + 0x200, WarnFrameSyncError);
   }
#endif
   // Set the linear DMA pointers to the first slot.
   RxDmaBufr  = DmaRxBuff[port] + *RxOffset;
#ifdef PIKACROSSOVER
   TxDmaBufr  = DmaTxBuff[(port^1)] + *TxOffsetCross;
#else
   TxDmaBufr  = DmaTxBuff[port] + *TxOffset;
#endif
   buffCnt = SAMPLES_PER_INTERRUPT;   

   for (k = 0; k < slotCnt; k++)  {
#ifdef PIKACROSSOVER
      if(k==0)  memcpy (TxDmaBufr, RxDmaBufr, buffCnt*sizeof(ADT_PCM16));
#else
	  memcpy (TxDmaBufr, RxDmaBufr, buffCnt*sizeof(ADT_PCM16));
#endif
      RxDmaBufr += buffCnt;
      TxDmaBufr += buffCnt;
   }   

   switchBuffers (port, RxOffset);
   switchBuffers (port, TxOffset);
   return rv;
}
#endif

#if 0 //def PIKA OLD code for the buffer copy
extern CircBufInfo_t *CrcRxBuff[NUM_MCBSPS][MAX_CHANS_PER_FRAME];	// act Rcv bufrs
extern CircBufInfo_t *CrcTxBuff[NUM_MCBSPS][MAX_CHANS_PER_FRAME];	// act Xmt bufrs
static inline ADT_Bool DmaLostSync (int DmaSlots,         ADT_UInt16  *DmaStart,
                                    ADT_UInt16 SWIOffset, ADT_UInt16 DmaCountRegOffset);
short int sin1k[8] = {0, 23167/8, 32767/8, 23167/8, 0, -23167/8, -32768/8, -23167/8};

void CopyDmaBuffers(
    ADT_UInt16 port,
	ADT_UInt16 numCfgSlots,					// number of configured slots
	ADT_UInt16 DmaBufrIndex,				// current index into DMA buffer
	CircBufInfo_t **pSlotRcvBufrBase,	// pntr to list of circ Rcv bufr pntrs
	CircBufInfo_t **pSlotXmtBufrBase,	// pntr to list of circ Xmt bufr pntrs
    ADT_UInt16 Loopback                     // Flag to enable Dma loopback
	)
{
   int k,j;							// loop index / counter
   CircBufInfo_t *pCircBufrInfo;	// pointer to Circular Buffer Info
   CircBufInfo_t **pSlotRcvBufr;	// current pointer to circ Rcv bufr pointer
   CircBufInfo_t **pSlotXmtBufr;	// current pointer to circ xmt bufr pointer
   ADT_UInt16 SlotPntrOffset;			// circ bufr list offset between elements
   ADT_UInt16 DmaIndex;				// index into DMA buffer
   ADT_UInt16   temp, lostSync; 
   ADT_UInt16 *pDmaRcvBuffer;				// Receive DMA buffer address
   ADT_UInt16 *pDmaXmtBuffer;				// Transmit DMA buffer address
   
   pDmaRcvBuffer = DmaRxBuff[port];
   pDmaXmtBuffer = DmaTxBuff[port];
   // program the configuration for the next RX DMA block transfer
   updateNextDmaAdress(DMACfg[port].RxEdmaBaseAddr, 1,
                DMACfg[port].DstStartL[DmaBufrIndex], 
                DMACfg[port].DstStartU[DmaBufrIndex],
                0);

   // program the configuration for the next TX DMA block transfer
   updateNextDmaAdress(DMACfg[port].TxEdmaBaseAddr, 0,
                DMACfg[port].SrcStartL[DmaBufrIndex], 
                DMACfg[port].SrcStartU[DmaBufrIndex],
                0);

   DmaIndex = numCfgSlots * DmaBufrIndex * SAMPLES_PER_INTERRUPT;

   IO_REG_READ(DMACfg[port].TxEdmaBaseAddr, CSAC_OFFSET, temp);
   temp = (temp >> 1); // byte addresss to word address conversion
   lostSync = DmaLostSync (numCfgSlots, TxDMABuffers[port], DmaIndex, temp);
   IO_REG_READ(DMACfg[port].RxEdmaBaseAddr, CDAC_OFFSET, temp); // read current copying address
   temp = (temp >> 1); // byte addresss to word address conversion
   lostSync = DmaLostSync (numCfgSlots, RxDMABuffers[port], DmaIndex, temp);

    // NOTE: 
    // No checks are made to determine if copies to/from the pcm circular buffer 
    // will overflow the end of the circ buffer. It's expected that the pcm 
    // buffer length is a multiple of NUM_FRAMES_PER_RUPT.

	// Determine the offset between elements in a list of circular buffer
	// pointers.
	//
	//  Note: MIPS are lower if done in this function even though this value
	//		  could be computed once at startup and saved as a global variable.
	SlotPntrOffset = &(CrcRxBuff[0][1]) - &(CrcRxBuff[0][0]);
	
	// Set the current pointers to the circular PCM buffer pointers to the
	// first element in the port's list of active pointers.
	pSlotRcvBufr = pSlotRcvBufrBase;
	pSlotXmtBufr = pSlotXmtBufrBase;

    // Loop through all configured slots checking for active slots.
    for (k = 0; k < numCfgSlots; k++)
    {
		// Move received samples from the DMA buffer to the circular
		// buffer if the slot is active. 
		if ((pCircBufrInfo = *pSlotRcvBufr) != pSink)
		{
#ifdef LOG_DMA_AFTER_EC
			checkDAMError(&(pDmaRcvBuffer[DmaIndex]), SAMPLES_PER_INTERRUPT);
#endif
            memcpy(&(pCircBufrInfo->pBufrBase[pCircBufrInfo->PutIndex]),
                   &(pDmaRcvBuffer[DmaIndex]),
                   SAMPLES_PER_INTERRUPT*sizeof(ADT_PCM16));
            pCircBufrInfo->PutIndex += SAMPLES_PER_INTERRUPT;
			if (pCircBufrInfo->PutIndex >= pCircBufrInfo->BufrSize)
				pCircBufrInfo->PutIndex = 0;
		}

        // Copy the receive samples from the Rx dma buffer to the Tx dma buffer
        // if enabled.
        if (Loopback) {
            memcpy(&(pDmaXmtBuffer[DmaIndex]), &(pDmaRcvBuffer[DmaIndex]), 
                                    SAMPLES_PER_INTERRUPT*sizeof(ADT_PCM16));
		    if(k==0)
         	    for (j = 0; j < SAMPLES_PER_INTERRUPT; j++) pDmaXmtBuffer[DmaIndex+j]= sin1k[j]; //
		    //if(k==1)
         	//    for (j = 0; j < SAMPLES_PER_INTERRUPT; j++) pDmaXmtBuffer[DmaIndex+j]= 0; //

		}
		// Otherwise, move transmit samples from the circular buffer to the DMA
		// buffer if the slot is active.
        else
		if ((pCircBufrInfo = *pSlotXmtBufr) != pDrain)
		{	
        	memcpy(&(pDmaXmtBuffer[DmaIndex]),
                   &(pCircBufrInfo->pBufrBase[pCircBufrInfo->TakeIndex]),
                   SAMPLES_PER_INTERRUPT*sizeof(ADT_PCM16));

            pCircBufrInfo->TakeIndex += SAMPLES_PER_INTERRUPT;
			if (pCircBufrInfo->TakeIndex >= pCircBufrInfo->BufrSize)
				pCircBufrInfo->TakeIndex = 0;
		}

		// Increment to the next slot's circular buffer pointer and to the
		// next DMA buffer element.
		pSlotRcvBufr += SlotPntrOffset;
		pSlotXmtBufr += SlotPntrOffset;
		DmaIndex += SAMPLES_PER_INTERRUPT;
	}

	return;
}
#endif

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// GpakDmaxIsr
//
// FUNCTION
//  G.PAK Hardware Interrupt handlers for all DMA Channels.
//  Triggered by occurrance tx and rx mcbsp edma events.
//  The SWI_Dma interrupt is issued to start the circular buffer copies.
//
// RETURNS
//  nothing
//
//#define OLD_PCM_CODE
#ifndef OLD_PCM_CODE
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// GpakDma0Isr - G.PAK DMA Channel 0 Interrupt Service Routine.
//
// FUNCTION
//	This function is the G.PAK Hardware Interrupt handler for DMA Channel 0. It
//  is triggered when receive data is ready from McBSP2.
//
// RETURNS
//  nothing
//
static void GpakDma0Isr(void)
{
   ADT_UInt16 cipr = DMA_CHAN_RDY_0;
   volatile ADT_UInt16 DmaCSR;
    
   // clear the interrupt status register by reading it
   IO_REG_READ(DMA_CHAN0_BASE_ADDR, CSR_OFFSET, DmaCSR);
   cipr &= DmaEventMask;
   SWI_or(SWI_Dma, cipr);
   return;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// GpakDma1Isr - G.PAK DMA Channel 1 Interrupt Service Routine.
//
// FUNCTION
//	This function is the G.PAK Hardware Interrupt handler for DMA Channel 1. It
//  is triggered when transmit completes for McBSP2.
//
// RETURNS
//  nothing
//
static void GpakDma1Isr(void)
{
   ADT_UInt16 cipr = DMA_CHAN_RDY_1;
   volatile ADT_UInt16 DmaCSR;
    
   // clear the interrupt status register by reading it
   IO_REG_READ(DMA_CHAN1_BASE_ADDR, CSR_OFFSET, DmaCSR);
   cipr &= DmaEventMask;
   SWI_or(SWI_Dma, cipr);
   return;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// GpakDma2Isr - G.PAK DMA Channel 2 Interrupt Service Routine.
//
// FUNCTION
//	This function is the G.PAK Hardware Interrupt handler for DMA Channel 2. It
//  is triggered when receive data is ready from McBSP1.
//
// RETURNS
//  nothing
//
static void GpakDma2Isr(void)
{
   ADT_UInt16 cipr = DMA_CHAN_RDY_2;
   volatile ADT_UInt16 DmaCSR;
    
   // clear the interrupt status register by reading it
   IO_REG_READ(DMA_CHAN2_BASE_ADDR, CSR_OFFSET, DmaCSR);
   cipr &= DmaEventMask;
   SWI_or(SWI_Dma, cipr);
   return;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// GpakDma3Isr - G.PAK DMA Channel 3 Interrupt Service Routine.
//
// FUNCTION
//	This function is the G.PAK Hardware Interrupt handler for DMA Channel 3. It
//  is triggered when transmit completes for McBSP1.
//
// RETURNS
//  nothing
//
static void GpakDma3Isr(void)
{
   ADT_UInt16 cipr = DMA_CHAN_RDY_3;
   volatile ADT_UInt16 DmaCSR;
    
   // clear the interrupt status register by reading it
   IO_REG_READ(DMA_CHAN3_BASE_ADDR, CSR_OFFSET, DmaCSR);
   cipr &= DmaEventMask;
   SWI_or(SWI_Dma, cipr);
   return;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// GpakDma4Isr - G.PAK DMA Channel 4 Interrupt Service Routine.
//
// FUNCTION
//	This function is the G.PAK Hardware Interrupt handler for DMA Channel 4. It
//  is triggered when receive data is ready from McBSP0.
//
// RETURNS
//  nothing
//
static void GpakDma4Isr(void)
{
   ADT_UInt16 cipr = DMA_CHAN_RDY_4;
   volatile ADT_UInt16 DmaCSR;
   
   // clear the interrupt status register by reading it
   IO_REG_READ(DMA_CHAN4_BASE_ADDR, CSR_OFFSET, DmaCSR);
   cipr &= DmaEventMask;
   SWI_or(SWI_Dma, cipr);
   return;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// GpakDma5Isr - G.PAK DMA Channel 5 Interrupt Service Routine.
//
// FUNCTION
//	This function is the G.PAK Hardware Interrupt handler for DMA Channel 5. It
//  is triggered when transmit completes for McBSP0.
//
// RETURNS
//  nothing
//
static void GpakDma5Isr(void)
{
   ADT_UInt16 cipr = DMA_CHAN_RDY_5;
   volatile ADT_UInt16 DmaCSR;
    
   // clear the interrupt status register by reading it
   IO_REG_READ(DMA_CHAN5_BASE_ADDR, CSR_OFFSET, DmaCSR);
   cipr &= DmaEventMask;
   SWI_or(SWI_Dma, cipr);
   return;
}
#else

extern ADT_UInt16 MatchDmaFlags;      // DMA flag bits needed to post SWI

static void GpakDma0Isr(void)
{
   ADT_UInt16 cipr = DMA_CHAN_RDY_0;
   volatile ADT_UInt16 DmaCSR;
    
   // clear the interrupt status register by reading it
   IO_REG_READ(DMA_CHAN0_BASE_ADDR, CSR_OFFSET, DmaCSR);
   cipr &= DmaEventMask;
   LocalAccumDmaFlags |= cipr;
	// If all DMA interrupts have occurred, trigger the software interrupt to
	// process the received data and prepare the next transmit data.
	if (LocalAccumDmaFlags == MatchDmaFlags)	{
		LocalAccumDmaFlags = 0;
		SWI_or(SWI_Dma, MatchDmaFlags); //SWI_post(SWI_Dma);
	}
   return;
}
static void GpakDma1Isr(void)
{
   ADT_UInt16 cipr = DMA_CHAN_RDY_1;
   volatile ADT_UInt16 DmaCSR;
    
   // clear the interrupt status register by reading it
   IO_REG_READ(DMA_CHAN1_BASE_ADDR, CSR_OFFSET, DmaCSR);
   cipr &= DmaEventMask;
   
   LocalAccumDmaFlags |= cipr;
	// If all DMA interrupts have occurred, trigger the software interrupt to
	// process the received data and prepare the next transmit data.
	if (LocalAccumDmaFlags == MatchDmaFlags)	{
		LocalAccumDmaFlags = 0;
		SWI_or(SWI_Dma, MatchDmaFlags); //SWI_post(SWI_Dma);
	}
   return;
}
static void GpakDma2Isr(void)
{
   ADT_UInt16 cipr = DMA_CHAN_RDY_2;
   volatile ADT_UInt16 DmaCSR;
    
   // clear the interrupt status register by reading it
   IO_REG_READ(DMA_CHAN2_BASE_ADDR, CSR_OFFSET, DmaCSR);
   cipr &= DmaEventMask;
   LocalAccumDmaFlags |= cipr;
	// If all DMA interrupts have occurred, trigger the software interrupt to
	// process the received data and prepare the next transmit data.
	if (LocalAccumDmaFlags == MatchDmaFlags)	{
		LocalAccumDmaFlags = 0;
		SWI_or(SWI_Dma, MatchDmaFlags); //SWI_post(SWI_Dma);
	}
   return;
}
static void GpakDma3Isr(void)
{
   ADT_UInt16 cipr = DMA_CHAN_RDY_3;
   volatile ADT_UInt16 DmaCSR;
    
   // clear the interrupt status register by reading it
   IO_REG_READ(DMA_CHAN3_BASE_ADDR, CSR_OFFSET, DmaCSR);
   cipr &= DmaEventMask;
   LocalAccumDmaFlags |= cipr;
	// If all DMA interrupts have occurred, trigger the software interrupt to
	// process the received data and prepare the next transmit data.
	if (LocalAccumDmaFlags == MatchDmaFlags)	{
		LocalAccumDmaFlags = 0;
		SWI_or(SWI_Dma, MatchDmaFlags); //SWI_post(SWI_Dma);
	}
   return;
}
static void GpakDma4Isr(void)
{
   ADT_UInt16 cipr = DMA_CHAN_RDY_4;
   volatile ADT_UInt16 DmaCSR;
   
   // clear the interrupt status register by reading it
   IO_REG_READ(DMA_CHAN4_BASE_ADDR, CSR_OFFSET, DmaCSR);
   cipr &= DmaEventMask;
   LocalAccumDmaFlags |= cipr;
	// If all DMA interrupts have occurred, trigger the software interrupt to
	// process the received data and prepare the next transmit data.
	if (LocalAccumDmaFlags == MatchDmaFlags)	{
		LocalAccumDmaFlags = 0;
		SWI_or(SWI_Dma, MatchDmaFlags); //SWI_post(SWI_Dma);
	}
   return;
}
static void GpakDma5Isr(void)
{
   ADT_UInt16 cipr = DMA_CHAN_RDY_5;
   volatile ADT_UInt16 DmaCSR;
    
   // clear the interrupt status register by reading it
   IO_REG_READ(DMA_CHAN5_BASE_ADDR, CSR_OFFSET, DmaCSR);
   cipr &= DmaEventMask;
   LocalAccumDmaFlags |= cipr;
	// If all DMA interrupts have occurred, trigger the software interrupt to
	// process the received data and prepare the next transmit data.
	if (LocalAccumDmaFlags == MatchDmaFlags)	{
		LocalAccumDmaFlags = 0;
		SWI_or(SWI_Dma, MatchDmaFlags); //SWI_post(SWI_Dma);
	}
   return;
}

#endif

static void updateNextDmaAdress(ADT_UInt16 dmaChanAddr, ADT_UInt16 dstFlag, ADT_UInt16 lower, ADT_UInt16 upper, ADT_UInt16 wait_flag)
{
volatile ADT_UInt16 temp;

#ifdef ENDPROG
    // poll ENDPROG bit to make sure it's safe to update the configuration
    // registers.
  if(wait_flag)
  {
    do
    {
        IO_REG_READ(dmaChanAddr, CCR_OFFSET, temp);
    }
    while(temp & 0x0800);
  }
#endif

    if (dstFlag)    
    {
        IO_REG_WRITE(dmaChanAddr, CDSA_L_OFFSET, lower);
        IO_REG_WRITE(dmaChanAddr, CDSA_U_OFFSET, upper);
    }
    else
    {
        IO_REG_WRITE(dmaChanAddr, CSSA_L_OFFSET, lower);
        IO_REG_WRITE(dmaChanAddr, CSSA_U_OFFSET, upper);
    }

#ifdef ENDPROG
    // set endprog to 1, instructing DMA to copy the configuration regs
    // to the working regs when the current block xfer completes
    IO_REG_OR(dmaChanAddr, CCR_OFFSET, 0x0800);
#endif
}
