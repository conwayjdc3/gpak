extern void _c_in00;

asm(" .sect \".core0init\"");
asm("      .global _c_int00");

#pragma CODE_SECTION(core0init,".core0init")
extern void interrupt core0init () {
   asm("      mvkl _c_int00, B0");
   asm("      mvkh _c_int00, B0");
   asm("      b    B0");
   asm("      nop  5");
}

asm(" .sect \".core1init\"");

#pragma CODE_SECTION(core1init,".core1init")
extern void interrupt core1init () {
   asm("      mvkl _c_int00, B0");
   asm("      mvkh _c_int00, B0");
   asm("      b    B0");
   asm("      nop  5");
}

asm(" .sect \".core2init\"");

#pragma CODE_SECTION(core2init,".core2init")
extern void interrupt core2init () {
   asm("      mvkl _c_int00, B0");
   asm("      mvkh _c_int00, B0");
   asm("      b    B0");
   asm("      nop  5");
}

asm(" .sect \".core3init\"");

#pragma CODE_SECTION(core3init,".core3init")
extern void interrupt core3init () {
   asm("      mvkl _c_int00, B0");
   asm("      mvkh _c_int00, B0");
   asm("      b    B0");
   asm("      nop  5");
}

asm(" .sect \".core4init\"");

#pragma CODE_SECTION(core4init,".core4init")
extern void interrupt core4init () {
   asm("      mvkl _c_int00, B0");
   asm("      mvkh _c_int00, B0");
   asm("      b    B0");
   asm("      nop  5");
}

asm(" .sect \".core5init\"");

#pragma CODE_SECTION(core5init,".core5init")
extern void interrupt core5init () {
   asm("      mvkl _c_int00, B0");
   asm("      mvkh _c_int00, B0");
   asm("      b    B0");
   asm("      nop  5");
}
