//{ TrilogyInit.c  (C6748)
//
// G.PAK initialization for Trilogy 
//
//---------------------------------------------------------------
//  Custom stubs.
//
//  customDSPInit      Customized configuration before GPAK initialization
//  customDSPStartUp   Customized configuration after GPAK initialization
//  customEncodeInit   Initialization of custom encoder
//  customDecodeInit   Initialization of custom decoder
//  customEncode       Converts pcm data to payload data.
//  customDecode       Converts payload data to pcm data
//  customMsg          Processes a custom G.PAK message
//  customIdle         User hook for idle processing
//}
#define _CSL_H_         // Avoid csl definitions
#include <stdio.h>
#include <std.h>
#include <stdio.h>
#include <stdint.h>

#include <string.h>
#include <soc.h>

#include "adt_typedef.h"
#include "GpakDefs.h"
#include "GpakExts.h"
#include "GpakErrs.h"
#include "sysconfig.h"
#include "GpakHpi.h"

int bsp_clk_div = 77;  // 200 MHz/100 == 2 MHz TDM clock
extern int mcbsp_loopback;

// -----------------------------------------------------------------------------
// McBSP Port Configuration 
// -----------------------------------------------------------------------------
typedef struct GpakPortConfig_t {

	GpakSlotCfg_t SlotsSelect1;         /* port 1 Slot selection */
	GpakActivation Port1Enable;
	ADT_Int32 Port1SlotMask0_31;
	ADT_Int32 Port1SlotMask32_63;
	ADT_Int32 Port1SlotMask64_95;
	ADT_Int32 Port1SlotMask96_127;

	GpakSlotCfg_t SlotsSelect2;         /* port 2 Slot selection */
	GpakActivation Port2Enable;
	ADT_Int32 Port2SlotMask0_31;
	ADT_Int32 Port2SlotMask32_63;
	ADT_Int32 Port2SlotMask64_95;
	ADT_Int32 Port2SlotMask96_127;

	GpakSlotCfg_t SlotsSelect3;         /* port 3 Slot selection */
	GpakActivation Port3Enable;
	ADT_Int32 Port3SlotMask0_31;
	ADT_Int32 Port3SlotMask32_63;
	ADT_Int32 Port3SlotMask64_95;
	ADT_Int32 Port3SlotMask96_127;

	ADT_Int16 TxDataDelay1, TxDataDelay2, TxDataDelay3;
	ADT_Int16 RxDataDelay1, RxDataDelay2, RxDataDelay3;
	ADT_Int16 ClkDiv1,      ClkDiv2,      ClkDiv3;
	ADT_Int16 FramePeriod1, FramePeriod2, FramePeriod3;
	ADT_Int16 FrameWidth1,  FrameWidth2,  FrameWidth3;
	GpakCompandModes Compand1,     Compand2,  Compand3;
	GpakActivation   SampRateGen1, SampRateGen2, SampRateGen3;

	GpakActivation AudioPort1Enable;
	ADT_Int32 AudioPort1TxSerMask;
	ADT_Int32 AudioPort1RxSerMask;
	ADT_Int32 AudioPort1SlotMask;

	GpakActivation AudioPort2Enable;
	ADT_Int32 AudioPort2TxSerMask;
	ADT_Int32 AudioPort2RxSerMask;
	ADT_Int32 AudioPort2SlotMask;
} GpakPortConfig_t;



ADT_UInt32 gpakFormatPortConfig (ADT_UInt16 *Msg, GpakPortConfig_t *PortCfg) {

    /* Build the Configure Serial Ports message. */
    Msg[0] = MSG_CONFIGURE_PORTS << 8;
    Msg[1] = 0;
    if (PortCfg->Port1Enable)        Msg[1] = 0x0001;
    if (PortCfg->Port2Enable)        Msg[1] |= 0x0002;
    if (PortCfg->Port3Enable)        Msg[1] |= 0x0004;
    if (PortCfg->AudioPort1Enable)   Msg[1] |= 0x0008;
    if (PortCfg->AudioPort2Enable)   Msg[1] |= 0x0010;

    Msg[2]  = (ADT_UInt16)((PortCfg->Port1SlotMask0_31  ) >> 16);  
    Msg[3]  = (ADT_UInt16)((PortCfg->Port1SlotMask0_31  ) & 0xffff);  
    Msg[4]  = (ADT_UInt16)((PortCfg->Port1SlotMask32_63 ) >> 16);  
    Msg[5]  = (ADT_UInt16)((PortCfg->Port1SlotMask32_63 ) & 0xffff);
    Msg[6]  = (ADT_UInt16)((PortCfg->Port1SlotMask64_95 ) >> 16);  
    Msg[7]  = (ADT_UInt16)((PortCfg->Port1SlotMask64_95 ) & 0xffff);
    Msg[8]  = (ADT_UInt16)((PortCfg->Port1SlotMask96_127) >> 16);  
    Msg[9]  = (ADT_UInt16)((PortCfg->Port1SlotMask96_127) & 0xffff);

    Msg[10] = (ADT_UInt16)((PortCfg->Port2SlotMask0_31  ) >> 16);
    Msg[11] = (ADT_UInt16)((PortCfg->Port2SlotMask0_31  ) & 0xffff);
    Msg[12] = (ADT_UInt16)((PortCfg->Port2SlotMask32_63 ) >> 16);
    Msg[13] = (ADT_UInt16)((PortCfg->Port2SlotMask32_63 ) & 0xffff);
    Msg[14] = (ADT_UInt16)((PortCfg->Port2SlotMask64_95 ) >> 16);
    Msg[15] = (ADT_UInt16)((PortCfg->Port2SlotMask64_95 ) & 0xffff);
    Msg[16] = (ADT_UInt16)((PortCfg->Port2SlotMask96_127) >> 16);
    Msg[17] = (ADT_UInt16)((PortCfg->Port2SlotMask96_127) & 0xffff);

    Msg[18] = (ADT_UInt16)((PortCfg->Port3SlotMask0_31  ) >> 16);
    Msg[19] = (ADT_UInt16)((PortCfg->Port3SlotMask0_31  ) & 0xffff);
    Msg[20] = (ADT_UInt16)((PortCfg->Port3SlotMask32_63 ) >> 16);
    Msg[21] = (ADT_UInt16)((PortCfg->Port3SlotMask32_63 ) & 0xffff);
    Msg[22] = (ADT_UInt16)((PortCfg->Port3SlotMask64_95 ) >> 16);
    Msg[23] = (ADT_UInt16)((PortCfg->Port3SlotMask64_95 ) & 0xffff);
    Msg[24] = (ADT_UInt16)((PortCfg->Port3SlotMask96_127) >> 16);
    Msg[25] = (ADT_UInt16)((PortCfg->Port3SlotMask96_127) & 0xffff);

    Msg[26] = (ADT_UInt16)((PortCfg->AudioPort1TxSerMask) >> 16);
    Msg[27] = (ADT_UInt16)((PortCfg->AudioPort1TxSerMask) & 0xffff);
    Msg[28] = (ADT_UInt16)((PortCfg->AudioPort1RxSerMask) >> 16);
    Msg[29] = (ADT_UInt16)((PortCfg->AudioPort1RxSerMask) & 0xffff);
    Msg[30] = (ADT_UInt16)((PortCfg->AudioPort1SlotMask ) >> 16);
    Msg[31] = (ADT_UInt16)((PortCfg->AudioPort1SlotMask ) & 0xffff);

    Msg[32] = (ADT_UInt16)((PortCfg->AudioPort2TxSerMask) >> 16);
    Msg[33] = (ADT_UInt16)((PortCfg->AudioPort2TxSerMask) & 0xffff);
    Msg[34] = (ADT_UInt16)((PortCfg->AudioPort2RxSerMask) >> 16);
    Msg[35] = (ADT_UInt16)((PortCfg->AudioPort2RxSerMask) & 0xffff);
    Msg[36] = (ADT_UInt16)((PortCfg->AudioPort2SlotMask ) >> 16);
    Msg[37] = (ADT_UInt16)((PortCfg->AudioPort2SlotMask ) & 0xffff);

    Msg[38] = 0; //PackBytes (PortCfg->ClkDiv2,     PortCfg->ClkDiv1);
    Msg[39] = 0; //PackBytes (PortCfg->FrameWidth1, PortCfg->ClkDiv3);
    Msg[40] = 0; //PackBytes (PortCfg->FrameWidth3, PortCfg->FrameWidth2);

    Msg[41] = (ADT_UInt16)(((PortCfg->FramePeriod1 << 4) & 0xfff0) |
                         ((PortCfg->Compand1 << 1) & 0xE) | (PortCfg->SampRateGen1 & 1) );
    Msg[42] = (ADT_UInt16)(((PortCfg->FramePeriod2 << 4) & 0xfff0) |
                         ((PortCfg->Compand2 << 1) & 0xE) | (PortCfg->SampRateGen2 & 1) );

    Msg[43] = (ADT_UInt16)(((PortCfg->FramePeriod3 << 4) & 0xfff0) |
                         ((PortCfg->Compand3 << 1) & 0xE) | (PortCfg->SampRateGen3 & 1) );

    Msg[44] = (ADT_UInt16 )( (PortCfg->TxDataDelay1 & 0x0003)       | ((PortCfg->RxDataDelay1 << 2) & 0x000c) |
                          ((PortCfg->TxDataDelay2 << 4) & 0x0030) | ((PortCfg->RxDataDelay2 << 6) & 0x00c0) |
                          ((PortCfg->TxDataDelay3 << 8) & 0x0300) | ((PortCfg->RxDataDelay3 << 10) & 0x0c00) );
    return 45;
}


void custom_start_port () { 
GpakPortConfig_t cfg;
ADT_UInt16 msg_buff[50];
ADT_UInt16 reply_buff[50];

    memset(&cfg, 0, sizeof(GpakPortConfig_t));
    cfg.SlotsSelect1 = SlotCfgAllActive;
    cfg.Port1Enable = Enabled;
    cfg.Port1SlotMask0_31   = 0xFFFFFFFF;
    cfg.TxDataDelay1 = 1;    
    cfg.Compand1 = cmpNone8;
    cfg.Compand2 = cmpNone8;
    cfg.Compand3 = cmpNone8;

    gpakFormatPortConfig (msg_buff, &cfg);
    ProcConfigSerialPortsMsg (msg_buff, reply_buff);

}

// configures Mcbsp for 8148EVM
void custom_start_port_evm () { 
GpakPortConfig_t cfg;
ADT_UInt16 msg_buff[50];
ADT_UInt16 reply_buff[50];
    memset(&cfg, 0, sizeof(cfg));
    cfg.SlotsSelect1        = SlotCfgAllActive;
    cfg.Port1Enable         = Enabled;
    cfg.Port1SlotMask0_31   = 0xFFFFFFFF;
    cfg.Port1SlotMask32_63  = 0;
    cfg.Port1SlotMask64_95  = 0;
    cfg.Port1SlotMask96_127 = 0;

    cfg.TxDataDelay1        = 0;

    cfg.RxDataDelay1        = 0;

    cfg.ClkDiv1             = bsp_clk_div;

    cfg.FramePeriod1        = (32*8) - 1; // number of clock cycles per frame (32 slots, 8-bits/slot)

    cfg.FrameWidth1         = 1 - 1; // framesync pulse width is 1 clock-period wide

    cfg.Compand1            = cmpNone8;

    cfg.SampRateGen1        = Enabled; 

//SS    cfg.txFrameSyncPolarity1 = fsActHigh;

//SS    cfg.rxFrameSyncPolarity1 = fsActHigh;

//SS    cfg.txClockPolarity1 = clkRising; // tx data driven on rising edge of Tx clock

//SS    cfg.rxClockPolarity1 = clkFalling; // rx data sampled on falling edge of Rx clock

    gpakFormatPortConfig (msg_buff, &cfg);

    mcbsp_loopback = 1;

    ProcConfigSerialPortsMsg (msg_buff, reply_buff);

}

void custom_stop_port () { 
GpakPortConfig_t cfg;
ADT_UInt16 msg_buff[50];
ADT_UInt16 reply_buff[50];


    cfg.SlotsSelect1        = SlotCfgNone;
    cfg.Port1Enable         = Disabled;
    cfg.Port1SlotMask0_31   = 0;
    cfg.Port1SlotMask32_63  = 0;
    cfg.Port1SlotMask64_95  = 0;
    cfg.Port1SlotMask96_127 = 0;

    gpakFormatPortConfig (msg_buff, &cfg);
    ProcConfigSerialPortsMsg (msg_buff, reply_buff);

}
