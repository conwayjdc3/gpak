/*
 * Copyright (c) 2007, Adaptive Digital Technologies, Inc.
 *
 * File Name: omap3530McBSP.c
 *
 * Description:
 *   This file contains G.PAK's omap3530 McBSP drivers.
 *
 * Version: 1.0
 *
 * Revision History:
 *   3/2/2010 - Initial release.
 *
 * 
 *      
 */
// Important: This driver is coded to use the second McBSP: (i.e., McBsp1 (0-based))
// It also expects the G.pak logical port to equal the physical port == 1 (0-based)
//==============================================
//
//  Build-time configuration values
//
//  sysConfig.dxDelay[port] - Sets channels as 'tri-state' or driven high
//  sysConfig.rxClockPolarity[port]      - Sets signal polarity and source
//  sysConfig.txClockPolarity[port]      - Sets signal polarity and source
//  sysConfig.rxFrameSyncPolarity[port]  - Sets signal polarity and source
//  sysConfig.txFrameSyncPolarity[port]  - Sets signal polarity and source
//

//=====================================================
//  Run-time configuration
//
//  Word     Bits
//Msg[1]     0x0001    = McBSP0 Enable
//Msg[1]     0x0002    = McBSP1 Enable
//Msg[1]     0x0004    = McBSP2 Enable
//Msg[1]     0x0008    = McASP0 Enable
//Msg[1]     0x0010    = McASP1 Enable
//Msg[2-9]   0xffff    = McBSP0 Slot Mask
//Msg[10-17] 0xffff    = McBSP1 Slot Mask
//Msg[18-25] 0xffff    = McBSP2 Slot Mask
//Msg[26-27] 0xffff    = McASP0 Tx Pins
//Msg[28-29] 0xffff    = McASP0 Rx Pins
//Msg[30-31] 0xffff    = McASP0 Slot Mask
//Msg[32-33] 0xffff    = McASP1 Tx Pins
//Msg[34-35] 0xffff    = McASP1 Rx Pins
//Msg[36-37] 0xffff    = McASP2 Slot Mask
//Msg[38]    0x00ff    = CLKDIV0      - Sets generated clock divisor
//Msg[38]    0xff00    = CLKDIV1      - Sets generated clock divisor
//Msg[39]    0x00ff    = CLKDIV2      - Sets generated clock divisor
//Msg[39]    0xff00    = FWID0        - Sets generated frame sync pulse width
//Msg[40]    0x00ff    = FWID1        - Sets generated frame sync pulse width
//Msg[40]    0xff00    = FWID2        - Sets generated frame sync pulse width
//Msg[41]    0xfff0    = FPER0        - Sets generated frame width
//Msg[41]    0x000E    = COMPAND0     - Sets compandingMode and wordsize
//Msg[41]    0x0001    = GRST0        - Enables generated signals
//Msg[42]    0xfff0    = FPER1        - Sets generated frame width
//Msg[42]    0x000E    = COMPAND1     - Sets compandingMode and wordsize
//Msg[42]    0x0001    = GRST0        - Enables generated signals
//Msg[43]    0xfff0    = FPER2        - Sets generated frame width
//Msg[43]    0x000E    = COMPAND2     - Sets compandingMode and wordsize
//Msg[43]    0x0001    = GRST0        - Enables generated signals
//Msg[44]    0x0003    = txDataDelay0 - Sets bit delay from frame sync
//Msg[44]    0x000c    = rxDataDelay0 - Sets bit delay from frame sync
//Msg[44]    0x0030    = txDataDelay1 - Sets bit delay from frame sync
//Msg[44]    0x00c0    = rxDataDelay1 - Sets bit delay from frame sync
//Msg[44]    0x0300    = txDataDelay2 - Sets bit delay from frame sync
//Msg[44]    0x0c00    = rxDataDelay2 - Sets bit delay from frame sync
//
//
//  sysConfig.rxDataDelay[port] = rxDataDelayx      - Delay between FS and first Rx bit
//  sysConfig.txDataDelay[port] = txDataDelayx      - Delay between FS and first Tx bit
//  sysConfig.compandingMode[port] = COMPANDx       - u-Law, A-law, none, ...
//  sysConfig.serialWordSize[port] (from COMPANDx   - bits per sample
//
//  genClockEnable[port] = GRSTx                    - Sample clock generates internal signals
//  ClkDiv[port]         = CLKDIVx                  - System clock to bit clock divisor
//  PulseWidth[port]     = FWIDx                    - Frame sync pulse width (bits)
//  FrameWidth[port]     = FPERx                    - Frame width (bits)


// Application related header files.
#include <std.h>
#include <hwi.h>
#include <clk.h>
#include <string.h>

#include "GpakDefs.h"
#include "GpakHpi.h"
#include "GpakExts.h"
#include "GpakPcm.h"
#include "64plus.h"

ADT_Bool startMcBSP (int port);
ADT_Bool stopMcBSP  (int port);
ADT_Bool setupMcBSP (int port );

TDM_Port McBSPPort = {
   startMcBSP, stopMcBSP, setupMcBSP
};

// McBSP Register offsets (32-bits per unit)
#define DRR2_OFF	    0x00000000 // R
#define DRR1_OFF	    0x00000004 // R 
#define DXR2_OFF	    0x00000008 // W
#define DXR1_OFF	    0x0000000C // W
#define DRR_OFF         0x00000000 // R  
#define DXR_OFF         0x00000008 // W  
#define SPCR2_OFF       0x00000010 // RW 
#define SPCR1_OFF       0x00000014 // RW 
#define RCR2_OFF        0x00000018 // RW 
#define RCR1_OFF        0x0000001C // RW 
#define XCR2_OFF        0x00000020 // RW 
#define XCR1_OFF        0x00000024 // RW 
#define SRGR2_OFF       0x00000028 // RW 
#define SRGR1_OFF       0x0000002C // RW 
#define MCR2_OFF        0x00000030 // RW 
#define MCR1_OFF        0x00000034 // RW 
#define RCERA_OFF       0x00000038 // RW 
#define RCERB_OFF       0x0000003C // RW 
#define XCERA_OFF       0x00000040 // RW 
#define XCERB_OFF       0x00000044 // RW 
#define PCR_OFF         0x00000048 // RW 
#define RCERC_OFF       0x0000004C // RW 
#define RCERD_OFF       0x00000050 // RW 
#define XCERC_OFF       0x00000054 // RW 
#define XCERD_OFF       0x00000058 // RW 
#define RCERE_OFF       0x0000005C // RW 
#define RCERF_OFF       0x00000060 // RW 
#define XCERE_OFF       0x00000064 // RW 
#define XCERF_OFF       0x00000068 // RW 
#define RCERG_OFF       0x0000006C // RW 
#define RCERH_OFF       0x00000070 // RW 
#define XCERG_OFF       0x00000074 // RW 
#define XCERH_OFF       0x00000078 // RW 
#define RINTCLR_OFF     0x00000080 // RW 
#define XINTCLR_OFF     0x00000084 // RW 
#define ROVFLCLR_OFF    0x00000088 // RW 
#define SYSCONFIG_OFF   0x0000008C // RW 
#define THRSH2_OFF      0x00000090 // RW 
#define THRSH1_OFF      0x00000094 // RW 
#define IRQSTATUS_OFF   0x000000A0 // RW 
#define IRQENABLE_OFF   0x000000A4 // RW 
#define WAKEUPEN_OFF    0x000000A8 // RW 
#define XCCR_OFF        0x000000AC // RW 
#define RCCR_OFF        0x000000B0 // RW 
#define XBUFFSTAT_OFF   0x000000B4 // R  
#define RBUFFSTAT_OFF   0x000000B8 // R  
#define SSELCR_OFF      0x000000BC // RW 
#define STATUS_OFF      0x000000C0 // R  


//  SPCR2 masks
#define XRST    0x00000001   // Tx reset
#define XRDY    0x00000002
#define XEMPTY  0x00000004
#define XSYNERR 0x00000008
#define GRST    0x00000040   // Clock generation reset
#define FRST    0x00000080   // Frame sync reset
#define SOFT    0x00000100
#define FREE    0x00000200


//  SPCR1 masks
#define RRST    0x00000001   // Rx reset
#define RRDY    0x00000002   // Rcv Ready
#define RFULL   0x00000004   // Rcv Ready
#define RSYNERR 0x00000008   // rx sync mode
#define DXENA   0x00000080   // Dx delay enable
#define RJZF    0x00000000   // Right justify zero fill
#define RJSE    0x00002000   // Right justify sign extend
#define LJZF    0x00004000   // Left justify zero fill
#define ALB     0x00008000   // Analog Loopback 

// RCR2, RCR1, XCR2, XCR1 masks
#define DLY0   0x00000000
#define DLY1   0x00000001
#define DLY2   0x00000002
#define WD8    0x00000000  // 8-bit wordlen
#define WD16   0x00000040  // 16-bit wordlen
#define WD32   0x000000A0  // 32-bit wordlen
#define PHASE  0x00008000

//  PCR masks
#define CLKRP 0x0001   // Rx clk polarity
#define CLKXP 0x0002   // Tx clk polarity
#define FSRP  0x0004   // Rx frame sync polarity
#define FSXP  0x0008   // Tx frame sync polarity
#define CLKRM 0x0100   // Rx clk source
#define CLKXM 0x0200   // Tx clk source
#define FSRM  0x0400   // Rx frame sync source
#define FSXM  0x0800   // Tx frame sync source
#define RIOEN 0x1000   // IO enable
#define XIOEN 0x2000   // IO enable

// FSGR masks
#define FSGM  0x1000   // Frame sync generated by frame boundary
#define CLKSM 0x2000   // Sample generator clock source is CPU

// IRQStatus, IRQ enable, WakeupEnable masks
#define IRQ_RSYNCERR     0x00000001
#define IRQ_RFSR         0x00000002
#define IRQ_REOF         0x00000004
#define IRQ_RRDY         0x00000008
#define IRQ_RUNDFLSTAT   0x00000010
#define IRQ_ROVFLSTAT    0x00000020
#define IRQ_XSYNCERR     0x00000080
#define IRQ_XFSX         0x00000100
#define IRQ_XEOF         0x00000200
#define IRQ_XRDY         0x00000400
#define IRQ_XUNDFLSTAT   0x00000800
#define IRQ_XOVFLSTAT    0x00001000
#define IRQ_XEMPTYEOF    0x00004000

// XCCR masks
#define XDISABLE     0x00000001  // Transmit disable
#define XDMAEN       0x00000008  // Transmit Dma enable
#define DLB          0x00000020  // Digital Loopback
#define XFULLCYC     0x00000800  // Transmit full cycle mode
#define DXDLY18ns    0x00000000  // 18 ns dx delay
#define DXDLY26ns    0x00001000  // 26 ns dx delay
#define DXDLY35ns    0x00002000  // 35 ns dx delay
#define DXDLY42ns    0x00003000  // 42 ns dx delay

// RCCR masks
#define RDISABLE     0x00000001  // Receive disable
#define RDMAEN       0x00000008  // Receive Dma enable
#define RFULLCYC     0x00000800  // Receive full cycle mode

// SYSCONFIG masks
#define CLKACT_ALL_ON 0x30
#define SIDLE_NEVER   0x08


#define  CONTROL_DEVCONF0_ADDR      0x48002274        
#define  CLKS_EXT                   0x00000040   // TODO: find out if extern clock is correct
#define  PER_CM_FCLKEN_PER_ADDR     0x48005000 
#define  PER_CM_ICLKEN_PER_ADDR     0x48005010
#define  CONTROL_PADCONF_MCBSP2_FSX 0x4800213C
#define  CONTROL_PADCONF_MCBSP2_DR  0x48002140
#define  PAD_INPUT_MASK             0x01000100
#define  DX_DR_DIRECTION            0x00000100


#define  WUGEN_IRQ34                4
#define  WUGEN_SYSCONFIG_ADDR       0x01C21008
#define  WUGEN_MEVT1_ADDR           0x01C21064
#define  WUGEN_MEVTCLR1_ADDR        0x01C21074
#define  WUGEN_MEVTSET1_ADDR        0x01C21084

      // McBSP-2 specific:
#define  BSP2_FCLK_EN               1
#define  BSP2_ICLK_EN               1

// McBSP Addressing
#define MCBSP_READ(addr, reg)         (*(volatile ADT_UInt32 *) (addr+reg))
#define MCBSP_WRITE(addr, reg, value) {*((volatile ADT_UInt32 *) (addr+reg)) = (ADT_UInt32) value;}
#define MCBSP_AND(addr, reg, value)   {*((volatile ADT_UInt32 *) (addr+reg)) &= (ADT_UInt32) value;}
#define MCBSP_OR(addr, reg, value)    {*((volatile ADT_UInt32 *) (addr+reg)) |= (ADT_UInt32) value;}

int dxrPrime = 32;
#define  MCBSP2_TXB_LEN             (1024 + 256)

// Serial port (stream) configuration related variables.
typedef struct {
   GpakActivation Enabled;
   
   // Clock/Frame sync register values
   ADT_UInt32 McSPCR2;   //  Serial Port Control 2
   ADT_UInt32 McSPCR1;   //  Serial Port Control 1
   ADT_UInt32 McPCR;     //  Pin Control
   ADT_UInt32 McRCR2;    //  Rcv Control 2
   ADT_UInt32 McRCR1;    //  Rcv Control 1
   ADT_UInt32 McXCR2;    //  Xmt Control 2
   ADT_UInt32 McXCR1;    //  Xmt Control 1
   ADT_UInt32 McMCR2;    //  Xmt MultiChannel Control 2
   ADT_UInt32 McMCR1;    //  Xmt MultiChannel Control 1
   ADT_UInt32 McRCER[8]; //  Bit masks identifying which of the 128 channels are received (A...H)
   ADT_UInt32 McXCER[8]; //  Bit masks identifying which of the 128 channels are transmitted (A...H)
   ADT_UInt32 McSRGR2;   //  sample rate generator 2
   ADT_UInt32 McSRGR1;   //  sample rate generator 1
   ADT_UInt32 McTHRSH2;  //  buffer threshold 1
   ADT_UInt32 McTHRSH1;  //  buffer threshold 2
   ADT_UInt32 McIRQENAB; //  IRQ enable
   ADT_UInt32 McSYSCONFIG; // System Configuration register
   ADT_UInt32 McXCCR;    // Transmit Configuration control register
   ADT_UInt32 McRCCR;    // Receive Configuration control register
} McBspInfo;

 McBspInfo McBsp[NUM_TDM_PORTS] = { 
  { Disabled }, 
  { Disabled },
  { Disabled }
};

ADT_UInt32 BaseAddr[NUM_TDM_PORTS] = { 0, 0, 0};
ADT_UInt16 McBSP_Configured[NUM_TDM_PORTS] = { FALSE, FALSE, FALSE };
ADT_Events RxEvents [NUM_TDM_PORTS] = { 0, 0, 0 };
ADT_Events TxEvents [NUM_TDM_PORTS] = { 0, 0, 0 };

static ADT_PCM16 * const RxDMABuffers[NUM_TDM_PORTS] = {
   BSP0DMA_RxBuffer, BSP1DMA_RxBuffer, BSP2DMA_RxBuffer
};

static ADT_PCM16 * const TxDMABuffers[NUM_TDM_PORTS] = {
   BSP0DMA_TxBuffer, BSP1DMA_TxBuffer, BSP2DMA_TxBuffer
};

//  Stored McBSP configuration message values
ADT_UInt16 ClkDiv[NUM_TDM_PORTS]     = { 0, 0, 0 }; // Clock divisor (0-255)
ADT_UInt16 PulseWidth[NUM_TDM_PORTS] = { 0, 0, 0 }; // Width (0-255 bits) of frame sync pulse
ADT_UInt16 FrameWidth[NUM_TDM_PORTS] = { 0, 0, 0 }; // Width (0-4095 bits) of frame
GpakActivation genClockEnable[NUM_TDM_PORTS] = { Disabled, Disabled, Disabled };
int samplesPerHwIsr[NUM_TDM_PORTS] = {0, 0, 0};
extern ADT_UInt16 MaxDmaSlots [NUM_TDM_PORTS];  // Max slots available for DMA [fixed at build]
extern volatile ADT_UInt16 MatchDmaFlags;    // DMA flag bits needed to post SWI

void ConfigureMcBSP (ADT_UInt32 McBspEnable[3], ADT_UInt32 McBspSlotMask[3][4]);
static ADT_UInt16 determineLastSlot (ADT_UInt32 Mask, ADT_UInt16 offset);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// validCompanding
//
// FUNCTION
//    Verification of companding mode parameter
//
// PARAMETERS
//                                                                                                       
//     mode - Companding mode
//
// RETURNS
//    TRUE - companding mode supported
//
static ADT_Bool validCompanding(GpakCompandModes mode) {
   return (cmpPCMU <= mode && mode <= cmpNone8);
}
      
static void bitDelay (int count, int port) { 
   int bitsPerFrame;
   unsigned long ticksPerFrame, startTime, delayTime;
   
   startTime = CLK_gethtime ();

   ticksPerFrame = CLK_countspms () / 8;
   bitsPerFrame = 8 * sysConfig.numSlotsOnStream[port];
   delayTime = ((ticksPerFrame / bitsPerFrame) + 1) * count;

   while (CLK_gethtime () - startTime < delayTime);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// PowerOnMcBSPs
//
// FUNCTION
//  
//     Power on McBSPs and setup p
//
// PARAMETERS
//
//     None
//

static void PowerOnMcBSPs () {
      REG_OR (PER_CM_FCLKEN_PER_ADDR, BSP2_FCLK_EN);
      REG_OR (PER_CM_ICLKEN_PER_ADDR, BSP2_ICLK_EN);
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// storeMcBSPConfiguration
//
// FUNCTION
//  
//     Stores configured values for McBSP registers.  
//     Called at start up and after configuration request verified
//
// PARAMETERS
//
//     port - McBSP port index
//
void storeMcBSPConfiguration (int port) {
   McBspInfo *Bsp;
   ADT_Word wordSize, pcr, bitsPerSample, cmpdMode;

   // Configuration dependent serial I/O parameters.
   if (NUM_TDM_PORTS <= port) return;
   Bsp = &McBsp[port];

   //  -- SPCR2 --
   Bsp->McSPCR2 = 0;

   //  -- SPCR1 --
   Bsp->McSPCR1 = RJSE;
   if (sysConfig.dxDelay[port] != 0) Bsp->McSPCR1 |= DXENA;

   //-- RCR and XCR --
   wordSize = WD8;        // 8-bit
   bitsPerSample = 8;
   cmpdMode = sysConfig.compandingMode[port];
   if (cmpdMode == cmpNone16) {
      wordSize = WD16;     // 16-bit
      bitsPerSample = 16;
   }
   sysConfig.serialWordSize[port] = bitsPerSample;

   // ORIG (single phase)
   Bsp->McRCR2 = sysConfig.rxDataDelay[port] & 0x0003;
   Bsp->McRCR1 = ((SltsPerFrame[port] - 1) << 8) | wordSize;

   Bsp->McXCR2 = sysConfig.txDataDelay[port] & 0x0003;
   Bsp->McXCR1 = ((SltsPerFrame[port] - 1) << 8) | wordSize;

   //  -- PCR --
   if (SltsPerFrame[port] == 0) {
      pcr = (RIOEN | XIOEN);     // Configure for GPIO (legacy)
   } else {
      pcr = 0;

      // Signal polarities
      if (sysConfig.rxClockPolarity[port] & 1)      pcr |= CLKRP; // 1 == rx data sampled on rising edge of CLKR
      if (sysConfig.txClockPolarity[port] & 1)      pcr |= CLKXP; // 1 == tx data driven on falling edge of CLKX
      if (sysConfig.rxFrameSyncPolarity[port] & 1)  pcr |= FSRP;  // 1 == rx framesync is active low
      if (sysConfig.txFrameSyncPolarity[port] & 1)  pcr |= FSXP;  // 1 == tx framesync is active low

      // Signal sources 2 = internal(1)/external(0)
      if (genClockEnable[port]) {
         if (sysConfig.rxClockPolarity[port] & 2)      pcr |= CLKRM;
         if (sysConfig.txClockPolarity[port] & 2)      pcr |= CLKXM;
         if (sysConfig.rxFrameSyncPolarity[port] & 2)  pcr |= FSRM;
         if (sysConfig.txFrameSyncPolarity[port] & 2)  pcr |= FSXM;
      }
   }
   Bsp->McPCR = pcr; 

   // enhanced multi-channel mode:   Rx channels enabled via RCERX.
   Bsp->McMCR2 = 0x00000203; // Tx 8-partition enhanced multichannel. symetric mode.
   Bsp->McMCR1 = 0x00000201; // Rx 8-partition enhanced multichammel. 

   // -- SRGR --
   Bsp->McSRGR2 = 0;
   Bsp->McSRGR1 = 0;

   // -- THRSH -- 
   Bsp->McTHRSH2 = MCBSP2_TXB_LEN - (SltsPerFrame[port] * sysConfig.samplesPerMs) - 1;
   Bsp->McTHRSH1 = (SltsPerFrame[port] * sysConfig.samplesPerMs) - 1;

   // -- IRQENABLE --
   Bsp->McIRQENAB = (IRQ_XRDY | IRQ_RRDY);

   // -- SYSCONFIG --
   Bsp->McSYSCONFIG = SIDLE_NEVER; 

   // -- XCCR, RCCR --
   Bsp->McXCCR = 0x1000;       // dma disabled
   Bsp->McRCCR = 0x0800;       // dma disabled

   samplesPerHwIsr[port] = SltsPerFrame[port] * sysConfig.samplesPerMs;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// setupMcBSP
//
// FUNCTION
//  
//     Writes McBSP registers in preparation for startup
//
//    NOTE: if DX is disabled, we will force transmission of zeroes on inactive slots
//          if DX is enabled, inactive slots will float
//
// PARAMETERS
//
//     port - McBSP port index
//
ADT_Bool setupMcBSP (int port) {
   int intrMask;
   ADT_UInt32 BspBase;
   McBspInfo *Bsp;
   int i;

   if (NUM_TDM_PORTS <= port) return FALSE;

   if (McBSP_Configured[port] == 0) return FALSE;

   Bsp = &McBsp[port];

   if (Bsp->Enabled == Disabled) return FALSE;
  
   BspBase = BaseAddr[port];

   intrMask = HWI_disable ();

   // Pin Control registers
   MCBSP_WRITE (BspBase, PCR_OFF, Bsp->McPCR);

   // Rx and TX control registers
   MCBSP_WRITE (BspBase, RCR2_OFF, Bsp->McRCR2);
   MCBSP_WRITE (BspBase, RCR1_OFF, Bsp->McRCR1);

   MCBSP_WRITE (BspBase, XCR2_OFF, Bsp->McXCR2);
   MCBSP_WRITE (BspBase, XCR1_OFF, Bsp->McXCR1);

   // Multi Channel Control registers.
   MCBSP_WRITE (BspBase, RCERA_OFF, Bsp->McRCER[0]);
   MCBSP_WRITE (BspBase, RCERB_OFF, Bsp->McRCER[1]);
   MCBSP_WRITE (BspBase, RCERC_OFF, Bsp->McRCER[2]);
   MCBSP_WRITE (BspBase, RCERD_OFF, Bsp->McRCER[3]);
   MCBSP_WRITE (BspBase, RCERE_OFF, Bsp->McRCER[4]);
   MCBSP_WRITE (BspBase, RCERF_OFF, Bsp->McRCER[5]);
   MCBSP_WRITE (BspBase, RCERG_OFF, Bsp->McRCER[6]);
   MCBSP_WRITE (BspBase, RCERH_OFF, Bsp->McRCER[7]);

   if (sysConfig.dxDelay[port] == 0) {
        MCBSP_WRITE (BspBase, XCERA_OFF, Bsp->McRCER[0]);
        MCBSP_WRITE (BspBase, XCERB_OFF, Bsp->McRCER[1]);
        MCBSP_WRITE (BspBase, XCERC_OFF, Bsp->McRCER[2]);
        MCBSP_WRITE (BspBase, XCERD_OFF, Bsp->McRCER[3]);
        MCBSP_WRITE (BspBase, XCERE_OFF, Bsp->McRCER[4]);
        MCBSP_WRITE (BspBase, XCERF_OFF, Bsp->McRCER[5]);
        MCBSP_WRITE (BspBase, XCERG_OFF, Bsp->McRCER[6]);
        MCBSP_WRITE (BspBase, XCERH_OFF, Bsp->McRCER[7]);
   } else {
        MCBSP_WRITE (BspBase, XCERA_OFF, Bsp->McXCER[0]);
        MCBSP_WRITE (BspBase, XCERB_OFF, Bsp->McXCER[1]);
        MCBSP_WRITE (BspBase, XCERC_OFF, Bsp->McXCER[2]);
        MCBSP_WRITE (BspBase, XCERD_OFF, Bsp->McXCER[3]);
        MCBSP_WRITE (BspBase, XCERE_OFF, Bsp->McXCER[4]);
        MCBSP_WRITE (BspBase, XCERF_OFF, Bsp->McXCER[5]);
        MCBSP_WRITE (BspBase, XCERG_OFF, Bsp->McXCER[6]);
        MCBSP_WRITE (BspBase, XCERH_OFF, Bsp->McXCER[7]);
   }
   MCBSP_WRITE (BspBase, MCR2_OFF,    Bsp->McMCR2);
   MCBSP_WRITE (BspBase, MCR1_OFF,    Bsp->McMCR1);

   // Sample rate generation registers
   MCBSP_WRITE (BspBase, SRGR2_OFF, Bsp->McSRGR2);
   MCBSP_WRITE (BspBase, SRGR1_OFF, Bsp->McSRGR1);

   // Serial port command registers
   MCBSP_WRITE (BspBase, SPCR2_OFF, Bsp->McSPCR2);
   MCBSP_WRITE (BspBase, SPCR1_OFF, Bsp->McSPCR1);

   // Rx and Tx threshold registers
   MCBSP_WRITE (BspBase, THRSH2_OFF, Bsp->McTHRSH2);
   MCBSP_WRITE (BspBase, THRSH1_OFF, Bsp->McTHRSH1);

   // Interrupt enable registers
   MCBSP_WRITE (BspBase, IRQENABLE_OFF, Bsp->McIRQENAB);

   //  Clear pending IRQ status
   MCBSP_WRITE (BspBase, IRQSTATUS_OFF, 0x5FBF);

   // Tx and Rx Configuration Control  registers
   MCBSP_WRITE (BspBase, XCCR_OFF, Bsp->McXCCR); 
   MCBSP_WRITE (BspBase, RCCR_OFF, Bsp->McRCCR); 

   // Write tx samples to prime the buffer
   for (i=0; i<dxrPrime;i++) {
        MCBSP_WRITE (BspBase, DXR_OFF, 0);
   }

   // System Configuration
   MCBSP_WRITE (BspBase, SYSCONFIG_OFF, Bsp->McSYSCONFIG);

   HWI_restore (intrMask);

   if (genClockEnable[port] == Enabled) {
   
      // Step 4) Start sample rate generator and waitfor 2 clock cycles
      MCBSP_OR (BspBase, SPCR2_OFF, GRST);

      // wait for two CLKG cycles: (CLKDIV * 4 CPUclocks) * 2 
      bitDelay (2, port);
   }


   // NOTE:  Step 6) Setup DMA is started by GpakPCM.c
   return TRUE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// startMcBSP
//
// FUNCTION
//  
//     Write McBSP registers to activate transmit and receive
//
// PARAMETERS
//
//     port - McBSP port index
//
ADT_UInt32 startSPCR;

ADT_Bool startMcBSP (int port) {
   int xempty, timeout = 5000;
   ADT_UInt32 BspBase;

   if (NUM_TDM_PORTS <= port) return FALSE;

   BspBase = BaseAddr[port];

   // Step 7) Enable McBSP transmit and receive
  //set RRST to release recv from reset
   MCBSP_OR (BspBase, SPCR1_OFF, RRST);

   //set XRST to release transmit from reset
   MCBSP_OR (BspBase, SPCR2_OFF, XRST);

   if (!genClockEnable[port]) return TRUE;

   // Step 8) Is not a step. It identifies where the frame sync is generated
   startSPCR = MCBSP_READ (BspBase, SPCR2_OFF);

   // Step 9) Wait for DXR to fill
   // poll XEMPTY until it equals 1
   xempty = startSPCR & XEMPTY;
   
   while (xempty == 0 && timeout--) {
      xempty = MCBSP_READ (BspBase, SPCR2_OFF) & XEMPTY;
   };

   // Step 10) Start Frame sync generator
   MCBSP_OR (BspBase, SPCR2_OFF, FRST);

   return TRUE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// stopMcBSP
//
// FUNCTION
//  
//     Write McBSP registers to deactivate: tx, rx
//     frame sync and sample clk
//
// PARAMETERS
//
//     port - McBSP port index
//
ADT_Bool stopMcBSP (int port) {

   ADT_UInt32 BspBase;
   McBspInfo *Bsp;

   if (NUM_TDM_PORTS <= port) return FALSE;
   if (McBSP_Configured[port] == 0) return FALSE;

   BspBase = BaseAddr[port];
   Bsp = &McBsp[port];

   if (Bsp->Enabled == Disabled) return FALSE;

   MCBSP_AND (BspBase, SPCR1_OFF, (~RRST));  
   MCBSP_AND (BspBase, SPCR2_OFF, (~(XRST | GRST | FRST)));  
   return TRUE;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// SetTransmitEnables
//
// FUNCTION
//   Writes McBSP registers to enable individual channels for transmission
//
//   NOTE: if DX is disabled, we force transmission of zeroes on inactive slots
//         if DX is enabled, inactive slots will float
//
// PARAMETERS
//
//     port - McBSP port index
//     Mask - Mask of individual time slots to enable
//
void SetTransmitEnables (int port, ADT_UInt32 Mask[]) {

   ADT_UInt32 BspBase;
   int i;
   McBspInfo *Bsp;

   if (NUM_TDM_PORTS <= port) return;
   
   Bsp = &McBsp[port];
   BspBase = BaseAddr[port];
   
   if (sysConfig.dxDelay[port] == 0) return;

   for (i=0; i<4; i++) {
        Bsp->McXCER[2*i]    |= (Mask[i] & 0xFFFF);
        Bsp->McXCER[2*i+1]  |= ((Mask[i] >> 16) & 0xFFFF);
   }

   MCBSP_WRITE (BspBase, XCERA_OFF, Bsp->McXCER[0]);
   MCBSP_WRITE (BspBase, XCERB_OFF, Bsp->McXCER[1]);
   MCBSP_WRITE (BspBase, XCERC_OFF, Bsp->McXCER[2]);
   MCBSP_WRITE (BspBase, XCERD_OFF, Bsp->McXCER[3]);
   MCBSP_WRITE (BspBase, XCERE_OFF, Bsp->McXCER[4]);
   MCBSP_WRITE (BspBase, XCERF_OFF, Bsp->McXCER[5]);
   MCBSP_WRITE (BspBase, XCERG_OFF, Bsp->McXCER[6]);
   MCBSP_WRITE (BspBase, XCERH_OFF, Bsp->McXCER[7]);

}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ClearTransmitEnables
//
// FUNCTION
//   Writes McBSP registers to disable individual channels for transmission 
//
//   NOTE: if DX is disabled, we force transmission of zeroes on inactive slots
//         if DX is enabled, inactive slots will float
//
// PARAMETERS
//
//     port - McBSP port index
//     Mask - Mask of individual time slots to enable
//
void ClearTransmitEnables (int port, ADT_UInt32 Mask[]) {

   ADT_UInt32 BspBase;
   int i;
   McBspInfo *Bsp;

   if (NUM_TDM_PORTS <= port) return;
   if (sysConfig.dxDelay[port] == 0) return;

   Bsp = &McBsp[port];
   BspBase = BaseAddr[port];
   
   for (i = 0; i < 4; i++) {
        Bsp->McXCER[2*i]    &= (~Mask[i] & 0xFFFF);
        Bsp->McXCER[2*i+1]  &= ((~Mask[i] >> 16) & 0xFFFF);
   }

   MCBSP_WRITE (BspBase, XCERA_OFF, Bsp->McXCER[0]);
   MCBSP_WRITE (BspBase, XCERB_OFF, Bsp->McXCER[1]);
   MCBSP_WRITE (BspBase, XCERC_OFF, Bsp->McXCER[2]);
   MCBSP_WRITE (BspBase, XCERD_OFF, Bsp->McXCER[3]);
   MCBSP_WRITE (BspBase, XCERE_OFF, Bsp->McXCER[4]);
   MCBSP_WRITE (BspBase, XCERF_OFF, Bsp->McXCER[5]);
   MCBSP_WRITE (BspBase, XCERG_OFF, Bsp->McXCER[6]);
   MCBSP_WRITE (BspBase, XCERH_OFF, Bsp->McXCER[7]);
   return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// McBSPFrameError
//
// FUNCTION
//    Determine if frame sync error occurred on McBSP
//
// RETURNS
//    TRUE - frame sync error detected
//
ADT_Bool McBSPFrameError (int port) {

   ADT_UInt32 BspBase;
   McBspInfo *Bsp;
   volatile ADT_UInt32 irqStatus;

   Bsp = &McBsp[port];
   if (Bsp->Enabled == Disabled) return FALSE;

   BspBase = BaseAddr[port];

   irqStatus = MCBSP_READ (BspBase, IRQSTATUS_OFF);
   irqStatus &= (IRQ_XSYNCERR | IRQ_RSYNCERR);

   // clear the error by writing back 1
   if (irqStatus != 0) {
        MCBSP_WRITE (BspBase, IRQSTATUS_OFF, irqStatus);
        return TRUE;
   } else {
        return FALSE;
   }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ProcConfigSerialPortsMsg - Process a Configure Serial Ports message.
//
// FUNCTION
//   Performs processing for a host's Configure Serial Ports message
//   Includes: parsing, parameter validation and serial port register store
//
// PARAMETERS
//
//     pCmd   - Command buffer
//     pReply - Reply buffer
//
// RETURNS
//   Parameter validation status
//
GPAK_PortConfigStat_t ProcConfigSerialPortsMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {

   ADT_UInt32 startDmaCnt;
   ADT_UInt32 McBspEnable[3];
   ADT_UInt32 SlotMask[3][4];   
   ADT_UInt32 txDataDelay[3];
   ADT_UInt32 rxDataDelay[3];
   ADT_UInt16 DmaChanCnt[3];
   GpakCompandModes compMode[3];
   ADT_UInt32 Mask;
   ADT_UInt16 *SlotMap;

   int i, j, k, LastSlot;
   unsigned long startTime;

   // Prepare the reply message.
   pReply[0] |= (MSG_CONFIG_PORTS_REPLY << 8);

   // Make sure there are no channels currently active.
   if (NumActiveChannels != 0)
      return Pc_ChannelsActive;

   //--------------------------------------------------------------------------
   // Parse message for McBSP configuration parameters
   //--------------------------------------------------------------------------
   McBspEnable[0] = (ADT_UInt32)(pCmd[1] & 0x0001);
   McBspEnable[1] = (ADT_UInt32)(pCmd[1] & 0x0002);
   McBspEnable[2] = (ADT_UInt32)(pCmd[1] & 0x0004);


   SlotMask[0][0] = (((ADT_UInt32)pCmd[2])<<16) | (ADT_UInt32)pCmd[3];
   SlotMask[0][1] = (((ADT_UInt32)pCmd[4])<<16) | (ADT_UInt32)pCmd[5];
   SlotMask[0][2] = (((ADT_UInt32)pCmd[6])<<16) | (ADT_UInt32)pCmd[7];
   SlotMask[0][3] = (((ADT_UInt32)pCmd[8])<<16) | (ADT_UInt32)pCmd[9];

   SlotMask[1][0] = (((ADT_UInt32)pCmd[10])<<16) | (ADT_UInt32)pCmd[11];
   SlotMask[1][1] = (((ADT_UInt32)pCmd[12])<<16) | (ADT_UInt32)pCmd[13];
   SlotMask[1][2] = (((ADT_UInt32)pCmd[14])<<16) | (ADT_UInt32)pCmd[15];
   SlotMask[1][3] = (((ADT_UInt32)pCmd[16])<<16) | (ADT_UInt32)pCmd[17];

   SlotMask[2][0] = (((ADT_UInt32)pCmd[18])<<16) | (ADT_UInt32)pCmd[19];
   SlotMask[2][1] = (((ADT_UInt32)pCmd[20])<<16) | (ADT_UInt32)pCmd[21];
   SlotMask[2][2] = (((ADT_UInt32)pCmd[22])<<16) | (ADT_UInt32)pCmd[23];
   SlotMask[2][3] = (((ADT_UInt32)pCmd[24])<<16) | (ADT_UInt32)pCmd[25];

   ClkDiv[0] = (ADT_UInt32)  (pCmd[38] & 0x00ff);
   ClkDiv[1] = (ADT_UInt32) ((pCmd[38] & 0x00ff) >> 8);
   ClkDiv[2] = (ADT_UInt32)  (pCmd[39] & 0x00ff);

   PulseWidth [0] =  (ADT_UInt32) ((pCmd[39] & 0xff00) >> 8);
   PulseWidth [1] =  (ADT_UInt32)  (pCmd[40] & 0x00ff);
   PulseWidth [2] =  (ADT_UInt32) ((pCmd[40] & 0xff00) >> 8);

   FrameWidth [0] =  (ADT_UInt32) ((pCmd[41] & 0xfff0) >> 4);
   FrameWidth [1] =  (ADT_UInt32) ((pCmd[42] & 0xfff0) >> 4);
   FrameWidth [2] =  (ADT_UInt32) ((pCmd[43] & 0xfff0) >> 4);

   genClockEnable[0] = (GpakActivation)(pCmd[41] & 0x0001);
   genClockEnable[1] = (GpakActivation)(pCmd[42] & 0x0001);
   genClockEnable[2] = (GpakActivation)(pCmd[43] & 0x0001);

   compMode[0] = (GpakCompandModes)((pCmd[41] & 0x000E)>>1);
   compMode[1] = (GpakCompandModes)((pCmd[42] & 0x000E)>>1);
   compMode[2] = (GpakCompandModes)((pCmd[43] & 0x000E)>>1);


   //--------------------------------------------------------------------------
   // Validate configuration parameters
   //--------------------------------------------------------------------------
   if      (!validCompanding(compMode[0])) return Pc_InvalidCompandMode0;
   else if (!validCompanding(compMode[1])) return Pc_InvalidCompandMode1;
   else if (!validCompanding(compMode[2])) return Pc_InvalidCompandMode2;

   txDataDelay[0] = (ADT_UInt32)(pCmd[44] & 0x0003);
   rxDataDelay[0] = (ADT_UInt32)((pCmd[44] >> 2) & 0x0003);
   txDataDelay[1] = (ADT_UInt32)((pCmd[44] >> 4) & 0x0003);
   rxDataDelay[1] = (ADT_UInt32)((pCmd[44] >> 6) & 0x0003);
   txDataDelay[2] = (ADT_UInt32)((pCmd[44] >> 8) & 0x0003);
   rxDataDelay[2] = (ADT_UInt32)((pCmd[44] >> 10) & 0x0003);

   if      ((2 < txDataDelay[0]) || (2 < rxDataDelay[0])) return Pc_InvalidDataDelay0;
   else if ((2 < txDataDelay[1]) || (2 < rxDataDelay[1])) return Pc_InvalidDataDelay1;
   else if ((2 < txDataDelay[2]) || (2 < rxDataDelay[2])) return Pc_InvalidDataDelay2;

   //--------------------------------------------------------------------------
   // For each enabled McBsp. Verify that
   //     The last selected slot by bit mask is within configured slots (SlotsPerFrame).
   //     The total selected slots is supported for DMA
   //--------------------------------------------------------------------------
   for (i = 0; i < NUM_TDM_PORTS; i++) {
      
      DmaChanCnt[i] = 0;

      if (!McBspEnable[i]) continue;
      
      // Determine the highest numbered selected slot.
      LastSlot = determineLastSlot (SlotMask[i][3],96);
      if (!LastSlot) LastSlot = determineLastSlot (SlotMask[i][2],64);
      if (!LastSlot) LastSlot = determineLastSlot (SlotMask[i][1],32);
      if (!LastSlot) LastSlot = determineLastSlot (SlotMask[i][0],0);
     
      // Count the number of selected slots.
      for (k = 0; k < 4; k++) {
         Mask = SlotMask[i][k];
         for (j = 0; j < 32; j++) {
            if ((Mask & 1) != 0) DmaChanCnt[i]++;
         
            Mask >>= 1;
         }
      }

      if (MaxDmaSlots[i] < DmaChanCnt[i])
         return (GPAK_PortConfigStat_t) (Pc_TooManySlots0 + i);        // Number active more than supported by build

      if (DmaChanCnt[i] == 0)
         return (GPAK_PortConfigStat_t) (Pc_NoSlots0 + i);             // No slots active
   }

   sysConfig.rxDataDelay[0] = rxDataDelay[0];
   sysConfig.txDataDelay[0] = txDataDelay[0];
   sysConfig.rxDataDelay[1] = rxDataDelay[1];
   sysConfig.txDataDelay[1] = txDataDelay[1];
   sysConfig.rxDataDelay[2] = rxDataDelay[2];
   sysConfig.txDataDelay[2] = txDataDelay[2];

   sysConfig.compandingMode[0] = compMode[0];
   sysConfig.compandingMode[1] = compMode[1];
   sysConfig.compandingMode[2] = compMode[2];


   // Configure the McBSP registers.
   ConfigureMcBSP (McBspEnable, SlotMask);
   StartGpakPcm   (&DmaChanCnt[0]);

   // Wait up to 10 ms for verification that the DMA has started.
   startTime = CLK_gethtime ();
   startDmaCnt = ApiBlock.DmaSwiCnt;

   if (MatchDmaFlags == 0) 
      return Pc_Success;

   do {
      if (2 <= (ApiBlock.DmaSwiCnt - startDmaCnt)) return Pc_Success;
   } while ((CLK_gethtime() - startTime) < (CLK_countspms() * 10));
   
   for (i=0; i<NUM_TDM_PORTS; i++) {
        SlotMap = pSlotMap[i];
        for (j=0; j<SltsPerFrame[i]; j++)
            SlotMap[j] = UNCONFIGURED_SLOT;
   }

   return Pc_NoInterrupts;
}



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ConfigureMcBSP - Configure G.PAK McBSP.
//
// FUNCTION
//   Configures PCM serial port I/O.  Called in response to host message
//
//  Inputs
//     McBspEnable - Array indicating which McBsp ports are to be configured.
//     McBspSlotMask - Array[port][word] of bit masks (four elements per port
//                     indicating which time slots are to be activated
//                     Each word represents 32 time slots. 
//
//
void ConfigureMcBSP (ADT_UInt32 McBspEnable[3], ADT_UInt32 McBspSlotMask[3][4]) { 

   int port, slot, idx;
   ADT_UInt32 Mask;                // mask value for testing
   ADT_UInt32 DmaSlots;
   ADT_UInt16 DmaIndx;             // DMA buffer index (also used for tx Pin)
   ADT_UInt16 *SlotMap;
   
   PowerOnMcBSPs ();
   //--------------------------------------------------------------------------
   // Map each active rx time slot to its DMA slot and establish register values
   //--------------------------------------------------------------------------
   for (port = 0; port < NUM_TDM_PORTS; port++) {
      McBspInfo *Bsp = &McBsp [port];

      SlotMap = pSlotMap[port];

      if (McBspEnable[port]) {   
          memset((void *)Bsp, 0, sizeof(McBspInfo));
          storeMcBSPConfiguration (port);
      } else {
          Bsp->Enabled = Disabled;
          for (slot = 0; slot < SltsPerFrame[port]; slot++) 
              SlotMap[slot] = UNCONFIGURED_SLOT;
          continue;
      }

      Bsp->Enabled = Enabled;

      // Configure for DMA channels
      slot = 0;
      DmaIndx = 0;

      // Determine DMA index of active slots
      for (idx = 0; idx < 4; idx++) {
         Mask = 1;
         DmaSlots = McBspSlotMask[port][idx];

         while (Mask != 0 && (DmaIndx < SltsPerFrame[port])) {
            if ((Mask & DmaSlots) != 0) SlotMap[slot++] = DmaIndx++;
            else                        SlotMap[slot++] = UNCONFIGURED_SLOT;
            Mask = Mask << 1;
         }        

         Bsp->McXCER [2*idx] = 0;
         Bsp->McXCER [2*idx+1] = 0;

         Bsp->McRCER [2*idx] = DmaSlots & 0xFFFF;
         Bsp->McRCER [2*idx+1] = (DmaSlots >> 16) & 0xFFFF;
      }

   McBSP_Configured[port] = McBspEnable[port];
   tdmPortFuncs = &McBSPPort;

   }
   return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// GetMasks
//
// FUNCTION
//    Get bit mask corresponding to port, pin, slot and 
//
//
// PARAMETERS
//    McBspID - 
// RETURNS
//  highest slot number if any bits in Mask are set. Otherwise returns 0.
//
void GetMasks (ADT_Word McBspId, int Slot, int SlotCnt, ADT_Word *TxMask) {

   ADT_Word MaskBit; 
   int group;
   ADT_UInt16 *SlotMap;
   
   memset (TxMask, 0, MULTI_REG * sizeof (ADT_Word));
   if (NUM_TDM_PORTS <= McBspId) return;

   SlotMap = pSlotMap[McBspId];

   MaskBit = 1 << (Slot % 32);
   group = (Slot / 32);
   
   // Add additional slots (circuit data) to slot masks
   while (0 < SlotCnt)  {
      if (SlotMap[group] != UNCONFIGURED_SLOT) {
         TxMask[group] |= MaskBit;
         SlotCnt--;
      }
      if (MaskBit != 0x8000000) {
         MaskBit <<= 1;
      } else {
         MaskBit = 1;
         group++;
      }
   }
}





// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// determineLastSlot - determine the highest slot number
//
// FUNCTION
// Determines the highest configured slot number based upon
// the 32-bit slot mask and offset
//
// RETURNS
//  highest slot number if any bits in Mask are set. Otherwise returns 0.
//
static ADT_UInt16 determineLastSlot (ADT_UInt32 Mask, ADT_UInt16 offset) {

   ADT_UInt16 j;

   for (j = 31; j != 0; j--)    {
      if ((Mask & 0x80000000) != 0)       {
         return (j + offset);
      }
      Mask = Mask << 1;
   }
   return(0);
}

int updateTdmLoopback(GpakSerialPort_t port, GpakActivation state) {
        return 0;
}

#define HWI_TDM_COMPLETE   8    // HW Interrupt vector for EDMA completions

static int firstTime = 1;
int isrCnt=0;
int txIsrCnt=0;
int rxIsrCnt=0;
int txoverflow=0;
int rxunderflow=0;
int hwIsrOnly = 1;

#pragma CODE_SECTION (GpakMcBsp1Isr, "FAST_PROG_SECT")
// McBSP1 ISR - coded only for mcBSP1...
static void GpakMcBsp1Isr (void) {
   volatile ADT_UInt32 irqStatus;
   ADT_UInt32 i,num,nfree,navail;
   ADT_PCM16 *pI16;

   irqStatus = MCBSP_READ (McBSP1_BASE, IRQSTATUS_OFF);

#ifdef _DEBUG
   isrCnt++;
   logTime (0x00100000ul | irqStatus);
#endif

    if (irqStatus & IRQ_XRDY) {
    	txIsrCnt++;
    	num =  samplesPerHwIsr[1];
        nfree = MCBSP_READ (McBSP1_BASE, XBUFFSTAT_OFF);
        pI16 = TxDMABuffers[1];

        if (nfree < num) {
            num = nfree;
            txoverflow++;
        }

        for (i=0; i<num; i++)
            MCBSP_WRITE (McBSP1_BASE, DXR_OFF, *pI16++);
    }

    if (irqStatus & IRQ_RRDY) {
    	rxIsrCnt++;
        num =  samplesPerHwIsr[1];
        navail = MCBSP_READ (McBSP1_BASE, RBUFFSTAT_OFF);
        pI16 = RxDMABuffers[1];

        if (navail < num) {
            num = navail;
            rxunderflow++;
            }

        for (i=0; i<num; i++)
            *pI16++ = MCBSP_READ (McBSP1_BASE, DRR_OFF);
    }

   	SWI_or (SWI_Dma, irqStatus);

   // clear the pending interrupts by writing back the register contents
   MCBSP_WRITE (McBSP1_BASE, IRQSTATUS_OFF, irqStatus);

   return;
}
void McBspHwIrqSetup () {

   if (firstTime) {
      C64_disableIER (1 << HWI_TDM_COMPLETE);
      // Remove all interrupts from combiner
      REG_WR (INTR_CMB,      0xFFFFFFFF);
      REG_WR (INTR_CMB + 4,  0xFFFFFFFF);
      REG_WR (INTR_CMB + 8,  0xFFFFFFFF);
      REG_WR (INTR_CMB + 12, 0xFFFFFFFF);

      // Map TDM McBSP interrupt completion signal to GpakMcBspIsr
      HWI_dispatchPlug (HWI_TDM_COMPLETE, (Fxn) &GpakMcBsp1Isr, -1, NULL);
      HWI_eventMap     (HWI_TDM_COMPLETE, CSL_MCBSP_1_EVT);

      // McBsp1 interrupts pass through the wakeup generator. 
      // Configure the wakeup generator clock to free run, and unmask IRQ34
      REG_WR (WUGEN_SYSCONFIG_ADDR, 0);
      REG_WR (WUGEN_MEVTCLR1_ADDR, WUGEN_IRQ34);

      firstTime = 0;
      C64_enableIER (1 << HWI_TDM_COMPLETE);
   }
 }

// -------------------------- stuff that was part of DMA module -------------

ADT_Events enableDMA (int port) {
   McBspInfo *Bsp;

   if (NUM_TDM_PORTS <= port) 
        return 0;
   Bsp = &McBsp[port];
   if (Bsp->Enabled == Disabled) 
        return 0;

   RxEvents[port] = IRQ_RRDY;
   TxEvents[port] = IRQ_XRDY;

   return (IRQ_RRDY | IRQ_XRDY);
}

ADT_Events disableDMA (int port) {
   McBspInfo *Bsp;

   if (NUM_TDM_PORTS <= port) 
        return 0;
   Bsp = &McBsp[port];
   if (Bsp->Enabled == Disabled) 
        return 0;

   RxEvents[port] = 0;
   TxEvents[port] = 0;

   return (IRQ_RRDY | IRQ_XRDY);
}

#ifndef _DEBUG
   #define CheckTxPointers(circ,dma) 
   #define CheckRxPointers(circ,dma) 
#else

inline void CheckTxPointers (CircBufInfo_t *circ, ADT_PCM16 *dma) {
   if (circ == pDrain) return;
   LogTransfer (0xC1, circ, &circ->pBufrBase[circ->TakeIndex], 0, dma, SAMPLES_PER_INTERRUPT);
   AppErr ("TxPointerError", getAvailable (circ) < SAMPLES_PER_INTERRUPT);
}
inline void CheckRxPointers (CircBufInfo_t *circ, ADT_PCM16 *dma) {
   if (circ == pSink) return;
   LogTransfer (0x1C, 0, dma, circ, &circ->pBufrBase[circ->PutIndex], SAMPLES_PER_INTERRUPT);
   AppErr ("RxPointerError", getFreeSpace (circ) < SAMPLES_PER_INTERRUPT);
}
#endif

void DMAInitialize () { 
   memset (TxDMABuffers[0], 0, sizeof(ADT_PCM16) * sysConfig.dmaBufferSize[0]);   
   memset (TxDMABuffers[1], 0, sizeof(ADT_PCM16) * sysConfig.dmaBufferSize[1]);   

   // setup Hw Interrupts
   McBspHwIrqSetup();
}


ADT_Bool CopyDmaTxBuffers (int port, int slotCnt, CircBufInfo_t **TxCircBufrList, int slip) {
    int k, buffCnt;
    CircBufInfo_t *TxCirc, **TxCircBufr;
    ADT_PCM16     *TxDmaBufr;
    ADT_PCM16     *DmaBufr0;

    TxCircBufr = TxCircBufrList;
    TxDmaBufr  = TxDMABuffers[port];
    DmaBufr0   = TxDmaBufr;

    for (k = 0; k < slotCnt; k++, TxCircBufr++)  {
        TxCirc = *TxCircBufr;

        CheckTxPointers (TxCirc, TxDmaBufr);   // Make sure there are enough samples for transfers
        buffCnt = TxCirc->BufrSize - TxCirc->TakeIndex;

        if (buffCnt <= SAMPLES_PER_INTERRUPT) {
            memcpy (TxDmaBufr, &TxCirc->pBufrBase[TxCirc->TakeIndex], buffCnt << 1);
            TxDmaBufr += buffCnt;
            buffCnt = SAMPLES_PER_INTERRUPT - buffCnt;
            TxCirc->TakeIndex = 0;
        } else {
            buffCnt = SAMPLES_PER_INTERRUPT;   
        }
        memcpy (TxDmaBufr, &TxCirc->pBufrBase[TxCirc->TakeIndex], buffCnt << 1);
        TxCirc->TakeIndex += buffCnt;
        TxDmaBufr += buffCnt;
    }

    customTDMOut (DmaBufr0, port, slotCnt);

    return 0;
}

ADT_Bool CopyDmaRxBuffers (int port, int slotCnt, CircBufInfo_t **RxCircBufrList, int slip) {
    int k, buffCnt;
    CircBufInfo_t *RxCirc, **RxCircBufr;
    ADT_PCM16     *RxDmaBufr;

    RxCircBufr = RxCircBufrList;
    RxDmaBufr = RxDMABuffers[port];

    customTDMIn (RxDmaBufr, port, slotCnt);

    for (k = 0; k < slotCnt; k++, RxCircBufr++)  {
        RxCirc = *RxCircBufr; 

        // Copy from active inbound DMA buffer to circular buffer
        CheckRxPointers (RxCirc, RxDmaBufr);   // Make sure there is enough samples/room for transfers

        buffCnt = RxCirc->BufrSize - RxCirc->PutIndex;
        if (buffCnt <= SAMPLES_PER_INTERRUPT) {
            memcpy (&RxCirc->pBufrBase[RxCirc->PutIndex], RxDmaBufr, buffCnt << 1);
            RxDmaBufr += buffCnt;
            buffCnt = SAMPLES_PER_INTERRUPT - buffCnt;
            RxCirc->PutIndex = 0;
        } else {
            buffCnt = SAMPLES_PER_INTERRUPT;   
        }
        memcpy (&RxCirc->pBufrBase[RxCirc->PutIndex], RxDmaBufr, buffCnt << 1);
        RxCirc->PutIndex += buffCnt;
        RxDmaBufr += buffCnt;
    }    
    return 0;
}

int TogglePCMInSignal (unsigned int port) {
   return 0;
}

int TogglePCMOutSignal (unsigned int port) {
   return 0;
}

int DmaLbRxToTxBuffers (int port, int slotCnt) {
    return 0;
}

void zeroTxBuffer (int port, int dmaSlot) { 
   memset (TxDMABuffers[port], 0, sizeof(ADT_PCM16) * sysConfig.dmaBufferSize[port]);   
}


void generateMcBSPSignals (int mcBSPMHz, int port, int slotsPerFrame, int SamplesPerSec) {

   int bitsPerFrame;
   ADT_UInt32 mcBSPClocksPerSec, mcBSPClocksPerFrame, clocksPerBit;

   sysConfig.rxClockPolarity[port] |= 2;;
   sysConfig.txClockPolarity[port] |= 2;;
   sysConfig.rxFrameSyncPolarity[port] |= 2;;
   sysConfig.txFrameSyncPolarity[port] |= 2;
   
   mcBSPClocksPerSec = mcBSPMHz * 1000000U;

   mcBSPClocksPerFrame = mcBSPClocksPerSec / SamplesPerSec;
  
   bitsPerFrame = slotsPerFrame * sysConfig.serialWordSize[port];

   if (bitsPerFrame == 0) {
      genClockEnable [port] = Disabled;
      return;
   }

   clocksPerBit = mcBSPClocksPerFrame / bitsPerFrame;


   PulseWidth [port] = 1;
   FrameWidth [port] = bitsPerFrame - 1;
   ClkDiv [port] = clocksPerBit - 1;

  genClockEnable [port] = Enabled;
}

