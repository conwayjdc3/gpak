/*
 * Copyright (c) 2007, Adaptive Digital Technologies, Inc.
 *
 * File Name: 64xTDMDma.c
 *
 * Description:
     This file contains G.PAK DMA functions to transfer between McBSPs and memory.
 *    
 * Version: 1.0
 *
 * Revision History:
 *   8/9/07 - Initial release.
 *
 */
/*
   The DMA configuration details for each McBSP port are defined below. 

                   Tx0  Rx0      Tx1  Rx1     Tx2  Rx2    
  McBSP Start Chn   12   13       14   15      17   18   (static for C64)
  McASP Start Chn   34   37       40   43                (static for C64)
    DMA Ping Lnk    64   66       68   70      72   74
    DMA Pong Lnk    65   67       69   71      73   75

    TCC Event       12   13       14   15       4    5
    
    
    DMA completion IRQ    8    (for ALL DMA channels)

   NOTE:  The DMA channels must not conflict with the channel assignments 
          made by TI and the algorithm instance data DMA.

                   ---------------------------
                   
   The DMA is setup to transfer 1ms of data between the McBSP and a DMA buffer. 
   Two linear buffers (ping and pong) are used so that the DMA software interrupt 
   can work with one set of buffers (e.g. ping) during the same 1 ms interval that
   the DMA is transfering data between the McBSPs and the other set of buffers (e.g. pong).

   The channel/frame (2 bytes/element) data is placed into the DSP's ping and pong buffers 
   as shown below:

 
      C1F1   C1F2   C1F3   C1F4   C1F5   C1F6   C1F7   C1F8
      C2F1   C2F2   C2F3   C2F4   C2F5   C2F6   C2F7   C2F8
      C3F1   C3F2   C3F3   C3F4   C3F5   C3F6   C3F7   C3F8
      C4F1   C4F2   C4F3   C4F4   C4F5   C4F6   C4F7   C4F8
      C5F1   C5F2   C5F3   C5F4   C5F5   C5F6   C5F7   C5F8
                                . 
                                . 
                                . 
      CNF1   CNF2   CNF3   CNF4   CNF5   CNF6   CNF7   CNF8
 

  The order of the data arriving at McBSP is as follows:

       C1F1 C2F1 C3F1 ... CNF1 | C1F2 C2F2 C3F2 ... CNF2 | C1F3 ... CNF8

  Where the | character symbolizes the beginning of a new frame of N channels.
   
  After 8 frames (1ms) of data, the DMA automatically switches between the ping and pong
  buffers and generates a hardware interrupt so that the CPU can begin processing the next
  1ms of data.
  
  
   Each buffer consists of 2 bytes per element, N channels per frame and 8 frames.
   The first element of the pong buffer is located immediately after the last element 
   in the ping buffer.  
   
   NOTE:  Separating the ping and pong buffers (as opposed to interleaving the 
          buffers as in earlier versions) reduces the need for L1 cache updates
          since the CPU and DMA will always be accessing different cache lines.

                   -------------------------
                   
   DMA transfers are linked together, using the DMA parameter blocks, such that 
   at the completion of a ping buffer transfer, the pong buffer transfer is 
   automatically started and vice versa.
   
   The CPU is notified that a transfer is complete via a common interrupt.
   The interrupt handler can determine which transfer(s) is complete by reading
   and clearing the CIPR register

                   -------------------------

   INTERFACES

   void DMAInitialize ()
   ADT_UInt16 enableDMA  (port) 
   ADT_UInt16 disableDMA (port)

   ADT_Bool DmaLostTxSync (int port, ADT_PCM16 *TxSwi);
   ADT_Bool DmaLostRxSync (int port, ADT_PCM16 *RxSwi);

   void CopyDmaTxBuffers (port, CircBufInfo_t **pCircTxBase,  ADT_UInt16 *pDmaTxBufr);
   void CopyDmaRxBuffers (port, ADT_UInt16 *pDmaRxBufr, CircBufInfo_t **pCircRxBase);

                      -------------------------

     DEBUGGING AIDES
     
   TxPatternSlot1    = (non-zer0) Pattern placed only in first slot.
   TxPatternAllSlots = (non-zero) Pattern placed in all slots.  TxPatternSlot1 takes precedence on first slot.

   RxPatternSlot1    = (non-zer0) Pattern placed only in first slot.
   RxPatternAllSlots = (non-zer0) Pattern placed in all slots.  RxPatternSlot1 takes precedence on first slot.

   FrameStamp = (true) 1st byte of each frame contains current frame count   
*/
#include <std.h>
#include <hwi.h>
#include <c64.h>

#include "GpakDefs.h"
#include "GpakExts.h"
#include "GpakPcm.h"
#include "GpakPragma.h"

extern int customTDMOut (ADT_PCM16 *dataOut, int port, int slotCnt);
extern int customTDMIn  (ADT_PCM16 *dataIn,  int port, int slotCnt);


#define NUM_TDM_PORTS 3
int TdmRxPriority = 0;  // Urgent
int TdmTxPriority = 1;  // High


static void initRxTCCParams (ADT_UInt32 deviceIdx);
static void initTxTCCParams (ADT_UInt32 deviceIdx);
static void GpakDmaIsr (void);

//---------------------------------------------------------
//   Configuration structures / defines
//

#define EDMA_BASE_ADDR     0x01A00000    // base address of EDMA Registers
#define PARAM_SIZE                 24

#define EDMA_EERL_ADDR     (EDMA_BASE_ADDR + 0x0000FFF4)  // Event enable
#define EDMA_EERH_ADDR     (EDMA_BASE_ADDR + 0x0000FFB4)

#define EDMA_ECRL_ADDR     (EDMA_BASE_ADDR + 0x0000FFF8)   // Event clear
#define EDMA_ECRH_ADDR     (EDMA_BASE_ADDR + 0x0000FFB8)

#define EDMA_CIERL_ADDR    (EDMA_BASE_ADDR + 0x0000FFE8)   // Interrupt enable
#define EDMA_CIERH_ADDR    (EDMA_BASE_ADDR + 0x0000FFA8)

#define EDMA_CIPRL_ADDR    (EDMA_BASE_ADDR + 0x0000FFE4)   // Interrupt pending
#define EDMA_CIPRH_ADDR    (EDMA_BASE_ADDR + 0x0000FFA4)

// DMA channel assignments (Fixed for C64)
#define Tx0_Chn 12
#define Rx0_Chn 13
#define Tx1_Chn 14
#define Rx1_Chn 15
#define Tx2_Chn 17
#define Rx2_Chn 18

// DMA ping and pong (pong = ping + 1)
#define Tx0_Ping 64
#define Rx0_Ping 66
#define Tx1_Ping 68
#define Rx1_Ping 70
#define Tx2_Ping 72
#define Rx2_Ping 74

// Use TCC 4 and 5 (rather than event code) for McBSP2 to avoid 
// conflict with 16 bit flags
#define Tx0_TCC  12
#define Rx0_TCC  13
#define Tx1_TCC  14
#define Rx1_TCC  15
#define Tx2_TCC   4   
#define Rx2_TCC   5

#define IRQ_EDMA_INT        8
#define IRQ_EDMA_MASK   0x100


typedef struct PortAddr {
  ADT_UInt32 Base;    // Base address of Serial port control registers
  ADT_UInt32 TxTCC;   // Tx TCC flag
  ADT_UInt32 RxTCC;   // Rx TCC flag
  ADT_UInt32 TxChn;   // Tx Chn flag
  ADT_UInt32 RxChn;   // Rx Chn flag
  ADT_UInt32 TxPing;
  ADT_UInt32 RxPing;
} PortAddr;
 
PortAddr DMACfg [] = {
 { 0x30000000, Tx0_TCC, Rx0_TCC, Tx0_Chn, Rx0_Chn, Tx0_Ping, Rx0_Ping },
 { 0x34000000, Tx1_TCC, Rx1_TCC, Tx1_Chn, Rx1_Chn, Tx1_Ping, Rx1_Ping },
 { 0x38000000, Tx2_TCC, Rx2_TCC, Tx2_Chn, Rx2_Chn, Tx2_Ping, Rx2_Ping }
};
#pragma DATA_SECTION (DMACfg,    "FAST_DATA_SECT:TDM")


struct ParamsAddr {
  ADT_UInt32 Rx;
  ADT_UInt32 Tx;
} ParamAddr[] = {
   { 0, 0 },
   { 0, 0 },
   { 0, 0 }
};
#pragma DATA_SECTION (ParamAddr,    "FAST_DATA_SECT:TDM")
   
   
static ADT_PCM16* const RxDMABuffers[] = {
   BSP0DMA_RxBuffer, BSP1DMA_RxBuffer, BSP2DMA_RxBuffer
};

static ADT_PCM16* const TxDMABuffers[] = {
   BSP0DMA_TxBuffer, BSP1DMA_TxBuffer, BSP2DMA_TxBuffer 
};


ADT_UInt16 DmaPongOffset [] = { 0, 0, 0 };  // Offset from ping buffer to pong buffer
#pragma DATA_SECTION (DmaPongOffset,    "FAST_DATA_SECT:TDM")

// -------------------------------------------
//  DMA TCC events to port assignments
ADT_UInt16 RxEvents [] = { 0, 0, 0 };
ADT_UInt16 TxEvents [] = { 0, 0, 0 };
#pragma DATA_SECTION (RxEvents,    "FAST_DATA_SECT:TDM")
#pragma DATA_SECTION (TxEvents,    "FAST_DATA_SECT:TDM")


//---------------------------------------------------------
//   Runtime structures
//
ADT_PCM16 *DmaRxBuff [NUM_TDM_PORTS];   // DMA Rcv buffer address for McBSP
ADT_PCM16 *DmaTxBuff [NUM_TDM_PORTS];   // DMA Xmt buffer address for McBSP
ADT_UInt16 DmaRxOffset [NUM_TDM_PORTS];   // current Rx DMA buffer offset (ping or pong) for McBSP
ADT_UInt16 DmaTxOffset [NUM_TDM_PORTS];   // current Tx DMA buffer offset (ping or pong) for McBSP
ADT_UInt16 DmaEventMask = 0;
#pragma DATA_SECTION (DmaRxBuff,    "FAST_DATA_SECT:TDM")
#pragma DATA_SECTION (DmaTxBuff,    "FAST_DATA_SECT:TDM")
#pragma DATA_SECTION (DmaRxOffset,  "FAST_DATA_SECT:TDM")
#pragma DATA_SECTION (DmaTxOffset,  "FAST_DATA_SECT:TDM")

//  Debugging aides
ADT_Bool  FrameStamp = FALSE;         // Time stamp is to be placed on every 8th sample
struct {
   ADT_Bool   firstTime;
   ADT_UInt16 TxPatternSlot1;       // Debug pattern to be placed on Tx timeslot 1
   ADT_UInt16 TxPatternAllSlots;    // Debug pattern to be placed on all Tx timeslots
   ADT_UInt16 RxPatternSlot1;       // Debug pattern to be placed on Rx timeslot 1
   ADT_UInt16 RxPatternAllSlots;    // Debug pattern to be placed on all Rx timeslots

   ADT_UInt16 TxSineSlot;           // Slot to transmit sine pattern
   ADT_UInt16 BroadcastPort;
   ADT_UInt16 BroadcastSlot;
   ADT_UInt16 captureTx;
   ADT_UInt16 captureRx;
} dma = { TRUE, 0, 0, 0, 0,
          0xffff, 0, 0xffff, 0xffff, 0xffff };



//--------------------------------------------------------------------------------
//
//   TEST_SWCOMPAND = Testing S/W compand

#ifdef TEST_SWCOMPAND // convert linear sampled to compressed data to test sw comapnding
   #define muLawExpand(inout,cnt)    G711_ADT_muLawExpand (inout,inout,cnt)
   #define muLawCompress(inout,cnt)  G711_ADT_muLawCompress(inout,inout,cnt)
#else 
   #define muLawExpand(inout, cnt)
   #define muLawCompress(inout,cnt)
#endif



//--------------------------------------------------------------------------------
//
//   Debug = Enable fixed pattern testing and out of sync buffers

#ifndef _DEBUG
   ADT_UInt16 TogglePCMInSignal (int port)  { return 0xff; }
   ADT_UInt16 TogglePCMOutSignal (int port) { return 0xff; }
   #define CheckTxPointers(circ,dma)  
   #define CheckRxPointers(circ,dma)    
   #define FixedPatternTx(TxDmaBufr) 
   #define FixedPatternRx(RxDmaBufr) 
#else

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ToggleTestSignal
//
// FUNCTION
//   Toggles receive port between DMA buffer (normal) and constant tone (test)
//
// RETURNS
//  nothing
//
static ADT_PCM16 SineTableM12[] = {
  -3, 2828, 4000, 2828, -2, -2828, -4000, -2828,
  -1, 2829, 4000, 2828,  0, -2828, -4000, -2828,
};
static ADT_PCM16 Drain [MAX_SAMPLES_PER_MS*2][MAX_CHANS_PER_FRAME];     // Dummy receive buffer
static ADT_PCM16 Dummy [MAX_SAMPLES_PER_MS*2][MAX_CHANS_PER_FRAME];     // Dummy transmit buffer
static ADT_PCM16 BroadcastBufr [MAX_SAMPLES_PER_MS];

ADT_UInt16 inCaptureBuff [900];
CircBufInfo_t captureTDMIn = {
  inCaptureBuff, sizeof (inCaptureBuff)/2, 0, 0 
};
ADT_UInt16 outCaptureBuff [900];
CircBufInfo_t captureTDMOut = {
  outCaptureBuff, sizeof (outCaptureBuff)/2, 0, 0 
};
#pragma DATA_SECTION (inCaptureBuff, "FAST_DATA_SECT:TDM")
#pragma DATA_SECTION (outCaptureBuff, "FAST_DATA_SECT:TDM")

static void sineBuffer (unsigned int port, ADT_PCM16 *Buffer) {
   int i, dmaSltCnt;

   // Populate dummy buffer with 1 kHz SINE wave on all channels
   dmaSltCnt = sysConfig.maxSlotsSupported[port];

   for (i=0; i<dmaSltCnt; i++) {
      memcpy (Buffer, SineTableM12, sizeof (SineTableM12));
      Buffer += (SAMPLES_PER_INTERRUPT * 2);
   }

}

ADT_UInt16 TogglePCMInSignal (unsigned int port) {

   if (NUM_TDM_PORTS <= port) return 0;

   sineBuffer (port, (ADT_PCM16 *) &Dummy);

   if (DmaRxBuff[port] == RxDMABuffers[port]) {
       DmaRxBuff[port] = (ADT_PCM16 *) &Dummy;
       return 1;
   }
   // Restore
   DmaRxBuff[port] = RxDMABuffers[port];
   return 0;
}

ADT_UInt16 TogglePCMOutSignal (unsigned int port) {

   if (NUM_TDM_PORTS <= port) return 0;

   if (DmaTxBuff[port] == TxDMABuffers[port]) {
       sineBuffer (port, TxDMABuffers[port]);
       DmaTxBuff[port] = (ADT_PCM16 *) &Drain;
       return 1;
   }
   // Restore with initial zeroes
   DmaTxBuff[port] = TxDMABuffers[port];
   memset (TxDMABuffers[port], 0, sysConfig.dmaBufferSize[port] * 2);
   return 0;
}

static void FixedPatternTx (ADT_PCM16 *TxDmaBufr) {
   ADT_PCM16 *DmaBufr;
   int i;

   if (dma.TxPatternSlot1) {
      DmaBufr = TxDmaBufr;
      for (i=0; i<SAMPLES_PER_INTERRUPT; i++) *DmaBufr++ = dma.TxPatternSlot1;
   }

   if (dma.TxSineSlot == 0) {
      memcpy (TxDmaBufr, SineTableM12, SAMPLES_PER_INTERRUPT * 2);
   }
   if (dma.captureTx == 0 && dma.captureRx != 0xffff) {
      copyLinearToCirc (&ApiBlock.DmaSwiCnt, &captureTDMOut, 1);
      copyLinearToCirc (TxDmaBufr, &captureTDMOut, SAMPLES_PER_INTERRUPT);
      captureTDMOut.TakeIndex = captureTDMOut.PutIndex;
   }

}
static void FixedPatternRx (ADT_PCM16 *RxDmaBufr) {
   ADT_PCM16 *DmaBufr;
   int i;

   // Overwrite channel(s) output with a fixed (non-zero) value
   if (dma.BroadcastSlot == 0)
      memcpy (BroadcastBufr, RxDmaBufr, sizeof (BroadcastBufr));
   else if (dma.BroadcastSlot != 0xffff)
      memcpy (RxDmaBufr, BroadcastBufr, sizeof (BroadcastBufr));

   if (dma.RxPatternSlot1) {
      DmaBufr = RxDmaBufr;
      for (i=0; i<SAMPLES_PER_INTERRUPT; i++) *DmaBufr++ = dma.RxPatternSlot1;
   }
   if (dma.captureRx == 0 && dma.captureTx != 0xffff) {
      copyLinearToCirc (&ApiBlock.DmaSwiCnt, &captureTDMIn, 1);
      copyLinearToCirc (RxDmaBufr, &captureTDMIn, SAMPLES_PER_INTERRUPT);
      captureTDMIn.TakeIndex = captureTDMIn.PutIndex;
   }
   if (FrameStamp) {
      *RxDmaBufr = (ADT_PCM16) ApiBlock.DmaSwiCnt;
   }     
}


inline void CheckTxPointers (CircBufInfo_t *circ, ADT_PCM16 *dma) {
   if (circ == pDrain) return;
   LogTransfer (0xC1, circ, &circ->pBufrBase[circ->TakeIndex], 0, dma, SAMPLES_PER_INTERRUPT);
   AppErr ("TxPointerError", getAvailable (circ) < SAMPLES_PER_INTERRUPT);
}
inline void CheckRxPointers (CircBufInfo_t *circ, ADT_PCM16 *dma) {
   if (circ == pSink) return;
   LogTransfer (0x1C, 0, dma, circ, &circ->pBufrBase[circ->PutIndex], SAMPLES_PER_INTERRUPT);
   AppErr ("RxPointerError", getFreeSpace (circ) < SAMPLES_PER_INTERRUPT);
}
#endif





//---------------------------------------------------
//{
//  DMAIntialize 
//
// FUNCTION
//   - Disables all DMA; initializes interrupt variables
//}
void DMAInitialize () {
   int port;

   C64_disableIER (IRQ_EDMA_MASK);
   if (dma.firstTime) {
      HWI_dispatchPlug (IRQ_EDMA_INT, (Fxn) &GpakDmaIsr, -1, NULL);
      dma.firstTime = FALSE;
   }
   DmaEventMask = 0;

   // Set the multiplexed interrupt control for all DMA interrupts.
   for (port=0; port<NUM_TDM_PORTS; port++) {
      DmaRxBuff[port] = RxDMABuffers[port];
      DmaTxBuff[port] = TxDMABuffers[port];

      disableDMA (port);
   }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{enableDMA
//
// FUNCTION
//   Enables Tx and Rx DMA for specified McBSP
//
// RETURNS
//    Bit mask of events that are generated at interrupt
//}
ADT_UInt16 enableDMA (int port) {

   ADT_UInt32 chnlBits, chnhBits;
   ADT_UInt16 tccBits;

   if (NUM_TDM_PORTS <= port) return 0;

   chnlBits = chnhBits = 0;
   if (DMACfg[port].RxChn < 32)
       chnlBits |= (1 << DMACfg[port].RxChn);
   else
       chnhBits |= (1 << (DMACfg[port].RxChn - 32));

   if (DMACfg[port].TxChn < 32)
       chnlBits |= (1 << DMACfg[port].TxChn);
   else
       chnhBits |= (1 << (DMACfg[port].TxChn - 32));

   RxEvents[port] = 1 << DMACfg[port].RxTCC;
   TxEvents[port] = 1 << DMACfg[port].TxTCC;
   
   tccBits = (1 << DMACfg[port].RxTCC) | (1 << DMACfg[port].TxTCC);
   DmaEventMask |= tccBits;

   // clear all pending dma events and transfer completion interrupts for port
   REG_WR (EDMA_ECRL_ADDR, chnlBits);
   REG_WR (EDMA_ECRH_ADDR, chnhBits);
   REG_WR (EDMA_CIPRL_ADDR, tccBits);

   initRxTCCParams (port);
   initTxTCCParams (port);

   DmaRxOffset[port] = 0; 
   DmaTxOffset[port] = 0; 
   DmaPongOffset[port] = SAMPLES_PER_INTERRUPT * MaxDmaSlots[port];
 
   // Zero receive buffer
   memset (RxDMABuffers[port], 0, sysConfig.dmaBufferSize[port] * 2); 

   // enable edma events and transfer completion interrupts for port
   REG_OR (EDMA_EERL_ADDR,  chnlBits);
   REG_OR (EDMA_EERH_ADDR,  chnhBits);
   REG_OR (EDMA_CIERL_ADDR, tccBits);
   
   C64_enableIER (IRQ_EDMA_MASK);
   return tccBits;
}



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{disableDMA
//
// FUNCTION
//   This function resets the EDMA controller
//
// RETURNS
//   
//}  Disabled transfer control bits 
ADT_UInt16 disableDMA (int port) {

    ADT_UInt32 chnBits;
    ADT_UInt16 tccBits;

   if (NUM_TDM_PORTS <= port) return 0;

    // Zero transmit buffer
    memset (TxDMABuffers[port], 0, sysConfig.dmaBufferSize[port] * 2); 

    chnBits = (1 << DMACfg[port].RxChn) | (1 << DMACfg[port].TxChn);
    tccBits = (1 << DMACfg[port].RxTCC) | (1 << DMACfg[port].TxTCC);
    DmaEventMask &= ~tccBits;

    RxEvents[port] = 0;
    TxEvents[port] = 0;

    // disable all edma events and interrupts for port
    REG_AND (EDMA_EERL_ADDR,  ~chnBits);
    REG_AND (EDMA_CIERL_ADDR, ~tccBits);

    // clear all pending dma events and interrupts for port
    REG_WR (EDMA_ECRL_ADDR,  chnBits);    
    REG_WR (EDMA_CIPRL_ADDR, tccBits);
    return tccBits;
}




// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{initRxTCCParams
//
// FUNCTION
//  Initializes EDMA parameter ram for EDMA channels that are driven by receive events
//
// Inputs
//    port            - McBSPn
//
//  Outputs
//    DMA Parameters - DMA transfer parameters
//    eer            - Event enable register
//    cier           - Channel interrupt enable register
//
// RETURNS
//  none
//}
static void initRxTCCParams (ADT_UInt32 port) {

   ADT_UInt32 tcc;
   ADT_UInt32 bytesPerSamp, sltsPerFrame, framesPerTransfer;
     
   ADT_UInt32 srcAddress;               // parameter RAM block word 1
   ADT_UInt32 frameElementCount;        // parameter RAM block word 2
   ADT_UInt32 dstAddress;               // parameter RAM block word 3
   ADT_UInt32 frameElementIndex;        // parameter RAM block word 4
   ADT_UInt32 elementReloadLinkAddr;    // parameter RAM block word 5

   ADT_UInt32 dstAddressInc;    

   ADT_UInt32 pingOffset;    // Offset to ping parameters
   ADT_UInt32 pongOffset;    // Offset to pong parameters

   ADT_UInt32 startAddr;    // address of start parameters
   ADT_UInt32 pingAddr;     // address of ping parameters 
   ADT_UInt32 pongAddr;     // address of ping parameters

   dmaTransferOpts_t dmaOpts;

   if (NUM_TDM_PORTS <= port) return;

    bytesPerSamp = 2;
    framesPerTransfer = SAMPLES_PER_INTERRUPT;
    sltsPerFrame      = MaxDmaSlots[port];
    
    tcc = DMACfg[port].RxTCC;
    srcAddress = DMACfg[port].Base;
    dstAddress = (ADT_UInt32) RxDMABuffers[port];

    pingOffset = DMACfg[port].RxPing * PARAM_SIZE;
    pongOffset = pingOffset + PARAM_SIZE;

    startAddr = EDMA_BASE_ADDR + (DMACfg[port].RxChn * PARAM_SIZE);
    pingAddr  = EDMA_BASE_ADDR + pingOffset;
    pongAddr  = EDMA_BASE_ADDR + pongOffset;


    dmaOpts.TransferOptions = 0;
    dmaOpts.Bits.FS     = 0;        // channel is array synchronized
    dmaOpts.Bits.LINK   = 1;        // parameter linking enabled
    dmaOpts.Bits.PDTD   = 0;        // peripheral device transfer to memory
    dmaOpts.Bits.PDTS   = 0;        // peripheral device transfer to peripheral
    dmaOpts.Bits.ATCC   = 0;        // alternate transfer code is don't care
    dmaOpts.Bits.ATCINT = 0;        // alternate transfer complete diasbled
    dmaOpts.Bits.TCCM   = 0;        // 2 msbs of TCC set to 0
    dmaOpts.Bits.TCC    = tcc;      // receive transfer complete code
    dmaOpts.Bits.TCINT  = 1;        // interrupt upon transfer complete
    dmaOpts.Bits.SUM    = 0;        // fixed source address
    dmaOpts.Bits._2DS   = 0;        // 1 dimensional source
    dmaOpts.Bits.ESIZE  = 1;        // 16-bit voice sample
    dmaOpts.Bits.PRI    = TdmTxPriority;
        
    dmaOpts.Bits.DUM    = 3;        // destination address modified by element and frame indices
    dmaOpts.Bits._2DD   = 0;        // 1 dimensional destination

    // (frame count - 1) | element count
    frameElementCount = (framesPerTransfer-1)<<16; 
    frameElementCount |= sltsPerFrame;

    // frame index | element index 
    frameElementIndex = (-((sltsPerFrame-1)*(framesPerTransfer) - 1) * bytesPerSamp)<<16;
    frameElementIndex |= (framesPerTransfer * bytesPerSamp); 

    // element reload index | link address
    elementReloadLinkAddr = sltsPerFrame<<16;
    elementReloadLinkAddr |= pongOffset;

    dstAddressInc = framesPerTransfer * sltsPerFrame * bytesPerSamp;

    // initialize the parameter ram for the entry-point parameters
    REG_WR ( startAddr,     dmaOpts.TransferOptions);
    REG_WR ((startAddr+4),  srcAddress);
    REG_WR ((startAddr+8),  frameElementCount);
    REG_WR ((startAddr+12), dstAddress);
    REG_WR ((startAddr+16), frameElementIndex);
    REG_WR ((startAddr+20), elementReloadLinkAddr);

    // initialize the parameter ram for ping buffer transfers
    REG_WR ( pingAddr,     dmaOpts.TransferOptions);
    REG_WR ((pingAddr+4),  srcAddress);
    REG_WR ((pingAddr+8),  frameElementCount);
    REG_WR ((pingAddr+12), dstAddress);
    REG_WR ((pingAddr+16), frameElementIndex);
    REG_WR ((pingAddr+20), elementReloadLinkAddr);

    // initialize the parameter ram for pong buffer transfers
    dstAddress += dstAddressInc;
    elementReloadLinkAddr &= 0xFFFF0000;
    elementReloadLinkAddr |= pingOffset;

    REG_WR ( pongAddr,     dmaOpts.TransferOptions);
    REG_WR ((pongAddr+4),  srcAddress);
    REG_WR ((pongAddr+8),  frameElementCount);
    REG_WR ((pongAddr+12), dstAddress);
    REG_WR ((pongAddr+16), frameElementIndex);
    REG_WR ((pongAddr+20), elementReloadLinkAddr);

    ParamAddr[port].Rx = startAddr + 0xc;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{initTxTCCParams - Initialize the EDMA parameter ram for transmit channels
//
// FUNCTION
//  Initializes EDMA parameter ram for EDMA channels that are driven by transmit events
//
// Inputs
//    port            - McBSPn
//
//  Outputs
//    DMA Parameters - DMA transfer parameters
//    eer            - Event enable register
//    cier           - Channel interrupt enable register
//
// RETURNS
//  none
//}
static void initTxTCCParams (ADT_UInt32 port) {
                
   unsigned tcc;
   ADT_UInt32 bytesPerSamp, sltsPerFrame, framesPerTransfer;

   ADT_UInt32 srcAddress;               // parameter RAM block word 1
   ADT_UInt32 frameElementCount;        // parameter RAM block word 2
   ADT_UInt32 dstAddress;               // parameter RAM block word 3
   ADT_UInt32 frameElementIndex;        // parameter RAM block word 4
   ADT_UInt32 elementReloadLinkAddr;    // parameter RAM block word 5

   ADT_UInt32 srcAddressInc;
     
   ADT_UInt32 pingOffset;    // Offset to ping parameters
   ADT_UInt32 pongOffset;    // Offset to pong parameters

   ADT_UInt32 startAddr;    // address of start parameters
   ADT_UInt32 pingAddr;     // address of ping parameters 
   ADT_UInt32 pongAddr;     // address of ping parameters


   dmaTransferOpts_t dmaOpts;

   if (NUM_TDM_PORTS <= port) return;

    bytesPerSamp = 2;
    framesPerTransfer = SAMPLES_PER_INTERRUPT;
    sltsPerFrame      = MaxDmaSlots[port];

    tcc = DMACfg[port].TxTCC;
    srcAddress = (ADT_UInt32) TxDMABuffers[port];
    dstAddress = DMACfg[port].Base + 4; 

    pingOffset = DMACfg[port].TxPing * PARAM_SIZE;
    pongOffset = pingOffset + PARAM_SIZE;

    startAddr = EDMA_BASE_ADDR + (DMACfg[port].TxChn * PARAM_SIZE);
    pingAddr  = EDMA_BASE_ADDR + pingOffset;
    pongAddr  = EDMA_BASE_ADDR + pongOffset;

    dmaOpts.TransferOptions = 0;       
    dmaOpts.Bits.FS     = 0;            // channel is element synchronized
    dmaOpts.Bits.LINK   = 1;            // parameter linking enabled
    dmaOpts.Bits.PDTD   = 0;            // peripheral device transfer to memory
    dmaOpts.Bits.PDTS   = 0;            // peripheral device transfer from memory
    dmaOpts.Bits.ATCC   = 0;            // alternate transfer code is don't care 
    dmaOpts.Bits.ATCINT = 0;            // alternate transfer complete diasbled
    dmaOpts.Bits.TCCM   = 0;            // 2 msbs of TCC set to 0
    dmaOpts.Bits.TCC    = tcc;          // transmit transfer complete code
    dmaOpts.Bits.TCINT  = 1;            // interrupt upon transfer complete
    dmaOpts.Bits.DUM    = 0;            // fixed destination address
    dmaOpts.Bits._2DD   = 0;            // 1 dimensional destination
    dmaOpts.Bits.ESIZE  = 1;            // 16-bit voice sample
    dmaOpts.Bits.PRI    = TdmRxPriority;

    dmaOpts.Bits.SUM    = 3;            // source address modified by element & frame indices
    dmaOpts.Bits._2DS   = 0;            // 1 dimensional source

    // (frame count - 1) | element count 
    frameElementCount = (framesPerTransfer-1)<<16; 
    frameElementCount |= sltsPerFrame;

    // frame index | element index
    frameElementIndex = (-((sltsPerFrame-1)*(framesPerTransfer) - 1) * bytesPerSamp)<<16;
    frameElementIndex |= (framesPerTransfer * bytesPerSamp); 

    // element reload index | link address
    elementReloadLinkAddr = sltsPerFrame<<16;
    elementReloadLinkAddr |= pongOffset;

    srcAddressInc = framesPerTransfer * sltsPerFrame * bytesPerSamp;

    // initialize the parameter ram for the entry-point parameters
    REG_WR ( startAddr,     dmaOpts.TransferOptions);
    REG_WR ((startAddr+4),  srcAddress);
    REG_WR ((startAddr+8),  frameElementCount);
    REG_WR ((startAddr+12), dstAddress);
    REG_WR ((startAddr+16), frameElementIndex);
    REG_WR ((startAddr+20), elementReloadLinkAddr);

    // initialize the parameter ram for ping buffer transfers
    REG_WR ( pingAddr,     dmaOpts.TransferOptions);
    REG_WR ((pingAddr+4),  srcAddress);
    REG_WR ((pingAddr+8),  frameElementCount);
    REG_WR ((pingAddr+12), dstAddress);
    REG_WR ((pingAddr+16), frameElementIndex);
    REG_WR ((pingAddr+20), elementReloadLinkAddr);

    // initialize the parameter ram for pong buffer transfers
    srcAddress += srcAddressInc;
    elementReloadLinkAddr &= 0xFFFF0000;
    elementReloadLinkAddr |= pingOffset;

    REG_WR ( pongAddr,     dmaOpts.TransferOptions);
    REG_WR ((pongAddr+4),  srcAddress);
    REG_WR ((pongAddr+8),  frameElementCount);
    REG_WR ((pongAddr+12), dstAddress);
    REG_WR ((pongAddr+16), frameElementIndex);
    REG_WR ((pongAddr+20), elementReloadLinkAddr);

    ParamAddr[port].Tx = startAddr + 0x4;

}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{DmaLostSync
//
// FUNCTION
//    checks that dma transfer and SWI copy are working in opposite buffers
//
// Inputs
//
//    DmaSlots    - Number of dma slot for port
//    DmsStart    - Start of DMA buffer
//    SWIOffset   - Offset of ping/pong bvffer used by SWI copy
//    DmaPos      - DMA register address pointing to current address of DMA transfer
//
// RETURNS
//    Lost sync indication
//}
static inline ADT_Bool DmaLostSync (int DmaSlots,         ADT_PCM16  *DmaStart,
                                    ADT_UInt16 SWIOffset, ADT_PCM16 **DmaPos) {
   ADT_PCM16 *DmaCur;
   ADT_Bool outOfRange, wrongBuffer;

   DmaCur = *DmaPos;

   // Verify within range of DMA buffer
   outOfRange = (DmaCur < DmaStart) || ((DmaStart + (DmaSlots * 2 * SAMPLES_PER_INTERRUPT)) <= DmaCur);
 
   // Verify within range of current (ping/pong) buffer
   if (SWIOffset == 0) DmaStart += (DmaSlots * SAMPLES_PER_INTERRUPT);
 
   wrongBuffer = (DmaCur < DmaStart) || ((DmaStart + (DmaSlots * SAMPLES_PER_INTERRUPT)) <= DmaCur);
 
   return (outOfRange || wrongBuffer);
}

//
//  Switch copy between ping and pong buffers
//
inline switchBuffers (int port, ADT_UInt16 *offset) {
   if (*offset == 0) *offset = DmaPongOffset[port]; 
   else              *offset = 0;
   return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{zeroTxBuffer
//
// FUNCTION
//  Zeroes slot on Tx Buffer when channel goes inactive
//
// Inputs
//    port            - McBSPn
//    dmaSlot         - dma index of slot to zero
//
// RETURNS
//  none
//}
void zeroTxBuffer (int port, int dmaSlot) {
   ADT_PCM16 *txSlot;

   if (NUM_TDM_PORTS <= port) return;

   txSlot = TxDMABuffers[port];

   // Ping buffer fill memory
   txSlot += dmaSlot * SAMPLES_PER_INTERRUPT; 
   memset (txSlot, 0, SAMPLES_PER_INTERRUPT * 2);

   // Pong buffer fill memory
   txSlot +=  DmaPongOffset [port]; 
   memset (txSlot, 0, SAMPLES_PER_INTERRUPT * 2);
}


#ifdef RMF
void AdjustDmaBuffers (ADT_UInt16 numSlots, ADT_PCM16 *DmaBase, int Offset) {

   ADT_PCM16 *NxtBufr, *PrvBufr, LastPcm;
   int i, k;

   NxtBufr = DmaBase + Offset;
   AdjustDmaOffset (Offset);
   PrvBufr = DmaBase + Offset  + SAMPLES_PER_INTERRUPT - 1;

   // Loop through all configured slots zeroing out buffer corresponding ping or pong buffer.
   for (k = 0; k < numSlots; k++)  {
      LastPcm = *PrvBufr;
      LogTransfer (0x10AAD, PrvBufr, NxtBufr, SAMPLES_PER_INTERRUPT);
      for (i = 0; i < SAMPLES_PER_INTERRUPT; i++, NxtBufr++)  {
          *NxtBufr = LastPcm;
      }
      NxtBufr += SAMPLES_PER_INTERRUPT;
      PrvBufr += 2 * SAMPLES_PER_INTERRUPT;
   }
   return;
}
#endif

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{CopyTxDmaBuffers - Copy channel PCM buffers to serial port DMA buffers.
//
// FUNCTION
//   Multiplexs PCM data from active channel PCM buffers onto a serial 
//   port's DMA buffers.
//
// Inputs
//     slotCnt       -  Count of TDM slots
//     TxCircBufrList -  List of transmit circular PCM buffer pointers
//     TxDmaBufr     -  Linear dma transmit buffer
//
// RETURNS
//     lostSync indication
//}
ADT_Bool CopyDmaTxBuffers (int port, int slotCnt, CircBufInfo_t **TxCircBufrList, int slip) {
   
   int k, buffCnt, lostSync;
   CircBufInfo_t *TxCirc, **TxCircBufr;
   ADT_PCM16     *TxDmaBufr;
   ADT_UInt16    *TxOffset;
   ADT_PCM16 *DmaBufr0;
   ADT_Word      SlipSamps;

   TxOffset = &DmaTxOffset[port];

   // Adjust the SWI copy buffer offset (TxOffset) if the DMA and SWI buffers are conflicting
   lostSync = DmaLostSync (slotCnt, TxDMABuffers[port], *TxOffset, (ADT_PCM16 **)(ParamAddr[port].Tx));
   if (lostSync) {
      SendWarningEvent (port + 0x200, WarnFrameSyncError);
      switchBuffers (port, TxOffset);
   }

   // Set the circular buffer and linear DMA pointers to the first slot.
   TxCircBufr = TxCircBufrList;
   TxDmaBufr  = DmaTxBuff[port] + *TxOffset;
   DmaBufr0   = TxDmaBufr;

   SlipSamps = 0;
   if (slip)
      SlipSamps = SAMPLES_PER_INTERRUPT;
   
   // Loop through all configured slots checking for active slots.
   for (k = 0; k < slotCnt; k++, TxCircBufr++)  {
      TxCirc = *TxCircBufr;
   
      // Copy from active outbound channels' circular buffer to linear DMA buffer
      TxCirc->SlipSamps += SlipSamps;
      CheckTxPointers (TxCirc, TxDmaBufr);   // Make sure there are enough samples for transfers
      buffCnt = TxCirc->BufrSize - TxCirc->TakeIndex;

      if (buffCnt <= SAMPLES_PER_INTERRUPT) {
         memcpy (TxDmaBufr, &TxCirc->pBufrBase[TxCirc->TakeIndex], buffCnt << 1);
         TxDmaBufr += buffCnt;
         buffCnt = SAMPLES_PER_INTERRUPT - buffCnt;
         TxCirc->TakeIndex = 0;
      } else {
         buffCnt = SAMPLES_PER_INTERRUPT;   
      }
      memcpy (TxDmaBufr, &TxCirc->pBufrBase[TxCirc->TakeIndex], buffCnt << 1);
      TxCirc->TakeIndex += buffCnt;
      TxDmaBufr += buffCnt;

      // NOTE: muLawExpand is for testing only
      muLawExpand ((void *)(TxDmaBufr-SAMPLES_PER_INTERRUPT), SAMPLES_PER_INTERRUPT);


#ifdef _DEBUG  // Verify that frame markers are correct
         if (FrameStamp)
            AppErr ("FrameStampError",
                         *(TxDmaBufr-(SAMPLES_PER_INTERRUPT*2)) != 0 &&
                         *(TxDmaBufr-(SAMPLES_PER_INTERRUPT*2)) != (short) (ApiBlock.DmaSwiCnt & 0xFFFF));
#endif


   }
   FixedPatternTx (DmaBufr0);   // Allow overwrite of slot 0 with fixed pattern for debug
   customTDMOut (DmaBufr0, port, slotCnt);
   switchBuffers (port, TxOffset);


   return lostSync;
}



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{CopyDmaRxBuffers - Copy serial port DMA buffers to channel PCM buffers.
//
// FUNCTION
//   Demultiplexs PCM data between a serial port's DMA buffers and the
//   PCM circular buffers of any active channels using the serial port.
//
// Inputs
//     port          -  Count of TDM slots
//     RxCircBufrList -  List of receive circular PCM buffer pointers
//     RxDmaBufr     -  Linear dma receive buffer
//
// RETURNS
//     lostSync indication
//}
ADT_Bool CopyDmaRxBuffers (int port, int slotCnt, CircBufInfo_t **RxCircBufrList, int slip) {
   
   int k, buffCnt, lostSync;
   CircBufInfo_t *RxCirc, **RxCircBufr;
   ADT_PCM16     *RxDmaBufr;
   ADT_UInt16    *RxOffset;

              
   RxOffset = &DmaRxOffset[port];

   // Adjust the SWI copy buffer offset (RxOffset) if the DMA and SWI buffers are conflicting
   lostSync = DmaLostSync (slotCnt, RxDMABuffers[port], *RxOffset, (ADT_PCM16 **)(ParamAddr[port].Rx));
   if (lostSync) {
      SendWarningEvent (port + 0x100, WarnFrameSyncError);
      switchBuffers (port, RxOffset);
   }

   // Set the circular buffer and linear DMA pointers to the first slot.
   RxCircBufr = RxCircBufrList;
   RxDmaBufr  = DmaRxBuff[port] + *RxOffset;
   
   // Loop through all configured slots checking for active slots.
   FixedPatternRx  (RxDmaBufr);  // Allow overwrite of slot 0 with fixed pattern for debug
   customTDMIn (RxDmaBufr, port, slotCnt);

   SlipSamps = 0;
   if (slip)
       SlipSamps = SAMPLES_PER_INTERRUPT;

   for (k = 0; k < slotCnt; k++, RxCircBufr++)  {

      RxCirc = *RxCircBufr; 

      // Copy from active inbound DMA buffer to circular buffer
      RxCirc->SlipSamps += SlipSamps;
      CheckRxPointers (RxCirc, RxDmaBufr);   // Make sure there is enough samples/room for transfers

      // NOTE: muLawCompress for testing only
      muLawCompress ((void *)RxDmaBufr, SAMPLES_PER_INTERRUPT);

      buffCnt = RxCirc->BufrSize - RxCirc->PutIndex;
      if (buffCnt <= SAMPLES_PER_INTERRUPT) {
         memcpy (&RxCirc->pBufrBase[RxCirc->PutIndex], RxDmaBufr, buffCnt << 1);
         RxDmaBufr += buffCnt;
         buffCnt = SAMPLES_PER_INTERRUPT - buffCnt;
         RxCirc->PutIndex = 0;
      } else {
         buffCnt = SAMPLES_PER_INTERRUPT;   
      }
      memcpy (&RxCirc->pBufrBase[RxCirc->PutIndex], RxDmaBufr, buffCnt << 1);
      RxCirc->PutIndex += buffCnt;
      RxDmaBufr += buffCnt;
   }   

   switchBuffers (port, RxOffset);
   return lostSync;
}




// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{GpakEdmaIsr
//
// FUNCTION
//  G.PAK Hardware Interrupt handler for all EDMA Channels.
//  It's triggered by the occurrance tx and rx mcbsp edma events. The 
//  CIPR register is checked to determine which events have occurred and
//  the SWI_Dma interrupt is issued to start the circular buffer copies.
//
// RETURNS
//  nothing
//}
static void GpakDmaIsr (void) {

   ADT_UInt32 cipr;

   // Indicate an interrupt has occurred
   REG_RD (EDMA_CIPRL_ADDR, cipr);

   cipr &= DmaEventMask;

#ifdef _DEBUG
   if (NumActiveChannels) logTime (0x00100000ul | cipr);
#endif

   // clear the pending edma interrupts by writing back the register contents
   REG_WR (EDMA_CIPRL_ADDR, cipr);

   SWI_or (SWI_Dma, cipr);
   return;
}


