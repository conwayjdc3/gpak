/*
 * Copyright (c) 2004-2007, Adaptive Digital Technologies, Inc.
 *
 * File Name: GpakWav.c
 *
 * Description:
 *   This file contains G.PAK functions to support Wavefile playback
 *
 *
 * Version: 1.0
 *
 * Revision History:
 *   11/13/01 - Initial release.
 *   04/2005  - Added GSM EFR
 *
 */

#include <std.h>
#include <stdio.h>
#include <ctype.h>
//#include <hwi.h>
//#include <swi.h>
//#include <tsk.h>
//#include <sem.h>

// Application related header files.
#include "GpakDefs.h"
#include "GpakHpi.h"
#include "GpakErrs.h"
#include "GpakExts.h"
#include "sysconfig.h"

static void (* G711_Decode) (ADT_UInt8 *Input, ADT_Int16 *Output, ADT_Int16 Length);
extern void G711_ADT_muLawExpand(ADT_UInt8 *Input, ADT_Int16 *Output, ADT_Int16 Length);
extern void G711_ADT_aLawExpand(ADT_UInt8 *Input, ADT_Int16 *Output, ADT_Int16 Length);


extern IHeap_Handle waveHeap;
typedef struct waveDLStats_t {
    int sample_rate;
    int duplicate_id;
    int list_malloc;
    int buf_malloc;
    int id_not_found;
    int first_block;
    int already_loaded;
    int out_of_sequence;
    int invalid_length;
    int last_block;
    int num_opened;
    int num_downloaded;
}  waveDLStats_t;

waveDLStats_t dl_stats;


wavefileList_t *waveListHead = NULL;
ADT_UInt8 *waveHeapBase = 0;
ADT_UInt32 waveHeapLengthI8 = 0;
ADT_UInt32 waveHeapFreeI8 = 0;

void initWaveHeap () {
	Error_Block	    errorBlock;
    Memory_Stats    stats;

    memset(&dl_stats, 0, sizeof(waveDLStats_t));

    Memory_getStats(waveHeap, &stats);
    waveHeapLengthI8 = stats.totalFreeSize;

	/* Allocate the entire heap  */
	Error_init(&errorBlock);
    waveHeapBase = Memory_alloc(waveHeap,  waveHeapLengthI8, 4, &errorBlock);

    if (waveHeapBase == 0) {
        return;
    }
    waveHeapFreeI8 = waveHeapLengthI8; 
}

ADT_UInt8 *waveMalloc (ADT_UInt32 num_bytes)
{
    ADT_UInt8 *ptr =0;
    ADT_UInt32 offset;

    if (waveHeapBase && (num_bytes <= waveHeapFreeI8)) {
        offset = waveHeapLengthI8 - waveHeapFreeI8; 

        // add padding if necessary to align on 4-byte boundary
        while (offset & 3) {
            waveHeapFreeI8--;
            if (num_bytes > waveHeapFreeI8) return 0;
            offset = waveHeapLengthI8 - waveHeapFreeI8; 
        }
        ptr = &waveHeapBase[offset];
        waveHeapFreeI8 -= num_bytes;    
    }    

    return ptr;
}


int size_wave_list = sizeof(wavefileList_t);
#pragma CODE_SECTION (ProcNewWavefileMsg, "SLOW_PROG_SECT")
ADT_UInt16 ProcNewWavefileMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {

wavefileList_t *pWave, *pCurrent;
ADT_UInt16 playbackId, coding, sampleRate;
ADT_UInt32 dataLengthI8, currHeapFreeI8;

    // Prepare the reply message.
    pReply[0] |= ( MSG_NEW_WAVEFILE_REPLY << 8);

    playbackId = pCmd[1];
    dataLengthI8 = ((ADT_UInt32)pCmd[3]<<16) | (ADT_UInt32)pCmd[2];
    coding = pCmd[4];
    sampleRate = pCmd[5];

 
    if (sampleRate != TDMRate) {
        pReply[1] = (ADT_UInt16) (Lw_InvalidSampleRate & 0xFF);
        dl_stats.sample_rate++;
        goto newWaveExit;
    }

    // scan the list for a duplicate playbackId
    pCurrent = waveListHead; 
    while (pCurrent != NULL) {
         if (pCurrent->info.playbackID == playbackId) {
            pReply[1] = (ADT_UInt16) (Lw_DuplicateId & 0xFF);
            dl_stats.duplicate_id++;
            goto newWaveExit;
         }
         pCurrent = pCurrent->next;
    }
 
    // snapshot amount of heap memory in case malloc fails
    currHeapFreeI8 = waveHeapFreeI8;

    // Allocate the List node
    pWave = (wavefileList_t *)waveMalloc(sizeof(wavefileList_t));
    if (pWave == NULL) {
        pReply[1] = (ADT_UInt16) (Lw_NotEnoughStorage & 0xFF);
        dl_stats.list_malloc++;
        goto newWaveExit;
    }

    // Allocate the audio sample buffer
    pWave->info.startAddress = (void *)waveMalloc(dataLengthI8);
    if (pWave->info.startAddress == NULL) {
        // restore heap free value on malloc fail
        waveHeapFreeI8 = currHeapFreeI8;
        pReply[1] = (ADT_UInt16) (Lw_NotEnoughStorage & 0xFF);
        dl_stats.buf_malloc++;
        goto newWaveExit;
    }        

    pWave->info.coding = (GpakCodecs)coding; 
    pWave->info.playbackID = playbackId;
    pWave->info.sampleRate = sampleRate;
    pWave->info.dataLengthI8 = dataLengthI8;
    pWave->info.playbackIndex = 0;
    pWave->info.state = WAVEFILE_STATE_OPEN;
    pWave->info.originalHeapFree = currHeapFreeI8;
    pWave->next = NULL;

    // add new entry to end of list
    if (waveListHead == NULL) {
        waveListHead  = pWave;
    } else {
        pCurrent = waveListHead; 
        while (pCurrent->next != NULL) {
            pCurrent = pCurrent->next;
        }
        pCurrent->next = pWave;
    }
   
    pReply[1] = ( Lw_Success & 0xFF);
    dl_stats.num_opened++;

newWaveExit:
   return 2;
}

#pragma CODE_SECTION (ProcLoadWavefileMsg, "SLOW_PROG_SECT")
ADT_UInt16 ProcLoadWavefileMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {

wavefileList_t *pCurrent, *pPrevious;
ADT_UInt16 playbackId, blocklenI8, lastBlock;
ADT_UInt32 block;
ADT_UInt8 *Src, *Dst;


    // Prepare the reply message.
    pReply[0] |= ( MSG_LOAD_WAVEFILE_REPLY << 8);

    playbackId = pCmd[1];
    block = ((ADT_UInt32)pCmd[3]<<16) | (ADT_UInt32)pCmd[2];
    blocklenI8= pCmd[4];
    lastBlock = pCmd[5];

    // scan the list for playbackId
    pCurrent = waveListHead;
#ifdef TEST_WAVELOAD
    pReply[1] = 0;
    {
    int i;
        for (i=0; i<blocklenI8/2; i++) {
            if (pCmd[6+i] != (ADT_UInt16)i)
                pReply[1]++;
        }
    }
    pReply[2] = pCmd[2];
    pReply[3] = pCmd[3];
    return 4;
#else
    pPrevious = NULL;
    while (pCurrent != NULL) {
         if (pCurrent->info.playbackID == playbackId) {
            break;
         }
         pPrevious = pCurrent;
         pCurrent = pCurrent->next;
    }

    if (pCurrent == NULL) {
        pReply[1] = ( Lw_IdNotFound & 0xFF);
        dl_stats.id_not_found++;
        goto loadWaveExit1;
    }

    if (pCurrent->info.state == WAVEFILE_STATE_OPEN) {
        // this is the first block
        if (block != 1) {
            pReply[1] = ( Lw_OutOfSequence & 0xFF);
            dl_stats.first_block++;
            goto loadWaveExit;
        }

        pCurrent->info.state = WAVEFILE_STATE_LOADING;
        pCurrent->info.dataLoadCountI8 = 0;
        pCurrent->info.prevLoadBlock = 0;
    } 

    if (pCurrent->info.state != WAVEFILE_STATE_LOADING) {
        pReply[1] = ( Lw_AlreadyLoaded & 0xFF);
        dl_stats.already_loaded++;
        goto loadWaveExit1;
    }

    if ((block - pCurrent->info.prevLoadBlock) != 1) {
        pReply[1] = ( Lw_OutOfSequence & 0xFF);
        dl_stats.out_of_sequence++;
        goto loadWaveExit;
    }
    pCurrent->info.prevLoadBlock = block;

    if ((pCurrent->info.dataLoadCountI8 + blocklenI8) > pCurrent->info.dataLengthI8) {
        pReply[1] = ( Lw_InvalidLength & 0xFF);
        dl_stats.invalid_length++;
        goto loadWaveExit;
    }
    

    if ((pCurrent->info.dataLoadCountI8+blocklenI8) == pCurrent->info.dataLengthI8) {
        if (lastBlock) {
            pCurrent->info.state = WAVEFILE_STATE_READY;
        } else {
            pReply[1] = ( Lw_LastBlockMissing & 0xFF);
            dl_stats.last_block++;
            goto loadWaveExit;
        }
    }       

    Src = (ADT_UInt8 *)&pCmd[6];
    Dst = (ADT_UInt8 *)pCurrent->info.startAddress;
    Dst += pCurrent->info.dataLoadCountI8;
    memcpy(Dst, Src, blocklenI8);
    pCurrent->info.dataLoadCountI8 += blocklenI8;

#endif
    pReply[1] = ( Lw_Success & 0xFF);
    pReply[2] = pCmd[2];
    pReply[3] = pCmd[3];
    dl_stats.num_downloaded++;

loadWaveExit1:
   return 4;

    // The download failed, release the memory and unlink this instance from list
loadWaveExit:
    waveHeapFreeI8 = pCurrent->info.originalHeapFree;
    if (pPrevious != NULL)
        pPrevious->next = NULL;
    else
        waveListHead = NULL;
    pReply[2] = pCmd[2];
    pReply[3] = pCmd[3];
   return 4;


}
#pragma CODE_SECTION (ProcPlayWavefileMsg, "SLOW_PROG_SECT")
GPAK_PlayWavefileStat_t ProcPlayWavefileMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {
    ADT_UInt16 ChannelId;        
//    ADT_UInt16 CoreID;        
    WavePlaybackCmd Command;
    GpakDeviceSide_t Direction;
    wavefileList_t   *pCurrent;
    ADT_UInt16       PlaybackId;
    chanInfo_t       *chan;   
    wavefileInfo_t   *pInfo, wavespec;

   // Get the Core id, Channel Id and Channel Type from the command message.
//   CoreID = pCmd[0] & 0xFF;
   ChannelId = (pCmd[1] >> 8) & 0xFF;
   Command = (WavePlaybackCmd) (pCmd[1] & 0xFF);
   PlaybackId = pCmd[2];
   Direction = (GpakDeviceSide_t )pCmd[3];

   pReply[0] |= (MSG_PLAY_WAVEFILE_REPLY << 8);
   pReply[1] = (ChannelId << 8);

      // Verify the Channel Id is valid and the channel is active.
   if (sysConfig.maxNumChannels <= ChannelId)
      return Pw_InvalidChannel;

   chan = chanTable[ChannelId];
   if (chan->channelType == inactive)
      return Pw_ChannelInactive;

    // scan the list for playbackId
    pCurrent = waveListHead;
    while (pCurrent != NULL) {
         if (pCurrent->info.playbackID == PlaybackId) {
            break;
         }
         pCurrent = pCurrent->next;
    }

    if (pCurrent == NULL) 
        return Pw_IdNotFound;
    wavespec = pCurrent->info;


    // Verify the file has been downloaded
    if (wavespec.state != WAVEFILE_STATE_READY) 
        return Pw_FileNotDownloaded;

    if ((Direction == ADevice) || (Direction == NetToDSP))
        pInfo = &(chan->pktToPcm.waveInfo);
    else if ((Direction == BDevice) || (Direction == DSPToNet))
        pInfo = &(chan->pcmToPkt.waveInfo);
    else
        return Pw_InvalidDirection;

    // Update Channel's wavefile state per Command
    if (Command == WavePlaybackStop) {
        if (pInfo->playbackID != PlaybackId)
            return Pw_IdNotFound;
        pInfo->state = 0;
    }
    else if ((Command == WavePlaybackOnce) || (Command == WavePlaybackLoop)) {
        if ((pInfo->state == WAVEFILE_STATE_PLAY_ONCE) || (pInfo->state == WAVEFILE_STATE_PLAY_LOOP))
            return Pw_PlaybackActive;
        wavespec.playbackID = PlaybackId;
        if (wavespec.sampleRate != chan->ProcessRate)
            return Pw_InvalidSampleRate; 
        *pInfo = wavespec;
        pInfo->playbackIndex = 0;
        if (Command == WavePlaybackOnce)
            pInfo->state =  WAVEFILE_STATE_PLAY_ONCE;                
        else
            pInfo->state =  WAVEFILE_STATE_PLAY_LOOP;                
    }
    else {
        return Pw_InvalidCommand;
    }

    return Pw_Success;
}

void waveFilePlayback (wavefileInfo_t *pInfo, ADT_Int16 *buff, ADT_UInt32 sampsPerFrame) {
ADT_UInt16 state;
ADT_Int16 *pSrc16;
ADT_UInt8 *pSrc8;
ADT_UInt32 len, i, numLeft, copyLen;

    state = pInfo->state;

    if ((state != WAVEFILE_STATE_PLAY_ONCE) && 
        (state != WAVEFILE_STATE_PLAY_LOOP))
        return;

    if (pInfo->coding == L16) {
        pSrc16 = (ADT_Int16 *)pInfo->startAddress;
        pSrc16 += pInfo->playbackIndex;
        len = pInfo->dataLengthI8/2;
        if ((pInfo->playbackIndex + sampsPerFrame) <= len) {
            for (i=0; i<sampsPerFrame; i++)
                *buff++ = *pSrc16++;
            pInfo->playbackIndex += sampsPerFrame;
            if (len-pInfo->playbackIndex == 0) {
                pInfo->playbackIndex = 0;
                if (state == WAVEFILE_STATE_PLAY_ONCE)
                    state = 0;
            } 
        } else {
            copyLen = len-pInfo->playbackIndex;
            numLeft = sampsPerFrame - copyLen;
            for (i=0; i<copyLen; i++)
                *buff++ = *pSrc16++;
            pInfo->playbackIndex = 0;
            if (state == WAVEFILE_STATE_PLAY_ONCE) {
                for (i=0; i<numLeft; i++)
                    *buff++ = 0;
                state = 0;
            } else {
                pSrc16 = (ADT_Int16 *)pInfo->startAddress;
                for (i=0; i<numLeft; i++)
                    *buff++ = *pSrc16++;
                pInfo->playbackIndex += numLeft;
            }
        }
    } else  {      
        pSrc8 = (ADT_UInt8 *)pInfo->startAddress;
        pSrc8 += pInfo->playbackIndex;
        len = pInfo->dataLengthI8;
        if (pInfo->coding ==  PCMU_64) 
            G711_Decode = G711_ADT_muLawExpand;
        else  
            G711_Decode = G711_ADT_aLawExpand;

        if ((pInfo->playbackIndex + sampsPerFrame) <= len) {
            (*G711_Decode) (pSrc8, buff, (ADT_Int16)sampsPerFrame);
            pInfo->playbackIndex += sampsPerFrame;
            if (len-pInfo->playbackIndex == 0) {
                pInfo->playbackIndex = 0;
                if (state == WAVEFILE_STATE_PLAY_ONCE)
                    state = 0;
            } 
        } else {
            copyLen = len-pInfo->playbackIndex;
            numLeft = sampsPerFrame - copyLen;
            (*G711_Decode) (pSrc8, buff, (ADT_Int16)copyLen);
            pInfo->playbackIndex = 0;
            if (state == WAVEFILE_STATE_PLAY_ONCE) {
                buff += copyLen;
                for (i=0; i<numLeft; i++)
                    *buff++ = 0;
                state = 0;
            } else {
                pSrc8 = (ADT_UInt8 *)pInfo->startAddress;
                (*G711_Decode) (pSrc8, &buff[copyLen], (ADT_Int16)numLeft);
                pInfo->playbackIndex += numLeft;
            }
        }

    }
    pInfo->state = state;
} 
