#include <std.h>
#include <clk.h>
#include <stdio.h>

#include "adt_typedef.h"
#include "psp_i2c.h"
#include "C:/Temp/evm_aic33.h"

#define I2C_WRITE_CMD 0x322


void *PSP_i2cOpen (ADT_UInt32 inst, void *callback, void *appHandle);
int   PSP_i2cTransfer (void *handle,        ADT_UInt32 slaveAddr,
                       ADT_UInt8 *buffer,   ADT_UInt32 bufLen,
                       ADT_UInt32 cmd,      ADT_Int32  timeout,
                       ADT_UInt32 *xferCnt, void *param);
void  PSP_i2cClose (void *handle);




//  Routines necessary to satisfy I2C driver
int PAL_osSemCreate (const char *name, ADT_Int32 initVal,
                     const void *attrs, int *hSem) {
    *hSem = 1;
    return 0;
}


int PAL_osSemGive (int *hSem) {
    return 0;
}

int PAL_osSemTake (int *hSem, ADT_Int32 timeOut) {
    return 0;
}

int PAL_osSemDelete (int *hSem) {
    return 0;
}


int PAL_osWaitMsecs (ADT_UInt32 mSecs) {
   unsigned long start;

   start = CLK_gethtime ();
   while ((CLK_gethtime () - start) < 100 * CLK_countspms());
   return 0;
}

void PAL_osProtectEntry (int level, void* imask) { }
void PAL_osProtectExit  (int level, void* imask) { }

ADT_UInt32 PAL_osGetCurrentTick(void) {
    return PRD_getticks();
}

ADT_Bool PAL_osCheckTimeOut (ADT_UInt32 startValue, ADT_UInt32 timeout) {

    return (PAL_osGetCurrentTick() - startValue < timeout);
}


int PAL_osMemSet (void *memAddr, ADT_UInt8 fillVal, ADT_UInt32 numBytes) {
   memset (memAddr, fillVal, numBytes);\
   return 0;
}


static int  i2c_setup (void);
static int  i2c_write (ADT_UInt16 slaveaddr, ADT_UInt8* data, ADT_UInt32 len);
static void i2c_close (void);


static void *i2cHandle = NULL;

#define I2C_OWN_ADDR                (0x10u)
#define I2C_NUM_BITS                (8u)
#define I2C_BUS_FREQ                (200000u)
#define I2C_INPUT_BUS_FREQ          (891000000/6)

const PSP_I2cConfig I2C_devParams =   {
    PSP_OPMODE_POLLED,
    I2C_OWN_ADDR,
    I2C_NUM_BITS,
    I2C_BUS_FREQ,
    I2C_INPUT_BUS_FREQ,
    FALSE,                // 7bit/10bit Addressing mode
    FALSE                 // Digital Loob Back (DLB) mode enabled 
};

void AIC33_open () {
   static EVM_AIC33_Setup  aic33setup = EVM_AIC33_16BITS_DSP_8k;
   Uint8 cmd[2];
   int i, stat;


    PSP_i2cCreate (0, &I2C_devParams);
   
    if (!i2c_setup ()) {
       printf ("I2c initialization failure\r\n");
       return;
    }

   // Select page 0
   cmd[0] = cmd[1] = 0; 
   stat = i2c_write (I2C_AIC33_U3_SLAVE_ADDR, cmd, 2); 
   if (stat != 0)  printf ("I2C Write failure %d\r\n", stat); 
      
   // Reset AIC
   cmd[0] = 1;
   cmd[1] = 0x80;
   stat = i2c_write (I2C_AIC33_U3_SLAVE_ADDR, cmd, 2); 
   if (stat != 0)  printf ("I2C Write failure %d\r\n", stat); 

   PAL_osWaitMsecs (100);

   for (i=0; i< EVM_AIC33_NREGS; i++) {
      if (aic33setup.regs [i][0] == EVM_AIC33_ENDSETUP) break;
      if (aic33setup.regs [i][0] != 0) break;
      stat = i2c_write (I2C_AIC33_U3_SLAVE_ADDR, &aic33setup.regs[i][1], 2);
      if (stat == 0)
         continue;

      printf ("I2C Write failure %d\r\n", stat); 
      break;
   }

   // Set AIC as master
   cmd[0] = 8;
   cmd[1] = 0xC0;
   i2c_write (I2C_AIC33_U3_SLAVE_ADDR, cmd, 2); 
   
   i2c_close();

   return;
}


static int i2c_setup (void) {

    if (NULL != i2cHandle) return TRUE;;

    i2cHandle = PSP_i2cOpen (0, NULL, NULL);

    return (NULL != i2cHandle);
}


static int i2c_write (ADT_UInt16 slaveaddr, ADT_UInt8 *data, ADT_UInt32 len) {

   ADT_UInt32 xferCnt;

   PAL_osWaitMsecs (5);

   return PSP_i2cTransfer (i2cHandle, slaveaddr, data, len,
                           I2C_WRITE_CMD, -1, &xferCnt, NULL);

}


static void i2c_close() {
   if (NULL == i2cHandle) return;
   PSP_i2cClose (i2cHandle);
   i2cHandle    = NULL;
}

