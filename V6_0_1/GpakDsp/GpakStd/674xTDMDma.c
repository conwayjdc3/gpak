/*
 * Copyright (c) 2008, Adaptive Digital Technologies, Inc.
 *
 * File Name: 674xTDMDma.c
 *
 * Description:
 *    This file contains G.PAK DMA functions to transfer between TDMs and memory.
 *
 * Version: 1.0
 *
 * Revision History:
 *   01/23/2007 - Initial release.
 *
 *
 */

/*
   The DMA configuration details for each TDM   port are defined below. 

                  Parameter block assignments
   
    Device           McASP    McBSP
                   Tx0  Rx0   
        Start Chn    2    3
    DMA Ping Lnk    64   66
    DMA Pong Lnk    65   67

    TCC Event        1    2
    Transfer queue   2    2
    Queue priority   0    0


    Shadow region 0 (interrupt signal 35) used to access the EDMA registers

    TDM DMA completion HW IRQ  8  (for ALL DMA channels)

                   ---------------------------
                   
   The DMA is setup to transfer 1ms of data between the TDM and a DMA buffer. 
   Two linear buffers (ping and pong) are used so that the DMA software interrupt 
   can work with one set of buffers (e.g. ping) during the same 1 ms interval that
   the DMA is transfering data between the TDMs and the other set of buffers (e.g. pong).

   The channel/frame (2 bytes/element) data is placed into the DSP's ping and pong buffers 
   as shown below:

 
      C1F1   C1F2   C1F3   C1F4   C1F5   C1F6   C1F7   C1F8
      C2F1   C2F2   C2F3   C2F4   C2F5   C2F6   C2F7   C2F8
      C3F1   C3F2   C3F3   C3F4   C3F5   C3F6   C3F7   C3F8
      C4F1   C4F2   C4F3   C4F4   C4F5   C4F6   C4F7   C4F8
      C5F1   C5F2   C5F3   C5F4   C5F5   C5F6   C5F7   C5F8
                                . 
                                . 
                                . 
      CNF1   CNF2   CNF3   CNF4   CNF5   CNF6   CNF7   CNF8
 

  The order of the data arriving at TDM is as follows:

       C1F1 C2F1 C3F1 ... CNF1 | C1F2 C2F2 C3F2 ... CNF2 | C1F3 ... CNF8

  Where the | character symbolizes the beginning of a new frame of N channels.
   
  After 8 frames (1ms) of data, the DMA automatically switches between the ping and pong
  buffers and generates a hardware interrupt so that the CPU can begin processing the next
  1ms of data.
  
  
   Each buffer consists of 2 bytes per element, N channels per frame and 8 frames.
   The first element of the pong buffer is located immediately after the last element 
   in the ping buffer.  
   
   NOTE:  Separating the ping and pong buffers (as opposed to interleaving the 
          buffers as in earlier versions) reduces the need for L1 cache updates
          since the CPU and DMA will always be accessing different cache lines.


                   -------------------------
                   
   DMA transfers are linked together, using the DMA parameter blocks, such that 
   at the completion of a ping buffer transfer, the pong buffer transfer is 
   automatically started and vice versa.
   
   The CPU is notified that a transfer is complete via a common interrupt.
   The interrupt handler can determine which transfer(s) is complete by reading
   and clearing the CIPR register

                   -------------------------

   INTERFACES

   void DMAInitialize ()
   ADT_Events enableDMA  (port) 
   ADT_Events disableDMA (port)

   ADT_Bool DmaLostTxSync (int port, ADT_PCM16 *TxSwi);
   ADT_Bool DmaLostRxSync (int port, ADT_PCM16 *RxSwi);

   void CopyDmaTxBuffers (port, CircBufInfo_t **pCircTxBase,  ADT_UInt16 *pDmaTxBufr);
   void CopyDmaRxBuffers (port, ADT_UInt16 *pDmaRxBufr, CircBufInfo_t **pCircRxBase);

                      -------------------------

     DEBUGGING AIDES
     
   TxPatternSlot1    = (non-zer0) Pattern placed only in first slot.
   TxPatternAllSlots = (non-zero) Pattern placed in all slots.  TxPatternSlot1 takes precedence on first slot.

   RxPatternSlot1    = (non-zer0) Pattern placed only in first slot.
   RxPatternAllSlots = (non-zer0) Pattern placed in all slots.  RxPatternSlot1 takes precedence on first slot.

   FrameStamp = (true) 1st byte of each frame contains current frame count   
*/

// Application related header files.
#include <std.h>
#include <hwi.h>
#include <exc.h>
#include <c64.h>
#include <string.h>
#include "adt_typedef.h"
#include "GpakPcm.h"

void PowerOnDevice (int DeviceCode, int Group);

#define SAMPLE_I8 2

#ifdef STANDALONE

// Test without G.PAK structures
#undef  _DEBUG

#define SAMPLES_PER_MS  16
#define MAX_SLOTS       2
#define SAMPLE_I8       2
#define DMA_BUFFER_I8  (SAMPLES_PER_MS * MAX_SLOTS * SAMPLE_I8)
struct syscfg {
   int maxSlotsSupported[NUM_TDM_PORTS];
   int samplesPerMs;
} sysConfig = {
  { MAX_SLOTS, MAX_SLOTS, MAX_SLOTS },
  SAMPLES_PER_MS
};
ADT_PCM16 BSP0DMA_TxBuffer[DMA_BUFFER_I8], BSP1DMA_TxBuffer[DMA_BUFFER_I8], BSP2DMA_TxBuffer[DMA_BUFFER_I8];
ADT_PCM16 BSP0DMA_RxBuffer[DMA_BUFFER_I8], BSP1DMA_RxBuffer[DMA_BUFFER_I8], BSP2DMA_RxBuffer[DMA_BUFFER_I8];
#pragma DATA_SECTION (BSP0DMA_TxBuffer, "EXT")
#pragma DATA_SECTION (BSP1DMA_TxBuffer, "EXT")
#pragma DATA_SECTION (BSP2DMA_TxBuffer, "EXT")

#pragma DATA_SECTION (BSP0DMA_RxBuffer, "EXT")
#pragma DATA_SECTION (BSP1DMA_RxBuffer, "EXT")
#pragma DATA_SECTION (BSP2DMA_RxBuffer, "EXT")

ADT_UInt16 SltsPerFrame[NUM_TDM_PORTS];   // Total slots on TDM frame [fixed at build]
ADT_UInt16 MaxDmaSlots [NUM_TDM_PORTS];   // Max slots available for DMA [fixed at build]. Used by DMA to perform DMA to TDM transfers
ADT_UInt16 DmaSlotCnt  [NUM_TDM_PORTS];   // Set by StartGpakPcm. Used by GpakPcm to perform DMA to circular transfers

#define customTDMOut(a,b,c) 
#define customTDMIn(a,b,c) 

#else
#include "GpakDefs.h"
#include "GpakExts.h"
#include "sysmem.h"
#include "sysconfig.h"
#include "GpakPcm.h"

extern  void InitTDMMode ();
extern int customTDMOut (ADT_PCM16 *dataOut, int port, int slotCnt);
extern int customTDMIn  (ADT_PCM16 *dataIn,  int port, int slotCnt);

#endif

EDMA_CC_t *CC0 = (EDMA_CC_t *) 0x1C00000;

TC_t *TC0 = (TC_t *) 0x1C08000;
TC_t *TC1 = (TC_t *) 0x1C08400;

#define DMA_IDLE   TRUE
#define DMA_ACTIVE FALSE

//--------------------------------------------------------------------------------
//
//   TEST_SWCOMPAND = Testing S/W compand
#ifdef TEST_SWCOMPAND // convert linear sampled to compressed data to test sw comapnding
   #define muLawExpand(inout,cnt)    G711_ADT_muLawExpand (inout,inout,cnt)
   #define muLawCompress(inout,cnt)  G711_ADT_muLawCompress(inout,inout,cnt)
#else 
   #define muLawExpand(inout, cnt)
   #define muLawCompress(inout,cnt)
#endif


struct errCnts {
   ADT_UInt32 cnt;

   ADT_UInt32 MissedEvents;
   ADT_UInt32 SecondaryEvents;
   ADT_UInt32 WaterMarkErr;

   ADT_UInt32 TC0Stat;
   ADT_UInt32 TC1Stat;
   ADT_UInt32 TC0Error;
   ADT_UInt32 TC1Error;

   ADT_UInt32 TDMErrors;

   ADT_UInt32 IntrStat;
   ADT_UInt32 InternalExc;
} err = { 0 };

extern __cregister volatile unsigned int IERR;

static const ADT_UInt32 dmaDrain[16];
static const ADT_UInt32 dmaSink[16] = {0, 0, 0, 0, 0, 0, 0, 0,
                                 0, 0, 0, 0, 0, 0, 0, 0 };

// ASPDMACfg specifies the physical DMA resources used by each logical port
// It MUST be initialized from customDSPInit during startup.
PortAddr ASPDMACfg [NUM_TDM_PORTS];
PortAddr DMACfg [NUM_TDM_PORTS];
#pragma DATA_SECTION (DMACfg,    "FAST_DATA_SECT:TDM")


static ADT_PCM16 * const RxDMABuffers[] = {
   BSP0DMA_RxBuffer, BSP1DMA_RxBuffer, BSP2DMA_RxBuffer
};

static ADT_PCM16 * const TxDMABuffers[] = {
   BSP0DMA_TxBuffer, BSP1DMA_TxBuffer, BSP2DMA_TxBuffer
};

//---------------------------------------
// Offset (bytes) from ping buffer to pong buffer
static ADT_UInt16 DmaPongOffset [NUM_TDM_PORTS] = { 0, 0, 0 };  
#pragma DATA_SECTION (DmaPongOffset,    "FAST_DATA_SECT:TDM")
static ADT_UInt16 TxDmaPongOffset [NUM_TDM_PORTS] = { 0, 0, 0 };  
#pragma DATA_SECTION (TxDmaPongOffset,    "FAST_DATA_SECT:TDM")

// -------------------------------------------
//  DMA event bits to port assignments for PCM thread to determine port and direction of event
//
ADT_Events RxEvents [NUM_TDM_PORTS] = { 0, 0, 0 };
ADT_Events TxEvents [NUM_TDM_PORTS] = { 0, 0, 0 };
#pragma DATA_SECTION (RxEvents,    "FAST_DATA_SECT:TDM")
#pragma DATA_SECTION (TxEvents,    "FAST_DATA_SECT:TDM")

//---------------------------------------------------------
//   Runtime structures
//
ADT_PCM16 *DmaRxBuff   [NUM_TDM_PORTS];   // DMA Rcv buffer address for McBSP
ADT_PCM16 *DmaTxBuff   [NUM_TDM_PORTS];   // DMA Xmt buffer address for McBSP
ADT_UInt16 DmaTxOffset [NUM_TDM_PORTS];   // current Tx DMA buffer offset (ping or pong) for McBSP
ADT_UInt16 DmaRxOffset [NUM_TDM_PORTS];   // current Rx DMA buffer offset (ping or pong) for McBSP
#pragma DATA_SECTION (DmaRxBuff,    "FAST_DATA_SECT:TDM")
#pragma DATA_SECTION (DmaTxBuff,    "FAST_DATA_SECT:TDM")
#pragma DATA_SECTION (DmaRxOffset,  "FAST_DATA_SECT:TDM")
#pragma DATA_SECTION (DmaTxOffset,  "FAST_DATA_SECT:TDM")


static void GpakDmaIsr (void);
static void GpakErrIsr (void);
static void DMAClearErrors (void);

static void initRxTCCParams (int port, ADT_Bool idle);
static void initTxTCCParams (int port, ADT_Bool idle);

extern ADT_UInt32 enableDMA  (int port);
extern ADT_UInt32 disableDMA (int port);
extern ADT_UInt32 clearTDMErrors (int port);

//  Debugging aides
ADT_Bool  FrameStamp = FALSE;         // Time stamp is to be placed on every 8th sample
struct dmaDebug {
   ADT_Bool   firstTime;
   ADT_UInt16 TxPatternSlot1;       // Debug pattern to be placed on Tx timeslot 1
   ADT_UInt16 TxPatternAllSlots;    // Debug pattern to be placed on all Tx timeslots
   ADT_UInt16 RxPatternSlot1;       // Debug pattern to be placed on Rx timeslot 1
   ADT_UInt16 RxPatternAllSlots;    // Debug pattern to be placed on all Rx timeslots

   ADT_UInt16 TxSineSlot;           // Slot to transmit sine pattern
   ADT_UInt16 BroadcastPort;
   ADT_UInt16 BroadcastSlot;
   ADT_UInt16 captureTx;
   ADT_UInt16 captureRx;
} dma = { TRUE, 0, 0, 0, 0,
          0xffff, 0, 0xffff, 0xffff, 0xffff };

int isrCnt=0;
int isrFlags=0;

#ifndef _DEBUG
//--------------------------------------------------------------------------------
//
//   Debug = Enable fixed pattern testing and out of sync buffers
   ADT_UInt16 TogglePCMInSignal (int port)  { return 0xff; }
   ADT_UInt16 TogglePCMOutSignal (int port) { return 0xff; }
   #define CheckTxPointers(circ,dma) 
   #define CheckRxPointers(circ,dma) 
   #define FixedPatternTx(TxDmaBufr) 
   #define FixedPatternRx(RxDmaBufr) 
#else

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ToggleTestSignal
//
// FUNCTION
//   Toggles receive port between DMA buffer (normal) and constant tone (test)
//
// RETURNS
//  nothing
//
ADT_PCM16 const SineTableM12[] = {
  -3, 2828, 4000, 2828, -2, -2828, -4000, -2828,
  -1, 2829, 4000, 2828,  0, -2828, -4000, -2828,
};
static ADT_PCM16 Drain [MAX_SAMPLES_PER_MS*2][MAX_CHANS_PER_FRAME];     // Dummy receive buffer
#pragma DATA_SECTION (Drain, "FAST_DATA_SECT:TDM")
#pragma DATA_ALIGN   (Drain, CACHE_L2_LINE_SIZE)

static ADT_PCM16 Dummy [MAX_SAMPLES_PER_MS*2][MAX_CHANS_PER_FRAME];     // Dummy transmit buffer
#pragma DATA_SECTION (Dummy, "FAST_DATA_SECT:TDM")
#pragma DATA_ALIGN   (Dummy, CACHE_L2_LINE_SIZE)

static ADT_PCM16 BroadcastBufr [MAX_SAMPLES_PER_MS];
#pragma DATA_SECTION (BroadcastBufr, "FAST_DATA_SECT:TDM")
#pragma DATA_ALIGN   (BroadcastBufr,  CACHE_L2_LINE_SIZE)

ADT_UInt16 inCaptureBuff [900];
CircBufInfo_t captureTDMIn = {
  inCaptureBuff, sizeof (inCaptureBuff)/2, 0, 0, 0 
};
#pragma DATA_SECTION (inCaptureBuff,  "FAST_DATA_SECT:TDM")
#pragma DATA_ALIGN   (inCaptureBuff,  CACHE_L2_LINE_SIZE)

ADT_UInt16 outCaptureBuff [900];
CircBufInfo_t captureTDMOut = {
  outCaptureBuff, sizeof (outCaptureBuff)/2, 0, 0, 0
};
#pragma DATA_SECTION (outCaptureBuff, "FAST_DATA_SECT:TDM")
#pragma DATA_ALIGN   (outCaptureBuff,  CACHE_L2_LINE_SIZE)


static void sineBuffer (unsigned int port, ADT_PCM16 *Buffer) {
   int i, dmaSltCnt;

   // Populate dummy buffer with 1 kHz SINE wave on all channels
   if (NUM_TDM_PORTS <= port) return;

   dmaSltCnt = sysConfig.maxSlotsSupported[port];

   for (i=0; i<dmaSltCnt; i++) {
      memcpy (Buffer, SineTableM12, sizeof (SineTableM12));
      Buffer += (sysConfig.samplesPerMs * 2 * (SAMPLE_I8 / sizeof (ADT_UInt16)));
   }

}

int TogglePCMInSignal (unsigned int port) {

   if (NUM_TDM_PORTS <= port) return 0;

   sineBuffer (port, (ADT_PCM16 *) &Dummy);

   if (DmaRxBuff[port] == RxDMABuffers[port]) {
       DmaRxBuff[port] = (ADT_PCM16 *) &Dummy;
       return 1;
   }
   // Restore
   DmaRxBuff[port] = RxDMABuffers[port];
   return 0;
}

int TogglePCMOutSignal (unsigned int port) {

   if (NUM_TDM_PORTS <= port) return 0;

   if (DmaTxBuff[port] == TxDMABuffers[port]) {
       sineBuffer (port, TxDMABuffers[port]);
       DmaTxBuff[port] = (ADT_PCM16 *) &Drain;
       return 1;
   }
   // Restore with initial zeroes
   DmaTxBuff[port] = TxDMABuffers[port];
   memset (TxDMABuffers[port], 0, sysConfig.dmaBufferSize[port] * 2 * (SAMPLE_I8 / sizeof (ADT_UInt16)));
   return 0;
}

static void FixedPatternTx (ADT_PCM16 *TxDmaBufr) {
   ADT_PCM16 *DmaBufr;
   int i;

   if (dma.TxPatternSlot1) {
      DmaBufr = TxDmaBufr;
      for (i=0; i<sysConfig.samplesPerMs; i++) *DmaBufr++ = dma.TxPatternSlot1;
   }

   if (dma.TxSineSlot == 0) {
      memcpy (TxDmaBufr, SineTableM12, sysConfig.samplesPerMs * 2);
   }
   if (dma.captureTx == 0 && dma.captureRx != 0xffff) {
      copyLinearToCirc (&ApiBlock.DmaSwiCnt, &captureTDMOut, 1);
      copyLinearToCirc (TxDmaBufr, &captureTDMOut, sysConfig.samplesPerMs);
      captureTDMOut.TakeIndex = captureTDMOut.PutIndex;
   }

}
static void FixedPatternRx (ADT_PCM16 *RxDmaBufr) {
   ADT_PCM16 *DmaBufr;
   int i;

   // Overwrite channel(s) output with a fixed (non-zero) value
   if (dma.BroadcastSlot == 0)
      memcpy (BroadcastBufr, RxDmaBufr, sizeof (BroadcastBufr));
   else if (dma.BroadcastSlot != 0xffff)
      memcpy (RxDmaBufr, BroadcastBufr, sizeof (BroadcastBufr));

   if (dma.RxPatternSlot1) {
      DmaBufr = RxDmaBufr;
      for (i=0; i<sysConfig.samplesPerMs; i++) *DmaBufr++ = dma.RxPatternSlot1;
   }
   if (dma.captureRx == 0 && dma.captureTx != 0xffff) {
      copyLinearToCirc (&ApiBlock.DmaSwiCnt, &captureTDMIn, 1);
      copyLinearToCirc (RxDmaBufr, &captureTDMIn, sysConfig.samplesPerMs);
      captureTDMIn.TakeIndex = captureTDMIn.PutIndex;
   }
   if (FrameStamp) {
      *RxDmaBufr = (ADT_PCM16) ApiBlock.DmaSwiCnt;
   }     
}

inline void CheckTxPointers (CircBufInfo_t *circ, ADT_PCM16 *dma) {
   if (circ == pDrain) return;
   LogTransfer (0x100C1, circ, &circ->pBufrBase[circ->TakeIndex], 0, dma, sysConfig.samplesPerMs);
   AppErr ("TxPointerError", (sysConfig.samplesPerMs < circ->BufrSize) && 
                             (getAvailable (circ) < sysConfig.samplesPerMs));
}
inline void CheckRxPointers (CircBufInfo_t *circ, ADT_PCM16 *dma) {
   if (circ == pSink) return;
   LogTransfer (0x1001C, 0, dma, circ, &circ->pBufrBase[circ->PutIndex], sysConfig.samplesPerMs);
   AppErr ("RxPointerError", (sysConfig.samplesPerMs < circ->BufrSize) &&
                             (getFreeSpace (circ) < sysConfig.samplesPerMs));
}
#endif

#pragma CODE_SECTION (DMAClearErrors, "SLOW_PROG_SECT")
static void DMAClearErrors (void) {
   // Channel controller errors (missed events and watermark levels)
   REG_RD (EDMA_EMR,    err.MissedEvents);
   REG_WR (EDMA_EMCR,   err.MissedEvents);

   REG_RD (EDMA_CCERR,    err.WaterMarkErr);
   REG_WR (EDMA_CCERRCLR, err.WaterMarkErr);

   REG_RD (EDMA_SHADOW_SPR, err.SecondaryEvents);
   REG_WR (EDMA_SHADOW_SPCR, err.SecondaryEvents);

   // Transfer controller errors
   REG_RD (TC0_ERR, err.TC0Error);   // Error details
   REG_RD (TC1_ERR, err.TC1Error);

   REG_RD (TC0_ERR_STAT,     err.TC0Stat);
   REG_WR (TC0_ERR_STAT_CLR, err.TC0Stat);

   REG_RD (TC1_ERR_STAT,     err.TC1Stat);
   REG_WR (TC1_ERR_STAT_CLR, err.TC1Stat);


   // Exception status
   REG_RD (INTR_XSTAT,     err.IntrStat);
   REG_WR (INTR_XSTAT_CLR, err.IntrStat);

   // Generate error interrupt signals if pending
   REG_WR (TC0_EVAL, 1);
   REG_WR (TC1_EVAL, 1);
   REG_WR (EDMA_ERREVAL, 1);
   
   // Generate completion interrupt if pending
   REG_WR (EDMA_SHADOW_IEVL, 1);
   
   err.InternalExc = IERR;
   IERR = 0;

   if (clearTDMErrors (0)) {
      err.TDMErrors++;
   }
   return;
}


#pragma CODE_SECTION (DMAClearAll, "SLOW_PROG_SECT")
void DMAClearAll () {

   // Clear all DMA error conditions
   DMAClearErrors ();

   // Clear all indications of edma events and event interrupts
   REG_WR (EDMA_EMCR, 0xffffffff);        // Clear missed events
   REG_WR (EDMA_GLB_IPCR, 0xffffffff);    // Clear pending interrupts
   REG_WR (EDMA_GLB_EPCR, 0xffffffff);    // Clear pending events
   REG_WR (EDMA_GLB_SPCR, 0xffffffff);    // Clear pending secondarys

   // Disable edma events and transfer completion interrupts for port
   REG_WR (EDMA_GLB_IECR, 0xffffffff);    // Disable completion interrupts
   REG_WR (EDMA_GLB_EECR, 0xffffffff);    // Disable events

   // Remove events from shadow regions
   REG_WR (EDMA_SHAD0, 0);
   REG_WR (EDMA_SHAD1, 0);

   // Disable all dma access
   memset ((void *)EDMA_PARAM_ADDR, 0, PARAM_CNT * PARAM_I8);

}
 
//---------------------------------------------------
#pragma CODE_SECTION (PowerOnDMA, "SLOW_PROG_SECT")
static void PowerOnDMA () {

   int intrMask;

   intrMask = HWI_disable ();

   PowerOnDevice (PSC_EDMA_CC_0, PSC0);
   PowerOnDevice (PSC_EDMA_TC_0, PSC0);
   PowerOnDevice (PSC_EDMA_TC_1, PSC0);

   HWI_restore (intrMask);

}

//---------------------------------------------------
//{ DMAIntialize 
//
// FUNCTION
//   - Disables all DMA; initializes interrupt variables
//}
#pragma CODE_SECTION (DMAInitialize, "SLOW_PROG_SECT")
void DMAInitialize () {
   int port;

   InitTDMMode ();

   C64_disableIER (1 << HWI_TDM_COMPLETE);
   C64_disableIER (1 << HWI_TDM_ERR);
   if (dma.firstTime) {
      PowerOnDMA ();

      memset (&err, 0, sizeof (err));
      
      // Map TDM DMA interrupt completion signal to GpakDmaIsr
      HWI_dispatchPlug (HWI_TDM_COMPLETE, (Fxn) &GpakDmaIsr, -1, NULL);
      HWI_eventMap     (HWI_TDM_COMPLETE, TDM_CompleteSgnl);

      // Map TDM Error signals to GpakErrIsr
      HWI_dispatchPlug (HWI_TDM_ERR, (Fxn) &GpakErrIsr, -1, NULL);
      HWI_eventMap     (HWI_TDM_ERR, ErrSgnl);

      DMAClearAll ();
      dma.firstTime = FALSE;

      // Identify McASP port parameters to DMA driver
      for (port = 0; port < NUM_TDM_PORTS; port++) 
         memcpy (&DMACfg[port], &ASPDMACfg[port], sizeof (PortAddr));

   }

   // Set the multiplexed interrupt control for all DMA interrupts.
   for (port=0; port<NUM_TDM_PORTS; port++) {
      DmaRxBuff[port] = RxDMABuffers[port];
      DmaTxBuff[port] = TxDMABuffers[port];
      disableDMA (port);

   }


   // Clear pending exceptions
   DMAClearErrors ();

   // Assign TDM error interrupt signals as exceptions
   activateCombinedEvent (DMA_CompleteSgnl);
   activateCombinedEvent (TDM_CCErrSgnl);
   activateCombinedEvent (TDM_Que0ErrSgnl);   REG_WR (TC0_ERR_STAT_ENBL, 0xD);
   activateCombinedEvent (TDM_Que1ErrSgnl);   REG_WR (TC1_ERR_STAT_ENBL, 0xD);

   activateCombinedEvent (DroppedIntrSgnl);

   REG_WR (INTR_XSTAT_CLR, 1);     // Clear pending exceptions
   C64_enableIER (1 << HWI_TDM_COMPLETE);
   C64_enableIER (1 << HWI_TDM_ERR);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{enableDMA
//
// FUNCTION
//   Enables Tx and Rx DMA for specified McBSP
//
// RETURNS
//    Bit mask of events that are generated at interrupt
//}
#pragma CODE_SECTION (enableDMA, "SLOW_PROG_SECT")
ADT_UInt32 enableDMA (int port) {

   ADT_UInt32 chnBits, shadowBits;

   if (NUM_TDM_PORTS <= port) return 0;


   // Convert Tx and Rx DMA channels to their corresponding event register bits
   chnBits = 0;
   if(DmaSlotCnt[port])
       chnBits |= (1 << DMACfg[port].RxChn); 
   if(TxDmaSlotCnt[port])
       chnBits |= (1 << DMACfg[port].TxChn); 
 

   // Disable edma events and transfer completion interrupts for port
   REG_WR (EDMA_SHADOW_IECR, chnBits);    // Disable completion interrupts
   REG_WR (EDMA_SHADOW_EECR, chnBits);    // Disable events

   // Assign events to shadow region
   REG_RD (EDMA_SHAD_DMA, shadowBits);
   REG_WR (EDMA_SHAD_DMA, (shadowBits | chnBits));

   // Allow ISR to determine if event is transmit or receive
   if(DmaSlotCnt[port])
      RxEvents[port] = 1 << DMACfg[port].RxChn;
   if(TxDmaSlotCnt[port])
      TxEvents[port] = 1 << DMACfg[port].TxChn;

   DmaTxOffset[port] = 0;  // Point DMA copy to ping buffer
   DmaRxOffset[port] = 0; 
   DmaPongOffset[port] = sysConfig.samplesPerMs * DmaSlotCnt[port] * (SAMPLE_I8 / sizeof (ADT_UInt16));
   TxDmaPongOffset[port] = sysConfig.samplesPerMs * TxDmaSlotCnt[port] * (SAMPLE_I8 / sizeof (ADT_UInt16));

   // Assign TDM channels to the designated transfer controller
   assignToTCQueue (DMACfg[port].TxChn, EDMA_QUE_TDM_TX);
   assignToTCQueue (DMACfg[port].RxChn, EDMA_QUE_TDM_RX);

   // Setup DMA transfer parameters
   initRxTCCParams (port, DMA_ACTIVE);
   initTxTCCParams (port, DMA_ACTIVE);

   // Clear all indications of edma events and event interrupts
   REG_WR (EDMA_SHADOW_SPCR, chnBits);    // Clear secondary event notifications
   REG_WR (EDMA_EMCR, chnBits);           // Clear missed event notifications

   DMAClearErrors ();

   // Enable edma events and transfer completion interrupts for port
   REG_WR (EDMA_SHADOW_IESR, chnBits);    // Enable completion interrupts
   REG_WR (EDMA_SHADOW_EESR, chnBits);    // Enable secondary events

   // Setup DMA transfer parameters
   //initRxTCCParams (port, DMA_ACTIVE);
   //initTxTCCParams (port, DMA_ACTIVE);

   return chnBits;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{disableDMA
//
// FUNCTION
//   This function resets the EDMA controller
//
// RETURNS
//   
//}  Disabled transfer control bits 
#pragma CODE_SECTION (disableDMA, "SLOW_PROG_SECT")
ADT_UInt32 disableDMA (int port) {

   ADT_UInt32 chnBits, shadowBits;

   if (NUM_TDM_PORTS <= port) return 0;

 
   chnBits = (1 << DMACfg[port].RxChn) | (1 << DMACfg[port].TxChn);

   RxEvents[port] = 0;
   TxEvents[port] = 0;

   // disable all edma events and interrupts for port
   REG_WR (EDMA_SHADOW_IECR, chnBits);    // Disable event interrupts
   REG_WR (EDMA_SHADOW_EECR, chnBits);    // Disable event notifications

   // clear all indications of edma events and event interrupts
   REG_WR (EDMA_EMCR, chnBits);           // clear missed event notifications 
   REG_WR (EDMA_SHADOW_SPCR, chnBits);    // Clear secondary event notifications

   // Remove events from shadow region
   REG_RD (EDMA_SHAD_DMA, shadowBits);
   REG_WR (EDMA_SHAD_DMA, (~shadowBits & chnBits));

    // Disable DMA parameters   
   initRxTCCParams (port, DMA_IDLE);
   initTxTCCParams (port, DMA_IDLE);

   // Zero transmit buffer to avoid noise on line
#ifndef STANDALONE
   memset (TxDMABuffers[port], 0, sysConfig.dmaBufferSize[port] * 2); 
#endif
   return chnBits;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{initRxTCCParams - Initialize the EDMA parameter ram for receive channels
//
// FUNCTION
//  Initializes EDMA parameter ram for EDMA channels that are driven by receive events
//
// Inputs
//    port        - McBSPn
//
//  Outputs
//    EDMA_PARAM registers for port
//
// RETURNS
//  none
//}
#pragma CODE_SECTION (initRxTCCParams, "SLOW_PROG_SECT")
static void initRxTCCParams (int port, ADT_Bool idle) {

   ADT_UInt32 Acnt, Bcnt, Ccnt;   // Count of elements in A, B, and C dimensions
   ADT_UInt32 BOffset, COffset;   // Offset of to next element in B and C dimensions
   ADT_UInt32 slotsPerFrame, tcc;

   dma3Opts_t dmaOpts;
   ADT_UInt32 srcAddr, dstAddr;
   ADT_UInt32 BcntAcnt, BcntLink;

   ADT_UInt32 startOffset, pingOffset, pongOffset;  // Register offsets to parameter blocks
   ADT_UInt32 startAddr,   pingAddr,   pongAddr;    // Parameter block addresses

   if (NUM_TDM_PORTS <= port) return;

   // Get slotsPerFrame from configuration data
   slotsPerFrame = DmaSlotCnt[port];

   // Get event codes and transfer addresses from static tables
   tcc     = DMACfg[port].RxChn;
   srcAddr = DMACfg[port].RxAddr;
   dstAddr = (ADT_UInt32) RxDMABuffers[port];

   // Calculate offsets into parameter tables
   startOffset = DMACfg[port].RxChn  * PARAM_I8;
   pingOffset  = DMACfg[port].RxPing * PARAM_I8;
   pongOffset  = pingOffset + PARAM_I8;

   startAddr = EDMA_PARAM_ADDR + startOffset;
   pingAddr  = EDMA_PARAM_ADDR + pingOffset;
   pongAddr  = EDMA_PARAM_ADDR + pongOffset;

   //------------- Set up parameter table entries
   // Options register
   dmaOpts.TransferOptions = 0;
   dmaOpts.Bits.TCC        = tcc;   // transfer complete code
   dmaOpts.Bits.TCINTEN    = 1;     // interrupt upon transfer complete only

   // Number of elements in each dimension
   Acnt = SAMPLE_I8;               // A dimension is bytes within slot
   Bcnt = slotsPerFrame;           // B dimension is slots within frame
   Ccnt = sysConfig.samplesPerMs;  // C dimension is frame

   if (idle) {
      dstAddr = (ADT_UInt32) dmaDrain;
      Bcnt = 1;
      Ccnt = 1;
   }

   // Offset between C1F1 and C2F1 (see diagram at top)
   BOffset = (Acnt * Ccnt);

   // Offset between CNF1 and C1F2 (see diagram at top)
   COffset = Acnt - ((Bcnt-1) * BOffset);

   // Shift B and C offsets into destination field
   BOffset  = BOffset << 16;
   COffset  = COffset << 16;
   BcntAcnt = (Bcnt << 16) | Acnt;

   BcntLink = (Bcnt<<16) | (pongAddr & 0xffff);

   REG_WR ( startAddr,     dmaOpts.TransferOptions);
   REG_WR ((startAddr+4),  srcAddr);
   REG_WR ((startAddr+8),  BcntAcnt);
   REG_WR ((startAddr+12), dstAddr);
   REG_WR ((startAddr+16), BOffset);
   REG_WR ((startAddr+20), BcntLink);
   REG_WR ((startAddr+24), COffset);
   REG_WR ((startAddr+28), Ccnt);

   REG_WR ( pingAddr,     dmaOpts.TransferOptions);
   REG_WR ((pingAddr+4),  srcAddr);
   REG_WR ((pingAddr+8),  BcntAcnt);
   REG_WR ((pingAddr+12), dstAddr);
   REG_WR ((pingAddr+16), BOffset);
   REG_WR ((pingAddr+20), BcntLink);
   REG_WR ((pingAddr+24), COffset);
   REG_WR ((pingAddr+28), Ccnt);

   dstAddr += DmaPongOffset[port] * 2;      // Point to pong offset
   BcntLink = (Bcnt<<16) | (pingAddr & 0xffff);

   REG_WR ( pongAddr,     dmaOpts.TransferOptions);
   REG_WR ((pongAddr+4),  srcAddr);
   REG_WR ((pongAddr+8),  BcntAcnt);
   REG_WR ((pongAddr+12), dstAddr);
   REG_WR ((pongAddr+16), BOffset);
   REG_WR ((pongAddr+20), BcntLink);
   REG_WR ((pongAddr+24), COffset);
   REG_WR ((pongAddr+28), Ccnt);

   // Store address for DMA slippage checking
   DMACfg[port].RxBuf = (ADT_PCM16 **) (startAddr + 12);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{initTxTCCParams - Initialize the EDMA parameter ram for McBSP transmit channels
//
// FUNCTION
//  Initializes EDMA parameter ram for EDMA channels that are driven by transmit events
//
// Inputs
//    port        - McBSPn
//
//  Outputs
//    EDMA_PARAM registers for port
//
// RETURNS
//  none
//}
#pragma CODE_SECTION (initTxTCCParams, "SLOW_PROG_SECT")
static void initTxTCCParams (int port, ADT_Bool idle) {

   ADT_UInt32 Acnt, Bcnt, Ccnt;   // Count of elements in A, B, and C dimensions
   ADT_UInt32 BOffset, COffset;   // Offset of to next element in B and C dimensions
   ADT_UInt32 slotsPerFrame, tcc;
   
   dma3Opts_t dmaOpts;
   ADT_UInt32 srcAddr, dstAddr;
   ADT_UInt32 BcntAcnt, BcntLink;

   ADT_UInt32 startOffset, pingOffset, pongOffset;  // Register offsets to parameter blocks
   ADT_UInt32 startAddr,   pingAddr,   pongAddr;    // Parameter block addresses

   if (NUM_TDM_PORTS <= port) return;

   // Get slotsPerFrame from configuration data
   slotsPerFrame = TxDmaSlotCnt[port];

   // Get event codes and transfer addresses from static tables
   tcc     = DMACfg[port].TxChn;
   dstAddr = DMACfg[port].TxAddr;
   srcAddr = (ADT_UInt32) TxDMABuffers[port];

   // Calculate offsets into parameter tables
   startOffset = DMACfg[port].TxChn  * PARAM_I8;
   pingOffset  = DMACfg[port].TxPing * PARAM_I8;
   pongOffset  = pingOffset + PARAM_I8;

   startAddr = EDMA_PARAM_ADDR + startOffset;
   pingAddr  = EDMA_PARAM_ADDR + pingOffset;
   pongAddr  = EDMA_PARAM_ADDR + pongOffset;

   //------------- Set up parameter table entries
   // Options register
   dmaOpts.TransferOptions = 0;
   dmaOpts.Bits.TCC        = tcc;   // transfer complete code
   dmaOpts.Bits.TCINTEN    = 1;     // interrupt upon transfer complete only

   // Number of elements in each dimension
   Acnt = SAMPLE_I8;               // A dimension is bytes within slot
   Bcnt = slotsPerFrame;           // B dimension is slots within frame
   Ccnt = sysConfig.samplesPerMs;  // C dimension is frame
   if (idle) {
      srcAddr = (ADT_UInt32) dmaSink;
      Bcnt = 1;
      Ccnt = 1;
   }

   // Offset between C1F1 and C2F1 (see diagram at top)
   BOffset = (Acnt * Ccnt);

   // Offset between CNF1 and C1F2 (see diagram at top)
   COffset = Acnt - ((Bcnt-1) * BOffset);

   // Mask out B and C offsets froms source fields
   BOffset  = BOffset & 0xffff;
   COffset  = COffset & 0xffff;
   BcntAcnt = (Bcnt << 16) | Acnt;

   BcntLink = (Bcnt<<16) | (pongAddr & 0xffff);

   REG_WR ( startAddr,     dmaOpts.TransferOptions);
   REG_WR ((startAddr+4),  srcAddr);
   REG_WR ((startAddr+8),  BcntAcnt);
   REG_WR ((startAddr+12), dstAddr);
   REG_WR ((startAddr+16), BOffset);
   REG_WR ((startAddr+20), BcntLink);
   REG_WR ((startAddr+24), COffset);
   REG_WR ((startAddr+28), Ccnt);

   REG_WR ( pingAddr,     dmaOpts.TransferOptions);
   REG_WR ((pingAddr+4),  srcAddr);
   REG_WR ((pingAddr+8),  BcntAcnt);
   REG_WR ((pingAddr+12), dstAddr);
   REG_WR ((pingAddr+16), BOffset);
   REG_WR ((pingAddr+20), BcntLink);
   REG_WR ((pingAddr+24), COffset);
   REG_WR ((pingAddr+28), Ccnt);

   srcAddr += TxDmaPongOffset[port] * 2;    // Point to pong buffer
   BcntLink = (Bcnt<<16) | (pingAddr & 0xffff);

   REG_WR ( pongAddr,     dmaOpts.TransferOptions);
   REG_WR ((pongAddr+4),  srcAddr);
   REG_WR ((pongAddr+8),  BcntAcnt);
   REG_WR ((pongAddr+12), dstAddr);
   REG_WR ((pongAddr+16), BOffset);
   REG_WR ((pongAddr+20), BcntLink);
   REG_WR ((pongAddr+24), COffset);
   REG_WR ((pongAddr+28), Ccnt);

   // Store address for DMA slippage checking
   DMACfg[port].TxBuf = (ADT_PCM16 **) (startAddr + 4);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{zeroTxBuffer
//
// FUNCTION
//  Zeroes slot on Tx Buffer when channel goes inactive
//
// Inputs
//    port            - McBSPn
//    dmaSlot         - dma index of slot to zero
//
// RETURNS
//  none
//}
#pragma CODE_SECTION (zeroTxBuffer, "SLOW_PROG_SECT")
void zeroTxBuffer (int port, int dmaSlot) {
   ADT_PCM16 *txSlot;

   if (NUM_TDM_PORTS <= port) return;

   txSlot = TxDMABuffers[port];

   // Ping buffer fill memory
   txSlot += dmaSlot * sysConfig.samplesPerMs * (SAMPLE_I8 / sizeof (ADT_UInt16)); 
   memset (txSlot, 0, sysConfig.samplesPerMs * 2 * (SAMPLE_I8 / sizeof (ADT_UInt16)));

   // Pong buffer fill memory
   txSlot +=  TxDmaPongOffset [port]; 
   memset (txSlot, 0, sysConfig.samplesPerMs * 2 * (SAMPLE_I8 / sizeof (ADT_UInt16)));
}


#ifndef STANDALONE
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//  Switch copy between ping and pong buffers
inline switchBuffers (int port, ADT_UInt16 *offset) {
   if (*offset == 0) *offset = DmaPongOffset[port]; 
   else              *offset = 0;
   return;
}
inline switchTxBuffers (int port, ADT_UInt16 *offset) {
   if (*offset == 0) *offset = TxDmaPongOffset[port]; 
   else              *offset = 0;
   return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{DmaLostSync
//
// FUNCTION
//    checks that dma transfer and SWI copy are working in opposite buffers
//
// Inputs
//
//    DmaSlots    - Number of dma slot for port
//    DmsStart    - Start of DMA buffer
//    SWIOffset   - Offset of ping/pong bvffer used by SWI copy
//    DmaPos      - DMA register address pointing to current address of DMA transfer
//
// RETURNS
//    Lost sync indication
//}
static inline ADT_Bool DmaLostSync (int DmaSlots,         ADT_PCM16  *DmaStart,
                                    ADT_UInt16 SWIOffset, ADT_PCM16 **DmaPos) {
   ADT_PCM16 *DmaCur;
   ADT_Bool outOfRange, wrongBuffer;

   DmaCur = *DmaPos;

   // Verify within range of DMA buffer
   outOfRange = (DmaCur < DmaStart) || ((DmaStart + (DmaSlots * 2 * sysConfig.samplesPerMs * (SAMPLE_I8 / sizeof (ADT_UInt16)))) <= DmaCur);
 
   // Verify within range of current (ping/pong) buffer
   if (SWIOffset == 0) DmaStart += (DmaSlots * sysConfig.samplesPerMs * SAMPLE_I8 / sizeof (ADT_UInt16));
 
   wrongBuffer = (DmaCur < DmaStart) || ((DmaStart + (DmaSlots * sysConfig.samplesPerMs * SAMPLE_I8 / sizeof (ADT_UInt16))) <= DmaCur);
 
   return (outOfRange || wrongBuffer);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{CopyTxDmaBuffers - Copy channel PCM buffers to serial port DMA buffers.
//
// FUNCTION
//   Multiplexs PCM data from active channel PCM buffers onto a serial 
//   port's DMA buffers.
//
// Inputs
//     slotCnt       -  Count of TDM slots
//     TxCircBufrList -  List of transmit circular PCM buffer pointers
//     TxDmaBufr     -  Linear dma transmit buffer
//
// RETURNS
//     lostSync indication
//}
#pragma CODE_SECTION (CopyDmaTxBuffers, "FAST_PROG_SECT")
ADT_Bool CopyDmaTxBuffers (int port, int slotCnt, CircBufInfo_t **TxCircBufrList, int slip) {
   
   int k, buffCnt, lostSync;
   CircBufInfo_t *TxCirc, **TxCircBufr;
   ADT_PCM16     *TxDmaBufr;
   ADT_UInt16    *TxOffset;
   ADT_PCM16 *DmaBufr0;
   ADT_Word SlipSamps;

   TxOffset = &DmaTxOffset[port];

   // Adjust the SWI copy buffer offset (TxOffset) if the DMA and SWI buffers are conflicting
   lostSync = DmaLostSync (slotCnt, TxDMABuffers[port], *TxOffset, DMACfg[port].TxBuf);
   if (lostSync) {
      switchTxBuffers (port, TxOffset);
      SendWarningEvent (port + 0x200, WarnFrameSyncError);
   }

   // Set the circular buffer and linear DMA pointers to the first slot.
   TxCircBufr = TxCircBufrList;
   TxDmaBufr  = DmaTxBuff[port] + *TxOffset;
   DmaBufr0   = TxDmaBufr;

   SlipSamps = 0;
   if (slip)
       SlipSamps = sysConfig.samplesPerMs;

   // Loop through all configured slots checking for active slots.
   for (k = 0; k < slotCnt; k++, TxCircBufr++)  {
      TxCirc = *TxCircBufr;
   
      // Copy from active outbound channels' circular buffer to linear DMA buffer
      TxCirc->SlipSamps += SlipSamps;
      CheckTxPointers (TxCirc, TxDmaBufr);   // Make sure there are enough samples for transfers
      buffCnt = TxCirc->BufrSize - TxCirc->TakeIndex;

      if (buffCnt <= sysConfig.samplesPerMs) {
         memcpy (TxDmaBufr, &TxCirc->pBufrBase[TxCirc->TakeIndex], buffCnt * SAMPLE_I8);
		 /*  Test code
		 for(i = 0; i < buffCnt; i++) {
		    TxDmaBufr[i] = ((ADT_DMA_Word)TxCirc->pBufrBase[TxCirc->TakeIndex + i]) ; //<< 8;
			if(SAMPLE_I8 == 4)
			   TxDmaBufr[i] = (TxDmaBufr[i]&0xffffff) << 8;
		 }
		 */
         TxDmaBufr += buffCnt;
         buffCnt = sysConfig.samplesPerMs - buffCnt;
         TxCirc->TakeIndex = 0;
      } else {
         buffCnt = sysConfig.samplesPerMs;   
      }
      if (buffCnt) {
         memcpy (TxDmaBufr, &TxCirc->pBufrBase[TxCirc->TakeIndex], buffCnt * SAMPLE_I8);
		 /*  Test code
		 for(i = 0; i < buffCnt; i++) {
		    TxDmaBufr[i] = ((ADT_DMA_Word)TxCirc->pBufrBase[TxCirc->TakeIndex + i]) ; // << 8;
			if(SAMPLE_I8 == 4)
			   TxDmaBufr[i] = (TxDmaBufr[i]&0xffffff) << 8;
		 }
		 */
         TxCirc->TakeIndex += buffCnt;
         TxDmaBufr += buffCnt;
      }
      // NOTE: muLawExpand is for testing only
      muLawExpand ((void *)(TxDmaBufr-sysConfig.samplesPerMs), sysConfig.samplesPerMs);


#ifdef FRAMESTAMP  // Verify that frame markers are correct
      if (FrameStamp)
         AppErr ("FrameStampError",
                      *(TxDmaBufr-(sysConfig.samplesPerMs*2)) != 0 &&
                      *(TxDmaBufr-(sysConfig.samplesPerMs*2)) != (short) (ApiBlock.DmaSwiCnt & 0xFFFF));
#endif


   }
   FixedPatternTx (DmaBufr0);   // Allow overwrite of slot 0 with fixed pattern for debug
   customTDMOut (DmaBufr0, port, slotCnt);

   switchTxBuffers (port, TxOffset);

   return lostSync;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{CopyDmaRxBuffers - Copy serial port DMA buffers to channel PCM buffers.
//
// FUNCTION
//   Demultiplexs PCM data between a serial port's DMA buffers and the
//   PCM circular buffers of any active channels using the serial port.
//
// Inputs
//     port          -  Count of TDM slots
//     RxCircBufrList -  List of receive circular PCM buffer pointers
//     RxDmaBufr     -  Linear dma receive buffer
//
// RETURNS
//     lostSync indication
//}
#pragma CODE_SECTION (CopyDmaRxBuffers, "FAST_PROG_SECT")
ADT_Bool CopyDmaRxBuffers (int port, int slotCnt, CircBufInfo_t **RxCircBufrList, int slip) {
   
   int k, buffCnt, lostSync;
   CircBufInfo_t *RxCirc, **RxCircBufr;
   ADT_PCM16     *RxDmaBufr;
   ADT_UInt16    *RxOffset;
   ADT_Word      SlipSamps;
              
   RxOffset = &DmaRxOffset[port];

   // Adjust the SWI copy buffer offset (RxOffset) if the DMA and SWI buffers are conflicting
   lostSync = DmaLostSync (slotCnt, RxDMABuffers[port], *RxOffset, DMACfg[port].RxBuf);
   if (lostSync) {
      switchBuffers (port, RxOffset);
      SendWarningEvent (port + 0x100, WarnFrameSyncError);
   }

   // Set the circular buffer and linear DMA pointers to the first slot.
   RxCircBufr = RxCircBufrList;
   RxDmaBufr  = DmaRxBuff[port] + *RxOffset;
   
   // Loop through all configured slots checking for active slots.
   FixedPatternRx  (RxDmaBufr);  // Allow overwrite of slot 0 with fixed pattern for debug
   customTDMIn (RxDmaBufr, port, slotCnt);
   
   SlipSamps = 0;
   if (slip)
       SlipSamps = sysConfig.samplesPerMs;

   for (k = 0; k < slotCnt; k++, RxCircBufr++)  {

      RxCirc = *RxCircBufr; 

      // Copy from active inbound DMA buffer to circular buffer
      RxCirc->SlipSamps += SlipSamps;
      CheckRxPointers (RxCirc, RxDmaBufr);   // Make sure there is enough samples/room for transfers

      // NOTE: muLawCompress for testing only
      muLawCompress ((void *)RxDmaBufr, sysConfig.samplesPerMs);

      buffCnt = RxCirc->BufrSize - RxCirc->PutIndex;
      if (buffCnt <= sysConfig.samplesPerMs) {
         memcpy (&RxCirc->pBufrBase[RxCirc->PutIndex], RxDmaBufr, buffCnt * SAMPLE_I8);
		 /* Test code
		 for(i = 0; i < buffCnt; i++) {
			if(2 < SAMPLE_I8)
			   RxDmaBufr[i] = (RxDmaBufr[i] & 0xFFFFFF) >> 8;
		    RxCirc->pBufrBase[RxCirc->PutIndex+i] = (ADT_Int16)(RxDmaBufr[i] & 0xFFFF); // >> 8);
		 }
		 */
         RxDmaBufr += buffCnt;
         buffCnt = sysConfig.samplesPerMs - buffCnt;
         RxCirc->PutIndex = 0;
         if (buffCnt == 0) continue;
      } else {
         buffCnt = sysConfig.samplesPerMs;   
      }
      if (buffCnt) {
	     memcpy (&RxCirc->pBufrBase[RxCirc->PutIndex], RxDmaBufr, buffCnt * SAMPLE_I8);
         /* Test code 
		 for(i = 0; i < buffCnt; i++) {
			if(2 < SAMPLE_I8)
			   RxDmaBufr[i] = (RxDmaBufr[i] & 0xFFFFFF) >> 8;
		    RxCirc->pBufrBase[RxCirc->PutIndex+i] = (ADT_Int16)(RxDmaBufr[i] & 0xFFFF); // >> 8);
	     }
		 */
         RxCirc->PutIndex += buffCnt;
         RxDmaBufr += buffCnt;
	  }
   }   

   switchBuffers (port, RxOffset);
   return lostSync;
}

#pragma CODE_SECTION (DmaLbRxToTxBuffers, "SLOW_PROG_SECT")
int DmaLbRxToTxBuffers (int port, int slotCnt) {
   int k, buffCnt, txlostSync, rxlostSync, rv;
   ADT_PCM16     *RxDmaBufr;
   ADT_UInt16    *RxOffset;
   ADT_PCM16     *TxDmaBufr;
   ADT_UInt16    *TxOffset;


   rv = 0;              
   RxOffset = &DmaRxOffset[port];
   TxOffset = &DmaTxOffset[port];

   slotCnt = DmaSlotCnt[port];
   // Adjust the SWI copy buffer offset (RxOffset) if the DMA and SWI buffers are conflicting
   rxlostSync = DmaLostSync (slotCnt, RxDMABuffers[port], *RxOffset, DMACfg[port].RxBuf);
   if (rxlostSync) {
      rv |= 1;
      switchBuffers (port, RxOffset);
      SendWarningEvent (port + 0x100, WarnFrameSyncError);
   }

   slotCnt = TxDmaSlotCnt[port];
   // Adjust the SWI copy buffer offset (TxOffset) if the DMA and SWI buffers are conflicting
   txlostSync = DmaLostSync (slotCnt, TxDMABuffers[port], *TxOffset, DMACfg[port].TxBuf);
   if (txlostSync) {
      rv |= 2;
      switchTxBuffers (port, TxOffset);
      SendWarningEvent (port + 0200, WarnFrameSyncError);
   }

   // Set the linear DMA pointers to the first slot.
   RxDmaBufr  = DmaRxBuff[port] + *RxOffset;
   TxDmaBufr  = DmaTxBuff[port] + *TxOffset;
   buffCnt = sysConfig.samplesPerMs;   

   for (k = 0; k < slotCnt; k++)  {
        memcpy (TxDmaBufr, RxDmaBufr, buffCnt << 1);
        RxDmaBufr += buffCnt;
        TxDmaBufr += buffCnt;
   }   

   switchBuffers (port, RxOffset);
   switchTxBuffers (port, TxOffset);

   return rv;
}
#endif


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{GpakEdmaIsr
//
// FUNCTION
//  G.PAK Hardware Interrupt handler for all EDMA Channels.
//  It's triggered by the occurrence of tx and rx mcbsp edma events. The 
//  CIPR register is checked to determine which events have occurred and
//  the SWI_Dma interrupt is issued to start the circular buffer copies.
//
// RETURNS
//  nothing
//}
#pragma CODE_SECTION (GpakDmaIsr, "FAST_PROG_SECT")
static void GpakDmaIsr (void) {

   ADT_UInt32 pendingEvents, pendingEventsClr;

   // Determine which interrupt is pending  
   REG_RD (EDMA_SHADOW_IPR, pendingEvents);

   isrCnt++;
   isrFlags |=pendingEvents;
#ifdef _DEBUG
   if (NumActiveChannels) logTime (0x00100000ul | pendingEvents);
#endif

#ifndef STANDALONE
   SWI_or (SWI_Dma, pendingEvents);
#endif

   // clear the edma pending interrupt flags
   REG_WR (EDMA_SHADOW_IPCR, pendingEvents);

   REG_RD (EDMA_SHADOW_IPR, pendingEventsClr);
   AppErr ("Problem", pendingEventsClr & pendingEvents);

   // clear the event combiner pending interrupt flag
   clearCombinedEvent (DMA_CompleteSgnl);

   // Force re-evaluation of any new interrupts during previous processing
   REG_WR   (EDMA_SHADOW_IEVL, 1);
   return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{GpakErrIsr
//
// FUNCTION
//  G.PAK Hardware Interrupt handler for error interrupts
//
//  It's triggered by the occurrence of edma errors.  Status registers
//  are read at the time of the error and stored in global memory for 
//  later review.
//
// RETURNS
//  nothing
//}
#pragma CODE_SECTION (GpakErrIsr, "SLOW_PROG_SECT")
static void GpakErrIsr (void) {
   logTime (0x00100005);
   err.cnt++;
   DMAClearErrors ();

   // clear all combined events that may have generated the ISR
   clearCombinedEvent (TDM_CCErrSgnl);
   clearCombinedEvent (TDM_Que0ErrSgnl);
   clearCombinedEvent (TDM_Que1ErrSgnl);
   clearCombinedEvent (McASP_ErrSgnl);
   logTime (0x00110005);
  
}

