//----------------------------------------------------------------------
//
//       GpakRTDX.c
//
//
//    This file implements ADT's messaging with ADT's host interface tool via TI's 
//    Real-time Data Exchange RTDX.
//
//    This adds an alternate to using the host APIs for communicating between the host
//    and DSPs, when a direct host interface is not available.
//
//    Messaging, events, and packet transfers (channel 0 only) are supported
//

#ifndef RTDX
// Stub out RTDX on release build
#include <std.h>

   #pragma CODE_SECTION (GpakRtdxSetup, "SLOW_PROG_SECT")
   void GpakRtdxSetup (void) {  return; }

   #pragma CODE_SECTION (PostRTDX, "FAST_PROG_SECT")
   void PostRTDX (void) {  return; }

void (*rtdxDiagnostics) (void) = NULL;

#else
#include <std.h>
#include <swi.h>
#include <string.h>
#include <stdio.h>
#include <mem.h>
#include <rtdx.h>

#include "GpakDefs.h"
#include "GpakExts.h"
#include "sysconfig.h"


//----------------------------------------
// DEBUGGING AIDS

#define CMDS 1
#define RPLS 2
#define MKRS 4
#define ERRS 8
#define ENBL 16

int RTDXfilter = ERRS | ENBL;

struct ResetStats {
   int ResetCnt, DisabledCnt;
} ResetStats = { 0, 0 };

struct CmdStats {
   int InactiveCnt;
   int BusyWaitReset, ThrowAwayCnt;
   int MarkerCnt,   NAKCnt,    UsageCnt, MsgCnt;
   int BusyWaitMrk, BusyWaitLen, BusyWaitCmd;
   int ReplyWait,   WriteWait;
   int ReadFail,    LenFail,    ACKFail, WriteFail;
} CmdStats;

void (*rtdxDiagnostics) (void) = NULL;

//----------------------------------------
// RTDX INTERFACE

#define RTDX_MARKER  ((ADT_UInt32) 0xb0b0b0b0l)
#define DSP_RESET    ((ADT_UInt32) 0x80000001l)
#define CPU_USAGE    ((ADT_UInt32) 0x80000002l)
#define HOST_EXIT    ((ADT_UInt32) 0x80000003l)
#define HOST_VERSION ((ADT_UInt32) 0x80040000l) // Host API version is in low word

RTDX_CreateInputChannel(GPakMsg);
RTDX_CreateOutputChannel(GPakReply);

RTDX_CreateOutputChannel(GPakPacketFromDSP);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// GpakRtdxInterface
//
// FUNCTION
//  Processes all messages from the host processor via the Rtdx interface.
//  Called every millisecond by software interrupt
//
//  This routine is processed as a state machine where processing continues
//  until it reaches a state where it must wait for an external condition to
//  occur.  Subsequent calls to the function will exit without processing until
//  the external condition occurs and the next stage(s) of processing is performed.
//
// RETURNS
//  nothing
//
void GpakRtdxReadCmd (void);
void GpakRtdxWriteEvts (void);
void GpakRtdxFindReset (void);

extern sysConfig_t sysConfig;
extern CircBufInfo_t pktBuffStructTable[], eventFIFOInfo;



#if (DSP_TYPE == 54 || DSP_TYPE == 55)
#define MAUBytes  2
#define HOSTBytes 2
#define Int16ToRTDX(i16) (i16)
#define Int8ToRTDX(i8)   (((i8) + 1) / 2)
typedef ADT_UInt16 MAU;
#elif (DSP_TYPE == 64)
#define MAUBytes  1
#define HOSTBytes 4
#define Int16ToRTDX(i16) ((((i16) * 2) + 3) & ~3)
#define Int8ToRTDX(i8)    (((i8) + 3) & ~3)
typedef ADT_UInt8 MAU;
#endif



//#define MAXBUFFER_I16S  (8)   // Max samples per frame + Header elements
#define MAXBUFFER_I16S  (240 + 6)   // Max samples per frame + Header elements
#define MAXBUFFER_BYTES (MAXBUFFER_I16S * 4)
#define MAXBUFFER_MAUS  (MAXBUFFER_BYTES / MAUBytes)

#pragma DATA_SECTION (UploadBuffer, "SLOW_DATA_SECT")
MAU   UploadBuffer [MAXBUFFER_MAUS];
MAU  *CurrentLocation = UploadBuffer;
ADT_Word  BufferredBytes = 0;
ADT_Word  EvtCnt = 0;


// RtdxActive - Set when host initially enables the reply link to the DSP
//    to initial RTDX and start looking for Reset message. 
//    Disabled when reply link is disabled by host or by DSP in response to
//    host exit.
//
// CmdLink - Set when reset message is detected.  Disabled when RtdxActive is
//    disabled.
//
// PktLink - Set when host enables GPakPacketFromDSP and a packet channel is 
//    available for transfers.  Disable when host disables GPakPacketFromDSP
//    or no packet channel is available.
//    

struct rtdxState {
   ADT_Bool  RtdxActive;   // Rtdx initialized on DSP
   ADT_Bool  CmdLink;      // Cmd link established with host
   int       Cmd;          // Current cmd state
   
} rtdxState = { FALSE, FALSE, -2 };

static ADT_Int32 NAK = -1;
static ADT_Int32 ACK = -2;
static ADT_Int32 RACK = -3;
static ADT_Int32 VACK = -4;

#pragma CODE_SECTION (circGetFreeSpace, "SLOW_PROG_SECT")
ADT_UInt16 circGetFreeSpace (CircBufInfo_t *buf) {
   ADT_UInt16 nAvail, tempTake;

   tempTake = buf->TakeIndex;
    if (tempTake <= buf->PutIndex)
        nAvail = buf->BufrSize - (buf->PutIndex - tempTake) - 1;
    else
        nAvail = tempTake - buf->PutIndex - 1;

    return (nAvail);

}

#pragma CODE_SECTION (PostRTDX, "FAST_PROG_SECT")
void PostRTDX (void) {
   if (SWI_RTDX != NULL) SWI_post (SWI_RTDX);
}


//----------------------------------------------------
//
//  Enable input channels allowing host to talk to DSP.
//
//----------------------------------------------------
far SWI_Handle SWI_RTDX = NULL;
static void GpakRtdxSwi (void);
static SWI_Attrs RtdxAttrs = {  (SWI_Fxn) GpakRtdxSwi, 0, 0, 0, 0 };

#pragma CODE_SECTION (GpakRtdxSetup, "SLOW_PROG_SECT")
void GpakRtdxSetup (void) {
   
   RtdxAttrs.priority = RtdxPriority;

#if (DSP_TYPE == 54)
   RtdxAttrs.iscfxn = TRUE;
#endif
   memset (&CmdStats,  0, sizeof (CmdStats));

   rtdxState.Cmd = -2;
   CurrentLocation = UploadBuffer;
   BufferredBytes = 0;

// NOTE:  RTDX_Init () is called as part of startup;
   SWI_RTDX = SWI_create (&RtdxAttrs);
   AppErr ("RTDXSemaphore", SWI_RTDX == NULL);

}


//----------------------------------------------------
//
//  Transfer messages, inbound packets and outbound packets
//
//----------------------------------------------------
#pragma CODE_SECTION (GpakRtdxSwi, "SLOW_PROG_SECT")

static void GpakRtdxSwi (void) {

   static ADT_Bool FirstTime = TRUE;
   int hostActive;
   
#if RTDX_POLLING_IMPLEMENTATION
   RTDX_Poll ();
#endif

   logTime (0x80000000ul);
   logTime (0x00800000ul);
   if (rtdxDiagnostics != NULL) (*rtdxDiagnostics) ();
   logTime (0x00810000ul);

   if (FirstTime) {
      // Use s/w interrupt disable as locking mechanism
      _register_lock      (SWI_disable);
      MEM_register_lock   ((Fxn) SWI_disable);

      _register_unlock    (SWI_enable);
      MEM_register_unlock ((Fxn) SWI_enable);
      FirstTime = FALSE;
   }

   // Shut down RTDX operation when host goes inactive
   hostActive = RTDX_isOutputEnabled (&GPakReply);
   if (!hostActive) {
      CmdStats.InactiveCnt++;

      logTime (0x04020000ul);
      RTDX_disableInput (&GPakMsg);
      if (rtdxState.CmdLink) {
         if (RTDXfilter & ENBL) printf ("RTDX disabled\n");
         ResetStats.DisabledCnt++;
         RTDX_Quit ();
         CmdStats.InactiveCnt = 0;
      }
      rtdxState.CmdLink    = FALSE;
      rtdxState.RtdxActive = FALSE;
      logTime (0x80010000ul);
      return;
   }


   if (!rtdxState.CmdLink) {
      logTime (0x04030000ul);
      GpakRtdxFindReset ();
      logTime (0x80010000ul);
      return;
   }
   
   logTime (0x04040000ul);
   GpakRtdxReadCmd ();

   logTime (0x04050000ul);
   GpakRtdxWriteEvts ();
   logTime (0x80010000ul);


}
//====================================================
//
//  Search for RESET marker, Issue RACK and size data when found.
//
//  CmdStates: 0  Issue read for marker
//             1  Wait for read completion.
//
//

static ADT_UInt32 MsgBytes;
#pragma DATA_SECTION (MsgBuffer, "SLOW_DATA_SECT")
static ADT_UInt32 MsgBuffer[400];

#pragma CODE_SECTION (GpakRtdxFindReset, "SLOW_PROG_SECT")
void GpakRtdxFindReset (void) {
   
   ADT_UInt32 Data32;
   int readStatus, readLen;


   if (!rtdxState.RtdxActive) {
      if (RTDXfilter & ENBL) printf ("RTDX enabled\n");

      GPakMsg.busy = FALSE;

      readLen = CmdStats.InactiveCnt;
      memset (&CmdStats,  0, sizeof (CmdStats));
      CmdStats.InactiveCnt = readLen;

      RTDX_Init ();
      RTDX_enableInput (&GPakMsg);
      
      rtdxState.RtdxActive = TRUE;
      rtdxState.Cmd = -2;    // Start searching for RESET MARKER
   }

NextState:

   while (RTDX_writing) ;
   switch (rtdxState.Cmd) {
   case -2:
      readStatus = RTDX_readNB (&GPakMsg, &MsgBuffer, sizeof (MsgBytes));
      if (readStatus != RTDX_OK) {
         if (RTDXfilter & ERRS) printf ("RTDX Read FAIL %d\n", readStatus);
         while (RTDX_writing) ;
         RTDX_write (&GPakReply, &NAK, sizeof (NAK));

         CmdStats.ReadFail++;
         return;
      }
      rtdxState.Cmd++;

  case -1:
      if (RTDX_channelBusy (&GPakMsg)) {
         CmdStats.BusyWaitReset++;
         return;
      }

      readLen = RTDX_sizeofInput (&GPakMsg);
      if (readLen < sizeof (MsgBytes)) {
         if (RTDXfilter & ERRS) printf ("RTDX Len FAIL %d\n", readLen);
         CmdStats.LenFail++;
         rtdxState.Cmd = -2;

         while (RTDX_writing) ;
         RTDX_write (&GPakReply, &NAK, sizeof (NAK));
         return;
      }
      MsgBytes = MsgBuffer[0];

      // Continue reading until a RESET marker is found
      if (MsgBytes != DSP_RESET) {
         if (RTDXfilter & MKRS) printf ("RTDX Reset throwaway %x\n", MsgBytes);
         rtdxState.Cmd = -2;
         CmdStats.ThrowAwayCnt++;
         goto NextState;
      }

      if (RTDXfilter & ENBL) printf ("RTDX reset found\n");
      ResetStats.ResetCnt++;

      while (RTDX_writing) ;
      RTDX_write (&GPakReply, &RACK, sizeof (RACK));

      Data32 = (ADT_UInt32) ApiBlock.NumBuiltChannels;
      while (RTDX_writing) ;
      RTDX_write (&GPakReply, &Data32, sizeof (Data32));

      Data32 = (ADT_UInt32) ApiBlock.MaxCmdMsgLen;
      while (RTDX_writing) ;
      RTDX_write (&GPakReply, &Data32, sizeof (Data32));
      memset (&CmdStats, 0, sizeof (CmdStats));

      
      rtdxState.Cmd = -2;          // Start searching for Messaging MARKER
      rtdxState.CmdLink = TRUE;
   }
   return;
}




//----------------------------------------------------
//
//  Transfer inbound message from RTDX to message buffer
//  and outbound response from response buffer to RTDX
//
//  state
//    -2: Issue read for marker
//    -1: Wait for read completion, 
//             Must find marker before proceeding.
//     0: Issue read for message length, or special command
//     1: Wait for message length or special commands
//             Issue REPLY on special commands
//             Issue read for message
//             Issue ACK or NACK on message length
//     2: Wait for message from host
//             Inform messaging thread of new messsage
//     3: Wait for reply from messaging thread
//             Send reply length to host
//     4: Wait for reply length write to complete
//             Send full reply to host
//
//-----------------------------------------------
#pragma CODE_SECTION (GpakRtdxReadCmd, "SLOW_PROG_SECT")
void GpakRtdxReadCmd (void) {
   
   ADT_UInt32 Data32;
   int readStatus, readLen;
   ADT_UInt32 len;

   // Message task processing cmd.  Wait until reply complete before continuing
   if (ApiBlock.CmdMsgLength != 0) return;


NextState:
   while (RTDX_writing) ;

   switch (rtdxState.Cmd) {
   case -2:
      // Issue read for marker
      readStatus = RTDX_readNB (&GPakMsg, &MsgBuffer, sizeof (MsgBytes));
      if (readStatus != RTDX_OK) {
         if (RTDXfilter & ERRS) printf ("RTDX read FAIL %d\n", readStatus);
         while (RTDX_writing) ;
         RTDX_write (&GPakReply, &NAK, sizeof (NAK));

         CmdStats.ReadFail++;
         return;
      }
      rtdxState.Cmd = -1;

  case -1:
      // Wait for marker
      if (RTDX_channelBusy (&GPakMsg)) {
         CmdStats.BusyWaitMrk++;
         return;
      }
      CmdStats.BusyWaitMrk = 0;

      readLen = RTDX_sizeofInput (&GPakMsg);
      if (readLen < sizeof (MsgBytes)) {
         if (RTDXfilter & ERRS) printf ("RTDX len FAIL %d\n", readLen);
         CmdStats.LenFail++;
         rtdxState.Cmd = -2;
         return;
      }
      MsgBytes = MsgBuffer[0];
      if (RTDXfilter & MKRS) printf ("RTDX markers %x\n", MsgBytes);
      if (MsgBytes == HOST_EXIT) {
         if (RTDXfilter & ENBL) printf ("RTDX host exit\n");
         RTDX_disableOutput (&GPakReply);
         return;
      } else if (MsgBytes != RTDX_MARKER) {
         CmdStats.ThrowAwayCnt++;
         rtdxState.Cmd = -2;
         goto NextState;
      } else 

      // Marker found, 
      CmdStats.MarkerCnt++;
      MsgBuffer[0] = 0;
      rtdxState.Cmd++;

   case 0: 
      // Issue read for message length or special command
      readStatus = RTDX_readNB (&GPakMsg, &MsgBuffer, sizeof (MsgBytes));
      if (readStatus != RTDX_OK) {
         // Failed read - NAK host to cause a path reset
         while (RTDX_writing) ;
         RTDX_write (&GPakReply, &NAK, sizeof (NAK));
         CmdStats.NAKCnt++;
         rtdxState.Cmd = -2;
         return;
      } 
      rtdxState.Cmd++;
   
   case 1: 
      // Wait for command length or special command
      if (RTDX_channelBusy (&GPakMsg)) {
         CmdStats.BusyWaitLen++;
         return;
      }
      readLen = RTDX_sizeofInput (&GPakMsg);
      if (readLen < sizeof (MsgBytes)) {
         if (RTDXfilter & ERRS) printf ("RTDX Cmd Len FAIL %d\n", readLen);
         CmdStats.LenFail++;
         rtdxState.Cmd = -2;
         return;
      }
      CmdStats.BusyWaitLen = 0;

      MsgBytes = MsgBuffer [0];  
      if (RTDXfilter & MKRS) printf ("RTDX MsgLen_Msg %x\n", MsgBytes);

      // Process special commands:
      //   Another marker. Reissue read.
      //   Reset.          Send RACK and sizing data
      //   CPU Usage.      Send CPU usage data
      //   Host Exit.      Disable RTDX 
      //   Host API Version. Send VACK and read the API version ID
      if (MsgBytes == RTDX_MARKER) {
         rtdxState.Cmd = 0;
         goto NextState;
      } else if (MsgBytes == DSP_RESET) {
         // Command is check DSP reset.  Send back DSP sizing info.
         while (RTDX_writing) ;
         RTDX_write (&GPakReply, &RACK, sizeof (RACK));
         Data32 = (ADT_UInt32) ApiBlock.NumBuiltChannels;
         while (RTDX_writing) ;
         RTDX_write (&GPakReply, &Data32, sizeof (Data32));
         Data32 = (ADT_UInt32) ApiBlock.MaxCmdMsgLen;
         while (RTDX_writing) ;
         RTDX_write (&GPakReply, &Data32, sizeof (Data32));
         memset (&CmdStats, 0, sizeof (CmdStats));
         rtdxState.Cmd = -2;
         return;
      } else if (MsgBytes == CPU_USAGE) {
         // Command is get DSP usage.  Send back DSP usage data.
         while (RTDX_writing) ;
         RTDX_write (&GPakReply, &ACK, sizeof (ACK));
         if (checkApiVer()) {
            while (RTDX_writing) ;
            len = (&ApiBlock.CpuPkUsage30 - &ApiBlock.u1.CpuUsage1) + 1;
            RTDX_write (&GPakReply, &ApiBlock.u1.CpuUsage1, len * sizeof (ADT_Word));
         } else { 
            while (RTDX_writing) ;
            RTDX_write (&GPakReply, &ApiBlock.u1.CpuUsage1, 7 * sizeof (ADT_Word));
         }
         while (RTDX_writing) ;
         RTDX_write (&GPakReply, &ApiBlock.DmaSwiCnt, sizeof (ADT_UInt32));
         CmdStats.UsageCnt++;
         rtdxState.Cmd = -2;
         return;
      } else if (MsgBytes == HOST_EXIT) {
         if (RTDXfilter & ENBL) printf ("RTDX host exit\n");
         RTDX_disableOutput (&GPakReply);
         return;
      } else if ((MsgBytes & 0xFFFF0000ul) == HOST_VERSION) {
         // Command is Update API Version   
         ApiBlock.ApiVersionId = (ADT_Word) (MsgBytes & 0x0000FFFF);
         while (RTDX_writing) ;
         RTDX_write (&GPakReply, &VACK, sizeof (VACK));
         rtdxState.Cmd = -2;
         return;
      }

      // Verify command length and issue read for message
      if ((ApiBlock.MaxCmdMsgLen < MsgBytes) || ((MsgBytes & 0x3) != 0) ||
          (MsgBytes == 0)) {
         // Command too large for command buffer or not multiple of 4 bytes.
         // Send NAK and purge buffer
         while (RTDX_writing) ;
         RTDX_write (&GPakReply, &NAK, sizeof (NAK));
         CmdStats.NAKCnt++;
         rtdxState.Cmd = -2;
         return;
      }

      // Read message.
      if (RTDX_readNB (&GPakMsg, (void *) ApiBlock.CmdMsgPointer, MsgBytes / MAUBytes) != RTDX_OK) {
         // Unable to post read request.   Issue NAK.
         while (RTDX_writing) ;
         RTDX_write (&GPakReply, &NAK, sizeof (NAK));
         CmdStats.NAKCnt++;
         rtdxState.Cmd = -2;
         return;
      }
      // Send ACK to host that request is being honored.
      while (RTDX_writing) ;
      if (RTDX_write (&GPakReply, &ACK, sizeof (ACK)) == 0) {
         CmdStats.ACKFail++;
         rtdxState.Cmd = -2;
         return;
      }
      rtdxState.Cmd++;
      
   case 2:  
      // Wait for host message transfer to complete.
      if (RTDX_channelBusy (&GPakMsg)) {
         CmdStats.BusyWaitCmd++;
         return; 
      }
      
      readLen = RTDX_sizeofInput (&GPakMsg);
      if (readLen != (MsgBytes / MAUBytes)) {
         CmdStats.LenFail++;
         if (RTDXfilter & ERRS) printf ("RTDX Msg Len FAIL %d. Expecting %d\n", readLen, MsgBytes/MAUBytes);
         rtdxState.Cmd = -2;
         return;
      }
      if (RTDXfilter & CMDS) printf ("Cmd: %4x\n", ApiBlock.CmdMsgPointer[0]);
   
      // Signal Message processing that a command exists in the message buffer
      ApiBlock.CmdMsgLength = MsgBytes;
      rtdxState.Cmd++;
      
   case 3:  
      // Wait for message processing thread to complete transaction
      if (ApiBlock.ReplyMsgLength == 0) {
         CmdStats.ReplyWait++;
         return;
      }
      
      // Send reply length to host
      Data32 = Int8ToRTDX (ApiBlock.ReplyMsgLength);
      while (RTDX_writing) ;
      if (RTDX_write (&GPakReply, &Data32, sizeof (Data32)) == 0) {
         CmdStats.WriteFail++;
         rtdxState.Cmd = -2;
         ApiBlock.ReplyMsgLength = 0;
         return;
      }
      rtdxState.Cmd++;

  case 4:
      // Send full reply to host
      if (RTDX_writing) {
         CmdStats.WriteWait++;
         return;
      }
      
      if (RTDX_write (&GPakReply, (void *) ApiBlock.ReplyMsgPointer, 
                      Int8ToRTDX (ApiBlock.ReplyMsgLength)) == 0) {
         CmdStats.WriteFail++;
         rtdxState.Cmd = -2;
      }
      if (RTDXfilter & RPLS) printf ("Rsp: %4x %4x\n\n", ApiBlock.ReplyMsgPointer[0], ApiBlock.ReplyMsgPointer[1]);
      rtdxState.Cmd = -2;
      ApiBlock.ReplyMsgLength = 0;
      CmdStats.MsgCnt++;
      return;    

   }

   return;    
}

//----------------------------------------------------
//
//  Transfer events to RTDX
//
//----------------------------------------------------
#pragma CODE_SECTION (GpakRtdxWriteEvts, "SLOW_PROG_SECT")
void GpakRtdxWriteEvts (void) {
   
   CircBufInfo_t *EvtBufr;
   EventFifoCIDMsg_t Msg;
   int bytePyldLen, i16PyldLen, i16PktLen;
   ADT_Word PutIndex, TakeIndex, BufrSize, i16Available;


   EvtBufr = &eventFIFOInfo;

 NextWrite:
   if (RTDX_writing) return;

   // Verify event data is available
   PutIndex  = EvtBufr->PutIndex;
   TakeIndex = EvtBufr->TakeIndex;
   BufrSize  = EvtBufr->BufrSize;

   if (PutIndex < TakeIndex)  i16Available = BufrSize - TakeIndex + PutIndex;
   else                       i16Available = PutIndex - TakeIndex;
   
   if (i16Available < EVENT_HDR_I16LEN)
      return;

   // Transfer event data (hdr + payload) from circular buffer into linear buffer
   copyCircToLinear (EvtBufr, &Msg,  EVENT_HDR_I16LEN);

   bytePyldLen = Msg.header.eventLength;
   i16PyldLen  = (bytePyldLen + 1) / 2;

   if (EVENT_MAX_CID_MSGLEN_WORDS < i16PyldLen) {
      EvtBufr->TakeIndex = EvtBufr->PutIndex;
      return;
   }

   //copyCircToLinear (EvtBufr, &Msg.payload, SzHostToInt16 (i16PyldLen));
   copyCircToLinear (EvtBufr, &Msg.payload, i16PyldLen);

   Msg.header.eventCode |= 0xee00;  // Marker for RTDX host.

   // Write event data to host and then search for more
   if (RTDX_isOutputEnabled (&GPakPacketFromDSP)) {
   		i16PktLen = EVENT_HDR_I16LEN + i16PyldLen;
   		RTDX_write (&GPakPacketFromDSP, &Msg, Int16ToRTDX (i16PktLen));
   }
    
   EvtCnt++;
   goto NextWrite;
}



#endif

