//----------------------------------------------------------------------
//
//       64xnetEndian.c
//
//
//    This file implements ADT's network endian conversions
//
#include <adt_typedef.h>

#define NETSECTION "MEDIUM_PROG_SECT"
#pragma CODE_SECTION (netI8, NETSECTION)
ADT_UInt8 netI8 (void *Buff, int *Offset) {

   ADT_UInt8 *buff = Buff;
   buff += *Offset;
   *Offset += 1;
   return *buff;
}


#pragma CODE_SECTION(netI16, NETSECTION)
ADT_UInt16 netI16 (void *Buff, int *Offset) {
   ADT_UInt16 temp;
   temp  = netI8 (Buff, Offset) << 8;
   temp |= netI8 (Buff, Offset);
   return temp;
}


#pragma CODE_SECTION (netI32, NETSECTION)
ADT_UInt32 netI32 (void *Buff, int *Offset) {
   ADT_UInt32 temp;

   temp = ((ADT_Int32) netI16 (Buff, Offset)) << 16;   
   temp |= netI16 (Buff, Offset);
   return temp;
}

#pragma CODE_SECTION (netIx, NETSECTION)
void netIx (void *Buff, int *Offset, void *Value, int ValueLen) {
   ADT_UInt8 *buff = Buff;

   memcpy (Value, buff + *Offset, ValueLen);
   *Offset = *Offset + ValueLen;
}


#pragma CODE_SECTION (I8net, NETSECTION)
void I8net (void *Buff, int *Offset, ADT_UInt8 value) {

   ADT_UInt8 *buff;

   buff = Buff;
   buff += *Offset;
   *buff = value;
   *Offset = *Offset + 1;
}

#pragma CODE_SECTION (I16net, NETSECTION)
void I16net (void *Buff, int *Offset, ADT_UInt16 value) {

   ADT_UInt8 *buff;
   buff = Buff;
   buff += *Offset;

   *buff++ = ((value >> 8) & 0xff);
   *buff =  (value & 0xff);
   *Offset = *Offset + 2;
}

#pragma CODE_SECTION (I32net, NETSECTION)
void I32net (void *Buff, int *Offset, ADT_UInt32 value) {
   I16net (Buff, Offset, value >> 16);
   I16net (Buff, Offset, value & 0xffff);  
}

#pragma CODE_SECTION (Ixnet, NETSECTION)
void Ixnet  (void *Buff, int *Offset, void *Value, int ValueLen) {
   ADT_UInt8 *buff = Buff;

   memcpy (buff + *Offset, Value, ValueLen);
   *Offset = *Offset + ValueLen;
}
