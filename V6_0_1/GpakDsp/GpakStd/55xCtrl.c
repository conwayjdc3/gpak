//---------------------------------------------------------------
// Host interrupt control initialization
//
#define _CSL_H_         // Avoid csl definitions
#include <std.h>
#include <stdio.h>
#include <hwi.h>
#include <swi.h>

#include "adt_typedef.h"

#include "sysconfig.h"

int hostToDspIntrCnt = 0;
int dspToHostIntrCnt = 0;

// HPI interrupt register values
#define IRQ_HPI_VECT  13
#define IRQ_HPI_EVNT   0

// HPI Register values
#define HPIC          (volatile ADT_UInt32 *)(0x01880000U)
#define HPI_HOST_INT   0x4
#define HPI_DSP_INT    0x2

// PCI Register Values
#define PCI64_RSTSRC  (volatile ADT_UInt32 *)(0x01C00000U)
#define RSTSRC_INTREQ        0x00000008U
#define RSTSRC_INTRST        0x00000010U

#define PCIIS   (volatile ADT_UInt32 *)(0x01C00008U)
#define PCIIEN  (volatile ADT_UInt32 *)(0x01C0000CU)
#define PCI_DSP_INT  8

#pragma CODE_SECTION (entry_code, "BootSect")
void entry_code () {
    asm("   .global _c_int00");
	asm("   B _c_int00");
}
//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Enable_Interrupt_From_Host 
//
// FUNCTION
//   Signal host that data is available
//
//
//}
#pragma CODE_SECTION (Gen_Host_Interrupt, "MEDIUM_PROG_SECT")
void Gen_Host_Interrupt () {
    asm(" BCLR HINT");  // assert the HINT pin
    asm(" NOP");        // wait 50 nS
    asm(" NOP");
    asm(" NOP");
    asm(" NOP");
    asm(" NOP");
    asm(" NOP");
    asm(" NOP");
    asm(" NOP");
    asm(" NOP");
    asm(" NOP");
    asm(" BSET HINT");  // de-assert HINT pin
	return;
}

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Enable_Interrupt_From_Host 
//
// FUNCTION
//   Enable DSP processing of interrupts from host
//
// Inputs
//   HostIsr - Function to process host interrupt
//
//}
#pragma CODE_SECTION (Enable_Interrupt_From_Host, "MEDIUM_PROG_SECT")
void Enable_Interrupt_From_Host (Fxn HostIsr) {

/////empty
}

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// HostHWI
//
// FUNCTION
//   Process hardware interrupt generated by host by issuing software interrupt
//
//}
#pragma CODE_SECTION (ProcessHostIsr, "FAST_PROG_SECT")
extern SWI_Handle SWI_Host;
void ProcessHostIsr () {

   // empty for C55x
   //SWI_post (SWI_Host);

   // Clear interrupt events (PCI or HPI)
   //*HPIC  |= HPI_DSP_INT;
   //*PCIIS |= PCI_DSP_INT;
   //hostToDspIntrCnt++;
}

#pragma CODE_SECTION (Gen_Core_Interrupt, "SLOW_PROG_SECT")
void Gen_Core_Interrupt (int frameIdx, int coreID) { return; }

#pragma CODE_SECTION (Read_Core_Interrupt, "SLOW_PROG_SECT")
ADT_UInt32 Read_Core_Interrupt (int coreID) {  return 0; }

#pragma CODE_SECTION (ACK_Core_Interrupt, "SLOW_PROG_SECT")
void ACK_Core_Interrupt (int flags, int coreID) { return; }
#pragma CODE_SECTION (Enable_Interrupt_From_Core0, "SLOW_PROG_SECT")
void Enable_Interrupt_From_Core0 (Fxn frameReadyISR) { return;}
#pragma CODE_SECTION (PowerOnDevice, "SLOW_PROG_SECT")
void PowerOnDevice (int DeviceCode) { return; }
#pragma CODE_SECTION (PowerOnPeripherals, "SLOW_PROG_SECT")
void PowerOnPeripherals () { return; }

void BCACHE_inv (int* addr, int sizeI8, int T) {}
void BCACHE_wb (int* addr, int sizeI8, int T) {}
void BCACHE_wbInv (int* addr, int sizeI8, int T) {}

void bitUnlock (int i, int *lockField) {}
int bitLock (int i, int *lockField) {}
void wordUnlock (int *lockField) {}
int wordLock (int *lockField) {}
