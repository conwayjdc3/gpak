#include <xdc/std.h>
#include <xdc/runtime/System.h>
#include "GpakBios.h"
#include "GpakPcm.h"

Int setup_edma_interrupts(Hwi_FuncPtr edmaIsr) {
  Int eventId;
  Hwi_Params hwiParams;
  Hwi_Handle hwiHandle;
  Int hostInt;
  //Error_Block eb;

  // Initialize the error block
  //Error_init(&eb);

  // Map EDMA completion events for McBSP0 Rx=24, Tx=25,McBSP1 Rx=26,Tx=27 to a single Host interrupt on CIC0 channel 0
  hostInt = 0;

  CpIntc_mapSysIntToHostInt(0, CSL_INTC0_CPU_3_2_EDMA3CCINT0, hostInt);
  CpIntc_dispatchPlug(CSL_INTC0_CPU_3_2_EDMA3CCINT0, edmaIsr, CSL_INTC0_CPU_3_2_EDMA3CCINT0, TRUE);

  CpIntc_mapSysIntToHostInt(0, CSL_INTC0_CPU_3_2_EDMA3CCINT1, hostInt);
  CpIntc_dispatchPlug(CSL_INTC0_CPU_3_2_EDMA3CCINT1, edmaIsr, CSL_INTC0_CPU_3_2_EDMA3CCINT1, TRUE);

  CpIntc_mapSysIntToHostInt(0, CSL_INTC0_CPU_3_2_EDMA3CCINT2, hostInt);
  CpIntc_dispatchPlug(CSL_INTC0_CPU_3_2_EDMA3CCINT2, edmaIsr, CSL_INTC0_CPU_3_2_EDMA3CCINT2, TRUE);

  CpIntc_mapSysIntToHostInt(0, CSL_INTC0_CPU_3_2_EDMA3CCINT3, hostInt);
  CpIntc_dispatchPlug(CSL_INTC0_CPU_3_2_EDMA3CCINT3, edmaIsr, CSL_INTC0_CPU_3_2_EDMA3CCINT3, TRUE);

  // Enable the Host interrupt on Intc 0
  CpIntc_enableHostInt(0, hostInt);

  // Get the corepack's INTC/event combiner input eventId associated with the Host interrupt
  eventId = CpIntc_getEventId(hostInt);

  // Plug the event associated with Host Interrupt.
  // The function must be 'CpIntc_dispatch' and argument 'hostInt'.
  EventCombiner_dispatchPlug(eventId, &CpIntc_dispatch, hostInt, TRUE);

  // Initialize the Hwi parameters
  Hwi_Params_init(&hwiParams);

  // The eventId must be set to the combined event
  hwiParams.eventId = (eventId / 32);

  // The arg must be set to hwiParams.eventId
  hwiParams.arg = hwiParams.eventId;

  // Enable the interrupt.
  hwiParams.enableInt = TRUE;

  // Create the Hwi on interrupt HWI_TDM_COMPLETE=8 then specify 'EventCombiner_dispatch'
  // as the function.
  hwiHandle = Hwi_create(HWI_TDM_COMPLETE, &EventCombiner_dispatch, &hwiParams, NULL);
  if (hwiHandle == 0) return -1;

  return 0;
}


Int setup_edmaErr_interrupts(Hwi_FuncPtr errIsr) {
  Int eventId;
  Hwi_Params hwiParams;
  Hwi_Handle hwiHandle;
  Int hostInt;
  //Error_Block eb;

  // Initialize the error block
  //Error_init(&eb);

  // Map EDMA error events for EDMA3 err=16, TC1 err = 19, TC2 err = 20 to a single Host interrupt on CIC0 channel 40
  hostInt = 40;

  CpIntc_mapSysIntToHostInt(0, CSL_INTC0_CPU_3_2_EDMACC_ERRINT, hostInt);
  CpIntc_dispatchPlug(CSL_INTC0_CPU_3_2_EDMACC_ERRINT, errIsr, CSL_INTC0_CPU_3_2_EDMACC_ERRINT, TRUE);

  CpIntc_mapSysIntToHostInt(0, CSL_INTC0_CPU_3_2_EDMATC_ERRINT1, hostInt);
  CpIntc_dispatchPlug(CSL_INTC0_CPU_3_2_EDMATC_ERRINT1, errIsr, CSL_INTC0_CPU_3_2_EDMATC_ERRINT1, TRUE);

  CpIntc_mapSysIntToHostInt(0, CSL_INTC0_CPU_3_2_EDMATC_ERRINT2, hostInt);
  CpIntc_dispatchPlug(CSL_INTC0_CPU_3_2_EDMATC_ERRINT2, errIsr, CSL_INTC0_CPU_3_2_EDMATC_ERRINT2, TRUE);


  // Enable the Host interrupt on Intc 0
  CpIntc_enableHostInt(0, hostInt);

  // Get the corepack's INTC/event combiner input eventId associated with the Host interrupt
  eventId = CpIntc_getEventId(hostInt);

  // Plug the event associated with Host Interrupt
  // The function must be 'CpIntc_dispatch' and argument 'hostInt'.
  EventCombiner_dispatchPlug(eventId, &CpIntc_dispatch, hostInt, TRUE);

  // Initialize the Hwi parameters
  Hwi_Params_init(&hwiParams);

  // The eventId must be set to the combined event
  hwiParams.eventId = (eventId / 32);

  // The arg must be set to hwiParams.eventId
  hwiParams.arg = hwiParams.eventId;

  // Enable the interrupt.
  hwiParams.enableInt = TRUE;

  // Create the Hwi on interrupt HWI_TDM_ERR=5 then specify 'EventCombiner_dispatch'
  // as the function.
  hwiHandle = Hwi_create(HWI_TDM_ERR, &EventCombiner_dispatch, &hwiParams, NULL);
  if (hwiHandle == 0) return -1;

  return 0;
}
