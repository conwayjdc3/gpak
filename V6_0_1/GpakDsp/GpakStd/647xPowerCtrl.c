//---------------------------------------------------------------
// Power control initialization
//
#define _CSL_H_         // Avoid csl definitions
#include <std.h>
#include <stdio.h>
#include <hwi.h>
#include <c64.h>
#include <clk.h>
#include <bcache.h>
#include "GpakExts.h"
#include "adt_typedef.h"

#include "64plus.h"
#include "sysconfig.h"

#ifdef CSL_PSC_0_REGS
   #define PSC_BASE     CSL_PSC_0_REGS
#else
   #ifdef CSL_PSC_REGS
      #define PSC_BASE     CSL_PSC_REGS
   #endif
#endif

#define PSC_CMD   ((volatile ADT_UInt32 *) (PSC_BASE + 0x120))  // PTCMD
#define PSC_STAT  ((volatile ADT_UInt32 *) (PSC_BASE + 0x128))  // PTSTAT
#define PSC_REG   ((volatile ADT_UInt32 *) (PSC_BASE + 0xA00))  // MDCTLn

#define PSC_EMAC0   7
#define PSC_EMAC1   8
#define PSC_LCL_ENABLE  0x100

int hostToDspIntrCnt;
int dspToHostIntrCnt;

// Force linker inclusion of reset jump vectors
extern void *core0init;
void *dummy = &core0init;

#pragma CODE_SECTION (updateBootProg, "FAST_PROG_SECT")
void updateBootProg (ADT_UInt32 type) { 
   *((volatile ADT_UInt32 *)0x02AB0008) = type; // Boot progress register
}

extern Fxn frameReadyISR;


#pragma CODE_SECTION (Gen_Core_Interrupt, "MEDIUM_PROG_SECT")
void Gen_Core_Interrupt (int frameBits, int coreID) {
   ADT_UInt32 *CoreIntrRegister;
      
   CoreIntrRegister = (ADT_UInt32 *) 0x02A80540;
   CoreIntrRegister += coreID;
   *CoreIntrRegister |= frameBits | 1;
}

#pragma CODE_SECTION (Read_Core_Interrupt, "FAST_PROG_SECT")
ADT_UInt32 Read_Core_Interrupt (int coreID) {
   ADT_UInt32 *CoreIntrRegister;
      
   CoreIntrRegister = (ADT_UInt32 *) 0x02A80540;
   CoreIntrRegister += coreID;
   return *CoreIntrRegister;
}

#pragma CODE_SECTION (ACK_Core_Interrupt, "FAST_PROG_SECT")
void ACK_Core_Interrupt (int flags, int coreID) {
   ADT_UInt32 *CoreACKRegister;
      
   CoreACKRegister = (ADT_UInt32 *) 0x02A80580;
   CoreACKRegister += coreID;
   *CoreACKRegister |= flags;
   
}

#pragma CODE_SECTION (Gen_Host_Interrupt, "MEDIUM_PROG_SECT")
void Gen_Host_Interrupt () {
   if (sysConfig.HostIF & HPI_IF)
      *HPIC |= HPI_HOST_INT; 
#ifdef PCI_BASE
   else if (sysConfig.HostIF & PCI_IF)
      *PCI_INT_GEN = PCI_HOST_INTS;
#endif
   dspToHostIntrCnt++;
}

#pragma CODE_SECTION (Enable_Interrupt_From_Host, "SLOW_PROG_SECT")
void Enable_Interrupt_From_Host (Fxn HostIsr) {
   int hostIntEvent;    // Event generating interrupt

   hostToDspIntrCnt = 0;
   dspToHostIntrCnt = 0;

   if (sysConfig.HostIF & PCI_IF) {
#ifdef PCI_BASE
      *PCI_DSP_INT_DISABLE = PCI_ALL_INTS;
      *PCI_DSP_INT_ENABLE = PCI_DSP_INTS;  // Enable PCI interrupts at DSP
      hostIntEvent = PCI_INT_EVENT;        // Identify event causing interrupt
#endif
   } else if (sysConfig.HostIF & HPI_IF) {
      hostIntEvent = HPI_INT_EVENT;
   }

   // Identify ISR for HOST generated interrupts
   HWI_dispatchPlug (HWI_HOST, HostIsr, -1, NULL);
   HWI_eventMap     (HWI_HOST, hostIntEvent);
   C64_enableIER (1 << HWI_HOST);
}

#pragma CODE_SECTION (Enable_Interrupt_From_Core0, "SLOW_PROG_SECT")
void Enable_Interrupt_From_Core0 (Fxn frameReadyISR) {

   ACK_Core_Interrupt (0xffffffff, DSPCore);

   // Identify ISR for CORE 0 generated interrupts
   HWI_dispatchPlug (HWI_FRAME_READY, frameReadyISR, -1, NULL);
   HWI_eventMap     (HWI_FRAME_READY, 84);
   C64_enableIER    (1 << HWI_FRAME_READY);
}

#pragma CODE_SECTION (PowerOnDevice, "SLOW_PROG_SECT")
void PowerOnDevice (int DeviceCode) {

   // 1. Wait for previous power transition to complete
   while (*PSC_STAT & 1);

   // 2. Enable clock domains for device code
   PSC_REG[DeviceCode]  = PSC_ON  | PSC_LCL_ENABLE;

   // 3. Transition power
   *PSC_CMD = 1;

   // 4. Wait for power transition to complete
   while (*PSC_STAT & 1);
}

#pragma CODE_SECTION (PowerOnPeripherals, "SLOW_PROG_SECT")
void PowerOnPeripherals () {

   volatile ADT_UInt32 *PscCmd  = PSC_CMD;
   volatile ADT_UInt32 *PscStat = PSC_STAT;
   volatile ADT_UInt32 *PscReg  = PSC_REG;
   int NetworkState = PSC_OFF;

   int v;

   // Map all (expect timer @ 14) hardware interrupt vectors (v) to bus error event.
   // To avoid conflicting assignments when mapping individual peripheral interrupts.
   for (v=4; v<14; v++)  HWI_eventMap (v, 0x7f);
   HWI_eventMap (15, 0x7f);

   if (DSPCore != 0) return;


   if (sysConfig.HostIF & RMII_IF) {
      NetworkState = 3 | PSC_LCL_ENABLE;
   }


    //---------------------------------------------------
    //  Enable power to peripherals.
    
    //  Power to HPI, PCI, and DDR2 must be turned on by host
    //  during download.

    //---------------------------------------------------
    // 1. Wait for previous power transition to complete
    while (*PscStat & 1);


    //---------------------------------------------------
    //  2. Enable clock domains for only those peripherals
    //     used by G.PAK 
    //  a. Ethernet peripherals
    PscReg[PSC_EMAC0] = NetworkState;


    //---------------------------------------------------
    //  3. Transition power state
    *PscCmd = 1;

    //---------------------------------------------------
    //  4. Wait for power transition to complete
    while (*PscStat & 1);

   // Disable timer on frame processing cores
   if (DSPCore != 0) {
      CLK_stop ();
   }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ HostHWI
//
// FUNCTION
//   Hardware interrupt generated by host
//
// Inputs
//   Nothing
//
// RETURNS
//   Nothing
//
//}
#pragma CODE_SECTION (ProcessHostIsr, "FAST_PROG_SECT")
extern void postRtpHostSWI();
void ProcessHostIsr () {

    postRtpHostSWI();

   // Clear interrupt events (PCI or HPI)
   *HPIC |= HPI_DSP_INT;

   hostToDspIntrCnt++;

   // Clear 
   clearInterrupt (HPI_INT_EVENT);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ CACHE_wbInvAllL2 
//
// FUNCTION
//   Trigger write back of 
//
// Inputs
//   CACHE_NO_WAIT or CACHE_WAIT
//
// RETURNS
//   Nothing
//
//}
#pragma CODE_SECTION (CACHE_wbAll, "SLOW_PROG_SECT")
void CACHE_wbAll () {
   BCACHE_wbAll();
}
