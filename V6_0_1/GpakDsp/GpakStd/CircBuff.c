/*
 * Copyright (c) 2001- 2007, Adaptive Digital Technologies, Inc.
 *
 * File Name: CircBuff.c
 *
 * Description:
 *   This file contains the circular buff access routines
 *
 * Version: 1.0
 *
 * Revision History:
 *   8/13/07 - Initial release.
 *
 */
// Application related header files.
#include <std.h>
#include "CircBuff.h"
#include "GpakExts.h"

#if (DSP_TYPE == 54 || DSP_TYPE == 55)
#else
#pragma CODE_SECTION (mmCpy, "FAST_PROG_SECT:IPStack")
void mmCpy (void *restrict pDst, void * restrict pSrc, ADT_UInt32 lenI8) {
   register ADT_UInt8 *pSrc8, *pDst8;
   register ADT_UInt32 lenI64;

   lenI64 = lenI8 / 8;
   pSrc8 = pSrc;
   pDst8 = pDst;

   while (lenI64--) {
#ifdef _TMS320C6700_PLUS
      memcpy (pDst8, pSrc8, 8);
#else
      _mem8 (pDst8) = _mem8 (pSrc8);
#endif
      pDst8 += 8;
      pSrc8 += 8;
   }
   lenI8 &= 7;
   while (lenI8--) *pDst8++ = *pSrc8++;
}
#endif

// ========================================================================
//{ Function Name:   copyCircToLinear
//
// Inputs:          CircBufInfo_t *src
//                  ADT_UInt16 len (16 bit units)
//
// Outputs:         ADT_UInt16 *dest
//
// Returns:         -
//
// Description:     Copies len 16-bit words from *src circular buffer into *dest
//                  linear buffer. Updates Take indices in *src structure.
//
// A slip count of non-zero indicates that extra samples are in the buffer that need to
// be removed.  The extra samples will be discarded as one contiguous block by
// adjusting the take pointer beyond the last discarded sample.
//} ========================================================================
int copyCircToLinear (CircBufInfo_t *src, void *voidDest, ADT_Word sampCnt) {

   ADT_UInt16 *Address;
   ADT_UInt16 *dest;
   ADT_Word   I16sTillEnd, availSamps;
   ADT_UInt16 slipSamps;

   dest = voidDest;

   SWI_disable();
   slipSamps = src->SlipSamps;
   if (slipSamps) {
      src->SlipSamps = 0;
   }
 
   Address = src->pBufrBase + src->TakeIndex;
   availSamps = getAvailable (src);
   LogTransfer (0x200C1, src, Address, 0, dest, sampCnt + slipSamps);
   if (availSamps < (sampCnt + slipSamps)) {
      // Return nulls when buffer does not contain requested samples
      SWI_enable();
      AppErr ("BuffUnderrun", (sampCnt + slipSamps) - availSamps);
      memset (dest, 0, sampCnt * sizeof (ADT_UInt16));
      return 0;
   }

   I16sTillEnd = src->BufrSize - src->TakeIndex;
   if (I16sTillEnd < sampCnt) {
      // Data wraps in circular buffer remove data in two copies
      i16cpy (dest, Address, I16sTillEnd);
      i16cpy (&(dest[I16sTillEnd]), src->pBufrBase, (sampCnt - I16sTillEnd));
   } else {
      i16cpy (dest, Address, sampCnt);
   }

   // Adjust take index by number of samples to be removed (including slipped samples)
   src->TakeIndex = src->TakeIndex + sampCnt + slipSamps;
   if (src->BufrSize <= src->TakeIndex)
      src->TakeIndex -= src->BufrSize;
   SWI_enable();
   return slipSamps;

}

// ========================================================================
//{ Function Name:   copyLinearToCirc
//
// Inputs:          ADT_UInt16 *src
//                  ADT_UInt16 len
//
// Outputs:         CircBufInfo_t *dest
//
// Returns:         Number of samples added to circular buffer to accound for
//                  TDM slips.
//
// Description:     Copies len 16-bit words from *src linear buffer into *dest
//                  circular buffer. Updates Put indices in *dest structure.
//
//  A non-zero slip count indicates that samples must be added into the circular
//  buffer.  The samples will be added as a contiguous block of silence (zeroes)
//} ========================================================================
int copyLinearToCirc (void *voidSrc, CircBufInfo_t *dest, ADT_Word lenPCM) {

   ADT_UInt16 *Address;
   ADT_UInt16 *src;
   ADT_Word   I16sTillEnd;
   ADT_UInt16 slipSamps;

   src = voidSrc;
   SWI_disable();
   slipSamps = dest->SlipSamps;
   dest->SlipSamps = 0;

   Address = dest->pBufrBase + dest->PutIndex;
   if (getFreeSpace (dest) <= (lenPCM + slipSamps)) {
      // Discard samples on overrun
      SWI_enable();
      AppErr ("BuffOverrun", (lenPCM + slipSamps));
      return 0;
   }

   LogTransfer (0x2001C, 0, src, dest, Address, lenPCM + slipSamps);
   if (slipSamps) {
      // Add a block of silence to adjust for the need 
      // of additional data due to a TDM slip
      I16sTillEnd = dest->BufrSize - dest->PutIndex;
      if (I16sTillEnd < slipSamps) {
         // Block of silence wraps within the circular buffer
         memset (Address, 0, I16sTillEnd*sizeof(ADT_UInt16));
         memset (dest->pBufrBase, 0, (slipSamps - I16sTillEnd)*sizeof(ADT_UInt16));
         dest->PutIndex = dest->PutIndex + slipSamps - dest->BufrSize;
      } else {
         memset (Address, 0, slipSamps*sizeof(ADT_UInt16));
         if (slipSamps == I16sTillEnd)
            dest->PutIndex = 0;
         else
            dest->PutIndex = dest->PutIndex + slipSamps;
      }
   }    

   Address = dest->pBufrBase + dest->PutIndex;

   I16sTillEnd = dest->BufrSize - dest->PutIndex;
   if (I16sTillEnd < lenPCM) {
      // Data is wrapped within the circular buffer
      i16cpy (Address, src, I16sTillEnd);
      i16cpy (dest->pBufrBase, &(src[I16sTillEnd]), (lenPCM - I16sTillEnd));
      dest->PutIndex = dest->PutIndex + lenPCM - dest->BufrSize;
   } else {
      i16cpy (Address, src, lenPCM);
      if (lenPCM == I16sTillEnd)
         dest->PutIndex = 0;
      else
         dest->PutIndex = dest->PutIndex + lenPCM;
   }
   SWI_enable();
   return slipSamps;
}

// ========================================================================
//{ Function Name:   addSlipToCirc
//
// Inputs:          CircBufInfo_t *dest
//                  ADT_Word slipCnt
//
// Outputs:         CircBufInfo_t *dest->SlipSamps
//
// Returns:         -
//
// Description:     Adds a slip count in the circular buffer structure to allow
//                  future adjustments for slips in the TDM bus due to different 
//                  rate TDM clocks
//
//  A non-zero slip count indicates that samples must be added into the circular
//  buffer.  The samples will be added as a contiguous block of silence (zeroes)
//} ========================================================================
void addSlipToCirc (CircBufInfo_t *dest, ADT_Word slipCnt) {

   SWI_disable();
   dest->SlipSamps += slipCnt;
   SWI_enable();
}

// Compute how much free space is available in the circular buffer for writing.
// Only allow buffer to be filled to bufLen-1 to resolve ambiguity as to whether
// the buffer is empty or full. (i.e., put == take == empty buffer)
ADT_Word getFreeSpace (CircBufInfo_t *buf) {
   ADT_Word nFree, tempTake;

   tempTake = buf->TakeIndex;
   if (tempTake <= buf->PutIndex)
      nFree = buf->BufrSize - (buf->PutIndex - tempTake) - 1;
   else
      nFree = tempTake - buf->PutIndex - 1;

   return (nFree);
}

// Compute how much data is available in the circular buffer for reading.
ADT_Word getAvailable (CircBufInfo_t *buf) {
   ADT_Word nAvail, tempPut;

   tempPut = buf->PutIndex;
   if (buf->TakeIndex <= tempPut)
      nAvail = tempPut - buf->TakeIndex;
   else
      nAvail = (buf->BufrSize - buf->TakeIndex) + tempPut;

   return (nAvail);
}

