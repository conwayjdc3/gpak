//
//{ GpakTsip.c
//
//  Each TSIP device can support: 
//      2 physical buses of 512 bytes per frame,
//      4 physical buses of 256 bytes per frame, or 
//      8 phyiscal buses of 128 bytes per frame
//  that are multiplexed into a single 'virtual' bus of 1024 bytes per 8 kHz frame.  Selection of the number
//  of pyhsical buses or 'links' is accomplished via the txDataRateN field in the TSIP configuration parameters.
//
//  To avoid mixed 8-bit and 16-bit data elememts, all TDM bus transfers are performed as 8-bit 
//  linear transfers into the linear TDM buffers.
//
//  Channel data may be arranged on the TDM bus in one of four formats: 8-bit narrow band, 8-bit wide band,
//  16-bit narrow band, or 16-bit wide band.  All channel data on all TDM buses must use the same format.
//  Data is transfered between the 8-bit TDM buffers to the 16-bit circular channel buffers using one
//  of four transfer routines selected by the TDM data format.
//
//  Data on TDM devices can be configured to be either in a companded or linear format.  The selection of
//  companded or linear data format is universal across all TDM devices and timeslots.
//
//  When the data is in a companded format, software companding is performed by the framing tasks to
//  convert between 16-bit linear and u-law or a-law companding modes.  Companding is configured by 
//  timeslot groups - 8 slots per group.  Each group of 8 consecutive timeslots can be configured for 
//  either u-law or a-law companding.  During channel setup, channels can be configured as a data channels
//  which will disable the framing task's software companding on those channels.
//}

#include "GpakDefs.h"
#include "GpakExts.h"
#include "64plus.h"
#include "GpakPcm.h"
#include <csl_tsip.h>
#include "GpakHpi.h"
#include <clk.h>
#include <c64.h>
#include <string.h>


//{  #defines
#define TSIP_FRAMES_PER_INTERRUPT   8    // Fixed rate of TSIP.
#define PER_FRAME_OVERHEAD_I8       8    // 32-bit ChanID/Framecnt at beginning and end of frame
#define TSIP_CORE_ID                1    // default dma channel (chan1: core 1)
#define DMA_TRANSFER_PRIORITY       0
#define MAX_DMA_TRANSFER_PRIORITY   0

#define ULAW_ZERO                   0xFF
#define ALAW_ZERO                   0x55

#define TS_INT_DLY      0    // timeslot delay before generating interrupt 0-value == 1 slot delay
#define ACK_ONLY        0
#define ACK_AND_DLY     (0xC000 | TS_INT_DLY)  // generate superframe interrupt on ACK and DLY
#define INT_MODE        ACK_ONLY

#define DMAENB  1   // DMA enable bit in TDMU Global Control
#define CHNENB  1   // CHNENB bit in Dma channel enable register 
#define XMTENB  1   // XMTENB bit in SIU control register
#define RCVENB  2   // RCVENB bit in SIU control register
#define DMARST  2   // DMARST bit in reset register
#define SIURST  1   // SIURST bit in reset register
#define EMUSOFT 2   // SOFT bit of Emulation and Test register

#define  EVTCLR1    (INTR_CTL_BASE + 0x44)  // Clear event register
#define  MEVTFLAG1  (INTR_CTL_BASE + 0xA4)  // Masked event pending register
#define  MEXPFLAG1  (INTR_CTL_BASE + 0xE4)  // Masked exception pending register

#define HWI_TSIP_INT 8     // HWI assigned TSIP events
#define EVT_CMB_1    1     // event combiner output for events 32...63

#define EVT_RSFINT0 0x00000002   // rx superframe0 event 33
#define EVT_XSFINT0 0x00000008   // tx superframe0 event 35
#define EVT_RSFINT1 0x00000020   // rx superframe1 event 37
#define EVT_XSFINT1 0x00000080   // tx superframe1 event 39
#define EVT_RSFINT2 0x00000200   // rx superframe2 event 41
#define EVT_XSFINT2 0x00000800   // tx superframe2 event 43
#define EVT_ERRINT0 0x00040000   // error0 event 50
#define EVT_ERRINT1 0x00100000   // error1 event 52
#define EVT_ERRINT2 0x00400000   // error2 event 54

// Register access macro
#ifdef CHIP_6472
#define TSIP_XCHEN              tsip->XCHEN[TSIP_CORE_ID].XCHEN 
#define TSIP_RCHEN              tsip->RCHEN[TSIP_CORE_ID].RCHEN

#define TSIP_DXCH_ABASE         tsip->DXCH[TSIP_CORE_ID].ABASE
#define TSIP_DXCH_AFALLOC       tsip->DXCH[TSIP_CORE_ID].AFALLOC
#define TSIP_DXCH_AFSIZE        tsip->DXCH[TSIP_CORE_ID].AFSIZE
#define TSIP_DXCH_AFCNT         tsip->DXCH[TSIP_CORE_ID].AFCNT
#define TSIP_DXCH_BBASE         tsip->DXCH[TSIP_CORE_ID].BBASE
#define TSIP_DXCH_BFALLOC       tsip->DXCH[TSIP_CORE_ID].BFALLOC
#define TSIP_DXCH_BFSIZE        tsip->DXCH[TSIP_CORE_ID].BFSIZE
#define TSIP_DXCH_BFCNT         tsip->DXCH[TSIP_CORE_ID].BFCNT

#define TSIP_DRCH_ABASE         tsip->DRCH[TSIP_CORE_ID].ABASE
#define TSIP_DRCH_AFALLOC       tsip->DRCH[TSIP_CORE_ID].AFALLOC
#define TSIP_DRCH_AFSIZE        tsip->DRCH[TSIP_CORE_ID].AFSIZE
#define TSIP_DRCH_AFCNT         tsip->DRCH[TSIP_CORE_ID].AFCNT
#define TSIP_DRCH_BBASE         tsip->DRCH[TSIP_CORE_ID].BBASE
#define TSIP_DRCH_BFALLOC       tsip->DRCH[TSIP_CORE_ID].BFALLOC
#define TSIP_DRCH_BFSIZE        tsip->DRCH[TSIP_CORE_ID].BFSIZE
#define TSIP_DRCH_BFCNT         tsip->DRCH[TSIP_CORE_ID].BFCNT

#define TSIP_XBMA(a)            tsip->XBM[TSIP_CORE_ID].XBMA[a]
#define TSIP_XBMB(a)            tsip->XBM[TSIP_CORE_ID].XBMB[a]
#define TSIP_RBMA(a)            tsip->RBM[TSIP_CORE_ID].RBMA[a]
#define TSIP_RBMB(a)            tsip->RBM[TSIP_CORE_ID].RBMB[a]

#define TSIP_ERRCNT             tsip->ERR[TSIP_CORE_ID].ERRCNT
#define TSIP_ERRQ               tsip->ERR[TSIP_CORE_ID].ERRQ
#define TSIP_ERRCTL             tsip->ERR[TSIP_CORE_ID].ERRCTL
#endif

#ifdef CHIP_6452
#define TSIP_XCHEN              tsip->XCHEN0.CHEN 
#define TSIP_RCHEN              tsip->RCHEN0.CHEN

#define TSIP_DXCH_ABASE         tsip->DXCH0.ABASE
#define TSIP_DXCH_AFALLOC       tsip->DXCH0.AFALLOC
#define TSIP_DXCH_AFSIZE        tsip->DXCH0.AFSIZE
#define TSIP_DXCH_AFCNT         tsip->DXCH0.AFCNT
#define TSIP_DXCH_BBASE         tsip->DXCH0.BBASE
#define TSIP_DXCH_BFALLOC       tsip->DXCH0.BFALLOC
#define TSIP_DXCH_BFSIZE        tsip->DXCH0.BFSIZE
#define TSIP_DXCH_BFCNT         tsip->DXCH0.BFCNT

#define TSIP_DRCH_ABASE         tsip->DRCH0.ABASE
#define TSIP_DRCH_AFALLOC       tsip->DRCH0.AFALLOC
#define TSIP_DRCH_AFSIZE        tsip->DRCH0.AFSIZE
#define TSIP_DRCH_AFCNT         tsip->DRCH0.AFCNT
#define TSIP_DRCH_BBASE         tsip->DRCH0.BBASE
#define TSIP_DRCH_BFALLOC       tsip->DRCH0.BFALLOC
#define TSIP_DRCH_BFSIZE        tsip->DRCH0.BFSIZE
#define TSIP_DRCH_BFCNT         tsip->DRCH0.BFCNT

#define TSIP_XBMA(a)            tsip->XBM0.BMA[a]
#define TSIP_XBMB(a)            tsip->XBM0.BMB[a]
#define TSIP_RBMA(a)            tsip->RBM0.BMA[a]
#define TSIP_RBMB(a)            tsip->RBM0.BMB[a]

#define TSIP_ERRCNT             tsip->ERR0.ERRCNT
#define TSIP_ERRQ               tsip->ERR0.ERRQ
#define TSIP_ERRCTL             tsip->ERR0.ERRCTL
#endif
//}

extern volatile ADT_UInt16 AccumDmaFlags;
extern volatile ADT_UInt16 MatchDmaFlags; 
extern int gblPkLoadingRst;

extern ADT_UInt16 TsipBuflenI8[];
extern ADT_UInt16 TsipFallocI8[];

extern void initDMAPhaseCounters ();
extern void PowerOnDevice (int device);
extern void IncPortRestartStat (int port);


ADT_UInt16 RxEvents [NUM_TDM_PORTS];
ADT_UInt16 TxEvents [NUM_TDM_PORTS];

static void GpakDmaIsr();
static ADT_Bool stopTsip (int port);
static ADT_Bool startTsip (int port);
static ADT_Bool setupTsip (int port);

// dma to channel buffer
typedef void (*DMA_TRANSFER) (void* restrict tdmBuff, ADT_UInt16 tdmOffset, void* restrict frame);
void dmaToCirc8BitNB  (ADT_UInt8*  restrict tdmBuff, ADT_UInt16 tdmFrameOffset, ADT_UInt64* restrict chBuff);
void dmaToCirc8BitWB  (ADT_UInt16* restrict tdmBuff, ADT_UInt16 tdmFrameOffset, ADT_UInt64* restrict chBuff);
void dmaToCirc16BitNB (ADT_UInt16* restrict tdmBuff, ADT_UInt16 tdmFrameOffset, ADT_UInt64* restrict chBuff);
void dmaToCirc16BitWB (ADT_UInt32* restrict tdmBuff, ADT_UInt16 tdmFrameOffset, ADT_UInt64* restrict chBuff);
DMA_TRANSFER dmaToCirc = (DMA_TRANSFER) dmaToCirc8BitNB;

// channel buffer to DMA
void circToDma8BitNB  (ADT_UInt8*  restrict tdmBuff, ADT_UInt16 tdmFrameOffset, ADT_UInt64* restrict chBuff);
void circToDma8BitWB  (ADT_UInt16* restrict tdmBuff, ADT_UInt16 tdmFrameOffset, ADT_UInt64* restrict chBuff);
void circToDma16BitNB (ADT_UInt16* restrict tdmBuff, ADT_UInt16 tdmFrameOffset, ADT_UInt64* restrict chBuff);
void circToDma16BitWB (ADT_UInt32* restrict tdmBuff, ADT_UInt16 tdmFrameOffset, ADT_UInt64* restrict chBuff);
DMA_TRANSFER circToDma = (DMA_TRANSFER) circToDma8BitNB;

static int firstTime = ADT_TRUE;
static int TDMZero = 0;

typedef struct TSIPModeCfg_t {
    ADT_UInt16 tdmSampleSizeI8;   // Bytes per sample on tdm bus (and dma buffer)
    ADT_UInt16 sampleRatekHz;     // Samples per kHz on TDM bus
    ADT_UInt16 compandedData;     // Data on TDM bus is companded
    ADT_UInt16 dmaChannelSizeI8;  // Bytes per channel in dma buffer
    TSIPMode_t mode;
} TSIPModeCfg_t;

#pragma DATA_SECTION (TSIPCfg, "TSIPCFG")
const TSIPModeCfg_t TSIPCfg = {
   1,          // TDM bus bytes per sample
   8,          // Samples per kHz on TDM bus 
   ADT_TRUE,   // TDM bus data is companded
   1,          // Bytes per channel in dma buffer
   tsip8BitNB  // Format of samples on TDM bus
};
TSIPModeCfg_t *tsipCfg = (TSIPModeCfg_t *) &TSIPCfg;  // Allow access to modifiable TSIP structure

//  TSIP configuration parameters
typedef struct TsipParams_t {
    ADT_UInt32      slotMask[32];   // timeslot enable/disable bit masks
    GpakActivation  singleClk;      // when Enabled: frame sync and serial clock signals are shared by the transmit and receive interfaces.
                                    // when Disabled: one framesync and serial clock for transmit, and a second framesync and clock for receive

    fsPolarity      txFsPolarity;   // tx framesync polarity 
    clkEdge         txDataClkEdge;  // clock edge used to clock tx data
    clkEdge         txFsClkEdge;    // clock edge used to sample tx framesync
    clkMode         txClkMode;      // tx clocking mode               (specifies tx and rx when redundant mode is active)
    dataRate        txDataRate;     // tx data rate                   (specifies tx and rx when redundant mode is active)
    clkFsSrc        txClkFsSrc;     // tx clock and frame sync source (specifies tx and rx when redundant mode is active)
    ADT_Int16       txDatDly;       // Transmit Data Delay: 
    txDisState      txDriveState;   // tx data line output drive state for disabled timeslots
    GpakActivation  txOutputDly;    // when Enabled: the tx output of an enabled timeslot (that follows a high-z timeslot) is delayed by 0.5 serial clock period
                                    // when Disabled: output delay is disabled

    fsPolarity      rxFsPolarity;   // rx framesync polarity 
    clkEdge         rxDataClkEdge;  // clock edge used to clock rx data
    clkEdge         rxFsClkEdge;    // clock edge used to sample rx framesync
    clkMode         rxClkMode;      // rx clocking mode (single or double) (only valid when redundant mode is inactive)
    dataRate        rxDataRate;     // rx data rate                        (only valid when redundant mode is inactive)
    clkFsSrc        rxClkFsSrc;     // tx clock and frame sync source      (only valid when redundant mode is inactive)
    ADT_Int16       rxDatDly;       // Receive Data Delay: 
    loopbackMode    loopBack;      // Loopback  mode
} TsipParams_t;
TsipParams_t  TsipParm[NUM_TDM_PORTS];
#pragma DATA_SECTION (TsipParm, "SLOW_DATA_SECT")
#pragma DATA_ALIGN   (TsipParm,  CACHE_L2_LINE_SIZE)

//  TSIP configuration register values
typedef struct  TsipRegs_t {
    ADT_UInt32 EMUTST;
    ADT_UInt32 SIUCTL;
    ADT_UInt32 XCLK, XCTL, XSIZE;
    ADT_UInt32 RCLK, RCTL, RSIZE;
    ADT_UInt32 TDMUCFG;
    ADT_UInt32 DMACTL;
    ADT_UInt32 XDLY;
    ADT_UInt32 RDLY;

    ADT_UInt32 DX_ABASE, DX_AFALLOC, DX_AFSIZE, DX_AFCNT;
    ADT_UInt32 DX_BBASE, DX_BFALLOC, DX_BFSIZE, DX_BFCNT;
    ADT_UInt32 DR_ABASE, DR_AFALLOC, DR_AFSIZE, DR_AFCNT;
    ADT_UInt32 DR_BBASE, DR_BFALLOC, DR_BFSIZE, DR_BFCNT;

    ADT_UInt32 XBMA[64], XBMB[64];
    ADT_UInt32 RBMA[64], RBMB[64];
} TsipRegs_t;
TsipRegs_t  TsipRegs[NUM_TDM_PORTS];
#pragma DATA_SECTION (TsipRegs, "SLOW_DATA_SECT")
#pragma DATA_ALIGN   (TsipRegs,  CACHE_L2_LINE_SIZE)

typedef struct TsipStat_t {
   ADT_UInt32 interFrameErrCnt;

   ADT_UInt32 TxIntCnt;
   ADT_UInt32 TxFrameCnt;
   ADT_UInt32 TxFrameCntSync;
   ADT_UInt32 TxFrameErrCnt;

   ADT_UInt32 RxIntCnt;
   ADT_UInt32 RxFrameCnt;
   ADT_UInt32 RxFrameCntSync;
   ADT_UInt32 RxFrameErrCnt;

   ADT_UInt16 FrameEndOffset;
   ADT_UInt32 LastWarning;

} TsipStat_t;
TsipStat_t TsipStats[NUM_TDM_PORTS];
#pragma DATA_SECTION (TsipStats, "FAST_DATA_SECT:TDM")

typedef struct Tsip_t {
   ADT_UInt32 IntCnt;
   ADT_UInt32 ErrorCnt;
   ADT_UInt32 ErrorEvents;
} Tsip_t;
static Tsip_t Tsip;
#pragma DATA_SECTION (Tsip, "FAST_DATA_SECT:TDM")

static TDM_Port TsipFunctions = { startTsip, stopTsip, setupTsip };

volatile CSL_TsipRegs *Tsip_Base[] = {
   (volatile CSL_TsipRegs *) CSL_TSIP_0_REGS,
   #ifdef CSL_TSIP_1_REGS
   (volatile CSL_TsipRegs *) CSL_TSIP_1_REGS,
   #endif

   #ifdef CSL_TSIP_2_REGS
   (volatile CSL_TsipRegs *) CSL_TSIP_2_REGS,
   #endif
   NULL
};
#define tsipAddr(portN) Tsip_Base[portN]

volatile ADT_UInt16 TsipRxEvents [] = { EVT_RSFINT0, EVT_RSFINT1, EVT_RSFINT2 };
volatile ADT_UInt16 TsipTxEvents [] = { EVT_XSFINT0, EVT_XSFINT1, EVT_XSFINT2 };
volatile ADT_UInt32 TsipErrEvents [] = { EVT_ERRINT0, EVT_ERRINT1, EVT_ERRINT2 };

static ADT_Bool TsipEnable [NUM_TDM_PORTS];

static ADT_UInt16 DmaTxOffset [NUM_TDM_PORTS];   // current Tx DMA buffer offset (ping or pong) for McBSP
static ADT_UInt16 DmaRxOffset [NUM_TDM_PORTS];   // current Rx DMA buffer offset (ping or pong) for McBSP
static ADT_UInt16 DmaPongOffset [NUM_TDM_PORTS];  


static const ADT_UInt8 *TsipTxBufs[NUM_TDM_PORTS] = {
    (ADT_UInt8 *)BSP0DMA_TxBuffer, 
    (ADT_UInt8 *)BSP1DMA_TxBuffer, 
    (ADT_UInt8 *)BSP2DMA_TxBuffer};

static const ADT_UInt8 *TsipRxBufs[NUM_TDM_PORTS] = {
    (ADT_UInt8 *)BSP0DMA_RxBuffer, 
    (ADT_UInt8 *)BSP1DMA_RxBuffer, 
    (ADT_UInt8 *)BSP2DMA_RxBuffer};

typedef struct errQHist_t {
    ADT_UInt32 port;    // tsip port
    ADT_UInt32 errc;    // error code
} errQHist_t;
#ifndef _DEBUG  // Error history logging
   errQHist_t TsipErrQHist;
#else
   #define TSIP_ERRQ_HISTLEN        32
   errQHist_t TsipErrQHist[TSIP_ERRQ_HISTLEN];
   int TsipErrQHistIdx;
   #pragma DATA_SECTION (TsipErrQHistIdx, "NON_CACHED_DATA")
#endif
#pragma DATA_SECTION (TsipErrQHist, "NON_CACHED_DATA")

#ifndef _DEBUG  // Frame count history logging
   #define logFrameCntError(start, end, dmaCnt, intCnt, ptr)
#else
   #define FCNT_HISTLEN 256
   typedef struct fcntHist_t {
       ADT_UInt32 start;
       ADT_UInt32 end;
       ADT_UInt32 dmaCnt;
       ADT_UInt32 intCnt;
       ADT_UInt8 *ptr;
   } fcntHist_t;

   // frame count error history
   fcntHist_t fcntErrHist[FCNT_HISTLEN];
   int fcntErrHistIdx=0;
   #pragma DATA_SECTION (fcntErrHist, "FAST_DATA_SECT")
   #pragma DATA_SECTION (fcntErrHistIdx, "FAST_DATA_SECT")

   inline void logFrameCntError(ADT_UInt32 start, ADT_UInt32 end, ADT_UInt32 dmaCnt, ADT_UInt32 intCnt, ADT_UInt8 *ptr) {
       fcntErrHist[fcntErrHistIdx].start = start;
       fcntErrHist[fcntErrHistIdx].end   = end;
       fcntErrHist[fcntErrHistIdx].dmaCnt = dmaCnt;
       fcntErrHist[fcntErrHistIdx].intCnt = intCnt;
       fcntErrHist[fcntErrHistIdx++].ptr = ptr;
       if (FCNT_HISTLEN <= fcntErrHistIdx) fcntErrHistIdx = 0;
       memset (&fcntErrHist[fcntErrHistIdx], 0xff, sizeof(fcntHist_t));
   }
#endif

#ifndef _DEBUG  // Circular pointer over/underrun checking
   #define CheckTxPointers(circ,dma) 
   #define CheckRxPointers(circ,dma) 
#else
   inline void CheckTxPointers (CircBufInfo_t *circ, ADT_UInt8 *dma) {
      if (circ == pDrain) return;
      LogTransfer (0x100C1, circ, &circ->pBufrBase[circ->TakeIndex], 0, dma, sysConfig.samplesPerMs);
      AppErr ("TxPointerError", (getAvailable(circ) < sysConfig.samplesPerMs)?getAvailable(circ) : ADT_FALSE);
   }
   inline void CheckRxPointers (CircBufInfo_t *circ, ADT_UInt8 *dma) {
      if (circ == pSink) return;
      LogTransfer (0x1001C, 0, dma, circ, &circ->pBufrBase[circ->PutIndex], sysConfig.samplesPerMs);
      AppErr ("RxPointerError", (getFreeSpace (circ) < sysConfig.samplesPerMs)?getFreeSpace(circ) : ADT_FALSE);
   }

   typedef struct TSIPVars_t {
      const TSIPModeCfg_t *TSIPCfg;
      ADT_Bool      (*TsipEnable)[NUM_TDM_PORTS];
      TsipStat_t    (*TsipStats)[NUM_TDM_PORTS];

      volatile CSL_TsipRegs  *(*Tsip_Base)[];
      TsipParams_t  (*TsipParm)[NUM_TDM_PORTS];
      TsipRegs_t    (*TsipRegs)[NUM_TDM_PORTS];

      const ADT_UInt8 *(*TsipTxBufs)[NUM_TDM_PORTS];
      const ADT_UInt8 *(*TsipRxBufs)[NUM_TDM_PORTS];

      ADT_UInt16  (*DmaTxOffset) [NUM_TDM_PORTS];   // current Tx DMA buffer offset (ping or pong) for McBSP
      ADT_UInt16  (*DmaRxOffset) [NUM_TDM_PORTS];   // current Rx DMA buffer offset (ping or pong) for McBSP
      ADT_UInt16  (*DmaPongOffset) [NUM_TDM_PORTS];  

      errQHist_t (*TsipErrQHist)[TSIP_ERRQ_HISTLEN];
      int *TsipErrQHistIdx;

      fcntHist_t (*fcntErrHist)[FCNT_HISTLEN];
      int *fcntErrHistIdx;
      
      Tsip_t *Tsip;
   } TSIPVars_t;

   TSIPVars_t TSIPVars = {
     &TSIPCfg,     &TsipEnable,    &TsipStats, 
     &Tsip_Base,   &TsipParm,      &TsipRegs,
     &TsipTxBufs,  &TsipRxBufs,
     &DmaTxOffset, &DmaRxOffset, &DmaPongOffset,

     &TsipErrQHist, &TsipErrQHistIdx,
     &fcntErrHist,  &fcntErrHistIdx,
     
     &Tsip
   };
#endif

inline void *addCoreOffset (void *addr) {
   ADT_UInt32 intAddr = (ADT_UInt32) addr;

   if ((intAddr & 0xf0000000) == 0) intAddr |= CoreOffsets[DSPCore];
   return (void *) intAddr;
}


//---------------------------------------------------------------
//{ DMA interface routines
// fill in the full Tx DMA buffer with silence
#pragma CODE_SECTION (zeroTxBufferFull, "SLOW_PROG_SECT")
void zeroTxBufferFull (int port) {
   if (NUM_TDM_PORTS <= port) return;

   memset ((void *)TsipTxBufs[port], TDMZero, TsipBuflenI8[port]);
}
// fill in a selected slot's entries in the Tx DMA buffer with silence
#pragma CODE_SECTION (zeroTxBuffer, "SLOW_PROG_SECT")
void zeroTxBuffer (int port, int dmaSlot) { 
   ADT_UInt8 *txSlot;
   int i;

   if (NUM_TDM_PORTS <= port) return;

   // offset to frame0, slotn in pingbuffer accounting for the 4-byte FCNT
   txSlot =  (ADT_UInt8 *) TsipTxBufs[port];
   txSlot += (4 + (dmaSlot * tsipCfg->dmaChannelSizeI8));

   // index through ping and pong buffers
   for (i=0; i<TSIP_FRAMES_PER_INTERRUPT*2; i++) {
      memset (txSlot, TDMZero, tsipCfg->dmaChannelSizeI8);
      txSlot += TsipFallocI8[port];
   }
}

// Make TSIP port's events known to G.PAK
#pragma CODE_SECTION (enableTsipEvents, "SLOW_PROG_SECT")
static ADT_UInt32 enableTsipEvents  (int port) { 
   if (NUM_TDM_PORTS <= port) return 0;

   RxEvents[port] = TsipRxEvents [port];
   TxEvents[port] = TsipTxEvents[port];
   return (RxEvents[port] | TxEvents [port]);

}
// Make TSIP hide port's events from G.PAK
#pragma CODE_SECTION (disableTsipEvents, "SLOW_PROG_SECT")
static void disableTsipEvents (int port) {
    RxEvents[port] = 0;
    TxEvents[port] = 0;
}
#pragma CODE_SECTION (InitTSIPMode, "SLOW_PROG_SECT")
static void InitTSIPMode () { 
   //===================================================
   // Adjust globals according to the
   // download-time TSIP configuration parameters
   //
   // NOTE: The follow can only be run once and must be
   //       run before frame initialization because
   //       of the potential modification of teh sysConfig.samplesPerMs
   //       parameter that is used by framing for data sizing.
   if (tsipCfg->sampleRatekHz == 8) {
      sysConfig.samplesPerMs = 8;
      TDMRate = 8000;
   } else if ((tsipCfg->sampleRatekHz == 16)  && (SysAlgs.wbEnable)) {
      sysConfig.samplesPerMs = 16;
      TDMRate = 16000;
   }

   if (tsipCfg->tdmSampleSizeI8 == 1) {
      if (sysConfig.samplesPerMs == 8) {
         tsipCfg->dmaChannelSizeI8 = 1;
         tsipCfg->mode = tsip8BitNB;
         dmaToCirc = (DMA_TRANSFER) dmaToCirc8BitNB;
         circToDma = (DMA_TRANSFER) circToDma8BitNB;
      } else {
         tsipCfg->dmaChannelSizeI8 = 2;
         tsipCfg->mode = tsip8BitWB;
         dmaToCirc = (DMA_TRANSFER) dmaToCirc8BitWB;
         circToDma = (DMA_TRANSFER) circToDma8BitWB;
      }
   } else if (tsipCfg->tdmSampleSizeI8 == 2) {
      tsipCfg->compandedData = ADT_FALSE;
      if (sysConfig.samplesPerMs == 8) {
         tsipCfg->dmaChannelSizeI8 = 2;
         tsipCfg->mode = tsip16BitNB;
         dmaToCirc = (DMA_TRANSFER) dmaToCirc16BitNB;
         circToDma = (DMA_TRANSFER) circToDma16BitNB;
      } else {
         tsipCfg->dmaChannelSizeI8 = 4;
         tsipCfg->mode = tsip16BitWB;
         dmaToCirc = (DMA_TRANSFER) dmaToCirc16BitWB;
         circToDma = (DMA_TRANSFER) circToDma16BitWB;
      }
   } else {
      tsipCfg->tdmSampleSizeI8 = 1;
      tsipCfg->dmaChannelSizeI8 = 1;
   }
}
#pragma CODE_SECTION (DMAInitialize, "SLOW_PROG_SECT")
void DMAInitialize () { 
   int port;
   volatile ADT_UInt32 mevtflag1;

   if (firstTime) {
      tdmPortFuncs = &TsipFunctions;

#ifdef _DEBUG   // Frame count logging
      fcntErrHistIdx = 0;
      TsipErrQHistIdx = 0;
#endif
      memset (&Tsip, 0, sizeof (Tsip));
      memset (TsipStats, 0, sizeof (TsipStats));
      for (port=0; port<NUM_TDM_PORTS; port++) {


         DmaPongOffset[port] = 0;  

         TsipEnable[port] = 0;

         // add core-offset to Tx and Rx DMA buffer pointers
         if (1 < DSPTotalCores) {
            TsipTxBufs[port] = addCoreOffset ((void *) TsipTxBufs[port]);
            TsipRxBufs[port] = addCoreOffset ((void *) TsipRxBufs[port]);
         }

         // initialize Tx DMA buffers with silence
         zeroTxBufferFull (port);

      }

      PowerOnDevice (PSC_TSIP0);

      #ifdef CSL_TSIP_1_REGS
      PowerOnDevice (PSC_TSIP1);
      #endif

      #ifdef CSL_TSIP_2_REGS
      PowerOnDevice (PSC_TSIP2);
      #endif

      // shutdown All Tsips
      for (port = 0; port <NUM_TDM_PORTS ; port++)
         stopTsip (port);  

      // Enable Tsip interrupts in combiner (a 0-value enables the event)
      REG_WR (INTR_CMB,      0xFFFFFFFF);
      REG_WR (INTR_CMB + 4,  0xFFFFFFFF);
      REG_WR (INTR_CMB + 8,  0xFFFFFFFF);
      REG_WR (INTR_CMB + 12, 0xFFFFFFFF);

      REG_WR (INTR_CMB + 4,  ~TSIP_EVTS);

      // Map TDM TSIPn interrupt completion signal to GpakDmaIsr
      HWI_dispatchPlug (HWI_TSIP_INT, (Fxn) &GpakDmaIsr, -1, NULL);
      HWI_eventMap     (HWI_TSIP_INT, EVT_CMB_1);

      InitTSIPMode ();
      firstTime = ADT_FALSE;
   }

   for (port=0; port<NUM_TDM_PORTS; port++) {
      disableTsipEvents(port);
   }

    // clear flagged events in combined event region 1
    REG_RD (MEVTFLAG1, mevtflag1);
    while (mevtflag1) {
        REG_WR (EVTCLR1, mevtflag1);
        REG_RD (MEVTFLAG1, mevtflag1);
    }; 

    // clear pending HW interrupts from combiner 1's output
   C64_clearIFR  (1 << HWI_TSIP_INT);
   C64_enableIER (1 << HWI_TSIP_INT);
}
#pragma CODE_SECTION (disableDMA, "SLOW_PROG_SECT")
ADT_Events disableDMA (int port)  {
   disableTsipEvents(port);
   zeroTxBufferFull (port);
   return 0; 
}
#pragma CODE_SECTION (enableDMA, "SLOW_PROG_SECT")
ADT_Events enableDMA  (int port)  { 

   TsipStat_t *Tsip;
   Tsip = &TsipStats[port];

   DmaTxOffset[port] = 0;  // Point DMA copy to ping buffers
   DmaRxOffset[port] = 0; 
   DmaPongOffset[port] = TSIP_FRAMES_PER_INTERRUPT * TsipFallocI8[port];

   // Zero statistics
   Tsip->TxIntCnt   = 0;
   Tsip->TxFrameCnt = 0;
   Tsip->TxFrameCntSync = 0;

   Tsip->RxIntCnt   = 0;
   Tsip->RxFrameCnt = 0;
   Tsip->RxFrameCntSync = 0;

   zeroTxBufferFull (port);

   return enableTsipEvents(port); 
}

//}

//---------------------------------------------------------------
//{ TSIP Error handling routines
// Log TSIP errors and send warning to host
#pragma CODE_SECTION (logTsipErr, "SLOW_PROG_SECT")
static void logTsipErr (int port, ADT_UInt32 errQ) {
   ADT_UInt32 errc;
   TsipStat_t *Tsip;

   errc = (errQ >> 24) & 0xFF;
#ifndef _DEBUG   // Error history
   TsipErrQHist.port = port;
   TsipErrQHist.errc = errQ;
#else
   TsipErrQHist[TsipErrQHistIdx].port = port;
   TsipErrQHist[TsipErrQHistIdx].errc = errc;
   TsipErrQHistIdx++;
   if (TSIP_ERRQ_HISTLEN <= TsipErrQHistIdx)    TsipErrQHistIdx = 0;
   TsipErrQHist[TsipErrQHistIdx].port = (ADT_UInt32)-1;
   TsipErrQHist[TsipErrQHistIdx].errc = (ADT_UInt32)-1;
#endif

   Tsip = &TsipStats [port];
   if ((ApiBlock.DmaSwiCnt - Tsip->LastWarning) < 500) return;
   Tsip->LastWarning = ApiBlock.DmaSwiCnt;

   // Error codes as described in 'Exception conditions (sect 2.7.4)' of TSIP.
   switch (errc) {
   case 0x0:     break;
   case 0x01:    SendWarningEvent (port, WarnTsip_1);         break;
   case 0x02:    SendWarningEvent (port, WarnTsip_2);         break;
   case 0x03:    SendWarningEvent (port, WarnTsip_3);         break;
   case 0x05:    SendWarningEvent (port, WarnTsip_5);         break;
   case 0x10:    SendWarningEvent (port, WarnTsip_16);        break;
   case 0x11:    SendWarningEvent (port, WarnTsip_17);        break;
   case 0x12:    SendWarningEvent (port, WarnTsip_18);        break;
   case 0x13:    SendWarningEvent (port, WarnTsip_19);        break;
   case 0x15:    SendWarningEvent (port, WarnTsip_21);        break;

   case 0xF1: case 0xF2: case 0xF3: case 0xF4: 
   case 0xF5: case 0xF6: case 0xF7:  
               SendWarningEvent (port, WarnTsip_RD);   break;
   default:    SendWarningEvent (port, WarnTsip_WR);   break;
   }
}

// Read and process TSIP error register
#pragma CODE_SECTION (readTsipErrors, "SLOW_PROG_SECT")
static ADT_Bool readTsipErrors (int port) {
   ADT_UInt32 errCnt, errQ, i;
   volatile CSL_TsipRegs   *tsip;

   tsip = tsipAddr(port);

   errCnt = TSIP_ERRCNT;
   errCnt &= 0x001F;             // mask error queueu count

   for (i=0; i<errCnt; i++) {
      errQ = TSIP_ERRQ;      // read the top queue entry
      TSIP_ERRCTL = 0x0001;  // pop the top entry off
      logTsipErr (port, errQ);
   }
   TSIP_ERRCTL = 0x00000102;  // clear overflow and queue
   return (errCnt != 0);
}

// Check TSIP errors.  Called by McBSPFrameError
#pragma CODE_SECTION (checkTsipErrors, "SLOW_PROG_SECT")
static ADT_Bool checkTsipErrors() {
    int intrMask;
    ADT_UInt32 tsipErrors;  
    ADT_Bool err = ADT_FALSE;

    // Critical section: disable HW ints, snapshot global error flags, 
    // then clear global error flags
    intrMask = HWI_disable ();
    tsipErrors = Tsip.ErrorEvents;
    Tsip.ErrorEvents = 0;
    HWI_restore (intrMask);

    if (tsipErrors == 0) return 0;
    if (tsipErrors & TsipErrEvents[0]) err |= readTsipErrors(0);
    if (tsipErrors & TsipErrEvents[1]) err |= readTsipErrors(1);
    if (tsipErrors & TsipErrEvents[2]) err |= readTsipErrors(2);

    return err;
}
//}

//---------------------------------------------------------------
//{ TSIP control routines

//  Stop TSIP operation
#pragma CODE_SECTION (stopTsip, "SLOW_PROG_SECT")
static ADT_Bool stopTsip (int port) {
   volatile CSL_TsipRegs *tsip;

   tsip = tsipAddr(port);

   // disable the receive and transmit serial interfaces:
   // SIU Global Control register's RCVENB and XMTENB
   tsip->SIUCTL &= ~(RCVENB | XMTENB);

   // disable receive and transmit timeslot manager on TSIP core's channel
   TSIP_XCHEN = 0;
   TSIP_RCHEN = 0;

   // disable the TDMU Global Control register's DMAENB bits
   tsip->TDMUCTL = 0;

   // place TSIP's DMA and SIU units in reset
   tsip->RST = (DMARST | SIURST); 

   // poll the reset register until reset completes
   while (tsip->RST);
   return ADT_TRUE;
}

//  Write TSIP registers with configured parameters 
#pragma CODE_SECTION (setupTsip, "SLOW_PROG_SECT")
static ADT_Bool setupTsip (int port) {

   volatile CSL_TsipRegs   *tsip;
   TsipRegs_t               *cfg;
   int i;

   if (!TsipEnable[port]) return ADT_FALSE;

   tsip = tsipAddr(port);

   cfg = &TsipRegs[port];

   tsip->EMUTST        = cfg->EMUTST;
   tsip->SIUCTL        = cfg->SIUCTL;
   tsip->XCLK          = cfg->XCLK;
   tsip->XCTL          = cfg->XCTL;
   tsip->XSIZE         = cfg->XSIZE;
   tsip->RCLK          = cfg->RCLK;
   tsip->RCTL          = cfg->RCTL;
   tsip->RSIZE         = cfg->RSIZE;
   tsip->TDMUCFG       = cfg->TDMUCFG;
   tsip->DMACTL        = cfg->DMACTL;
   tsip->XDLY          = cfg->XDLY;
   tsip->RDLY          = cfg->RDLY;

   TSIP_DXCH_ABASE    = cfg->DX_ABASE;
   TSIP_DXCH_AFALLOC  = cfg->DX_AFALLOC;
   TSIP_DXCH_AFSIZE   = cfg->DX_AFSIZE;
   TSIP_DXCH_AFCNT    = cfg->DX_AFCNT;
   TSIP_DXCH_BBASE    = cfg->DX_BBASE;
   TSIP_DXCH_BFALLOC  = cfg->DX_BFALLOC;
   TSIP_DXCH_BFSIZE   = cfg->DX_BFSIZE;
   TSIP_DXCH_BFCNT    = cfg->DX_BFCNT;

   TSIP_DRCH_ABASE    = cfg->DR_ABASE;
   TSIP_DRCH_AFALLOC  = cfg->DR_AFALLOC;
   TSIP_DRCH_AFSIZE   = cfg->DR_AFSIZE;
   TSIP_DRCH_AFCNT    = cfg->DR_AFCNT;
   TSIP_DRCH_BBASE    = cfg->DR_BBASE;
   TSIP_DRCH_BFALLOC  = cfg->DR_BFALLOC;
   TSIP_DRCH_BFSIZE   = cfg->DR_BFSIZE;
   TSIP_DRCH_BFCNT    = cfg->DR_BFCNT;

   for (i=0; i<64; i++) {
      TSIP_XBMA(i) = cfg->XBMA[i];
      TSIP_XBMB(i) = cfg->XBMB[i];
      TSIP_RBMA(i) = cfg->RBMA[i];
      TSIP_RBMB(i) = cfg->RBMB[i];
   }
   return ADT_TRUE;

}

// Start TSIP operation
#pragma CODE_SECTION (startTsip, "SLOW_PROG_SECT")
static ADT_Bool startTsip (int port) {

   volatile CSL_TsipRegs   *tsip;
   volatile ADT_UInt32     reg, xstat, rstat;
   volatile ADT_UInt32 startTime, evtFlags, tout;
   int shift;

   // clear any flagged events on this port
   evtFlags  = enableTsipEvents (port); 
   REG_WR (EVTCLR1, evtFlags);

   tsip = tsipAddr(port);

   // enable the receive transmit units for TSIP core
   TSIP_XCHEN = CHNENB;
   TSIP_RCHEN = CHNENB;

   // start the receive and transmit serial interfaces running        
   reg = tsip->SIUCTL;
   reg |= (RCVENB | XMTENB);
   tsip->SIUCTL = reg;

   // enable TSIP dma unit
   tsip->TDMUCTL = DMAENB;

   // Wait for TSIP to indicate that it on the TSIP core is operating
   logTime (0x00100000ul | 0xDEAD);
   tout =  CLK_countspms();
   startTime = CLK_gethtime ();

   // NOTE: Return TRUE even though timeout occurs.  This will prevent GpakPcm
   //       from treating this in the same fashion as if the TSIP was disabled.
   shift = TSIP_CORE_ID * 2;
   do {
      xstat = (tsip->XCHST) >> shift;
      rstat = (tsip->RCHST) >> shift;
      if (tout < (CLK_gethtime() - startTime)) return ADT_TRUE;
   } while (((xstat & 1) == 0) && ((rstat & 1) == 0));
   return ADT_TRUE;

}
//}

//---------------------------------------------------------------
//{  TSIP configuration routines
//
// cfgChanBitMap modifies:
//  1) Port's SlotMap which linka G.PAK slot IDs to dma slots.
//  2) Port's slot enable/companding map (slotEnableWords).
//
// Returns:
//   size (I8) of active dma buffer.
#pragma CODE_SECTION (cfgChanBitMap, "SLOW_PROG_SECT")
static void cfgChanBitMap (ADT_UInt32 *cmpdWords, // pointer to output buffer where channel's companding bitmap is stored
     ADT_UInt32 *slotEnableWords,      // G.PAK slot enable bitmap (from host configuration message)
     dataRate rate,                    // input: data rate
     ADT_UInt16 *dmaI8,                // output: number of dma bytes in frame
     int port) {                       // input: TDM port

   ADT_UInt32 slotEnableWord, slotEnableBit;
   ADT_UInt32 cmpdWord, cmpdBits, cmpdBits0;

   int slotEnableIdx, cmpdIdx;
   int slotIdx, dmaIdx;

   int slot, link;
   int maxSlots, activeLinks;

   ADT_UInt16 *slotMap;

   slotMap = pSlotMap[port];

   switch (rate) {
   case rate_8Mbps:    activeLinks=8;        maxSlots=128;        break;
   case rate_16Mbps:   activeLinks=4;        maxSlots=256;        break;
   case rate_32Mbps:   activeLinks=2;        maxSlots=512;        break;

   default:    
      AppErr ("Invalid TSIP ", ADT_TRUE);
   };

   slotIdx = 0;
   dmaIdx = 0;

   slotEnableWord = slotEnableWords[0];
   slotEnableIdx  = 0;
   slotEnableBit  = 1;

   // Set companding bits to 8-bit linear or none.
   // G.PAK disallows u-law and a-law hardware companding modes to avoid mixed 8-bit and 16-bit dma slots.
   cmpdWord = 0;;
   cmpdIdx  = 0;
   if (tsipCfg->mode == tsip8BitNB) {
      cmpdBits0  = 0x40000000;
   } else if (tsipCfg->mode == tsip16BitWB) {
      cmpdBits0  = 0x55000000;
   } else {
      cmpdBits0  = 0x50000000;
   }
   cmpdBits = cmpdBits0;

   // Set SlotMap and cmpdMask fields according to value of slotMask
   for (slot=0; slot < maxSlots; slot++) {
      for (link=0; link < activeLinks; link++) {
         if (slotEnableWord & slotEnableBit) {
            cmpdWord |= cmpdBits;
            slotMap[slotIdx++] = dmaIdx++;
         } else {
            slotMap[slotIdx++] = UNCONFIGURED_SLOT;
         }
         slotEnableBit <<= 1;  // One enabling bit per slot.
         cmpdBits  >>= 2;      // Two companding bits per slot.
      }

      // NOTE: The slot enable and companding mask bits will never shift out of 
      //       the 32-bit range in the middle of the link loop, because all possible 
      //       active link values are even divisors of 16 and 32.

      // Move to next word when mask is shifted beyond the high order bit
      if (slotEnableBit == 0) {
         slotEnableBit = 1;
         slotEnableIdx++;

         slotEnableWord = slotEnableWords[slotEnableIdx];  // Get next slot mask word
      }
      if (cmpdBits == 0) {
         cmpdWords[cmpdIdx] = cmpdWord;
         cmpdWord = 0;

         cmpdBits = cmpdBits0;
         cmpdIdx++;
      }
   }

   *dmaI8 = dmaIdx * tsipCfg->dmaChannelSizeI8;
}

// Parse message into configuration parameters
#pragma CODE_SECTION (parseTsipParams, "SLOW_PROG_SECT")
static void parseTsipParams (ADT_UInt16 *cmd, TsipParams_t *tp) {
   int i, idx;
   ADT_UInt32 primaryBitMask;

   if (tsipCfg->dmaChannelSizeI8 == 1) {
      primaryBitMask = 0xffffffff;
   } else if (tsipCfg->dmaChannelSizeI8 == 2) {
      primaryBitMask = 0x55555555;
   } else {
      primaryBitMask = 0x11111111;
   }

    for (i=0,idx=0; i<32; i++, idx+=2) {
        tp->slotMask[i] = primaryBitMask & ((((ADT_UInt32)cmd[idx])<<16) | (ADT_UInt32)cmd[idx+1]);
    }

    tp->singleClk       = (GpakActivation)  (cmd[64] & 0x0001);
    tp->txFsPolarity    = (fsPolarity)     ((cmd[64] >> 1)  & 0x0001); 
    tp->txDataClkEdge   = (clkEdge)        ((cmd[64] >> 2)  & 0x0001); 
    tp->txFsClkEdge     = (clkEdge)        ((cmd[64] >> 3)  & 0x0001);  
    tp->txClkMode       = (clkMode)        ((cmd[64] >> 4)  & 0x0001);    
    tp->txClkFsSrc      = (clkFsSrc)       ((cmd[64] >> 5)  & 0x0001);   
    tp->txOutputDly     = (GpakActivation) ((cmd[64] >> 6)  & 0x0001); 
    tp->rxFsPolarity    = (fsPolarity)     ((cmd[64] >> 7)  & 0x0001);   
    tp->rxDataClkEdge   = (clkEdge)        ((cmd[64] >> 8)  & 0x0001);  
    tp->rxFsClkEdge     = (clkEdge)        ((cmd[64] >> 9)  & 0x0001);    
    tp->rxClkMode       = (clkMode)        ((cmd[64] >> 10) & 0x0001);      
    tp->rxClkFsSrc      = (clkFsSrc)       ((cmd[64] >> 11) & 0x0001);     
    tp->txDriveState    = (txDisState)      (cmd[65] & 0x0003);
    tp->txDataRate      = (dataRate)       ((cmd[65] >> 2) & 0x0003);   
    tp->rxDataRate      = (dataRate)       ((cmd[65] >> 4) & 0x0003);     
    tp->loopBack        = (loopbackMode)   ((cmd[65] >> 6) & 0x0003);     
    tp->txDatDly        = (ADT_Int16) cmd[66];     
    tp->rxDatDly        = (ADT_Int16) cmd[67];       

    if (tp->singleClk == Enabled) {
        tp->rxClkMode       = tp->txClkMode;
        tp->rxClkFsSrc      = tp->txClkFsSrc;
        tp->rxDataRate      = tp->txDataRate;
    }
}

// Store configuration parameters in TSIP structure
#pragma CODE_SECTION (storeTsipConfiguration, "SLOW_PROG_SECT")
static int storeTsipConfiguration (int port) {

   TsipParams_t *parm;
   TsipRegs_t *cfg;
   ADT_UInt16 dmaI8;
   ADT_UInt32 dmaFrameI8;

    cfg  = &TsipRegs[port];
    parm = &TsipParm[port]; 

    // Setup the A-context transmit/receive channel bitmaps
    cfgChanBitMap (cfg->XBMA, parm->slotMask, parm->txDataRate, &dmaI8, port);
    memcpy (cfg->RBMA, cfg->XBMA, sizeof(cfg->RBMA));

    // B-context not used.
    memset (cfg->XBMB, 0, sizeof(cfg->XBMB));
    memset (cfg->RBMB, 0, sizeof(cfg->RBMB));

    // Emulation and Test ------------------------------------------------------
    if (parm->loopBack == internalLoopback)       cfg->EMUTST  = 0x00010000; // enable internal digital loopback
    else if (parm->loopBack == externalLoopback)  cfg->EMUTST  = 0x00030000; // enable external link loopback
    else                                          cfg->EMUTST  = EMUSOFT;

    // Serial Unit Interface (SIU) Global Control ------------------------------
    cfg->SIUCTL  = 0;                   // Disable Rx/Tx
    if (parm->singleClk == Disabled)    cfg->SIUCTL = (1<<4);

    // Transmit Clock Source ---------------------------------------------------
    cfg->XCLK = 0;
    if (parm->txClkFsSrc == clkFsB)     cfg->XCLK = 1;

    // Transmit Control --------------------------------------------------------
    cfg->XCTL = 0;
    if (parm->txClkMode == clkSingleRate)       cfg->XCTL = 1;

    if (parm->txDataRate == rate_16Mbps)        cfg->XCTL |= (1<<1);
    else if (parm->txDataRate == rate_32Mbps)   cfg->XCTL |= (2<<1);

    if (parm->txDataClkEdge == clkFalling)      cfg->XCTL |= (1<<5);
    if (parm->txFsClkEdge == clkFalling)        cfg->XCTL |= (1<<6);
    if (parm->txFsPolarity == fsActHigh)        cfg->XCTL |= (1<<7);

    if (parm->txDriveState == low)              cfg->XCTL |= (2<<8);
    else if (parm->txDriveState == high)        cfg->XCTL |= (3<<8);

    if (parm->txOutputDly == Enabled)           cfg->XCTL |= (1<<10);

    cfg->XCTL |= ((parm->txDatDly & 0x3FFF) << 16);

    // Transmit Size -----------------------------------------------------------
    cfg->XSIZE  = (dmaI8 - 1)  & 0x7F;                      // active slot count
    cfg->XSIZE |= ((TSIP_FRAMES_PER_INTERRUPT - 1) << 16);  // frame count

    // Receive Clock Source ----------------------------------------------------
    cfg->RCLK = 0;
    if (parm->rxClkFsSrc == clkFsB)     cfg->RCLK = 1;

    // Receive Control ---------------------------------------------------------
    cfg->RCTL = 0;
    if (parm->rxClkMode == clkSingleRate)       cfg->RCTL = 1;

    if (parm->rxDataRate == rate_16Mbps)        cfg->RCTL |= (1<<1);
    else if (parm->rxDataRate == rate_32Mbps)   cfg->RCTL |= (2<<1);

    if (parm->rxDataClkEdge == clkFalling)      cfg->RCTL |= (1<<5);
    if (parm->rxFsClkEdge == clkFalling)        cfg->RCTL |= (1<<6);
    if (parm->rxFsPolarity == fsActHigh)        cfg->RCTL |= (1<<7);

    cfg->RCTL |= ((parm->rxDatDly & 0x3FFF) << 16);

    // Receive Size ------------------------------------------------------------
    cfg->RSIZE  = (dmaI8 - 1)  & 0x7F;                      // active slot count
    cfg->RSIZE |= ((TSIP_FRAMES_PER_INTERRUPT - 1) << 16);  // frame count

    // TDMU Global Configuration -----------------------------------------------
    cfg->TDMUCFG = 0;   // Little endian (N/A for G.PAK since linear 16 companding is not supported)

    // DMATCU Global Control ---------------------------------------------------
    cfg->DMACTL = DMA_TRANSFER_PRIORITY | (MAX_DMA_TRANSFER_PRIORITY << 4);

    // Interrupt Selection/Delay -----------------------------------------------
    cfg->XDLY = INT_MODE;
    cfg->RDLY = INT_MODE;

    // Because the dma transfer size is 32-bits, the smallest possible
    // storage for a single frame of data is 12 bytes: 8 bytes for CID/FRCNT, 
    // plus 4 bytes for 1, 2, 3, or 4 timeslots). 
    // Increase the framesize to be a multiple of 4 if it's not already.
    dmaFrameI8 = dmaI8 + PER_FRAME_OVERHEAD_I8;
    dmaFrameI8 = (dmaFrameI8 + 3) & ~3;    

    // A-Context transmit registers: base, frame allocation, frame size, frame count
    cfg->DX_ABASE    =  (ADT_UInt32) TsipTxBufs[port];
    cfg->DX_AFALLOC  =  TsipFallocI8[port];
    cfg->DX_AFSIZE   =  dmaFrameI8;
    cfg->DX_AFCNT    =  TSIP_FRAMES_PER_INTERRUPT * 2; // dma wraps back to start after two super-frames

    // A-Context receive registers: base, frame allocation, frame size, frame count
    cfg->DR_ABASE    =  (ADT_UInt32) TsipRxBufs[port];
    cfg->DR_AFALLOC  =  TsipFallocI8[port];
    cfg->DR_AFSIZE   =  dmaFrameI8;
    cfg->DR_AFCNT    =  TSIP_FRAMES_PER_INTERRUPT * 2; // dma wraps back to start after two super-frames

    // B-context are not used
    // B-Context transmit registers: base, frame allocation, frame size, frame count
    cfg->DX_BBASE    =  (ADT_UInt32) TsipTxBufs[port];
    cfg->DX_BFALLOC  =  TsipFallocI8[port];
    cfg->DX_BFSIZE   =  dmaFrameI8;
    cfg->DX_BFCNT    =  TSIP_FRAMES_PER_INTERRUPT * 2;

    // B-Context receive registers: base, frame allocation, frame size, frame count
    cfg->DR_BBASE    =  (ADT_UInt32) TsipRxBufs[port];
    cfg->DR_BFALLOC  =  TsipFallocI8[port];
    cfg->DR_BFSIZE   =  dmaFrameI8;
    cfg->DR_BFCNT    =  TSIP_FRAMES_PER_INTERRUPT * 2;
    return dmaFrameI8 - 4;
}

// Validate TSIP rate field value
#pragma CODE_SECTION (validTsipRate, "FAST_PROG_SECT")
static inline int validTsipRate (dataRate rate) {
    return ((rate == rate_8Mbps) || (rate == rate_16Mbps) || (rate == rate_32Mbps));
}

// Validate inactive slot transmission state value
#pragma CODE_SECTION (validTsipTxDis, "SLOW_PROG_SECT")
static inline int validTsipTxDis (txDisState state) {
    return ((state == highz) || (state == low) || (state == high));
}

// Count primary TSIP slots
#pragma CODE_SECTION (countTsipSlots, "SLOW_PROG_SECT")
static ADT_UInt16 countTsipSlots (ADT_UInt32 slotMask[]) {

   ADT_UInt16 cnt;
   ADT_UInt32 Mask;
   int i,k;

    cnt = 0;
    for (i=0; i<32; i++) {
        Mask = slotMask[i];
        for (k=0; k<32; k++) {
            if ((Mask & 1) != 0) cnt++;
            Mask >>= 1;
        }
    }
    return cnt;
}

// Process TSIP configuration message
//  Parse message, verify parameters, stop TSIPs, configure TSIP registers, restart TSIPs
#pragma CODE_SECTION (ProcConfigSerialPortsMsg, "SLOW_PROG_SECT")
GPAK_PortConfigStat_t ProcConfigSerialPortsMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {

   int i, j, cmdIdx;
   ADT_UInt32 startDmaCnt;
   ADT_UInt32 startTime;
   TsipStat_t *Tsip;

   TsipParams_t *tp;
   ADT_UInt16 tsipSlotCnt[NUM_TDM_PORTS];
   ADT_UInt16 *SlotMap;

   // Prepare the reply message.
   pReply[0] |= (MSG_CONFIG_PORTS_REPLY << 8);

   // Make sure there are no channels currently active.
   if (NumActiveChannels != 0)
      return Pc_ChannelsActive;

    // Parse TSIP parameters.
    for (i=0, cmdIdx=2; i<NUM_TDM_PORTS; i++) {
        TsipEnable[i] =  pCmd[1] & (1<<i);

        tsipSlotCnt[i] = 0;
        if (TsipEnable[i]) {
            tp = &TsipParm[i];
            parseTsipParams (&pCmd[cmdIdx], tp);
            if (!validTsipRate(tp->txDataRate))     return (GPAK_PortConfigStat_t) ((int) Pc_NoSlots0 + i);
            if (!validTsipRate(tp->rxDataRate))     return (GPAK_PortConfigStat_t) ((int) Pc_NoSlots0 + i);
            if (!validTsipTxDis(tp->txDriveState))  return (GPAK_PortConfigStat_t) ((int) Pc_NoSlots0 + i);
            tsipSlotCnt[i] = countTsipSlots (tp->slotMask);

            if (MaxDmaSlots[i] < tsipSlotCnt[i])    return (GPAK_PortConfigStat_t) ((int) Pc_TooManySlots0 + i);
            if (tsipSlotCnt[i] == 0)                return (GPAK_PortConfigStat_t) ((int) Pc_NoSlots0 + i);
        }
        cmdIdx += 68;   // numI16 elements per tsip in cmd buffer
    }

    // Copy companding modes into SlotCompandMode array
    for (i=0, cmdIdx=206; i<NUM_TDM_PORTS; i++) {
        for (j=0; j<4; j++,cmdIdx+=2)
            SlotCompandMode[i][j] = (((ADT_UInt32)pCmd[cmdIdx])<<16)   | (ADT_UInt32)pCmd[cmdIdx+1];
        UseSlotCmpMask[i] = tsipCfg->compandedData;
    }

    // Zero source buffers
    for (i=0; i<SINK_DRAIN_LEN_I16; i++)
       drainBuff[i] = ULAW_ZERO;

    // Store TSIP parameters in register format
    for (i=0; i<NUM_TDM_PORTS;i++) {
        Tsip = &TsipStats [i];
        if (TsipEnable[i]) {
            Tsip->FrameEndOffset = storeTsipConfiguration(i);
        } else {
            SlotMap = pSlotMap[i];
            for (j=0; j<SltsPerFrame[i]; j++) SlotMap[j] = UNCONFIGURED_SLOT;
            Tsip->FrameEndOffset = 0;
        }
    }

    // clear pending HW interrupts from combiner 1's output
    C64_clearIFR  (1 << HWI_TSIP_INT);
    C64_enableIER (1 << HWI_TSIP_INT);

    // Startup TDM buses
    StartGpakPcm (&tsipSlotCnt[0]);

    //===============================================================
    // Wait up to 10 ms for verification that the DMA has started.
    startTime = CLK_gethtime ();
    startDmaCnt = ApiBlock.DmaSwiCnt;

    if (MatchDmaFlags == 0) 
        return Pc_Success;
    do {
        if (2 <= (ADT_Int32) (ApiBlock.DmaSwiCnt - startDmaCnt)) return Pc_Success;
    } while ((CLK_gethtime() - startTime) < (CLK_countspms() * 10));

    StopSerialPortIo ();
    return Pc_NoInterrupts;
}
//}

//---------------------------------------------------------------
//{ TSIP dma<->circ transfer routines
// Check frame counters (located between DMA frame buffers) against expected values
static int inline validateTxFrameCnt (int port, ADT_UInt8 *buff) {
   int i;
   int frameError = 0;
   TsipStat_t *Tsip;

   ADT_UInt32 FrameCntAddr;
   ADT_UInt32 FrameCnt, NextFrameCnt;
   ADT_UInt16 frameI8 = TsipFallocI8[port];

   Tsip = &TsipStats [port];

   FrameCntAddr = (ADT_UInt32) buff + Tsip->FrameEndOffset;
   FrameCnt = *((ADT_UInt32 *)FrameCntAddr) & 0x00FFFFFF;

   if (Tsip->TxFrameCntSync == 0) {
      Tsip->TxFrameCnt = FrameCnt;
   }

   NextFrameCnt = Tsip->TxFrameCnt;

   // Loop through dma buffer and verify that end frame count values
   // match expected values.
   for (i=0; i<TSIP_FRAMES_PER_INTERRUPT; i++) {
      FrameCnt = *((ADT_UInt32 *)FrameCntAddr)   & 0x00FFFFFF;
      frameError |= (int)(NextFrameCnt - FrameCnt);

      FrameCntAddr += frameI8;

      NextFrameCnt = (FrameCnt + 1) & 0x00FFFFFF;            
   }

   // save the expected frame count
   Tsip->TxFrameCnt = NextFrameCnt;
   Tsip->TxFrameCntSync++;

   if (!frameError) return 0;

   logFrameCntError (FrameCnt, NextFrameCnt, ApiBlock.DmaSwiCnt, Tsip->TxIntCnt, buff);

   Tsip->TxFrameErrCnt++;
   Tsip->TxFrameCntSync  = 0;

   if ((ApiBlock.DmaSwiCnt - Tsip->LastWarning) < 500) return 1;
   Tsip->LastWarning = ApiBlock.DmaSwiCnt;
   SendWarningEvent (port, WarnTsipTxFcntErr);
   return 1;
}
static int inline validateRxFrameCnt (int port, ADT_UInt8 *buff) {
   int i;
   int frameError = 0;
   TsipStat_t *Tsip;

   ADT_UInt32 FrameCntStartAddr, FrameCntEndAddr;
   ADT_UInt32 FrameCnt, NextFrameCnt;
   ADT_UInt16 frameI8 = TsipFallocI8[port];
      
   Tsip = &TsipStats [port];

   FrameCntStartAddr = (ADT_UInt32) buff;
   FrameCntEndAddr   = (ADT_UInt32) buff + Tsip->FrameEndOffset;

   FrameCnt = (*((ADT_UInt32 *)FrameCntStartAddr)) & 0x00FFFFFF;
   if (Tsip->RxFrameCntSync == 0) {
      Tsip->RxFrameCnt = FrameCnt;
   }
   NextFrameCnt = Tsip->RxFrameCnt;

   // verify that the current start count is in proper sequence from
   // previous interrupt.
   if (FrameCnt != NextFrameCnt) {
      frameError = 1;
      logFrameCntError (FrameCnt, NextFrameCnt, ApiBlock.DmaSwiCnt, Tsip->RxIntCnt, buff);
      Tsip->interFrameErrCnt++;
   }

   // Loop through dma buffer and verify that start and end frame count values match
   for (i=0; i<TSIP_FRAMES_PER_INTERRUPT; i++) {
      FrameCnt = (*((ADT_UInt32 *)FrameCntStartAddr)) & 0x00FFFFFF;
      frameError |= (int)(NextFrameCnt - FrameCnt);
      FrameCnt = (*((ADT_UInt32 *)FrameCntEndAddr)) & 0x00FFFFFF;
      frameError |= (int)(NextFrameCnt - FrameCnt);

      FrameCntStartAddr += frameI8;
      FrameCntEndAddr   += frameI8;
      NextFrameCnt = (FrameCnt + 1) & 0x00FFFFFF;            
   }

   // save the expected frame count
   Tsip->RxFrameCnt = NextFrameCnt;
   Tsip->RxFrameCntSync++;

   if (!frameError) return 0;

   logFrameCntError (FrameCnt, NextFrameCnt, ApiBlock.DmaSwiCnt, Tsip->RxIntCnt, buff);

   Tsip->RxFrameErrCnt++;
   Tsip->RxFrameCntSync = 0;

   if ((ApiBlock.DmaSwiCnt - Tsip->LastWarning) < 500) return 1;
   Tsip->LastWarning = ApiBlock.DmaSwiCnt;
   SendWarningEvent (port, WarnTsipRxFcntErr);
   return 1;
}

ADT_UInt16 kHzTone [] = { 0xAB, 0xA3, 0xAB, 0xFF, 0x2B, 0x23, 0x2B, 0xFF  };

#pragma CODE_SECTION (toneToCirc, "FAST_PROG_SECT")
void toneToCirc (ADT_UInt8* restrict tdmBuff, ADT_UInt16 tdmFrameOffset, ADT_UInt16* restrict chBuff) {
   i16cpy (chBuff, kHzTone, 8);
}

#pragma CODE_SECTION (dmaToCircNOOP, "FAST_PROG_SECT")
void dmaToCircNOOP (ADT_UInt8* restrict tdmBuff, ADT_UInt16 tdmFrameOffset, ADT_UInt64* restrict chBuff) {}

// 8-bit narrowband DMA to channel buffer
#pragma CODE_SECTION (dmaToCirc8BitNB, "FAST_PROG_SECT")
void dmaToCirc8BitNB (ADT_UInt8* restrict tdmBuff, ADT_UInt16 tdmFrameOffset, ADT_UInt64* restrict chBuff) {
   // Read one byte from tdm buffer.  Append to 64 bit register.   Store as 64 bit values.
   register ADT_UInt64 chData;

   chData =   (ADT_UInt64) *tdmBuff;          tdmBuff += tdmFrameOffset;
   chData |= ((ADT_UInt64) *tdmBuff) << 16;   tdmBuff += tdmFrameOffset;
   chData |= ((ADT_UInt64) *tdmBuff) << 32;   tdmBuff += tdmFrameOffset;
   _amem8 (chBuff++) = chData | ((ADT_UInt64) *tdmBuff) << 48;   tdmBuff += tdmFrameOffset;

   chData =   (ADT_UInt64) *tdmBuff;          tdmBuff += tdmFrameOffset;
   chData |= ((ADT_UInt64) *tdmBuff) << 16;   tdmBuff += tdmFrameOffset;
   chData |= ((ADT_UInt64) *tdmBuff) << 32;   tdmBuff += tdmFrameOffset;
   _amem8 (chBuff) = chData | ((ADT_UInt64) *tdmBuff) << 48;

}
// 8-bit wideband DMA to channel buffer
#pragma CODE_SECTION (dmaToCirc8BitWB, "FAST_PROG_SECT")
void dmaToCirc8BitWB (ADT_UInt16* restrict tdmBuff, ADT_UInt16 tdmFrameOffset, ADT_UInt64* restrict chBuff) {
   // Read two bytes from tdm buffer.  Split into two 16-bit halves of a 32-bit register. Append to 64 bit register.  Store as 64 bit values.
   register ADT_UInt32 tdmValue;
   register ADT_UInt64 chData;

   tdmValue = *tdmBuff;   chData =   (ADT_UInt64) _unpklu4 (tdmValue);                              tdmBuff += tdmFrameOffset;
   tdmValue = *tdmBuff;   _amem8 (chBuff++) = chData | ((ADT_UInt64) _unpklu4 (tdmValue)) << 32;    tdmBuff += tdmFrameOffset;

   tdmValue = *tdmBuff;   chData =   (ADT_UInt64) _unpklu4 (tdmValue);                              tdmBuff += tdmFrameOffset;
   tdmValue = *tdmBuff;   _amem8 (chBuff++) = chData | ((ADT_UInt64) _unpklu4 (tdmValue)) << 32;    tdmBuff += tdmFrameOffset;

   tdmValue = *tdmBuff;   chData =   (ADT_UInt64) _unpklu4 (tdmValue);                              tdmBuff += tdmFrameOffset;
   tdmValue = *tdmBuff;   _amem8 (chBuff++) = chData | ((ADT_UInt64) _unpklu4 (tdmValue)) << 32;    tdmBuff += tdmFrameOffset;

   tdmValue = *tdmBuff;   chData =   (ADT_UInt64) _unpklu4 (tdmValue);                              tdmBuff += tdmFrameOffset;
   tdmValue = *tdmBuff;   _amem8 (chBuff)   = chData | ((ADT_UInt64) _unpklu4 (tdmValue)) << 32;
}
// 16-bit narrowband DMA to channel buffer
#pragma CODE_SECTION (dmaToCirc16BitNB, "FAST_PROG_SECT")
void dmaToCirc16BitNB (ADT_UInt16* restrict tdmBuff, ADT_UInt16 tdmFrameOffset, ADT_UInt64* restrict chBuff) {
   // Read two bytes from tdm buffer.  Append to 64 bit register.  Store as 64 bit values.
   register ADT_UInt64 chData;

   chData =   (ADT_UInt64) *tdmBuff;          tdmBuff += tdmFrameOffset;
   chData |= ((ADT_UInt64) *tdmBuff) << 16;   tdmBuff += tdmFrameOffset;
   chData |= ((ADT_UInt64) *tdmBuff) << 32;   tdmBuff += tdmFrameOffset;
   _amem8 (chBuff++) = chData | ((ADT_UInt64) *tdmBuff) << 48;   tdmBuff += tdmFrameOffset;

   chData =   (ADT_UInt64) *tdmBuff;          tdmBuff += tdmFrameOffset;
   chData |= ((ADT_UInt64) *tdmBuff) << 16;   tdmBuff += tdmFrameOffset;
   chData |= ((ADT_UInt64) *tdmBuff) << 32;   tdmBuff += tdmFrameOffset;
   _amem8 (chBuff) = chData | ((ADT_UInt64) *tdmBuff) << 48;
}
// 16-bit wideband DMA to channel buffer
#pragma CODE_SECTION (dmaToCirc16BitWB, "FAST_PROG_SECT")
void dmaToCirc16BitWB (ADT_UInt32* restrict tdmBuff, ADT_UInt16 tdmFrameOffset, ADT_UInt64* restrict chBuff) {
   // Read four bytes from tdm buffer.  Append to 64 bit register.  Store as 64 bit values.
   register ADT_UInt64 chData;

   chData =   (ADT_UInt64) *tdmBuff;                             tdmBuff += tdmFrameOffset;
   _amem8 (chBuff++) = chData | ((ADT_UInt64) *tdmBuff) << 32;   tdmBuff += tdmFrameOffset;

   chData =   (ADT_UInt64) *tdmBuff;                             tdmBuff += tdmFrameOffset;
   _amem8 (chBuff++) = chData | ((ADT_UInt64) *tdmBuff) << 32;   tdmBuff += tdmFrameOffset;

   chData =   (ADT_UInt64) *tdmBuff;                             tdmBuff += tdmFrameOffset;
   _amem8 (chBuff++) = chData | ((ADT_UInt64) *tdmBuff) << 32;   tdmBuff += tdmFrameOffset;

   chData =   (ADT_UInt64) *tdmBuff;                             tdmBuff += tdmFrameOffset;
   _amem8 (chBuff)   = chData | ((ADT_UInt64) *tdmBuff) << 32;
}


// channel buffer to 8-bit narrowband DMA
#pragma CODE_SECTION (circToDma8BitNB, "FAST_PROG_SECT")
void circToDma8BitNB (ADT_UInt8* restrict tdmBuff, ADT_UInt16 tdmFrameOffset, ADT_UInt64* restrict chBuff) {
   // Read channel data into 64 bit register.  Copy low byte of each successive 16-bit value into 8-bit tdmbuffer.
   register ADT_UInt64 chData;
   chData = _amem8 (chBuff++);
   *tdmBuff = (ADT_UInt8)  (chData & 0xff);          tdmBuff += tdmFrameOffset;
   *tdmBuff = (ADT_UInt8) ((chData >> 16) & 0xff);   tdmBuff += tdmFrameOffset;
   *tdmBuff = (ADT_UInt8) ((chData >> 32) & 0xff);   tdmBuff += tdmFrameOffset;
   *tdmBuff = (ADT_UInt8) ((chData >> 48) & 0xff);   tdmBuff += tdmFrameOffset;

   chData = _amem8 (chBuff);
   *tdmBuff = (ADT_UInt8)  (chData & 0xff);          tdmBuff += tdmFrameOffset;
   *tdmBuff = (ADT_UInt8) ((chData >> 16) & 0xff);   tdmBuff += tdmFrameOffset;
   *tdmBuff = (ADT_UInt8) ((chData >> 32) & 0xff);   tdmBuff += tdmFrameOffset;
   *tdmBuff = (ADT_UInt8) ((chData >> 48) & 0xff);
}
// channel buffer to 8-bit wideband DMA
#pragma CODE_SECTION (circToDma8BitWB, "FAST_PROG_SECT")
void circToDma8BitWB (ADT_UInt16* restrict tdmBuff, ADT_UInt16 tdmFrameOffset, ADT_UInt64* restrict chBuff) {
   // Read channel data into 64 bit register.  Merge low bytes of two successive 16-bits into 16-bit tdmbuffer.
   register ADT_UInt32 tdmValue;
   register ADT_UInt64 chData;

   chData = _amem8 (chBuff++);
   tdmValue = (ADT_UInt32)  (chData & 0x00ff00ff);         tdmValue |= (tdmValue >> 8); *tdmBuff = tdmValue; tdmBuff += tdmFrameOffset;
   tdmValue = (ADT_UInt32) ((chData >> 32) & 0x00ff00ff);  tdmValue |= (tdmValue >> 8); *tdmBuff = tdmValue; tdmBuff += tdmFrameOffset;

   chData = _amem8 (chBuff++);
   tdmValue = (ADT_UInt32)  (chData & 0x00ff00ff);         tdmValue |= (tdmValue >> 8); *tdmBuff = tdmValue; tdmBuff += tdmFrameOffset;
   tdmValue = (ADT_UInt32) ((chData >> 32) & 0x00ff00ff);  tdmValue |= (tdmValue >> 8); *tdmBuff = tdmValue; tdmBuff += tdmFrameOffset;

   chData = _amem8 (chBuff++);
   tdmValue = (ADT_UInt32)  (chData & 0x00ff00ff);         tdmValue |= (tdmValue >> 8); *tdmBuff = tdmValue; tdmBuff += tdmFrameOffset;
   tdmValue = (ADT_UInt32) ((chData >> 32) & 0x00ff00ff);  tdmValue |= (tdmValue >> 8); *tdmBuff = tdmValue; tdmBuff += tdmFrameOffset;

   chData = _amem8 (chBuff);
   tdmValue = (ADT_UInt32)  (chData & 0x00ff00ff);         tdmValue |= (tdmValue >> 8); *tdmBuff = tdmValue; tdmBuff += tdmFrameOffset;
   tdmValue = (ADT_UInt32) ((chData >> 32) & 0x00ff00ff);  tdmValue |= (tdmValue >> 8); *tdmBuff = tdmValue; 
}
// channel buffer to 16-bit narrowband DMA
#pragma CODE_SECTION (circToDma16BitNB, "FAST_PROG_SECT")
void circToDma16BitNB (ADT_UInt16* restrict tdmBuff, ADT_UInt16 tdmFrameOffset, ADT_UInt64* restrict chBuff) {
   // Read channel data into 64 bit register.  Copy each successive 16-bit value into 16-bit tdmbuffer.
   register ADT_UInt64 chData;
   chData = _amem8 (chBuff++);
   *tdmBuff = (ADT_UInt16)  (chData & 0xffff);          tdmBuff += tdmFrameOffset;
   *tdmBuff = (ADT_UInt16) ((chData >> 16) & 0xffff);   tdmBuff += tdmFrameOffset;
   *tdmBuff = (ADT_UInt16) ((chData >> 32) & 0xffff);   tdmBuff += tdmFrameOffset;
   *tdmBuff = (ADT_UInt16) ((chData >> 48) & 0xffff);   tdmBuff += tdmFrameOffset;

   chData = _amem8 (chBuff);
   *tdmBuff = (ADT_UInt16)  (chData & 0xffff);          tdmBuff += tdmFrameOffset;
   *tdmBuff = (ADT_UInt16) ((chData >> 16) & 0xffff);   tdmBuff += tdmFrameOffset;
   *tdmBuff = (ADT_UInt16) ((chData >> 32) & 0xffff);   tdmBuff += tdmFrameOffset;
   *tdmBuff = (ADT_UInt16) ((chData >> 48) & 0xffff);

}
// channel buffer to 16-bit wideband DMA
#pragma CODE_SECTION (circToDma16BitWB, "FAST_PROG_SECT")
void circToDma16BitWB (ADT_UInt32* restrict tdmBuff, ADT_UInt16 tdmFrameOffset, ADT_UInt64* restrict chBuff) {
   // Read channel data into 64 bit register.  Copy each successive 32-bit value into 32-bit tdmbuffer.
   register ADT_UInt64 chData;
   chData = _amem8 (chBuff++);
   *tdmBuff = (ADT_UInt32)  chData;          tdmBuff += tdmFrameOffset;
   *tdmBuff = (ADT_UInt32) (chData >> 32);   tdmBuff += tdmFrameOffset;

   chData = _amem8 (chBuff++);
   *tdmBuff = (ADT_UInt32)  chData;          tdmBuff += tdmFrameOffset;
   *tdmBuff = (ADT_UInt32) (chData >> 32);   tdmBuff += tdmFrameOffset;

   chData = _amem8 (chBuff++);
   *tdmBuff = (ADT_UInt32)  chData;          tdmBuff += tdmFrameOffset;
   *tdmBuff = (ADT_UInt32) (chData >> 32);   tdmBuff += tdmFrameOffset;

   chData = _amem8 (chBuff);
   *tdmBuff = (ADT_UInt32)  chData;          tdmBuff += tdmFrameOffset;
   *tdmBuff = (ADT_UInt32) (chData >> 32);
}

// Change between ping and pong buffers
inline switchBuffers (int port, ADT_UInt16 *offset) {
   if (*offset == 0) *offset = DmaPongOffset[port]; 
   else              *offset = 0;
   return;
}

// Perform transfers between TDM buffers and channel buffers
#pragma CODE_SECTION (CopyDmaTxBuffers, "FAST_PROG_SECT")
ADT_Bool CopyDmaTxBuffers (int port, int slotCnt, CircBufInfo_t **TxCircBufrList, int slip) {
   int k, badFrame;  
   CircBufInfo_t *TxCirc, **TxCircBufr;
   ADT_UInt16    *TxOffset;
   ADT_UInt8     *TxDmaBufr, *pTxDma;
   ADT_UInt16    dmaFrameOffset;
   ADT_Word      SlipSamps;
   TsipStat_t *Tsip;

   ADT_UInt32 ScheduledStart;
   ADT_UInt32 TicksToComplete;

   ScheduledStart = CLK_gethtime();

   Tsip = &TsipStats [port];

   TxOffset   = &DmaTxOffset[port];
   TxDmaBufr  = (ADT_UInt8 *) (TsipTxBufs[port] + *TxOffset);

   Tsip->TxIntCnt++;

   // Check that frame counter are valid
   badFrame = validateTxFrameCnt (port, TxDmaBufr);

   // Set the circular buffer and linear DMA pointers to the first slot.
   TxCircBufr = TxCircBufrList;
   dmaFrameOffset = TsipFallocI8[port] / tsipCfg->dmaChannelSizeI8;

   SlipSamps = 0;
   if (slip) SlipSamps = sysConfig.samplesPerMs;
   pTxDma = &TxDmaBufr[4];
   TxCirc = *TxCircBufr++;

   // copy directly from circ buffer into dmabuffer
   for (k = 0; k < slotCnt; k++, TxCirc = *TxCircBufr++,  pTxDma+=tsipCfg->dmaChannelSizeI8)  {
      CheckTxPointers (TxCirc, pTxDma);   // Make sure there are enough samples for transfers
      (*circToDma)(pTxDma, dmaFrameOffset, (ADT_UInt16 *)&TxCirc->pBufrBase[TxCirc->TakeIndex]);    
      TxCirc->SlipSamps += SlipSamps;
      TxCirc->TakeIndex += sysConfig.samplesPerMs;
      if (TxCirc->BufrSize <= TxCirc->TakeIndex) TxCirc->TakeIndex = 0;
   }

   if (gblPkLoadingRst & 1) {
      MultiCoreCpuPeakUsage[0] = 0;
      MultiCoreCpuPeakUsage[1] = 0;
      gblPkLoadingRst &= ~1;
   }

   TicksToComplete = CLK_gethtime() - ScheduledStart;
   MultiCoreCpuUsage[0] = (TicksToComplete * 10)  / (CLK_countspms() / 100L);
   if (MultiCoreCpuPeakUsage[0] < MultiCoreCpuUsage[0]) MultiCoreCpuPeakUsage[0] = MultiCoreCpuUsage[0];

   customTDMOut ((void *) TxDmaBufr, port, slotCnt);
   switchBuffers (port, TxOffset);
   return badFrame;
}
#pragma CODE_SECTION (CopyDmaRxBuffers, "FAST_PROG_SECT")
ADT_Bool CopyDmaRxBuffers (int port, int slotCnt, CircBufInfo_t **RxCircBufrList, int slip) {
   int k, badFrame;  
   CircBufInfo_t *RxCirc, **RxCircBufr;
   ADT_UInt16    *RxOffset;
   ADT_UInt8     *RxDmaBufr, *pRxDma;
   ADT_UInt16    dmaFrameOffset;
   ADT_Word      SlipSamps;
   TsipStat_t *Tsip;

   ADT_UInt32 ScheduledStart;
   ADT_UInt32 TicksToComplete;

   ScheduledStart = CLK_gethtime();


   Tsip = &TsipStats [port];

   RxOffset   = &DmaRxOffset[port];
   RxDmaBufr  = (ADT_UInt8 *) (TsipRxBufs[port] + *RxOffset);

   Tsip->RxIntCnt++;

   // Check that frame counters are valid
   badFrame = validateRxFrameCnt (port, RxDmaBufr);

   // Set the circular buffer and linear DMA pointers to the first slot.
   RxCircBufr = RxCircBufrList;
   dmaFrameOffset = TsipFallocI8[port] / tsipCfg->dmaChannelSizeI8;

   // Loop through all configured slots checking for active slots.
   customTDMIn ((void *) RxDmaBufr, port, slotCnt);

   SlipSamps = 0;
   if (slip) SlipSamps = sysConfig.samplesPerMs;
   pRxDma = &RxDmaBufr[4];
   RxCirc = *RxCircBufr++;

   // copy directly into circ buffer from dma buffer
   for (k = 0; k < slotCnt; k++, RxCirc = *RxCircBufr++, pRxDma+=tsipCfg->dmaChannelSizeI8)  {
      CheckRxPointers (RxCirc, pRxDma);   // Make sure there are enough samples for transfers
      (*dmaToCirc) (pRxDma, dmaFrameOffset, (ADT_UInt16 *)&RxCirc->pBufrBase[RxCirc->PutIndex]);
      RxCirc->SlipSamps += SlipSamps;
      RxCirc->PutIndex += sysConfig.samplesPerMs;
      if (RxCirc->BufrSize <= RxCirc->PutIndex) RxCirc->PutIndex = 0;
   }

   TicksToComplete = CLK_gethtime() - ScheduledStart;
   MultiCoreCpuUsage[1] = (TicksToComplete * 10)  / (CLK_countspms() / 100L);
   if (MultiCoreCpuPeakUsage[1] < MultiCoreCpuUsage[1]) MultiCoreCpuPeakUsage[1] = MultiCoreCpuUsage[1];


   switchBuffers (port, RxOffset);
   return badFrame;
}


#pragma CODE_SECTION (DmaLbRxToTxBuffers, "SLOW_PROG_SECT")
int DmaLbRxToTxBuffers (int port, int slotCnt) {
    // todo fill this in
    return 0;
}
//}

//#define SKIP_SWI
#ifdef SKIP_SWI
#define INT_LOOP    0
#define EXT_LOOP    1
#define TX_PATT     2

extern int testPort;
extern ADT_UInt32 testRxEvt;
extern ADT_UInt32 testTxEvt;

const ADT_UInt8 impulse[8] = {0x01, 0x01,   0x01,   0x01,   0x01, 0x01,   0x01,   0x01};
const ADT_UInt8 *pPattern = impulse;
ADT_UInt8 patternValue = 0x7F;
ADT_UInt8 otherValue   = 0x55;

#define RX_SAMP_LEN  (8*256)
ADT_UInt16 testDmaSlot = 4;
ADT_UInt8 testTxCnt = 0;
ADT_UInt8 testRxCnt = 0xFF;
int rxNoSyncErr = 0;
int rxInSyncErr = 0;
int testRxSync  = 0;
ADT_UInt8 rxSamps[RX_SAMP_LEN];
int rxSampIdx=0;
volatile int pauseHere = 0;
ADT_UInt32 accumEvents = 0;

int copyMode = EXT_LOOP;

void internalLoopTest (ADT_UInt32 completeEvents) {

    ADT_UInt8     *pTx, *pRx;
    ADT_UInt16    fallocI8, offset;
    int i;

    fallocI8 = TsipFallocI8[testPort];
    offset = 4 + testDmaSlot;


    if (completeEvents & testTxEvt) {
        pTx  = TsipTxBufs[testPort] + DmaTxOffset[testPort] + offset;
        for (i=0; i<8; i++, pTx+=fallocI8) {
            *pTx = testTxCnt++;
        }
        if (DmaTxOffset[testPort] == 0) DmaTxOffset[testPort] = DmaPongOffset[testPort]; 
        else                            DmaTxOffset[testPort] = 0;                     
    }

    if (completeEvents & testRxEvt) {
        pRx  = TsipRxBufs[testPort] + DmaRxOffset[testPort] + offset;
        for (i=0; i<8; i++, pRx+=fallocI8) {
            if (rxSampIdx < RX_SAMP_LEN) {
                rxSamps[rxSampIdx++] = *pRx;
                if (rxSampIdx == RX_SAMP_LEN)
                    pauseHere++;
            }
            if (*pRx != testRxCnt) {
                if (!testRxSync)    
                    rxNoSyncErr++;
                else            
                    rxInSyncErr++;
                testRxCnt = *pRx;
            } else {
                testRxSync = 1;
            }
            testRxCnt++;
        }
        if (DmaRxOffset[testPort] == 0) DmaRxOffset[testPort] = DmaPongOffset[testPort]; 
        else                            DmaRxOffset[testPort] = 0;                     
    }
}

void externalLoopTest(ADT_UInt32 completeEvents) {
    ADT_UInt8     *pTx, *pRx;
    ADT_UInt16    fallocI8, offset;
    int i,j;

    fallocI8 = TsipFallocI8[testPort];
    offset = 4;

    accumEvents |= (completeEvents & (testTxEvt | testRxEvt));

    if (accumEvents  == (testTxEvt | testRxEvt)) {
        pTx  = TsipTxBufs[testPort] + DmaTxOffset[testPort] + offset;
        pRx  = TsipRxBufs[testPort] + DmaRxOffset[testPort] + offset;
       
        // Loop the Rx DMA buffer samples into the Tx DMA buffer
        for (i=0; i<8; i++, pTx+=fallocI8, pRx+=fallocI8) {
            for (j=0; j<DmaSlotCnt[testPort]; j++) {
                pTx[j] = pRx[j];
                if (rxSampIdx < RX_SAMP_LEN) {
                    rxSamps[rxSampIdx++] = pRx[j];
                    if (rxSampIdx == RX_SAMP_LEN)   rxSampIdx = 0;
                }
            }
        }

        if (DmaTxOffset[testPort] == 0) DmaTxOffset[testPort] = DmaPongOffset[testPort]; 
        else                            DmaTxOffset[testPort] = 0;                     

        if (DmaRxOffset[testPort] == 0) DmaRxOffset[testPort] = DmaPongOffset[testPort]; 
        else                            DmaRxOffset[testPort] = 0;                     

        accumEvents = 0;
    }
}

void txPatternTest(ADT_UInt32 completeEvents) {
    ADT_UInt8     *pTx;
    ADT_UInt16    fallocI8, offset;
    int i, j;

    fallocI8 = TsipFallocI8[testPort];
    offset = 4;

    if (completeEvents & testTxEvt) {
        pTx  = TsipTxBufs[testPort] + DmaTxOffset[testPort] + offset;
        for (i=0; i<8; i++, pTx+=fallocI8) {
             for (j=0; j<DmaSlotCnt[testPort]; j++) {
                 //pTx[j] = pPattern[i];
                  pTx[j] = patternValue;
             }
             pTx[testDmaSlot] = otherValue;
        }

        if (DmaTxOffset[testPort] == 0) DmaTxOffset[testPort] = DmaPongOffset[testPort]; 
        else                            DmaTxOffset[testPort] = 0;                     
    }
}
#endif

// TDM hardware interrupt.  Start DMA software interrupt.
#pragma CODE_SECTION (GpakDmaIsr, "FAST_PROG_SECT")
static void GpakDmaIsr () {

   ADT_UInt32 mevtflag1, errEvents, completeEvents;

    // determine which combined event(s) occurred
    REG_RD (MEVTFLAG1, mevtflag1);

    completeEvents  = mevtflag1 & TSIP_COMPLETE_EVTS;
    errEvents       = mevtflag1 & TSIP_ERROR_EVTS;

#ifdef _DEBUG
    logTime (0x00100000ul | mevtflag1);
#endif
   Tsip.IntCnt++;

#ifdef SKIP_SWI
    if      (copyMode == INT_LOOP)  internalLoopTest(completeEvents);
    else if (copyMode == EXT_LOOP)  externalLoopTest(completeEvents);
    else if (copyMode == TX_PATT)   txPatternTest(completeEvents);
#else
// ===========================================================
    // trigger a software interrupt only on complete events - NOT on error events
    if (completeEvents) 
        SWI_or (SWI_Dma, completeEvents);
#endif

    // save off the error events for inspection in the SWI
    if (errEvents) {
        Tsip.ErrorCnt++;
        Tsip.ErrorEvents |= errEvents;
    }

    // clear the flagged events
    REG_WR (EVTCLR1, mevtflag1);
}

#pragma CODE_SECTION (McBSPFrameError, "SLOW_PROG_SECT")
ADT_Bool McBSPFrameError (int port)   { return (ADT_Bool) checkTsipErrors(); }

//---------------------------------------------------------------------
//{  Stub fucntions required by G.PAK's TDM interface
// 64plusTDMDma stub functions required by GpakPcm:
int ClkDiv, FrameWidth, PulseWidth, genClockEnable;
#pragma CODE_SECTION (TogglePCMOutSignal, "SLOW_PROG_SECT")
ADT_UInt16 TogglePCMOutSignal (int port)    { return 0xff; }
#pragma CODE_SECTION (TogglePCMInSignal, "SLOW_PROG_SECT")
ADT_UInt16 TogglePCMInSignal (int port)     { return 0xff; }

#pragma CODE_SECTION (storeMcBSPConfiguration, "SLOW_PROG_SECT")
void storeMcBSPConfiguration (int port)                 { }
#pragma CODE_SECTION (setupMcBSP, "SLOW_PROG_SECT")
GpakActivation setupMcBSP (int port)                    { return Disabled; }
#pragma CODE_SECTION (startMcBSP, "SLOW_PROG_SECT")
void startMcBSP (int port)                              { }
#pragma CODE_SECTION (stopMcBsp, "SLOW_PROG_SECT")
GpakActivation stopMcBsp (int port)                     { return Disabled; }
#pragma CODE_SECTION (SetTransmitEnables, "SLOW_PROG_SECT")
void SetTransmitEnables (int port, ADT_UInt32 Mask[])   { }
#pragma CODE_SECTION (ClearTransmitEnables, "SLOW_PROG_SECT")
void ClearTransmitEnables (int port, ADT_UInt16 Mask[]) { }
#pragma CODE_SECTION (ConfigureMcBSP, "SLOW_PROG_SECT")
void ConfigureMcBSP (ADT_UInt32 McBspEnable[3], ADT_UInt32 McBspSlotMask[3][4]) { }
#pragma CODE_SECTION (GetMasks, "SLOW_PROG_SECT")
void GetMasks (ADT_Word McBspId, int Slot, int SlotCnt, ADT_Word *TxMask) { }
#pragma CODE_SECTION (updateTdmLoopback, "SLOW_PROG_SECT")
int updateTdmLoopback(GpakSerialPort_t port, GpakActivation state) { return 0; }
//}
