/*
 * Copyright (c) 2007, Adaptive Digital Technologies, Inc.
 *
 * File Name: 64xMcASP.c
 *
 * Description:
 *   This file contains G.PAK McASP I/O functions.
 *
 * Version: 1.0
 *
 * Revision History:
 *   9/07 - Initial release.
 *
 *
 * NOTE: 
 *    This version only supports a two slot McASP.
 */
   
// Application related header files.

#include "GpakDefs.h"                                      
#include "GpakExts.h"
#include "sysmem.h"
#include "sysconfig.h"
#include "GpakPcm.h"

#define NUM_MCASP_PORTS 2

//-------------------------------------------------------------------
//  McASP register addresses
//-------------------------------------------------------------------
#define MCASP0_ADDR 0x1B4C000
#define MCASP1_ADDR 0x1B5C000

#define PID_OFFSET         0x000 //PID Peripheral Identification register [Register value: 0x0010 0101]
#define PWRDEMU_OFFSET     0x004 //PWRDEMU Power down and emulation management register
#define PFUNC_OFFSET       0x010 //PFUNC Pin function register
#define PDIR_OFFSET        0x014 //PDIR Pin direction register
#define PDOUT_OFFSET       0x018 //PDOUT Pin data out register
#define PDIN_PDSET_OFFSET  0x01C //PDIN/PDSET Pin data in / data set register
                                           //Read returns: PDIN
                                           //Writes affect: PDSET
#define PDCLR_OFFSET       0x020 //PDCLR Pin data clear register
#define GBLCTL_OFFSET      0x044 //GBLCTL Global control register
#define AMUTE_OFFSET       0x048 //AMUTE Mute control register
#define DLBCTL_OFFSET      0x04C //DLBCTL Digital Loop-back control register
#define DITCTL_OFFSET      0x050 //DITCTL DIT mode control register
#define RGBLCTL_OFFSET     0x060 //RGBLCTL Alias of GBLCTL containing only Receiver Reset bits, allows transmit to be reset
                                           //independently from receive.

#define RMASK_OFFSET       0x064 //RMASK Receiver format unit bit mask register
#define RFMT_OFFSET        0x068 //RFMT Receive bit stream format register
#define AFSRCTL_OFFSET     0x06C //AFSRCTL Receive frame sync control register
#define ACLKRCTL_OFFSET    0x070 //ACLKRCTL Receive clock control register
#define AHCLKRCTL_OFFSET   0x074 //AHCLKRCTL High-frequency receive clock control register
#define RTDM_OFFSET        0x078 //RTDM Receive TDM slot 0�31 register
#define RINTCTL_OFFSET     0x07C //RINTCTL Receiver interrupt control register
#define RSTAT_OFFSET       0x080 //RSTAT Status register � Receiver
#define RSLOT_OFFSET       0x084 //RSLOT Current receive TDM slot register
#define RCLKCHK_OFFSET     0x088 //RCLKCHK Receiver clock check control register
#define XGBLCTL_OFFSET     0x0A0 //XGBLCTL Alias of GBLCTL containing only Transmitter Reset bits, allows transmit to be reset
                                           //independently from receive.
#define XMASK_OFFSET       0x0A4 //XMASK Transmit format unit bit mask register
#define XFMT_OFFSET        0x0A8 //XFMT Transmit bit stream format register
#define AFSXCTL_OFFSET     0x0AC //AFSXCTL Transmit frame sync control register
#define ACLKXCTL_OFFSET    0x0B0 //ACLKXCTL Transmit clock control register
#define AHCLKXCTL_OFFSET   0x0B4 //AHCLKXCTL High-frequency Transmit clock control register
#define XTDM_OFFSET        0x0B8 //XTDM Transmit TDM slot 0�31 register
#define XINTCTL_OFFSET     0x0BC //XINTCTL Transmit interrupt control register
#define XSTAT_OFFSET       0x0C0 //XSTAT Status register � Transmitter
#define XSLOT_OFFSET       0x0C4 //XSLOT Current transmit TDM slot
#define XCLKCHK_OFFSET     0x0C8 //XCLKCHK Transmit clock check control register
#define XEVTCTL_OFFSET     0x0CC //XEVTCTL Transmitter DMA control register
#define SRCTL0_OFFSET      0x180 //SRCTL0 Serializer 0 control register
#define SRCTL1_OFFSET      0x184 //SRCTL1 Serializer 1 control register
#define SRCTL2_OFFSET      0x188 //SRCTL2 Serializer 2 control register
#define SRCTL3_OFFSET      0x18C //SRCTL3 Serializer 3 control register
#define SRCTL4_OFFSET      0x190 //SRCTL4 Serializer 4 control register
#define SRCTL5_OFFSET      0x194 //SRCTL5 Serializer 5 control register
#define SRCTL6_OFFSET      0x198 //SRCTL6 Serializer 6 control register
#define SRCTL7_OFFSET      0x19C //SRCTL7 Serializer 7 control register
#define XBUF0_OFFSET       0x200 //XBUF0 Transmit Buffer for Pin 0
#define XBUF1_OFFSET       0x204 //XBUF1 Transmit Buffer for Pin 1
#define XBUF2_OFFSET       0x208 //XBUF2 Transmit Buffer for Pin 2
#define XBUF3_OFFSET       0x20C //XBUF3 Transmit Buffer for Pin 3
#define XBUF4_OFFSET       0x210 //XBUF4 Transmit Buffer for Pin 4
#define XBUF5_OFFSET       0x214 //XBUF5 Transmit Buffer for Pin 5
#define XBUF6_OFFSET       0x218 //XBUF6 Transmit Buffer for Pin 6
#define XBUF7_OFFSET       0x21C //XBUF7 Transmit Buffer for Pin 7
#define RBUF0_OFFSET       0x280 //RBUF0 Receive Buffer for Pin 0
#define RBUF1_OFFSET       0x284 //RBUF1 Receive Buffer for Pin 1
#define RBUF2_OFFSET       0x288 //RBUF2 Receive Buffer for Pin 2
#define RBUF3_OFFSET       0x28C //RBUF3 Receive Buffer for Pin 3
#define RBUF4_OFFSET       0x290 //RBUF4 Receive Buffer for Pin 4
#define RBUF5_OFFSET       0x294 //RBUF5 Receive Buffer for Pin 5
#define RBUF6_OFFSET       0x298 //RBUF6 Receive Buffer for Pin 6
#define RBUF7_OFFSET       0x29C //RBUF7 Receive Buffer for Pin 7
#define XRBUF_PDB_OFFSET   0x3C000000 // RBUF/XBUFx McASPx receive buffers or McASPx transmit buffers via

//-------------------------------------------------------------------
//  McASP register configuration parameters
//-------------------------------------------------------------------
#define McASPPinTypes  0x0000UL   // All pins are McAsp, none GPIO
#define McASPSlotsPerFrame8bit   4UL
#define McASPSlotsPerFrame16bit  2UL
#define McASPRxFormat8bit    0x8030UL   // 0 delay, MSB first, zero padding,  8 bit word, data bus, rotate 0 bits
#define McASPTxFormat8bit    0x8032UL   // 0 delay, MSB first, zero padding,  8 bit word, data bus, rotate 8 bits
#define McASPRxFormat16bit   0x8070UL   // 0 delay, MSB first, zero padding, 16 bit word, data bus, no rotate
#define McASPTxFormat16bit   0x8070UL   // 0 delay, MSB first, zero padding, 16 bit word, data bus, no rotate
#define McASPFrameSync8bit  (McASPSlotsPerFrame8bit << 7)    // High polarity, external, one-bin, 2-slot TDM    
#define McASPFrameSync16bit (McASPSlotsPerFrame16bit << 7)   // High polarity, external, one-bin, 4-slot TDM    
#define McASPClk       0x00C0UL   // Divide by 1, external, async, falling edge
#define McASPHFClk     0x0000UL   // Divide by 1, rising edge, external
#define McASPInactivePin    0UL   // Pin inactive with 3-state drive
#define McASPTransmitPin  0xDUL   // Pin transmit with signal high during inactive slots
#define McASPReceivePin     2UL   // Pin receive
#define McASPSlotWidth     16UL   // Slot width in bits of McASP data
#define McASPSlotMask       3UL


#define MCASP_READ(addr, reg)         (*(volatile ADT_UInt32 *) (addr+reg))
#define MCASP_WRITE(addr, reg, value) {*((volatile ADT_UInt32 *) (addr+reg)) = (ADT_UInt32) value;}

ADT_UInt32 McASP_Base[] = { MCASP0_ADDR, MCASP1_ADDR };
ADT_UInt16 McASP_Configured[] = { 0, 0 };



//-------------------------------------------------------------------
//  Interface data for DMA processing
//-------------------------------------------------------------------

typedef struct {
  ADT_UInt32 Base;    // Base address of Serial port control registers
  ADT_UInt32 TxTCC;   // Tx TCC flag
  ADT_UInt32 RxTCC;   // Rx TCC flag
  ADT_UInt32 TxChn;   // Tx Chn flag
  ADT_UInt32 RxChn;   // Rx Chn flag
  ADT_UInt32 TxPing;
  ADT_UInt32 RxPing;
} PortAddr;
 
extern PortAddr DMACfg [];
extern ADT_UInt16 SltsPerFrame[];                   // Slots on TDM frame [fixed at build]
extern ADT_UInt16 MaxDmaSlots [];                   // Max slots available for DMA [fixed at build]

extern volatile ADT_UInt16 MatchDmaFlags;

#define Tx0_TCC   0
#define Rx0_TCC   1
#define Tx1_TCC   2
#define Rx1_TCC   3

#define Tx0_Chn 34
#define Rx0_Chn 37
#define Tx1_Chn 40
#define Rx1_Chn 43

PortAddr ASPDMACfg [] = {
 { 0x3C000000, Tx0_TCC, Rx0_TCC, Tx0_Chn, Rx0_Chn, 0, 0 },
 { 0x3C100000, Tx1_TCC, Rx1_TCC, Tx1_Chn, Rx1_Chn, 0, 0 }
};


static void startMcASP (int port);
static void setupMcASP (int port);

void StartMcASPPortIo (void);
void StopMcASPPortIo  (void);


SCD_Port McASPPort = {
   StartMcASPPortIo, StopMcASPPortIo
};


static int pollRegister (ADT_UInt32 Base, ADT_UInt32 regAddr, ADT_UInt32 expectedVal, ADT_UInt32 mask) {
   volatile ADT_UInt32 regVal;
   volatile ADT_UInt32 timeout = 1000;

    do {
        regVal = MCASP_READ (Base, regAddr);
    } while (((regVal & mask) != expectedVal) && (timeout--));
    return (timeout == 0);
}



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ConfigureMcASP 
//
// FUNCTION
//   Configures McASP serial port I/O.
//
//  Inputs
//     McAspEnable - Array indicating which McAsp ports are to be configured.
//
// RETURNS
//  Status code indicating success or a specific error.
//
void ConfigureMcASP (ADT_UInt32 McAspEnable[2]) { 

   ADT_UInt32 j, port;
   ADT_UInt16 *SlotMap;

    //--------------------------------------------------------------------------
    // For each enabled McAsp. Verify that
    //       configured tx and rx mask values are within build's range
    //       configured tx and rx bits are equal (same number transmit and receive slots)
    //       the quantity of slots requested is supported by the build
   //--------------------------------------------------------------------------
   secondaryPort = &McASPPort;

   for (port=0; port < 2; port++)   {  

      McASP_Configured[port] = McAspEnable[port];
      
      if (!McAspEnable[port]) continue;

       SlotMap = pSlotMap[port];

      // Override globals for DMA to support 2-slot McASP
      DMACfg[port].Base  =  ASPDMACfg[port].Base;  // Transfer address
      DMACfg[port].TxChn =  ASPDMACfg[port].TxChn;  // TX start channel
      DMACfg[port].RxChn =  ASPDMACfg[port].RxChn;  // RX start channel
      DMACfg[port].TxTCC =  ASPDMACfg[port].TxTCC;  // TX channel control bit
      DMACfg[port].RxTCC =  ASPDMACfg[port].RxTCC;  // RX channel control bit

      sysConfig.serialWordSize[port] = 16;
      SltsPerFrame[port] = 2;
      DmaSlotCnt[port]   = 2;
      MaxDmaSlots[port]  = 2;
      for (j = 0; j < SltsPerFrame[port]; j++)
         SlotMap[j] = j;

   }
   return;
}



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// StartMcASPPortIo - Start serial port input/output.
//
// FUNCTION
//   This function sets up the serial ports and DMA controllers to start PCM I/O
//  after serial port configuration has occuured. It is also used to restart
//  I/O after a serial port error occurs.
//
// RETURNS
//  nothing
//
void StartMcASPPortIo (void) {

   int port, mask;
   ADT_UInt16 flags = 0;

   // Protect critical code from interruption.
   mask = HWI_disable();

   for (port = 0; port < NUM_MCASP_PORTS; port++)  {
      if (McASP_Configured[port] == 0) continue;

      setupMcASP (port);  // Write to McBSP configuration registers (McBSP disabled)
      flags |= enableDMA  (port);  // Configure DMA registers and enable transfer events
      startMcASP (port);  // Remove McBSP from reset
   }
   MatchDmaFlags |= flags;
   HWI_restore (mask);
}

static void setupMcASP (int port) {
   // Steps used for initialization are defined in McAsp Reference Guide
   ADT_UInt32 gblctrl;           // local copy of mcasp global control register
   ADT_UInt32 Base;
   ADT_UInt16 McASPRxFormat, McASPTxFormat, McASPFrameSync;

   Base = McASP_Base [port];

   //----------- 1) Place McAsp in reset
   gblctrl = MCASP_READ (Base, GBLCTL_OFFSET);
   if (gblctrl)  {
       gblctrl = 0;
       MCASP_WRITE (Base, GBLCTL_OFFSET, 0);
       pollRegister (Base, GBLCTL_OFFSET, gblctrl, 0xFFFFFF); 
   }

   // Setup McASP0 and EDMA if any slots are configured on first McASP port
   if (McASP_Configured[port] == 0) return;
   if (sysConfig.serialWordSize[port] == 8) {
      McASPRxFormat = McASPRxFormat8bit;
      McASPTxFormat = McASPTxFormat8bit;
      McASPFrameSync =  McASPFrameSync8bit;
   } else {
      McASPRxFormat = McASPRxFormat16bit;
      McASPTxFormat = McASPTxFormat16bit;
      McASPFrameSync =  McASPFrameSync16bit;
   }

   //----------- 2) Configure McAsp registers
   // a) Power management
   MCASP_WRITE (Base, PWRDEMU_OFFSET, 0UL);

   // b) Receive registers
   MCASP_WRITE (Base, RMASK_OFFSET,       0xFFFFFFFFUL);    // No padding data bits
   MCASP_WRITE (Base, RFMT_OFFSET,        McASPRxFormat);   // Data format
   MCASP_WRITE (Base, AFSRCTL_OFFSET,     McASPFrameSync);  // Frame sync format
   MCASP_WRITE (Base, ACLKRCTL_OFFSET,    McASPClk);        // Clock format
   MCASP_WRITE (Base, AHCLKRCTL_OFFSET,   McASPHFClk);      // High freq clock format
   MCASP_WRITE (Base, RTDM_OFFSET,        McASPSlotMask);   // Mask of active slots
   MCASP_WRITE (Base, RINTCTL_OFFSET,     0UL);             // No McAsp interrupts
   MCASP_WRITE (Base, RCLKCHK_OFFSET,     0UL);             // No clock checking

   // c) Transmit registers
   MCASP_WRITE (Base, XMASK_OFFSET,       0xFFFFFFFFUL);    // No padding data bits
   MCASP_WRITE (Base, XFMT_OFFSET,        McASPTxFormat);   // Data format
   MCASP_WRITE (Base, AFSXCTL_OFFSET,     McASPFrameSync);  // Frame sync format
   MCASP_WRITE (Base, ACLKXCTL_OFFSET,    McASPClk);        // Clock format
   MCASP_WRITE (Base, AHCLKXCTL_OFFSET,   McASPHFClk);      // High freq clock format
   MCASP_WRITE (Base, XTDM_OFFSET,        McASPSlotMask);   // Mask of active slots
   MCASP_WRITE (Base, XINTCTL_OFFSET,     0UL);             // No McAsp interrupts
   MCASP_WRITE (Base, XCLKCHK_OFFSET,     0UL);             // No clock checking

   // d) Pin state registers
   MCASP_WRITE (Base, SRCTL0_OFFSET,      McASPTransmitPin); // Tx
   MCASP_WRITE (Base, SRCTL1_OFFSET,      McASPReceivePin);  // Rx
   MCASP_WRITE (Base, SRCTL2_OFFSET,      McASPInactivePin); // Inactive
   MCASP_WRITE (Base, SRCTL3_OFFSET,      McASPInactivePin);
   MCASP_WRITE (Base, SRCTL4_OFFSET,      McASPInactivePin);
   MCASP_WRITE (Base, SRCTL5_OFFSET,      McASPInactivePin);
   MCASP_WRITE (Base, SRCTL6_OFFSET,      McASPInactivePin);
   MCASP_WRITE (Base, SRCTL7_OFFSET,      McASPInactivePin);

   // e) Global registers
   MCASP_WRITE (Base, PFUNC_OFFSET,       McASPPinTypes);   // McAsp vs GPIO pins
   MCASP_WRITE (Base, PDIR_OFFSET,        1);               // Pin 0 is Tx
   MCASP_WRITE (Base, DITCTL_OFFSET,      0UL);             // DIT mode not used
   MCASP_WRITE (Base, DLBCTL_OFFSET,      0UL);             // Loopback not used
   MCASP_WRITE (Base, AMUTE_OFFSET,       0UL);             // Mute not used
}        

static void startMcASP (int port) {
   int gblctrl, rv;
   ADT_UInt32 Base;

   Base = McASP_Base [port];

   //----------- 6) Activate Serializers
   // a) Clear status
   MCASP_WRITE (Base, XSTAT_OFFSET, 0x0000FFFFUL);
   MCASP_WRITE (Base, RSTAT_OFFSET, 0x0000FFFFUL);

   // b) Serializer enable
   gblctrl = 0x00000404;   
   MCASP_WRITE (Base, GBLCTL_OFFSET, gblctrl);
   rv = pollRegister (Base, GBLCTL_OFFSET, gblctrl, 0xFFFFFFFF); 

   //----------- 7) Verify transmit buffers clear by XDATA flag of XSTAT register 
   rv |= pollRegister (Base, XSTAT_OFFSET, 0, 0x20); 

   //----------- 8) State machine release
   gblctrl |= 0x00000808;   
   MCASP_WRITE (Base, GBLCTL_OFFSET, gblctrl);
   rv |= pollRegister (Base, GBLCTL_OFFSET, gblctrl, 0xFFFFFFFF); 
        
   //----------- 9) Frame sync release
   gblctrl |= 0x00001010;
   MCASP_WRITE (Base, GBLCTL_OFFSET, gblctrl);
   rv |= pollRegister (Base, GBLCTL_OFFSET, gblctrl, 0xFFFFFFFF); 
        
   if (rv) {
      // Report setup failure and disable McAsp
      MCASP_WRITE (Base, GBLCTL_OFFSET, 0);
   }

}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// StopSerialPortIo - Stop all serial port and McAsp input/output.
//
// FUNCTION
//   This function stops all serial ports, McAsp ports, and DMA controllers.
//
// RETURNS
//  nothing
//
void StopMcASPPortIo () {

   ADT_UInt32 Base;
   int port;

   for (port = 0; port < NUM_MCASP_PORTS; port++)  {
      if (McASP_Configured[port] == 0) continue;

       Base = McASP_Base [port];

       MCASP_WRITE (Base, GBLCTL_OFFSET, 0); // reset mcasp
       pollRegister (Base, GBLCTL_OFFSET, 0, 0xFFFFFFFF);
   }
   return;
}

