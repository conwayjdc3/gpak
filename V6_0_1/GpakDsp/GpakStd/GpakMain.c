/*
 * Copyright (c) 2001- 2007, Adaptive Digital Technologies, Inc.
 *
 * File Name: GpakMain.c
 *
 * Description:
 *   This file contains the G.PAK main program function.
 *
 * Version: 1.0
 *
 * Revision History:
 *   11/13/01 - Initial release.
 *
 */
// Application related header files.
#include <std.h>
//#include <clk.h>
//#include <hwi.h>
//#include <c64.h>
#include <stdio.h>
#include "GpakDefs.h"
#include "GpakExts.h"
#include <_lock.h>

#ifdef IPC_SUPPORT
#include <ti/ipc/Ipc.h>
#include <ti/ipc/MultiProc.h>
#endif

#define CORE_WAIT
#ifdef AVAYA_INTEGRATION
extern far int numTestCores;
extern void AlgTest();
#endif
// Definition of G.726 Low MIPS tables.
extern void processLoopbackCoder ();
extern void customIdleFunc ();
extern void pendingChannelSetup ();
extern int CSL_chipReadReg (int reg);
extern void ipStackInit ();
extern void PostRTDX ();
extern void PowerOnPeripherals ();
extern void ScheduleFramingTasks();
extern void StopSerialPortIo();
extern void StartSerialPortIo();
extern void teardownLoopBack();
extern void CheckFrameHealth();

extern short int *G726QuanTables[];
extern GpakIfBlock_t **HostApiPtr;
extern volatile ADT_UInt32 MatchDmaFlags;       // count of all configured time slots
extern int custom_cmd;

// Error recovery and debug statistic variables.
#define DMA_STOP_THRESHOLD 10         // num consecutive DMA stops for reset
ADT_Word PrevDmaSwiCnt = 0;
ADT_Word DmaSwiStopCnt = 0;
ADT_Word DmaMissedCnt  = 0;
ADT_Word NoMcBSPCnt    = 0;

extern int msgCore;
extern ADT_UInt16 tdmCore;
extern ADT_UInt16 minChanCore;

volatile chanInfo_t *PendingChannel = NULL;

static ADT_Bool FirstIdle;

extern coreLock dmaLock;
coreLock *pdmaLock = &dmaLock;
//#pragma DATA_SECTION (dmaLock, "SHARED_LOCK_SECT:dmaLock") 
//#pragma DATA_ALIGN   (dmaLock, 64) 

extern coreLock tdmLock;
coreLock *ptdmLock = &tdmLock;
//#pragma DATA_SECTION (tdmLock, "SHARED_LOCK_SECT:tdmLock") 
//#pragma DATA_ALIGN   (tdmLock, 64) 

extern coreLock initLock;
coreLock *pinitLock = &initLock;
//#pragma DATA_SECTION (initLock, "SHARED_LOCK_SECT:initLock") 
//#pragma DATA_ALIGN   (initLock, 64) 

volatile int init0Cnt;
volatile int init1Cnt;
volatile int cinitCnt;
#pragma DATA_SECTION (init0Cnt, "SHARED_DATA_SECT") 
#pragma DATA_SECTION (init1Cnt, "SHARED_DATA_SECT") 
#pragma DATA_SECTION (cinitCnt, "SHARED_DATA_SECT") 

#define MAIN_TRACE(val) System_printf("Core %d: main %d\n", DSPCore, val)
//#define MAIN_TRACE(val) ;

char sysHaltText[50];
#define RS_AVAYA_INIT
#ifdef RS_AVAYA_INIT
void Set_Psc_All_On(void);
int Set_PSC_State(unsigned int pd,unsigned int id,unsigned int state);
void Wait_Soft( int nloop );
extern void wakeup_cores(void);
extern void write_boot_magic_number(uint32_t core);
#endif
void unlock_chip_regs();
extern int read_boot_magic_number(uint32_t core);
extern void wakeup_core(uint32_t core);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// main - Main G.PAK program.
//
// FUNCTION
//  This function performs all G.PAK application initialization upon startup
//  prior to interrupts being enabled and BIOS starting.
//
// RETURNS
//  nothing
//

void main () {
   EventFifoMsg_t msg;
   int mask;

#if (DSP_TYPE == 64)
   DSPCore = CSL_chipReadReg (17);  // Read which core this system is running on.
#else
   DSPCore = 0;
#endif
   MAIN_TRACE(1);

#ifdef AVAYA_INTEGRATION
   AlgTest();
   DSPTotalCores = numTestCores;
   if (numTestCores == 1) {
       msgCore = 0;
       tdmCore = 0;
       minChanCore = 0;
   } else if (numTestCores == 2) {
       msgCore = 1;
       tdmCore = 1;
       minChanCore = 1;
   } else {
       msgCore = 1;
       tdmCore = 1;
       minChanCore = 2;
   }
#endif

   // Clear and then freeze cache to allow updates to all cores.
   if (DSPTotalCores != 1) {
      BCACHE_wbInvAll ();
      BCACHE_setMode (BCACHE_FREEZE, BCACHE_L1D);
      BCACHE_setMode (BCACHE_FREEZE, BCACHE_L2);
   }

   MAIN_TRACE(2);

#ifdef IPC_SUPPORT
{
   int32_t     status;
   MultiProc_setLocalId(DSPCore);
   status = Ipc_start();
   if (status < 0)
       System_printf("Core %d: Ipc_start failed\n", DSPCore);
   else
       System_printf("Core %d: Ipc_start success\n", DSPCore);
}
#endif

   CpuUsage   = &MultiCoreCpuUsage[DSPCore * FRAME_TASK_CNT];
   CpuPkUsage = &MultiCoreCpuPeakUsage[DSPCore * FRAME_TASK_CNT];
   ApiBlock.u1.CpuUsage   = MultiCoreCpuUsage;
   ApiBlock.u2.CpuPkUsage = MultiCoreCpuPeakUsage;


   // Core synchronization
   //
   // NOTE: Cores will wait until:
   //   a) core zero initializes initLock.Inst to zero and
   //   b) all cores have incremented the cinitCnt variable
   //      before they can proceed
   if (DSPCore == 0) {
      init0Cnt = 0;
      init1Cnt = 0;
      cinitCnt = 0;
      pinitLock->Inst = 0;   // Allow access to initCnt variable
      Set_Psc_All_On();
	  wakeup_cores();
      while (cinitCnt < (DSPTotalCores-1));
      cinitCnt++;
   }
   else
   {
	    write_boot_magic_number(DSPCore);
        mask = ADDRESS_lock (&pinitLock->Inst);
        cinitCnt++;
   	    BCACHE_wbInvAll ();
        ADDRESS_unlock (&pinitLock->Inst, mask);
   	    while (cinitCnt < DSPTotalCores);
   }

   MAIN_TRACE(3);

   if (DSPCore == 0) {
      // Must be called before custom frame initialization (customDSPInit) to determine 
      // TDMRate and WideB flags based upon the TSIP configuration.
      if (DSPTotalCores != 1) DMAInitialize ();
      pdmaLock->Inst = 0;   // Signal that DMA has been initialized
   } else {
      // Wait until DMA has been initialized
      mask = ADDRESS_lock (&pdmaLock->Inst);
      ADDRESS_unlock (&pdmaLock->Inst, mask);
   }

   MAIN_TRACE(4);

   // Note: BIOS initialization has occurred and interrupts are disabled.
   // - - - - - - -  Perform application specific initialization.  - - - - - -
   customDSPInit ();   // Board/core initialization

   MAIN_TRACE(5);
   FirstIdle = TRUE;

   PowerOnPeripherals ();                        // Pinmux/power on Timer, Ethernet

   mask = ADDRESS_lock (&pinitLock->Inst);
   customDSPStartUp ();    // Customized port/channel setup
   MAIN_TRACE(6);

   #ifdef RS_AVAYA_INIT
   if (DSPCore != 0)
   {
	Wait_Soft(DSPCore*1000);
   	init0Cnt++;
   }
   #else
   init0Cnt++;
   #endif

   ADDRESS_unlock (&pinitLock->Inst, mask);


   // Wait for all cores to complete customDSPInit before continuing
#ifdef CORE_WAIT
   #ifdef RS_AVAYA_INIT
   if (DSPCore == 0)
   {
   	    while (init0Cnt < (DSPTotalCores-1));
   	    init0Cnt++;
   }
   else
   {
   	    while (init0Cnt < DSPTotalCores);
   }
   #else
   while (init0Cnt < DSPTotalCores);
   #endif
#endif
   // TDM initialization
   if ((1 == DSPTotalCores) || (DSPCore == tdmCore)) {
      InitGpakPcm ();
      ptdmLock->Inst = 0;
   } else {
      // Wait for TDM initialization to complete before proceeding.
      mask = ADDRESS_lock (&ptdmLock->Inst);
      ADDRESS_unlock (&ptdmLock->Inst, mask);
   }
   MAIN_TRACE(7);

   // Initialize instance pools and messaging structures.
   // Allocate messaging swi or task.
   if (DSPCore < 2) 
      InitGpakInterface ();

   MAIN_TRACE(8);

   if (DSPCore == 0) {
      initGpakCapture();

      GpakRtdxSetup ();                             // Alternate host communications

      if (SysCodecs.g726LowMipsEnable)
         G726_ADT_configure (G726QuanTables, ULAW); // G.726 Low MIPS tables

      if (SysAlgs.noiseSuppressEnable)
         NCAN_ADT_config ();                        // Noise canceller initialization

   }
   FLUSH_wait ();
   MAIN_TRACE(9);

   InitFrameTasks ();         // Framing task data.

   MAIN_TRACE(10);

   // Set up each core's RTP Buffer to Jitter Buffer transfers
   if (sysConfig.maxJitterms != 0) {
      RTP_Init ();                                 // RTP stack. MUST BE AFTER InitGpakInterface

      #ifdef RTCP_ENABLED
      rtcp_system_init();
      #endif
   }
   MAIN_TRACE(11);

   if (DSPCore == 0) {
       if (SysAlgs.networkStack) {
           ipStackInit ();
       }
   }
   MAIN_TRACE(12);

   mask = ADDRESS_lock (&pinitLock->Inst);
   customDSPStartUp ();    // Customized port/channel setup
   #ifdef RS_AVAYA_INIT
   Wait_Soft(DSPCore * 1000);
   #endif
   init1Cnt++;
   ADDRESS_unlock (&pinitLock->Inst, mask);

   MAIN_TRACE(13);

   // Wait for all cores to complete customDSPStartUp before continuing
#ifdef CORE_WAIT
   while (init1Cnt < DSPTotalCores);
#endif
   logTime (0x234432);  // Initialize time and data logging

   // send msg to host informing it that the dsp is ready for API commands
   msg.header.channelId = DSPCore;
   msg.header.deviceSide = ADevice;
   msg.header.eventLength = EVENT_FIFO_MSGLEN_ZERO;
   msg.header.eventCode = EventDspReady;
   writeEventIntoFifo (&msg);
   MAIN_TRACE(14);


   // Require core resynchronization on reset
   if (DSPCore == 0) {
      init0Cnt = 0;
      init1Cnt = 0;
      cinitCnt = 0;
      pinitLock->Inst = -1;
      ptdmLock->Inst  = -1;
      pdmaLock->Inst  = -1;
      // Store the address of the API block in the pointer used by HPI/PCI hosts.
      // This will signal host that DSP is ready
      *HostApiPtr = &ApiBlock;
   }

   //==========================================================================
   // Note: BIOS Startup will occur after exiting, interrupts will be enabled,
   //       and the BIOS Idle loop will be entered.

   if (DSPTotalCores != 1) {
      BCACHE_setMode (BCACHE_NORMAL, BCACHE_L1D);
      BCACHE_setMode (BCACHE_NORMAL, BCACHE_L2);
   }

   System_printf("Core %d up\n", DSPCore);

#if defined(_TMS320C6600)
   BIOS_start();
#endif
   return;
}
#ifdef RS_AVAYA_INIT
//*****************************************************
// Power definitions
#define PSC_BASE            0x02350000
#define PSC_PTCMD           *( unsigned int* )( PSC_BASE+0x120 )
#define PSC_PTSTAT          *( unsigned int* )( PSC_BASE+0x128 )
#define PSC_PDCTL_BASE      ( PSC_BASE+0x300 )
#define PSC_MDSTAT_BASE     ( PSC_BASE+0x800 )
#define PSC_MDCTL_BASE      ( PSC_BASE+0xA00 )

#define LPSC_EMIF25_SPI 3 // EMIF16
#define LPSC_TSIP 4
#define LPSC_DEBUGSS_TRC 5
// PD 1
#define LPSC_TETB_TRC 6
// PD 2
#define LPSC_PKTPROC 7  // PA
#define LPSC_CPGMAC 8   // SGMII
#define LPSC_Crypto  9  // SA
// PD 3
#define LPSC_PCIEX 10
// PD 4
#define LPSC_SRIO 11
// PD 5
#define LPSC_Hyperbridge 12
// PD 7
#define LPSC_MSMCSRAM 14

// Power domains definitions
#define PD0         (0)     // Power Domain-0
#define PD1         (1)     // Power Domain-1
#define PD2         (2)     // Power Domain-2
#define PD3         (3)     // Power Domain-3
#define PD4         (4)     // Power Domain-4
#define PD5         (5)     // Power Domain-5
#define PD6         (6)     // Power Domain-6
#define PD7         (7)     // Power Domain-7
#define PD8         (8)     // Power Domain-8
#define PD9         (9)     // Power Domain-9
#define PD10        (10)    // Power Domain-10
#define PD11        (11)    // Power Domain-11
#define PD12        (12)    // Power Domain-12
#define PD13        (13)    // Power Domain-13
#define PD14        (14)    // Power Domain-14
#define PD15        (15)    // Power Domain-15
#define PD16        (16)    // Power Domain-16
#define PD17        (17)    // Power Domain-17

#define PSC_SYNCRESET (0x1)
#define PSC_DISABLE   (0x2)
#define PSC_ENABLE    (0x3)

/****************************************************************************
 *
 * NAME
 *      Wait_Soft
 *
 * PURPOSE:
 *      Wait for a specified delay in number of empty loop.
 *
 * USAGE
 *      This routine can be called as:
 *
 *      Wait_Soft(nloop)
 *
 *      nloop - (i) number of empty loop to do for delay
 *
 * RETURN VALUE
 *      NONE
 *
 * REFERENCE
 *
 ****************************************************************************/
void Wait_Soft( int nloop )
{
    int i;

    // 1 sec ~ 40000 loop on P4 3.4GHz
    for( i = 0 ; i < nloop ; i++ )
    {
    }
}
/****************************************************************************
 *
 * NAME
 *      Set_Psc_All_On
 *
 * PURPOSE:
 *      Enable all PSC modules and DSP power domains on ALWAYSON, and wait
 *      for these power transitions to complete.
 *
 * USAGE
 *      This routine can be called as:
 *
 *      Set_Psc_All_On()
 *
 * RETURN VALUE
 *      NONE
 *
 * REFERENCE
 *
 ****************************************************************************/
void Set_Psc_All_On(void)
{
    //unsigned int i=0;

    // Only core0 can set PSC
    //if (core == 0)
    //{
        //GEL_TextOut( "Power on all PSC modules and DSP domains... \n");
        Set_PSC_State(PD0, LPSC_EMIF25_SPI, PSC_ENABLE);
        Set_PSC_State(PD0, LPSC_TSIP, PSC_ENABLE);
        Set_PSC_State(PD1, LPSC_TETB_TRC, PSC_ENABLE);
        Set_PSC_State(PD2, LPSC_PKTPROC, PSC_ENABLE);
        Set_PSC_State(PD2, LPSC_CPGMAC, PSC_ENABLE);
        Set_PSC_State(PD2, LPSC_Crypto, PSC_ENABLE);
        Set_PSC_State(PD3, LPSC_PCIEX, PSC_ENABLE);
        Set_PSC_State(PD4, LPSC_SRIO, PSC_ENABLE);
        Set_PSC_State(PD5, LPSC_Hyperbridge, PSC_ENABLE);
        Set_PSC_State(PD7, LPSC_MSMCSRAM, PSC_ENABLE);

        //GEL_TextOut( "Power on all PSC modules and DSP domains... Done.\n" );
    //}
    //else
    //{
        ////GEL_TextOut("DSP core #%d cannot set PSC.\n",,2,,,DNUM);
    //}
}
/****************************************************************************
 *
 * NAME
 *      Set_PSC_State
 *
 * PURPOSE:
 *      Set a new power state for the specified domain id in a power controler
 *      domain. Wait for the power transition to complete.
 *
 * USAGE
 *      This routine can be called as:
 *
 *      Set_PSC_State(unsigned int pd,unsigned int id,unsigned int state)
 *
 *      pd    - (i) power domain.
 *
 *      id    - (i) module id to use for module in the specified power domain
 *
 *      state - (i) new state value to set
 *                  0 = RESET
 *                  1 = SYNC RESET
 *                  2 = DISABLE
 *                  3 = ENABLE
 *
 * RETURN VALUE
 *      0 if ok, !=0 for error
 *
 * REFERENCE
 *
 ****************************************************************************/
int Set_PSC_State(unsigned int pd,unsigned int id,unsigned int state)
{
    unsigned int* mdctl;
    unsigned int* mdstat;
    unsigned int* pdctl;
    int ret=0;

    // GEL_TextOut( "Set_PSC_State for pd = %d \n",,2,,,pd);


    // Only core0 can set PSC
    //if (DNUM == 0)
    //{
        mdctl = ( unsigned int* )(PSC_MDCTL_BASE + ( 4 * id ));
        mdstat = ( unsigned int* )( PSC_MDSTAT_BASE + ( 4 * id ));
        pdctl = ( unsigned int* )(PSC_PDCTL_BASE + ( 4 * pd ));

        // If state is already set, do nothing
        if ( ( *mdstat & 0x1f ) == state )
        {
            return(0);
        }

        Wait_Soft(150);
        // Check if we got timeout error while waiting
        if (PSC_PTSTAT & (0x1 << pd))
        {
            //GEL_TextOut( "Set_PSC_State... Timeout Error #01 pd=%d, md=%d!\n",,2,,,pd,id);
            ret=1;
        }
        else
        {
            // Set power domain control
            *pdctl = (*pdctl) | 0x00000001;

            // Set MDCTL NEXT to new state
            *mdctl = ((*mdctl) & ~(0x1f)) | state;

            // Start power transition by setting PTCMD GO to 1
            PSC_PTCMD = (PSC_PTCMD) | (0x1<<pd);

            // Wait for PTSTAT GOSTAT to clear
            Wait_Soft(150);
            if ((PSC_PTSTAT & (0x1 << pd)) != 0 ) {
                //GEL_TextOut( "Set_PSC_State... Timeout Error #02 pd=%d, md=%d!\n",,2,,,pd,id);
                return 1;
            }

            // Verify state changed
            Wait_Soft(150);
            if(( *mdstat & 0x1f ) != state ) {
                //if ((pd == 2) && (id == 9) ) {
                    //GEL_TextOut( "Security Accelerator disabled!\n",,2,,);
                //} else {
                    //GEL_TextOut( "Set_PSC_State... Timeout Error #03 pd=%d, md=%d!\n",,2,,,pd,id);
                //}
                return 1;
            }
        }
    //}
    //else
    //{
        //GEL_TextOut("DSP core #%d cannot set PSC.\n",,2,,,DNUM);
    //}

    //   GEL_TextOut( "Exit Set_PSC_State \n");

    return(ret);
}

#endif



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// GpakSchedulingTimer - G.PAK Scheduling Periodic Timer.
//
// FUNCTION
//   Called every millisecond to schedule framing and messaging thread
//  when serial ports are not configured.
//
// RETURNS
//  nothing
//
//volatile int schedTimerCount = 0;
volatile int schedTimerCount0 = 0;
volatile int schedTimerCount1 = 0;
#pragma CODE_SECTION (GpakSchedulingTimer, "FAST_PROG_SECT")

void GpakSchedulingTimer (void) {

  // schedTimerCount++;
   if (DSPCore == 0)
        schedTimerCount0++;
   if (DSPCore == 1)
        schedTimerCount1++;

   if (1 < DSPCore) {
      // Timer interrupt is skipped for framing-only cores
      // Interrupts must remain enabled so that SecondsClock_getTime( ) works
      return;
   }
   // If no TDM channels are active, schedule the framing task via timer.
    logTime (0x80000009ul);
    logTime (0x02000000ul);

   if (DSPCore == msgCore) {
       customTimer ();
       CheckFrameHealth();

       // Schedule the message task when a message is pending
       if (SysAlgs.apiCacheEnable && (ApiBlock.pRdWrBlock->CmdMsgLength != 0)) {
           CACHE_INV (ApiBlock.pRdWrBlock, CACHE_L2_LINE_SIZE);
           activateMsgHandler ();
       } else if (ApiBlock.CmdMsgLength != 0) {
           activateMsgHandler ();
       } else if (custom_cmd) {
           activateMsgHandler ();
       }
       teardownLoopBack();

       
   }

#ifdef _DEBUG
   searchDataBuffer();
#endif

   if (DSPCore == tdmCore) {
      if (MatchDmaFlags == 0) {
          NoMcBSPCnt++;
          logTime (0x020A0000ul);
          ScheduleFramingTasks();
          logTime (0x80010009ul);
          return;
      }

      // Verify DMA is occurring
      if (ApiBlock.DmaSwiCnt != PrevDmaSwiCnt) {
          PrevDmaSwiCnt = ApiBlock.DmaSwiCnt;
          DmaSwiStopCnt = 0;
          logTime (0x020B0000ul);
          logTime (0x80010009ul);
          return;
      }

      // Missed DMA
      DmaSwiStopCnt++;
      // Restart I/O if a number of consecutive DMA stopped occurrences
      // have been detected.
      if (DMA_STOP_THRESHOLD <= DmaSwiStopCnt) {
          StopSerialPortIo();
          DmaSwiStopCnt = 0;
          StartSerialPortIo();
          DmaMissedCnt++;
          SendWarningEvent (DmaMissedCnt & 0xffff, WarnPortRestart);
      }
   }
   logTime (0x020F0000ul);
   logTime (0x80010009ul | (DmaSwiStopCnt << 8));

   return;
}
#ifdef AEC_SUPPORTED
static int LastAECRunTime;
#endif
volatile int idleFuncCount0 = 0;
volatile int idleFuncCount1 = 0;

#pragma CODE_SECTION (GpakIdleTaskFunc, "SLOW_PROG_SECT")
void GpakIdleTaskFunc () {
   if (FirstIdle) {
      // Use s/w interrupt disable as rts.lib locking mechanism
      _register_lock      ((void (*)()) SWI_disable);
      _register_unlock    ((void (*)()) SWI_enable);
      FirstIdle = FALSE;
   }
   if (DSPCore == 0)
        idleFuncCount0++;
   if (DSPCore == 1)
        idleFuncCount1++;

    customIdleFunc();
    return;

}

#pragma CODE_SECTION (SystemHalt, "SLOW_PROG_SECT")
void SystemHalt (const char *format, ...) {
   int mask;
   va_list params;

   va_start (params, format);
   vsprintf (sysHaltText, format, params);
   va_end (params);

   AppErr (sysHaltText, TRUE);
   mask = HWI_disable ();
   while (format);
   HWI_restore (mask);
   
}


