#include "adt_typedef.h"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// Unpacks 16-bit words that were packed in the Least Significant Bits (LSB) manner
#pragma CODE_SECTION(unpackLSB,".gpakCritical:frame")
void unpackLSB(ADT_UInt16 *in,      // input buffer: packed data "wordSize-bits" wide
               ADT_UInt16 *out,     // output buffer: unpacked data "width-bits" wide
               ADT_UInt16 num,      // number of output codewords to unpack
               ADT_UInt16 width,    // width (in bits) of each codeword to unpack
               ADT_UInt16 leftShifts) // number of bits to left shift the output 

{
	ADT_UInt16 inIndex, outIndex;
	ADT_UInt16 mask, bitsLeft;
	ADT_UInt16 temp, wordSize; 

    wordSize = 16;

    bitsLeft = wordSize;
    mask = (1<<width) - 1;
    inIndex = outIndex = 0;

    while (outIndex < num)  {
        out[outIndex++] = (mask & in[inIndex])<<leftShifts;
        bitsLeft -= width;
        in[inIndex] >>= width;
        if (0 == bitsLeft)  {
            // The previous codeword pulled out of the current input word 
            // depleated the word of bits. Increment the input pointer.
            bitsLeft = wordSize;
            inIndex++;
        } else
        if ((bitsLeft < width) && (outIndex < num)) {
            // Not enough bits remaining in the current word being unpacked
            // to form a codeword - (i.e. the codeword straddles a word boundary.
            // Must concatenate the remaining bits with the LSBs of the nex word.

            // mask and store the remaining bits of current input word
            out[outIndex] = in[inIndex++] & ((1<<bitsLeft)-1);

            // mask the LSBs of next input word. Left shift for proper alignment
            temp = (in[inIndex] & ((1<<(width-bitsLeft))-1))<<bitsLeft;
            
            // or in the MSBs of the codeword
            out[outIndex] |= temp;

            // shift it
            out[outIndex++]  <<= leftShifts;

            // adjust current input word for next codeword extraction
            in[inIndex] >>= (width-bitsLeft);
            bitsLeft += (wordSize - width);
        }
    }
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// Packs 16-bit words in the Most Significant Bits (MSB) manner
// Codewords that are "width"-bits wide are packed into 8-bit bytes such that
// the msb of the 1st code word is aligned with the msb of the output byte. The
// byte is filled from msb down to lsb. If there are not enough bits remaining in the 
// byte being filled to store an entire codeword, then the msbs of the current codeword
// that fit in the byte are stored, and the remaining lsbs of the codeword are
// stored in the msbs of the next byte. Pairs of packed bytes are then packed
// together into 16-bit words.
//
// For example: Say we have a buffer of 4-bit wide codewords with the following 
// values:  In[ ] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, a, b, c, d, e, f, 0}
// The output packed buffer will be:
//  Out[ ] = {0x3412, 0x7856, 0xbc9a, 0xf0de)
#pragma CODE_SECTION(packMSB,".gpakCritical:frame")
void packMSB (ADT_UInt16 *inword,      // input buffer: unpacked data
             ADT_UInt16 *outword,      // output buffer: packed data
             ADT_UInt16 num,           // number of codewords to pack
             ADT_UInt16 width) {       // width (in bits) of each codeword to pack

	ADT_UInt16 in, out;
	ADT_UInt16 bitsAvail, fillLow, shift;
	ADT_UInt16 lowByte, highByte;
	ADT_UInt16 currentInword, inputMask;

    if (width > 8)
        return;

    lowByte = 0;
    highByte = 0;
    bitsAvail = 8;
    fillLow = 1;
    in = out = 0;
    shift = 8-width;
    inputMask = (1<<width)-1;

    while (in < num) {
       if (fillLow) {
           if (bitsAvail == 0){
              fillLow = 0;
              bitsAvail = 8;
              shift = 8-width;
          } else if ((bitsAvail < width) && (in < num)) {
              // fill remaining bits in lowByte LSBs
              lowByte |= currentInword>>(width-bitsAvail);
              fillLow = 0;

              // put rest of codeword into highByte MSBs
              shift = bitsAvail = 8-(width-bitsAvail);
              currentInword = inword[in++] & inputMask;
              highByte = (currentInword<<shift) & 0x00ff;
              shift -= width;
          } else {
              currentInword = inword[in++] & inputMask;
              lowByte |= currentInword<<shift;
              bitsAvail -= width;
              shift -= width;
          }

          if (num == in)
             outword[out++] = (highByte<<8) | lowByte;
       } else {
          if (bitsAvail == 0) {
             outword[out++] = (highByte<<8) | lowByte;
             lowByte = highByte = 0;
             fillLow = 1;
             bitsAvail = 8;
             shift = 8-width;
          } else if ((bitsAvail < width) && (in < num)) {
             // fill remaining bits in highByte LSBs
             highByte |= currentInword>>(width-bitsAvail);
             outword[out++] = (highByte<<8) | lowByte;
             lowByte = highByte = 0;
             fillLow = 1;

             // put rest of codeword into lowByte MSBs
             shift = bitsAvail = 8-(width-bitsAvail);
             currentInword = inword[in++] & inputMask;
             lowByte = (currentInword<<shift) & 0x00ff;
             shift -= width;

             if (num == in)
                outword[out++] = lowByte;
          } else {
             currentInword = inword[in++] & inputMask;
             highByte |= currentInword<<shift;
             bitsAvail -= width;
             shift -= width;

             if (num == in)
                 outword[out++] = (highByte<<8) | lowByte;
          }
       }
    }
}                


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// Packs 16-bit words in the LSB Significant Bits (LSB) manner
// Codewords that are "width"-bits wide are packed into 8-bit bytes such that
// the lsb of the 1st code word is aligned with the lsb of the output byte. The
// byte is filled from lsb up to msb. If there are not enough bits remaining in the 
// byte being filled to store an entire codeword, then the lsbs of the current codeword
// that fit in the byte are stored, and the remaining msbs of the codeword are
// stored in the lsbs of the next byte. Pairs of packed bytes are then packed
// together into 16-bit words.
//
// For example: Say we have a buffer of 4-bit wide codewords with the following 
// values:  In[ ] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, a, b, c, d, e, f, 0}
// The output packed buffer will be:
//  Out[ ] = {0x4321, 0x8765, 0xcba9, 0x0fed)
#pragma CODE_SECTION(packLSB,".gpakCritical:frame")
void packLSB(ADT_UInt16 *inword,      // input buffer: unpacked data
             ADT_UInt16 *outword,     // output buffer: packed data
             ADT_UInt16 num,      // number of codewords to pack
             ADT_UInt16 width)    // width (in bits) of each codeword to pack
{
	ADT_UInt16 in, out;
	ADT_UInt16 bitsAvail, fillLow, shift;
	ADT_UInt16 lowByte, highByte;
	ADT_UInt16 inputMask, currentIn;

    inputMask = (1<<width)-1;
    lowByte = 0;
    highByte = 0;
    bitsAvail = 8;
    fillLow = 1;
    in = out = 0;
    shift = 0;

    while (in < num) {
       if (fillLow) {
          if (bitsAvail == 0) {
             fillLow = 0;
             bitsAvail = 8;
             shift = 0;
          } else if (bitsAvail < width) {
             // fill remaining bits in lowByte MSBs
             currentIn = inword[in++]&inputMask;
             lowByte |= ((currentIn<<shift) & 0x00ff);
             fillLow = 0;

             // put rest of codeword into highByte LSBs
             highByte = currentIn>>bitsAvail;
             shift = width-bitsAvail;
             bitsAvail = 8 - shift;
          } else {
             currentIn = inword[in++] & inputMask;
             lowByte |= currentIn<<shift;
             bitsAvail -= width;
             shift += width;
          }

          if (num == in)
             outword[out++] = (highByte<<8) | lowByte;
       } else {
          if (bitsAvail == 0) {
              outword[out++] = (highByte<<8) | lowByte;
              lowByte = highByte = 0;
              fillLow = 1;
              bitsAvail = 8;
              shift = 0;
          } else if (bitsAvail < width) {
              // fill remaining bits in highByte MSBs
              currentIn = inword[in++] & inputMask;
              highByte |= ((currentIn<<shift) & 0x00ff);
              outword[out++] = (highByte<<8) | lowByte;
              lowByte = highByte = 0;
              fillLow = 1;

              // put rest of codeword into lowByte LSBs
              lowByte = currentIn>>bitsAvail;
              shift = width-bitsAvail;
              bitsAvail = 8 - shift;

              if (num == in)
                 outword[out++] = lowByte;
          } else {
              currentIn = inword[in++] & inputMask;
              highByte |= currentIn<<shift;
              bitsAvail -= width;
              shift += width;

              if (num == in)
                 outword[out++] = (highByte<<8) | lowByte;
          }
       }
    }
}                


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// Unpacks 16-bit words that were packed in the Most Significant Bits (MSB) manner
#pragma CODE_SECTION(unpackMSB,".gpakCritical:frame")
void unpackMSB(ADT_UInt16 *in,      // input buffer: packed data "wordSize-bits" wide
               ADT_UInt16 *out,     // output buffer: unpacked data "width-bits" wide
               ADT_UInt16 num,      // number of output codewords to unpack
               ADT_UInt16 width,    // width (in bits) of each codeword to unpack
               ADT_UInt16 outLS)    // number of left shifts to apply to output data

{
	ADT_UInt16 inIndex, outIndex;
	ADT_UInt16 mask, bitsLeft, unpackLow;
	ADT_UInt16 temp,shift;
	ADT_UInt16 lowByte, highByte, *currentByte;

    if (width > 8)
        return;

    bitsLeft = 8;
    shift = 8-width;
    mask = ((1<<width) - 1)<<shift;
    if (shift >= outLS)
        shift = shift - outLS;

    inIndex = outIndex = 0;
    lowByte = in[inIndex] & 0xff;
    highByte = (in[inIndex++]>>8) & 0xff;
    unpackLow = 1;
    currentByte = &lowByte;

    while (outIndex < num) {
       if (0 == bitsLeft) {
          // The previous codeword pulled out of the current input byte 
          // depleated the byte of bits. Point to the next input byte.
          bitsLeft = 8;
          if (unpackLow) {
              currentByte = &highByte;
              unpackLow = 0;
          } else {
              currentByte = &lowByte;
              unpackLow = 1;
              lowByte = in[inIndex] & 0xff;
              highByte = (in[inIndex++]>>8) & 0xff;
          }
       } else if ((bitsLeft < width) && (outIndex < num)) {
          // Not enough bits remaining in the current word being unpacked
          // to form a codeword - (i.e. the codeword straddles a byte boundary.
          // Must concatenate the remaining bits with the MSBs of the next word.

          out[outIndex] = *currentByte;
          if (unpackLow) {
             currentByte = &highByte;
             unpackLow = 0;
          } else {
             currentByte = &lowByte;
             unpackLow = 1;
             lowByte = in[inIndex] & 0xff;
             highByte = (in[inIndex++]>>8) & 0xff;
          }

          temp = (*currentByte>>bitsLeft) & mask;
          out[outIndex] |= temp;
          out[outIndex++] >>= shift;
          *currentByte <<= (width-bitsLeft);
          *currentByte &= 0x00ff;
          bitsLeft += (8 - width);
       } else {
          // More than enough bits available in the current byte. The 
          // codeword for extraction is in the MSBs of the byte.
          out[outIndex++] = (mask & *currentByte)>>shift;
          bitsLeft -= width;
          *currentByte <<= width; // shift up the remaining bits into MSBs
          *currentByte &= 0x00ff;
       }
    }
}

