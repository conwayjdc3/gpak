#include "GpakDefs.h"
#include "GpakExts.h"
#include "sysconfig.h"
#include "sysmem.h"
#include <stdio.h>
#include <string.h>
#include "GpakPcm.h"

extern ADT_UInt16 *pPcmEcDaState[],  *pPktEcDaState[];
extern ADT_UInt16 *pPcmEcSaState[],  *pPktEcSaState[];
extern ADT_UInt16 *pPcmEcEchoPath[], *pPktEcEchoPath[];
extern ADT_UInt16 *pPcmEcBackEp[],   *pPktEcBackEp[];
extern ADT_UInt16 *pPcmEcBackFar[],  *pPktEcBackFar[];
extern ADT_UInt16 *pPcmEcBackNear[], *pPktEcBackNear[];


extern ecInstanceInfo_t pcmEcInfo;
extern ecInstanceInfo_t pktEcInfo;

#define EC_CIRC_STATE_PRE   0   // pre trigger-point buffer fill
#define EC_CIRC_STATE_POST  1   // post trigger-point buffer fill
#define CACHE_NOWAIT 0

typedef struct gpakCapt_t {
   int writeCapture;
   int sendCaptureEvt;
   int g168Index; 
   int g168PlayBack;
   int g168ChID;       // Channel ID of EC captures
   int circCapt;
   int circState;
   int circTrigger;
   int calcCheckSum;
   int chanId;         // Channel ID of channel encode/decode captures
} gpakCapt_t;

typedef struct captureEvt {
   EventFifoHdr_t  header;    
   ADT_Int32 aDataAddr, aDataLenI32;
   ADT_Int32 bDataAddr, bDataLenI32;
   ADT_Int32 cDataAddr, cDataLenI32;
   ADT_Int32 dDataAddr, dDataLenI32;
} captureEvt;


gpakCapt_t captCtrl;
#pragma DATA_SECTION (captCtrl, "NON_CACHED_DATA")

CircBufInfo_t captureA;
CircBufInfo_t captureB;
CircBufInfo_t captureC;
CircBufInfo_t captureD;
#pragma DATA_SECTION (captureA, "NON_CACHED_DATA")
#pragma DATA_SECTION (captureB, "NON_CACHED_DATA")
#pragma DATA_SECTION (captureC, "NON_CACHED_DATA")
#pragma DATA_SECTION (captureD, "NON_CACHED_DATA")

#ifndef _DEBUG  // File write of echo capture buffers
   #define writeCapturedBuffers()
   #define writeCapturedCircBuffers()
#else
ADT_UInt16 dumpBuff[256];
#pragma DATA_SECTION (dumpBuff, "SLOW_DATA_SECT:logging")
#pragma DATA_ALIGN   (dumpBuff,  CACHE_L2_LINE_SIZE)

struct CPTVars {
   CircBufInfo_t *captureA, *captureB, *captureC, *captureD;
   gpakCapt_t    *captCtrl;
} CPTVars = {
   &captureA, &captureB, &captureC, &captureD,
   &captCtrl
};
#pragma CODE_SECTION (writeCapturedBuffers, "SLOW_PROG_SECT")
static void writeCapturedBuffers () {
   FILE *f;
   ADT_UInt16 *buff;
   ADT_Word count, total;


   printf ("Printing capture buffers\n");
   total = captureA.BufrSize - getFreeSpace (&captureA) - 1;
   if (0 < total) {
      count = 256;
      buff  = captureA.pBufrBase;
      printf ("Capture A %u samples\n", total);

      f = fopen ("C:\\CaptureA.pcm", "wb");
      while (total) {
         if (total < 256) count = total;
         fwrite (buff, 2, count, f);
         buff += count;
         total -= count;
      }
      fclose (f);
   }
   captureA.PutIndex = 0;

   total = captureB.BufrSize - getFreeSpace (&captureB) - 1;
   if (0 < total) {
      count = 256;
      buff  = captureB.pBufrBase;
      printf ("Capture B %u samples\n", total);

      f = fopen ("C:\\CaptureB.pcm", "wb");
      while (total) {
         if (total < 256) count = total;
         fwrite (buff, 2, count, f);
         buff += count;
         total -= count;
      }
      fclose (f);
   }
   captureB.PutIndex = 0;

   total = captureC.BufrSize - getFreeSpace (&captureC) - 1;
   if (0 < total) {
      count = 256;
      buff  = captureC.pBufrBase;

      printf ("Capture C %u samples\n", total);
      
      f = fopen ("C:\\captureC.pcm", "wb");
      while (total) {
         if (total < 256) count = total;
         fwrite (buff, 2, count, f);
         buff += count;
         total -= count;
      }
      fclose (f);
   }
   captureC.PutIndex = 0;

   total = captureD.BufrSize - getFreeSpace (&captureD) - 1;
   if (0 < total) {
      count = 256;
      buff  = captureD.pBufrBase;

      printf ("Capture D %u samples\n", total);
      
      f = fopen ("C:\\CaptureD.pcm", "wb");
      while (total) {
         if (total < 256) count = total;
         fwrite (buff, 2, count, f);
         buff += count;
         total -= count;
      }
      fclose (f);
   }
   captureD.PutIndex = 0;

   printf ("Capture buffers printed\n");
}

#pragma CODE_SECTION (writeCapturedCircBuffers, "SLOW_PROG_SECT")
static void writeCapturedCircBuffers () {
   FILE *f;
   ADT_Word count, total;


   printf ("Printing capture Circbuffers\n");
   total = captureA.BufrSize - getFreeSpace (&captureA) - 1;
   if (0 < total) {
      count = 256;
      printf ("Capture A %u samples\n", total);

      f = fopen ("C:\\CaptureA.pcm", "wb");
      while (total) {
         if (total < 256) count = total;
         copyCircToLinear (&captureA, dumpBuff, count);
         fwrite (dumpBuff, 2, count, f);
         total -= count;
      }
      fclose (f);
   }
   captureA.PutIndex = 0;
   captureA.TakeIndex = 0;

   total = captureB.BufrSize - getFreeSpace (&captureB) - 1;
   if (0 < total) {
      count = 256;
      printf ("Capture B %u samples\n", total);

      f = fopen ("C:\\CaptureB.pcm", "wb");
      while (total) {
         if (total < 256) count = total;
         copyCircToLinear (&captureB, dumpBuff, count);
         fwrite (dumpBuff, 2, count, f);
         total -= count;
      }
      fclose (f);
   }
   captureB.PutIndex = 0;
   captureB.TakeIndex = 0;

   total = captureC.BufrSize - getFreeSpace (&captureC) - 1;
   if (0 < total) {
      count = 256;
      printf ("Capture C %u samples\n", total);
      
      f = fopen ("C:\\CaptureC.pcm", "wb");
      while (total) {
         if (total < 256) count = total;
         copyCircToLinear (&captureC, dumpBuff, count);
         fwrite (dumpBuff, 2, count, f);
         total -= count;
      }
      fclose (f);
   }
   captureC.PutIndex = 0;
   captureC.TakeIndex = 0;

   total = captureD.BufrSize - getFreeSpace (&captureD) - 1;
   if (0 < total) {
      count = 256;
      printf ("Capture D %u samples\n", total);
      
      f = fopen ("C:\\CaptureD.pcm", "wb");
      while (total) {
         if (total < 256) count = total;
         copyCircToLinear (&captureD, dumpBuff, count);
         fwrite (dumpBuff, 2, count, f);
         total -= count;
      }
      fclose (f);
   }

   captureD.PutIndex = 0;
   captureD.TakeIndex = 0;

   printf ("Capture buffers printed\n");
}

#endif

void startChanCapture (ADT_Int16 chanId);

#pragma CODE_SECTION (captureSignal, "SLOW_PROG_SECT")
int captureSignal (CircBufInfo_t *cBuff, void *data, int samples) {

   if (getFreeSpace (cBuff) < samples) return -1;
   copyLinearToCirc (data, cBuff, samples);
   return 0;
}

//---------------------------------------------
//{  Echo cancellation capture routines
#pragma CODE_SECTION (g168Capture, "SLOW_PROG_SECT")
void g168Capture (ADT_UInt16 index,  ADT_PCM16 *nearInBuff, 
                  ADT_PCM16 *farInBuff, int frameSize) {

   if (index != captCtrl.g168Index) return;
   if (captureA.BufrSize == 0) return;
   if (captCtrl.g168PlayBack) {
      if ((captureA.BufrSize - frameSize) < getFreeSpace (&captureA)) return;
      copyCircToLinear (&captureA, nearInBuff, frameSize);
      copyCircToLinear (&captureB, farInBuff,  frameSize);
   } else {
      captureSignal (&captureA, nearInBuff, frameSize);
      captureSignal (&captureB, farInBuff,  frameSize);
   }
}
#pragma CODE_SECTION (g168PostCapture, "SLOW_PROG_SECT")
void g168PostCapture (ADT_UInt16 index, ADT_PCM16 *nearOutBuff, 
                      ADT_PCM16 *farOutBuff, int frameSize) { 

   ADT_UInt32 Take;
   ADT_UInt16 EcanType;
   ecInstanceInfo_t *ecInfo = NULL;

   if (index != captCtrl.g168Index) return;
   if (captCtrl.g168PlayBack) return;
   if (captureC.BufrSize == 0) return;

   captureSignal  (&captureC, nearOutBuff, frameSize);
   captureSignal  (&captureD, farOutBuff,  frameSize);

   if (captCtrl.circCapt == TRUE) {
      if (captCtrl.circTrigger == TRUE) {
         // the capture trigger has occurred, move Take index ahead by 1/2
         // the length of the capture buffer, so that 50% of captured data
         // is pre-trigger, and 50% is post-trigger.
         Take = captureA.PutIndex + captureA.BufrSize/2;
         if (Take >= captureA.BufrSize)
             Take = Take - captureA.BufrSize;

         captureA.TakeIndex = Take;
         captureB.TakeIndex = Take;
         captureC.TakeIndex = Take;
         captureD.TakeIndex = Take;
         captCtrl.circTrigger = FALSE;
         captCtrl.circState = EC_CIRC_STATE_POST;
      }

      if (captCtrl.circState == EC_CIRC_STATE_PRE) {
         // the state is pre-trigger, continuous capture, adjust Take==Put to
         // maintain 100 % free-space
         captureA.TakeIndex = captureA.PutIndex;
         captureB.TakeIndex = captureB.PutIndex;
         captureC.TakeIndex = captureC.PutIndex;
         captureD.TakeIndex = captureD.PutIndex;
      } 
   }
   EcanType = (index >> 8) & 0xFF;

   if (EcanType == G168_PCM) {
      ecInfo = &pcmEcInfo;
   } else {
      ecInfo = &pktEcInfo;
   }

   // Calculate checksums and append them on end of ec instance structures.
   if (captCtrl.calcCheckSum && (ecInfo != NULL)) {

      index &= 0xff;
      appendCheckSum (pPcmEcChan[index],     ecInfo->EcChanMemSize * 2);
      appendCheckSum (pPcmEcSaState[index],  ecInfo->EcSAStateMemSize * 2);
      appendCheckSum (pPcmEcEchoPath[index], ecInfo->EcEchoPathMemSize * 2);
      appendCheckSum (pPcmEcBackEp[index],   ecInfo->EcBgEchoPathMemSize * 2);
   }

   if ((frameSize + 20) < getFreeSpace (&captureA)) return;

   captCtrl.g168Index = 0xfffe;
   captCtrl.sendCaptureEvt = TRUE;

}

#pragma CODE_SECTION (initEcCapture, "SLOW_PROG_SECT")
static void initEcCapture (ADT_UInt16 chID, ADT_UInt16 index, ADT_Bool circCapture) {

   SWI_disable ();
   captureA.PutIndex = 0;
   captureB.PutIndex = 0;
   captureC.PutIndex = 0;
   captureD.PutIndex = 0;
   captCtrl.g168Index = index;      
   captCtrl.g168PlayBack = FALSE;
   captCtrl.g168ChID = chID;
   captCtrl.circCapt = circCapture;
   captCtrl.circState = EC_CIRC_STATE_PRE;
   captCtrl.circTrigger = FALSE;

   g168PostCapture (index, (ADT_PCM16 *) 0x0, (ADT_PCM16 *) 0x0, 0);
   SWI_enable ();
}
#pragma CODE_SECTION (startEcCapture, "SLOW_PROG_SECT")
void startEcCapture (ADT_UInt16 chID, ADT_UInt16 index) {
   initEcCapture (chID, index, ADT_FALSE);
}

#pragma CODE_SECTION (startEcCircCapture, "SLOW_PROG_SECT")
void startEcCircCapture (ADT_UInt16 chID, ADT_UInt16 index) {
   initEcCapture (chID, index, ADT_TRUE);
}
#pragma CODE_SECTION (startEcPlayBack, "SLOW_PROG_SECT")
void startEcPlayBack (ADT_UInt16 index) {
   captureA.TakeIndex = 0;
   captureB.TakeIndex = 0;
   captureC.TakeIndex = 0;
   captureD.TakeIndex = 0;
   captCtrl.g168Index = index;
   captCtrl.g168PlayBack = TRUE;
}
#pragma CODE_SECTION (triggerEcCircCaptureStop, "SLOW_PROG_SECT")
void triggerEcCircCaptureStop (ADT_UInt16 index) {

   // only trigger when in pre-trigger state
   if ((captCtrl.circTrigger == TRUE) || (captCtrl.circState == EC_CIRC_STATE_POST))
        return;

   if (index != captCtrl.g168Index) return;
   captCtrl.circTrigger = TRUE;

}

#pragma CODE_SECTION (stopEcCapture, "SLOW_PROG_SECT")
void stopEcCapture () {
   captureEvt msg;

   CACHE_WB (captureA.pBufrBase,  captureA.BufrSize * sizeof (ADT_UInt16));
   CACHE_WB (captureB.pBufrBase,  captureB.BufrSize * sizeof (ADT_UInt16));
   CACHE_WB (captureC.pBufrBase,  captureC.BufrSize * sizeof (ADT_UInt16));
   CACHE_WB (captureD.pBufrBase,  captureD.BufrSize * sizeof (ADT_UInt16));

   // Send EcComplete Message
   msg.header.channelId  = captCtrl.g168ChID;
   msg.header.deviceSide = chanTable [captCtrl.g168ChID]->ProcessRate / 1000;

   msg.header.eventCode = EventEcCaptureComplete;
   msg.header.eventLength =  sizeof (msg) - sizeof (EventFifoHdr_t);

   msg.aDataLenI32  = captureA.PutIndex / 2;
   msg.aDataAddr    = (((ADT_Int32) captureA.pBufrBase) >> 2) << 2;

   msg.bDataLenI32 = captureB.PutIndex / 2;
   msg.bDataAddr    = (((ADT_Int32) captureB.pBufrBase) >> 2) << 2;

   msg.cDataLenI32  = captureC.PutIndex / 2;
   msg.cDataAddr    = (((ADT_Int32) captureC.pBufrBase) >> 2) << 2;

   msg.dDataLenI32 = captureD.PutIndex / 2;
   msg.dDataAddr    = (((ADT_Int32) captureD.pBufrBase) >> 2) << 2;

   writeEventIntoFifo ((EventFifoMsg_t *) &msg);
   startEcCapture (0xffff, 0xfffe);  // Clear for next capture
   return;
}
#pragma CODE_SECTION (stopEcCircCapture, "SLOW_PROG_SECT")
void stopEcCircCapture () {

   captureEvt msg;

   CACHE_WB (captureA.pBufrBase,  captureA.BufrSize * sizeof (ADT_UInt16));
   CACHE_WB (captureB.pBufrBase,  captureB.BufrSize * sizeof (ADT_UInt16));
   CACHE_WB (captureC.pBufrBase,  captureC.BufrSize * sizeof (ADT_UInt16));
   CACHE_WB (captureD.pBufrBase,  captureD.BufrSize * sizeof (ADT_UInt16));

   // Send EcComplete Message

   msg.header.channelId  = captCtrl.g168ChID;
   msg.header.deviceSide = chanTable [captCtrl.g168ChID]->ProcessRate / 1000;
   
   msg.header.eventCode = EventEcCaptureComplete;
   msg.header.eventLength =  sizeof (msg) - sizeof (EventFifoHdr_t);

   msg.aDataLenI32  = captureA.BufrSize / 2;
   msg.aDataAddr    = (((ADT_Int32) captureA.pBufrBase) >> 2) << 2;

   msg.bDataLenI32 = captureB.BufrSize / 2;
   msg.bDataAddr    = (((ADT_Int32) captureB.pBufrBase) >> 2) << 2;

   msg.cDataLenI32  = captureC.BufrSize / 2;
   msg.cDataAddr    = (((ADT_Int32) captureC.pBufrBase) >> 2) << 2;

   msg.dDataLenI32 = captureD.BufrSize / 2;
   msg.dDataAddr    = (((ADT_Int32) captureD.pBufrBase) >> 2) << 2;

   writeEventIntoFifo ((EventFifoMsg_t *) &msg);
   startEcCapture (0xffff, 0xfffe);  // Clear for next capture
   return;
}
//}

//----------------------------------------------
//{  Channel input/output capture routines
#pragma CODE_SECTION (startChanCapture, "SLOW_PROG_SECT")
void startChanCapture (ADT_Int16 chanId) {
   captureA.PutIndex = 0;
   captureB.PutIndex = 0;
   captureC.PutIndex = 0;
   captureD.PutIndex = 0;
   captCtrl.chanId = chanId;
}
#pragma CODE_SECTION (stopChanCapture, "SLOW_PROG_SECT")
void stopChanCapture () {
   captureEvt msg;

   CACHE_WB (captureA.pBufrBase,  captureA.BufrSize * sizeof (ADT_UInt16));
   CACHE_WB (captureB.pBufrBase,  captureB.BufrSize * sizeof (ADT_UInt16));
   CACHE_WB (captureC.pBufrBase,  captureC.BufrSize * sizeof (ADT_UInt16));
   CACHE_WB (captureD.pBufrBase,  captureD.BufrSize * sizeof (ADT_UInt16));


   // Send Caputure Complete Message
   msg.header.channelId  = captCtrl.chanId;
   msg.header.deviceSide = chanTable [captCtrl.chanId]->ProcessRate / 1000;

   msg.header.eventCode = EventCaptureComplete;
   msg.header.eventLength =  sizeof (msg) - sizeof (EventFifoHdr_t);

   msg.aDataLenI32  = captureA.BufrSize / 2;
   msg.aDataAddr    = (((ADT_Int32) captureA.pBufrBase) >> 2) << 2;

   msg.bDataLenI32 = captureB.BufrSize / 2;
   msg.bDataAddr    = (((ADT_Int32) captureB.pBufrBase) >> 2) << 2;

   msg.cDataLenI32  = captureC.BufrSize / 2;
   msg.cDataAddr    = (((ADT_Int32) captureC.pBufrBase) >> 2) << 2;

   msg.dDataLenI32 = captureD.BufrSize / 2;
   msg.dDataAddr    = (((ADT_Int32) captureD.pBufrBase) >> 2) << 2;

   writeEventIntoFifo ((EventFifoMsg_t *) &msg);

   startChanCapture (-1);
   return;
}
#pragma CODE_SECTION (captureDecoderInput, "SLOW_PROG_SECT")
void captureDecoderInput  (chanInfo_t *chan, void *data, int samples) {

   if (chan->ChannelId != captCtrl.chanId) return; 
   captureSignal (&captureA, data, samples);
   return;
}
#pragma CODE_SECTION (captureDecoderOutput, "SLOW_PROG_SECT")
void captureDecoderOutput (chanInfo_t *chan, void *data, int samples) {

   if (chan->ChannelId != captCtrl.chanId) return; 
   captureSignal (&captureB, data, samples);
   return;
}
#pragma CODE_SECTION (captureEncoderInput, "SLOW_PROG_SECT")
void captureEncoderInput  (chanInfo_t *chan, void *data, int samples) {

   if (chan->ChannelId != captCtrl.chanId) return; 
   captureSignal (&captureC, data, samples);
   return;
}
#pragma CODE_SECTION (captureEncoderOutput, "SLOW_PROG_SECT")
void captureEncoderOutput (chanInfo_t *chan, void *data, int samples) {

   if (chan->ChannelId != captCtrl.chanId) return; 
   captureSignal (&captureD, data, samples);
   if (captureD.PutIndex + samples < captureD.BufrSize) return;

   stopChanCapture ();
   return;
}
//}

#pragma CODE_SECTION (initGpakCapture, "SLOW_PROG_SECT")
void initGpakCapture () {   

   memset (&captureA, 0, sizeof(CircBufInfo_t));
   memset (&captureB, 0, sizeof(CircBufInfo_t));
   memset (&captureC, 0, sizeof(CircBufInfo_t));
   memset (&captureD, 0, sizeof(CircBufInfo_t));

   captCtrl.writeCapture    = FALSE;
   captCtrl.sendCaptureEvt  = FALSE;
   captCtrl.g168Index       = 0xfffe; 
   captCtrl.g168PlayBack    = FALSE;
   captCtrl.g168ChID        = -1;
   captCtrl.circCapt        = FALSE;
   captCtrl.circState       = EC_CIRC_STATE_PRE;
   captCtrl.circTrigger     = FALSE;
   captCtrl.calcCheckSum    = FALSE;
   captCtrl.chanId          = -1;
}

#pragma CODE_SECTION (GpakDbg, "SLOW_PROG_SECT")
void GpakDbg () {   

   // Check for end of Capture Condition
   if (!captCtrl.sendCaptureEvt) return;

   // Stop capture and send event to host
   if (captCtrl.circCapt == FALSE)
      stopEcCapture ();
   else
      stopEcCircCapture ();
   captCtrl.sendCaptureEvt = FALSE;
   captCtrl.calcCheckSum    = FALSE;

   // DSP to write EC capture to file
   if (!captCtrl.writeCapture) return;
   
   if (captCtrl.circCapt == FALSE)
      writeCapturedBuffers();
   else 
      writeCapturedCircBuffers();
}
