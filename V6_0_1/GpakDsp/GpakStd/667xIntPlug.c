#include <xdc/std.h>
#include <xdc/runtime/System.h>
#include "GpakBios.h"
#include "GpakPcm.h"
#include <stdio.h>
#include <stdint.h>

extern ADT_UInt16 tdmCore;

Int setup_edma_interrupts(Hwi_FuncPtr edmaIsr, int DSPCore) {
    Int eventId;
    Hwi_Params hwiParams;
    Hwi_Handle hwiHandle;
    Int hostInt;
    //Error_Block eb;

    // Initialize the error block
    //Error_init(&eb);
    if (DSPCore != tdmCore) return -1;

    // Map EDMA completion events for TSIP0 Rx=53, Tx=55, TSIP1 Rx=57, Tx=59 to a single Host interrupt on CIC0 channel 32
    hostInt = 32 + (11*DSPCore);

    CpIntc_mapSysIntToHostInt(0, CSL_INTC0_CPU_3_1_EDMA3CCINT0, hostInt);
    CpIntc_dispatchPlug(CSL_INTC0_CPU_3_1_EDMA3CCINT0, edmaIsr, CSL_INTC0_CPU_3_1_EDMA3CCINT0, TRUE);

    CpIntc_mapSysIntToHostInt(0, CSL_INTC0_CPU_3_1_EDMA3CCINT1, hostInt);
    CpIntc_dispatchPlug(CSL_INTC0_CPU_3_1_EDMA3CCINT1, edmaIsr, CSL_INTC0_CPU_3_1_EDMA3CCINT1, TRUE);

    CpIntc_mapSysIntToHostInt(0, CSL_INTC0_CPU_3_1_EDMA3CCINT2, hostInt);
    CpIntc_dispatchPlug(CSL_INTC0_CPU_3_1_EDMA3CCINT2, edmaIsr, CSL_INTC0_CPU_3_1_EDMA3CCINT2, TRUE);

    CpIntc_mapSysIntToHostInt(0, CSL_INTC0_CPU_3_1_EDMA3CCINT3, hostInt);
    CpIntc_dispatchPlug(CSL_INTC0_CPU_3_1_EDMA3CCINT3, edmaIsr, CSL_INTC0_CPU_3_1_EDMA3CCINT3, TRUE);

    // Enable the Host interrupt on Intc 0
    CpIntc_enableHostInt(0, hostInt);

    // Get the corepack's INTC/event combiner input eventId associated with the Host interrupt
    eventId = CpIntc_getEventId(hostInt);

    // Plug the event associated with Host Interrupt.
    // The function must be 'CpIntc_dispatch' and argument 'hostInt'.
    EventCombiner_dispatchPlug(eventId, &CpIntc_dispatch, hostInt, TRUE);

    // Initialize the Hwi parameters
    Hwi_Params_init(&hwiParams);

    // The eventId must be set to the combined event
    hwiParams.eventId = (eventId / 32);

    // The arg must be set to hwiParams.eventId
    hwiParams.arg = hwiParams.eventId;

    // Enable the interrupt.
    hwiParams.enableInt = TRUE;

    // Create the Hwi on interrupt HWI_TDM_COMPLETE=8 then specify 'EventCombiner_dispatch'
    // as the function.
    hwiHandle = Hwi_create(HWI_DMA_COMPLETE, &EventCombiner_dispatch, &hwiParams, NULL);
    if (hwiHandle == 0) return -1;

    return 0;
}

Int setup_edmaErr_interrupts(Hwi_FuncPtr errIsr, int DSPCore) {
    Int eventId;
    Hwi_Params hwiParams;
    Hwi_Handle hwiHandle;
    Int hostInt;

    if (DSPCore != tdmCore) return -1;

    // Map EDMA error events for EDMA3 err=16, TC1 err = 19, TC2 err = 20 to a single Host interrupt on CIC0 channel 40
    hostInt = 2 + (8*DSPCore);

    CpIntc_mapSysIntToHostInt(0, CSL_INTC0_CPU_3_1_EDMATC_ERRINT0, hostInt);
    CpIntc_dispatchPlug(CSL_INTC0_CPU_3_1_EDMATC_ERRINT0, errIsr, CSL_INTC0_CPU_3_1_EDMATC_ERRINT0, TRUE);

    CpIntc_mapSysIntToHostInt(0, CSL_INTC0_CPU_3_1_EDMATC_ERRINT1, hostInt);
    CpIntc_dispatchPlug(CSL_INTC0_CPU_3_1_EDMATC_ERRINT1, errIsr, CSL_INTC0_CPU_3_1_EDMATC_ERRINT1, TRUE);

    CpIntc_mapSysIntToHostInt(0, CSL_INTC0_CPU_3_1_EDMATC_ERRINT2, hostInt);
    CpIntc_dispatchPlug(CSL_INTC0_CPU_3_1_EDMATC_ERRINT2, errIsr, CSL_INTC0_CPU_3_1_EDMATC_ERRINT2, TRUE);

    // Enable the Host interrupt on Intc 0
    CpIntc_enableHostInt(0, hostInt);

    // Get the corepack's INTC/event combiner input eventId associated with the Host interrupt
    eventId = CpIntc_getEventId(hostInt);

    // Plug the event associated with Host Interrupt
    // The function must be 'CpIntc_dispatch' and argument 'hostInt'.
    EventCombiner_dispatchPlug(eventId, &CpIntc_dispatch, hostInt, TRUE);

    // Initialize the Hwi parameters
    Hwi_Params_init(&hwiParams);

    // The eventId must be set to the combined event
    hwiParams.eventId = (eventId / 32);

    // The arg must be set to hwiParams.eventId
    hwiParams.arg = hwiParams.eventId;

    // Enable the interrupt.
    hwiParams.enableInt = TRUE;

    // Create the Hwi on interrupt HWI_TDM_ERR=5 then specify 'EventCombiner_dispatch'
    // as the function.
    hwiHandle = Hwi_create(HWI_DMA_ERROR, &EventCombiner_dispatch, &hwiParams, NULL);
    if (hwiHandle == 0) return -1;

    return 0;
}
Int setup_Combined_TSIP_interrupts(Hwi_FuncPtr Combined_TSIP_DMA_ERR_Isr, int DSPCore) {

    Int eventId;
    Hwi_Params hwiParams;
    Int intVector;

    eventId = CSL_GEM_TSIP0_RFSINT_N ;
    EventCombiner_dispatchPlug(CSL_GEM_TSIP0_RSFINT_N, Combined_TSIP_DMA_ERR_Isr, CSL_GEM_TSIP0_RSFINT_N, TRUE);
    EventCombiner_dispatchPlug(CSL_GEM_TSIP0_XSFINT_N, Combined_TSIP_DMA_ERR_Isr, CSL_GEM_TSIP0_XSFINT_N, TRUE);
    EventCombiner_dispatchPlug(CSL_GEM_TSIP1_RSFINT_N, Combined_TSIP_DMA_ERR_Isr, CSL_GEM_TSIP1_RSFINT_N, TRUE);
    EventCombiner_dispatchPlug(CSL_GEM_TSIP1_XSFINT_N, Combined_TSIP_DMA_ERR_Isr, CSL_GEM_TSIP1_XSFINT_N, TRUE);
    EventCombiner_dispatchPlug(CSL_GEM_TSIP0_ERRINT_N, Combined_TSIP_DMA_ERR_Isr, CSL_GEM_TSIP0_ERRINT_N, TRUE);
    EventCombiner_dispatchPlug(CSL_GEM_TSIP1_ERRINT_N, Combined_TSIP_DMA_ERR_Isr, CSL_GEM_TSIP1_ERRINT_N, TRUE);

    Hwi_Params_init(&hwiParams);
    hwiParams.arg = (eventId / 32);
    hwiParams.eventId = (eventId / 32);
    hwiParams.enableInt = TRUE;

    intVector = HWI_COMBINED_INT;
    Hwi_create(intVector, &EventCombiner_dispatch, & hwiParams, NULL);

    return 0;
}
