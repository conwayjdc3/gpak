#define _CSL_H_         // Avoid csl definitions
#include <stdio.h>
#include <std.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <soc.h>

#include "adt_typedef.h"
#include "GpakDefs.h"
#include "GpakExts.h"
#include "GpakErrs.h"
#include "sysconfig.h"
#include "GpakHpi.h"
extern ADT_UInt16 ProcTearDownChannelMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply);
extern GPAK_ChannelConfigStat_t ProcConfigureChannelMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply);

#define CORE_ID(chan) ((chan >> 8) & 0xFF)
#define CHAN_ID(chan) (chan & 0xFF)
#define CNFR_ID(cnfr) (cnfr & 0xFF)
#define API_VERSION_ID      0x0609u     // G.PAK API Version Id

typedef enum  GpakChannelTypes {
   GpakVoiceChannel = 0,
   GpakDataChannel
} GpakChannelTypes;

typedef struct PcmPcmCfg_t {
   GpakSerialPort_t    InPortA;            /* Input A  Serial Port Id */
   ADT_UInt16          InSlotA;            /*          Time Slot      */
   ADT_UInt16          InPinA;             /*          Serial Pin     */
   GpakSerialPort_t    OutPortA;           /* Output A Serial Port Id */
   ADT_UInt16          OutSlotA;           /*          Time Slot */
   ADT_UInt16          OutPinA;            /*          Serial Pin  */
   GpakSerialPort_t    InPortB;            /* Input B  Serial Port Id */
   ADT_UInt16          InSlotB;            /*          Time Slot */
   ADT_UInt16          InPinB;             /*          Serial Pin  */
   GpakSerialPort_t    OutPortB;           /* Output B Serial Port Id */
   ADT_UInt16          OutSlotB;           /*          Time Slot */
   ADT_UInt16          OutPinB;            /*          Serial Pin  */
   GpakActivation      EcanEnableA;        /* Input A Echo Cancel enable */
   GpakActivation      EcanEnableB;        /* Input B Echo Cancel enable */
   GpakActivation      AECEcanEnableA;     /* Acoustic Echo Cancel enable */
   GpakActivation      AECEcanEnableB;     /* Acoustic Echo Cancel enable */
   GpakFrameHalfMS     FrameHalfMS;        /* Frame Size */
   GpakActivation      NoiseSuppressA;     /* Output A Noise suppression */
   GpakActivation      NoiseSuppressB;     /* Output B Noise suppression */

   GpakActivation      AgcEnableA;          /* Output A AGC enable */
   GpakActivation      AgcEnableB;          /*          AGC enable */
   GpakToneTypes       ToneTypesA;          /* Input A Tone Detect Types */
   GpakToneTypes       ToneTypesB;          /*         Tone Detect Types */
   GpakCidMode_t       CIdModeA;            /* Caller Id mode */
   GpakCidMode_t       CIdModeB;            /* Caller Id mode */
   GpakChannelTypes    Coding;              /* VoiceChannel or DataChannel */
   ADT_Int16           ToneGenGainG1A;      /* Gain Control Block 1A */
   ADT_Int16           OutputGainG2A;       /* Gain Control Block 2A */
   ADT_Int16           InputGainG3A;        /* Gain Control Block 3A */
   ADT_Int16           ToneGenGainG1B;      /* Gain Control Block 1B */
   ADT_Int16           OutputGainG2B;       /* Gain Control Block 2B */
   ADT_Int16           InputGainG3B;        /* Gain Control Block 3B */

   GpakCompandModes    InCompandingA;      /* input  A TDM companding */
   GpakCompandModes    OutCompandingA;     /* output A TDM companding */
   GpakCompandModes    InCompandingB;      /* input  B TDM companding */
   GpakCompandModes    OutCompandingB;     /* output B TDM companding */
} PcmPcmCfg_t;
//  PCM to PKT channel config
typedef struct PcmPktCfg_t {
   GpakSerialPort_t    PcmInPort;          /* PCM Input  Serial Port Id */
   ADT_UInt16          PcmInSlot;          /*            Time Slot */
   ADT_UInt16          PcmInPin;           /*            Serial Pin  */
   GpakSerialPort_t    PcmOutPort;         /* PCM Output Serial Port Id */
   ADT_UInt16          PcmOutSlot;         /*            Time Slot */
   ADT_UInt16          PcmOutPin;          /*            Serial Pin  */
   GpakCodecs          PktInCoding;        /* Packet In  Coding */
   GpakFrameHalfMS     PktInFrameHalfMS;   /*            Frame Size */
   GpakCodecs          PktOutCoding;       /* Packet Out Coding */
   GpakFrameHalfMS     PktOutFrameHalfMS;  /*            Frame Size */
   GpakActivation      PcmEcanEnable;      /* PCM Echo Cancel enable */
   GpakActivation      PktEcanEnable;      /* Packet Echo Cancel enable */
   GpakActivation      AECEcanEnable;      /* Acoustic Echo Cancel enable */
   GpakActivation      VadEnable;          /* VAD enable */
   GpakActivation      AgcEnable;          /* AGC enable */
   GpakToneTypes       ToneTypes;          /* Tone Detect Types */
   GpakCidMode_t       CIdMode;            /* Caller Id mode */
   GpakFaxMode_t       FaxMode;            /* T38 Fax Mode */
   GpakFaxTransport_t  FaxTransport;       /* T.38 Fax Transport Mode */
   GpakChannelTypes    Coding;             /* VoiceChannel or DataChannel */
   ADT_Int16           ToneGenGainG1;      /* Gain Control Block 2B */
   ADT_Int16           OutputGainG2;       /* Gain Control Block 2B */
   ADT_Int16           InputGainG3;        /* Gain Control Block 3B */
   GpakActivation      DtxEnable;          /* Enables packet Dtx */
   GpakCompandModes    InCompanding;       /* input TDM companding */
   GpakCompandModes    OutCompanding;      /* output TDM companding */
} PcmPktCfg_t;

// PKT to PKT channel config
typedef struct PktPktCfg_t {
   GpakCodecs          PktInCodingA;       /* Packet In  A Coding */
   GpakFrameHalfMS     PktInFrameHalfMSA;  /*            A Frame Size */
   GpakCodecs          PktOutCodingA;      /* Packet Out A Coding */
   GpakFrameHalfMS     PktOutFrameHalfMSA; /*            A Frame Size */
   GpakActivation      EcanEnableA;        /* Pkt A Echo Cancel enable */
   GpakActivation      VadEnableA;         /*       VAD enable */
   GpakToneTypes       ToneTypesA;         /*       Tone Detect Types */
   GpakActivation      AgcEnableA;         /*       AGC Enable */
   ADT_Int16           OutputGainG2A;      /* Gain Control Block 2A */
   ADT_Int16           InputGainG3A;       /* Gain Control Block 3A */
   GpakActivation      DtxEnableA;         /* Enables A packet Dtx */

   ADT_UInt16          ChannelIdB;         /* Channel Id B */
   GpakCodecs          PktInCodingB;       /* Packet In B Coding */
   GpakFrameHalfMS     PktInFrameHalfMSB;  /*             Frame Size */
   GpakCodecs          PktOutCodingB;      /* Packet Out B Coding */
   GpakFrameHalfMS     PktOutFrameHalfMSB; /*              Frame Size */
   GpakActivation      EcanEnableB;        /* Pkt B Echo Cancel enable */
   GpakActivation      VadEnableB;         /*       VAD enable */
   GpakToneTypes       ToneTypesB;         /*       Tone Detect Types */
   GpakActivation      AgcEnableB;         /*       AGC Enable */
   ADT_Int16           OutputGainG2B;      /* Gain Control Block 2B */
   ADT_Int16           InputGainG3B;       /* Gain Control Block 3B */
   GpakActivation      DtxEnableB;         /* Enables B packet Dtx */
} PktPktCfg_t;

// Ciruit data channel config
typedef struct CircuitCfg_t {
   GpakSerialPort_t    PcmInPort;          /* PCM Input Serial Port Id */
   ADT_UInt16  PcmInSlot;                  /*           Time Slot */
   ADT_UInt16  PcmInPin;                   /*           Serializer  */
   GpakSerialPort_t    PcmOutPort;         /* PCM Output Serial Port Id */
   ADT_UInt16  PcmOutSlot;                 /*            Time Slot */
   ADT_UInt16  PcmOutPin;                  /*            Serializer */
   ADT_UInt16  MuxFactor;                  /* Circuit Mux Factor */
} CircuitCfg_t;

// Conference to PCM channel config
typedef struct CnfPcmCfg_t {
   ADT_UInt16          ConferenceId;       /* Conference Identifier */
   GpakSerialPort_t    PcmInPort;          /* PCM Input  Serial Port Id */
   ADT_UInt16          PcmInSlot;          /*            Time Slot */
   ADT_UInt16          PcmInPin;           /*            Pin  */

   GpakSerialPort_t    PcmOutPort;         /* PCM Output Serial Port Id */
   ADT_UInt16          PcmOutSlot;         /*            Time Slot */
   ADT_UInt16          PcmOutPin;          /*            Pin  */

   GpakActivation      EcanEnable;         /* Echo Cancel enable */
   GpakActivation      AECEcanEnable;      /* Acoustic Echo Cancel enable */
   GpakActivation      AgcInEnable;        /* Input AGC enable */
   GpakActivation      AgcOutEnable;       /* Output AGC enable */

   GpakToneTypes       ToneTypes;          /* Tone Detect Types */
   ADT_Int16           ToneGenGainG1;      /* Gain Control Block 1 */
   ADT_Int16           OutputGainG2;       /* Gain Control Block 2 */
   ADT_Int16           InputGainG3;        /* Gain Control Block 3 */

   GpakCompandModes    InCompanding;     /* input TDM companding */
   GpakCompandModes    OutCompanding;    /* output TDM companding */

} CnfPcmCfg_t;

// Conference to PKT channel config
typedef struct CnfPktCfg_t {
   ADT_UInt16          ConferenceId;       /* Conference Identifier */
   GpakCodecs          PktInCoding;        /* Packet In Coding */
   GpakCodecs          PktOutCoding;       /* Packet Out Coding */
   GpakActivation      EcanEnable;         /* Echo Cancel enable */
   GpakActivation      VadEnable;          /* VAD enable */
   GpakActivation      AgcInEnable;        /* Input AGC enable */
   GpakActivation      AgcOutEnable;       /* Output AGC enable */
   GpakToneTypes       ToneTypes;          /* Tone Detect Types */
   ADT_Int16           ToneGenGainG1;      /* Gain Control Block 1 */
   ADT_Int16           OutputGainG2;       /* Gain Control Block 2 */
   ADT_Int16           InputGainG3;        /* Gain Control Block 3 */
   GpakFrameHalfMS     PktFrameHalfMS;     /* Pkt Frame Size */
   GpakActivation      DtxEnable;          /* Enables packet Dtx */

} CnfPktCfg_t;

// Composite conference channel config
typedef struct CnfCmpCfg_t {
   ADT_UInt16          ConferenceId;       /* Conference Identifier */
   GpakSerialPort_t    PcmOutPort;         /* PCM Output Serial Port Id */
   ADT_UInt16          PcmOutSlot;         /*            Time Slot */
   ADT_UInt16          PcmOutPin;          /*            Pin  */

   GpakCodecs          PktOutCoding;       /* Packet Out Coding */
   GpakActivation      VadEnable;          /* VAD enable */
   GpakActivation      DtxEnable;          /* Enables packet Dtx */

   GpakCompandModes    InCompanding;     /* input TDM companding */
   GpakCompandModes    OutCompanding;    /* output TDM companding */
} CnfCmpCfg_t;

// Loopback Coder channel config
typedef struct LbCdrCgf_t {
   GpakCodecs          PktInCoding;        /* Packet In Coding */
   GpakCodecs          PktOutCoding;       /* Packet Out Coding */
   GpakFrameHalfMS     PktFrameHalfMS;     /* Pkt Frame Size */
} LbCdrCgf_t;

// Main channel configuration structure
typedef union GpakChannelConfig_t {

    PcmPktCfg_t PcmPkt;    /* PCM to Packet channel type. */
    PcmPcmCfg_t PcmPcm;    /* PCM to PCM channel type. */
    PktPktCfg_t PktPkt;    /* Packet to Packet channel type. */

    CircuitCfg_t Circuit;  /* Circuit Data channel type. */

    CnfPcmCfg_t ConferPcm;  /* Conference PCM channel type. */
    CnfPktCfg_t ConferPkt;  /* Conference Packet channel type. */
    CnfCmpCfg_t ConferComp; /* Conference Composite channel type. */
    LbCdrCgf_t  LpbkCoder;  /* loopback Coder channel type */

} GpakChannelConfig_t;

typedef struct GpakChannelStatus_t {
    ADT_UInt16 ChannelId;          /* Channel Identifier */
    GpakChanType ChannelType;      /* Channel Type */
    ADT_UInt16 NumPktsToDsp;       /* number of input packets recvd. by DSP */
    ADT_UInt16 NumPktsFromDsp;     /* number of output packets sent by DSP */
    ADT_UInt16 NumPktsToDspB;      /* number of input packets recvd. by DSP (B-side of pkt-pkt only) */
    ADT_UInt16 NumPktsFromDspB;    /* number of output packets sent by DSP (B-side of pkt-pkt only) */
    ADT_UInt16 BadPktHeaderCnt;    /* Invalid Packet Headers count */
    ADT_UInt16 PktOverflowCnt;     /* Packet Overflow count */
    ADT_UInt16 PktUnderflowCnt;    /* Packet Underflow count */
    ADT_UInt16 StatusFlags;        /* Status Flag bits */
    GpakChannelConfig_t ChannelConfig;     /* Channel Configuration */
} GpakChannelStatus_t;


ADT_UInt32 gpakFormatChannelConfig (ADT_UInt16 *Msg, GpakChannelConfig_t *pChan,
                                                ADT_UInt16 ChannelId, GpakChanType ChannelType) {

    ADT_UInt32 MsgLenI16;                     /* message length */
    int        Voice;

    Msg[0] = PackBytes (MSG_CONFIGURE_CHANNEL, CORE_ID(ChannelId));
    Msg[1] = PackBytes (CHAN_ID(ChannelId), ChannelType);

    /* Build the Configure Channel message based on the Channel Type. */
    switch (ChannelType) {
    case pcmToPacket:
        if (pChan->PcmPkt.Coding == GpakVoiceChannel) Voice = 1;
        else                                          Voice = 0;
        Msg[2] = PackPortandSlot (pChan->PcmPkt.PcmInPort,    pChan->PcmPkt.PcmInSlot);
        Msg[3] = PackPortandSlot (pChan->PcmPkt.PcmOutPort,   pChan->PcmPkt.PcmOutSlot);
        Msg[4] = PackBytes (pChan->PcmPkt.PktInCoding,  pChan->PcmPkt.PktInFrameHalfMS);
        Msg[5] = PackBytes (pChan->PcmPkt.PktOutCoding, pChan->PcmPkt.PktOutFrameHalfMS);
        Msg[6] = (ADT_UInt16) ( ( pChan->PcmPkt.PcmEcanEnable         << 15) |
                              ((pChan->PcmPkt.PktEcanEnable &    1) << 14) |
                              ((pChan->PcmPkt.VadEnable     &    1) << 13) |
                              ((pChan->PcmPkt.AgcEnable     &    1) << 12) |
                              ((pChan->PcmPkt.AECEcanEnable &    1) << 11) |
                              ((Voice                       &    1) << 10) |
                              ((pChan->PcmPkt.DtxEnable     &    1) << 9) );
        Msg[7] = 0;  // PinIn/PinOut Deprecated
        Msg[8] = (ADT_UInt16) ( ((pChan->PcmPkt.FaxMode     &    3) << 14) | 
                              ((pChan->PcmPkt.FaxTransport  &    3) << 12) |
                              ((pChan->PcmPkt.ToneTypes     & 0x0FFF)) );
        MsgLenI16 = 9;

        // 4_2 ---------------------------------------
        Msg[9] = (ADT_UInt16) ( ((pChan->PcmPkt.OutCompanding & 7) << 5) |
                                ((pChan->PcmPkt.InCompanding  & 7) << 2) |
                                (pChan->PcmPkt.CIdMode & 3));
        Msg[10] = pChan->PcmPkt.ToneGenGainG1;
        Msg[11] = pChan->PcmPkt.OutputGainG2;
        Msg[12] = pChan->PcmPkt.InputGainG3;
        MsgLenI16 += 4;
        // ---------------------- 

        break;

    case pcmToPcm:
        if (pChan->PcmPcm.Coding == GpakVoiceChannel) Voice = 1;
        else                                          Voice = 0;

        Msg[2] = PackPortandSlot (pChan->PcmPcm.InPortA,  pChan->PcmPcm.InSlotA);
        Msg[3] = PackPortandSlot (pChan->PcmPcm.OutPortA, pChan->PcmPcm.OutSlotA);
        Msg[4] = PackPortandSlot (pChan->PcmPcm.InPortB,  pChan->PcmPcm.InSlotB);
        Msg[5] = PackPortandSlot (pChan->PcmPcm.OutPortB, pChan->PcmPcm.OutSlotB);
        Msg[6] = PackBytes ( 
                        ( ( pChan->PcmPcm.EcanEnableA         << 7) |
                          ((pChan->PcmPcm.EcanEnableB    & 1) << 6) |
                          ((pChan->PcmPcm.AECEcanEnableA & 1) << 5) |
                          ((pChan->PcmPcm.AECEcanEnableB & 1) << 4) |
                          ((pChan->PcmPcm.AgcEnableA     & 1) << 3) |
                          ((pChan->PcmPcm.AgcEnableB     & 1) << 2) |
                          ( Voice                             << 1) ),
                           pChan->PcmPcm.FrameHalfMS);
        Msg[7] = 0;  // PinIn/PinOut Deprecated
        Msg[8] = 0;  // PinIn/PinOut Deprecated
        Msg[9]   = (pChan->PcmPcm.ToneTypesA & 0xFFF) | ((pChan->PcmPcm.NoiseSuppressA << 12) & 0x1000);
        Msg[10]  = (pChan->PcmPcm.ToneTypesB & 0xFFF) | ((pChan->PcmPcm.NoiseSuppressB << 12) & 0x1000);
        MsgLenI16 = 11;

        // 4_2 ---------------------------------------
        Msg[11] = (ADT_UInt16) ( ((pChan->PcmPcm.OutCompandingB & 7) << 13) |
                                 ((pChan->PcmPcm.OutCompandingA & 7) << 10) |
                                 ((pChan->PcmPcm.InCompandingB  & 7) << 7)  |
                                 ((pChan->PcmPcm.InCompandingA  & 7) << 4)  |
                                 ((pChan->PcmPcm.CIdModeA & 3) << 2) |
                                 (pChan->PcmPcm.CIdModeB & 3));
        Msg[12] = pChan->PcmPcm.ToneGenGainG1A;
        Msg[13] = pChan->PcmPcm.OutputGainG2A;
        Msg[14] = pChan->PcmPcm.InputGainG3A;
        Msg[15] = pChan->PcmPcm.ToneGenGainG1B;
        Msg[16] = pChan->PcmPcm.OutputGainG2B;
        Msg[17] = pChan->PcmPcm.InputGainG3B;
        MsgLenI16 += 7;
        // ---------------------------------

        break;

    case packetToPacket:
        Msg[2] = PackBytes (pChan->PktPkt.PktInCodingA,  pChan->PktPkt.PktInFrameHalfMSA);
        Msg[3] = PackBytes (pChan->PktPkt.PktOutCodingA, pChan->PktPkt.PktOutFrameHalfMSA);
        Msg[4] = PackBytes (
                     (  ( pChan->PktPkt.EcanEnableA      << 7) |
                        ((pChan->PktPkt.VadEnableA  & 1) << 6) |
                        ((pChan->PktPkt.EcanEnableB & 1) << 5) |
                        ((pChan->PktPkt.VadEnableB  & 1) << 4) |
                        ((pChan->PktPkt.AgcEnableA  & 1) << 3) |
                        ((pChan->PktPkt.AgcEnableB  & 1) << 2) |
                        ((pChan->PktPkt.DtxEnableA  & 1) << 1) |
                        ((pChan->PktPkt.DtxEnableB  & 1) << 0)),
                          CHAN_ID(pChan->PktPkt.ChannelIdB));
        Msg[5] = PackBytes (pChan->PktPkt.PktInCodingB,  pChan->PktPkt.PktInFrameHalfMSB);
        Msg[6] = PackBytes (pChan->PktPkt.PktOutCodingB, pChan->PktPkt.PktOutFrameHalfMSB);
        Msg[7] = pChan->PktPkt.ToneTypesA & 0xFFF;
        Msg[8] = pChan->PktPkt.ToneTypesB & 0xFFF;
        Msg[9] = pChan->PktPkt.OutputGainG2A;
        Msg[10] = pChan->PktPkt.InputGainG3A;
        Msg[11] = pChan->PktPkt.OutputGainG2B;
        Msg[12] = pChan->PktPkt.InputGainG3B;
        MsgLenI16 = 13;
        break;

    case circuitData:
        Msg[2] = PackPortandSlot (pChan->Circuit.PcmInPort, pChan->Circuit.PcmInSlot);
        Msg[3] = PackPortandSlot (pChan->Circuit.PcmOutPort, pChan->Circuit.PcmOutSlot);
        Msg[4] = (ADT_UInt16) (pChan->Circuit.MuxFactor << 8);
        Msg[5] =  0;  // PinIn/PinOut Deprecated
        MsgLenI16 = 6;
        break;

    case conferencePcm:

        Msg[2] = PackBytes (pChan->ConferPcm.ConferenceId,
                        ( ((pChan->ConferPcm.AgcInEnable    & 1) << 5) |
                          ((pChan->ConferPcm.EcanEnable     & 1) << 4) |
                          ((pChan->ConferPcm.AECEcanEnable  & 1) << 3) ) 
                          );
        Msg[3] = PackPortandSlot (pChan->ConferPcm.PcmInPort, pChan->ConferPcm.PcmInSlot);
        Msg[4] = PackPortandSlot (pChan->ConferPcm.PcmOutPort, pChan->ConferPcm.PcmOutSlot);
        Msg[5] = (ADT_UInt16) ( ((pChan->ConferPcm.OutCompanding & 7) << 3) |
                                 (pChan->ConferPcm.InCompanding  & 7));
        Msg[6]  = pChan->ConferPcm.ToneTypes & 0xFFF;
        MsgLenI16 = 7;

        // 4_2 ---------------------------------------
        Msg[7]  = pChan->ConferPcm.ToneGenGainG1;
        Msg[8]  = (ADT_UInt16) (pChan->ConferPcm.AgcOutEnable & 1);
        Msg[9] = pChan->ConferPcm.OutputGainG2;
        Msg[10] = pChan->ConferPcm.InputGainG3;
        MsgLenI16 += 4;
        // ------------------------------------

        break;

    case conferencePacket:
        Msg[2] = PackBytes (pChan->ConferPkt.ConferenceId,
                        ((pChan->ConferPkt.VadEnable  & 1)  << 7) |
                        ((pChan->ConferPkt.AgcInEnable & 1) << 5) |
                        ((pChan->ConferPkt.EcanEnable & 1)  << 4) |
                        ((pChan->ConferPkt.DtxEnable & 1)   << 3) );
        Msg[3] = PackBytes (pChan->ConferPkt.PktInCoding, pChan->ConferPkt.PktOutCoding);
        Msg[4] = pChan->ConferPkt.ToneTypes & 0xFFF;
        MsgLenI16 = 5;

        // 4_2 ---------------------------------------
        Msg[5]  = (ADT_UInt16) (pChan->ConferPkt.AgcOutEnable & 1);
        Msg[6]  = pChan->ConferPkt.ToneGenGainG1;
        Msg[7]  = pChan->ConferPkt.OutputGainG2;
        Msg[8]  = pChan->ConferPkt.InputGainG3;
        MsgLenI16 += 4;

        // 5_0 ------------------------------------
        Msg[9] = pChan->ConferPkt.PktFrameHalfMS;
        MsgLenI16 += 1;

        break;

    case conferenceComposite:
        Msg[2] = PackBytes (pChan->ConferComp.ConferenceId,
                       ((pChan->ConferComp.VadEnable       & 1) << 7) |
                       ((pChan->ConferComp.DtxEnable       & 1) << 6) );
        Msg[3] = PackPortandSlot (pChan->ConferComp.PcmOutPort,   pChan->ConferComp.PcmOutSlot);
        Msg[4] = PackBytes (pChan->ConferComp.PktOutCoding, 
                        ( ((pChan->ConferComp.OutCompanding & 7) << 3) |
                          (pChan->ConferComp.InCompanding   & 7)) );
        MsgLenI16 = 5;
        break;

    // 4_2 ---------------------------------------
    case loopbackCoder:
        Msg[2] = PackBytes (pChan->LpbkCoder.PktInCoding, pChan->LpbkCoder.PktOutCoding);
        Msg[3] = PackBytes (pChan->LpbkCoder.PktFrameHalfMS, 0);
        MsgLenI16 = 4;
        break;

    default:
        MsgLenI16 = 0;
        break;
    }
    return MsgLenI16;
}


typedef struct chanSetup_t {
    ADT_UInt16      chan_id;
    ADT_UInt16      InSlotA;
    ADT_UInt16      OutSlotA;
    ADT_UInt16      InSlotB;
    ADT_UInt16      OutSlotB;
    GpakFrameHalfMS FrameHalfMS;
} chanSetup_t;

chanSetup_t  chan_setup = {0, 0, 0, 1, 1, Frame_5ms};

#if 0 //ss
void custom_pcm_pcm (int setup) {
ADT_UInt32 msg_buff[25];
ADT_UInt32 reply_buff[25];
ADT_UInt16 *Msg, *Rply;
GpakChannelConfig_t cfg;
PcmPcmCfg_t *pcm;

    Msg = (ADT_UInt16 *)msg_buff;
    Rply = (ADT_UInt16 *)reply_buff;

    ApiBlock.ApiVersionId = API_VERSION_ID;

    if (!setup) {
        Msg[0] = PackBytes (MSG_TEAR_DOWN_CHANNEL, CORE_ID (chan_setup.chan_id));
        Msg[1] = (ADT_UInt16) (CHAN_ID(chan_setup.chan_id) << 8);

        ProcTearDownChannelMsg (Msg, Rply);
        return;
    }

    pcm = &(cfg.PcmPcm);

    memset(pcm, 0, sizeof(PcmPcmCfg_t));
    pcm->InPortA        = McBSPPort0;
    pcm->InSlotA        = chan_setup.InSlotA;
    pcm->OutPortA       = McBSPPort0;
    pcm->OutSlotA       = chan_setup.OutSlotA;
    pcm->InPortB        = McBSPPort0;
    pcm->InSlotB        = chan_setup.InSlotB;
    pcm->OutPortB       = McBSPPort0;
    pcm->OutSlotB       = chan_setup.OutSlotB;
    pcm->EcanEnableA    = Disabled;
    pcm->EcanEnableB    = Disabled;
    pcm->AECEcanEnableA = Disabled;
    pcm->AECEcanEnableB = Disabled;
    pcm->FrameHalfMS    = chan_setup.FrameHalfMS;
    pcm->NoiseSuppressA = Disabled;
    pcm->NoiseSuppressB = Disabled;
    pcm->AgcEnableA     = Disabled;    
    pcm->AgcEnableB     = Disabled;    
    pcm->ToneTypesA     = Disabled;    
    pcm->ToneTypesB     = Disabled;    
    pcm->CIdModeA       = Disabled;      
    pcm->CIdModeB       = Disabled;      
    pcm->Coding         = GpakVoiceChannel;        
    pcm->ToneGenGainG1A = 0;
    pcm->OutputGainG2A  = 0; 
    pcm->InputGainG3A   = 0;  
    pcm->ToneGenGainG1B = 0;
    pcm->OutputGainG2B  = 0; 
    pcm->InputGainG3B   = 0;  

    pcm->InCompandingA  = cmpNone8;
    pcm->OutCompandingA = cmpNone8;
    pcm->InCompandingB  = cmpNone8;
    pcm->OutCompandingB = cmpNone8;

    gpakFormatChannelConfig (Msg, &cfg, chan_setup.chan_id, pcmToPcm);
    ProcConfigureChannelMsg (Msg, Rply);

}

#endif

#define PCMPkt   pChanStatus->ChannelConfig.PcmPkt
#define PCMPcm   pChanStatus->ChannelConfig.PcmPcm
#define PKTPkt   pChanStatus->ChannelConfig.PktPkt
#define CIRCData pChanStatus->ChannelConfig.Circuit
#define CNFPcm   pChanStatus->ChannelConfig.ConferPcm
#define CNFPkt   pChanStatus->ChannelConfig.ConferPkt
#define CNFCmp   pChanStatus->ChannelConfig.ConferComp
#define LPBCdr   pChanStatus->ChannelConfig.LpbkCoder

int gpakParseChanStatusResponse (ADT_UInt32 ReplyLength, ADT_UInt16 *Msg, GpakChannelStatus_t *pChanStatus, int *pStatus) {

    *pStatus = (int) Byte0 (Msg[1]);
    if (*pStatus != Cs_Success)
        return -1;

    pChanStatus->ChannelType = (GpakChanType) Byte1 (Msg[2]);
    if (pChanStatus->ChannelType == inactive) 
       return 0;

      if (ReplyLength < 7)
         return -1;

    pChanStatus->BadPktHeaderCnt =  Msg[3];
    pChanStatus->PktOverflowCnt =   Msg[4];
    pChanStatus->PktUnderflowCnt =  Msg[5];
    pChanStatus->StatusFlags =      Msg[6];

    switch (pChanStatus->ChannelType)  {
    case pcmToPacket:
       if (ReplyLength < 14)
          return -1;


       PCMPkt.PcmInPort =      (GpakSerialPort_t) UnpackPort (Msg[7]);
       PCMPkt.PcmInSlot =                         UnpackSlot (Msg[7]);
       PCMPkt.PcmOutPort =     (GpakSerialPort_t) UnpackPort (Msg[8]);
       PCMPkt.PcmOutSlot =                        UnpackSlot (Msg[8]);
       PCMPkt.PktInCoding       = (GpakCodecs)         Byte1 (Msg[9]);
       PCMPkt.PktInFrameHalfMS  = (GpakFrameHalfMS)    Byte0 (Msg[9]);
       PCMPkt.PktOutCoding      = (GpakCodecs)         Byte1 (Msg[10]);
       PCMPkt.PktOutFrameHalfMS = (GpakFrameHalfMS)    Byte0 (Msg[10]);

       if (Msg[11] & 0x8000)  PCMPkt.PcmEcanEnable = Enabled;
       else                   PCMPkt.PcmEcanEnable = Disabled;

       if (Msg[11] & 0x4000)  PCMPkt.PktEcanEnable = Enabled;
       else                   PCMPkt.PktEcanEnable = Disabled;

       if (Msg[11] & 0x2000)  PCMPkt.VadEnable = Enabled;
       else                   PCMPkt.VadEnable = Disabled;

       if (Msg[11] & 0x1000)  PCMPkt.AgcEnable = Enabled;
       else                   PCMPkt.AgcEnable = Disabled;

       if (Msg[11] & 0x0800)  PCMPkt.AECEcanEnable = Enabled;
       else                   PCMPkt.AECEcanEnable = Disabled;

       if (Msg[11] & 0x0400)  PCMPkt.Coding = GpakVoiceChannel;
       else                   PCMPkt.Coding = GpakDataChannel;

       if (Msg[11] & 0x0200)  PCMPkt.DtxEnable = Enabled;
       else                   PCMPkt.DtxEnable = Disabled;
 


       PCMPkt.PcmInPin =  0;   // Deprecated
       PCMPkt.PcmOutPin = 0;  // Deprecated

       PCMPkt.FaxMode =       (GpakFaxMode_t) ((Msg[13] >> 14) & 0x0003);
       PCMPkt.FaxTransport =  (GpakFaxTransport_t) ((Msg[13] >> 12) & 0x0003);
       PCMPkt.ToneTypes =     (GpakToneTypes) (Msg[13] & 0x0FFF);

        // 4_2 ----------------------------------------------------
        PCMPkt.CIdMode       =   (GpakCidMode_t) (Msg[14] & 0x0003);
        PCMPkt.OutCompanding = (GpakCompandModes)((Msg[14] >> 2) & 0x0007);
        PCMPkt.InCompanding  = (GpakCompandModes)((Msg[14] >> 5) & 0x0007);
        PCMPkt.ToneGenGainG1 =   Msg[15];
        PCMPkt.OutputGainG2  =   Msg[16];
        PCMPkt.InputGainG3   =   Msg[17];
        pChanStatus->NumPktsToDsp    = Msg[18];
        pChanStatus->NumPktsFromDsp  = Msg[19];
        // ---------------------------------------------------------

       break;

    case pcmToPcm:
       if (ReplyLength < 16)
          return -1;

       PCMPcm.InPortA =   (GpakSerialPort_t) UnpackPort (Msg[7]);
       PCMPcm.InSlotA =                      UnpackSlot (Msg[7]);
       PCMPcm.OutPortA =  (GpakSerialPort_t) UnpackPort (Msg[8]);
       PCMPcm.OutSlotA =                     UnpackSlot (Msg[8]);
       PCMPcm.InPortB =   (GpakSerialPort_t) UnpackPort (Msg[9]);
       PCMPcm.InSlotB =                      UnpackSlot (Msg[9]);
       PCMPcm.OutPortB =  (GpakSerialPort_t) UnpackPort (Msg[10]);
       PCMPcm.OutSlotB =                     UnpackSlot (Msg[10]);

       if (Msg[11] & 0x8000)    PCMPcm.EcanEnableA = Enabled;
       else                     PCMPcm.EcanEnableA = Disabled;

       if (Msg[11] & 0x4000)    PCMPcm.EcanEnableB = Enabled;
       else                     PCMPcm.EcanEnableB = Disabled;

       if (Msg[11] & 0x2000)    PCMPcm.AECEcanEnableA = Enabled;
       else                     PCMPcm.AECEcanEnableA = Disabled;

       if (Msg[11] & 0x1000)    PCMPcm.AECEcanEnableB = Enabled;
       else                     PCMPcm.AECEcanEnableB = Disabled;

       if (Msg[11] & 0x0800)    PCMPcm.AgcEnableA = Enabled;
       else                     PCMPcm.AgcEnableA = Disabled;

       if (Msg[11] & 0x0400)    PCMPcm.AgcEnableB = Enabled;
       else                     PCMPcm.AgcEnableB = Disabled;

       if (Msg[11] & 0x0200)    PCMPcm.Coding = GpakVoiceChannel;
       else                     PCMPcm.Coding = GpakDataChannel;

       PCMPcm.FrameHalfMS =     (GpakFrameHalfMS) Byte0 (Msg[11]);

       PCMPcm.InPinA =  0;  // Deprecated
       PCMPcm.InPinB =  0;  // Deprecated
       PCMPcm.OutPinA = 0;  // Deprecated
       PCMPcm.OutPinB = 0;  // Deprecated

       PCMPcm.ToneTypesA =  (GpakToneTypes) (Msg[14] & 0xFFF);
       PCMPcm.ToneTypesB =  (GpakToneTypes) (Msg[15] & 0xFFF);
       PCMPcm.NoiseSuppressA = (GpakActivation) ((Msg[14] >> 12) & 0x0001);
       PCMPcm.NoiseSuppressB = (GpakActivation) ((Msg[15] >> 12) & 0x0001);

       // 4_2 ----------------------------------------------------
       PCMPcm.CIdModeA         = (GpakCidMode_t) ((Msg[16] >> 2) & 3);
       PCMPcm.CIdModeB         = (GpakCidMode_t) (Msg[16] & 3);

       PCMPcm.OutCompandingB   = (GpakCompandModes )((Msg[16] >> 13) & 7);
       PCMPcm.OutCompandingA   = (GpakCompandModes )((Msg[16] >> 10) & 7);
       PCMPcm.InCompandingB    = (GpakCompandModes )((Msg[16] >> 7) & 7);
       PCMPcm.InCompandingA    = (GpakCompandModes )((Msg[16] >> 4) & 7);

       PCMPcm.ToneGenGainG1A   = Msg[17];
       PCMPcm.OutputGainG2A    = Msg[18];
       PCMPcm.InputGainG3A     = Msg[19];
       PCMPcm.ToneGenGainG1B   = Msg[20];
       PCMPcm.OutputGainG2B    = Msg[21];
       PCMPcm.InputGainG3B     = Msg[22];
       pChanStatus->NumPktsToDsp    = 0;
       pChanStatus->NumPktsFromDsp  = 0;
       // --------------------------------------------------------
       break;

    case packetToPacket:
       if (ReplyLength < 14)
          return -1;

       PKTPkt.PktInCodingA =         (GpakCodecs)       Byte1 (Msg[7]);
       PKTPkt.PktInFrameHalfMSA =    (GpakFrameHalfMS) Byte0 (Msg[7]);
       PKTPkt.PktOutCodingA =        (GpakCodecs)       Byte1 (Msg[8]);
       PKTPkt.PktOutFrameHalfMSA =   (GpakFrameHalfMS) Byte0 (Msg[8]);

       if (Msg[9] & 0x8000)  PKTPkt.EcanEnableA = Enabled;
       else                  PKTPkt.EcanEnableA = Disabled;

       if (Msg[9] & 0x4000)  PKTPkt.VadEnableA = Enabled;
       else                  PKTPkt.VadEnableA = Disabled;

       if (Msg[9] & 0x2000)  PKTPkt.EcanEnableB = Enabled;
       else                  PKTPkt.EcanEnableB = Disabled;

       if (Msg[9] & 0x1000)  PKTPkt.VadEnableB = Enabled;
       else                  PKTPkt.VadEnableB = Disabled;

       if (Msg[9] & 0x0800)  PKTPkt.AgcEnableA = Enabled;
       else                  PKTPkt.AgcEnableA = Disabled;

       if (Msg[9] & 0x0400)  PKTPkt.AgcEnableB = Enabled;
       else                  PKTPkt.AgcEnableB = Disabled;

       if (Msg[9] & 0x0200)  PKTPkt.DtxEnableA = Enabled;
       else                  PKTPkt.DtxEnableA = Disabled;

       if (Msg[9] & 0x0100)  PKTPkt.DtxEnableB = Enabled;
       else                  PKTPkt.DtxEnableB = Disabled;

       PKTPkt.ChannelIdB =                              Byte0 (Msg[9]);
       PKTPkt.PktInCodingB =           (GpakCodecs)     Byte1 (Msg[10]);
       PKTPkt.PktInFrameHalfMSB =    (GpakFrameHalfMS) Byte0 (Msg[10]);
       PKTPkt.PktOutCodingB =          (GpakCodecs)     Byte1 (Msg[11]);
       PKTPkt.PktOutFrameHalfMSB =   (GpakFrameHalfMS) Byte0 (Msg[11]);

       PKTPkt.ToneTypesA = (GpakToneTypes) (Msg[12] & 0xFFF);
       PKTPkt.ToneTypesB = (GpakToneTypes) (Msg[13] & 0xFFF);

       // 4_2 and beyond ---------------------------------
       pChanStatus->NumPktsToDsp    = Msg[14];
       pChanStatus->NumPktsFromDsp  = Msg[15];
       pChanStatus->NumPktsToDspB    = Msg[16];
       pChanStatus->NumPktsFromDspB  = Msg[17];
       PKTPkt.OutputGainG2A  = Msg[18];
       PKTPkt.InputGainG3A   = Msg[19];
       PKTPkt.OutputGainG2B  = Msg[20];
       PKTPkt.InputGainG3B   = Msg[21];
       // -------------------------------------
       break;

    case circuitData:
       if (ReplyLength < 11)
           return -1;

       CIRCData.PcmInPort =  (GpakSerialPort_t) UnpackPort (Msg[7]);
       CIRCData.PcmInSlot =                     UnpackSlot (Msg[7]);
       CIRCData.PcmOutPort = (GpakSerialPort_t) UnpackPort (Msg[8]);
       CIRCData.PcmOutSlot =                    UnpackSlot (Msg[8]);
       CIRCData.MuxFactor =                     Byte1 (Msg[9]);
       CIRCData.PcmInPin  =   0;  // Deprecated
       CIRCData.PcmOutPin =   0;  // Deprecated
       break;

    case conferencePcm:
       if (ReplyLength < 12)
            return -1;

       CNFPcm.ConferenceId = Byte1 (Msg[7]);

       if (Msg[7] & 0x0020) CNFPcm.AgcInEnable = Enabled;
       else                 CNFPcm.AgcInEnable = Disabled;

       if (Msg[7] & 0x0010)  CNFPcm.EcanEnable = Enabled;
       else                  CNFPcm.EcanEnable = Disabled;

       if (Msg[7] & 0x0008)  CNFPcm.AECEcanEnable = Enabled;
       else                  CNFPcm.AECEcanEnable = Disabled;

       CNFPcm.PcmInPort  =  (GpakSerialPort_t) UnpackPort (Msg[8]);
       CNFPcm.PcmInSlot  =                     UnpackSlot (Msg[8]);
       CNFPcm.PcmOutPort =  (GpakSerialPort_t) UnpackPort (Msg[9]);
       CNFPcm.PcmOutSlot =                     UnpackSlot (Msg[9]);
       CNFPcm.PcmInPin   =  0;  // Deprecated
       CNFPcm.PcmOutPin  =  0;  // Deprecated

       CNFPcm.ToneTypes        =  (GpakToneTypes) (Msg[11] & 0xFFF);

       // 4_2 ----------------------------------------------------
       CNFPcm.AgcOutEnable  = (GpakActivation) (Msg[12] & 1);
       CNFPcm.ToneGenGainG1 = Msg[13];
       CNFPcm.OutputGainG2  = Msg[14];
       CNFPcm.InputGainG3   = Msg[15];
       CNFPcm.OutCompanding = (GpakCompandModes )((Msg[10] >> 3) & 7);
       CNFPcm.InCompanding  = (GpakCompandModes )(Msg[10] & 7);
       // --------------------------------------------------------
       break;

    case conferenceMultiRate:
    case conferencePacket:
       if (ReplyLength < 10)
            return -1;

       CNFPkt.ConferenceId =   Byte1 (Msg[7]);

       if (Msg[7] & 0x0080) CNFPkt.VadEnable = Enabled;
       else                 CNFPkt.VadEnable = Disabled;

       if (Msg[7] & 0x0020) CNFPkt.AgcInEnable = Enabled;
       else                 CNFPkt.AgcInEnable = Disabled;

       if (Msg[7] & 0x0010) CNFPkt.EcanEnable = Enabled;
       else                 CNFPkt.EcanEnable = Disabled;

       if (Msg[7] & 0x0008) CNFPkt.DtxEnable = Enabled;
       else                 CNFPkt.DtxEnable = Disabled;

       CNFPkt.PktInCoding =  (GpakCodecs) Byte1 (Msg[8]);
       CNFPkt.PktOutCoding = (GpakCodecs) Byte0 (Msg[8]);
       CNFPkt.ToneTypes    = (GpakToneTypes) (Msg[9] & 0xFFF);

       // 4_2 ----------------------------------------------------
       CNFPkt.AgcOutEnable  = (GpakActivation) (Msg[10] & 1);
       CNFPkt.ToneGenGainG1 = Msg[11];
       CNFPkt.OutputGainG2  = Msg[12];
       CNFPkt.InputGainG3   = Msg[13];
       pChanStatus->NumPktsToDsp    = Msg[14];
       pChanStatus->NumPktsFromDsp  = Msg[15];
       // --------------------------------------------------------

       if (ReplyLength < 17) CNFPkt.PktFrameHalfMS = (GpakFrameHalfMS) 0;
       else                  CNFPkt.PktFrameHalfMS = (GpakFrameHalfMS) Msg[16];

       break;

    case conferenceComposite:
       if (ReplyLength < 10)
          return -1;

       CNFCmp.ConferenceId = Byte1 (Msg[7]);

       if (Msg[7] & 0x0080) CNFCmp.VadEnable = Enabled;
       else                 CNFCmp.VadEnable = Disabled;

       if (Msg[7] & 0x0040) CNFCmp.DtxEnable = Enabled;
       else                 CNFCmp.DtxEnable = Disabled;

       CNFCmp.PcmOutPort = (GpakSerialPort_t) UnpackPort (Msg[8]);
       CNFCmp.PcmOutSlot =                    UnpackSlot (Msg[8]);
       CNFCmp.PktOutCoding =     (GpakCodecs) Byte1 (Msg[9]);
       CNFCmp.PcmOutPin =     0;  // Deprecated
       CNFCmp.OutCompanding = (GpakCompandModes )((Msg[9] >> 3) & 7);
       CNFCmp.InCompanding  = (GpakCompandModes )(Msg[9] & 7);
       break;

    // 4_2 ---------------------------------------
    case loopbackCoder:
       if (ReplyLength < 8)
          return -1;

        LPBCdr.PktInCoding  = (GpakCodecs) Byte1 (Msg[7]);
        LPBCdr.PktOutCoding = (GpakCodecs) Byte0 (Msg[7]);
        pChanStatus->NumPktsToDsp    = Msg[8];
        pChanStatus->NumPktsFromDsp  = Msg[9];
        break;
    // --------------------------------------------

    default:
       return -1;
    }
    return 0;
}

#if 0 //ss
void custom_chan_status () {
   int replyI8, status, Status;
   ADT_UInt32 msg_buff[25];
   ADT_UInt32 reply_buff[25];
   ADT_UInt16 *Msg, *Rply;
    GpakChannelStatus_t chanStatus;

    Msg = (ADT_UInt16 *)msg_buff;
    Rply = (ADT_UInt16 *)reply_buff;
    ApiBlock.ApiVersionId = API_VERSION_ID;

    Msg[0] = PackBytes (MSG_CHAN_STATUS_RQST, CORE_ID(chan_setup.chan_id));
    Msg[1] = (ADT_UInt16) (CHAN_ID (chan_setup.chan_id) << 8);

    replyI8 = ProcChannelStatusMsg (Msg, Rply);
    status = gpakParseChanStatusResponse (replyI8, Rply, &chanStatus, &Status);
}
#endif

typedef struct TdmPattern_t {
   int              chan_id;
   ADT_UInt16       value;
   GpakDeviceSide_t deviceSide;
} TdmPattern_t;

TdmPattern_t tdm_pattern = {0, 0x1234, NullDevice};

void custom_fixed_value () {
   chanInfo_t *chan;

   chan = chanTable[tdm_pattern.chan_id];
   chan->tdmPattern.deviceSide = tdm_pattern.deviceSide;
   chan->tdmPattern.value      = tdm_pattern.value;
}
