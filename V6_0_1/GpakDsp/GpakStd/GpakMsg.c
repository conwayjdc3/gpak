/*
 * Copyright (c) 2004-2007, Adaptive Digital Technologies, Inc.
 *
 * File Name: GpakMsg.c
 *
 * Description:
 *   This file contains G.PAK functions to service the Host API messages.
 *
 *
 * Version: 1.0
 *
 * Revision History:
 *   11/13/01 - Initial release.
 *   04/2005  - Added GSM EFR
 *
 */
//#define CPU_TEST
#include <std.h>
#include <stdio.h>
#include <ctype.h>
//#include <hwi.h>
//#include <swi.h>
//#include <tsk.h>
//#include <sem.h>

// Application related header files.
#include "GpakDefs.h"
#include "GpakHpi.h"
#include "GpakErrs.h"
#include "GpakExts.h"
#include  <sysmem.h>
#include "sysconfig.h"

#ifndef AEC_LIB_VERSION
   #define AEC_LIB_VERSION 0
#endif

ADT_UInt16 minChanCore = 2; // lowest core that can process gpak channels
int msgCore = 1; // default core that processes gpak messages

SEM_Handle msgSem = NULL;
extern void process_custom_cmd ();
extern int custom_cmd;
extern far int frameTaskCnt;

#pragma DATA_SECTION (msgTaskStack, "STACK_ALLOCATION:MsgTask")
#pragma DATA_ALIGN   (msgTaskStack, 128)
// jdc why was this sized so large?? static char msgTaskStack [0x4800];
static char msgTaskStack [1024*4];
extern GPAK_TearDownChanStat_t DeactivateChannel (chanInfo_t *chan);
extern GpakTestMode RTPLoopBack;
extern ADT_UInt16 ProcTxCallerIDMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply);
extern ADT_UInt16 ProcMemoryTestMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply);
extern ADT_UInt16 ProcNewWavefileMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply);
extern ADT_UInt16 ProcLoadWavefileMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply);
extern GPAK_PlayWavefileStat_t ProcPlayWavefileMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply);

#if (DSP_TYPE == 54)
#define GPAK_INIT_STATUS 0x5555  // G.PAK Initialization Status value
#elif (DSP_TYPE == 55)
#define GPAK_INIT_STATUS 0x5555  // G.PAK Initialization Status value
#elif (DSP_TYPE == 64)
#define GPAK_INIT_STATUS 0x55555555  // G.PAK Initialization Status value
#endif

#define MAX_CMD_MSG_BYTES   2052  // max length of a command message (bytes)
#define MSG_TASK_POLL_PER     1   // Message task's API polling period (msec)
//====================================================================
//
//====================================================================
// Note: The HostApiPtr is located at a fixed address 
// (for example byte address of 0x200, it must be matched with the APIs). 
#ifndef HWI_STACKCHECK_DISABLE
extern int HWI_STKTOP, HWI_STKBOTTOM;
#endif
void msgTask (void);
#ifdef __TI_EABI__
extern void _c_int00();
const ADT_UInt32 EntryPoint = (ADT_UInt32)(_c_int00);
#else
extern void c_int00();
const ADT_UInt32 EntryPoint = (ADT_UInt32)(c_int00);
#endif
GpakIfBlock_t **HostApiPtr = (GpakIfBlock_t **)&EntryPoint;   // Host's pointer to DSP's API memory block
#pragma DATA_SECTION(EntryPoint,     "HOST_API")

ADT_UInt32 CmdMsgBuffer  [MAX_CMD_MSG_BYTES/4];     // Command message buffer
ADT_UInt32 ReplyMsgBuffer[MAX_CMD_MSG_BYTES/4];     // Reply message buffer
#pragma DATA_SECTION(CmdMsgBuffer,   "CMDMSGBUF")
#pragma DATA_SECTION(ReplyMsgBuffer, "RPLYMSGBUF")


GpakIfBlock_t ApiBlock;         // G.PAK API memory block used by host
ADT_UInt8 ApiWrBlock[CACHE_L2_LINE_SIZE];
ADT_UInt8 ApiRdBlock[CACHE_L2_LINE_SIZE];
ADT_UInt8 ApiRdWrBlock[CACHE_L2_LINE_SIZE];

#pragma DATA_SECTION(ApiBlock,       "GPAK_IF")
#pragma DATA_ALIGN  (ApiBlock,       CACHE_L2_LINE_SIZE)

#pragma DATA_ALIGN (ApiWrBlock,     CACHE_L2_LINE_SIZE)
#pragma DATA_ALIGN (ApiRdBlock,     CACHE_L2_LINE_SIZE)
#pragma DATA_ALIGN (ApiRdWrBlock,   CACHE_L2_LINE_SIZE)
#pragma DATA_SECTION(ApiWrBlock,    "SLOW_DATA_SECT:APIBlock")
#pragma DATA_SECTION(ApiRdBlock,    "SLOW_DATA_SECT:APIBlock")
#pragma DATA_SECTION(ApiRdWrBlock,  "SLOW_DATA_SECT:APIBlock")

ADT_Word *CpuUsage   = &ApiBlock.u1.CpuUsage1;
ADT_Word *CpuPkUsage = &ApiBlock.u2.CpuPkUsage1;

// System status variables.
volatile ADT_UInt16 NumActiveChannels = 0;        // number of active channels
#pragma DATA_SECTION (NumActiveChannels, "SHARED_DATA_SECT:NumActiveChannels") 



ADT_UInt16 SelfTestStatus = 0x0007; // Self Test status flags

// System test variables.
GpakTestMode CurrentTestMode;        // current Test Mode
ADT_UInt16   CurrentTestParm;        // current Test Parameter

far int gblPkLoadingRst;
#pragma DATA_SECTION (gblPkLoadingRst, "SHARED_DATA_SECT:statsReset") 

//coreLock ResourceLocks [RESOURCE_CNT];
//#pragma DATA_SECTION (ResourceLocks, "SHARED_LOCK_SECT:ResourceLocks") 
//#pragma DATA_ALIGN   (ResourceLocks, 64) 

extern coreLock fifoLock;
coreLock *pfifoLock = &fifoLock;
//#pragma DATA_SECTION (fifoLock, "SHARED_LOCK_SECT:fifoLock") 
//#pragma DATA_ALIGN   (fifoLock, 64) 

#define inRange(lo,val,hi)  ((lo <= val) && (val <= hi)) 
#define OutOfRange(a,lo,hi)  ( (a<lo) || (hi<a) )
#define ReplyBytes(words)    ( (words) * 2 )

#ifdef _DEBUG  // MSGVars
typedef struct MSGVars_t {
   GpakIfBlock_t      *ApiBlock;
   GpakIfBlock_t    ***HostApiPtr;
   const ADT_UInt32   *EntryPoint;

   ADT_UInt32 **CmdMsgBuffer;
   ADT_UInt32 **ReplyMsgBuffer;
   volatile ADT_UInt16  *NumActiveChannels;
   ADT_UInt32 **eventFIFOBuff;
   CircBufInfo_t *eventFIFOInfo;

   coreLock **ResourceLocks;
   coreLock *fifoLock;
} MSGVars_t;

const MSGVars_t MSGVars = {
   &ApiBlock,
   &HostApiPtr,
   &EntryPoint,

   (ADT_UInt32 **) &CmdMsgBuffer,
   (ADT_UInt32 **) &ReplyMsgBuffer,
   &NumActiveChannels,
   (ADT_UInt32 **) &eventFIFOBuff,
   &eventFIFOInfo,

   (coreLock **) &ResourceLocks,
   &fifoLock
};
#endif

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// InitGpakInterface - Initialize G.PAK interface with host processor.
//
// FUNCTION
//  This function initializes the G.PAK interface with the host processor.
//
// RETURNS
//  nothing
//
void ServiceGpakInterface (void);

SWI_Handle SWI_Msg = NULL;
static SWI_Attrs swi_attr = { (SWI_Fxn) ServiceGpakInterface, 0, 0, 0, 0};

//  Channel locking is used to allow the messaging task to modify 
//  a channel's data structures (tone gen, AEC parameters, etc.) while
//  the channel is active.  This ensures that the framing and the
//  messaging tasks have exclusive access to the channel's data structures
#pragma CODE_SECTION (CHN_lock, "FAST_PROG_SECT")
void CHN_lock (chanInfo_t *pChan) {
   SWI_disable ();  // Prevent higher priority framing tasks access causing lock-out
}

// Release access to channel structure.
#pragma CODE_SECTION (CHN_unlock, "FAST_PROG_SECT")
void CHN_unlock (chanInfo_t *pChan) {
   SWI_enable ();
}


// 
#pragma CODE_SECTION (ADDRESS_lock, "INIT_PROG_SECT")
int ADDRESS_lock (volatile int *lockAddress) {
   int mask;

   // Prevent multiple thread access
   mask = HWI_disable ();
   if (1 < DSPTotalCores)  wordLock (DSPCore, lockAddress);
   return mask;
}

#pragma CODE_SECTION (ADDRESS_unlock, "INIT_PROG_SECT")
void ADDRESS_unlock (volatile int *lockAddress, int mask) {
   if (1 < DSPTotalCores) wordUnlock (lockAddress);
   HWI_restore (mask);
}


// Frame locking is used to allow the messaging and framing tasks to have sole access
// to shared resources.  The shared resources locked by this function are: framing
// queues, event messages, conference tone generation, and tone detection allocation

// Wait while another core has access to the critical structures
#pragma CODE_SECTION (FRAME_lock, "FAST_PROG_SECT")
int FRAME_lock (ADT_Int32 lockIdx) {
   return ADDRESS_lock (&ResourceLocks[lockIdx].Inst);
}

// Release access to the core's framing structure
#pragma CODE_SECTION (FRAME_unlock, "FAST_PROG_SECT")
void FRAME_unlock (ADT_Int32 lockIdx, ADT_UInt32 mask) {
   ADDRESS_unlock (&ResourceLocks[lockIdx].Inst, mask);
}


#pragma CODE_SECTION (checkApiVer, "SLOW_PROG_SECT")
int checkApiVer() {
   int rv = 0;    

   if (SysAlgs.apiCacheEnable) {
      CACHE_INV(ApiBlock.pRdBlock, CACHE_L2_LINE_SIZE);
      rv = (ApiBlock.pRdBlock->ApiVersionId > DEFAULT_GPAK_API_VERSION);
   } else {
      rv = (ApiBlock.ApiVersionId > DEFAULT_GPAK_API_VERSION);
   }
   return rv;
}


//========================================================================
//
//  Internal message processing routines
//
//  The inputs into all message processing routine are:
//    pCmd  - Pointer to message buffer
//    pRply - Pointer to reply buffer
//
//  Return values are either reply length or reply status
//
//========================================================================
#pragma CODE_SECTION (setupTdmFixedValue, "SLOW_PROG_SECT")
static int setupTdmFixedValue (chanInfo_t *chan, ADT_UInt16 *pCmd) {
   GpakSerialPort_t port;
   ADT_UInt16 slot, value;
   GpakActivation mode;
   GpakDeviceSide_t deviceSide;

   if (ApiBlock.ApiVersionId <= 0x0604u) {
      port = (GpakSerialPort_t) pCmd[0];
      slot = pCmd[1];
      if (port == SerialPortNull) return 0;

      if ((port == chan->pcmToPkt.OutSerialPortId) && (slot == chan->pcmToPkt.OutSlotId_MuxFact))
         deviceSide = ADevice;
      else if ((port == chan->pktToPcm.OutSerialPortId) && (slot == chan->pktToPcm.OutSlotId))
         deviceSide = BDevice;
      else
         deviceSide = NullDevice;
   } else {
      deviceSide = (GpakDeviceSide_t) pCmd[0];
   }

   if ((deviceSide != ADevice) && (deviceSide != BDevice) && (deviceSide != NullDevice))
      return 0;

   value = pCmd[2];
   mode  = (GpakActivation) (pCmd[3] & 1);
   if (mode == Disabled)
      deviceSide = NullDevice;

   // Compensate for buffer allocations.
   if (chan->channelType == pcmToPcm) {
      if (deviceSide == ADevice) deviceSide = BDevice;
      else if (deviceSide == BDevice) deviceSide = ADevice;
   }

   chan->tdmPattern.deviceSide = deviceSide;
   chan->tdmPattern.value = value;
   return 1;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//   ProcTestModeMsg - Process a Test Mode message.
//    Returns reply length
#pragma CODE_SECTION (ProcTestModeMsg, "SLOW_PROG_SECT")
static ADT_UInt16 ProcTestModeMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {

   GpakTestMode TestMode;  
   ADT_UInt16 TestParm;    
   int ByteCnt, ShortCnt;
   ADT_UInt16 ChannelId;        
   chanInfo_t *chan;   

#ifdef _DEBUG
   ADT_UInt16 StackFree;   
   int  *pMem;
   //int StackValue;
#endif

   // Get the Test Mode and Test Parameter from the command message.
   TestMode = (GpakTestMode) pCmd[1];
   TestParm =  pCmd[2];

   // Prepare the reply message.
   pReply[0] |= (MSG_TEST_REPLY << 8);
   pReply[1] = TestMode;
   pReply[2] = Tm_Success;
   pReply[3] = 0;


   switch (TestMode) {
   case DisableTest:
      RTPLoopBack = DisableTest;
      break;

   case PacketLoopBack:   // Loop back packets on packet channels
      RTPLoopBack = PacketLoopBack;
      pReply[2] = Tm_Success;     
      break;

   case PacketDaisyChain:   // Daisy chain packet to packet channels
      RTPLoopBack = PacketDaisyChain;
      pReply[2] = Tm_Success;     
      break;

   case PacketCrossOver:   // Odd-even cross-over on packet channels
      RTPLoopBack = PacketCrossOver;
      pReply[2] = Tm_Success;     
      break;

#ifdef  _DEBUG
   case SyncPcmInput:
      // Setup the current Test Mode and Test Parameter.
      CurrentTestMode = TestMode;
      CurrentTestParm = TestParm;
      break;

   case TogglePCMInTone:
      pReply[3] = TogglePCMInSignal (TestParm);
      break;

   case TogglePCMOutTone:
      pReply[3] = TogglePCMOutSignal (TestParm);
      break;

   case StackFreeAtBottom:  // Measure free stack bottom to top
      StackFree = 0;
#ifndef HWI_STACKCHECK_DISABLE
      pMem = &HWI_STKTOP;
      StackValue = *pMem++;
      while (pMem < &HWI_STKBOTTOM) {
         if (*pMem++ != StackValue) break;
         StackFree++;
      }
#endif
      pReply[3] = StackFree;
      break;

   case StackFreeAtTop:   // Measure the stack free from top down.
      StackFree = 0;
#ifndef HWI_STACKCHECK_DISABLE
      pMem = &HWI_STKBOTTOM;
      StackValue = *pMem--;
      while (&HWI_STKTOP < pMem) {
         if (*pMem != StackValue) break;
         StackFree++;
      }
#endif
      pReply[3] = StackFree;
      break;

   case AddressValue:      //  Return value at specified address
      pMem      = (int *) TestParm;
      pReply[3] = (ADT_UInt16) *pMem;
      break;

   case InternalChannelLink:    // Link pcm-pkt channels
      pReply[2] = pktPktLoopBack ((TestParm >> 8) & 0xff, TestParm & 0xff);
      break;
#endif

   case StartChanCapt:
      ChannelId = TestParm & 0xff;
      if (sysConfig.maxNumChannels <= ChannelId) {
        pReply[2] = Tm_InvalidTest;
        break;
      }
      startChanCapture (ChannelId);
      pReply[2] = Tm_Success;     
      break;

   case StartEcCapt:    //  TestParm = side | Channel ID
      ChannelId = TestParm & 0xff;
      if (sysConfig.maxNumChannels <= ChannelId) {
        pReply[2] = Tm_InvalidTest;
        break;
      }
      if (TestParm <= 0xff)
         startEcCapture (ChannelId, chanTable[ChannelId]->pcmToPkt.EcIndex);
      else 
         startEcCapture (ChannelId, chanTable[ChannelId]->pktToPcm.EcIndex);

      pReply[2] = Tm_Success;     
      break;

   case TdmFixedValue:
      ChannelId = pCmd[2];
      if (ChannelId >= sysConfig.maxNumChannels) {
         pReply[2] = Tm_InvalidTest;
         return (ReplyBytes(3));
      }

      chan = chanTable[ChannelId];
      if (setupTdmFixedValue(chan, &pCmd[3]) == 0) {
         pReply[2] = Tm_InvalidTest;
         return (ReplyBytes(3));
      }
      break;

   case TdmLoopback:
      if (updateTdmLoopback ( (GpakSerialPort_t)pCmd[2], (GpakActivation)pCmd[3]) == 0) {
         pReply[2] = Tm_InvalidTest;
         return (ReplyBytes(3));
      }
      break;

   case CustomVarLen:
      ByteCnt = customMsg (&pCmd[2], &pReply[3]);
      if (ByteCnt < 0) {
         pReply[2] = Tm_InvalidTest;
         ShortCnt = 0;
      } else 
         ShortCnt = (ByteCnt + 1) / 2;
      return (ReplyBytes (3 + ShortCnt));

   case ReadPlayRecAddr:
      pReply [3] = ((ADT_UInt32) PlayRecStartAddr) >> 16;
      pReply [4] = ((ADT_UInt32) PlayRecStartAddr) & 0xFFFF;
      pReply[2] = Tm_Success;     
      return ReplyBytes (5);
      
   default:
      ByteCnt = ProcessUserTest (TestMode, TestParm, &pReply[3]);
      if (ByteCnt < 0) {
         pReply[2] = Tm_InvalidTest;
         ShortCnt = 0;
      } else 
         ShortCnt = (ByteCnt + 1) / 2;
      return (ReplyBytes (3 + ShortCnt));

   }

   // Return with the length of the reply message in transfer units.
   return ReplyBytes (4);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ProcSystemConfigMsg - Process a System Configuration Request message.
//    Returns reply length
#pragma CODE_SECTION (ProcSystemConfigMsg, "SLOW_PROG_SECT")
static ADT_UInt16 ProcSystemConfigMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {

   // Prepare the reply message.
   pReply[0] |= (MSG_SYS_CONFIG_REPLY << 8);

   // Store the Sytem Configuration information in the reply message.
   pReply[1]  = (sysConfig.gpakVersion >> 16);
   pReply[2]  = (sysConfig.gpakVersion & 0xFFFF);
   pReply[3]  = (sysConfig.maxNumChannels << 8) | (NumActiveChannels & 0xFF);
   pReply[4]  = (sysConfig.enab.LongWord >> 16);
   pReply[5]  = (sysConfig.enab.LongWord & 0xFFFF);
   pReply[6]  = sysConfig.packetProfile;
   pReply[7]  = sysConfig.rtpPktType;
   pReply[8]  = sysConfig.cfgFramesChans;
   pReply[9]  = sysConfig.numSlotsOnStream[0];
   pReply[10] = sysConfig.numSlotsOnStream[1];
   pReply[11] = sysConfig.numSlotsOnStream[2];
   pReply[12] = sysConfig.maxSlotsSupported[0];
   pReply[13] = sysConfig.maxSlotsSupported[1];
   pReply[14] = sysConfig.maxSlotsSupported[2];
   pReply[15] = sysConfig.maxG168PcmTapLength / 8;
   pReply[16] = sysConfig.maxG168PcmNFIRSegments;
   pReply[17] = sysConfig.maxG168PcmFIRSegmentLength / 8;
   pReply[18] = sysConfig.maxG168PktTapLength / 8;
   pReply[19] = sysConfig.maxG168PktNFIRSegments;
   pReply[20] = sysConfig.maxG168PktFIRSegmentLength / 8;
   pReply[21] = (sysConfig.maxNumConferences << 8) | (sysConfig.maxNumConfChans & 0xFF);
   pReply[22] = SelfTestStatus;
   pReply[23] = sysConfig.numPcmEcans;
   pReply[24] = sysConfig.numPktEcans;
   pReply[25] = (sysConfig.maxToneDetTypes & 0xff) | (NumActiveChannels & 0xFF00);

   pReply[26] = sysConfig.AECInstances;
   pReply[27] = sysConfig.AECTailLen;
   return ReplyBytes (28);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ProcReadSystemParmsMsg - Process a Read System Parameters message.
//    Returns reply length
#pragma CODE_SECTION (ProcReadSystemParmsMsg, "SLOW_PROG_SECT")
static ADT_UInt16 ProcReadSystemParmsMsg (ADT_Int16 *pCmd, ADT_UInt16 *pReply) {

   // Prepare the reply message.
   pReply[0] |= (MSG_READ_SYS_PARMS_REPLY << 8);

   // Store the Sytem Parameters information in the reply message.
   pReply[1] = sysConfig.agcTargetPower;
   pReply[2] = sysConfig.agcMinGain;
   pReply[3] = sysConfig.agcMaxGain;

   pReply[4] = sysConfig.vadNoiseFloorZThresh;
   pReply[5] = sysConfig.vadHangOverTime;
   pReply[6] = sysConfig.vadWindowMs;

   pReply[7] = ((g168_PCM_EcParams.NLPType & 0x000F) |
             ((g168_PCM_EcParams.AdaptEnable << 4) & 0x0010) |
             ((g168_PCM_EcParams.G165DetectEnable << 5) & 0x0020) |
             ((g168_PKT_EcParams.NLPType << 8) & 0x0F00) |
             ((g168_PKT_EcParams.AdaptEnable << 12) & 0x1000) |
             ((g168_PKT_EcParams.G165DetectEnable << 13) & 0x2000));

   pReply[8] =  g168_PCM_EcParams.TapLength/8;
   pReply[9] =  g168_PCM_EcParams.DoubleTalkThres;
   pReply[10] = g168_PCM_EcParams.NLPThres;
   pReply[11] = g168_PCM_EcParams.NLPUpperLimitThresConv;
   pReply[12] = g168_PCM_EcParams.NLPUpperLimitThresUnConv;
   pReply[13] = g168_PCM_EcParams.NLPMaxSupp;
   pReply[14] = 0-g168_PCM_EcParams.CNGNoiseThreshold;
   pReply[15] = g168_PCM_EcParams.AdaptLimit;
   pReply[16] = g168_PCM_EcParams.CrossCorrLimit;
   pReply[17] = g168_PCM_EcParams.NFIRSegments;
   pReply[18] = g168_PCM_EcParams.FIRSegmentLength/8;
   pReply[19] = g168_PCM_EcParams.FIRTapCheckPeriod/8;

   pReply[20] = g168_PKT_EcParams.TapLength/8;
   pReply[21] = g168_PKT_EcParams.DoubleTalkThres;
   pReply[22] = g168_PKT_EcParams.NLPThres;
   pReply[23] = g168_PKT_EcParams.NLPUpperLimitThresConv;
   pReply[24] = g168_PKT_EcParams.NLPUpperLimitThresUnConv;
   pReply[25] = g168_PKT_EcParams.NLPMaxSupp;
   pReply[26] = 0-g168_PKT_EcParams.CNGNoiseThreshold;
   pReply[27] = g168_PKT_EcParams.AdaptLimit;
   pReply[28] = g168_PKT_EcParams.CrossCorrLimit;
   pReply[29] = g168_PKT_EcParams.NFIRSegments;
   pReply[30] = g168_PKT_EcParams.FIRSegmentLength/8;
   pReply[31] = g168_PKT_EcParams.FIRTapCheckPeriod/8;

   pReply[32] = sysConfig.numConfDominant;
   pReply[33] = sysConfig.maxConfNoiseSuppress;

   pReply[34] = sysConfig.agcLowSigThreshDBM;
   pReply[35] = sysConfig.agcTargetPowerB; 
   pReply[36] = sysConfig.agcMinGainB;
   pReply[37] = sysConfig.agcMaxGainB;
   pReply[38] = sysConfig.agcLowSigThreshDBMB;

   if (ApiBlock.ApiVersionId <= DEFAULT_GPAK_API_VERSION)
      return ReplyBytes (39);

   // 4_2, 5_0 -----------------------------------------------
   pReply[39] = (sysConfig.type2CID & 1);
   pReply[40] = sysConfig.numSeizureBytes;
   pReply[41] = sysConfig.numMarkBytes;
   pReply[42] = sysConfig.numPostambleBytes;
   pReply[43] = sysConfig.fskType;
   pReply[44] = sysConfig.fskLevel;
   pReply[45] = sysConfig.msgType;

   pReply[46] = g168_PCM_EcParams.MaxDoubleTalkThres;
   pReply[47] = g168_PKT_EcParams.MaxDoubleTalkThres;
   pReply[7] |= ((g168_PCM_EcParams.MixedFourWireMode  << 6) & 0x0040);
   pReply[7] |= ((g168_PKT_EcParams.MixedFourWireMode  << 14) & 0x4000);
   if (g168_PCM_EcParams.ReconvergenceCheckEnable)
      pReply[7] |=  0x8080; // bit 15 and bit 7

   if (sysConfig.VadReport == Enabled)
      pReply[39] |= (1<<1);

   // Return with the length of the reply message.
   return ReplyBytes (48);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ProcWriteSystemParmsMsg - Process a Write System Parameters message.
//  Returns reply status 
#pragma CODE_SECTION (ProcWriteSystemParmsMsg, "SLOW_PROG_SECT")
static GPAK_SysParmsStat_t ProcWriteSystemParmsMsg (ADT_Int16 *pCmd, ADT_UInt16 *pReply) {

   ADT_Int16 MsgFlags;    // Group update flags

   ADT_Int16 AgcTargetPower;      // A-side AGC Parameter
   ADT_Int16 AgcLossLimit;
   ADT_Int16 AgcGainLimit;
   ADT_Int16 AgcLowSignal;

   ADT_Int16 AgcTargetPowerB;     // A-side AGC Parameters
   ADT_Int16 AgcLossLimitB;
   ADT_Int16 AgcGainLimitB;
   ADT_Int16 AgcLowSignalB;

   ADT_Int16 VadNoiseFloor;      // VAD Parameters
   ADT_Int16 VadHangTime;       
   ADT_Int16 VadWindowMs;     
   
   ADT_Int16 NumConfDominant;      // Conference Parameters
   ADT_Int16 MaxConfNoiseSuppress;

   // Echo cancellation parameters
   ADT_Int16 MinTailLength;
   G168Params_t PcmEC, PktEC;
   ADT_Int16    ECFlags;

   // Caller ID 
   ADT_Int16  numSeizureBytes;
   ADT_Int16  numMarkBytes;
   ADT_Int16  numPostambleBytes;
   ADT_Int16  fskType;
   ADT_Int16  fskLevel;
   ADT_Int16  msgType;          

    // Prepare the reply message.
    pReply[0] |= (MSG_WRITE_SYS_PARMS_REPLY << 8);

    // Default the reply status to indicate success. 
    MsgFlags = pCmd[1];

   // Validate the message's A-side AGC parameters.
   if (MsgFlags & 0x0001) {
      if (!sysConfig.numAGCChans)
         return Sp_AgcNotConfigured;

      AgcTargetPower = pCmd[2];
      AgcLossLimit   = pCmd[3];
      AgcGainLimit   = pCmd[4];
      AgcLowSignal   = pCmd[35];

      if (OutOfRange (AgcTargetPower, MIN_AGC_TARGET_PWR, MAX_AGC_TARGET_PWR))
         return Sp_BadAgcTargetPower;
         
      if (OutOfRange (AgcLossLimit, MIN_AGC_LOSS_LIMIT, MAX_AGC_LOSS_LIMIT))
         return Sp_BadAgcLossLimit;
         
      if (OutOfRange (AgcGainLimit, MIN_AGC_GAIN_LIMIT, MAX_AGC_GAIN_LIMIT))
         return Sp_BadAgcGainLimit;

      if (OutOfRange (AgcLowSignal, MIN_AGC_LOW_SIG_LIMIT, MAX_AGC_LOW_SIG_LIMIT))
         return Sp_BadAgcLowSignal;

   }

   // Validate the message's B-side AGC parameters.
   if (MsgFlags & 0x0020) {
      if (!sysConfig.numAGCChans)
         return Sp_AgcNotConfigured;

      AgcTargetPowerB = pCmd[36];
      AgcLossLimitB   = pCmd[37];
      AgcGainLimitB   = pCmd[38];
      AgcLowSignalB   = pCmd[39];

      if (OutOfRange (AgcTargetPowerB, MIN_AGC_TARGET_PWR, MAX_AGC_TARGET_PWR))
         return Sp_BadAgcTargetPower;
         
      if (OutOfRange (AgcLossLimitB, MIN_AGC_LOSS_LIMIT, MAX_AGC_LOSS_LIMIT))
         return Sp_BadAgcLossLimit;
         
      if (OutOfRange (AgcGainLimitB, MIN_AGC_GAIN_LIMIT, MAX_AGC_GAIN_LIMIT))
         return Sp_BadAgcGainLimit;

      if (OutOfRange (AgcLowSignalB, MIN_AGC_LOW_SIG_LIMIT, MAX_AGC_LOW_SIG_LIMIT))
         return Sp_BadAgcLowSignal;

   }

   // Validate the message's VAD parameters.
   if (MsgFlags & 0x0002) {
      if (!SysAlgs.vadCngEnable)
         return Sp_VadNotConfigured;

      VadNoiseFloor = pCmd[5];
      VadHangTime   = pCmd[6];
      VadWindowMs = pCmd[7];
         
      if (OutOfRange (VadNoiseFloor, MIN_VAD_NOISE_FLOOR, MAX_VAD_NOISE_FLOOR))
         return Sp_BadVadNoiseFloor;
         
      if (OutOfRange (VadHangTime,   MIN_VAD_HANG_TIME, MAX_VAD_HANG_TIME))
         return Sp_BadVadHangTime;
         
      if (OutOfRange (VadWindowMs, MIN_VAD_WIN_MS, MAX_VAD_WIN_MS))
         return Sp_BadVadWindowSize;
   }


   // Validate the message's PCM Echo Canceller parameters.
   ECFlags = pCmd[8];
   if (SysAlgs.g168VarAEnable)
       MinTailLength = 64;
   else
       MinTailLength = 256;

   if (MsgFlags & 0x0004) {
      if (sysConfig.numPcmEcans == 0)
         return Sp_PcmEcNotConfigured;

      memcpy (&PcmEC, &g168_PCM_EcParams, sizeof (g168_PCM_EcParams));
      PcmEC.NLPType           = (ECFlags & 0x000F);
      PcmEC.AdaptEnable       = Enabled;
      PcmEC.G165DetectEnable  = ((ECFlags >> 5) & 1);
      PcmEC.TapLength               = pCmd[9] * 8;
      PcmEC.DoubleTalkThres         = pCmd[10];

      PcmEC.NLPThres               = pCmd[11];
      PcmEC.NLPUpperLimitThresConv  = pCmd[12];
      PcmEC.NLPUpperLimitThresUnConv= pCmd[13];
      PcmEC.NLPMaxSupp              = pCmd[14];
      PcmEC.CNGNoiseThreshold       = 0-pCmd[15];
      PcmEC.AdaptLimit              = pCmd[16];
      PcmEC.CrossCorrLimit          = pCmd[17];
      PcmEC.NFIRSegments            = pCmd[18];
      PcmEC.FIRSegmentLength        = pCmd[19] * 8;
      PcmEC.FIRTapCheckPeriod       = pCmd[20] * 8;
  
      
      if (OutOfRange (PcmEC.NLPType, MIN_EC_NLP_TYPE, MAX_EC_NLP_TYPE))
         return Sp_BadPcmEcNlpType;

      if (OutOfRange (PcmEC.TapLength, MinTailLength, sysConfig.maxG168PcmTapLength) ||
          ((PcmEC.TapLength & (MinTailLength-1)) != 0))
         return Sp_BadPcmEcTapLength;

      if (OutOfRange (PcmEC.DoubleTalkThres, MIN_EC_DBL_TALK, MAX_EC_DBL_TALK))
         return Sp_BadPcmEcDblTalkThresh;

      if (OutOfRange (PcmEC.NLPThres, MIN_EC_NLP_THRESH, MAX_EC_NLP_THRESH))
         return Sp_BadPcmEcNlpThreshold;

      if (OutOfRange (PcmEC.NLPUpperLimitThresConv, MIN_EC_NLP_CONVERGE, MAX_EC_NLP_CONVERGE))
         return Sp_BadPcmEcNLPUpperConv;

      if (OutOfRange (PcmEC.NLPUpperLimitThresUnConv, MIN_EC_NLP_NONCONVERGE, MAX_EC_NLP_NONCONVERGE))
        return Sp_BadPcmEcNLPUpperUnConv;

      if (OutOfRange (PcmEC.NLPMaxSupp, MIN_EC_NLP_MAXSUPP, MAX_EC_NLP_MAXSUPP))
        return Sp_BadPcmEcMaxSupp;

      if (OutOfRange (PcmEC.CNGNoiseThreshold, MIN_EC_CNG_THRESH, MAX_EC_CNG_THRESH))
         return Sp_BadPcmEcCngThreshold;

      if (OutOfRange (PcmEC.AdaptLimit, MIN_EC_ADAPT_LIMIT, MAX_EC_ADAPT_LIMIT))
         return Sp_BadPcmEcAdaptLimit;

      if (OutOfRange (PcmEC.CrossCorrLimit, MIN_EC_CROSSCOR_LMT, MAX_EC_CROSSCOR_LMT))
         return Sp_BadPcmEcCrossCorrLim;

      if (OutOfRange (PcmEC.NFIRSegments, MIN_EC_NUM_FIR_SEGS, sysConfig.maxG168PcmNFIRSegments))
         return Sp_BadPcmEcNumFirSegs;

      if (OutOfRange (PcmEC.FIRSegmentLength, MIN_EC_FIR_SEG_LEN, sysConfig.maxG168PcmFIRSegmentLength) ||
         (PcmEC.TapLength < PcmEC.FIRSegmentLength * PcmEC.NFIRSegments) || ((PcmEC.FIRSegmentLength & 0xf) != 0))
         return Sp_BadPcmEcFirSegLen;

      if (OutOfRange (PcmEC.FIRTapCheckPeriod, MIN_EC_FIR_TAP_CHECK, MAX_EC_FIR_TAP_CHECK))
        return Sp_BadPcmEcFIRTapCheck;
   }


   // Validate the message's Packet Echo Canceller parameters.
   if (pCmd[1] & 0x0008) {
      if (sysConfig.numPktEcans == 0)
         return Sp_PktEcNotConfigured;
      memcpy (&PktEC, &g168_PKT_EcParams, sizeof (g168_PKT_EcParams));

      PktEC.NLPType           = ((ECFlags >> 8) & 0x000F);
      PktEC.AdaptEnable       = Enabled;
      PktEC.G165DetectEnable  = ((ECFlags >> 13) & 1);
      PktEC.TapLength               = pCmd[21] * 8;
      PktEC.DoubleTalkThres         = pCmd[22];

      PktEC.NLPThres                = pCmd[23];
      PktEC.NLPUpperLimitThresConv  = pCmd[24];
      PktEC.NLPUpperLimitThresUnConv= pCmd[25];
      PktEC.NLPMaxSupp              = pCmd[26];
      PktEC.CNGNoiseThreshold       = 0-pCmd[27];
      PktEC.AdaptLimit              = pCmd[28];
      PktEC.CrossCorrLimit          = pCmd[29];
      PktEC.NFIRSegments            = pCmd[30];
      PktEC.FIRSegmentLength        = pCmd[31] * 8;
      PktEC.FIRTapCheckPeriod       = pCmd[32] * 8;
      
      if (OutOfRange (PktEC.NLPType, MIN_EC_NLP_TYPE, MAX_EC_NLP_TYPE))
         return Sp_BadPktEcNlpType;
         
      if (OutOfRange (PktEC.TapLength, MinTailLength, sysConfig.maxG168PktTapLength) ||
          ((PktEC.TapLength & (MinTailLength-1)) != 0))
         return Sp_BadPktEcTapLength;
         
      if (OutOfRange (PktEC.DoubleTalkThres, MIN_EC_DBL_TALK, MAX_EC_DBL_TALK))
         return Sp_BadPktEcDblTalkThresh;
         
      if (OutOfRange (PktEC.NLPThres, MIN_EC_NLP_THRESH, MAX_EC_NLP_THRESH))
         return Sp_BadPktEcNlpThreshold;
         
      if (OutOfRange (PktEC.NLPUpperLimitThresConv, MIN_EC_NLP_CONVERGE, MAX_EC_NLP_CONVERGE))
         return Sp_BadPktEcNLPUpperConv;

      if (OutOfRange (PktEC.NLPUpperLimitThresUnConv, MIN_EC_NLP_NONCONVERGE, MAX_EC_NLP_NONCONVERGE))
        return Sp_BadPktEcNLPUpperUnConv;

      if (OutOfRange (PktEC.NLPMaxSupp, MIN_EC_NLP_MAXSUPP, MAX_EC_NLP_MAXSUPP))
        return Sp_BadPcmEcMaxSupp;

      if (OutOfRange (PktEC.CNGNoiseThreshold, MIN_EC_CNG_THRESH, MAX_EC_CNG_THRESH))
         return Sp_BadPktEcCngThreshold;
         
      if (OutOfRange (PktEC.AdaptLimit, MIN_EC_ADAPT_LIMIT, MAX_EC_ADAPT_LIMIT))
         return Sp_BadPktEcAdaptLimit;
         
      if (OutOfRange (PktEC.CrossCorrLimit, MIN_EC_CROSSCOR_LMT, MAX_EC_CROSSCOR_LMT))
         return Sp_BadPktEcCrossCorrLim;
         
      if (OutOfRange (PktEC.NFIRSegments, MIN_EC_NUM_FIR_SEGS, sysConfig.maxG168PktNFIRSegments))
         return Sp_BadPktEcNumFirSegs;
         
      if (OutOfRange (PktEC.FIRSegmentLength, MIN_EC_FIR_SEG_LEN, sysConfig.maxG168PktFIRSegmentLength) ||
         (PktEC.TapLength < PktEC.FIRSegmentLength * PktEC.NFIRSegments) || ((PktEC.FIRSegmentLength & 0xf) != 0))
         return Sp_BadPktEcFirSegLen;

      if (OutOfRange (PktEC.FIRTapCheckPeriod, MIN_EC_FIR_TAP_CHECK, MAX_EC_FIR_TAP_CHECK))
        return Sp_BadPktEcFIRTapCheck;

   }

   // Validate the message's Conference parameters.
   if (MsgFlags & 0x0010) {
      NumConfDominant = (ADT_Int16) pCmd[33];
      MaxConfNoiseSuppress = (ADT_Int16) pCmd[34];
      if (OutOfRange (NumConfDominant, 1, sysConfig.maxNumConfChans))
         return Sp_BadConfDominant;
         
      if (OutOfRange (MaxConfNoiseSuppress, 0, MAX_MAX_CONF_NOISE_SUPPRESS))
         return Sp_BadMaxConfNoiseSup;
   }

   // -----------------------------------------------
   // 4_2 < API Version.   Validate Caller ID parameters
   if (DEFAULT_GPAK_API_VERSION < ApiBlock.ApiVersionId) {

        if (MsgFlags & 0x0040) {
            if ((sysConfig.numRxCidChans == 0) && (sysConfig.numTxCidChans == 0))
                return Sp_CidNotConfigured;

            //type2CID           = pCmd[40] & 1; // NOT Changable by users in sysConfig
            numSeizureBytes    = pCmd[41];
            numMarkBytes       = pCmd[42];
            numPostambleBytes  = pCmd[43];
            fskType            = (GpakFskType) pCmd[44];
            fskLevel           = pCmd[45];
            msgType            = pCmd[46] & 0x00FF;

            if (OutOfRange (numSeizureBytes, 0, CID_MAX_SEIZURE_BYTES))
                return Sp_BadCidNumSeizureBytes;

            if ( (OutOfRange (numMarkBytes, 7, 13)) && (OutOfRange (numMarkBytes, 19, 25)) )
                return Sp_BadCidNumMarkBytes;

            if (OutOfRange (numPostambleBytes, 0, CID_MAX_POST_BYTES))
                return Sp_BadCidNumMarkBytes;

            if (OutOfRange (fskType, fskV23, fskBell202))
                return Sp_BadCidFskType;

            if (OutOfRange (fskLevel, CID_MIN_FSK_LEVEL, 0))
                return Sp_BadCidFskType;
        }

        if (MsgFlags & 0x0080)
            sysConfig.VadReport = Enabled;
        else
            sysConfig.VadReport = Disabled;

        if (MsgFlags & 0x0004) {
            PcmEC.G165DetectEnable  = ((ECFlags >> 5) & 1);
            PcmEC.MixedFourWireMode = ((ECFlags >> 6) & 1);
            if ((ECFlags >> 7) & 1)
                PcmEC.ReconvergenceCheckEnable = 2;
            PcmEC.MaxDoubleTalkThres = pCmd[47];
            if (OutOfRange (PcmEC.MaxDoubleTalkThres, MIN_EC_DBL_TALK, MAX_EC_MAX_DBL_TALK))
                return Sp_BadPcmEcMaxDblTalkThresh;
        }

        if (pCmd[1] & 0x0008) {
            PktEC.G165DetectEnable  = ((ECFlags >> 13) & 1);
            PktEC.MixedFourWireMode = ((ECFlags >> 14) & 1);
            if ((ECFlags >> 15) & 1)
                PcmEC.ReconvergenceCheckEnable = 2;
            PktEC.MaxDoubleTalkThres = pCmd[48];
            if (OutOfRange (PktEC.MaxDoubleTalkThres, MIN_EC_DBL_TALK, MAX_EC_MAX_DBL_TALK))
                return Sp_BadPktEcMaxDblTalkThresh;
        }
    }


   // Store the values from the message based on the message's update flags.
   // Store the AGC parameters.
   if (MsgFlags & 0x0001) {
      sysConfig.agcTargetPower = AgcTargetPower;
      sysConfig.agcMinGain = AgcLossLimit;
      sysConfig.agcMaxGain = AgcGainLimit;
      sysConfig.agcLowSigThreshDBM = AgcLowSignal;
   }

   // Store the values from the message based on the message's update flags.
   // Store the AGC parameters.
   if (MsgFlags & 0x0020) {
      sysConfig.agcTargetPowerB = AgcTargetPowerB;
      sysConfig.agcMinGainB = AgcLossLimitB;
      sysConfig.agcMaxGainB = AgcGainLimitB;
      sysConfig.agcLowSigThreshDBMB = AgcLowSignalB;
   }

   // Store the VAD parameters.
   if (MsgFlags & 0x0002) {
      sysConfig.vadNoiseFloorZThresh = VadNoiseFloor;
      sysConfig.vadHangOverTime = VadHangTime;
      sysConfig.vadWindowMs = VadWindowMs;
   }

   // Store the PCM Echo Canceller parameters.
   if (MsgFlags & 0x0004) {
      memcpy (&g168_PCM_EcParams, &PcmEC, sizeof (g168_PCM_EcParams));
   }

   // Store the Packet Echo Canceller parameters.
   if (MsgFlags & 0x0008) {
      memcpy (&g168_PKT_EcParams, &PktEC, sizeof (g168_PKT_EcParams));
   }

   // Store the Conference parameters.
   if (MsgFlags & 0x0010)   {
      sysConfig.numConfDominant = NumConfDominant;
      sysConfig.maxConfNoiseSuppress = MaxConfNoiseSuppress;
   }

   // -----------------------------------------------
   // 4_2 < API Version.  Add caller ID values
   if (DEFAULT_GPAK_API_VERSION < ApiBlock.ApiVersionId) {

      // Update Caller ID parameters
      if (MsgFlags & 0x0040) {
         //sysConfig.type2CID              = type2CID; // NOT Changable by users in sysConfig
         sysConfig.numSeizureBytes       = numSeizureBytes;
         sysConfig.numMarkBytes          = numMarkBytes;
         sysConfig.numPostambleBytes     = numPostambleBytes;
         sysConfig.fskType               = (GpakFskType) fskType;
         sysConfig.fskLevel              = fskLevel;
         sysConfig.msgType               = msgType;
      }
   }

   // Return with the reply's return status.
   return Sp_Success;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ProcWriteDtmfCfgParmsMsg - Process a Write Dtmf detector Parameters message.
//  Returns reply status 
static GPAK_DtmfDetConfigStat_t ProcWriteDtmfCfgParmsMsg (ADT_Int16 *pCmd, ADT_UInt16 *pReply) {

   ADT_Int16   MsgFlags;
   DTMF_Config_Param_t parms;

   // Prepare the reply message.
   pReply[0] |= (MSG_WRITE_DTMFCFG_PARMS_REPLY << 8);

   // Default the reply status to indicate success. 
   MsgFlags = pCmd[1];

   if (NumActiveChannels)    return Dc_ChanActive;

   if (!SysAlgs.dtmfEnable || (MsgFlags == 0))  return Dc_DtmfDetNotConfigured;

   memset (&parms, 0, sizeof(DTMF_Config_Param_t));
   if (ApiBlock.CmdMsgLength < 7) {
      MsgFlags &= 0x000f;
      MsgFlags |= 0x1000;
   }

   if (MsgFlags & 0x0002) {  // Signal level
      parms.UpdateMinSigLevel = 1;
      parms.Minimum_Signal_Level = pCmd[2];
      if (parms.Minimum_Signal_Level > 0) return Dc_InvalidSigLev;
   }
   if (MsgFlags & 0x0004) {  // Frequency deviation
      parms.UpdateMaxFreqDev = 1;
      parms.Max_Freq_Deviation = pCmd[3];
      if (parms.Max_Freq_Deviation < 0)  return Dc_InvalidFreqDev;
   }
   if (MsgFlags & 0x0008) {  // Twist
      parms.UpdateMaxTwist = 1;
      parms.MaxFwdTwist = pCmd[4];
      parms.MaxRevTwist = pCmd[5];
      if (OutOfRange (parms.MaxFwdTwist, 0, 12))  return Dc_invalidTwist;
      if (OutOfRange (parms.MaxRevTwist, 0, 12))  return Dc_invalidTwist;
   }
   if (MsgFlags & 0x0010) {  // SNR
      parms.UpdateSNRFlag = 1;
      parms.SNRFlag = pCmd[6];
      if (OutOfRange (parms.SNRFlag, 0, 5)) return Dc_invalidToneQuality;
   }
   if (MsgFlags & 0x0020) {  // Max Waivers
      parms.UpdateMaxWaivedCriteria = 1;
      parms.MaxWaivedCriteria = pCmd[7];
      if (OutOfRange (parms.MaxWaivedCriteria, 0, 5)) return Dc_invalidMaxWaivers;
   }
   if (MsgFlags & 0x0040) {  // Waiver settings
      parms.UpdateTrailingEdgeRelaxation = 1;
      parms.ValidityMask = pCmd[8];
   }

   if (MsgFlags & 0x1000) DTMF_ADT_config (&parms, 0);   // Waivers
   else                   DTMF_ADT_config (&parms, 1);   // Nominal

   // Return with the reply's return status.
   return Dc_Success;
}
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ProcConfigureChannelMsg - Process a Configure Channel message.
//  Returns reply status 
#pragma CODE_SECTION (ProcConfigureChannelMsg, "FAST_PROG_SECT")
static GPAK_ChannelConfigStat_t ProcConfigureChannelMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {
   ADT_UInt16 ChannelIdA, CoreID;        
   GpakChanType ChannelType;     
   GPAK_ChannelConfigStat_t rtn;
   chanInfo_t *chan;   

   // Get the Core id, Channel Id and Channel Type from the command message.
   CoreID = pCmd[0] & 0xFF;
   ChannelIdA = (pCmd[1] >> 8) & 0xFF;
   ChannelType = (GpakChanType) (pCmd[1] & 0xFF);

   pReply[0] |= (MSG_CONFIG_CHAN_REPLY << 8);
   pReply[1] = (ChannelIdA << 8);

   // Verify that the Core ID is valid
   if (DSPTotalCores <= CoreID)
      return Cc_InvalidCore;

   // jdc c6678 if ((1 < DSPTotalCores) && (CoreID <= 1))
   if ( !((minChanCore <= CoreID) && (CoreID < DSPTotalCores)) )
      return Cc_InvalidCore;

      // Verify the first Channel Id is valid and the channel is not active.
   if (sysConfig.maxNumChannels <= ChannelIdA)
      return Cc_InvalidChannelA;

   chan = chanTable[ChannelIdA];
   if (chan->channelType != inactive)
      return Cc_ChannelActiveA;

   chan->CoreID = CoreID;
   chan->channelType = ChannelType;


   rtn = processChannelMsg (chan, pCmd);
   if (rtn != Cc_Success) 
      chan->channelType = inactive;

   // Return with the reply's return status.
   return rtn;
      

}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ ProcTearDownChannelMsg - Process a Tear Down Channel message.
//
// FUNCTION
//   Performs processing for a host's Tear Down Channel message
//
// RETURNS
//  Length (dwords) of reply message.
//}
#pragma CODE_SECTION (ProcTearDownChannelMsg, "FAST_PROG_SECT")
static ADT_UInt16 ProcTearDownChannelMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {

   GPAK_TearDownChanStat_t RetStatus, RetStatusB;
   ADT_UInt16 ChannelId;
   chanInfo_t *pChan;

   RetStatusB = Td_Success;

   // Get the Channel Id from the command message.
   ChannelId = (pCmd[1] >> 8) & 0xFF;

   teardownLog(ChannelId, 0);

   // Prepare the reply message.
   pReply[0] |= (MSG_TEAR_DOWN_REPLY << 8);
   pReply[1] = (ChannelId << 8);

   // Verify the Channel Id is valid and the channel is active.
   if (ChannelId >= sysConfig.maxNumChannels)
      RetStatus = Td_InvalidChannel;

   else if (chanTable[ChannelId]->channelType == inactive)
      RetStatus = Td_ChannelNotActive;

   else if (chanTable[ChannelId]->algCtrl.ControlCode & ALG_CTRL_DEQUEUE_CMD)
      RetStatus = Td_ChannelNotActive;

   else   {
      // Deactivate channels by unlinking them from the appropriate
      // Framing tasks (or conferences) and PCM slot(s).
      pChan = chanTable[ChannelId];
      if (pChan->channelType == packetToPacket) {
         // Deactivate Pkt-Pkt partner channels.
         chanInfo_t *pChanB;
         pChanB = chanTable [pChan->PairedChannelId];

         removeFromIPSession (pChanB->ChannelId);
         RetStatusB = DeactivateChannel (pChanB); // Remove channels from DMA transfers and framing queues
         tearDownChannel   (pChanB);
      }

      if (pChan->channelType == loopbackCoder) {
         DeactivateLpbkCoder (pChan);
         RetStatus = Td_Success;
      } else {
         removeFromIPSession (pChan->ChannelId);
         teardownLog(ChannelId, 1);
         RetStatus = DeactivateChannel (pChan); // Remove channels from DMA transfers and framing queues
         if (RetStatusB != Td_Success)
            RetStatus = RetStatusB;
         tearDownChannel   (pChan);
      }

      // Set the reply status to indicate success.
   }
   teardownLog(ChannelId, 9);

   // Store the return status code in the reply message.
   pReply[1] |= (ADT_UInt16) (RetStatus & 0xFF);

   // Return with the length of the reply message.
   return ReplyBytes (2);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ProcChannelStatusMsg - Process a Channel Status message.
#pragma CODE_SECTION (ProcChannelStatusMsg, "SLOW_PROG_SECT")
static ADT_UInt16 ProcChannelStatusMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {
   GPAK_ChannelStatusStat_t rtn;   // reply's return status code
   ADT_UInt16 ChannelId;
   ADT_UInt16 RplyLen;
   chanInfo_t *pChan;

   // Get the Channel Id from the command message.
   ChannelId = (pCmd[1] >> 8) & 0xFF;

   // Prepare the reply message.
   pReply[0] = (MSG_CHAN_STATUS_REPLY << 8) | (chanTable[ChannelId]->CoreID & 0xff);
   pReply[1] = (ChannelId << 8);

   // Set the default reply message length.
   RplyLen = 3;

   // Verify the Channel Id is valid.
   if (sysConfig.maxNumChannels <= ChannelId)   {
      pReply[2] = inactive << 8;
      rtn = Cs_InvalidChannel;
   } else {
      // Store the channel status information in the reply message.
      pChan = chanTable[ChannelId];
      pReply[2] = (ADT_UInt16) (pChan->channelType << 8);
      rtn = Cs_Success;
      pReply[3] = pChan->invalidHdrCount;
      pReply[4] = pChan->overflowCount;
      pReply[5] = pChan->underflowCount;
      pReply[6] = pChan->FrameCount;

      // Store the channel type dependent information in the reply message.
      switch (pChan->channelType) {
      case inactive:    break;

      case packetToPacket:
         // Verify the partner channel is also configured.
         if ((pChan->PairedChannelId < sysConfig.maxNumChannels) &&
            (chanTable[pChan->PairedChannelId]->channelType == packetToPacket))
            RplyLen = ProcPkt2PktStatMsg(pReply, pChan);
         else
            rtn = Cs_InvalidChannel;
         break;

      case pcmToPcm:            RplyLen = ProcPcm2PcmStatMsg  (pReply, pChan); break;
      case pcmToPacket:         RplyLen = ProcPcm2PktStatMsg  (pReply, pChan); break;
      case conferencePcm:       RplyLen = ProcConfPcmStatMsg  (pReply, pChan); break;
      case conferenceMultiRate:
      case conferencePacket:    RplyLen = ProcConfPktStatMsg  (pReply, pChan); break;
      case conferenceComposite: RplyLen = ProcConfCompStatMsg (pReply, pChan); break;
      case circuitData:         RplyLen = ProcCircDataStatMsg (pReply, pChan); break;
      case loopbackCoder:       RplyLen = ProcCfgLpbkStatMsg  (pReply, pChan); break;
      case customChannel:       RplyLen = customProcChanStatMsg (pReply, pChan); break;

      default:
         pReply[2] = inactive << 8;
         rtn = Cs_InvalidChannel;
         break;
      }
   }

   // Store the return status code in the reply message.
   pReply[1] |= (ADT_UInt16) (rtn & 0xFF);

   // Return with the length of the reply message.
   return ReplyBytes (RplyLen);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ ProcToneGenMsg - Process a Tone Generation Request message.
//
// FUNCTION
//   This function performs all processing for a host's Tone Generation Request
//   message and builds the reply message.
//
// RETURNS
//  none
//}
#pragma CODE_SECTION (ProcToneGenMsg, "SLOW_PROG_SECT")
static GPAK_ToneGenStat_t ProcToneGenMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {

   ADT_UInt16 ChannelId;              // tone channel identifier
   chanInfo_t  *pChan;               // pointer to Channel structure
   GpakDeviceSide_t deviceSide;
   int Factor;

   GpakToneGenCmd_t  ToneCmd;         // tone command
   GpakToneGenType_t ToneType;
   ADT_Int16 Frequency[4], OnTime[4], OffTime[4], Level[4];

   TGInfo_t     *tgInfo;
   TGParams_1_t *tgParams;
   
   int numFreqs, i, numOns, numOffs;

   // Get the Channel Id from the command message.
   ChannelId = pCmd[1];

   // Prepare the reply message.
   pReply[0] |= (MSG_TONEGEN_REPLY << 8);
   pReply[1] = ChannelId;

   if (sysConfig.maxNumChannels <= ChannelId)
      return Tg_InvalidChannel;

   if (chanTable[ChannelId]->channelType == inactive)
      return Tg_InactiveChannel;

   // Get the tone command, and tone parameters from
   // the request message.
   ToneCmd = (GpakToneGenCmd_t) (pCmd[2] & 1);
   deviceSide = (GpakDeviceSide_t)  ((pCmd[2] >> 1) & 0x0001);
   ToneType = (GpakToneGenType_t) ((pCmd[2] >> 2) & 0x0003);

   // Verify the channel supports tone generation.
   pChan = chanTable[ChannelId];
   if ((deviceSide == ADevice) && (pChan->pktToPcm.TGInfo.index == VOID_INDEX))
      return Tg_toneGenNotEnabledA;
   if ((deviceSide == BDevice) && (pChan->pcmToPkt.TGInfo.index == VOID_INDEX))
      return Tg_toneGenNotEnabledB;

   // Validate the tone command
   if ((ToneType != TgCadence) && (ToneType != TgContinuous) && (ToneType != TgBurst))
      return Tg_InvalidToneGenType;

   if (ToneCmd == ToneGenStart) {
      Frequency[0] = (ADT_Int16) pCmd[3];
      Frequency[1] = (ADT_Int16) pCmd[4];
      Frequency[2] = (ADT_Int16) pCmd[5];
      Frequency[3] = (ADT_Int16) pCmd[6];
      Level[0]   = (ADT_Int16) pCmd[7];
      OnTime[0]  = (ADT_Int16) pCmd[8];
      OffTime[0] = (ADT_Int16) pCmd[9];

      // ----------------------------------------------------
      // 4_2 < API Version.   Per frequency level and timing
      if (DEFAULT_GPAK_API_VERSION < ApiBlock.ApiVersionId) {
           // 4_2 -----------------------------------------------
         Level[1]  = (ADT_Int16) pCmd[10];
         Level[2]  = (ADT_Int16) pCmd[11];
         Level[3]  = (ADT_Int16) pCmd[12];

         OnTime[1] = (ADT_Int16) pCmd[13];
         OnTime[2] = (ADT_Int16) pCmd[14];
         OnTime[3] = (ADT_Int16) pCmd[15];

         OffTime[1] = (ADT_Int16) pCmd[16];
         OffTime[2] = (ADT_Int16) pCmd[17];
         OffTime[3] = (ADT_Int16) pCmd[18];
      } else {
         for (i=1; i<4; i++) {
            if (Frequency[i] != 0)  Level[i]   = Level[0];
            else                    Level[i]   = 0;
            OnTime[i]  = 0;
            OffTime[i] = 0;
         }
      }

      // Validate the tone parameters.
      numFreqs = 0;
      numOns = 0;
      numOffs = 0;
      for (i=0; i<4; i++) {
         if (Frequency[i] != 0) numFreqs++;
         if (OnTime[i] != 0)    numOns++;
         if (OffTime[i] != 0)   numOffs++;
      }

      // Verify non-zero frequencies are in contiquous array elements and at start of frequency array
      for (i=0; i<numFreqs; i++) {
         if (Frequency[i] == 0)
            return Tg_InvalidFrequency;
         if ((Level[i] < (MIN_TONEGEN_LEVEL*10)) || ((MAX_TONEGEN_LEVEL*10) < Level[i]))
            return Tg_InvalidLevel;
      }

      // Verify non-zero times are in contiquous array elements and at start of frequency array
      if (ToneType != TgContinuous) {
         if (numOns == 0)           return Tg_InvalidOnTime;
         if (numOffs < numOns - 1)  return Tg_InvalidOnTime;

         for (i=0; i<numOns; i++) {
            if (OnTime[i] == 0) return Tg_InvalidOnTime;
         }
         for (i=0; i<numOffs; i++) {
            if (OffTime[i] == 0) return Tg_InvalidOnTime;
         }
      }
   }

   if (deviceSide == ADevice)
      tgInfo = &(pChan->pktToPcm.TGInfo);
   else
      tgInfo = &(pChan->pcmToPkt.TGInfo);

   // Protect framing task from operating on parameters while change is being made
   tgInfo->update = Disabled;
   if (ToneCmd == ToneGenStart) {
      tgParams = tgInfo->toneGenParmPtr;
      CLEAR_INST_CACHE (tgParams, sizeof (TGParams_1_t));
      tgParams->ToneType = ToneType; 
      tgParams->Level[0]    = Level[0];
      tgParams->Level[1]    = Level[1];
      tgParams->Level[2]    = Level[2];
      tgParams->Level[3]    = Level[3];
      tgParams->NOnOffs  = numOns;
      tgParams->NFreqs   = numFreqs;  

      // Tone Gen algorithm is fixed for 8Khz tones
      // for 16Khz rates, half the freq, double the duration
      if (pChan->ProcessRate == 8000) {
         Factor = 1;
      } else if (pChan->ProcessRate == 16000){
         Factor = 2;
      }

      tgParams->OnTime[0]   = OnTime[0] * Factor;
      tgParams->OnTime[1]   = OnTime[1] * Factor;
      tgParams->OnTime[2]   = OnTime[2] * Factor;
      tgParams->OnTime[3]   = OnTime[3] * Factor;
      tgParams->OffTime[0]  = OffTime[0] * Factor;
      tgParams->OffTime[1]  = OffTime[1] * Factor;
      tgParams->OffTime[2]  = OffTime[2] * Factor;
      tgParams->OffTime[3]  = OffTime[3] * Factor;
      tgParams->Freq[0]  = Frequency[0] / Factor;
      tgParams->Freq[1]  = Frequency[1] / Factor;
      tgParams->Freq[2]  = Frequency[2] / Factor;
      tgParams->Freq[3]  = Frequency[3] / Factor;
      FLUSH_INST_CACHE (tgParams, sizeof (TGParams_1_t));
   }
   tgInfo->cmd = ToneCmd;
   
   FLUSH_wait ();
   tgInfo->update = Enabled;

   return Tg_Success;
}
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ ProcDtmfDialMsg - Process a DtmfDial Request message.
//
// FUNCTION
//   This function performs all processing for a host's Dtmf Dial Request
//   message and builds the reply message.
//
// RETURNS
//  none
//}


static GPAK_DtmfDialStat_t ProcDtmfDialMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {

   ADT_UInt16 ChannelId;              // tone channel identifier
   chanInfo_t  *pChan;               // pointer to Channel structure
   GpakDeviceSide_t DeviceSide;

   ADT_Int16 OnTime, InterdigitTime, Level[2];
   ADT_Int16 DialDigits[16];
   ADT_Int16 NumDigits, i;
   DtmfDialInfo_t  *dial;   
   TGInfo_t     *tgInfo;


   // Get the Channel Id from the command message.
   ChannelId = pCmd[1];

   // Prepare the reply message.
   pReply[0] |= (MSG_DTMFDIAL_REPLY << 8);
   pReply[1] = ChannelId;

   NumDigits      = (ADT_Int16) ((pCmd[2] >> 8) & 0x00FF);
   DeviceSide     = (GpakDeviceSide_t)  (pCmd[2] & 0x0001);
   OnTime         = (ADT_Int16) pCmd[3];
   InterdigitTime = (ADT_Int16) pCmd[4];
   Level[0]       = (ADT_Int16) pCmd[5];
   Level[1]       = (ADT_Int16) pCmd[6];

   for (i=0; i<8; i++) {
      DialDigits[2*i]   =   pCmd[7+i] & 0xFF;
      DialDigits[2*i+1] =  (pCmd[7+i] >> 8) & 0xFF;
   }


   if (sysConfig.maxNumChannels <= ChannelId)
      return Dd_InvalidChannel;

   if (chanTable[ChannelId]->channelType == inactive)
      return Dd_InactiveChannel;

   // Verify the channel supports tone generation.
   pChan = chanTable[ChannelId];
   if ((DeviceSide == ADevice) && (pChan->pktToPcm.TGInfo.index == VOID_INDEX))
      return Dd_ToneGenNotEnabledA;
   if ((DeviceSide == BDevice) && (pChan->pcmToPkt.TGInfo.index == VOID_INDEX))
      return Dd_ToneGenNotEnabledB;

   // Verify the channel supports dtmf dialing
   pChan = chanTable[ChannelId];
   if ((DeviceSide == ADevice) && (pChan->pktToPcm.TGInfo.dtmfDialParmPtr == NULL))
      return Dd_DtmfDialNotEnabledA;
   if ((DeviceSide == BDevice) && (pChan->pcmToPkt.TGInfo.dtmfDialParmPtr == NULL))
      return Dd_DtmfDialNotEnabledB;

   if (OnTime == 0)
      return Dd_InvalidOnTime;
   if (InterdigitTime == 0)
      return Dd_InvalidInterdigitTime;

   if ((Level[0] < MIN_TONEGEN_LEVEL) || (Level[0] > MAX_TONEGEN_LEVEL))
      return Dd_InvalidLevel;
   if ((Level[1] < MIN_TONEGEN_LEVEL) || (Level[1] > MAX_TONEGEN_LEVEL))
      return Dd_InvalidLevel;

   if ((NumDigits == 0) || (NumDigits > 16))
      return  Dd_InvalidNumDigits;

   for (i=0; i<NumDigits; i++) {
      if (DialDigits[i] > 15)
          return Dd_InvalidDtmfDigit;
   }

   if (DeviceSide == ADevice)
      tgInfo = &(pChan->pktToPcm.TGInfo);
   else
      tgInfo = &(pChan->pcmToPkt.TGInfo);

   // Protect framing task from operating on parameters while change is being made
   tgInfo->update = Disabled;
   tgInfo->dtmfDial = Disabled;
   dial = tgInfo->dtmfDialParmPtr;
   CLEAR_INST_CACHE (dial, sizeof (DtmfDialInfo_t));
   memset(dial->Digits, 0, sizeof(ADT_UInt16)*16);
   for (i=0; i<NumDigits; i++)
       dial->Digits[i] = DialDigits[i];
   dial->NumDigits = NumDigits;
   dial->CurrentDigit = 0;
   dial->Rate = pChan->ProcessRate;
   dial->NSampsOn  = OnTime;
   dial->NSampsOff = InterdigitTime;
   dial->NSampsGen = 0;
   if (pChan->ProcessRate == 8000) {
      dial->NSampsOn  = OnTime * 8;
      dial->NSampsOff = InterdigitTime * 8;
   } else if (pChan->ProcessRate == 16000) {
      dial->NSampsOn  = OnTime * 16;
      dial->NSampsOff = InterdigitTime * 16;
   }

   dial->Level[0] = Level[0];
   dial->Level[1] = Level[1];
   dial->State = DialToneOn;
   FLUSH_INST_CACHE (dial, sizeof (DtmfDialInfo_t));
   
   FLUSH_wait ();
   CHN_lock (pChan);
   tgInfo->update = Enabled;
   tgInfo->dtmfDial = Enabled;
   CHN_unlock (pChan);

   return Dd_Success;

}
//{ ProcTonePktGenMsg - Process a Tone Generation Request message.
//
// FUNCTION
//   This function performs all processing for a host's Tone Generation Request
//   message and builds the reply message.
//
// RETURNS
//  none
//}
extern const ADT_UInt16 packetToRelayDTMFTbl[];
extern const ADT_UInt16 packetToRelayMFR1Tbl[];
#pragma CODE_SECTION (ProcTonePktGenMsg, "SLOW_PROG_SECT")
static GPAK_ToneGenStat_t ProcTonePktGenMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {

   ADT_UInt16 ChannelId;              // tone channel identifier
   chanInfo_t *pChan;                // pointer to Channel structure
   chanInfo_t *pChanB;
   FwdBuf_t   *fwdBuff;

   GpakDeviceSide_t deviceSide;

   GpakToneGenCmd_t  ToneCmd;         // tone command
   GpakToneCodes_t   ToneCode;
   ADT_Int16 OnTime, Level;
   ADT_Int16 RelayPkt;
   ADT_UInt16 Pyld[2];
   ADT_UInt16 Freq[2];

   // Get the Channel Id from the command message.
   ChannelId = pCmd[1];
   // Verify the channel supports tone generation.
   pChan = chanTable[ChannelId];
   pChanB = chanTable [pChan->PairedChannelId];
   RelayPkt = (pChan->pcmToPkt.toneTypes & Tone_Relay);

   // Prepare the reply message.
   pReply[0] |= (MSG_TONEPKTGEN_REPLY << 8);
   pReply[1] = ChannelId;

   if (sysConfig.maxNumChannels <= ChannelId)
      return Tg_InvalidChannel;


   // Get the tone command, and tone parameters from
   // the request message.
   ToneCmd    = (GpakToneGenCmd_t)  (pCmd[2] & 1);
   deviceSide = (GpakDeviceSide_t) ((pCmd[2] >> 1) & 0x0001);
   ToneCode   = (GpakToneCodes_t)  ((pCmd[2] >> 8) & 0x00FF);
   Freq[0]    = pCmd[3];
   Freq[1]    = pCmd[4];
   Level   = (ADT_Int16) pCmd[5];
   OnTime  = (ADT_Int16) pCmd[6];

   if (deviceSide == ADevice) // only generate tone pkt on Bside(pkt side)
      return Tg_toneGenNotEnabledA;

   fwdBuff = (FwdBuf_t *) pChan->pcmToPkt.tonePktGet;
   if ((Enabled == pChan->pktToPcm.ForwardTonePkts) && (fwdBuff->fwdType <= FWD_PACKET_TONE_MSG))
      return Tg_ToneGenPktBusy;

   if (ToneCmd == ToneGenStart) {
      // Validate the tone parameters.
      if (Level < 0)
         return Tg_InvalidLevel;

   }

   // If tone relay - Format event packet for relay
   // otherwise     - Initialize tone generator to produce tone.
   if ((ToneCmd == ToneGenStart) && RelayPkt) { // insert a tone pkt
      Pyld[0] = (Level & 0x003F) << 8;      // Signal Power: bits 0..5 of 2nd octet
      Pyld[0] |= 0x8000;                    // Set end of tone flag
      Pyld[0] |= ToneCode;
      Pyld[1] = ntohs (OnTime);

      CHN_lock (pChanB);
      fwdBuff->nullPktCnt = 0;
      fwdBuff->pktHdr.ChanId     = pChan->ChannelId;
      fwdBuff->pktHdr.PayldClass = PayClassStartTone;
      fwdBuff->pktHdr.PayldType  = EventPkt;
      fwdBuff->pktHdr.PayldBytes = RTP_EVT_PAYLOAD_LEN;
      i8cpy  (&fwdBuff->pktData, Pyld, RTP_EVT_PAYLOAD_LEN);
      fwdBuff->fwdType = FWD_PACKET_TONE_MSG;

#ifdef _DEBUG
      fwdBuff->pktData[(RTP_EVT_PAYLOAD_LEN + 1) / 2] = 0;
#endif
      pChan->pcmToPkt.TDInstances.ToneTimeStamp = pChan->pcmToPkt.FrameTimeStamp;
      CHN_unlock (pChanB);

   } else if ((ToneCmd == ToneGenStart) && (!RelayPkt)) {
      TRGenerateInstance_t*  toneRegenInst;
      TRGenerateEvent_t      toneEvt;
      // Get current status of tone generator
      toneRegenInst = pChanB->pktToPcm.TGInfo.toneRlyGenPtr;
      if (toneRegenInst == NULL) {
         return Tg_InactiveChannel;
      } 
      CHN_lock (pChanB);
      InitToneRelayGen (&pChanB->pktToPcm.TGInfo);

      toneEvt.Amplitude   =  Level;
      toneEvt.TimeElapsed =  OnTime;
        // Convert RFC2833 tone id to ADT tone index
      if ((Freq[0]==0) && (Freq[1]==0)) {
         toneEvt.UseIndex = 1;
         if(ToneCode <= 15)   toneEvt.ToneIndex = packetToRelayDTMFTbl[ToneCode];
         else if ((128 <= ToneCode) && (ToneCode <= 141)) toneEvt.ToneIndex = packetToRelayMFR1Tbl[ToneCode-128];
         else  {
            // Set frequencies for tone types not supported by ADT's tone relay generator
            toneEvt.UseIndex = 0;
            toneEvt.ToneFrequency[1] = 0;
            if      (ToneCode == 32) toneEvt.ToneFrequency[0] = 2100;  // fax CED tone
            else if (ToneCode == 36) toneEvt.ToneFrequency[0] = 1100;  // fax CNG tone
            else if (ToneCode == 39) toneEvt.ToneFrequency[0] = 1850;  // V.21 Channel 2 "0" bit
            else if (ToneCode == 40) toneEvt.ToneFrequency[0] = 1650;  // V.21 Channel 2 "1" bit
            else toneEvt.TimeElapsed =  0; // no tone generated
         }
      } else {
         toneEvt.UseIndex = 0;
         toneEvt.ToneFrequency[0] = Freq[0];
         toneEvt.ToneFrequency[1] = Freq[1];
      }
      TR_ADT_sendNewEventToGenerator (toneRegenInst, &toneEvt);
      CHN_unlock (pChanB);
   } else if ((ToneCmd == ToneGenStop) && (!RelayPkt)) {
      TRGenerateInstance_t*  toneRegenInst;
      TRGenerateEvent_t      toneEvt;
      // Get current status of tone generator
      toneRegenInst = pChanB->pktToPcm.TGInfo.toneRlyGenPtr;
      if (toneRegenInst == NULL) return Tg_InactiveChannel;

      CHN_lock (pChanB);
      InitToneRelayGen (&pChanB->pktToPcm.TGInfo);
      toneEvt.UseIndex = 0;
      toneEvt.ToneFrequency[1] = 0;
      toneEvt.ToneFrequency[0] = 0;
      toneEvt.Amplitude   =  0;
      toneEvt.TimeElapsed =  0;
      TR_ADT_sendNewEventToGenerator (toneRegenInst, &toneEvt);
      CHN_unlock (pChanB);
   }
   

   return Tg_Success;
}
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ ProcInsertTonePktGenMsg - Process a Tone Generation Request message.
//
// FUNCTION
//   This function performs all processing for a host's Tone Generation Request
//   message and builds the reply message.
//
// RETURNS
//  none
//}
#pragma CODE_SECTION (ProcInsertTonePktGenMsg, "SLOW_PROG_SECT")
static GPAK_InsertEventStat_t ProcInsertTonePktGenMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {

   ADT_UInt16 ChannelId;              // tone channel identifier
   chanInfo_t  *pChan, *pChanB;       // pointer to Channel structure

   ADT_UInt16 *pyld;
   ADT_UInt16 PayldBytes;

   FwdBuf_t  *fwdBuff;

   // Get the Channel Id from the command message.
   ChannelId  = pCmd[1];
   PayldBytes = pCmd[2];

   // Prepare the reply message.
   pReply[0] |= (MSG_INRTPTONEPKTGEN_REPLY << 8);
   pReply[1] = ChannelId;

   // Validate the tone command
   if (sysConfig.maxNumChannels <= ChannelId)
      return Ti_InvalidChannel;

   pChan = chanTable[ChannelId];
   if ((pChan->pcmToPkt.SamplesPerFrame << 1) < (PayldBytes + 16))
      return Ti_PyldTooLarge;

   if (pChan->channelType == packetToPacket)
      pChanB = chanTable [pChan->PairedChannelId];
   else
      pChanB = pChan;

   if (Enabled != pChanB->pktToPcm.ForwardTonePkts)
      return Ti_ForwardingNotEnabled;

   fwdBuff = (FwdBuf_t *) pChanB->pktToPcm.tonePktPut;
   if (fwdBuff == NULL)
      return Ti_NotConfigured;

   if (FWD_PACKET_TONE_MSG <= fwdBuff->fwdType)
      return Ti_ToneGenPktBusy;


   // If tone relay - Format packet for relay
   // otherwise     - Initialize tone generator to produce tone.
   if (pChan->pcmToPkt.toneTypes & Tone_Relay) {

      // Build packet header
      CHN_lock (pChanB);
      fwdBuff->nullPktCnt = 0;
      fwdBuff->pktHdr.ChanId     = pChan->ChannelId;
      fwdBuff->pktHdr.PayldBytes = PayldBytes;
      fwdBuff->pktHdr.PayldClass = PayClassStartTone;
      fwdBuff->pktHdr.PayldType  = EventPkt;
      pyld = (ADT_UInt16 *)&pCmd[3];
      pyld[0] |= 0x8000;                   // Set end of tone flag since duration is for full tone.

      i8cpy  (fwdBuff->pktData, pyld, PayldBytes);

      fwdBuff->fwdType = FWD_PACKET_TONE_MSG;
#ifdef _DEBUG
      fwdBuff->pktData[(PayldBytes + 1) / 2] = 0;
#endif
      pChan->pcmToPkt.TDInstances.ToneTimeStamp = pChan->pcmToPkt.FrameTimeStamp;
      CHN_unlock (pChanB);

   } else {
      // Initialize tone generation
      TRGenerateInstance_t* toneRegenInst;
      TRGenerateEvent_t     toneEvt;
      // Get current status of tone generator
      toneRegenInst = pChanB->pktToPcm.TGInfo.toneRlyGenPtr;
      if (toneRegenInst == NULL) return Ti_InvalidChannel;

      CHN_unlock (pChanB);
      InitToneRelayGen (&pChanB->pktToPcm.TGInfo);
      parse2833Event (&toneEvt, pyld,pChan->pcmToPkt.SamplesPerFrame);

      if (pChanB->pktToPcm.Coding == G722_64)
         toneEvt.TimeElapsed *= 2;

      if (pChanB->ProcessRate == 16000) {
         toneEvt.ToneFrequency[0] /= 2;
         toneEvt.ToneFrequency[1] /= 2;
      }
      TR_ADT_sendNewEventToGenerator (toneRegenInst, &toneEvt);

      CHN_unlock (pChanB);
   } 
   return Ti_Success;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ SendWarningEvent
//
// FUNCTION
//   This function builds and places a warning event in the event buffer
//
// RETURNS
//  none
//}
#pragma CODE_SECTION (SendWarningEvent, "MEDIUM_PROG_SECT")
void SendWarningEvent (ADT_UInt16 ChanId, ADT_UInt16 Warning) {

   EventFifoMsg_t eventMsg;

#if _DEBUG
   {
   char Buff[80];
    
   sprintf (Buff, "XXXX=%d  Chan: %1d  Warning: %d\n", ApiBlock.DmaSwiCnt, ChanId, Warning);
   CaptureText (Buff);
   }
#endif


   eventMsg.header.channelId = ChanId;
   eventMsg.header.eventCode =  EventWarning;
   eventMsg.header.eventLength = EVENT_FIFO_MSGLEN_ZERO; 
   eventMsg.header.deviceSide = Warning;
   writeEventIntoFifo (&eventMsg);
   logTime (0x08000000ul | (Warning << 8) | ChanId);
   
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ writeEventIntoFifo
//
// FUNCTION
//   This function places a warning event in the event buffer and posts a host interrupt
//
// RETURNS
//  none
//}
#pragma CODE_SECTION (writeEventIntoFifo, "MEDIUM_PROG_SECT")
int writeEventIntoFifo (EventFifoMsg_t *msg) {
   ADT_UInt16 byteLen, i16Len;
   int mask, empty;

   // don't send events not supported by the older API.
   if (ApiBlock.ApiVersionId <= DEFAULT_GPAK_API_VERSION) {
      switch (msg->header.eventCode) {
      case EventToneDetect:
          msg->header.eventLength  = 2;  // older API tone paylen is 2
          break;
      case EventVadStateVoice:
      case EventVadStateSilence:
      case EventTxCidMessageComplete:
      case EventRxCidCarrierLock:
      case EventRxCidMessageComplete:
      case EventLoopbackTeardownComplete:
      case EventDSPDebug:
          return 1;
      default:
          break;
      }
   }


   // force the length to be an even multiple of host transfer word size
   byteLen = msg->header.eventLength + (EVENT_HDR_I16LEN * 2);
   i16Len  = (byteLen + 1) / 2;

//   mask = ADDRESS_lock (&fifoLock.Inst);
   mask = ADDRESS_lock (&pfifoLock->Inst);
   if (SysAlgs.apiCacheEnable) {
        CACHE_INV(ApiBlock.pRdBlock, CACHE_L2_LINE_SIZE);
        eventFIFOInfo.TakeIndex = ApiBlock.pRdBlock->EventFIFOTakeIndex;
        eventFIFOInfo.PutIndex  = ApiBlock.pWrBlock->EventFIFOPutIndex;
   }

   empty = (eventFIFOInfo.PutIndex == eventFIFOInfo.TakeIndex);

   if (getFreeSpace (&eventFIFOInfo) - 2 < i16Len) { 
      //ADDRESS_unlock (&fifoLock.Inst, mask);
      ADDRESS_unlock (&pfifoLock->Inst, mask);
      return 0;
   }

   copyLinearToCirc (msg, &eventFIFOInfo, Int16PadForHost(i16Len));

   if (SysAlgs.apiCacheEnable) {
        CACHE_WBINV(eventFIFOInfo.pBufrBase, (eventFIFOInfo.BufrSize * sizeof(ADT_UInt16)));
        ApiBlock.pWrBlock->EventFIFOPutIndex  = eventFIFOInfo.PutIndex;
        CACHE_WBINV (ApiBlock.pWrBlock, CACHE_L2_LINE_SIZE);
   }

   //ADDRESS_unlock (&fifoLock.Inst, mask);
   ADDRESS_unlock (&pfifoLock->Inst, mask);

   // send an interrupt to the host informing it that an async event is ready
   if (empty) SetHostPortInterrupt (0);
   return (1);

}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ ProcReadAECParmsMsg 
//
// FUNCTION
//   Processing for a host's Read AEC Parameters message
//
// RETURNS
//  Length (dwords) of reply message.
//}
#if (DSP_TYPE == 54)
static ADT_UInt16            ProcReadAECParmsMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {
   pReply[0] |= (MSG_NULL_REPLY << 8);
   return  ReplyBytes (1);
}
static GPAK_AECParmsStat_t   ProcWriteAECParmsMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {   return AEC_NotConfigured;}
static GPAK_PlayRecordStat_t ProcPlayRecMsg(ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {   return PrUnsupportedCommand;}
#else
#pragma CODE_SECTION (ProcReadAECParmsMsg, "SLOW_PROG_SECT")
static ADT_UInt16            ProcReadAECParmsMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {

   // Prepare the reply message.
   pReply[0] |= (MSG_READ_AEC_PARMS_REPLY << 8);
   if (sysConfig.AECInstances == 0) {
     return  ReplyBytes (1);
   }

   // Store the AEC Parameters information in the reply message.

#if (AEC_LIB_VERSION >= 0x0420)

   pReply[1] = (ADT_UInt16) AECConfig.activeTailLengthMSec;
   pReply[2] = (ADT_UInt16) AECConfig.totalTailLengthMSec;
   pReply[3] = (ADT_UInt16) AECConfig.txNLPAggressiveness;
   pReply[4] = (ADT_UInt16) AECConfig.maxTxLossSTdB;
   pReply[5] = (ADT_UInt16) AECConfig.maxTxLossDTdB;
   pReply[6] = (ADT_UInt16) AECConfig.maxRxLossdB;
   pReply[7] = (ADT_UInt16) AECConfig.initialRxOutAttendB;
   pReply[8] = (ADT_UInt16) AECConfig.targetResidualLeveldBm;
   pReply[9] = (ADT_UInt16) AECConfig.maxRxNoiseLeveldBm;
   pReply[10] = (ADT_UInt16) AECConfig.worstExpectedERLdB;
   pReply[11] = (ADT_UInt16) AECConfig.rxSaturateLeveldBm;
   pReply[12] = (ADT_UInt16) AECConfig.noiseReduction1Setting;
   pReply[13] = (ADT_UInt16) AECConfig.noiseReduction2Setting;
   pReply[14] = (ADT_UInt16) AECConfig.cngEnable;
   pReply[15] = (ADT_UInt16) AECConfig.fixedGaindB10;
   pReply[16] = (ADT_UInt16) AECConfig.txAGCEnable;
   pReply[17] = (ADT_UInt16) AECConfig.txAGCMaxGaindB;
   pReply[18] = (ADT_UInt16) AECConfig.txAGCMaxLossdB;
   pReply[19] = (ADT_UInt16) AECConfig.txAGCTargetLeveldBm;
   pReply[20] = (ADT_UInt16) AECConfig.txAGCLowSigThreshdBm;
   pReply[21] = (ADT_UInt16) AECConfig.rxAGCEnable;
   pReply[22] = (ADT_UInt16) AECConfig.rxAGCMaxGaindB;
   pReply[23] = (ADT_UInt16) AECConfig.rxAGCMaxLossdB;
   pReply[24] = (ADT_UInt16) AECConfig.rxAGCTargetLeveldBm;
   pReply[25] = (ADT_UInt16) AECConfig.rxAGCLowSigThreshdBm;
   pReply[26] = (ADT_UInt16) AECConfig.rxBypassEnable;
   pReply[27] = (ADT_UInt16) AECConfig.maxTrainingTimeMSec;
   pReply[28] = (ADT_UInt16) AECConfig.trainingRxNoiseLeveldBm;

#if (AEC_LIB_VERSION >= 0x0430)

   pReply[29] = (ADT_UInt16) AECConfig.antiHowlEnable;

   // Return with the length of the reply message.
   return ReplyBytes (30);

#else

   // Return with the length of the reply message.
   return ReplyBytes (29);

#endif

#else
#ifdef AEC_SUPPORTED
    pReply[1]   = (ADT_UInt16) AECConfig.activeTailLengthMSec;
    pReply[2]   = (ADT_UInt16) AECConfig.totalTailLengthMSec;
    pReply[3]   = (ADT_UInt16) AECConfig.maxTxNLPThresholddB;
    pReply[4]   = (ADT_UInt16) AECConfig.maxTxLossdB;
    pReply[5]   = (ADT_UInt16) AECConfig.maxRxLossdB;
    pReply[6]   = (ADT_UInt16) AECConfig.targetResidualLeveldBm;
    pReply[7]   = (ADT_UInt16) AECConfig.maxRxNoiseLeveldBm;
    pReply[8]   = (ADT_UInt16) AECConfig.worstExpectedERLdB;
    pReply[9]   = (ADT_UInt16) AECConfig.noiseReductionSetting;
    pReply[10]  = (ADT_UInt16) AECConfig.cngEnable;
    pReply[11]  = (ADT_UInt16) AECConfig.agcEnable;
    pReply[12]  = (ADT_UInt16) AECConfig.agcMaxGaindB;
    pReply[13]  = (ADT_UInt16) AECConfig.agcMaxLossdB;
    pReply[14]  = (ADT_UInt16) AECConfig.agcTargetLeveldBm;
    pReply[15]  = (ADT_UInt16) AECConfig.agcLowSigThreshdBm;
    pReply[16]  = (ADT_UInt16) AECConfig.maxTrainingTimeMSec;
    pReply[17]  = (ADT_UInt16) AECConfig.rxSaturateLeveldBm;
    pReply[18]  = (ADT_UInt16) AECConfig.trainingRxNoiseLeveldBm;
    pReply[19]  = (ADT_UInt16) AECConfig.fixedGaindB10;
#endif
   // Return with the length of the reply message.
   return ReplyBytes (20);

#endif
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ ProcWriteAECParmsMsg
//
// FUNCTION
//   Performs processing for a host's Write AEC Parameters message and builds the reply message.
//
// RETURNS
//  Return status for reply message.
//}
#pragma CODE_SECTION (ProcWriteAECParmsMsg, "SLOW_PROG_SECT")
static GPAK_AECParmsStat_t   ProcWriteAECParmsMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {
#ifdef AEC_SUPPORTED
#if (AEC_LIB_VERSION >= 0x0420)
    ADT_Int16 activeTailLengthMSec;
    ADT_Int16 totalTailLengthMSec;
    ADT_Int16 txNLPAggressiveness;
    ADT_Int16 maxTxLossSTdB;
    ADT_Int16 maxTxLossDTdB;
    ADT_Int16 maxRxLossdB;
    ADT_Int16 initialRxOutAttendB;
    ADT_Int16 targetResidualLeveldBm;
    ADT_Int16 maxRxNoiseLeveldBm;
    ADT_Int16 worstExpectedERLdB;
    ADT_Int16 rxSaturateLeveldBm;
    ADT_Int16 noiseReduction1Setting;
    ADT_Int16 noiseReduction2Setting;
    ADT_Int16 cngEnable;
    ADT_Int16 fixedGaindB10;
    ADT_Int16 txAGCEnable;
    ADT_Int16 txAGCMaxGaindB;
    ADT_Int16 txAGCMaxLossdB;
    ADT_Int16 txAGCTargetLeveldBm;
    ADT_Int16 txAGCLowSigThreshdBm;
    ADT_Int16 rxAGCEnable;
    ADT_Int16 rxAGCMaxGaindB;
    ADT_Int16 rxAGCMaxLossdB;
    ADT_Int16 rxAGCTargetLeveldBm;
    ADT_Int16 rxAGCLowSigThreshdBm;
    ADT_Int16 rxBypassEnable;
    ADT_Int16 maxTrainingTimeMSec;
    ADT_Int16 trainingRxNoiseLeveldBm;
#if (AEC_LIB_VERSION >= 0x0430)
    ADT_Int16 antiHowlEnable;
#endif
#else
    ADT_Int16       activeTailLengthMSec;   
    ADT_Int16       totalTailLengthMSec;    
    ADT_Int16       maxTxNLPThresholddB;    
    ADT_Int16       maxTxLossdB;            
    ADT_Int16       maxRxLossdB;            
    ADT_Int16       targetResidualLeveldBm; 
    ADT_Int16       maxRxNoiseLeveldBm;     
    ADT_Int16       worstExpectedERLdB;     
    ADT_UInt16      noiseReductionEnable;   
    ADT_UInt16      cngEnable;              
    ADT_UInt16      agcEnable;              
    ADT_Int16       agcMaxGaindB;           
    ADT_Int16       agcMaxLossdB;           
    ADT_Int16       agcTargetLeveldBm;      
    ADT_Int16       agcLowSigThreshdBm;     
    ADT_Int16       maxTrainingTimeMSec;    
    ADT_Int16       rxSaturateLeveldBm;     
    ADT_Int16       trainingRxNoiseLeveldBm;
    ADT_Int16       fixedGaindB10;          
#endif

    // Prepare the reply message.
   pReply[0] |= (MSG_WRITE_AEC_PARMS_REPLY << 8);

   if (sysConfig.AECInstances == 0)
      return AEC_NotConfigured;

   // Assign configuration parameters to local variables
#if (AEC_LIB_VERSION >= 0x0420)
    activeTailLengthMSec    = (ADT_Int16) pCmd[1];
    totalTailLengthMSec     = (ADT_Int16) pCmd[2];
    txNLPAggressiveness     = (ADT_Int16) pCmd[3];
    maxTxLossSTdB           = (ADT_Int16) pCmd[4];
    maxTxLossDTdB           = (ADT_Int16) pCmd[5];
    maxRxLossdB             = (ADT_Int16) pCmd[6];
    initialRxOutAttendB     = (ADT_Int16) pCmd[7];
    targetResidualLeveldBm  = (ADT_Int16) pCmd[8];
    maxRxNoiseLeveldBm      = (ADT_Int16) pCmd[9];
    worstExpectedERLdB      = (ADT_Int16) pCmd[10];
    rxSaturateLeveldBm      = (ADT_Int16) pCmd[11];
    noiseReduction1Setting  = (ADT_Int16) pCmd[12];
    noiseReduction2Setting  = (ADT_Int16) pCmd[13];
    cngEnable               = (ADT_Int16) pCmd[14];
    fixedGaindB10           = (ADT_Int16) pCmd[15];
    txAGCEnable             = (ADT_Int16) pCmd[16];
    txAGCMaxGaindB          = (ADT_Int16) pCmd[17];
    txAGCMaxLossdB          = (ADT_Int16) pCmd[18];
    txAGCTargetLeveldBm     = (ADT_Int16) pCmd[19];
    txAGCLowSigThreshdBm    = (ADT_Int16) pCmd[20];
    rxAGCEnable             = (ADT_Int16) pCmd[21];
    rxAGCMaxGaindB          = (ADT_Int16) pCmd[22];
    rxAGCMaxLossdB          = (ADT_Int16) pCmd[23];
    rxAGCTargetLeveldBm     = (ADT_Int16) pCmd[24];
    rxAGCLowSigThreshdBm    = (ADT_Int16) pCmd[25];
    rxBypassEnable          = (ADT_Int16) pCmd[26];
    maxTrainingTimeMSec     = (ADT_Int16) pCmd[27];
    trainingRxNoiseLeveldBm = (ADT_Int16) pCmd[28];
#if (AEC_LIB_VERSION >= 0x0430)
    antiHowlEnable          = (ADT_Int16) pCmd[29];
#endif

#else
    activeTailLengthMSec     = (ADT_Int16) pCmd[1]; 
    totalTailLengthMSec      = (ADT_Int16) pCmd[2]; 
    maxTxNLPThresholddB      = (ADT_Int16) pCmd[3]; 
    maxTxLossdB              = (ADT_Int16) pCmd[4]; 
    maxRxLossdB              = (ADT_Int16) pCmd[5]; 
    targetResidualLeveldBm   = (ADT_Int16) pCmd[6]; 
    maxRxNoiseLeveldBm       = (ADT_Int16) pCmd[7]; 
    worstExpectedERLdB       = (ADT_Int16) pCmd[8]; 
    noiseReductionEnable     = (ADT_UInt16) pCmd[9]; 
    cngEnable                = (ADT_UInt16) pCmd[10];
    agcEnable                = (ADT_UInt16) pCmd[11];
    agcMaxGaindB             = (ADT_Int16) pCmd[12];
    agcMaxLossdB             = (ADT_Int16) pCmd[13];
    agcTargetLeveldBm        = (ADT_Int16) pCmd[14];
    agcLowSigThreshdBm       = (ADT_Int16) pCmd[15];
    maxTrainingTimeMSec      = (ADT_Int16) pCmd[16];
    rxSaturateLeveldBm       = (ADT_Int16) pCmd[17];
    trainingRxNoiseLeveldBm  = (ADT_Int16) pCmd[18];  
    fixedGaindB10            = (ADT_Int16) pCmd[19];  
#endif

    // Verify AEC parameters are within range
#if (AEC_LIB_VERSION >= 0x0420)
    if (OutOfRange ( totalTailLengthMSec, MIN_AEC_TAIL_LEN_MS, sysConfig.AECTailLen))
      return AEC_BadTailLength;
    if (OutOfRange ( activeTailLengthMSec, MIN_AEC_TAIL_LEN_MS, sysConfig.AECTailLen))
      return AEC_BadTailLength;
    if ((activeTailLengthMSec) % 8 != 0)
      return AEC_BadTailLength;

    if (OutOfRange ( txNLPAggressiveness, MIN_AEC_TX_NLP_AGGRESSIVENESS, MAX_AEC_TX_NLP_AGGRESSIVENESS))
      return AEC_BadTxNLPThreshold;
    if (OutOfRange ( maxTxLossSTdB, MIN_AEC_TX_LOSS_DB, MAX_AEC_TX_LOSS_DB))
      return AEC_BadTxLoss;
    if (OutOfRange ( maxTxLossDTdB, MIN_AEC_TX_LOSS_DB, MAX_AEC_TX_LOSS_DB))
      return AEC_BadTxLoss;
    if (OutOfRange ( maxRxLossdB, MIN_AEC_RX_LOSS_DB, MAX_AEC_RX_LOSS_DB))
      return AEC_BadRxLoss;
    if (OutOfRange ( initialRxOutAttendB, MIN_AEC_RX_LOSS_DB, MAX_AEC_RX_LOSS_DB))
      return AEC_BadRxLoss;
    if (OutOfRange ( targetResidualLeveldBm,  MIN_AEC_TARGET_RESIDUAL_DBM,  MAX_AEC_TARGET_RESIDUAL_DBM))
      return AEC_BadTargetResidualLevel;
    if (OutOfRange ( maxRxNoiseLeveldBm, MIN_AEC_RX_NOISELEV_DBM, MAX_AEC_RX_NOISELEV_DBM))
      return AEC_BadMaxRxNoiseLevel;
    if (OutOfRange ( worstExpectedERLdB, MIN_AEC_WORST_ERL_DB, MAX_AEC_WORST_ERL_DB))
      return AEC_BadWorstExpectedERL;
    if (OutOfRange ( rxSaturateLeveldBm, MIN_AEC_RX_SATLEV_DB, MAX_AEC_RX_SATLEV_DB))
      return AEC_BadRxSaturateLevel;
    if (OutOfRange ( noiseReduction1Setting, MIN_AEC_NR_SETTING, MAX_AEC_NR_SETTING))
        return AEC_BadNoiseReduction;
    if (OutOfRange ( noiseReduction2Setting, MIN_AEC_NR_SETTING, MAX_AEC_NR_SETTING))
        return AEC_BadNoiseReduction;
    if (OutOfRange ( fixedGaindB10, MIN_AEC_FIXGAIN_DB, MAX_AEC_FIXGAIN_DB))
      return AEC_BadFixedGain;
    if (OutOfRange ( txAGCMaxGaindB, MIN_AEC_AGC_GAIN_DB, MAX_AEC_AGC_GAIN_DB))
      return AEC_BadAgcMaxGain;
    if (OutOfRange ( txAGCMaxLossdB, MIN_AEC_AGC_LOSS_DB, MAX_AEC_AGC_LOSS_DB))
      return AEC_BadAgcMaxLoss;
    if (OutOfRange ( txAGCTargetLeveldBm, MIN_AEC_AGC_TRGTLEV_DBM, MAX_AEC_AGC_TRGTLEV_DBM))
      return AEC_BadAgcTargetLevel;
    if (OutOfRange ( txAGCLowSigThreshdBm, MIN_AEC_AGC_LOWSIG_THRESH_DBM, MAX_AEC_AGC_LOWSIG_THRESH_DBM))
      return AEC_BadAgcLowSigThresh;
    if (OutOfRange ( rxAGCMaxGaindB, MIN_AEC_AGC_GAIN_DB, MAX_AEC_AGC_GAIN_DB))
      return AEC_BadAgcMaxGain;
    if (OutOfRange ( rxAGCMaxLossdB, MIN_AEC_AGC_LOSS_DB, MAX_AEC_AGC_LOSS_DB))
      return AEC_BadAgcMaxLoss;
    if (OutOfRange ( rxAGCTargetLeveldBm, MIN_AEC_AGC_TRGTLEV_DBM, MAX_AEC_AGC_TRGTLEV_DBM))
      return AEC_BadAgcTargetLevel;
    if (OutOfRange ( rxAGCLowSigThreshdBm, MIN_AEC_AGC_LOWSIG_THRESH_DBM, MAX_AEC_AGC_LOWSIG_THRESH_DBM))
      return AEC_BadAgcLowSigThresh;
    if (OutOfRange ( maxTrainingTimeMSec, MIN_AEC_TRAIN_TIME_MS, MAX_AEC_TRAIN_TIME_MS))
      return AEC_BadMaxTrainingTime;
    if (OutOfRange ( trainingRxNoiseLeveldBm, MIN_AEC_TRAIN_RX_NOISE_DBM, MAX_AEC_TRAIN_RX_NOISE_DBM))
      return AEC_BadTrainingRxNoiseLevel;
#else
    if (OutOfRange ( totalTailLengthMSec, MIN_AEC_TAIL_LEN_MS * 2, sysConfig.AECTailLen))
      return AEC_BadTailLength;
    if (OutOfRange ( activeTailLengthMSec, MIN_AEC_TAIL_LEN_MS, sysConfig.AECTailLen / 2))
      return AEC_BadTailLength;
    if ((activeTailLengthMSec) % 8 != 0)
      return AEC_BadTailLength;

    if (OutOfRange ( maxTxNLPThresholddB, MIN_AEC_TX_NLP_THRESH_DB, MAX_AEC_TX_NLP_THRESH_DB))
      return AEC_BadTxNLPThreshold;
    if (OutOfRange ( maxTxLossdB, MIN_AEC_TX_LOSS_DB, MAX_AEC_TX_LOSS_DB))
      return AEC_BadTxLoss;
    if (OutOfRange ( maxRxLossdB, MIN_AEC_RX_LOSS_DB, MAX_AEC_RX_LOSS_DB))
      return AEC_BadRxLoss;
    if (OutOfRange ( targetResidualLeveldBm,  MIN_AEC_TARGET_RESIDUAL_DBM,  MAX_AEC_TARGET_RESIDUAL_DBM))
      return AEC_BadTargetResidualLevel;
    if (OutOfRange ( maxRxNoiseLeveldBm, MIN_AEC_RX_NOISELEV_DBM, MAX_AEC_RX_NOISELEV_DBM))
      return AEC_BadMaxRxNoiseLevel;
    if (OutOfRange ( worstExpectedERLdB, MIN_AEC_WORST_ERL_DB, MAX_AEC_WORST_ERL_DB))
      return AEC_BadWorstExpectedERL;
    if (OutOfRange ( agcMaxGaindB, MIN_AEC_AGC_GAIN_DB, MAX_AEC_AGC_GAIN_DB))
      return AEC_BadAgcMaxGain;
    if (OutOfRange ( agcMaxLossdB, MIN_AEC_AGC_LOSS_DB, MAX_AEC_AGC_LOSS_DB))
      return AEC_BadAgcMaxLoss;
    if (OutOfRange ( agcTargetLeveldBm, MIN_AEC_AGC_TRGTLEV_DBM, MAX_AEC_AGC_TRGTLEV_DBM))
      return AEC_BadAgcTargetLevel;
    if (OutOfRange ( agcLowSigThreshdBm, MIN_AEC_AGC_LOWSIG_THRESH_DBM, MAX_AEC_AGC_LOWSIG_THRESH_DBM))
      return AEC_BadAgcLowSigThresh;
    if (OutOfRange ( maxTrainingTimeMSec, MIN_AEC_TRAIN_TIME_MS, MAX_AEC_TRAIN_TIME_MS))
      return AEC_BadMaxTrainingTime;
    if (OutOfRange ( rxSaturateLeveldBm, MIN_AEC_RX_SATLEV_DB, MAX_AEC_RX_SATLEV_DB))
      return AEC_BadRxSaturateLevel;
    if (OutOfRange ( trainingRxNoiseLeveldBm, MIN_AEC_TRAIN_RX_NOISE_DBM, MAX_AEC_TRAIN_RX_NOISE_DBM))
      return AEC_BadTrainingRxNoiseLevel;
    if (OutOfRange ( fixedGaindB10, MIN_AEC_FIXGAIN_DB, MAX_AEC_FIXGAIN_DB))
      return AEC_BadFixedGain;
#endif

   // Save the AEC parameters in the AECConfig structure.
#if (AEC_LIB_VERSION >= 0x0420)
    AECConfig.activeTailLengthMSec    = activeTailLengthMSec;
    AECConfig.totalTailLengthMSec     = totalTailLengthMSec;
    AECConfig.txNLPAggressiveness     = txNLPAggressiveness;
    AECConfig.maxTxLossSTdB           = maxTxLossSTdB;
    AECConfig.maxTxLossDTdB           = maxTxLossDTdB;
    AECConfig.maxRxLossdB             = maxRxLossdB;
    AECConfig.initialRxOutAttendB     = initialRxOutAttendB;
    AECConfig.targetResidualLeveldBm  = targetResidualLeveldBm;
    AECConfig.maxRxNoiseLeveldBm      = maxRxNoiseLeveldBm;
    AECConfig.worstExpectedERLdB      = worstExpectedERLdB;
    AECConfig.rxSaturateLeveldBm      = rxSaturateLeveldBm;
    AECConfig.noiseReduction1Setting  = noiseReduction1Setting;
    AECConfig.noiseReduction2Setting  = noiseReduction2Setting;
    AECConfig.cngEnable               = cngEnable;
    AECConfig.fixedGaindB10           = fixedGaindB10;
    AECConfig.txAGCEnable             = txAGCEnable;
    AECConfig.txAGCMaxGaindB          = txAGCMaxGaindB;
    AECConfig.txAGCMaxLossdB          = txAGCMaxLossdB;
    AECConfig.txAGCTargetLeveldBm     = txAGCTargetLeveldBm;
    AECConfig.txAGCLowSigThreshdBm    = txAGCLowSigThreshdBm;
    AECConfig.rxAGCEnable             = rxAGCEnable;
    AECConfig.rxAGCMaxGaindB          = rxAGCMaxGaindB;
    AECConfig.rxAGCMaxLossdB          = rxAGCMaxLossdB;
    AECConfig.rxAGCTargetLeveldBm     = rxAGCTargetLeveldBm;
    AECConfig.rxAGCLowSigThreshdBm    = rxAGCLowSigThreshdBm;
    AECConfig.rxBypassEnable          = rxBypassEnable;
    AECConfig.maxTrainingTimeMSec     = maxTrainingTimeMSec;
    AECConfig.trainingRxNoiseLeveldBm = trainingRxNoiseLeveldBm;
#if (AEC_LIB_VERSION >= 0x0430)
    AECConfig.antiHowlEnable          = antiHowlEnable;
#endif
#else
    AECConfig.activeTailLengthMSec     = activeTailLengthMSec;
    AECConfig.totalTailLengthMSec      = totalTailLengthMSec;    
    AECConfig.maxTxNLPThresholddB      = maxTxNLPThresholddB;    
    AECConfig.maxTxLossdB              = maxTxLossdB;            
    AECConfig.maxRxLossdB              = maxRxLossdB;            
    AECConfig.targetResidualLeveldBm   = targetResidualLeveldBm; 
    AECConfig.maxRxNoiseLeveldBm       = maxRxNoiseLeveldBm;     
    AECConfig.worstExpectedERLdB       = worstExpectedERLdB;     
    AECConfig.rxSaturateLeveldBm       = rxSaturateLeveldBm;     
    AECConfig.noiseReductionSetting    = noiseReductionEnable ? 1 : 0;
    AECConfig.cngEnable                = cngEnable ? Enabled : Disabled;              
    AECConfig.fixedGaindB10            = fixedGaindB10;          
    AECConfig.agcEnable                = agcEnable ? Enabled : Disabled;              
    AECConfig.agcMaxGaindB             = agcMaxGaindB;           
    AECConfig.agcMaxLossdB             = agcMaxLossdB;           
    AECConfig.agcTargetLeveldBm        = agcTargetLeveldBm;      
    AECConfig.agcLowSigThreshdBm       = agcLowSigThreshdBm;     
    AECConfig.maxTrainingTimeMSec      = (ADT_UInt16)maxTrainingTimeMSec;    
    AECConfig.trainingRxNoiseLeveldBm  = trainingRxNoiseLeveldBm;
#endif
#endif
   // Return with the reply's return status.
   return AEC_Success;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ ProcPlayRecordMsg - Process a voice playback-record Request message.
//
// FUNCTION
//   This function performs all processing for a host's voice playback record Request
//   message and builds the reply message.
//
//
// RETURNS
//  none
//}

//  Format of play/record buffer
typedef struct playRecBuff {
   ADT_UInt32           buffI16;  // Length of buffer in I16 elements
   GpakPlayRecordMode_t coding;   // Data coding
   ADT_UInt16           buff[1];  // Data buffer
} playRecBuff;

#pragma CODE_SECTION (ProcPlayRecMsg, "SLOW_PROG_SECT")
static GPAK_PlayRecordStat_t ProcPlayRecMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {

   chanInfo_t *chan;                   // pointer to Channel structure
   ADT_UInt16 ChannelId;               // channel identifier
   ADT_UInt16 FrameRate, CvtFrameRate; // Frame rate in TDM samples and Cvt samples
   GpakDeviceSide_t deviceSide;        // device side

   GpakPlayRecordCmd_t  Cmd;       // command
   GpakPlayRecordMode_t coding;    // data coding


   playRecBuff *playBuff;     // start address of memory block
   ADT_UInt32 blockAddr;      // memory block
   ADT_UInt32 blockI16;       // memory block buffer length.

   GpakPlayRecordState_t state;
   GpakToneCodes_t stopTone;
   playRecInfo_t *pInfo, *pPlayInfo, *pRecInfo;

   pcm2pkt_t *PcmPkt;
   pkt2pcm_t *PktPcm;

   // Get the Channel Id from the command message.
   ChannelId = pCmd[1];

   // Prepare the reply message.
   pReply[0] |= (MSG_PLAY_RECORD_RESPONSE << 8);
   pReply[1] = ChannelId;
   chan = chanTable[ChannelId];
   PcmPkt = &chan->pcmToPkt;
   PktPcm = &chan->pktToPcm;


   if (sysConfig.maxNumChannels <= ChannelId)
      return PrInvalidChannel;

   if (chan->channelType == inactive)
      return PrInactiveChannel;

   //------------------------------------------------
   // Parse message and verify fields
   Cmd = (GpakPlayRecordCmd_t) (pCmd[0] & 0xFF);

   // force 32-bit alignment for recording address
   blockAddr  = PACK16TO32(pCmd[2], pCmd[3]);
   if (blockAddr & 3)
      return PrInvalidAddressAlignment;

   playBuff = (playRecBuff *) blockAddr;

   // convert the blocklength from bytes to 16-bit words. 
   blockI16 = PACK16TO32(pCmd[4], pCmd[5]);
   blockI16 >>= 1;

   coding = (GpakPlayRecordMode_t) ((pCmd[6] >> 8) & 0x00ff);
   if ((coding != RecordingL16) && (coding != RecordingAlaw) && (coding != RecordingUlaw))
      return PrUnsupportedRecordMode;

   // Verify device-side channel type combination
   deviceSide = (GpakDeviceSide_t) (pCmd[7] & 0x0003);
   switch (chan->channelType) {

   case conferencePcm:
   case conferencePacket:
   case conferenceMultiRate:
   case conferenceComposite:
      if (deviceSide != ADevice) return PrInvalidDevice;
      break;

   case customChannel:
   case pcmToPcm:       break;

// jdc: allow b-side recording   
   case pcmToPacket: break;
   case packetToPacket:
      if (((Cmd == StartRecording) || (Cmd == StartFileRecording)) && (deviceSide != ADevice))
         return PrInvalidDevice;
      break;
       
   default:
      AppErr ("Invalid channel type", TRUE);
   }

   // Verify valid tone and detection capabilities
   if ((Cmd == StartRecording) || (Cmd == StartFileRecording))
      stopTone = (GpakToneCodes_t) (pCmd[6] & 0x00ff);
   else 
      stopTone = tdsNoToneDetected;

   if (stopTone != tdsNoToneDetected) {
      if (15 < stopTone)
         return PrInvalidStopTone;

      if ((deviceSide == ADevice) && ((chan->pcmToPkt.toneTypes & DTMF_tone) == 0))
         return PrToneDetectionUnsupported;

      if ((deviceSide == BDevice) && ((chan->pktToPcm.toneTypes & DTMF_tone) == 0))
         return PrToneDetectionUnsupported;
   }


   // Find device's instance structure and converted frame rate
   CvtFrameRate = 0;
#define ToCvtSamples(TDMSamples) (TDMSamples * (16000 / TDMRate))
   if (chan->channelType == conferenceMultiRate) {
      CvtFrameRate = ToCvtSamples(GetGpakCnfr (chan->CoreID, chan->PairedChannelId)->FrameSize);
   }
   if (chan->channelType == customChannel) {
      customGetPlaybackInstances (chan, deviceSide, &pPlayInfo, &pRecInfo, &FrameRate);
      CvtFrameRate = ToCvtSamples(FrameRate);
   } else if (deviceSide == ADevice) {
      pPlayInfo = &PktPcm->playA;
      pRecInfo  = &PcmPkt->recA;
   } else if (deviceSide == BDevice) {
      pPlayInfo = &PcmPkt->playB;
      pRecInfo  = &PktPcm->recB;
   }

   if ((pPlayInfo == NULL) || (pRecInfo == NULL))
      return PrInvalidDevice;

   // Set state based upon command
   switch (Cmd) {
   default:                      return PrUnsupportedCommand;
   case StartPlayBack:           state = PlayBack;                  break;
   case StartFilePlayBack:       state = (GpakPlayRecordState_t) (PlayBack | FileRecording);  break;
   case StartContinuousPlayBack: state = (GpakPlayRecordState_t) (PlayBack | PlayContinuous); break;

   case StartRecording:          state = Record;                    break;
   case StartFileRecording:      state = (GpakPlayRecordState_t) (Record | FileRecording);    break;

   case StopPlayBack:
      if (pPlayInfo->state != Inactive)  {
         playRecordEnd (ChannelId, EventPlaybackComplete, pPlayInfo, deviceSide);
      }
      return PrSuccess;

  case StopRecording:
      if (pRecInfo->state != Inactive) {
         playRecordEnd (ChannelId, EventRecordingStopped, pRecInfo, deviceSide);
      }
      return PrSuccess;
   }
   if (blockAddr < (ADT_UInt32) PlayRecStartAddr)
      return PrStartAddrOutOfRange;

   if (state & PlayBack) {
      // Allocate, size, and initialize rate conversion buffer
      pInfo = pPlayInfo;
      if (CvtFrameRate == 0) {
         if (deviceSide == ADevice) 
            CvtFrameRate = ToCvtSamples(PktPcm->SamplesPerFrame);
         else
            CvtFrameRate = ToCvtSamples(PcmPkt->SamplesPerFrame);
      }
      if (chan->ProcessRate < TDMRate) {
         pInfo->CvtInst = AllocDownSamplingInst (&pInfo->CvtIndex);
         if (pInfo->CvtInst == NULL) return PrRateConvertersUnavailable;
         InitDownSamplingInst (pInfo->CvtInst, CvtFrameRate);
      } else if (TDMRate < chan->ProcessRate) {
         pInfo->CvtInst   = AllocUpSamplingInst (&pInfo->CvtIndex);
         if (pInfo->CvtInst == NULL) return PrRateConvertersUnavailable;
         pInfo->CvtIndex |= PlayRecordUpConvertBit;
         InitUpSamplingInst (pInfo->CvtInst, CvtFrameRate);
      }

      // Retrieve coding type and length from play buffer
      CACHE_INV (playBuff, 8); 
      if (state & FileRecording)
         playBuff->coding = coding;
      else {
         coding = playBuff->coding; 
         if (playBuff->buffI16 < blockI16)
            blockI16 = playBuff->buffI16;  // limit playback to actual length of recording
      }

      CACHE_INV (playBuff, (blockI16*2) + 8); 
      pInfo->Circ.PutIndex  = blockI16 - 1;

   } else if (state & Record) {
      // Store coding type in play buffer
      pInfo = pRecInfo;
      if (CvtFrameRate == 0) {
         if (deviceSide == ADevice) 
            CvtFrameRate = ToCvtSamples(PcmPkt->SamplesPerFrame);
         else
            CvtFrameRate = ToCvtSamples(PktPcm->SamplesPerFrame);
      }
      if (chan->ProcessRate < TDMRate) {
         pInfo->CvtInst   = AllocUpSamplingInst (&pInfo->CvtIndex);
         pInfo->CvtIndex |= PlayRecordUpConvertBit;
         if (pInfo->CvtInst == NULL) return PrRateConvertersUnavailable;
      } else if (TDMRate < chan->ProcessRate) {
         pInfo->CvtInst = AllocDownSamplingInst (&pInfo->CvtIndex);
         if (pInfo->CvtInst == NULL) return PrRateConvertersUnavailable;
         InitUpSamplingInst (pInfo->CvtInst, CvtFrameRate);
      }
      playBuff->coding = coding;
      pInfo->Circ.PutIndex  = 0;
   }
   pInfo->CvtFrame = CvtFrameRate;
   pInfo->blockStart = (void *) playBuff;
   pInfo->mode     = coding;
   pInfo->stopTone = stopTone;

   pInfo->Circ.pBufrBase = playBuff->buff;
   pInfo->Circ.BufrSize  = blockI16;
   pInfo->Circ.TakeIndex = 0;

   pInfo->state = state;
   return PrSuccess;
}
#endif // end if not 54x
 

#pragma CODE_SECTION (GetEcanState, "SLOW_PROG_SECT")
static ADT_UInt16 GetEcanState (chanInfo_t *pChan, ADT_UInt16 *pBuf, GpakDeviceSide_t device) {

   G168Params_t *pEcan;
   ADT_UInt16 ecanLen, echoType;
   ADT_Int16 adaptRestore;

   if (device == ADevice) {
      pEcan = (G168Params_t *) pChan->pcmToPkt.pG168Chan;
      echoType = (pChan->pcmToPkt.EcIndex) >> 8;
      if (echoType == G168_PCM)  ecanLen = sysConfig.EcanPcmSaveLenI16;
      else                       ecanLen = sysConfig.EcanPktSaveLenI16;
   } else {
      pEcan = (G168Params_t *) pChan->pktToPcm.pG168Chan;
      echoType = (pChan->pktToPcm.EcIndex) >> 8;
      if (echoType == G168_PCM)  ecanLen = sysConfig.EcanPcmSaveLenI16;
      else                       ecanLen = sysConfig.EcanPktSaveLenI16;
   }

    // Prevent framing task from modifying coeffients while reading
   adaptRestore = pEcan->AdaptEnable;
   LEC_ADT_coefStore ((G168ChannelInstance_t *) pEcan, (ADT_Int16 *)pBuf);
   pEcan->AdaptEnable = adaptRestore;
   return ecanLen;
}    

#pragma CODE_SECTION (SetEcanState, "SLOW_PROG_SECT")
static ADT_UInt16 SetEcanState (chanInfo_t *pChan, ADT_UInt16 *pBuf, GpakDeviceSide_t device) {
   G168Params_t * pEcan;
   ADT_UInt16 ecanLen, echoType;
   ADT_Int16 adaptRestore;

    if (device == ADevice) {
        pEcan = (G168Params_t *) pChan->pcmToPkt.pG168Chan;
        echoType = (pChan->pcmToPkt.EcIndex) >> 8;
        if (echoType == G168_PCM)  ecanLen = sysConfig.EcanPcmSaveLenI16;
        else                       ecanLen = sysConfig.EcanPktSaveLenI16;

    } else {
        pEcan = (G168Params_t *) pChan->pktToPcm.pG168Chan;
        echoType = (pChan->pktToPcm.EcIndex) >> 8;
        if (echoType == G168_PCM)  ecanLen = sysConfig.EcanPcmSaveLenI16;
        else                       ecanLen = sysConfig.EcanPktSaveLenI16;
    }

    // Prevent framing task from modifying coeffients while updating
   adaptRestore = pEcan->AdaptEnable;
   pEcan->AdaptEnable = FALSE;

   LEC_ADT_coefLoad ((G168ChannelInstance_t *) pEcan, (ADT_Int16 *)pBuf);
   pEcan->AdaptEnable = adaptRestore;

    return ecanLen;
}    

#pragma CODE_SECTION (ProcReadEcanStateMsg, "SLOW_PROG_SECT")
static ADT_UInt16 ProcReadEcanStateMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {
   ADT_UInt16 ChannelId;        
   chanInfo_t *chan;   
   int valid;
   GpakDeviceSide_t device;
   ADT_UInt16 replyLen;

   valid = 1;
   ChannelId = (pCmd[1] >> 8) & 0xFF;
   device = (GpakDeviceSide_t) pCmd[2];
   // Prepare the reply message.
   pReply[0] |= (MSG_READ_ECAN_STATE_REPLY << 8);
   pReply[1] = ChannelId;

   if (1 < DSPTotalCores) 
      valid = 0;

   if (ChannelId >= sysConfig.maxNumChannels)
        valid = 0;

   if (valid) { 
        chan = chanTable[ChannelId];
        if (chan->channelType == inactive)
            valid = 0;
        else {
            if ((device == ADevice) && chan->pcmToPkt.EchoCancel == Disabled) 
                valid = 0;
            if ((device == BDevice) && chan->pktToPcm.EchoCancel == Disabled) 
                valid = 0;
        }
   }

   if (valid)  {
      replyLen = 2 + GetEcanState (chan, (ADT_UInt16 *) &(pReply[2]), device);      
   } else {
      pReply[1] |= 0xFF00;
        replyLen = 2;
    }

   return (replyLen);
 }

#pragma CODE_SECTION (ProcWriteEcanStateMsg , "SLOW_PROG_SECT")
static ADT_UInt16 ProcWriteEcanStateMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) { 

   ADT_UInt16 ChannelId;        
   chanInfo_t *chan;   
   int valid;
   GpakDeviceSide_t device;
   ADT_UInt16 reply;

   valid = 1;
   ChannelId = (pCmd[1] >> 8) & 0xFF;
   device = (GpakDeviceSide_t) pCmd[2];
   reply = 0;


   // Prepare the reply message.
   pReply[0] |= (MSG_WRITE_ECAN_STATE_REPLY << 8);
   pReply[1] = ChannelId;

   if (1 < DSPTotalCores) 
      valid = 0;

   if (ChannelId >= sysConfig.maxNumChannels)
        valid = 0;

   if (valid) { 
        chan = chanTable[ChannelId];
        if (chan->channelType == inactive)
            valid = 0;
        else {
            if ((device == ADevice) && chan->pcmToPkt.EchoCancel == Disabled) 
               valid = 0;
            if ((device == BDevice) && chan->pktToPcm.EchoCancel == Disabled) 
               valid = 0;
        }
   }

   if (valid) 
      SetEcanState (chan, (ADT_UInt16 *) &(pCmd[3]), device);
   else {
      reply = 0xFF00;
    }

   return reply;
}

#pragma CODE_SECTION (ProcReadLecChanStatusMsg, "SLOW_PROG_SECT")
static ADT_UInt16 ProcReadLecChanStatusMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {
   ADT_UInt16 channelId;        // channel identifier
   GpakDeviceSide_t chanSide;   // channel side
   chanInfo_t *pChan;           // pointer to Channel structure
   G168Params_t *pEcan;         // pointer to ECAN context
   G168StatusV11_t lecStatus;   // LEC status

   // Get the Channel Id and Side from the command message.
   channelId = (pCmd[1] >> 8) & 0xFF;
   chanSide = (GpakDeviceSide_t) pCmd[2];

   // Prepare the reply message.
   pReply[0] |= (MSG_READ_LEC_STATUS_REPLY << 8);
   pReply[1] = channelId << 8;
   pReply[2] = chanSide;

   // Verify the Channel Id is valid.
   if (channelId >= sysConfig.maxNumChannels)
   {
      pReply[1] |= (lecChanRead_InvalidChannel & 0xFF);
      return 3;
   }

   // Verify the Channel is active.
   pChan = chanTable[channelId];
   if (pChan->channelType == inactive) 
   {
      pReply[1] |= (lecChanRead_InactiveChannel & 0xFF);
      return 3;
   }

   // Verify the LEC is enabled.
   if (chanSide == ADevice)
   {
      if (pChan->pcmToPkt.EchoCancel != Enabled)
      {
         pReply[1] |= (lecChanRead_NotEnabled & 0xFF);
         return 3;
      }
      pEcan = (G168Params_t *) pChan->pcmToPkt.pG168Chan;
   }
   else
   {
      if (pChan->pktToPcm.EchoCancel != Enabled)
      {
         pReply[1] |= (lecChanRead_NotEnabled & 0xFF);
         return 3;
      }
      pEcan = (G168Params_t *) pChan->pktToPcm.pG168Chan;
   }

   // Get the LEC Status from the LEC algorithm.
   SWI_disable();
   LEC_ADT_g168GetStatus((G168ChannelInstance_t *) pEcan, &lecStatus);
   SWI_enable();

   // Store the LEC Status information in the reply message.
   pReply[3] = (ADT_UInt16) lecStatus.AdaptReport;
   pReply[4] = (ADT_UInt16) lecStatus.CrossCorrReport;
   pReply[5] = (ADT_UInt16) lecStatus.G165Status.Word;
   pReply[6] = (ADT_UInt16) lecStatus.ConvergenceStat;
   pReply[7] = (ADT_UInt16) lecStatus.NLPFlag;
   pReply[8] = (ADT_UInt16) lecStatus.DoubleTalkFlag;
   pReply[9] = (ADT_UInt16) lecStatus.SmartPacketMode;
   pReply[10] = (ADT_UInt16) lecStatus.ERL1;
   pReply[11] = (ADT_UInt16) lecStatus.ERL2;
   pReply[12] = (ADT_UInt16) lecStatus.ERLE;
   pReply[13] = (ADT_UInt16) lecStatus.StatusFlags.Word;
   pReply[14] = (ADT_UInt16) lecStatus.EventFlags.Word;

   pReply[1] |= (lecChanRead_Success & 0xFF);
   return 15;
}

#pragma CODE_SECTION (ProcWriteRtpTxTimeoutValueMsg , "SLOW_PROG_SECT")
static ADT_UInt16 ProcWriteRtpTxTimeoutValueMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {

    ADT_UInt16 channelId;        // channel identifier
    chanInfo_t *pChan;           // pointer to Channel structure

    // Get the Channel Id from the command message.

    channelId = (pCmd[0] ) & 0xFF;

    // Prepare the reply message.
    pReply[0] |= (MSG_WRITE_RTP_TIMEOUT_REPLY << 8);
    pReply[1] = channelId << 8;

    // Verify the Channel Id is valid.
    if (channelId >= sysConfig.maxNumChannels)
    {
        pReply[1] |= (RTPStat_ChannelError & 0xFF);
        return 2;
    }
    pChan = chanTable[channelId];

    // Set the ED137B Configuration
    SWI_disable();
    pChan->RtpRxTimeoutValue = pCmd[1];
    SWI_enable();

    pReply[1] |= (RTPStat_Success & 0xFF);
    return 2;

}

#pragma CODE_SECTION (ProcConfigureED137BMsg, "SLOW_PROG_SECT")
static ADT_UInt16 ProcConfigureED137BMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {
    ADT_UInt16 channelId;        // channel identifier
    chanInfo_t *pChan;           // pointer to Channel structure

    // Get the Channel Id from the command message.

    channelId = (pCmd[0] ) & 0xFF;

    // Prepare the reply message.
    pReply[0] |= (MSG_CONFIGURE_ED137B_PARMS_REPLY << 8);
    pReply[1] = channelId << 8;

    // Verify the Channel Id is valid.
    if (channelId >= sysConfig.maxNumChannels)
    {
      pReply[1] |= (ED137B_InvalidChannel & 0xFF);
      return 2;
    }
    pChan = chanTable[channelId];

    // Set the ED137B Configuration
    SWI_disable();
    pChan->ED137BcfgParms.rtp_ext_en                = pCmd[1];
    pChan->ED137BcfgParms.device                    = pCmd[2];
    pChan->ED137BcfgParms.txrxmode                  = pCmd[3];
    pChan->ED137BcfgParms.follow_squ_with_vad       = pCmd[4];
    pChan->ED137BcfgParms.ptt_rep                   = pCmd[5];
    pChan->ED137BcfgParms.R2SKeepAlivePeriod        = pCmd[6];
    pChan->ED137BcfgParms.R2SKeepAliveMultiplier    = pCmd[7];
    pChan->ED137BcfgParms.Ptt_val                   = pCmd[8];
    pChan->ED137BcfgParms.mask                      = pCmd[9];
    SWI_enable();

    pReply[1] |= (ED137B_Success & 0xFF);
    return 2;
}

#pragma CODE_SECTION (ProcUpdateED137B_PTT_Msg, "SLOW_PROG_SECT")
static ADT_UInt16 ProcUpdateED137B_PTT_Msg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {
    ADT_UInt16 channelId;        // channel identifier
    chanInfo_t *pChan;           // pointer to Channel structure

    // Get the Channel Id from the command message.
    channelId = (pCmd[0] ) & 0xFF;

    // Prepare the reply message.
    pReply[0] |= (MSG_UPDATE_ED137B_PTT_PARMS_REPLY << 8);
    pReply[1] = channelId << 8;

    // Verify the Channel Id is valid.
    if (channelId >= sysConfig.maxNumChannels)
    {
      pReply[1] |= (ED137B_InvalidChannel & 0xFF);
      return 2;
    }
    pChan = chanTable[channelId];

    // Check to see if a Previous Command is Pending
    if(( pChan->ED137TxState.TxState & TX_UPDATE_PTT) && (pChan->channelType != inactive))
    {
        pReply[1] |= (ED137B_PendingCommand & 0xFF);
        return 2;
    }

    // Set the ED137B Configuration

    if(pCmd[1] <= 0X04 && pCmd[2] <= 0X3F)
    {
        SWI_disable();
        pChan->ED137BPTT_Parms.PTT_Type = pCmd[1];
        pChan->ED137BPTT_Parms.SQU      = pCmd[2];
        pChan->ED137BPTT_Parms.PTT_ID   = pCmd[3];
        pChan->ED137BPTT_Parms.PM       = pCmd[4];
        pChan->ED137BPTT_Parms.PTTS     = pCmd[5];
        pChan->ED137TxState.TxState = pChan->ED137TxState.TxState | TX_UPDATE_PTT;
        SWI_enable();
        pReply[1] |= (ED137B_Success & 0xFF);
    }
    else
    {
        pReply[1] |= ( ED137B_Invalid_Param & 0xFF);
    }
    return 2;
}

#pragma CODE_SECTION (ProcPersistentFeatureMsg, "SLOW_PROG_SECT")
static ADT_UInt16 ProcPersistentFeatureMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {
    ADT_UInt16 channelId;        // channel identifier
    chanInfo_t *pChan;           // pointer to Channel structure

    // Get the Channel Id from the command message.
    channelId = (pCmd[0] ) & 0xFF;

    // Prepare the reply message.
    pReply[0] |= (MSG_CONFIGURE_ED137B_PERSISTENT_FEATURES_PARMS_REPLY << 8);
    pReply[1] = channelId << 8;

    // Verify the Channel Id is valid.
    if (channelId >= sysConfig.maxNumChannels)
    {
      pReply[1] |= (ED137B_InvalidChannel & 0xFF);
      return 2;
    }
    pChan = chanTable[channelId];

    // Check to see if a Previous Command is Pending
    if((pChan->ED137TxState.TxState & TX_UPDATE_PF_MSG) && (pChan->channelType != inactive))
    {
        pReply[1] |= (ED137B_PendingCommand & 0xFF);
        return 2;
    }

    // Set the ED137B Configuration
    if(0x0B<=pCmd[1]<=0xF)
    {
        SWI_disable();
        pChan->ED137B_PersistentFeature.PersistentFeature_Type  = pCmd[1];
        pChan->ED137B_PersistentFeature.PersistentFeature_Mask  = pCmd[2];
        pChan->ED137B_PersistentFeature.PersistentFeature_Value = pCmd[3];
        pChan->ED137TxState.TxState = pChan->ED137TxState.TxState | TX_UPDATE_PF_MSG;
        SWI_enable();
        pReply[1] |= (ED137B_Success & 0xFF);
    }
    else
    {
        pReply[1] |= (ED137B_Invalid_Param & 0xFF);
    }
    return 2;
}

#pragma CODE_SECTION (ProcUpdatePersistentFeatureMsg, "SLOW_PROG_SECT")
static ADT_UInt16 ProcUpdatePersistentFeatureMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {
    ADT_UInt16 channelId;        // channel identifier
    chanInfo_t *pChan;           // pointer to Channel structure

    // Get the Channel Id from the command message.
    channelId = (pCmd[0] ) & 0xFF;

    // Prepare the reply message.
    pReply[0] |= (MSG_CONFIGURE_ED137B_PERSISTENT_FEATURES_UPDATE_PARMS_REPLY << 8);
    pReply[1] = channelId << 8;

    // Verify the Channel Id is valid.
    if (channelId >= sysConfig.maxNumChannels)
    {
      pReply[1] |= (ED137B_InvalidChannel & 0xFF);
      return 2;
    }
    pChan = chanTable[channelId];

    // Check to see if a Previous Command is Pending
    if((pChan->ED137TxState.TxState & TX_UPDATE_PF_MSG) && (pChan->channelType != inactive))
    {
        pReply[1] |= (ED137B_PendingCommand & 0xFF);
        return 2;
    }

    // Set the ED137B Configuration
    SWI_disable();
    pChan->ED137B_PersistentFeature.PersistentFeature_Type  = pCmd[1];
    pChan->ED137B_PersistentFeature.PersistentFeature_Value = pCmd[2];
    pChan->ED137TxState.TxState = pChan->ED137TxState.TxState | TX_UPDATE_PF_MSG;
    SWI_enable();

    pReply[1] |= (ED137B_Success & 0xFF);
    return 3;
}

#pragma CODE_SECTION (ProcNonPersistentFeatureMsg, "SLOW_PROG_SECT")
static ADT_UInt16 ProcNonPersistentFeatureMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {
    ADT_UInt16 channelId;        // channel identifier
    chanInfo_t *pChan;           // pointer to Channel structure
    ADT_UInt16 NPF_value_cnt, NPF_Cnt ,NPF_index = 2;

    // Get the Channel Id from the command message.
    channelId = (pCmd[0] ) & 0xFF;

    // Prepare the reply message.
    pReply[0] |= (MSG_CONFIGURE_ED137B_NON_PERSISTENT_FEATURES_PARMS_REPLY << 8);
    pReply[1] = channelId << 8;

    // Verify the Channel Id is valid.
    if (channelId >= sysConfig.maxNumChannels)
    {
      pReply[1] |= (ED137B_InvalidChannel & 0xFF);
      return 2;
    }
    pChan = chanTable[channelId];

    // Check to see if a Previous Command is Pending
    if((pChan->ED137TxState.TxState & TX_NON_PF_MSG) && (pChan->channelType != inactive))
    {
        pReply[1] |= (ED137B_PendingCommand & 0xFF);
        return 2;
    }

    // Set the ED137B Configuration
    SWI_disable();

    pChan->ED137B_NPF.Num_NPF =   pCmd[1];
    for(NPF_Cnt = 0; NPF_Cnt < pChan->ED137B_NPF.Num_NPF; NPF_Cnt++)
    {
        pChan->ED137B_NPF.ED137B_NONPersistentFeatureParms[NPF_Cnt].NONPersistentFeature_Type     =   pCmd[NPF_index];
        NPF_index++;
        pChan->ED137B_NPF.ED137B_NONPersistentFeatureParms[NPF_Cnt].NONPersistentFeature_Length   =   pCmd[NPF_index];
        NPF_index++;
        for(NPF_value_cnt = 0; NPF_value_cnt < pChan->ED137B_NPF.ED137B_NONPersistentFeatureParms[NPF_Cnt].NONPersistentFeature_Length;NPF_value_cnt++)
    {
            pChan->ED137B_NPF.ED137B_NONPersistentFeatureParms[NPF_Cnt].NONPersistentFeature_Value[NPF_value_cnt] = pCmd[NPF_value_cnt+NPF_index];
        }
        NPF_index += pChan->ED137B_NPF.ED137B_NONPersistentFeatureParms[NPF_Cnt].NONPersistentFeature_Length;
    }
    pChan->ED137TxState.TxState = pChan->ED137TxState.TxState | TX_NON_PF_MSG;
    SWI_enable();

    pReply[1] |= (ED137B_Success & 0xFF);
    return 3;
}


#pragma CODE_SECTION (ProcED137B_GateAudioFeatureFeatureMsg, "SLOW_PROG_SECT")
static ADT_UInt16 ProcED137B_GateAudioFeatureFeatureMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {
    ADT_UInt16 channelId;        // channel identifier
    chanInfo_t *pChan;           // pointer to Channel structure

    // Get the Channel Id from the command message.
    channelId = (pCmd[0] ) & 0xFF;

    // Prepare the reply message.
    pReply[0] |= (MSG_CONFIGURE_ED137B_GATE_AUDIO_FEATURE_PARMS_REPLY << 8);
    pReply[1] = channelId << 8;

    // Verify the Channel Id is valid.
    if (channelId >= sysConfig.maxNumChannels)
    {
      pReply[1] |= (ED137B_InvalidChannel & 0xFF);
      return 2;
    }
    pChan = chanTable[channelId];

    // Check to see if a Previous Command is Pending
    if((pChan->ED137TxState.TxState & TX_NON_PF_MSG) && (pChan->channelType != inactive))
    {
        pReply[1] |= (ED137B_PendingCommand & 0xFF);
        return 2;
    }

    // Set the ED137B Configuration
    SWI_disable();
    pChan->ED137B_GateAudioFeature.Enable_Mask =   pCmd[1];
    SWI_enable();

    pReply[1] |= (ED137B_Success & 0xFF);
    return 3;
}
// AEC status and state messaging
#if (AEC_LIB_VERSION >= 0x0420)

extern ADT_UInt8 aecStateBufr[];        // AEC State buffer
extern ADT_UInt32 aecStateBufrSize;     // AEC State buffer size (bytes)
ADT_UInt16 aecStateDataLen;		        // AEC State's data length (bytes)

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Process Read AEC Status message.
//
// Returns: The number of bytes in the reply message.
//
#pragma CODE_SECTION (ProcReadAecStatusMsg, "SLOW_PROG_SECT")
static ADT_UInt16 ProcReadAecStatusMsg(ADT_UInt16 *pCmd, ADT_UInt16 *pReply)
{
   ADT_UInt16 ChannelId;        // channel identifier
   chanInfo_t *pChan;           // pointer to Channel structure
   IAECG4_Status aecStatus;     // AEC status

   // Get the Channel Id from the command message.
   ChannelId = (pCmd[1] >> 8) & 0xFF;

   // Prepare the reply message.
   pReply[0] |= (MSG_READ_AEC_STATUS_REPLY << 8);
   pReply[1] = ChannelId << 8;

   // Verify the Channel Id is valid.
   if (ChannelId >= sysConfig.maxNumChannels)
   {
      pReply[1] |= (aecChanRead_InvalidChannel & 0xFF);
      return 4;
   }

   // Verify the Channel is active.
   pChan = chanTable[ChannelId];
   if (pChan->channelType == inactive) 
   {
      pReply[1] |= (aecChanRead_InactiveChannel & 0xFF);
      return 4;
   }

   // Verify the AEC is enabled.
   if (pChan->pcmToPkt.AECEchoCancel != Enabled)
   {
      pReply[1] |= (aecChanRead_NotEnabled & 0xFF);
      return 4;
   }

   // Get the AEC Status from the AEC algorithm.
   SWI_disable();
   AECG4_ADT_control((IALG_Handle) pChan->pcmToPkt.pG168Chan, IAECG4_GETSTATUS,
                     (IALG_Status *) &aecStatus);
   SWI_enable();

   // Store the AEC Status information in the reply message.
   pReply[2] = (ADT_UInt16) aecStatus.txInPowerdBm10;
   pReply[3] = (ADT_UInt16) aecStatus.txOutPowerdBm10;
   pReply[4] = (ADT_UInt16) aecStatus.rxInPowerdBm10;
   pReply[5] = (ADT_UInt16) aecStatus.rxOutPowerdBm10;
   pReply[6] = (ADT_UInt16) aecStatus.residualPowerdBm10;
   pReply[7] = (ADT_UInt16) aecStatus.erlDirectdB10;
   pReply[8] = (ADT_UInt16) aecStatus.erlIndirectdB10;
   pReply[9] = (ADT_UInt16) aecStatus.erldB10BestEstimate;
   pReply[10] = (ADT_UInt16) aecStatus.worstPerBinERLdB10BestEstimate;
   pReply[11] = (ADT_UInt16) aecStatus.erledB10;
   pReply[12] = (ADT_UInt16) aecStatus.shortTermERLEdB10;
   pReply[13] = (ADT_UInt16) aecStatus.shadowERLEdB10;
   pReply[14] = (ADT_UInt16) aecStatus.rxVADState;
   pReply[15] = (ADT_UInt16) aecStatus.txVADState;
   pReply[16] = (ADT_UInt16) aecStatus.rxVADStateLatched;
   pReply[17] = (ADT_UInt16) aecStatus.currentBulkDelaySamples;
   pReply[18] = (ADT_UInt16) aecStatus.txAttenuationdB10;
   pReply[19] = (ADT_UInt16) aecStatus.rxAttenuationdB10;
   pReply[20] = (ADT_UInt16) aecStatus.rxOutAttenuationdB10;
   pReply[21] = (ADT_UInt16) aecStatus.nlpThresholddB10;
   pReply[22] = (ADT_UInt16) aecStatus.nlpSaturateFlag;
   pReply[23] = (ADT_UInt16) aecStatus.aecState;
   pReply[24] = (ADT_UInt16) aecStatus.sbcngResidualPowerdBm10;
   pReply[25] = (ADT_UInt16) aecStatus.sbcngCNGPowerdBm10;
   pReply[26] = (ADT_UInt16) aecStatus.rxOutAttendB10;
   pReply[27] = (ADT_UInt16) aecStatus.sbMaxAttendB10;
   pReply[28] = (ADT_UInt16) aecStatus.sbMaxClipLeveldBm10;

#if (AEC_LIB_VERSION >= 0x0430)

   pReply[29] = (ADT_UInt16) aecStatus.instantaneousERLEdB100;
   pReply[30] = (ADT_UInt16) aecStatus.dynamicNLPAggressivenessAdjustdB10;

   pReply[1] |= (aecChanRead_Success & 0xFF);
   return 62;

#else

   pReply[1] |= (aecChanRead_Success & 0xFF);
   return 58;

#endif

}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Process Save AEC State message.
//
// Returns: The number of bytes in the reply message.
//
#pragma CODE_SECTION (ProcSaveAecStateMsg, "SLOW_PROG_SECT")
static ADT_UInt16 ProcSaveAecStateMsg(ADT_UInt16 *pCmd, ADT_UInt16 *pReply)
{
   ADT_UInt16 channelId;        // channel identifier
   ADT_UInt16 blockOffset;      // block offset
   ADT_UInt16 blockLength;      // block length (bytes)
   chanInfo_t *pChan;           // pointer to Channel structure
   ADT_Int32 aecStateLength;    // AEC State length (bytes)
   ADT_UInt16 remainingLength;  // remaining length to transfer to host

   // Get the Channel Id and block info from the command message.
   channelId = (pCmd[1] >> 8) & 0xFF;
   blockOffset = pCmd[2];
   blockLength = pCmd[3];

   // Initialize the reply message.
   pReply[0] |= (MSG_SAVE_AEC_STATE_REPLY << 8);
   pReply[1] = channelId << 8;
   pReply[2] = 0;
   pReply[3] = blockOffset;
   pReply[4] = 0;

   // Verify the Channel Id is valid.
   if (channelId >= sysConfig.maxNumChannels)
   {
      pReply[1] |= (aecChanRead_InvalidChannel & 0xFF);
      return 10;
   }

   // Verify the Channel is active.
   pChan = chanTable[channelId];
   if (pChan->channelType == inactive) 
   {
      pReply[1] |= (aecChanRead_InactiveChannel & 0xFF);
      return 10;
   }

   // Verify the AEC is enabled.
   if (pChan->pcmToPkt.AECEchoCancel != Enabled)
   {
      pReply[1] |= (aecChanRead_NotEnabled & 0xFF);
      return 10;
   }

   // Put a block of AEC State data in the reply message.
   if (blockOffset == 0)
   {
      SWI_disable();
      aecStateLength =
         AECG4_ADT_saveRestoreState((IAECG4_Handle) pChan->pcmToPkt.pG168Chan,
                                    (XDAS_Int8 *) aecStateBufr, aecStateBufrSize,
                                    SAVE_RESTORE_ACTION_GET_LENGTH);
      if (aecStateLength <= aecStateBufrSize)
      {
         AECG4_ADT_saveRestoreState((IAECG4_Handle) pChan->pcmToPkt.pG168Chan,
                                    (XDAS_Int8 *) aecStateBufr, aecStateBufrSize,
                                    SAVE_RESTORE_ACTION_SAVE);
         SWI_enable();
         aecStateDataLen = (ADT_UInt16) aecStateLength;
      }
      else
      {
         SWI_enable();
         pReply[1] |= (aecChanRead_InactiveChannel & 0xFF);
         return 10;
      }
   }
   pReply[2] = aecStateDataLen;
   if (blockOffset < aecStateDataLen)
   {
      remainingLength = aecStateDataLen - blockOffset;
      if (blockLength > remainingLength)
      {
         blockLength = remainingLength;
      }
      pReply[4] = blockLength;
      memcpy(&(pReply[5]), &(aecStateBufr[blockOffset]), blockLength);
   }
   pReply[1] |= (aecChanRead_Success & 0xFF);
   return (10 + blockLength);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Process Restore AEC State message.
//
// Returns: The number of bytes in the reply message.
//
#pragma CODE_SECTION (ProcRestoreAecStateMsg, "SLOW_PROG_SECT")
static ADT_UInt16 ProcRestoreAecStateMsg(ADT_UInt16 *pCmd, ADT_UInt16 *pReply)
{
   ADT_UInt16 channelId;        // channel identifier
   ADT_UInt16 stateLength;      // length of AEC State data (bytes)
   ADT_UInt16 blockOffset;      // block offset
   ADT_UInt16 blockLength;      // block length (bytes)
   chanInfo_t *pChan;           // pointer to Channel structure

   // Get the Channel Id and block info from the command message.
   channelId = (pCmd[1] >> 8) & 0xFF;
   stateLength = pCmd[2];
   blockOffset = pCmd[3];
   blockLength = pCmd[4];

   // Initialize the reply message.
   pReply[0] |= (MSG_RESTORE_AEC_STATE_REPLY << 8);
   pReply[1] = channelId << 8;
   pReply[2] = stateLength;
   pReply[3] = blockOffset;
   pReply[4] = blockLength;

   // Verify the Channel Id is valid.
   if (channelId >= sysConfig.maxNumChannels)
   {
      pReply[1] |= (aecChanRead_InvalidChannel & 0xFF);
      return 10;
   }

   // Verify the Channel is active.
   pChan = chanTable[channelId];
   if (pChan->channelType == inactive) 
   {
      pReply[1] |= (aecChanRead_InactiveChannel & 0xFF);
      return 10;
   }

   // Verify the AEC is enabled.
   if (pChan->pcmToPkt.AECEchoCancel != Enabled)
   {
      pReply[1] |= (aecChanRead_NotEnabled & 0xFF);
      return 10;
   }

   // Verify the specified state length fits in the AEC State buffer.
   if ((stateLength > aecStateBufrSize) ||
       ((blockOffset + blockLength) > aecStateBufrSize))
   {
      pReply[1] |= (aecChanRead_NotEnabled & 0xFF);
      return 10;
   }

   // Store the block of state data in the AEC State buffer and restore it to
   // the AEC if all state data was received.
   memcpy(&(aecStateBufr[blockOffset]), &(pReply[5]), blockLength);
   if ((blockOffset + blockLength) >= stateLength)
   {
      SWI_disable();
      AECG4_ADT_saveRestoreState((IAECG4_Handle) pChan->pcmToPkt.pG168Chan,
                                 (XDAS_Int8 *) aecStateBufr, stateLength,
                                 SAVE_RESTORE_ACTION_RESTORE);
      SWI_enable();
   }

   pReply[1] |= (aecChanRead_Success & 0xFF);
   return 10;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Process AEC Gain Change message.
//
// Returns: The number of bytes in the reply message.
//
#pragma CODE_SECTION (ProcAecGainChangeMsg, "SLOW_PROG_SECT")
static ADT_UInt16 ProcAecGainChangeMsg(ADT_UInt16 *pCmd, ADT_UInt16 *pReply)
{
   ADT_UInt16 ChannelId;        // channel identifier
   chanInfo_t *pChan;           // pointer to Channel structure

   // Get the Channel Id from the command message.
   ChannelId = (pCmd[1] >> 8) & 0xFF;

   // Prepare the reply message.
   pReply[0] |= (MSG_AEC_GAIN_CHANGE_REPLY << 8);
   pReply[1] = ChannelId << 8;

   // Verify the Channel Id is valid.
   if (ChannelId >= sysConfig.maxNumChannels)
   {
      pReply[1] |= (aecChanRead_InvalidChannel & 0xFF);
      return 4;
   }

   // Verify the Channel is active.
   pChan = chanTable[ChannelId];
   if (pChan->channelType == inactive) 
   {
      pReply[1] |= (aecChanRead_InactiveChannel & 0xFF);
      return 4;
   }

   // Verify the AEC is enabled.
   if (pChan->pcmToPkt.AECEchoCancel != Enabled)
   {
      pReply[1] |= (aecChanRead_NotEnabled & 0xFF);
      return 4;
   }

// $$$ TBD

   pReply[1] |= (aecChanRead_Success & 0xFF);
   return 4;
}

#endif


#pragma CODE_SECTION (ProcAlgControlMsg, "SLOW_PROG_SECT")
static ADT_UInt16 ProcAlgControlMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) { 

   ADT_UInt16      ChannelId;      // channel identifier
   GpakAlgCtrl_t   ControlCode;    // algorithm control code
   GpakToneTypes   ActTonetype;    // tone detector type to activate
   GpakToneTypes   DeactTonetype;  // tone detector type to deactivate
   chanInfo_t      *pChan;        // pointer to Channel structure
   GpakDeviceSide_t AorB;
   ADT_UInt16      NLP,i,curr,next, mask;
   GpakToneTypes NextTone, CurrTone;
   ADT_Int16       gainVal;
   GpakCodecs      CodecType;
   // Get the Channel Id and Control Code from the command message.
   ChannelId = (pCmd[1] >> 8) & 0xFF;
   ControlCode = (GpakAlgCtrl_t) (pCmd[2] & 0xFF);
   NLP  = (pCmd[2] >> 8)  & 0xF;
   AorB = (GpakDeviceSide_t) ((pCmd[2] >> 12) & 0x1);
   DeactTonetype =  (GpakToneTypes) pCmd[3];
   ActTonetype   =  (GpakToneTypes) pCmd[4];
   CodecType     =  (GpakCodecs) pCmd[6];

   // Prepare the reply message.
   pReply[0] |= (MSG_ALG_CONTROL_REPLY << 8);
   pReply[1]  = (ChannelId << 8);

   // Verify the Channel Id is valid
   if (ChannelId >= sysConfig.maxNumChannels)
      return Ac_InvalidChannel;

   // Verify that the Channel is active
   pChan = chanTable[ChannelId];
   if (pChan->channelType == inactive) 
      return Ac_InactiveChannel;

   // Verify that the previous control command was processed
   if (pChan->algCtrl.ControlCode != ALG_CTRL_VOID_CMD)
      return Ac_PrevCmdPending;

   // validate the control code
   switch (ControlCode) {
   // Echo canceller control
   case EnableEcan: case BypassEcan: case ResetEcan: 
   case EnableEcanAdapt: case BypassEcanAdapt: case UpdateEcanNLP:            
      if (((AorB == ADevice) && !pChan->pcmToPkt.EchoCancel) ||
          ((AorB == BDevice) && !pChan->pktToPcm.EchoCancel))
          return Ac_ECNotEnabled;
      if ((ControlCode == UpdateEcanNLP) && !ValidNLP(NLP))
          return Ac_ECInvalidNLP;
      break;

   // Enable / Disable algorithms
   case EnableVad: case BypassVad: case EnableVAGC: case BypassVAGC:
   case RTPTransmitEnable:
   case RTPTransmitDisable:
   case RTPReceiveEnable:
   case RTPReceiveDisable:
   case CID2RXRestart:
   case CID1RXRestart:
   case SetCodecType:
      break;

   // Tone suppression control
  case EnableToneSuppress: case BypassToneSuppress: 
      if (AorB == ADevice) {
         if ((pChan->pcmToPkt.toneTypes & DTMF_tone) == 0) 
            return Ac_DetectorNotCfgrd;
      } else {
         if ((pChan->pktToPcm.toneTypes & DTMF_tone) == 0) 
            return Ac_DetectorNotCfgrd;
      }
      break;

   // Tone detection instantiation
   case ModifyToneDetector:
      // Verify the tonetype
      mask = (ADT_UInt16) ~(ToneDetectAll | Tone_Squelch | Notify_Host);
      if ((DeactTonetype & mask) != 0)
          return Ac_InvalidTone;
      if ((ActTonetype & mask) != 0)
          return Ac_InvalidTone;

      // pcmPkt only supports A-Side
      mask = ~mask;
      if ((AorB == BDevice) && (pChan->channelType == pcmToPacket) &&  (ActTonetype & mask))
          return Ac_InvalidTone;

      // verify that the number of detectors selected falls within the
      // configuration range
      if (AorB == ADevice) {                       
         CurrTone = pChan->pcmToPkt.toneTypes & ToneDetect;
         NextTone = ((pChan->pcmToPkt.toneTypes & (~DeactTonetype)) | ActTonetype) & ToneDetect;
         for (i=0,curr=0, next=0; i<16; i++) {
            if (NextTone & (1<<i)) next++;
            if (CurrTone & (1<<i)) curr++;                    
         }
         if (next > sysConfig.maxToneDetTypes)
            return Ac_TooManyActiveDetectorsA; 
      } else {
         CurrTone = pChan->pktToPcm.toneTypes & ToneDetect;
         NextTone = ((pChan->pktToPcm.toneTypes & (~DeactTonetype)) | ActTonetype) & ToneDetect;
         for (i=0,curr=0, next=0; i<16; i++) {
            if (NextTone & (1<<i)) next++;
            if (CurrTone & (1<<i)) curr++;                    
         }
         if (next > sysConfig.maxToneDetTypes)
             return Ac_TooManyActiveDetectorsB; 
      }

      // verify there are enough detectors in the pool. Any currently
      // allocated  detectors will be freed prior to allocating the next detectors.
      if (next > (ToneDetectorsAvail + curr))
          return Ac_InsuffDetectorsAvail;
      if ((ActTonetype & CED_tone)       && (CedDetAvail == 0))                   return Ac_InsuffDetectorsAvail;
      if ((ActTonetype & CNG_tone)       && (CngDetAvail == 0))                   return Ac_InsuffDetectorsAvail;

      // verify the selected detectors have been configured 
      if ((ActTonetype & DTMF_tone)      && (SysAlgs.dtmfEnable == 0))      return Ac_DetectorNotCfgrd;
      if ((ActTonetype & MFR1_tone)      && (SysAlgs.mfr1Enable == 0))      return Ac_DetectorNotCfgrd;
      if ((ActTonetype & MFR2Fwd_tone)   && (SysAlgs.mfr2FwdEnable == 0))   return Ac_DetectorNotCfgrd;
      if ((ActTonetype & MFR2Back_tone)  && (SysAlgs.mfr2BackEnable == 0))  return Ac_DetectorNotCfgrd;
      if ((ActTonetype & CallProg_tone)  && (SysAlgs.cprgEnable == 0))      return Ac_DetectorNotCfgrd;
      break;

   // Gain settings
   case UpdateTdmOutGain: 
   case UpdateTdmInGain:
      gainVal = pCmd[5];

      if (gainVal != (ADT_Int16)ADT_GAIN_MUTE) {
            if (gainVal > 40)  gainVal = 40;
            if (gainVal < -40) gainVal = -40;
            gainVal *= 10;  // scale to 0.1 dB units
      }
      break;
     
   default:
      return Ac_InvalidCode;
    };

   // load the channel algorithm control: framing task will perform the update
   pChan->algCtrl.CodecType        = CodecType;
   pChan->algCtrl.DeactTonetype    = DeactTonetype;
   pChan->algCtrl.ActTonetype      = ActTonetype;
   pChan->algCtrl.NLP              = NLP;
   pChan->algCtrl.AorB             = AorB;
   pChan->algCtrl.Gain             = gainVal;
   pChan->algCtrl.ControlCode      = ControlCode; // always write this last!!
 
   return Ac_Success;
}

#pragma CODE_SECTION (ProcReadAgcPowerMsg, "SLOW_PROG_SECT")
static ADT_UInt16 ProcReadAgcPowerMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) { 
   ADT_UInt16      ChannelId;      // channel identifier
   chanInfo_t      *pChan;          // pointer to Channel structure

   // Get the Channel Id and Control Code from the command message.
   ChannelId = (pCmd[1] >> 8) & 0xFF;

   // Prepare the reply message.
   pReply[0] |= (MSG_READ_AGC_POWER_REPLY << 8);
   pReply[1]  = (ChannelId << 8);

   // default power level indicates disabled instance
   pReply[2] = 0xFF9D;    // -99 dBm
   pReply[3] = 0xFF9D;    // -99 dBm

   // Verify the Channel Id is valid
   if (ChannelId >= sysConfig.maxNumChannels)
      return (Ar_InvalidChannel);

    // Verify that the Channel is active
   pChan = chanTable[ChannelId];
   if (pChan->channelType == inactive) 
      return Ar_InactiveChannel;

   // Verify at least one instance of AGC is running 
   if ((pChan->pcmToPkt.AGC == Disabled) && (pChan->pktToPcm.AGC == Disabled))
      return Ar_AGCNotEnabled;

   // Copy the AGC power from A side if enabled
   if (pChan->pcmToPkt.AGC == Enabled)
      pReply[2] = pChan->pcmToPkt.AGCPower;

   // Copy the AGC power from B side if enabled
   if (pChan->pktToPcm.AGC == Enabled)
      pReply[3] = pChan->pktToPcm.AGCPower;

   return Ar_Success;

}

#pragma CODE_SECTION (ProcResetChanStatsMsg, "SLOW_PROG_SECT")
static ADT_UInt16 ProcResetChanStatsMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) { 
   ADT_UInt16      ChannelId;      // channel identifier
   chanInfo_t      *pChan;          // pointer to Channel structure
   int             pktReset;

   // Get the Channel Id and Control Code from the command message.
   ChannelId = (pCmd[1] >> 8) & 0xFF;

   // Prepare the reply message.
   pReply[0] |= (MSG_RESET_CHAN_STATS_REPLY << 8);
   pReply[1]  = ChannelId;

   // Verify the Channel Id is valid
   if (ChannelId >= sysConfig.maxNumChannels) {
      return 0xFF00;
   }

   // Verify that the Channel is active
   pChan = chanTable[ChannelId];
   if (pChan->channelType == inactive) {
      return 0xFF00;
   }


   if (pChan->algCtrl.ControlCode != ALG_CTRL_VOID_CMD)
      return 0xFF00;

    // Reset the selected statistics
   pktReset = pCmd[2] & 1;
   if (pktReset) 
      pChan->algCtrl.ControlCode = ResetChanStats;

   return 0;
}

#pragma CODE_SECTION (ProcResetSysStatsMsg, "SLOW_PROG_SECT")
static ADT_UInt16 ProcResetSysStatsMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) { 
   int framingReset;
   int cpuUsageReset;

   // Prepare the reply message.
   pReply[0] |= (MSG_RESET_SYS_STATS_REPLY << 8);

   framingReset  = pCmd[2] & 1;
   cpuUsageReset = pCmd[2] & 2;

   if (framingReset) 
      ResetMcBspStats();

   if (cpuUsageReset)
      gblPkLoadingRst = (1 << DSPTotalCores) - 1;

   return 0;
}
 
#pragma CODE_SECTION (ProcMcBspStatsMsg, "SLOW_PROG_SECT")
static ADT_UInt16 ProcMcBspStatsMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) { 
   PortStat_t ps;

    // Prepare the reply message.
    pReply[0] |= (MSG_READ_MCBSP_STATS_REPLY << 8);
    pReply[1] = 0;

    ReadPortStats(1, &ps);
    pReply[2]  = ps.Status;
    pReply[3]  = (ADT_UInt16) ((ps.RxIntCount) >> 16);
    pReply[4]  = (ADT_UInt16) ps.RxIntCount;
    pReply[5]  = ps.RxSlips;                                    
    pReply[6]  = ps.RxDmaErrors;                                
    pReply[7]  = (ADT_UInt16) ((ps.TxIntCount) >> 16);
    pReply[8]  = (ADT_UInt16) ps.TxIntCount;
    pReply[9]  = ps.TxSlips;
    pReply[10] = ps.TxDmaErrors;
    pReply[11] = ps.FrameSyncErrors;
    pReply[12] = ps.RestartCount;

    ReadPortStats(2, &ps);
    pReply[13]  = ps.Status;
    pReply[14]  = (ADT_UInt16) ((ps.RxIntCount) >> 16);
    pReply[15]  = (ADT_UInt16) ps.RxIntCount;
    pReply[16]  = ps.RxSlips;                                    
    pReply[17]  = ps.RxDmaErrors;                                
    pReply[18]  = (ADT_UInt16) ((ps.TxIntCount) >> 16);
    pReply[19]  = (ADT_UInt16) ps.TxIntCount;
    pReply[20]  = ps.TxSlips;
    pReply[21]  = ps.TxDmaErrors;
    pReply[22]  = ps.FrameSyncErrors;
    pReply[23]  = ps.RestartCount;

    ReadPortStats(3, &ps);
    pReply[24]  = ps.Status;
    pReply[25]  = (ADT_UInt16) ((ps.RxIntCount) >> 16);
    pReply[26]  = (ADT_UInt16) ps.RxIntCount;
    pReply[27]  = ps.RxSlips;                                    
    pReply[28]  = ps.RxDmaErrors;                                
    pReply[29]  = (ADT_UInt16) ((ps.TxIntCount) >> 16);
    pReply[30]  = (ADT_UInt16) ps.TxIntCount;
    pReply[31]  = ps.TxSlips;
    pReply[32]  = ps.TxDmaErrors;
    pReply[33]  = ps.FrameSyncErrors;
    pReply[34]  = ps.RestartCount;

    return 35;
}

#pragma CODE_SECTION (ProcConfigArbToneDetMsg, "SLOW_PROG_SECT")
static GPAK_ConfigArbToneStat_t ProcConfigArbToneDetMsg (ADT_UInt16 *pCmd) {
   ADT_UInt16 CfgId;
   ADT_UInt16 i, j, found, toneIdx;
   ADT_Int16  lastFreq, cnt;

   ARBIT_ADT_Param_t parms;
   ADT_Int16 freq[NUM_DISTINCT_FREQS];
   Tones_Index_t tones;

    CfgId = pCmd[1];
    // Verify the configuration Id
    if (CfgId >= numArbToneCfgs)
        return Cat_InvalidID;

    // extract the parameters
    parms.structSize       =  sizeof(parms);
    parms.num_Distinct_Freqs =  (ADT_Int16) Byte1 (pCmd[2]);
    parms.num_Tones          =  (ADT_Int16) Byte0 (pCmd[2]);
   
    parms.min_Power          =  (ADT_Int16) Byte1 (pCmd[3]);
    parms.max_Freg_Deviation =  (ADT_Int16) Byte0 (pCmd[3]);

    // Verify the number of frequencies and tones
    if (parms.num_Distinct_Freqs > NUM_DISTINCT_FREQS)
        return Cat_InvalidNumFreqs;
    if (parms.num_Tones > NUM_TONES)
        return Cat_InvalidNumTones;

    // extract and validate the frequencies
    // Frequencies must be in ascending order
    pCmd += 4;
    memset(freq, 0, sizeof(freq));
    for (i=0, lastFreq=0, cnt=0; i<NUM_DISTINCT_FREQS; i++) {
        freq[i] = (ADT_Int16) pCmd[i];
        if (freq[i] == 0) break;
       
        cnt++;
        if (freq[i] < lastFreq) return Cat_InvalidFreqs;
        lastFreq = freq[i];
    }
    if (cnt != parms.num_Distinct_Freqs)
        return Cat_InvalidFreqs;

    // extract the frequencies and indexes of the tone
    pCmd += 32;
    memset(&tones, 0, sizeof(tones));
    for (i=0, cnt=0; i<parms.num_Tones; i++, cnt++) {
        tones.ToneInfo[i][0] = pCmd[3*i];       // freq 1
        tones.ToneInfo[i][1] = pCmd[3*i+1];     // freq 2
        toneIdx = pCmd[3*i+2];     // tone index

        // break if both frequencies are 0
        if (!tones.ToneInfo[i][0] && !tones.ToneInfo[i][1]) break;

        // Tone index must be between 101 and 116 inclusive
        if ((toneIdx < 101) || (116 < toneIdx))
           return Cat_InvalidIndex;

        // ensure f1 is non-zero
        if (tones.ToneInfo[i][0] == 0) {
            tones.ToneInfo[i][0] = tones.ToneInfo[i][1];
            tones.ToneInfo[i][1] = 0;
        }

        // verify that f1 and f2 are among the distinct frequencies
        if (tones.ToneInfo[i][0]) {
            found = 0;
            for (j=0; j<parms.num_Distinct_Freqs; j++) {
               if (tones.ToneInfo[i][0] != freq[j]) continue;
               found = 1;
               break;
            }
            if (!found) return Cat_InvalidTones;
        }
        if (tones.ToneInfo[i][1]) {
            found = 0;
            for (j=0; j<parms.num_Distinct_Freqs; j++) {
               if (tones.ToneInfo[i][1] != freq[j]) continue;
               found = 1;
               break;
            }
            if (!found) return Cat_InvalidTones;
        }

        tones.ToneInfo[i][2] = toneIdx - 101; // Adjust G.PAK range to tone detector range
    }
    if (cnt != parms.num_Tones)
        return Cat_InvalidNumTones;


    // Configure the detector group instance.
    CLEAR_INST_CACHE (ArbToneCfgInst[CfgId], sizeof (ArbitDtInstance_t));

    ARBIT_ADT_config (ArbToneCfgInst[CfgId], &tones, &freq[0], &parms);
    ArbToneConfigured[CfgId] = 1;
    activeArbToneCfg = CfgId;

    FLUSH_INST_CACHE (ArbToneCfgInst[CfgId], sizeof (ArbitDtInstance_t));
    FLUSH_wait ();
    return Cat_Success;
}

#pragma CODE_SECTION (ProcActiveArbToneDetMsg, "SLOW_PROG_SECT")
static GPAK_ActiveArbToneStat_t ProcActiveArbToneDetMsg (ADT_UInt16 *pCmd) {
 ADT_UInt16 CfgId;

    CfgId = pCmd[1];

    // Verify the configuration Id
    if (CfgId >= numArbToneCfgs)
        return Aat_InvalidID;

    // Verify that the detector has been configured
    if (ArbToneConfigured[CfgId] == 0) 
        return Aat_NotConfigured;

    activeArbToneCfg = CfgId;
 
    return Aat_Success;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ProcWriteSpeexParamsMsg - Process a Write Speex Parameters message.
//  Returns reply status 
#pragma CODE_SECTION (ProcWriteSpeexParamsMsg, "SLOW_PROG_SECT")
static GPAK_SpeexParamsStat_t ProcWriteSpeexParamsMsg (ADT_Int16 *pCmd, ADT_UInt16 *pReply) {

   ADT_Bool  variableRate, perceptualEnhancement;
   ADT_UInt8 quality, complexity; 

   // Prepare the reply message.
   pReply[0] = (MSG_WRITE_SPEEX_PARAMS_REPLY << 8);

   variableRate          = pCmd[0] & 1;
   perceptualEnhancement = pCmd[0] & 2;

   complexity = Byte0 (pCmd[1]);
   quality    = Byte1 (pCmd[1]);

   if (complexity < 1 || 10 < complexity)
      return SpxInvalidComplexity;

   if (10 < quality)
      return SpxInvalidQuality;

   SpeexDflt.Quality    = quality;
   SpeexDflt.Complexity = complexity;
   SpeexDflt.DecodeEnhancementEnable = (perceptualEnhancement != 0);
   SpeexDflt.VBREnable  = (variableRate != 0);
   SpeexDflt.VBRQuality = (float) quality;
   return SpxSuccess;
}

//=====================================================
//{  SRTP configuration message processing
//
//   Called by messaging task upon receipt of SRTP configuration message
//
//
//  Word     Bits
//Msg[0]     0xff00    - Command tag
//Msg[0]     0x00ff    - Commnand ID

//Msg[1]     0xffff    - Encode.EncryptionScheme
//Msg[2]     0xffff    - Encode.AuthenticationScheme

//Msg[3]     0xffff    - Encode.AuthenticationI8
//Msg[4]     0xff00    - Encode.MasterKey[0]
//Msg[4]     0x00ff    - Encode.MasterKey[1]
//Msg[5]     0xff00    - Encode.MasterKey[2]
//Msg[5]     0x00ff    - Encode.MasterKey[3]
//Msg[6]     0xff00    - Encode.MasterKey[4]
//Msg[6]     0x00ff    - Encode.MasterKey[5]
//Msg[7]     0xff00    - Encode.MasterKey[6]
//Msg[7]     0x00ff    - Encode.MasterKey[7]
//Msg[8]     0xff00    - Encode.MasterKey[8]
//Msg[8]     0x00ff    - Encode.MasterKey[9]
//Msg[9]     0xff00    - Encode.MasterKey[10]
//Msg[9]     0x00ff    - Encode.MasterKey[11]
//Msg[10]     0xff00    - Encode.MasterKey[12]
//Msg[10]     0x00ff    - Encode.MasterKey[13]
//Msg[11]     0xff00    - Encode.MasterKey[14]
//Msg[11]     0x00ff    - Encode.MasterKey[15]
//}
typedef struct SRTPEventFifoMsg_t {
    EventFifoHdr_t  header;    
    ADT_UInt16      payload[EVENT_FIFO_SRTPNEWKEY_MSGLEN_BYTES/2];
} SRTPEventFifoMsg_t;

//  SRTP callback to request new key.
#pragma CODE_SECTION (ProcConfigSRTPMsg, "SLOW_PROG_SECT")
static ADT_Bool srtpCallbackProvideKey (void *pAppHandle,  SrtpKeyInfo_t *pKeyInfo) {
    GpakSrtpKeyInfo_t *srtpKeyInfoPtr = (GpakSrtpKeyInfo_t *)pAppHandle;

   SRTPEventFifoMsg_t msg;

   CLEAR_INST_CACHE (srtpKeyInfoPtr, sizeof (GpakSrtpKeyInfo_t));
   FLUSH_wait ();

   if (srtpKeyInfoPtr->NewKeyFlag == 0) {
      if ((pKeyInfo->LookAhead) && (srtpKeyInfoPtr->Direction == NetToDSP))
          pKeyInfo->u.Mki.MkiValue = 0;
      // send msg to host informing it that the dsp is in need of new Key
      msg.header.eventCode = EventNeedSRTPMasterKey;
      msg.header.channelId = srtpKeyInfoPtr->channelId;
      msg.header.deviceSide = srtpKeyInfoPtr->Direction; //either DSPToNet(TX), or NetToDSP(RX)
      msg.header.eventLength = EVENT_FIFO_SRTPNEWKEY_MSGLEN_BYTES; //(6*2);
      msg.payload[0] = (ADT_UInt16) (pKeyInfo->pkt.srtp.roc & 0xFFFF);
      msg.payload[1] = (ADT_UInt16) ((pKeyInfo->pkt.srtp.roc >> 16) & 0xFFFF);
      msg.payload[2] = (ADT_UInt16) (pKeyInfo->u.Mki.MkiValue & 0xFFFF);
      msg.payload[3] = (ADT_UInt16) ((pKeyInfo->u.Mki.MkiValue >> 16) & 0xFFFF);
      msg.payload[4] = (ADT_UInt16) pKeyInfo->pkt.srtp.seq;
      msg.payload[5] = (ADT_UInt16) pKeyInfo->u.Mki.MkiI8;
      writeEventIntoFifo ((EventFifoMsg_t *)&msg);
      return 0;
   }

   memcpy (pKeyInfo->pKeyValue,  srtpKeyInfoPtr->MasterKey,  32*sizeof(ADT_UInt8));
   memcpy (pKeyInfo->pSaltValue, srtpKeyInfoPtr->MasterSalt, 14*sizeof(ADT_UInt8));
   pKeyInfo->KeyDerivationRate = srtpKeyInfoPtr->KDR;

   pKeyInfo->u.Mki.MaxKeyLife[0] = srtpKeyInfoPtr->MasterKeyLife[0];
   pKeyInfo->u.Mki.MaxKeyLife[1] = srtpKeyInfoPtr->MasterKeyLife[1];
   pKeyInfo->u.Mki.MaxKeyLife[2] = srtpKeyInfoPtr->MasterKeyLife[2];

   pKeyInfo->u.Mki.MkiValue = (((ADT_UInt32) srtpKeyInfoPtr->MKI[0]) << 24) |
                              (((ADT_UInt32) srtpKeyInfoPtr->MKI[1]) << 16) |
                              (((ADT_UInt32) srtpKeyInfoPtr->MKI[2]) <<  8) |
                              (((ADT_UInt32) srtpKeyInfoPtr->MKI[3]) <<  0) ;

   pKeyInfo->u.Mki.MkiI8         = srtpKeyInfoPtr->MKI_I8;
#if 0
        {
        int i;
        printf("KEYSCHEME=%d KEYSIZE_I8=%d KDR=%d\n",pKeyInfo->KeyScheme, srtpKeyInfoPtr->KeySizeU8, pKeyInfo->KeyDerivationRate);
        printf("MKL0=%u MKL1=%u MKL2=%u MKV=%x MKI_I8=%d\n",pKeyInfo->u.Mki.MaxKeyLife[0], pKeyInfo->u.Mki.MaxKeyLife[1], pKeyInfo->u.Mki.MaxKeyLife[2], pKeyInfo->u.Mki.MkiValue, pKeyInfo->u.Mki.MkiI8);
        for(i=0; i<srtpKeyInfoPtr->KeySizeU8/2; i++)
            printf("KEY[%d]=%x\n",i, pKeyInfo->pKeyValue[i]);   
        for(i=0; i<7; i++)
            printf("SALT[%d]=%x\n",i, pKeyInfo->pSaltValue[i]);
        }
#endif

   srtpKeyInfoPtr->NewKeyFlag = 0; // Clear the flag to indicate that there is no pending key.
   FLUSH_INST_CACHE (srtpKeyInfoPtr, sizeof (GpakSrtpKeyInfo_t));
   FLUSH_wait ();
   return 1;
}
#pragma CODE_SECTION (ProcConfigSRTPMsg, "SLOW_PROG_SECT")
GpakSrtpStatus_t ProcConfigSRTPMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {
   SrtpInstanceCfg_t SRTPInstanceCfg;
   SrtpCallBack_t   *pfSRTPCallBack;
   GpakSrtpKeyInfo_t *srtpKeyInfoPtr;
   GpakSrtpStatus_t retInit;

   ADT_UInt16 lenI16;
   int  chanId, FrameSize;
   void *pSRTPInst;
   void *pSRTPScratch = NULL;


   chanId   = (pCmd [1] & 0x00FF);

   pReply[0] |= (MSG_SRTP_CONFIG_REPLY << 8);
   pReply[1] = (chanId << 8);

   if (sysConfig.SRTP_Enable == 0)
      return GPAK_SRTPNotSupported;

   // Validate configuration parameters
   if (sysConfig.maxNumChannels < chanId)
      return GPAK_SRTPChannelError;

   if (chanTable[chanId]->channelType != inactive)
      return GPAK_SRTPChannelActive;

   SRTPInstanceCfg.APIVersion = SRTP_API_VERSION;
   SRTPInstanceCfg.KeySizeU8  = 16;
   SRTPInstanceCfg.SaltSizeU8 = 14;
   SRTPInstanceCfg.AuthKeySizeU8 = 20;

   if((pCmd [51] < 16) || (pCmd [51] > 32))
   {
       return GPAK_SRTPNotSupported; // wrong keysize
   } else {
       SRTPInstanceCfg.KeySizeU8  = pCmd [51];
   }
   if(pCmd [50] == 0) // in case frame size forgot to config
       return GPAK_SRTPNotSupported;
   if (ApiBlock.ApiVersionId < 0x0603u) {
      FrameSize = pCmd [50];
   } else {
      FrameSize = halfMSToSamps (pCmd [50]);
   }
   // Encoding is performed in framing task.  Allocate from framing task's scratch data.
   pSRTPScratch = getSAScratch (FrameSize, &lenI16);
   if (pSRTPScratch == NULL) return GPAK_SRTPNotSupported;

   //{ SRTP encoder message parsing.
   SRTPInstanceCfg.InstType     = INST_SRTP_TX;
   SRTPInstanceCfg.EncryptType  = (SrtpEncryptType_t) (pCmd [1] & 0xFF00) >> 8;
   SRTPInstanceCfg.AuthType     = (SrtpAuthType_t) pCmd [2];
   SRTPInstanceCfg.AuthTagLenU8 = pCmd [3];
   SRTPInstanceCfg.MkiLengthU8  =  (pCmd [24] & 0xFF00) >> 8;
   if (SRTPInstanceCfg.MkiLengthU8 == 0)
      SRTPInstanceCfg.KeyScheme = KEY_PSK;
   else if (SRTPInstanceCfg.MkiLengthU8 <= 4)
      SRTPInstanceCfg.KeyScheme = KEY_MKI;
   else
      return GPAK_SRTP_INVALID_MKI_LENGTH;

   CLEAR_INST_CACHE (SrtpTxChanData[chanId], srtpTxI8);
   FLUSH_wait ();
   CLEAR_INST_CACHE (SrtpTxKeyData[chanId],  sizeof (GpakSrtpKeyInfo_t));
   FLUSH_wait ();

   // key info
   srtpKeyInfoPtr = SrtpTxKeyData[chanId];
   srtpKeyInfoPtr->channelId = chanId;
   srtpKeyInfoPtr->Direction = DSPToNet; // Tx direction
   srtpKeyInfoPtr->MasterKey[0] = (pCmd [4] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterKey[1] = (pCmd [4] & 0x00FF);
   srtpKeyInfoPtr->MasterKey[2] = (pCmd [5] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterKey[3] = (pCmd [5] & 0x00FF);
   srtpKeyInfoPtr->MasterKey[4] = (pCmd [6] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterKey[5] = (pCmd [6] & 0x00FF);
   srtpKeyInfoPtr->MasterKey[6] = (pCmd [7] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterKey[7] = (pCmd [7] & 0x00FF);
   srtpKeyInfoPtr->MasterKey[8] = (pCmd [8] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterKey[9] = (pCmd [8] & 0x00FF);
   srtpKeyInfoPtr->MasterKey[10] = (pCmd [9] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterKey[11] = (pCmd [9] & 0x00FF);
   srtpKeyInfoPtr->MasterKey[12] = (pCmd [10] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterKey[13] = (pCmd [10] & 0x00FF);
   srtpKeyInfoPtr->MasterKey[14] = (pCmd [11] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterKey[15] = (pCmd [11] & 0x00FF);

   srtpKeyInfoPtr->MasterSalt[0] = (pCmd [12] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterSalt[1] = (pCmd [12] & 0x00FF);
   srtpKeyInfoPtr->MasterSalt[2] = (pCmd [13] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterSalt[3] = (pCmd [13] & 0x00FF);
   srtpKeyInfoPtr->MasterSalt[4] = (pCmd [14] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterSalt[5] = (pCmd [14] & 0x00FF);
   srtpKeyInfoPtr->MasterSalt[6] = (pCmd [15] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterSalt[7] = (pCmd [15] & 0x00FF);
   srtpKeyInfoPtr->MasterSalt[8] = (pCmd [16] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterSalt[9] = (pCmd [16] & 0x00FF);
   srtpKeyInfoPtr->MasterSalt[10] = (pCmd [17] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterSalt[11] = (pCmd [17] & 0x00FF);
   srtpKeyInfoPtr->MasterSalt[12] = (pCmd [18] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterSalt[13] = (pCmd [18] & 0x00FF);

   srtpKeyInfoPtr->MasterKeyLife[0] = pCmd [19];
   srtpKeyInfoPtr->MasterKeyLife[1] = pCmd [20];
   srtpKeyInfoPtr->MasterKeyLife[2] = pCmd [21];

   srtpKeyInfoPtr->MKI[0] = (pCmd [22] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MKI[1] = (pCmd [22] & 0x00FF);
   srtpKeyInfoPtr->MKI[2] = (pCmd [23] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MKI[3] = (pCmd [23] & 0x00FF);
   srtpKeyInfoPtr->MKI_I8 = SRTPInstanceCfg.MkiLengthU8;
   srtpKeyInfoPtr->KDR = (pCmd [24] & 0x00FF);
   srtpKeyInfoPtr->KeySizeU8 = SRTPInstanceCfg.KeySizeU8;

   if(SRTPInstanceCfg.KeySizeU8 >16) {
       srtpKeyInfoPtr->MasterKey[16] = (pCmd [53] & 0xFF00) >> 8;
       srtpKeyInfoPtr->MasterKey[17] = (pCmd [53] & 0x00FF);
       srtpKeyInfoPtr->MasterKey[18] = (pCmd [54] & 0xFF00) >> 8;
       srtpKeyInfoPtr->MasterKey[19] = (pCmd [54] & 0x00FF);
       srtpKeyInfoPtr->MasterKey[20] = (pCmd [55] & 0xFF00) >> 8;
       srtpKeyInfoPtr->MasterKey[21] = (pCmd [55] & 0x00FF);
       srtpKeyInfoPtr->MasterKey[22] = (pCmd [56] & 0xFF00) >> 8;
       srtpKeyInfoPtr->MasterKey[23] = (pCmd [56] & 0x00FF);
       srtpKeyInfoPtr->MasterKey[24] = (pCmd [57] & 0xFF00) >> 8;
       srtpKeyInfoPtr->MasterKey[25] = (pCmd [57] & 0x00FF);
       srtpKeyInfoPtr->MasterKey[26] = (pCmd [58] & 0xFF00) >> 8;
       srtpKeyInfoPtr->MasterKey[27] = (pCmd [58] & 0x00FF);
       srtpKeyInfoPtr->MasterKey[28] = (pCmd [59] & 0xFF00) >> 8;
       srtpKeyInfoPtr->MasterKey[29] = (pCmd [59] & 0x00FF);
       srtpKeyInfoPtr->MasterKey[30] = (pCmd [60] & 0xFF00) >> 8;
       srtpKeyInfoPtr->MasterKey[31] = (pCmd [60] & 0x00FF);
   }
   srtpKeyInfoPtr->NewKeyFlag = ADT_TRUE; // new key

   //}
   pSRTPInst      = &SrtpTxChanData[chanId]->Instance;
   pfSRTPCallBack = srtpCallbackProvideKey;
   retInit = (GpakSrtpStatus_t) SrtpInitializeInstance (pSRTPInst, pSRTPScratch, &SRTPInstanceCfg, pfSRTPCallBack, srtpKeyInfoPtr);
   SrtpTxKeyData[chanId]->Configured = ADT_TRUE;    // set the flag to indicate the srtp channel configed
   SrtpTxKeyData[chanId]->dataErrs = 0;
   SrtpTxKeyData[chanId]->lenErrs = 0;

   FLUSH_INST_CACHE (SrtpTxChanData[chanId], srtpTxI8);
   FLUSH_wait ();
   FLUSH_INST_CACHE (SrtpTxKeyData[chanId],  sizeof (GpakSrtpKeyInfo_t));
   FLUSH_wait ();

   if (retInit != GPAK_SRTP_SUCCESS)
      return retInit;

   // Decoding is performed by HostSWI.  Use common scratch buffer
   pSRTPScratch = (void *) &SrtpScratchPtr[0];

   //{ SRTP decoder message parsing
   SRTPInstanceCfg.InstType     = INST_SRTP_RX;
   SRTPInstanceCfg.EncryptType  = (SrtpEncryptType_t) pCmd [25];
   SRTPInstanceCfg.AuthType     = (SrtpAuthType_t) pCmd [26];
   SRTPInstanceCfg.AuthTagLenU8 = pCmd [27];
   SRTPInstanceCfg.MkiLengthU8  =  (pCmd [48] & 0xFF00) >> 8;
   if (SRTPInstanceCfg.MkiLengthU8 == 0)
      SRTPInstanceCfg.KeyScheme = KEY_PSK;
   else if (SRTPInstanceCfg.MkiLengthU8 <= 4)
      SRTPInstanceCfg.KeyScheme = KEY_MKI;
   else
      return GPAK_SRTP_INVALID_MKI_LENGTH;

   if((pCmd [52] < 16) || (pCmd [52] > 32))
   {
       return GPAK_SRTPNotSupported; // wrong keysize
   } else {
       SRTPInstanceCfg.KeySizeU8  = pCmd [52];
   }
   CLEAR_INST_CACHE (SrtpRxChanData[chanId], srtpRxI8);
   FLUSH_wait ();
   CLEAR_INST_CACHE (SrtpRxKeyData[chanId],  sizeof (GpakSrtpKeyInfo_t));
   FLUSH_wait ();

   // Key info
   srtpKeyInfoPtr = SrtpRxKeyData[chanId];
   srtpKeyInfoPtr->channelId = chanId;
   srtpKeyInfoPtr->Direction = NetToDSP; // Rx direction
   srtpKeyInfoPtr->MasterKey[0] = (pCmd [28] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterKey[1] = (pCmd [28] & 0x00FF);
   srtpKeyInfoPtr->MasterKey[2] = (pCmd [29] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterKey[3] = (pCmd [29] & 0x00FF);
   srtpKeyInfoPtr->MasterKey[4] = (pCmd [30] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterKey[5] = (pCmd [30] & 0x00FF);
   srtpKeyInfoPtr->MasterKey[6] = (pCmd [31] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterKey[7] = (pCmd [31] & 0x00FF);
   srtpKeyInfoPtr->MasterKey[8] = (pCmd [32] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterKey[9] = (pCmd [32] & 0x00FF);
   srtpKeyInfoPtr->MasterKey[10] = (pCmd [33] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterKey[11] = (pCmd [33] & 0x00FF);
   srtpKeyInfoPtr->MasterKey[12] = (pCmd [34] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterKey[13] = (pCmd [34] & 0x00FF);
   srtpKeyInfoPtr->MasterKey[14] = (pCmd [35] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterKey[15] = (pCmd [35] & 0x00FF);

   srtpKeyInfoPtr->MasterSalt[0] = (pCmd [36] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterSalt[1] = (pCmd [36] & 0x00FF);
   srtpKeyInfoPtr->MasterSalt[2] = (pCmd [37] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterSalt[3] = (pCmd [37] & 0x00FF);
   srtpKeyInfoPtr->MasterSalt[4] = (pCmd [38] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterSalt[5] = (pCmd [38] & 0x00FF);
   srtpKeyInfoPtr->MasterSalt[6] = (pCmd [39] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterSalt[7] = (pCmd [39] & 0x00FF);
   srtpKeyInfoPtr->MasterSalt[8] = (pCmd [40] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterSalt[9] = (pCmd [40] & 0x00FF);
   srtpKeyInfoPtr->MasterSalt[10] = (pCmd [41] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterSalt[11] = (pCmd [41] & 0x00FF);
   srtpKeyInfoPtr->MasterSalt[12] = (pCmd [42] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterSalt[13] = (pCmd [42] & 0x00FF);

   srtpKeyInfoPtr->MasterKeyLife[0] = pCmd [43];
   srtpKeyInfoPtr->MasterKeyLife[1] = pCmd [44];
   srtpKeyInfoPtr->MasterKeyLife[2] = pCmd [45];

   srtpKeyInfoPtr->MKI[0] = (pCmd [46] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MKI[1] = (pCmd [46] & 0x00FF);
   srtpKeyInfoPtr->MKI[2] = (pCmd [47] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MKI[3] = (pCmd [47] & 0x00FF);
   srtpKeyInfoPtr->MKI_I8 = SRTPInstanceCfg.MkiLengthU8;
   srtpKeyInfoPtr->KDR = (pCmd [48] & 0x00FF);
   srtpKeyInfoPtr->ROC = pCmd [49];
   srtpKeyInfoPtr->KeySizeU8 = SRTPInstanceCfg.KeySizeU8;
   if(SRTPInstanceCfg.KeySizeU8 >16) {
       srtpKeyInfoPtr->MasterKey[16] = (pCmd [61] & 0xFF00) >> 8;
       srtpKeyInfoPtr->MasterKey[17] = (pCmd [61] & 0x00FF);
       srtpKeyInfoPtr->MasterKey[18] = (pCmd [62] & 0xFF00) >> 8;
       srtpKeyInfoPtr->MasterKey[19] = (pCmd [62] & 0x00FF);
       srtpKeyInfoPtr->MasterKey[20] = (pCmd [63] & 0xFF00) >> 8;
       srtpKeyInfoPtr->MasterKey[21] = (pCmd [63] & 0x00FF);
       srtpKeyInfoPtr->MasterKey[22] = (pCmd [64] & 0xFF00) >> 8;
       srtpKeyInfoPtr->MasterKey[23] = (pCmd [64] & 0x00FF);
       srtpKeyInfoPtr->MasterKey[24] = (pCmd [65] & 0xFF00) >> 8;
       srtpKeyInfoPtr->MasterKey[25] = (pCmd [65] & 0x00FF);
       srtpKeyInfoPtr->MasterKey[26] = (pCmd [66] & 0xFF00) >> 8;
       srtpKeyInfoPtr->MasterKey[27] = (pCmd [66] & 0x00FF);
       srtpKeyInfoPtr->MasterKey[28] = (pCmd [67] & 0xFF00) >> 8;
       srtpKeyInfoPtr->MasterKey[29] = (pCmd [67] & 0x00FF);
       srtpKeyInfoPtr->MasterKey[30] = (pCmd [68] & 0xFF00) >> 8;
       srtpKeyInfoPtr->MasterKey[31] = (pCmd [68] & 0x00FF);
   }
   srtpKeyInfoPtr->NewKeyFlag = ADT_TRUE; // new key
   //}

   pSRTPInst      = &SrtpRxChanData[chanId]->Instance;
   pfSRTPCallBack = srtpCallbackProvideKey;
   retInit = (GpakSrtpStatus_t) SrtpInitializeInstance (pSRTPInst, pSRTPScratch, &SRTPInstanceCfg, pfSRTPCallBack, srtpKeyInfoPtr);
   SrtpRxKeyData[chanId]->Configured = ADT_TRUE;   // set the flag to indicate the srtp channel configed
   SrtpRxKeyData[chanId]->dataErrs = 0;
   SrtpRxKeyData[chanId]->lenErrs = 0;

   FLUSH_INST_CACHE (SrtpRxChanData[chanId], srtpRxI8);
   FLUSH_wait ();
   FLUSH_INST_CACHE (SrtpRxKeyData[chanId],  sizeof (GpakSrtpKeyInfo_t));
   FLUSH_wait ();

   return retInit;
}

#pragma CODE_SECTION (ProcConfigSRTPNewKeyMsg, "SLOW_PROG_SECT")
SrtpStatus_t ProcConfigSRTPNewKeyMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {
   int  chanId;

   GpakDeviceSide_t   Direction;
   GpakSrtpKeyInfo_t *srtpKeyInfoPtr;

   chanId   = (pCmd [1] & 0x00FF);
   Direction = (GpakDeviceSide_t) ((pCmd [1] & 0xFF00) >> 8);

   if (sysConfig.SRTP_Enable == 0)
      return SRTPNotSupported;

   // Validate configuration parameters
   if (sysConfig.maxNumChannels < chanId)
      return SRTPChannelError;

   if (chanTable[chanId]->channelType == inactive)
      return SRTPChannelInactive;

   if (Direction == DSPToNet) {
      srtpKeyInfoPtr = SrtpTxKeyData[chanId];
   } else if (Direction == NetToDSP) {
      srtpKeyInfoPtr = SrtpRxKeyData[chanId];
   } else
      return SRTP_INVALID_DIRECTION;

   if((pCmd [2] < 16) || (pCmd [2] > 32))
   {
       return SRTP_INVALID_DIRECTION; // wrong keysize
   }
   if (!srtpKeyInfoPtr->Configured) return SRTPNotConfigured;


   pReply[0] |= (MSG_SRTPNEWKEY_CONFIG_REPLY << 8);
   pReply[1] = (chanId << 8);


   CHN_lock (chanTable[chanId]);
   CLEAR_INST_CACHE (srtpKeyInfoPtr, sizeof (GpakSrtpKeyInfo_t));
   FLUSH_wait ();

   // fill the key info per channel
   srtpKeyInfoPtr->channelId = chanId;
   srtpKeyInfoPtr->Direction = Direction; // Tx direction
   srtpKeyInfoPtr->KeySizeU8 = pCmd [2]; // KeySizeU8
   srtpKeyInfoPtr->MasterKey[0] = (pCmd [4] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterKey[1] = (pCmd [4] & 0x00FF);
   srtpKeyInfoPtr->MasterKey[2] = (pCmd [5] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterKey[3] = (pCmd [5] & 0x00FF);
   srtpKeyInfoPtr->MasterKey[4] = (pCmd [6] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterKey[5] = (pCmd [6] & 0x00FF);
   srtpKeyInfoPtr->MasterKey[6] = (pCmd [7] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterKey[7] = (pCmd [7] & 0x00FF);
   srtpKeyInfoPtr->MasterKey[8] = (pCmd [8] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterKey[9] = (pCmd [8] & 0x00FF);
   srtpKeyInfoPtr->MasterKey[10] = (pCmd [9] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterKey[11] = (pCmd [9] & 0x00FF);
   srtpKeyInfoPtr->MasterKey[12] = (pCmd [10] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterKey[13] = (pCmd [10] & 0x00FF);
   srtpKeyInfoPtr->MasterKey[14] = (pCmd [11] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterKey[15] = (pCmd [11] & 0x00FF);

   srtpKeyInfoPtr->MasterSalt[0] = (pCmd [12] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterSalt[1] = (pCmd [12] & 0x00FF);
   srtpKeyInfoPtr->MasterSalt[2] = (pCmd [13] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterSalt[3] = (pCmd [13] & 0x00FF);
   srtpKeyInfoPtr->MasterSalt[4] = (pCmd [14] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterSalt[5] = (pCmd [14] & 0x00FF);
   srtpKeyInfoPtr->MasterSalt[6] = (pCmd [15] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterSalt[7] = (pCmd [15] & 0x00FF);
   srtpKeyInfoPtr->MasterSalt[8] = (pCmd [16] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterSalt[9] = (pCmd [16] & 0x00FF);
   srtpKeyInfoPtr->MasterSalt[10] = (pCmd [17] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterSalt[11] = (pCmd [17] & 0x00FF);
   srtpKeyInfoPtr->MasterSalt[12] = (pCmd [18] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MasterSalt[13] = (pCmd [18] & 0x00FF);

   srtpKeyInfoPtr->MasterKeyLife[0] = pCmd [19];
   srtpKeyInfoPtr->MasterKeyLife[1] = pCmd [20];
   srtpKeyInfoPtr->MasterKeyLife[2] = pCmd [21];

   srtpKeyInfoPtr->MKI[0] = (pCmd [22] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MKI[1] = (pCmd [22] & 0x00FF);
   srtpKeyInfoPtr->MKI[2] = (pCmd [23] & 0xFF00) >> 8;
   srtpKeyInfoPtr->MKI[3] = (pCmd [23] & 0x00FF);
   srtpKeyInfoPtr->MKI_I8 = (pCmd [24] & 0xFF00) >> 8;;
   srtpKeyInfoPtr->KDR    = (pCmd [24] & 0x00FF);

   if(srtpKeyInfoPtr->KeySizeU8 >16) {
       srtpKeyInfoPtr->MasterKey[16] = (pCmd [25] & 0xFF00) >> 8;
       srtpKeyInfoPtr->MasterKey[17] = (pCmd [25] & 0x00FF);
       srtpKeyInfoPtr->MasterKey[18] = (pCmd [26] & 0xFF00) >> 8;
       srtpKeyInfoPtr->MasterKey[19] = (pCmd [26] & 0x00FF);
       srtpKeyInfoPtr->MasterKey[20] = (pCmd [27] & 0xFF00) >> 8;
       srtpKeyInfoPtr->MasterKey[21] = (pCmd [27] & 0x00FF);
       srtpKeyInfoPtr->MasterKey[22] = (pCmd [28] & 0xFF00) >> 8;
       srtpKeyInfoPtr->MasterKey[23] = (pCmd [28] & 0x00FF);
       srtpKeyInfoPtr->MasterKey[24] = (pCmd [29] & 0xFF00) >> 8;
       srtpKeyInfoPtr->MasterKey[25] = (pCmd [29] & 0x00FF);
       srtpKeyInfoPtr->MasterKey[26] = (pCmd [30] & 0xFF00) >> 8;
       srtpKeyInfoPtr->MasterKey[27] = (pCmd [30] & 0x00FF);
       srtpKeyInfoPtr->MasterKey[28] = (pCmd [31] & 0xFF00) >> 8;
       srtpKeyInfoPtr->MasterKey[29] = (pCmd [31] & 0x00FF);
       srtpKeyInfoPtr->MasterKey[30] = (pCmd [32] & 0xFF00) >> 8;
       srtpKeyInfoPtr->MasterKey[31] = (pCmd [32] & 0x00FF);
   }
   // set the flag to indicate the srtp new key available
   srtpKeyInfoPtr->NewKeyFlag = ADT_TRUE; // new key

   FLUSH_INST_CACHE (srtpKeyInfoPtr, sizeof (GpakSrtpKeyInfo_t));
   FLUSH_wait ();
   CHN_unlock (chanTable[chanId]);

   return SRTP_STAT_SUCCESS;
}

#pragma CODE_SECTION (activateMsgHandler, "FAST_PROG_SECT")
void activateMsgHandler () {
   if (SWI_Msg != NULL) {
      SWI_post (SWI_Msg);
   } else if (msgSem != NULL) {
      SEM_postBinary (msgSem);
   }
}

#pragma CODE_SECTION (gpakVersionFromBuild, "SLOW_PROG_SECT")
int gpakVersionFromBuild () {
   char *sval;
   int   val;
   
   // Extract version number from build's 'VERSION'
   sval = VERSION;
   while (*sval != 0 && !isdigit(*sval)) sval++;
   if (*sval == 0) return sysConfig.gpakVersion;
   
   val = (*sval++ - '0') << 8;                    // First digit in 0x00 position,
   while (*sval != 0 && !isdigit(*sval)) sval++;
   if (*sval != 0) val |= (*sval++ - '0') << 4;   // Second digit in 00x0 position

   while (*sval != 0 && !isdigit(*sval)) sval++;
   if (*sval != 0) {
     val <<= 4;
     val |= (*sval++ - '0') << 4;     // Third digit in 00x0 position after shift upwards
   }

   // Extract subversion number from build's 'SUBVERSION'
   sval = SUBVERSION;
   while (*sval != 0 && !isdigit(*sval)) sval++;
   if (*sval != 0) val |= (*sval++ - '0');   // First digit in 000x position

   while (*sval != 0 && !isdigit(*sval)) sval++;
   if (*sval != 0) {
     val <<= 4;
     val |= (*sval++ - '0') << 4;     // Second digit in 000x position after shift upwards
   }
   return val;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ InitGpakInterface - Initialize G.PAK messaging interface with host processor.
//
// FUNCTION
//  Initializes the G.PAK interface with the host processor.
//
// RETURNS
//  nothing
//}
#pragma CODE_SECTION (InitGpakInterface, "SLOW_PROG_SECT")
void InitGpakInterface (void) {
   int i;         // loop index / counter

   sysConfig.gpakVersion = gpakVersionFromBuild ();

   // Multi-core with network stack -- messaging is task on core 1
   if (SysAlgs.networkStack && (DSPCore == msgCore)) {
      struct TSK_Attrs hwi_attrs;

      hwi_attrs = TSK_ATTRS;
      hwi_attrs.priority  = 8;  // Must be less than OS_TASKPRIKERN
      hwi_attrs.stack     = msgTaskStack;
      hwi_attrs.stacksize = sizeof (msgTaskStack);
      hwi_attrs.name = "msgTask";
      hwi_attrs.exitflag = FALSE;
#ifdef _DEBUG
      hwi_attrs.initstackflag = 1;
#else
      hwi_attrs.initstackflag = 0;
#endif

      msgSem = SEM_create (0, NULL);
      TSK_create ((Fxn) msgTask, &hwi_attrs);
      SWI_Msg = NULL;
      if (DSPCore != 0) return;
   } 

   // Single core or multi-core without network stack -- messaging is swi
   if ((!SysAlgs.networkStack || (DSPTotalCores == 1)) && (sysConfig.DspType != Ti667)) {
      swi_attr.priority = MsgPriority;
#if (DSP_TYPE == 54)
      swi_attr.iscfxn = TRUE;
#endif

      SWI_Msg = SWI_create (&swi_attr);
      AppErr ("SWI Message Create", SWI_Msg == NULL);
   }

   // Messaging is processed by core 0.
   if (DSPCore != 0) return;

   //{ Initialize channel structures.
   for (i = 0; i < sysConfig.maxNumChannels; i++)   {
      chanTable[i] = AllocateInstanceStructures (i);
      chanTable[i]->channelType = inactive;
   }
   //}==================================================

   //{ Initialize event FIFO interface
   eventFIFOInfo.pBufrBase = (ADT_UInt16 *)eventFIFOBuff;
   eventFIFOInfo.BufrSize = sysConfig.evtFifoLenI16;
   eventFIFOInfo.PutIndex = 0;
   eventFIFOInfo.TakeIndex = 0;
   eventFIFOInfo.SlipSamps = 0;
//   fifoLock.Inst = 0;
//   CACHE_WB (&fifoLock.Inst, 4);
   pfifoLock->Inst = 0;
   CACHE_WB (&pfifoLock->Inst, 4);
   //}==================================================

   for (i=0; i<RESOURCE_CNT; i++) ResourceLocks[i].Inst = 0;
   

   //{ Initialize API memory block
   ApiBlock.PktBufrMem      = pktBuffStructTable;
 
   ApiBlock.CmdMsgPointer   = (ADT_UInt16 *) CmdMsgBuffer;
   ApiBlock.ReplyMsgPointer = (ADT_UInt16 *) ReplyMsgBuffer;
   ApiBlock.EventMsgPointer = &eventFIFOInfo;

   ApiBlock.DspStatus       = GPAK_INIT_STATUS;
   ApiBlock.VersionId       = (ADT_UInt16) sysConfig.gpakVersion;
   ApiBlock.MaxCmdMsgLen    = MAX_CMD_MSG_BYTES;
   ApiBlock.CmdMsgLength    = 0;
   ApiBlock.ReplyMsgLength  = 0;
   ApiBlock.EventMsgLength  = 0;
   ApiBlock.NumBuiltChannels = sysConfig.maxNumChannels;
   ApiBlock.DmaSwiCnt       = 0;

   // 4_2 ----------------------------------------------------------------------
   ApiBlock.ApiVersionId    = DEFAULT_GPAK_API_VERSION;

   //---------------------------------------------------------------------------   

   ApiBlock.pWrBlock   = (WrIfBlock_t   *) 0;
   ApiBlock.pRdBlock   = (RdIfBlock_t   *) 0;
   ApiBlock.pRdWrBlock = (RdWrIfBlock_t *) 0;

   // Init the extended API blocks when api cache is enabled
   if (SysAlgs.apiCacheEnable) {
      memset(ApiWrBlock,   0, CACHE_L2_LINE_SIZE);
      memset(ApiRdBlock,   0, CACHE_L2_LINE_SIZE);
      memset(ApiRdWrBlock, 0, CACHE_L2_LINE_SIZE);

      ApiBlock.pWrBlock   = (WrIfBlock_t   *) &ApiWrBlock;
      ApiBlock.pRdBlock   = (RdIfBlock_t   *) &ApiRdBlock;
      ApiBlock.pRdWrBlock = (RdWrIfBlock_t *) &ApiRdWrBlock;

      ApiBlock.pRdBlock->ApiVersionId =  DEFAULT_GPAK_API_VERSION;
      ApiBlock.pRdWrBlock->DspStatus  =  GPAK_INIT_STATUS;

      CACHE_WBINV(&ApiBlock,    sizeof(ApiBlock));
      CACHE_WBINV(ApiWrBlock,   CACHE_L2_LINE_SIZE);
      CACHE_WBINV(ApiRdBlock,   CACHE_L2_LINE_SIZE);
      CACHE_WBINV(ApiRdWrBlock, CACHE_L2_LINE_SIZE);
   }

   // CPU Usage
   memset (CpuUsage,   0, sizeof (ADT_Word) * FRAME_TASK_CNT * DSPTotalCores);
   memset (CpuPkUsage, 0, sizeof (ADT_Word) * FRAME_TASK_CNT * DSPTotalCores);
#ifdef CPU_TEST
    for (i=0; i<DSPTotalCores; i++) {
        int j=0;
        for (j=0; j<FRAME_TASK_CNT; j++) {
            MultiCoreCpuUsage[i*FRAME_TASK_CNT+ j]     = (i+1)*1000  + (j+1)*10;
            MultiCoreCpuPeakUsage[i*FRAME_TASK_CNT+ j] = (i+1)*10000 + (j+1)*10;
        }                
    }
#endif
   gblPkLoadingRst = 0;

   // Initialize System status variables.
   NumActiveChannels = 0;

   // Initialize the Test Mode.
   CurrentTestMode = DisableTest;

   //}==================================================

   //{ Initialize algorithm instance pools
   NumPcmEcansUsed = 0;
   for (i = 0; i < sysConfig.numPcmEcans; i++)   PcmEcInUse[i] = 0;
      
   NumPktEcansUsed = 0;
   for (i = 0; i < sysConfig.numPktEcans; i++)   PktEcInUse[i] = 0;

#if (DSP_TYPE == 64)
   NumAECEcansUsed = 0;
#ifdef AEC_SUPPORTED
   for (i = 0; i < sysConfig.AECInstances; i++)  {
        AECInUse[i] = 0;
        AECHandles[i] = (IAECG4_Handle) 0;
   }
#endif
#endif
     
   numFaxChansUsed = 0;
   for (i=0; i<sysConfig.numFaxChans; i++)     faxChanInUse[i] = 0;
      
   numAGCChansAvail  = sysConfig.numAGCChans;
   for (i=0; i<sysConfig.numAGCChans; i++)     agcChanInUse[i] = 0;

   numToneGenChansAvail = sysConfig.numTGChans;
   for (i=0; i< sysConfig.numTGChans; i++)     toneGenChanInUse[i] = 0;

   CedDetTotal = CedDetAvail;
   for (i=0; i<CedDetTotal; i++)  CedDetInUse[i] = 0;
    
   CngDetTotal = CngDetAvail;
   for (i=0; i<CngDetTotal; i++)  CngDetInUse[i] = 0;

   ArbToneDetectorsTotal = ArbToneDetectorsAvail;
   for (i=0; i<ArbToneDetectorsTotal; i++)  ArbToneDetectorInUse[i] = 0;
   for (i=0; i<numArbToneCfgs; i++)         ArbToneConfigured[i] = 0;

   ToneDetectorsTotal = ToneDetectorsAvail;
   for (i=0; i<ToneDetectorsTotal; i++)  ToneDetectorInUse[i] = 0;

   numRxCidChansAvail = sysConfig.numRxCidChans;
   for (i=0; i<numRxCidChansAvail; i++) rxCidInUse[i] = 0;    

   numTxCidChansAvail = sysConfig.numTxCidChans;
   for (i=0; i<numTxCidChansAvail; i++) txCidInUse[i] = 0;

   PcmBuffAvail = sysConfig.maxNumPcmBuffs;
   for (i=0; i<PcmBuffAvail; i++)  PcmBuffInUse[i] = 0;

   BulkDelayBuffAvail = sysConfig.maxNumBulkDelayBuffs;
   for (i=0; i<BulkDelayBuffAvail; i++)  BulkDelayBuffInUse[i] = 0;

   PktBuffAvail = sysConfig.maxNumPktBuffs;
   for (i=0; i<PktBuffAvail; i++)  PktBuffInUse[i] = 0;

   numSrcUpBy2ChansAvail = sysConfig.numSrcChans;
   for (i=0; i< sysConfig.numSrcChans; i++)     srcUpBy2ChanInUse[i] = 0;
   numSrcDownBy2ChansAvail = sysConfig.numSrcChans;
   for (i=0; i< sysConfig.numSrcChans; i++)     srcDownBy2ChanInUse[i] = 0;

   if (sysConfig.SRTP_Enable) {
      for (i=0; i< sysConfig.maxNumChannels; i++) {
          SrtpTxKeyData[i]->Configured = ADT_FALSE;
          SrtpRxKeyData[i]->Configured = ADT_FALSE;
      }
   }
   //}

   return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ ServiceGpakInterface - Service G.PAK interface with host processor.
//
// FUNCTION
//  This function checks for and processes all messages from the host processor
//  via the G.PAK interface.
//
// RETURNS
//  nothing
//}
#pragma CODE_SECTION (ServiceGpakInterface, "SLOW_PROG_SECT")
void ServiceGpakInterface (void) {
   ADT_UInt16 *pCmdBufr;    // Command message buffer pointer
   ADT_UInt16 *pReplyBufr;  // Reply message buffer pointer
   ADT_Word *pCmdMsgLength;
   ADT_Word *pReplyMsgLength;
   int replyI8, msgID, i;

   if (custom_cmd) {
        ApiBlock.CmdMsgLength = 0;
        process_custom_cmd ();
        custom_cmd = 0;
        return;
   }

   if (SysAlgs.apiCacheEnable) {
      // No need to invalidate cache, as it was done just prior to 
      // posting this interrupt.
      pCmdMsgLength     = &(ApiBlock.pRdWrBlock->CmdMsgLength);
      pReplyMsgLength   = &(ApiBlock.pRdWrBlock->ReplyMsgLength);
   } else {
      pCmdMsgLength     = &(ApiBlock.CmdMsgLength);
      pReplyMsgLength   = &(ApiBlock.ReplyMsgLength);
   }


   // Check for the presence of a Command message.
   logTime (0x80000001ul);
   if (*pCmdMsgLength == 0) {
      logTime (0x80010001ul);
      return;
   }

   
   pCmdBufr   = ApiBlock.CmdMsgPointer;
   pReplyBufr = ApiBlock.ReplyMsgPointer;
      
   // Prepare the reply with the command's serial number.
   pReplyBufr[0] = pCmdBufr[0] & 0xFF;
   logTime (0x04000000ul | pCmdBufr[0]);

   // Process based on the Command message type code.
   msgID = (pCmdBufr[0] >> 8) & 0xFF;

   switch (msgID)   {
   // System configuration and status messages
   case MSG_RESET_SYS_STATS:     { // Zero system statistics
      pReplyBufr[1] =  (ADT_UInt16) ProcResetSysStatsMsg(pCmdBufr, pReplyBufr);        
      replyI8 = ReplyBytes (2);
      break;
      }
   case MSG_READ_CPU_USAGE:      { // Read CPU usage
      ADT_Word *pSrc, *pSrcPk;
      ADT_UInt32 *pDst;   
      int srcLenI8;

      pReplyBufr[0] |= (MSG_READ_CPU_USAGE_REPLY << 8);
      memset(&pReplyBufr[2], 0, sizeof(ADT_Word)*96);
#if 0 
      pReplyBufr[1] = (ADT_UInt16)DSPTotalCores;
      //pReplyBufr[1] = (ADT_UInt16)(frameTaskCnt << 8) | (ADT_UInt16)DSPTotalCores;
      replyI8 = FRAME_TASK_CNT * DSPTotalCores * sizeof (ADT_Word);
      if (DSPTotalCores == 1) {
         memcpy (&pReplyBufr[2], CpuUsage, replyI8);
         memcpy (&pReplyBufr[2 + replyI8/sizeof (ADT_UInt16)], CpuPkUsage, replyI8);
      } else {
         memcpy (&pReplyBufr[2], MultiCoreCpuUsage, replyI8);
         memcpy (&pReplyBufr[2 + replyI8/sizeof (ADT_UInt16)], MultiCoreCpuPeakUsage, replyI8);
      }
#else
      pReplyBufr[1] = (ADT_UInt16)(frameTaskCnt << 8) | (ADT_UInt16)DSPTotalCores;
      pDst = (ADT_UInt32 *)&pReplyBufr[2];
      if (DSPTotalCores == 1) {
            pSrc   = CpuUsage;
            pSrcPk = CpuPkUsage;
      } else {
            pSrc   = MultiCoreCpuUsage;
            pSrcPk = MultiCoreCpuPeakUsage;
      }
      srcLenI8 = sizeof (ADT_Word) * frameTaskCnt;
      replyI8 = 0;
      for (i=0; i<DSPTotalCores; i++) {
            memcpy (pDst, pSrc, srcLenI8);
            pDst += frameTaskCnt;
            pSrc += FRAME_TASK_CNT;
            replyI8 += srcLenI8;

            memcpy (pDst, pSrcPk, srcLenI8);
            pDst += frameTaskCnt;
            pSrcPk += FRAME_TASK_CNT;
            replyI8 += srcLenI8;
      }
#endif
      replyI8 = 4 + 2 * replyI8;
      break;
      }
   case MSG_SYS_CONFIG_RQST:     { // Read system configuration.
      replyI8 = ProcSystemConfigMsg (pCmdBufr, pReplyBufr);
      break;
      }
   case MSG_READ_SYS_PARMS:      { // Read system parameters.
      replyI8 = ProcReadSystemParmsMsg ((ADT_Int16 *) pCmdBufr, pReplyBufr);
      break;
      }
   case MSG_WRITE_SYS_PARMS:     { // Write system parameters.
      pReplyBufr[1] = (ADT_UInt16) (ProcWriteSystemParmsMsg ((ADT_Int16 *) pCmdBufr, pReplyBufr) << 8);
      replyI8 = ReplyBytes (2);
      break;
      }
   case MSG_READ_NOISE_PARAMS:   { // Read global noise suppression parameters
      replyI8 = ReplyBytes (ProcReadNoiseParmsMsg ((ADT_Int16 *)pCmdBufr, (ADT_Int16 *)pReplyBufr));
      break;
   }
   case MSG_WRITE_NOISE_PARAMS:  { // Write global noise suppression parameters
      pReplyBufr[1] = (ADT_UInt16)   (ProcWriteNoiseParmsMsg ((ADT_Int16 *)pCmdBufr, pReplyBufr) << 8);
      replyI8 = ReplyBytes (2);
      break;
   }
   case MSG_READ_AEC_PARMS:      { // Read global AEC parameters
      replyI8 = ProcReadAECParmsMsg (pCmdBufr, pReplyBufr);
      break;
      }
   case MSG_WRITE_AEC_PARMS:     { // Write global AEC parameters
      pReplyBufr[1] = (ADT_UInt16) (ProcWriteAECParmsMsg (pCmdBufr, pReplyBufr) << 8);
      replyI8 = ReplyBytes (2);
      break;     
      }
   case MSG_CONFIG_ARB_DETECTOR: { // Write generic tone detection parameters
      pReplyBufr[0] |= (MSG_CONFIG_ARB_DETECTOR_REPLY << 8);
      pReplyBufr[1] = ((pCmdBufr[1] << 8) & 0xFF00) | ((ADT_UInt16) (ProcConfigArbToneDetMsg(pCmdBufr) & 0xFF));
      replyI8 = ReplyBytes(2);
      break;
      }
   case MSG_ACTIVE_ARB_DETECTOR: { // Select which tone detection parameters to use for future channels
      pReplyBufr[0] |= (MSG_ACTIVE_ARB_DETECTOR_REPLY << 8);
      pReplyBufr[1] = ((pCmdBufr[1] << 8) & 0xFF00) | ((ADT_UInt16) (ProcActiveArbToneDetMsg(pCmdBufr) & 0xFF));
      replyI8 = ReplyBytes(2);
      break;
      }
   case MSG_WRITE_SPEEX_PARAMS:  { // Write global speex parameters.
      pReplyBufr[1] = ProcWriteSpeexParamsMsg ((ADT_Int16 *) pCmdBufr, pReplyBufr);
      replyI8 = ReplyBytes (2);
      break;
      }
   case MSG_WRITE_DTMFCFG_PARMS: { // Write global DTMF detection paramters
      pReplyBufr[1] = (ADT_UInt16) (ProcWriteDtmfCfgParmsMsg ((ADT_Int16 *) pCmdBufr, pReplyBufr) << 8);
      replyI8 = ReplyBytes (2);
      break;
      }
   case MSG_SET_VLAN_TAG:        { // Configure VLAN tag
      replyI8 = ProcessSetVLANTagMsg (pCmdBufr, pReplyBufr);
      break;
      }
   case MSG_GET_VLAN_TAG:        { // Read VLAN tag
      replyI8 = ProcessGetVLANTagMsg (pCmdBufr, pReplyBufr);
      break;
      }

   // TDM bus configuration and control messages
   case MSG_CONFIGURE_PORTS:     { // Configure Serial Ports.
      pReplyBufr[1] =  (ADT_UInt16) (ProcConfigSerialPortsMsg (pCmdBufr, pReplyBufr) << 8);
      replyI8 = ReplyBytes (2);
      break;
      }
   case MSG_READ_MCBSP_STATS:    { // Read TDM port statistic
      replyI8 = ReplyBytes (ProcMcBspStatsMsg(pCmdBufr, pReplyBufr));        
      break;
      }

   // Conference configuration and control messages
   case MSG_CONFIG_CONFERENCE:   { // Configure conference.
      pReplyBufr[0] |= (MSG_CONFIG_CONF_REPLY << 8);
      pReplyBufr[1] = (pCmdBufr[1] & 0xFF00) | ((ADT_UInt16) (ProcConfigConferenceMsg(pCmdBufr) & 0xFF));
      replyI8 = ReplyBytes (2);
      break;
      }
   case MSG_CONFTONEGEN:         { // Generate tone for all conference members
      pReplyBufr[2] = (ADT_UInt16) (ProcConfToneGenMsg(pCmdBufr, pReplyBufr) << 8);
      replyI8 = ReplyBytes (3);
      break;
      }

   //  Channel configuration and status messages
   case MSG_CONFIGURE_CHANNEL:  {  // Configure Channel.
      pReplyBufr[1] |= (ADT_UInt16) (ProcConfigureChannelMsg (pCmdBufr, pReplyBufr) & 0xFF);
      replyI8 = ReplyBytes (2);
      break;
      }
   case MSG_CHAN_STATUS_RQST:   {  // Channel Status Request.
      replyI8 = ProcChannelStatusMsg (pCmdBufr, pReplyBufr);
      break;
      }
   case MSG_TEAR_DOWN_CHANNEL:  {  // Tear Down Channel.
      replyI8 = ProcTearDownChannelMsg (pCmdBufr, pReplyBufr);
      break;
      }
   case MSG_RTP_CONFIG:         {  // RTP configuration parameters 
      pReplyBufr[1] |= (ADT_UInt16) (ProcConfigRTPMsg (pCmdBufr, pReplyBufr) & 0xFF);
      replyI8 = ReplyBytes (2);
      break;
      }
   case MSG_RTP_V6_CONFIG:       {  // RTP IpV6 configuration parameters
      pReplyBufr[1] |= (ADT_UInt16) (ProcConfigRTPv6Msg (pCmdBufr, pReplyBufr) & 0xFF);
      replyI8 = ReplyBytes (2);
      break;
      }
   case MSG_SRTP_CONFIG:        {  // SRTP configuration parameters 
      pReplyBufr[1] |= (ADT_UInt16) (ProcConfigSRTPMsg (pCmdBufr, pReplyBufr) & 0xFF);
      replyI8 = ReplyBytes (2);
      break;
      }
   case MSG_READ_AGC_POWER:     {  // Read AGC power level
      pReplyBufr[1] |=  (ADT_UInt16) ProcReadAgcPowerMsg(pCmdBufr, pReplyBufr);        
      replyI8 = ReplyBytes (4);
      break;
      }
   case MSG_RTP_STATUS_RQST:    {  // Obtain RTP statistics
      replyI8 = ReplyBytes (ProcRTPStatusMsg (pCmdBufr, pReplyBufr));
      break;
      }

   //  Channel runtime control messages
   case MSG_ALG_CONTROL:        {  // Algorithm control
      pReplyBufr[1] |=  (ADT_UInt16) ProcAlgControlMsg(pCmdBufr, pReplyBufr);        
      replyI8 = ReplyBytes (2);
      break;
      }
   case MSG_READ_ECAN_STATE:    {  // Read echo canceller state
      replyI8 = ReplyBytes (ProcReadEcanStateMsg(pCmdBufr, pReplyBufr));        
      break;
      }
   case MSG_WRITE_ECAN_STATE:   {  // Write echo canceller state
      pReplyBufr[1] |=  (ADT_UInt16) ProcWriteEcanStateMsg(pCmdBufr, pReplyBufr);        
      replyI8 = ReplyBytes (2);
      break;
      }
   case MSG_SRTPNEWKEY_CONFIG:  {  // SRTP key delivery
      pReplyBufr[1] |= (ADT_UInt16) (ProcConfigSRTPNewKeyMsg (pCmdBufr, pReplyBufr) & 0xFF);
      replyI8 = ReplyBytes (2);
      break;
      }
   case MSG_TONEGEN:            {  // Tone generation
      pReplyBufr[2] = (ADT_UInt16) (ProcToneGenMsg(pCmdBufr, pReplyBufr) << 8);
      replyI8 = ReplyBytes (3);
      break;
      }
   case MSG_TONEPKTGEN:         {  // Tone packet insertion
      pReplyBufr[2] = (ADT_UInt16) (ProcTonePktGenMsg(pCmdBufr, pReplyBufr) << 8);
      replyI8 = ReplyBytes (3);
      break;
      }
   case MSG_INRTPTONEPKTGEN:    {
      pReplyBufr[2] = (ADT_UInt16) (ProcInsertTonePktGenMsg(pCmdBufr, pReplyBufr) << 8);
      replyI8 = ReplyBytes (3);
      break;
      }
   case MSG_PLAY_RECORD:        {  // Voice record and playback
      pReplyBufr[2] = (ADT_UInt16) (ProcPlayRecMsg(pCmdBufr, pReplyBufr) << 8);        
      replyI8 = ReplyBytes (3);
      break;
      }
   case MSG_RESET_CHAN_STATS:   {  // Zero channel statistic
      pReplyBufr[1] |=  (ADT_UInt16) ProcResetChanStatsMsg(pCmdBufr, pReplyBufr);        
      replyI8 = ReplyBytes (2);
      break;
      }
   case MSG_WRITE_TXCID_DATA:   {  // Generate CID signaling
      pReplyBufr[1] |=  (ADT_UInt16) ProcTxCallerIDMsg(pCmdBufr, pReplyBufr);
      replyI8 = ReplyBytes(2);
      break;
      }
   case MSG_DTMFDIAL:           {  // Generate dialing sequence of DTMF tones
      pReplyBufr[2] = (ADT_UInt16) (ProcDtmfDialMsg(pCmdBufr, pReplyBufr) << 8);
      replyI8 = ReplyBytes (3);
      break;
      }
    case MSG_READ_MEMTEST_STATUS: {
      replyI8 = ReplyBytes(ProcMemoryTestMsg (pCmdBufr, pReplyBufr));
      break;
      }
   //  Test mode controls
   case MSG_TEST_MODE:          {  // Test Mode.
      replyI8 = ProcTestModeMsg (pCmdBufr, pReplyBufr);
      break;
      }

   // Read LEC Status.
   case MSG_READ_LEC_STATUS:
   {
      replyI8 = ReplyBytes (ProcReadLecChanStatusMsg(pCmdBufr, pReplyBufr));        
      break;
   }

   // Write the RTP TX Timeout Value
   case MSG_WRITE_RTP_TIMEOUT:
   {
       replyI8 = ReplyBytes (ProcWriteRtpTxTimeoutValueMsg (pCmdBufr, pReplyBufr));
       break;
   }

   //  Configure the ED137B interface parameters
   case MSG_CONFIGURE_ED137B_PARMS:
   {
       replyI8 = ReplyBytes (ProcConfigureED137BMsg(pCmdBufr, pReplyBufr));
       break;
   }

   // Update ED137B PPT Parameters
   case MSG_UPDATE_ED137B_PTT_PARMS:
   {
      replyI8 = ReplyBytes (ProcUpdateED137B_PTT_Msg(pCmdBufr, pReplyBufr));
      break;
   }

   //  Write ED137B Persistent Feature Parameters
   case MSG_CONFIGURE_ED137B_PERSISTENT_FEATURES_PARMS:
   {
      replyI8 = ReplyBytes (ProcPersistentFeatureMsg(pCmdBufr, pReplyBufr));
      break;
   }

   //  Update ED137B Persistent Feature Parameters
   case MSG_ED137B_PERSISTENT_FEATURES_UPDATE_PARMS:
   {
       replyI8 = ReplyBytes (ProcUpdatePersistentFeatureMsg(pCmdBufr, pReplyBufr));
       break;
   }

   //  Update ED137B Persistent Feature Parameters
   case MSG_CONFIGURE_ED137B_NON_PERSISTENT_FEATURES_PARMS:
   {
      replyI8 = ReplyBytes (ProcNonPersistentFeatureMsg(pCmdBufr, pReplyBufr));
      break;
   }
  case MSG_CONFIGURE_ED137B_GATE_AUDIO_FEATURE_PARMS:
  {
     replyI8 = ReplyBytes (ProcED137B_GateAudioFeatureFeatureMsg(pCmdBufr, pReplyBufr));
     break;
  }
   // AEC status and state messaging
#if (AEC_LIB_VERSION >= 0x0420)

   // Read AEC Status.
   case MSG_READ_AEC_STATUS:
   {
      replyI8 = ProcReadAecStatusMsg(pCmdBufr, pReplyBufr);
      break;
   }

   // Save AEC State.
   case MSG_SAVE_AEC_STATE:
   {
      replyI8 = ProcSaveAecStateMsg(pCmdBufr, pReplyBufr);
      break;
   }

   // Restore AEC State.
   case MSG_RESTORE_AEC_STATE:
   {
      replyI8 = ProcRestoreAecStateMsg(pCmdBufr, pReplyBufr);
      break;
   }

   // Inform AEC of Gain Change.
   case MSG_AEC_GAIN_CHANGE:
   {
      replyI8 = ProcAecGainChangeMsg(pCmdBufr, pReplyBufr);
      break;
   }

#endif
   /* Open a new wavefile for download/playback */
   case MSG_NEW_WAVEFILE:
   {
        replyI8 = ProcNewWavefileMsg(pCmdBufr, pReplyBufr);
        break;
   } 

   /* Load a new wavefile for download/playback */
   case MSG_LOAD_WAVEFILE:
   {
        replyI8 = ProcLoadWavefileMsg(pCmdBufr, pReplyBufr);
        break;
   } 

   case MSG_PLAY_WAVEFILE:
   {
        pReplyBufr[1] |= (ADT_UInt16) ProcPlayWavefileMsg(pCmdBufr, pReplyBufr);        
        replyI8 = ReplyBytes (2);
        break;
   } 

   case MSG_RTCP_BYE:
   {
#ifdef RTCP_ENABLED
        replyI8 = rtcp_process_bye_msg(pCmdBufr, pReplyBufr);
#else
      pReplyBufr[0] |= (MSG_NULL_REPLY << 8);
      replyI8 = ReplyBytes (1);
#endif
        break;
   }
   
   default:                     {  // Custom or unknown messages
      replyI8 = customMsg (pCmdBufr, pReplyBufr);
      if (0 < replyI8) {
         break;
      }
      if (replyI8 == -1) {
         return;
      }
      pReplyBufr[0] |= (MSG_NULL_REPLY << 8);
      replyI8 = ReplyBytes (1);
      break;
      }
   }

   if (SysAlgs.apiCacheEnable) {
      cacheWbInvBuf (pReplyBufr, replyI8);
   }

   *pReplyMsgLength = replyI8;

   logTime (0x04010000ul | pReplyBufr[0]);

   // Indicate the Command message was processed.
   *pCmdMsgLength = 0;
   if (SysAlgs.apiCacheEnable) {
      CACHE_WBINV (ApiBlock.pRdWrBlock, CACHE_L2_LINE_SIZE);
   }

   logTime (0x80010001ul);

}


#pragma CODE_SECTION (msgTask, "SLOW_PROG_SECT")
void msgTask (void) {
   while (msgSem != NULL) {
      SEM_pendBinary (msgSem, SYS_FOREVER);
      if (ApiBlock.CmdMsgLength == 0) continue;

      ServiceGpakInterface ();
   }
   AppErr ("Msg task exit", TRUE);
   TSK_exit ();
}
