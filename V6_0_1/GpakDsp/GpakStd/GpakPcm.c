/*
 * Copyright (c) 2004-2007, Adaptive Digital Technologies, Inc.
 *
 * File Name: GpakPcm.c
 *
 * Description:
 *   This file contains G.PAK PCM I/O functions.
 *
 * Version: 1.0
 *
 * Revision History:
 *   11/13/01 - Initial release.
 *    1/2007  - Separate McBSP and EDMA drivers
 */

//
//{  Globals

// SltsPerFrame[]  - TDM slots per frame set to sysConfig.numSlotsOnStream
// MaxDmaSlots[]   - Max slots processed in a frame set to sysConfig.maxSlotsSupported
// DmaSlotCnt[]    - Active slots in a frame set during port configuration

//
//
//} Application related header files.
#include <std.h>
//#include <tsk.h>
//#include <clk.h>
//#include <swi.h>
#include <stdio.h>
#include "GpakDefs.h"
#include "GpakExts.h"
#include "GpakErrs.h"
#include "GpakPcm.h"
#include "sysmem.h"
#include "sysconfig.h"

#ifndef _DEBUG
#define LogBuff(a,b)
#endif


#define STATS(stmt) { stmt }

extern int DmaLbRxToTxBuffers (int port, int slotCnt);
extern ADT_Word DmaSwiStopCnt;
extern ADT_UInt32 RxEvents [];             // Rx DMA Flags (configured by DMA)
extern ADT_UInt32 TxEvents [];             // Tx DMA Flags (configured by DMA)
extern void ClearTransmitEnables (int port, ADT_UInt32 Mask[]);
extern void GetMasks (ADT_Word McBspId, int Slot, int SlotCnt, ADT_Word *TxMask);
extern void initDMAPhaseCounters ();
extern void ScheduleFramingTasks();
extern volatile chanInfo_t *PendingChannel;
extern ADT_Bool McBSPFrameError (int port);
extern void setCircPointersPktPkt (chanInfo_t *chan,
          pcm2pkt_t *EncodeA,      pkt2pcm_t *DecodeA,         // A channel buffers
          pcm2pkt_t *EncodeB,      pkt2pcm_t *DecodeB,         // B channel buffers
          ADT_UInt16 EncodeAPhase, ADT_UInt16 DecodeAPhase,    // A channel phases
          ADT_UInt16 EncodeBPhase, ADT_UInt16 DecodeBPhase);  // B channel phases
extern void SetTransmitEnables (int port, ADT_UInt32 Mask[]);
extern void storeMcBSPConfiguration (int port);
extern void zeroTxBuffer (int port, int dmaSlot);

void StopSerialPortIo (void);

int TDMRate;                // TDM sampling rate (Hz)
#pragma DATA_SECTION (TDMRate, "NON_CACHED_DATA:TDM")

// Serial port (stream) configuration related variables.
ADT_UInt16 SltsPerFrame[NUM_TDM_PORTS];  // Slots on TDM frame [fixed at build]
ADT_UInt16 MaxDmaSlots [NUM_TDM_PORTS];  // Max slots available for DMA [fixed at build]
ADT_UInt16 DmaSlotCnt  [NUM_TDM_PORTS];  // Active DMA count [fixed at TDM configure]
#pragma DATA_SECTION (SltsPerFrame, "NON_CACHED_DATA:TDM")
#pragma DATA_SECTION (MaxDmaSlots,  "NON_CACHED_DATA:TDM")
#pragma DATA_SECTION (DmaSlotCnt,   "NON_CACHED_DATA:TDM")

GpakCompandModes (*customCompandMode) (int port, int slot, int direction) = NULL;
#pragma DATA_SECTION (customCompandMode,  "NON_CACHED_DATA:TDM")
ADT_UInt16 TxSltsPerFrame[NUM_TDM_PORTS];  // Slots on TDM frame [fixed at build]
ADT_UInt16 TxDmaSlotCnt  [NUM_TDM_PORTS];  // Active DMA count [fixed at TDM configure]
#pragma DATA_SECTION (TxSltsPerFrame, "NON_CACHED_DATA:TDM")
#pragma DATA_SECTION (TxDmaSlotCnt,   "NON_CACHED_DATA:TDM")

ADT_UInt32 SlotCompandMode[NUM_TDM_PORTS][4]; // 0 == U-law, 1 == A-law
int UseSlotCmpMask[NUM_TDM_PORTS];
#pragma DATA_SECTION (SlotCompandMode,  "NON_CACHED_DATA:TDM")
#pragma DATA_SECTION (UseSlotCmpMask,   "NON_CACHED_DATA:TDM")

volatile ADT_UInt32 AccumDmaFlags = 0;      // accumulated DMA flag bits
volatile ADT_UInt32 MatchDmaFlags = 0;      // DMA flag bits needed to post SWI
#pragma DATA_SECTION (AccumDmaFlags,  "FAST_DATA_SECT:TDM")
#pragma DATA_SECTION (MatchDmaFlags,  "NON_CACHED_DATA:TDM")

TDM_Port *tdmPortFuncs = NULL;    // TDM functions for starting and stopping the TDM device
#pragma DATA_SECTION (tdmPortFuncs, "NON_CACHED_DATA:TDM")

GpakActivation tdmLoopback[NUM_TDM_PORTS];
#pragma DATA_SECTION (tdmLoopback, "NON_CACHED_DATA:TDM")

volatile ADT_UInt16 lbReady[NUM_TDM_PORTS];
#pragma DATA_SECTION (lbReady, "NON_CACHED_DATA:TDM")

// --------------------------------------------------------------------------
// BIOS SWI handle
static void GpakDmaSwi(void);
static SWI_Attrs swi_attr = { (SWI_Fxn)  GpakDmaSwi, 0, 0, 0, 0};
SWI_Handle SWI_Dma = NULL;

// --------------------------------------------------------------------------
// Sink and Drain Circular buffers for DMA copies to/from inactive timeslots
static CircBufInfo_t sinkCirc;
static CircBufInfo_t zeroesCirc;
CircBufInfo_t *pSink;
CircBufInfo_t *pDrain;

ADT_UInt16 sinkBuff[SINK_DRAIN_LEN_I16*3*2];
ADT_UInt16 drainBuff[SINK_DRAIN_LEN_I16*3*2];
#pragma DATA_SECTION (sinkCirc,   "FAST_DATA_SECT:TDM")
#pragma DATA_SECTION (zeroesCirc, "FAST_DATA_SECT:TDM")
#pragma DATA_SECTION (pSink,      "FAST_DATA_SECT:TDM")
#pragma DATA_SECTION (pDrain,     "FAST_DATA_SECT:TDM")
#pragma DATA_SECTION (sinkBuff,   "FAST_DATA_SECT:TDM")
#pragma DATA_ALIGN   (sinkBuff,  8)
#pragma DATA_SECTION (drainBuff,  "FAST_DATA_SECT:TDM")
#pragma DATA_ALIGN   (drainBuff, 8)

// --------------------------------------------------------------------------
//  Variables used for pending channel setup.
static CircBufInfo_t **ATxCircBuff,  **BTxCircBuff;   // Circular buffer pointers
static CircBufInfo_t **ARxCircBuff,  **BRxCircBuff;
#pragma DATA_SECTION (ATxCircBuff,  "FAST_DATA_SECT:TDM")
#pragma DATA_SECTION (ARxCircBuff,  "FAST_DATA_SECT:TDM")
#pragma DATA_SECTION (BTxCircBuff,  "FAST_DATA_SECT:TDM")
#pragma DATA_SECTION (BRxCircBuff,  "FAST_DATA_SECT:TDM")

static ADT_Word  ATxMask[MULTI_REG], BTxMask[MULTI_REG];   // Multi channel mask
static int SlotCnt;

// --------------------------------------------------------------------------
// Statistics
typedef struct PortStats {
   ADT_UInt32   StatID;

   ADT_UInt32   RxInts;
   ADT_UInt32   RxSlips;                   // Total slips detected
   ADT_UInt32   RxDmaErrors;               // Total number of Rx DMA buffers out of sync

   ADT_UInt32   TxInts;
   ADT_UInt32   TxSlips;                   // Total number of slips
   ADT_UInt32   TxDmaErrors;               // Total number of Tx DMA buffers out of sync

   ADT_UInt32   FrameSyncErrs;             // Total number of frame sync errors
   ADT_UInt32   StartCnt;                  // Count of times port has been started
   ADT_UInt32   FailureCnt;                // Count of times port has been failed

   ADT_UInt32   RetryCnt;
   ADT_UInt32   StartTime;
} PortStats;

PortStats TDMStats[NUM_TDM_PORTS];
#pragma DATA_SECTION (TDMStats, "STATS:TDM")

far int DmaSwInts = 0;
far int frameErrCnt = 0;        // Count of consecutive frame errors
far int frameErrorRetries = 0;  // Number of consecutive frame errors before restarting serial ports
#pragma DATA_SECTION (DmaSwInts, "SHARED_DATA_SECT:TDM")



#ifdef _DEBUG  // TDMVars structure
   #define LogChannel LogTransfer
typedef struct TDMVars_t {
   int   *TDMRate;

   ADT_PCM16 (*BSP0DMA_TxBuffer)[16], (*BSP1DMA_TxBuffer)[16], (*BSP2DMA_TxBuffer)[16];
   ADT_PCM16 (*BSP0DMA_RxBuffer)[16], (*BSP1DMA_RxBuffer)[16], (*BSP2DMA_RxBuffer)[16];

   PortStats   (*TDMStats)[NUM_TDM_PORTS];
   volatile ADT_UInt32 *AccumDmaFlags;
   volatile ADT_UInt32 *MatchDmaFlags;
   int           *DmaSwInts;
   ADT_Word      *DmaSwiStopCnt;
   int           *frameErrorRetries, *frameErrCnt;

   ADT_UInt16  (*SltsPerFrame)[NUM_TDM_PORTS];
   ADT_UInt16  (*MaxDmaSlots) [NUM_TDM_PORTS];
   ADT_UInt16  (*DmaSlotCnt)  [NUM_TDM_PORTS];
   ADT_UInt16  (*RxEvents)    [NUM_TDM_PORTS];
   ADT_UInt16  (*TxEvents)    [NUM_TDM_PORTS];

   ADT_UInt32 (*SlotCompandMode)[NUM_TDM_PORTS][4];
   int        (*UseSlotCmpMask)[NUM_TDM_PORTS];
   ADT_UInt16      const (*pSlotMap)[NUM_TDM_PORTS];    // Timeslot to DMA index
   CircBufInfo_t** const (*pCrcRxBuff)[NUM_TDM_PORTS];  // DMA Index to Circular Buffer
   CircBufInfo_t** const (*pCrcTxBuff)[NUM_TDM_PORTS];

   SWI_Handle    *SWI_Dma;
   CircBufInfo_t *sinkCirc, *zeroesCirc;

   ADT_Word      (*ATxMask)[MULTI_REG], (*BTxMask)[MULTI_REG];

   TDM_Port **tdmPortFuncs;

} TDMVars_t;

const TDMVars_t TDMVars = {
   &TDMRate,

   &BSP0DMA_TxBuffer, &BSP1DMA_TxBuffer, &BSP2DMA_TxBuffer, 
   &BSP0DMA_RxBuffer, &BSP1DMA_RxBuffer, &BSP2DMA_RxBuffer,

   (PortStats (*)[NUM_TDM_PORTS]) TDMStats, &AccumDmaFlags, &MatchDmaFlags,
   &DmaSwInts, &DmaSwiStopCnt, &frameErrorRetries, &frameErrCnt,
 
   (ADT_UInt16 (*)[NUM_TDM_PORTS]) SltsPerFrame,
   (ADT_UInt16 (*)[NUM_TDM_PORTS]) MaxDmaSlots,
   (ADT_UInt16 (*)[NUM_TDM_PORTS]) DmaSlotCnt,
   (ADT_UInt16 (*)[NUM_TDM_PORTS]) RxEvents,
   (ADT_UInt16 (*)[NUM_TDM_PORTS]) TxEvents,

   (ADT_UInt32 (*)[NUM_TDM_PORTS][4]) SlotCompandMode, 
   (int        (*)[NUM_TDM_PORTS]) UseSlotCmpMask, 
   (ADT_UInt16      const (*)[NUM_TDM_PORTS]) pSlotMap, 
   (CircBufInfo_t** const (*)[NUM_TDM_PORTS]) pCrcRxBuff, 
   (CircBufInfo_t** const (*)[NUM_TDM_PORTS]) pCrcTxBuff,

   &SWI_Dma,  &sinkCirc, &zeroesCirc,

   (ADT_Word (*)[MULTI_REG]) ATxMask,
   (ADT_Word (*)[MULTI_REG]) BTxMask,

   &tdmPortFuncs
};
#else
   #define LogChannel(a,b,c,d,e,f)
#endif

int TDMRetryCnt = 5;
int TDMRetryWindow = 100;
#pragma DATA_SECTION (TDMRetryCnt,    "SHARED_DATA_SECT:TDM")
#pragma DATA_SECTION (TDMRetryWindow, "SHARED_DATA_SECT:TDM")

#pragma CODE_SECTION (ResetMcBspStats, "SLOW_PROG_SECT")
void ResetMcBspStats(void) {

   int         port;
   PortStats   *pStats;

    SWI_disable();
    for (port = 0; port < NUM_TDM_PORTS; port++)  {
        pStats = &TDMStats[port];
        pStats->RxInts          = 0;
        pStats->RxSlips         = 0;
        pStats->RxDmaErrors     = 0;
        pStats->TxInts          = 0;
        pStats->TxSlips         = 0;
        pStats->TxDmaErrors     = 0;
        pStats->FrameSyncErrs   = 0;
        pStats->StartCnt      = 0;
    }
    SWI_enable();
}

//  Check for too many restarts within 100 millisecond time frame
#pragma CODE_SECTION (TDMErrorReset, "SLOW_PROG_SECT")
ADT_Bool TDMErrorReset (int port) { 
   PortStats* stats;
   stats = &TDMStats[port];

   // Fail if 5 or more retries within 100 milliseconds
   // Reset start time if 5 or fewer retries occur after more than 500 milliseconds.
   if (stats->StartTime) {
      stats->RetryCnt++;
      if (TDMRetryWindow < (CLK_getltime () - stats->StartTime)) {
         if (stats->RetryCnt < TDMRetryCnt) {
            stats->StartTime = 0;
         } else { 
            stats->FailureCnt++;
            StopSerialPortIo();
            SendWarningEvent (port, WarnTDMFailure);
            return ADT_FALSE;
         }
      }
   } else {
      stats->RetryCnt = 0;
      stats->StartTime = CLK_getltime ();
   }

   return ADT_TRUE;
}

#pragma CODE_SECTION (ReadPortStats, "SLOW_PROG_SECT")
void ReadPortStats(int port, PortStat_t *pSt) {
   PortStats   *pStats;
                               
    memset (pSt, 0, sizeof(PortStat_t));

    if (port == 0) return;
    if (port > NUM_TDM_PORTS) return;
    if (DmaSlotCnt[port-1] == 0) return;

    pStats = &TDMStats[port-1];

    SWI_disable();
    pSt->Status           = (GpakActivation) ((TxEvents [port-1] | RxEvents [port-1]) != 0);
    pSt->RxIntCount       = pStats->RxInts;
    pSt->RxSlips          = pStats->RxSlips;
    pSt->RxDmaErrors      = pStats->RxDmaErrors;
    pSt->TxIntCount       = pStats->TxInts;
    pSt->TxSlips          = pStats->TxSlips;
    pSt->TxDmaErrors      = pStats->TxDmaErrors;
    pSt->FrameSyncErrors  = pStats->FrameSyncErrs;
    if (pStats->StartCnt) pSt->RestartCount  = pStats->StartCnt - 1;
    SWI_enable();

}

#pragma CODE_SECTION (getSlotCompandMode, "SLOW_PROG_SECT")
// return the companding mode for a slot. Each bit in slotCompandMode array 
// represents 8 slots.
GpakCompandModes getSlotCompandMode(int port, int slot, int direction) {
int bitpos, word, val;

   if (port == SerialPortNull) 
      return cmpL8PcmU;

   if (customCompandMode != NULL)
      return (*customCompandMode) (port, slot, direction);

   if (!UseSlotCmpMask[port])
      return sysConfig.compandingMode[port];

   word = (slot/256) & 3;
   bitpos = ( (slot - (word*256)) /8) & 31;
   val = SlotCompandMode[port][word] & (1 << bitpos);

   if (val == 0)   return cmpL8PcmU;
   else            return cmpL8PcmA;
}

//{- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// InitGpakPcm - Initialize G.PAK PCM I/O.
//
// FUNCTION
//   This function initializes PCM I/O at system startup.
//
// RETURNS
//  nothing
//}
#pragma CODE_SECTION (InitGpakPcm, "SLOW_PROG_SECT")
void InitGpakPcm (void) {
   int port, j;
   ADT_Bool BspEnabled;
   ADT_UInt16 *SlotMap;
   CircBufInfo_t **CrcRxBuff;   // act Rcv bufrs
   CircBufInfo_t **CrcTxBuff;   // act Tx bufrs
 
   TDMRate = sysConfig.samplesPerMs * 1000;

#if (DSP_TYPE == 54)
   swi_attr.iscfxn = TRUE;
#endif
   swi_attr.mailbox = 0;
   swi_attr.priority = DMAPriority;
   SWI_Dma = SWI_create (&swi_attr);
   AppErr ("SWI Create", SWI_Dma == NULL);
 
   ApiBlock.DmaSwiCnt = 0;

   sinkCirc.pBufrBase  = sinkBuff;
   sinkCirc.BufrSize   = SAMPLES_PER_INTERRUPT*3*2;
   sinkCirc.PutIndex   = 0;
   sinkCirc.TakeIndex  = 0;
   sinkCirc.SlipSamps  = 0;
   pSink  = &sinkCirc;

   zeroesCirc.pBufrBase  = drainBuff;
   zeroesCirc.BufrSize   = SAMPLES_PER_INTERRUPT*3*2;
   zeroesCirc.PutIndex   = 0;
   zeroesCirc.TakeIndex  = 0;
   zeroesCirc.SlipSamps  = 0;
   pDrain  = &zeroesCirc;
   memset (drainBuff, 0, sizeof(drainBuff));

   // Initialize debug statistic variables.
   memset (TDMStats, 0, sizeof (TDMStats));

   // Perform TDM = DMA initialization
   DMAInitialize ();
    
   // Initialize each serial port.
   for (port = 0; port < NUM_TDM_PORTS; port++)  {
       disableDMA (port);
       BspEnabled = FALSE;
       if (tdmPortFuncs) 
            BspEnabled = tdmPortFuncs->StopIO (port);
       tdmLoopback[port] = Disabled;
       lbReady[port] = 0;
       TDMStats[port].StatID = 0x2220 | port;

       SlotMap = pSlotMap[port];

       UseSlotCmpMask[port] = 0;
       if (sysConfig.compandingMode[port] == cmpL8PcmU) {
            UseSlotCmpMask[port] = 1;
            SlotCompandMode[port][0] = 0;
            SlotCompandMode[port][1] = 0;
            SlotCompandMode[port][2] = 0;
            SlotCompandMode[port][3] = 0;
       } else if (sysConfig.compandingMode[port] == cmpL8PcmA) {
            UseSlotCmpMask[port] = 1;
            SlotCompandMode[port][0] = 0xFFFFFFFF;
            SlotCompandMode[port][1] = 0xFFFFFFFF;
            SlotCompandMode[port][2] = 0xFFFFFFFF;
            SlotCompandMode[port][3] = 0xFFFFFFFF;
       } 

       // Determine the number of time slots on the serial port's stream and
       // initialize configuration dependent serial port I/O parameters.
       if (sysConfig.numSlotsOnStream[port] != 0) {
          SltsPerFrame[port] = sysConfig.numSlotsOnStream[port];
          TxSltsPerFrame[port] = sysConfig.numSlotsOnStream[port];
          MaxDmaSlots[port]  = sysConfig.maxSlotsSupported[port];
          if (SltsPerFrame[port] < MaxDmaSlots[port])
             MaxDmaSlots[port] = SltsPerFrame[port];

          if (BspEnabled == TRUE)
             storeMcBSPConfiguration (port);

          // Indicate each slot is not configured.
          SlotMap = pSlotMap[port];
          for (j = 0; j < SltsPerFrame[port]; j++)
             SlotMap[j] = UNCONFIGURED_SLOT;
          // Indicate each slot is not configured.
          SlotMap = pTxSlotMap[port];
          for (j = 0; j < TxSltsPerFrame[port]; j++)
             SlotMap[j] = UNCONFIGURED_SLOT;

       } else  {
          SltsPerFrame[port] = 0;
          TxSltsPerFrame[port] = 0;
          MaxDmaSlots[port] = 0;
       }

       CrcRxBuff = pCrcRxBuff[port];
       CrcTxBuff = pCrcTxBuff[port];
       for (j = 0; j < MaxDmaSlots[port]; j++) {
            CrcRxBuff[j] = pSink;
            CrcTxBuff[j] = pDrain;
       }      

      // Indicate there are no slots currently configured for the port.
      DmaSlotCnt[port] = 0;
	  TxDmaSlotCnt[port] = 0;

   }

   return;
}


//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// StartSerialPortIo - Start serial port input/output.
//
// FUNCTION
//   This function sets up the serial ports and DMA controllers to start PCM I/O
//  after serial port configuration has occuured. It is also used to restart
//  I/O after a serial port error occurs.
//
// RETURNS
//  nothing
//}
#pragma CODE_SECTION (StartSerialPortIo, "SLOW_PROG_SECT")
void StartSerialPortIo (void) {
   //ADT_Bool RestartFail;
   int port, mask;
   ADT_UInt32 flags = 0;

   // Initialize the count of consecutive DMA Stopped occurrences.
   DmaSwiStopCnt = 0;


   if (!tdmPortFuncs) return;

   // Protect critical code from interruption.
   mask = HWI_disable();
   for (port = 0; port < NUM_TDM_PORTS; port++)  {

    //SS TSIP DEBUG      for (port = 0; port < 1; port++)  {
      //RestartFail = !TDMErrorReset (port);
      //if (RestartFail) break;
      if (tdmPortFuncs->SetupIO (port) == FALSE) continue;  // Write to Port configuration registers (port disabled)
      flags |= enableDMA  (port);  // Configure DMA registers and enable transfer events
      if (tdmPortFuncs->StartIO (port) == FALSE) continue;
      MatchDmaFlags = flags;
      TDMStats[port].StartCnt++;
   }  
   // Unprotect critical code.
   HWI_restore(mask);
   return;
}

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// StopSerialPortIo - Stop all serial port input/output.
//
// FUNCTION
//   This function stops all serial ports and DMA controllers.
//
// RETURNS
//  nothing
//}
#pragma CODE_SECTION (StopSerialPortIo, "SLOW_PROG_SECT")
void StopSerialPortIo (void) {

   int port, mask;

   if (!tdmPortFuncs) return;

   // Protect critical code from interruption.
   mask = HWI_disable();
   MatchDmaFlags = 0;
   for (port = 0; port < NUM_TDM_PORTS; port++)  {
      if (tdmPortFuncs->StopIO (port) == FALSE) continue;
      disableDMA (port);
   }
   HWI_restore(mask);
   return;
}

extern int updateTdmLoopback(GpakSerialPort_t port, GpakActivation state);

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// StartGpakPcm 
//
// FUNCTION
//   Starts PCM processing after configuration
//}
#pragma CODE_SECTION (StartGpakPcm, "SLOW_PROG_SECT")
void StartGpakPcm (ADT_UInt16 *DmaChanCnt) {

   int port, chan, chanCnt;
   CircBufInfo_t **CrcRxBuff;   // act Rcv bufrs
   CircBufInfo_t **CrcTxBuff;   // act Tx bufrs

   
   StopSerialPortIo();

   // Configure each serial port.
   for (port = 0; port < NUM_TDM_PORTS; port++) {

      TDMStats[port].RetryCnt = 0;

      chanCnt = *DmaChanCnt++;
      if((chanCnt == 0) && (TxDmaSlotCnt[port] == 0)) continue;
      //DmaSlotCnt[port] = chanCnt;
	  chanCnt = DmaSlotCnt[port];
      CrcRxBuff = pCrcRxBuff[port];
      // Initialize the port's active slot buffer pointers to sink/drain.
      for (chan = 0; chan < chanCnt; chan++) {
         CrcRxBuff[chan] = pSink;
      }

	  chanCnt = TxDmaSlotCnt[port];
      CrcTxBuff = pCrcTxBuff[port];

      // Initialize the port's active slot buffer pointers to sink/drain.
      for (chan = 0; chan < chanCnt; chan++) {
         CrcTxBuff[chan] = pDrain;
      }

   }
   DMAInitialize ();

   // Initialize the DMA phase counters.
   initDMAPhaseCounters ();

   AccumDmaFlags = 0;

   StartSerialPortIo();
   
   //for (port = 0; port < NUM_TDM_PORTS; port++) {
   //     updateTdmLoopback(port, 1);
   //     updateTdmLoopback(port, 0);
   //}
   //StopSerialPortIo();
   //StartSerialPortIo();
   return;
}



//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ValidateSlots - Verify slots are valid for PCM I/O.
//
// FUNCTION
//   This function verifies the specified slots are valid for PCM I/O and
//   determines the address of the first slot's active slot buffer pointer
//
//   Inputs
//        Port - Serial port Id
//   StartSlot - starting time slot within TDM frame
//    NumSlots - number of contiguous time slots (for circuit data)
//
//   Output
//     dmaCirc - pointer to update circular buffer address for DMA copy
//
// RETURNS
//    1 = Valid slot(s)
//    0 = Invalid port
//   -1 = Invalid first slot
//   -2 = Invalid contiguous slot
//   -3 = Invalid port word size
//}
#define RXSLOT TRUE
#define TXSLOT FALSE
#pragma CODE_SECTION (ValidateSlots, "SLOW_PROG_SECT")
static int ValidateSlots (GpakSerialPort_t Port, ADT_Word Slot,
                               ADT_Word SlotCnt, ADT_Bool RxFlag, 
                                    CircBufInfo_t ***circDma) {

   int DmaSlot;
   CircBufInfo_t **dmaCirc;
   ADT_UInt16 *SlotMap, SlotCntSltsPerFrame, DmaSlotCntPort;
   CircBufInfo_t **CrcRxBuff;   // act Rcv bufrs
   CircBufInfo_t **CrcTxBuff;   // act Tx bufrs

   CrcRxBuff = pCrcRxBuff[Port];
   CrcTxBuff = pCrcTxBuff[Port];

   if (RxFlag) {
      SlotCntSltsPerFrame = SltsPerFrame[Port];
	  DmaSlotCntPort = DmaSlotCnt[Port];
      SlotMap   = pSlotMap[Port];
      *circDma = &pSink;   
	  
   } else {
      SlotCntSltsPerFrame = TxSltsPerFrame[Port];
	  DmaSlotCntPort = TxDmaSlotCnt[Port];
      SlotMap   = pTxSlotMap[Port];
      *circDma = &pDrain;   
   }

   // Null port always valid
   if (Port == SerialPortNull) 
      return 1;

   // Verify the port is valid and was configured into build
   if (NUM_TDM_PORTS <= Port)
      return 0;

   if (SlotCntSltsPerFrame == 0)
      return 0;

   // Verify the number of slots and word size for circuit data channels
   if ((SlotCntSltsPerFrame <= Slot) ||
       (SlotMap[Slot] == UNCONFIGURED_SLOT))
      return -1;
   
   if (1 < SlotCnt) {
      if (DmaSlotCntPort <= (SlotMap[Slot] + SlotCnt - 1))
         return -2;
         
      if (sysConfig.serialWordSize[Port] != 8)
         return -3;
   }
   
   // Determine the address of the first slot's active slot buffer pointer.
   DmaSlot = SlotMap[Slot];
   AppErr ("DMA Slot", SlotCntSltsPerFrame <= DmaSlot);
   if (RxFlag)
      dmaCirc = &(CrcRxBuff[DmaSlot]);
   else 
      dmaCirc = &(CrcTxBuff[DmaSlot]);

   *circDma = dmaCirc;

   // Verify that there are sufficient quantity of contiguous slots
   while (0 < SlotCnt--)   {
      if ((RxFlag  && (*dmaCirc != pSink)) || 
          (!RxFlag && (*dmaCirc != pDrain)))
        return -4;
      dmaCirc++;
   }
   return 1;
}


#pragma CODE_SECTION (LocateTimeSlot, "SLOW_PROG_SECT")
static int LocateTimeSlot (GpakSerialPort_t Port, ADT_Word Slot,
                           ADT_Bool RxFlag, CircBufInfo_t ***dmaCirc) {

   int DmaSlot;
   ADT_UInt16 *SlotMap;
   CircBufInfo_t **CrcRxBuff;   // act Rcv bufrs
   CircBufInfo_t **CrcTxBuff;   // act Tx bufrs
   
   if (Port == SerialPortNull) {
       if (RxFlag)
           *dmaCirc = &pSink;   
       else
           *dmaCirc = &pDrain;   
      return -1;
   }
   AppErr ("McBSP port", NUM_TDM_PORTS <= Port || SltsPerFrame[Port] <= Slot);

   if (RxFlag) {
      SlotMap = pSlotMap[Port];
      DmaSlot = SlotMap[Slot];
      AppErr ("Dma slot", SltsPerFrame[Port] <= DmaSlot);
   } else {
      SlotMap = pTxSlotMap[Port];
      DmaSlot = SlotMap[Slot];
      AppErr ("Dma slot", TxSltsPerFrame[Port] <= DmaSlot);
   }
   CrcRxBuff = pCrcRxBuff[Port];
   CrcTxBuff = pCrcTxBuff[Port];

   if (RxFlag)
      *dmaCirc = &(CrcRxBuff[DmaSlot]);
   else 
      *dmaCirc = &(CrcTxBuff[DmaSlot]);
   return DmaSlot;

}
//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ActivateChannel 
//
// FUNCTION
//   This function activates a G.PAK channel by linking the channel to the
//   appropriate framing rate queues and linking the channel's PCM buffers to
//   the PCM DMA I/O.
//
// RETURNS
//  Status code indicating success or a specific error.
//

//
//  Pending channel setup data
//
//  The following data is used to delay channel setup completion until all McBSPs are phased aligned.
//}


#pragma FUNC_CANNOT_INLINE (channelSetupComplete)
#pragma CODE_SECTION (channelSetupComplete, "FAST_PROG_SECT")
ADT_Bool channelSetupComplete (void) { return PendingChannel == NULL; }

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// pendingChannelSetup 
//
// FUNCTION
//   This function completes a channel setup that was started by the messaging task.
//   Specifically, it 
//      1) synchronizes the circular buffer pointers with the DMA linear
//         buffer phase count
//      2) adds the channel to the framing task queue, and
//      3) adds the timeslot to the DMA processing.
//}
#undef  Dec
#define Enc        chan->pcmToPkt
#define pairedEnc  pairedChan->pcmToPkt
#define EncInBuff  chan->pcmToPkt.activeInBuffer
#define EncOutBuff chan->pcmToPkt.outbuffer

#define Dec        chan->pktToPcm
#define pairedDec  pairedChan->pktToPcm
#define DecInBuff  chan->pktToPcm.inbuffer
#define DecOutBuff (&chan->pktToPcm.outbuffer)

#pragma CODE_SECTION (pendingChannelSetup, "FAST_PROG_SECT")
#define MAX_U32  ((ADT_UInt64)4294967295U)
#define MAX_U33  ((ADT_UInt64)8589934591U)
void pendingChannelSetup (void) {

   chanInfo_t *chan;
   chanInfo_t *pairedChan;

   ADT_UInt16 EncPhase;      // pointer to Receive DMA phase count
   ADT_UInt16 DecPhase;      // pointer to Transmit DMA phase count
   ADT_UInt16 pairedEncPhase;      // pointer to Receive DMA phase count
   ADT_UInt16 pairedDecPhase;      // pointer to Transmit DMA phase count
   int i;
   ADT_UInt64 ts64, maxTS = MAX_U32;
   ADT_UInt16 rtpSamplingRate;
   chan = (chanInfo_t *) PendingChannel;
   rtpSamplingRate = RtpChanData[chan->ChannelId]->Instance.SampleRate;
  
   //---------------------------------------------------------------------
   // Adjust circular buffer's pointers using phase count
   //---------------------------------------------------------------------
   EncPhase = FrameRatePhaseCount (chan->CoreID, Enc.SamplesPerFrame);
   DecPhase = FrameRatePhaseCount (chan->CoreID, Dec.SamplesPerFrame);

   if (chan->channelType == packetToPacket) {
      pairedChan = chanTable[chan->PairedChannelId];
      pairedEncPhase = FrameRatePhaseCount (pairedChan->CoreID, pairedEnc.SamplesPerFrame);
      pairedDecPhase = FrameRatePhaseCount (pairedChan->CoreID, pairedDec.SamplesPerFrame);

      setCircPointersPktPkt (chan, &Enc, &Dec, &pairedEnc, &pairedDec, EncPhase, DecPhase, pairedEncPhase, pairedDecPhase);

        //RMF  Todo -  Fix time stamp for channel skip
#if 0
      Enc.FrameTimeStamp = (ApiBlock.DmaSwiCnt * sysConfig.samplesPerMs) - EncPhase;
      Dec.FrameTimeStamp = (ApiBlock.DmaSwiCnt * sysConfig.samplesPerMs) - DecPhase;

      pairedEnc.FrameTimeStamp = (ApiBlock.DmaSwiCnt * sysConfig.samplesPerMs) - pairedEncPhase;
      pairedDec.FrameTimeStamp = (ApiBlock.DmaSwiCnt * sysConfig.samplesPerMs) - pairedDecPhase;
#else
	  ts64 = ApiBlock.DmaSwiCnt;
      ts64 *= (ADT_UInt64)sysConfig.samplesPerMs;
	  if((rtpSamplingRate == 8000) && (sysConfig.samplesPerMs == 16)) {
		  maxTS = MAX_U33;
	  }
      if (ts64 > maxTS) {
         ts64 = ts64 - maxTS - 1;
      } else {
         ts64 = ts64;
      }

	  Enc.FrameTimeStamp = ts64 - EncPhase;
	  Dec.FrameTimeStamp = ts64 - DecPhase;
      pairedEnc.FrameTimeStamp = ts64 - pairedEncPhase;
      pairedDec.FrameTimeStamp = ts64 - pairedDecPhase;
#endif
      chan->Flags       |= CF_ACTIVE;
      pairedChan->Flags |= CF_ACTIVE;

      LogChannel (0xE0100u, EncPhase,                      (void *) DecPhase,  0, 0, chan->channelType);
      LogChannel (0xE0200u, (void *) Enc.SamplesPerFrame, Dec.SamplesPerFrame, 0, 0, chan->ChannelId);
      LogChannel (0xE0100u, pairedEncPhase,           (void *) pairedDecPhase, 0, 0, pairedChan->channelType);
      LogChannel (0xE0200u, (void *) pairedEnc.SamplesPerFrame, pairedDec.SamplesPerFrame, 0, 0, pairedChan->ChannelId);
      return;

   }

   setCircPointers (chan, &Enc, &Dec, EncPhase, DecPhase);

   //---------------------------------------------------------------------
   // Link channel's PCM buffers to DMA buffer
   //---------------------------------------------------------------------
   if (ARxCircBuff != &pSink) {
      for (i = 0; i < SlotCnt; i++) {
        *ARxCircBuff++ = EncInBuff;
      }
   }

   if (BRxCircBuff != &pSink) {
      *BRxCircBuff = DecInBuff;
   }

   if (ATxCircBuff != &pDrain) {
      *ATxCircBuff = EncOutBuff;
      SetTransmitEnables (Enc.OutSerialPortId, ATxMask);
   }

   if (BTxCircBuff != &pDrain) {
      for (i = 0; i < SlotCnt; i++) {
        *BTxCircBuff++ = DecOutBuff;
      }
      SetTransmitEnables (Dec.OutSerialPortId, BTxMask);
   }
#if 0
   //RMF  Todo -  Fix time stamp for channel skip
   Enc.FrameTimeStamp = (ApiBlock.DmaSwiCnt * sysConfig.samplesPerMs) - EncPhase;
   Dec.FrameTimeStamp = (ApiBlock.DmaSwiCnt * sysConfig.samplesPerMs) - DecPhase;
#else
	  ts64 = ApiBlock.DmaSwiCnt;
      ts64 *= (ADT_UInt64)sysConfig.samplesPerMs;
	  if((rtpSamplingRate == 8000) && (sysConfig.samplesPerMs == 16)) {
		  maxTS = MAX_U33;
	  }
      if (ts64 > maxTS) {
         ts64 = ts64 - maxTS - 1;
      } else {
         ts64 = ts64;
      }
   Enc.FrameTimeStamp = ts64 - EncPhase;
   Dec.FrameTimeStamp = ts64 - DecPhase;

#endif
   chan->Flags |= CF_ACTIVE;

   LogChannel (0xE0100u | chan->channelType, EncPhase, (void *) DecPhase, 0, 0, 0);
   LogChannel (0xE0200u | chan->ChannelId, (void *) Enc.SamplesPerFrame, Dec.SamplesPerFrame, 0, 0, 0);

}
#undef Enc
#undef EncInBuff
#undef EncOutBuff

#undef Dec
#undef DecInBuff
#undef DecOutBuff




#define Enc        chan->pcmToPkt
#define pairedEnc  pairedChan->pcmToPkt
#define EncInBuff  chan->pcmToPkt.activeInBuffer
#define EncOutBuff chan->pcmToPkt.outbuffer

#define Dec        chan->pktToPcm
#define pairedDec  pairedChan->pktToPcm
#define DecInBuff  chan->pktToPcm.inbuffer
#define DecOutBuff (&chan->pktToPcm.outbuffer)

#pragma CODE_SECTION (ActivateChannel, "SLOW_PROG_SECT")
GPAK_ChannelConfigStat_t ActivateChannel (chanInfo_t *chan) {

   int Status;

   //---------------------------------------------------------------------
   // Verify that the requested time slots are available on the ports and
   // obtain the pointer for the circular buffer address used by the DMA copy
   //---------------------------------------------------------------------
   while (!channelSetupComplete());  // Wait for previous channel setup to be complete.

   if (chan->channelType == circuitData)
      SlotCnt = Enc.OutSlotId_MuxFact;
   else
      SlotCnt = 1;

   if ((Status = ValidateSlots (Enc.InSerialPortId, Enc.InSlotId,
                                SlotCnt, RXSLOT, &ARxCircBuff)) != 1) {
      if (Status == 0)         return Cc_InvalidInputPortA;
      else if (Status == -1)   return Cc_InvalidInputSlotA;
      else if (Status == -2)   return Cc_InvalidInputCktSlots;
      else if (Status == -3)   return Cc_InvalidInCktPortSize;
      else                     return Cc_BusyInputSlotA;
   }
   Enc.InCompandingMode =  getSlotCompandMode(Enc.InSerialPortId, Enc.InSlotId, 0);

   if ((Status = ValidateSlots (Enc.OutSerialPortId, Enc.OutSlotId_MuxFact,
                                SlotCnt, TXSLOT, &ATxCircBuff)) != 1)   {
      if (Status == 0)         return Cc_InvalidOutputPortB;
      else if (Status == -1)   return Cc_InvalidOutputSlotB;
      else                     return Cc_BusyOutputSlotB;
   }
   Enc.OutCompandingMode =  getSlotCompandMode(Enc.OutSerialPortId, Enc.OutSlotId_MuxFact, 1);

   if ((Status = ValidateSlots (Dec.InSerialPortId, Dec.InSlotId_MuxFact,
                                SlotCnt, RXSLOT, &BRxCircBuff)) != 1) {
      if (Status == 0)         return Cc_InvalidInputPortB;
      else if (Status == -1)   return Cc_InvalidInputSlotB;
      else                     return Cc_BusyInputSlotB;
   }
   Dec.InCompandingMode =  getSlotCompandMode(Dec.InSerialPortId, Dec.InSlotId_MuxFact, 0);
     
   if ((Status = ValidateSlots (Dec.OutSerialPortId, Dec.OutSlotId,
                                 SlotCnt, TXSLOT, &BTxCircBuff)) != 1) {

      if (Status == 0)         return Cc_InvalidOutputPortA;
      else if (Status == -1)   return Cc_InvalidOutputSlotA;
      else if (Status == -2)   return Cc_InvalidOutputCktSlots;
      else if (Status == -3)   return Cc_InvalidOutCktPortSize;
      else                     return Cc_BusyOutputSlotA;
   }
   Dec.OutCompandingMode =  getSlotCompandMode(Dec.OutSerialPortId, Dec.OutSlotId, 1);

   if ((ARxCircBuff != &pSink) && (ARxCircBuff == BRxCircBuff))  
      return Cc_BusyInputSlotB; // Same Rx time slot for A and B sides

   if ((ATxCircBuff != &pDrain) && (ATxCircBuff == BTxCircBuff))  
      return Cc_BusyOutputSlotB; // Same Tx time slot for A and B sides

   GetMasks (Enc.OutSerialPortId, Enc.OutSlotId_MuxFact, SlotCnt, &ATxMask[0]);
   GetMasks (Dec.OutSerialPortId, Dec.OutSlotId,         SlotCnt, &BTxMask[0]);

   //---------------------------------------------------------------------
   //  Place channel in pre-pending framing task queue.
   //---------------------------------------------------------------------
   NumActiveChannels ++;
   SWI_disable();
   AddtoPrePendQueue (chan); 

   //---------------------------------------------------------------------
   //  Notify DMASWI to complete channel setup
   //---------------------------------------------------------------------
   PendingChannel = chan;
   SWI_enable();
   logTime (0x200E0000ul | chan->ChannelId);
   return (Cc_Success);
}

#pragma CODE_SECTION (ActivateChannelPktPkt, "SLOW_PROG_SECT")
GPAK_ChannelConfigStat_t ActivateChannelPktPkt (chanInfo_t *chan) {

   chanInfo_t    *pairedChan;

   //---------------------------------------------------------------------
   // Verify that the requested time slots are available on the ports and
   // obtain the pointer for the circular buffer address used by the DMA copy
   //---------------------------------------------------------------------
   while (!channelSetupComplete());  // Wait for previous channel setup to be complete.

   ARxCircBuff = &pSink;
   BRxCircBuff = &pSink;
   ATxCircBuff = &pDrain;
   BTxCircBuff = &pDrain;

   pairedChan = chanTable[chan->PairedChannelId];
   
   //---------------------------------------------------------------------
   //  Place both sides of pkt pkt channel in the pre-pending queue.
   //
   //  SWI_disable prevents framing task from running
   //  before channel setup is complete
   //---------------------------------------------------------------------
   logTime (0x200E0000ul | chan->ChannelId);
   NumActiveChannels += 2;

   // Lock out frame processing on the channels until they have been added to the queues
   SWI_disable();
   AddtoPrePendQueue (chan); 
   AddtoPrePendQueue (pairedChan); 

   //---------------------------------------------------------------------
   //  Notify DMASWI to complete channel setup
   //---------------------------------------------------------------------
   PendingChannel = chan;
   SWI_enable();

   logTime (0x200E0000ul | pairedChan->ChannelId);
   return (Cc_Success);
}
#undef Enc
#undef EncInBuff
#undef EncOutBuff

#undef Dec
#undef DecInBuff
#undef DecOutBuff

#pragma CODE_SECTION (ticksPerChannelFrame, "SLOW_PROG_SECT")
unsigned long ticksPerChannelFrame (chanInfo_t *chan) {

   int samplesPerFrame;

   samplesPerFrame = chan->pcmToPkt.SamplesPerFrame;
   if (samplesPerFrame < chan->pktToPcm.SamplesPerFrame)
      samplesPerFrame = chan->pktToPcm.SamplesPerFrame;

   return (samplesPerFrame / sysConfig.samplesPerMs) * CLK_countspms();
}

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// DeactivateChannel - Deactivate a G.PAK Channel.
//
// FUNCTION
//   This function deactivates a G.PAK channel by unlinking the channel from the
//   framing rate queues and unlinking the channel's PCM buffers from the PCM
//  DMA I/O.
//
// RETURNS
//  nothing
//}
#define Enc chan->pcmToPkt
#define Dec chan->pktToPcm

#pragma FUNC_CANNOT_INLINE (dequeuedSuccessfully)
#pragma CODE_SECTION (dequeuedSuccessfully, "SLOW_PROG_SECT")
int dequeuedSuccessfully (chanInfo_t *chan) {
  return (chan->algCtrl.ControlCode == ALG_CTRL_DEQUEUE_CMPLT);
}

// -----jdc debug --------------------
#pragma  CODE_SECTION (reset_deact_state, "MEDIUM_PROG_SECT")
ADT_UInt16 reset_deact_state(ADT_UInt16 InPortId, ADT_UInt16 InSlotId, ADT_UInt16 OutPortId, ADT_UInt16 OutSlotId) {

   CircBufInfo_t **ARxCircBuff;
   CircBufInfo_t **ATxCircBuff; 
   ADT_UInt16 rv = 0;

   LocateTimeSlot ((GpakSerialPort_t)InPortId,  InSlotId,  RXSLOT, &ARxCircBuff);
   LocateTimeSlot ((GpakSerialPort_t)OutPortId, OutSlotId, TXSLOT, &ATxCircBuff);

   SWI_disable ();  // Prevent higher priority framing tasks access causing lock-out
   if (ATxCircBuff != &pDrain) {
        *ATxCircBuff = pDrain;
        rv |= 1;
   }

   if (ARxCircBuff != &pSink) {
        *ARxCircBuff = pSink;
        rv |= 2;
   }
   SWI_enable ();

   return rv;
}
// ----------------------------------


#pragma CODE_SECTION (DeactivateChannel, "SLOW_PROG_SECT")
GPAK_TearDownChanStat_t DeactivateChannel (chanInfo_t *chan) {
   int i; 
   int  SlotCnt;
   ADT_UInt16 *SlotMap;
   ADT_UInt32 start, ticks, duration;

   chanInfo_t    **EncHead,  **DecHead; 

   CircBufInfo_t **ARxCircBuff,  **BRxCircBuff;
   CircBufInfo_t **ATxCircBuff,  **BTxCircBuff; 

   ADT_Word        ATxMcBsp,       BTxMcBsp;
   ADT_Word        ATxBufrIdx,     BTxBufrIdx;    // 

   ADT_Word     ATxMask[MULTI_REG], BTxMask[MULTI_REG];   // Multi channel mask
   
   ATxMcBsp = Enc.OutSerialPortId;
   BTxMcBsp = Dec.OutSerialPortId;
   
   if (chan->channelType == circuitData)
      SlotCnt = Enc.OutSlotId_MuxFact;
   else
      SlotCnt = 1;

   while (!channelSetupComplete());  // Wait for previous channel setup to be complete.


   LocateTimeSlot (Enc.InSerialPortId,  Enc.InSlotId,          RXSLOT, &ARxCircBuff);
   LocateTimeSlot (Enc.OutSerialPortId, Enc.OutSlotId_MuxFact, TXSLOT, &ATxCircBuff);

   LocateTimeSlot (Dec.InSerialPortId,  Dec.InSlotId_MuxFact,  RXSLOT, &BRxCircBuff);
   LocateTimeSlot (Dec.OutSerialPortId, Dec.OutSlotId,         TXSLOT, &BTxCircBuff);

   if (ATxCircBuff != &pDrain) {
      teardownLog(chan->ChannelId, 2);
      GetMasks (Enc.OutSerialPortId, Enc.OutSlotId_MuxFact, SlotCnt, &ATxMask[0]);
      SlotMap   = pSlotMap[ATxMcBsp];
      ATxBufrIdx = SlotMap[Enc.OutSlotId_MuxFact];
   }

   if (BTxCircBuff != &pDrain) {
      GetMasks (Dec.OutSerialPortId, Dec.OutSlotId, SlotCnt, &BTxMask[0]);
      SlotMap   = pSlotMap[BTxMcBsp];
      BTxBufrIdx = SlotMap[Dec.OutSlotId];
   }

   switch (chan->channelType) {
   case conferenceMultiRate:
      EncHead = &(GetGpakCnfr (chan->CoreID, chan->PairedChannelId)->MemberList);
      DecHead = GetPktToPcmQueueHead (chan->CoreID, Dec.SamplesPerFrame);
      break;

   case conferencePcm:
   case conferencePacket:
      EncHead = &(GetGpakCnfr (chan->CoreID, chan->PairedChannelId)->MemberList);
      DecHead = NULL;
      break;

   case conferenceComposite:
      EncHead = &(GetGpakCnfr (chan->CoreID, chan->PairedChannelId)->CompositeList);
      DecHead = NULL;
      break;
  
   default:
      teardownLog(chan->ChannelId, 3);
      EncHead = GetPcmToPktQueueHead (chan->CoreID, Enc.SamplesPerFrame);
      DecHead = GetPktToPcmQueueHead (chan->CoreID, Dec.SamplesPerFrame);
      break;
   }
   

   CHN_lock (chan);   // Ensure that framing is not actively processing this channel before tearing it down

   if (ATxCircBuff != &pDrain) {      
      // Disable channel's time slot.  
      ClearTransmitEnables (ATxMcBsp, ATxMask);

      for (i = 0; i < SlotCnt && ATxCircBuff != &pDrain; i++) {
           // Clear DMA transmission slot
           zeroTxBuffer (ATxMcBsp, ATxBufrIdx++);

          // Remove channel from DMA copy list
          *ATxCircBuff = pDrain;
           ATxCircBuff++;  
          teardownLog(chan->ChannelId, 4);

      }
  }

   if (BTxCircBuff != &pDrain) {
      // Disable channel's time slot.
      ClearTransmitEnables (BTxMcBsp, BTxMask);

      // Clear DMA transmission slot
      zeroTxBuffer (BTxMcBsp, BTxBufrIdx);

      // Remove channel from DMA copy
      *BTxCircBuff = pDrain;
   }


   // Remove time slots from DMA copy list
   if (ARxCircBuff != &pSink) {

      for (i = 0; i < SlotCnt; i++) {
         *ARxCircBuff = pSink;
          ARxCircBuff++;
          teardownLog(chan->ChannelId, 5);
      }
   }
   
   if (BRxCircBuff != &pSink)
      *BRxCircBuff = pSink;

   DequeueChannel (chan, EncHead, DecHead);
   CHN_unlock (chan);

   // Wait for channel to dequeue.
   // Return failure if timeout before dequeueing
   start = CLK_gethtime ();
   ticks = ticksPerChannelFrame (chan) * 5;
   do {
     duration = CLK_gethtime () - start;
     if (dequeuedSuccessfully (chan)) break;

     if (ticks < duration) {
       AppErr ("Dequeue Failure", NumActiveChannels);
       NumActiveChannels --;
       return Td_TimeOut;
     }

     if (SysAlgs.rtpNetDelivery) TSK_sleep (1);

   } while (TRUE);
   teardownLog(chan->ChannelId, 7);

   logTime (0x200D0000ul | chan->ChannelId);

   LogBuff (0x1000DA00u, chan->pcmToPkt.activeInBuffer);  
   LogBuff (0x1000DA00u, chan->pcmToPkt.outbuffer);
   LogBuff (0x1000DA00u, &chan->pcmToPkt.ecFarPtr);
   LogBuff (0x1000DB00u, chan->pktToPcm.inbuffer);  
   LogBuff (0x1000DB00u, &chan->pktToPcm.outbuffer);
   LogBuff (0x1000DB00u, &chan->pktToPcm.ecFarPtr);
   NumActiveChannels --;

   return Td_Success;
}
#undef Enc
#undef Dec


//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// GpakDmaSwi - G.PAK DMA Software Interrupt.
//
// FUNCTION
//   This function is the G.PAK Software Interrupt handler to support DMA. It is
//   triggered by a set of DMA hardware interrupts to move PCM data between the
//  serial ports and PCM circular buffers. The set of interrupts occur once per
//  msec corresponding to the collection of 8 PCM samples for each enabled time
//  slot.
//
// RETURNS
//  nothing
//}

#pragma CODE_SECTION (GpakDmaSwi, "FAST_PROG_SECT")
void GpakDmaSwi (void) {
   int events, TxEvent, RxEvent;
   int port, slotCnt;
   int frameErr, lbSync, slip;
   CircBufInfo_t **CrcRxBuff;   // act Rcv bufrs
   CircBufInfo_t **CrcTxBuff;   // act Tx bufrs
   PortStats *portStats;

   logTime (0x8000000Ful);
   events = SWI_getmbox ();

   if (MatchDmaFlags == 0) {
      return;
   }
   logTime (0x01000000ul |  events);

   // Copy PCM between channel circular buffers and DMA linear buffers
   for (port = 0; port < NUM_TDM_PORTS; port++)  {
      slotCnt = DmaSlotCnt[port];
      if(slotCnt < TxDmaSlotCnt[port])
	     slotCnt = TxDmaSlotCnt[port];
	  if (slotCnt == 0) continue;

      portStats = &TDMStats[port];  
      TxEvent = events & TxEvents [port];
      RxEvent = events & RxEvents [port];

      CrcRxBuff = pCrcRxBuff[port];
      CrcTxBuff = pCrcTxBuff[port];

      if (tdmLoopback[port] == Disabled) {
         //=============================================================
         // Tdm Loopback is disabled... process Tx followed by Rx
         slip = 0;
         if (TxEvent) {
            portStats->TxInts++;
            if (TxEvent & AccumDmaFlags) {
               portStats->TxSlips++;
               //TDMErrorReset (port);
               SendWarningEvent (port, WarnTxDmaSlip);
               slip = 1;
            }
            slotCnt = TxDmaSlotCnt[port];
            if (CopyDmaTxBuffers (port, slotCnt, &CrcTxBuff[0], slip)){
               portStats->TxDmaErrors++;
               //TDMErrorReset (port);
            }
         }
        
         slip = 0;
         if (RxEvent) {
            portStats->RxInts++;
            if (RxEvent & AccumDmaFlags) {
               portStats->RxSlips++;
               //TDMErrorReset (port);
               SendWarningEvent (port, WarnRxDmaSlip);
               slip = 1;
            }
            slotCnt = DmaSlotCnt[port];
            if (CopyDmaRxBuffers (port, slotCnt, &CrcRxBuff[0], slip)){
               portStats->RxDmaErrors++;
               //TDMErrorReset (port);
            }
         }

      } else {
         //=============================================================
         // Tdm Loopback is enabled... process Rx and Tx together
         if (RxEvent) portStats->RxInts++;
         if (TxEvent) portStats->TxInts++;
                 
         lbReady[port] |= (TxEvent | RxEvent);
         if (lbReady[port] == (RxEvents [port] | TxEvents [port])) {
            if (RxEvent & AccumDmaFlags) {
               portStats->RxSlips++;
               SendWarningEvent (port, WarnRxDmaSlip);
            }
            if (TxEvent & AccumDmaFlags) {
               portStats->TxSlips++;
               SendWarningEvent (port, WarnTxDmaSlip);
            }
            lbSync = DmaLbRxToTxBuffers (port, slotCnt);
            if (lbSync & 1) portStats->RxDmaErrors++;
            if (lbSync & 2) portStats->TxDmaErrors++;
            lbReady[port] = 0;
         }
      }
      //=============================================================
      AccumDmaFlags |= (TxEvent | RxEvent);
   }

   if (AccumDmaFlags == MatchDmaFlags) {
      // Determine if a framing error was detected on any active serial port.
      for (port = 0; port < NUM_TDM_PORTS; port++)  {
         if (DmaSlotCnt[port] == 0) continue;

         frameErr = McBSPFrameError (port);
         if (frameErr == 0) continue;
         STATS (portStats->FrameSyncErrs++;)
         if (frameErrCnt++ < frameErrorRetries) continue;
         
         LogTransfer (0x80fe0000, 0, 0, port, frameErr, 0);
         SendWarningEvent (port, WarnFrameSyncError);

         StopSerialPortIo();
         StartSerialPortIo();
         frameErrCnt = 0;
         break;
      }

      AccumDmaFlags = 0;
         
      // Schedule the framing tasks.
      ScheduleFramingTasks();

      if (SysAlgs.apiCacheEnable) {
            ApiBlock.pWrBlock->DmaSwiCnt = ApiBlock.DmaSwiCnt;
            CACHE_WBINV(ApiBlock.pWrBlock, CACHE_L2_LINE_SIZE); 
      }

      DmaSwInts++;

   }
   logTime (0x01010000ul | AccumDmaFlags);
   logTime (0x8001000Ful);
   return;
}

