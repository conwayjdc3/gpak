//
//{ GpakTsip.c
//
//  Each TSIP device can support: 
//      2 physical buses of 512 bytes per frame,
//      4 physical buses of 256 bytes per frame, or 
//      8 phyiscal buses of 128 bytes per frame
//  that are multiplexed into a single 'virtual' bus of 1024 bytes per 8 kHz frame.  Selection of the number
//  of pyhsical buses or 'links' is accomplished via the txDataRateN field in the TSIP configuration parameters.
//
//  To avoid mixed 8-bit and 16-bit data elememts, all TDM bus transfers are performed as 8-bit 
//  linear transfers into the linear TDM buffers.
//
//  Channel data may be arranged on the TDM bus in one of four formats: 8-bit narrow band, 8-bit wide band,
//  16-bit narrow band, or 16-bit wide band.  All channel data on all TDM buses must use the same format.
//  Data is transfered between the 8-bit TDM buffers to the 16-bit circular channel buffers using one
//  of four transfer routines selected by the TDM data format.
//
//  Data on TDM devices can be configured to be either in a companded or linear format.  The selection of
//  companded or linear data format is universal across all TDM devices and timeslots.
//
//  When the data is in a companded format, software companding is performed by the framing tasks to
//  convert between 16-bit linear and u-law or a-law companding modes.  Companding is configured by 
//  timeslot groups - 8 slots per group.  Each group of 8 consecutive timeslots can be configured for 
//  either u-law or a-law companding.  During channel setup, channels can be configured as a data channels
//  which will disable the framing task's software companding on those channels.
//}

#include <std.h>
#include <hwi.h>
#include <swi.h>
#include <clk.h>
#include <c64.h>
#include <string.h>
#include <csl_tsip.h>

#include "GpakDefs.h"
#include "GpakExts.h"
#include "64plus.h"
#include "GpakPcm.h"
#include "GpakHpi.h"

#if !defined (_DEBUG) && !defined (_LOG) && !defined (TEST)
   #define logDMA(a,b,c)
#else
   extern logDMA (char* type, int port, ADT_UInt64 event);
#endif

#ifdef TEST   // disable static and inline identifiers
   #define static
   #define inline
#endif

typedef void (DMA_TRANSFER) ();



//{  #defines
#define TSIP_STAT_ID             0x55550001
#define TSIP_CORE_ID                1
#define TSIP_FRAMES_PER_INTERRUPT   8    // Fixed rate of TSIP.
#define DMA_TRANSFER_PRIORITY       0
#define MAX_DMA_TRANSFER_PRIORITY   0
#define TSIP_SYNC_I8                4           // Size of TSIP Frame ID

#define ULAW_ZERO                   0xFF
#define ALAW_ZERO                   0x55

#define TS_INT_DLY      0    // timeslot delay before generating interrupt 0-value == 1 slot delay
#define ACK_ONLY        0
#define ACK_AND_DLY     (0xC000 | TS_INT_DLY)  // generate superframe interrupt on ACK and DLY
#define INT_MODE        ACK_ONLY

#define DMAENB  1   // DMA enable bit in TDMU Global Control
#define CHNENB  1   // CHNENB bit in Dma channel enable register 
#define XMTENB  1   // XMTENB bit in SIU control register
#define RCVENB  2   // RCVENB bit in SIU control register
#define DMARST  2   // DMARST bit in reset register
#define SIURST  1   // SIURST bit in reset register
#define EMUSOFT 2   // SOFT bit of Emulation and Test register

// Register access macro
#define DRCH  DRCH[tdmCore]
#define DXCH  DXCH[tdmCore]
#define RBM   RBM[tdmCore]
#define XBM   XBM[tdmCore]
#define RCHN  RCHEN[tdmCore]
#define XCHN  XCHEN[tdmCore]
#define ERR   ERR[tdmCore]

//}

//{  externals
extern volatile ADT_UInt16 AccumDmaFlags;
extern volatile ADT_UInt16 MatchDmaFlags; 

extern ADT_UInt16 TsipBuflenI8[];
extern ADT_UInt16 TsipFallocI8[];

extern ADT_UInt16 RxEvents [];
extern ADT_UInt16 TxEvents [];

extern void PowerOnDevice (int device);


// dma buffer to channel buffer transfers
extern DMA_TRANSFER dmaToCirc8BitNB;
extern DMA_TRANSFER dmaToCirc8BitWB;
extern DMA_TRANSFER dmaToCirc16BitNB;
extern DMA_TRANSFER dmaToCirc16BitWB;
extern DMA_TRANSFER* dmaToCirc;

// channel buffer to DMA buffer transfers
extern DMA_TRANSFER circToDma8BitNB;
extern DMA_TRANSFER circToDma8BitWB;
extern DMA_TRANSFER circToDma16BitNB;
extern DMA_TRANSFER circToDma16BitWB;
extern DMA_TRANSFER* circToDma;
//}

static ADT_Bool stopTsip (int port);
static ADT_Bool startTsip (int port);
static ADT_Bool setupTsip (int port);


static int firstTime = ADT_TRUE;
#pragma DATA_SECTION (firstTime, "NON_CACHED_DATA")

// NOTE:  Least significant bit controls whether A (bit=0) or B (bit=1) configuration is active
static ADT_UInt8 TSIPCfgID = 0xAA;


typedef struct TSIPModeCfg_t {
    ADT_UInt16 tdmSampleSizeI8;   // Bytes per sample on tdm bus (and dma buffer)
    ADT_UInt16 sampleRatekHz;     // Samples per kHz on TDM bus
    ADT_UInt16 compandedData;     // Data on TDM bus is companded
    ADT_UInt16 dmaChannelSizeI8;  // Bytes per channel in dma buffer
    TSIPMode_t mode;
} TSIPModeCfg_t;

const TSIPModeCfg_t TSIPCfg = {
   1,          // TDM bus bytes per sample
   8,          // Samples per kHz on TDM bus 
   ADT_TRUE,   // TDM bus data is companded
   1,          // Bytes per channel in dma buffer
   tsip8BitNB  // Format of samples on TDM bus
};
#pragma DATA_SECTION (TSIPCfg, "TSIPCFG")

TSIPModeCfg_t* const tsipCfg = (TSIPModeCfg_t *) &TSIPCfg;  // Allow access to modifiable TSIP structure

//  TSIP configuration parameters
typedef struct TsipParams_t {
    ADT_UInt32      slotMask[32];   // timeslot enable/disable bit masks
    GpakActivation  singleClk;      // when Enabled: frame sync and serial clock signals are shared by the transmit and receive interfaces.
                                    // when Disabled: one framesync and serial clock for transmit, and a second framesync and clock for receive

    fsPolarity      txFsPolarity;   // tx framesync polarity 
    clkEdge         txDataClkEdge;  // clock edge used to clock tx data
    clkEdge         txFsClkEdge;    // clock edge used to sample tx framesync
    clkMode         txClkMode;      // tx clocking mode               (specifies tx and rx when redundant mode is active)
    dataRate        txDataRate;     // tx data rate                   (specifies tx and rx when redundant mode is active)
    clkFsSrc        txClkFsSrc;     // tx clock and frame sync source (specifies tx and rx when redundant mode is active)
    ADT_Int16       txDatDly;       // Transmit Data Delay: 
    txDisState      txDriveState;   // tx data line output drive state for disabled timeslots
    GpakActivation  txOutputDly;    // when Enabled: the tx output of an enabled timeslot (that follows a high-z timeslot) is delayed by 0.5 serial clock period
                                    // when Disabled: output delay is disabled

    fsPolarity      rxFsPolarity;   // rx framesync polarity 
    clkEdge         rxDataClkEdge;  // clock edge used to clock rx data
    clkEdge         rxFsClkEdge;    // clock edge used to sample rx framesync
    clkMode         rxClkMode;      // rx clocking mode (single or double) (only valid when redundant mode is inactive)
    dataRate        rxDataRate;     // rx data rate                        (only valid when redundant mode is inactive)
    clkFsSrc        rxClkFsSrc;     // tx clock and frame sync source      (only valid when redundant mode is inactive)
    ADT_Int16       rxDatDly;       // Receive Data Delay: 
    loopbackMode    loopBack;      // Loopback  mode
} TsipParams_t;
TsipParams_t  TsipParm[NUM_TDM_PORTS];
#pragma DATA_SECTION (TsipParm, "SLOW_DATA_SECT")
#pragma DATA_ALIGN   (TsipParm,  CACHE_L2_LINE_SIZE)

typedef struct FrameCnts {
   ADT_UInt32 FrameCnt;
   ADT_UInt32 FrameCntSync;
   ADT_UInt32 FrameErrCnt;
} FrameInfo;

// Port statistics
typedef struct TsipPortStat_t {
   ADT_UInt32 setupCnt;
   ADT_UInt32 teardownCnt;
   ADT_UInt16 FrameEndOffset;
   ADT_UInt32 LastWarningMsec;
   ADT_UInt32 LastError;
   ADT_UInt32 interFrameErrCnt;

   FrameInfo Rx;
   FrameInfo Tx;

} TsipPortStat_t;

// Interrupt statistics
typedef struct TsipStat_t {
   ADT_UInt32 statID;
   ADT_UInt32 IntrCnt;        // TSIP frame interrupts
   ADT_UInt32 ErrorCnt;       // TSIP error interrupts
   ADT_UInt32 LastErrorEvent; // Last error event detected
   ADT_UInt32 ErrorEvents;    // 
   TsipPortStat_t Port[NUM_TDM_PORTS];
} TsipStat_t;
TsipStat_t tsipStats;
#pragma DATA_SECTION (tsipStats, "STATS:TDM")

// Port enable status
static ADT_Bool TsipEnable [NUM_TDM_PORTS];
#pragma DATA_SECTION (TsipEnable, "NON_CACHED_DATA")

//  Register structure
typedef struct  TsipRegs_t {
    ADT_UInt32 EMUTST;
    ADT_UInt32 SIUCTL;
    ADT_UInt32 XCLK, XCTL, XSIZE;
    ADT_UInt32 RCLK, RCTL, RSIZE;
    ADT_UInt32 TDMUCFG;
    ADT_UInt32 DMACTL;
    ADT_UInt32 XDLY;
    ADT_UInt32 RDLY;

    ADT_UInt32 DX_ABASE, DX_AFALLOC, DX_AFSIZE, DX_AFCNT;
    ADT_UInt32 DX_BBASE, DX_BFALLOC, DX_BFSIZE, DX_BFCNT;
    ADT_UInt32 DR_ABASE, DR_AFALLOC, DR_AFSIZE, DR_AFCNT;
    ADT_UInt32 DR_BBASE, DR_BFALLOC, DR_BFSIZE, DR_BFCNT;

    ADT_UInt32 XBMA[64], XBMB[64];
    ADT_UInt32 RBMA[64], RBMB[64];
} TsipRegs_t;
static TsipRegs_t  TsipCfgRegs[NUM_TDM_PORTS];
#pragma DATA_SECTION (TsipCfgRegs, "SLOW_DATA_SECT")
#pragma DATA_ALIGN   (TsipCfgRegs,  CACHE_L2_LINE_SIZE)

// Base register addresses
volatile CSL_TsipRegs* TsipRegs[] = {
   (volatile CSL_TsipRegs *) CSL_TSIP_0_REGS,
   #ifdef CSL_TSIP_1_REGS
   (volatile CSL_TsipRegs *) CSL_TSIP_1_REGS,
   #endif

   #ifdef CSL_TSIP_2_REGS
   (volatile CSL_TsipRegs *) CSL_TSIP_2_REGS,
   #endif
   NULL
};
#define tsipAddr(portN) TsipRegs[portN]

static const TDM_Port TsipFunctions = { startTsip, stopTsip, setupTsip };

volatile ADT_UInt32 TsipRxEvents []  = { TSIP_RX_SF_INT0, TSIP_RX_SF_INT1, TSIP_RX_SF_INT2 };
#pragma DATA_SECTION (TsipRxEvents, "NON_CACHED_DATA")
volatile ADT_UInt32 TsipTxEvents []  = { TSIP_TX_SF_INT0, TSIP_TX_SF_INT1, TSIP_TX_SF_INT2 };
#pragma DATA_SECTION (TsipTxEvents, "NON_CACHED_DATA")
volatile ADT_UInt32 TsipErrEvents [] = { TSIP_ERR_INT0, TSIP_ERR_INT1, TSIP_ERR_INT2 };
#pragma DATA_SECTION (TsipErrEvents, "NON_CACHED_DATA")

volatile ADT_UInt8* RxTsipBuffers[] = { (ADT_UInt8*)BSP0DMA_RxBuffer, (ADT_UInt8*)BSP1DMA_RxBuffer, (ADT_UInt8*)BSP2DMA_RxBuffer};
#pragma DATA_SECTION (RxTsipBuffers, "NON_CACHED_DATA")
volatile ADT_UInt8* TxTsipBuffers[] = { (ADT_UInt8*)BSP0DMA_TxBuffer, (ADT_UInt8*)BSP1DMA_TxBuffer, (ADT_UInt8*)BSP2DMA_TxBuffer};
#pragma DATA_SECTION (TxTsipBuffers, "NON_CACHED_DATA")

const ADT_UInt16 TDM_frames_per_interrupt = TSIP_FRAMES_PER_INTERRUPT;
const ADT_UInt16 tdmCore = TSIP_CORE_ID;
ADT_UInt16* TDMSampsPerMs = &sysConfig.samplesPerMs;


typedef struct errQHist_t {
    int        port;    // tsip port
    ADT_UInt32 errc;    // error code
    ADT_UInt32 delta;
} errQHist_t;

typedef struct fcntHist_t {
   ADT_UInt32 start;
   ADT_UInt32 end;
   ADT_UInt32 dmaCnt;
   ADT_UInt8 *ptr;
} fcntHist_t;

#ifndef _DEBUG  // Error history logging
   errQHist_t TsipErrHist;
   static void logTsipErr (int port, ADT_UInt32 errQ) {
      TsipErrHist.port = port;
      TsipErrHist.errc = errQ;
   }
#else
   #define TSIP_ERRQ_HISTLEN        32
   errQHist_t TsipErrHist[TSIP_ERRQ_HISTLEN + 1];
   int TsipErrHistIdx = 0;
   int lastErrTime = 0;
   static void logTsipErr (int port, ADT_UInt32 errQ) {
      errQHist_t* tsipErrLog;
      int time;

      time = CLK_gethtime ();
      tsipErrLog = &TsipErrHist[TsipErrHistIdx++];
      tsipErrLog->port = port;
      tsipErrLog->errc = errQ;
      tsipErrLog->delta = time - lastErrTime;
      lastErrTime = time;

      if (TSIP_ERRQ_HISTLEN <= TsipErrHistIdx)    TsipErrHistIdx = 0;

      tsipErrLog++;
      tsipErrLog->port = -1;
   }
   #pragma DATA_SECTION (TsipErrHistIdx, "NON_CACHED_DATA")
   #pragma DATA_SECTION (TsipErrHist,    "NON_CACHED_DATA")
#endif

#ifndef _DEBUG  // Frame count history logging
   fcntHist_t fcntErrHist;
   #pragma DATA_SECTION (fcntErrHist, "FAST_DATA_SECT")

   inline void logFrameCntError(ADT_UInt32 start, ADT_UInt32 end, ADT_UInt32 dmaCnt, ADT_UInt8 *ptr) {
       fcntErrHist.start  = start;
       fcntErrHist.end    = end;
       fcntErrHist.dmaCnt = dmaCnt;
       fcntErrHist.ptr = ptr;
   }
#else
   #define FCNT_HISTLEN 256

   // frame count error history
   fcntHist_t fcntErrHist[FCNT_HISTLEN];
   int fcntErrHistIdx=0;
   #pragma DATA_SECTION (fcntErrHist, "FAST_DATA_SECT")
   #pragma DATA_SECTION (fcntErrHistIdx, "FAST_DATA_SECT")

   inline void logFrameCntError(ADT_UInt32 start, ADT_UInt32 end, ADT_UInt32 dmaCnt, ADT_UInt8 *ptr) {
       fcntErrHist[fcntErrHistIdx].start = start;
       fcntErrHist[fcntErrHistIdx].end   = end;
       fcntErrHist[fcntErrHistIdx].dmaCnt = dmaCnt;
       fcntErrHist[fcntErrHistIdx++].ptr = ptr;
       if (FCNT_HISTLEN <= fcntErrHistIdx) fcntErrHistIdx = 0;
       memset (&fcntErrHist[fcntErrHistIdx], 0xff, sizeof(fcntHist_t));
   }
#endif

#ifdef _DEBUG  //  Variable summary

   typedef struct TSIPVars_t {
      const TSIPModeCfg_t *TSIPCfg;
      ADT_Bool      (*TsipEnable)[NUM_TDM_PORTS];
      TsipStat_t    (*tsipStats);

      volatile CSL_TsipRegs  *(*TsipRegs)[];
      TsipParams_t  (*TsipParm)[NUM_TDM_PORTS];
      TsipRegs_t    (*TsipCfgRegs)[NUM_TDM_PORTS];

      volatile ADT_UInt8 *(*TxTsipBuffers)[NUM_TDM_PORTS];
      volatile ADT_UInt8 *(*RxTsipBuffers)[NUM_TDM_PORTS];

      errQHist_t (*TsipErrHist)[TSIP_ERRQ_HISTLEN+1];
      int *TsipErrHistIdx;

      fcntHist_t (*fcntErrHist)[FCNT_HISTLEN];
      int *fcntErrHistIdx;
      
   } TSIPVars_t;

   TSIPVars_t TSIPVars = {
     &TSIPCfg,     &TsipEnable,    &tsipStats, 
     &TsipRegs,    &TsipParm,      &TsipCfgRegs,
     &TxTsipBuffers,  &RxTsipBuffers,

     &TsipErrHist, &TsipErrHistIdx,
     &fcntErrHist,  &fcntErrHistIdx,
   };
#endif

void *addCoreOffset (void *addr) {
   ADT_UInt32 intAddr = (ADT_UInt32) addr;

   if ((intAddr & 0xf0000000) == 0) intAddr |= ((0x10 + DSPCore) << 24);
   return (void *) intAddr;
}

//---------------------------------------------------------------
//{ DMA support interface routines
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ zeroTxBuffer
//
// FUNCTION
//  Zeroes slot on Tx Buffer when channel goes inactive
//
// Inputs
//    port            - McBSPn
//    dmaSlot         - dma index of slot to zero
//
// RETURNS
//  none
//}
#pragma CODE_SECTION (zeroTxBuffer, "SLOW_PROG_SECT")
void zeroTxBuffer (int port, int dmaSlot) { 
   ADT_UInt8 *txSlot;
   int i;

   if (NUM_TDM_PORTS <= port) return;

   // offset to frame0, slotn in pingbuffer accounting for the 4-byte FCNT
   txSlot =  (ADT_UInt8 *) TxTsipBuffers[port];
   txSlot += (4 + (dmaSlot * tsipCfg->dmaChannelSizeI8));

   // index through ping and pong buffers
   for (i=0; i<TSIP_FRAMES_PER_INTERRUPT*2; i++) {
      memset (txSlot, ULAW_ZERO, tsipCfg->dmaChannelSizeI8);
      txSlot += TsipFallocI8[port];
   }
}
#pragma CODE_SECTION (zeroTxBufferFull, "SLOW_PROG_SECT")
static void zeroTxBufferFull (int port) {
   int i;
   ADT_UInt8  *CIDStart, *CIDEnd;
   ADT_UInt16 frameI8 = TsipFallocI8[port];

   if (NUM_TDM_PORTS <= port) return;

   CIDStart = (void *) TxTsipBuffers[port];

   // ULAW zero entire buffer
   memset (CIDStart, ULAW_ZERO, TsipBuflenI8[port]);

   // Fill in configuration ID at beginning of each frame
   if (tsipStats.Port[port].FrameEndOffset == 0) return;
   CIDStart += 3;
   CIDEnd    = CIDStart + tsipStats.Port[port].FrameEndOffset;
   
   for (i=0; i<TSIP_FRAMES_PER_INTERRUPT*2; i++) {
      *CIDStart = TSIPCfgID;      CIDStart += frameI8;
      *CIDEnd   = TSIPCfgID;      CIDEnd   += frameI8;
   }
   return;
}

#pragma CODE_SECTION (InitTSIPMode, "SLOW_PROG_SECT")
void InitTSIPMode () { 
   int port, evts;
   //===================================================
   // Adjust globals according to the
   // download-time TSIP configuration parameters
   //
   // NOTE: The follow can only be run once and must be
   //       run before frame initialization because
   //       of the potential modification of teh sysConfig.samplesPerMs
   //       parameter that is used by framing for data sizing.
   if (firstTime && (DSPCore == tdmCore)) {

      tdmPortFuncs = (TDM_Port*) &TsipFunctions;

      memset (&tsipStats, 0, sizeof (tsipStats));
      tsipStats.statID = TSIP_STAT_ID;

      for (port=0; port<NUM_TDM_PORTS; port++) {

         TsipEnable[port] = 0;

         // add core-offset to Tx and Rx DMA buffer pointers
         if (1 < DSPTotalCores) {
            TxTsipBuffers[port] = addCoreOffset ((void *) TxTsipBuffers[port]);
            RxTsipBuffers[port] = addCoreOffset ((void *) RxTsipBuffers[port]);
         }

      }
      PowerOnDevice (PSC_TSIP0);

      #ifdef CSL_TSIP_1_REGS
      PowerOnDevice (PSC_TSIP1);
      #endif

      #ifdef CSL_TSIP_2_REGS
      PowerOnDevice (PSC_TSIP2);
      #endif

      // shutdown All Tsips
      for (port = 0; port <NUM_TDM_PORTS ; port++)
         stopTsip (port);  

      firstTime = ADT_FALSE;
   }

   // Clear and enable TSIP_EVTS interrupts (completion and error) in combiner
   REG_WR (CMB_EVT_CLR, TSIP_EVTS);
   REG_WR (CMB_EVT_CLR, TSIP_ERROR_EVTS);

   REG_RD (CMB_EVT_ENB, evts);
   REG_WR (CMB_EVT_ENB, (evts & ~(TSIP_EVTS | TSIP_ERROR_EVTS)));

   if (tsipCfg->sampleRatekHz == 8) {
      sysConfig.samplesPerMs = 8;
      TDMRate = 8000;
   } else if ((tsipCfg->sampleRatekHz == 16)  && (SysAlgs.wbEnable)) {
      sysConfig.samplesPerMs = 16;
      TDMRate = 16000;
   }

   if (tsipCfg->tdmSampleSizeI8 == 1) {
      if (sysConfig.samplesPerMs == 8) {
         tsipCfg->dmaChannelSizeI8 = 1;
         tsipCfg->mode = tsip8BitNB;
         dmaToCirc = dmaToCirc8BitNB;
         circToDma = circToDma8BitNB;
      } else {
         tsipCfg->dmaChannelSizeI8 = 2;
         tsipCfg->mode = tsip8BitWB;
         dmaToCirc = dmaToCirc8BitWB;
         circToDma = circToDma8BitWB;
      }
   } else if (tsipCfg->tdmSampleSizeI8 == 2) {
      tsipCfg->compandedData = ADT_FALSE;
      if (sysConfig.samplesPerMs == 8) {
         tsipCfg->dmaChannelSizeI8 = 2;
         tsipCfg->mode = tsip16BitNB;
         dmaToCirc = dmaToCirc16BitNB;
         circToDma = circToDma16BitNB;
      } else {
         tsipCfg->dmaChannelSizeI8 = 4;
         tsipCfg->mode = tsip16BitWB;
         dmaToCirc = dmaToCirc16BitWB;
         circToDma = circToDma16BitWB;
      }
   } else {
      tsipCfg->tdmSampleSizeI8 = 1;
      tsipCfg->dmaChannelSizeI8 = 1;
   }
}

#pragma CODE_SECTION (getTSIPMode, "SLOW_PROG_SECT")
TSIPMode_t getTSIPMode () { return tsipCfg->mode; }


//}

//---------------------------------------------------------------
//{ TSIP Error handling routines (read TSIP error registers and log)
// Log TSIP errors and send warning to host
#pragma CODE_SECTION (warnTsipErr, "SLOW_PROG_SECT")
static void warnTsipErr (int port, ADT_UInt32 errQ) {
   ADT_UInt32 errc;
   TsipPortStat_t* portStats;

   errc = (errQ >> 24) & 0xFF;
   
   logTsipErr (port, errQ);

   portStats = &tsipStats.Port [port];
   if ((ApiBlock.DmaSwiCnt - portStats->LastWarningMsec) < 500) return;
   portStats->LastWarningMsec = ApiBlock.DmaSwiCnt;
   portStats->LastError   = errQ;

   // Error codes as described in 'Exception conditions (sect 2.7.4)' of TSIP.
   switch (errc) {
   case 0x0:     break;
   case 0x01:    SendWarningEvent (port, WarnTsip_1);         break;
   case 0x02:    SendWarningEvent (port, WarnTsip_2);         break;
   case 0x03:    SendWarningEvent (port, WarnTsip_3);         break;
   case 0x05:    SendWarningEvent (port, WarnTsip_5);         break;
   case 0x10:    SendWarningEvent (port, WarnTsip_16);        break;
   case 0x11:    SendWarningEvent (port, WarnTsip_17);        break;
   case 0x12:    SendWarningEvent (port, WarnTsip_18);        break;
   case 0x13:    SendWarningEvent (port, WarnTsip_19);        break;
   case 0x15:    SendWarningEvent (port, WarnTsip_21);        break;

   case 0xF1: case 0xF2: case 0xF3: case 0xF4: 
   case 0xF5: case 0xF6: case 0xF7:  
               SendWarningEvent (port, WarnTsip_RD);   break;
   default:    SendWarningEvent (port, WarnTsip_WR);   break;
   }
}

// Read and process TSIP error register
#pragma CODE_SECTION (readTsipErrors, "SLOW_PROG_SECT")
static ADT_Bool readTsipErrors (int port) {
   ADT_UInt32 errCnt, errQ, i;
   volatile CSL_TsipRegs* tsip;

   tsip = tsipAddr(port);

   errCnt = tsip->ERR.ERRCNT & 0x1F;

   for (i=0; i<errCnt; i++) {
      errQ = tsip->ERR.ERRQ;      // read the top queue entry
      tsip->ERR.ERRCTL = 0x0001;  // pop the top entry off
      warnTsipErr (port, errQ);
   }
   tsip->ERR.ERRCTL = 0x00000102;  // clear overflow and queue
   return (errCnt != 0);
}

// Check TSIP errors.  Called by McBSPFrameError.  Will cause TDM reset if error is detected.
static ADT_Bool checkTsipErrors() {
    int intrMask;
    ADT_UInt32 tsipErrors;  
    ADT_Bool err = ADT_FALSE;

    // Critical section: disable HW ints, snapshot global error flags, 
    // then clear global error flags
    intrMask = HWI_disable ();
    tsipErrors = tsipStats.LastErrorEvent;
    tsipStats.LastErrorEvent = 0;
    HWI_restore (intrMask);

    if (tsipErrors == 0) return 0;
    if (tsipErrors & TsipErrEvents[0]) err |= readTsipErrors(0);
    if (tsipErrors & TsipErrEvents[1]) err |= readTsipErrors(1);
    if (tsipErrors & TsipErrEvents[2]) err |= readTsipErrors(2);
    return err;
}

#pragma CODE_SECTION (validateRxFrameCnt, "FAST_PROG_SECT")
int validateRxFrameCnt (int port, int pong) {
   TsipPortStat_t* portStats;
   ADT_UInt8 *buff;
   FrameInfo*  frmInf;
   int frameError = 0;

   ADT_UInt32 *FirstFrameCntAddr, *LastFrameCntAddr;
   ADT_UInt32 FirstFrameCnt,       LastFrameCnt, deltaFcnt;
   ADT_UInt16 frameI8 = TsipFallocI8[port];

   buff = (ADT_UInt8 *) RxTsipBuffers[port];
   if (pong) buff += (frameI8 * TDM_frames_per_interrupt);

   portStats = &tsipStats.Port [port];
   frmInf    = &portStats->Rx;

   FirstFrameCntAddr = (ADT_UInt32*) buff;
   LastFrameCntAddr  = (ADT_UInt32*) (buff + ((TSIP_FRAMES_PER_INTERRUPT-1) * frameI8) + portStats->FrameEndOffset);

   FirstFrameCnt = *FirstFrameCntAddr & 0x00FFFFFF;
   LastFrameCnt  = *LastFrameCntAddr  & 0x00FFFFFF;
   if (frmInf->FrameCntSync == 0) frmInf->FrameCnt = FirstFrameCnt;

   // Verify that the first start count is next in sequence from previous interrupt.
   if (FirstFrameCnt != frmInf->FrameCnt)  frameError = 1;

   // Verify that the last count in the frame is TSIP_FRAMES_PER_INTERRUPT-1 higher than the first.
   if (LastFrameCnt > FirstFrameCnt) deltaFcnt =  LastFrameCnt - FirstFrameCnt;
   else                              deltaFcnt =  0x01000000 - FirstFrameCnt + LastFrameCnt;
   if (deltaFcnt != (TSIP_FRAMES_PER_INTERRUPT - 1)) frameError = 1;

   // Update the frame count expected for the next millisecond sampling
   frmInf->FrameCnt = (LastFrameCnt + 1) & 0x00FFFFFF;
   frmInf->FrameCntSync++;

   if (!frameError) return 0;

   logFrameCntError (FirstFrameCnt, LastFrameCnt, ApiBlock.DmaSwiCnt, buff);

   frmInf->FrameErrCnt++;
   frmInf->FrameCntSync = 0;

   // Throttle frame count errors to no more than 1 every 1/2 second
   if ((ApiBlock.DmaSwiCnt - portStats->LastWarningMsec) < 500) return 1;
   portStats->LastWarningMsec = ApiBlock.DmaSwiCnt;
   SendWarningEvent (port, WarnTsipRxFcntErr);
   return 1;
}
//}

//---------------------------------------------------------------
//{ TSIP control routines (setup/start/stop)
//  Write TSIP registers with configured parameters 
#pragma CODE_SECTION (setupTsip, "SLOW_PROG_SECT")
static ADT_Bool setupTsip (int port) {

   volatile CSL_TsipRegs   *tsip;
   TsipPortStat_t          *portStats;
   TsipRegs_t              *cfg;
   int i;

   if (!TsipEnable[port]) {
      RxEvents[port] = 0;
      TxEvents[port] = 0;
      return ADT_FALSE;
   }

   TSIPCfgID &= 0xfe;   // We currently only support A configuration.

   // Notify DMA of TSIP events.
   RxEvents[port] = TsipRxEvents[port];
   TxEvents[port] = TsipTxEvents[port];

   // Zero statistics
   portStats = &tsipStats.Port[port];
   portStats->setupCnt++;
   portStats->Tx.FrameCnt     = 0;
   portStats->Tx.FrameCntSync = 0;

   portStats->Rx.FrameCnt     = 0;
   portStats->Rx.FrameCntSync = 0;

   zeroTxBufferFull (port);

   // Setup TSIP registers.
   tsip = tsipAddr(port);
   cfg = &TsipCfgRegs[port];

   tsip->EMUTST        = cfg->EMUTST;
   tsip->SIUCTL        = cfg->SIUCTL;
   tsip->XCLK          = cfg->XCLK;
   tsip->XCTL          = cfg->XCTL;
   tsip->XSIZE         = cfg->XSIZE;
   tsip->RCLK          = cfg->RCLK;
   tsip->RCTL          = cfg->RCTL;
   tsip->RSIZE         = cfg->RSIZE;
   tsip->TDMUCFG       = cfg->TDMUCFG;
   tsip->DMACTL        = cfg->DMACTL;
   tsip->XDLY          = cfg->XDLY;
   tsip->RDLY          = cfg->RDLY;

   tsip->DXCH.ABASE = cfg->DX_ABASE;

   tsip->DXCH.ABASE    = cfg->DX_ABASE;
   tsip->DXCH.AFALLOC  = cfg->DX_AFALLOC;
   tsip->DXCH.AFSIZE   = cfg->DX_AFSIZE;
   tsip->DXCH.AFCNT    = cfg->DX_AFCNT;
   tsip->DXCH.BBASE    = cfg->DX_BBASE;
   tsip->DXCH.BFALLOC  = cfg->DX_BFALLOC;
   tsip->DXCH.BFSIZE   = cfg->DX_BFSIZE;
   tsip->DXCH.BFCNT    = cfg->DX_BFCNT;

   tsip->DRCH.ABASE    = cfg->DR_ABASE;
   tsip->DRCH.AFALLOC  = cfg->DR_AFALLOC;
   tsip->DRCH.AFSIZE   = cfg->DR_AFSIZE;
   tsip->DRCH.AFCNT    = cfg->DR_AFCNT;
   tsip->DRCH.BBASE    = cfg->DR_BBASE;
   tsip->DRCH.BFALLOC  = cfg->DR_BFALLOC;
   tsip->DRCH.BFSIZE   = cfg->DR_BFSIZE;
   tsip->DRCH.BFCNT    = cfg->DR_BFCNT;

   for (i=0; i<64; i++) {
      tsip->XBM.XBMA[i] = cfg->XBMA[i];
      tsip->XBM.XBMB[i] = cfg->XBMB[i];
      tsip->RBM.RBMA[i] = cfg->RBMA[i];
      tsip->RBM.RBMB[i] = cfg->RBMB[i];
   }
   return ADT_TRUE;

}

// Start TSIP operation
#pragma CODE_SECTION (startTsip, "SLOW_PROG_SECT")
static ADT_Bool startTsip (int port) {

   volatile CSL_TsipRegs   *tsip;
   volatile ADT_UInt32    reg, xstat, rstat;
   volatile ADT_UInt32    startTime, tout;
   volatile ADT_UInt16    evtFlags;
   int     shift;

   // clear any flagged events on this port
   evtFlags  =  RxEvents[port] | TxEvents[port];

   REG_WR (CMB_EVT_CLR, evtFlags);


   tsip = tsipAddr(port);

   // enable the receive transmit units for TSIP core
   tsip->RCHN.RCHEN = (TSIPCfgID << 8) | CHNENB;
   tsip->XCHN.XCHEN = (TSIPCfgID << 8) | CHNENB;

   // start the receive and transmit serial interfaces running        
   reg = tsip->SIUCTL;
   reg |= (RCVENB | XMTENB);
   tsip->SIUCTL = reg;

   // enable TSIP dma unit
   tsip->TDMUCTL = DMAENB;

   // Wait for interrupts indicating that the TSIP is operating
   logTime (0x00100000ul | 0xDEAD);
   tout =  CLK_countspms();
   startTime = CLK_gethtime ();

   // NOTE: Return TRUE even though timeout occurs.  This will prevent GpakPcm
   //       from treating this in the same fashion as if the TSIP was disabled.
   shift = tdmCore * 2;
   do {
      xstat = (tsip->XCHST) >> shift;
      rstat = (tsip->RCHST) >> shift;
      if (tout < (CLK_gethtime() - startTime)) return ADT_TRUE;
   } while (((xstat & 1) == 0) && ((rstat & 1) == 0));
   return ADT_TRUE;

}

//  Stop TSIP operation
#pragma CODE_SECTION (stopTsip, "SLOW_PROG_SECT")
static ADT_Bool stopTsip (int port) {
   volatile CSL_TsipRegs *tsip;
   TsipPortStat_t *portStats;

   portStats = &tsipStats.Port [port];


   RxEvents[port] = 0;
   TxEvents[port] = 0;

   tsip = tsipAddr (port);

   // disable the receive and transmit serial interfaces:
   // SIU Global Control register's RCVENB and XMTENB
   tsip->SIUCTL &= ~(RCVENB | XMTENB);

   // disable receive and transmit timeslot manager on TSIP core's channel
   tsip->RCHN.RCHEN = 0;
   tsip->XCHN.XCHEN = 0;

   // disable the TDMU Global Control register's DMAENB bits
   tsip->TDMUCTL = 0;

   // place TSIP's DMA and SIU units in reset
   tsip->RST = (DMARST | SIURST); 

   // poll the reset register until reset completes
   while (tsip->RST);

   portStats->teardownCnt++;
   portStats->Rx.FrameCntSync = 0;
   portStats->Tx.FrameCntSync = 0;

   return ADT_TRUE;
}

//}

//---------------------------------------------------------------
//{  TSIP configuration routines
//
// cfgChanBitMap modifies:
//  1) Port's SlotMap which linka G.PAK slot IDs to dma slots.
//  2) Port's slot enable/companding map (slotEnableWords).
//
// Returns:
//   size (I8) of active dma buffer.
//---------------------------------------------------------------
//
// Set current configuration IDs for TSIP buffers
#pragma CODE_SECTION (cfgChanBitMap, "SLOW_PROG_SECT")
static void cfgChanBitMap (ADT_UInt32 *cmpdWords, // pointer to output buffer where channel's companding bitmap is stored
     ADT_UInt32 *slotEnableWords,      // G.PAK slot enable bitmap (from host configuration message)
     dataRate rate,                    // input: data rate
     ADT_UInt16 *dmaI8,                // output: number of dma bytes in frame
     int port) {                       // input: TDM port

   ADT_UInt32 slotEnableWord, slotEnableBit;
   ADT_UInt32 cmpdWord, cmpdBits, cmpdBits0;

   int slotEnableIdx, cmpdIdx;
   int slotIdx, dmaIdx;

   int slot, link;
   int maxSlots, activeLinks;

   ADT_UInt16 *slotMap;

   slotMap = pSlotMap[port];

   switch (rate) {
   case rate_8Mbps:    activeLinks=8;        maxSlots=128;        break;
   case rate_16Mbps:   activeLinks=4;        maxSlots=256;        break;
   case rate_32Mbps:   activeLinks=2;        maxSlots=512;        break;

   default:    
      AppErr ("Invalid TSIP ", ADT_TRUE);
   };

   slotIdx = 0;
   dmaIdx = 0;

   slotEnableWord = slotEnableWords[0];
   slotEnableIdx  = 0;
   slotEnableBit  = 1;

   // Set companding bits to 8-bit linear or none.
   // G.PAK disallows u-law and a-law hardware companding modes to avoid mixed 8-bit and 16-bit dma slots.
   cmpdWord = 0;;
   cmpdIdx  = 0;
   if (tsipCfg->mode == tsip8BitNB) {
      cmpdBits0  = 0x40000000;
   } else if (tsipCfg->mode == tsip16BitWB) {
      cmpdBits0  = 0x55000000;
   } else {
      cmpdBits0  = 0x50000000;
   }
   cmpdBits = cmpdBits0;

   // Set SlotMap and cmpdMask fields according to value of slotMask
   for (slot=0; slot < maxSlots; slot++) {
      for (link=0; link < activeLinks; link++) {
         if (slotEnableWord & slotEnableBit) {
            cmpdWord |= cmpdBits;
            slotMap[slotIdx++] = dmaIdx++;
         } else {
            slotMap[slotIdx++] = UNCONFIGURED_SLOT;
         }
         slotEnableBit <<= 1;  // One enabling bit per slot.
         cmpdBits  >>= 2;      // Two companding bits per slot.
      }

      // NOTE: The slot enable and companding mask bits will never shift out of 
      //       the 32-bit range in the middle of the link loop, because all possible 
      //       active link values are even divisors of 16 and 32.

      // Move to next word when mask is shifted beyond the high order bit
      if (slotEnableBit == 0) {
         slotEnableBit = 1;
         slotEnableIdx++;

         slotEnableWord = slotEnableWords[slotEnableIdx];  // Get next slot mask word
      }
      if (cmpdBits == 0) {
         cmpdWords[cmpdIdx] = cmpdWord;
         cmpdWord = 0;

         cmpdBits = cmpdBits0;
         cmpdIdx++;
      }
   }

   *dmaI8 = dmaIdx * tsipCfg->dmaChannelSizeI8;
}

// Parse  into configuration parameters
#pragma CODE_SECTION (parseTsipParams, "SLOW_PROG_SECT")
static void parseTsipParams (ADT_UInt16 *cmd, TsipParams_t *tp) {
   int i, idx;
   ADT_UInt32 primaryBitMask;

   if (tsipCfg->dmaChannelSizeI8 == 1) {
      primaryBitMask = 0xffffffff;
   } else if (tsipCfg->dmaChannelSizeI8 == 2) {
      primaryBitMask = 0x55555555;
   } else {
      primaryBitMask = 0x11111111;
   }

    for (i=0,idx=0; i<32; i++, idx+=2) {
        tp->slotMask[i] = primaryBitMask & ((((ADT_UInt32)cmd[idx])<<16) | (ADT_UInt32)cmd[idx+1]);
    }

    tp->singleClk       = (GpakActivation)  (cmd[64] & 0x0001);
    tp->txFsPolarity    = (fsPolarity)     ((cmd[64] >> 1)  & 0x0001); 
    tp->txDataClkEdge   = (clkEdge)        ((cmd[64] >> 2)  & 0x0001); 
    tp->txFsClkEdge     = (clkEdge)        ((cmd[64] >> 3)  & 0x0001);  
    tp->txClkMode       = (clkMode)        ((cmd[64] >> 4)  & 0x0001);    
    tp->txClkFsSrc      = (clkFsSrc)       ((cmd[64] >> 5)  & 0x0001);   
    tp->txOutputDly     = (GpakActivation) ((cmd[64] >> 6)  & 0x0001); 
    tp->rxFsPolarity    = (fsPolarity)     ((cmd[64] >> 7)  & 0x0001);   
    tp->rxDataClkEdge   = (clkEdge)        ((cmd[64] >> 8)  & 0x0001);  
    tp->rxFsClkEdge     = (clkEdge)        ((cmd[64] >> 9)  & 0x0001);    
    tp->rxClkMode       = (clkMode)        ((cmd[64] >> 10) & 0x0001);      
    tp->rxClkFsSrc      = (clkFsSrc)       ((cmd[64] >> 11) & 0x0001);     
    tp->txDriveState    = (txDisState)      (cmd[65] & 0x0003);
    tp->txDataRate      = (dataRate)       ((cmd[65] >> 2) & 0x0003);   
    tp->rxDataRate      = (dataRate)       ((cmd[65] >> 4) & 0x0003);     
    tp->loopBack        = (loopbackMode)   ((cmd[65] >> 6) & 0x0003);     
    tp->txDatDly        = (ADT_Int16) cmd[66];     
    tp->rxDatDly        = (ADT_Int16) cmd[67];       

    if (tp->singleClk == Enabled) {
        tp->rxClkMode       = tp->txClkMode;
        tp->rxClkFsSrc      = tp->txClkFsSrc;
        tp->rxDataRate      = tp->txDataRate;
    }
}
// Store configuration parameters in TSIP structure
#pragma CODE_SECTION (storeTsipConfiguration, "SLOW_PROG_SECT")
static int storeTsipConfiguration (int port) {

   TsipParams_t *parm;
   TsipRegs_t *cfg;
   ADT_UInt16 dmaI8;
   ADT_UInt32 dmaFrameI8;

    cfg  = &TsipCfgRegs[port];
    parm = &TsipParm[port]; 

    // Setup the A-context transmit/receive channel bitmaps
    cfgChanBitMap (cfg->XBMA, parm->slotMask, parm->txDataRate, &dmaI8, port);
    memcpy (cfg->RBMA, cfg->XBMA, sizeof(cfg->RBMA));

    // B-context not used.
    memset (cfg->XBMB, 0, sizeof(cfg->XBMB));
    memset (cfg->RBMB, 0, sizeof(cfg->RBMB));

    // Emulation and Test ------------------------------------------------------
    if (parm->loopBack == internalLoopback)       cfg->EMUTST  = 0x00010000; // enable internal digital loopback
    else if (parm->loopBack == externalLoopback)  cfg->EMUTST  = 0x00030000; // enable external link loopback
    else                                          cfg->EMUTST  = EMUSOFT;

    // Serial Unit Interface (SIU) Global Control ------------------------------
    cfg->SIUCTL  = 0;                   // Disable Rx/Tx
    if (parm->singleClk == Disabled)    cfg->SIUCTL = (1<<4);

    // Transmit Clock Source ---------------------------------------------------
    cfg->XCLK = 0;
    if (parm->txClkFsSrc == clkFsB)     cfg->XCLK = 1;

    // Transmit Control --------------------------------------------------------
    cfg->XCTL = 0;
    if (parm->txClkMode == clkSingleRate)       cfg->XCTL = 1;

    if (parm->txDataRate == rate_16Mbps)        cfg->XCTL |= (1<<1);
    else if (parm->txDataRate == rate_32Mbps)   cfg->XCTL |= (2<<1);

    if (parm->txDataClkEdge == clkFalling)      cfg->XCTL |= (1<<5);
    if (parm->txFsClkEdge == clkFalling)        cfg->XCTL |= (1<<6);
    if (parm->txFsPolarity == fsActHigh)        cfg->XCTL |= (1<<7);

    if (parm->txDriveState == low)              cfg->XCTL |= (2<<8);
    else if (parm->txDriveState == high)        cfg->XCTL |= (3<<8);

    if (parm->txOutputDly == Enabled)           cfg->XCTL |= (1<<10);

    cfg->XCTL |= ((parm->txDatDly & 0x3FFF) << 16);

    // Transmit Size -----------------------------------------------------------
    cfg->XSIZE  = 127;                                      // virtual slot count
    cfg->XSIZE |= ((TSIP_FRAMES_PER_INTERRUPT - 1) << 16);  // frame count

    // Receive Clock Source ----------------------------------------------------
    cfg->RCLK = 0;
    if (parm->rxClkFsSrc == clkFsB)     cfg->RCLK = 1;

    // Receive Control ---------------------------------------------------------
    cfg->RCTL = 0;
    if (parm->rxClkMode == clkSingleRate)       cfg->RCTL = 1;

    if (parm->rxDataRate == rate_16Mbps)        cfg->RCTL |= (1<<1);
    else if (parm->rxDataRate == rate_32Mbps)   cfg->RCTL |= (2<<1);

    if (parm->rxDataClkEdge == clkFalling)      cfg->RCTL |= (1<<5);
    if (parm->rxFsClkEdge == clkFalling)        cfg->RCTL |= (1<<6);
    if (parm->rxFsPolarity == fsActHigh)        cfg->RCTL |= (1<<7);

    cfg->RCTL |= ((parm->rxDatDly & 0x3FFF) << 16);

    // Receive Size ------------------------------------------------------------
    cfg->RSIZE  = 127;                                      // virtual slot count
    cfg->RSIZE |= ((TSIP_FRAMES_PER_INTERRUPT - 1) << 16);  // frame count

    // TDMU Global Configuration -----------------------------------------------
    cfg->TDMUCFG = 0;   // Little endian (N/A for G.PAK since linear 16 companding is not supported)

    // DMATCU Global Control ---------------------------------------------------
    cfg->DMACTL = DMA_TRANSFER_PRIORITY | (MAX_DMA_TRANSFER_PRIORITY << 4);

    // Interrupt Selection/Delay -----------------------------------------------
    cfg->XDLY = INT_MODE;
    cfg->RDLY = INT_MODE;

    // Because the dma transfer size is 32-bits, the smallest possible
    // storage for a single frame of data is 12 bytes: 8 bytes for CID/FRCNT, 
    // plus 4 bytes for 1, 2, 3, or 4 timeslots). 
    // Increase the framesize to be a multiple of 4 if it's not already.
    dmaFrameI8 = dmaI8 + (TSIP_SYNC_I8 * 2);
    dmaFrameI8 = (dmaFrameI8 + 3) & ~3;    

    // A-Context transmit registers: base, frame allocation, frame size, frame count
    cfg->DX_ABASE    =  (ADT_UInt32) TxTsipBuffers[port];
    cfg->DX_AFALLOC  =  TsipFallocI8[port];
    cfg->DX_AFSIZE   =  dmaFrameI8;
    cfg->DX_AFCNT    =  TSIP_FRAMES_PER_INTERRUPT * 2; // dma wraps back to start after two super-frames

    // A-Context receive registers: base, frame allocation, frame size, frame count
    cfg->DR_ABASE    =  (ADT_UInt32) RxTsipBuffers[port];
    cfg->DR_AFALLOC  =  TsipFallocI8[port];
    cfg->DR_AFSIZE   =  dmaFrameI8;
    cfg->DR_AFCNT    =  TSIP_FRAMES_PER_INTERRUPT * 2; // dma wraps back to start after two super-frames

    // B-context are not used
    // B-Context transmit registers: base, frame allocation, frame size, frame count
    cfg->DX_BBASE    =  (ADT_UInt32) TxTsipBuffers[port];
    cfg->DX_BFALLOC  =  TsipFallocI8[port];
    cfg->DX_BFSIZE   =  dmaFrameI8;
    cfg->DX_BFCNT    =  TSIP_FRAMES_PER_INTERRUPT * 2;

    // B-Context receive registers: base, frame allocation, frame size, frame count
    cfg->DR_BBASE    =  (ADT_UInt32) RxTsipBuffers[port];
    cfg->DR_BFALLOC  =  TsipFallocI8[port];
    cfg->DR_BFSIZE   =  dmaFrameI8;
    cfg->DR_BFCNT    =  TSIP_FRAMES_PER_INTERRUPT * 2;
    return dmaFrameI8 - 4;
}

// Validate TSIP rate field value
#pragma CODE_SECTION (validTsipRate, "FAST_PROG_SECT")
static inline int validTsipRate (dataRate rate) {
    return ((rate == rate_8Mbps) || (rate == rate_16Mbps) || (rate == rate_32Mbps));
}

// Validate inactive slot transmission state value
#pragma CODE_SECTION (validTsipTxDis, "SLOW_PROG_SECT")
static inline int validTsipTxDis (txDisState state) {
    return ((state == highz) || (state == low) || (state == high));
}

// Count primary TSIP slots
#pragma CODE_SECTION (countTsipSlots, "SLOW_PROG_SECT")
static ADT_UInt16 countTsipSlots (ADT_UInt32 slotMask[]) {

   ADT_UInt16 cnt;
   ADT_UInt32 Mask;
   int i,k;

    cnt = 0;
    for (i=0; i<32; i++) {
        Mask = slotMask[i];
        for (k=0; k<32; k++) {
            if ((Mask & 1) != 0) cnt++;
            Mask >>= 1;
        }
    }
    return cnt;
}

// Process TSIP configuration message
//  Parse message, verify parameters, stop TSIPs, configure TSIP registers, restart TSIPs
#pragma CODE_SECTION (ProcConfigSerialPortsMsg, "SLOW_PROG_SECT")
GPAK_PortConfigStat_t ProcConfigSerialPortsMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {

   int i, j, cmdIdx;
   ADT_UInt32 startDmaCnt;
   ADT_UInt32 startTime;
   TsipPortStat_t *portStats;
   TsipParams_t *tp;

   ADT_UInt16 tsipSlotCnt[NUM_TDM_PORTS];   // Number of G.PAK (primary) slots on port.
   ADT_UInt16 *SlotMap;

   // Prepare the reply message.
   pReply[0] |= (MSG_CONFIG_PORTS_REPLY << 8);

   // Make sure there are no channels currently active.
   if (NumActiveChannels != 0)
      return Pc_ChannelsActive;

   // Parse TSIP parameters.
   for (i=0, cmdIdx=2; i<NUM_TDM_PORTS; i++) {
      TsipEnable[i] =  pCmd[1] & (1<<i);

      tsipSlotCnt[i] = 0;
      if (TsipEnable[i]) {
         tp = &TsipParm[i];
         parseTsipParams (&pCmd[cmdIdx], tp);
         if (!validTsipRate(tp->txDataRate))     return (GPAK_PortConfigStat_t) ((int) Pc_NoSlots0 + i);
         if (!validTsipRate(tp->rxDataRate))     return (GPAK_PortConfigStat_t) ((int) Pc_NoSlots0 + i);
         if (!validTsipTxDis(tp->txDriveState))  return (GPAK_PortConfigStat_t) ((int) Pc_NoSlots0 + i);
         tsipSlotCnt[i] = countTsipSlots (tp->slotMask);

         if (MaxDmaSlots[i] < tsipSlotCnt[i])    return (GPAK_PortConfigStat_t) ((int) Pc_TooManySlots0 + i);
         if (tsipSlotCnt[i] == 0)                return (GPAK_PortConfigStat_t) ((int) Pc_NoSlots0 + i);
      }
      cmdIdx += 68;   // numI16 elements per tsip in cmd buffer
   }

   // Copy companding modes into SlotCompandMode array
   for (i=0, cmdIdx=206; i<NUM_TDM_PORTS; i++) {
      for (j=0; j<4; j++,cmdIdx+=2)
         SlotCompandMode[i][j] = (((ADT_UInt32)pCmd[cmdIdx])<<16) | (ADT_UInt32)pCmd[cmdIdx+1];
      UseSlotCmpMask[i] = tsipCfg->compandedData;
   }

   // Zero source buffers
   for (i=0; i<SINK_DRAIN_LEN_I16; i++)
      drainBuff[i] = ULAW_ZERO;

    // Store TSIP parameters in register format
   for (i=0; i<NUM_TDM_PORTS; i++) {
      portStats = &tsipStats.Port [i];
      TsipFallocI8 [i] = (TSIP_SYNC_I8 * 2) + (tsipCfg->dmaChannelSizeI8 * MaxDmaSlots[i]) + 3;
      TsipFallocI8 [i] &= ~3;
      if (TsipEnable[i]) {
         portStats->FrameEndOffset = storeTsipConfiguration(i);
      } else {
         SlotMap = pSlotMap[i];
         for (j=0; j<SltsPerFrame[i]; j++) SlotMap[j] = UNCONFIGURED_SLOT;
         portStats->FrameEndOffset = 0;
      }
   }


   // Startup TDM buses
   StartGpakPcm (&tsipSlotCnt[0]);

   //===============================================================
   // Wait up to 10 ms for verification that the DMA has started.
   startTime = CLK_gethtime ();
   startDmaCnt = ApiBlock.DmaSwiCnt;

   if (MatchDmaFlags == 0) return Pc_Success;

   do {
      if (2 <= (ADT_Int32) (ApiBlock.DmaSwiCnt - startDmaCnt)) return Pc_Success;
   } while ((CLK_gethtime() - startTime) < (CLK_countspms() * 10));

   StopSerialPortIo ();
   return Pc_NoInterrupts;
}
//}

// Called by combined event interrupt. Process TSIP superframe interrupts
#pragma CODE_SECTION (GpakTSIPIsr, "FAST_PROG_SECT")
void GpakTSIPIsr (ADT_UInt32 pendingInterrupts) {

   REG_WR (CMB_EVT_CLR, pendingInterrupts);

   // Read and clear pending interrupts
   logDMA ("TSIPIntr.....", pendingInterrupts, (ADT_UInt64) pendingInterrupts);

   logTime (0x00100000ul | pendingInterrupts);

   tsipStats.IntrCnt++;

   // ===========================================================
   //  TSIP Tx completion --  Issue request to copy next superframe 
   //                         of channel data to DMA transfer buffer
   if (pendingInterrupts & TSIP_TX_INTRS) 
      SWI_or (SWI_Dma, pendingInterrupts & TSIP_TX_INTRS);

   // ===========================================================
   //  TSIP Rx completion --  Issue request to DMA TSIP buffer to
   //                         DMA transfer buffer
   if (pendingInterrupts & TSIP_RX_SF_INT0) issueRxDMA (0);
   if (pendingInterrupts & TSIP_RX_SF_INT1) issueRxDMA (1);
   if (pendingInterrupts & TSIP_RX_SF_INT2) issueRxDMA (2);
   return;
}

// Called by combined event interrupt. Process TSIP error interrupts
#pragma CODE_SECTION (GpakTSIPErrIsr, "FAST_PROG_SECT")
void GpakTSIPErrIsr (ADT_UInt32 pendingInterrupts) {
   logDMA ("TSIPErr......", pendingInterrupts, (ADT_UInt64) pendingInterrupts);

   // Store errors for SWI processing.  
   // Restarts TDM processing if error (overrun or underrun) detected.
   tsipStats.ErrorCnt++;
   tsipStats.ErrorEvents    |= pendingInterrupts;
   tsipStats.LastErrorEvent  = pendingInterrupts;
   REG_WR (CMB_EVT_CLR, TSIP_ERROR_EVTS);
}

#pragma CODE_SECTION (McBSPFrameError, "SLOW_PROG_SECT")
ADT_Bool McBSPFrameError (int port)   { return (ADT_Bool) checkTsipErrors(); }

//---------------------------------------------------------------------
//{  Stub fucntions required by G.PAK's TDM interface
// 64plusTDMDma stub functions required by GpakPcm:
int ClkDiv, FrameWidth, PulseWidth, genClockEnable;
#pragma CODE_SECTION (TogglePCMOutSignal, "SLOW_PROG_SECT")
ADT_UInt16 TogglePCMOutSignal (int port)    { return 0xff; }
#pragma CODE_SECTION (TogglePCMInSignal, "SLOW_PROG_SECT")
ADT_UInt16 TogglePCMInSignal (int port)     { return 0xff; }

#pragma CODE_SECTION (storeMcBSPConfiguration, "SLOW_PROG_SECT")
void storeMcBSPConfiguration (int port)                 { }
#pragma CODE_SECTION (setupMcBSP, "SLOW_PROG_SECT")
GpakActivation setupMcBSP (int port)                    { return Disabled; }
#pragma CODE_SECTION (startMcBSP, "SLOW_PROG_SECT")
void startMcBSP (int port)                              { }
#pragma CODE_SECTION (stopMcBsp, "SLOW_PROG_SECT")
GpakActivation stopMcBsp (int port)                     { return Disabled; }
#pragma CODE_SECTION (SetTransmitEnables, "SLOW_PROG_SECT")
void SetTransmitEnables (int port, ADT_UInt32 Mask[])   { }
#pragma CODE_SECTION (ClearTransmitEnables, "SLOW_PROG_SECT")
void ClearTransmitEnables (int port, ADT_UInt16 Mask[]) { }
#pragma CODE_SECTION (ConfigureMcBSP, "SLOW_PROG_SECT")
void ConfigureMcBSP (ADT_UInt32 McBspEnable[3], ADT_UInt32 McBspSlotMask[3][4]) { }
#pragma CODE_SECTION (GetMasks, "SLOW_PROG_SECT")
void GetMasks (ADT_Word McBspId, int Slot, int SlotCnt, ADT_Word *TxMask) { }
#pragma CODE_SECTION (updateTdmLoopback, "SLOW_PROG_SECT")
int updateTdmLoopback(GpakSerialPort_t port, GpakActivation state) { return 0; }
//}
