/*
 * Copyright (c) 2004-2007, Adaptive Digital Technologies, Inc.
 *
 * File Name: LbCoderStub.c
 *
 * Description:
 *   This file contains functions to support Loopback Coder type channels.
 *
 * Version: 1.0
 *
 * Revision History:
 *   5/27/09 - Initial release.
 */
   
// Application related header files.
#include "GpakDefs.h"

void lbCoderInit() {
}

void *getBGScratch (ADT_UInt16 *bgI16) {
    *bgI16 = 0;
    return (void *)0;
}

GPAK_ChannelConfigStat_t ProcCfgLpbkMsg (ADT_UInt16 *pCmd, chanInfo_t *pChan) {
   return Cc_Success;
}

ADT_UInt16 ProcCfgLpbkStatMsg  (ADT_UInt16 *pReply, chanInfo_t *pChan) {

   return 10;
}

void SetupLoopbackCoder (chanInfo_t *pChan) {
}


void DeactivateLpbkCoder(chanInfo_t *pChan) {
}

void teardownLoopBack() {
}

void processLoopbackCoder() {
}
