/*
 * Copyright (c) 2005, Adaptive Digital Technologies, Inc.
 *
 * File Name: NoiseSuppressStub.c
 *
 * Description:
 *   This file contains functions to allow 
 *
 * Version: 1.0
 *
 * Revision History:
 *
 *   5/2005  - Initial release
 *
 */
	
// Application related header files.
#include "GpakDefs.h"
#include "GpakErrs.h"
#include "GpakExts.h"
#include "GpakHpi.h"
#include "sysconfig.h"

#define ReplyWords(wordCnt)    ( (wordCnt + 1) / 2 )   // number of 32-bit words to needed to support 16-bit word count

ADT_UInt16 NoiseSuppress (NSuppress_t *Noise, short **inBufr, CircBufInfo_t *outBuffer,
                      ADT_UInt16 FrameSize) {
  return FrameSize;
}
GPAK_SysParmsStat_t ProcWriteNoiseParmsMsg (ADT_Int16 *pCmd,   ADT_UInt16 *pReply) {
   pReply[0] |= (MSG_WRITE_NOISE_PARAMS_REPLY << 8);
   return Sp_NoiseSuppressionNotConfigured;
}

ADT_UInt16 ProcReadNoiseParmsMsg (ADT_Int16 *pCmd, ADT_Int16 *pReply) {

  pReply[0] |= (MSG_NULL_REPLY << 8);
  return  ReplyWords (1);
}

