/*
 * Copyright (c) 2004, Adaptive Digital Technologies, Inc.
 *
 * File Name: ToneGenStub.c
 *
 * Description:
 *   This file contains the G.PAK Tone Generation stub functions.
 *
 * Version: 1.0
 *
 * Revision History:
 *   06/15/04 - Initial release.
 *
 */


void TG_ADT_init (void *Instance,	void *CPParams) {
	return;
}

void TG_ADT_init_1 (void *Instance, void *CPParams) {
	return;
}

void TG_ADT_init_highres (void *Instance, void *CPParams) {
	return;
}

short int TG_ADT_generate (void *Instance,	short int NSamples,
	short int OutSignal[])
{
	return (0);
}
