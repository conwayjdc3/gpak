/*
 * Copyright (c) 2004-2007, Adaptive Digital Technologies, Inc.
 *
 * File Name: CircDataStub.c
 *
 * Description:
 *   This file contains stubs of functions that support Circuit Data type
 *   channels when that channel type is not configured in the build.
 *
 * Version: 1.0
 *
 * Revision History:
 *   4/21/04 - Initial release.
 *   2/2007  - Merging C54-C64
 */
	
// Application related header files.
#include "GpakDefs.h"
#include "GpakErrs.h"
#include "GpakExts.h"
#include "sysconfig.h"


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ProcCfgCircDataMsg - Process a Configure Circuit Data Channel message.
//
//
GPAK_ChannelConfigStat_t ProcCfgCircDataMsg (ADT_UInt16 *pCmd, chanInfo_t *pChan) {
   return (Cc_ChanTypeNotConfigured);
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ProcCircDataStatMsg - Process a Circuit Data Channel Status message.
//
//
ADT_UInt16 ProcCircDataStatMsg (ADT_UInt16 *pReply, chanInfo_t *pChan) {
   return (0);
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// FrameOutCircData - Host to PCM framing for a Circuit Data type channel.
//
void FrameOutCircData (chanInfo_t *chan,  void *pktBuff,  ADT_PCM16 *pcmBuff, ADT_PCM16 *pECFarWk,
                                           int FrameSize, int pcmBuffBytes) {
   return;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// FrameInCircData - PCM to Host framing for a Circuit Data type channel.
//
void FrameInCircData (chanInfo_t * chan,  ADT_PCM16 *pcmBuff,    void *pktBuff,
                                          ADT_PCM16 *pECFarWork, int FrameSize) {

    return;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// SetupCircDataChan - Setup a Circuit Data channel.
//
//
void SetupCircDataChan (chanInfo_t *pChanInfo) {

   return;
}
