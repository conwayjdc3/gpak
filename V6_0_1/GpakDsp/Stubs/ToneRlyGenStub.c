/*
 * Copyright (c) 2004, Adaptive Digital Technologies, Inc.
 *
 * File Name: ToneRlyGenStub.c
 *
 * Description:
 *   This file contains the G.PAK Tone Relay Generate stub functions.
 *
 * Version: 1.0
 *
 * Revision History:
 *   06/15/04 - Initial release.
 *
 */
#include "GpakDefs.h"
TRResultCode_e TR_ADT_generateInit (TRGenerateInstance_t* inst, TRGenerateParamsV701_t* params) {	return TR_RESULT_OK;  }

ADT_Int16 TR_ADT_generator (TRGenerateInstance_t* inst, ADT_Int16 frameSize, ADT_Int16* OutBuf) { return 0; }

TRResultCode_e TR_ADT_sendNewEventToGenerator (TRGenerateInstance_t* inst, TRGenerateEvent_t* toneEvt) { return TR_RESULT_OK; }
