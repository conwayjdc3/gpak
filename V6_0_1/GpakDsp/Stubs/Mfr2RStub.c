/*
 * Copyright (c) 2002, Adaptive Digital Technologies, Inc.
 *
 * File Name: Mfr2RStub.c
 *
 * Description:
 *   This file contains the G.PAK MFR2 Reverse stub functions.
 *
 * Version: 1.0
 *
 * Revision History:
 *   04/23/02 - Initial release.
 *
 */
#include "GpakDefs.h"

#if ((DSP_TYPE == 54) ||(DSP_TYPE == 55))
void MFR2R_ADT_initialize(TDInstance_t *TDChannel){
	return;
}
#elif (DSP_TYPE == 64)
void MFR2R_ADT_initialize(TDInstance_t *DTMFChannel, TDScratch_t *ScratchPtr) {
   return;
}
void MFR2R_ADT_toneSuppress(
 	    void *pToneRelayDt,		//Tone relay instance
		TDInstance_t *DTMFInstance,
		ADT_Int16 InputSamples[],
		ADT_Int16 FrameSize,
		ADT_Int16 Suppression,
		ADT_Int16 OutputSamples[]) {
    return;
}
#endif
