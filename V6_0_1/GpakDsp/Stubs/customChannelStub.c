#include "GpakDefs.h"


#pragma CODE_SECTION (customProcChanCfgMsg, "SLOW_PROG_SECT")
GPAK_ChannelConfigStat_t customProcChanCfgMsg (ADT_UInt16 *pCmd, chanInfo_t *pChan) {
   // Called by messaging to parse a custom channel configuration message
   return Cc_InvalidChannelType;
}

#pragma CODE_SECTION (customProcChanStatMsg, "SLOW_PROG_SECT")
ADT_UInt16 customProcChanStatMsg (ADT_UInt16 *pReply, chanInfo_t *pChan) { 
   // Called by messaging to obtain a custom channel status reply
   return 0; 
}

#pragma CODE_SECTION (customSetupChan, "SLOW_PROG_SECT")
void customSetupChan (chanInfo_t *pChan) {
   // Called by messaging to allocate and initialize channel structures
}

#pragma CODE_SECTION (customFrameDecode, "SLOW_PROG_SECT")
void customFrameDecode (chanInfo_t *pChan, void  *pktBuff, ADT_PCM16 *pcmBuff,
                      ADT_PCM16 *pECFarWork, int SampsPerFrame, int pcmBuffBytes) {
   // Called by framing to decode a vocoder payload
}

#pragma CODE_SECTION (customFrameEncode, "SLOW_PROG_SECT")
void customFrameEncode (chanInfo_t *pChan, ADT_PCM16 *pcmBuff, void *pktBuff,
                     ADT_PCM16 *pECFarWork, int SampsPerFrame, int pktBuffBytes) {
    // Called by framing to encode a vocoder payload
}

#pragma CODE_SECTION (customSetDevicePointers, "SLOW_PROG_SECT")
void customSetDevicePointers (chanInfo_t *chan, 
                      pcm2pkt_t *pcmToPkt,  pkt2pcm_t *pktToPcm, 
                      ADT_UInt16 APhase,    ADT_UInt16 BPhase) {
   // Called by TDM thread to initialize the channel's circular buffers
}

#pragma CODE_SECTION (customGetPlaybackInstances, "SLOW_PROG_SECT")
void customGetPlaybackInstances (chanInfo_t *chan, GpakDeviceSide_t deviceSide,
                                 playRecInfo_t **playInfo, playRecInfo_t **recInfo,
                                 ADT_UInt16 *samplesPerFrame) {
   // Called by framing obtain a custom channel's playback and record instances
   *playInfo = *recInfo = NULL;
   *samplesPerFrame = 0;
}

#pragma CODE_SECTION (customProcessConference, "SLOW_PROG_SECT")
void customProcessConference (int cnfrID, ConferenceInfo_t *Cnfr, ConfInstance_t *CnfrInst,
                       ADT_Int16 *pWork1, void *scratch) {
   // Called by framing to process a custom conference
}

#pragma CODE_SECTION (customTeardown, "SLOW_PROG_SECT")
void customTeardown  (chanInfo_t *chan) { 
   // Called by messaging to deallocate channel instance memory
   return; 
}

#pragma CODE_SECTION (customInvalidateChannelCache, "MEDIUM_PROG_SECT")
void customInvalidateChannelCache  (chanInfo_t *chan) { 
   // Called by framing to invalidate custom instance data allocated to the channel
   return; 
}
