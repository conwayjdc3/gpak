#include "GpakDefs.h"

void MELPE_ADT_encode (ADT_Int16 PCM[], unsigned char *pyld, MelpE_Enc_Inst_t *inst) { return; }
void MELPE_ADT_decode (unsigned char *pyld, ADT_Int16 PCM[], MelpE_Dec_Inst_t *inst, ADT_Int16 Erase) { return; }

void MELPE_ADT_initEncode (MelpE_Enc_Inst_t *inst, ADT_Int16 rate, ADT_Int16 frameSize, ADT_Int32 *Scratch) { return; }
void MELPE_ADT_initDecode (MelpE_Dec_Inst_t *inst, ADT_Int16 rate, ADT_Int16 PostFilterEnable, ADT_Int16 frameSize, ADT_Int32 *Scratch) { return; }

