/*
 * Copyright (c) 2002, Adaptive Digital Technologies, Inc.
 *
 * File Name: Mfr1Stub.c
 *
 * Description:
 *   This file contains the G.PAK MFR1 stub functions.
 *
 * Version: 1.0
 *
 * Revision History:
 *   04/23/02 - Initial release.
 *
 */
#include "GpakDefs.h"

#if ((DSP_TYPE == 54) ||(DSP_TYPE == 55))
void MFR1_ADT_initialize(TDInstance_t *TDChannel){
	return;
}
#elif (DSP_TYPE == 64)
void MFR1_ADT_initialize(TDInstance_t *DTMFChannel, TDScratch_t *ScratchPtr) {
   return;
}
#endif
