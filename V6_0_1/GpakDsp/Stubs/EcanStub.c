/*
 * Copyright (c) 2002, Adaptive Digital Technologies, Inc.
 *
 * File Name: EcanStub.c
 *
 * Description:
 *   This file contains the G.PAK Echo Canceller stub functions.
 *
 * Version: 1.0
 *
 * Revision History:
 *   04/23/02 - Initial release.
 *
 */
#include "GpakDefs.h"

#if (DSP_TYPE == 54)
void LEC_ADT_g168Init  (G168ChannelInstance_t *Channel,  G168Params_t *Params,
   ADT_Int16 *EchoPath,         G168_DA_State_t *DAState,  G168_SA_State_t *SAState,
   G168_Scratch_t *G168Scratch,  ADT_Int16 *BG_EchoPath,  G168_DAScratch_t *G168_DAScratch,
   G168_BG_SA_State_t *BG_FarState, G168_BG_SA_State_t *BG_NearState,
   ADT_Int16  *Shared_EchoPath) {
	return;
}
#elif (DSP_TYPE == 64)
ADT_UInt8 LEC_ADT_g168Init (G168ChannelInstance_t *Channel,  G168Params_t *Params, 
   ADT_Int16 *EchoPath        , G168_DA_State_t *DAState,   G168_SA_State_t *SAState,
   G168_Scratch_t *G168Scratch,  ADT_Int16 *BG_EchoPath,    G168_DAScratch_t *DAScratch) {
   return 0;
}
#endif

#if (DSP_TYPE == 64)
void
#else
ADT_Int16 
#endif
    LEC_ADT_g168Cancel (G168ChannelInstance_t *Channel, ADT_Int16 Near[], ADT_Int16 Far[],
                         ADT_Int16 NearPreNLP[]) {
#if (DSP_TYPE != 64)
	return 1;
#endif
}

void LEC_ADT_coefStore(G168ChannelInstance_t *ChannelInst, Int16 *store_p) {
    return;
}

void LEC_ADT_coefLoad(G168ChannelInstance_t *ChannelInst, Int16 *store_p) {
    return;
}
void LEC_ADT_g168GetConfig (G168ChannelInstance_t *Channel, 
					   G168Params_t *Params) {
    return;
}

void LEC_ADT_g168GetStatus (G168ChannelInstance_t*Channel, G168StatusV11_t* status) {
   return;
}
