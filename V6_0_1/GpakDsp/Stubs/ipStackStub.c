//==========================================================================
//          File:  ipStackStub.C
//   Description:  Contains stubs for RTP functions required by G.PAK
//
//==========================================================================
#include <adt_typedef.h>

//#pragma CODE_SECTION (ipStackInit, "SLOW_PROG_SECT")
// jdc temp...void ipStackInit () { return; }

#pragma CODE_SECTION (addToIPSession, "SLOW_PROG_SECT")
int addToIPSession (void *new, void *RTPSession, ADT_UInt16 ChanId) {
//int addToIPSession (ADT_UInt32 rmtIPAddr, ADT_UInt16 rmtPort, ADT_UInt32 rmtSSRC, ADT_UInt8 rmtMAC[], void *RTPSession, ADT_UInt16 ChanId) {
   return 1;
}

#pragma CODE_SECTION (removeFromIPSession, "SLOW_PROG_SECT")
int removeFromIPSession (ADT_UInt16 ChanId) { return 1; }

#pragma CODE_SECTION (rtpPortValid, "SLOW_PROG_SECT")
int rtpPortValid (int rtpPort) { return ADT_TRUE; }
