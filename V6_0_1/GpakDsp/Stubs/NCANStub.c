/*
 * Copyright (c) 2005, Adaptive Digital Technologies, Inc.
 *
 * File Name: NCANStub.c
 *
 * Description:
 *   This file contains functions to allow 
 *
 * Version: 1.0
 *
 * Revision History:
 *
 *   5/2005  - Initial release
 *
 */
	
// Application related header files.
#include "GpakDefs.h"
#include "GpakErrs.h"
#include "GpakExts.h"
#include "GpakHpi.h"
#include "sysconfig.h"

void NCAN_ADT_config () {
  return;
}
void NCAN_ADT_init(NCAN_Channel_t *NCAN_Channel, NCAN_Params_t *NC_Params) {
  return;
}
void NCAN_ADT_cancel(NCAN_Channel_t *NCAN_Channel, ADT_Int16 *InSignal, ADT_Int16 CNGEnable, ADT_Int16 *OutSignal) {
  return;
}
