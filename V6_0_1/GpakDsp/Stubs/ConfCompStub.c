/*
 * Copyright (c) 2004, Adaptive Digital Technologies, Inc.
 *
 * File Name: ConfCompStub.c
 *
 * Description:
 *   This file contains stubs of functions that support Conference Composite
 *   type channels when that channel type is not configured in the build.
 *
 * Version: 1.0
 *
 * Revision History:
 *   4/21/04 - Initial release.
 *
 */
	
// Application related header files.
#include "GpakDefs.h"
#include "GpakErrs.h"
#include "GpakExts.h"


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ProcCfgConfCompMsg - Process a Configure Conference Composite Channel msg.
//
GPAK_ChannelConfigStat_t ProcCfgConfCompMsg (ADT_UInt16 *pCmd, chanInfo_t *pChan) {
   return (Cc_ChanTypeNotConfigured);
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ProcConfCompStatMsg - Process a Conference Composite Channel Status message.
//
ADT_UInt16 ProcConfCompStatMsg (ADT_UInt16 *pReply, chanInfo_t *pChan) {

   return (0);
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// SetupConfCompChan - Setup a Conference Composite channel.
//
//
void SetupConfCompChan (chanInfo_t *pChanInfo) {

   return;
}
