/*
 * Copyright (c) 2002, Adaptive Digital Technologies, Inc.
 *
 * File Name: G723Stub.c
 *
 * Description:
 *   This file contains the G.PAK G.723 Vocoder stub functions.
 *
 * Version: 1.0
 *
 * Revision History:
 *   10/14/02 - Initial release.
 *
 */
#include "GpakDefs.h"

void G722_ADT_reset (G722ChannelInstance_t *Channel_Ins, short int Mode) {
    return; }

void G722_ADT_encode (G722ChannelInstance_t *Channel_Ins, short int in_array[], short int *EncodeWord) {
    return; }

void G722_ADT_decode (G722ChannelInstance_t *Channel_Ins, short int DecodeWord, short int out_array[]) {
    return; }
