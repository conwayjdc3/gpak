/*
 * Copyright (c) 2002, Adaptive Digital Technologies, Inc.
 *
 * File Name: ConfStub.c
 *
 * Description:
 *   This file contains the G.PAK Conference stub functions.
 *
 * Version: 1.0
 *
 * Revision History:
 *   07/10/02 - Initial release.
 *
 */
#include "GpakDefs.h"

ADT_Int16 CONF_ADT_init (ConfParams_t *Params, ConfInstance_t *Inst,
                    ConfScratch_t *Scratch, AGCInstance_t *AgcChan_p) {
	return (0);
}

ADT_Int16 CONF_ADT_run (ConfInstance_t *Inst, ADT_Int16 *CompositeOut)
{
	return (0);
}

ADT_Int16 CONF_ADT_addMember (ConfInstance_t *Inst, conf_Member_t *Mem) {
	return (0);
}

void CONF_ADT_removeMember (ConfInstance_t *Inst, conf_Member_t *Mem) {
	return;
}

void CONF_ADT_close (ConfInstance_t *Inst) {
	return;
}

void CONF_ADT_setPriority(conf_Member_t *pBlock) {
    return;
}

void CONF_ADT_clearPriority(conf_Member_t *pBlock) {
    return;
}



