/*
 * Copyright (c) 2002, Adaptive Digital Technologies, Inc.
 *
 * File Name: G726Stub.c
 *
 * Description:
 *   This file contains the G.PAK G.726 stub functions.
 *
 * Version: 1.0
 *
 * Revision History:
 *   04/23/02 - Initial release.
 *
 */
#include "adt_typedef.h"
#include "g726_user.h"

int G726_ADT_Init(G726ChannelInstance_t  *vpState, int rate, int law) 
{ return 0; }

unsigned char G726_ADT_Encode(G726ChannelInstance_t  *vpState, unsigned char cI)
{ return 0; }

unsigned char G726_ADT_Decode(G726ChannelInstance_t  *vpState, unsigned char cI)
{ return 0; }
