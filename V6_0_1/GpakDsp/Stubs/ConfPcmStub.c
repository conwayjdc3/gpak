/*
 * Copyright (c) 2004, Adaptive Digital Technologies, Inc.
 *
 * File Name: ConfPcmStub.c
 *
 * Description:
 *   This file contains stubs of functions that support Conference PCM type
 *   channels when that channel type is not configured in the build.
 *
 * Version: 1.0
 *
 * Revision History:
 *   4/21/04 - Initial release.
 *
 */
	
// Application related header files.
#include "GpakDefs.h"
#include "GpakErrs.h"
#include "GpakExts.h"
#include "sysconfig.h"


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ProcCfgConfPcmMsg - Process a Configure Conference PCM Channel message.
//
GPAK_ChannelConfigStat_t ProcCfgConfPcmMsg (ADT_UInt16 *pCmd, chanInfo_t *pChan) {
   return Cc_ChanTypeNotConfigured;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ProcConfPcmStatMsg - Process a Conference PCM Channel Status message.
//
ADT_UInt16 ProcConfPcmStatMsg (ADT_UInt16 *pReply, chanInfo_t *pChan) {

   return (0);
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// SetupConfPcmChan - Setup a Conference PCM channel.
//
void SetupConfPcmChan (chanInfo_t *pChanInfo) {
   return;
}
