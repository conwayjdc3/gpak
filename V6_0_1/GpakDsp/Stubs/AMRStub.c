/*
 * Copyright (c) 2007, Adaptive Digital Technologies, Inc.
 *
 * File Name: AMRStub.c
 *
 * Description:
 *   This file contains the G.PAK AMR Vocoder stub functions.
 *
 * Version: 1.0
 *
 * Revision History:
 *   02/07 - Initial release.
 *
 */
#include "GpakDefs.h"
#if (DSP_TYPE == 54)
void AMR_ADT_encInit (AMREncInstance_t *AMREncInst , INT16 dtx_flag) {
   return;
}
void AMR_ADT_encode(AMREncInstance_t *AMREncInst, INT16 mode, INT16 dtx_flag, 
                 INT16 speech[], INT16 compressed[], INT16* usedMode,INT16* TxFrType, INT16 Output_type) {
    return;
}


void 	AMR_ADT_decInit (AMRDecInstance_t  *DecInst) {
   return;
}
void AMR_ADT_decode(AMRDecInstance_t *AMRDecInst, INT16 mode, INT16 parm[],
                   INT16 frame_type, INT16 speech[],INT16 flag_13bit, INT16 InputType) {
   return;
}

#elif ((DSP_TYPE == 64) || (DSP_TYPE == 55))
void AMR_ADT_encInit (AMREncodeChannelInstance_t *state, ADT_Int16 dtx_flag, AMREncodeScratchInstance_t *Scratch) {
   return;
}

void AMR_ADT_encode (AMREncodeChannelInstance_t *ptrInst, ADT_Int16 mode, 	ADT_Int16 dtx, 
                     ADT_Int16 new_speech[], ADT_Int16 ana[], ADT_Int16* usedMode, 
                     ADT_Int16   *TxFrType,  ADT_Int16 Output_type) {
   return;
}

void AMR_ADT_decInit (AMRDecodeChannelInstance_t *state, AMRDecodeScratchInstance_t *Scratch) {
   return;
}
void AMR_ADT_decode(AMRDecodeChannelInstance_t *st, ADT_Int16 mode, ADT_Int16 *serial,
                    ADT_Int16 frame_type,  ADT_Int16 speech[],  ADT_Int16 InputType) {
   return;
}
#endif
