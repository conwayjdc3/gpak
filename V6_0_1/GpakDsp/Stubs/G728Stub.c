/*
 * Copyright (c) 2002, Adaptive Digital Technologies, Inc.
 *
 * File Name: G728Stub.c
 *
 * Description:
 *   This file contains the G.PAK G.728 stub functions.
 *
 * Version: 1.0
 *
 * Revision History:
 *   04/23/02 - Initial release.
 *
 */
#include "GpakDefs.h"

#if (DSP_TYPE == 54)
void InitializeG728(
	struct G728Channel *ptr
	)
{
	return;
}

void EncodeG728(
	struct G728Channel *ptr, 
	short int in_array[],
	short int EncodeStream[]
	)
{
	return;
}

void DecodeG728
	(
	struct G728Channel *ptr,
	short int DecodeStream[],
	short int out_array[],
	short int PostFiltEnable
	)
{
	return;
}

#elif ((DSP_TYPE == 64) || (DSP_TYPE == 55))
void  G728_ADT_encInit (G728EncodeChannelInstance_t* ptrInst, G728ScratchInstance_t* ScratchInst, ADT_Int16 Rate) 
{ return; }
void  G728_ADT_encode (G728EncodeChannelInstance_t* ptrInst, ADT_Int16 in_array[], ADT_Int16 EncodeOutStream[])
{ return; }


void  G728_ADT_decInit (G728DecodeChannelInstance_t* ptrInst, G728ScratchInstance_t* ScratchInst, ADT_Int16 PostEnable, ADT_Int16 Rate)
{ return; }
void  G728_ADT_decode (G728DecodeChannelInstance_t* ptrInst, ADT_Int16 out_array[], ADT_Int16 DecodeInStream[])
{ return; }

#endif
