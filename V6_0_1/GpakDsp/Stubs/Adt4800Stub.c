/*
 * Copyright (c) 2002, Adaptive Digital Technologies, Inc.
 *
 * File Name: Adt4800Stub.c
 *
 * Description:
 *   This file contains the G.PAK ADT-4800 Vocoder stub functions.
 *
 * Version: 1.0
 *
 * Revision History:
 *   07/25/02 - Initial release.
 *
 */
#include "GpakDefs.h"

void decInitialize4800Channel (
   Dec48Chan_t *TheChanPtr
   )
{
	return;
}

void Decode_ADT_4800 (
   Dec48Chan_t *TheChanPtr,
   short int DecodeStream[],
   short int OutputSpeech[],
   short int FrameEraseFlag
   )
{
	return;
}

void codInitialize4800Channel (
    Enc48Chan_t *TheChanPtr
   )
{
	return;
}

void Encode_ADT_4800 (
   Enc48Chan_t *TheChanPtr,
   short int InputSpeech[],
   short int EncodeStream[]
   )
{
	return;
}
