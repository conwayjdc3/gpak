/*
 * Copyright (c) 2005, Adaptive Digital Technologies, Inc.
 *
 * File Name: t38FaxRelayStub.c
 *
 * Description:
 *   This file contains functions to allow 
 *
 * Version: 1.0
 *
 * Revision History:
 *
 *   5/2005  - Initial release
 *
 */
	
// Application related header files.
#include "GpakDefs.h"
#include "GpakErrs.h"
#include "GpakExts.h"
#include "GpakHpi.h"
#include "sysconfig.h"

int RlyPacketToRelay (struct FAX_STRUCT *f,unsigned char *msgbuf, int length ) {
    return(-1);
}

int RlyPacketFmRelay (struct FAX_STRUCT *f,unsigned char *pMsgRet, int MaxLen ) {
    return(0);
}
    
struct FAX_STRUCT* RlyRelayInitChannel(unsigned char* mem,int n,struct RlyInitChannelStruct *p) {
    return((struct FAX_STRUCT*)0);
}

int RlyRelayIf (ADT_Int16 *in,  ADT_Int16 *out, int FillLength,
	struct FAX_STRUCT *fax
#ifdef ASYNC_RX_TX
,  int correction
#endif
)
{
    return(0);
}

void AllocFaxRelay (chanInfo_t *pChan) {

    // init fax state variables
    memset (&pChan->fax, 0, sizeof (faxInfo_t));
    return;
}

void DeallocFaxRelay (chanInfo_t *pChan) {
    return;
}

void processFaxRelay (chanInfo_t *pChan,  ADT_PCM16 *pcmInBuff, 
                     void *pScratch, int FrameSize) { 
    return;
}




void T38_Check_Timeout (chanInfo_t *pChan,  int SamplesPerFrame) {
   return;
}

void T38_Restart_Voice (chanInfo_t *pChan) {
   return;
}



int T38_Add_Packet (chanInfo_t *pChan, void *pyld, int PayldBytes, ADT_UInt32 timeStamp) {
   return TRUE;
}

void T38_Encode_Start (void) { 
   return;
}

void *FaxBufrReset (void) {
   return (void *) 0x90000000;
}

void captureFaxOutput (chanInfo_t *pChan, void *data, int FrameSize) {
   return;
}
void invalidateFaxRelay (chanInfo_t *pChan) { return; }

#pragma CODE_SECTION (startFaxCapture, "SLOW_PROG_SECT")
void startFaxCapture (int startCondition, int enableFlags) { return; }

// Disable active captures and send capture complete message to host when
// endCondition is true
#pragma CODE_SECTION (stopFaxCapture, "SLOW_PROG_SECT")
void stopFaxCapture (int endCondition) {   return; }

