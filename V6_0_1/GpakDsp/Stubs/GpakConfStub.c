/*
 * Copyright (c) 2004, Adaptive Digital Technologies, Inc.
 *
 * File Name: GpakConfStub.c
 *
 * Description:
 *   This file contains functions to support G.PAK conferences.
 *
 * Version: 1.0
 *
 * Revision History:
 *   5/07/04 - Initial release.
 *
 */

// Application related header files.
#include "GpakDefs.h"
#include "GpakExts.h"
#include "GpakErrs.h"

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// GetCnfrMbrQueueHead - Get the address of a conference member queue head.
//}
#pragma CODE_SECTION (GetGpakCnfr, "SLOW_PROG_SECT")
ConferenceInfo_t *GetGpakCnfr (int CoreID, int Cnfr) {

   return NULL;
}

#pragma CODE_SECTION (ProcConfToneGenMsg, "SLOW_PROG_SECT")
ADT_UInt16 ProcConfToneGenMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) { 

       return Tg_InvalidChannel;

}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ProcConfigConferenceMsg - Process a Configure Conference message.
//
#pragma CODE_SECTION (ProcConfigConferenceMsg, "SLOW_PROG_SECT")
GPAK_ConferConfigStat_t ProcConfigConferenceMsg (ADT_UInt16 *pCmd) {

   return Cf_InvalidConference;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ProcessConference - Process a conference and all associated channels.
//
#pragma CODE_SECTION (ProcessConference, "SLOW_PROG_SECT")
void ProcessConference (ConferenceInfo_t *pConference,
   ConfInstance_t *pConfChan, ADT_PCM16 *pInWork, void *pOutWork, void *work2, ADT_UInt16 workI8) {

   return;
}
