/*
 * Copyright (c) 2006, Adaptive Digital Technologies, Inc.
 *
 * File Name: AECStub.c
 *
 * Description:
 *   This file contains the G.PAK acoustic echo cancellation stub functions.
 *
 * Version: 1.0
 *
 * Revision History:
 *   03/06 - Initial release.
 *
 */
 
//#include "std.h"
//#include "ialg.h"

#include <std.h>
#ifndef AEC_SUPPORTED
#define NULL 0
#endif


#include <ti\xdais\ialg.h>
#ifdef AEC_SUPPORTED
/*
// ===========================================================================
// AECG4_create
//
//  Create an AECG4 instance object (using parameters specified by prms)
*/
#pragma CODE_SECTION (AECG4_ADT_alloc, "SLOW_PROG_SECT")
void AECG4_ADT_alloc (void *Params, void *fnxs, IALG_MemRec *memTab) {
   memset (memTab, 0, sizeof (IALG_MemRec) * 7);
   return;
}
#else
void AECG4_ADT_alloc (void *Params, void *fnxs, void *memTab) {

return;
}
#endif

#pragma CODE_SECTION (AECG4_ADT_createStatic, "SLOW_PROG_SECT")
void *AECG4_ADT_createStatic (void *fxns, void *params, void *memTab) { return NULL; }

/*
// ===========================================================================
// AECG4_apply
*/
#pragma CODE_SECTION (AECG4_ADT_apply, "SLOW_PROG_SECT")
void AECG4_ADT_apply (void *handle, void* ptrRxIn, void* ptrRxOut, void* ptrTxIn, void* ptrTxOut) { }

/*
// ===========================================================================
// AECG4_backgroundHandler
*/
#pragma CODE_SECTION (AECG4_ADT_backgroundHandler, "SLOW_PROG_SECT")
void AECG4_ADT_backgroundHandler (void *handle) { }

/*
// ===========================================================================
// AECG_reset
*/
#pragma CODE_SECTION (AECG4_ADT_reset, "SLOW_PROG_SECT")
void AECG4_ADT_reset (void *handle, void *prms) { }
