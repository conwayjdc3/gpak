/*
 * Copyright (c) 2004, Adaptive Digital Technologies, Inc.
 *
 * File Name: Pcm2PktStub.c
 *
 * Description:
 *   This file contains stubs of functions that support PCM To Packet type
 *   channels when that channel type is not configured in the build.
 *
 * Version: 1.0
 *
 * Revision History:
 *   4/21/04 - Initial release.
 *
 */
	
// Application related header files.
#include "GpakDefs.h"
#include "GpakErrs.h"
#include "GpakExts.h"


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ProcCfgPcm2PktMsg - Process a Configure PCM To Packet Channel message.
//

GPAK_ChannelConfigStat_t ProcCfgPcm2PktMsg (ADT_UInt16 *pCmd, chanInfo_t *pChan){

   return Cc_ChanTypeNotConfigured;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ProcPcm2PktStatMsg - Process a PCM To Packet Channel Status message.
//
ADT_UInt16 ProcPcm2PktStatMsg (ADT_UInt16 *pReply, chanInfo_t *pChan) {
   return (0);
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// FramePcmA2PktB - Host to PCM framing for a PCM To Packet type channel.
void FramePcmA2PktB  (chanInfo_t *chan, ADT_PCM16 *pktBuff, void *pcmBuff,
                       ADT_PCM16 *Scratch, int FrameSize) {

   return;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// FramePktB2PcmA - PCM to Host framing for a PCM To Packet type channel.
//
void FramePktB2PcmA (chanInfo_t *chan, void *pcmBuff, ADT_PCM16 *pktBuff,
                      ADT_PCM16 *pECFarWork, int FrameSize, int pcmBuffBytes) {
   return;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// SetupPcm2PktChan - Setup a PCM To Packet channel.

void SetupPcm2PktChan (chanInfo_t *pChanInfo){

   return;
}
