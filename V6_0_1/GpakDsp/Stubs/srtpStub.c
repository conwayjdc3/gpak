/*
 * Copyright (c) 2002, Adaptive Digital Technologies, Inc.
 *
 * File Name: G723Stub.c
 *
 * Description:
 *   This file contains the G.PAK G.723 Vocoder stub functions.
 *
 * Version: 1.0
 *
 * Revision History:
 *   10/14/02 - Initial release.
 *
 */
#include "GpakDefs.h"

ADT_UInt32 SrtpEvalContextSizeU8(
    SrtpInstanceCfg_V1_t *pCfgParms,   // pointer to SRTP configuration parameters
    ADT_UInt16 *pScratchMemSizeU8,      // pointer to size of scratch mem variable
    ADT_UInt16 *pExtraPktLenU8          // pointer to extra packet length variable
    )
{
	return 0;
}

SrtpStatus_t SrtpInitializeInstance (
    void       *pInstMem,            // pointer to aligned SRTP/SRTCP instance memory
    ADT_UInt32 *pScratchMem,         // pointer to scratch memory
    SrtpInstanceCfg_V1_t *pCfgParms, // pointer to SRTP configuration parameters
    SrtpCallBack_t *pAppCallBack,    // pointer to application callback function
    void *pAppHandle                 // application handle passed in callback
    )
{
	return SRTP_STAT_SUCCESS;
}

ADT_UInt16 SrtpEncryptU8 (
    void *pInstMem,                 // pointer to aligned SRTP/SRTCP instance memory
    void *pInPkt,                   // pointer to unencrypted input packet
    ADT_UInt16 InPktLenU8,          // input packet length (octets)
    void *pOutPktBufr               // pointer to encrypted output packet buffer
    )
{
	return 0;
}
ADT_UInt16 SrtpDecryptU8 (
    void *pInstMem,            // pointer to aligned SRTP/SRTCP instance memory
    void *pInPkt,              // pointer to encrypted input packet
    ADT_UInt16 InPktLenU8,     // input packet length (octets)
    void *pOutPktBufr          // pointer to unencrypted output packet buffer
    )
{
	return 0;
}
void SrtpSetRollOverCounter (
    void *pInstMem,            // pointer to aligned SRTP/SRTCP instance memory
    ADT_UInt32 rollOverCount   // roll over counter value
    )
{
	return;
}
SrtpGetStatistics (
    void *pInstMem,             // pointer to aligned SRTP/SRTCP instance memory
    SrtpStats_t *pStatistics    // pointer to statistics variable
    )
{
	return FALSE;
}
