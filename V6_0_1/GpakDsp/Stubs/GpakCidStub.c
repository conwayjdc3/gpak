#include <string.h>
#include "GpakDefs.h"
#include "sysconfig.h"
#include "GpakHpi.h"

#if 1
void NullCID (cidInfo_t *pCid) {
    memset(pCid, 0, sizeof(cidInfo_t));
    pCid->RxCIDIndex = VOID_INDEX;
    pCid->TxCIDIndex = VOID_INDEX;
}
#endif

#if 1
void getRxCidPtrs(int i, CIDRX_Inst_t **ppRxCidChan, ADT_UInt8 **pRxCidBuf) {
    *ppRxCidChan = (CIDRX_Inst_t *)0;
    *pRxCidBuf   = (ADT_UInt8 *)0;
}
#endif

#if 1
void getTxCidPtrs(int i, CIDTX_Inst_t **ppTxCidChan, ADT_UInt8 **pTxCidMsgBuf, ADT_UInt8 **pTxCidBrstBuf) {
    *ppTxCidChan = (CIDTX_Inst_t *)0;
    *pTxCidMsgBuf   = (ADT_UInt8 *)0;
    *pTxCidBrstBuf   = (ADT_UInt8 *)0;
}
#endif

#if 1
void txCidProcess(int chanId, GpakDeviceSide_t side, cidInfo_t *pInfo, ADT_PCM16 *pcmBuff, int FrameSize) {
}
#endif

#if 1
#pragma CODE_SECTION (ProcTxCallerIDMsg, "SLOW_PROG_SECT")
ADT_UInt16 ProcTxCallerIDMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) { 
   ADT_UInt16      ChannelId;      // channel identifier

   // Get the Channel Id and Control Code from the command message.
   ChannelId = (pCmd[1] >> 8) & 0xFF;

   // Prepare the reply message.
   pReply[0] |= (MSG_WRITE_TXCID_DATA_REPLY << 8);
   pReply[1]  = (ChannelId << 8);

   return Scp_TxCidNotEnabled;
}
#endif


#if 1
void rxCidInit(cidInfo_t *pInfo, int FrameSize) {
}
#endif

#if 1
void rxCidProcess(int chanId, GpakDeviceSide_t side, cidInfo_t *pInfo, ADT_PCM16 *pcmBuff, int FrameSize) {
}
#endif

#if 1
void rxCid2SndProcess(int chanId, GpakDeviceSide_t side, cidInfo_t *pInfo, ADT_PCM16 *pcmBuff, int FrameSize) {
}
#endif

#if 1
void txCid2RcvProcess(int chanId, GpakDeviceSide_t side, cidInfo_t *pInfo, ADT_PCM16 *pcmBuff, int FrameSize) {

}
#endif
