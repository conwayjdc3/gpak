//==========================================================================
//          File:  rtpStub.C
//   Description:  Contains stubs for RTP functions required by G.PAK
//
//==========================================================================
#include "GpakExts.h"
#include "GpakHpi.h"
#include "GpakErrs.h"
#include "GpakEnum.h"
#include "GpakHpi.h"


int RTPLoopBack = DisableTest;

ADT_UInt32 HostSWIMaxTicks = 0;
ADT_UInt16 RTPScratchI8 = 1;
static int RTPBuff;

RtpChanData_t *RtpChanData[];

void RTP_Init () { 
   return;
}

// jdc return was: int
GPAK_RTPConfigStat_t ProcConfigRTPMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {
   int         chanId;

   chanId               = (pCmd [1] & 0x00FF);
   pReply[0] |= (MSG_RTP_CONFIG_REPLY << 8);
   pReply[1] = (chanId << 8);

    return (GPAK_RTPConfigStat_t)1;
}

int sendPacketToRTP  (chanInfo_t *chan, PktHdr_t *PktHdr, ADT_UInt16 *pyld)  { return 0; }
// jdc return was: int
packetStatus_t getPacketFromRTP (chanInfo_t *chan, PktHdr_t *PktHdr, ADT_UInt16 *pyld, ADT_UInt16 maxPayI8) { return (packetStatus_t)0; }

void postRtpHostSWI() { }
int ProcRTPStatusMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {
   int       chanId;
   chanId   = (pCmd [1] >> 8) & 0x00FF;
   pReply[0] = (MSG_RTP_STATUS_REPLY << 8) | ((ADT_UInt16)RTPStat_NotConfigured & 0xFF);
   pReply[1] = chanId << 8;
   return 2;
}


RTP_STAT RTP_ADT_Init  (RTPCONNECT *RTPConnect, NETHANDLE *RTPHandle, rtpConfig_APIV5_t *pConfig) { 
   return RTP_OPEN_STAT_SUCCESS; 
}
void  RTP_ADT_Close (RTPCONNECT *RTPConnect) {}

void *RTPAlloc (ADT_UInt32 sizeI8) {  return &RTPBuff; }
void  RTPFree  (void* Buff) { return; }
ADT_UInt32 RTPTime (int instID) { return 0; }


short int netTransmit (void * hndl, ADT_UInt32* data, int *dataI8) { return 0; }

#pragma CODE_SECTION (ProcessSetVLANTagMsg, "SLOW_PROG_SECT")
int ProcessSetVLANTagMsg (ADT_UInt16 *cmd, ADT_UInt16 *reply) {
   ADT_UInt16 vlanIdx;

   vlanIdx  = Byte0 (cmd[0]);

   // Format reply
   reply[0] |= MSG_SET_VLAN_TAG_REPLY << 8;
   reply[1]  = (vlanIdx << 8) | vlanUnAvailable;
   return 4;
} 

#pragma CODE_SECTION (ProcessGetVLANTagMsg, "SLOW_PROG_SECT")
int ProcessGetVLANTagMsg (ADT_UInt16 *cmd, ADT_UInt16 *reply) {
   ADT_UInt16 vlanIdx;

   vlanIdx  = Byte0 (cmd[0]);

   // Format reply
   reply[0] |= MSG_GET_VLAN_TAG_REPLY << 8;
   reply[1]  = (vlanIdx << 8) | vlanUnAvailable;
   return 4;
}

//-------------------------------------------------------------------------
//  If IP Stack is enabled with RTP disabled.
#pragma CODE_SECTION (rtpGetPacket, "MEDIUM_PROG_SECT")
int rtpGetPacket (void *IPBuff, int IPBuffI8, ADT_UInt16 *ChanID, int coreID) {
   return 0;
}

#pragma CODE_SECTION (storeInboundRTPPacket, "MEDIUM_PROG_SECT")
int storeInboundRTPPacket (ADT_UInt16 chanID, void *RTPBuff, int pktI8, void *PktHndl) {
   return 1;
}

#pragma CODE_SECTION (freePktHandles, "MEDIUM_PROG_SECT")
int freePktHandles (int coreID) {   return 1; }

#pragma CODE_SECTION (sendHostCoreMsg, "FAST_PROG_SECT")
int sendHostCoreMsg (ADT_UInt32* Data, int dataI8) { return 0; }
