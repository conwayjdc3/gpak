#include "GpakDefs.h"

SpeexEncodeHandle_t SPEEX_ADT_initEncoder (SpeexEncState_t *Inst,      ADT_UInt32 InstI8, 
                                           SpeexEncScratch_t *scratch, ADT_UInt32 scratchI8,
                                           SpeexControlAndStatus_t *cfg, ADT_UInt8 *result) {
   return NULL;
}

SpeexDecodeHandle_t SPEEX_ADT_initDecoder (SpeexDecState_t *Inst,      ADT_UInt32 InstI8, 
                                           SpeexDecScratch_t *scratch, ADT_UInt32 scratchI8,
                                           SpeexControlAndStatus_t *cfg, ADT_UInt8 *result) {
   return NULL;
}


int SPEEX_ADT_encode (SpeexEncodeHandle_t Inst, ADT_Int16 *PCM, ADT_UInt16 PCMCnt, 
                     ADT_UInt8 *payload, ADT_UInt16 *payloadI1) {
   return 0;
}

int SPEEX_ADT_decode (SpeexDecodeHandle_t Inst, ADT_UInt8 *payload, ADT_UInt16 payloadI1, 
                      ADT_Int16 *PCM, ADT_UInt16 *PCMCnt) {
   return 0;
}
