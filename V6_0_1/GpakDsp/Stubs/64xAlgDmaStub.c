#include "GpakDefs.h"
#include "GpakDma.h"
#include "GpakExts.h"

int measureDmaWait = 0;

void dmaAllocateChains () {
  return;
}

void dmaInitInfoStruct(
    chanInfo_t *pGpakChan, // Gpak channel structure
    ADT_UInt16 EcIndexA,   // index to "A-direction" echo canceller instance
    ADT_UInt16 EcIndexB)  // index to "B-direction" echo canceller instance
{

}

void dmaInitCtrl(
    dmaControl_t *dmaCtrl, // pointer to dma control struct
    int group)             // 1 == group B, 0 == group A
{

}

void dmaRestoreCoderPtrs(chanInfo_t *pGpakChan)
{

}

void dmaSetPtrs(
    dmaControl_t *dmaCtrl,  // pointer to dma control struct
    chanInfo_t *pGpakChan,  // pointer to gpak channel struct
    int         FrameSize)  // frame size
{

}

void dmaWaitforXferComplete(dmaControl_t *dmaCtrl, int rdFlag)
{
    return;
}

void dmaXfer(dmaControl_t *dmaCtrl, int channelId, int rdFlag)
{

}


void dmaInvalidateL1Cache(
    ecInstanceInfo_t *pEcInfo, 
    int EcIndex, 
    short int *encPtr, 
    GpakCodecs encCoding, 
    short int *decPtr, 
    GpakCodecs decCoding)
{

}

void dmaTrace(dmaControl_t *dmaCtrl, ADT_UInt16 channelId, ADT_UInt16 code)
{
    return;
}

