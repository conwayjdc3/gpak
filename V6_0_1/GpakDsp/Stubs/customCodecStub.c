// customCodecStub.c

#include "adt_typedef.h"

#pragma CODE_SECTION (customEncodeInit,  "MEDIUM_PROG_SECT")
#pragma CODE_SECTION (customDecodeInit,  "MEDIUM_PROG_SECT")
#pragma CODE_SECTION (customEncode,      "MEDIUM_PROG_SECT")
#pragma CODE_SECTION (customDecode,      "MEDIUM_PROG_SECT")
#pragma CODE_SECTION (customPack,        "MEDIUM_PROG_SECT")
#pragma CODE_SECTION (customUnpack,      "MEDIUM_PROG_SECT")
#pragma CODE_SECTION (customTeardown,    "MEDIUM_PROG_SECT")

int customEncodeInit (pcm2pkt_t *PcmPkt) { return 0; }
int customDecodeInit (pkt2pcm_t *PktPcm) { return 0; }
int customEncode     (chanInfo_t *pChan, void *pcm, void *params) { return 0; }
int customDecode     (chanInfo_t *pChan, void *params, void *pcm) { return 0; }
int customPack       (void *params, void *payload) { return 0; }
int customUnpack     (PktHdr_t *PktHdr, void *payload, void *params) { return 0; }

