/*
 * Copyright (c) 2002, Adaptive Digital Technologies, Inc.
 *
 * File Name: VadStub.c
 *
 * Description:
 *   This file contains the G.PAK VAD and CNG stub functions.
 *
 * Version: 1.0
 *
 * Revision History:
 *   04/23/02 - Initial release.
 *
 */
#include "GpakDefs.h"

void VADCNG_ADT_init (VADCNG_Instance_t *ChannelInst, 
                      VADCNG_ADT_Param_t *pParams) {
    return;
}

ADT_Int16 VADCNG_ADT_vad (VADCNG_Instance_t *ChannelInst, ADT_Int16 pcm_in[], ADT_Int16 *VadLvl) {
    return 1;
}

void VADCNG_ADT_cng (VADCNG_Instance_t *ChannelInst, ADT_Int16 VadFlag, ADT_Int16 Level, 
                     ADT_Int16 OutputBuffer[]) {
    return;
}
