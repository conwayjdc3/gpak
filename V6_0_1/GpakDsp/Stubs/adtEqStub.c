#include "adt_typedef.h"
#include "adtEq.h"

ADT_Int16 spkrEqCoef[] = { 0 };
ADT_Int16 micEqCoef[]  = { 0 };

#pragma CODE_SECTION (eqInit, "SLOW_PROG_SECT")
void eqInit(EqChannel_t *pChannel, EqParams_t *pParams)
{
}

#pragma CODE_SECTION (eqApply, "SLOW_PROG_SECT")
void eqApply(EqChannel_t *pChannel, ADT_Int16 *pIn, ADT_Int16 *pOut, ADT_Int16 NSamples)
{

}
