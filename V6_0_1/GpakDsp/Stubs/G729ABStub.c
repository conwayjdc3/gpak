/*
 * Copyright (c) 2002, Adaptive Digital Technologies, Inc.
 *
 * File Name: G728Stub.c
 *
 * Description:
 *   This file contains the G.PAK G.728 stub functions.
 *
 * Version: 1.0
 *
 * Revision History:
 *   04/23/02 - Initial release.
 *
 */
#include "adt_typedef.h"
#if (DSP_TYPE == 54)
   #define G729_ADT_MODE  G729_ADT_AB
   #include "g729user.h"
// ================ Encoder Interface ==========================

void codInitializeG729(CODG729_ADT_Channel_t *CODG729Channel, 
   CODG729_ADT_I_t *CODG729I,  CODG729_ADT_E_t *CODG729E,
   CODG729_ADT_H_t *CODG729H,  CODG729_ADT_B_t *CODG729B) {}

int EncodeG729(
   CODG729_ADT_Channel_t *CODG729Channel,  ADT_Int16 Speech[], 
   ADT_Int16 Compressed[],   ADT_Int16 VADEnable, 
   ADT_Int16 *VADFlag) { return 0; }



// ================ Decoder Interface ==========================

void decInitializeG729(DECG729_ADT_Channel_t *DECG729Channel, 
   DECG729_ADT_I_t *DECG729I, DECG729_ADT_E_t *DECG729E,
   DECG729_ADT_H_t *DECG729H, DECG729_ADT_B_t *DECG729B) {}
   
int DecodeG729(DECG729_ADT_Channel_t *DECG729Channel,
   ADT_Int16 Compressed[],  ADT_Int16 Speech[],
   ADT_Int16 FrameErase,   ADT_Int16 VADFlag) { return 0; }


#elif (DSP_TYPE == 64)
// Genereric G729 stub functions
int g729EncInit(void *chan, void *scratch) {
    return 0;
}

int g729Encode (void *chan, void *pcm, void *data, short Vad, short *FrameType) {

int i;
short *p = (short *)data;

    for (i=0; i<5; i++)
        *p++ = 0;
    
    *FrameType = 1;

    return 0;
}

int g729DecInit(void *chan, void *scratch) {
    return 0;    
}

int g729Decode (void *chan, void *pyld, void *pcm, short FrameErase, short VadFlag) {

int i;
short *p = (short *)pcm;

    for (i=0; i<80; i++)
        *p++ = 0;

    return 0;
}
#endif



