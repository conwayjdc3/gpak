/*
 * Copyright (c) 2007, Adaptive Digital Technologies, Inc.
 *
 * File Name: AMRFormatStub.c
 *
 * Description:
 *   This file contains the G.PAK AMR Vocoder stub functions.
 *
 * Version: 1.0
 *
 * Revision History:
 *   04/05 - Initial release.
 *
 */
#include "GpakDefs.h"

void parseAMRPayload (ADT_UInt16 *Payload, ADT_UInt16 *AMRData) {
  return;
}

void formatAMRPayload (ADT_UInt16 *AMRData, ADT_UInt16 *Payload) {
  return;
}
