// customAlgStub.c

#include "adt_typedef.h"

#pragma CODE_SECTION (custAecInput,       "MEDIUM_PROG_SECT")
#pragma CODE_SECTION (custAecOutput,      "MEDIUM_PROG_SECT")
#pragma CODE_SECTION (customVad, "MEDIUM_PROG_SECT")
void custAecInput  (chanInfo_t* ch, ADT_Int16* pNear, ADT_Int16* pFar, int FrameSize) { return; }
void custAecOutput (chanInfo_t* ch, ADT_Int16* pNear, ADT_Int16* pFar, int FrameSize) { return; }
void customVad (int *vad) { return; }

