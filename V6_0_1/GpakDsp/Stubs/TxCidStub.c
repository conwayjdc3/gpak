#include "GpakDefs.h"

// TX CID initialization function prototype
short int CIDTX_ADT_init(
    CIDTX_Inst_t   *pTxCidInstance,    // TX CID instance pointer
    CIDTX_Params_t *pTxCidParameters,  // TX CID params pointer  
    ADT_UInt8      *pMessageBytes,     // Message byte buffer pointer
    ADT_UInt8      *pBurstBuffer,      // Burst buffer pointer
    ADT_UInt16     *pNumBurstBytes     // number of burst bytes to be modulated
    )
{     
   return 0;
}

// TX CID transmit function prototype
// returns cid transmit status code 
ADT_UInt16 CIDTX_ADT_transmit(
    CIDTX_Inst_t   *pTxCidInstance,   // TX  CID instance pointer
    ADT_Int16      *pOutputSamples    // pointer to output samples buffer
    )
{ 
    return 0;
}


