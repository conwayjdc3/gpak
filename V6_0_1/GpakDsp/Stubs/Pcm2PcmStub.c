/*
 * Copyright (c) 2004, Adaptive Digital Technologies, Inc.
 *
 * File Name: Pcm2PcmStub.c
 *
 * Description:
 *   This file contains stubs of functions that support PCM To PCM type channels
 *   when that channel type is not configured in the build.
 *
 * Version: 1.0
 *
 * Revision History:
 *   4/21/04 - Initial release.
 *
 */
	
// Application related header files.
#include "GpakDefs.h"
#include "GpakErrs.h"
#include "GpakExts.h"


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ProcCfgPcm2PcmMsg - Process a Configure PCM To PCM Channel message.
//
GPAK_ChannelConfigStat_t ProcCfgPcm2PcmMsg (ADT_UInt16 *pCmd, chanInfo_t *pChan) {

   return (Cc_ChanTypeNotConfigured);
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ProcPcm2PcmStatMsg - Process a PCM To PCM Channel Status message.
//
//
ADT_UInt16 ProcPcm2PcmStatMsg (ADT_UInt16 *pReply, chanInfo_t *pChan) {

   return (0);
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// FramePcmA2PcmB - Host to PCM framing for a PCM To PCM type channel.
//
void FramePcmA2PcmB (chanInfo_t *chan,  ADT_PCM16 *InWork,  ADT_PCM16 *OutWork,
                      ADT_PCM16 *Scratch, int Size) {

   return;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// FramePcmB2PcmA - PCM to Host framing for a PCM To PCM type channel.
//
 void FramePcmB2PcmA  (chanInfo_t *chan, ADT_PCM16 *InWork, ADT_PCM16 *OutWork,
                      ADT_PCM16 *Scratch, int Size) {
  return;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// SetupPcm2PcmChan - Setup a PCM To PCM channel.
//
void SetupPcm2PcmChan (chanInfo_t *pChanInfo) {

  return;
}
