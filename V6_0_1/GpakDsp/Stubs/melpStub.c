/*
 * Copyright (c) 2007, Adaptive Digital Technologies, Inc.
 *
 * File Name: AMRStub.c
 *
 * Description:
 *   This file contains the G.PAK AMR Vocoder stub functions.
 *
 * Version: 1.0
 *
 * Revision History:
 *   02/07 - Initial release.
 *
 */
#include "GpakDefs.h"
void MELP_ADT_initEnc(MELP_AnaChannel_t *MELP_AnaChannel, MELP_Scratch_t *MELP_Scratch)
{ return; }

void MELP_ADT_initDec(MELP_SynChannel_t *MELP_SynChannel, MELP_Scratch_t *MELP_Scratch)
{ return; }

void MELP_ADT_encode (MELP_AnaChannel_t *MELP_AnaChannel, ADT_Int16 speech_in[], ADT_UInt16 chbuf[])
{ return; }

void MELP_ADT_decode (MELP_SynChannel_t *MELP_SynChannel, ADT_UInt16 chbuf[], ADT_Int16 speech_out[])
{ return; }
