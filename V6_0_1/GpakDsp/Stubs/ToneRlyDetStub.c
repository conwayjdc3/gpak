/*
 * Copyright (c) 2004, Adaptive Digital Technologies, Inc.
 *
 * File Name: ToneRlyDetStub.c
 *
 * Description:
 *   This file contains the G.PAK Tone Relay Detect stub functions.
 *
 * Version: 1.0
 *
 * Revision History:
 *   06/15/04 - Initial release.
 *
 */
#include "GpakDefs.h"

int TR_ADT_toneTable;

      
TRResultCode_e TR_ADT_detectInit (TRDetectInstance_t* inst, TRDetectParamsV701_t* params) { return TR_RESULT_OK; }


void TR_ADT_detect (TRDetectInstance_t* inst, ADT_Int16* input, ADT_Int16 inputCnt, ADT_Int16* output, 
		TRDetectResults_t* results) { return; }

void TR_ADT_getLastDetectResults (TRDetectInstance_t* inst, TRDetectResults_t* results) { return; }

