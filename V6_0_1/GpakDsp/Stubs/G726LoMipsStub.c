/*
 * Copyright (c) 2002, Adaptive Digital Technologies, Inc.
 *
 * File Name: G726LoMipsStub.c
 *
 * Description:
 *   This file contains the G.PAK G.726 Low MIPS stub functions.
 *
 * Version: 1.0
 *
 * Revision History:
 *   04/23/02 - Initial release.
 *
 */

void G726_ADT_configure(
	short int **G726QuanTablePointers,
	short int law
	)
{
	return;
}

short int G726ENC_ADT_initialize(
	void *Instance,
	short int Rate
	)
{
	return (0);
}

short int G726DEC_ADT_initialize(
	void *Instance,
	short int Rate
	)
{
	return (0);
}

void G726ENC_ADT_encode(
	void *Channel,
	short int Input[],
	short int Output[],
	short int BlockSize
	)
{
	return;
}

void G726DEC_ADT_decode(
	void *Channel,
	short int Input[],
	short int Output[],
	short int BlockSize
	)
{
	return;
}
