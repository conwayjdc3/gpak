/*
 * Copyright (c) 2002, Adaptive Digital Technologies, Inc.
 *
 * File Name: G723Stub.c
 *
 * Description:
 *   This file contains the G.PAK G.723 Vocoder stub functions.
 *
 * Version: 1.0
 *
 * Revision History:
 *   10/14/02 - Initial release.
 *
 */
#include "GpakDefs.h"
#if (DSP_TYPE == 54)

void codInitializeG723Channel (struct G723EncChannel *TheChanPtr) { return; }

void EncodeG723 (struct G723EncChannel *TheChanPtr,
   short int InputSpeech[],  short int EncodeStream[],  short int HipassEnable,
   short int Rate,  short int VADEnable) { return; }

void decInitializeG723Channel (struct G723DecChannel *TheChanPtr) { return; }

void DecodeG723 (struct G723DecChannel *TheChanPtr, short int DecodeStream[],
   short int OutputSpeech[],  short int PostFiltEnable,  short int FrameEraseFlag){
   return; }

#elif ((DSP_TYPE == 64) || (DSP_TYPE == 55))
void G723_ADT_initDec (G723DEC_ChannelInst_t *Channel_Inst,  G723DEC_Scratch_t *Scratch) {
    return; }

void G723_ADT_decode (G723DEC_ChannelInst_t *Channel_Inst,  CONTROLDEF *ConTrol,
    Word32 Stream[], Word16 Output[], Word16 FrameEraseFlag) {
    return; }

void G723_ADT_initEnc (G723ENC_ChannelInst_t *Channel_Inst,  G723ENC_Scratch_t *Scratch) {
    return; }

void G723_ADT_encode (G723ENC_ChannelInst_t *Channel_Inst, CONTROLDEF *ConTrol,
    Word16 Input[], Word32 Stream[]) {
    return; }

ADT_Int16 G723_ADT_getSize4Decoder(ADT_PCM16 *Stream) { return 0; }

#endif
