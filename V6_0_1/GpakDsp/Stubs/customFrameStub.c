// customFrameStub.c

#include "adt_typedef.h"

#pragma CODE_SECTION (customTimer,  "FAST_PROG_SECT")
void customTimer () { return; }

#pragma CODE_SECTION (customIdleFunc,     "SLOW_PROG_SECT")
void customIdleFunc () {   return; }

#pragma CODE_SECTION (SetHostPortInterrupt, "MEDIUM_PROG_SECT")
void SetHostPortInterrupt (int frameSamples) {    return;  }

#pragma CODE_SECTION (ToggleFrameTaskLED, "MEDIUM_PROG_SECT")
void ToggleFrameTaskLED () {  return; }

