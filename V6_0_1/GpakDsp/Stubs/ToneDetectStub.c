/*
 * Copyright (c) 2002, Adaptive Digital Technologies, Inc.
 *
 * File Name: DtmfStub.c
 *
 * Description:
 *   This file contains the G.PAK DTMF stub functions.
 *
 * Version: 1.0
 *
 * Revision History:
 *   04/23/02 - Initial release.
 *
 */
#include "GpakDefs.h"

void TD_ADT_toneDetect(
				TDInstance_t *TDChannel, 
				short int InputSamples[],
				short int FrameSize) {
	return;
}

