// customMsgStub.c

#include "adt_typedef.h"


#pragma CODE_SECTION (ProcessUserTest, "SLOW_PROG_SECT")
int ProcessUserTest (ADT_UInt16 testID, ADT_UInt16 param, ADT_UInt16 *rply) {   return 1;   }

#pragma CODE_SECTION (customMsg,       "SLOW_PROG_SECT")
int customMsg (ADT_UInt16 *Cmd, ADT_UInt16 *Reply) {    return 0;   }

