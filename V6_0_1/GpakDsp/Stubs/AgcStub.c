/*
 * Copyright (c) 2002, Adaptive Digital Technologies, Inc.
 *
 * File Name: AgcStub.c
 *
 * Description:
 *   This file contains the G.PAK AGC stub functions.
 *
 * Version: 1.0
 *
 * Revision History:
 *   04/23/02 - Initial release.
 *
 */
#include "GpakDefs.h"

#if (DSP_TYPE == 54)
int ADT_Run_AGC (AGC_ChanInst_t   *AgcChan_p,         // IN/OUT
                  ADT_Int16       *InputSamples_p,    // INPUT
                  ADT_Int16        BlockShift,        // INPUT
                  ADT_Int16       *OutputSamples_p,   // OUTPUT
                  ADT_Int32       *Gains               // OUTPUT
            ) {
  return 1;
}

void ADT_Init_AGC (AGC_ChanInst_t *AgcChan_p,
    ADT_Int16 targetPowerIn,      // INPUT range: 0 ... -30
    ADT_Int16 maxLossLimitIn,     // INPUT range: 0 ... -23
    ADT_Int16 maxGainLimitIn,     // INPUT range: 0 ... +23
    ADT_Int16  NAGCSamples,       // INPUT number of samples
    ADT_Int16  lowSigThreshDBM)   // INPUT threshold in dBm of 
                                  //       low energy signal. 
                                  //       Range: -20 ... -65

{
   return;
}
#elif ((DSP_TYPE == 64) || (DSP_TYPE == 55))
ADT_Int16 AGC_ADT_run(
   AGCInstance_t    *pChannel,
   ADT_Int16        *pInputSamples,
   ADT_Int16        FrameSize,
   ADT_Int16        ForceLowSignalState,	// INPUT
   ADT_Int16        BlockShift,
   ADT_Int16        *pOutputSamples,
   ADT_Int32        *Gains)
{
    return 0;

}

void AGC_ADT_agcinit (AGCInstance_t  *AgcChan_p,
                      AGC_ADT_Param_t *pParams)
{
   return;
}
#endif

