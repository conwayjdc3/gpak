/*
 * Copyright (c) 2004, Adaptive Digital Technologies, Inc.
 *
 * File Name: ConfPktStub.c
 *
 * Description:
 *   This file contains stubs of functions that support Conference Packet type
 *   channels when that channel type is not configured in the build.
 *
 * Version: 1.0
 *
 * Revision History:
 *   4/21/04 - Initial release.
 *
 */
	
// Application related header files.
#include "GpakDefs.h"
#include "GpakErrs.h"
#include "GpakExts.h"

#pragma CODE_SECTION (MultiRateCnfrFrame, "SLOW_PROG_SECT")
void MultiRateCnfrFrame (chanInfo_t *pChan,  void *pktBuff, ADT_PCM16 *pcmBuff, 
                    ADT_PCM16 *pECFarWork, int FrameSize, int pcmBuffBytes) {
   return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ProcCfgConfPktMsg - Process a Configure Conference Packet Channel message.
//
#pragma CODE_SECTION (ProcCfgConfPktMsg, "SLOW_PROG_SECT")
GPAK_ChannelConfigStat_t ProcCfgConfPktMsg (ADT_UInt16 *pCmd, chanInfo_t *pChan) {

   return  Cc_ChanTypeNotConfigured;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ProcConfPktStatMsg - Process a Conference Packet Channel Status message.
//
#pragma CODE_SECTION (ProcConfPktStatMsg, "SLOW_PROG_SECT")
ADT_UInt16 ProcConfPktStatMsg (ADT_UInt16 *pReply, chanInfo_t *pChan) {
   return (0);
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// SetupConfPktChan - Setup a Conference Packet channel.
//
#pragma CODE_SECTION (SetupConfPktChan, "SLOW_PROG_SECT")
void SetupConfPktChan (chanInfo_t *pChanInfo) {
   return;
}
