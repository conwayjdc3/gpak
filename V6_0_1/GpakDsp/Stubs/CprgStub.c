/*
 * Copyright (c) 2004, Adaptive Digital Technologies, Inc.
 *
 * File Name: CprgStub.c
 *
 * Description:
 *   This file contains the G.PAK Call Progress stub functions.
 *
 * Version: 1.0
 *
 * Revision History:
 *   06/15/04 - Initial release.
 *
 */
#include "GpakDefs.h"

#if ((DSP_TYPE == 54) ||(DSP_TYPE == 55))
void CPRG_ADT_initialize(TDInstance_t *TDChannel) {
	return;
}
#elif (DSP_TYPE == 64)
void CPRG_ADT_initialize(TDInstance_t *DTMFChannel, TDScratch_t *ScratchPtr) {
   return;
}
void CPRG_ADT_toneSuppress(
 	    void *pToneRelayDt,		//Tone relay instance
		TDInstance_t *DTMFInstance,
		ADT_Int16 InputSamples[],
		ADT_Int16 FrameSize,
		ADT_Int16 Suppression,
		ADT_Int16 OutputSamples[]) {
    return;
}
#endif
