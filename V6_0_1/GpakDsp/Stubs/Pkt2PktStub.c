/*
 * Copyright (c) 2004, Adaptive Digital Technologies, Inc.
 *
 * File Name: Pkt2PktStub.c
 *
 * Description:
 *   This file contains stubs of functions that support Packet To Packet type
 *   channels when that channel type is not configured in the build.
 *
 * Version: 1.0
 *
 * Revision History:
 *   4/21/04 - Initial release.
 *
 */
	
// Application related header files.
#include "GpakDefs.h"
#include "GpakErrs.h"
#include "GpakExts.h"


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ProcCfgPkt2PktMsg - Process a Configure Packet To Packet Channel message.
//
GPAK_ChannelConfigStat_t ProcCfgPkt2PktMsg (ADT_UInt16 *pCmd, chanInfo_t *pChan) {

   return Cc_ChanTypeNotConfigured;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ActivatePkt2Pkt - Activate a Packet To Packet channel.
//
void ActivatePkt2Pkt (chanInfo_t *pChan) {

 return;
}




// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ProcPkt2PktStatMsg - Process a Packet To Packet Channel Status message.

ADT_UInt16 ProcPkt2PktStatMsg (ADT_UInt16 *pReply, chanInfo_t *pChan) {

   return (0);
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// PktPktDecode - Host to PCM framing for a Packet To Packet type channel.
//
void PktPktDecode  (chanInfo_t *chan, void *pktBuff, ADT_PCM16 *pcmBuff,  
                       ADT_PCM16 *pECFarWk, int FrameSize, int pcmBuffBytes) {

    return;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// PktPktEncode - PCM to Host framing for a Packet To Packet type channel.
//
void PktPktEncode (chanInfo_t *chan, ADT_PCM16 *pcmBuff,    void *pktBuff,
                                        ADT_PCM16 *pECFarWork, int FrameSize) {

   return;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// SetupPkt2PktChan - Setup a Packet To Packet channel.
//
void SetupPkt2PktChan (chanInfo_t *pChanInfo) {
   return;
}
