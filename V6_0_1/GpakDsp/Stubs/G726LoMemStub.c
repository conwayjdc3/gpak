/*
 * Copyright (c) 2002, Adaptive Digital Technologies, Inc.
 *
 * File Name: G726LoMemStub.c
 *
 * Description:
 *   This file contains the G.PAK G.726 Low Memory stub functions.
 *
 * Version: 1.0
 *
 * Revision History:
 *   04/23/02 - Initial release.
 *
 */
#include "GpakDefs.h"

short int G726ENC_ADT_initializeLomem(
	G726EncInstance_t *Instance,
	short int Rate,
	short int Law
	)
{
	return (0);
}

short int G726DEC_ADT_initializeLomem(
	G726DecInstance_t *Instance,
	short int Rate,
	short int Law
	)
{
	return (0);
}

void G726ENC_ADT_encodeLomem(
	G726EncInstance_t *Channel,
	short int Input[],
	short int Output[],
	short int BlockSize
	)
{
	return;
}

void G726DEC_ADT_decodeLomem(
	G726DecInstance_t *Channel,
	short int Input[],
	short int Output[],
	short int BlockSize
	)
{
	return;
}
