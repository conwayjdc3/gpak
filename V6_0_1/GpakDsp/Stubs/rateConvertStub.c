/*
 * Copyright (c) 2011, Adaptive Digital Technologies, Inc.
 *
 * File Name: RateConvertStub.c
 *
 * Description:
 *   This file contains the G.PAK RateConver functions.
 *
 * Version: 1.0
 *
 * Revision History:
 *   08/31/2011 - Initial release.
 *
 */
#include "adt_typedef.h"

ADT_Int16 SRC_ADT_dec_coeff;
ADT_Int16 SRC_ADT_int_coeff_by_2;

void SRC_ADT_InitdecimateBy2    (void *ptr, ADT_Int16 Len)  { return; }
void SRC_ADT_InitInterpolateBy2 (void *ptr, ADT_Int16 Len)  { return; }

void SRC_ADT_decimateBy2 (void *State, void *input, ADT_Int16 FrameL, 
                          void *output, void *dec_intr_coeff, ADT_Int16 coefLen) { return; }
void SRC_ADT_interpolateBy2 (void *State, void *input, ADT_Int16 FrameL, 
                          void *output, void *int_coeff, ADT_Int16 CoefLen) { return; }
