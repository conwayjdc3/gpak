/*
 * Copyright (c) 2002, Adaptive Digital Technologies, Inc.
 *
 * File Name: DtmfStub.c
 *
 * Description:
 *   This file contains the G.PAK DTMF stub functions.
 *
 * Version: 1.0
 *
 * Revision History:
 *   04/23/02 - Initial release.
 *
 */
#include "GpakDefs.h"

#if ((DSP_TYPE == 54) ||(DSP_TYPE == 55))
void DTMF_ADT_initialize(TDInstance_t *TDChannel){
	return;
}
#elif (DSP_TYPE == 64)
void DTMF_ADT_initialize(TDInstance_t *DTMFChannel, TDScratch_t *ScratchPtr) {
   return;
}
#endif

void DTMF_ADT_config (DTMF_Config_Param_t *DTMFConfigParam, short int Nominal) { return; }

void DTMF_ADT_toneSuppress(
      void *pToneRelayDt,		//Tone relay instance
		TDInstance_t *DTMFInstance,
		ADT_Int16 InputSamples[],
		ADT_Int16 FrameSize,
		ADT_Int16 Suppression,
		ADT_Int16 OutputSamples[]) { return; }
