#ifndef _64x_h
#define _64x_h

#define McBSP0_BASE 0x018c0000
#define McBSP1_BASE 0x01900000
#define McBSP2_BASE 0x01A40000

#define MCASP0_BASE 0x01B4C000
#define MCASP1_BASE 0x01B5C000

// McASP and McBSP DMA ping and pong (pong = ping + 1)
#define Tx0_Ping 64
#define Rx0_Ping 66
#define Tx1_Ping 68
#define Rx1_Ping 70
#define Tx2_Ping 72
#define Rx2_Ping 74

// 64x McASP DMA resources
#define McASPTx0_TCC   0
#define McASPRx0_TCC   1
#define McASPTx1_TCC   2
#define McASPRx1_TCC   3

#define McASPTx0_Chn 34
#define McASPRx0_Chn 37
#define McASPTx1_Chn 40
#define McASPRx1_Chn 43

#define MCASP0_RxAddr 0x3C000000
#define MCASP0_TxAddr 0x3C000004
#define MCASP1_RxAddr 0x3C100000
#define MCASP1_TxAddr 0x3C100004

// Default 64x McASP Default Dma Resources
#define MCASP0_CFG_DEFAULTS { MCASP0_TxAddr, MCASP0_RxAddr, McASPTx0_Chn, McASPRx0_Chn, Tx0_Ping, Rx0_Ping, 0, 0, McASPTx0_TCC, McASPRx0_TCC }
#define MCASP1_CFG_DEFAULTS { MCASP1_TxAddr, MCASP1_RxAddr, McASPTx1_Chn, McASPRx1_Chn, Tx1_Ping, Rx1_Ping, 0, 0, McASPTx1_TCC, McASPRx1_TCC }

// 64x McBSP DMA resources
#define MCBSP0_RxAddr 0x30000000
#define MCBSP0_TxAddr 0x30000004
#define MCBSP1_RxAddr 0x34000000
#define MCBSP1_TxAddr 0x34000004
#define MCBSP2_RxAddr 0x38000000
#define MCBSP2_TxAddr 0x38000004

// DMA channel assignments (Fixed for C64)
#define McBSPTx0_Chn 12
#define McBSPRx0_Chn 13
#define McBSPTx1_Chn 14
#define McBSPRx1_Chn 15
#define McBSPTx2_Chn 17
#define McBSPRx2_Chn 18


// Use TCC 4 and 5 (rather than event code) for McBSP2 to avoid 
// conflict with 16 bit flags
#define McBSPTx0_TCC  12
#define McBSPRx0_TCC  13
#define McBSPTx1_TCC  14
#define McBSPRx1_TCC  15
#define McBSPTx2_TCC   4   
#define McBSPRx2_TCC   5


// Default 64x McBSP Default Dma Resources
#define MCBSP0_CFG_DEFAULTS { MCBSP0_TxAddr, MCBSP0_RxAddr, McBSPTx0_Chn, McBSPRx0_Chn, Tx0_Ping, Rx0_Ping, 0, 0, McBSPTx0_TCC, McBSPRx0_TCC }
#define MCBSP1_CFG_DEFAULTS { MCBSP1_TxAddr, MCBSP1_RxAddr, McBSPTx1_Chn, McBSPRx1_Chn, Tx1_Ping, Rx1_Ping, 0, 0, McBSPTx1_TCC, McBSPRx1_TCC }
#define MCBSP2_CFG_DEFAULTS { MCBSP2_TxAddr, MCBSP2_RxAddr, McBSPTx2_Chn, McBSPRx2_Chn, Tx2_Ping, Rx2_Ping, 0, 0, McBSPTx2_TCC, McBSPRx2_TCC }


#endif
