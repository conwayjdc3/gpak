/*___________________________________________________________________________
 |
 |   File: faxceddet.h
 |
 |   This file is the Adaptive Digital Technologies, Inc. 
 |   private header file for the 2100Hz tone detectors.
 |   
 |   This software is the property of Adaptive Digital Technologies, Inc.
 |   Unauthorized use is prohibited.
 |
 |   Copyright (c) 2000-2001, Adaptive Digital Technologies, Inc.
 |
 |   Version 2
 |   July 17, 2001
 |
 |   www.adt-inc.com
 |   610-825-0182
 |___________________________________________________________________________
*/
#ifndef _FAXCEDDET_H
#define _FAXCEDDET_H

/******************************************************************/
/* Following are declarements for DTMF method to detect 2100Hz */



typedef struct 
{
/* Outputs */
	short int G164Flag;
	short int G165Flag;
/* States */
//	short int DetectorState;
	long int Signal;
	long int SignalPlusNoise;
	short int G164Count;
	short int G165Count;
	short int G164State;
	short int G165State;
//	short int GorState0;
//	short int GorState1;
	short int G165SampleCount;
	short int PrevPhase0;
	short int PrevPhase1;
	short int AvgPhase;
	short int PhaseVariance;
	short int PrevPhaseVariance;
	short int PhaseError;
/******************************************************************/
/* Following are new elements for DTMF method to detect 2100Hz */
	short int DftN;    // keep track of samples no. already done Dft
    short int *DFTCosPtr;
    short int *DFTSinePtr;
	short int CosSum; // CosSum and SinSum: I and Q 
	short int SinSum;
/* Above are new elements for DTMF method to detect 2100Hz */
/******************************************************************/
	short int G165ReleaseFlag; // added to Re_Enable EchoCanceller, huafeng add
	short int G165ReleaseCount; // release signal should hold at least 100ms
	short int G165ReleaseState; // release state machine
	short int G165FlagLatched; // move this flag from g168channel, huafeng add/delete
}	FaxCEDChannel_t;   


void FAXCED_ADT_detectInit(FaxCEDChannel_t *Channel);

void FAXCED_ADT_detect(FaxCEDChannel_t *Channel,FaxCEDChannel_t *Channel2,
							short int *InputSamples, short int *InputSamples2, 
							short int NInputSamples);
#endif // ifndef _FAXCEDDET_H  
