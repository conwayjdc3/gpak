/*******************************************************************
*
* 	File: 	g711a1a2_user.h
*
* 	Description: 	public header file for G.711A1A2 Coder.
*
*	Copyright(c) 2003 - 2015 by Adaptive Digital Technologies, Inc.
*	All Rights Reserved
*
* Revision History
* 10/29/2015
*    Update for 64-bit
* 12/20/2018
*    Update to support more than 10ms frame
********************************************************************/

#ifndef G711_ENC_ADT_H
#define G711_ENC_ADT_H

#ifndef ADT_LAW_TYPE
#define ADT_LAW_TYPE
#define MU_LAW      1
#define A_LAW       0
#endif
#ifndef _ADT_TYPEDEF_USER
#include <common/include/adt_typedef_user.h>
#endif

typedef enum
{
	G711A1A2_RESULT_OK,
	G711A1A2_RESULT_API_VERSION_MISMATCH,
	G711A1A2_RESULT_INSTANCE_TOO_SMALL,
	G711A1A2_RESULT_INSTANCE_SCRATCH_TOO_SMALL,
	G711A1A2_RESULT_FRAME_SIZE_ERROR,
	G711A1A2_RESULT_ALLOCATE_ERROR,
	G711A1A2_RESULT_NULL_POINTER,
	G711A1A2_RESULT_CREATE_AND_DELETE_NOT_SUPPORTED,
}		G711A1A2Result_e;	

#define G711A1A2_API_VERSION 4
#define MAX_G711_FRAME_SIZE 80
#define MAX_G711_CNGPAYLOAD_SIZE 11

#ifndef ADT_G711A1_ONLY
#define G711_ENC_CHANNEL_SIZE_IN_BYTES (1672+16)
#define G711_ENC_SCRATCH_SIZE_IN_BYTES (632)
#define G711_DEC_CHANNEL_SIZE_IN_BYTES (1804)
#define G711_DEC_SCRATCH_SIZE_IN_BYTES (624)
#else
#define G711_ENC_CHANNEL_SIZE_IN_BYTES (16)
#define G711_ENC_SCRATCH_SIZE_IN_BYTES (16)
#define G711_DEC_CHANNEL_SIZE_IN_BYTES (1712)
#define G711_DEC_SCRATCH_SIZE_IN_BYTES (232)
#endif

typedef struct 
{
	ADT_Int64 Space[(G711_ENC_CHANNEL_SIZE_IN_BYTES+7)/8];
} G711ENC_ADT_Instance_t; 

typedef struct 
{
	ADT_Int64 Space[(G711_ENC_SCRATCH_SIZE_IN_BYTES+7)/8]; 
} G711ENC_ADT_Scratch_t; 

typedef struct 
{
	ADT_Int64 dec_Channel[(G711_DEC_CHANNEL_SIZE_IN_BYTES+7)/8]; 
} G711DEC_ADT_Instance_t; 

typedef struct 
{
	ADT_Int64 dec_Scratch[(G711_DEC_SCRATCH_SIZE_IN_BYTES+7)/8]; 
} G711DEC_ADT_Scratch_t; 

typedef struct
{
	ADT_UInt16 APIVersion;
	G711ENC_ADT_Scratch_t *pScratch;
	ADT_UInt32 InstanceSizeInBytes;	//If too small, returns error code and correct size is placed here
	ADT_UInt32 ScratchSizeInBytes;	//If too small, returns error code and correct size is placed here
	ADT_UInt8 EnableVADCNG;
}		G711EncParams_t;

typedef struct
{
	ADT_UInt16 APIVersion;
	G711DEC_ADT_Scratch_t *pScratch;
	ADT_UInt32 InstanceSizeInBytes;	//If too small, returns error code and correct size is placed here
	ADT_UInt32 ScratchSizeInBytes;	//If too small, returns error code and correct size is placed here
	ADT_UInt8 EnablePLC;
	ADT_UInt8 EnableVADCNG;
}		G711DecParams_t;

ADT_API G711A1A2Result_e G711_ADT_initEncode(G711ENC_ADT_Instance_t *pEncChannel, 
					G711EncParams_t *pParams
					);

ADT_API G711A1A2Result_e G711_ADT_encode(G711ENC_ADT_Instance_t *pEncChannel,     
 					ADT_Int16      *pInputSamples,         
					ADT_Int16	   nInputSamples,       
 					ADT_UInt8	   lawType,			    
					ADT_UInt8      *pEncodedOutput,     
 					ADT_UInt16		*pNOutputBytes      
					);

#define G711_ADT_encodeM(a,b,c,d,e,f) G711_ADT_encode(a,b,c,d,e,f)

ADT_API G711A1A2Result_e G711_ADT_initDecode(G711DEC_ADT_Instance_t *pDecChannel,
					G711DecParams_t *pParams
					);

ADT_API G711A1A2Result_e G711_ADT_decode(G711DEC_ADT_Instance_t *pDecChannel, 
					ADT_UInt8		*pEncodedInput,		
					ADT_Int16		nInputBytes,		
					ADT_UInt8		lawType,			
					ADT_Int16		nOutputSamples,		
					ADT_Int16		*pOutput			
					);
#define G711_ADT_decodeM(a,b,c,d,e,f) G711_ADT_decode(a,b,c,d,e,f)
ADT_API G711ENC_ADT_Instance_t *G711_ADT_createEncode(G711EncParams_t *pParams, G711A1A2Result_e *pResult);
ADT_API G711DEC_ADT_Instance_t *G711_ADT_createDecode(G711DecParams_t *pParams, G711A1A2Result_e *pResult);
ADT_API G711A1A2Result_e G711_ADT_deleteEncode(G711ENC_ADT_Instance_t *pInstance);
ADT_API G711A1A2Result_e G711_ADT_deleteDecode(G711DEC_ADT_Instance_t *pInstance);

void G711_ADT_muLawExpand(ADT_UInt8 *Input, ADT_Int16 *Output, ADT_Int16 Length);
void G711_ADT_muLawCompress(ADT_Int16 *Input, ADT_UInt8 *Output, ADT_Int16 Length);
void G711_ADT_aLawExpand(ADT_UInt8 *Input, ADT_Int16 *Output, ADT_Int16 Length);
void G711_ADT_aLawCompress(ADT_Int16 *Input, ADT_UInt8 *Output, ADT_Int16 Length);

#endif
