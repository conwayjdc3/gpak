/*****************************************************************************/
/*---------------------------------------------------------------------------*/
/*                     TEXAS INSTRUMENTS INC                                 */
/*                     Multimedia Codecs Group                               */
/*---------------------------------------------------------------------------*/
/*                                                                           */
/* (C) Copyright 2005  Texas Instruments Inc.  All rights reserved.          */
/*                                                                           */
/* Exclusive property of the Multimedia Codecs Group, Texas Instruments Inc. */
/* Any handling, use, disclosure, reproduction, duplication, transmission    */
/* or storage of any part of this work by any means is subject to restriction*/
/* and prior written permission set forth.                                   */
/*                                                                           */
/* This copyright notice, restricted rights legend, or other proprietary     */
/* markings must be reproduced without modification in any authorized copies */
/* of any part of this work. Removal or modification of any part of this     */
/* notice is prohibited.                                                     */
/*---------------------------------------------------------------------------*/
/*
 *  ======== ig729ab.h ========
 *  xDAIS compatible interface header template for the G729AB module
 */
#ifndef IG729AB_
#define IG729AB_

#include "ti/xdais/dm/xdm.h"
#include "ti/xdais/dm/ispeech1.h"
/* TABLE ADDRESS AND LENGTH DEFINITIONS For table relocation*/

#ifdef _TMS320C6X

/* Table Relocation details */
extern const XDAS_Int16 G729AB_TII_table1[];
extern const XDAS_Int16 G729AB_TII_table2[];
extern const XDAS_Int16 G729AB_TII_table3[];
extern const XDAS_Int16 G729AB_TII_table4[];
extern const XDAS_Int16 G729AB_TII_table5[];
extern const XDAS_Int16 G729AB_TII_table6[];
extern const Void *G729AB_TII_tabptrs[];

/* Encoder packet sizes */

#define G729SPEECHPACKETSIZE        10  /* In bytes */
#define G729SIDPACKETSIZE           2   /* In bytes */
#define G729FRAMELEN                80  /* Number of Samples corresponding */

#define G729AB_NUM_ENC_TABLES            0
#define G729AB_NUM_DEC_TABLES            0
#define G729AB_NUM_COMMON_TABLES         6
#define G729AB_NUM_TABLES             (G729AB_NUM_ENC_TABLES +  \
                                        G729AB_NUM_DEC_TABLES +   \
                                        G729AB_NUM_COMMON_TABLES)

#define G729AB_TABLE1_START           G729AB_TII_table1     /*Table 1*/
#define G729AB_TABLE2_START           G729AB_TII_table2     /*Table 2*/
#define G729AB_TABLE3_START           G729AB_TII_table3     /*Table 3*/
#define G729AB_TABLE4_START           G729AB_TII_table4     /*Table 4*/
#define G729AB_TABLE5_START           G729AB_TII_table5     /*Table 5*/
#define G729AB_TABLE6_START           G729AB_TII_table6     /*Table 6*/
#define G729AB_TABLEPTR_DEFAULT       G729AB_TII_tabptrs    /* Array of ptrs*/

/* Table Lengths in bytes */
#define G729AB_TABLE1_L               (575 * 2)
#define G729AB_TABLE2_L               (539 * 2) 
#define G729AB_TABLE3_L               (513 * 2) 
#define G729AB_TABLE4_L               (600 * 2) 
#define G729AB_TABLE5_L               (500 * 2) 
#define G729AB_TABLE6_L               (500 * 2) 

#endif /* _TMS320C6X */

#endif /* IG729AB_ */
/* Nothing past this point */
