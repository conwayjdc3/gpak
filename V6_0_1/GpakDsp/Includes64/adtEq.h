#ifndef ADT_EQ_H_
#define ADT_EQ_H_

#include "adt_typedef.h"

typedef struct 
{
	ADT_Int16 *pCoef;
	ADT_Int16 *pState;
	ADT_Int16 coefLength;
}	EqChannel_t;

typedef struct 
{
	ADT_Int16 *pCoef;
	ADT_Int16 *pState;
	ADT_Int16 coefLength;
	ADT_Int16 frameSize;
}	EqParams_t;

void eqInit(EqChannel_t *pChannel, EqParams_t *pParams);
void eqApply(EqChannel_t *pChannel, ADT_Int16 *pIn, ADT_Int16 *pOut, ADT_Int16 NSamples);
#endif

