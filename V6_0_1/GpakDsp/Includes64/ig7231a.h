/******************************************************************************
*-----------------------------------------------------------------------------*
*                     TEXAS INSTRUMENTS INC                                   *
*                     Multimedia Codecs Group                                 *
*                                                                             *
*-----------------------------------------------------------------------------*
*                                                                             *
* (C) Copyright 2006  Texas Instruments Inc.  All rights reserved.            *
*                                                                             *
* Exclusive property of the Multimedia Codecs Group, Texas Instruments Inc.   *
* Any handling, use, disclosure, reproduction, duplication, transmission      *
* or storage of any part of this work by any means is subject to restrictions *
* and prior written permission set forth.                                     *
*                                                                             *
* This copyright notice, restricted rights legend, or other proprietary       *
* markings must be reproduced without modification in any authorized copies   *
* of any part of this work. Removal or modification of any part of this       *
* notice is prohibited.                                                       *
*                                                                             *
******************************************************************************/

/*
 *  ======== ig7231a.h ========
 *  xDAIS compatible interface header template for the G7231A module
 */
#ifndef IG7231A_
#define IG7231A_

#include "ti/xdais/dm/ispeech1.h"

#define G7231A_NUM_ENC_TABLES         0
#define G7231A_NUM_DEC_TABLES         0
#define G7231A_NUM_COMMON_TABLES      8
#define G7231A_NUM_TABLES             (G7231A_NUM_ENC_TABLES +  \
                                      G7231A_NUM_DEC_TABLES +   \
                                      G7231A_NUM_COMMON_TABLES)

#define G7231A_TABLE1_START           G7231A_TII_CosineTable
#define G7231A_TABLE2_START           G7231A_TII_ShiftTable
#define G7231A_TABLE3_START           G7231A_TII_Band0Tb8
#define G7231A_TABLE4_START           G7231A_TII_Band1Tb8
#define G7231A_TABLE5_START           G7231A_TII_Band2Tb8
#define G7231A_TABLE6_START           G7231A_TII_AcbkGainTable085
#define G7231A_TABLE7_START           G7231A_TII_AcbkGainTable170
#define G7231A_TABLE8_START           G7231A_TII_LspDcTable

/* Table Lengths in bytes */ // Total Table size - 18932 */
#define G7231A_TABLE1_L               1024
#define G7231A_TABLE2_L               1328
#define G7231A_TABLE3_L               1536 
#define G7231A_TABLE4_L               1536 
#define G7231A_TABLE5_L               2048
#define G7231A_TABLE6_L               3400
#define G7231A_TABLE7_L               6800
#define G7231A_TABLE8_L               1312

extern XDAS_Int16 G7231A_TII_CosineTable[];
extern XDAS_Int16 G7231A_TII_ShiftTable[];
extern XDAS_Int16 G7231A_TII_Band0Tb8[];
extern XDAS_Int16 G7231A_TII_Band1Tb8[];
extern XDAS_Int16 G7231A_TII_Band2Tb8[];
extern XDAS_Int16 G7231A_TII_AcbkGainTable085[];
extern XDAS_Int16 G7231A_TII_AcbkGainTable170[];
extern XDAS_Int16 G7231A_TII_LspDcTable[];


#endif /* IG7231A_ */

