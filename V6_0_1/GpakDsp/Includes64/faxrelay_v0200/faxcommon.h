/***************************************************************************
* $RCSfile: common.h,v $                
* $Revision: 9.2 $              
* Revision $Author: peter $                              
* Revision $Date: 2010-02-15 15:51:46 $            
* Author of Origin: Peter B. Miller             
* Date of Origin: 11-14-96  
*                   
* Company: MESi               
* Website: www.mesi.net             
* Description: common symbol and macro definitions for all modules.								
*                   
* Copyright (C) MESi 1996-2010, all rights reserved.         
***************************************************************************/

#if !defined(COMMON_INCLUSION_)
#define COMMON_INCLUSION_

	/**** basics ****/

#if !defined DISABLED
#define DISABLED					0   
#endif /* DISABLED */               
#if !defined ENABLED                
#define ENABLED						1   
#endif /* ENABLED */                
#if !defined NOT_DETECTED           
#define NOT_DETECTED				0   
#endif /* NOT_DETECTED */           
#if !defined DETECTED               
#define DETECTED					1   
#endif /* DETECTED */               
#if !defined FAIL                   
#define FAILURE 					0
#endif /* FAILURE */                
#if !defined SUCCESS                
#define SUCCESS 					1   
#endif /* SUCCESS */                
#if !defined(NULL)
#define NULL 						((void*)0)
#endif /* NULL */

#define CIRC 						short				

	/**** types ****/               

struct COMPLEX {short real,imag;};

	/**** macros ****/

//++++#if !defined(MESI_MODIFICATIONS) /* ABS BUGFIX AGAIN!!! PBM 7-07-2013 */
////++++#if !defined(MESI_MODIFICATIONS) /* ABS BUGFIX PBM 11-22-2012 */
////#define ABS(a) ((a)>=0?(a):-(a))
////++++#else /* MESI_MODIFICATIONS         ABS BUGFIX PBM 11-22-2012 */
////#define ABS(a) ((a)>=0?((a)==-32768?32767:-(a)):-(a))
////++++#endif /* MESI_MODIFICATIONS        ABS BUGFIX PBM 11-22-2012 */
//++++#else /* MESI_MODIFICATIONS         ABS BUGFIX AGAIN!!! PBM 7-07-2013 */
#define ABS(a) ((a)>=0?(a): ((a)==-32768?32767:-(a)))
//++++#endif /* MESI_MODIFICATIONS        ABS BUGFIX AGAIN!!! PBM 7-07-2013 */

#define LABS(a) ABS(a)
#define MPY(a,b) (short)(((((long)(a)*(long)(b)))>>15))
#define MPYR(a,b) (short)(((((long)(a)*(long)(b))+0x4000l)>>15))

/****************************************************************************
 * Macro: CIRC_WRAP(ptr, base, len)											
 * Macro to circular wrap the pointer (ptr) associated with the buffer 		
 * having starting address of (base) and of length (len). This macro is		
 * useful for generically implementing circular addressing.					
 ****************************************************************************/
#define CIRC_WRAP(ptr, base, len) \
	{ \
	if(ptr >= base+len) \
		{ \
		ptr-=len; \
		if(ptr >= base+len) \
			{ \
			ptr+=len; \
			ptr+=len; \
			} \
		} \
	else if (ptr < base) \
		{ \
		ptr+=len; \
		if(ptr < base) \
			{ \
			ptr-=len; \
			ptr-=len; \
			} \
		} \
	}
	
/****************************************************************************
 * Macro: CIRC_MODIFY(ptr, mod, base, len)									
 * Macro to circular modify by (mod) the pointer (ptr) associated with the 	
 * buffer having starting address of (base) and of length (len). This macro	
 * is useful for generically implementing circular addressing.				
 ****************************************************************************/
#define CIRC_MODIFY(ptr, mod, base, len) \
	{ \
	ptr+=(mod); \
	CIRC_WRAP(ptr,base,len) \
	}

/****************************************************************************
 * Macro: CIRC_INCREMENT(ptr, mod, base, len)								
 * Macro to circular modify by (mod) the pointer (ptr) associated with the 	
 * buffer having starting address of (base) and of length (len). This macro	
 * is useful for generically implementing circular addressing. 				
 * NOTE: The increment (mod) must be a positive value. 					   	
 ****************************************************************************/
#define CIRC_INCREMENT(ptr, mod, base, len) \
	{ \
	ptr+=(mod); \
	if (ptr >= (base+len)) ptr-=len; \
	else if (ptr < base) ptr-=len; \
	}

/****************************************************************************
 * Macro: CIRC_DECREMENT(ptr, mod, base, len)								
 * Macro to circular modify by (mod) the pointer (ptr) associated with the 	
 * buffer having starting address of (base) and of length (len). This macro	
 * is useful for generically implementing circular addressing. 				
 * NOTE: The increment (mod) must be a negative value. 					   	
 ****************************************************************************/
#define CIRC_DECREMENT(ptr, mod, base, len) \
	{ \
	ptr+=(mod); \
	if (ptr < base) ptr+=len; \
	else if (ptr >= (base+len)) ptr+=len; \
	}

/****************************************************************************
 * Macros to provide saturation functionality.								
 ****************************************************************************/
#define positive_saturate(a,b) if ((b) > 32767l) a=32767
#define negative_saturate(a,b) if ((b) < -32768l) a=(-32767-1)
#define saturate(a,b) if ((b) > 32767l) a=32767; \
		  			  if ((b) < -32768l) a=(-32767-1)

/****************************************************************************
 * Macro: SUBC(NUM,DENOM)													
 * Emulates DSP Subtract Conditionally. NUM must be a long, and DENOM 		
 * must be less than NUM. NUM is not preserved. For 16 bit integer division	
 * you call this macro 16 times.											
 * Results:																	
 *		the quotient is in the lower 16 bits of NUM							
 *		the remainder is in the upper 16 bits of NUM						
 ****************************************************************************/
#define SUBC(NUM,DENOM) \
		{if ((NUM-(((long)DENOM)<<15))>=0) \
			NUM=((NUM-(((long)DENOM)<<15))<<1)+1; \
		else \
			NUM=NUM<<1;}

/****************************************************************************
 * Macro: LIBPORT_KEYWORD
 * Modified for extern declarations for functions in header files to support
 * Windows DLL export and import using the _declspec() keyword.
 ****************************************************************************/
#if defined(__arm__)
//#error("Compiler __arm__was defined")
#define EXTERN_LIBPORT_KEYWORD extern 

#elif defined(_WIN32) & (defined(_DLL) | defined(_WINDLL))
#define EXTERN_LIBPORT_KEYWORD __declspec(dllexport)
#elif defined(_WIN32)
#define EXTERN_LIBPORT_KEYWORD __declspec(dllimport)
#else
#define EXTERN_LIBPORT_KEYWORD extern 
#endif

/****************************************************************************/
#endif /* COMMON_INCLUSION_ */

