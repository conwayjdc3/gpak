/*************************************************************************
* $RCSfile: FaxStruct.h,v $
* $Revision: 9.2 $
* $Date: 2010-02-15 16:13:45 $
* $Author: peter $
* Company: MESi
* Description: Fax relay structure definition
*
* Copyright (C) MESi 1996-2010, all rights reserved.
***************************************************************************/
#if !defined(__FAXSTRUCT___)
#define __FAXSTRUCT___

struct FAX_STRUCT
{
    struct MIF_STRUCT *ModemIf;	/* address of Modem Interface structure */
    struct SEQ_STRUCT *Seq;          /* address of Sequencer structure */
    struct START_PTRS *start_ptrs;	/* modem channel memory address table */
    short RelayFunction;			/* relay type specifier */				
	short channel;					/* channel number */
#ifdef SEQ_COMM_LINK
    struct ippcStruct *comm;		/* for the external communications link */
#endif
};

#if 0 
struct FAX_STRUCT
{
    struct START_PTRS *start_ptrs;	/* modem channel memory address table */
    struct MIF_STRUCT *ModemIf;		/* address of Modem Interface structure */
    struct SEQ_STRUCT *Seq;         /* address of Sequencer structure */
    short RelayFunction;			/* relay type specifier */				
	short channel;					/* channel number */
#ifdef SEQ_COMM_LINK                
    struct ippcStruct *comm;		/* for the external communications link */
#endif
};
#endif

#if 0  //old
struct FaxStruct
{
    struct ModemIfStruct *ModemIf;	/* address of Modem Interface structure */
    struct SeqStruct *Seq;          /* address of Sequencer structure */
    struct START_PTRS *start_ptrs;	/* modem channel memory address table */
    short RelayFunction;			/* relay type specifier */				
	short channel;					/* channel number */
#ifdef SEQ_COMM_LINK
    struct ippcStruct *comm;		/* for the external communications link */
#endif
};
#endif






/*****************************************************************************/
#endif /* __FAXSTRUCT___ */
