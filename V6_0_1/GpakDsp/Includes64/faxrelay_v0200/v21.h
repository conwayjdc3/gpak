/***************************************************************************
* $RCSfile: v21.h,v $                
* $Revision: 9.3 $              
* Revision $Author: peter $                              
* Revision $Date: 2010-02-15 15:51:54 $            
* Author of Origin: Peter B. Miller             
* Date of Origin: 09-18-96
*                   
* Company: MESi               
* Website: www.mesi.net             
* Description: Structure definitions and extern references for v21.								
*                   
* Copyright (C) MESi 1996-2010, all rights reserved.         
***************************************************************************/

#if !defined(V21_INCLUSION_)
#define V21_INCLUSION_

	/**** state_ID definitions ****/

#define	TX_V21_CH1_SILENCE_ID		0x2101
#define TX_V21_CH1_MESSAGE_ID 		(0x2100|MESSAGE_ID)

#define	TX_V21_CH2_SILENCE1_ID		0x2181
#define	TX_V21_CH2_ANS_ID			0x2182
#define	TX_V21_CH2_SILENCE2_ID		0x2183
#define TX_V21_CH2_MESSAGE_ID 		(0x2180|MESSAGE_ID)

#define RX_V21_CH1_ID 				0x2100
#define RX_V21_CH1_MARKS_ID 		0x2101
#define RX_V21_CH1_START_BIT_ID 	0x2102
#define RX_V21_CH1_MESSAGE_ID 		(0x2100|MESSAGE_ID)

#define RX_V21_CH2_ID 				0x2180
#define RX_V21_CH2_MARKS_ID 		0x2181
#define RX_V21_CH2_START_BIT_ID 	0x2182
#define RX_V21_CH2_MESSAGE_ID 		(0x2180|MESSAGE_ID)

	/**** FSK modulator common structure ****/

#define TX_V21_MEMBERS \
	unsigned short Sreg; \
	unsigned short Sreg_low

struct TX_V21_BLOCK
	{
	TX_CONTROL_MEMBERS;
	TX_FSK_MEMBERS;
#if defined(VSIM)
	TX_V21_MEMBERS;
#endif /* VSIM */
	};

	/**** FSK demodulator common structure ****/

#define RX_V21_MEMBERS \
	unsigned short Dreg; \
	unsigned short Dreg_low

struct RX_V21_BLOCK
	{
	RX_CONTROL_MEMBERS;
	RX_FSK_MEMBERS;
#if defined(VSIM)
	RX_V21_MEMBERS;
#endif /* VSIM */
	};

	/**** functions ****/

#if defined(XDAIS_API)
extern void V21_MESI_TxInitV21Ch1(struct START_PTRS *);
extern void V21_MESI_TxInitV21Ch2(struct START_PTRS *);
extern void V21_MESI_RxInitV21Ch1(struct START_PTRS *);
extern void V21_MESI_RxInitV21Ch2(struct START_PTRS *);
#else /* XDAIS_API */
EXTERN_LIBPORT_KEYWORD void Tx_init_v21_ch1(struct START_PTRS *);
EXTERN_LIBPORT_KEYWORD void Tx_init_v21_ch2(struct START_PTRS *);
EXTERN_LIBPORT_KEYWORD void Tx_init_v21h_ch2(struct START_PTRS *);
EXTERN_LIBPORT_KEYWORD void Rx_init_v21_ch1(struct START_PTRS *);
EXTERN_LIBPORT_KEYWORD void Rx_init_v21_ch2(struct START_PTRS *);
#endif /* XDAIS_API */ 

/****************************************************************************/

#endif /* V21_INCLUSION_ */

