/*************************************************************************
 * $RCSfile: packdata.h,v $
 * $Revision: 9.3 $
 * $Date: 2010-02-15 15:51:51 $
 * $Author: peter $
 * Company: MESi
 * Description: HDLC_ IP Network Interface for fax relay
 *                   
 * Copyright (C) MESi 1996-2010, all rights reserved.         
 ***************************************************************************/

#ifndef __PACKDATA__
#define __PACKDATA__

#ifndef TRUE
#define TRUE (1==1)
#define FALSE !TRUE
#endif /* TRUE */
struct PackData{
    unsigned short data;
    unsigned short bits;
    unsigned short mask;
    unsigned short count;
    };

void PkdInit(struct PackData *p,short bits);
short PkdFlush(struct PackData *p);
short PkdPack(struct PackData *p,unsigned short data,unsigned short *retval);
short PkdNeedData(struct PackData *p);
void PkdNextOctet(struct PackData *p,short octet);
short PkdUnpack(struct PackData *p);
/****************************************************************************/
#endif /* __PACKDATA__ */
