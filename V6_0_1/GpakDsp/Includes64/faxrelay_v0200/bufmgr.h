/*************************************************************************
* $RCSfile: bufmgr.h,v $
* $Revision: 9.3 $
* $Date: 2010-06-23 14:32:11 $
* $Author: peter $
* Company: MESi
* Description: Buffer Manager
*
* Copyright (C) MESi 1996-2010, all rights reserved.
***************************************************************************/
#ifndef __BUFMGR__
#define __BUFMGR__
#ifdef BUFLOG
#include <stdio.h>
#endif /* BUFLOG */
/*
 * "BmrRwStruct" - structure to contain packet buffers
 */
struct BmrRwStruct{
    struct SEQ_STRUCT *Seq;
    short index;
    unsigned char *data;
    short len;
    short *buf;
};

#define BMR_SRC_CHAR 				0x01
#define BMR_SRC_INT  				0x02
#define BMR_SRC_BUF  				0x04
#define BMR_DST_CHAR 				0x10
#define BMR_DST_INT  				0X20
#define BMR_DST_BUF  				0X40
/*
 * The number of buffer sections available is defined as NSECTIONS.  This can
 * be used a one buffer of size NSECTIONS*OCTETS_PER_BUFFER or as
 * NSECTIONS buffers each containing OCTETS_PER_BUFFER bytes.
 */

#ifndef NSECTIONS
#define NSECTIONS 					250
#endif /* NSECTIONS */
/*
 * The maximum number of octets (bytes) allowed for a buffer is OCTETS_PER_BUFFER.
 */
#ifndef OCTETS_PER_BUFFER
#define OCTETS_PER_BUFFER 			30
#endif /* OCTETS_PER_BUFFER */

/*
 * set the size of block copy.  The stack is used for temp storage during
 * the copy operation.  Therefore the stack must be at least this large.
 */
#ifndef BMR_BLOCK_COPY_SIZE
#define BMR_BLOCK_COPY_SIZE 		64
#endif

/* The buffer manager maintains linked lists to pass and queue buffers 
 * from one process to another.  The number of linked lists is defined
 * by NLLISTS and the index to each list element is defined below.
 * The NET_IF linked lists are for the network interfaces.
 */
#define NLLISTS 					6
#define LL_TO_MODEM 				0
#define LL_FM_MODEM 				1
#define LL_TO_NET   				2
#define LL_FM_NET   				3
#define LL_TO_NET_IF				4
#define LL_FM_NET_IF				5

#define WORDS_PER_BUFFER 			((OCTETS_PER_BUFFER+1)>>1)

struct BufLinkStruct
{
    struct BufLinkStruct *pNextSect; // pointer to next buffer section
    short count;
    struct BufLinkStruct *pLinkNext;
    unsigned short data[WORDS_PER_BUFFER];
};

#define BUFFER_SECTION_OVERHEAD 	(((sizeof (struct BufLinkStruct)-sizeof(unsigned short)*WORDS_PER_BUFFER)/sizeof(short)))
#define BUF_COUNT_MASK				(short)0x7FFF
#define BUF_EMPTY_SECTION_MASK 		(short)0x8000
#define BMR_SIZE_BUFFER 			(WORDS_PER_BUFFER+BUFFER_SECTION_OVERHEAD)

struct BufStruct
{
	short *(LinkedLists[NLLISTS]);
	short nSections;
	short nTotalSections;
	/* 
	 * when using 32-bit machines, the pointers must be on 32-bit boundaries
	 * To force a 32-bit alignment, the EmptySections pointer must be 
	 * located here, immediately before the buf array. * The buffers 'b' must 
	 * be at the end of the structure if the allocation is going to be dynamic.
	 */
	struct BufLinkStruct *EmptySections;
	/*
	 * NSECTIONS is the number of buffer sections allocated, each 2*BMR_SIZE_BUFFER octets long  
	 */
	short buf[NSECTIONS][BMR_SIZE_BUFFER]; // (short) because it's a packed buffer
};
#define BmrGetSize(sect) (sizeof(struct BufStruct) + \
    (sect*BMR_SIZE_BUFFER*sizeof(short)) - \
	(NSECTIONS*BMR_SIZE_BUFFER*sizeof(short)))

	/**** functions ****/

extern struct BufStruct *BmrInit(struct BufStruct *B, int nIntAvail);
extern  short *BmrGetBuffer(struct SEQ_STRUCT *q);
extern  void BmrReturnBuffer(struct SEQ_STRUCT *q,short *buf);
extern  int BmrLinkBuffer(struct SEQ_STRUCT *q,int ListNum,short *buf,int fTail);
extern  int BmrLinkBufferTail(struct SEQ_STRUCT *q,int ListNum,short *buf);
extern  int BmrLinkBufferHead(struct SEQ_STRUCT *q,int ListNum,short *buf);
extern  int BmrLinkLocalBuffer(struct SEQ_STRUCT *q,short **List,short *buf,int fTail);
extern  int BmrLinkLocalBufferTail(struct SEQ_STRUCT *q,short **List,short *buf);
extern  int BmrLinkLocalBufferHead(struct SEQ_STRUCT *q,short **List,short *buf);
extern  short *BmrUnlinkLocalBuffer(struct SEQ_STRUCT *q,short **List);
extern  short *BmrUnlinkBuffer(struct SEQ_STRUCT *q,int ListNum);

extern  int BmrWriteOctet(struct SEQ_STRUCT *q,int index,int data,short *buf);
extern  int BmrWriteOctets(struct BmrRwStruct *b);
extern  int BmrWriteWord(struct SEQ_STRUCT *q,int index,int data,short *buf);
extern  int BmrReadOctet(int index,short *buf);
extern  int BmrReadOctets(struct BmrRwStruct *b);
extern  int BmrReadWord(int index,short *buf);

extern int BmrCopyData( struct SEQ_STRUCT *qdst,int idst,short *bufdst,
                 short *bufsrc,int isrc,int n);
extern  int BmrGetLen(short *buf);
extern  void BmrSetLen(short *buf,int length);

	/**** macros ****/

/****************************************************************************/
#endif /* __BUFMGR__ */
