/***************************************************************************
 * $RCSfile: rxtx.h,v $                
 * $Revision: 9.7 $              
 * Revision $Author: peter $                              
 * Revision $Date: 2010-12-03 20:32:53 $            
 * Author of Origin: Peter B. Miller             
 * Date of Origin: 11-12-2003
 *                   
 * Company: MESi               
 * E-mail: infomaster@mesi.net            
* Website: www.mesi.net             
 * Description: Structure definitions and extern references for all 		
 * transmitter() and receiver() top level API. 
 *                   
 * Copyright (C) MESi 1996-2010, all rights reserved.         
 ***************************************************************************/

#if !defined(RXTX_INCLUSION_)
#define RXTX_INCLUSION_

	/**** state_ID definitions ****/

#define TX_SILENCE_ID 				0x0100
#define RX_IDLE_ID 					0x0100
#define RX_MEASURE_POWER_ID 		0x0101

#define MESSAGE_ID					0x0040
#define CALL_DIRECTION_ID			0x0080

	/**** Rx->status response messages ****/

#define STATUS_OK					0x00
#define FEATURE_NOT_SUPPORTED		0x01
#define TIMEOUT_FAILURE				0x02
#define DETECT_FAILURE 				0x10
#define DETECT_ENDED 				0x10
#define ANS_TONE_DETECTED			0x11
#define ANS_REVERSAL_DETECTED		0x12
#define ANSAM_DETECTED				0x13
#define SYNC_FAILURE 				0x20
#define CHECKSUM_FAILURE 			0x21
#define CRC_FAILURE 				0x22
#define CID_DETECTED 				0x23
#define TRAIN_LOOPS_FAILURE 		0x30
#define START_EQ_FAILURE 			0x30
#define TRAIN_EQ_FAILURE 			0x31
#define SCR1_FAILURE 				0x40
#define LOSS_OF_LOCK				0x50
#define GAIN_HIT_STATUS				0x51
#define EXCESSIVE_MSE_STATUS		0x52
#define EXCESSIVE_RTD_STATUS		0x53
#define RETRAIN						0x60
#define RETRAIN_FAILURE				0x61
#define RESPONDING_TO_RETRAIN		0x62
#define V34_INFOMARKS_RETRY			0x62
#define RENEGOTIATE					0x62
#define RENEGOTIATE_FAILURE			0x63
#define V22_USB1_DETECTED			0x70
#define V22_S1_DETECTED				0x71
#define V22_SB1_DETECTED			0x72
#define V22_F1M_DETECTED			0x73
//++++#if !defined(MESI_MODIFICATIONS) /* BELL212 DETECT BUGFIX2 PBM 04-01-2011 */
//++++#else /* MESI_MODIFICATIONS         BELL212 DETECT BUGFIX2 PBM 04-01-2011 */
#define V22_F2M_DETECTED			0x74
//++++#endif /* MESI_MODIFICATIONS        BELL212 DETECT BUGFIX2 PBM 04-01-2011 */
#define V32_ANS_DETECTED			0x80
#define V32_AA_DETECTED				0x81
#define V32_AC_DETECTED				0x82
#define V32_SELFLOCK_DETECTED		0x83
#define GSTN_CLEARDOWN_REQUESTED	0x90

	/**** Tx->mode bit field definitions ****/

#define TX_LONG_RESYNC_FIELD		0x0001
#define TX_V26_FAST_SYN_FIELD		0x0001
#define TX_V34_CONTROL_RESYNC		0x0001
#define TX_TEP_FIELD				0x0002
#define TX_CID_TYPE2_BIT			0x0002
#define TX_DIRECTION_BIT			0x0002	// 0=ANSWER, 1=CALL
#define TX_BELLCORE_MODE_BIT		0x0008
#define TX_V32TCM_MODE_BIT			0x0008
#define TX_SOURCE_BIT				0x0008	// 0=recipient, 1=source
#define TX_V32BIS_MODE_BIT			0x0010
#define TX_V32_SPECIAL_TRAIN_BIT	0x0020
#define TX_V34_PRIMARY_RESYNC		0x0020
#define TX_SCRAMBLER_DISABLE_BIT	0x0040
#define TX_V26_ALT_A_BIT			0x0080
#define INTERCEPT_MODE_BIT			0x0100
#define TX_DATA_REVERSE_BIT         0x0400	// 0=normal, 1=bit-reversed

	/**** Rx->mode bit field definitions ****/

#define RX_LONG_RESYNC_FIELD		0x0001
#define RX_V26_FAST_SYN_FIELD		0x0001
#define RX_V34_CONTROL_RESYNC		0x0001
#define RX_DETECTOR_DISABLE			0x0002
#define RX_DIRECTION_BIT			0x0002	// 0=ANSWER, 1=CALL
#define RX_CID_TYPE2_BIT			0x0004
#define RX_LOS_FIELD				0x0004
#define RX_BELLCORE_MODE_BIT		0x0008
#define RX_STU_III_BIT				0x0008
#define RX_SOURCE_BIT				0x0008	// 0=recipient, 1=source
#define RX_EC_COEF_SAVE_BIT			0x0010
#define RX_EQ_COEF_SAVE_BIT			0x0020
#define RX_V34_PRIMARY_RESYNC		0x0020
#define RX_DESCRAMBLER_DISABLE_BIT	0x0040
#define RX_V26_ALT_A_BIT			0x0080
#define FAX_DEMOD_DISABLE_BIT		0x0080
//++++#if !defined(MESI_MODIFICATIONS) /* ANSAM DETECTOR INTEGRATION PBM 6-21-2010 */
//#define V34_HALF_DUPLEX_SELECT_BIT	0x0080	// 0=full duplex, 1=half duplex
//#define RX_EC_DISABLE_BIT			0x0100
//++++#else /* MESI_MODIFICATIONS         ANSAM DETECTOR INTEGRATION PBM 6-21-2010 */
#define RX_EC_DISABLE_BIT			0x0100	// 0=EC enabled, 1=EC disabled
#define V34_HALF_DUPLEX_SELECT_BIT	0x0200	// 0=v.34 full duplex, 1=v.34 half duplex
#define RX_DATA_REVERSE_BIT         0x0400	// 0=normal, 1=bit-reversed
//++++#endif /* MESI_MODIFICATIONS        ANSAM DETECTOR INTEGRATION PBM 6-21-2010 */

	/**** Tx->scale attenuation values ****/

#define ATTENUATE_2DB				26029	/* 32768*10exp(-2 dB/20)	*/
#define ATTENUATE_3DB				23198	/* 32768*10exp(-3 dB/20)	*/
#define ATTENUATE_6DB				16423	/* 32768*10exp(-6 dB/20)	*/
#define ATTENUATE_7DB				14636	/* 32768*10exp(-7 dB/20)	*/
#define ATTENUATE_9DB				11627	/* 32768*10exp(-9 dB/20)	*/
#define ATTENUATE_10DB				10362	/* 32768*10exp(-10 dB/20)	*/
#define ATTENUATE_12DB				8231	/* 32768*10exp(-12 dB/20)	*/
#define ATTENUATE_16DB				5192	/* 32768*10exp(-16 dB/20)	*/
#define ATTENUATE_20DB				3277	/* 32768*10exp(-20 dB/20)	*/
#define ATTENUATE_28DB				1305	/* 32768*10exp(-28 dB/20)	*/
#define ATTENUATE_30DB				1036	/* 32768*10exp(-30 dB/20)	*/
			   
	/**** transmitter/receiver defaults ****/

#define TRANSMIT_FOREVER			-1	  
#define TX_MINUS_16DBM0				32767
#define RX_MINUS_48DBM0				130		/* 32768*10exp(-48 dB/20) */	
#define RXTX_PBB_A1 				31744	/* 1-3.0/PBB_LEN */
#if !defined NUM_SAMPLES
#define NUM_SAMPLES 				20		/* 20 samples per processing frame */                 
#endif /* NUM_SAMPLES */
#if !defined TX_NUM_SAMPLES
#define TX_NUM_SAMPLES 				NUM_SAMPLES        
#endif /* TX_NUM_SAMPLES */
#if !defined RX_NUM_SAMPLES
#define RX_NUM_SAMPLES 				NUM_SAMPLES        
#endif /* RX_NUM_SAMPLES */

/****************************************************************************/

	/************************************************/
	/**** code, coefficients, and channel memory ****/
	/************************************************/

/****************************************************************************/

	/**** code enables ****/

#if !defined(TRANSMITTER)
#define TRANSMITTER					ENABLED
#endif /* TRANSMITTER */
#if !defined(RECEIVER)
#define RECEIVER					ENABLED
#endif /* RECEIVER */
				  
	/**** struct START_PTRS members ****/

#define TRANSMITTER_START_PTRS \
	short *Tx_block_start; \
	CIRC *Tx_sample_start; \
	CIRC *Tx_data_start; \
	CIRC *Tx_fir_start

#define RECEIVER_START_PTRS \
	short *Rx_block_start; \
	CIRC *Rx_sample_start; \
	CIRC *Rx_data_start; \
	CIRC *Rx_fir_start; \
	short *EQ_coef_start; \
	short *EC_coef_start; \
	short *encoder_start; \
	short *decoder_start; \
	CIRC *trace_back_start

struct START_PTRS 
	{
	TRANSMITTER_START_PTRS;
	RECEIVER_START_PTRS;
	};

	/**** Tx_block control members ****/

#define TX_CONTROL_MEMBERS \
	struct START_PTRS *start_ptrs; \
	short (*state)(struct TX_BLOCK *); \
	unsigned short state_ID; \
	unsigned short rate; \
	short scale; \
	short system_delay; \
	short *sample_head; \
	short *sample_tail; \
	unsigned short sample_len; \
	short *data_head; \
	short *data_tail; \
	unsigned short data_len; \
	short sample_counter; \
	short symbol_counter; \
	short call_counter; \
	short num_samples; \
	short mode; \
	short terminal_count; \
	short Nbits;\
	short Nmask; \
	unsigned short bit_register; \
	unsigned short bit_register_low; \
	short bit_index

struct TX_BLOCK 
	{
	TX_CONTROL_MEMBERS;
	};

	/**** Rx_block members ****/

#define RX_CONTROL_MEMBERS \
	struct START_PTRS *start_ptrs; \
	short (*state)(struct RX_BLOCK *); \
	unsigned short state_ID; \
	short status; \
	unsigned short rate; \
	short power; \
	short *sample_head; \
	short *sample_tail; \
	short *sample_stop; \
	unsigned short sample_len; \
	short *data_head; \
	short *data_tail; \
	unsigned short data_len; \
	short sample_counter; \
	short symbol_counter; \
	short call_counter; \
	short num_samples; \
	short mode; \
	short threshold; \
	short detector_mask; \
	short digit_CP_mask; \
	short temp0; \
	short temp1; \
	short Nbits; \
	short Nmask; \
	unsigned short bit_register; \
	unsigned short bit_register_low; \
	short bit_index

struct RX_BLOCK 
	{
	RX_CONTROL_MEMBERS;
	};

	/**** tables and coefficients ****/

#if defined(VSIM)
extern short sin_table[];
#else /* VSIM */
extern const short sin_table[];
#endif /* VSIM */

#if defined(XDAIS_API)
extern void RXTX_MESI_TxBlockInit(struct START_PTRS *);
extern short RXTX_MESI_transmitter(struct START_PTRS *);
extern void RXTX_MESI_TxInitSilence(struct START_PTRS *);

extern void RXTX_MESI_RxBlockInit(struct START_PTRS *);
extern short RXTX_MESI_receiver(struct START_PTRS *);
extern void RXTX_MESI_RxInitIdle(struct START_PTRS *);
extern void RXTX_MESi_RxInitMeasurePower(struct START_PTRS *);
#else /* XDAIS_API */
EXTERN_LIBPORT_KEYWORD void Tx_block_init(struct START_PTRS *);
EXTERN_LIBPORT_KEYWORD short transmitter(struct START_PTRS *);
EXTERN_LIBPORT_KEYWORD void Tx_init_silence(struct START_PTRS *);

EXTERN_LIBPORT_KEYWORD void Rx_block_init(struct START_PTRS *);
EXTERN_LIBPORT_KEYWORD short receiver(struct START_PTRS *);
EXTERN_LIBPORT_KEYWORD void Rx_init_idle(struct START_PTRS *);
EXTERN_LIBPORT_KEYWORD void Rx_init_measure_power(struct START_PTRS *);
#endif /* XDAIS_API */ 

	/**** macros ****/

/****************************************************************************
 * Macros to access Tx_block and Rx_block control section members.			
 ****************************************************************************/

#define set_Tx_num_samples(A, PTR) \
	((struct TX_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->num_samples=A
#define set_Tx_rate(A, PTR) \
	((struct TX_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->rate=A
#define set_Tx_scale(A, PTR) \
	((struct TX_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->scale=A
#define set_system_delay(A, PTR) \
	((struct TX_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->system_delay=A
#define set_Tx_terminal_count(A, PTR) \
	((struct TX_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->terminal_count=A
#define set_Tx_mode(A, PTR) \
	((struct TX_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->mode=A

#define get_Tx_state_ID(PTR) \
	((struct TX_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->state_ID
#define get_Tx_terminal_count(PTR) \
	((struct TX_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->terminal_count
#define get_Tx_mode(PTR) \
	((struct TX_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->mode
#define get_Tx_sample_head(PTR) \
	((struct TX_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->sample_head
#define get_Tx_sample_tail(PTR) \
	((struct TX_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->sample_tail
#define get_Tx_data_head(PTR) \
	((struct TX_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->data_head
#define get_Tx_data_tail(PTR) \
	((struct TX_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->data_tail

#define set_Rx_num_samples(A, PTR) \
	((struct RX_BLOCK *)((struct START_PTRS *)PTR)->Rx_block_start)->num_samples=A
#define set_Rx_rate(A, PTR) \
	((struct RX_BLOCK *)((struct START_PTRS *)PTR)->Rx_block_start)->rate=A
#define set_Rx_mode(A, PTR) \
	((struct RX_BLOCK *)((struct START_PTRS *)PTR)->Rx_block_start)->mode=A
#define get_Rx_state_ID(PTR) \
	((struct RX_BLOCK *)((struct START_PTRS *)PTR)->Rx_block_start)->state_ID
#define get_Rx_mode(PTR) \
	((struct RX_BLOCK *)((struct START_PTRS *)PTR)->Rx_block_start)->mode
#define get_Rx_status(PTR) \
	((struct RX_BLOCK *)((struct START_PTRS *)PTR)->Rx_block_start)->status
#define get_Rx_sample_head(PTR) \
	((struct RX_BLOCK *)((struct START_PTRS *)PTR)->Rx_block_start)->sample_head
#define get_Rx_sample_tail(PTR) \
	((struct RX_BLOCK *)((struct START_PTRS *)PTR)->Rx_block_start)->sample_tail
#define get_Rx_data_head(PTR) \
	((struct RX_BLOCK *)((struct START_PTRS *)PTR)->Rx_block_start)->data_head
#define get_Rx_data_tail(PTR) \
	((struct RX_BLOCK *)((struct START_PTRS *)PTR)->Rx_block_start)->data_tail


/****************************************************************************/
#endif /* RXTX_INCLUSION_ */



