/*************************************************************************
 * $RCSfile: hdlc.h,v $
 * $Revision: 9.5 $
 * $Date: 2010-12-03 20:32:51 $
 * $Author: peter $
 *                   
 * Company: MESi               
 * Website: www.mesi.net             
 * Description: HDLC IP Network Interface for fax relay
 *                   
 * Copyright (C) MESi 1996-2010, all rights reserved.         
 ***************************************************************************/

#if !defined(HDLC_INCLUSION_)
#define HDLC_INCLUSION_

	/**** Return Codes ****/
	
#define RETURN_CODE_HDLC_NO_DATA    -1  
#define RETURN_CODE_HDLC_FLAG       -2  
#define RETURN_CODE_HDLC_GOOD_CRC   -3  
#define RETURN_CODE_HDLC_BAD_CRC    -4  
#define RETURN_CODE_HDLC_VALID_DATA	0

#define HDLC_FIVE_ONES   			1   
#define HDLC_SIX_ONES    			2   
#define HDLC_RX_DATA     			4   
#define HDLC_FLAG_SEARCH 			8   

 	/**** common structure ****/

struct HDLC_STRUCT {
    unsigned short fcs;
    unsigned short runningCount;
    unsigned short stuff;
    unsigned short onescount;
    unsigned short raw;
    unsigned short bitcount;
    unsigned short flags;
};

	/**** functions ****/

EXTERN_LIBPORT_KEYWORD void HDLC_PartialFcs(short currentByte, struct HDLC_STRUCT *h);
EXTERN_LIBPORT_KEYWORD void HDLC_TxFlag(struct HDLC_STRUCT *h, unsigned char *ret);
EXTERN_LIBPORT_KEYWORD unsigned short HDLC_TxClosingFlag(struct HDLC_STRUCT *h, unsigned short *ret);
EXTERN_LIBPORT_KEYWORD short HDLC_TxOctet(struct HDLC_STRUCT *h, unsigned char b, unsigned char *ret);
EXTERN_LIBPORT_KEYWORD short HDLC_TxCrc(struct HDLC_STRUCT *h, unsigned char *ret);
EXTERN_LIBPORT_KEYWORD short HDLC_UnstuffOctet(struct HDLC_STRUCT *h, short b);
EXTERN_LIBPORT_KEYWORD short HDLC_UnstuffBit(struct HDLC_STRUCT *h, short b);
EXTERN_LIBPORT_KEYWORD void HDLC_Init(struct HDLC_STRUCT *h);

	/**** macros ****/

#define HDLC_GetCrc(h) 			((h)->fcs)
#define HDLC_InvertCrc(h) 		((h)->fcs^=0xffff)
#define HDLC_ResetCrc(h) 		((h)->fcs=0xffff)
#define HDLC_CheckCrc(h) 		((h)->fcs == 0x1D0F?TRUE:FALSE)
#define HDLC_LastFrameSize(h) 	((short)(h)->runningCount)

/****************************************************************************/
#endif /* HDLC_INCLUSION_ */

