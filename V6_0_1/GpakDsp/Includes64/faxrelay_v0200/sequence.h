/*************************************************************************
 * $RCSfile: sequence.h,v $
 * $Revision: 9.4 $
 * $Date: 2010-06-23 14:32:13 $
 * $Author: peter $
 * Company: MESi
 * Description: Sequencer Code
 *                   
 * Copyright (C) MESi 1996-2010, all rights reserved.         
 ***************************************************************************/
#ifndef __SEQUENCE__
#define __SEQUENCE__

/*
* When changing the max values, check the bit field sizes in the structure
* below and adjust accordingly.
*/
#define MAX_STATE_MACHINE 		5
#define MAX_EVENTS 				128
#define LAST_ENTRY 				0x8000
#define ALLOW_USER_STATE 		0x4000
#define STATE_MACHINE_MASK 		0x3FFF
#define MAX_TIMERS 				16
#ifndef TRUE
#define TRUE (1==1)
#define FALSE !TRUE
#endif

typedef struct SEQ_STRUCT *SeqStructTypePointer;
typedef void (*PFI)(SeqStructTypePointer);

/* Event Table Struct */

//++++#if !defined(MESI_MODIFICATIONS) /* TI C5500 BUG WORKAROUND PBM 2-01-2011 */
//struct ETS {
//	int        event; 	// triggering event
//	PFI        action;	// action routine to execute
//	const struct ETS *nextstate; // next state to switch to
//	};
//++++#else /* MESI_MODIFICATIONS         TI C5500 BUG WORKAROUND PBM 2-01-2011 */
#if defined(__TMS320C55X__)
/*
 * FIXME: The TI C5500 tools version v4.x.x up to v4.3.8 do not generate correct code
 * if this ETS structure is declared as const. The values that it initializes in
 * the "nextstate member are all 2x the correct address. It seems that the compiler
 * or linker confuses byte and word addresses for .const section. So when
 * ETS struxctures are created in T30.C they are in .bss and an aditional section
 * is required in .cinit to initialize properly. 
 * PBM 2-01-2011
 */
struct ETS {
	short 		event; 	// triggering event
	PFI        	action;	// action routine to execute
	struct ETS 	*nextstate; // next state to switch to
	};
#define ETS_STRUCT struct ETS
#else /* defined(__TMS320C55X__) */
struct ETS {
	short		event; 	// triggering event
	PFI        	action;	// action routine to execute
	const struct ETS *nextstate; // next state to switch to
	};
#define ETS_STRUCT const struct ETS
#endif /* defined(__TMS320C55X__) */
//++++#endif /* MESI_MODIFICATIONS        TI C5500 BUG WORKAROUND PBM 2-01-2011 */

/* state machine structure. One for each state machine */
struct sm{
	int ID;
	struct ETS *pCurrentState;
	};

struct tmr{
	int num;
	unsigned int value;
	};

	/**** Sequencer Health codes ****/

#define SEQ_HEALTHY              	    0x0000		// (legacy) see sequence.c, relay.c
#define SEQ_HEALTH_HEALTHY                  0x0000		// see sequence.c, relay.c
#define SEQ_HEALTH_ABORT                    0x0001
#define SEQ_HEALTH_BUF_POOL_EMPTY           0x0002		// see bufmgr.c
#define SEQ_HEALTH_BUF_EMPTY_SECTION        0x0004		// see bufmgr.c
#define SEQ_HEALTH_BUF_INTERNAL_FAULT       0x0008		// see bufmgr.c
#define SEQ_HEALTH_STATE_MACHINE_FULL       0x0010		// see sequence.c
#define SEQ_HEALTH_EVENT_OVERFLOW           0x0020		// see sequence.c
#define SEQ_HEALTH_TIMER_OVERFLOW	    0x0040 		// see sequence.c

	/**** Sequencer memory structure ****/

struct SEQ_STRUCT {
    struct FAX_STRUCT *FaxPtrs;
    unsigned int health;
	unsigned int MasterTimer;
	struct tmr Timer[MAX_TIMERS];
	struct sm StateMachine[MAX_STATE_MACHINE];
	unsigned int Events[MAX_EVENTS];
	unsigned int EventHead;
	unsigned int EventTail;
	unsigned int nTimers; // six bits used 
	unsigned int nStateMachines; // six bits used
	unsigned int ResidualSamples; // four bits used
	short *LastRxSampleHead;
	/* pRxSampleHead contains the address of the RxSampleHead pointer
	* This contains &START_PTRS->Rx_sample_block->sample_head
	*/
	short **pRxSampleHead;
	void *pBufMgr; 						/* pointer to BufStruct structure */
	struct T30_PROTOCOL_STRUCT *Protocol;	/* pointer to protocol structure */
#if defined(BUFLOG) | defined(SNOOP_CALL_LOG) | defined(NEED_REALTIME)
#if !defined(NEED_REALTIME)
#define NEED_REALTIME
#endif /* NEED_REALTIME */
	unsigned long RealTime;/* the count in ms since the call began */
#endif /* BUFLOG|SNOOP_CALL_LOG|NEED_REALTIME */
	short *DepBuffer;
    short *PartialIpFrame;
#ifdef INTERCEPT_SUPPORT
	int BitsPerBaud;
#endif /* INTERCEPT_SUPPORT */
#ifdef HOOK_SWITCH
	struct HookSwitchStruct *HookSwitch;
#endif
#ifdef _MSWINDOWS
	char DisplayState[80];
#endif /*_MSWINDOWS*/
    short AllowStateChange;
	};

extern void AddEvent(struct SEQ_STRUCT *q, unsigned int Event);
extern void DelEvent(struct SEQ_STRUCT *q, unsigned int Event);
extern int Sequencer(struct FAX_STRUCT *f);
extern int AddStateMachine(struct SEQ_STRUCT *q, int StateID, struct ETS *StateTable);
extern int DelStateMachine(struct SEQ_STRUCT *q, int StateID);
extern int SeqTimeCheck(struct FAX_STRUCT *f);
extern void StopTimer(struct SEQ_STRUCT *q, int TimeNum);
extern void StartTimer(struct SEQ_STRUCT *q, int TimeNum, unsigned int Nomst);
extern unsigned int ReadTimer(struct SEQ_STRUCT *q, int TimeNum);
extern void TimerTick(struct SEQ_STRUCT *q);
extern void InitSequencer(struct FAX_STRUCT *);
extern void SeqBufDepEvent(struct SEQ_STRUCT *q,short *buf, int Event);
extern int SeqGetEventRemain(struct SEQ_STRUCT *q);

#define BlockStateChange(q) (((struct SEQ_STRUCT *)q)->AllowStateChange=FALSE)

/****************************************************************************/
#endif /* __SEQUENCE__ */

