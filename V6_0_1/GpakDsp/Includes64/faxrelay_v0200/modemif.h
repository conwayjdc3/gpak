/*************************************************************************
* $RCSfile: modemif.h,v $
* $Revision: 9.2 $
* $Date: 2010-02-15 16:13:49 $
* $Author: peter $
* Company: MESi
* Description: Header for Modem interface code
*
* Copyright (C) MESi 1996-2010, all rights reserved.
***************************************************************************/
#ifndef __MODEM_IF__
#define __MODEM_IF__
#include "sequence.h"
#include "packdata.h"
#include "hdlc.h"

#ifdef VOICE
#ifndef VOICE_PACKET_SIZE
#define VOICE_PACKET_SIZE 		80
#endif /* VOICE_PACKET_SIZE */
#endif /* VOICE */

#if !defined(FAX_PACKET_SIZE)
#define FAX_PACKET_SIZE			10		/* packet length in msec. */
#endif /* FAX_PACKET_SIZE */

	/**** transmitter modemIF states ****/

#define MIF_TX_IDLE				0
#define MIF_TX_FLAGS			1
#define MIF_TX_V21DATA			2
#define MIF_TX_UNDERRUN			3
#define MIF_TX_WAIT_EMPTY		4
#define MIF_TX_FAX_DATA			5
#ifdef VOICE
#define MIF_TX_AUDIO        	6
#endif /* VOICE */

   	/* Receiver Configuration-Specific Data: Modulation Type */

#define MIF_MOD_V27_2400		0x00	// table 2/T.30, DCS bits 9-14
#define MIF_MOD_V29_9600		0x20    // table 2/T.30, DCS bits 9-14
#define MIF_MOD_V27_4800		0x10    // table 2/T.30, DCS bits 9-14
#define MIF_MOD_V29_7200		0x30    // table 2/T.30, DCS bits 9-14
#define MIF_MOD_V17_14400		0x04    // table 2/T.30, DCS bits 9-14
#define MIF_MOD_V17_9600		0x24    // table 2/T.30, DCS bits 9-14
#define MIF_MOD_V17_12000		0x14    // table 2/T.30, DCS bits 9-14
#define MIF_MOD_V17_7200		0x34    // table 2/T.30, DCS bits 9-14
#define MIF_MOD_V34_SOURCE		0x38	// table 2/T.30, DCS bits 9-14 Reserved
#define MIF_MOD_V34_RECIPIENT	0x3c	// table 2/T.30, DCS bits 9-14 Reserved

#define V17_MODEM_ID 			0x1700
#define V27_MODEM_ID 			0x2700
#define V29_MODEM_ID 			0x2900
#define V21_MODEM_ID 			0x2100
#define V8_MODEM_ID 			0x0800
#define V34_MODEM_ID 			0x3400

#define MIF_DIALTONE			1
#define MIF_BUSY				2
#define MIF_REORDER				3
#define MIF_ECSD				4
#define MIF_RINGBACK			5
#define MIF_RX_SILENCE          (0)
#define MIF_RX_OR_MASK          (0)
#define MIF_RX_AUTO_DETECT_MASK (AUTO_DETECT_MASK)
#define MIF_RX_V21_MASK         (V21_CH2_MASK|AUTO_DETECT_MASK)
#define MIF_RX_CNG_MASK         (CNG_MASK|AUTO_DETECT_MASK)
#define MIF_RX_CED_MASK         (CED_MASK|AUTO_DETECT_MASK)
#define MIF_RX_ANSAM_MASK       (ANSAM_MASK|AUTO_DETECT_MASK)
#define MIF_RX_CALLPROG_MASK    (CALL_PROGRESS_MASK|AUTO_DETECT_MASK)

#define MIF_LOG_INPUT           0x01
#define MIF_8BIT_INPUT          0x02
#define MIF_16BIT_INPUT         0x04
#define MIF_MULAW_INPUT         0x08
#define MIF_TEXT_INPUT          0x10
#define MIF_SOUNDCARD_INPUT     0x20
#define MIF_SOUNDCARD_OUTPUT    0x40
#define MIF_SOUNDCARD_LOOPBACK  0x80
#define MIF_FILE_INPUT          0x100
#define MIF_FILE_OUTPUT         0x200

   	/* Receiver Configuration-Specific Data: Mode codes */

#define MIF_MODE_NO_ECM			0   	// sends zeros 		
#define MIF_MODE_ECM			1		// sends flags
#define MIF_MODE_TRAINING		2		// sends last received data packet

/* DCS Table Mapping xxxx xxxx xxxy zzzz
 *          x = data rate
 *          y = MIF_LONGTRAIN_BIT
 *          z = bits per baud
 */
#define MIF_DCS_BITRATE_MASK	0xffe0
#define MIF_DCS_BITSPERBAUD_MASK 0x000f
#define MIF_LONGTRAIN_BIT 		0x10
#define MIF_RESYNC_BIT 			0x00
#define MIF_MOD_V17 			0
#define MIF_MOD_V27 			1
#define MIF_MOD_V29 			2
#define MIF_MOD_V33 			3

struct MIF_STRUCT{
    struct PackData pack;
    struct PackData txPacked;
    struct HDLC_STRUCT rxhdlc;
    struct HDLC_STRUCT txhdlc;
 	unsigned short OldRxState;
 	unsigned short OldTxState;
	unsigned short RxBitRate;
	short *RxBuf;		/* point to BufMgr buffer for rcvr */
	int RxCount;	/* count of bytes in current frame */
	int RxFlagCount;
	short *TxBuf;		/* pointer to buffer for the transmitter */
	int TxFlagCount;
	int TxState;
	int TxFlagReq;  /* number of flags that need to be modulated */
	int TxCount;	/* offset from TxOffsetMsg of next octet to tx */
	int TxFrameSize; /* number of octets in current frame */
	int SeqNr;
	int TxDataType;	/* ecm, non-ecm, training */
	unsigned short BitsPerBaud;
	int Residue;  /* number of bits of flag that were not transmitted. */
			/* tx data buffer to prevent underrunning */
    int RcvdFirstPacket;
#ifdef VOICE
    int SampleDefecit;
    int VoiceStatus;
    int voiceEnergy;
    int RxNeedsInit;
#endif
    int PacketLen;    /* duration of output frame in ms */
	};

extern void MifModemIf(struct FAX_STRUCT *);
extern void MifInitModemIf(struct MIF_STRUCT *);
extern const unsigned short MifDcs2Rate[];

/*****************************************************************************/
#endif /* __MODEM_IF__ */
