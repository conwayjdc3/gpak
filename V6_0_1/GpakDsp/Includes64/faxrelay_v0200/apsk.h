/***************************************************************************
 * $RCSfile: apsk.h,v $
 * $Revision: 9.9 $
 * Revision $Author: peter $
 * Revision $Date: 2010-12-03 20:32:47 $
 * Author of Origin: Peter B. Miller
 * Date of Origin: 11-12-2003
 *
 * Company: MESi
 * E-mail: infomaster@mesi.net
 * Website: www.mesi.net
 * Description: memory and symbol definitions for APSK modem modules.
 *
 * Copyright (C) MESi 1996-2010, all rights reserved.
 ***************************************************************************/

#if !defined(APSK_INCLUSION_)
#define APSK_INCLUSION_

	/**** APSK modulator common structure ****/

#define TX_APSK_MEMBERS \
	short interpolate; \
	short decimate; \
	short coef_ptr; \
	short *coef_start; \
	short *fir_head;	\
	short *fir_tail;	\
	short fir_len; \
	short fir_taps; \
	short sym_clk_offset; \
	short sym_clk_memory; \
	short sym_clk_phase; \
	unsigned short carrier; \
	short *map_ptr; \
	short *amp_ptr; \
	unsigned short phase; \
	unsigned short Sreg; \
	unsigned short Sreg_low; \
	short fir_scale; \
	unsigned short Ereg

struct TX_APSK_BLOCK
	{
	TX_CONTROL_MEMBERS;
	TX_APSK_MEMBERS;
	};

	/**** APSK demodulator common structures ****/

struct IIR_RESONATOR_BLOCK {
	short coef;
	short dnm1;
	short dnm2;
};

#define RX_APSK_MEMBERS \
	short (*decoder_ptr)(struct RX_BLOCK *); \
	short (*slicer_ptr)(struct RX_BLOCK *); \
	short (*timing_ptr)(struct RX_BLOCK *); \
	short baud_counter; \
	short *data_ptr; \
	short *sample_ptr;	/* This member not used */ \
	short fir_taps; \
	short *coef_start; \
	short coef_ptr; \
	short sym_clk_phase; \
	short interpolate; \
	short decimate; \
	short oversample;	/* This member not used */ \
	short *timing_start; /* This member not used */ \
	short I; \
	short Q; \
	short IEQ; \
	short QEQ; \
	short Iprime; \
	short Qprime; \
	short Inm1; \
	short Qnm1; \
	short Inm2; \
	short Qnm2; \
	short Inm3; \
	short Qnm3; \
	short Ihat; \
	short Qhat; \
	short Ihat_nm2; \
	short Qhat_nm2; \
	unsigned short What; \
	short *fir_ptr; \
	short IEQprime_error; \
	short QEQprime_error; \
	unsigned short EQ_MSE; \
/*++++#if !defined(MESI_MODIFICATIONS)*/ /* EQ_MSE_COEF MODS PBM 6-23-2009 */ \
/*++++#else*/ /* MESI_MODIFICATIONS         EQ_MSE_COEF MODS PBM 6-23-2009 */ \
	unsigned short EQ_MSE_low; \
	short EQ_MSE_coef; \
/*++++#endif*/ /* MESI_MODIFICATIONS        EQ_MSE_COEF MODS PBM 6-23-2009 */ \
	short EQ_2mu; \
	short COS; \
	short SIN; \
	unsigned short LO_memory; \
	unsigned short LO_frequency; \
	short LO_phase; \
	unsigned short vco_memory; \
	short phase_error; \
	short loop_memory; \
	short loop_memory_low; \
	short loop_K1; \
	short loop_K2; \
	struct IIR_RESONATOR_BLOCK PJ1; /* This member not used */ \
	struct IIR_RESONATOR_BLOCK PJ2; /* This member not used */ \
	short agc_gain; \
	short agc_K; \
	short frequency_est; \
	short sym_clk_memory; \
	short timing_threshold; \
	short coarse_error; \
	short LOS_counter; /* This member not used */  \
	unsigned short LOS_monitor; \
	short map_shift; \
	short Phat; \
	short phase; \
	short EQ_taps; \
	unsigned short Dreg; \
	unsigned short Dreg_low; \
	unsigned short pattern_reg; \
	short *trace_back_ptr; \
	short *signal_map_ptr; \
	short *EC_fir_ptr; \
	short *EC_sample_ptr; \
	short EC_2mu;	\
	short EC_MSE;	\
	short EC_taps; \
	short EC_shift; \
	short RTD

struct RX_APSK_BLOCK
	{
	RX_CONTROL_MEMBERS;
	RX_APSK_MEMBERS;
	};

	/**** RCOS filter coefficients ****/

/*
 * RCOS COEFFICIENT LENGTH:  The RCOS filter coefficient length is calculated
 * based on the specified number of taps, interpolate and decimate rates,
 * oversample rate, and the extension for polyphase operation. A standard FIR
 * filter requires TAPS coefficients for the specified number of taps. An
 * interpolator/decimator structure requires TAPS*INTERP coefficients. To
 * implement a polyphase filter we oversample by OVERSAMPLE, and implement
 * a method for adjusting the sample phase coincident with filter coefficient
 * phase. For modulator filter, the polyphase span is one symbol, or the
 * decimation rate. For the demodulator (sample-rate to symbol rate), the
 * polyphase span is one sample, or the interpolate rate. The RCOS coefficients
 * are shared by modulator and demodulator for a given symbol rate, and
 * also for several symbol rates for some types of modems. The length set for
 * RCOS coefficients is fixed by the demodulator since those filters must be
 * longer than the modulators so the RX_RCOSxxx_LEN is calculated for demodulators
 * as:
 * RX_RCOSxxx_LEN=M*(TAPS*INTERP+(INTERP-1))
 *                |    |     |      |
 *                |    |     |      +- for the polyphase expansion over one
 *                |    |     |         sample
 *                |    |     +- interpolate rate for interp/decimate
 *                |    +- number of filter taps
 *                +- M is the oversample rate, OVERSAMPLE
 * We include OVERSAMPLE in the INTERP term and simplify to:
 *   RX_RCOSxxx_LEN=(TAPS*(INTERP+1) - OVERSAMPLE)
 *
 * Similarly, for transmit filter operation the polyphase expansion is over
 * DECIMATE samples:
 *   TX_RCOSxxx_LEN=(TAPS*INTERP + DECIMATE-OVERSAMPLE)
 */

#define ROLL600                     0.75f
#define OVERSAMPLE600               1   
#define INTERP600                   (3*OVERSAMPLE600)
#define DEC600                      (20*OVERSAMPLE600)
#define TAPS600                     60  
#define RX_RCOS600_LEN              (TAPS600*INTERP600+DEC600)
#define TX_TAPS600                  3   
#define TX_RCOS600_LEN 				(TX_TAPS600*2*DEC600+INTERP600-OVERSAMPLE600)
#define INTERP_SAMPLE_RATE600       (8000*INTERP600)

#define ROLL1200                    0.75f
#define OVERSAMPLE1200              4   
#define INTERP1200                  (3*OVERSAMPLE1200)
#define DEC1200                     (10*OVERSAMPLE1200)
#define TAPS1200                    18  
#define RCOS1200_LEN				(TAPS1200*INTERP1200+DEC1200)
#define TX_TAPS1200					3   
#define INTERP_SAMPLE_RATE1200      (8000*INTERP1200)

#define ROLL1600                    0.75f
#define OVERSAMPLE1600              2   
#define INTERP1600                  (6*OVERSAMPLE1600)
#define DEC1600                     (15*OVERSAMPLE1600)
#define TAPS1600                    18  
#define RCOS1600_LEN                (TAPS1600*INTERP1600+DEC1600)
#define TX_TAPS1600					3   
#define INTERP_SAMPLE_RATE1600      (8000*INTERP1600)

#define ROLL2400					0.75f
#define OVERSAMPLE2400				8   
#define INTERP2400                  (3*OVERSAMPLE2400)
#define DEC2400                     (5*OVERSAMPLE2400)
#define TAPS2400					16  
#define RCOS2400_LEN                (TAPS2400*INTERP2400+DEC2400)
#define TX_TAPS2400					5   
#define INTERP_SAMPLE_RATE2400      (8000*INTERP2400)

//#if defined(V34_APSK_COEFFICIENTS) & (V34_APSK_COEFFICIENTS != DISABLED)

#define V34_OVERSAMPLE 				64

#define V34_ROLL600 				0.75f
#define V34_OVERSAMPLE600 			1
#define V34_INTERP600 				(3*V34_OVERSAMPLE600)
#define V34_DEC600 					(20*V34_OVERSAMPLE600)
#define V34_TAPS600		 			60
#define V34_RX_RCOS600_LEN 			(V34_TAPS600*V34_INTERP600+V34_DEC600-V34_OVERSAMPLE600)
#define V34_TX_TAPS600		 		((short)(V34_TAPS600*V34_INTERP600/(2*V34_DEC600)))
#define V34_TX_RCOS600_LEN 			(V34_TX_TAPS600*2*V34_DEC600+V34_INTERP600-V34_OVERSAMPLE600)
#define V34_INTERP_SAMPLE_RATE600   (8000*V34_INTERP600)

#define V34_ROLL2400 				0.50f
#define V34_OVERSAMPLE2400 			V34_OVERSAMPLE
#define V34_INTERP2400 				(3*V34_OVERSAMPLE2400)
#define V34_DEC2400 				(5*V34_OVERSAMPLE2400)
#define V34_TAPS2400		 		20  
#define V34_RCOS2400_LEN 			(V34_TAPS2400*V34_INTERP2400+V34_DEC2400-V34_OVERSAMPLE2400)
#define V34_TX_TAPS2400		 		((short)(V34_TAPS2400*V34_INTERP2400/(2*V34_DEC2400)))
#define V34_INTERP_SAMPLE_RATE2400	(8000*V34_INTERP2400)

#define V34_ROLL2743 				0.50f
#define V34_OVERSAMPLE2743 			V34_OVERSAMPLE
#define V34_INTERP2743 				(24*V34_OVERSAMPLE2743)
#define V34_DEC2743 				(35*V34_OVERSAMPLE2743)
#define V34_TAPS2743		 		33  
#define V34_RCOS2743_LEN 			(V34_TAPS2743*V34_INTERP2743+V34_DEC2743-V34_OVERSAMPLE2743)
#define V34_TX_TAPS2743		 		((short)(V34_TAPS2743*V34_INTERP2743/(2*V34_DEC2743)))
#define V34_INTERP_SAMPLE_RATE2743	(8000*V34_INTERP2743)

#define V34_ROLL2800 				0.50f
#define V34_OVERSAMPLE2800 			V34_OVERSAMPLE
#define V34_INTERP2800 				(7*V34_OVERSAMPLE2800)
#define V34_DEC2800 				(10*V34_OVERSAMPLE2800)
#define V34_TAPS2800		 		33  
#define V34_RCOS2800_LEN 			(V34_TAPS2800*V34_INTERP2800+V34_DEC2800-V34_OVERSAMPLE2800)
#define V34_TX_TAPS2800		 		((short)(V34_TAPS2800*V34_INTERP2800/(2*V34_DEC2800)))
#define V34_INTERP_SAMPLE_RATE2800	(8000*V34_INTERP2800)

#define V34_ROLL3000 				0.35f
#define V34_OVERSAMPLE3000 			V34_OVERSAMPLE
#define V34_INTERP3000 				(3*V34_OVERSAMPLE3000)
#define V34_DEC3000 				(4*V34_OVERSAMPLE3000)
#define V34_TAPS3000		 		24  
#define V34_RCOS3000_LEN 			(V34_TAPS3000*V34_INTERP3000+V34_DEC3000-V34_OVERSAMPLE3000)
#define V34_TX_TAPS3000		 		((short)(V34_TAPS3000*V34_INTERP3000/(2*V34_DEC3000)))
#define V34_INTERP_SAMPLE_RATE3000	(8000*V34_INTERP3000)

#define V34_ROLL3200 				0.20f
#define V34_OVERSAMPLE3200 			((short)(0.75*V34_OVERSAMPLE))
#define V34_INTERP3200 				(4*V34_OVERSAMPLE3200)
#define V34_DEC3200 				(5*V34_OVERSAMPLE3200)
#define V34_TAPS3200		 		38  
#define V34_RCOS3200_LEN 			(V34_TAPS3200*V34_INTERP3200+V34_DEC3200-V34_OVERSAMPLE3200)
#define V34_TX_TAPS3200		 		((short)(V34_TAPS3200*V34_INTERP3200/(2*V34_DEC3200)))
#define V34_INTERP_SAMPLE_RATE3200	(8000*V34_INTERP3200)

#define V34_ROLL3429 				0.20f
#define V34_OVERSAMPLE3429 			((short)(0.5*V34_OVERSAMPLE))
#define V34_INTERP3429 				(6*V34_OVERSAMPLE3429)
#define V34_DEC3429 				(7*V34_OVERSAMPLE3429)
#define V34_TAPS3429		 		40  
#define V34_RCOS3429_LEN 			(V34_TAPS3429*V34_INTERP3429+V34_DEC3429-V34_OVERSAMPLE3429)
#define V34_TX_TAPS3429		 		((short)(V34_TAPS3429*V34_INTERP3429/(2*V34_DEC3429)))
#define V34_INTERP_SAMPLE_RATE3429	(8000*V34_INTERP3429)

//#endif /* V34_APSK_COEFFICIENTS */

	/***********************************/
	/**** APSK modulator parameters ****/
	/***********************************/

/*
 * APSK demodulator operating point, OP_POINT 
 */
#define OP_POINT 			        	8192	// 32768*(1/4)
#define OP_POINT_SHIFT		        	2		// 2^-OP_POINT_SHIFT=1/4
#define ONE_OVER_OP_POINT				(1<<OP_POINT_SHIFT)	// 4
/*
 * WHAT_SCALE is used in the Phase Detector (PD) and Loss Of Signal. 
 */
#define WHAT_SCALE						2048	// 32768*(1/16)
#define WHAT_SCALE_SHIFT				4		// 2^4=16
/*
 * PLL_K1_SCALE and PLL_K2_SCALE is used in the PLL loop filter coefficients 
 * K1 and K2 to scale them up to reasonable integer values. 
 */
#define PLL_SCALE_SHIFT					0
#define PLL_SCALE						(1<<PLL_SCALE_SHIFT)
#define PLL_K1_SCALE_SHIFT				0
#define PLL_K1_SCALE					(1<<PLL_K1_SCALE_SHIFT)
#define PLL_K2_SCALE_SHIFT				0
#define PLL_K2_SCALE					(1<<PLL_K2_SCALE_SHIFT)

#define EQ_MSE_GAIN_SHIFT				4
#define EQ_MSE_GAIN						(1<<EQ_MSE_GAIN_SHIFT)
#define EQ_MSE_SCALE					OP_POINT*EQ_MSE_GAIN
#define EQ_MSE_SCALE_SHIFT				(OP_POINT_SHIFT+EQ_MSE_GAIN_SHIFT)
#define LOS_SCALE						OP_POINT
#define LOS_SCALE_SHIFT					OP_POINT_SHIFT

/*
 * This behaves differently for OP_POINT=1/4 and needs to be re-visited.
 * This value was adjusted because v22 was not detecting S1 fast enough.
 */
#define COARSE_THR 						4096 // (32768*OP_POINT*0.5)
#define COARSE_ERROR_SHIFT				4
#define TIMING_TABLE_LEN				(4*4)
#define COEF_INCR 						(0*4)
#define COEF_DECR 						(1*4)
#define FIR_INCR 						(2*4)
#define FIR_DECR 						(3*4)
#if !defined(RX_SAMPLE_INCREMENT)
#define RX_SAMPLE_INCREMENT				1	// 1 for one real entity per sampling interval (2 for complex sample)
#endif /* RX_SAMPLE_INCREMENT */

	/***********************************************************************
	 * Rx->timing_threshold bit fields:
	 * TTTT TTTT TTTT IIII
	 * -------------- ----
	 *         |        |_ bits0-7: increment
	 *         |_ bits8-15: threshold
	 ************************************************************************/

#define SYM_CLK_THRESHOLD				16384
#define SYM_CLK_THRESHOLD_SHIFT			4
#define SYM_CLK_INCREMENT_FIELD 		((1<<SYM_CLK_THRESHOLD_SHIFT)-1)
#define SYM_CLK_THRESHOLD_FIELD			(~SYM_CLK_INCREMENT_FIELD)
#define SYM_CLK_INCREMENT				1		// default timing adjustment
#define ALTS_TIMING_ENABLE
#define ALTS_SYM_CLK_THRESHOLD			0
#define ALTS_SYM_CLK_INCREMENT_SHIFT	1		// increment=1<<0 = 1
#define ALTS_SYM_CLK_INCREMENT			(1<<ALTS_SYM_CLK_INCREMENT_SHIFT)
#define ALTS_NUM_REPLAY_SYMBOLS			6		// number of replay symbols (must be even)
#define EQ_ALTS_BACKUP_MASK				6		// mask to ensure even symbol backup

#define REV_CORR_LEN 					16
#define REV_CORR_DELAY 					(4*REV_CORR_LEN/4)	/* 2 symbols*2x baud*(REV_CORR_LEN/4)*/

	/***********************************************************************
	 * LOS detector (See 07-06-2004 for design details)
	 * Rx->LOS_monitor bit fields:
	 * L S GGGGGGGGGGG CCC
	 * - - ----------- ---
	 * | |      |       |_ bits 0-2: 3-bit LOS counter
	 * | |      |_ bits3-13: 10-bit Gap counter (up to 1023)
	 * | |_ bit 14: LOS state bit, 0=no signal, 1=signal detected
	 * |_ bit 15: Lock monitor bit, 0=unlocked, 1=locked.
	 ************************************************************************/

#define LOS_THRESHOLD		  			(LOS_SCALE/4)	/* 6 dB drop */
#define LOS_COUNTER_SHIFT				0
#define LOS_COUNTER_FIELD               (0x0007u<<LOS_COUNTER_SHIFT)
#define LOS_COUNTER_INCREMENT			(0x0001u<<LOS_COUNTER_SHIFT)
#define GAP_COUNTER_SHIFT				3
#define GAP_COUNTER_FIELD               (0x07ffu<<GAP_COUNTER_SHIFT)
#define GAP_COUNTER_INCREMENT			(0x0001u<<GAP_COUNTER_SHIFT)
#define LOS_STATE_SHIFT					14
#define LOS_STATE_FIELD                 (0x0001u<<LOS_STATE_SHIFT)
#define LOS_UNDETECTED					0
#define LOS_DETECTED					LOS_STATE_FIELD
#define LOCK_STATE_SHIFT				15
#define LOCK_STATE_FIELD				(0x0001u<<LOCK_STATE_SHIFT)
#define LOCK_DETECTED					LOCK_STATE_FIELD
#define LOCK_UNDETECTED					0
#define LOS_GAP_DETECTED				(LOCK_STATE_FIELD+LOS_STATE_FIELD)

#define LOS_DETECT_COUNT				4	/* consec. counts for detecting loss-of-lock */
#define LOS_UNDETECT_COUNT				4	/* consec. counts for detecting lock */

	/**** internal buffering in Rx_fir[] ****/

/*
 * See 2-25-2004 for initial design details.
 * Rx_fir[] is enlarged and used to store intermediate signals for precoder,
 * Noise Predicter filter, and proposed polyphase filter. We also make provision
 * to store the demod T/2 delay line for Iprime, Inm1, Inm2, Inm3.
 *
 *                                 Rx_fir[]
 *                           ~                       ~
 *                           |   :                   |
 *                           | I, Q                  |
 *              Rx->fir_ptr->|                       |
 *                           |   :                   |
 *                           |  Polyphase Buffer     |
 *                           |  (filtered by AEQ)    |
 *                           |   :                   |
 *                           | IEQ, QEQ              |
 *  POLYPHASE_BUFFER_OFFSET->|                       |
 *                           |   :                   |
 *                           |  IQ Delay Buffer      |
 *                           |  (rotated by VCO)     |
 *                           |   :                   |
 *                           | Inm3, Qnm3            |
 *                           | Inm2, Qnm2            |
 *                           | Inm1, Qnm1            |
 *                           | Iprime, Qprime        |
 *   IQ_DELAY_BUFFER_OFFSET->|                       |
 *                           | NPF_nm3 (mid-phase)   |
 *                           | NPF_nm3               |
 *                           | NPF_nm2 (mid-phase)   |
 *                           | NPF_nm2               |
 *                           | NPF_nm1 (mid-phase)   |
 *                           | NPF_nm1               |
 *                           | NPF_prime (mid-phase) |
 *                           | NPF_prime             |
 *                           |   :                   |
 *                           |  inverse precoder     |
 *                           |  (see v34.h)          |
 *                           |   :                   |
 *                           ~                       ~
 *
 */

#define POLYPHASE_FIR_LEN			7
#if !defined(POLYPHASE_BUFFER_LEN)
#define POLYPHASE_BUFFER_LEN		0	// default to disable
#endif /* POLYPHASE_BUFFER_LEN */
#if !defined(IQ_DELAY_BUFFER_LEN)
#define IQ_DELAY_BUFFER_LEN			4	// IQprime, IQ_nm1, IQ_nm2, IQ_nm3
#endif /* IQ_DELAY_BUFFER_LEN */
#if !defined(IQHAT_DELAY_BUFFER_LEN)
#define IQHAT_DELAY_BUFFER_LEN		0	// for future IQHat delay line for DFE
#endif /* IQHAT_DELAY_BUFFER_LEN */

#if POLYPHASE_BUFFER_LEN == 0
#define POLYPHASE_BUFFER_OFFSET		(2*IQ_DELAY_BUFFER_LEN)
#define IQ_DELAY_BUFFER_OFFSET		(2*IQ_DELAY_BUFFER_LEN)
#else /* POLYPHASE_BUFFER_LEN == 0 */
#define POLYPHASE_BUFFER_OFFSET		(2*POLYPHASE_BUFFER_LEN)
#define IQ_DELAY_BUFFER_OFFSET		(POLYPHASE_BUFFER_OFFSET + 2*IQ_DELAY_BUFFER_LEN)
#endif /* POLYPHASE_BUFFER_LEN == 0 */
#define IQ_IPRIME_OFFSET			(IQ_DELAY_BUFFER_OFFSET)
#define IQ_INM1_OFFSET				(IQ_DELAY_BUFFER_OFFSET-2)
#define IQ_INM2_OFFSET				(IQ_DELAY_BUFFER_OFFSET-4)
#define IQ_INM3_OFFSET				(IQ_DELAY_BUFFER_OFFSET-6)

	/**** Automatic Gain Control ****/

//++++#if !defined(MESI_MODIFICATIONS) /* BELL212 DETECT BUGFIX2 PBM 04-01-2011 */
//#define AGC_REF 					8192	/* OP_POINT */
//++++#else /* MESI_MODIFICATIONS         BELL212 DETECT BUGFIX2 PBM 04-01-2011 */
#define AGC_REF 					2048	/* OP_POINT^2 */
#define AGC_GAIN_SCALE_SHIFT		6				
#define AGC_GAIN_SCALE				(1<<AGC_GAIN_SCALE_SHIFT)	/* 2^6=64 */
#define AGC_UNITY_GAIN				512	/* 32768/AGC_GAIN_SCALE */
//++++#endif /* MESI_MODIFICATIONS        BELL212 DETECT BUGFIX2 PBM 04-01-2011 */
#define AGC_EST_SEED 				14338	/* -41 dB*/
#define AGC_EST_STEP 				16423
#define AGC_UPDATE_DISABLED			0

#define COS_PI_BY_4 				23170	/* 32768*cos(pi/4)*/
#define TWENTY_SIX_DEGREES 			4836
#define FOURTY_FIVE_DEGREES			8192
#define NINETY_DEGREES 				16384
#define ONE_EIGHTY_DEGREES 			32768
#define RX_DET_LEN 					80

	/**** tables and coefficients ****/

extern const short Rx_timing600[];
extern const short Rx_timing1200[];
extern const short Rx_timing1600[];
extern const short Rx_timing2400[];
extern const short Rx_timing3000[];
extern const short Rx_timing3200[];

#if defined(VSIM)
extern short Tx_RCOS600_f1200[];                                              
extern short Tx_RCOS600_f2400[];                                              
extern short Rx_RCOS600_f1200[];                                              
extern short Rx_RCOS600_f2400[];                                              
extern short RCOS1200_f1800[];                                                
extern short RCOS1600_f1800[];                                                
extern short RCOS2400_f1800[];

extern short v34_Tx_RCOS600_f1200[];                                              
extern short v34_Tx_RCOS600_f2400[];                                              
extern short v34_Rx_RCOS600_f1200[];                                              
extern short v34_Rx_RCOS600_f2400[];                                              
extern short v34_RCOS2400_f1800[];
extern short v34_RCOS3000_f1900[];
extern short v34_RCOS3200_f1875[];
extern short v34_RCOS3429_f1959[];
#else /* VSIM */
extern const short Tx_RCOS600_f1200[];                                              
extern const short Tx_RCOS600_f2400[];                                              
extern const short Rx_RCOS600_f1200[];                                              
extern const short Rx_RCOS600_f2400[];                                              
extern const short RCOS1200_f1800[];                                                
extern const short RCOS1600_f1800[];                                                
extern const short RCOS2400_f1800[];    

extern const short v34_Tx_RCOS600_f1200[];                                              
extern const short v34_Tx_RCOS600_f2400[];                                              
extern const short v34_Rx_RCOS600_f1200[];                                              
extern const short v34_Rx_RCOS600_f2400[];                                              
extern const short v34_RCOS2400_f1800[];    
extern const short v34_RCOS3000_f1900[];
extern const short v34_RCOS3200_f1875[];
extern const short v34_RCOS3429_f1959[];
#endif /* VSIM */

	/**** transmitter functions ****/

EXTERN_LIBPORT_KEYWORD void Tx_init_APSK(struct START_PTRS *);
EXTERN_LIBPORT_KEYWORD short APSK_modulator(struct TX_BLOCK *);

	/**** receiver functions ****/

EXTERN_LIBPORT_KEYWORD void Rx_init_APSK(struct START_PTRS *);
EXTERN_LIBPORT_KEYWORD short APSK_demodulator(struct RX_BLOCK *);
EXTERN_LIBPORT_KEYWORD short no_slicer(struct RX_BLOCK *);
EXTERN_LIBPORT_KEYWORD short no_timing(struct RX_BLOCK *);
EXTERN_LIBPORT_KEYWORD short sgn_timing(struct RX_BLOCK *);
EXTERN_LIBPORT_KEYWORD short APSK_timing(struct RX_BLOCK *);
#if defined(ALTS_TIMING_ENABLE)
EXTERN_LIBPORT_KEYWORD short ALTs_timing_estimator(short, struct RX_BLOCK *);
#endif /* ALTS_TIMING_ENABLE */
EXTERN_LIBPORT_KEYWORD short coarse_timing(struct RX_BLOCK *);
EXTERN_LIBPORT_KEYWORD short no_decoder(struct RX_BLOCK *);
EXTERN_LIBPORT_KEYWORD short Rx_train_loops(struct RX_BLOCK *);
EXTERN_LIBPORT_KEYWORD short Rx_detect_EQ(struct RX_BLOCK *);
EXTERN_LIBPORT_KEYWORD void agc_gain_estimator(struct RX_BLOCK *);
EXTERN_LIBPORT_KEYWORD short Rx_fir_autocorrelator(struct RX_BLOCK *);
EXTERN_LIBPORT_KEYWORD short IIR_resonator(short, short*);
EXTERN_LIBPORT_KEYWORD short v21_inband_detector(struct RX_BLOCK *);
#if defined(VSIM)
extern void VSIM_FrequencyOffsetRotator(short *sample, struct RX_APSK_BLOCK *Rx);
#endif /* VSIM */

/****************************************************************************/

#endif /* APSK_INCLUSION_ */
