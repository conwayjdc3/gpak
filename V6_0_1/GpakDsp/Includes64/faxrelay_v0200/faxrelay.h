/*************************************************************************
* $RCSfile: relay.h,v $
* $Revision: 9.3 $
* $Date: 2010-06-23 14:32:12 $
* $Author: peter $
* Company: MESi
* Description: Top lelev relay interface header
*
* Copyright (C) MESi 1996-2010, all rights reserved.
***************************************************************************/
#if !defined(__RELAY__)
#define __RELAY__

#include "faxcommon.h"
#include "faxconfig.h"
#include "rxtx.h"
#include "apsk.h"
#include "sequence.h"
#include "modemif.h"
#include "bufmgr.h"
#include "t30.h"
#include "FaxStruct.h"


#define TONE1004HZ                  0x1000
#define V22BIS_MODEM                0x2200
#define V32BIS_MODEM                0x3200
#define V32_MODEM                   0x3201
#define V17_MODEM                   0x1700
#define V21_MODEM                   0x2100
#define V27_MODEM                   0x2700
#define V29_MODEM                   0x2900

#define SAMPLES_ONE_MS 				8

enum SYSTEM_FUNCTION {
	FAX_RELAY_FUNCTION, 
	FAX_INTERCEPT_FUNCTION, 
	IP_FAX_INTERCEPT_FUNCTION, 
	VOICE_RELAY_FUNCTION, 
	DATA_RELAY_FUNCTION, 
	BENT_PIPE_FUNCTION
	};

struct RlyInitChannelStruct {
    int Layer3;
    int RelayFunction;
#ifdef HOOK_SWITCH
    struct HookSwitchStruct *hookswitch;
#endif
    };

/****************************************************************************
* Function: RlyRelayIf
* Input:    in         input samples or NULL if buffer copy if not needed
*           out        output sample or NULL if buffer copy is not needed
*           Length     Number of samples to produce/consume per itteration
*           fax        Relay Structure pointer
*           correction additional samples needed for transmitter
*                      this is for windows soundcard aps only
* Returns : status
* Purpose : Interface from sample interface to the modems
* This functions in two distinct modes that serve the same function.
* Case 1.  A high level process gathers "Length" samples (8, 20, 80,etc)
*  and places them in a linear buffer "in" and calls the RlyRelayIf.  It then
*  copies the sample from "out" and delivers them to the codec.
* Case 2.  "in" and "out" are NULL pointers the calling function manages the
*  placement of samples in/out of the relay internal structures.
****************************************************************************/
extern int RlyRelayIf(short *in,  short *out, int FillLength, struct FAX_STRUCT *fax
#ifdef ASYNC_RX_TX
,  int correction
#endif
);

/****************************************************************************
 * Function  RlyPacketToRelay
 * Input:    f           Relay Structure pointer
 *           msgbuf      pointer to msg for relay
 *           length      number of bytes in buffer
 * Returns   1           Packet accepted
 *           0           Packet too large at present time, try again later
 *           -1          Packet too large for current buffer pool (fail)
 * Purpose   Attempt to send a packet to the relay
 ****************************************************************************/
extern int RlyPacketToRelay(struct FAX_STRUCT *f,unsigned char *msgbuf, int length);
#define PKT_ACCEPTED  				 1
#define PKT_NOT_ACCEPTED			 0
#define PKT_TOO_LARGE 				-1

/****************************************************************************
 * Function  RlyPacketFmRelay
 * Input:    f           Relay Structure pointer
 *           pMsgRet     point to array of chars where packet will be placed
 *           MaxLen      max size of packet
 * Returns   >0          length of packet stored at pMsgRet
 *           0           no packets available
 *           <0          Packet is greater than MaxLen.
 *                         Packet requires -return bytes of memory.
 * Purpose   Attempt to get a packet from the relay
 ****************************************************************************/
extern int RlyPacketFmRelay(struct FAX_STRUCT *f,unsigned char *pMsgRet,int MaxLen);
#define NO_PKT_AVAIL 				0

/****************************************************************************
 * Function  RlyRelayInitChannel
 * Input:    mem       address of channel memory
 *           n         size of mem
 *           p         pointer to channel init parameters
 * Returns   FAX_STRUCT pointer to FAX_STRUCT
 * Purpose   Initializes channel memory
 ****************************************************************************/
extern struct FAX_STRUCT* RlyRelayInitChannel(unsigned char* mem,int n,struct RlyInitChannelStruct *p);

/****************************************************************************
 * Macro:    RLY_CHANNEL_SIZE
 * Purpose   Estimates channel memory size based on #define NSECTIONS
 *           and is used for static memory models
 *           (e.g.) unsigned char FaxMem[2 ][RLY_CHANNEL_SIZE];
 *           would allocate two fax channels
 ****************************************************************************/
#if defined(CIRC_MEMORY_ALIGNMENT)
/*
 * The "modem memory"  = 6*128+80 is hard-wired for the C5400 CIRC alignment requirements.
 * DON'T touch this until you can verify the requirements on a C54xx target for correct 
 * operation!! (PBM 04-14-2006)
 */
#define RLY_CHANNEL_SIZE \
		((6*128+80)*sizeof(short) + /* modem memory */ \
		sizeof(struct T30_PROTOCOL_STRUCT) + \
		sizeof(struct SEQ_STRUCT) + \
		BmrGetSize(NSECTIONS))
#else /* CIRC_MEMORY_ALIGNMENT */
#define FAX_MEM_ALIGNMENT 				(8-1)	// align to 8 byte boundaries
#define RLY_CHANNEL_SIZE \
		((sizeof(struct FAX_STRUCT)+FAX_MEM_ALIGNMENT)&~FAX_MEM_ALIGNMENT) + \
		((START_PTRS_LEN*sizeof(short)+FAX_MEM_ALIGNMENT)&~FAX_MEM_ALIGNMENT) + \
		((RX_BLOCK_LEN*sizeof(short)+FAX_MEM_ALIGNMENT)&~FAX_MEM_ALIGNMENT) + \
		((RX_FIR_LEN*sizeof(short)+FAX_MEM_ALIGNMENT)&~FAX_MEM_ALIGNMENT) + \
		((EQ_COEF_LEN*sizeof(short)+FAX_MEM_ALIGNMENT)&~FAX_MEM_ALIGNMENT) + \
		((EC_COEF_LEN*sizeof(short)+FAX_MEM_ALIGNMENT)&~FAX_MEM_ALIGNMENT) + \
		((ENCODER_BLOCK_LEN*sizeof(short)+FAX_MEM_ALIGNMENT)&~FAX_MEM_ALIGNMENT) + \
		((TRACEBACK_LEN*sizeof(short)+FAX_MEM_ALIGNMENT)&~FAX_MEM_ALIGNMENT) + \
		((RX_SAMPLE_LEN*sizeof(short)+FAX_MEM_ALIGNMENT)&~FAX_MEM_ALIGNMENT) + \
		((DECODER_BLOCK_LEN*sizeof(short)+FAX_MEM_ALIGNMENT)&~FAX_MEM_ALIGNMENT) + \
		((TX_BLOCK_LEN*sizeof(short)+FAX_MEM_ALIGNMENT)&~FAX_MEM_ALIGNMENT) + \
		((TX_SAMPLE_LEN*sizeof(short)+FAX_MEM_ALIGNMENT)&~FAX_MEM_ALIGNMENT) + \
		((TX_DATA_LEN*sizeof(short)+FAX_MEM_ALIGNMENT)&~FAX_MEM_ALIGNMENT) + \
		((TX_FIR_LEN*sizeof(short)+FAX_MEM_ALIGNMENT)&~FAX_MEM_ALIGNMENT) + \
		((RX_DATA_LEN*sizeof(short)+FAX_MEM_ALIGNMENT)&~FAX_MEM_ALIGNMENT) + \
		((sizeof(struct MIF_STRUCT)+FAX_MEM_ALIGNMENT)&~FAX_MEM_ALIGNMENT) + \
		((sizeof(struct SEQ_STRUCT)+FAX_MEM_ALIGNMENT)&~FAX_MEM_ALIGNMENT) + \
		((sizeof(struct T30_PROTOCOL_STRUCT)+FAX_MEM_ALIGNMENT)&~FAX_MEM_ALIGNMENT) + \
        BmrGetSize(NSECTIONS)
#endif /* CIRC_MEMORY_ALIGNMENT */
/*****************************************************************************/
#endif /* __RELAY__ */

