#ifndef _G168_LMS_PRIV_H
#define _G168_LMS_PRIV_H

#define G165_DETECT_ENABLE
#define NB_TONE_DETECT_ENABLE
#define CONVERGENCE_MONITOR_ENABLE 1

#define T30_SIG_DETECT

#ifdef G165_DETECT_ENABLE
#include "g165det.h"
#endif

#include "g168_user.h"

#ifdef DEMO_RELEASE
#ifndef MAX_DEMO_TIME
#define MAX_DEMO_TIME 7200000
#endif
#endif

#define FOUR_WIRE_ERL_T1 360	//36 dB. At ERL higher than this, we consider it a 4-wire line (no echo)
#define FOUR_WIRE_ERL_T2 180	//12 dB. At ERL lower than this, we consider it 2-wire line

#define FOUR_WIRE_PAR 150	//15 dB. In the ERL gray zone above, if PAR is less than this,
							// we declare a 4-wire line
#define FOUR_WIRE_ERLE 90	//9 dB. If ERLE is in the gray zone above and ERLE is less than this, 
							//we declare a 4-wire line
#define NLP_SUPP_TARGET 600	//60 dB


typedef struct 
{
	ADT_Int32 Ly;
	ADT_Int16 *FarFIRState;		//[FIR_SIZE];
	ADT_Int16 *NearFIRState;	//[FIR_SIZE];
	ADT_Int16 *EchoPath;		//[MAX_TAP_LENGTH_D+1];
	ADT_Int16 *iabsyHist;		//[MAX_IABSY_SIZE];
	ADT_Int16 *Coef;			//[MAX_TAP_LENGTH_D];
	ADT_Int16 Output;
	ADT_Int16 Modulo;
} BGChannel_t;

typedef struct
{
	Int16 STDCSum;
	Int16 LTDCSum;
	Int16 DC;
	Int16 DCSampleCount;
}	DC_t;


#define DT_STATE_NONE 0
#define DT_STATE_MAYBE 1
#define DT_STATE_SOFT 2
#define DT_STATE_HARD 3

typedef struct
{
	ADT_Int32 	Corr0;
	
	ADT_Int16 	LocalDTGrayFlag;
	ADT_Int16	j;
	ADT_Int16	LastUpdateSample;
	ADT_Int16 	AdaptCount;
	ADT_Int16 	update;
	ADT_Int16	abss;
	
	ADT_Int16	StepForeBuff[6];
	ADT_Int16 	UpdateBuffer[20];
	ADT_Int16 	RefCoef[N_CORR];
	ADT_Int16   CurrentDTState;
	ADT_Int16   MaxDTState;
	ADT_Int16 	CrossCorrCount;  
#ifndef SHORT_TAIL
	ADT_Int32 	StepBuff[64];
	ADT_Int16 	NearDecSampleArray[20];  
#endif

	ADT_Int16 NearCopy[2*MAX_FRAME_SIZE];
#ifndef SHORT_TAIL
	//ADT_Int16 Dummy[2];    // for alignment must change the init() too

	ADT_Int16 ExpHistogram[64];
	ADT_Int16 Seg16Flags[MAX_TAP_LENGTH/16];
	ADT_Int32 Seg32Flags[MAX_TAP_LENGTH/16];
	//ADT_Int32 PNLMS_Peak16[MAX_TAP_LENGTH/16];
#endif
	DTStruct_t DTStruct;

}		G168PRIV_Scratch_t;

typedef struct
{

	#ifdef G165_DETECT_ENABLE
		ADT_Int32 G165DetFarChannel[14];
		ADT_Int32 G165DetNearChannel[14];
	#endif
 
	ADT_Int32 LuHistory[16];

	DTStruct_t DTStruct; 
	Int32 M2[MAX_TAP_LENGTH/16+1];
	ADT_Int16 Dummy_for_M; // ensure that M below has right alignment
	ADT_Int16 M[MAX_TAP_LENGTH/16+1];

	#ifndef SHORT_TAIL
		ADT_Int16 iabsyHist[MAX_TAP_LENGTH/16];
		ADT_Int16 Coef[MAX_FIR_SEGMENTS*MAX_FIR_SEGMENT_LENGTH+7];

	 	ADT_Int16 BG_iabsyHist[(BG_IABSY_LENGTH + 7)]; //& (unsigned long)0xfffffff8];
		ADT_Int16 Dummy2[7];
		ADT_Int16 BG_FarFIRState[FIR_SIZE];
		ADT_Int16 Dummy[4];
		ADT_Int16 BG_NearFIRState[FIR_SIZE];
	#endif
	
}		G168PRIV_SA_State_t;

//-----------------------------------------------
// The G168Channel structure contains all the per-channel (per-instance)
// persistant variables and arrays.
//-----------------------------------------------
typedef struct	//G168Channel_t
{
	G168Params_t Params;
//-----------------------------------------------
// Internal Variables (long)
//-----------------------------------------------
	ADT_Int32 y;		/* near end power state for dc-block filter) */
	ADT_Int32 Lu; 	/* error power state (absu0 input)*/
	ADT_Int32 Ly;	/* far power state (absy0+cutoff) input)*/
	ADT_Int32 s_hat; /* near power state (abss0 input) fordoubletalk det. */
	ADT_Int32 sin_hat;
	ADT_Int32 y_hat;	/* far power state (absy0 input) for doubletalk det. */
	ADT_Int32 NearBackgroundNoiseLevel; /* near end background noise level (for CNG) */
	ADT_Int32 *LuHistory_p; /* Past values of Lu */

//-----------------------------------------------
// Control variables 
//-----------------------------------------------
	ADT_Int16 hangt;				/* time out before double talk can go inactive set to 75 mesec0 */
									/* This means if near end signal is greater than 1/2 the far end */
									/* then we have double talk */
//-----------------------------------------------
// Internal variables
//-----------------------------------------------
	ADT_Int16 H;					/* Circular counter */
	ADT_Int16 hcntr;				/* echo canceeler variable when non-zero there is doubletalk */
	ADT_Int16 NUpdatesPerSample;	// TapLength/N_PHASES

	ADT_Int16 MaxM;					/* Keeps track of far end power for purpose
									   of double-talk detection */
	//ADT_Int16 xold;					/* Previous near end sample for DC Blocking Filter */
	ADT_Int16 NLPThres;				/* NLP Threshold */
	ADT_Int16 iabsy;				/* Estimate of reciprocal of far end power */
	ADT_Int16 Update;				/* If set, adapt coefficients Else, don't */
									/* This differs from AdaptEnable in that it is
									   controlled within the EC */
	ADT_Int16 PrevBGUpdate;			/* Previous value of Update */
	ADT_Int16 Output;				/* Cancelled Near Signal (before NLP) */
	ADT_Int16 LMS_BGOutput;			/* Cancelled Near Signal using background coeffients */
	ADT_Int16 *Coef_p;				/* FIR Coefficients */
	ADT_Int16 *Coef_Smart_p;		/* Compressed FIR Coef, the length only NF*FS */
	ADT_Int16 *EchoPath_p;			/* Echo Path array poADT_Inter */
	ADT_Int16 *EchoPathStart_p;		/* Start of echo path array */
	ADT_Int16 *M_p;					/* Partial Maximum Array */
	ADT_Int32 *Peak16_p;			/* Points to Peak16 array (FindPeakTaps) */
	ADT_Int16 *Seg16Flags_p;		/* Points to Seg16Flags array (FindPeakTaps) */
	//ADT_Int16 *FG_PNLMS_Shift_p;		/* Points to FG_PNLMS_Shift[] */
	//ADT_Int16 *BG_PNLMS_Shift_p;		/* Points to BG_PNLMS_Shift[] */
	//ADT_Int32 *PNLMS_Peak16_p;		/* Points to PNLMS_Peak16 */
	ADT_Int16 *NearCopy_p;			/* Points to scratch.nearcopy */
	ADT_Int16 *ExpHistogram_p;		/* Pointer to FPT Exp aaray in scratch memory */

	ADT_Int16 LMS_StepAdjust;		/* right shift count for adjusting background LMS step size */
#ifndef SHORT_TAIL
	ADT_Int16 BGLMS_StepAdjust;		/* right shift count for adjusting LMS step size */
	ADT_Int16 CurrentBGLMS_StepAdjust; /* current value of BGLMS_StepAdjust */
	ADT_Int16 BGOutputValid;		/* if BG_LMS executed previously, setup this flag to 1, else clear to 0 */
#endif
    ADT_Int16 CurrentLMS_StepAdjust; /* current value of LMS_StepAdjust */
	ADT_Int16 CurrentNLPThreshold;	/* Current NLP Threshold */

	ADT_Int16 CurrentAdaptLimit;
	ADT_Int16 CNGLevel;				/* The current CNG signal level (scale) */
	ADT_Int16 CNGSampleCount;		/* Modulo counter to decide when to update CNG Level */
	ADT_Int16 CNGState;				/* Current state of CNG (states defined above in this file */
	ADT_Int16 CNGRampCount;			/* Counts the ramp down of the CNG */
	ADT_Int16 SSCount;				/* Signalling Tone Counter */

//-----------------------------------------------
// Variables and arrays used by the stationary signal detector
//-----------------------------------------------
#ifdef NB_TONE_DETECT_ENABLE
	ADT_Int32 Corr[N_CORR];			/* Autocorrelator output */
	ADT_Int32 PastCorr[N_CORR];		/* Previous cycle's cross-correlator output */
	ADT_Int16 CorrSampleCount;		/* Cross Correlation cycle counter */
	ADT_Int16 MeanRefCoef[N_CORR];	/* Mean value of reflection coefficients */
	ADT_Int16 VarianceRefCoef[N_CORR]; /* Variance of reflection coefficients */
	ADT_Int16 NBToneStat;			/* 0 = no NB, 1 = some NB, 2 = strong NB */
	ADT_Int16 NBFrameCount;
#endif
//-----------------------------------------------
// Variables and arrays used by the background LMS
//-----------------------------------------------
	
#ifndef SHORT_TAIL
	ADT_Int16 NTapUpdateBlocks;			/* Number of segments in the cross-correlation
										   algorithm */
	ADT_Int16 TapUpdateBlockSize;		/* Size of each cross-correlation segment */
	ADT_Int16 PeakCheckSampleCount;	/* Counter to determine when to check for FIR peak */
	ADT_Int16 *iabsyHist_p;			/*Past values of iabsy */ 
	SmartFIRData_t SmartFIRData[3];	/* Smart FIR Stuff */
#else
	SmartFIRData_t SmartFIRData[1];	/* Smart FIR Stuff */
#endif

//-----------------------------------------------
//  Hoth Noise Generator State Array
//-----------------------------------------------
	ADT_Int16 HState[3];
//-----------------------------------------------
// Variables and arrays used by the Convergence Check function
//-----------------------------------------------

//	ADT_Int16 PastCoef[MAX_TAP_LENGTH];	/* Coefficients from last frame during 
//										   which coefficients were updated */
	ADT_Int16 ConvergenceStat;			/* Indicates degree of convergence of FIR
										   Lower is more converged */
	ADT_Int16 FarAvg; //hfz_change_2
	ADT_Int16 NearAvg;
	ADT_Int16 ConvergenceFlag;			/* If set, FIR is well converged */

	ADT_Int16 DTGrayFlag;				/* If set, double-talk detector is in gray region */

	ADT_Int16 DTState;					/* If set, double-talk state. 0 = no double-talk, 1 = possible double-talk, 2 = soft double-talk, 3 = hard double-talk */

	DTStruct_t *DTStruct_p;				/* Restore information after double-talk detection */
	ADT_Int16 *DTCoef_p;				/* Points to saved coefficient arrays */

	DTStruct_t *DTScratchStruct_p;
	
	ADT_Int16 SoftNLPScale;			/* scale factor for Soft NLP */
	ADT_Int16 SoftNLPScaleTarget;	/* Target scale factor for Soft NLP */

	DC_t NearDC;
	
	BGChannel_t BGChannel;

#ifdef G165_DETECT_ENABLE
	G165Channel_t *G165DetNearChannel_p;
	G165Channel_t *G165DetFarChannel_p;
	ADT_Int16 G165FlagLatched;
#endif

	ADT_Int16 *IabsyHistForeCircPtr;
	ADT_Int16 *MCircPtr;
	ADT_UInt32 PeakAmp0;
    ADT_Int16 PeakCheckCount;	// reduce mips for multiple channel
    ADT_Int16 PeakCheckTime;	// reduce mips for multiple channel
	ADT_Int32 Lu0;
	G168PRIV_Scratch_t *ScratchPtr;
	ADT_UInt32 CMjsq;             /* Timer counter for Demo purpose 
								so user can't run the cancel funtion 
								more than max times ADT defined */
	ADT_Int32 PrevLu; /* Lu of previous frame, for CNGLevel computation */
	ADT_Int16  seedd;
    ADT_Int16 NLPMaxSuppScale;		// NLP supress scale,limit on scale the NLP will suppress

	DC_t FarDC;

#ifdef V10_HIGH_ERL
	Int32 *M2_p;				/* Partial Max Power Array */
	Int32 MaxM2;				/* Max(M2) */
	Int32 *M2_CircPtr;
#endif
#ifdef T30_SIG_DETECT
	Int16 T30Flag;				/* Indicates T.30 V21-modulated flags signal present */
#endif

#ifdef V10_HIGH_ERL
	Int16 ERL1;		//ERL computed using power of foreground coefficients
	Int16 ERL2;		//ERL computed using RInPower and SInPower
	Int16 ERL;		//Best estimate of ERL
	Int16 WorstRecentERL2;
	Int16 RInPowerdBm;	//for debug
#ifdef SHORT_TAIL
	Int16 ERLAdaptSampleCount;
#endif
	Int16 ERLE;
	Int16 EnergySampleCount;
	Int16 ERL2SampleCount;
	Int16 FourWireState;
	Int16 CoefQ;
	Int16 OldCoefQ;
	Int16 BGCoefQ;
	Int16 OldBGCoefQ;
	Int16 NearInPowerdBm;
	Int16 ResidualPowerdBm;
	Int16 ReconvPendSampleCount;
	Int32 CNGResidualPower;		// Residual energy computed for the purposes of CNG
	Int32 CNGHothNoisePower;	//
	Int40 NearInEnergy;
	Int40 ResidualEnergy;
#endif

}		G168Channel_t;

//-----------------------------------------------
// Function Prototypes
//-----------------------------------------------

void LEC_ADT_g165Detect_ASM(G168Channel_t *Channel,ADT_Int16 NearEnd[],ADT_Int16 FarEnd[],  ADT_Int16 *LocalBypassEnable);
#endif //#ifdef _G168_LMS_PRIV_H
