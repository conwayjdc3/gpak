#ifndef ADT_GAIN_SUM_H
#define ADT_GAIN_SUM_H

#include "adt_typedef.h"
#define ADT_GAIN_MUTE 0x8000  // use this gain value to enable gainblock muting

void ADT_SumLinear(
	ADT_Int16 *pInput1,		// pointer to Input buffer 1
	ADT_Int16 *pInput2,		// pointer to Input buffer 2
	ADT_Int16 *pOutput,		// pointer to Output buffer
	ADT_Int16 Length		// Length of Array to be processed
	);

void ADT_GainBlock(
	ADT_Int16 GainValue,	// gain value (units of .1 dBm)
	ADT_Int16 *pInput,		// pointer to Input buffer
	ADT_Int16 *pOutput,		// pointer to Output buffer
	ADT_Int16 Length		// Length of Array to be processed
	);
	
#endif //#ifndef ADT_GAIN_SUM_H

