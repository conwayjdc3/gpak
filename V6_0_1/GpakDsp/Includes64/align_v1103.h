#ifndef _ALIGN_H
#define _ALIGN_H
#include "adt_typedef.h"
// Functions/Macros to help with array and structure alignment

// Define padding sizes and address masks
//   NBITS is the number of bits per word
//   Y is the required byte alignment
// PAD is the number of IntX elements that need to be padded to the array
// MASK is the address Mask to help compute an aligned address

// BYTE_ALIGN_COUNT is set on a platform-by-platform basis. This value specifies the byte boundary alignment
//  that is required to make use of aligned wide word loads and stores.
#ifndef _TMS320C6X
#ifdef SSE2
#define ALIGN_DATA
// For now, set the BYTE_ALIGN_COUNT to 1 because GCC doesn't support the 128-bit aligned load/store intrinsics
//   (FYI: MS C and intel ICC both do support it)
//#define BYTE_ALIGN_COUNT 16
#define BYTE_ALIGN_COUNT 1
#else	// default at byte align count of 16, just to be safe
 #define BYTE_ALIGN_COUNT 16
#endif
#endif
#ifdef _TMS320C6X
#define ALIGN_DATA
#define BYTE_ALIGN_COUNT 8
#endif

#define MAX_PAD(NBITS,BYTE_ALIGN) (BYTE_ALIGN - 1)
#define MASK(NBITS,BYTE_ALIGN) (0xffffffffffffffff ^ MAX_PAD(NBITS,BYTE_ALIGN))
#define LENGTH(L,NBITS,BYTE_ALIGN) (L+MAX_PAD(NBITS,BYTE_ALIGN))
#define PTR_MASK(BYTE_ALIGN) (0xffffffffffffffff ^ (BYTE_ALIGN-1))

#ifdef ALIGN_DATA
#define PTR_ALIGN(P, BYTE_ALIGN) ((void *) ( (((ADT_Int64) P) + BYTE_ALIGN-1) & PTR_MASK(BYTE_ALIGN)) )
#else
#define PTR_ALIGN(P, BYTE_ALIGN) P
#endif
#ifdef ALIGN_DATA
#define ADT_Int16Aligned(_array, _length, _align_bytes) ADT_Int16 _array[LENGTH(_length, 16, _align_bytes)]
#define ADT_Int32Aligned(_array, _length, _align_bytes) ADT_Int32 _array[LENGTH(_length, 32, _align_bytes)]
#else
#define ADT_Int16Aligned(_array, _length, _align_bytes) ADT_Int16 _array[_length]
#define ADT_Int32Aligned(_array, _length, _align_bytes) ADT_Int32 _array[_length]
#endif

// Sample usage
//  ADT_Int16Aligned(a, 10, 8)  // declare a short int array "a" of length 10 for use with 8 byte alignment
//  short int *pa_align8 = PTR_ALIGN(a, 8)

// Specific byte aligns

#endif
