#ifndef _TI_T38_USER_H
#define _TI_T38_USER_H

#include "adt_typedef.h"

//---------------------------------------------------------------------------------
// SF_DETECTOR
//
#define SFDET_SOFT_DECISION 1   // Required for Fax Detect


#define N_CORR 3
typedef struct  SF_Channel {
   ADT_Int16 PastIn[N_CORR+1];
   ADT_Int32 Corr[N_CORR];         /* Autocorrelator output */
   ADT_Int32 PastCorr[N_CORR];      /* Previous cycle's cross-correlator output */
   ADT_Int16 CorrSampleCount;      /* Cross Correlation cycle counter */
   ADT_Int16 MeanRefCoef[N_CORR];   /* Mean value of reflection coefficients */
   ADT_Int16 VarianceRefCoef[N_CORR]; /* Variance of reflection coefficients */
   ADT_Int16 NBToneStat;         /* 0 = no NB, 1 = some NB, 2 = strong NB */
   ADT_Int16 Frequency;
   ADT_Int16 NBFrameCount;
#ifdef SFDET_SOFT_DECISION
   ADT_Int16 RefCoefDelta[2];
   ADT_Int32 Corr0;
#endif
#ifdef SF_DEBUG
   ADT_Int16 RefCoef[N_CORR];
#endif
}   SFChannel_t;
void SFDET_ADT_init   (SFChannel_t *pChannel);
void SFDET_ADT_detect (SFChannel_t *pSFChannel, ADT_Int16 *pIn, ADT_Int16 Length);


//---------------------------------------------------------------------------------
// FAX_DETECTOR
//

//Fax detector result codes
#define FDET_NULL 0   //No fax signal detected
#define FDET_CNG 1    //Fax CNG signal detected
#define FDET_CED 2    //Fax CED signal detected
#define FDET_V21 3    //Fax V.21 modulation detected
#define FDET_V21_L FDET_NULL  // Used as frequency deviation index
#define N_TONE_TYPES 3

// NOTE: This maps Fax detector tone values (above) into ti-T38 tone values
//       and into string values.

#ifdef _IFFAX_H_
#define iffax_UNK ((iffaxToneID_e) -1)
#define TIToneMapping  iffax_UNK,   iffax_TONE_CNG, iffax_TONE_CED,  iffax_UNK, 
#endif
#define toneStrMapping  "NONE",     "CNG",          "CED",           "V21"


//Channel Initialization Parameters
typedef struct FaxDetectParams {
   ADT_Int16 MaxFreqDevPctTimes10;   //Maximum frequency deviation, in percentage*10
   ADT_Int16 MinLeveldBm;         //Minimum signal level, in dBm
   ADT_Int16 MinDuration;         //Minimum tone duration, in samples
}      FaxDetectParams_t;

typedef struct FaxDetectInst {
   ADT_Int16 MaxFreqDev[N_TONE_TYPES+1];
   ADT_Int32 MinCorr0;
   ADT_Int16 IntermediateTone;     // Last tone detected
   ADT_Int16 CurrentTone;          // Last tone reported
   ADT_Int16 Duration;
   ADT_Int16 MinDuration;
   SFChannel_t SFChannel;

}   FaxDetect_t;

void FDET_ADT_init   (FaxDetect_t *pFDetChannel, FaxDetectParams_t *pParams);
int  FDET_ADT_detect (FaxDetect_t *pFDetChannel, ADT_Int16 *pSignal, ADT_Int16 Length, ADT_Int16 *Result);


//---------------------------------------------------------------------------------
// TI_T38 Data Structures
//
typedef struct TI_T38 {
   int  channelId;
   FaxDetect_t toneInst;
   void *fmInst;
   void *fiuInst;
} TI_T38;

// G.PAK configuration variables
extern TI_T38 TI_Fax[];
extern ADT_UInt64 fmInst [];
extern ADT_UInt64 fiuInst[];
extern const ADT_UInt32 fmInstI8;
extern const ADT_UInt32 fiuInstI8;

char *InitTIRelayStructures (int numFaxChans);
char *OpenTIRelayInst       (void *fiuInst, void *fmInst, void *chan, int rtpPayType);
void ActivateTIRelayInst    (void *fiuInst, void *fmInst);
void CloseTIRelayInst       (void *fiuInst, void *fmInst, int *TxPkts, int *RxPkts);

#endif
