

#ifndef _SPEEX_USER_H
#define _SPEEX_USER_H

#include "adt_typedef.h"
/* Defines */

/** modeID for the defined narrowband mode */
/* Mode IDs */
#define SPEEX_MODEID_NB 0	/*Narrowband Mode*/
#define SPEEX_MODEID_WB 1	/*Wideband Mode */
#define SPEEX_MODEID_UWB 2	/*Ultrawideband Mode */


typedef struct 
{
	ADT_UInt32	ModeID;				//SPEEX_MODEID_NB (narrowband), SPPEX_MODEID_WB(wideband), SPEEX_MODEID_UWB(ultrawideband)
	ADT_UInt32	DecodeEnhancementEnable; //Decoder speech enhancement enable
	ADT_UInt32	FrameSize;			//Read Only
	ADT_UInt32	Quality;			//Write Only (0..10)
	ADT_UInt32	LBSubMode;			//Low-band bit rate (wideband only)
	ADT_UInt32	HBSubMode;			//High-band bit rate (wideband only)
	ADT_UInt32	Complexity;			//1..10 with 10 most complex (highest CPU utilization)

	ADT_UInt32 SamplingRate;		//Actual sampling rate to be used for bit-rate computation

	/* VBR stuff */
	ADT_UInt32	VBREnable;			//Variable bit rate enable
	float		VBRQuality;			//Variable bit rate quality (0-10)


	/* CBR Stuff */
	ADT_UInt32	EncodeBitRate;		//Specify maximum encoder bit rate for CBR (set to zero if you want VBR mode)

	/* ABR */
	ADT_UInt32	ABREnable;			//Average Bit Rate Enable
	ADT_UInt32	AverageBitRate;		//Average bit rate, in bits per second

	ADT_UInt32	DecodeBitRate;		//Decoder bit rate - read only
	void 		*pfSpeexInBandHandler;	//define a handler function for in-band Speex requests
	void		*pfUserInBandHandler;	//define a handler function for in-band user requests
	ADT_UInt32	VADEnable;			//VAD enable
	ADT_UInt32	DTXEnable;			//DTX Enable
	ADT_UInt32	ExpectedPacketLossRate;	//Expected packet loss rate, expressed as a percentage
	ADT_UInt32	MaxVBRBitRate;		//Maximum bit rate in variable bit rate mode
	ADT_UInt32	HPFEnable;			//Highpass Filter Enable
	ADT_UInt32	MajorVersion;		//Version number information - read only
	ADT_UInt32	MinorVersion;
	ADT_UInt32	MicroVersion;
	ADT_UInt32	ExtraVersion;

} SpeexControlAndStatus_t;

#ifndef SPEEX_MAX_MODE
# define SPEEX_MAX_MODE 2
#endif
#define INT8_TO_INT64(x) ((x+16)/8)
#define SPEEX_BUFF_SPACE 1024
#if SPEEX_MAX_MODE == SPEEX_MODEID_NB
# define SPEEX_ENC_STATE_SIZE_64 INT8_TO_INT64(5437+SPEEX_BUFF_SPACE)
# define SPEEX_DEC_STATE_SIZE_64 INT8_TO_INT64(4749+SPEEX_BUFF_SPACE)
#elif SPEEX_MAX_MODE == SPEEX_MODEID_WB
# define SPEEX_ENC_STATE_SIZE_64 INT8_TO_INT64(6685+SPEEX_BUFF_SPACE)
# define SPEEX_DEC_STATE_SIZE_64 INT8_TO_INT64(5816+SPEEX_BUFF_SPACE)
#else
# define SPEEX_ENC_STATE_SIZE_64 INT8_TO_INT64(8093+SPEEX_BUFF_SPACE)
# define SPEEX_DEC_STATE_SIZE_64 INT8_TO_INT64(7043+SPEEX_BUFF_SPACE)
#endif
# define SPEEX_ENC_SCRATCH_SIZE_64 INT8_TO_INT64(32080)
# define SPEEX_DEC_SCRATCH_SIZE_64 INT8_TO_INT64(16080)



typedef ADT_UInt64 SpeexEncState_t[SPEEX_ENC_STATE_SIZE_64];
typedef ADT_UInt64 SpeexEncScratch_t[SPEEX_ENC_SCRATCH_SIZE_64];
typedef ADT_UInt64 SpeexDecState_t[SPEEX_DEC_STATE_SIZE_64];
typedef ADT_UInt64 SpeexDecScratch_t[SPEEX_DEC_SCRATCH_SIZE_64];

typedef void *SpeexEncodeHandle_t;
typedef void *SpeexDecodeHandle_t;
/* SPEEX_ADT_initMemory result codes */

#define SPEEX_ADT_INIT_MEM_OK 0
#define SPEEX_ADT_INIT_MEM_STATESIZE_ERROR 1
#define SPEEX_ADT_INIT_MEM_SCRATCHSIZE_ERROR 2


#define SPEEX_ENCINIT_OK 0
#define SPEEX_ENCINIT_FAIL 1
#define SPEEX_ENCINIT_WARNING_BIT_RATE_OVERRIDING_QUALITY 2
#define SPEEX_ENCINIT_WARNING_DTX_WITHOUT_VAD_VBR_OR_ABR 3


SpeexEncodeHandle_t SPEEX_ADT_initEncoder(
	SpeexEncState_t *pSpeexEncState, 
	ADT_UInt32 StateSizeInBytes, 
	SpeexEncScratch_t *pEncScratch, 
	ADT_UInt32 ScratchSizeInBytes,
	SpeexControlAndStatus_t *pControl, 
	ADT_UInt8 *pResult);

#define SPEEX_DECINIT_OK 0
#define SPEEX_DECINIT_FAIL 1

SpeexDecodeHandle_t SPEEX_ADT_initDecoder(
	SpeexDecState_t *pSpeexDecState, 
	ADT_UInt32 StateSizeInBytes, 
	SpeexDecScratch_t *pDecScratch, 
	ADT_UInt32 ScratchSizeInBytes,
	SpeexControlAndStatus_t *pControl, 
	ADT_UInt8 *pResult);


int SPEEX_ADT_encode(
	SpeexEncodeHandle_t EncodeHandle, 
	ADT_Int16 *pInputPCM, ADT_UInt16 NInputSamples, 
	ADT_UInt8 *pCompressedBytes, ADT_UInt16 *pNOutputBits);

int SPEEX_ADT_decode(
	SpeexDecodeHandle_t DecodeHandle,
	ADT_UInt8 *pCompressedBytes, ADT_UInt16 NInputBits, 
	ADT_Int16 *pOutputPCM, ADT_UInt16 *pNOutputSamples);

void SPEEX_ADT_deleteEncoder(SpeexEncodeHandle_t Handle);
void SPEEX_ADT_deleteDecoder(SpeexDecodeHandle_t Handle);

void SPEEX_ADT_getStatus(SpeexEncodeHandle_t *pEncodeHandle, SpeexDecodeHandle_t *pDecodeHandle, SpeexControlAndStatus_t *pStatus);


#endif //_SPEEX_USER_H
