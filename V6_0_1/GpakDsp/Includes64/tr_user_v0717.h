#ifndef _TR_USER_H
#define _TR_USER_H

#include <common/include/adt_typedef_user.h>
#include "tonedet_user_v0716.h"

#define TR_API_VERSION 701	/* Tone Relay API Version */

#define TR_NO_TONE_DETECTED 100     // return tone index value for no detection

// ============== Structure element definitions ==============
//
// ToneIndex -  output from detector, input to generator that specifies the tone 
//              pair. 
//              0-15...dtmf
//              20-34...mfr1
//              40-54...mfr2 forward
//              60-74...mfr2 backward
//              80-84...call progress
//
// Amplitude -      output from detector, input to generator. Positive value 
//                  specifying amplitude in minus dB (relative to full scale)
//
// TimeElapsed -    output from detector, input to generator. Elapsed time 
//                  of tone in numsamples (at 8000 samples/sec)
//
// UseIndex -       input flag to generator and detector indicating whether it 
//                  should use a tone index or frequency pairs to specify the
//                  tone.
//
// ToneFrequency[] - input to generator, output from detector  that is only 
//                  valid when UseIndex is 0 specifies the tone frequencies.
//                     
// TimeStamp -		input to generator, time stamp in units of samples (8000 samples/sec).
//
// LastFrameFlag -	input to generator, set to 1 when the last tone packet has been signalled via receive
//					RTP packet

/* Tone Relay Detect results structure */

typedef struct
{
	ADT_Int8	ToneIndex;
	ADT_Int8	Amplitude;
	ADT_UInt16	TimeElapsed;
}		TRDetectResults_t;

/* Tone Relay Generate event structure */

typedef struct 
{
	ADT_UInt32	TimeStamp;
	ADT_UInt8	ToneIndex;
	ADT_UInt8	Amplitude;
	ADT_UInt16	TimeElapsed;
	ADT_Int16	ToneFrequency[2];
	ADT_UInt8	UseIndex;
	ADT_UInt8	LastFrameFlag;
	ADT_UInt8	UpdateFlag;		//Used internally to indicate that this event is an update of the previous event
   ADT_UInt8   StartBit;       // Set if start of tone, (e.g., MBit is set)
   ADT_UInt16  SampleRate;
}		TRGenerateEvent_t;


#define TRD_SIZE_IN_BYTES 358
#define TRG_SIZE_IN_BYTES 78

/* Tone Relay Detect and Generate Instance Structures */

typedef struct
{
	ADT_Int64 Space[(TRD_SIZE_IN_BYTES+7)/sizeof(ADT_Int64)];
}		TRDetectInstance_t;

typedef struct
{
	ADT_Int64 Space[(TRG_SIZE_IN_BYTES+7)/sizeof(ADT_Int64)];
}		TRGenerateInstance_t;

/* Tone Relay Detect and Generate initialization parameters structures */

typedef struct 
{
	ADT_UInt16		APIVersion;
//	TRDetectInstance_t *pTRGDetInstance;
	ADT_UInt16		TRDetInstanceSizeInBytes;	//size of host-program-allocated tone detector instance structure (pointed to by pTRDetInstance)
	TDInstance_t	*pDTMFDetInstance;			//pointer to DTMF detector instance, instantiated by host
	TDInstance_t	*pMFR1DetInstance;			//pointer to MFR1 detector instance, instantiated by host			
	TDInstance_t	*pMFR2FDetInstance;			//pointer to MFR2-F detector instance, instantiated by host
	TDInstance_t	*pMFR2RDetInstance;			//pointer to MFR2-R detector instance, instantiated by host
	TDInstance_t	*pCPRGDetInstance;			//pointer to CPRG detector instance, instantiated by host
	ADT_Int16		*pSuppressDelayBuffer;		//delay buffer, used to delay signal in order to suppress the onset of a tone before it is detected
												// Set pointer to NULL(0) for no delay
	ADT_Int16		SuppressDelaySizeInSamples;	//Size of suppress delay buffer in samples.
	// DTMF Detector configuration - see tonedet_user.h or tone detector users guide
	DTMF_Config_Param_t *pDTMFConfigNominal;	//Optional DTMF detector nominal configuration - set to zero to use default settings
	DTMF_Config_Param_t *pDTMFConfigWaiver;		//Optional DTMF detector waiver configuration - set to zero to use default settings
	// DTMF Suppressor configuration - see tonedet_user.h or tone detector users guide
	ADT_Int16		DTMFSuppressionType;		//DTMF Suppression type - for future use, Currently hard-coded to DEFAULT_SUPPRESSION
}		TRDetectParamsV701_t;

typedef struct
{
	ADT_UInt16		APIVersion;
//	TRDetectInstance_t *pTRGenerateInstance;
	ADT_UInt16		TRGenInstanceSizeInBytes;
	TRGenerateEvent_t *pPastEventStorage;		//Stores past tone events in case the generator falls behind the input events
	ADT_UInt8		NPastGenEventsToStore;	//Number of past events to store. This must be equal to the size of the PastEvenStorage, in uints of TRGenEvent_t
	ADT_UInt8		UseEnforceDefaults;		//If 1, use enforced generator minimum on/off times. If zero, use specified values (below)
	ADT_Int16		DTMFMinOnTimeMSec;		//Minimum DTMF on time in msec, enforced by relay generator
	ADT_Int16		DTMFMinOffTimeMSec; 	//Minimum DTMF off time in msec, enforced by relay generator
	ADT_Int16		MFR1MinOnTimeMSec;		//Minimum MFR1 On Time in msec excluding KP tone, enforced by relay generator
	ADT_Int16		MFR1KPMinOnTimeMSec;	//Minimum MFR1 On Time in msec for KP tone, enforced by relay generator
	ADT_Int16		MFR1MinOffTimeMSec;		//Minimum MFR1 Off Time in msec, enforced by relay generator
	ADT_Int16		MFR2MinOnTimeMSec;		//(future use) Minimum MFR2 On Time in msec for KP tone, enforced by relay generator
	ADT_Int16		MFR2MinOffTimeMSec;		//(future use) Minimum MFR2 Off Time in msec, enforced by relay generator

}		TRGenerateParamsV701_t;

/* Result Codes */
typedef enum {
	TR_RESULT_OK,
	TR_RESULT_API_VERSION_MISMATCH,
	TR_RESULT_INSTANCE_SIZE_TOO_SMALL,
	TR_RESULT_NULL_POINTER,
	TR_RESULT_CREATE_ERROR,
	TR_RESULT_DELETE_ERROR,
	TR_RESULT_GEN_EVENT_OVERFLOW
}		TRResultCode_e;


/* Initialization Functions */

ADT_API TRResultCode_e TR_ADT_detectInit(	
		TRDetectInstance_t	*pDetInstance, 
		TRDetectParamsV701_t	*pTRDetectParams
		);

ADT_API TRResultCode_e TR_ADT_generateInit(
		TRGenerateInstance_t	*pGenInstance, 
		TRGenerateParamsV701_t	*pTRGenParams
		);

/* Execute Functions */

ADT_API void TR_ADT_detect(
		TRDetectInstance_t	*pDetInstance, 
		ADT_Int16			*pInputSamples, 
		ADT_Int16			NInputSamples, 
		ADT_Int16			*pOutputSamples, 
		TRDetectResults_t	*pTRDetectResults
		);

ADT_API void TR_ADT_getLastDetectResults(
		TRDetectInstance_t *pDetInstance,
		TRDetectResults_t	*pTRDetectResults
		);

ADT_API TRResultCode_e TR_ADT_sendNewEventToGenerator(
		TRGenerateInstance_t	*pGenInstance, 
		TRGenerateEvent_t		*pEvent
		);

ADT_API ADT_Int16 TR_ADT_generator(
		TRGenerateInstance_t	*pGenInstance, 
		ADT_Int16				NOutputSamples, 
		ADT_Int16				*pOutputSamples	//Returns 1 if generator is active, 0 otherwise
		);

/* Create and delete functions (for systems that use malloc) */

/* 
 *	Note that the individual detector instances specified in TRDetParamsV701_t may be treated as a flag to simplify the "create" API usage.
 *  In other words, if you pass a pointer to a detector instance, create functions as it normally would, using the host application's 
 *  tone detector instances. If the passed detector instance is set to 1, TR_ADT_detectCreate will instantiate the detector using MZ_ADT_malloc
 *  detector instance is set to 0, the detector will not be instantiated and it will not be used.
 */

ADT_API TRDetectInstance_t		*TR_ADT_detectCreate(
		TRDetectParamsV701_t	*pTRDetParams, 
		TRResultCode_e			*pResultCode
		);

ADT_API TRGenerateInstance_t	*TR_ADT_generateCreate(
		TRGenerateParamsV701_t	*pTRGenParams, 
		TRResultCode_e			*pResultcode);

ADT_API TRResultCode_e TR_ADT_detectDelete(
		TRDetectInstance_t *pTRDetInstance
		);

ADT_API TRResultCode_e TR_ADT_generateDelete(
		TRGenerateInstance_t *pTRGenInstance
		);

/* For debug only */
void TR_ADT_initDump(char *pFileName);
void TR_ADT_closeDump();
ADT_API void TR_ADT_dumpDetect(TRDetectInstance_t *pTRDetInstance);
ADT_API void TR_ADT_dumpGenerate(TRGenerateInstance_t *pTRGenInstance);

#endif //_TR_USER_H
