/*
   ipStack.h

   G.Pak support for 64x IP stack
   

 */
#ifndef IPSTACK_H
#define IPSTACK_H

#include "adt_typedef.h"

#define HWI_IP_STACK   9        // Network MAC interrupt

// G.PAK Required Interfaces
extern void RTP_Init ();     

typedef struct _rtpAddr {
   ADT_UInt32   SSRC;
   ADT_UInt32   IP;      // actual IP address.
   ADT_UInt32   dstIP;   // reported destination IP address.
   ADT_UInt16   Port;
   ADT_UInt8    MAC[6];
} rtpAddr;


// RTP stack interfaces
//
// NOTE: All fields are in network byte order
//       THIS STRUCTURE MUST MATCH GpakIPCfg_t IN GpakApi.h.
typedef struct ipStackCfg {
   ADT_UInt32 RTPSrcIP;       // Source IP for out-bound RTP packets
   ADT_UInt8  TxMac[6];       // MAC Address used for packet transmissions

   ADT_UInt8  RxMac[6];       // MAC Address used for packet reception

   ADT_UInt32 IPAddr;         // Local IP address.  Set to NULL for DHCP assignment
   ADT_UInt32 GateWayIP;      // Gateway IP.        Ignored for DHCP
   ADT_UInt32 IPMask;         // Sub-net mask.      Ignored for DHCP

   ADT_UInt16 rtpPort;        // Gpak RTP/UDP port
   ADT_UInt16 msgPort;        // Gpak TCP messaging port
   ADT_UInt16 evtPort;        // Gpak TCP messaging port
   ADT_UInt16 portRange;      // Gpak RTP range (default 4000) // Alignment filler
   
   ADT_UInt32 DHCPIP;         // IP address to substitute as source IP on outbound RTP packets

   char       DomainName[64]; // Domain name.       Ignored for DHCP
} IPStackCfg;
extern const IPStackCfg IPConfig;

typedef struct VLAN_Tag_t {
   void  *vlanDeviceHndl;
   ADT_Int16  ifIdx;
   ADT_UInt16 Tag;       // Priority | VLAN_ID in network byte order
} VLAN_TAG;
extern VLAN_TAG VLANTags[];

// IPToSess is a list of IP:port:SSRC to RTP sessions
// this list is used to supply the IP address, port address and
// RTP session instance for open RTP sockets.

typedef struct IPToSess_t {
// NOTE:  lcl.SSRC is set to non-zero when the rmt MAC address becomes available to bypass ARP messaging

   rtpAddr    lcl;        //  Local MAC, IP (unicast), Port and SSRC addresses
   rtpAddr    rmt;        // Remote MAC, IP (unicast), Port and SSRC addresses

   ADT_UInt8 vlanIndex;    // Index into VLANTags table
   ADT_UInt8 DSCP;         // IP TOS field
   ADT_UInt8 hashIdx;     // Hash index into table
   ADT_UInt8 idx;         // Actual table index

   ADT_UInt16 ChanId;     // Index of channel corresponding to RTP Session
   void *RTPSession;      // Pointer to RTP session instance
} IPToSess_t;
extern IPToSess_t IPToSess [0x100];

extern void RTPMemInit ();     
extern void GpakStackInit ();
extern int sendHostCoreMsg (ADT_UInt32* Data, int dataI8);

extern far int fireHoseTest;
extern far int useCheckSumAsChannel;
extern far int UseMDIO;
extern far unsigned int EMAC_ModeFlag;

// Hostcore messaging
#define DELETE_MULTICAST_IP 0
#define ADD_MULTICAST_IP    1
#define UPDATE_VLAN_TAG     2

#endif
