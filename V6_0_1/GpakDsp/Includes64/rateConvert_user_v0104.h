#ifndef _RATECONVERTER_ADT_USER_H
#define _RATECONVERTER_ADT_USER_H

#ifdef CPP
   #define ADT_FT extern "C"
#else
   #define ADT_FT
#endif

#include "adt_typedef.h"

#define MAX_SRC_FRAME_SIZE 320
#define DEC_COEF_SIZE 116
#define INT_COEF_SIZE 112



// sample coefficients, users may change to its own tables
extern const ADT_Int16 SRC_ADT_dec_coeff[DEC_COEF_SIZE];
extern const ADT_Int16 SRC_ADT_int_coeff_by_2[INT_COEF_SIZE];
#ifdef USERS_COEF
static const ADT_Int16 dec_coeff[DEC_COEF_SIZE] = {
37,53,16,-25,-26,25,34,-14,-50,2,            
57,22,-62,-47,54,78,-37,-103,4,123,
39,-128,-92,116,147,-80,-199,21,236,60,
-250,-158,231,264,-172,-365,69,448,79,-493,
-269,483,492,-398,-735,216,983,93,-1219,-584,
1425,1395,-1586,-3044,1687,10283,14662,10283,1687,-3044,
-1586,1395,1425,-584,-1219,93,983,216,-735,-398,
492,483,-269,-493,79,448,69,-365,-172,264,
231,-158,-250,60,236,21,-199,-80,147,116,        
-92,-128,39,123,4,-103,-37,78,54,-47,
-62,22,57,2,-50,-14,34,25,-26,-25,
16,53,37,0,0,0};

static const ADT_Int16 int_coeff_by_2[] = {
-21, 576, 590, -382, 
255, -166, 82, 9, 
-114, 226, -336, 435, 
-504, 529, -497, 397,
-223, -27, 347, -724,
1143, -1580, 2012, -2411,
2753, -3015, 3180, 29532,
3180, -3015, 2753, -2411,
2012, -1580, 1143, -724,
347, -27, -223, 397,
-497, 529, -504, 435,
-336, 226, -114, 9, 
82, -166, 255, -382,
590, 576, -21, 0,

186, 835, -22, -148,
219, -268, 302, -320,
311, -267, 185, -62,
-99, 286, -485, 678, 
-841, 948, -971, 883, 
-654,248, 389, -1364, 
2952,-6194, 20603, 20603, 
-6194,2952, -1364, 389, 
248,-654, 883, -971, 
948,-841, 678, -485, 
286,-99, -62, 185,
-267, 311,-320, 302, 
-268, 219,-148, -22, 
835, 186,0,0};
#endif
#ifdef __TMS320C55XX__

typedef struct {
	ADT_Int32 Data[((DEC_COEF_SIZE+MAX_SRC_FRAME_SIZE)>>1)+4];
} RateDownBy2_Channel_t;

typedef struct {
	ADT_Int32 Data[((INT_COEF_SIZE+MAX_SRC_FRAME_SIZE)>>1)+4];
} RateUpBy2_Channel_t;

#else

typedef struct {
	ADT_Int64 Data[((DEC_COEF_SIZE+MAX_SRC_FRAME_SIZE)>>2)+2];
} RateDownBy2_Channel_t;

typedef struct {
	ADT_Int64 Data[((INT_COEF_SIZE+MAX_SRC_FRAME_SIZE)>>2)+2];
} RateUpBy2_Channel_t;

#endif

ADT_FT void SRC_ADT_InitdecimateBy2 (RateDownBy2_Channel_t *ptr, ADT_Int16 Len);
ADT_FT void SRC_ADT_InitInterpolateBy2 (RateUpBy2_Channel_t *ptr, ADT_Int16 Len);

ADT_FT void SRC_ADT_decimateBy2 (RateDownBy2_Channel_t *State, ADT_Int16 *input, ADT_Int16 FrameL, ADT_Int16 *output, const ADT_Int16 *dec_intr_coeff, ADT_Int16 coefLen);
ADT_FT void SRC_ADT_interpolateBy2 (RateUpBy2_Channel_t *State, ADT_Int16 *input, ADT_Int16 FrameL, ADT_Int16 *output, const ADT_Int16 *int_coeff, ADT_Int16 CoefLen);

#endif //_RATECONVERTER_ADT_USER_H
