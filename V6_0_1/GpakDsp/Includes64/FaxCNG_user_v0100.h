/*___________________________________________________________________________
 |
 |   File: FAXCNG_user.h
 |
 |   This file is the Adaptive Digital Technologies, Inc. 
 |   public header file for the FAXCNG tone detectors.
 |   
 |   This software is the property of Adaptive Digital Technologies, Inc.
 |   Unauthorized use is prohibited.
 |
 |   Copyright (c) 2004, Adaptive Digital Technologies, Inc.
 |
 |   Version 1.0
 |   Oct. 19, 2004
 |
 |   www.adt-inc.com
 |   610-825-0182
 |___________________________________________________________________________
*/
#ifndef _FAXCNG_USER_H
#define _FAXCNG_USER_H
#include "adt_typedef.h"

// Define the result (event) codes
#define FAXCNG_NULL 0
#define FAXCNG_EARLY -1
#define FAXCNG_FALSE_EARLY -2
#define FAXCNG_LEAD_EDGE -3
#define FAXCNG_TRAIL_EDGE -4
#define FAXCNG_DIGIT -5

typedef struct
{
	ADT_Int32 FAXCNGInstanceSpace[7];
} FAXCNGInstance_t;

#define FAXCNG_DFT_N_4 80
#define FAXCNG_SCRATCH_SIZE  (FAXCNG_DFT_N_4+8)/2

typedef struct
{
	ADT_Int32 FAXCNGScratchSpace[FAXCNG_SCRATCH_SIZE];
} FAXCNGScratch_t;


void FAXCNG_ADT_init(FAXCNGInstance_t *FAXCNGInstance);

ADT_Int16 FAXCNG_ADT_toneDetect(
				FAXCNGInstance_t *FAXCNGInstance, 
				ADT_Int16 InputSamples[],
				ADT_Int16 NInputSamples,
				FAXCNGScratch_t *scratch);
#endif
