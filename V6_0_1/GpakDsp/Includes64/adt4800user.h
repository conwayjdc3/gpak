//;*******************************************************************
//;
//; Copyright(c) 2001-2002 Adaptive Digital Technologies Inc. 
//;              All Rights Reserved
//;
//;
//; File Name:  adt4800user.h
//; 
//;
//; Description:   application interface for ADT 4800 bps voice coder
//;
//;
//;******************************************************************
#ifndef ADT_4800USER_H
#define ADT_4800USER_H

typedef struct {
int LPCWind[120];
long HiState[2];
int f[145];
int PrevLsp[10];
int PrevErr[145];
int PrevExc[145];
int zstate[10];
int pstate[10];
int zstate_r[10];
int pstate_r[10];
long Err[5];
int SinDet;
}Enc48Chan_t;       // Encoder Channel Type

typedef struct {
int PrevLspDec[10];
int PrevExcDec[145];
int synth_state[10];
int PostFirDl[10];
int PostIirDl[10];
int InterIndx;
int DecPark;
int DecGain;
int InterGain;
int Ecount;
int Rseed;
}Dec48Chan_t;       // Decoder Channel Type

void decInitialize4800Channel (
   Dec48Chan_t *TheChanPtr
   );

void Decode_ADT_4800 (
   Dec48Chan_t *TheChanPtr,
   short int DecodeStream[],
   short int OutputSpeech[],
   short int FrameEraseFlag
   );

void codInitialize4800Channel (
    Enc48Chan_t *TheChanPtr
   );

void Encode_ADT_4800 (
   Enc48Chan_t *TheChanPtr,
   short int InputSpeech[],
   short int EncodeStream[]
   );
#endif

