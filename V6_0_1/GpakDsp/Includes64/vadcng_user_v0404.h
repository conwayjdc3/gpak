#ifndef _VADCNG_USER_H
#define _VADCNG_USER_H

#include "adt_typedef.h"

#define SILENCE     0
#define VOICE       1
#define NON_VOICE   2

typedef struct 
{
	ADT_Int32 vad_vars[16]; 
} VADCNG_Instance_t;

typedef struct 
{
	ADT_Int16 Thres;
	ADT_Int16 HangMSec;
	ADT_Int16 VadFrameSize;
	ADT_Int16 CngFrameSize;
	ADT_Int16 WindowSizeMSec;
	ADT_Int16 SamplingRate; 
} VADCNG_ADT_Param_t;


// Function Prototypes.
void VADCNG_ADT_init(VADCNG_Instance_t *ChannelInst, 
   					 VADCNG_ADT_Param_t *pParams);
					
ADT_Int16 VADCNG_ADT_vad (VADCNG_Instance_t *ChannelInst, 
						ADT_Int16 pcm_in[], 
						ADT_Int16 *VadLvl);
						
void VADCNG_ADT_cng (VADCNG_Instance_t *ChannelInst, 
					ADT_Int16 VadFlag, 
					ADT_Int16 Level, 
					ADT_Int16 OutputBuffer[]);

#endif
