// Copyright (c) 2009, Adaptive Digital Technologies, Inc.
//
// File Name: srtp.h
//
//
// Description:
//   This file contains SRTP/SRTCP related definitions and API prototypes.
//
// Version: 1.2
//
// Revision History:
//   04/29/09 - Initial release.
//   05/01/09 - Name changes, comments, new parameter.
//   05/04/09 - Corrected data type name errors.
//   05/16/09 - Added null encryption type and init failed status code.
//   05/19/09 - Revised configuration information and application callback
//              parameters.
//   05/26/09 - Added status codes.
//   06/01/09 - Changed key derivation behavior.
//

#ifndef _SRTP_USER_H
#define _SRTP_USER_H
#include "adt_typedef.h"

// Definition of SRTP/SRTCP instance types.
typedef enum SrtpInstType_t {
    INST_SRTP_RX = 0,           // SRTP Rx (decrypt) instance
    INST_SRTP_TX = 1,           // SRTP Tx (encrypt) instance
    INST_SRTCP_RX = 2,          // SRTCP Rx (decrypt) instance
    INST_SRTCP_TX = 3           // SRTCP Tx (encrypt) instance
} SrtpInstType_t;

// Definition of Key schemes.
typedef enum SrtpKeyScheme_t {
    KEY_PSK = 0,                // Pre-shared Master key
    KEY_MKI = 1,                // Master Key Identifier
    KEY_FT =  2                 // From To
} SrtpKeyScheme_t;

// Definition of Encrytion types.
typedef enum SrtpEncryptType_t {
    ENCRYPT_NONE = 0,           // None
    ENCRYPT_CM = 1,             // Counter Mode
    ENCRYPT_F8 = 2,             // F8 Mode
    ENCRYPT_CBC = 3             // CBC
} SrtpEncryptType_t;

// Definition of Authentication types.
typedef enum SrtpAuthType_t {
    AUTH_NONE = 0,              // None
    AUTH_HMAC_SHA = 1,          // HMAC-SHA
    AUTH_MD5 = 2                // MD5
} SrtpAuthType_t;

// Definition of SRTP/SRTCP status codes.
typedef enum SrtpStatus_t {
    SRTP_STAT_SUCCESS = 0,              // success
    SRTP_STAT_INVALID_INST_TYPE = 1,    // invalid Instance type
    SRTP_STAT_INVALID_KEY_SCHEME = 2,   // invalid Key Scheme
    SRTP_STAT_INVALID_ENCRYPT_TYPE = 3, // invalid Encryption type
    SRTP_STAT_INVALID_AUTH_TYPE = 4,    // invalid Authentication type
    SRTP_STAT_INVALID_KEY_SIZE = 5,     // invalid Key size
    SRTP_STAT_INVALID_SALT_SIZE = 6,    // invalid Salt size
    SRTP_STAT_UNSUPPORTED_CFG = 7,      // unsupported Encryption/Authentication
    SRTP_STAT_INVALID_VERSION = 8,      // invalid version
    SRTP_STAT_INVALID_ALIGNMENT = 9,     // Instance structure not aligned correctly
   SRTP_INVALID_DIRECTION = 10,   // invalid direction field
   SRTPNotConfigured,             // SRTP not configed yet
   SRTPNotSupported,             // SRTP not supported in the build 
   SRTPChannelError,             // chan id out of range
   SRTPChannelActive,            // Gpak Channel already acvtived, SRTP config must be called before channelConfig
   SRTPChannelInactive           // SRTP Channel not acvtived, gpakSRTPNewKey must be called after channelConfig
} SrtpStatus_t;

// Definition of SRTP/SRTCP instance configuration parameters.
#define SRTP_API_VERSION 1
typedef struct SrtpInstanceCfg_V1_t {
    ADT_UInt16  APIVersion;
    SrtpInstType_t    InstType;        // instance type
    SrtpKeyScheme_t   KeyScheme;      // Key scheme
    SrtpEncryptType_t EncryptType;  // Encryption type
    SrtpAuthType_t    AuthType;        // Authentication type
    ADT_UInt16 KeySizeU8;               // Session key length (octets, even number)
    ADT_UInt16 SaltSizeU8;              // Session salt length (octets, even number)
    ADT_UInt16 AuthKeySizeU8;           // Authentication Key size (octets, even)
    ADT_UInt16 AuthTagLenU8;            // Authentication Tag length (octets, even)
    ADT_UInt16 MkiLengthU8;             // MKI length (octets)
} SrtpInstanceCfg_V1_t;

// Definition of SRTP/SRTCP key information.
//
// NOTE:  When LookAhead is true the call back function should return the next key 
//        that will become active AFTER the one for the current packet as identified 
//        by the pkt structure or the MKI value.
//
//        When LookAhead is false the call back functin should return the key needed for
//        the current packet as identified by the pkt structure or the MKI value.
//
typedef struct  SrtpKeyInfo_t {
    SrtpKeyScheme_t KeyScheme;
    ADT_UInt16 *pKeyValue;              // pointer to next master key buffer
    ADT_UInt16 *pSaltValue;             // pointer to next master salt value
    ADT_UInt16 KeyDerivationRate;       // Key Derivation Rate (power of 2)
    ADT_Bool   LookAhead;               // Key Request is a look ahead
    // Key scheme type dependent information.
    // MaxKeyLife, From, and To are 48-bit integer values represented as 3 16-bit integers 
    // with the most signification 16-bits in element 0.  The values indicate the 
    // how many packets to send/receive before requesting the next master key.
    union {
        struct srtp {
            ADT_UInt32 roc;            // Identifies roc for next rtp packet
            ADT_UInt16 seq;            // Identifies seq for next rtp packet
        } srtp;

        struct strcpPkt {
            ADT_UInt32 index;          // Identifies index for next srtcp packet
        } srtcp;
    } pkt;
    union {
        // KEY_MKI scheme only.
        struct Mki {
            ADT_UInt16 MaxKeyLife[3];   // max key lifetime (most significant first)
            ADT_UInt32 MkiValue;        // MKI value
            ADT_UInt16 MkiI8;
        } Mki;

        // KEY_FT scheme only.
        struct Ft {
            ADT_UInt16 From[3];         // From Index (most significant first)
            ADT_UInt16 To[3];           // To Index (most significant first)
        } Ft;
    } u;
} SrtpKeyInfo_t;

// Definition of SRTP/SRTCP statistics.
typedef struct SrtpStats_t {
    ADT_UInt32 pktCount;            // count of packets processed
    ADT_UInt32 rollOverCounter;     // current roll over counter
    ADT_UInt16 rekeyCount;          // count of rekeys
    ADT_UInt16 replayCount;         // count of packets replayed
    ADT_UInt16 authFailCount;       // count of authentication failures
} SrtpStats_t;


//{ Prototype of application callback function to obtain a Key value.
//
//  This callback function is used to obtain a Key value for an SRTP/SRTCP
//  instance. The callback occurs whenever another key is needed. The function
//  returns TRUE if the key is provided or FALSE to indicate the key can't be
//  provided. The calling parameters are described below.
//
//   pAppHandle - The application supplied value to identify the particular
//                SRTP/SRTCP instance. This can be any value of convenience to
//                the application and is specified in the SrtpInitializeInstance
//                function.
//   pKeyInfo - A pointer to a structure for storing common key information and
//              key scheme type dependent information. The key scheme type must
//              be the same as specified in the instance configuration. The
//              structure elements are described below.
//
//     pKeyValue - A pointer to a 16 bit aligned buffer for storing the Key
//                 value. The pointer is set by the caller and the application
//                 must store the key at this location. The length of the key
//                 value must be the same as specified in the instance
//                 configuration.
//     pSaltValue - A pointer to a 16 bit aligned buffer for storing the Salt
//                  value. The pointer is set by the caller and the application
//                  must store the salt at this location. The length of the salt
//                  value must be the same as specified in the instance
//                  configuration.
//     KeyDerivationRate - The Key Derivation Rate power of 2. Upon entry it is
//                         initialized to a value that disables key derivation.
//                         It can be set by the application. Upon return, key
//                         derivation is enabled only if the the value is less
//                         than or equal to the largest value allowed.
//
//     Roc - rollover counter of current srtp packet
//     seq - sequence number of current srtp packet
//     index - index number of current srtcp pakcet
//
//     MaxKeyLife - The maximum life of the key. Upon entry it is initialized to
//                  the largest value allowed. It can be set by the application.
//                  Upon return, the value is limited to the largest value
//                  allowed. This element is applicable to KEY_PSK and KEY_MKI
//                  key schemes only.
//     MkiValue - The MKI value. For receive type instances, upon entry it is
//                set to the MKI of the key needed. For transmit type instances,
//                the application must set this value. Upon return, the value is
//                limited to the largest value allowed based on the MKI length
//                specified in the instance configuration. This element is
//                applicable to the KEY_MKI key scheme only.
//     From - The From index value. The application must set this value. This
//            element is applicable to the KEY_FT key scheme only.
//     To - The To index value. The application must set this value. This
//          element is applicable to the KEY_FT key scheme only.
//}
typedef ADT_Bool (SrtpCallBack_t) (void *pAppHandle, SrtpKeyInfo_t *pKeyInfo);


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ SrtpEvalContextSizeU8 - Evaluate size of context memory for an SRTP instance.
//
// FUNCTION
//  This function determines the size of context memory and scratch memory
//  needed for an SRTP/SRTCP instance based on the specified configuration. It
//  also provides the number of extra octets needed in a packet buffer to
//  contain the Authentication Tag, MKI, and RTCP index.
//
//   Note: The configuration parameters are passed by reference for performance
//         reasons only and will not be written by this function.
//
// RETURNS
//  The number of contiguous octets of memory needed for the instance's context.
//  A value of zero indicates an invalid configuration.
//
//  The number of packet buffer octets needed for the Authentication Tag and MKI
//  is stored in the specified variable. This number of octets will be appended
//  to a packet when encrypting.
//
//}
extern ADT_UInt32 SrtpEvalContextSizeU8(
    SrtpInstanceCfg_V1_t *pCfgParms,   // pointer to SRTP configuration parameters
    ADT_UInt16 *pScratchMemSizeU8,      // pointer to size of scratch mem variable
    ADT_UInt16 *pExtraPktLenU8          // pointer to extra packet length variable
    );


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ SrtpInitializeInstance - Initialize an SRTP instance.
//
// FUNCTION
//  This function initializes an SRTP or SRTCP instance based on the specified
//  configuration and registers the callback function used to provide key
//  values.
//
//   Note 1: The context memory must be allocated by the caller and be 32 bit
//           aligned and sized as per the SrtpEvalContextSize return value for
//           the same configuration.
//
//   Note 2: The configuration parameters are passed by reference for
//           performance reasons only and will not be written by this function.
//
//   Note 3: The callback occurs whenever a new Key value is needed. The
//           specified application handle is passed in the callback for use by
//           the application in associating the callback with an SRTP/SRTCP
//           instance.
//
// RETURNS
//  SRTP_STAT_SUCCESS = Context initialized successfully and ready for service.
//  SRTP_STAT_INVALID_INST_TYPE = The Instance type is invalid.
//  SRTP_STAT_INVALID_KEY_SCHEME = The Key Scheme is invalid.
//  SRTP_STAT_INVALID_ENCRYPT_TYPE = The Encryption type is invalid.
//  SRTP_STAT_INVALID_AUTH_TYPE = The Authentication type is invalid.
//  SRTP_STAT_INVALID_KEY_SIZE = The Key size is invalid.
//  SRTP_STAT_INVALID_SALT_SIZE = The Salt size is invalid.
//  SRTP_STAT_UNSUPPORTED_CFG = The Encryption/Authentication/Key Size
//                              combination is unsupported.
//
//}
extern SrtpStatus_t SrtpInitializeInstance (
    void       *pInstMem,            // pointer to aligned SRTP/SRTCP instance memory
    ADT_UInt32 *pScratchMem,         // pointer to scratch memory
    SrtpInstanceCfg_V1_t *pCfgParms, // pointer to SRTP configuration parameters
    SrtpCallBack_t *pAppCallBack,    // pointer to application callback function
    void *pAppHandle                 // application handle passed in callback
    );


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ SrtpEncryptU8 - Encrypt an SRTP/SRTCP packet.
//
// FUNCTION
//  This function performs encryption of an SRTP or SRTCP packet.
//
//   Note 1: The output packet buffer must be large enough to contain the
//           encrypted packet. The size must be at least the length of the input
//           packet plus the number of octets needed for the Authentication Tag
//           and MKI.
//
//   Note 2: The input and output packet buffers may be the same.
//
// RETURNS
//  Length of encrypted packet stored in output buffer (units of octets). A
//  value of zero indicates an error.
//
//}
extern ADT_UInt16 SrtpEncryptU8 (
    void *pInstMem,                 // pointer to aligned SRTP/SRTCP instance memory
    void *pInPkt,                   // pointer to unencrypted input packet
    ADT_UInt16 InPktLenU8,          // input packet length (octets)
    void *pOutPktBufr               // pointer to encrypted output packet buffer
    );


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ SrtpDecryptU8 - Decrypt an SRTP/SRTCP packet.
//
// FUNCTION
//  This function performs decryption of an SRTP or SRTCP packet.
//
//   Note 1: The output packet buffer must be large enough to contain the
//           decrypted packet. The size must be at least the length of the input
//           packet.
//
//   Note 2: The input and output packet buffers may be the same.
//
// RETURNS
//  Length of decrypted packet stored in output buffer (units of octets). A
//  value of zero indicates an error.
//
//}
extern ADT_UInt16 SrtpDecryptU8 (
    void *pInstMem,            // pointer to aligned SRTP/SRTCP instance memory
    void *pInPkt,              // pointer to encrypted input packet
    ADT_UInt16 InPktLenU8,     // input packet length (octets)
    void *pOutPktBufr          // pointer to unencrypted output packet buffer
    );


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ SrtpSetRollOverCounter - Set the roll over counter for an SRTP instance.
//
// FUNCTION
//  This function sets an SRTP instance's roll over counter to the specified
//  value.
//
// RETURNS
//  nothing
//
//}
extern void SrtpSetRollOverCounter (
    void *pInstMem,            // pointer to aligned SRTP/SRTCP instance memory
    ADT_UInt32 rollOverCount   // roll over counter value
    );


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ SrtpGetStatistics - Get statistics for an SRTP instance.
//
// FUNCTION
//  This function gets an SRTP instance's statistics.
//
// RETURNS
//  An indication of success or failure. TRUE indicates the statistics were
//  obtained and stored in the specified variable. FALSE indicates the
//  statistics could not be obtained.
//
//}
extern ADT_Bool SrtpGetStatistics (
    void *pInstMem,             // pointer to aligned SRTP/SRTCP instance memory
    SrtpStats_t *pStatistics    // pointer to statistics variable
    );
#define SrtpInstanceCfg_t SrtpInstanceCfg_V1_t
#endif /* _SRTP_H */
