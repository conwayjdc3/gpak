#ifndef _TONERELAYGEN_H
#define _TONERELAYGEN_H

#include "toneRelayDefs.h"

typedef struct
{
    TRCodedToneData_t   toneData;
    ADT_Int32           Data[11];
} TRGenInstance_t;

// The following structure contains minimum on and off times that can optionally be enforced by the relay generator.
// Defaults are used unless overridden by TR_ADT_configureEnforcedOnOffTimes.
// A minimum value of 0 msec can be used individually to disable the enforcement

typedef struct
{
	ADT_Int16 DTMFMinOnTimeMSec;		//Minimum DTMF on time in msec
	ADT_Int16 DTMFMinOffTimeMSec; 	//Minimum DTMF off time in msec
	ADT_Int16 MFR1MinOnTimeMSec;		//Minimum MFR1 On Time in msec excluding KP tone
	ADT_Int16 MFR1KPMinOnTimeMSec;	//Minimum MFR1 On Time in msec for KP tone
	ADT_Int16 MFR1MinOffTimeMSec;	//Minimum MFR1 Off Time in msec
}		OnOffTimeEnforce_t;

ADT_Int16  TR_ADT_generate(TRGenInstance_t *ToneRelayGenInst, 
                           ADT_UInt16       NSamples, 
                           ADT_Int16       OutSignal[]);
                           
void TR_ADT_genInit(TRGenInstance_t *ToneRelayGenInst);

void TR_ADT_configureEnforcedOnOffTimes(OnOffTimeEnforce_t *pOnOffTimeEnforce);

#endif
