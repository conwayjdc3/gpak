#ifndef _TONERELAYDEFS_H
#define _TONERELAYDEFS_H
#include <common/include/adt_typedef_user.h>

#define N_TABLE_TONES       100     // num entries in table for tone index to 
                                    // frequency pair translation
#define N_ENTRIES_PER_TONE  2       // 2 freqs per tone index
#define NO_TONE_DETECTED    100     // return tone index value for no detection


// ============== TRCodedToneData_t structure member definitions ==============
//
// ToneIndex -  output from detector, input to generator that specifies the tone 
//              pair. 
//              0-15...dtmf
//              20-34...mfr1
//              40-54...mfr2 forward
//              60-74...mfr2 backward
//              80-84...call progress
//
// Amplitude -      output from detector, input to generator. Positive value 
//                  specifying amplitude in minus dB (relative to full scale)
//
// TimeElapsed -    output from detector, input to generator. Elapsed time 
//                  of tone in numsamples (at 8000 samples/sec)
//
// GenActive -      output flag from generator indicating if the current frame
//                  contains generated tone samples.
//
// UseIndex -       input flag to generator and detector indicating whether it 
//                  should use a tone index or frequency pairs to specify the
//                  tone.
//
// ToneFrequency[][] - input to generator, output from detector  that is only 
//                  valid when UseIndex is 0 specifies the tone frequencies.
//                     

typedef struct
{
	ADT_Int16 ToneIndex;            
	ADT_Int16 Amplitude;            
	ADT_UInt16 TimeElapsed;          
   ADT_Int16 GenActive;            
   ADT_Int16 UseIndex;             
   ADT_Int16 ToneFrequency[N_ENTRIES_PER_TONE];
   ADT_UInt16 SampleRate;
   ADT_Int16  LastFrameFlag;
} TRCodedToneData_t;

#endif
