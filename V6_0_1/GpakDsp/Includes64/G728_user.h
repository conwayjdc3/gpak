#ifndef G728_USER_H
#define G728_USER_H

// Fundtion Prototype, users SHOULD NOT modified any item below 
#include "adt_typedef.h"
typedef struct
{
	ADT_Int32 Channel_data[360+2]; // short int 718
} G728EncodeChannelInstance_t; 


typedef struct
{
	ADT_Int32 Channel_data[535+1]; // short int1068
} G728DecodeChannelInstance_t; 

typedef struct
{
	ADT_Int32 Channel_data[103]; //short int 206 
} G728ScratchInstance_t; 


void  G728_ADT_encInit (G728EncodeChannelInstance_t *ptrInst, G728ScratchInstance_t *ScratchInst, ADT_Int16 Rate);
void  G728_ADT_encode (G728EncodeChannelInstance_t *ptrInst, ADT_Int16 in_array[], ADT_Int16 EncodeOutStream[]);


void  G728_ADT_decInit (G728DecodeChannelInstance_t *ptrInst, G728ScratchInstance_t *ScratchInst, ADT_Int16 PostEnable, ADT_Int16 Rate);
void  G728_ADT_decode (G728DecodeChannelInstance_t *ptrInst, ADT_Int16 out_array[], ADT_Int16 DecodeInStream[]);

#endif // end of #ifndef G728_USER_H
