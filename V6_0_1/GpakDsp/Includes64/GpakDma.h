#ifndef _GPAK_DMA_H
#define _GPAK_DMA_H

#include "GpakPcm.h"


// The "A" and "B" suffixes denote the direction of pcm data flow during 
// frame processing. The "A" direction is the PCM-input direction. The "B"
// direction is the PCM-output direction.
#define XFER_A 0            // group A DMA transfer (pcm input processing)
#define XFER_B 1            // group B DMA transfer (pcm output processing)

#define DMA_WR_EXT 0        // DMA write to external memory
#define DMA_RD_EXT 1        // DMA read from external memory

#define PCM_EC 0            // PCM echo cancellor
#define PKT_EC 1            // PKT echo cancellor

#define MAX_NUM_XFERS 6     // maximum number of DMA transfers per instance6

#define INCREMENT_MOD3(a) if ((++a) > 2) a = 0

//////// used for debug only /////////////////////
typedef struct
{
    int gpakChan;
    int dmaChan;
    int dmaCount;
    int rdFlag;
    int loadIndex; 
    int storeIndex;
    int procIndex; 
    int groupFlag;
} dmaDebug_t;
///////////////////////////////////////////////////
typedef union
{
    ADT_UInt32 TransferOptions;
    struct 
    {
        unsigned FS:1;
        unsigned LINK:1;
        unsigned PDTD:1;
        unsigned PDTS:1;
        unsigned rsvd1:1;
        unsigned ATCC:6;        
        unsigned rsvd2:1;           
        unsigned ATCINT:1;           
        unsigned TCCM:2;     
        unsigned rsvd3:1;    
        unsigned TCC:4;        
        unsigned TCINT:1;        
        unsigned DUM:2;       
        unsigned _2DD:1;     
        unsigned SUM:2;     
        unsigned _2DS:1;     
        unsigned ESIZE:2;     
        unsigned PRI:3;    
    } Bits;                              
} dmaTransferOpts_t;


typedef enum
{
    ELEMENT_SIZE_32 = 0,   // indicates a 32-bit DMA element size
    ELEMENT_SIZE_16 = 1,   // indicates a 16-bit DMA element size
    ELEMENT_SIZE_8 = 2     // indicates an 8-bit DMA element size
} DMAElementSize;


// Define the DMA transfer parameter structure. It contains the parameters
// used by each channel that is part of a DMA instance
typedef struct
{
    void *src;                 // src address
    void *dst;                 // dst address
    void **intMemQueue;  // internal memory buffer queue
//    DMAElementSize size;       // element size
    ADT_UInt32 count;          // count of elements to DMA in units of size
    ADT_UInt32 tccCode;        // transfer complete code for this DMA channel
    ADT_UInt32 *paramBaseAddr; // Base address of edma parameter block
    dmaTransferOpts_t Opt;     // DMA parameter options
} dmaXferParams_t;

// Define the DMA transfer information structure. It holds information about a
// DMA instance.
typedef struct
{
    ADT_UInt32      numXfers;       // number of DMA transfers in this instance
    ADT_UInt32      ciprlMatch;     // expected ciprl bits for completed xfers
    ADT_UInt32      ciprhMatch;     // expected ciprh bits for completed xfers
    ADT_UInt32      ccerl;          // channel-chain enable low bits
    ADT_UInt32      ccerh;          // channel-chain enable high bits
    void            **ppGpakCodPtr; // address of gpak Vocoder pointer
    void            **ppGpakEcPtr;  // address of gpak Echo Canceller pointer
    ADT_UInt16      *pTccCodes;
    dmaXferParams_t dmaXferParams[MAX_NUM_XFERS];
} dmaXferInfo_t;


// define the DMA control structure. Used in the framing task and conference
typedef struct
{
    int loadIndex;         // index into internal memory queue for DMA loads
    int storeIndex;        // index into internal memory queue for DMA stores
    int procIndex;         // index into internal memory queue for processing
    int groupFlag;         // 0 == group A, 1 == group B
    int rdChan;            // Outstanding read channel
    int wrChan;            // Outstanding write channel
    int ptrUpdateChan;     // Outstanding pointer update channel
    
    dmaXferInfo_t *pLoad;  // pointer to DMA information structure for loads
    dmaXferInfo_t *pStore; // pointer to DMA information structure for stores
} dmaControl_t;

// dma transfer infoformation for pcm-input processing internal memory loads
extern dmaXferInfo_t xferInfoLoadA[];  
// dma transfer infoformation for pcm-output processing internal memory loads
extern dmaXferInfo_t xferInfoLoadB[];  
// dma transfer infoformation for pcm-input processing internal memory stores
extern dmaXferInfo_t xferInfoStoreA[]; 
// dma transfer infoformation for pcm-output processing internal memory loads
extern dmaXferInfo_t xferInfoStoreB[]; 

// This function sets up the DMA information structures that specify the 
// details of how channel memory is DMA'd between external and internal memory.
// It's called at channel setup after all echo cancellers have been allocated
// and initialized.
// 
void dmaInitInfoStruct(
    chanInfo_t *pGpakChan, // Gpak channel structure
    ADT_UInt16 EcIndexA,   // index to "A-direction" echo canceller instance
    ADT_UInt16 EcIndexB);  // index to "B-direction" echo canceller instance

// wait for current transfer to complete. Fails if transfer is not ready 
// within timeout period. Return of 1 indicates trsnafer completed
void dmaWaitforXferComplete(
    dmaControl_t *dmaCtrl, // pointer to dma control structure
    int rdFlag);           // 1 == DMA rd from external memory

// This function starts a DMA transfer
void dmaXfer(
    dmaControl_t *dmaCtrl,  // pointer to dma control structure
    int channelId,          // gpak channel ID for this transfer
    int rdFlag);            // 1 == DMA rd from external memory


// This function loads the relevent gpak instance pointers with corresponding
// internal memory buffer pointers
void dmaSetPtrs(
    dmaControl_t    *dmaCtrl,    // pointer to dma control struct
    chanInfo_t      *pGpakChan,  // pointer to gpak channel struct
    int             FrameSize);  // frame size

// this function initializes a dma control structure
void dmaInitCtrl(
    dmaControl_t *dmaCtrl, // pointer to dma control struct
    int group);            // 1 == group B, 0 == group A

// This function invalidates L1 Cache for those components that have non-zero
// pointers
void dmaInvalidateL1Cache(
    ecInstanceInfo_t *pEcInfo, // Echo canceller Instance Info ptr
    int EcIndex,               // echo canceller instance index
    short int *encPtr,         // encoder chan ptr
    GpakCodecs encCoding,      // encoder type
    short int *decPtr,         // decoder chan ptr
    GpakCodecs decCoding);     // decoder type
        
#endif
