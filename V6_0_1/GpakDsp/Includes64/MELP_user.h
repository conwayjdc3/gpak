#ifndef MELPADT_USER_H_
#define MELPADT_USER_H_
/* TYPE DEFINITIONS */
#include "adt_typedef.h"
/* ANALYSIS CHANNEL */
typedef struct
{
  ADT_Int32 Space[621];
} MELP_AnaChannel_t;

/* SYNTHESIS CHANNEL */
typedef struct
{
  ADT_Int32 Space[227];
} MELP_SynChannel_t;

/* SCRATCH REGION */
typedef struct {
  ADT_Int32 Space[766];
} MELP_Scratch_t;

#define MELP_FRAME_SIZE 180
#define MELP_COMPRESS_SIZE 9

void MELP_ADT_initEnc(MELP_AnaChannel_t *MELP_AnaChannel, MELP_Scratch_t *MELP_Scratch);
void MELP_ADT_initDec(MELP_SynChannel_t *MELP_SynChannel, MELP_Scratch_t *MELP_Scratch);

void MELP_ADT_encode (MELP_AnaChannel_t *MELP_AnaChannel, ADT_Int16 speech_in[], ADT_UInt16 chbuf[]);
void MELP_ADT_decode (MELP_SynChannel_t *MELP_SynChannel, ADT_UInt16 chbuf[], ADT_Int16 speech_out[]);

#endif

