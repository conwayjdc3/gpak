#ifndef DMA_PRI_H
#define DMA_PRI_H

#define L2_PRI      0   // L2 controller dma priority
#define RX0_PRI     2   // mcbsp0 rx edma priority
#define RX1_PRI     2   // mcbsp1 rx edma priority
#define RX2_PRI     2   // mcbsp2 rx edma priority
#define TX0_PRI     1   // mcbsp0 tx edma priority
#define TX1_PRI     1   // mcbsp1 tx edma priority
#define TX2_PRI     1   // mcbsp2 tx edma priority
#define ALGRD_PRI   3 // 1 algorithm read chained dma priority
#define ALGWR_PRI   3 // 2 algorithm write chained dma priority
#endif

