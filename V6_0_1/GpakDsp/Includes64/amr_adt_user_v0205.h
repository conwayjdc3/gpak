#ifndef AMR_ADT_USER_H
#define AMR_ADT_USER_H
#include "adt_typedef.h"
#define OFF 0
#define ON 1
#define L_FRAME      160       /* Frame size                               */
#define MAX_SERIAL_SIZE 244    /* max. num. of serial bits                 */
#define MR475 0
#define	MR515 1
#define MR59 2
#define	MR67 3
#define MR74 4
#define MR795 5
#define MR102 6
#define MR122 7
#define MRDTX 8
#define N_MODES 9

#define AMR_TX_SPEECH  0
#define AMR_TX_SID_FIRST 1
#define AMR_TX_SID_UPDATE 2
#define AMR_TX_NO_DATA 3

//bit stream type
#define BITSTREAM_PARAS 0 //Parameter Array
#define BITSTREAM_PACKED 1  // packed bitstream
#define BITSTREAM_PACKED_ORDERED 2 // packed and ordered bitstream

typedef struct 
{
#ifndef VAD2
	ADT_Int64 Channel_data[262];	
#else
	ADT_Int64 Channel_data[271];	
#endif
} AMREncodeChannelInstance_t; 

typedef struct 
{
	ADT_Int64 Channel_data[1160];	
} AMREncodeScratchInstance_t; 


void AMR_ADT_encInit (AMREncodeChannelInstance_t *state, ADT_Int16 dtx_flag, AMREncodeScratchInstance_t *Scratch);
void AMR_ADT_encode (AMREncodeChannelInstance_t *ptrInst, 
					ADT_Int16 mode, 
					ADT_Int16 dtx, 
					ADT_Int16 new_speech[], 
					ADT_Int16 ana[], 
					ADT_Int16* usedMode, 
					ADT_Int16   *TxFrType,
                    ADT_Int16    Output_type);
                    
void AMR_ADT_sid_sync (AMREncodeChannelInstance_t *ptrInst, ADT_Int16 mode, ADT_Int16  *tx_frame_type);    


typedef struct 
{
	ADT_Int64 Channel_data[154];	
} AMRDecodeChannelInstance_t; 

typedef struct 
{
	ADT_Int64 Channel_data[174];	
} AMRDecodeScratchInstance_t; 

#define MAX_SERIAL_SIZE 244    /* max. num. of serial bits                 */
#define EHF_MASK 0x0008        /* encoder homing frame pattern     */        

#define MAX_PRM_SIZE    57     /* max. num. of params                      */
#define L_FRAME      160       /* Frame size                               */

enum RXFrameType { RX_SPEECH_GOOD = 0,
                   RX_SPEECH_DEGRADED,
                   RX_ONSET,
                   RX_SPEECH_BAD,
                   RX_SID_FIRST,
                   RX_SID_UPDATE,
                   RX_SID_BAD,
                   RX_NO_DATA,
                   RX_N_FRAMETYPES     /* number of frame types */
                 };

// decoder channel init
void AMR_ADT_decInit (AMRDecodeChannelInstance_t *state, AMRDecodeScratchInstance_t *Scratch);
// decoder function
void AMR_ADT_decode(AMRDecodeChannelInstance_t *st, 
					ADT_Int16 mode, 
					ADT_Int16 *serial,
                    ADT_Int16 frame_type, 
                    ADT_Int16 speech[], 
                    ADT_Int16 InputType);

// utilities
void AMR_ADT_getMode(AMRDecodeChannelInstance_t *AMRDecInst,ADT_Int16 fr_type,ADT_Int16 *mode);
void AMR_ADT_bits2prm (
    ADT_Int16 mode,     /* i : AMR mode                                    */
    ADT_Int16 bits[],      /* i : serial bits       (size <= MAX_SERIAL_SIZE) */
    ADT_Int16 prm[]        /* o : analysis parameters  (size <= MAX_PRM_SIZE) */
);

#endif
