#ifndef _AEC_USER_H
#define _AEC_USER_H
#if 0

#define SB_TABLES_SIZE ((SB_DEC_MAX_FIRLEN+2*SB_REC_MAX_FIRLEN+128)*sizeof(float))

#define SB_DATA_SIZE ((32*SB_MAX_SUBFRAME_SIZE) * sizeof(float))
#define SB_DEC_SCRATCH_SIZE ((64+SB_DEC_MAX_FIRLEN) * sizeof(float)) 
#define SB_REC_SCRATCH_SIZE (2*SB_MAX_SUPERFRAME_SIZE*sizeof(float))
#define AEC_SCRATCH_SIZE (5*SB_DATA_SIZE+SB_DEC_SCRATCH_SIZE+SB_REC_SCRATCH_SIZE)


#define SB_DEC_SIZE ((SB_DEC_MAX_FIRLEN+SB_MAX_SUPERFRAME_SIZE) * sizeof(float) + 264 + 8)
#define SB_REC_SIZE (54*(SB_REC_MAX_FIRLEN+SB_MAX_SUPERFRAME_SIZE)/8*sizeof(float) + 368 + 8)
#define LMS_SIZE ((4*LMS_MAX_TAPS+SB_MAX_SUBFRAME_SIZE)*sizeof(float) + 36 + 8)
//#define AEC_SIZE (2*SB_DEC_SIZE + SB_REC_SIZE + 27*LMS_SIZE + 128*sizeof(float) + 1944)
#define AEC_SIZE 100000
#endif
#define AEC_SCRATCH_SIZE 6720
#define AEC_SIZE 39956

typedef struct
{
	double Space[AEC_SCRATCH_SIZE/sizeof(double)];	
}	AEC_Scratch_t;

typedef struct
{
	double Space[AEC_SIZE/sizeof(double)];
}	AEC_t;

#define Void void
#include "std.h"
#include "iaec.h"

void AEC_ADT_initAEC(AEC_t *pAEC,  IAEC_Params *pAECParams, AEC_Scratch_t *pAECScratch);
void AEC_ADT_configAEC(IAEC_Params *pAECParams, AEC_Scratch_t *pScratch);
void AEC_ADT_runAEC(AEC_t *pAEC, ADT_Int16 *Far, ADT_Int16 *Near, ADT_Int16 *NearOut);
int AEC_ADT_getStructSize(IAEC_Params *pAECParams);
int AEC_ADT_getScratchSize(IAEC_Params *pAECParams);
int AEC_ADT_getInitScratchSize(IAEC_Params *pAECParams);
#endif // _AEC_USER_H
