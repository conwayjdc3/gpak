/*****************************************************************************
*----------------------------------------------------------------------------*
*                     TEXAS INSTRUMENTS INC                                  *
*                     Multimedia Codecs Group                                *
*                                                                            *
*----------------------------------------------------------------------------*
*                                                                            *
* (C) Copyright 2006  Texas Instruments Inc.  All rights reserved.           *
*                                                                            *
* Exclusive property of the Multimedia Codecs Group, Texas Instruments Inc.  *
* Any handling, use, disclosure, reproduction, duplication, transmission     *
* or storage of any part of this work by any means is subject to restrictions*
* and prior written permission set forth.                                    *
*                                                                            *
* This copyright notice, restricted rights legend, or other proprietary      *
* markings must be reproduced without modification in any authorized copies  *
* of any part of this work. Removal or modification of any part of this      *
* notice is prohibited.                                                      *
*                                                                            *
*****************************************************************************/

/***********************************************************************
 *   File: g7231aenc_tii.h                                             *
 *                                                                     *
 *   (c) Copyright 2006, Texas Instruments Inc.                        *
 ***********************************************************************/
#ifndef _G7231AENC_TII_H
#define _G7231AENC_TII_H

#include "ti/xdais/dm/isphenc1.h"
#include "ispeech1_g723.h"

/*         
 *  ======== G7231AENC_TII_IALG ========
 *  TII's implementation of the IALG interface for G7231A
 */
extern IALG_Fxns G7231AENC_TII_IALG;

/*
 *  ======== G7231AENC_TII_IG7231AENC ========
 *  TII's implementation of the IG7231A interface
 */
extern ISPHENC1_Fxns G7231AENC_TII_IG7231AENC;

/*
 *  ======== G7231AENC_TII_alloc ========
 */
extern int G7231AENC_TII_alloc (const IALG_Params * instParams,
                                struct IALG_Fxns **fxns, IALG_MemRec memTab[]);

/*
 *  ======== G7231AENC_TII_exit ========
 */
extern Void G7231AENC_TII_exit (Void);

/*
 *  ======== G7231AENC_TII_free ========
 */
extern int G7231AENC_TII_free (IALG_Handle handle, IALG_MemRec memTab[]);

/*
 *  ======== G7231AENC_TII_init ========
 */
extern Void G7231AENC_TII_init (Void);

/*
 *  ======== G7231AENC_TII_initObj ========
 */
extern int G7231AENC_TII_initObj (IALG_Handle handle,
                                  const IALG_MemRec memTab[],
                                  IALG_Handle pHandle,
                                  const IALG_Params * instParams);
/*
 *  ======== G7231AENC_TII_numAlloc ========
 */
extern int G7231AENC_TII_numAlloc (Void);

/*
 *  ======== G7231AENC_TII_moved ========
 */

extern Void G7231AENC_TII_moved (IALG_Handle handle,
                                 const IALG_MemRec memTab[],
                                 IALG_Handle pHandle,
                                 const IALG_Params * instParams);
/*
 *  ======== G7231AENC_TII_G7231AControl ========
 */
extern XDAS_Int32 G7231AENC_TII_g7231aControl (ISPHENC1_Handle handle,
                                               ISPHENC1_Cmd id, 
                                               ISPHENC1_DynamicParams *params,
                                               ISPHENC1_Status *status);
/*
 *  ======== G7231AENC_TII_G7231AEncode ========
 */
extern XDAS_Int32 G7231AENC_TII_g7231aEncode (ISPHENC1_Handle handle,  
                                       XDM1_SingleBufDesc *inBufs,
                                       XDM1_SingleBufDesc *outBufs, 
                                       ISPHENC1_InArgs *inargs, 
                                       ISPHENC1_OutArgs *outargs);

#endif /* G7231AENC_TII_ */
