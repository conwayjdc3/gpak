/*******************************************************************************
*-----------------------------------------------------------------------------*
*                     TEXAS INSTRUMENTS INC                                   *
*                     Multimedia Codecs Group                                 *
*                                                                             *
*-----------------------------------------------------------------------------*
*                                                                             *
* (C) Copyright 2006  Texas Instruments Inc.  All rights reserved.            *
*                                                                             *
* Exclusive property of the Multimedia Codecs Group, Texas Instruments Inc.   *
* Any handling, use, disclosure, reproduction, duplication, transmission      *
* or storage of any part of this work by any means is subject to restrictions *
* and prior written permission set forth.                                     *
*                                                                             *
* This copyright notice, restricted rights legend, or other proprietary       *
* markings must be reproduced without modification in any authorized copies   *
* of any part of this work. Removal or modification of any part of this       *
* notice is prohibited.                                                       *
*                                                                             *
*******************************************************************************/

/***********************************************************************
 *   File: g7231adec_tii.h                                              *
 *                                                                     *
 *   (c) Copyright 2006, Texas Instruments Inc.                        *
 ***********************************************************************/
#ifndef G7231ADEC_TII_
#define G7231ADEC_TII_

#include "ti/xdais/dm/isphdec1.h"
#include "ispeech1_g723.h"

/*
 *  ======== G7231ADEC_TII_IALG ========
 *  TII's implementation of the IALG interface for G7231A
 */

extern IALG_Fxns G7231ADEC_TII_IALG;

/*
 *  ======== G7231ADEC_TII_IG7231ADEC ========
 *  TII's implementation of the IG7231A interface
 */

extern ISPHDEC1_Fxns G7231ADEC_TII_IG7231ADEC;

/*
 *  ======== G7231ADEC_TII_alloc ========
 */

extern Int G7231ADEC_TII_alloc (const IALG_Params * instParams,
                                struct IALG_Fxns **fxns, IALG_MemRec memTab[]);

/*
 *  ======== G7231ADEC_TII_exit ========
 */

extern Void G7231ADEC_TII_exit (Void);

/*
 *  ======== G7231ADEC_TII_free ========
 */

extern Int G7231ADEC_TII_free (IALG_Handle handle, IALG_MemRec memTab[]);

/*
 *  ======== G7231ADEC_TII_init ========
 */

extern Void G7231ADEC_TII_init (Void);

/*
 *  ======== G7231ADEC_TII_initObj ========
 */

extern Int G7231ADEC_TII_initObj (IALG_Handle handle,
                                  const IALG_MemRec memTab[],
                                  IALG_Handle pHandle,
                                  const IALG_Params * instParams);

/*
 *  ======== G7231ADEC_TII_numAlloc ========
 */

extern Int G7231ADEC_TII_numAlloc (Void);

/*
 *  ======== G7231ADEC_TII_moved ========
 */

extern Void G7231ADEC_TII_moved (IALG_Handle handle,
                                 const IALG_MemRec memTab[],
                                 IALG_Handle pHandle,
                                 const IALG_Params * instParams);

/*
 *  ======== G7231ADEC_TII_g7231aControl ========
 */

extern XDAS_Int32 G7231ADEC_TII_g7231aControl (ISPHDEC1_Handle handle, 
                                        ISPHDEC1_Cmd id, 
                                        ISPHDEC1_DynamicParams *params,
                                        ISPHDEC1_Status *status);

/*
 *  ======== G7231ADEC_TII_g7231aDecode ========
 */

extern XDAS_Int32 G7231ADEC_TII_g7231aDecode (ISPHDEC1_Handle handle, 
                                       XDM1_SingleBufDesc *inBufs, 
                                       XDM1_SingleBufDesc *outBufs, 
                                       ISPHDEC1_InArgs *inArgs, 
                                       ISPHDEC1_OutArgs *outArgs);

#endif /* G7231ADEC_TII_ */
