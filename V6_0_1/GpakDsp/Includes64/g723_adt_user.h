#ifndef G723_ADT_USER_H

#define G723_ADT_USER_H
#include "adt_typedef.h"
//G723 Encoder channel instance:

typedef struct {
	Word32 G723EncData[355]; 
} G723ENC_ChannelInst_t;

// G723 Encoder Scratch instance
typedef struct {
	Word32 Data[1220];	
} G723ENC_Scratch_t;

typedef struct {
	Word32 Data[100];
} G723DEC_ChannelInst_t;

// G723 Decoder Scratch instance
typedef struct {
	Word32 Data[608]; 
} G723DEC_Scratch_t;

#ifndef CONTROLDEF_IN_H
#define CONTROLDEF_IN_H
typedef struct
{
	Word16 WrkMode ;
	Word16 WrkRate ;
	Flag   UseHp ;
	Flag   UsePf ;
	Flag   UseVx ;

} CONTROLDEF ;	// sizeof(DECCNGDEF) = 10

/* Definition of the working mode */
//enum Wmode { Both, Cod, Dec } ;
#define Both			0
#define Cod				1
#define Dec				2
/* Coder rate */
//enum Crate { Rate63, Rate53 } ;
#define Rate63			0
#define Rate53			1

#define False			0
#define True			1

/* Coder global constants */
#define FrameLen		240

#endif



// G723 Encoder function protypes
void G723_ADT_initEnc(G723ENC_ChannelInst_t *Channel_Inst, 
					G723ENC_Scratch_t *Scratch
					);		
void G723_ADT_encode (  G723ENC_ChannelInst_t *Channel_Inst, 
				CONTROLDEF *ConTrol,
				Word16 Input[], 
				Word32 Stream[]
);

// G723 Decoder function protypes
void G723_ADT_initDec(G723DEC_ChannelInst_t *Channel_Inst, 
					G723DEC_Scratch_t *Scratch
					);		

void G723_ADT_decode(G723DEC_ChannelInst_t *Channel_Inst, 
    CONTROLDEF *ConTrol,
    Word32 Stream[], 
    Word16 Output[], 
    Word16 FrameEraseFlag
    );
    
Word16 G723_ADT_getSize4Decoder(Word16 *Stream);

#endif
