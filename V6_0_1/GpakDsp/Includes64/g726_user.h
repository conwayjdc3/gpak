#ifndef _G726_USER_H
#define _G726_USER_H

#define ULAW 0
#define ALAW 1
#define LINEAR 2


typedef struct
{
	ADT_UInt32 Channel_data[19]; 
} G726ChannelInstance_t; 


int G726_ADT_Init(G726ChannelInstance_t  *vpState, int rate, int law);
unsigned char G726_ADT_Encode(G726ChannelInstance_t  *vpState, unsigned char cI);
unsigned char G726_ADT_Decode(G726ChannelInstance_t  *vpState, unsigned char cI);





//INT16 encode_g726 (INT16 pcm_in, INT16 rate, INT16 encode_law);  
//INT16 decode_g726 (INT16 pcm_in, INT16 rate, INT16 decode_law);
//INT16 QUAN(INT16 Rate);
//INT16 encode_g711 (INT16 input, INT16 law, INT16 sign);
//INT16 decode_g711 (INT16 code, INT16 law);


#endif
