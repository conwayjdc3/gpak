/*___________________________________________________________________________
 |
 |   File: G165blk.h
 |
 |   This file is the Adaptive Digital Technologies, Inc. 
 |   private header file for the 2100Hz tone detectors.
 |   
 |   This software is the property of Adaptive Digital Technologies, Inc.
 |   Unauthorized use is prohibited.
 |
 |   Copyright (c) 2000-2001, Adaptive Digital Technologies, Inc.
 |
 |   Version 2
 |   July 17, 2001
 |
 |   www.adt-inc.com
 |   610-825-0182
 |___________________________________________________________________________
*/
#ifndef _G165BLK_H
#define _G165BLK_H
#include "ADT_typedef.h"
/******************************************************************/
/* Following are declarements for DTMF method to detect 2100Hz */

#define G165DFT_N 80  


typedef struct 
{
/* Outputs */
	ADT_Int16 G164Flag;
	ADT_Int16 G165Flag;
/* States */
//	ADT_Int16 DetectorState;
	ADT_Int32 Signal;
	ADT_Int32 SignalPlusNoise;
	ADT_Int16 G164Count;
	ADT_Int16 G165Count;
	ADT_Int16 G164State;
	ADT_Int16 G165State;
//	ADT_Int16 GorState0;
//	ADT_Int16 GorState1;
	ADT_Int16 G165SampleCount;
	ADT_Int16 PrevPhase0;
	ADT_Int16 PrevPhase1;
	ADT_Int16 AvgPhase;
	ADT_Int16 PhaseVariance;
	ADT_Int16 PrevPhaseVariance;
	ADT_Int16 PhaseError;
/******************************************************************/
/* Following are new elements for DTMF method to detect 2100Hz */
	ADT_Int16 DftN;    // keep track of samples no. already done Dft
    ADT_Int16 *DFTCosPtr;
    ADT_Int16 *DFTSinePtr;
	ADT_Int16 CosSum; // CosSum and SinSum: I and Q 
	ADT_Int16 SinSum;
/* Above are new elements for DTMF method to detect 2100Hz */
/******************************************************************/
	ADT_Int16 G165ReleaseFlag; // added to Re_Enable EchoCanceller, huafeng add
	ADT_Int16 G165ReleaseCount; // release signal should hold at least 100ms
	ADT_Int16 G165ReleaseState; // release state machine
	//ADT_Int16 G165FlagLatched; // move this flag from g168channel, huafeng add/delete
}	G165Channel_t;   


void LEC_ADT_g165DetectInit(G165Channel_t *Channel);

void LEC_ADT_g165Detect(G165Channel_t *G165Channel,
							ADT_Int16 *InputSamples, 
							ADT_Int16 NInputSamples);

void LEC_ADT_g165ReleaseCheck(G165Channel_t *G165Channel, 
						 ADT_Int16 *InputSamples, 
						 ADT_Int16 NInputSamples);
#endif // ifndef _G165BLK_H  
