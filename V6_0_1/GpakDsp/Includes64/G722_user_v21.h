#ifndef G722_USER_H
#define G722_USER_H
#include "adt_typedef.h"


typedef struct
{	 
	ADT_Int64 Channel_data[24];	/*short int Channel_data[96];*/

} G722ChannelInstance_t; 

void G722_ADT_reset(G722ChannelInstance_t *Channel_Ins, short int Mode);

void G722_ADT_encode (G722ChannelInstance_t *Channel_Ins, short int in_array[], short int *EncodeWord);
void G722_ADT_decode (G722ChannelInstance_t *Channel_Ins, short int DecodeWord, short int out_array[]);

#endif

