#ifndef INTDEC_ADT_USER_H_
#define INTDEC_ADT_USER_H_

#include "adt_typedef.h"

void INTDEC_ADT_init(ADT_Int16 *InterpFIRState, ADT_Int16 *decimFIRState);
void INTDEC_ADT_decimate(ADT_Int16 *pState, ADT_Int16 *pInput, ADT_Int16 NInput, ADT_Int16 *pFIRScratch, ADT_Int16 *pOutput);
void INTDEC_ADT_interpolate(ADT_Int16 *pState, ADT_Int16 *pInput, ADT_Int16 NInput, ADT_Int16 *pFIRScratch, ADT_Int16 *pOutput);

/* interpolation and decimation filter length */
#define FIRLEN 64 

#endif
