#ifndef _667x_h
#define _667x_h
/*
 * Copyright (c) 2008, Adaptive Digital Technologies, Inc.
 *
 * File Name: L1x.h
 *
 * Description:
 *    This file contains the register definitions for the L1x family of processors
 *    that are needed specifically by the L1x drivers.
 *
 * Version: 1.0
 *
 * Revision History:
 *   05/23/2008 - Initial release.
 *
 *
 */
#include <soc.h>

typedef ADT_UInt16 ADT_Events;

#define REG_WR(addr, val) *(volatile ADT_UInt32 *)(addr) = (ADT_UInt32) val
#define REG_RD(addr, val) val = *(volatile ADT_UInt32 *)(addr) 
#define REG_AND(addr,val) *(volatile ADT_UInt32 *)(addr) &= val
#define REG_OR(addr,val)  *(volatile ADT_UInt32 *)(addr) = (*(volatile ADT_UInt32 *)addr) | val

#define REG64_WR(addr,val) *(volatile ADT_Int64 *)(addr) = (ADT_Int64) (val)
#define REG64_RD(addr,val) val = *(volatile ADT_Int64 *) (addr)

//---------------------------------------------
//   Host interface registers
//{---------------------------------------------

//}------------------------------------------------------------------------------------
// EDMA registers
//{------------------------------------------------------------------------------------
// Configuration
//#define EDMA_CFG_BASE  (CSL_EDMA30CC_0_REGS)  // base address of global EDMA3 registers
//SS Cornet
//#define EDMA_CFG_BASE  (CSL_EDMA3CC_REGS)  // base address of global EDMA3 registers
#define EDMA_CFG_BASE  (CSL_EDMA3CC_1_REGS)  // base address of global EDMA3 registers


#define EDMA_CFG       (EDMA_CFG_BASE + 0x0004) // Configuration sizing
#define EDMA_CH_MAP    (EDMA_CFG_BASE + 0x0100) // Channel to parameter mapping
#define EDMA_WATERMARK (EDMA_CFG_BASE + 0x0620) // Watermark warning thresholds

// Event queueing
#define EDMA_QUE0     (EDMA_CFG_BASE + 0x0240) // Transfer controller queue assignment register
#define EDMA_QUEPRI   (EDMA_CFG_BASE + 0x0284) // Transfer controller queue priority register

// Missed event notification
#define EDMA_EMR      (EDMA_CFG_BASE + 0x0300) // Missed event notification
#define EDMA_EMRH     (EDMA_CFG_BASE + 0x0304)
#define EDMA_EMCR     (EDMA_CFG_BASE + 0x0308) // Missed event notification clear
#define EDMA_EMCRH    (EDMA_CFG_BASE + 0x030C)

// Error notification
#define EDMA_CCERR    (EDMA_CFG_BASE + 0x0318) // TC queue overrun error
#define EDMA_CCERRCLR (EDMA_CFG_BASE + 0x031C) // TC queue overrun error clear
#define EDMA_ERREVAL  (EDMA_CFG_BASE + 0x0320) // TC queue overrun error eval

// NOTE:  DMA Queue_n is assigned to TransferController_n
// Assign dma channels to a specified queue / transfer controlller
#define EDMA_QUE_TDM_TX 2
#define EDMA_QUE_TDM_RX 1

#define assignToTCQueue(chn,que)           \
   {  ADT_UInt32 *asgReg, *cfgReg;         \
      ADT_UInt32 value, asgMask;           \
      asgReg = (ADT_UInt32 *) EDMA_QUE0;   \
      asgReg += (chn)>>3;                  \
      REG_RD (asgReg, value);              \
      asgMask = 7 << (((chn)&7)*4);        \
      value &= ~asgMask;                   \
      value |= (que) << (((chn)&7)*4);     \
      REG_WR (asgReg, value);              \
      REG_RD (EDMA_CFG, value);            \
      if (value & 0x01000000) {            \
         cfgReg = (ADT_UInt32 *) (EDMA_CH_MAP + (chn)*4); \
         REG_WR (cfgReg, (chn) << 5);      \
      }                                    \
   }

//}---------------------------
// EDMA events and event registers
//{---------------------------
#define McASPRx0_Chn  (CSL_EDMA3_CHA_MCASP0_RX)
#define McASPTx0_Chn  (CSL_EDMA3_CHA_MCASP0_TX)

//EDM3 user guide
#define McBSPTx0_Chn  (CSL_EDMA3CC2_XEVT0_MCBSP_A)
#define McBSPRx0_Chn  (CSL_EDMA3CC2_REVT0_MCBSP_A)
#define McBSPTx1_Chn  (CSL_EDMA3CC2_XEVT1_MCBSP_B)
#define McBSPRx1_Chn  (CSL_EDMA3CC2_REVT1_MCBSP_B)

#define EDMA_GLOBAL_BASE (EDMA_CFG_BASE + 0x1000)  // base address of global region registers

// Shadow region assignment
#define EDMA_SHAD0    (EDMA_CFG_BASE + 0x0340) // Shadow region 0 assignment register
#define EDMA_SHAD0H   (EDMA_CFG_BASE + 0x0344)
#define EDMA_SHAD1    (EDMA_CFG_BASE + 0x0348) // Shadow region 1 assignment register
#define EDMA_SHAD1H   (EDMA_CFG_BASE + 0x034C)
#define EDMA_SHAD_DMA  EDMA_SHAD1              //
#define EDMA_SHAD_DMAH EDMA_SHAD1H
//SS Verify  #define EDMA_SHADOW_BASE (EDMA_CFG_BASE + 0x2200)  // base address of shadow region01 registers

//}---------------------------
// EDMA event registers
//{---------------------------
#if SHADOW_REGION == 0
#define EDMA_SHADOW_BASE  (CSL_EDMA3CC_0_REGS + 0x2000)  // base address of shadow region 0 registers
   #define EDMA_SHADOW_ASGN  (EDMA_SHAD0)
   #define EDMA_SHADOW_ASGNH (EDMA_SHAD0H)
#elif SHADOW_REGION == 1
   #define EDMA_SHADOW_BASE  (CSL_EDMA3CC_1_REGS + 0x2200)  // base address of shadow region 1 registers
   #define EDMA_SHADOW_ASGN  (EDMA_SHAD1)
   #define EDMA_SHADOW_ASGNH (EDMA_SHAD1H)
#else
   #define EDMA_SHADOW_BASE  (CSL_EDMA3CC_0_REGS + 0x1000)  // base address of global region registers
   #undef EDMA_SHADOW_ASGN
#endif

// Event notification
#define EDMA_SHADOW_EPR  (EDMA_SHADOW_BASE + 0x00)   // Event pending
#define EDMA_SHADOW_EPCR (EDMA_SHADOW_BASE + 0x08)   // Event pending clear
#define EDMA_SHADOW_EPCRH (EDMA_SHADOW_BASE + 0x0C)   // Event pending clear
#define EDMA_SHADOW_EPSR (EDMA_SHADOW_BASE + 0x10)   // Event pendingn set
#define EDMA_SHADOW_EPSRH (EDMA_SHADOW_BASE + 0x14)   // Event pendingn set

//#define EDMA3_ENR  (EDMA_SHADOW_BASE + 0x00)   // Event notification
//#define EDMA3_ENCR (EDMA_SHADOW_BASE + 0x08)   // Event notification clear
//#define EDMA3_ENSR (EDMA_SHADOW_BASE + 0x10)   // Event notification set

// Event enabling
#define EDMA_SHADOW_EER   (EDMA_SHADOW_BASE + 0x20)  // Event enabled
#define EDMA_SHADOW_EECR  (EDMA_SHADOW_BASE + 0x28)  // Event enable clear
#define EDMA_SHADOW_EECRH (EDMA_SHADOW_BASE + 0x2C)
#define EDMA_SHADOW_EESR  (EDMA_SHADOW_BASE + 0x30)  // Event enable set
#define EDMA_SHADOW_EESRH (EDMA_SHADOW_BASE + 0x34)

// Event enabling
//#define EDMA3_EER  (EDMA_SHADOW_BASE + 0x20)  // Event enabled
//#define EDMA3_EECR (EDMA_SHADOW_BASE + 0x28)  // Event enable clear
//#define EDMA3_EESR (EDMA_SHADOW_BASE + 0x30)  // Event enable set

// Second occurance of event before clearing of first event
#define EDMA_SHADOW_SPR   (EDMA_SHADOW_BASE + 0x38)   // Secondary event pending
#define EDMA_SHADOW_SPRH  (EDMA_SHADOW_BASE + 0x3C)
#define EDMA_SHADOW_SPCR  (EDMA_SHADOW_BASE + 0x40)   // Secondary event pending clear
#define EDMA_SHADOW_SPCRH (EDMA_SHADOW_BASE + 0x44)

// Second occurance of event before clearing of first event
//#define EDMA3_SNR  (EDMA_SHADOW_BASE + 0x38)   // Secondary event notification
//#define EDMA3_SNCR (EDMA_SHADOW_BASE + 0x40)   // Secondary event notification clear

// Event interrupt enabling
#define EDMA_SHADOW_IER  (EDMA_SHADOW_BASE + 0x50)  // Event interrupt enabled
#define EDMA_SHADOW_IECR (EDMA_SHADOW_BASE + 0x58)  // Event interrupt enable clear
#define EDMA_SHADOW_IECRH (EDMA_SHADOW_BASE + 0x5C)  // Event interrupt enable clear
#define EDMA_SHADOW_IESR (EDMA_SHADOW_BASE + 0x60)  // Event interrupt enable set
#define EDMA_SHADOW_IESRH (EDMA_SHADOW_BASE + 0x64)  // Event interrupt enable set

// Event interrupt enabling
//#define EDMA3_IER  (EDMA_SHADOW_BASE + 0x50)  // Event interrupt enabled
//#define EDMA3_IECR (EDMA_SHADOW_BASE + 0x58)  // Event interrupt enable clear
//#define EDMA3_IESR (EDMA_SHADOW_BASE + 0x60)  // Event interrupt enable set

// Interrupt notification
#define EDMA_SHADOW_IPR  (EDMA_SHADOW_BASE + 0x68)   // Interrupt pending
#define EDMA_SHADOW_IPRH (EDMA_SHADOW_BASE + 0x6C)   // Interrupt pending
#define EDMA_SHADOW_IPCR (EDMA_SHADOW_BASE + 0x70)   // Interrupt pending clear
#define EDMA_SHADOW_IPCRH (EDMA_SHADOW_BASE + 0x74)   // Interrupt pending clear
#define EDMA_SHADOW_IEVL (EDMA_SHADOW_BASE + 0x78)   // Interrupt evaluator

// Interrupt notification
//#define EDMA3_INR  (EDMA_SHADOW_BASE + 0x68)   // Interrupt notification
//#define EDMA3_INCR (EDMA_SHADOW_BASE + 0x70)   // Interrupt notification clear
//#define EDMA3_IEVL (EDMA_SHADOW_BASE + 0x78)   // Interrupt evaluator

// DMA Global Clearing
#define EDMA_GLB_EPCR  (EDMA_GLOBAL_BASE + 0x08)  // Event pending clear
#define EDMA_GLB_EPCRH (EDMA_GLOBAL_BASE + 0x0C)
#define EDMA_GLB_EECR  (EDMA_GLOBAL_BASE + 0x28)  // Event enable clear
#define EDMA_GLB_EECRH (EDMA_GLOBAL_BASE + 0x2C)
#define EDMA_GLB_SPCR  (EDMA_GLOBAL_BASE + 0x40)  // Secondary event pending clear
#define EDMA_GLB_SPCRH (EDMA_GLOBAL_BASE + 0x44)
#define EDMA_GLB_IECR  (EDMA_GLOBAL_BASE + 0x58)  // Event interrupt enable clear
#define EDMA_GLB_IECRH (EDMA_GLOBAL_BASE + 0x5C)
#define EDMA_GLB_IPCR  (EDMA_GLOBAL_BASE + 0x70)  // Interrupt pending clear
#define EDMA_GLB_IPCRH (EDMA_GLOBAL_BASE + 0x74)
#define EDMA_GLB_IEVL  (EDMA_GLOBAL_BASE + 0x78)  // Interrupt evaluator

#define TSIP_RX_SF_INT0 (EVT_BIT(CSL_GEM_TSIP0_RSFINT_N))  // rx superframe0 event 0x35
#define TSIP_TX_SF_INT0 (EVT_BIT(CSL_GEM_TSIP0_XSFINT_N))  // tx superframe0 event 0x37
#define TSIP_RX_SF_INT1 (EVT_BIT(CSL_GEM_TSIP1_RSFINT_N))  // rx superframe1 event 0x39
#define TSIP_TX_SF_INT1 (EVT_BIT(CSL_GEM_TSIP1_XSFINT_N))  // tx superframe1 event 0x3b
#define TSIP_ERR_INT0   (EVT_BIT(CSL_GEM_TSIP0_ERRINT_N))  // error0 event 0x12
#define TSIP_ERR_INT1   (EVT_BIT(CSL_GEM_TSIP1_ERRINT_N))  // error1 event 0x13

#define TSIP_RX_INTRS      (TSIP_RX_SF_INT0 | TSIP_RX_SF_INT1 )
#define TSIP_TX_INTRS      (TSIP_TX_SF_INT0 | TSIP_TX_SF_INT1 )
#define TSIP_COMPLETE_EVTS (TSIP_RX_INTRS | TSIP_TX_INTRS)
#define TSIP_ERROR_EVTS    (TSIP_ERR_INT0 | TSIP_ERR_INT1 )
#define TSIP_EVTS          (TSIP_COMPLETE_EVTS | TSIP_ERROR_EVTS)
#ifdef CSL_TSIP_CNT

   #define TSIP_RX_SF_INT0 (EVT_BIT(CSL_INTC_EVENTID_RSFINT0))  // rx superframe0 event 33
   #define TSIP_TX_SF_INT0 (EVT_BIT(CSL_INTC_EVENTID_XSFINT0))  // tx superframe0 event 35
   #define TSIP_RX_SF_INT1 (EVT_BIT(CSL_INTC_EVENTID_RSFINT1))  // rx superframe1 event 37
   #define TSIP_TX_SF_INT1 (EVT_BIT(CSL_INTC_EVENTID_XSFINT1))  // tx superframe1 event 39
   #define TSIP_RX_SF_INT2 (EVT_BIT(CSL_INTC_EVENTID_RSFINT2))  // rx superframe2 event 41
   #define TSIP_TX_SF_INT2 (EVT_BIT(CSL_INTC_EVENTID_XSFINT2))  // tx superframe2 event 43
   #define TSIP_ERR_INT0   (EVT_BIT(CSL_INTC_EVENTID_ERRINT0))  // error0 event 50
   #define TSIP_ERR_INT1   (EVT_BIT(CSL_INTC_EVENTID_ERRINT1))  // error1 event 52
   #define TSIP_ERR_INT2   (EVT_BIT(CSL_INTC_EVENTID_ERRINT2))  // error2 event 54

   #define TSIP_RX_INTRS      (TSIP_RX_SF_INT0 | TSIP_RX_SF_INT1 | TSIP_RX_SF_INT2)
   #define TSIP_TX_INTRS      (TSIP_TX_SF_INT0 | TSIP_TX_SF_INT1 | TSIP_TX_SF_INT2)
   #define TSIP_COMPLETE_EVTS (TSIP_RX_INTRS | TSIP_TX_INTRS)
   #define TSIP_ERROR_EVTS    (TSIP_ERR_INT0 | TSIP_ERR_INT1 | TSIP_ERR_INT2)
   #define TSIP_EVTS          (TSIP_COMPLETE_EVTS | TSIP_ERROR_EVTS)
#endif

//}---------------------------
// EDMA parameter registers
//{---------------------------

//#define EDMA_PARAM_ADDR   (EDMA_CFG_BASE + 0x4000) // base address of EDMA parameter registers
#define EDMA_PARAM_ADDR   (CSL_EDMA3CC_1_REGS + 0x4000) // base address of EDMA parameter registers
#define PARAM_SIZE 32
#define PARAM_I8   32

//SS Cornet
//#define PARAM_CNT  CSL_EDMA3_NUM_DMACH
#define PARAM_CNT CSL_EDMA3_EDMA3CC2_NUM_DMACH

//=================================================
// G.Pak assignments
//{=================================================

//--------- Logical interrupts
#ifdef CSL_INTC_EVENTID_EDMA3CCINT_LOCAL
   #define TDM_CompleteSgnl  CSL_INTC_EVENTID_EDMA3CCINT_LOCAL  // Completion region 0
#else
   #define CSL_INTC_EVENTID_EDMA3CC_INT0  CSL_INTC0_CPU_3_2_EDMA3CCINT0
   #define TDM_CompleteSgnl  CSL_INTC_EVENTID_EDMA3CC_INT0      // Completion region 0
#endif

#ifndef CSL_INTC_EVENTID_EDMA3TC0_ERRINT
   #define CSL_INTC_EVENTID_EDMA3TC0_ERRINT CSL_INTC0_CPU_3_2_EDMATC_ERRINT0
   #define CSL_INTC_EVENTID_EDMA3TC1_ERRINT CSL_INTC0_CPU_3_2_EDMATC_ERRINT1
   #define CSL_INTC_EVENTID_EDMA3TC2_ERRINT CSL_INTC0_CPU_3_2_EDMATC_ERRINT2
#endif
#define CSL_INTC_EVENTID_EDMA3CC_ERRINT CSL_INTC0_CPU_3_2_EDMACC_ERRINT
#if 0
#ifndef CSL_INTC_EVENTID_EDMA3TC0_ERRINT
   #define CSL_INTC_EVENTID_EDMA3TC0_ERRINT CSL_INTC_EVENTID_EDMA3CC_ERRINT0
   #define CSL_INTC_EVENTID_EDMA3TC1_ERRINT CSL_INTC_EVENTID_EDMA3CC_ERRINT1
   #define CSL_INTC_EVENTID_EDMA3TC2_ERRINT CSL_INTC_EVENTID_EDMA3CC_ERRINT2
#endif

#endif

// Combined interrupt events

#define CombinedEventIntr (CSL_INTC_EVENTID_EDMA3CC_ERRINT/32)                // Combined interrupt for error event.

#define TDM_CCErrSgnl     CSL_INTC_EVENTID_EDMA3CC_ERRINT  // Channel controller error
#define TDM_Que0ErrSgnl   CSL_INTC_EVENTID_EDMA3TC0_ERRINT // Queue 0 error
#define TDM_Que1ErrSgnl   CSL_INTC_EVENTID_EDMA3TC1_ERRINT // Queue 1 error
#define TDM_Que2ErrSgnl   CSL_INTC_EVENTID_EDMA3TC2_ERRINT // Queue 2 error
#define DroppedIntrSgnl   CSL_INTC_EVENTID_INTERR          // Dropped interrupt error

#if 0 //SS
#define CombinedEventIntr (CSL_INTC0_CPU_3_2_EDMACC_ERRINT /32)                // Combined interrupt for error event.

#define TDM_CompleteSgnl  CSL_GEM_EVT0                      // Combined interrupt (0-31)
#define DMA_CompleteSgnl  CSL_INTC0_CPU_3_2_EDMA3CCINT0     // Channel controller interrupt
#define ErrSgnl           CSL_GEM_EVT1                      // Combined interrupt (56-58)
#define TDM_CCErrSgnl     CSL_INTC0_CPU_3_2_EDMACC_ERRINT   // Channel controller error
#define TDM_Que0ErrSgnl   CSL_INTC0_CPU_3_2_EDMATC_ERRINT0  // TC Queue 0 error
#define TDM_Que1ErrSgnl   CSL_INTC0_CPU_3_2_EDMATC_ERRINT1  // TC Queue 1 error
#define DroppedIntrSgnl   CSL_GEM_INTERR                    // Dropped interrupt error
#endif



#define EVT_BIT(evt)  (1 << (evt%32))
#define DMA_ERROR_EVTS  (EVT_BIT (TDM_CCErrSgnl)    | EVT_BIT (TDM_Que0ErrSgnl) | \
                         EVT_BIT (TDM_Que1ErrSgnl)  | EVT_BIT (TDM_Que2ErrSgnl))

#define assignToTC(chn,que)                \
   {  ADT_UInt32 *asgReg, *cfgReg;         \
      ADT_UInt32 value, asgMask;           \
      asgReg = (ADT_UInt32 *) EDMA_QUE0;   \
      asgReg += (chn)>>3;                  \
      REG_RD (asgReg, value);              \
      asgMask = 7 << (((chn)&7)*4);        \
      value &= ~asgMask;                   \
      value |= (que) << (((chn)&7)*4);     \
      REG_WR (asgReg, value);              \
      REG_RD (EDMA_CFG, value);            \
      if (value & 0x07000000) {            \
         cfgReg = (ADT_UInt32 *) (EDMA_CH_MAP + (chn)*4); \
         REG_WR (cfgReg, (chn) << 5);      \
      }                                    \
   }

#define assignPriority(que,priority)         \
   {  ADT_UInt32 *asgReg;                    \
      ADT_UInt32 value, asgMask;             \
      asgReg = (ADT_UInt32 *) EDMA_QUEPRI;   \
      asgMask = 7 << (que*4);                \
      REG_RD (asgReg, value);                \
      value &= ~asgMask;                     \
      value |= priority << (que*4);          \
      REG_WR (asgReg, value);                \
   }

//}------------------------------------------------------------------------------------
// Transfer controller registers
//{------------------------------------------------------------------------------------

// SS Cornet
//#define TC0_BASE (CSL_EDMA30TC_0_REGS)
//#define TC1_BASE (CSL_EDMA30TC_1_REGS)

//SSNoteC Switch Fabric limitation does not connect McBSP to TC1, TC2
//#define TC0_BASE (CSL_EDMA3TC0_REGS)
//#define TC1_BASE (CSL_EDMA3TC1_REGS)
//#define TC2_BASE (CSL_EDMA3TC2_REGS)

#define TC0_BASE CSL_EDMA0TC0_REGS
#define TC1_BASE CSL_EDMA0TC1_REGS

#define TC0_ERR_STAT (TC0_BASE + 0x0120) // Transfer control error status
#define TC1_ERR_STAT (TC1_BASE + 0x0120) //
#define TC2_ERR_STAT (TC2_BASE + 0x0120) //

#define TC0_ERR_STAT_ENBL (TC0_BASE + 0x0124) // Transfer control error status enable
#define TC1_ERR_STAT_ENBL (TC1_BASE + 0x0124) //
#define TC2_ERR_STAT_ENBL (TC2_BASE + 0x0124) //

#define TC0_ERR_STAT_CLR  (TC0_BASE + 0x0128) // Transfer control error status clear
#define TC1_ERR_STAT_CLR  (TC1_BASE + 0x0128) //
#define TC2_ERR_STAT_CLR  (TC2_BASE + 0x0128) //

#define TC0_ERR  (TC0_BASE + 0x012C) // Transfer control error details
#define TC1_ERR  (TC1_BASE + 0x012C) // 
#define TC2_ERR  (TC2_BASE + 0x012C) //

#define TC0_EVAL (TC0_BASE + 0x0130) // Transfer control error pulse
#define TC1_EVAL (TC1_BASE + 0x0130) // 
#define TC2_EVAL (TC2_BASE + 0x0130) //

#define TC1_READ_RATE (TC1_BASE + 0x0140)
#define TC2_READ_RATE (TC2_BASE + 0x0140)

#define TC1_CFG (TC1_BASE + 0x0004)
#define TC2_CFG (TC2_BASE + 0x0004)
//}------------------------------------------------------------------------------------
// CC and TCC register structure
//{------------------------------------------------------------------------------------
// TCC parameter options
typedef union dma3Opts_t {
    ADT_UInt32 TransferOptions;
    struct DmaOptBits {
        unsigned SAM:1;      // Constant source addressing
        unsigned DAM:1;      // Constant destination addressing
        unsigned SYNCDIM:1;  // A-B transfers
        unsigned STATIC:1;   // PARAM is not updated after TR is submitted (0 should be used for DMA)

        unsigned rsvd1:4;

        unsigned FWID:3;     // FIFO width
        unsigned TCCMOD:1;   // Early transfer complete (TR submitted)

        unsigned TCC:6;      // Transfer complete event
        unsigned rsvd2:2;

        unsigned TCINTEN:1;  // Generate IPR on final TRs
        unsigned ITCINTEN:1; // Generate IPR on intermediate TRs
        unsigned TCCHEN:1;   // Generate CER on final TRs
        unsigned ITCCHEN:1;  // Generate CER on intermediate TRs

        unsigned PRIVID:4;   // Read only - priviledge of requestor

        unsigned rsvd3:4;
    } Bits;
} dma3Opts_t;

typedef struct DMAChanQue_t {
   ADT_UInt32 Evt0Queue:4;
   ADT_UInt32 Evt1Queue:4;
   ADT_UInt32 Evt2Queue:4;
   ADT_UInt32 Evt3Queue:4;
   ADT_UInt32 Evt4Queue:4;
   ADT_UInt32 Evt5Queue:4;
   ADT_UInt32 Evt6Queue:4;
   ADT_UInt32 Evt7Queue:4;
   ADT_UInt32 Evt8Queue:4;
   ADT_UInt32 Evt9Queue:4;
   ADT_UInt32 Evt10Queue:4;
   ADT_UInt32 Evt11Queue:4;
   ADT_UInt32 Evt12Queue:4;
   ADT_UInt32 Evt13Queue:4;
   ADT_UInt32 Evt14Queue:4;
   ADT_UInt32 Evt15Queue:4;
   ADT_UInt32 Evt16Queue:4;
   ADT_UInt32 Evt17Queue:4;
   ADT_UInt32 Evt18Queue:4;
   ADT_UInt32 Evt19Queue:4;
   ADT_UInt32 Evt20Queue:4;
   ADT_UInt32 Evt21Queue:4;
   ADT_UInt32 Evt22Queue:4;
   ADT_UInt32 Evt23Queue:4;
   ADT_UInt32 Evt24Queue:4;
   ADT_UInt32 Evt25Queue:4;
   ADT_UInt32 Evt26Queue:4;
   ADT_UInt32 Evt27Queue:4;
   ADT_UInt32 Evt28Queue:4;
   ADT_UInt32 Evt29Queue:4;
   ADT_UInt32 Evt30Queue:4;
   ADT_UInt32 Evt31Queue:4;
   
} DMAChanQue_t;

typedef struct ccerr_t {
   ADT_UInt32 que0WatermarkOverrun:1;
   ADT_UInt32 que1WatermarkOverrun:15;
   ADT_UInt32 tccErrorCntExceeded:16;
} ccerr_t;

typedef struct evntEntry_t {
   ADT_UInt32 evnt:6;
   ADT_UInt32 evntTrigger:26;
} evntEntry_t;

typedef struct queStat_t {
   ADT_UInt32 startEntry:8;
   ADT_UInt32 validEntryCnt:8;
   ADT_UInt32 maxWaterMark:8;
   ADT_UInt32 waterMarkExceeded:8;
} queStat_t;

typedef struct ccstat_t {
   ADT_UInt32  dmaEvtActive:1;
   ADT_UInt32  qdmaEvtActive:1;
   ADT_UInt32  transferRequestActive:1;
   ADT_UInt32  writeActiveOrPending:1;
   ADT_UInt32  controllerActive:4;
   ADT_UInt32  outstandingCompletions:8;
   ADT_UInt32  q0Active:1;
   ADT_UInt32  q1Active:15;
} ccstat_t;

typedef struct shadow_t {
   struct {
      ADT_UInt64 active;
      ADT_UInt64 clear;
      ADT_UInt64 set;
      ADT_UInt64 chain;
      ADT_UInt64 enable;
      ADT_UInt64 enableClear;
      ADT_UInt64 enableSet;
      ADT_UInt64 secondaryActive;
      ADT_UInt64 secondaryClear;
   } evt;

   ADT_UInt8  dummy [8];
   struct {
      ADT_UInt64 enable;
      ADT_UInt64 enableClear;
      ADT_UInt64 enableSet;
      ADT_UInt64 pending;
      ADT_UInt64 clear;
      ADT_UInt64 eval;
   } intr;
} shadow_t;

typedef struct params_t {
   dma3Opts_t opts;
   ADT_UInt32 src;
   ADT_UInt16 ACnt;
   ADT_UInt16 BCnt;
   ADT_UInt32 dst;
   ADT_UInt16 srcBOffset;
   ADT_UInt16 dstBOffset;
   ADT_UInt16 linkAddr;
   ADT_UInt16 BReload;
   ADT_UInt16 srcCOffset;
   ADT_UInt16 dstCOffset;
   ADT_UInt32 CCnt;
} params_t;

typedef struct EDMA_Glob_t {
   ADT_UInt8  dummy1[0x240];
   DMAChanQue_t evtToQueue;
   ADT_UInt8  dummy2[0xB0];
   ADT_UInt32 missedEvents;
   ADT_UInt32 dummy3;
   ADT_UInt32 clearMissedEvents;
   ADT_UInt8  dummy4[8];
   ccerr_t    CCERR;
   ccerr_t    CCERR_CLR;
   ADT_UInt32 ErrEval;
   ADT_UInt8  dummy5[0x1C];
   ADT_UInt64 DmaRegionEvents[4];
   ADT_UInt8  dummy6[0xA0];
   evntEntry_t Q0Entries[16];
   evntEntry_t Q1Entries[16];
   ADT_UInt8  dummy7[0x180];
   queStat_t  Q0Stat;
   queStat_t  Q1Stat;
   ADT_UInt8  dummy8[0x18];
   ADT_UInt32 waterMarkThreshold;
   ADT_UInt8  dummy9[0x1C];
   ccstat_t   CCSTAT;
} EDMA_Glob_t;

typedef struct EDMA_CC_t {
   EDMA_Glob_t global;
   ADT_UInt8  dummy1[0x9B8];
   shadow_t   global_region;
   ADT_UInt8  dummy2[0xF80];
   shadow_t   region0;
   ADT_UInt8  dummy3[0x180];
   shadow_t   region1;
   ADT_UInt8  dummy4[0x1D80];
   params_t   params[2];
   params_t   unusedParams[30];
   params_t   McASPsPingPong[4];
   params_t   McBSPsPingPong[8];
   
} EDMA_CC_t;


typedef struct tcstat_t {
   ADT_UInt32 busy:1;
   ADT_UInt32 srcAct:1;
   ADT_UInt32 writePending:1;
   ADT_UInt32 dummy:1;
   ADT_UInt32 dstActCnt:3;
   ADT_UInt32 dummy2:4;
   ADT_UInt32 dstFifoStart:2;
   ADT_UInt32 dummy3:19;
} tcstat_t;

typedef struct errstat_t {
   ADT_UInt32 busErr:1;
   ADT_UInt32 dummy:1;
   ADT_UInt32 transferErr:1;
   ADT_UInt32 mmrAddrErr:1;
   ADT_UInt32 dummy2:28;
} errstat_t;

typedef struct errdetails_t {
   ADT_UInt32  type:4;
   ADT_UInt32  dummy1:4;
   ADT_UInt32  tcc:6;
   ADT_UInt32  dummy2:2;
   ADT_UInt32  cmpltIntrEnable:1;
   ADT_UInt32  cmpltChainEnable:1;
   ADT_UInt32  dummy3:22;
} errdetails_t;

typedef struct proxy_t {
   ADT_UInt32 DSPInitiated:8;
   ADT_UInt32 Supervisor:24;
} proxy_t;

typedef struct dstFIFOInst_t {
   dma3Opts_t Opt;
   ADT_UInt32 dummy1;
   ADT_UInt16 ACnt;
   ADT_UInt16 BCnt;
   ADT_UInt32 Addr;
   ADT_UInt16 dummy2;
   ADT_UInt16 BIdx;
   proxy_t    proxy;
} dstFIFOInst_t;

typedef struct dstFIFO_t {
   ADT_UInt32 AReload;
   ADT_UInt32 dummy1;
   ADT_UInt32 BAddr;
   ADT_UInt8  dummy2[0x74];
   dstFIFOInst_t inst[4];
} dstFIFO_t;

typedef struct TC_t {
   ADT_UInt8 dummy1 [0x100];
   union {
      tcstat_t fields;
      ADT_UInt32 value;
   } tcstat;
   ADT_UInt8 dummy2 [0x1C];
   union {
      errstat_t fields;
      ADT_UInt32 value;
   } errstat;
   union {
      errstat_t fields;
      ADT_UInt32 value;
   } errEnable;
   union {
      errstat_t fields;
      ADT_UInt32 value;
   } errClear;
   union {
      errdetails_t fields;
      ADT_UInt32 value;
   } errDetails;
   ADT_UInt8  dummy3 [0xC];
   ADT_UInt32 cyclesBetweenRead;
   ADT_UInt8  dummy4 [0xFC];
   struct {
      dma3Opts_t Opt;
      ADT_UInt32 Addr;
      ADT_UInt16 ACnt;
      ADT_UInt16 BCnt;
      ADT_UInt32 dummy1;  // destAddr
      ADT_UInt32 BIdx;
      union {
         proxy_t fields;
         ADT_UInt32 value;
      } proxy;
      ADT_UInt32 AReload;
      ADT_UInt32 BAddr;
      ADT_UInt32 dummy3;
   } ActiveSrc;
   ADT_UInt8 dummy5 [0x1C];
   dstFIFO_t FIFOs;
} TC_t;
//}
//
//
// Interrupt controller registers
//{------------------------------------------------------------------------------------
#define ALL_EVENTS 0xffffffffffffffff

//SS Cornet
//#define INTR_CTL_BASE  (CSL_INTC_0_REGS)
//#define INTR_CTL_BASE  (CSL_C66X_COREPAC_REG_BASE_ADDRESS_REGS)
#define INTR_CTL_BASE  (CSL_CP_INTC_0_REGS)
#define INTR_PND       (INTR_CTL_BASE)
#define INTR_CLR       (INTR_CTL_BASE + 0x40)  // Interrupt signal clear (128 bits)
#define INTR_CMB       (INTR_CTL_BASE + 0x80)  // Interrupt signal combine (128 bits)
#define INTR_XSTAT     (INTR_CTL_BASE + 0x180) // Interrupt exception status
#define INTR_XSTAT_CLR (INTR_CTL_BASE + 0x184) // Interrupt exception status clear

#define SYS_CFG_BASE   (CSL_SYSCFG_0_REGS)
#define CHIPSIG_SET    (SYS_CFG_BASE + 0x174)
#define CHIPSIG_CLR    (SYS_CFG_BASE + 0x178)

//#define CSL_INTC_EVENTID_EDMA3CC_ERRINT CSL_INTC0_CPU_3_1_EDMACC_ERRINT

#define CombinedEventIntr (CSL_INTC_EVENTID_EDMA3CC_ERRINT/32)                // Combined interrupt for error event.


#define CMB_EVT_PND (INTR_PND + (4 * CombinedEventIntr))   // event combiner interrupt pending for events
#define CMB_EVT_CLR (INTR_CLR + (4 * CombinedEventIntr))   // event combiner interrupt disable for events
#define CMB_EVT_ENB (INTR_CMB + (4 * CombinedEventIntr))   // event combiner interrupt enable for events

#define assignErrorInterrupt(sgnl)           \
   {  ADT_UInt32 *xReg;                      \
      ADT_UInt32 value;                      \
      xReg  = (ADT_UInt32 *) INTR_CMB;       \
      xReg += sgnl / 32;                     \
      REG_RD (xReg, value);                  \
      value &= ~(1 << (sgnl&0x1F));          \
      REG_WR (xReg, value);                  \
   }

#define assignToCombiner assignErrorInterrupt

// Clear interrupt events from event combiner
#define clearCombinedEvent(evnt)             \
   {  ADT_UInt32 *cReg;                      \
      ADT_UInt32 bits;                       \
      cReg  = (ADT_UInt32 *) INTR_CLR;       \
      cReg += evnt / 32;                     \
      bits  = (1 << (evnt&0x1F));            \
      REG_WR (cReg, bits);                   \
   }

#define activateCombinedEvent(sgnl)           \
   {  ADT_UInt32 *xReg;                      \
      ADT_UInt32 value;                      \
      xReg  = (ADT_UInt32 *) INTR_CMB;       \
      xReg += sgnl / 32;                     \
      REG_RD (xReg, value);                  \
      value &= ~(1 << (sgnl&0x1F));          \
      REG_WR (xReg, value);                  \
   }
//}------------------------------------------------------------------------------------
// Pin Mux Registers.  These registers control which peripherals 
// are connected to which input signals.
//{------------------------------------------------

#define DEV_CFG_BASE            CSL_SYSCFG_0_REGS
#define KICK0R         (volatile ADT_UInt32*)(DEV_CFG_BASE + 0x038)
#define KICK1R         (volatile ADT_UInt32*)(DEV_CFG_BASE + 0x03c)
#define KICK0R_UNLOCK   (0x83E70B13)
#define KICK1R_UNLOCK   (0x95A4F1E0)


#define CFG_PINMUX0    (volatile ADT_UInt32*)(DEV_CFG_BASE + 0x120)
#define MUX0_McASPMask   0x00FFFFFF
#define MUX0_McASPActive 0x00111111    // McASP clock and frame sync pins

#define CFG_PINMUX1    (volatile ADT_UInt32*)(DEV_CFG_BASE + 0x124)
#define MUX1_McASPMask   0xFFFFFFFF
#define MUX1_McASPActive 0x11111111    // McASP data pins (8-15)

#define MUX1_McBSPMask   0xFFFFFFFF
#define MUX1_McBSPActive 0x22222220    // McASP data pins (8-15)

#define CFG_PINMUX2    (volatile ADT_UInt32*)(DEV_CFG_BASE + 0x128)
#define MUX2_McASPMask   0xFFFFFFFF
#define MUX2_McASPActive 0x11111111    // McASP data pins (0-7)

#define UnlockSysRegs() { *KICK0R = KICK0R_UNLOCK;  *KICK1R = KICK1R_UNLOCK; }


#if 0 // RMF
#define MUX1_Timer1Mask   0xFFCFFFFF
#define MUX1_Timer1Active 0x00100000

#endif






//}------------------------------------------------
// Power and sleep control registers.  
// power on/off of peripherals
//{------------------------------------------------
#define PSC0  0
#define PSC0_BASE  CSL_PSC_0_REGS
#define PSC0_CMD   ((volatile ADT_UInt32 *) (PSC0_BASE + 0x120))  // PTCMD
#define PSC0_STAT  ((volatile ADT_UInt32 *) (PSC0_BASE + 0x128))  // PTSTAT
#define PSC0_REG   ((volatile ADT_UInt32 *) (PSC0_BASE + 0xA00))  // MDCTLn

#define PSC_EDMA_CC_0  0
#define PSC_EDMA_TC_0  1
#define PSC_EDMA_TC_1  2


#define PSC1  1
//SS Cornet
//#define PSC1_BASE  CSL_PSC_1_REGS
#define PSC1_BASE  CSL_PSC_REGS
#define PSC1_CMD   ((volatile ADT_UInt32 *) (PSC1_BASE + 0x120))  // PTCMD
#define PSC1_STAT  ((volatile ADT_UInt32 *) (PSC1_BASE + 0x128))  // PTSTAT
#define PSC1_REG   ((volatile ADT_UInt32 *) (PSC1_BASE + 0xA00))  // MDCTLn

#define PSC_TSIP0    9
#define PSC_TSIP1    10

#define PSC_EDMA_CC_1  0
#define PSC_McASP0     7
#define PSC_I2C       11
#define PSC_McBSP0     14
#define PSC_McBSP1     15

#define PSC_OFF 2
#define PSC_ON  3


#if 0

#define PSC_TIMER1   28

#endif



//}-----------------------
// Cache control registers.
//{------------------------------------------------
#define CACHE_BASE  CSL_CACHE_0_REGS
#define L2WBINV     (volatile ADT_UInt32 *) (CACHE_BASE + 0x5004)


//}=================================================
//
//
//
// McASP registers 
//{------------------------------------------------------------------------------------
#define NUM_MCASP_PORTS CSL_MCASP_PER_CNT
#define McASP0_BASE     (CSL_MCASP_0_CTRL_REGS) // Cfg bus address
#define McASP0_DATA     (CSL_MCASP_0_DATA_REGS) // Data bus address
#define McASP0_FIFO     (CSL_MCASP_0_FIFO_REGS) // FIFO address

//}------------------------------------------------------------------------------------
// McASP register structure
//{------------------------------------------------------------------------------------
typedef struct rst_t {
   ADT_UInt8  RxRST;
   ADT_UInt8  TxRST;
   ADT_UInt16 dummy1;
} rst_t;

typedef struct fmt_t {
   ADT_UInt32 rotate:3;
   ADT_UInt32 CFGPortAccess:1;
   ADT_UInt32 slotSize:4;
   ADT_UInt32 padBit:5;
   ADT_UInt32 padValue:2;
   ADT_UInt32 msbFirst:1;
   ADT_UInt32 delay:2;
   ADT_UInt32 dummy:14;
} fmt_t;

typedef struct fs_t {
   ADT_UInt32 fallingEdge:1;
   ADT_UInt32 internalSrc:1;
   ADT_UInt32 dummy:2;
   ADT_UInt32 wordSize:1;
   ADT_UInt32 dummy2:2;
   ADT_UInt32 slotCnt:9;
   ADT_UInt32 dummy3:16;
} fs_t;

typedef struct clk_t {
   ADT_UInt32 divisorM1:5; 
   ADT_UInt32 internalSrc:1;
   ADT_UInt32 separateTxRx:1;
   ADT_UInt32 txFallingEdge:1;
   ADT_UInt32 dummy:24;
} clk_t;

typedef struct hclk_t {
   ADT_UInt32 divisorM1:12;
   ADT_UInt32 dummy:2;
   ADT_UInt32 inverted:1;
   ADT_UInt32 internalSrc:1;
   ADT_UInt32 dummy2:16;
} hclk_t;

typedef struct intr_t {
   ADT_UInt32  underOver:1;
   ADT_UInt32  syncErr:1;
   ADT_UInt32  clkFail:1;
   ADT_UInt32  dmaErr:1;
   ADT_UInt32  lastSlot:1;
   ADT_UInt32  dataReady:1;
   ADT_UInt32  dummy:1;
   ADT_UInt32  startOfFrame:1;
   ADT_UInt32  dummy2:24;
} intr_t;

typedef struct stat_t {
   ADT_UInt32  underOver:1;
   ADT_UInt32  syncErr:1;
   ADT_UInt32  clkFail:1;
   ADT_UInt32  slotEven:1;
   ADT_UInt32  lastSlot:1;
   ADT_UInt32  dataReady:1;
   ADT_UInt32  startOfFrame:1;
   ADT_UInt32  dmaErr:1;
   ADT_UInt32  anyErr:1;
   ADT_UInt32  dummy:23;
} stat_t;

typedef struct clkChk_t {
   ADT_UInt32 sysClkDivPow2:4;
   ADT_UInt32 dummy:4;
   ADT_UInt32 min:8;
   ADT_UInt32 max:8;
   ADT_UInt32 cnt:8;
} clkChk_t;

typedef struct dir_t {
   union {
      rst_t fields;
      ADT_UInt32 value;
   } rst;
   ADT_UInt32 DataMask;
   union {
      fmt_t fields;
      ADT_UInt32 value;
   } fmt;
   union {
      fs_t fields;
      ADT_UInt32 value;
   } fs;
   union {
      clk_t fields;
      ADT_UInt32 value;
   } clk;
   union {
      hclk_t fields;
      ADT_UInt32 value;
   } hclk;
   ADT_UInt32 ActiveSlotMask;
   union {
      intr_t fields;
      ADT_UInt32 value;
   } intr;
   union {
      stat_t fields;
      ADT_UInt32 value;
   } stat;
   ADT_UInt32 currentSlot;
   union {
      clkChk_t fields;
      ADT_UInt32 value;
   } clkChk;
   ADT_UInt32 evtCtl;
   ADT_UInt8  dummy[0x10];
} dir_t;

typedef struct McASP_t {
   ADT_UInt8 dummy1[0x10];
   ADT_UInt32 PinIsGPIO;
   ADT_UInt32 PinIsTx;
   ADT_UInt8  dummy2[0x48];
   dir_t      Rx;
   dir_t      Tx;
} McASP_t;


typedef struct fifoCtrl_t {
   ADT_UInt8  WordsPerDmaTransfer;
   ADT_UInt8  WordCntPerDmaEvent;   // 
   ADT_UInt16 FIFOEnabled;
}  fifoCtrl_t;

typedef struct McASP_Fifo_t {
   ADT_UInt8 dummy[0x10];
   fifoCtrl_t writeCtl;
   ADT_UInt32 wordsInWriteFIFO;
   fifoCtrl_t readCtl;
   ADT_UInt32 wordsInReadFIFO;
} McASP_Fifo_t;

//}------------------------------------------------------------------------------------
// McBSP registers 
//{------------------------------------------------------------------------------------
#define McBSP0_BASE  (CSL_Mcbsp0_CFG_DATA_REGS)
#define McBSP1_BASE  (CSL_Mcbsp1_CFG_DATA_REGS)
#define McBSPTx0_Addr (McBSP0_BASE + 4)
#define McBSPRx0_Addr (McBSP0_BASE)
#define McBSPTx1_Addr (McBSP1_BASE + 4)
#define McBSPRx1_Addr (McBSP1_BASE)
#define McBSP0_FIFO     (CSL_Mcbsp0_FIFO_CFG_REGS) // FIFO address
#define McBSP1_FIFO     (CSL_Mcbsp1_FIFO_CFG_REGS) // FIFO address
#define McBSP0_DATA   (CSL_Mcbsp0_FIFO_DATA_REGS)
#define McBSP1_DATA   (CSL_Mcbsp1_FIFO_DATA_REGS)

//}------------------------------------------------
//
//
// G.Pak specific assignments
//{=================================================
extern PortAddr DMACfg[];


//
//                          TDM
//   Shadow region:          1
//   Transfer queue:         0         
//   Transfer priority       0


#define TDM_Priority  0    // Transfer controller priority for TDM data
#define TDM_TC        1    // TDM data transfers are assigned to DMA transfer controller 0

//  Index values for TDM - DMA 'ping' parameter sets.  'pong' indices are 'ping' + 1
#define McASPRx_Ping  (PARAM_CNT + 0)  //  Rx McASP
#define McASPTx_Ping  (PARAM_CNT + 2)  //  Tx McASP
#define Rx0_Ping      (PARAM_CNT + 4)  //  Rx McBSP0
#define Tx0_Ping      (PARAM_CNT + 6)  //  Tx McBSP0
#define Rx1_Ping      (PARAM_CNT + 8)  //  Rx McBSP1
#define Tx1_Ping      (PARAM_CNT + 10) //  Tx McBSP1

//--------------------------------------------
// Hardware interrupts
#define HWI_DMA_COMPLETE  4   // HW Interrupt vector for EDMA completions
#define HWI_COMBINED_INT  5   // Interrupts from combiner (TSIP events+error)
#define HWI_FRAME_READY   6   // Interrupt from scheduler to framing task cores
// Ecm.eventGroupHwiNum[0] = 7;  Forced by BIOS in app.cfg
// Ecm.eventGroupHwiNum[1] = 8;  Forced by BIOS in app.cfg
// Ecm.eventGroupHwiNum[2] = 9;  Forced by BIOS in app.cfg
// Ecm.eventGroupHwiNum[3] = 10; Forced by BIOS in app.cfg
#define HWI_DMA_ERROR    12  // EDMA errors
//  HWI_TIMER        = 15        Set by BIOS


//-------------------------------------------------------
// Interupt events

#define HostIntEvent       CSL_INTC_EVENTID_SYSCFG_CHIPINT3    // Host to DSP interrupt
#define HostToDSPIntBit    8

#define DSPToHostIntBit    4


#define McASP_ErrSgnl      CSL_INTC_EVENTID_MCASP0INT          // McASP error signaled

#endif
