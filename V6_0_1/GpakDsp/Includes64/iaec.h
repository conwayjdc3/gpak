/*
 *  ======== iaec.h ========
 *  IAEC Interface Header
 */
#ifndef IAEC_
#define IAEC_
#ifndef NO_DAIS

#include <ialg.h>
#include "adt_typedef.h"

/*
 *  ======== IAEC_Handle ========
 *  This handle is used to reference all AEC instance objects
 */
typedef struct IAEC_Obj *IAEC_Handle;

/*
 *  ======== IAEC_Obj ========
 *  This structure must be the first field of all AEC instance objects
 */
typedef struct IAEC_Obj {
    struct IAEC_Fxns *fxns;
} IAEC_Obj;

/*
 *  ======== IAEC_Status ========
 *  Status structure defines the parameters that can be changed or read
 *  during real-time operation of the alogrithm.
 */
typedef struct IAEC_Status {
    Int size;	/* must be first field of all status structures */

} IAEC_Status;
/*
 *  ======== IAEC_Cmd ========
 *  The Cmd enumeration defines the control commands for the AEC
 *  control method.
 */

typedef enum IAEC_Cmd {
	IAEC_GETSTATUS,
	IAEC_SETSTATUS
} IAEC_Cmd;

#endif //NO_DAIS
/*
 *  ======== IAEC_Params ========
 *  This structure defines the creation parameters for all AEC objects
 */
typedef struct IAEC_Params {
    ADT_Int32 size;	/* must be first field of all params structures */
    ADT_Int16 SubbandDecFIRLen;
    ADT_Int16 SubbandRecFIRLen;
    ADT_Int16 TapLength;
    ADT_Int8 DoubleTalkSaveRestoreEnable;
    ADT_Int8 NLPEnable;
    ADT_Int8 NLPMuteThreshold;
    ADT_Int8 NLPSuppThreshold;
    ADT_Int8 NoiseSuppThreshold;
    ADT_Int8 DoubleTalkIThresh;
    ADT_Int8 DoubleTalkOThresh;
    ADT_Int8 AntiHowlEnable;
    ADT_Int8 MaxNoiseReduction;
    ADT_Int8 MinFBPowerUpdate;
    ADT_Int8 MinSBPowerUpdate;
    ADT_Int16 StepSize;
    ADT_Int16 SuperFrameSize;
    ADT_Int8 EnableDecHammingWindow;
    ADT_Int8 EnableRecHammingWindow;
    ADT_Int16 FIRTransitionWidth;
    ADT_Int16 SamplingRate;

} IAEC_Params;
#ifndef NO_DAIS

/*
 *  ======== IAEC_PARAMS ========
 *  Default parameter values for AEC instance objects
 */
extern IAEC_Params IAEC_PARAMS;

/*
 *  ======== IAEC_Fxns ========
 *  This structure defines all of the operations on AEC objects
 */
typedef struct IAEC_Fxns {
    IALG_Fxns	ialg;    /* IAEC extends IALG */
    void  (*aec)(IAEC_Handle handle, ADT_Int16 *Far, ADT_Int16 *Near, ADT_Int16 *NearOut);
	void  (*init)(IAEC_Params *pParams, float *pScratch);
} IAEC_Fxns;
#endif //NO_DAIS

#endif	/* IAEC_ */
