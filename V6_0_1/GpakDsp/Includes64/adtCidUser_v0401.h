/*___________________________________________________________________________
 |
 |   File: adtCidUser.h
 |
 |   This file is the Adaptive Digital Technologies, Inc. 
 |   public header file for the Caller ID product.
 |   
 |   This software is the property of Adaptive Digital Technologies, Inc.
 |   Unauthorized use is prohibited.
 |
 |   Copyright (c) 2002, Adaptive Digital Technologies, Inc.
 |
 |   www.adt-inc.com
 |   610-825-0182
 |___________________________________________________________________________
*/
#ifndef ADT_CID_USER_H
#define ADT_CID_USER_H

#define TAS_ENABLE_TYPE_II 1
#include <adt_typedef.h>

// IMPORTANT: do not modify the following values!
#ifdef _TMS320C6X
#define CID_RX_INST_LEN 24 // Size of RX CID instance data 
#define CID_TX_INST_LEN 28 // Size of TX CID instance data
#endif

#ifdef WIN32
#define CID_RX_INST_LEN 24 // Size of RX CID instance data 
#define CID_TX_INST_LEN 27 // Size of TX CID instance data
#endif

#ifdef __TMS320C55X__
#define CID_RX_INST_LEN 26 // Size of RX CID instance data 
#define CID_TX_INST_LEN 29 // Size of TX CID instance data
#endif

// Define RX CID function result codes
#define CID_RX_OK_CODE                  0   // function operating 
#define CID_RX_DECODE_MESSSAGE_CODE     1   // receive message being decoded
#define CID_RX_MESSAGE_AVAILABLE_CODE   2	// receive message is ready

#define CID_RX_WAIT_TAS_CODE            3   // TAS has not been detected so far
#define CID_RX_TAS_DETECTED_CODE        4   // TAS is detected and is still on
#define CID_RX_SENDING_ACK_CODE         5	// ACK being sent
#define CID_RX_WAIT_MARK_CODE           6   // WAITING for the caller ID

#define CID_RX_TAS_TOO_SHORT_CODE       0xF1   // Error code, tas duration is short, restart the cid Rx
#define CID_RX_FAILED_MARK_CODE         0xF2   // Error code, After ACK sent, Rx can't detect data(MARK) before timeout
#define CID_STARTBIT_ERROR_CODE         0xF3   // Error code, critical start bit error during decode msg stage, restart
#define CID_STOPBIT_ERROR_CODE          0xF4   // Error code, stop bit error during decode msg stage, keep going

// Define TX CID function result codes

#define CID_TX_IDLE_CODE                0	// Transmit Done
#define CID_TX_SENDING_FSK_CODE	        1	// Transmit FSK

#define CID_TX_SENDING_TAS_CODE	    	2	// Transmit TAS
#define CID_TX_WAITING_ACK_CODE         3	// Detecting ACK(DTMF'D')
#define CID_TX_ACK_DETECTED_CODE        4	// Deteced ACK(DTMF'D'), will move to sending caller ID

#define CID_TX_FAILED_ACK_CODE          5	// Error code, ACK not detected, aborted sending caller ID, in IDLE

// Define the maximum lengths of fields in a caller ID burst
// User parameter values may differ, but cannot exceed these limits
#define MAX_SEIZURE_BYTES   38
#define MAX_MARK_BYTES      23
#define MAX_POSTAMBLE_BYTES 10
#define MAX_OVERHEAD_BYTES   3    
#define MAX_MESSAGE_SIZE   253
#define MAX_MESSAGE_BYTES  (MAX_MESSAGE_SIZE+MAX_OVERHEAD_BYTES)

// NUM_BURST_DATA_BYTES is the number of formatted data bytes after start
// and stop bits have been added:
// (i.e 5/4 times the original number of bytes... add 1 for rounding)
#define NUM_BURST_DATA_BYTES   (((((MAX_MESSAGE_BYTES+MAX_POSTAMBLE_BYTES))*5)>>2) + 1)

// MAX_BURST_BUFLEN is the max size the of formatted burst buffer
#define MAX_BURST_BUFLEN (MAX_SEIZURE_BYTES+MAX_MARK_BYTES+NUM_BURST_DATA_BYTES)

// FSK-type definition
#define  FSK_V23			0   // V.23 modem signal
#define  FSK_BELL202		1	// Bell 202 modem signal

// Receive CID instance structure
typedef struct
{
    ADT_Int64 data[CID_RX_INST_LEN];
} CIDRX_Inst_t;

// Transmit CID instance structure
typedef struct
{
    ADT_Int64 data[CID_TX_INST_LEN];
} CIDTX_Inst_t;


// If not using the CIDRX's ACK sending function on the Tx path,
// users must provide following callback function to notify the application's 
// tone genrator, to send the TAS ACK (DTMF - 'D') on the Tx path
typedef void (toneAckNotify)  (void* toneGenHandler);

// Transmit CID TAS parameter
typedef struct
{
    void          *toneTasHandler; // TAS Tone instance used by the CallerID to detect TAS signal
    void          *toneGenHandler; // Tone GEN instance used by the CallerID to send TE-ACK
    toneAckNotify *toneGenNotify;// callback function to notify external tone generator 
	ADT_Int16     ACK_Level;    // ACK signal level
} CIDRX_TAS_Params_t;

// Receive CID TAS parameter
typedef struct
{
    void          *toneTasHandler; // TAS Tone instance used by the CallerID to generate TAS signal
    void          *toneAckHandler; // Tone Detector instance used by the CallerID to detect TE-ACK
    void          *toneAckSctrach; // Tone Detector Scratch used by the CallerID to detect TE-ACK
	ADT_Int16     TAS_Level;    // TAS tone signal level
} CIDTX_TAS_Params_t;

// instance size for the Tone generator
#define TONEGEN_INST_SIZE_32      16
typedef struct{
	ADT_Int64 data[TONEGEN_INST_SIZE_32/2];
} CIDRX_ACKGEN_Inst_t;

typedef struct{
	ADT_Int64 data[TONEGEN_INST_SIZE_32/2];
} CIDTX_TASGEN_Inst_t;

// instance size for the TAS tone detector
#define TONE_TAS_DET_INST_SIZE_32 38
typedef struct{
	ADT_UInt32 data[TONE_TAS_DET_INST_SIZE_32];
} CIDRX_TASDET_Inst_t;

// instance size for the ACK(DTMF'D' detector
#define TONE_ACK_DET_INST_SIZE_32 38
typedef struct{
	ADT_UInt32 data[TONE_ACK_DET_INST_SIZE_32];
} CIDTX_ACKDET_Inst_t;

#define TONE_ACK_DET_SCRACTH_SIZE_32 64
typedef struct{
	ADT_UInt32 data[TONE_ACK_DET_SCRACTH_SIZE_32];
} CIDTX_ACKDET_Scratch_t;



// Receive CID parameter structure
typedef struct
{
	ADT_Int16   APIVersion;        // API Version Control
	ADT_UInt16  fskType;           // type of fsk signal: FSK_V23 or FSK_BELL202
    ADT_UInt16  frameSize;         // number of samples per frame
#ifdef TAS_ENABLE_TYPE_II
    CIDRX_TAS_Params_t TAS_Params;
	ADT_Int8   type2Eanble;       // number of samples per frame
#endif
} CIDRX_Params_V4_t;
#define CIDRX_Params_t CIDRX_Params_V4_t
// Transmit CID parameter structure
typedef struct
{
	ADT_Int16   APIVersion;        // API Version Control
    ADT_UInt16  formatBurstFlag;   // non-zero == format a TxCid Burst
    ADT_UInt16  numSeizureBytes;   // number of seizure bytes
    ADT_UInt16  numMarkBytes;      // number of mark bytes
    ADT_UInt16  numPostambleBytes; // number of postamblel bytes
    ADT_UInt16  fskType;           // FSK_V23 or FSK_BELL202
    ADT_UInt16  frameSize;         // transmit frame size
	ADT_Int16   fskLevel;          // FSK signal level, -dBm (0 ... -19)
    ADT_UInt8   msgType;           // message type identifier
    ADT_UInt8   msgLen;            // number of message bytes
#ifdef TAS_ENABLE_TYPE_II
    CIDTX_TAS_Params_t TAS_Params;
	ADT_Int8   type2Eanble;       // number of samples per frame
#endif
} CIDTX_Params_V4_t;
#define CIDTX_Params_t CIDTX_Params_V4_t
// Receive CID initialization function
// pScratch must point to a buffer defined as: ADT_Int32 Scratch[FRAME_SIZE*2];
// where: FRAME_SIZE equals the same value as the frameSize parameter.
short int CIDRX_ADT_init (
    CIDRX_Inst_t    *pInstance,     // pointer to instance data structure
    CIDRX_Params_V4_t  *pParams,       // pointer to parameter structure
    ADT_UInt8       *pRxMessage,    // pointer to message buffer
    ADT_Int32       *pScratch       // pointer to scratch buffer
    );      

// Receive CID process function
// Returm value equals RX CID result codes
ADT_UInt16 CIDRX_ADT_receive (
    CIDRX_Inst_t    *pInstance,     // pointer to instance data structure
    ADT_Int16       *pInput,        // pointer to receive input sample buffer
    ADT_UInt16      *pNumMsgBytes   // pointer to variable holding number of
    );                              // received bytes in message.

//char *CIDRX_ADT_parseMessage(CIDRX_Channel_t *Channel);

// TX CID initialization function prototype
short int CIDTX_ADT_init(
    CIDTX_Inst_t   *pTxCidInstance,    // TX CID instance pointer
    CIDTX_Params_V4_t *pTxCidParameters,  // TX CID params pointer  
    ADT_UInt8      *pMessageBytes,     // Message byte buffer pointer
    ADT_UInt8      *pBurstBuffer,      // Burst buffer pointer
    ADT_UInt16     *pNumBurstBytes     // number of burst bytes to be modulated
    );

// TX CID transmit function prototype
// returns cid transmit status code 
ADT_UInt16 CIDTX_ADT_transmit(
    CIDTX_Inst_t   *pTxCidInstance,   // TX  CID instance pointer
    ADT_Int16      *pOutputSamples    // pointer to output samples buffer
    );

#ifdef TAS_ENABLE_TYPE_II
// Rx send path function, it must be called if users do not use their own
// tone generator to transmit the TE_ACK
short int CIDRX_ADT_genACK(CIDRX_Inst_t *pInstance, ADT_Int16 *pOutput);

// Tx detect TAS_ACK(DTMF 'D'), called on Rx path
// The function below CIDTX_ADT_detACK(), should be called 
// when users does not use their ACK signal detector
// 
short int CIDTX_ADT_detACK (CIDTX_Inst_t *pInstance, ADT_Int16 *pinput);

// If users uses external their own ack signal detector
// They must call the function below CIDTX_ADT_signalACK()
// to inform the caller ID the ACK singal has been detected
void CIDTX_ADT_signalACK(CIDTX_Inst_t *pInstance);

#endif


#endif //ADT_CID_USER_H
