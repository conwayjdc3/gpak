/*****************************************************************************/
/*---------------------------------------------------------------------------*/
/*                     TEXAS INSTRUMENTS INC                                 */
/*                     Multimedia Codecs Group                               */
/*---------------------------------------------------------------------------*/
/*                                                                           */
/* (C) Copyright 2005  Texas Instruments Inc.  All rights reserved.          */
/*                                                                           */
/* Exclusive property of the Multimedia Codecs Group, Texas Instruments Inc. */
/* Any handling, use, disclosure, reproduction, duplication, transmission    */
/* or storage of any part of this work by any means is subject to restriction*/
/* and prior written permission set forth.                                   */
/*                                                                           */
/* This copyright notice, restricted rights legend, or other proprietary     */
/* markings must be reproduced without modification in any authorized copies */
/* of any part of this work. Removal or modification of any part of this     */
/* notice is prohibited.                                                     */
/*---------------------------------------------------------------------------*/
/***********************************************************************
 *   File: g729abenc_tii.h                                           *
 *                                                                     *
 *   (c) Copyright 2005, Texas Instruments Inc.                        *
 ***********************************************************************/
#ifndef G729ABENC_TII_
#define G729ABENC_TII_

#include "ti/xdais/dm/isphenc1.h"

/*
 *  ======== G729ABENC_TII_IALG ========
 *  TII's implementation of the IALG interface for G729AB
 */
extern IALG_Fxns G729ABENC_TII_IALG;
/*
 *  ======== G729AB_TII_IG729ABENC ========
 *  TII's implementation of the IG729AB interface
 */
extern ISPHENC1_Fxns G729ABENC_TII_IG729ABENC;
/*
 *  ======== G729ABENC_TII_alloc ========
 */
extern Int G729ABENC_TII_alloc(const IALG_Params *instParams,
                                struct IALG_Fxns **fxns,
                                IALG_MemRec memTab[]);

/*
 *  ======== G729ABENC_TII_free ========
 */
extern Int G729ABENC_TII_free(IALG_Handle handle,IALG_MemRec memTab[]);
                                /*

 *  ======== G729ABENC_TII_exit ========
 */
extern Void G729ABENC_TII_exit(Void);


/*
 *  ======== G729ABENC_TII_init ========
 */
extern Void G729ABENC_TII_init(Void);

/*
 *  ======== G729ABENC_TII_initObj ========
 */
extern Int G729ABENC_TII_initObj(IALG_Handle handle,
                                 const IALG_MemRec memTab[],
                                 IALG_Handle pHandle,
                                 const IALG_Params *instParams);

/*
 *  ======== G729ABENC_TII_moved ========
 */
extern Void G729ABENC_TII_moved(IALG_Handle handle, 
                                const IALG_MemRec memTab[], 
                                IALG_Handle pHandle,
                                const IALG_Params *instParams);

/*
 *  ======== G729ABENC_TII_numAlloc ========
 */
extern Int G729ABENC_TII_numAlloc(Void);

/*
 *  ======== G729ABENC_TII_g729abControl ========
 */
extern Int G729ABENC_TII_g729abControl(ISPHENC1_Handle handle,
                                              ISPHENC1_Cmd id,
                                              ISPHENC1_DynamicParams *params,
                                              ISPHENC1_Status *status);

/*
 *  ======== G729ABENC_TII_g729abEncode ========
 */
extern Int G729ABENC_TII_g729abEncode (ISPHENC1_Handle g729abInst,
                                        XDM1_SingleBufDesc *inBufs,
                                        XDM1_SingleBufDesc *outBufs,
                                        ISPHENC1_InArgs *inArgs,
                                        ISPHENC1_OutArgs *outArgs);
     

#endif  /* G729ABENC_TII_ */
