/*___________________________________________________________________________
 |
 |   File: faxcng.h
 |
 |   This file is the Adaptive Digital Technologies, Inc. 
 |   private header file for the faxcng tone detectors.
 |   
 |   This software is the property of Adaptive Digital Technologies, Inc.
 |   Unauthorized use is prohibited.
 |
 |   Copyright (c) 2000, Adaptive Digital Technologies, Inc.
 |
 |   Version 1.1
 |   February 7, 2000
 |
 |   www.adt-inc.com
 |   610-825-0182
 |___________________________________________________________________________
*/
#ifndef _FAXCNGBLK_H
#define _FAXCNGBLK_H

//#include "FAXCNG_user.h"
#include "adt_typedef.h"

#define DFT_FAXCNG_N            80  /* number of points for FAXCNG DFT frame */
#define MIN_HIT_FRAMES_FAXCNG   42 // 420 mS on		was 2
#define MIN_MISS_FRAMES_FAXCNG  2 // was 2

#define bit unsigned char
typedef union
{
	ADT_UInt16	Word;
	struct 
	{
		unsigned EqualPrev:1;	/* tone is equal to previous tone */
		unsigned Valid:1;		/* tone is valid (power, SNR, and valid checks passed*/
		unsigned Active:1;	/* tone is active */
		unsigned Unused:13;
	}		Bits;
} FAXCNGFlag_t;


typedef struct
{
	ADT_Int32 NoisePower;
	ADT_Int32 CosSum;
	ADT_Int32 SinSum;
	ADT_Int16 SampleCount;
	ADT_Int16 HitFrameCount;
	ADT_Int16 MissFrameCount;
	ADT_Int16 PrevPhase;
    ADT_Int16 MinHitFrameThresh;
    ADT_Int16 MinMissFrameThresh;
    ADT_Int16 Result;
	FAXCNGFlag_t Flags;
} FAXCNGChannel_t;

extern ADT_Int16 TD_ADT_div_s ( ADT_Int16 x, ADT_Int16 y);

extern void FAXCNG_ADT_DFT(FAXCNGChannel_t *FAXCNGChannel, 
                    ADT_Int16 *InputSamples_p, 
                    ADT_Int16 *ScratchSamples, 
                    ADT_Int16 NDFTSamples);
#endif // ifndef _FAXCNGBLK_H

