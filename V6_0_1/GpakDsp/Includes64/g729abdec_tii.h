/*******************************************************************************
*-----------------------------------------------------------------------------*
*                     TEXAS INSTRUMENTS INC                                   *
*                     Multimedia Codecs Group                                 *
*                                                                             *
*-----------------------------------------------------------------------------*
*                                                                             *
* (C) Copyright 2004  Texas Instruments Inc.  All rights reserved.            *
*                                                                             *
* Exclusive property of the Multimedia Codecs Group, Texas Instruments Inc.   *
* Any handling, use, disclosure, reproduction, duplication, transmission      *
* or storage of any part of this work by any means is subject to restrictions *
* and prior written permission set forth.                                     *
*                                                                             *
* This copyright notice, restricted rights legend, or other proprietary       *
* markings must be reproduced without modification in any authorized copies   *
* of any part of this work. Removal or modification of any part of this       *
* notice is prohibited.                                                       *
*                                                                             *
*******************************************************************************/

/***********************************************************************
 *                                                                     *
 *  xDAIS compatible interface header file for the G729AB module       *
 *  created by : Shwetha.H.S                                           *
 *                                                                     *
 ***********************************************************************/

/*
 *  ======== g729abdec_tii.h ========
 *  Interface for the G729AB_tii module; TII's implementation
 *  of the IG729AB interface
 */
#ifndef G729ABDEC_TII_
#define G729ABDEC_TII_


#include "ti/xdais/dm/isphdec1.h"


/*
 *  ======== G729ABDEC_TII_IALG ========
 *  TII's implementation of the IALG interface for G729AB
 */

extern IALG_Fxns G729ABDEC_TII_IALG;

/*
 *  ======== G729ABDEC_TII_IG729ABDEC ========
 *  TII's implementation of the IG729AB interface
 */

extern ISPHDEC1_Fxns G729ABDEC_TII_IG729ABDEC;

/*
 *  ======== G729ABDEC_TII_alloc ========
 */

extern Int G729ABDEC_TII_alloc(const IALG_Params *instParams,
                               struct IALG_Fxns **fxns,
                               IALG_MemRec memTab[]);

/*
 *  ======== G729ABDEC_TII_free ========
 */

extern Int G729ABDEC_TII_free(IALG_Handle handle,IALG_MemRec memTab[]);

/*
 *  ======== G729ABDEC_TII_initObj ========
 */

extern Int G729ABDEC_TII_initObj(IALG_Handle handle,
                                 const IALG_MemRec memTab[],
                                 IALG_Handle pHandle,
                                 const IALG_Params *instParams);

/*
 *  ======== G729ABDEC_TII_moved ========
 */

extern Void G729ABDEC_TII_moved(IALG_Handle handle,
                                const IALG_MemRec memTab[], 
                                IALG_Handle pHandle,
                                const IALG_Params *instParams);

/*
 *  ======== G729ABDEC_TII_numAlloc ========
 */

extern Int G729ABDEC_TII_numAlloc(Void);

/*
 *  ======== G729ABDEC_TII_g729abControl ========
 */
extern Int G729ABDEC_TII_g729abControl(ISPHDEC1_Handle handle,
                                       ISPHDEC1_Cmd    id,
                                       ISPHDEC1_DynamicParams *params,
                                       ISPHDEC1_Status *status);

/*
 *  ======== G729ABDEC_TII_g729abDecode ========
 */

extern Int G729ABDEC_TII_g729abDecode(ISPHDEC1_Handle handle,
                                       XDM1_SingleBufDesc *inBufs,
                                       XDM1_SingleBufDesc *outBufs,
                                       ISPHDEC1_InArgs *inArgs,
                                       ISPHDEC1_OutArgs *outArgs);

#endif  /* G729ABDEC_TII_ */

/* Nothing beyond this point */

