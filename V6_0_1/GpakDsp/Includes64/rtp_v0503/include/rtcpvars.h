#ifndef RTCPVARS_H  // RTCP header
#define RTCPVARS_H 1

#include <common/include/adt_typedef_user.h>

#ifndef MAX_MEMBERS
   #define MAX_MEMBERS   32
#endif

typedef struct RTCP_Member {
   ADT_UInt32 SSRC;     // Member's SSRC
   ADT_UInt32 timeout;  // RTP clock time (in units of milliseconds) at which member expires
   unsigned int senderFlag:1;    // Member is sender
   unsigned int SSRCCollision:1; // SSRC collision has occurred
} RTCP_Member;

typedef struct RTCP_SDES_ITEM {
   ADT_UInt8 type;
   ADT_UInt8 firstPktInCycle;
   ADT_UInt8 txInterval;
   ADT_UInt8 dataI8;
   ADT_UInt8 *data;
} RTCP_SDES_ITEM;

typedef struct RTCP_SDES_TABLE {
   ADT_UInt16 itemCnt;
   ADT_UInt16 pktsPerCycle;
   ADT_UInt16 currentPkt;
   RTCP_SDES_ITEM item[1];
} RTCP_SDES_TABLE;

typedef struct RTCP_Vars_t {
   // Application Callbacks
   SCHEDULER_FXN   *Scheduler;       // Scheduler callback
   NTPTIME_FXN     *NTPTime;         // NTP time callback
   RTCP_NOTIFY_FXN *RTCP_Notify;     // RTCP report callback 
   NETFUNCTION     *NetTransmit;     // Network delivery callback
   NETHANDLE       *RTCPHandle;      // RTCP network handle

   // Member varialbes
   ADT_UInt16 memberCnt;   // Count of members
   ADT_UInt16 senderCnt;   // Count of members that are also senders
   RTCP_Member memberList[MAX_MEMBERS];   // List of active members

   // State variables for transmission scheduling
   ADT_UInt16 transportOverheadI8;    // Anticipated overhead to RTCP packets by transport layers (UDP/IP/Ethernet).
   ADT_UInt16 timeoutMultiplier;      // Inactivity timeout multiplier.
   ADT_UInt16 avgPktI8;               // Estimated average RTCP packet size.
   ADT_UInt16 prevTxMemberCnt;        // Number of members active when nextTxTime was calculated.
   ADT_UInt32 prevTxTime;             // RTP clock time at which previous RTCP packet was transmitted.
   ADT_UInt32 nextTxTime;             // RTP clock time at which next RTCP packet is due to transmit
   ADT_UInt32 minPeriodMs;            // Minimum time (in milliseconds) between RTCP packet transmissions.
                                      // Recommended as equivalent of 5 seconds.
   ADT_UInt32 bandwidthI8PerSec;      // Total bandwidth (bytes / second) allocated to session's RTCP channel.
                                      // Recommended as 5% of total bandwidth available to RTP and RTCP traffic
   ADT_UInt32 senderBandwidthPercent;  // Minimum percent [0 to 100] of bandWidth allocated to senders.
                                      // Recommended as 25
   // State variables to generate receiver report statistics
   ADT_UInt32 RemoteSSRC;            // SSRC of inbound RTP packets.
   ADT_UInt32 LastNTP;               // Middle 32-bits of last received NTP field.
   ADT_UInt32 SRRcvTimeSamps;        // RTP clock time (in units of milliseconds) of last SR received for RemoteSSRC.
   ADT_UInt32 PktsExpectedPrior;     // Expected number of packets prior to last RTCP transmission
   ADT_UInt32 PktsReceivedPrior;     // Number of packets received prior to last RTCP transmission

   // Dynamic memory allocation pointers
   RTCP_SDES_TABLE *SDES;             // Pointer to source description block data
   ADT_UInt8  *ByeMsg;                // Pointer to bye message length and text
   ADT_UInt32 *PktBuff;               // Pointer memory buffer large enough to hold largest RTCP generated packet.
} RTCP_Vars_t;

#endif
