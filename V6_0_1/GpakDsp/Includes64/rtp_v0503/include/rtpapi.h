//==========================================================================

//         File:    rtpapi.h
//  Description:    RTP/RTCP Application Program Interface header file.
//
//  Copyright (c) 2010, Adaptive Digital Technologies, Inc.
//==========================================================================
#ifndef  __RTPAPI_H

#define __RTPAPI_H


#include <common/include/adt_typedef_user.h>
#include <common/private/include/adt_typedef.h>

#ifdef DSP_TYPE
   #undef  TI_PRAGMA
   #define TI_PRAGMA(str) _Pragma (str)
#else
   #undef  TI_PRAGMA
   #define TI_PRAGMA(str)
#endif
#ifdef _WIN32
    #define WIN32_CODE_SECT(a) __pragma (code_seg (#a))
    #define WIN32_DATA_SECT(a) __pragma (data_seg (#a))
#else
    #define WIN32_CODE_SECT(a)
    #define WIN32_DATA_SECT(a)
#endif

#ifdef RTCP
   #ifdef __BIOS__ 
      #include "rtcpuser.h"
   #else
      #include "rtp/include/rtcpuser.h"
   #endif   
   #include "rtcpvars.h"
#else
   #ifdef __BIOS__ 
      #include "rtpuser.h"
   #else
      #include "rtp/include/rtpuser.h"
   #endif   
#endif

extern void ADT_printf (const char *, ...);

#ifdef _USRDLL // for the DLL creation
#define RTPAlloc   RTPAllocFunc
#define RTPFree    RTPFreeFunc
#define RTPLock    RTPLockFunc
#define RTPUnlock  RTPUnlockFunc
#define RTPWait    RTPWaitFunc
#define ADT_printf ADT_PrintfFunc
#else
extern void ADT_printf (const char *, ...);
#endif

extern RTPALLOC_FUNC  *RTPAllocFunc;
extern RTPFREE_FUNC   *RTPFreeFunc;
extern RTPLOCK_FUNC   *RTPLockFunc;
extern RTPUNLOCK_FUNC *RTPUnlockFunc;
extern RTPWAIT        *RTPWaitFunc;
extern ADT_API PRINTF_FUNC *ADT_PrintfFunc;



extern const int adtRtpVersion;
extern ADT_UInt32 rtpRandom32 (void);

//  Structure identifying RTP header fields
typedef struct rtphdr {
   ADT_UInt16 BitFlds;       // Bit fields = Vers:2, pad:1 xind:1 Cc:4 marker:1 pyldtype:7
   ADT_UInt16 Sequence;      // Sequence number
   ADT_UInt32 TimeStamp;     // Time stamp
   ADT_UInt32 SSRC;          // SSRC
   ADT_UInt32 CSRC[1];       // Remainder of header
} RTPHeader;
#define RTP_HDR_I8LEN 12


//  Field positions within 1st 16 bits of RTP Packet
#define RTP_VERSION_MASK    0xC000  // Version number mask
#define RTP_CURRENT_VERSION 0x8000  // Current version
#define RTP_PADDING_BIT     0x2000  // Padding bit field
#define RTP_EXTEND_BIT      0x1000  // Extension bit field
#define RTP_CC_MASK         0x0f00  // SSRC count field
#define RTP_MARKER_BIT      0x0080  // Marker bit field
#define RTP_PAYLD_MASK      0x007f  // Payload type field

#define SLOPE_MEASURE_WINDOW_MS  10000 // 0603/2019 faster measuring // 60000  // measure average jbdepth at this interval
#define DRIFT_ADJUST_COUNT_THESH 4      // number consecutive increases or decreases of jitter depth to cause an adjustment

// Extension Header format
typedef struct RTPExtension {
  ADT_UInt16 ProfileVars;
  ADT_UInt16 I32Len;         // Length (in 32-bit units) of remainder of header
  ADT_UInt32 Remainder[1];
} RTPExtension;

 
struct jbh {
   struct jbh *Next, *Previous;  // Jitter buffer chain pointers
   ADT_UInt16 HdrI32Len;         // Header size in 32-bit words (extension included)
   ADT_UInt16 PyldI8Len;         // Payload size in bytes
   RTPHeader  *Hdr;              // Pointer to RTP Header
   ADT_UInt32 PktData[1];        // Start of Packet (for internal memory allocation)
};

#if DSP_TYPE==54 || DSP_TYPE==55
   #define CPYSIZE(a) (((a) + 1)/2)
   #define JB_HDR_I8LEN ((sizeof (struct jbh) - sizeof (ADT_UInt32)) * 2)
#else
   #define CPYSIZE(a) (a)
#ifdef EXTERNAL_ALLOCATE
   #define JB_HDR_I8LEN (sizeof (struct jbh))
#else 
   #define JB_HDR_I8LEN (sizeof (struct jbh) - sizeof (ADT_UInt32))
#endif
#endif

//
//  NOTE:   If RTPCONNECT is increased in size, the size of structure_data 
//          in the RTPAPIUSER.H file must be increased accordingly.
// 
//          PktI8Len is set to 32-bit field to force alignment of the
//          RTP header data on a 32-bit boundary. This alignment is necessary
//          to correctly access the 32-bit fields within the header.
//

typedef struct RTPDATA {

    // Session configuration variables
    ADT_UInt32      State;             // State of session
    ADT_UInt32      SSRC;              // Member ID
    RTPVALIDATION  *ProfileValidation; // Profile dependent validation function
    NETHANDLE      *NetHandle;         // Application defined network connection
    NETFUNCTION    *NetTransmit;       // Application defined network transmit
    CLOCKFUNCTION  *RTPTime;           // pointer to clock function
    TONEDURATION   *ToneDurationCallBack;  // Application defined callback to that returns tone packet duration
    ADT_UInt16      ProbationParam, JitterBufferDelay;  // Application configurable parameters
    ADT_UInt16      DelayMin, DelayMax;  // for jitter buffer underflow/overflow
    ADT_UInt16      DelayTargetMin;  // min JitterBufferDelay
    ADT_UInt16      DelayTarget;     // target JitterBufferDelay
    ADT_UInt16      DelayTargetMax;  // max JitterBufferDelay
    ADT_UInt16      JitterMode;
    ADT_UInt32      SlopeMeasureStartTime;
    ADT_Int32       SlopeMeasureStartDepth;
    ADT_Int32       AvgDepth;
    ADT_UInt8       DepthDecreaseCount;
    ADT_UInt8       DepthIncreaseCount;
    ADT_UInt16      chanId;            // chan Id of this instance

    // Session transmit state variables
    ADT_UInt16 TransmitSequence;
    ADT_UInt32 TransmitTimeStampOffset;  // Random offsets

    // Session receive state variables
    ADT_UInt32 ProbationCount, BadSeq;         // Probationary values.
    ADT_UInt16 MaxSeq;                        // Largest sequence number encountered
    ADT_UInt16 OutOfRangeCount;   // Count of consecutive timestamp range drops

    // Session jitter buffer state variables
    struct jbh * volatile OldestPyld;       // Oldest jitter buffer entry
    struct jbh * volatile YoungestPyld;     // Youngest jitter buffer entry
    ADT_UInt32 TimeStampSync;     // Application clock conversion offset.
    ADT_UInt32 LastDelivered;     // Raw timestamp of last delivered payload
    ADT_UInt32 LastPlayout;       // Last playout time, adjusted to RTP timestamp.

    // RTCP statistics (not yet implemented) support variables
    struct RTPStats {
       RTPSTATVars                // From rtpuser.h
    } stats;

    // Jitter and Clockskew Tracking
   ADT_Int32  JitterEstimate;  // current estimate of packet jitter
   ADT_UInt32 ArrivalTime;     // time of arrival for most recently arrived packet
   ADT_UInt32 PrevTransitTime; // network transit time of previous arrived packet
   ADT_Int32  JitterMin;
   ADT_Int32  JitterMax;
   ADT_UInt32 StartJitterEst;

   // Adaptive Jitter Buffer
   ADT_Int32  LastJitterEst;  // last estimate of packet jitter that is used to calculate the Jitter delay
   ADT_UInt32 AdaptStartTime;  //start time used to calculate how much time before jitter adapt
   ADT_UInt16 Est2DelayScale; //Scale to calculate jitter delay from jitter estimate
   ADT_UInt16 Dec_jitter_cnt;  // the number of consecutive frames in lower jitter stage 
   ADT_UInt16 Inc_jitter_cnt;  // the number of consecutive frames in higher jitter stage 
   ADT_UInt16 SampleRate;      // Sampling rate in Hertz

#ifdef RTCP // RTCP_STATE_I32 != 0
   RTCP_Vars_t rtcp;
#endif

   // Bit mapped fields
   unsigned int Pad32:1;               // Pad outbound packets to 32-bit boundary
   unsigned int Dummy1:15;
   unsigned int Dummy2:16;
} RTPDATA;


// this structure is used for debug only
//==========================================================================
//   Constant and structure definitions
//==========================================================================
#define SESSION_MASK    0xffff0000
#define WAIT_MASK       0x0000000f
#define SESSION_ACTIVE  0x789a0000  // Bits to indicate session is active
#define BUFFER_ACTIVE   0x00000002  // Bit to indicate jitter buffer is in use
#define TRANSMIT_ACTIVE 0x00000004  // Bit to indicate transmission is in use

// RTCP state bits
#define RTCP_ACTIVE        0x00010
#define RTCP_INITIAL       0x00020
#define RTCP_BYE_REQUESTED 0x00040

// Logging bits
#define LOG_PKT         0x00000100  // Log RTCP packet info
#define LOG_MBR         0x00000200  // Log RTCP member info
#define LOG_SCH         0x00000400  // Log RTCP scheduling info
#define LOG_SESSION     0x00001000  // Bit to indicate RTP session is to be logged in memory
#define LOG_ERR         0x00002000  // Log error messages
#define LOG_WRN         0x00004000  // Log warning messages
#define LOG_INF         0x00008000  // Log informational messages


#define RTP_SEQ_WRAP    0x10000      // Value at which sequence number wraps to zero.


#ifdef EXTERNAL_ALLOCATE
extern void (*RTPPayloadFree) (void *hndl, void*data, ADT_UInt16 dataI8);
#else
#define RTPPayloadFree(hndl,data,dataI8)
#endif

                      
#endif
