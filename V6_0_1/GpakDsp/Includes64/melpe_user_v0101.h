#ifndef MELP_USER_H
#define MELP_USER_H
 
#include "adt_typedef.h"

#ifdef NPP_PROCESS 
#include "nradt_user.h"
#endif

typedef struct{
 ADT_Int64 enc_data[820];
 } MelpE_Enc_Inst_t;

typedef struct{
 ADT_Int64 dec_data[200];
 } MelpE_Dec_Inst_t;


void MELPE_ADT_encode (ADT_Int16 sp_in[], unsigned char *, MelpE_Enc_Inst_t *ptr);
void MELPE_ADT_decode (unsigned char *, ADT_Int16 sp_out[], MelpE_Dec_Inst_t *ptr, ADT_Int16 Erase);

void	MELPE_ADT_initEncode (MelpE_Enc_Inst_t *ptr, ADT_Int16 rate, ADT_Int16 frameSize, ADT_Int32 *Scratch);
void	MELPE_ADT_initDecode (MelpE_Dec_Inst_t *ptr, ADT_Int16 rate, ADT_Int16 PostFilterEnable, ADT_Int16 frameSize, ADT_Int32 *Scratch);

#endif
