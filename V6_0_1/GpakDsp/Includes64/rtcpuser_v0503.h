/*******************************************************************

 Program name  : rtcpuser.h

 Description   : RTP/RTCP Application Program Interface header file.

 Revision History
 11/2/2015
 Update for 64-bit

 (c) Copyright 2001 -2015, Adaptive Digital Technologies, Inc.

********************************************************************/

// The following constants and structures are used to internally size RTCP data structures.
// This file must NOT be changed by the application developer.
#ifndef RTCPUSER_H  // RTCP header
#define RTCPUSER_H 1

#ifdef __BIOS__ 
   #include <adt_typedef_user.h>
#else
   #include <common/include/adt_typedef_user.h>
#endif

#define MAX_SDES_ITEMS 8
#define RTCP_STATE_I32 132   // Size of struct RTCP_Vars_t

//  Enumerated variables
typedef enum RTCP_STAT {
   RTCPSuccess = 0,
   RTCPInvalidVersion,
   RTPNotOpen,
   RTCPInUse,
   RTCPClosed,

   RTCPZeroBandwidth = 5,
   RTCPSchedulerError,
   RTCPNetTransmitError,
   RTCPInsufficientMemory,
   RTCPSDESItemCnt,

   RTCPInvalidFormat = 10
} RTCP_STAT;

typedef enum RTCP_BLOCK_TYPES {
   RTCP_SR   = 200,
   RTCP_RR   = 201,
   RTCP_SDES = 202,
   RTCP_BYE  = 203,
   RTCP_APP  = 204,
   RTCP_EXT  = 205    // Profile extension of SR/RR.
} RTCP_BLOCK_TYPES;

typedef enum SDES_ITEM_ID {
   RTCP_SDES_END = 0,    RTCP_SDES_CNAME,
   RTCP_SDES_NAME,       RTCP_SDES_EMAIL,
   RTCP_SDES_PHONE,      RTCP_SDES_LOC,
   RTCP_SDES_TOOL,       RTCP_SDES_NOTE,
   RTCP_SDES_PRIV
} SDES_ITEM_ID;


//  RTCP Notify reporting structures
typedef struct senderReport_t {
   ADT_UInt64 NTPTime;
   ADT_UInt32 RTPTime;
   ADT_UInt32 PktsSent;
   ADT_UInt32 BytesSent;
} senderReport_t;

typedef struct receiverReport_t {
   ADT_UInt8  fractionPktsLost;
   ADT_UInt32 cumPktsLost;
   ADT_UInt32 extendedMaxSeq;
   ADT_UInt32 JitterEstimate;
   ADT_UInt32 LSR;
   ADT_UInt32 DLSR;
} receiverReport_t;

typedef struct sdesReportItem_t {
   SDES_ITEM_ID type;
   ADT_UInt8    dataI8;
   ADT_UInt8   *data;
} sdesReportItem_t;

typedef struct sdesReport_t {
   ADT_UInt8 itemCnt;
   sdesReportItem_t item[MAX_SDES_ITEMS];
} sdesReport_t;

typedef struct byeReport_t {
   ADT_UInt8 reasonI8;
   ADT_UInt8 *reason;
} byeReport_t;

typedef struct appReport_t {
   ADT_UInt32 blockI8;
   ADT_UInt8  *block;
} appReport_t;

typedef union reportData_t {
  senderReport_t   SR;      // Parsed sender report fields
  receiverReport_t RR;      // Parsed receiver report fields
  sdesReport_t     SDES;    // Parsed source descriptor fields
  byeReport_t      BYE;     // Parsed bye report fields     
  appReport_t      APP;     // Pointer/length of application defined report
  appReport_t      SRExt;   // Pointer/length of extended SR/RR block
} reportData_t;

typedef struct SDES_CFG_ITEM {
   ADT_UInt8 type;             // SDES type (see SDES_ITEM_ID)
   ADT_UInt8 firstPktInCycle;  // First packet of pktsPerCycle packets to send pkt on
   ADT_UInt8 txInterval;       // Number of packets to skip between transmissions
   ADT_UInt8 dataBytes;        // Size of text data in bytes
   ADT_UInt8 *textData;        // Pointer to text data for type
} SDES_CFG_ITEM;

typedef struct SDES_CFG {
   ADT_UInt8 itemCnt;          // Number of SDES items configured
   ADT_UInt8 pktsPerCycle;     // Number of packets to form a cycle
   SDES_CFG_ITEM item[MAX_SDES_ITEMS];
} SDES_CFG;


//   RTCP Configuration structures
typedef void (RTCP_CALLBACK_FXN) (void *hndl);
typedef void (RTCP_NOTIFY_FXN)   (ADT_UInt16 chanId, ADT_UInt32 SSRC, RTCP_BLOCK_TYPES type, reportData_t *Data);

typedef ADT_UInt64 (NTPTIME_FXN) (void);
typedef ADT_Int16 (NET_FXN)  (void* RTCPNetHandle, ADT_UInt32* data, ADT_Int32* len);
typedef void (SCHEDULER_FXN) (RTCP_CALLBACK_FXN, ADT_UInt32 timeMS, void *hndl);

typedef struct rtcpConfig_APIV5_t {
   ADT_UInt16 APIVersion;                // API Version ID
   ADT_UInt16 transportOverheadBytes;    // Overhead (in bytes) added to RTCP packets due to transport layer headers
   ADT_UInt16 timeoutMultiplier;         // See RFC 3550; section 6.3.5; M
                                         //         - Multiplier to allocate for timeout 
                                         //         - Default is 5
   ADT_UInt32 minRTCPPktPeriodMs;        // See RFC 3550; section 6.3.1; Tmin
                                         //         - Minimum period (milliseconds) between RTCP transmissions
                                         //         - Default is 5000
   ADT_UInt32 rtcpBandwidthBytesPerSec;  // See RFC 3550; section 6.3; rtcp_bw * 'session bandwidth'
                                         //         - Bandwidth (bytes per second) to allocate to RTCP traffic
   ADT_Float32 senderBandwidthFraction;  // See RFC 3550; section 6.2; S / (S + R)
                                         //         - Fraction (0.0 to 1.0) of RTCP bandwidth to allocate to senders
                                         //         - Default is .25
   SCHEDULER_FXN   *Scheduler;           // Pointer to scheduling callback.
   NTPTIME_FXN     *NTPTime;             // Pointer to network time callback.
   RTCP_NOTIFY_FXN *RTCP_Notify;         // Pointer to rtcp block reporting callback.
   NET_FXN         *NetTransmit;         // Pointer to network delivery callback.
   SDES_CFG sdes;                        // Source descriptor information.
} rtcpConfig_APIV5_t;

#ifdef __BIOS__ 
   #include "rtpuser.h"
#else
   #include "rtp/include/rtpuser.h"
#endif

//   Prototypes
extern ADT_API RTCP_STAT RTCP_ADT_Init          (struct RTPCONNECT *Sess, void *RTCPNetHandle, rtcpConfig_APIV5_t *cfg);
extern ADT_API RTCP_STAT RTCP_ADT_OnReceive     (struct RTPCONNECT *Sess, void *pkt, int pktI8);
extern ADT_API RTCP_STAT RTCP_ADT_OnRTPTransmit (struct RTPCONNECT *Sess, void *pkt, int pktI8);
extern ADT_API RTCP_STAT RTCP_ADT_Bye           (struct RTPCONNECT *Sess, ADT_UInt8 *byeMsg, ADT_UInt8 byeI8);
extern ADT_API void RTCP_ADT_Close              (struct RTPCONNECT *Sess);
#endif
