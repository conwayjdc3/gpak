#ifndef _G729AB_USER_H
#define _G729AB_USER_H

// Copyright(c) 2003 Adaptive Digital Technologies Inc.
// All Rights Reserved
#ifndef _ADT_TYPEDEF
	#define _ADT_TYPEDEF

	#ifdef _TMS320C5XX
		#define ADT_Int16 short int				// 16b
		#define ADT_Int32 long int				// 32b
		#define ADT_UInt16 unsigned int			// 16b
		#define ADT_UInt32 unsigned long int	// 32b
		#define ADT_Int40 long int				// 40b
	#endif

	#ifdef _TMS320C6X
		#define ADT_Int16 short             	// 16b
		#define ADT_Int32 int               	// 32b
		#define ADT_UInt16 unsigned short   	// 32b
		#define ADT_UInt32 unsigned int     	// 32b
		#define ADT_Int40 long   				// 40b
	#endif

	#ifdef WIN32
		#define ADT_Int16 short int             // 16b
		#define ADT_Int32 long int              // 32b
		#define ADT_UInt16 unsigned int         // 32b
		#define ADT_UInt32 unsigned long int    // 32b
		#define ADT_Int40 _int64              	// 64b
	#endif

#endif //_ADT_TYPEDEF

#define ENCODE_CHAN_SIZE    537
#define ENCODE_SCRATCH_SIZE 600

#define DECODE_CHAN_SIZE    550
#define DECODE_SCRATCH_SIZE 200
typedef struct encodeChannel
{
    ADT_Int32 encodeData[ENCODE_CHAN_SIZE];
} G729ENC_ADT_Instance_t;

typedef struct encodeScratch
{
    ADT_Int32 encodeScratch[ENCODE_SCRATCH_SIZE];
} G729ENC_ADT_Scratch_t;

typedef struct decodeChannel
{
    ADT_Int32 decodeData[DECODE_CHAN_SIZE];
} G729DEC_ADT_Instance_t;

typedef struct decodeScratch
{
    ADT_Int32 decodeScratch[DECODE_SCRATCH_SIZE];
} G729DEC_ADT_Scratch_t;

// ================ Encoder Interface ==========================

void G729AB_ADT_Encode(
	G729ENC_ADT_Instance_t *Channel,
	short int *Speech,
	char *Compressed,
	short int VADEnable,
	short int *VADFlag
);

void G729AB_ADT_EncodeInit(G729ENC_ADT_Instance_t *Channel, G729ENC_ADT_Scratch_t *scratch);

void G729AB_ADT_SetEncPtrs(G729ENC_ADT_Instance_t *pInstance);


// ================ Decoder Interface ==========================

void G729AB_ADT_Decode(
	G729DEC_ADT_Instance_t *Channel,
	char *Compressed,
	short int *Speech,
	short int FrameErase,
	short int VADFlag
);

void G729AB_ADT_DecodeInit(G729DEC_ADT_Instance_t *Channel, G729DEC_ADT_Scratch_t *scratch);

void G729AB_ADT_SetDecPtrs(G729DEC_ADT_Instance_t *pInstance);

#endif //_G729AB_USER_H
