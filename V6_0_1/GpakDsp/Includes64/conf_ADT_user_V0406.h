// This file contains structures and function definitions
// for using ADT's conference software
#ifndef __CONF_ADT_USER
	#define __CONF_ADT_USER
#ifndef __ADT_TYPEDEF
	#include "adt_typedef.h"
#endif

/*
#include ".\agc\agc_user.h"
#include ".\vad\vadcng_user.h"
#include ".\intdec\intdec_user.h" 
*/

#include "agc_user_v0404.h"
//#include "vadcng_user_v0404.h"
#include "vadcng_user_v0406.h"
#include "intdec_user_v0404.h" 


#define NULL 0
// Note that there is a build option to set the allowable types of conference named CONF_BUILD_TYPE.
// It takes the values of conference type below. It is a required build option!
#ifndef CONF_BUILD_TYPE
#error "You must define a conference type of 1 (8 kHz), 2 (16 kHz) or 3 (mixed)
#endif

// Define conference types. Note that these are implicitly bit mapped. Mixed is an or of the two types
#define MEMBER_TYPE_8K_B 1		//Member has I/O sampling rate of 8 kHz
#define MEMBER_TYPE_16K_B 2		//Member has I/O sampling rate of 16 kHz
#define CONF_SUM_8K_B 4			//Conference summation is performed at 8 kHz
#define CONF_SUM_16K_B 8		//Conference summation is performed at 16 kHz

#define CONF_SUM_MIXED (MEMBER_TYPE_8K_B | MEMBER_TYPE_16K_B)

#if (CONF_BUILD_TYPE & CONF_SUM_MIXED) == CONF_SUM_MIXED
#define CONF_BUILD_MIXED 
#endif

// Define conference types
#define CONF_TYPE_8K_ONLY (MEMBER_TYPE_8K_B | CONF_SUM_8K_B)		// 8 kHz members only
#define CONF_TYPE_16K_ONLY (MEMBER_TYPE_16K_B | CONF_SUM_16K_B)		//16 kHz members only
#define CONF_TYPE_MIXED_8K (MEMBER_TYPE_8K_B | MEMBER_TYPE_16K_B | CONF_SUM_8K_B) //mixture of 8 kHz and 16 kHz members, with summation done at 8 kHz
#define CONF_TYPE_MIXED_16K (MEMBER_TYPE_8K_B | MEMBER_TYPE_16K_B | CONF_SUM_16K_B) //mixture of 8 kHz and 16 kHz members, with summation done at 16 kHz


#define CONF_INSTANCE_SIZE 96	
//#define FIRLEN 64

// structure of parameters to be passed by user at the time of configuring the conference
typedef struct
{
	ADT_Int16 ConfType;				// CONF_TYPE_8K_ONLY, CONF_TYPE_16K_ONLY, CONF_TYPE_MIXED_8K or CONF_TYPE_MIXED_16K
	ADT_Int16 MaxConferenceChannels;
	ADT_Int16 FrameSizeMSecQ4;		// Frame size in milliseconds
	ADT_Int16 DominantN;			// Dominant N
	ADT_Int16 MaxNoiseSuppression;	// range: 0..24 in dBm
	ADT_Int16 RunVADExternally;		// 0: Conference runs VAD. 1: VAD run by host
	ADT_Int16 VADNoiseFloorThresh;	// range: 0..-96 in dBm
	ADT_Int16 VADHangMSec;			// Hang time in milliseconds
	ADT_Int16 VADWindowSizeMSec;    // Window size in milliseconds 
	ADT_Int16 AGCTargetPowerIn;		// range: 0 ... -20 in dBm
	ADT_Int16 AGCMaxLossLimitIn;	// range: 0 ... -23 in dB
	ADT_Int16 AGCMaxGainLimitIn;	// range: 0 ... +23 in dB
	ADT_Int16 AGCLowSigThreshdBm;         // INPUT threshold in dBm of
	                                // low energy signal.
	                                // Range: -20 ... -65
}ConfParams_t;


//scratch space declared by user
#if 0
#define SCRATCH_SIZE ((2*MAX_FRAME_SIZE*4+MAX_CHANNELS_PER_CONFERENCE*2+MAX_DOMINANT_N*6+5*4+7)/8)
typedef struct
{
	double Conf_Scratch[SCRATCH_SIZE];
} ConfScratch_t;
#else
typedef struct
{
	ADT_Int32 Gains[MAX_FRAME_SIZE];
	union
	{
		ADT_Int16 _16[MAX_FRAME_SIZE];
		ADT_Int32 _32[MAX_FRAME_SIZE];
	}		Composite;
#ifndef V1_X
	ADT_UInt32 UnbiasedSortLevel[MAX_DOMINANT_N];
	ADT_UInt32 BiasedSortLevel[MAX_DOMINANT_N];
	ADT_Int16 UnbiasedSortIndex[MAX_DOMINANT_N];
	ADT_Int16 BiasedSortIndex[MAX_DOMINANT_N];
#endif //V1_X
	ADT_Int16 FIRScratch[MAX_FRAME_SIZE+FIRLEN];
} ConfScratch_t;

#endif


//conference channel instance to be declared by user once per conference
typedef struct
{
	double Conf_ch[(CONF_INSTANCE_SIZE+7)/8];
} ConfInstance_t;


//block of memory to be declared by user 
//when a new participant is added
typedef struct
{
	VADCNG_Instance_t *pVAD;		//pointer to the VAD channel: if allocated by user
									// 0: if user wants ADT to do the memory allocation	
	ADT_Int16 *pInPCM;				//pointer to input PCM buffer
	ADT_Int16 *pOutPCM;				//pointer to output PCM buffer
	ADT_Int16 *pLink;				//  pass NULL
	ADT_UInt32 SortLevel; 			// Signal level used for sorting
	ADT_Int16 malloc_ADT;			// pass NULL				
	ADT_Int16 DominantFlag; 		// If set, currently a dominant speaker
	ADT_Int16 Scale;				// Scale factor applied before adding this member
	ADT_Int16 VADFlag;
	ADT_Int16 MemberType;			//8 kHz or 16 kHz
	ADT_Int16 Priority;				//0 - normal, 1 - priority
	ADT_Int16 Preemptive; //0 - normal, 1 - preemptive
#ifdef CONF_BUILD_MIXED
	ADT_Int16 InFIRState[FIRLEN];
	ADT_Int16 OutFIRState[FIRLEN];
#endif
} conf_Member_t;


//function prototypes
ADT_Int16 CONF_ADT_init(ConfParams_t *ConferenceParams,		//pointer to conference parameters
				   ConfInstance_t *ConfInstance,			//pointer to conference channel instance
				   ConfScratch_t *ConfScratch, 				// Number of speakers at start of conference
				   AGCInstance_t *AgcChan_p);
	

// returns  1 if there is any activity in the conference
//			0 otherwise				  							
ADT_Int16 CONF_ADT_run(ConfInstance_t *ConfInstance,
				  ADT_Int16 *pCompositePCM);				//composite signal
				  											// 0 if coposite output is not desired

// returns  0 if member was added succesfully
//			1 otherwise				  							
ADT_Int16 CONF_ADT_addMember(ConfInstance_t *ConfInstance,	//pointer to conference channel instance
				   	   conf_Member_t *pBlock);				// pointer to member block

void CONF_ADT_setPriority(conf_Member_t *pBlock);			//sets a member as a priority speaker

void CONF_ADT_clearPriority(conf_Member_t *pBlock);			//sets a member as a normal (non-priority) speaker

void CONF_ADT_setPreemptive(ConfInstance_t *pConfInstance, conf_Member_t *pBlock);	//sets a member as a priority speaker
void CONF_ADT_clearPreemptive(ConfInstance_t *pConfInstance, conf_Member_t *pBlock);	//sets a member as a normal (non-priority) speaker
void CONF_ADT_removeMember(ConfInstance_t *ConfInstance,	//pointer to conference channel instance
							conf_Member_t *pBlock);			// pointer to member block

void CONF_ADT_close(ConfInstance_t *ConfInstance			//pointer to conference channel instance
							);

#endif //__CONF_ADT_USER
