#ifndef plus_h
#define plus_h

/*
 * Copyright (c) 2008, Adaptive Digital Technologies, Inc.
 *
 * File Name: 64plus.h
 *
 * Description:
 *    This file contains the register definitions for the 64+ family of processors
 *    that are needed specifically by the 64+ drivers.
 *
 * Version: 1.0
 *
 * Revision History:
 *   05/23/2008 - Initial release.
 *
 *
 */
#include <soc.h>

// NOTES:
//
//                          TDM     Algorithm
//   Shadow region:          0         1
//   Transfer queue:         0         
//   Transfer priority       0
typedef ADT_UInt16 ADT_Events;

#define REG_WR(addr, val) *(volatile ADT_UInt32 *)(addr) = (ADT_UInt32) val
#define REG_RD(addr, val) val = *(volatile ADT_UInt32 *)(addr) 
#define REG_AND(addr,val) *(volatile ADT_UInt32 *)(addr) &= val
#define REG_OR(addr,val)  *(volatile ADT_UInt32 *)(addr) = (*(volatile ADT_UInt32 *)addr) | val


//=================================================
// G.Pak assignments
//{=================================================

//--------- Logical interrupts
#ifdef CSL_INTC_EVENTID_EDMA3CCINT_LOCAL
   #define TDM_CompleteSgnl  CSL_INTC_EVENTID_EDMA3CCINT_LOCAL  // Completion region 0
#else
   #define CSL_INTC_EVENTID_EDMA3CC_INT0  CSL_INTC0_CPU_3_1_EDMA3CCINT0
   #define TDM_CompleteSgnl  CSL_INTC_EVENTID_EDMA3CC_INT0      // Completion region 0
#endif

#ifndef CSL_INTC_EVENTID_EDMA3TC0_ERRINT
   #define CSL_INTC_EVENTID_EDMA3TC0_ERRINT CSL_INTC0_CPU_3_1_EDMATC_ERRINT0
   #define CSL_INTC_EVENTID_EDMA3TC1_ERRINT CSL_INTC0_CPU_3_1_EDMATC_ERRINT1
   #define CSL_INTC_EVENTID_EDMA3TC2_ERRINT CSL_INTC0_CPU_3_1_EDMATC_ERRINT2
#endif
#define CSL_INTC_EVENTID_EDMA3CC_ERRINT CSL_INTC0_CPU_3_1_EDMACC_ERRINT
#if 0
#ifndef CSL_INTC_EVENTID_EDMA3TC0_ERRINT
   #define CSL_INTC_EVENTID_EDMA3TC0_ERRINT CSL_INTC_EVENTID_EDMA3CC_ERRINT0
   #define CSL_INTC_EVENTID_EDMA3TC1_ERRINT CSL_INTC_EVENTID_EDMA3CC_ERRINT1
   #define CSL_INTC_EVENTID_EDMA3TC2_ERRINT CSL_INTC_EVENTID_EDMA3CC_ERRINT2
#endif

#endif
#define CombinedEventIntr (CSL_INTC_EVENTID_EDMA3CC_ERRINT/32)                // Combined interrupt for error event.

#define TDM_CCErrSgnl     CSL_INTC_EVENTID_EDMA3CC_ERRINT  // Channel controller error
#define TDM_Que0ErrSgnl   CSL_INTC_EVENTID_EDMA3TC0_ERRINT // Queue 0 error
#define TDM_Que1ErrSgnl   CSL_INTC_EVENTID_EDMA3TC1_ERRINT // Queue 1 error
#define TDM_Que2ErrSgnl   CSL_INTC_EVENTID_EDMA3TC2_ERRINT // Queue 2 error
#define DroppedIntrSgnl   CSL_INTC_EVENTID_INTERR          // Dropped interrupt error


#define EVT_BIT(evt)  (1 << (evt%32))
#define DMA_ERROR_EVTS  (EVT_BIT (TDM_CCErrSgnl)    | EVT_BIT (TDM_Que0ErrSgnl) | \
                         EVT_BIT (TDM_Que1ErrSgnl)  | EVT_BIT (TDM_Que2ErrSgnl))

//--------- Transfer controller assignments
#define TDM_TC        0    // Transfer controller for TDM data
#define TDM_Priority  0    // Transfer controller priority for TDM data

//}---------------------------------------------
//   Host interface registers
//{---------------------------------------------
#ifdef CSL_PCI_0_REGS
#define PCI_BASE       (CSL_PCI_0_REGS)
#define PCI_STATUS_SET    ((volatile ADT_UInt32 *) (PCI_BASE + 0x10))
#define PCI_STATUS_CLR    ((volatile ADT_UInt32 *) (PCI_BASE + 0x14))
#define PCI_INT_GEN       PCI_STATUS_SET
#define PCI_INT_CLR       PCI_STATUS_CLR 

#define PCI_ALL_INTS     0x0F000000u
#define PCI_HOST_INTS     0x01000000u        //  DSP to Host Interrupt
#define PCI_DSP_INTS      0x02000000u        //  Host to DSP Interrupt

#define PCI_DSP_INT_ENABLE  ((volatile ADT_UInt32 *) (PCI_BASE + 0x30))
#define PCI_DSP_INT_DISABLE ((volatile ADT_UInt32 *) (PCI_BASE + 0x34))
#define PCI_INT_EVENT      56
#endif
   
#define HPI_BASE       (CSL_HPI_0_REGS)
#define HPIC           ((volatile ADT_UInt32 *) (HPI_BASE + 0x30))
#define HPI_HOST_INT   0x04
#define HPI_DSP_INT    0x02

#define HPI_INT_EVENT       47

//}---------------------------------------------
//  Serial port DMA parameters 
//{---------------------------------------------

#define REG64_WR(addr,val) *(volatile ADT_Int64 *)(addr) = (ADT_Int64) (val)
#define REG64_RD(addr,val) val = *(volatile ADT_Int64 *) (addr)

//}------------------------------------------------------------------------------------
// EDMA registers
//{------------------------------------------------------------------------------------

#define ALL_EVENTS 0xffffffffffffffffl

#define EDMA_CFG_BASE  (CSL_EDMA3CC_0_REGS)  // base address of global EDMA3 registers

// Configuration
#define EDMA_CFG      (EDMA_CFG_BASE + 0x0004) // Configuration sizing
#define EDMA_CH_MAP   (EDMA_CFG_BASE + 0x0100) // Channel to parameter mapping

// Event queueing
#define EDMA_QUE0     (EDMA_CFG_BASE + 0x0240) // Transfer controller queue assignment register
#define EDMA_QUEPRI   (EDMA_CFG_BASE + 0x0284) // Transfer controller queue priority register

// Missed event notification
#define EDMA_EMR      (EDMA_CFG_BASE + 0x0300) // Missed event notification
#define EDMA_EMCR     (EDMA_CFG_BASE + 0x0308) // Missed event notification clear

// Error notification
#define EDMA_CCERR    (EDMA_CFG_BASE + 0x0318) // TC queue overrun error
#define EDMA_CCERRCLR (EDMA_CFG_BASE + 0x031C) // TC queue overrun error clear
#define EDMA_ERREVAL  (EDMA_CFG_BASE + 0x0320) // TC queue overrun error eval

// Shadow region assignment
#define EDMA_SHAD0    (EDMA_CFG_BASE + 0x0340) // Shadow region 0 assignment register
#define EDMA_SHAD1    (EDMA_CFG_BASE + 0x0348) // Shadow region 1 assignment register


//}---------------------------
// EDMA event registers
//{---------------------------
#if SHADOW_REGION == 0
   #define EDMA_SHADOW_BASE  (CSL_EDMA3CC_0_REGS + 0x2000)  // base address of shadow region 0 registers
   #define EDMA_SHADOW_ASGN  (EDMA_SHAD0)
#elif SHADOW_REGION == 1
   #define EDMA_SHADOW_BASE  (CSL_EDMA3CC_0_REGS + 0x2200)  // base address of shadow region 1 registers
   #define EDMA_SHADOW_ASGN  (EDMA_SHAD1)
#else
   #define EDMA_SHADOW_BASE  (CSL_EDMA3CC_0_REGS + 0x1000)  // base address of global region registers
   #undef EDMA_SHADOW_ASGN
#endif
#define EDMA_TCC_GLOBAL (CSL_EDMA3CC_0_REGS + 0x1000)  // base address of global region registers

// Event notification
#define EDMA3_ENR  (EDMA_SHADOW_BASE + 0x00)   // Event notification
#define EDMA3_ENCR (EDMA_SHADOW_BASE + 0x08)   // Event notification clear
#define EDMA3_ENSR (EDMA_SHADOW_BASE + 0x10)   // Event notification set

// Event enabling
#define EDMA3_EER  (EDMA_SHADOW_BASE + 0x20)  // Event enabled
#define EDMA3_EECR (EDMA_SHADOW_BASE + 0x28)  // Event enable clear
#define EDMA3_EESR (EDMA_SHADOW_BASE + 0x30)  // Event enable set

// Second occurance of event before clearing of first event
#define EDMA3_SNR  (EDMA_SHADOW_BASE + 0x38)   // Secondary event notification
#define EDMA3_SNCR (EDMA_SHADOW_BASE + 0x40)   // Secondary event notification clear

// Event interrupt enabling
#define EDMA3_IER  (EDMA_SHADOW_BASE + 0x50)  // Event interrupt enabled
#define EDMA3_IECR (EDMA_SHADOW_BASE + 0x58)  // Event interrupt enable clear
#define EDMA3_IESR (EDMA_SHADOW_BASE + 0x60)  // Event interrupt enable set

// Interrupt notification
#define EDMA3_INR  (EDMA_SHADOW_BASE + 0x68)   // Interrupt notification
#define EDMA3_INCR (EDMA_SHADOW_BASE + 0x70)   // Interrupt notification clear
#define EDMA3_IEVL (EDMA_SHADOW_BASE + 0x78)   // Interrupt evaluator



// DMA Global Clearing
#define EDMA_GLB_ENCR (EDMA_SHADOW_BASE + 0x08)  // Event notification clear
#define EDMA_GLB_EECR (EDMA_SHADOW_BASE + 0x28)  // Event enable clear
#define EDMA_GLB_SNCR (EDMA_SHADOW_BASE + 0x40)  // Secondary event notification clear
#define EDMA_GLB_IECR (EDMA_SHADOW_BASE + 0x58)  // Event interrupt enable clear
#define EDMA_GLB_INCR (EDMA_SHADOW_BASE + 0x70)  // Interrupt notification clear
#define EDMA_GLB_IEVL (EDMA_SHADOW_BASE + 0x78)  // Interrupt evaluator

#define TSIP_RX_SF_INT0 (EVT_BIT(CSL_GEM_TSIP0_RSFINT_N))  // rx superframe0 event 0x35
#define TSIP_TX_SF_INT0 (EVT_BIT(CSL_GEM_TSIP0_XSFINT_N))  // tx superframe0 event 0x37
#define TSIP_RX_SF_INT1 (EVT_BIT(CSL_GEM_TSIP1_RSFINT_N))  // rx superframe1 event 0x39
#define TSIP_TX_SF_INT1 (EVT_BIT(CSL_GEM_TSIP1_XSFINT_N))  // tx superframe1 event 0x3b
#define TSIP_ERR_INT0   (EVT_BIT(CSL_GEM_TSIP0_ERRINT_N))  // error0 event 0x12
#define TSIP_ERR_INT1   (EVT_BIT(CSL_GEM_TSIP1_ERRINT_N))  // error1 event 0x13

#define TSIP_RX_INTRS      (TSIP_RX_SF_INT0 | TSIP_RX_SF_INT1 )
#define TSIP_TX_INTRS      (TSIP_TX_SF_INT0 | TSIP_TX_SF_INT1 )
#define TSIP_COMPLETE_EVTS (TSIP_RX_INTRS | TSIP_TX_INTRS)
#define TSIP_ERROR_EVTS    (TSIP_ERR_INT0 | TSIP_ERR_INT1 )
#define TSIP_EVTS          (TSIP_COMPLETE_EVTS | TSIP_ERROR_EVTS)
#ifdef CSL_TSIP_CNT

   #define TSIP_RX_SF_INT0 (EVT_BIT(CSL_INTC_EVENTID_RSFINT0))  // rx superframe0 event 33
   #define TSIP_TX_SF_INT0 (EVT_BIT(CSL_INTC_EVENTID_XSFINT0))  // tx superframe0 event 35
   #define TSIP_RX_SF_INT1 (EVT_BIT(CSL_INTC_EVENTID_RSFINT1))  // rx superframe1 event 37
   #define TSIP_TX_SF_INT1 (EVT_BIT(CSL_INTC_EVENTID_XSFINT1))  // tx superframe1 event 39
   #define TSIP_RX_SF_INT2 (EVT_BIT(CSL_INTC_EVENTID_RSFINT2))  // rx superframe2 event 41
   #define TSIP_TX_SF_INT2 (EVT_BIT(CSL_INTC_EVENTID_XSFINT2))  // tx superframe2 event 43
   #define TSIP_ERR_INT0   (EVT_BIT(CSL_INTC_EVENTID_ERRINT0))  // error0 event 50
   #define TSIP_ERR_INT1   (EVT_BIT(CSL_INTC_EVENTID_ERRINT1))  // error1 event 52
   #define TSIP_ERR_INT2   (EVT_BIT(CSL_INTC_EVENTID_ERRINT2))  // error2 event 54

   #define TSIP_RX_INTRS      (TSIP_RX_SF_INT0 | TSIP_RX_SF_INT1 | TSIP_RX_SF_INT2)
   #define TSIP_TX_INTRS      (TSIP_TX_SF_INT0 | TSIP_TX_SF_INT1 | TSIP_TX_SF_INT2)
   #define TSIP_COMPLETE_EVTS (TSIP_RX_INTRS | TSIP_TX_INTRS)
   #define TSIP_ERROR_EVTS    (TSIP_ERR_INT0 | TSIP_ERR_INT1 | TSIP_ERR_INT2)
   #define TSIP_EVTS          (TSIP_COMPLETE_EVTS | TSIP_ERROR_EVTS)
#endif


//}---------------------------
// EDMA parameter registers
//{---------------------------
#define EDMA_PARAM_ADDR   (CSL_EDMA3CC_0_REGS + 0x4000) // base address of EDMA parameter registers
#define PARAM_SIZE 32
#if 0
typedef union {
    ADT_UInt32 TransferOptions;
    struct {
        unsigned SAM:1;
        unsigned DAM:1;
        unsigned SYNCDIM:1;
        unsigned STATIC:1;
        unsigned rsvd1:4;
        unsigned FWID:3;
        unsigned TCCMOD:1;
        unsigned TCC:6;
        unsigned rsvd2:2;
        unsigned TCINTEN:1;
        unsigned ITCINTEN:1;
        unsigned TCCHEN:1;
        unsigned ITCCHEN:1;
        unsigned PRIVID:4;
        unsigned rsvd3:4;
    } Bits;
} dma3Opts_t;

typedef union dma3Opts_t {
    ADT_UInt32 TransferOptions;
    struct DmaOptBits {
        unsigned SAM:1;      // Constant source addressing
        unsigned DAM:1;      // Constant destination addressing
        unsigned SYNCDIM:1;  // A-B transfers
        unsigned STATIC:1;   // PARAM is not updated after TR is submitted (0 should be used for DMA)

        unsigned rsvd1:4;

        unsigned FWID:3;     // FIFO width
        unsigned TCCMOD:1;   // Early transfer complete (TR submitted)

        unsigned TCC:6;      // Transfer complete event
        unsigned rsvd2:2;

        unsigned TCINTEN:1;  // Generate IPR on final TRs
        unsigned ITCINTEN:1; // Generate IPR on intermediate TRs
        unsigned TCCHEN:1;   // Generate CER on final TRs
        unsigned ITCCHEN:1;  // Generate CER on intermediate TRs

        unsigned PRIVID:4;   // Read only - priviledge of requestor

        unsigned rsvd3:4;
    } Bits;
} dma3Opts_t;
#endif
#define assignToTC(chn,que)                \
   {  ADT_UInt32 *asgReg, *cfgReg;         \
      ADT_UInt32 value, asgMask;           \
      asgReg = (ADT_UInt32 *) EDMA_QUE0;   \
      asgReg += (chn)>>3;                  \
      REG_RD (asgReg, value);              \
      asgMask = 7 << (((chn)&7)*4);        \
      value &= ~asgMask;                   \
      value |= (que) << (((chn)&7)*4);     \
      REG_WR (asgReg, value);              \
      REG_RD (EDMA_CFG, value);            \
      if (value & 0x07000000) {            \
         cfgReg = (ADT_UInt32 *) (EDMA_CH_MAP + (chn)*4); \
         REG_WR (cfgReg, (chn) << 5);      \
      }                                    \
   }

#define assignPriority(que,priority)         \
   {  ADT_UInt32 *asgReg;                    \
      ADT_UInt32 value, asgMask;             \
      asgReg = (ADT_UInt32 *) EDMA_QUEPRI;   \
      asgMask = 7 << (que*4);                \
      REG_RD (asgReg, value);                \
      value &= ~asgMask;                     \
      value |= priority << (que*4);          \
      REG_WR (asgReg, value);                \
   }


//}------------------------------------------------------------------------------------
// Transfer controller registers
//{------------------------------------------------------------------------------------

#if 0
#define TC0_BASE (CSL_EDMA3TC_0_REGS)
#define TC1_BASE (CSL_EDMA3TC_1_REGS)
#define TC2_BASE (CSL_EDMA3TC_2_REGS)
#endif

#define TC0_BASE CSL_EDMA1TC0_REGS
#define TC1_BASE CSL_EDMA1TC1_REGS
#define TC2_BASE CSL_EDMA1TC1_REGS

#define TC0_ERR_STAT (TC0_BASE + 0x0120) // Transfer control error status
#define TC1_ERR_STAT (TC1_BASE + 0x0120) //
#define TC2_ERR_STAT (TC2_BASE + 0x0120) //

#define TC0_ERR_STAT_ENBL (TC0_BASE + 0x0124) // Transfer control error status enable
#define TC1_ERR_STAT_ENBL (TC1_BASE + 0x0124) //
#define TC2_ERR_STAT_ENBL (TC2_BASE + 0x0124) //

#define TC0_ERR_STAT_CLR  (TC0_BASE + 0x0128) // Transfer control error status clear
#define TC1_ERR_STAT_CLR  (TC1_BASE + 0x0128) //
#define TC2_ERR_STAT_CLR  (TC2_BASE + 0x0128) //

#define TC0_ERR  (TC0_BASE + 0x012C) // Transfer control error details
#define TC1_ERR  (TC1_BASE + 0x012C) // 
#define TC2_ERR  (TC2_BASE + 0x012C) // 

#define TC0_EVAL (TC0_BASE + 0x0130) // Transfer control error pulse
#define TC1_EVAL (TC1_BASE + 0x0130) // 
#define TC2_EVAL (TC2_BASE + 0x0130) // 


//}------------------------------------------------------------------------------------
// Interrupt controller registers
//{------------------------------------------------------------------------------------
#ifdef CSL_SYS_0_INT_CNTL_REGS
   #define INTR_CTL_BASE (CSL_SYS_0_INT_CNTL_REGS)
#endif

#ifdef CSL_INTC_0_REGS
   #define INTR_CTL_BASE (CSL_INTC_0_REGS)
#endif

#define INTR_PND       (INTR_CTL_BASE)
#define INTR_CLR       (INTR_CTL_BASE + 0x40) // Interrupt signal clear (128 bits)
#define INTR_CMB       (INTR_CTL_BASE + 0x80) // Interrupt signal combine (128 bits)

#define INTR_EXCP_0    (INTR_CTL_BASE + 0xC0)  // Interrupt signal to exception vector mapping
#define INTR_XSTAT     (INTR_CTL_BASE + 0x180) // Interrupt exception status
#define INTR_XSTAT_CLR (INTR_CTL_BASE + 0x184) // Interrupt exception status clear

#define INTR_MUX_0     (INTR_CTL_BASE + 0x104) // Interrupt signal to HW vector mapping

#define CMB_EVT_PND (INTR_PND + (4 * CombinedEventIntr))   // event combiner interrupt pending for events
#define CMB_EVT_CLR (INTR_CLR + (4 * CombinedEventIntr))   // event combiner interrupt disable for events
#define CMB_EVT_ENB (INTR_CMB + (4 * CombinedEventIntr))   // event combiner interrupt enable for events


#define clearInterrupt(sgnl)                 \
   {  ADT_UInt32 *cReg;                      \
      ADT_UInt32 bits;                       \
      cReg  = (ADT_UInt32 *) INTR_CLR;       \
      cReg += sgnl / 32;                     \
      bits  = (1 << (sgnl&0x1F));            \
      REG_WR (cReg, bits);                   \
   }

#define mapInterrupt(sgnl,vect)    HWI_eventMap (vect,sgnl)

#define assignErrorInterrupt(sgnl)           \
   {  ADT_UInt32 *xReg;                      \
      ADT_UInt32 value;                      \
      xReg  = (ADT_UInt32 *) INTR_CMB;       \
      xReg += sgnl / 32;                     \
      REG_RD (xReg, value);                  \
      value &= ~(1 << (sgnl&0x1F));          \
      REG_WR (xReg, value);                  \
   }

#define assignToCombiner assignErrorInterrupt

#define HWI_DMA_COMPLETE 4    // HW Interrupt vector for EDMA completions
#define HWI_COMBINED_INT 5       // Interrupts from combiner
#define HWI_FRAME_READY 6        // Interrupt from scheduler to framing task cores
//  HWI_TDM_ERR      =  7        Set by DMA module
//  HWI_TDM_COMPLETE =  8        Set by DMA module
//  HWI_EMAC_RCV     =  9        Forced by IP stack
//  HWI_EMAC_XMT     = 10        Forced by IP stack
//  HWI_RTDX_XMT     = 11        Forced by BIOS
//  HWI_RTDX_RCV     = 12        Forced by BIOS
#define HWI_HOST       13        // Interrupt from host (data available)
//  HWI_TIMER        = 14        Forced by BIOS
//  HWI_TIMER        = 15        Forced by BIOS

//----------------------------------------------------------------------------   
// NOTE:  The TDM DMA channel assignments must NOT conflict with the
//        DMA channel assignments used for the algorithm instance DMA


//}------------------------------------------------------------------------------------
// McASP registers 
//{------------------------------------------------------------------------------------
#define NUM_MCASP_PORTS CSL_MCASP_PER_CNT
#define McASP0_BASE     (CSL_MCASP_0_REGS)           // Cfg bus address
#define McASP0_DATA     (CSL_MCASP_0_DATA_BASE_ADDR) // Data bus address
#define McASPTx0_Chn    (EDMA3_DRV_HW_CHANNEL_MCASP0_TX)
#define McASPRx0_Chn    (EDMA3_DRV_HW_CHANNEL_MCASP0_RX)



//}------------------------------------------------------------------------------------
// McBSP registers 
//{------------------------------------------------------------------------------------
#ifndef CSL_MCBSP_0_REGS
#define CSL_MCBSP_0_REGS 0xf0000000
#define EDMA3_DRV_HW_CHANNEL_MCBSP0_TX 255
#define EDMA3_DRV_HW_CHANNEL_MCBSP0_RX 255
#endif

#ifndef CSL_MCBSP_1_REGS
#define CSL_MCBSP_1_REGS 0xf0000000
#define EDMA3_DRV_HW_CHANNEL_MCBSP1_TX 255
#define EDMA3_DRV_HW_CHANNEL_MCBSP1_RX 255
#endif

#ifndef CSL_MCBSP_2_REGS
#define CSL_MCBSP_2_REGS 0xf0000000
#endif

#define McBSP0_BASE  (CSL_MCBSP_0_REGS)
#define McBSP1_BASE  (CSL_MCBSP_1_REGS)
#define McBSP2_BASE  (CSL_MCBSP_2_REGS)
#define McBSPTx0_Chn (EDMA3_DRV_HW_CHANNEL_MCBSP0_TX)
#define McBSPRx0_Chn (EDMA3_DRV_HW_CHANNEL_MCBSP0_RX)
#define McBSPTx1_Chn (EDMA3_DRV_HW_CHANNEL_MCBSP1_TX)
#define McBSPRx1_Chn (EDMA3_DRV_HW_CHANNEL_MCBSP1_RX)
#define McBSPTx0_Addr (CSL_MCBSP_0_REGS + 4)
#define McBSPRx0_Addr (CSL_MCBSP_0_REGS)
#define McBSPTx1_Addr (CSL_MCBSP_1_REGS + 4)
#define McBSPRx1_Addr (CSL_MCBSP_1_REGS)

// DMA ping and pong (pong = ping + 1)
#define Tx0_Ping 64
#define Rx0_Ping 66
#define Tx1_Ping 68
#define Rx1_Ping 70
#define Tx2_Ping 72
#define Rx2_Ping 74



//}------------------------------------------------
// Pin Mux Registers.  These registers control which peripherals 
// are connected to which input signals.
//{------------------------------------------------
#ifdef CSL_SYS_0_REGS
#define DEV_CFG_BASE            CSL_SYS_0_REGS
#define CFG_PINMUX0             (volatile ADT_UInt32*)(DEV_CFG_BASE + 0x00)
#define CFG_PINMUX1             (volatile ADT_UInt32*)(DEV_CFG_BASE + 0x04)
#define CFG_VDD3P3V_PWDN        (volatile ADT_UInt32*)(DEV_CFG_BASE + 0x48)
#endif

// Power module bit definitions for CFG_VDD3P3V_PWDN
#define MUX_BLK_UART      0x0C00
#define MUX_BLK_TIMER1    0x0200
#define MUX_BLK_McBSPs    0x0180
#define MUX_BLK_McASPs    0x0080
#define MUX_BLK_EMAC_MII  0x0010
#define MUX_BLK_HPI       0x0010
#define MUX_BLK_EMAC_RMII 0x0006
#define MUX_BLK_PCI       0x303E

// MUX0 Register bits
#define MUX0_RMIIMask     0xFFFFFFF7
#define MUX0_RMIIActive   0x00000008

// MUX0 Register bits
#define MUX1_McBSPMask    0xFC3CFFFF
#define MUX1_McBSPActive  0x01420000

#define MUX1_McASPMask    0xFC3FFFFF
#define MUX1_McASPActive  0x02800000

#define MUX1_Timer1Mask   0xFFCFFFFF
#define MUX1_Timer1Active 0x00100000

#define MUX1_PCIMask      0xFFFFFF8E
#define MUX1_PCIActive    0x00000001

#define MUX1_HPIMask      0xFFFFFF8E
#define MUX1_HPIActive    0x00000010

//}------------------------------------------------
// Power and sleep control registers.  These registers control
// which peripherals are powered on
//{------------------------------------------------
#ifdef CSL_PSC_0_REGS
   #define PSC_BASE     CSL_PSC_0_REGS
#else
   #ifdef CSL_PSC_REGS
      #define PSC_BASE     CSL_PSC_REGS
   #endif
#endif

#define PSC_CMD   ((volatile ADT_UInt32 *) (PSC_BASE + 0x120))  // PTCMD
#define PSC_STAT  ((volatile ADT_UInt32 *) (PSC_BASE + 0x128))  // PTSTAT
#define PSC_REG   ((volatile ADT_UInt32 *) (PSC_BASE + 0xA00))  // MDCTLn

#define PSC_EDMA_CC   2  // Channel controller
#define PSC_EDMA_TC0  3
#define PSC_EDMA_TC1  4
#define PSC_EDMA_TC2  5

#define PSC_EMAC_CTRL 6
#define PSC_MDIO      7
#define PSC_EMAC      8

#define PSC_TSIP0    9
#define PSC_TSIP1    10
#define PSC_TSIP2    11
#define PSC_McASP0    9

#define PSC_HPI      12  // Should be set by host for HPI access
#define PSC_DDR2     13  // Should be set by host 
#define PSC_PCI      15  // Should be set by host for PCI access

#define PSC_McBSP0   16
#define PSC_McBSP1   17
#define PSC_I2C      18

#define PSC_GPIO     26
#define PSC_TIMER1   28

#define PSC_OFF 2
#define PSC_ON  3



//}-----------------------
// Cache control registers.
//{------------------------------------------------
#define CACHE_BASE  CSL_CACHE_0_REGS
#define L2WBINV     (volatile ADT_UInt32 *) (CACHE_BASE + 0x5004)

#endif
