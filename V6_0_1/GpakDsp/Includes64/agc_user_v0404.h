/*******************************************************************

 Program name  : AGC_user.H

 Description : public header file for Automatic Gain Control Algorithm.

 Date   :  6/16/2004

 Version  :  2.0

 Revision History

 (c) Copyright 2004, Adaptive Digital Technologies, Inc.

********************************************************************/
#ifndef AGC_USER_H
#define AGC_USER_H
#include "adt_typedef.h"

#define AGC_SIZE 17

typedef struct
{
    ADT_Int64 agcvars[10];
} AGCInstance_t;

// Define sampling rates in Hz
#define FS_8000 8000
#define FS_12000 12000
#define FS_16000 16000

typedef struct 
{
   ADT_Int16  targetPowerIn;      // INPUT range: 0 ... -30
   ADT_Int16  maxLossLimitIn;     // INPUT range: 0 ... -23
   ADT_Int16  maxGainLimitIn;     // INPUT range: 0 ... +23
   //ADT_Int16  NAGCSamples;        // INPUT number of samples
   ADT_Int16  lowSigThreshDBM;    // INPUT threshold in dBm of low energy signal 
   ADT_Int16  StickyControl;      // INPUT Flag, 0/1/2, to remember gain when signal below above
   ADT_Int16  StickyInitialGain;  // INPUT range: -23 ... +23
   ADT_Int16  SamplingRate;       // INPUT sampling rate of the input/output data

} AGC_ADT_Param_t;

void AGC_ADT_agcinit(
   AGCInstance_t * pChannel,
   AGC_ADT_Param_t * pParams
);

ADT_Int16 AGC_ADT_run(
   AGCInstance_t *pChannel,
   ADT_Int16 * pInputSamples,
   ADT_Int16 FrameSize,
   ADT_Int16 ForceLowSignalState,	// INPUT
   ADT_Int16 BlockShift,
   ADT_Int16 * pOutputSamples,
   ADT_Int32 * Gains);

/* question: need to return the signal power level for SPLIT INTERFACE?  */
ADT_Int16 AGC_ADT_computeGain(
  AGCInstance_t *pInstance,
  ADT_Int16 * pInputSamples,
  ADT_Int16 FrameSize,
  ADT_Int16 BlockShift
);
void AGC_ADT_applyGain(
  AGCInstance_t *pInstance,
  ADT_Int16 * pInputSamples,
  ADT_Int16 FrameSize,
  ADT_Int16 ForceLowSignalState,
  ADT_Int16 * pOutputSamples,
  ADT_Int32 * Gains);

#endif  /* file inclusion */
