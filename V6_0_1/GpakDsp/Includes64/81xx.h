#ifndef _81xx_h
#define _81xx_h
/*
 * Copyright (c) 2008, Adaptive Digital Technologies, Inc.
 *
 * File Name: 811x.h
 *
 * Description:
 *    This file contains the register definitions for the 814x, 816x family of processors
 *    that are needed specifically by the 81xxx drivers.
 *
 * Version: 1.0
 *
 * Revision History:
 *   05/23/2008 - Initial release.
 *
 *
 */
#include <soc.h>
#define NUM_TDM_PORTS 3

typedef ADT_UInt16 ADT_Events;

#define REG_WR(addr, val) *(volatile ADT_UInt32 *)(addr) = (ADT_UInt32) val
#define REG_RD(addr, val) val = *(volatile ADT_UInt32 *)(addr) 
#define REG_AND(addr,val) *(volatile ADT_UInt32 *)(addr) &= val
#define REG_OR(addr,val)  *(volatile ADT_UInt32 *)(addr) = (*(volatile ADT_UInt32 *)addr) | val

#define REG64_WR(addr,val) *(volatile ADT_Int64 *)(addr) = (ADT_Int64) (val)
#define REG64_RD(addr,val) val = *(volatile ADT_Int64 *) (addr)

//---------------------------------------------
//   Host interface registers
//{---------------------------------------------

//}------------------------------------------------------------------------------------
// EDMA registers
//{------------------------------------------------------------------------------------
// Configuration
#define EDMA_CFG_BASE  (CSL_EDMA30CC_0_REGS)  // base address of global EDMA3 registers
#define EDMA_CFG       (EDMA_CFG_BASE + 0x0004) // Configuration sizing
#define EDMA_CH_MAP    (EDMA_CFG_BASE + 0x0100) // Channel to parameter mapping
#define EDMA_WATERMARK (EDMA_CFG_BASE + 0x0620) // Watermark warning thresholds

// Event queueing
#define EDMA_QUE0     (EDMA_CFG_BASE + 0x0240) // Transfer controller queue assignment register
#define EDMA_QUEPRI   (EDMA_CFG_BASE + 0x0284) // Transfer controller queue priority register

// Missed event notification
#define EDMA_EMR      (EDMA_CFG_BASE + 0x0300) // Missed event notification
#define EDMA_EMRH     (EDMA_CFG_BASE + 0x0304) // Missed event notification
#define EDMA_EMCR     (EDMA_CFG_BASE + 0x0308) // Missed event notification clear
#define EDMA_EMCRH    (EDMA_CFG_BASE + 0x030C) // Missed event notification clear

// Error notification
#define EDMA_CCERR    (EDMA_CFG_BASE + 0x0318) // TC queue overrun error
#define EDMA_CCERRCLR (EDMA_CFG_BASE + 0x031C) // TC queue overrun error clear
#define EDMA_ERREVAL  (EDMA_CFG_BASE + 0x0320) // TC queue overrun error eval

// NOTE:  DMA Queue_n is assigned to TransferController_n
// Assign dma channels to a specified queue / transfer controlller
#define EDMA_QUE_TDM_TX 0
#define EDMA_QUE_TDM_RX 1

#define assignToTCQueue(chn,que)           \
   {  ADT_UInt32 *asgReg, *cfgReg;         \
      ADT_UInt32 value, asgMask;           \
      asgReg = (ADT_UInt32 *) EDMA_QUE0;   \
      asgReg += (chn)>>3;                  \
      REG_RD (asgReg, value);              \
      asgMask = 7 << (((chn)&7)*4);        \
      value &= ~asgMask;                   \
      value |= (que) << (((chn)&7)*4);     \
      REG_WR (asgReg, value);              \
      REG_RD (EDMA_CFG, value);            \
      if (value & 0x01000000) {            \
         cfgReg = (ADT_UInt32 *) (EDMA_CH_MAP + (chn)*4); \
         REG_WR (cfgReg, (chn) << 5);      \
      }                                    \
   }

//}---------------------------
// EDMA events and event registers
//{---------------------------
#define McASPRx0_Chn  (CSL_EDMA3_CHA_MCASP0_RX)
#define McASPTx0_Chn  (CSL_EDMA3_CHA_MCASP0_TX)

#define McASPRx1_Chn  (CSL_EDMA3_CHA_MCASP1_RX)
#define McASPTx1_Chn  (CSL_EDMA3_CHA_MCASP1_TX)

#define McASPRx2_Chn  (CSL_EDMA3_CHA_MCASP2_RX)
#define McASPTx2_Chn  (CSL_EDMA3_CHA_MCASP2_TX)

#define McASPRx3_Chn  (CSL_EDMA3_CHA_MCASP3_RX)
#define McASPTx3_Chn  (CSL_EDMA3_CHA_MCASP3_TX)

#define McASPRx4_Chn  (CSL_EDMA3_CHA_MCASP4_RX)
#define McASPTx4_Chn  (CSL_EDMA3_CHA_MCASP4_TX)

#define McASPRx5_Chn  (CSL_EDMA3_CHA_MCASP5_RX)
#define McASPTx5_Chn  (CSL_EDMA3_CHA_MCASP5_TX)

// TCC codes ... 'somewhat' arbitrary must be sure that ARM-side EDMA is not in comflict
#define McASPRx0_Tcc   (0)
#define McASPTx0_Tcc   (1)
                       
#define McASPRx1_Tcc   (2)
#define McASPTx1_Tcc   (3)

#define McASPRx2_Tcc   (4)
#define McASPTx2_Tcc   (5)

#define McASPRx3_Tcc   (6)
#define McASPTx3_Tcc   (7)

#define McASPRx4_Tcc   (8)
#define McASPTx4_Tcc   (9)

#define McASPRx5_Tcc   (10)
#define McASPTx5_Tcc   (11)



#define McBSPTx0_Chn  (CSL_EDMA3_CHA_MCBSP0_TX)
#define McBSPRx0_Chn  (CSL_EDMA3_CHA_MCBSP0_RX)

#define EDMA_GLOBAL_BASE (EDMA_CFG_BASE + 0x1000)  // base address of global region registers


// Shadow region assignment
#define EDMA_SHAD0     (EDMA_CFG_BASE + 0x0340) // Shadow region 0 assignment register
#define EDMA_SHAD0H    (EDMA_CFG_BASE + 0x0344) // Shadow region 0 assignment register
#define EDMA_SHAD1     (EDMA_CFG_BASE + 0x0348) // Shadow region 1 assignment register
#define EDMA_SHAD1H    (EDMA_CFG_BASE + 0x034C) // Shadow region 1 assignment register
#define EDMA_SHAD_DMA  EDMA_SHAD1              //
#define EDMA_SHAD_DMAH EDMA_SHAD1H             //
#define EDMA_SHADOW_BASE (EDMA_CFG_BASE + 0x2200)  // base address of shadow region01 registers

// Event notification
#define EDMA_SHADOW_EPR   (EDMA_SHADOW_BASE + 0x00)   // Event pending
#define EDMA_SHADOW_EPRH  (EDMA_SHADOW_BASE + 0x04)   // Event pending high
#define EDMA_SHADOW_EPCR  (EDMA_SHADOW_BASE + 0x08)   // Event pending clear
#define EDMA_SHADOW_EPCRH (EDMA_SHADOW_BASE + 0x0C)   // Event pending clear high
#define EDMA_SHADOW_EPSR  (EDMA_SHADOW_BASE + 0x10)   // Event pending set
#define EDMA_SHADOW_EPSRH (EDMA_SHADOW_BASE + 0x14)   // Event pending set high

// Event enabling
#define EDMA_SHADOW_EER   (EDMA_SHADOW_BASE + 0x20)  // Event enabled
#define EDMA_SHADOW_EERH  (EDMA_SHADOW_BASE + 0x24)  // Event enabled high
#define EDMA_SHADOW_EECR  (EDMA_SHADOW_BASE + 0x28)  // Event enable clear
#define EDMA_SHADOW_EECRH (EDMA_SHADOW_BASE + 0x2C)  // Event enable clear high
#define EDMA_SHADOW_EESR  (EDMA_SHADOW_BASE + 0x30)  // Event enable set
#define EDMA_SHADOW_EESRH (EDMA_SHADOW_BASE + 0x34)  // Event enable set high

// Second occurance of event before clearing of first event
#define EDMA_SHADOW_SPR   (EDMA_SHADOW_BASE + 0x38)   // Secondary event pending
#define EDMA_SHADOW_SPRH  (EDMA_SHADOW_BASE + 0x3C)   // Secondary event pending high
#define EDMA_SHADOW_SPCR  (EDMA_SHADOW_BASE + 0x40)   // Secondary event pending clear
#define EDMA_SHADOW_SPCRH (EDMA_SHADOW_BASE + 0x44)   // Secondary event pending clear high

// Event interrupt enabling
#define EDMA_SHADOW_IER  (EDMA_SHADOW_BASE + 0x50)  // Event interrupt enabled
#define EDMA_SHADOW_IECR (EDMA_SHADOW_BASE + 0x58)  // Event interrupt enable clear
#define EDMA_SHADOW_IESR (EDMA_SHADOW_BASE + 0x60)  // Event interrupt enable set

// Interrupt notification
#define EDMA_SHADOW_IPR  (EDMA_SHADOW_BASE + 0x68)   // Interrupt pending
#define EDMA_SHADOW_IPCR (EDMA_SHADOW_BASE + 0x70)   // Interrupt pending clear
#define EDMA_SHADOW_IEVL (EDMA_SHADOW_BASE + 0x78)   // Interrupt evaluator

// DMA Global Clearing
#define EDMA_GLB_EPCR  (EDMA_GLOBAL_BASE + 0x08)  // Event pending clear
#define EDMA_GLB_EPCRH (EDMA_GLOBAL_BASE + 0x0C)  // Event pending clear high
#define EDMA_GLB_EECR  (EDMA_GLOBAL_BASE + 0x28)  // Event enable clear
#define EDMA_GLB_EECRH (EDMA_GLOBAL_BASE + 0x2C)  // Event enable clear high
#define EDMA_GLB_SPCR  (EDMA_GLOBAL_BASE + 0x40)  // Secondary event pending clear
#define EDMA_GLB_SPCRH (EDMA_GLOBAL_BASE + 0x44)  // Secondary event pending clear
#define EDMA_GLB_IECR  (EDMA_GLOBAL_BASE + 0x58)  // Event interrupt enable clear
#define EDMA_GLB_IPCR  (EDMA_GLOBAL_BASE + 0x70)  // Interrupt pending clear
#define EDMA_GLB_IEVL  (EDMA_GLOBAL_BASE + 0x78)  // Interrupt evaluator


//}---------------------------
// EDMA parameter registers
//{---------------------------
#define EDMA_PARAM_ADDR   (EDMA_CFG_BASE + 0x4000) // base address of EDMA parameter registers
#define PARAM_I8   32
#define PARAM_CNT  CSL_EDMA3_NUM_DMACH


//}------------------------------------------------------------------------------------
// Transfer controller registers
//{------------------------------------------------------------------------------------

#define TC0_BASE (CSL_EDMA30TC_0_REGS)
#define TC1_BASE (CSL_EDMA30TC_1_REGS)

#define TC0_ERR_STAT (TC0_BASE + 0x0120) // Transfer control error status
#define TC1_ERR_STAT (TC1_BASE + 0x0120) //

#define TC0_ERR_STAT_ENBL (TC0_BASE + 0x0124) // Transfer control error status enable
#define TC1_ERR_STAT_ENBL (TC1_BASE + 0x0124) //

#define TC0_ERR_STAT_CLR  (TC0_BASE + 0x0128) // Transfer control error status clear
#define TC1_ERR_STAT_CLR  (TC1_BASE + 0x0128) //

#define TC0_ERR  (TC0_BASE + 0x012C) // Transfer control error details
#define TC1_ERR  (TC1_BASE + 0x012C) // 

#define TC0_EVAL (TC0_BASE + 0x0130) // Transfer control error pulse
#define TC1_EVAL (TC1_BASE + 0x0130) // 

//}------------------------------------------------------------------------------------
// CC and TCC register structure
//{------------------------------------------------------------------------------------
// TCC parameter options
typedef union dma3Opts_t {
    ADT_UInt32 TransferOptions;
    struct DmaOptBits {
        unsigned SAM:1;      // Constant source addressing
        unsigned DAM:1;      // Constant destination addressing
        unsigned SYNCDIM:1;  // A-B transfers
        unsigned STATIC:1;   // PARAM is not updated after TR is submitted (0 should be used for DMA)

        unsigned rsvd1:4;

        unsigned FWID:3;     // FIFO width
        unsigned TCCMOD:1;   // Early transfer complete (TR submitted)

        unsigned TCC:6;      // Transfer complete event
        unsigned rsvd2:2;

        unsigned TCINTEN:1;  // Generate IPR on final TRs
        unsigned ITCINTEN:1; // Generate IPR on intermediate TRs
        unsigned TCCHEN:1;   // Generate CER on final TRs
        unsigned ITCCHEN:1;  // Generate CER on intermediate TRs

        unsigned PRIVID:4;   // Read only - priviledge of requestor

        unsigned rsvd3:4;
    } Bits;
} dma3Opts_t;

typedef struct DMAChanQue_t {
   ADT_UInt32 Evt0Queue:4;
   ADT_UInt32 Evt1Queue:4;
   ADT_UInt32 Evt2Queue:4;
   ADT_UInt32 Evt3Queue:4;
   ADT_UInt32 Evt4Queue:4;
   ADT_UInt32 Evt5Queue:4;
   ADT_UInt32 Evt6Queue:4;
   ADT_UInt32 Evt7Queue:4;
   ADT_UInt32 Evt8Queue:4;
   ADT_UInt32 Evt9Queue:4;
   ADT_UInt32 Evt10Queue:4;
   ADT_UInt32 Evt11Queue:4;
   ADT_UInt32 Evt12Queue:4;
   ADT_UInt32 Evt13Queue:4;
   ADT_UInt32 Evt14Queue:4;
   ADT_UInt32 Evt15Queue:4;
   ADT_UInt32 Evt16Queue:4;
   ADT_UInt32 Evt17Queue:4;
   ADT_UInt32 Evt18Queue:4;
   ADT_UInt32 Evt19Queue:4;
   ADT_UInt32 Evt20Queue:4;
   ADT_UInt32 Evt21Queue:4;
   ADT_UInt32 Evt22Queue:4;
   ADT_UInt32 Evt23Queue:4;
   ADT_UInt32 Evt24Queue:4;
   ADT_UInt32 Evt25Queue:4;
   ADT_UInt32 Evt26Queue:4;
   ADT_UInt32 Evt27Queue:4;
   ADT_UInt32 Evt28Queue:4;
   ADT_UInt32 Evt29Queue:4;
   ADT_UInt32 Evt30Queue:4;
   ADT_UInt32 Evt31Queue:4;
   
} DMAChanQue_t;

typedef struct ccerr_t {
   ADT_UInt32 que0WatermarkOverrun:1;
   ADT_UInt32 que1WatermarkOverrun:15;
   ADT_UInt32 tccErrorCntExceeded:16;
} ccerr_t;

typedef struct evntEntry_t {
   ADT_UInt32 evnt:6;
   ADT_UInt32 evntTrigger:26;
} evntEntry_t;

typedef struct queStat_t {
   ADT_UInt32 startEntry:8;
   ADT_UInt32 validEntryCnt:8;
   ADT_UInt32 maxWaterMark:8;
   ADT_UInt32 waterMarkExceeded:8;
} queStat_t;

typedef struct ccstat_t {
   ADT_UInt32  dmaEvtActive:1;
   ADT_UInt32  qdmaEvtActive:1;
   ADT_UInt32  transferRequestActive:1;
   ADT_UInt32  writeActiveOrPending:1;
   ADT_UInt32  controllerActive:4;
   ADT_UInt32  outstandingCompletions:8;
   ADT_UInt32  q0Active:1;
   ADT_UInt32  q1Active:15;
} ccstat_t;

typedef struct shadow_t {
   struct {
      ADT_UInt64 active;
      ADT_UInt64 clear;
      ADT_UInt64 set;
      ADT_UInt64 chain;
      ADT_UInt64 enable;
      ADT_UInt64 enableClear;
      ADT_UInt64 enableSet;
      ADT_UInt64 secondaryActive;
      ADT_UInt64 secondaryClear;
   } evt;

   ADT_UInt8  dummy [8];
   struct {
      ADT_UInt64 enable;
      ADT_UInt64 enableClear;
      ADT_UInt64 enableSet;
      ADT_UInt64 pending;
      ADT_UInt64 clear;
      ADT_UInt64 eval;
   } intr;
} shadow_t;

typedef struct params_t {
   dma3Opts_t opts;
   ADT_UInt32 src;
   ADT_UInt16 ACnt;
   ADT_UInt16 BCnt;
   ADT_UInt32 dst;
   ADT_UInt16 srcBOffset;
   ADT_UInt16 dstBOffset;
   ADT_UInt16 linkAddr;
   ADT_UInt16 BReload;
   ADT_UInt16 srcCOffset;
   ADT_UInt16 dstCOffset;
   ADT_UInt32 CCnt;
} params_t;

typedef struct EDMA_Glob_t {
   ADT_UInt8  dummy1[0x240];
   DMAChanQue_t evtToQueue;
   ADT_UInt8  dummy2[0xB0];
   ADT_UInt32 missedEvents;
   ADT_UInt32 dummy3;
   ADT_UInt32 clearMissedEvents;
   ADT_UInt8  dummy4[8];
   ccerr_t    CCERR;
   ccerr_t    CCERR_CLR;
   ADT_UInt32 ErrEval;
   ADT_UInt8  dummy5[0x1C];
   ADT_UInt64 DmaRegionEvents[4];
   ADT_UInt8  dummy6[0xA0];
   evntEntry_t Q0Entries[16];
   evntEntry_t Q1Entries[16];
   ADT_UInt8  dummy7[0x180];
   queStat_t  Q0Stat;
   queStat_t  Q1Stat;
   ADT_UInt8  dummy8[0x18];
   ADT_UInt32 waterMarkThreshold;
   ADT_UInt8  dummy9[0x1C];
   ccstat_t   CCSTAT;
} EDMA_Glob_t;

typedef struct EDMA_CC_t {
   EDMA_Glob_t global;
   ADT_UInt8  dummy1[0x9B8];
   shadow_t   global_region;
   ADT_UInt8  dummy2[0xF80];
   shadow_t   region0;
   ADT_UInt8  dummy3[0x180];
   shadow_t   region1;
   ADT_UInt8  dummy4[0x1D80];
   params_t   params[2];
   params_t   unusedParams[30];
   params_t   McASPsPingPong[4];
   params_t   McBSPsPingPong[8];
   
} EDMA_CC_t;


typedef struct tcstat_t {
   ADT_UInt32 busy:1;
   ADT_UInt32 srcAct:1;
   ADT_UInt32 writePending:1;
   ADT_UInt32 dummy:1;
   ADT_UInt32 dstActCnt:3;
   ADT_UInt32 dummy2:4;
   ADT_UInt32 dstFifoStart:2;
   ADT_UInt32 dummy3:19;
} tcstat_t;

typedef struct errstat_t {
   ADT_UInt32 busErr:1;
   ADT_UInt32 dummy:1;
   ADT_UInt32 transferErr:1;
   ADT_UInt32 mmrAddrErr:1;
   ADT_UInt32 dummy2:28;
} errstat_t;

typedef struct errdetails_t {
   ADT_UInt32  type:4;
   ADT_UInt32  dummy1:4;
   ADT_UInt32  tcc:6;
   ADT_UInt32  dummy2:2;
   ADT_UInt32  cmpltIntrEnable:1;
   ADT_UInt32  cmpltChainEnable:1;
   ADT_UInt32  dummy3:22;
} errdetails_t;

typedef struct proxy_t {
   ADT_UInt32 DSPInitiated:8;
   ADT_UInt32 Supervisor:24;
} proxy_t;

typedef struct dstFIFOInst_t {
   dma3Opts_t Opt;
   ADT_UInt32 dummy1;
   ADT_UInt16 ACnt;
   ADT_UInt16 BCnt;
   ADT_UInt32 Addr;
   ADT_UInt16 dummy2;
   ADT_UInt16 BIdx;
   proxy_t    proxy;
} dstFIFOInst_t;

typedef struct dstFIFO_t {
   ADT_UInt32 AReload;
   ADT_UInt32 dummy1;
   ADT_UInt32 BAddr;
   ADT_UInt8  dummy2[0x74];
   dstFIFOInst_t inst[4];
} dstFIFO_t;

typedef struct TC_t {
   ADT_UInt8 dummy1 [0x100];
   union {
      tcstat_t fields;
      ADT_UInt32 value;
   } tcstat;
   ADT_UInt8 dummy2 [0x1C];
   union {
      errstat_t fields;
      ADT_UInt32 value;
   } errstat;
   union {
      errstat_t fields;
      ADT_UInt32 value;
   } errEnable;
   union {
      errstat_t fields;
      ADT_UInt32 value;
   } errClear;
   union {
      errdetails_t fields;
      ADT_UInt32 value;
   } errDetails;
   ADT_UInt8  dummy3 [0xC];
   ADT_UInt32 cyclesBetweenRead;
   ADT_UInt8  dummy4 [0xFC];
   struct {
      dma3Opts_t Opt;
      ADT_UInt32 Addr;
      ADT_UInt16 ACnt;
      ADT_UInt16 BCnt;
      ADT_UInt32 dummy1;  // destAddr
      ADT_UInt32 BIdx;
      union {
         proxy_t fields;
         ADT_UInt32 value;
      } proxy;
      ADT_UInt32 AReload;
      ADT_UInt32 BAddr;
      ADT_UInt32 dummy3;
   } ActiveSrc;
   ADT_UInt8 dummy5 [0x1C];
   dstFIFO_t FIFOs;
} TC_t;
//}
//
//
// Interrupt controller registers
//{------------------------------------------------------------------------------------
#define ALL_EVENTS 0xffffffffffffffff

#define INTR_CTL_BASE  (CSL_INTC_0_REGS)
#define INTR_CLR       (INTR_CTL_BASE + 0x40)  // Interrupt signal clear (128 bits)
#define INTR_CMB       (INTR_CTL_BASE + 0x80)  // Interrupt signal combine (128 bits)
#define INTR_XSTAT     (INTR_CTL_BASE + 0x180) // Interrupt exception status
#define INTR_XSTAT_CLR (INTR_CTL_BASE + 0x184) // Interrupt exception status clear

#define INTR_FLAG_0    (INTR_CTL_BASE)
#define INTR_FLAG_1    (INTR_CTL_BASE + 4)
#define INTR_FLAG_2    (INTR_CTL_BASE + 8)
#define INTR_FLAG_3    (INTR_CTL_BASE + 12)

#define INTR_CLR_0    (INTR_CTL_BASE + 0x40)
#define INTR_CLR_1    (INTR_CTL_BASE + 0x44)
#define INTR_CLR_2    (INTR_CTL_BASE + 0x48)
#define INTR_CLR_3    (INTR_CTL_BASE + 0x4c)

// Clear interrupt events from event combiner
#define clearCombinedEvent(evnt)             \
   {  ADT_UInt32 *cReg;                      \
      ADT_UInt32 bits;                       \
      cReg  = (ADT_UInt32 *) INTR_CLR;       \
      cReg += evnt / 32;                     \
      bits  = (1 << (evnt&0x1F));            \
      REG_WR (cReg, bits);                   \
   }

#define activateCombinedEvent(sgnl)           \
   {  ADT_UInt32 *xReg;                      \
      ADT_UInt32 value;                      \
      xReg  = (ADT_UInt32 *) INTR_CMB;       \
      xReg += sgnl / 32;                     \
      REG_RD (xReg, value);                  \
      value &= ~(1 << (sgnl&0x1F));          \
      REG_WR (xReg, value);                  \
   }


//}-----------------------
// Cache control registers.
//{------------------------------------------------
#define CACHE_BASE  CSL_CACHE_0_REGS
#define L2WBINV     (volatile ADT_UInt32 *) (CACHE_BASE + 0x5004)


//}=================================================
//
//
//
// McASP registers 
//{------------------------------------------------------------------------------------
#define McASP0_BASE     (CSL_MCASP_0_CTRL_REGS) // Cfg bus address
#define McASP0_DATA     (CSL_MCASP_0_DATA_REGS) // Data bus address
#define McASP0_FIFO     (CSL_MCASP_0_FIFO_REGS) // FIFO address

#define McASP1_BASE     (CSL_MCASP_1_CTRL_REGS) // Cfg bus address
#define McASP1_DATA     (CSL_MCASP_1_DATA_REGS) // Data bus address
#define McASP1_FIFO     (CSL_MCASP_1_FIFO_REGS) // FIFO address

#define McASP2_BASE     (CSL_MCASP_2_CTRL_REGS) // Cfg bus address
#define McASP2_DATA     (CSL_MCASP_2_DATA_REGS) // Data bus address
#define McASP2_FIFO     (CSL_MCASP_2_FIFO_REGS) // FIFO address
#define MCASP2_XBUF0_CFG (CSL_MCASP_2_CTRL_REGS + 0x200)
#define MCASP2_RBUF1_CFG (CSL_MCASP_2_CTRL_REGS + 0x284)

#define McASP3_BASE     (CSL_MCASP_3_CTRL_REGS) // Cfg bus address
#define McASP3_DATA     (CSL_MCASP_3_DATA_REGS) // Data bus address
#define McASP3_FIFO     (CSL_MCASP_3_FIFO_REGS) // FIFO address
#define MCASP3_XBUF0_CFG (CSL_MCASP_3_CTRL_REGS + 0x200)
#define MCASP3_XBUF1_CFG (CSL_MCASP_3_CTRL_REGS + 0x204)
#define MCASP3_XBUF2_CFG (CSL_MCASP_3_CTRL_REGS + 0x208)
#define MCASP3_XBUF3_CFG (CSL_MCASP_3_CTRL_REGS + 0x20C)

#define MCASP3_RBUF0_CFG (CSL_MCASP_3_CTRL_REGS + 0x280)
#define MCASP3_RBUF1_CFG (CSL_MCASP_3_CTRL_REGS + 0x284)
#define MCASP3_RBUF2_CFG (CSL_MCASP_3_CTRL_REGS + 0x288)
#define MCASP3_RBUF3_CFG (CSL_MCASP_3_CTRL_REGS + 0x28C)

#define McASP4_BASE     (CSL_MCASP_4_CTRL_REGS) // Cfg bus address
#define McASP4_DATA     (CSL_MCASP_4_DATA_REGS) // Data bus address
#define McASP4_FIFO     (CSL_MCASP_4_FIFO_REGS) // FIFO address

#define McASP5_BASE     (CSL_MCASP_5_CTRL_REGS) // Cfg bus address
#define McASP5_DATA     (CSL_MCASP_5_DATA_REGS) // Data bus address
#define McASP5_FIFO     (CSL_MCASP_5_FIFO_REGS) // FIFO address

//}------------------------------------------------------------------------------------
// McASP register structure
//{------------------------------------------------------------------------------------
typedef struct rst_t {
   ADT_UInt8  RxRST;
   ADT_UInt8  TxRST;
   ADT_UInt16 dummy1;
} rst_t;

typedef struct fmt_t {
   ADT_UInt32 rotate:3;
   ADT_UInt32 CFGPortAccess:1;
   ADT_UInt32 slotSize:4;
   ADT_UInt32 padBit:5;
   ADT_UInt32 padValue:2;
   ADT_UInt32 msbFirst:1;
   ADT_UInt32 delay:2;
   ADT_UInt32 dummy:14;
} fmt_t;

typedef struct fs_t {
   ADT_UInt32 fallingEdge:1;
   ADT_UInt32 internalSrc:1;
   ADT_UInt32 dummy:2;
   ADT_UInt32 wordSize:1;
   ADT_UInt32 dummy2:2;
   ADT_UInt32 slotCnt:9;
   ADT_UInt32 dummy3:16;
} fs_t;

typedef struct clk_t {
   ADT_UInt32 divisorM1:5; 
   ADT_UInt32 internalSrc:1;
   ADT_UInt32 separateTxRx:1;
   ADT_UInt32 txFallingEdge:1;
   ADT_UInt32 dummy:24;
} clk_t;

typedef struct hclk_t {
   ADT_UInt32 divisorM1:12;
   ADT_UInt32 dummy:2;
   ADT_UInt32 inverted:1;
   ADT_UInt32 internalSrc:1;
   ADT_UInt32 dummy2:16;
} hclk_t;

typedef struct intr_t {
   ADT_UInt32  underOver:1;
   ADT_UInt32  syncErr:1;
   ADT_UInt32  clkFail:1;
   ADT_UInt32  dmaErr:1;
   ADT_UInt32  lastSlot:1;
   ADT_UInt32  dataReady:1;
   ADT_UInt32  dummy:1;
   ADT_UInt32  startOfFrame:1;
   ADT_UInt32  dummy2:24;
} intr_t;

typedef struct stat_t {
   ADT_UInt32  underOver:1;
   ADT_UInt32  syncErr:1;
   ADT_UInt32  clkFail:1;
   ADT_UInt32  slotEven:1;
   ADT_UInt32  lastSlot:1;
   ADT_UInt32  dataReady:1;
   ADT_UInt32  startOfFrame:1;
   ADT_UInt32  dmaErr:1;
   ADT_UInt32  anyErr:1;
   ADT_UInt32  dummy:23;
} stat_t;

typedef struct clkChk_t {
   ADT_UInt32 sysClkDivPow2:4;
   ADT_UInt32 dummy:4;
   ADT_UInt32 min:8;
   ADT_UInt32 max:8;
   ADT_UInt32 cnt:8;
} clkChk_t;

typedef struct dir_t {
   union {
      rst_t fields;
      ADT_UInt32 value;
   } rst;
   ADT_UInt32 DataMask;
   union {
      fmt_t fields;
      ADT_UInt32 value;
   } fmt;
   union {
      fs_t fields;
      ADT_UInt32 value;
   } fs;
   union {
      clk_t fields;
      ADT_UInt32 value;
   } clk;
   union {
      hclk_t fields;
      ADT_UInt32 value;
   } hclk;
   ADT_UInt32 ActiveSlotMask;
   union {
      intr_t fields;
      ADT_UInt32 value;
   } intr;
   union {
      stat_t fields;
      ADT_UInt32 value;
   } stat;
   ADT_UInt32 currentSlot;
   union {
      clkChk_t fields;
      ADT_UInt32 value;
   } clkChk;
   ADT_UInt32 evtCtl;
   ADT_UInt8  dummy[0x10];
} dir_t;

typedef struct McASP_t {
   ADT_UInt8 dummy1[0x10];
   ADT_UInt32 PinIsGPIO;
   ADT_UInt32 PinIsTx;
   ADT_UInt8  dummy2[0x48];
   dir_t      Rx;
   dir_t      Tx;
} McASP_t;


typedef struct fifoCtrl_t {
   ADT_UInt8  WordsPerDmaTransfer;
   ADT_UInt8  WordCntPerDmaEvent;   // 
   ADT_UInt16 FIFOEnabled;
}  fifoCtrl_t;

typedef struct McASP_Fifo_t {
   ADT_UInt8 dummy[0x10];
   fifoCtrl_t writeCtl;
   ADT_UInt32 wordsInWriteFIFO;
   fifoCtrl_t readCtl;
   ADT_UInt32 wordsInReadFIFO;
} McASP_Fifo_t;

//}------------------------------------------------------------------------------------
// McBSP registers 
//{------------------------------------------------------------------------------------
#define McBSP0_BASE  (CSL_MCBSP_0_CTRL_REGS)
#define McBSP1_BASE  (CSL_MCBSP_1_CTRL_REGS)


//}------------------------------------------------
//
//
// G.Pak specific assignments
//{=================================================

//  DMA to TDM port configuration structure
typedef struct PortAddr {
  ADT_UInt32 TxAddr, RxAddr;    // Tx and Rx TDM bus transfer address
  ADT_UInt32 TxChn,  RxChn;     // Tx and Rx event parameter block indices
  ADT_UInt32 TxPing, RxPing;    // Tx and Rx Ping parameter indices (pong = ping + 1)
  ADT_PCM16 **TxBuf, **RxBuf;   // Tx and Rx current DMA transfer pointers
  ADT_UInt32 TxTcc,  RxTcc;     // Tx and Rx Tcc Codes
} PortAddr;

extern PortAddr DMACfg[];


//
//                          TDM
//   Shadow region:          1
//   Transfer queue:         0         
//   Transfer priority       0



#define TDM_Priority  0    // Transfer controller priority for TDM data
#define TDM_TC        0    // TDM data transfers are assigned to DMA transfer controller 0

//  Index values for TDM - DMA 'ping' parameter sets.  'pong' indices are 'ping' + 1
#define McASPRx0_Ping  (PARAM_CNT + 0)   //  Rx McASP
#define McASPTx0_Ping  (PARAM_CNT + 2)   //  Tx McASP
#define McASPRx1_Ping  (PARAM_CNT + 4)   //  Rx McASP
#define McASPTx1_Ping  (PARAM_CNT + 6)   //  Tx McASP
#define McASPRx2_Ping  (PARAM_CNT + 8)   //  Rx McASP
#define McASPTx2_Ping  (PARAM_CNT + 6)   //  Tx McASP
#define McASPRx3_Ping  (PARAM_CNT + 10)  //  Rx McASP
#define McASPTx3_Ping  (PARAM_CNT + 12)  //  Tx McASP
#define McASPRx4_Ping  (PARAM_CNT + 14)  //  Rx McASP
#define McASPTx4_Ping  (PARAM_CNT + 16)  //  Tx McASP
#define McASPRx5_Ping  (PARAM_CNT + 18)  //  Rx McASP
#define McASPTx5_Ping  (PARAM_CNT + 20)  //  Tx McASP
#define Rx0_Ping       (PARAM_CNT + 4)  //  Rx McBSP0
#define Tx0_Ping       (PARAM_CNT + 6)  //  Tx McBSP0
#define Rx1_Ping       (PARAM_CNT + 8)  //  Rx McBSP1
#define Tx1_Ping       (PARAM_CNT + 10) //  Tx McBSP1


//--------------------------------------------
// Hardware interrupts
#define HWI_TDM_ERR       5    // Used by DMA module to process TDM bus errors
#define HWI_ASP_ERR       6    // USed by McASP for Framing errors
#define HWI_TDM_COMPLETE  8    // Used by DMA module to process TDM completion events
//  HWI_EMAC_RCV     =  9        Forced by IP stack
//  HWI_EMAC_XMT     = 10        Forced by IP stack
//  HWI_RTDX_XMT     = 11        Forced by BIOS
//  HWI_RTDX_RCV     = 12        Forced by BIOS
//  HWI_TIMER        = 15        Set by BIOS


//-------------------------------------------------------
// Interupt events
#define DMA_CompleteSgnl   CSL_INTC_EVENTID_EDMA3_0_CC0_INT1   // Channel controller interrupt
#define ASP_CompleteSgnl   CSL_INTC_EVENTID_EVT2   // combined mcasp2 tx + rx interrupts

// Combined interrupt events
#define ErrSgnl           CSL_INTC_EVENTID_EVT0               // Combined DMA interrupt (21, 22, 27)
#define TDM_CCErrSgnl     CSL_INTC_EVENTID_EDMA3_0_CC0_ERRINT // Channel controller error
#define TDM_Que0ErrSgnl   CSL_INTC_EVENTID_EDMA3_0_TC0_ERRINT // TC Queue 0 error
#define TDM_Que1ErrSgnl   CSL_INTC_EVENTID_EDMA3_0_TC1_ERRINT // TC Queue 1 error
#define DroppedIntrSgnl   CSL_INTC_EVENTID_INTERR             // Dropped interrupt error

#define McASP2_Transmit   CSL_INTC_EVENTID_MCASP2_TXINT
#define McASP2_Receive    CSL_INTC_EVENTID_MCASP2_RXINT
#define McASP3_Transmit   CSL_INTC_EVENTID_MCASP3_TXINT
#define McASP3_Receive    CSL_INTC_EVENTID_MCASP3_RXINT
#define McASP_ErrSgnl     CSL_INTC_EVENTID_EVT2               // Combiner McASP error interrupt
#define McASP0_TxErrSgnl  CSL_INTC_EVENTID_MCASP0_TXINT       // McASP0 Tx error signaled   (70)
#define McASP0_RxErrSgnl  CSL_INTC_EVENTID_MCASP0_RXINT       // McASP0 Rx error signaled   (71)
#define McASP1_TxErrSgnl  CSL_INTC_EVENTID_MCASP1_TXINT       // McASP1 Tx error signaled   (72)
#define McASP1_RxErrSgnl  CSL_INTC_EVENTID_MCASP1_RXINT       // McASP1 Rx error signaled   (73)
#define McASP2_TxErrSgnl  CSL_INTC_EVENTID_MCASP2_TXINT       // McASP2 Tx error signaled   (74)
#define McASP2_RxErrSgnl  CSL_INTC_EVENTID_MCASP2_RXINT       // McASP2 Rx error signaled   (75)

#endif // _81xx
