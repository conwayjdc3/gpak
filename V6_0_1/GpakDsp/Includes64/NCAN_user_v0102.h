#ifndef _ADT_TYPEDEF
	#define _ADT_TYPEDEF

	#ifdef _TMS320C5XX
		#define ADT_Int16 short int				//16b
		#define ADT_Int32 long int				//32b
		#define ADT_UInt16 unsigned int			//16b
		#define ADT_UInt32 unsigned long int	//32b
		#define ADT_Int40 long int				//40b
	#endif

	#ifdef _TMS320C6X
		#define ADT_Int16 short             	// 16b
		#define ADT_Int32 int               	// 32b
		#define ADT_UInt16 unsigned short   	// 32b
		#define ADT_UInt32 unsigned int     	// 32b
		#define ADT_Int40 long   				// 40b
	#endif

	#ifdef WIN32
		#define ADT_Int16 short int             // 16b
		#define ADT_Int32 long int              // 32b
		#define ADT_UInt16 unsigned int         // 32b
		#define ADT_UInt32 unsigned long int    // 32b
		#define ADT_Int40 _int64              	// 64b
	#endif 

#endif //_ADT_TYPEDEF
#define DECOMP_LEVELS 4
#define NCAN_FRAME_SIZE (1<<DECOMP_LEVELS)
typedef struct 
{
	ADT_Int16 LowThreshdB;	// Threshold below which we declare noise
	ADT_Int16 HighThreshdB;	// Threshold above which we declare speech
	ADT_Int16 HangMSec;
	ADT_Int16 FrameSize;
	ADT_Int16 WindowSize;
}		VAD_Params_t;
typedef ADT_Int32 NCAN_Channel_t[425];
typedef struct 
{
	ADT_Int16 MaxAttendB;
}		NC_Params_t;
typedef struct
{
	NC_Params_t NC_Params;
	VAD_Params_t VAD_Params;
} 		NCAN_Params_t;

void NCAN_ADT_config();
void NCAN_ADT_init(NCAN_Channel_t *NCAN_Channel, NCAN_Params_t *NC_Params);
void NCAN_ADT_cancel(NCAN_Channel_t *NCAN_Channel, ADT_Int16 *InSignal, ADT_Int16 CNGEnable, ADT_Int16 *OutSignal);
