#ifndef ARBIT_TONEDT_USER_H
#define ARBIT_TONEDT_USER_H

#include "adt_typedef_user.h"

#define NUM_DISTINCT_FREQS 32
#define NUM_TONES	16


// Define the result (event) codes
#define ARBIT_TD_NULL           (0)
#define ARBIT_TD_EARLY          (-1)
#define ARBIT_TD_FALSE_EARLY    (-2)
#define ARBIT_TD_LEAD_EDGE      (-3)
#define ARBIT_TD_TRAIL_EDGE     (-4)
#define ARBIT_TD_DIGIT          (-5)

typedef struct 
{
	ADT_Int16 Result;
	ADT_Int16 ToneIndex;
}	ARBIT_TDStatus_t;


typedef struct
{
	ARBIT_TDStatus_t Status;
	ADT_Int32 TDInstanceSpace[19];
	ADT_Int16 Gor[NUM_DISTINCT_FREQS * 2];
} ARBIT_TDInstance_t;


#define ARBIT_CHANNEL_SIZE ((((4 + NUM_DISTINCT_FREQS)* NUM_DISTINCT_FREQS+3)&0xfffC)*sizeof(ADT_Int16) + \
							sizeof(ADT_Int32) + \
							4*sizeof(ADT_Int16) + \
							4*sizeof(ADT_Int16 *))


typedef struct 
{
	ADT_Int32	CData[ARBIT_CHANNEL_SIZE>>2];
} ArbitDtInstance_t;

typedef struct 
{
	//Arbitrary tone detecet requirements
	ADT_Int16 ToneInfo[NUM_TONES][3];
	// freq1 freq2 IndexRequested

} Tones_Index_t;


typedef struct 
{
	ADT_Int16 structSize; // parameter structure's size
	ADT_Int16 num_Distinct_Freqs; // distinct freqs need to be detected
	ADT_Int16 num_Tones;  // number of tones needs to be detected
	ADT_Int16 min_Power; // miminum power of tone in dB,'0' means default evel
	ADT_Int16 max_Freg_Deviation; // frequency in spec range, , '0' means default
							  // default is 2.5%
} ARBIT_ADT_Param_t;

ADT_API void ARBIT_ADT_config(ArbitDtInstance_t *ArbitChan, Tones_Index_t *pTones,
					ADT_Int16 *freqs, ARBIT_ADT_Param_t *ArbitParams);

ADT_API void ARBIT_ADT_init(ArbitDtInstance_t *ArbitChan, ARBIT_TDInstance_t *CprgInst);

ADT_API void TDLOWMEM_ADT_toneDetect(ARBIT_TDInstance_t *TDChannel, ADT_Int16 InputSamples[], ADT_Int16 FrameSize);

#endif

