/***************************************************************************
* $RCSfile: fsk.h,v $                
* $Revision:   1.1  $              
* Revision $Author:   rfuchs  $                              
* Revision $Date:   Nov 29 2007 11:47:10  $            
* Author of Origin: Peter B. Miller             
* Date of Origin: 09-27-99  
*                   
* Company: MESi               
* Website: www.mesi.net             
* Description: Structure definitions and extern references for FSK.								
*                   
* Copyright (C) MESi 1996-2006, all rights reserved.         
***************************************************************************/

#if !defined(FSK_INCLUSION_)
#define FSK_INCLUSION_
						 
#define RX_FSK_MARKS_ID 			0x1
#define RX_FSK_START_BIT_ID 		0x2
						 
	/**** FSK modulator common structure ****/
			  
#define TX_FSK_MEMBERS \
	short interpolate; \
	short decimate; \
	short coef_ptr; \
	short carrier; \
	short tone_scale; \
	unsigned short vco_memory; \
	short frequency;\
	short frequency_shift

struct TX_FSK_BLOCK                                        
	{                                                      
	TX_CONTROL_MEMBERS;                                      
	TX_FSK_MEMBERS;                                      
	};               

	/**** FSK demodulator common structure ****/
	
#define RX_FSK_MEMBERS \
	short mark_coef; \
	short space_coef; \
	short sym; \
	short coef_len; \
	short coef_ptr; \
	short interpolate; \
	short decimate; \
	short sym_nm1; \
	short sym_hat; \
	short sym_hat_nm2; \
	short sym_clk_memory; \
	short baud_counter; \
	short sym_level; \
	short MARKs_counter; \
	short LOS_threshold; \
	short LOS_memory

struct RX_FSK_BLOCK                                        
	{                                                      
	RX_CONTROL_MEMBERS;                                      
	RX_FSK_MEMBERS;                                     
	};                                                     

	/**** FSK demodulator parameters ****/

#define FSK_LOS_LIMIT	 			4		/* 4 consecutive LOS events */
#define FSK_LOS_THRESHOLD			16384	/* 32768*10exp(-6 dB/20)	*/

	/**** functions ****/

#if defined(XDAIS_API)
extern void FSK_MESI_TxInitFSK(struct START_PTRS *);
extern short FSK_MESI_FSKmodulator(struct TX_BLOCK *);
extern void FSK_MESI_RxInitFSK(struct START_PTRS *);
extern short FSK_MESI_FSKdemodulator(struct RX_BLOCK *);
extern short FSK_MESI_MARKsDetect(struct RX_BLOCK *);
extern short FSK_MESI_StartBitDetect(struct RX_BLOCK *);
extern short FSK_MESI_RxComputeSymLevel(struct START_PTRS *);
#else /* XDAIS_API */
extern void Tx_init_FSK(struct START_PTRS *);
extern short FSK_modulator(struct TX_BLOCK *);
extern void Rx_init_FSK(struct START_PTRS *);
extern short FSK_demodulator(struct RX_BLOCK *);
extern short FSK_MARKs_detect(struct RX_BLOCK *);
extern short FSK_start_bit_detect(struct RX_BLOCK *);
extern short Rx_compute_sym_level(struct START_PTRS *);
#endif /* XDAIS_API */ 

/****************************************************************************/

#endif /* FSK_INCLUSION_ */


