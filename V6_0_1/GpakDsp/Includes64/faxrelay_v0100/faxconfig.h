/***************************************************************************
 * $RCSfile: config.h,v $                
 * $Revision:   1.1  $              
 * Revision $Author:   rfuchs  $                              
 * Revision $Date:   Nov 29 2007 11:46:58  $            
 * Author of Origin: Peter B. Miller             
 * Date of Origin: 10-29-97  
 *                   
 * Company: MESi               
 * Website: www.mesi.net             
 * Description: This module defines default configuration constants and profiles 
 *  used by all modules to control the conditional assembly of the various		
 *	signal generators and processors. At the top of the file are global
 * 	definitions that apply generically to all modules. Then a suite of 
 *	"demo" profiles are defined that show specific configurations for the
 *	many demonstration projects that use these components. Typically a
 *	command-line MAKEFILE or IDE would define a symbol corresponding to the 
 *	type of demo configuration desired.
 *   
 * Copyright (C) MESi 1996-2006, all rights reserved.         
 ****************************************************************************/

#if !defined(CONFIG_INCLUSION_)
#define CONFIG_INCLUSION_

	/************************/
	/**** Global symbols ****/
	/************************/
			 
#if !defined DISABLED
#define DISABLED					0
#endif /* DISABLED */

#if !defined ENABLED
#define ENABLED						1
#endif /* ENABLED */

#if !defined DUMP_BUFFER
#define DUMP_BUFFER					ENABLED
#endif /* DUMP_BUFFER */
#if !defined IQ_DAC_WRITE
#define IQ_DAC_WRITE				DISABLED
#endif /* IQ_DAC_WRITE */

#if !defined COMPILER
#define COMPILER					ENABLED
#endif /* COMPILER */

	/****************************************/
	/**** common buffer size definitions ****/
	/****************************************/

#if !defined(SIN_TABLE_LEN)
#define SIN_TABLE_LEN 				256
#endif /* SIN_TABLE_LEN */
#define SIN_BUF_LEN					(SIN_TABLE_LEN+SIN_TABLE_LEN/4)
#if SIN_TABLE_LEN == 128
#define SIN_TABLE_SHIFT 			(16-7)
#endif /* SIN_TABLE_LEN == 128 */
#if SIN_TABLE_LEN == 256
#define SIN_TABLE_SHIFT 			(16-8)
#endif /* SIN_TABLE_LEN == 256 */
#if SIN_TABLE_LEN == 512
#define SIN_TABLE_SHIFT 			(16-9)
#endif /* SIN_TABLE_LEN == 512 */
#if SIN_TABLE_LEN == 1024
#define SIN_TABLE_SHIFT 			(16-10)
#endif /* SIN_TABLE_LEN == 1024 */
#if SIN_TABLE_LEN == 2048
#define SIN_TABLE_SHIFT 			(16-11)
#endif /* SIN_TABLE_LEN == 2048 */

#define DFT_COEF					572	/* 32768*(sqrt(2)/ANALYSIS_LEN)*fudge */
#define DFT_COEF_LEN				256

/****************************************************************************
 * "Demo" project profiles. 												
 * Each of the demonstration programs (demos) has a custom memory and		
 * component profile. These profiles are declared externally in the 		
 * command-line MAKEFILE, an IDE C-preprocessor Definition, or an options 	
 * (*.opt) text file that is included at the command line (i.e. @file.opt).	
 * Examples are "VMODEM_DEMO, V22_DEMO, etc.).	   
 * For each memory element the existance of the LEN specification is tested.
 * If it exists then an external method is being used to override the default
 * size for that element for the specified profile. An "elif" is used to 
 * determine if the LEN is zero and if so it is then #undefined so that the
 * memory creation module won't try to create it. The C programming language 
 * does not permit testing of sizeof() operator so the test for pre-existence
 * must happen here. Examples include	
 * #defining TX_SAMPLE_LEN to 511, and TX_BLOCK_LEN	to sizeof(struct TX_BLOCK).
 * #defining TX_SAMPLE_LEN to 511, and TX_BLOCK_LEN	to 						
 * sizeof(struct TX_BLOCK). Then, various source file conditions are set 	
 * to enable or disable functionality in each module. Examples include		
 * #defining TX_ACOUSTIC_ECHO_CANCELLER to DISABLED or TX_V26_MODEM to ENABLED.						
 ****************************************************************************/

    /*******************************/
	/**** configuration for G167 ****/
	/*******************************/
	
#if defined(G167_DEMO)

#include "rxtx.h"
#include "apsk.h"
#include "g167.h"
#if !defined(TX_BLOCK_LEN)
#define TX_BLOCK_LEN				(sizeof(struct TX_G167_BLOCK)/sizeof(short))
#elif TX_BLOCK_LEN == 0
#undef TX_BLOCK_LEN
#endif /* TX_BLOCK_LEN */
#if !defined(TX_SAMPLE_LEN)
#define TX_SAMPLE_LEN				511
#endif /* TX_SAMPLE_LEN */
#if !defined(TX_DATA_LEN)
#define TX_DATA_LEN					63
#endif /* TX_DATA_LEN */

#if !defined(RX_BLOCK_LEN)
#define RX_BLOCK_LEN				sizeof(struct RX_G167_BLOCK)
#endif /* RX_BLOCK_LEN */
#if !defined(RX_SAMPLE_LEN)
#define RX_SAMPLE_LEN				511
#endif /* RX_SAMPLE_LEN */
#if !defined(RX_DATA_LEN)
#define RX_DATA_LEN					63
#endif /* RX_DATA_LEN */
#if !defined(NEC_COEF_LEN)
#define NEC_COEF_LEN 				256
#endif /* NEC_COEF_LEN */
#if !defined(FEC_COEF_LEN)
#define FEC_COEF_LEN 				0
#endif /* FEC_COEF_LEN */
#if !defined(EC_COEF_LEN)
#define EC_COEF_LEN 				(NEC_COEF_LEN+FEC_COEF_LEN)
#endif /* EC_COEF_LEN */

#if !defined(TX_ACOUSTIC_ECHO_CANCELLER) && !defined(ACOUSTIC_ECHO_CANCELLER)
#define TX_ACOUSTIC_ECHO_CANCELLER	ENABLED
#endif /* TX_ACOUSTIC_ECHO_CANCELLER */
#if !defined(RX_ACOUSTIC_ECHO_CANCELLER) && !defined(ACOUSTIC_ECHO_CANCELLER)
#define RX_ACOUSTIC_ECHO_CANCELLER	ENABLED
#endif /* RX_ACOUSTIC_ECHO_CANCELLER */
#if !defined(ACOUSTIC_ECHO_CANCELLER)
#define ACOUSTIC_ECHO_CANCELLER		(TX_ACOUSTIC_ECHO_CANCELLER|RX_ACOUSTIC_ECHO_CANCELLER)
#endif /* ACOUSTIC_ECHO_CANCELLER */

#if !defined(ECHO_CANCELLER)
#define ECHO_CANCELLER				(ENABLED & TRANSMITTER & RECEIVER)
#endif /* ECHO_CANCELLER */

#endif /* G167_DEMO */

	/**************************************/
	/**** BAUDOT demo configuration    ****/
	/**************************************/

#if defined(BAUDOT_DEMO)

#include "rxtx.h"
#include "fsk.h"
#include "baudot.h"
#if !defined(TX_BLOCK_LEN)
#define TX_BLOCK_LEN				(sizeof(struct TX_BAUDOT_BLOCK)/sizeof(short))
#elif TX_BLOCK_LEN == 0
#undef TX_BLOCK_LEN
#endif /* TX_BLOCK_LEN */
#if !defined(TX_SAMPLE_LEN)
#define TX_SAMPLE_LEN				41
#endif /* TX_SAMPLE_LEN */
#if !defined(TX_DATA_LEN)
#define TX_DATA_LEN					15
#endif /* TX_DATA_LEN */
#if !defined(TX_FIR_LEN)
#define TX_FIR_LEN					(2*6)
#endif /* TX_FIR_LEN */

#if !defined(RX_BLOCK_LEN)
#define RX_BLOCK_LEN				(sizeof(struct RX_BAUDOT_BLOCK)/sizeof(short))
#elif RX_BLOCK_LEN == 0
#undef RX_BLOCK_LEN
#endif /* RX_BLOCK_LEN */
#if !defined(RX_SAMPLE_LEN)
#define RX_SAMPLE_LEN				127
#endif /* RX_SAMPLE_LEN */
#if !defined(RX_DATA_LEN)
#define RX_DATA_LEN					15
#endif /* RX_DATA_LEN */

#if !defined(TX_BAUDOT_MODEM)
#define TX_BAUDOT_MODEM				ENABLED
#endif /* TX_BAUDOT_MODEM */
#if !defined(RX_BAUDOT_MODEM)       
#define RX_BAUDOT_MODEM				ENABLED
#endif /* RX_BAUDOT_MODEM */
#if !defined(BAUDOT_MODEM)
#define BAUDOT_MODEM				(TX_BAUDOT_MODEM|RX_BAUDOT_MODEM)
#endif /* BAUDOT_MODEM */
#if !defined(TX_FSK_MODULATOR)
#define TX_FSK_MODULATOR			(ENABLED & TRANSMITTER)
#endif /* TX_FSK_MODULATOR */
#if !defined(RX_FSK_DEMODULATOR)
#define RX_FSK_DEMODULATOR			(ENABLED & RECEIVER)
#endif /* RX_FSK_MODULATOR */

#endif /* BAUDOT_DEMO */
	
	/**************************************/
	/**** Caller ID demo configuration ****/
	/**************************************/

#if defined(CID_DEMO)

#include "rxtx.h"
#include "fsk.h"
#include "cid.h"
#if !defined(TX_BLOCK_LEN)
#define TX_BLOCK_LEN				(sizeof(struct TX_CID_BLOCK)/sizeof(short))
#elif TX_BLOCK_LEN == 0
#undef TX_BLOCK_LEN
#endif /* TX_BLOCK_LEN */
#if !defined(TX_SAMPLE_LEN)
#define TX_SAMPLE_LEN				41
#endif /* TX_SAMPLE_LEN */
#if !defined(TX_DATA_LEN)
#define TX_DATA_LEN					7
#endif /* TX_DATA_LEN */
#if !defined(TX_FIR_LEN)
#define TX_FIR_LEN					128
#endif /* TX_FIR_LEN */

#if !defined(RX_BLOCK_LEN)
#define RX_BLOCK_LEN				(sizeof(struct RX_CID_BLOCK)/sizeof(short))
#elif RX_BLOCK_LEN == 0
#undef RX_BLOCK_LEN
#endif /* RX_BLOCK_LEN */
#if !defined(RX_SAMPLE_LEN)
#define RX_SAMPLE_LEN				127
#endif /* RX_SAMPLE_LEN */
#if !defined(RX_DATA_LEN)
#define RX_DATA_LEN					7
#endif /* RX_DATA_LEN */
#if !defined(RX_FIR_LEN)
#define RX_FIR_LEN					128
#endif /* RX_FIR_LEN */

#if !defined(TX_V23_MODEM)
#define TX_V23_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V23_MODEM */
#if !defined(RX_V23_MODEM)
#define RX_V23_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V23_MODEM */

#if !defined(TX_CID_MODEM)
#define TX_CID_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_CID_MODEM */
#if !defined(RX_CID_MODEM)
#define RX_CID_MODEM				(ENABLED & RECEIVER)
#endif /* RX_CID_MODEM */
#if !defined(TX_CAS)
#define TX_CAS						(ENABLED & TRANSMITTER)
#endif /* TX_CAS */
#if !defined(RX_CAS)
#define RX_CAS						(ENABLED & RECEIVER)
#endif /* RX_CAS */

#if !defined(TX_FSK_MODULATOR)
#define TX_FSK_MODULATOR			(ENABLED & TRANSMITTER)
#endif /* TX_FSK_MODULATOR */
#if !defined(RX_FSK_DEMODULATOR)
#define RX_FSK_DEMODULATOR			(ENABLED & RECEIVER)
#endif /* RX_FSK_MODULATOR */

#endif /* CID_DEMO */

	/********************************************/
	/**** DTMF Caller ID demo configuration *****/
	/********************************************/

#if defined(CIDDTMF_DEMO)

#include "rxtx.h"
#include "gendet.h"
#include "ciddtmf.h"
#if !defined(TX_BLOCK_LEN)
#define TX_BLOCK_LEN				(sizeof(struct TX_GEN_BLOCK)/sizeof(short))
#elif TX_BLOCK_LEN == 0
#undef TX_BLOCK_LEN
#endif /* TX_BLOCK_LEN */
#if !defined(TX_SAMPLE_LEN)
#define TX_SAMPLE_LEN				41
#endif /* TX_SAMPLE_LEN */
#if !defined(TX_DATA_LEN)
#define TX_DATA_LEN					16	/* must be minimum of DTMF_CID_MAX_NUM_DIGITS (16) */
#endif /* TX_DATA_LEN */
#if !defined(TX_FIR_LEN)
#define TX_FIR_LEN					128
#endif /* TX_FIR_LEN */

#if !defined(RX_BLOCK_LEN)
#define RX_BLOCK_LEN				(sizeof(struct RX_DET_BLOCK)/sizeof(short))
#elif RX_BLOCK_LEN == 0
#undef RX_BLOCK_LEN
#endif /* RX_BLOCK_LEN */
#if !defined(RX_SAMPLE_LEN)
#define RX_SAMPLE_LEN				127
#endif /* RX_SAMPLE_LEN */
#if !defined(RX_DATA_LEN)
#define RX_DATA_LEN					16	/* must be minimum of DTMF_CID_MAX_NUM_DIGITS (16) */
#endif /* RX_DATA_LEN */
#if !defined(RX_FIR_LEN)
#define RX_FIR_LEN					128
#endif /* RX_FIR_LEN */

#if !defined(TX_DTMF)
#define TX_DTMF						(ENABLED & TRANSMITTER)
#endif /* TX_DTMF */
#if !defined(RX_DTMF)
#define RX_DTMF						(ENABLED & RECEIVER)
#endif /* RX_DTMF */

#if !defined(TX_CIDDTMF_MODEM)
#define TX_CIDDTMF_MODEM			(ENABLED & TRANSMITTER)
#endif /* TX_CIDDTMF_MODEM */
#if !defined(RX_CIDDTMF_MODEM)
#define RX_CIDDTMF_MODEM			(ENABLED & RECEIVER)
#endif /* RX_CIDDTMF_MODEM */
   
#endif /* CIDDTMF_DEMO */

	/********************************************/
	/**** Japan Caller ID demo configuration ****/
	/********************************************/

#if defined(CIDJAPAN_DEMO)

#include "rxtx.h"
#include "fsk.h"
#include "cidjapan.h"
#if !defined(TX_BLOCK_LEN)
#define TX_BLOCK_LEN				(sizeof(struct TX_JAPAN_CID_BLOCK)/sizeof(short))
#elif TX_BLOCK_LEN == 0
#undef TX_BLOCK_LEN
#endif /* TX_BLOCK_LEN */
#if !defined(TX_SAMPLE_LEN)
#define TX_SAMPLE_LEN				41
#endif /* TX_SAMPLE_LEN */
#if !defined(TX_DATA_LEN)
#define TX_DATA_LEN					7
#endif /* TX_DATA_LEN */
#if !defined(TX_FIR_LEN)
#define TX_FIR_LEN					128
#endif /* TX_FIR_LEN */

#if !defined(RX_BLOCK_LEN)
#define RX_BLOCK_LEN				(sizeof(struct RX_JAPAN_CID_BLOCK)/sizeof(short))
#elif RX_BLOCK_LEN == 0
#undef RX_BLOCK_LEN
#endif /* RX_BLOCK_LEN */
#if !defined(RX_SAMPLE_LEN)
#define RX_SAMPLE_LEN				127
#endif /* RX_SAMPLE_LEN */
#if !defined(RX_DATA_LEN)
#define RX_DATA_LEN					7
#endif /* RX_DATA_LEN */
#if !defined(RX_FIR_LEN)
#define RX_FIR_LEN					128
#endif /* RX_FIR_LEN */

#if !defined(TX_V23_MODEM)
#define TX_V23_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V23_MODEM */
#if !defined(RX_V23_MODEM)
#define RX_V23_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V23_MODEM */

#if !defined(TX_CIDJAPAN_MODEM)
#define TX_CIDJAPAN_MODEM			(ENABLED & TRANSMITTER)
#endif /* TX_CIDJAPAN_MODEM */
#if !defined(RX_CIDJAPAN_MODEM)
#define RX_CIDJAPAN_MODEM			(ENABLED & RECEIVER)
#endif /* RX_CIDJAPAN_MODEM */

#if !defined(TX_FSK_MODULATOR)
#define TX_FSK_MODULATOR			(ENABLED & TRANSMITTER)
#endif /* TX_FSK_MODULATOR */
#if !defined(RX_FSK_DEMODULATOR)
#define RX_FSK_DEMODULATOR			(ENABLED & RECEIVER)
#endif /* RX_FSK_MODULATOR */

#endif /* CIDJAPAN_DEMO */

	/********************************************/
	/**** All Caller ID demo configuration ****/
	/********************************************/

#if defined(CLIP_DEMO)

#include "rxtx.h"
#include "gendet.h"
#if !defined(TX_BLOCK_LEN)
#define TX_BLOCK_LEN				(sizeof(struct TX_GEN_BLOCK)/sizeof(short))
#elif TX_BLOCK_LEN == 0
#undef TX_BLOCK_LEN
#endif /* TX_BLOCK_LEN */
#if !defined(TX_SAMPLE_LEN)
#define TX_SAMPLE_LEN				41
#endif /* TX_SAMPLE_LEN */
#if !defined(TX_DATA_LEN)
#define TX_DATA_LEN					16	/* must be minimum of DTMF_CID_MAX_NUM_DIGITS (16) */
#endif /* TX_DATA_LEN */
#if !defined(TX_FIR_LEN)
#define TX_FIR_LEN					128
#endif /* TX_FIR_LEN */

#if !defined(RX_BLOCK_LEN)
#define RX_BLOCK_LEN				(sizeof(struct RX_DET_BLOCK)/sizeof(short))
#elif RX_BLOCK_LEN == 0
#undef RX_BLOCK_LEN
#endif /* RX_BLOCK_LEN */
#if !defined(RX_SAMPLE_LEN)
#define RX_SAMPLE_LEN				127
#endif /* RX_SAMPLE_LEN */
#if !defined(RX_DATA_LEN)
#define RX_DATA_LEN					16	/* must be minimum of DTMF_CID_MAX_NUM_DIGITS (16) */
#endif /* RX_DATA_LEN */
#if !defined(RX_FIR_LEN)
#define RX_FIR_LEN					128
#endif /* RX_FIR_LEN */

#if !defined(TX_DTMF)
#define TX_DTMF						(ENABLED & TRANSMITTER)
#endif /* TX_DTMF */
#if !defined(RX_DTMF)
#define RX_DTMF						(ENABLED & RECEIVER)
#endif /* RX_DTMF */

#if !defined(TX_V23_MODEM)
#define TX_V23_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V23_MODEM */
#if !defined(RX_V23_MODEM)
#define RX_V23_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V23_MODEM */

#if !defined(TX_CID_MODEM)
#define TX_CID_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_CID_MODEM */
#if !defined(RX_CID_MODEM)
#define RX_CID_MODEM				(ENABLED & RECEIVER)
#endif /* RX_CID_MODEM */
#if !defined(TX_CAS)
#define TX_CAS						(ENABLED & TRANSMITTER)
#endif /* TX_CAS */
#if !defined(RX_CAS)
#define RX_CAS						(ENABLED & RECEIVER)
#endif /* RX_CAS */
#if !defined(TX_CIDJAPAN_MODEM)
#define TX_CIDJAPAN_MODEM			(ENABLED & TRANSMITTER)
#endif /* TX_CIDJAPAN_MODEM */
#if !defined(RX_CIDJAPAN_MODEM)
#define RX_CIDJAPAN_MODEM			(ENABLED & RECEIVER)
#endif /* RX_CIDJAPAN_MODEM */
#if !defined(TX_CIDDTMF_MODEM)
#define TX_CIDDTMF_MODEM			(ENABLED & TRANSMITTER)
#endif /* TX_CIDDTMF_MODEM */
#if !defined(RX_CIDDTMF_MODEM)
#define RX_CIDDTMF_MODEM			(ENABLED & RECEIVER)
#endif /* RX_CIDDTMF_MODEM */

#if !defined(TX_FSK_MODULATOR)
#define TX_FSK_MODULATOR			(ENABLED & TRANSMITTER)
#endif /* TX_FSK_MODULATOR */
#if !defined(RX_FSK_DEMODULATOR)
#define RX_FSK_DEMODULATOR			(ENABLED & RECEIVER)
#endif /* RX_FSK_MODULATOR */

#endif /* CLIP_DEMO */
	
	/*********************/
	/**** CPTD config ****/
	/*********************/

#if defined(CPTD_DEMO)

#include "rxtx.h"
#include "gendet.h"
#if !defined(TX_BLOCK_LEN)
#define TX_BLOCK_LEN				(sizeof(struct TX_GEN_BLOCK)/sizeof(short))
#elif TX_BLOCK_LEN == 0
#undef TX_BLOCK_LEN
#endif /* TX_BLOCK_LEN */
#if !defined(TX_SAMPLE_LEN)
#define TX_SAMPLE_LEN				41
#endif /* TX_SAMPLE_LEN */
#if !defined(TX_DATA_LEN)
#define TX_DATA_LEN					1
#endif /* TX_DATA_LEN */

#if !defined(RX_BLOCK_LEN)
#define RX_BLOCK_LEN				(sizeof(struct RX_DET_BLOCK)/sizeof(short))
#elif RX_BLOCK_LEN == 0
#undef RX_BLOCK_LEN
#endif /* RX_BLOCK_LEN */
#if !defined(RX_SAMPLE_LEN)
#define RX_SAMPLE_LEN				127
#endif /* RX_SAMPLE_LEN */
#if !defined(RX_DATA_LEN)
#define RX_DATA_LEN					1
#endif /* RX_DATA_LEN */

#if !defined(TX_CALL_PROGRESS)
#define TX_CALL_PROGRESS			(ENABLED & TRANSMITTER)
#endif /* TX_CALL_PROGRESS */
#if !defined(RX_CALL_PROGRESS)
#define RX_CALL_PROGRESS			(ENABLED & RECEIVER)
#endif /* RX_CALL_PROGRESS */

#endif /* CPTD_DEMO */

	/**************************/
	/**** Data demo config ****/
	/**************************/

#if defined(DATA_DEMO)

#include "rxtx.h"
#include "apsk.h"
#include "v32.h"
#include "tcm.h"
#if !defined(TX_BLOCK_LEN)
#define TX_BLOCK_LEN				(sizeof(struct TX_V32_BLOCK)/sizeof(short))
#elif TX_BLOCK_LEN == 0
#undef TX_BLOCK_LEN
#endif /* TX_BLOCK_LEN */
#if !defined(TX_SAMPLE_LEN)
#define TX_SAMPLE_LEN				511
#endif /* TX_SAMPLE_LEN */
#if !defined(TX_DATA_LEN)
#define TX_DATA_LEN					31
#endif /* TX_DATA_LEN */
#if !defined(TX_FIR_LEN)
#define TX_FIR_LEN					12
#endif /* TX_FIR_LEN */
#if !defined(ENCODER_BLOCK_LEN)
#define ENCODER_BLOCK_LEN			1
#endif /* ENCODER_BLOCK_LEN */

#if !defined(RX_BLOCK_LEN)
#define RX_BLOCK_LEN				(sizeof(struct RX_V32_BLOCK)/sizeof(short))
#elif RX_BLOCK_LEN == 0
#undef RX_BLOCK_LEN
#endif /* RX_BLOCK_LEN */
#if !defined(RX_SAMPLE_LEN)
#define RX_SAMPLE_LEN				127
#endif /* RX_SAMPLE_LEN */
#if !defined(RX_DATA_LEN)
#define RX_DATA_LEN					31
#endif /* RX_DATA_LEN */
#if !defined(RX_FIR_LEN)
#define RX_FIR_LEN					(2*64)
#endif /* RX_FIR_LEN */
#if !defined(EQ_COEF_LEN)
#define EQ_COEF_LEN					(2*64)
#endif /* EQ_COEF_LEN */
#if !defined(NEC_COEF_LEN)
#define NEC_COEF_LEN 				160
#endif /* NEC_COEF_LEN */
#if !defined(FEC_COEF_LEN)
#define FEC_COEF_LEN 				NEC_COEF_LEN
#endif /* FEC_COEF_LEN */
#if !defined(EC_COEF_LEN)
#define EC_COEF_LEN 				(NEC_COEF_LEN+FEC_COEF_LEN)
#endif /* EC_COEF_LEN */
#if !defined(DECODER_BLOCK_LEN)
#define DECODER_BLOCK_LEN			(sizeof(struct TCM8_DECODER_BLOCK)/sizeof(short))
#elif DECODER_BLOCK_LEN == 0
#undef DECODER_BLOCK_LEN
#endif /* DECODER_BLOCK_LEN */
#if !defined(TRACEBACK_LEN)
#define TRACEBACK_LEN	  			(TCM8_TRACEBACK_LEN*TCM8_DELAY_STATES)
#endif /* TRACEBACK_LEN */

#if !defined(RX_TIMING600_TABLE)
#define RX_TIMING600_TABLE			ENABLED
#endif /* RX_TIMING600_TABLE */
#if !defined(RX_TIMING2400_TABLE)
#define RX_TIMING2400_TABLE			ENABLED
#endif /* RX_TIMING2400_TABLE */ 
      	
#if !defined(TX_RCOS600_F1200_TABLE)
#define TX_RCOS600_F1200_TABLE		ENABLED
#endif /* TX_RCOS600_F1200_TABLE */
#if !defined(TX_RCOS600_F2400_TABLE)
#define TX_RCOS600_F2400_TABLE		ENABLED
#endif /* TX_RCOS600_F2400_TABLE */
#if !defined(RX_RCOS600_F1200_TABLE)
#define RX_RCOS600_F1200_TABLE		ENABLED
#endif /* RX_RCOS600_F1200_TABLE */
#if !defined(RX_RCOS600_F2400_TABLE)
#define RX_RCOS600_F2400_TABLE		ENABLED
#endif /* RX_RCOS600_F2400_TABLE */
#if !defined(RCOS2400_F1800_TABLE)
#define RCOS2400_F1800_TABLE		ENABLED
#endif /* RCOS2400_F1800_TABLE */

#if !defined(TX_V22A_MODEM) && !defined(TX_V22_MODEM)
#define TX_V22A_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V22A_MODEM */
#if !defined(TX_V22C_MODEM) && !defined(TX_V22_MODEM)
#define TX_V22C_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V22C_MODEM */
#if !defined(TX_V22_MODEM)
#define TX_V22_MODEM				(TX_V22A_MODEM | TX_V22C_MODEM)
#endif /* TX_V22_MODEM */
#if !defined(RX_V22A_MODEM) && !defined(RX_V22_MODEM)
#define RX_V22A_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V22A_MODEM */
#if !defined(RX_V22C_MODEM) && !defined(RX_V22_MODEM)
#define RX_V22C_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V22C_MODEM */
#if !defined(RX_V22_MODEM)
#define RX_V22_MODEM				(RX_V22A_MODEM | RX_V22C_MODEM)
#endif /* RX_V22_MODEM */
		   
#if !defined(TX_V32A_MODEM) && !defined(TX_V32_MODEM)
#define TX_V32A_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V32A_MODEM */
#if !defined(TX_V32C_MODEM) && !defined(TX_V32_MODEM)
#define TX_V32C_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V32C_MODEM */
#if !defined(TX_V32_MODEM)
#define TX_V32_MODEM				(TX_V32A_MODEM | TX_V32C_MODEM)
#endif /* TX_V32_MODEM */
#if !defined(RX_V32A_MODEM) && !defined(RX_V32_MODEM)
#define RX_V32A_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V32A_MODEM */
#if !defined(RX_V32C_MODEM) && !defined(RX_V32_MODEM)
#define RX_V32C_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V32C_MODEM */
#if !defined(RX_V32_MODEM)
#define RX_V32_MODEM				(RX_V32A_MODEM | RX_V32C_MODEM)
#endif /* RX_V32_MODEM */

#if !defined(TCM_7200)
#define TCM_7200					ENABLED 
#endif /* TCM_7200 */
#if !defined(TCM_9600)
#define TCM_9600					ENABLED 
#endif /* TCM_9600 */
#if !defined(TCM_12000)
#define TCM_12000					ENABLED 
#endif /* TCM_12000 */
#if !defined(TCM_14400)
#define TCM_14400					ENABLED 
#endif /* TCM_14400 */
#define TCM_ENCODER					((TCM_7200|TCM_9600|TCM_12000|TCM_14400) \
									& TRANSMITTER)
#define TCM_DECODER                 ((TCM_7200|TCM_9600|TCM_12000|TCM_14400) \
									& RECEIVER)
		   
#if !defined(CED_TONE)
#define CED_TONE					ENABLED
#endif /* CED_TONE */
										
#if !defined(TX_DTMF)
#define TX_DTMF						(ENABLED & TRANSMITTER)
#endif /* TX_DTMF */
				
#if !defined(TX_APSK_MODULATOR)
#define TX_APSK_MODULATOR			(ENABLED & TRANSMITTER)
#endif /* TX_APSK_MODULATOR */
#if !defined(RX_APSK_DEMODULATOR)
#define RX_APSK_DEMODULATOR			(ENABLED & RECEIVER)
#endif /* RX_APSK_DEMODULATOR */
	   
#endif /* DATA_DEMO */

	/**************************/
	/**** DTMF demo config ****/
	/**************************/
						
#if defined(DTMF_DEMO)

#include "rxtx.h"
#include "gendet.h"
#if !defined(TX_BLOCK_LEN)
#define TX_BLOCK_LEN				(sizeof(struct TX_GEN_BLOCK)/sizeof(short))
#elif TX_BLOCK_LEN == 0
#undef TX_BLOCK_LEN
#endif /* TX_BLOCK_LEN */
#if !defined(TX_SAMPLE_LEN)
#define TX_SAMPLE_LEN				41
#endif /* TX_SAMPLE_LEN */
#if !defined(TX_DATA_LEN)
#define TX_DATA_LEN					15
#endif /* TX_DATA_LEN */

#if !defined(RX_BLOCK_LEN)
#define RX_BLOCK_LEN				(sizeof(struct RX_DET_BLOCK)/sizeof(short))
#elif RX_BLOCK_LEN == 0
#undef RX_BLOCK_LEN
#endif /* RX_BLOCK_LEN */
#if !defined(RX_SAMPLE_LEN)
#define RX_SAMPLE_LEN				127
#endif /* RX_SAMPLE_LEN */
#if !defined(RX_DATA_LEN)
#define RX_DATA_LEN					15
#endif /* RX_DATA_LEN */
#if !defined(RX_FIR_LEN)
#define RX_FIR_LEN					42	/* 3*NUM_FILTERS */
#endif /* RX_FIR_LEN */

#if !defined(TX_DTMF)
#define TX_DTMF						(ENABLED & TRANSMITTER)
#endif /* TX_DTMF */
#if !defined(RX_DTMF)
#define RX_DTMF						(ENABLED & RECEIVER)
#endif /* RX_DTMF */

#endif /* DTMF_DEMO */

	/*************************************/
	/**** configuration for VFAX demo ****/
	/*************************************/
			    							   
#if defined(FAX_DEMO)

#include "rxtx.h"
#include "apsk.h"
#include "v27.h"				/* has the largest Tx_spare[] and Rx_spare[] req't */
#include "tcm.h"
#if !defined(TX_BLOCK_LEN)
#define TX_BLOCK_LEN				(sizeof(struct TX_V27_BLOCK)/sizeof(short))
#elif TX_BLOCK_LEN == 0
#undef TX_BLOCK_LEN
#endif /* TX_BLOCK_LEN */
#if !defined(TX_SAMPLE_LEN)
#define TX_SAMPLE_LEN				41
#endif /* TX_SAMPLE_LEN */
#if !defined(TX_DATA_LEN)
#define TX_DATA_LEN					32
#endif /* TX_DATA_LEN */
#if !defined(TX_FIR_LEN)
#define TX_FIR_LEN					12
#endif /* TX_FIR_LEN */

#if !defined(RX_BLOCK_LEN)
#define RX_BLOCK_LEN				(sizeof(struct RX_V27_BLOCK)/sizeof(short))
#elif RX_BLOCK_LEN == 0
#undef RX_BLOCK_LEN
#endif /* RX_BLOCK_LEN */
#if !defined(RX_SAMPLE_LEN)
#define RX_SAMPLE_LEN				127
#endif /* RX_SAMPLE_LEN */
#if !defined(RX_DATA_LEN)
#define RX_DATA_LEN					32
#endif /* RX_DATA_LEN */
#if !defined(RX_FIR_LEN)
#define RX_FIR_LEN					(2*64)
#endif /* RX_FIR_LEN */
#if !defined(EQ_COEF_LEN)
#define EQ_COEF_LEN					(2*64)
#endif /* EQ_COEF_LEN */
#if !defined(DECODER_BLOCK_LEN)
#define DECODER_BLOCK_LEN			(sizeof(struct TCM8_DECODER_BLOCK)/sizeof(short))
#elif DECODER_BLOCK_LEN == 0
#undef DECODER_BLOCK_LEN
#endif /* DECODER_BLOCK_LEN */
#if !defined(TRACEBACK_LEN)
#define TRACEBACK_LEN	  			(TCM8_TRACEBACK_LEN*TCM8_DELAY_STATES)
#endif /* TRACEBACK_LEN */

#if !defined(RX_TIMING1200_TABLE)
#define RX_TIMING1200_TABLE			ENABLED
#endif /* RX_TIMING1200_TABLE */       	
#if !defined(RX_TIMING1600_TABLE)
#define RX_TIMING1600_TABLE			ENABLED
#endif /* RX_TIMING1600_TABLE */       	
#if !defined(RX_TIMING2400_TABLE)
#define RX_TIMING2400_TABLE			ENABLED
#endif /* RX_TIMING2400_TABLE */       	

#if !defined(RCOS1200_F1800_TABLE)
#define RCOS1200_F1800_TABLE	   	ENABLED
#endif /* RCOS1200_F1800_TABLE */
#if !defined(RCOS1600_F1800_TABLE)
#define RCOS1600_F1800_TABLE		ENABLED
#endif /* RCOS1600_F1800_TABLE */
#if !defined(RCOS2400_F1800_TABLE)
#define RCOS2400_F1800_TABLE		ENABLED
#endif /* RCOS2400_F1800_TABLE */
				  
#if !defined(TX_V17_MODEM)
#define TX_V17_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V17_MODEM */
#if !defined(RX_V17_MODEM)
#define RX_V17_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V17_MODEM */

#if !defined(TX_V21_MODEM)
#define TX_V21_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V21_MODEM */
#if !defined(RX_V21_MODEM)
#define RX_V21_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V21_MODEM */

#if !defined(TX_V27_2400_MODEM) && !defined(TX_V27_MODEM)
#define TX_V27_2400_MODEM			(ENABLED & TRANSMITTER)
#endif /* TX_V27_2400_MODEM */
#if !defined(TX_V27_4800_MODEM) && !defined(TX_V27_MODEM)
#define TX_V27_4800_MODEM			(ENABLED & TRANSMITTER)
#endif /* TX_V27_4800_MODEM */
#if !defined(TX_V27_MODEM)
#define TX_V27_MODEM				(TX_V27_2400_MODEM | TX_V27_4800_MODEM)
#endif /* TX_V27_MODEM */
#if !defined(RX_V27_2400_MODEM) && !defined(RX_V27_MODEM)
#define RX_V27_2400_MODEM			(ENABLED & RECEIVER)
#endif /* RX_V27_2400_MODEM */
#if !defined(RX_V27_4800_MODEM) && !defined(RX_V27_MODEM)
#define RX_V27_4800_MODEM			(ENABLED & RECEIVER)
#endif /* RX_V27_4800_MODEM */
#if !defined(RX_V27_MODEM)
#define RX_V27_MODEM				(RX_V27_2400_MODEM | RX_V27_4800_MODEM)
#endif /* RX_V27_MODEM */
		
#if !defined(TX_V29_MODEM)
#define TX_V29_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V29_MODEM */
#if !defined(RX_V29_MODEM)
#define RX_V29_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V29_MODEM */

#if !defined(TCM_7200)
#define TCM_7200					ENABLED 
#endif /* TCM_7200 */
#if !defined(TCM_9600)
#define TCM_9600					ENABLED 
#endif /* TCM_9600 */
#if !defined(TCM_12000)
#define TCM_12000					ENABLED 
#endif /* TCM_12000 */
#if !defined(TCM_14400)
#define TCM_14400					ENABLED 
#endif /* TCM_14400 */
#define TCM_ENCODER					((TCM_7200|TCM_9600|TCM_12000|TCM_14400) \
									& TRANSMITTER)
#define TCM_DECODER                 ((TCM_7200|TCM_9600|TCM_12000|TCM_14400) \
									& RECEIVER)
		   
#if !defined(CNG_TONE)
#define CNG_TONE					ENABLED
#endif /* CNG_TONE */
#if !defined(CED_TONE)
#define CED_TONE					ENABLED
#endif /* CED_TONE */
#if !defined(TEP_1700_TONE)
#define TEP_1700_TONE				ENABLED
#endif /* TEP_1700_TONE */
#if !defined(TEP_1800_TONE)
#define TEP_1800_TONE				ENABLED
#endif /* TEP_1800_TONE */
				 
#if !defined(TX_APSK_MODULATOR)
#define TX_APSK_MODULATOR			(ENABLED & TRANSMITTER)
#endif /* TX_APSK_MODULATOR */
#if !defined(RX_APSK_DEMODULATOR)
#define RX_APSK_DEMODULATOR			(ENABLED & RECEIVER)
#endif /* RX_APSK_DEMODULATOR */

#endif /* FAX_DEMO */

	/*********************************/
	/**** configuration Fax Relay ****/
	/*********************************/
			    							   
#if defined(FAX_RELAY)

#include "rxtx.h"
#include "apsk.h"
#include "v27.h"				/* has the largest Tx_spare[] and Rx_spare[] req't */
#include "tcm.h"
#if !defined(TX_BLOCK_LEN)
#define TX_BLOCK_LEN				(sizeof(struct TX_V27_BLOCK)/sizeof(short))
#elif TX_BLOCK_LEN == 0
#undef TX_BLOCK_LEN
#endif /* TX_BLOCK_LEN */
#if !defined(TX_SAMPLE_LEN)
#define TX_SAMPLE_LEN				(2*8+1)
#endif /* TX_SAMPLE_LEN */
#if !defined(TX_DATA_LEN)
#define TX_DATA_LEN					15
#endif /* TX_DATA_LEN */
#if !defined(TX_FIR_LEN)
#define TX_FIR_LEN					12
#endif /* TX_FIR_LEN */

#if !defined(RX_BLOCK_LEN)
#define RX_BLOCK_LEN				(sizeof(struct RX_V27_BLOCK)/sizeof(short))
#elif RX_BLOCK_LEN == 0
#undef RX_BLOCK_LEN
#endif /* RX_BLOCK_LEN */
#if !defined(RX_SAMPLE_LEN)
#define RX_SAMPLE_LEN				(80+8+1)
#endif /* RX_SAMPLE_LEN */
#if !defined(RX_DATA_LEN)
#if defined(VOICE)
#define RX_DATA_LEN					RX_SAMPLE_LEN /* voice copies samples to Rx_data[]  */
#else /* VOICE */
#define RX_DATA_LEN					4
#endif /* VOICE */
#endif /* RX_DATA_LEN */
#if !defined(RX_FIR_LEN)
#define RX_FIR_LEN					(2*64)
#endif /* RX_FIR_LEN */
#if !defined(EQ_COEF_LEN)
#define EQ_COEF_LEN					(2*64)
#endif /* EQ_COEF_LEN */
#if !defined(DECODER_BLOCK_LEN)
#define DECODER_BLOCK_LEN			(sizeof(struct TCM8_DECODER_BLOCK)/sizeof(short))
#elif DECODER_BLOCK_LEN == 0
#undef DECODER_BLOCK_LEN
#endif /* DECODER_BLOCK_LEN */
#if !defined(TRACEBACK_LEN)
#define TRACEBACK_LEN	  			(TCM8_TRACEBACK_LEN*TCM8_DELAY_STATES)
#endif /* TRACEBACK_LEN */

#if !defined(RX_TIMING1200_TABLE)
#define RX_TIMING1200_TABLE			ENABLED
#endif /* RX_TIMING1200_TABLE */       	
#if !defined(RX_TIMING1600_TABLE)
#define RX_TIMING1600_TABLE			ENABLED
#endif /* RX_TIMING1600_TABLE */       	
#if !defined(RX_TIMING2400_TABLE)
#define RX_TIMING2400_TABLE			ENABLED
#endif /* RX_TIMING2400_TABLE */       	

#if !defined(RCOS1200_F1800_TABLE)
#define RCOS1200_F1800_TABLE	   	ENABLED
#endif /* RCOS1200_F1800_TABLE */
#if !defined(RCOS1600_F1800_TABLE)
#define RCOS1600_F1800_TABLE		ENABLED
#endif /* RCOS1600_F1800_TABLE */
#if !defined(RCOS2400_F1800_TABLE)
#define RCOS2400_F1800_TABLE		ENABLED
#endif /* RCOS2400_F1800_TABLE */
				  
#if !defined(TX_V17_MODEM)
#define TX_V17_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V17_MODEM */
#if !defined(RX_V17_MODEM)
#define RX_V17_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V17_MODEM */

#if !defined(TX_V21_MODEM)
#define TX_V21_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V21_MODEM */
#if !defined(RX_V21_MODEM)
#define RX_V21_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V21_MODEM */

#if !defined(TX_V27_2400_MODEM) && !defined(TX_V27_MODEM)
#define TX_V27_2400_MODEM			(ENABLED & TRANSMITTER)
#endif /* TX_V27_2400_MODEM */
#if !defined(TX_V27_4800_MODEM) && !defined(TX_V27_MODEM)
#define TX_V27_4800_MODEM			(ENABLED & TRANSMITTER)
#endif /* TX_V27_4800_MODEM */
#if !defined(TX_V27_MODEM)
#define TX_V27_MODEM				(TX_V27_2400_MODEM | TX_V27_4800_MODEM)
#endif /* TX_V27_MODEM */
#if !defined(RX_V27_2400_MODEM) && !defined(RX_V27_MODEM)
#define RX_V27_2400_MODEM			(ENABLED & RECEIVER)
#endif /* RX_V27_2400_MODEM */
#if !defined(RX_V27_4800_MODEM) && !defined(RX_V27_MODEM)
#define RX_V27_4800_MODEM			(ENABLED & RECEIVER)
#endif /* RX_V27_4800_MODEM */
#if !defined(RX_V27_MODEM)
#define RX_V27_MODEM				(RX_V27_2400_MODEM | RX_V27_4800_MODEM)
#endif /* RX_V27_MODEM */
		
#if !defined(TX_V29_MODEM)
#define TX_V29_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V29_MODEM */
#if !defined(RX_V29_MODEM)
#define RX_V29_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V29_MODEM */

#if !defined(TCM_7200)
#define TCM_7200					ENABLED 
#endif /* TCM_7200 */
#if !defined(TCM_9600)
#define TCM_9600					ENABLED 
#endif /* TCM_9600 */
#if !defined(TCM_12000)
#define TCM_12000					ENABLED 
#endif /* TCM_12000 */
#if !defined(TCM_14400)
#define TCM_14400					ENABLED 
#endif /* TCM_14400 */
#define TCM_ENCODER					((TCM_7200|TCM_9600|TCM_12000|TCM_14400) \
									& TRANSMITTER)
#define TCM_DECODER                 ((TCM_7200|TCM_9600|TCM_12000|TCM_14400) \
									& RECEIVER)
		   
#if !defined(CNG_TONE)
#define CNG_TONE					ENABLED
#endif /* CNG_TONE */
#if !defined(CED_TONE)
#define CED_TONE					ENABLED
#endif /* CED_TONE */
#if !defined(TEP_1700_TONE)
#define TEP_1700_TONE				ENABLED
#endif /* TEP_1700_TONE */
#if !defined(TEP_1800_TONE)
#define TEP_1800_TONE				ENABLED
#endif /* TEP_1800_TONE */
				 
#if !defined(TX_APSK_MODULATOR)
#define TX_APSK_MODULATOR			(ENABLED & TRANSMITTER)
#endif /* TX_APSK_MODULATOR */
#if !defined(RX_APSK_DEMODULATOR)
#define RX_APSK_DEMODULATOR			(ENABLED & RECEIVER)
#endif /* RX_APSK_DEMODULATOR */

#endif /* FAX_RELAY */
	
	/********************************/
	/**** FSVS-210 configuration ****/
	/********************************/

#if defined(FSVS_DEMO)
#undef VSIM_DEMO // needed to permit VSIM-supported builds

#include "rxtx.h"
#include "apsk.h"
#include "v32.h"
#include "tcm.h"
#include "fsvs.h"

#if !defined(TX_BLOCK_LEN)
#define TX_BLOCK_LEN				(sizeof(struct TX_FSVS_BLOCK)/sizeof(short))
#elif TX_BLOCK_LEN == 0
#undef TX_BLOCK_LEN
#endif /* TX_BLOCK_LEN */
#if !defined(TX_SAMPLE_LEN)
#define TX_SAMPLE_LEN 				511
#endif /* TX_SAMPLE_LEN */
#if !defined(TX_DATA_LEN)
#define TX_DATA_LEN 				15
#endif /* TX_DATA_LEN */
#if !defined(TX_FIR_LEN)
#define TX_FIR_LEN 					12
#endif /* TX_FIR_LEN */
#if !defined(ENCODER_BLOCK_LEN)
#define ENCODER_BLOCK_LEN			(sizeof(struct FSVS_ENCODER_BLOCK)/sizeof(short))
#elif ENCODER_BLOCK_LEN == 0
#undef ENCODER_BLOCK_LEN
#endif /* ENCODER_BLOCK_LEN */

#if !defined(RX_BLOCK_LEN)
#define RX_BLOCK_LEN				(sizeof(struct RX_FSVS_BLOCK)/sizeof(short))
#elif RX_BLOCK_LEN == 0
#undef RX_BLOCK_LEN
#endif /* RX_BLOCK_LEN */
#if !defined(RX_SAMPLE_LEN)
#define RX_SAMPLE_LEN 				127
#endif /* RX_SAMPLE_LEN */
#if !defined(RX_DATA_LEN)
#define RX_DATA_LEN 				15
#endif /* RX_DATA_LEN */
#if !defined(RX_FIR_LEN)
#define RX_FIR_LEN 					(2*128) //(2*64)
#endif /* RX_FIR_LEN */
#if !defined(EQ_COEF_LEN)
#define EQ_COEF_LEN 				(2*64)
#endif /* EQ_COEF_LEN */
#if !defined(NEC_COEF_LEN)
#define NEC_COEF_LEN 				160
#endif /* NEC_COEF_LEN */
#if !defined(FEC_COEF_LEN)
#define FEC_COEF_LEN 				NEC_COEF_LEN
#endif /* FEC_COEF_LEN */
#if !defined(EC_COEF_LEN)
#define EC_COEF_LEN 				(NEC_COEF_LEN+FEC_COEF_LEN)
#endif /* EC_COEF_LEN */
#if !defined(DECODER_BLOCK_LEN)
#define DECODER_BLOCK_LEN			(sizeof(struct TCM8_DECODER_BLOCK)/sizeof(short))
#elif DECODER_BLOCK_LEN == 0
#undef DECODER_BLOCK_LEN
#endif /* DECODER_BLOCK_LEN */
#if !defined(TRACEBACK_LEN)
#define TRACEBACK_LEN	  			(TCM8_TRACEBACK_LEN*TCM8_DELAY_STATES)
#endif /* TRACEBACK_LEN */
#if !defined(CHANNEL_ECHO_LEN)
#define CHANNEL_ECHO_LEN			(2*8000)
#endif /* CHANNEL_ECHO_LEN */

#if !defined(RX_TIMING1200_TABLE)
#define RX_TIMING1200_TABLE			ENABLED
#endif /* RX_TIMING1200_TABLE */       	
#if !defined(RX_TIMING2400_TABLE)
#define RX_TIMING2400_TABLE			ENABLED
#endif /* RX_TIMING2400_TABLE */       	

#if !defined(RCOS1200_F1800_TABLE)
#define RCOS1200_F1800_TABLE	   	ENABLED
#endif /* RCOS1200_F1800_TABLE */
#if !defined(RCOS2400_F1800_TABLE)
#define RCOS2400_F1800_TABLE		ENABLED
#endif /* RCOS2400_F1800_TABLE */

#if !defined(TX_FSVS_MODEM)
#define TX_FSVS_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_FSVS_MODEM */
#if !defined(RX_FSVS_MODEM)
#define RX_FSVS_MODEM				(ENABLED & RECEIVER)
#endif /* RX_FSVS_MODEM */

#if !defined(TX_V26_MODEM)
#define TX_V26_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V26_MODEM */
#if !defined(RX_V26_MODEM)
#define RX_V26_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V26_MODEM */

#if !defined(TX_V32A_MODEM) && !defined(TX_V32_MODEM)
#define TX_V32A_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V32A_MODEM */
#if !defined(TX_V32C_MODEM) && !defined(TX_V32_MODEM)
#define TX_V32C_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V32C_MODEM */
#if !defined(TX_V32_MODEM)
#define TX_V32_MODEM				(TX_V32A_MODEM | TX_V32C_MODEM)
#endif /* TX_V32_MODEM */
#if !defined(RX_V32A_MODEM) && !defined(RX_V32_MODEM)
#define RX_V32A_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V32A_MODEM */
#if !defined(RX_V32C_MODEM) && !defined(RX_V32_MODEM)
#define RX_V32C_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V32C_MODEM */
#if !defined(RX_V32_MODEM)
#define RX_V32_MODEM				(RX_V32A_MODEM | RX_V32C_MODEM)
#endif /* RX_V32_MODEM */

#if !defined(TCM_7200)
#define TCM_7200					DISABLED 
#endif /* TCM_7200 */
#if !defined(TCM_9600)
#define TCM_9600					ENABLED 
#endif /* TCM_9600 */
#if !defined(TCM_12000)
#define TCM_12000					DISABLED 
#endif /* TCM_12000 */
#if !defined(TCM_14400)
#define TCM_14400					DISABLED 
#endif /* TCM_14400 */
#define TCM_ENCODER					((TCM_7200|TCM_9600|TCM_12000|TCM_14400) \
									& TRANSMITTER)
#define TCM_DECODER                 ((TCM_7200|TCM_9600|TCM_12000|TCM_14400) \
									& RECEIVER)
								   
#if !defined(TX_APSK_MODULATOR)
#define TX_APSK_MODULATOR			(ENABLED & TRANSMITTER)
#endif /* TX_APSK_MODULATOR */
#if !defined(RX_APSK_DEMODULATOR)
#define RX_APSK_DEMODULATOR			(ENABLED & RECEIVER)
#endif /* RX_APSK_DEMODULATOR */
#if !defined(ECHO_CANCELLER)
#define ECHO_CANCELLER				(ENABLED & TRANSMITTER & RECEIVER)
#endif /* ECHO_CANCELLER */
#endif /* FSVS_DEMO */

	/************************************/
	/**** configuration for LRDM demo ****/
	/************************************/
				  
#if defined(LRDM_DEMO)

#include "rxtx.h"
#include "apsk.h"
#include "v32.h"
#include "tcm.h"
#if !defined(TX_BLOCK_LEN)
#define TX_BLOCK_LEN				(sizeof(struct TX_V32_BLOCK)/sizeof(short))
#elif TX_BLOCK_LEN == 0
#undef TX_BLOCK_LEN
#endif /* TX_BLOCK_LEN */
#if !defined(TX_SAMPLE_LEN)
#define TX_SAMPLE_LEN 				2047
#endif /* TX_SAMPLE_LEN */
#if !defined(TX_DATA_LEN)
#define TX_DATA_LEN 				31
#endif /* TX_DATA_LEN */
#if !defined(TX_FIR_LEN)
#define TX_FIR_LEN 					12
#endif /* TX_FIR_LEN */
#if !defined(ENCODER_BLOCK_LEN)
#define ENCODER_BLOCK_LEN			1
#endif /* ENCODER_BLOCK_LEN */

#if !defined(RX_BLOCK_LEN)
#define RX_BLOCK_LEN				(sizeof(struct RX_V32_BLOCK)/sizeof(short))
#elif RX_BLOCK_LEN == 0
#undef RX_BLOCK_LEN
#endif /* RX_BLOCK_LEN */
#if !defined(RX_SAMPLE_LEN)
#define RX_SAMPLE_LEN 				127	
#endif /* RX_SAMPLE_LEN */
#if !defined(RX_DATA_LEN)
#define RX_DATA_LEN 				31
#endif /* RX_DATA_LEN */
#if !defined(RX_FIR_LEN)
#define RX_FIR_LEN 					(2*64)
#endif /* RX_FIR_LEN */
#if !defined(EQ_COEF_LEN)
#define EQ_COEF_LEN 				RX_FIR_LEN
#endif /* EQ_COEF_LEN */
#if !defined(NEC_COEF_LEN)
#define NEC_COEF_LEN 				160
#endif /* NEC_COEF_LEN */
#if !defined(FEC_COEF_LEN)
#define FEC_COEF_LEN 				NEC_COEF_LEN
#endif /* FEC_COEF_LEN */
#if !defined(EC_COEF_LEN)
#define EC_COEF_LEN 				(NEC_COEF_LEN+FEC_COEF_LEN)
#endif /* EC_COEF_LEN */
#if !defined(DECODER_BLOCK_LEN)
#define DECODER_BLOCK_LEN			(sizeof(struct TCM8_DECODER_BLOCK)/sizeof(short))
#elif DECODER_BLOCK_LEN == 0
#undef DECODER_BLOCK_LEN
#endif /* DECODER_BLOCK_LEN */
#if !defined(TRACEBACK_LEN)
#define TRACEBACK_LEN	  			(TCM8_TRACEBACK_LEN*TCM8_DELAY_STATES)
#endif /* TRACEBACK_LEN */

#if !defined(RX_TIMING600_TABLE)
#define RX_TIMING600_TABLE			ENABLED
#endif /* RX_TIMING600_TABLE */
#if !defined(RX_TIMING1200_TABLE)
#define RX_TIMING1200_TABLE			DISABLED
#endif /* RX_TIMING1200_TABLE */       	
#if !defined(RX_TIMING1600_TABLE)
#define RX_TIMING1600_TABLE			DISABLED
#endif /* RX_TIMING1600_TABLE */       	
#if !defined(RX_TIMING2400_TABLE)
#define RX_TIMING2400_TABLE			ENABLED
#endif /* RX_TIMING2400_TABLE */ 

#if !defined(TX_RCOS600_F1200_TABLE)
#define TX_RCOS600_F1200_TABLE		ENABLED
#endif /* TX_RCOS600_F1200_TABLE */
#if !defined(TX_RCOS600_F2400_TABLE)
#define TX_RCOS600_F2400_TABLE		ENABLED
#endif /* TX_RCOS600_F2400_TABLE */
#if !defined(RX_RCOS600_F1200_TABLE)
#define RX_RCOS600_F1200_TABLE		ENABLED
#endif /* RX_RCOS600_F1200_TABLE */
#if !defined(RX_RCOS600_F2400_TABLE)
#define RX_RCOS600_F2400_TABLE		ENABLED
#endif /* RX_RCOS600_F2400_TABLE */
#if !defined(RCOS1200_F1800_TABLE)
#define RCOS1200_F1800_TABLE	   	DISABLED
#endif /* RCOS1200_F1800_TABLE */
#if !defined(RCOS1600_F1800_TABLE)
#define RCOS1600_F1800_TABLE		DISABLED
#endif /* RCOS1600_F1800_TABLE */
#if !defined(RCOS2400_F1800_TABLE)
#define RCOS2400_F1800_TABLE		ENABLED
#endif /* RCOS2400_F1800_TABLE */

#if !defined(TX_V22A_MODEM) && !defined(TX_V22_MODEM)
#define TX_V22A_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V22A_MODEM */
#if !defined(TX_V22C_MODEM) && !defined(TX_V22_MODEM)
#define TX_V22C_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V22C_MODEM */
#if !defined(TX_V22_MODEM)
#define TX_V22_MODEM				(TX_V22A_MODEM | TX_V22C_MODEM)
#endif /* TX_V22_MODEM */
#if !defined(RX_V22A_MODEM) && !defined(RX_V22_MODEM)
#define RX_V22A_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V22A_MODEM */
#if !defined(RX_V22C_MODEM) && !defined(RX_V22_MODEM)
#define RX_V22C_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V22C_MODEM */
#if !defined(RX_V22_MODEM)
#define RX_V22_MODEM				(RX_V22A_MODEM | RX_V22C_MODEM)
#endif /* RX_V22_MODEM */

#if !defined(TX_V32A_MODEM) && !defined(TX_V32_MODEM)
#define TX_V32A_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V32A_MODEM */
#if !defined(TX_V32C_MODEM) && !defined(TX_V32_MODEM)
#define TX_V32C_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V32C_MODEM */
#if !defined(TX_V32_MODEM)
#define TX_V32_MODEM				(TX_V32A_MODEM | TX_V32C_MODEM)
#endif /* TX_V32_MODEM */
#if !defined(RX_V32A_MODEM) && !defined(RX_V32_MODEM)
#define RX_V32A_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V32A_MODEM */
#if !defined(RX_V32C_MODEM) && !defined(RX_V32_MODEM)
#define RX_V32C_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V32C_MODEM */
#if !defined(RX_V32_MODEM)
#define RX_V32_MODEM				(RX_V32A_MODEM | RX_V32C_MODEM)
#endif /* RX_V32_MODEM */

#if !defined(TCM_7200)
#define TCM_7200					ENABLED 
#endif /* TCM_7200 */
#if !defined(TCM_9600)
#define TCM_9600					ENABLED 
#endif /* TCM_9600 */
#if !defined(TCM_12000)
#define TCM_12000					ENABLED 
#endif /* TCM_12000 */
#if !defined(TCM_14400)
#define TCM_14400					ENABLED 
#endif /* TCM_14400 */
#define TCM_ENCODER					((TCM_7200|TCM_9600|TCM_12000|TCM_14400) \
									& TRANSMITTER)
#define TCM_DECODER                 ((TCM_7200|TCM_9600|TCM_12000|TCM_14400) \
									& RECEIVER)
	   
#if !defined(CED_TONE)
#define CED_TONE					ENABLED
#endif /* CED_TONE */
										
#if !defined(TX_DTMF)
#define TX_DTMF						(ENABLED & TRANSMITTER)
#endif /* TX_DTMF */
				
#if !defined(TX_APSK_MODULATOR)
#define TX_APSK_MODULATOR			(ENABLED & TRANSMITTER)
#endif /* TX_APSK_MODULATOR */
#if !defined(RX_APSK_DEMODULATOR)
#define RX_APSK_DEMODULATOR			(ENABLED & RECEIVER)
#endif /* RX_APSK_DEMODULATOR */
#if !defined(ECHO_CANCELLER)
#define ECHO_CANCELLER				(ENABLED & TRANSMITTER & RECEIVER)
#endif /* ECHO_CANCELLER */

#endif /* LRDM_DEMO */
	
	/************************/
	/**** MF demo config ****/
	/************************/
						
#if defined(MF_DEMO)

#include "rxtx.h"
#include "gendet.h"
#if !defined(TX_BLOCK_LEN)
#define TX_BLOCK_LEN				(sizeof(struct TX_GEN_BLOCK)/sizeof(short))
#elif TX_BLOCK_LEN == 0
#undef TX_BLOCK_LEN
#endif /* TX_BLOCK_LEN */
#if !defined(TX_SAMPLE_LEN)
#define TX_SAMPLE_LEN				41
#endif /* TX_SAMPLE_LEN */
#if !defined(TX_DATA_LEN)
#define TX_DATA_LEN					15
#endif /* TX_DATA_LEN */

#if !defined(RX_BLOCK_LEN)
#define RX_BLOCK_LEN				(sizeof(struct RX_DET_BLOCK)/sizeof(short))
#elif RX_BLOCK_LEN == 0
#undef RX_BLOCK_LEN
#endif /* RX_BLOCK_LEN */
#if !defined(RX_SAMPLE_LEN)
#define RX_SAMPLE_LEN				127
#endif /* RX_SAMPLE_LEN */
#if !defined(RX_DATA_LEN)
#define RX_DATA_LEN					15
#endif /* RX_DATA_LEN */
#if !defined(RX_FIR_LEN)
#define RX_FIR_LEN					48	/* 3*NUM_FILTERS+2*NUM_DIGITS */
#endif /* RX_FIR_LEN */

#if !defined(TX_MF)
#define TX_MF						(ENABLED & TRANSMITTER)
#endif /* TX_MF */
#if !defined(RX_MF)
#define RX_MF						(ENABLED & RECEIVER)
#endif /* RX_MF */

#endif /* MF_DEMO */

	/******************************************/
	/**** RXTX demo sets up minimal memory ****/
	/******************************************/
	
#if defined(RXTX_DEMO)

#include "rxtx.h"
#if !defined(TX_BLOCK_LEN)
#define TX_BLOCK_LEN				(sizeof(struct TX_BLOCK)/sizeof(short))
#elif TX_BLOCK_LEN == 0
#undef TX_BLOCK_LEN
#endif /* TX_BLOCK_LEN */
#if !defined(TX_SAMPLE_LEN)
#define TX_SAMPLE_LEN				41
#endif /* TX_SAMPLE_LEN */
#if !defined(TX_DATA_LEN)
#define TX_DATA_LEN					7
#endif /* TX_DATA_LEN */

#if !defined(RX_BLOCK_LEN)
#define RX_BLOCK_LEN				(sizeof(struct RX_BLOCK)/sizeof(short))
#elif RX_BLOCK_LEN == 0
#undef RX_BLOCK_LEN
#endif /* RX_BLOCK_LEN */
#if !defined(RX_SAMPLE_LEN)
#define RX_SAMPLE_LEN				127
#endif /* RX_SAMPLE_LEN */
#if !defined(RX_DATA_LEN)
#define RX_DATA_LEN					7
#endif /* RX_DATA_LEN */

#endif /* RXTX_DEMO */

	/*************************/
	/**** tonedemo config ****/
	/*************************/

#if defined(TONE_DEMO)

#include "rxtx.h"
#include "gendet.h"
#if !defined(TX_BLOCK_LEN)
#define TX_BLOCK_LEN				(sizeof(struct TX_GEN_BLOCK)/sizeof(short))
#elif TX_BLOCK_LEN == 0
#undef TX_BLOCK_LEN
#endif /* TX_BLOCK_LEN */
#if !defined(TX_SAMPLE_LEN)
#define TX_SAMPLE_LEN				41
#endif /* TX_SAMPLE_LEN */
#if !defined(TX_DATA_LEN)
#define TX_DATA_LEN					1
#endif /* TX_DATA_LEN */

#if !defined(RX_BLOCK_LEN)
#define RX_BLOCK_LEN				(sizeof(struct RX_DET_BLOCK)/sizeof(short))
#elif RX_BLOCK_LEN == 0
#undef RX_BLOCK_LEN
#endif /* RX_BLOCK_LEN */
#if !defined(RX_SAMPLE_LEN)
#define RX_SAMPLE_LEN				127
#endif /* RX_SAMPLE_LEN */
#if !defined(RX_DATA_LEN)
#define RX_DATA_LEN					1
#endif /* RX_DATA_LEN */
#if !defined(RX_FIR_LEN)
#define RX_FIR_LEN					42	/* 3*NUM_FILTERS+2*NUM_DIGITS */
#endif /* RX_FIR_LEN */

#endif /* TONE_DEMO */

	/************************************/
	/**** configuration for V17 demo ****/
	/************************************/
				  
#if defined(V17_DEMO)

#include "rxtx.h"
#include "apsk.h"
#include "v17.h"
#include "tcm.h"
#if !defined(TX_BLOCK_LEN)
#define TX_BLOCK_LEN				(sizeof(struct TX_V17_BLOCK)/sizeof(short))
#elif TX_BLOCK_LEN == 0
#undef TX_BLOCK_LEN
#endif /* TX_BLOCK_LEN */
#if !defined(TX_SAMPLE_LEN)
#define TX_SAMPLE_LEN 				41
#endif /* TX_SAMPLE_LEN */
#if !defined(TX_DATA_LEN)
#define TX_DATA_LEN 				7
#endif /* TX_DATA_LEN */
#if !defined(TX_FIR_LEN)
#define TX_FIR_LEN 					12
#endif /* TX_FIR_LEN */

#if !defined(RX_BLOCK_LEN)
#define RX_BLOCK_LEN				(sizeof(struct RX_V17_BLOCK)/sizeof(short))
#elif RX_BLOCK_LEN == 0
#undef RX_BLOCK_LEN
#endif /* RX_BLOCK_LEN */
#if !defined(RX_SAMPLE_LEN)
#define RX_SAMPLE_LEN 				127	
#endif /* RX_SAMPLE_LEN */
#if !defined(RX_DATA_LEN)
#define RX_DATA_LEN 				7
#endif /* RX_DATA_LEN */
#if !defined(RX_FIR_LEN)
#define RX_FIR_LEN 					(2*64)
#endif /* RX_FIR_LEN */
#if !defined(EQ_COEF_LEN)
#define EQ_COEF_LEN 				RX_FIR_LEN
#endif /* EQ_COEF_LEN */
#if !defined(DECODER_BLOCK_LEN)
#define DECODER_BLOCK_LEN			(sizeof(struct TCM8_DECODER_BLOCK)/sizeof(short))
#elif DECODER_BLOCK_LEN == 0
#undef DECODER_BLOCK_LEN
#endif /* DECODER_BLOCK_LEN */
#if !defined(TRACEBACK_LEN)
#define TRACEBACK_LEN	  			(TCM8_TRACEBACK_LEN*TCM8_DELAY_STATES)
#endif /* TRACEBACK_LEN */

#if !defined(RX_TIMING2400_TABLE)
#define RX_TIMING2400_TABLE			ENABLED
#endif /* RX_TIMING2400_TABLE */       	

#if !defined(RCOS2400_F1800_TABLE)
#define RCOS2400_F1800_TABLE		ENABLED
#endif /* RCOS2400_F1800_TABLE */

#if !defined(TX_V17_MODEM)
#define TX_V17_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V17_MODEM */
#if !defined(RX_V17_MODEM)
#define RX_V17_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V17_MODEM */

#if !defined(TCM_7200)
#define TCM_7200					ENABLED 
#endif /* TCM_7200 */
#if !defined(TCM_9600)
#define TCM_9600					ENABLED 
#endif /* TCM_9600 */
#if !defined(TCM_12000)
#define TCM_12000					ENABLED 
#endif /* TCM_12000 */
#if !defined(TCM_14400)
#define TCM_14400					ENABLED 
#endif /* TCM_14400 */
#define TCM_ENCODER					((TCM_7200|TCM_9600|TCM_12000|TCM_14400) \
									& TRANSMITTER)
#define TCM_DECODER                 ((TCM_7200|TCM_9600|TCM_12000|TCM_14400) \
									& RECEIVER)

#if !defined(TEP_1800_TONE)
#define TEP_1800_TONE				ENABLED
#endif /* TEP_1800_TONE */
	
#if !defined(TX_APSK_MODULATOR)
#define TX_APSK_MODULATOR			(ENABLED & TRANSMITTER)
#endif /* TX_APSK_MODULATOR */
#if !defined(RX_APSK_DEMODULATOR)
#define RX_APSK_DEMODULATOR			(ENABLED & RECEIVER)
#endif /* RX_APSK_DEMODULATOR */

#endif /* V17_DEMO */
 
	/************************************/
	/**** configuration for V21 demo ****/
	/************************************/
	
#if defined(V21_DEMO)

#include "rxtx.h"
#include "fsk.h"
#include "v21.h"
#if !defined(TX_BLOCK_LEN)
#define TX_BLOCK_LEN				(sizeof(struct TX_V21_BLOCK)/sizeof(short))
#elif TX_BLOCK_LEN == 0
#undef TX_BLOCK_LEN
#endif /* TX_BLOCK_LEN */
#if !defined(TX_SAMPLE_LEN)
#define TX_SAMPLE_LEN				41
#endif /* TX_SAMPLE_LEN */
#if !defined(TX_DATA_LEN)
#define TX_DATA_LEN					15
#endif /* TX_DATA_LEN */

#if !defined(RX_BLOCK_LEN)
#define RX_BLOCK_LEN				(sizeof(struct RX_V21_BLOCK)/sizeof(short))
#elif RX_BLOCK_LEN == 0
#undef RX_BLOCK_LEN
#endif /* RX_BLOCK_LEN */
#if !defined(RX_SAMPLE_LEN)
#define RX_SAMPLE_LEN				127
#endif /* RX_SAMPLE_LEN */
#if !defined(RX_DATA_LEN)
#define RX_DATA_LEN					15
#endif /* RX_DATA_LEN */

#if !defined(TX_V21_CH1_MODEM)
#define TX_V21_CH1_MODEM			(ENABLED & TRANSMITTER)
#endif /* TX_V21_CH1_MODEM */
#if !defined(TX_V21_MODEM)
#define TX_V21_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V21_MODEM */
#if !defined(RX_V21_CH1_MODEM)
#define RX_V21_CH1_MODEM			(ENABLED & RECEIVER)
#endif /* RX_V21_CH1_MODEM */
#if !defined(RX_V21_MODEM)
#define RX_V21_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V21_MODEM */

#if !defined(TX_BELL103_MODEM)
#define TX_BELL103_MODEM			(ENABLED & TRANSMITTER)
#endif /* TX_BELL103_MODEM */
#if !defined(RX_BELL103_MODEM)
#define RX_BELL103_MODEM			(ENABLED & RECEIVER)
#endif /* RX_BELL103_MODEM */

#if !defined(CED_TONE)
#define CED_TONE					ENABLED
#endif /* CED_TONE */
				   
#if !defined(TX_FSK_MODULATOR)
#define TX_FSK_MODULATOR			(ENABLED & TRANSMITTER)
#endif /* TX_FSK_MODULATOR */
#if !defined(RX_FSK_DEMODULATOR)
#define RX_FSK_DEMODULATOR			(ENABLED & RECEIVER)
#endif /* RX_FSK_MODULATOR */

#endif /* V21_DEMO */
		   
	/************************************/
	/**** configuration for V22 demo ****/
	/************************************/
	
#if defined(V22_DEMO)

#include "rxtx.h"
#include "apsk.h"
#include "v22.h"
#if !defined(TX_BLOCK_LEN)
#define TX_BLOCK_LEN				(sizeof(struct TX_V22_BLOCK)/sizeof(short))
#elif TX_BLOCK_LEN == 0
#undef TX_BLOCK_LEN
#endif /* TX_BLOCK_LEN */
#if !defined(TX_SAMPLE_LEN)
#define TX_SAMPLE_LEN				41
#endif /* TX_SAMPLE_LEN */
#if !defined(TX_DATA_LEN)
#define TX_DATA_LEN					15
#endif /* TX_DATA_LEN */
#if !defined(TX_FIR_LEN)
#define TX_FIR_LEN					12
#endif /* TX_FIR_LEN */

#if !defined(RX_BLOCK_LEN)
#define RX_BLOCK_LEN				(sizeof(struct RX_V22_BLOCK)/sizeof(short))
#elif RX_BLOCK_LEN == 0
#undef RX_BLOCK_LEN
#endif /* RX_BLOCK_LEN */
#if !defined(RX_SAMPLE_LEN)
#define RX_SAMPLE_LEN				127
#endif /* RX_SAMPLE_LEN */
#if !defined(RX_DATA_LEN)
#define RX_DATA_LEN					15
#endif /* RX_DATA_LEN */
#if !defined(RX_FIR_LEN)
#define RX_FIR_LEN					(2*64)
#endif /* RX_FIR_LEN */
#if !defined(EQ_COEF_LEN)
#define EQ_COEF_LEN					(2*64)
#endif /* EQ_COEF_LEN */

#if !defined(RX_TIMING600_TABLE)
#define RX_TIMING600_TABLE			ENABLED
#endif /* RX_TIMING600_TABLE */
      	
#if !defined(TX_RCOS600_F1200_TABLE)
#define TX_RCOS600_F1200_TABLE		ENABLED
#endif /* TX_RCOS600_F1200_TABLE */
#if !defined(TX_RCOS600_F2400_TABLE)
#define TX_RCOS600_F2400_TABLE		ENABLED
#endif /* TX_RCOS600_F2400_TABLE */
#if !defined(RX_RCOS600_F1200_TABLE)
#define RX_RCOS600_F1200_TABLE		ENABLED
#endif /* RX_RCOS600_F1200_TABLE */
#if !defined(RX_RCOS600_F2400_TABLE)
#define RX_RCOS600_F2400_TABLE		ENABLED
#endif /* RX_RCOS600_F2400_TABLE */

#if !defined(TX_V22A_MODEM) && !defined(TX_V22_MODEM)
#define TX_V22A_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V22A_MODEM */
#if !defined(TX_V22C_MODEM) && !defined(TX_V22_MODEM)
#define TX_V22C_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V22C_MODEM */
#if !defined(TX_V22_MODEM)
#define TX_V22_MODEM				(TX_V22A_MODEM | TX_V22C_MODEM)
#endif /* TX_V22_MODEM */
#if !defined(RX_V22A_MODEM) && !defined(RX_V22_MODEM)
#define RX_V22A_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V22A_MODEM */
#if !defined(RX_V22C_MODEM) && !defined(RX_V22_MODEM)
#define RX_V22C_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V22C_MODEM */
#if !defined(RX_V22_MODEM)
#define RX_V22_MODEM				(RX_V22A_MODEM | RX_V22C_MODEM)
#endif /* RX_V22_MODEM */
		   
#if !defined(CED_TONE)
#define CED_TONE					ENABLED
#endif /* CED_TONE */
				   
#if !defined(TX_APSK_MODULATOR)
#define TX_APSK_MODULATOR			(ENABLED & TRANSMITTER)
#endif /* TX_APSK_MODULATOR */
#if !defined(RX_APSK_DEMODULATOR)
#define RX_APSK_DEMODULATOR			(ENABLED & RECEIVER)
#endif /* RX_APSK_DEMODULATOR */

#endif /* V22_DEMO */
		   
	/*****************************************/
	/**** configuration for dnld_v22 demo ****/
	/*****************************************/
	
#if defined(DNLD_V22)

#include "rxtx.h"
#include "apsk.h"
#include "v22.h"
#if !defined(TX_BLOCK_LEN)
#define TX_BLOCK_LEN				(sizeof(struct TX_V22_BLOCK)/sizeof(short))
#elif TX_BLOCK_LEN == 0
#undef TX_BLOCK_LEN
#endif /* TX_BLOCK_LEN */
#if !defined(TX_SAMPLE_LEN)
#define TX_SAMPLE_LEN				41
#endif /* TX_SAMPLE_LEN */
#if !defined(TX_DATA_LEN)
#define TX_DATA_LEN					62
#endif /* TX_DATA_LEN */
#if !defined(TX_FIR_LEN)
#define TX_FIR_LEN					12
#endif /* TX_FIR_LEN */

#if !defined(RX_BLOCK_LEN)
#define RX_BLOCK_LEN				(sizeof(struct RX_V22_BLOCK)/sizeof(short))
#elif RX_BLOCK_LEN == 0
#undef RX_BLOCK_LEN
#endif /* RX_BLOCK_LEN */
#if !defined(RX_SAMPLE_LEN)
#define RX_SAMPLE_LEN				127
#endif /* RX_SAMPLE_LEN */
#if !defined(RX_DATA_LEN)
#define RX_DATA_LEN					7
#endif /* RX_DATA_LEN */
#if !defined(RX_FIR_LEN)
#define RX_FIR_LEN					(2*64)
#endif /* RX_FIR_LEN */
#if !defined(EQ_COEF_LEN)
#define EQ_COEF_LEN					(2*64)
#endif /* EQ_COEF_LEN */

#if !defined(RX_TIMING600_TABLE)
#define RX_TIMING600_TABLE			ENABLED
#endif /* RX_TIMING600_TABLE */
      	
#if !defined(TX_RCOS600_F1200_TABLE)
#define TX_RCOS600_F1200_TABLE		ENABLED
#endif /* TX_RCOS600_F1200_TABLE */
#if !defined(TX_RCOS600_F2400_TABLE)
#define TX_RCOS600_F2400_TABLE		ENABLED
#endif /* TX_RCOS600_F2400_TABLE */
#if !defined(RX_RCOS600_F1200_TABLE)
#define RX_RCOS600_F1200_TABLE		ENABLED
#endif /* RX_RCOS600_F1200_TABLE */
#if !defined(RX_RCOS600_F2400_TABLE)
#define RX_RCOS600_F2400_TABLE		ENABLED
#endif /* RX_RCOS600_F2400_TABLE */

#if !defined(TX_V22A_MODEM) && !defined(TX_V22_MODEM)
#define TX_V22A_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V22A_MODEM */
#if !defined(TX_V22C_MODEM) && !defined(TX_V22_MODEM)
#define TX_V22C_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V22C_MODEM */
#if !defined(TX_V22_MODEM)
#define TX_V22_MODEM				(TX_V22A_MODEM | TX_V22C_MODEM)
#endif /* TX_V22_MODEM */
#if !defined(RX_V22A_MODEM) && !defined(RX_V22_MODEM)
#define RX_V22A_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V22A_MODEM */
#if !defined(RX_V22C_MODEM) && !defined(RX_V22_MODEM)
#define RX_V22C_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V22C_MODEM */
#if !defined(RX_V22_MODEM)
#define RX_V22_MODEM				(RX_V22A_MODEM | RX_V22C_MODEM)
#endif /* RX_V22_MODEM */
		   
#if !defined(CED_TONE)
#define CED_TONE					ENABLED
#endif /* CED_TONE */
				   
#if !defined(TX_APSK_MODULATOR)
#define TX_APSK_MODULATOR			(ENABLED & TRANSMITTER)
#endif /* TX_APSK_MODULATOR */
#if !defined(RX_APSK_DEMODULATOR)
#define RX_APSK_DEMODULATOR			(ENABLED & RECEIVER)
#endif /* RX_APSK_DEMODULATOR */

#endif /* V22_DEMO */
	
	/************************************/
	/**** configuration for V23 demo ****/
	/************************************/
				  
#if defined(V23_DEMO)

#include "rxtx.h"
#include "fsk.h"
#include "v23.h"
#if !defined(TX_BLOCK_LEN)
#define TX_BLOCK_LEN				(sizeof(struct TX_V23_BLOCK)/sizeof(short))
#elif TX_BLOCK_LEN == 0
#undef TX_BLOCK_LEN
#endif /* TX_BLOCK_LEN */
#if !defined(TX_SAMPLE_LEN)
#define TX_SAMPLE_LEN				41
#endif /* TX_SAMPLE_LEN */
#if !defined(TX_DATA_LEN)
#define TX_DATA_LEN					15
#endif /* TX_DATA_LEN */

#if !defined(RX_BLOCK_LEN)
#define RX_BLOCK_LEN				(sizeof(struct RX_V23_BLOCK)/sizeof(short))
#elif RX_BLOCK_LEN == 0
#undef RX_BLOCK_LEN
#endif /* RX_BLOCK_LEN */
#if !defined(RX_SAMPLE_LEN)
#define RX_SAMPLE_LEN				127
#endif /* RX_SAMPLE_LEN */
#if !defined(RX_DATA_LEN)
#define RX_DATA_LEN					15
#endif /* RX_DATA_LEN */

#if !defined(TX_V23_MODEM)
#define TX_V23_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V23_MODEM */
#if !defined(RX_V23_MODEM)
#define RX_V23_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V23_MODEM */
#if !defined(TX_BELL202_MODEM)
#define TX_BELL202_MODEM			(ENABLED & TRANSMITTER)
#endif /* TX_BELL202_MODEM */
#if !defined(RX_BELL202_MODEM)
#define RX_BELL202_MODEM			(ENABLED & RECEIVER)
#endif /* RX_BELL202_MODEM */

#if !defined(TX_FSK_MODULATOR)
#define TX_FSK_MODULATOR			(ENABLED & TRANSMITTER)
#endif /* TX_FSK_MODULATOR */
#if !defined(RX_FSK_DEMODULATOR)
#define RX_FSK_DEMODULATOR			(ENABLED & RECEIVER)
#endif /* RX_FSK_MODULATOR */

#endif /* V23_DEMO */
		
	/************************************/
	/**** configuration for V26 demo ****/
	/************************************/
				  
#if defined(V26_DEMO)

#include "rxtx.h"
#include "apsk.h"
#include "v26.h"
#if !defined(TX_BLOCK_LEN)
#define TX_BLOCK_LEN				(sizeof(struct TX_V26_BLOCK)/sizeof(short))
#elif TX_BLOCK_LEN == 0
#undef TX_BLOCK_LEN
#endif /* TX_BLOCK_LEN */
#if !defined(TX_SAMPLE_LEN)
#define TX_SAMPLE_LEN 				511
#endif /* TX_SAMPLE_LEN */
#if !defined(TX_DATA_LEN)
#define TX_DATA_LEN 				15
#endif /* TX_DATA_LEN */
#if !defined(TX_FIR_LEN)
#define TX_FIR_LEN 					12
#endif /* TX_FIR_LEN */

#if !defined(RX_BLOCK_LEN)
#define RX_BLOCK_LEN				(sizeof(struct RX_V26_BLOCK)/sizeof(short))
#elif RX_BLOCK_LEN == 0
#undef RX_BLOCK_LEN
#endif /* RX_BLOCK_LEN */
#if !defined(RX_SAMPLE_LEN)
#define RX_SAMPLE_LEN 				127	
#endif /* RX_SAMPLE_LEN */
#if !defined(RX_DATA_LEN)
#define RX_DATA_LEN 				15
#endif /* RX_DATA_LEN */
#if !defined(RX_FIR_LEN)
#define RX_FIR_LEN 					(2*64)
#endif /* RX_FIR_LEN */
#if !defined(EQ_COEF_LEN)
#define EQ_COEF_LEN 				(2*V26_EQ_LEN)
#endif /* EQ_COEF_LEN */
#if !defined(NEC_COEF_LEN)
#define NEC_COEF_LEN 				V26_EC_LEN
#endif /* NEC_COEF_LEN */
#if !defined(FEC_COEF_LEN)
#define FEC_COEF_LEN 				NEC_COEF_LEN
#endif /* FEC_COEF_LEN */
#if !defined(EC_COEF_LEN)
#define EC_COEF_LEN 				(NEC_COEF_LEN+FEC_COEF_LEN)
#endif /* EC_COEF_LEN */

#if !defined(RX_TIMING1200_TABLE)
#define RX_TIMING1200_TABLE			ENABLED
#endif /* RX_TIMING1200_TABLE */       	

#if !defined(RCOS1200_F1800_TABLE)
#define RCOS1200_F1800_TABLE	   	ENABLED
#endif /* RCOS1200_F1800_TABLE */

#if !defined(TX_V26_MODEM)
#define TX_V26_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V26_MODEM */
#if !defined(RX_V26_MODEM)
#define RX_V26_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V26_MODEM */
#if !defined(TX_APSK_MODULATOR)
#define TX_APSK_MODULATOR			(ENABLED & TRANSMITTER)
#endif /* TX_APSK_MODULATOR */
#if !defined(RX_APSK_DEMODULATOR)
#define RX_APSK_DEMODULATOR			(ENABLED & RECEIVER)
#endif /* RX_APSK_DEMODULATOR */
#if !defined(ECHO_CANCELLER)
#define ECHO_CANCELLER				(ENABLED & TRANSMITTER & RECEIVER)
#endif /* ECHO_CANCELLER */

#endif /* V26_DEMO */
		
	/************************************/
	/**** configuration for V27 demo ****/
	/************************************/
				  
#if defined(V27_DEMO)

#include "rxtx.h"
#include "apsk.h"
#include "v27.h"
#if !defined(TX_BLOCK_LEN)
#define TX_BLOCK_LEN				(sizeof(struct TX_V27_BLOCK)/sizeof(short))
#elif TX_BLOCK_LEN == 0
#undef TX_BLOCK_LEN
#endif /* TX_BLOCK_LEN */
#if !defined(TX_SAMPLE_LEN)
#define TX_SAMPLE_LEN				41
#endif /* TX_SAMPLE_LEN */
#if !defined(TX_DATA_LEN)
#define TX_DATA_LEN					7
#endif /* TX_DATA_LEN */
#if !defined(TX_FIR_LEN)
#define TX_FIR_LEN					12
#endif /* TX_FIR_LEN */

#if !defined(RX_BLOCK_LEN)
#define RX_BLOCK_LEN				(sizeof(struct RX_V27_BLOCK)/sizeof(short))
#elif RX_BLOCK_LEN == 0
#undef RX_BLOCK_LEN
#endif /* RX_BLOCK_LEN */
#if !defined(RX_SAMPLE_LEN)
#define RX_SAMPLE_LEN				127
#endif /* RX_SAMPLE_LEN */
#if !defined(RX_DATA_LEN)
#define RX_DATA_LEN					7
#endif /* RX_DATA_LEN */
#if !defined(RX_FIR_LEN)
#define RX_FIR_LEN					(2*64)
#endif /* RX_FIR_LEN */
#if !defined(EQ_COEF_LEN)
#define EQ_COEF_LEN					(2*64)
#endif /* EQ_COEF_LEN */

#if !defined(RX_TIMING1200_TABLE)
#define RX_TIMING1200_TABLE			ENABLED
#endif /* RX_TIMING1200_TABLE */       	
#if !defined(RX_TIMING1600_TABLE)
#define RX_TIMING1600_TABLE			ENABLED
#endif /* RX_TIMING1600_TABLE */       	

#if !defined(RCOS1200_F1800_TABLE)
#define RCOS1200_F1800_TABLE	   	ENABLED
#endif /* RCOS1200_F1800_TABLE */
#if !defined(RCOS1600_F1800_TABLE)
#define RCOS1600_F1800_TABLE		ENABLED
#endif /* RCOS1600_F1800_TABLE */

#if !defined(TX_V27_2400_MODEM) && !defined(TX_V27_MODEM)
#define TX_V27_2400_MODEM			(ENABLED & TRANSMITTER)
#endif /* TX_V27_2400_MODEM */
#if !defined(TX_V27_4800_MODEM) && !defined(TX_V27_MODEM)
#define TX_V27_4800_MODEM			(ENABLED & TRANSMITTER)
#endif /* TX_V27_4800_MODEM */
#if !defined(TX_V27_MODEM)
#define TX_V27_MODEM				(TX_V27_2400_MODEM | TX_V27_4800_MODEM)
#endif /* TX_V27_MODEM */
#if !defined(RX_V27_2400_MODEM) && !defined(RX_V27_MODEM)
#define RX_V27_2400_MODEM			(ENABLED & RECEIVER)
#endif /* RX_V27_2400_MODEM */
#if !defined(RX_V27_4800_MODEM) && !defined(RX_V27_MODEM)
#define RX_V27_4800_MODEM			(ENABLED & RECEIVER)
#endif /* RX_V27_4800_MODEM */
#if !defined(RX_V27_MODEM)
#define RX_V27_MODEM				(RX_V27_2400_MODEM | RX_V27_4800_MODEM)
#endif /* RX_V27_MODEM */
		
#if !defined(TEP_1800_TONE)
#define TEP_1800_TONE				ENABLED
#endif /* TEP_1800_TONE */
			   
#if !defined(TX_APSK_MODULATOR)
#define TX_APSK_MODULATOR			(ENABLED & TRANSMITTER)
#endif /* TX_APSK_MODULATOR */
#if !defined(RX_APSK_DEMODULATOR)
#define RX_APSK_DEMODULATOR			(ENABLED & RECEIVER)
#endif /* RX_APSK_DEMODULATOR */

#endif /* V27_DEMO */
		 
	/************************************/
	/**** configuration for V29 demo ****/
	/************************************/
				  
#if defined(V29_DEMO)

#include "rxtx.h"
#include "apsk.h"
#include "v29.h"
#if !defined(TX_BLOCK_LEN)
#define TX_BLOCK_LEN				(sizeof(struct TX_V29_BLOCK)/sizeof(short))
#elif TX_BLOCK_LEN == 0
#undef TX_BLOCK_LEN
#endif /* TX_BLOCK_LEN */
#if !defined(TX_SAMPLE_LEN)
#define TX_SAMPLE_LEN				41
#endif /* TX_SAMPLE_LEN */
#if !defined(TX_DATA_LEN)
#define TX_DATA_LEN					15
#endif /* TX_DATA_LEN */
#if !defined(TX_FIR_LEN)
#define TX_FIR_LEN					12
#endif /* TX_FIR_LEN */
#if !defined(ENCODER_BLOCK_LEN)
#define ENCODER_BLOCK_LEN			1
#endif /* ENCODER_BLOCK_LEN */

#if !defined(RX_BLOCK_LEN)
#define RX_BLOCK_LEN				(sizeof(struct RX_V29_BLOCK)/sizeof(short))
#elif RX_BLOCK_LEN == 0
#undef RX_BLOCK_LEN
#endif /* RX_BLOCK_LEN */
#if !defined(RX_SAMPLE_LEN)
#define RX_SAMPLE_LEN				127
#endif /* RX_SAMPLE_LEN */
#if !defined(RX_DATA_LEN)
#define RX_DATA_LEN					15
#endif /* RX_DATA_LEN */
#if !defined(RX_FIR_LEN)
#define RX_FIR_LEN					(2*64)
#endif /* RX_FIR_LEN */
#if !defined(EQ_COEF_LEN)
#define EQ_COEF_LEN					(2*64)
#endif /* EQ_COEF_LEN */

#if !defined(RX_TIMING2400_TABLE)
#define RX_TIMING2400_TABLE			ENABLED
#endif /* RX_TIMING2400_TABLE */       	

#if !defined(RCOS2400_F1800_TABLE)
#define RCOS2400_F1800_TABLE		ENABLED
#endif /* RCOS2400_F1800_TABLE */

#if !defined(TX_V29_MODEM)
#define TX_V29_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V29_MODEM */
#if !defined(RX_V29_MODEM)
#define RX_V29_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V29_MODEM */
				 
#if !defined(TEP_1700_TONE)
#define TEP_1700_TONE				ENABLED
#endif /* TEP_1700_TONE */
			   
#if !defined(TX_APSK_MODULATOR)
#define TX_APSK_MODULATOR			(ENABLED & TRANSMITTER)
#endif /* TX_APSK_MODULATOR */
#if !defined(RX_APSK_DEMODULATOR)
#define RX_APSK_DEMODULATOR			(ENABLED & RECEIVER)
#endif /* RX_APSK_DEMODULATOR */

#endif /* V29_DEMO */
    
	/************************************/
	/**** configuration for V32 demo ****/
	/************************************/
				  
#if defined(V32_DEMO)

#include "rxtx.h"
#include "apsk.h"
#include "v32.h"
#include "tcm.h"
#if !defined(TX_BLOCK_LEN)
#define TX_BLOCK_LEN				(sizeof(struct TX_V32_BLOCK)/sizeof(short))
#elif TX_BLOCK_LEN == 0
#undef TX_BLOCK_LEN
#endif /* TX_BLOCK_LEN */
#if !defined(TX_SAMPLE_LEN)
#define TX_SAMPLE_LEN 				511
#endif /* TX_SAMPLE_LEN */
#if !defined(TX_DATA_LEN)
#define TX_DATA_LEN 				15
#endif /* TX_DATA_LEN */
#if !defined(TX_FIR_LEN)
#define TX_FIR_LEN 					12
#endif /* TX_FIR_LEN */

#if !defined(RX_BLOCK_LEN)
#define RX_BLOCK_LEN				(sizeof(struct RX_V32_BLOCK)/sizeof(short))
#elif RX_BLOCK_LEN == 0
#undef RX_BLOCK_LEN
#endif /* RX_BLOCK_LEN */
#if !defined(RX_SAMPLE_LEN)
#define RX_SAMPLE_LEN 				127	
#endif /* RX_SAMPLE_LEN */
#if !defined(RX_DATA_LEN)
#define RX_DATA_LEN 				15
#endif /* RX_DATA_LEN */
#if !defined(RX_FIR_LEN)
#define RX_FIR_LEN 					(2*64)
#endif /* RX_FIR_LEN */
#if !defined(EQ_COEF_LEN)
#define EQ_COEF_LEN 				RX_FIR_LEN
#endif /* EQ_COEF_LEN */
#if !defined(NEC_COEF_LEN)
#define NEC_COEF_LEN 				160
#endif /* NEC_COEF_LEN */
#if !defined(FEC_COEF_LEN)
#define FEC_COEF_LEN 				NEC_COEF_LEN
#endif /* FEC_COEF_LEN */
#if !defined(EC_COEF_LEN)
#define EC_COEF_LEN 				(NEC_COEF_LEN+FEC_COEF_LEN)
#endif /* EC_COEF_LEN */
#if !defined(DECODER_BLOCK_LEN)
#define DECODER_BLOCK_LEN			(sizeof(struct TCM8_DECODER_BLOCK)/sizeof(short))
#elif DECODER_BLOCK_LEN == 0
#undef DECODER_BLOCK_LEN
#endif /* DECODER_BLOCK_LEN */
#if !defined(TRACEBACK_LEN)
#define TRACEBACK_LEN	  			(TCM8_TRACEBACK_LEN*TCM8_DELAY_STATES)
#endif /* TRACEBACK_LEN */

#if !defined(RX_TIMING2400_TABLE)
#define RX_TIMING2400_TABLE			ENABLED
#endif /* RX_TIMING2400_TABLE */       	

#if !defined(RCOS2400_F1800_TABLE)
#define RCOS2400_F1800_TABLE		ENABLED
#endif /* RCOS2400_F1800_TABLE */

#if !defined(TX_V32A_MODEM) && !defined(TX_V32_MODEM)
#define TX_V32A_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V32A_MODEM */
#if !defined(TX_V32C_MODEM) && !defined(TX_V32_MODEM)
#define TX_V32C_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V32C_MODEM */
#if !defined(TX_V32_MODEM)
#define TX_V32_MODEM				(TX_V32A_MODEM | TX_V32C_MODEM)
#endif /* TX_V32_MODEM */
#if !defined(RX_V32A_MODEM) && !defined(RX_V32_MODEM)
#define RX_V32A_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V32A_MODEM */
#if !defined(RX_V32C_MODEM) && !defined(RX_V32_MODEM)
#define RX_V32C_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V32C_MODEM */
#if !defined(RX_V32_MODEM)
#define RX_V32_MODEM				(RX_V32A_MODEM | RX_V32C_MODEM)
#endif /* RX_V32_MODEM */

#if !defined(TCM_7200)
#define TCM_7200					ENABLED 
#endif /* TCM_7200 */
#if !defined(TCM_9600)
#define TCM_9600					ENABLED 
#endif /* TCM_9600 */
#if !defined(TCM_12000)
#define TCM_12000					ENABLED 
#endif /* TCM_12000 */
#if !defined(TCM_14400)
#define TCM_14400					ENABLED 
#endif /* TCM_14400 */
#define TCM_ENCODER					((TCM_7200|TCM_9600|TCM_12000|TCM_14400) \
									& TRANSMITTER)
#define TCM_DECODER                 ((TCM_7200|TCM_9600|TCM_12000|TCM_14400) \
									& RECEIVER)
		   
#if !defined(CED_TONE)
#define CED_TONE					ENABLED
#endif /* CED_TONE */
#if !defined(RX_CALL_PROGRESS)
#define RX_CALL_PROGRESS			(ENABLED & RECEIVER)
#endif /* RX_CALL_PROGRESS */
#if !defined(TX_DTMF)
#define TX_DTMF						(ENABLED & TRANSMITTER)
#endif /* TX_DTMF */
							  
#if !defined(TX_APSK_MODULATOR)
#define TX_APSK_MODULATOR			(ENABLED & TRANSMITTER)
#endif /* TX_APSK_MODULATOR */
#if !defined(RX_APSK_DEMODULATOR)
#define RX_APSK_DEMODULATOR			(ENABLED & RECEIVER)
#endif /* RX_APSK_DEMODULATOR */
#if !defined(ECHO_CANCELLER)
#define ECHO_CANCELLER				(ENABLED & TRANSMITTER & RECEIVER)
#endif /* ECHO_CANCELLER */

#endif /* V32_DEMO */

	/*****************************************************/
	/**** User-defined vector buffer lengths for v.34 ****/
	/*****************************************************/

#if defined(V34_DEMO)
#undef VSIM_DEMO
				   
#if defined(V34_DEMO2)
#include <stdio.h>
extern struct RX_V34_BLOCK *RxA,*RxC;
extern void dump_write(short);
#endif /* V34_DEMO2 */
				   
#include "rxtx.h"
#include "apsk.h"
#include "v34.h"
#if !defined(TX_BLOCK_LEN)
#define TX_BLOCK_LEN				(sizeof(struct TX_V34_BLOCK)/sizeof(short))
#elif TX_BLOCK_LEN == 0
#undef TX_BLOCK_LEN
#endif /* TX_BLOCK_LEN */
#if !defined TX_SAMPLE_LEN
#define TX_SAMPLE_LEN 				511
#endif /* TX_SAMPLE_LEN */
#if !defined TX_DATA_LEN
#define TX_DATA_LEN 				31 
#endif /* TX_DATA_LEN */
#if !defined TX_FIR_LEN
#define TX_FIR_LEN					(2*63)	// larger than standard - be sure DSP matches 
											// can be as small as 62 (or 2*31):
											//	2*TX_TAPS_MAX + 2*SIZEOF(struct PRECODER_BLOCK)
#endif /* TX_FIR_LEN */
#if !defined(ENCODER_BLOCK_LEN)
#define ENCODER_BLOCK_LEN			(sizeof(struct V34_ENCODER_BLOCK)/sizeof(short))
#elif ENCODER_BLOCK_LEN == 0
#undef ENCODER_BLOCK_LEN
#endif /* ENCODER_BLOCK_LEN */

#if !defined(RX_BLOCK_LEN)
#define RX_BLOCK_LEN				(sizeof(struct RX_V34_BLOCK)/sizeof(short))
#elif RX_BLOCK_LEN == 0
#undef RX_BLOCK_LEN
#endif /* RX_BLOCK_LEN */
#if !defined RX_SAMPLE_LEN
#define RX_SAMPLE_LEN 				256		/* required by v34p2 */
#endif /* RX_SAMPLE_LEN */
#if !defined RX_DATA_LEN
#define RX_DATA_LEN 				31
#endif /* RX_DATA_LEN */
#if !defined RX_FIR_LEN
#define RX_FIR_LEN 					(2*(V34_EQ_LEN \
									+POLYPHASE_BUFFER_LEN \
									+IQ_DELAY_BUFFER_LEN \
									+INV_PRECODER_BLOCK_LEN))
#endif /* RX_FIR_LEN */
#if !defined EQ_COEF_LEN
#define EQ_COEF_LEN 				(2*V34_EQ_LEN \
									+2*V34_PRECODER_FIR_LEN \
									+2*V34_PRECODER_FIR_LEN) // add 3 for Tx precoder_h, 3 for Rx precoder_H
#endif /* EQ_COEF_LEN */
#if !defined NEC_COEF_LEN
#define NEC_COEF_LEN 				160
#endif /* NEC_COEF_LEN */
#if !defined FEC_COEF_LEN
#define FEC_COEF_LEN 				NEC_COEF_LEN
#endif /* FEC_COEF_LEN */
#if !defined(EC_COEF_LEN)
#define EC_COEF_LEN 				(NEC_COEF_LEN+FEC_COEF_LEN)
#endif /* EC_COEF_LEN */
#if !defined(DECODER_BLOCK_LEN)
#define DECODER_BLOCK_LEN			(sizeof(struct V34_DECODER_BLOCK)/sizeof(short))
#elif DECODER_BLOCK_LEN == 0
#undef DECODER_BLOCK_LEN
#endif /* DECODER_BLOCK_LEN */
#if !defined(TRACEBACK_LEN)
#define TRACEBACK_LEN 				0
#endif /* TRACEBACK_LEN */

#if !defined(RCOS3429_F1959_TABLE)
#define RCOS3429_F1959_TABLE
#endif /* RCOS3429_F1959_TABLE */

#if !defined(TX_V8_MODEM)
#define TX_V8_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V8_MODEM */
#if !defined(RX_V8_MODEM)
#define RX_V8_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V8_MODEM */

#if !defined(V34_SR2400)
#define V34_SR2400					ENABLED
#endif /* V34_SR2400 */
#if !defined(V34_SR2743)
#define V34_SR2743					DISABLED
#endif /* V34_SR2743 */
#if !defined(V34_SR2800)
#define V34_SR2800					DISABLED
#endif /* V34_SR2800 */
#if !defined(V34_SR3000)
#define V34_SR3000					ENABLED
#endif /* V34_SR3000 */
#if !defined(V34_SR3200)
#define V34_SR3200					ENABLED
#endif /* V34_SR3200 */
#if !defined(V34_SR3429)
#define V34_SR3429					ENABLED
#endif /* V34_SR3429 */

#define TX_V34_MODEM				((V34_SR2400|V34_SR2743|V34_SR2800|V34_SR3000|V34_SR3200|V34_SR3429) \
									& TRANSMITTER)

#define RX_V34_MODEM				((V34_SR2400|V34_SR2743|V34_SR2800|V34_SR3000|V34_SR3200|V34_SR3429) \
									& RECEIVER)
#if !defined(V34_16STATE_TCM)
#define V34_16STATE_TCM				ENABLED
#endif /* V34_16STATE_TCM */
#if !defined(V34_32STATE_TCM)
#define V34_32STATE_TCM				DISABLED
#endif /* V34_32STATE_TCM */
#if !defined(V34_64STATE_TCM)
#define V34_64STATE_TCM				DISABLED
#endif /* V34_64STATE_TCM */

#if !defined(CED_TONE)
#define CED_TONE					ENABLED
#endif /* CED_TONE */
#if !defined(RX_CALL_PROGRESS)
#define RX_CALL_PROGRESS			(ENABLED & RECEIVER)
#endif /* RX_CALL_PROGRESS */
#if !defined(TX_DTMF)
#define TX_DTMF						(ENABLED & TRANSMITTER)
#endif /* TX_DTMF */

#if !defined(TX_APSK_MODULATOR)
#define TX_APSK_MODULATOR			(ENABLED & TRANSMITTER)
#endif /* TX_APSK_MODULATOR */
#if !defined(RX_APSK_DEMODULATOR)
#define RX_APSK_DEMODULATOR			(ENABLED & RECEIVER)
#endif /* RX_APSK_DEMODULATOR */
#if !defined(ECHO_CANCELLER)
#define ECHO_CANCELLER				(ENABLED & TRANSMITTER & RECEIVER)
#endif /* ECHO_CANCELLER */

#endif /* V34_DEMO */
											  
	/****************************/
	/**** vmodem demo config ****/
	/****************************/
						
#if defined(VMODEM_DEMO)

#include "rxtx.h"
#include "apsk.h"
#include "v27.h"
#if !defined(TX_BLOCK_LEN)
#define TX_BLOCK_LEN				(sizeof(struct TX_V27_BLOCK)/sizeof(short))
#elif TX_BLOCK_LEN == 0
#undef TX_BLOCK_LEN
#endif /* TX_BLOCK_LEN */
#if !defined(TX_SAMPLE_LEN)
#define TX_SAMPLE_LEN				41
#endif /* TX_SAMPLE_LEN */
#if !defined(TX_DATA_LEN)
#define TX_DATA_LEN					7
#endif /* TX_DATA_LEN */
#if !defined(TX_FIR_LEN)
#define TX_FIR_LEN					12
#endif /* TX_FIR_LEN */
#if !defined(RX_BLOCK_LEN)
#define RX_BLOCK_LEN				(sizeof(struct RX_V27_BLOCK)/sizeof(short))
#elif RX_BLOCK_LEN == 0
#undef RX_BLOCK_LEN
#endif /* RX_BLOCK_LEN */
#if !defined(RX_SAMPLE_LEN)
#define RX_SAMPLE_LEN				127
#endif /* RX_SAMPLE_LEN */
#if !defined(RX_DATA_LEN)
#define RX_DATA_LEN					7
#endif /* RX_DATA_LEN */
#if !defined(RX_FIR_LEN)
#define RX_FIR_LEN					(2*64)
#endif /* RX_FIR_LEN */
#if !defined(EQ_COEF_LEN)
#define EQ_COEF_LEN					(2*64)
#endif /* EQ_COEF_LEN */

#if !defined(RX_TIMING1200_TABLE)
#define RX_TIMING1200_TABLE			ENABLED
#endif /* RX_TIMING1200_TABLE */       	
#if !defined(RX_TIMING1600_TABLE)
#define RX_TIMING1600_TABLE			ENABLED
#endif /* RX_TIMING1600_TABLE */       	

#if !defined(RCOS1200_F1800_TABLE)
#define RCOS1200_F1800_TABLE	   	ENABLED
#endif /* RCOS1200_F1800_TABLE */
#if !defined(RCOS1600_F1800_TABLE)
#define RCOS1600_F1800_TABLE		ENABLED
#endif /* RCOS1600_F1800_TABLE */

#if !defined(TX_V27_2400_MODEM) && !defined(TX_V27_MODEM)
#define TX_V27_2400_MODEM			(ENABLED & TRANSMITTER)
#endif /* TX_V27_2400_MODEM */
#if !defined(TX_V27_4800_MODEM) && !defined(TX_V27_MODEM)
#define TX_V27_4800_MODEM			(ENABLED & TRANSMITTER)
#endif /* TX_V27_4800_MODEM */
#if !defined(TX_V27_MODEM)
#define TX_V27_MODEM				(TX_V27_2400_MODEM | TX_V27_4800_MODEM)
#endif /* TX_V27_MODEM */
#if !defined(RX_V27_2400_MODEM) && !defined(RX_V27_MODEM)
#define RX_V27_2400_MODEM			(ENABLED & RECEIVER)
#endif /* RX_V27_2400_MODEM */
#if !defined(RX_V27_4800_MODEM) && !defined(RX_V27_MODEM)
#define RX_V27_4800_MODEM			(ENABLED & RECEIVER)
#endif /* RX_V27_4800_MODEM */
#if !defined(RX_V27_MODEM)
#define RX_V27_MODEM				(RX_V27_2400_MODEM | RX_V27_4800_MODEM)
#endif /* RX_V27_MODEM */

#if !defined(CED_TONE)
#define CED_TONE					ENABLED
#endif /* CED_TONE */
#if !defined(TX_APSK_MODULATOR)
#define TX_APSK_MODULATOR			(ENABLED & TRANSMITTER)
#endif /* TX_APSK_MODULATOR */
#if !defined(RX_APSK_DEMODULATOR)
#define RX_APSK_DEMODULATOR			(ENABLED & RECEIVER)
#endif /* RX_APSK_DEMODULATOR */

#endif /* VMODEM_DEMO */

	/********************************************************************/
	/**** User-defined vector buffer lengths for VSIM_DEMO (default) ****/
	/********************************************************************/

#if defined(VSIM_DEMO)

#include "rxtx.h"
#include "apsk.h"
#include "v32.h"
#include "tcm.h"
#if !defined(TX_BLOCK_LEN)
#define TX_BLOCK_LEN				(sizeof(struct TX_V32_BLOCK)/sizeof(short))
#elif TX_BLOCK_LEN == 0
#undef TX_BLOCK_LEN
#endif /* TX_BLOCK_LEN */

#if !defined(TX_SAMPLE_LEN)
#define TX_SAMPLE_LEN 				2047
#endif /* TX_SAMPLE_LEN */
#if !defined(TX_DATA_LEN)
#define TX_DATA_LEN 				32 
#endif /* TX_DATA_LEN */
#if !defined(TX_FIR_LEN)
#define TX_FIR_LEN 					(2*64) 
#endif /* TX_FIR_LEN */
#if !defined(ENCODER_BLOCK_LEN)
#define ENCODER_BLOCK_LEN			1
#endif /* ENCODER_BLOCK_LEN */

#if !defined(RX_BLOCK_LEN)
#define RX_BLOCK_LEN				(sizeof(struct RX_V32_BLOCK)/sizeof(short))
#elif RX_BLOCK_LEN == 0
#undef RX_BLOCK_LEN
#endif /* RX_BLOCK_LEN */
#if !defined(RX_SAMPLE_LEN)
#define RX_SAMPLE_LEN 				256
#endif /* RX_SAMPLE_LEN */
#if !defined(RX_DATA_LEN)
#define RX_DATA_LEN 				32
#endif /* RX_DATA_LEN */
#if !defined(RX_FIR_LEN)
#define RX_FIR_LEN 		(EQ_COEF_LEN+2*(POLYPHASE_BUFFER_LEN+IQ_DELAY_BUFFER_LEN))
#endif /* RX_FIR_LEN */
#if !defined(EQ_COEF_LEN)
#define EQ_COEF_LEN 	(2*64)
#endif /* EQ_COEF_LEN */
#if !defined(NEC_COEF_LEN)
#define NEC_COEF_LEN 				160
#endif /* NEC_COEF_LEN */
#if !defined(FEC_COEF_LEN)
#define FEC_COEF_LEN 				NEC_COEF_LEN
#endif /* FEC_COEF_LEN */
#if !defined(EC_COEF_LEN)
#define EC_COEF_LEN 				(NEC_COEF_LEN+FEC_COEF_LEN)
#endif /* EC_COEF_LEN */
#if !defined(DECODER_BLOCK_LEN)
#define DECODER_BLOCK_LEN			(sizeof(struct TCM8_DECODER_BLOCK)/sizeof(short))
#elif DECODER_BLOCK_LEN == 0
#undef DECODER_BLOCK_LEN
#endif /* DECODER_BLOCK_LEN */
#if !defined(TRACEBACK_LEN)
#define TRACEBACK_LEN	  			(TCM8_TRACEBACK_LEN*TCM8_DELAY_STATES)
#endif /* TRACEBACK_LEN */

#if !defined(RX_TIMING600_TABLE)
#define RX_TIMING600_TABLE			ENABLED
#endif /* RX_TIMING600_TABLE */
#if !defined(RX_TIMING1200_TABLE)
#define RX_TIMING1200_TABLE			ENABLED
#endif /* RX_TIMING1200_TABLE */       	
#if !defined(RX_TIMING1600_TABLE)
#define RX_TIMING1600_TABLE			ENABLED
#endif /* RX_TIMING1600_TABLE */       	
#if !defined(RX_TIMING2400_TABLE)
#define RX_TIMING2400_TABLE			ENABLED
#endif /* RX_TIMING2400_TABLE */ 

#if !defined(TX_RCOS600_F1200_TABLE)
#define TX_RCOS600_F1200_TABLE		ENABLED
#endif /* TX_RCOS600_F1200_TABLE */
#if !defined(TX_RCOS600_F2400_TABLE)
#define TX_RCOS600_F2400_TABLE		ENABLED
#endif /* TX_RCOS600_F2400_TABLE */
#if !defined(RX_RCOS600_F1200_TABLE)
#define RX_RCOS600_F1200_TABLE		ENABLED
#endif /* RX_RCOS600_F1200_TABLE */
#if !defined(RX_RCOS600_F2400_TABLE)
#define RX_RCOS600_F2400_TABLE		ENABLED
#endif /* RX_RCOS600_F2400_TABLE */
#if !defined(RCOS1200_F1800_TABLE)
#define RCOS1200_F1800_TABLE	   	ENABLED
#endif /* RCOS1200_F1800_TABLE */
#if !defined(RCOS1600_F1800_TABLE)
#define RCOS1600_F1800_TABLE		ENABLED
#endif /* RCOS1600_F1800_TABLE */
#if !defined(RCOS2400_F1800_TABLE)
#define RCOS2400_F1800_TABLE		ENABLED
#endif /* RCOS2400_F1800_TABLE */

#if !defined(TX_ACOUSTIC_ECHO_CANCELLER) && !defined(ACOUSTIC_ECHO_CANCELLER)
#define TX_ACOUSTIC_ECHO_CANCELLER	ENABLED
#endif /* TX_ACOUSTIC_ECHO_CANCELLER */
#if !defined(RX_ACOUSTIC_ECHO_CANCELLER) && !defined(ACOUSTIC_ECHO_CANCELLER)
#define RX_ACOUSTIC_ECHO_CANCELLER	ENABLED
#endif /* RX_ACOUSTIC_ECHO_CANCELLER */
#if !defined(ACOUSTIC_ECHO_CANCELLER)
#define ACOUSTIC_ECHO_CANCELLER		(TX_ACOUSTIC_ECHO_CANCELLER|RX_ACOUSTIC_ECHO_CANCELLER)
#endif /* ACOUSTIC_ECHO_CANCELLER */

#if !defined(TX_BAUDOT_MODEM)
#define TX_BAUDOT_MODEM				ENABLED
#endif /* TX_BAUDOT_MODEM */
#if !defined(RX_BAUDOT_MODEM)       
#define RX_BAUDOT_MODEM				ENABLED
#endif /* RX_BAUDOT_MODEM */
#if !defined(BAUDOT_MODEM)
#define BAUDOT_MODEM				(TX_BAUDOT_MODEM|RX_BAUDOT_MODEM)
#endif /* BAUDOT_MODEM */

#if !defined(TX_V17_MODEM)
#define TX_V17_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V17_MODEM */
#if !defined(RX_V17_MODEM)
#define RX_V17_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V17_MODEM */

#if !defined(TX_V21_MODEM)
#define TX_V21_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V21_MODEM */
#if !defined(RX_V21_MODEM)
#define RX_V21_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V21_MODEM */
#if !defined(TX_BELL103_MODEM)
#define TX_BELL103_MODEM			(ENABLED & TRANSMITTER)
#endif /* TX_BELL103_MODEM */
#if !defined(RX_BELL103_MODEM)
#define RX_BELL103_MODEM			(ENABLED & RECEIVER)
#endif /* RX_BELL103_MODEM */

#if !defined(TX_V22A_MODEM) && !defined(TX_V22_MODEM)
#define TX_V22A_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V22A_MODEM */
#if !defined(TX_V22C_MODEM) && !defined(TX_V22_MODEM)
#define TX_V22C_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V22C_MODEM */
#if !defined(TX_V22_MODEM)
#define TX_V22_MODEM				(TX_V22A_MODEM | TX_V22C_MODEM)
#endif /* TX_V22_MODEM */
#if !defined(RX_V22A_MODEM) && !defined(RX_V22_MODEM)
#define RX_V22A_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V22A_MODEM */
#if !defined(RX_V22C_MODEM) && !defined(RX_V22_MODEM)
#define RX_V22C_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V22C_MODEM */
#if !defined(RX_V22_MODEM)
#define RX_V22_MODEM				(RX_V22A_MODEM | RX_V22C_MODEM)
#endif /* RX_V22_MODEM */

#if !defined(TX_V23_MODEM)
#define TX_V23_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V23_MODEM */
#if !defined(RX_V23_MODEM)
#define RX_V23_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V23_MODEM */
#if !defined(TX_BELL202_MODEM)
#define TX_BELL202_MODEM			(ENABLED & TRANSMITTER)
#endif /* TX_BELL202_MODEM */
#if !defined(RX_BELL202_MODEM)
#define RX_BELL202_MODEM			(ENABLED & RECEIVER)
#endif /* RX_BELL202_MODEM */
		   
#if !defined(TX_V26_MODEM)
#define TX_V26_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V26_MODEM */
#if !defined(RX_V26_MODEM)
#define RX_V26_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V26_MODEM */
   
#if !defined(TX_V27_2400_MODEM) && !defined(TX_V27_MODEM)
#define TX_V27_2400_MODEM			(ENABLED & TRANSMITTER)
#endif /* TX_V27_2400_MODEM */
#if !defined(TX_V27_4800_MODEM) && !defined(TX_V27_MODEM)
#define TX_V27_4800_MODEM			(ENABLED & TRANSMITTER)
#endif /* TX_V27_4800_MODEM */
#if !defined(TX_V27_MODEM)
#define TX_V27_MODEM				(TX_V27_2400_MODEM | TX_V27_4800_MODEM)
#endif /* TX_V27_MODEM */
#if !defined(RX_V27_2400_MODEM) && !defined(RX_V27_MODEM)
#define RX_V27_2400_MODEM			(ENABLED & RECEIVER)
#endif /* RX_V27_2400_MODEM */
#if !defined(RX_V27_4800_MODEM) && !defined(RX_V27_MODEM)
#define RX_V27_4800_MODEM			(ENABLED & RECEIVER)
#endif /* RX_V27_4800_MODEM */
#if !defined(RX_V27_MODEM)
#define RX_V27_MODEM				(RX_V27_2400_MODEM | RX_V27_4800_MODEM)
#endif /* RX_V27_MODEM */
		
#if !defined(TX_V29_MODEM)
#define TX_V29_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V29_MODEM */
#if !defined(RX_V29_MODEM)
#define RX_V29_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V29_MODEM */

#if !defined(TX_V32A_MODEM) && !defined(TX_V32_MODEM)
#define TX_V32A_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V32A_MODEM */
#if !defined(TX_V32C_MODEM) && !defined(TX_V32_MODEM)
#define TX_V32C_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_V32C_MODEM */
#if !defined(TX_V32_MODEM)
#define TX_V32_MODEM				(TX_V32A_MODEM | TX_V32C_MODEM)
#endif /* TX_V32_MODEM */
#if !defined(RX_V32A_MODEM) && !defined(RX_V32_MODEM)
#define RX_V32A_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V32A_MODEM */
#if !defined(RX_V32C_MODEM) && !defined(RX_V32_MODEM)
#define RX_V32C_MODEM				(ENABLED & RECEIVER)
#endif /* RX_V32C_MODEM */
#if !defined(RX_V32_MODEM)
#define RX_V32_MODEM				(RX_V32A_MODEM | RX_V32C_MODEM)
#endif /* RX_V32_MODEM */

#if !defined(TCM_7200)
#define TCM_7200					ENABLED 
#endif /* TCM_7200 */
#if !defined(TCM_9600)
#define TCM_9600					ENABLED 
#endif /* TCM_9600 */
#if !defined(TCM_12000)
#define TCM_12000					ENABLED 
#endif /* TCM_12000 */
#if !defined(TCM_14400)
#define TCM_14400					ENABLED 
#endif /* TCM_14400 */
#define TCM_ENCODER					((TCM_7200|TCM_9600|TCM_12000|TCM_14400) \
									& TRANSMITTER)
#define TCM_DECODER                 ((TCM_7200|TCM_9600|TCM_12000|TCM_14400) \
									& RECEIVER)
		   
#if !defined(BELL_2225_TONE)
#define BELL_2225_TONE				ENABLED
#endif /* BELL_2225_TONE */
#if !defined(CNG_TONE)
#define CNG_TONE					ENABLED
#endif /* CNG_TONE */
#if !defined(CED_TONE)
#define CED_TONE					ENABLED
#endif /* CED_TONE */
#if !defined(TEP_1700_TONE)
#define TEP_1700_TONE				ENABLED
#endif /* TEP_1700_TONE */
#if !defined(TEP_1800_TONE)
#define TEP_1800_TONE				ENABLED
#endif /* TEP_1800_TONE */
#if !defined(P1800_DETECTOR)
#define P1800_DETECTOR				ENABLED
#endif /* P1800_DETECTOR */
#if !defined(V32_AUTOMODE)
#define V32_AUTOMODE				DISABLED
#endif /* V32_AUTOMODE */
							 
#if !defined(TX_CALL_PROGRESS)
#define TX_CALL_PROGRESS			(ENABLED & TRANSMITTER)
#endif /* TX_CALL_PROGRESS */
#if !defined(RX_CALL_PROGRESS)
#define RX_CALL_PROGRESS			(ENABLED & RECEIVER)
#endif /* RX_CALL_PROGRESS */

#if !defined(TX_DTMF)
#define TX_DTMF						(ENABLED & TRANSMITTER)
#endif /* TX_DTMF */
#if !defined(RX_DTMF)
#define RX_DTMF						(ENABLED & RECEIVER)
#endif /* RX_DTMF */
#if !defined(TX_MF)
#define TX_MF						(ENABLED & TRANSMITTER)
#endif /* TX_MF */
#if !defined(RX_MF)
#define RX_MF						(ENABLED & RECEIVER)
#endif /* RX_MF */

#if !defined(TX_CID_MODEM)
#define TX_CID_MODEM				(ENABLED & TRANSMITTER)
#endif /* TX_CID_MODEM */
#if !defined(RX_CID_MODEM)
#define RX_CID_MODEM				(ENABLED & RECEIVER)
#endif /* RX_CID_MODEM */
#if !defined(TX_CAS)
#define TX_CAS						(ENABLED & TRANSMITTER)
#endif /* TX_CAS */
#if !defined(RX_CAS)
#define RX_CAS						(ENABLED & RECEIVER)
#endif /* RX_CAS */
#if !defined(TX_CIDJAPAN_MODEM)
#define TX_CIDJAPAN_MODEM			(ENABLED & TRANSMITTER)
#endif /* TX_CIDJAPAN_MODEM */
#if !defined(RX_CIDJAPAN_MODEM)
#define RX_CIDJAPAN_MODEM			(ENABLED & RECEIVER)
#endif /* RX_CIDJAPAN_MODEM */
#if !defined(TX_CIDDTMF_MODEM)
#define TX_CIDDTMF_MODEM			(ENABLED & TRANSMITTER)
#endif /* TX_CIDDTMF_MODEM */
#if !defined(RX_CIDDTMF_MODEM)
#define RX_CIDDTMF_MODEM			(ENABLED & RECEIVER)
#endif /* RX_CIDDTMF_MODEM */

#if !defined(TX_APSK_MODULATOR)
#define TX_APSK_MODULATOR			(ENABLED & TRANSMITTER)
#endif /* TX_APSK_MODULATOR */
#if !defined(RX_APSK_DEMODULATOR)
#define RX_APSK_DEMODULATOR			(ENABLED & RECEIVER)
#endif /* RX_APSK_DEMODULATOR */
#if !defined(ECHO_CANCELLER)
#define ECHO_CANCELLER				(ENABLED & TRANSMITTER & RECEIVER)
#endif /* ECHO_CANCELLER */

#endif /* VSIM_DEMO */

/****************************************************************************/

	/****************************/
	/**** global definitions ****/
	/****************************/
	
/****************************************************************************/
#if !defined(START_PTRS_LEN)
#define START_PTRS_LEN				(sizeof(struct START_PTRS)/sizeof(short))
#elif START_PTRS_LEN == 0
#undef START_PTRS_LEN
#endif /* START_PTRS_LEN */

/****************************************************************************/
#endif /* CONFIG_INCLUSION_ */

