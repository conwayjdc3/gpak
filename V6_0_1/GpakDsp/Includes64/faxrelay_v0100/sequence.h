/*************************************************************************
 * $RCSfile: sequence.h,v $
 * $Revision:   1.1  $
 * $Date:   Nov 29 2007 11:47:10  $
 * $Author:   rfuchs  $
 * Company: MESi
 * Description: Sequencer Code
 *                   
 * Copyright (C) MESi 1996-2006, all rights reserved.         
 ***************************************************************************/
#ifndef __SEQUENCE__
#define __SEQUENCE__

/*
* When changing the max values, check the bit field sizes in the structure
* below and adjust accordingly.
*/
#define MAX_STATE_MACHINE 		5
#define MAX_EVENTS 				128
#define LAST_ENTRY 				0x8000
#define ALLOW_USER_STATE 		0x4000
#define STATE_MACHINE_MASK 		0x3FFF
#define MAX_TIMERS 				16
#ifndef TRUE
#define TRUE (1==1)
#define FALSE !TRUE
#endif

typedef struct SeqStruct *SeqStructTypePointer;
typedef void (*PFI)(SeqStructTypePointer);

/* Event Table Struct */

struct ETS {
	int        event; 	// triggering event
	PFI        action;	// action routine to execute
	const struct ETS *nextstate; // next state to switch to
	};
/* state machine structure. One for each state machine */
struct sm{
	int ID;
	struct ETS *pCurrentState;
	};

struct tmr{
	int num;
	unsigned int value;
	};

	/**** Sequencer return codes ****/

#define SEQ_HEALTHY              	0x0000		// see sequence.c, relay.c
#define SEQ_BUF_POOL_EMPTY       	0x0001		// see bufmgr.c
#define SEQ_BUF_RETURN         		0x0002		// see bufmgr.c
#define SEQ_BUF_INTERNAL         	0x0004		// see bufmgr.c
#define SEQ_SM_FULL              	0x0008		// see sequence.c
#define SEQ_EVENT_OVERFLOW       	0x0010		// see sequence.c
#define SEQ_TIMER_OVERFLOW			0x0020 		// see sequence.c

struct SeqStruct{
    struct FaxStruct *FaxPtrs;
    unsigned int health;
	unsigned int MasterTimer;
	struct tmr Timer[MAX_TIMERS];
	struct sm StateMachine[MAX_STATE_MACHINE];
	unsigned int Events[MAX_EVENTS];
	unsigned int EventHead;
	unsigned int EventTail;
	unsigned int nTimers; // six bits used 
	unsigned int nStateMachines; // six bits used
	unsigned int ResidualSamples; // four bits used
	short *LastRxSampleHead;
	/* pRxSampleHead contains the address of the RxSampleHead pointer
	* This contains &START_PTRS->Rx_sample_block->sample_head
	*/
	short **pRxSampleHead;
	void *pBufMgr; 						/* pointer to BufStruct structure */
	struct ProtocolStruct *Protocol;	/* pointer to protocol structure */
#if defined(BUFLOG) | defined(SNOOP_CALL_LOG) | defined(NEED_REALTIME)
#if !defined(NEED_REALTIME)
#define NEED_REALTIME
#endif /* NEED_REALTIME */
	unsigned long RealTime;/* the count in ms since the call began */
#endif /* BUFLOG|SNOOP_CALL_LOG|NEED_REALTIME */
	short *DepBuffer;
    short *PartialIpFrame;
#ifdef INTERCEPT_SUPPORT
	int BitsPerBaud;
#endif /* INTERCEPT_SUPPORT */
#ifdef HOOK_SWITCH
	struct HookSwitchStruct *HookSwitch;
#endif
#ifdef _MSWINDOWS
	char DisplayState[80];
#endif /*_MSWINDOWS*/
    short AllowStateChange;
	};

extern void AddEvent(struct SeqStruct *q, unsigned int Event);
extern void DelEvent(struct SeqStruct *q, unsigned int Event);
extern int Sequencer(struct FaxStruct *f);
extern int AddStateMachine(struct SeqStruct *q, int StateID, struct ETS *StateTable);
extern int DelStateMachine(struct SeqStruct *q, int StateID);
extern int SeqTimeCheck(struct FaxStruct *f);
extern void StopTimer(struct SeqStruct *q, int TimeNum);
extern void StartTimer(struct SeqStruct *q, int TimeNum, unsigned int Nomst);
extern unsigned int ReadTimer(struct SeqStruct *q, int TimeNum);
extern void TimerTick(struct SeqStruct *q);
extern void InitSequencer(struct FaxStruct *);
extern void SeqBufDepEvent(struct SeqStruct *q,short *buf, int Event);
extern int SeqGetEventRemain(struct SeqStruct *q);

#define BlockStateChange(q) (((struct SeqStruct *)q)->AllowStateChange=FALSE)

/****************************************************************************/
#endif /* __SEQUENCE__ */

