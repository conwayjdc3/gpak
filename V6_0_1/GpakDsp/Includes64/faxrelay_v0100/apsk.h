/***************************************************************************
 * $RCSfile: apsk.h,v $
 * $Revision:   1.1  $
 * Revision $Author:   rfuchs  $
 * Revision $Date:   Nov 29 2007 11:46:58  $
 * Author of Origin: Peter B. Miller
 * Date of Origin: 11-12-2003
 *
 * Company: MESi
 * E-mail: infomaster@mesi.net
 * Website: www.mesi.net
 * Description: memory and symbol definitions for APSK modem modules.
 *
 * Copyright (C) MESi 1996-2006, all rights reserved.
 ***************************************************************************/

#if !defined(APSK_INCLUSION_)
#define APSK_INCLUSION_

	/**** APSK modulator common structure ****/

#define TX_APSK_MEMBERS \
	short interpolate; \
	short decimate; \
	short coef_ptr; \
	short *coef_start; \
	short *fir_head;	\
	short *fir_tail;	\
	short fir_len; \
	short fir_taps; \
	short sym_clk_offset; \
	short sym_clk_memory; \
	short sym_clk_phase; \
	unsigned short carrier; \
	short *map_ptr; \
	short *amp_ptr; \
	unsigned short phase; \
	unsigned short Sreg; \
	unsigned short Sreg_low; \
	short fir_scale; \
	unsigned short Ereg

struct TX_APSK_BLOCK
	{
	TX_CONTROL_MEMBERS;
	TX_APSK_MEMBERS;
	};

	/**** APSK demodulator common structures ****/

struct IIR_RESONATOR_BLOCK {
	short coef;
	short dnm1;
	short dnm2;
};

#define RX_APSK_MEMBERS \
	short (*decoder_ptr)(struct RX_BLOCK *); \
	short (*slicer_ptr)(struct RX_BLOCK *); \
	short (*timing_ptr)(struct RX_BLOCK *); \
	short baud_counter; \
	short *data_ptr; \
	short *sample_ptr;	/* This member not used */ \
	short fir_taps; \
	short *coef_start; \
	short coef_ptr; \
	short sym_clk_phase; \
	short interpolate; \
	short decimate; \
	short oversample;	/* This member not used */ \
	short *timing_start; /* This member not used */ \
	short I; \
	short Q; \
	short IEQ; \
	short QEQ; \
	short Iprime; \
	short Qprime; \
	short Inm1; \
	short Qnm1; \
	short Inm2; \
	short Qnm2; \
	short Inm3; \
	short Qnm3; \
	short Ihat; \
	short Qhat; \
	short Ihat_nm2; \
	short Qhat_nm2; \
unsigned short What; \
	short *fir_ptr; \
	short IEQprime_error; \
	short QEQprime_error; \
unsigned short EQ_MSE; \
	short EQ_2mu; \
	short COS; \
	short SIN; \
	unsigned short LO_memory; \
	unsigned short LO_frequency; \
	short LO_phase; \
	unsigned short vco_memory; \
	short phase_error; \
	short loop_memory; \
	short loop_memory_low; \
	short loop_K1; \
	short loop_K2; \
	struct IIR_RESONATOR_BLOCK PJ1; \
	struct IIR_RESONATOR_BLOCK PJ2; \
	short agc_gain; \
	short agc_K; \
	short frequency_est; \
	short sym_clk_memory; \
	short timing_threshold; \
	short coarse_error; \
	short LOS_counter; /* This member not used */  \
	unsigned short LOS_monitor; \
	short map_shift; \
	short Phat; \
	short phase; \
	short EQ_taps; \
	unsigned short Dreg; \
	unsigned short Dreg_low; \
	unsigned short pattern_reg; \
	short *trace_back_ptr; \
	short *signal_map_ptr; \
	short *EC_fir_ptr; \
	short *EC_sample_ptr; \
	short EC_2mu;	\
	short EC_MSE;	\
	short EC_taps; \
	short EC_shift; \
	short RTD

struct RX_APSK_BLOCK
	{
	RX_CONTROL_MEMBERS;
	RX_APSK_MEMBERS;
	};

	/**** APSK demodulator operating point, OP_POINT ****/

#define OP_POINT 			        8192	/* 32768*(1/4) */
#define OP_POINT_SHIFT		        2

	/**** RCOS filter coefficients ****/

/*
 *  RCOS COEFFICIENT LENGTH:  The RCOS filter coefficient length is calculated
 * based on the specified number of taps, interpolate and decimate rates,
 * oversample rate, and the extension for polyphase operation. A standard FIR
 * filter requires TAPS coefficients for the specified number of taps. An
 * interpolator/decimator structure requires TAPS*INTERP coefficients. To
 * implement a polyphase filter we oversample by OVERSAMPLE, and implement
 * a method for adjusting the sample phase coincident with filter coefficient
 * phase. For modulator filter, the polyphase span is one symbol, or the
 * decimation rate. For the demodulator (sample-rate to symbol rate), the
 * polyphase span is one sample, or the interpolate rate. The RCOS coefficients
 * are shared by modulator and demodulator for a given symbol rate, and
 * also for several symbol rates for some types of modems. The length set for
 * RCOS coefficients is fixed by the demodulator since those filters must be
 * longer than the modulators so the RX_RCOSxxx_LEN is calculated for demodulators
 * as:
 * RX_RCOSxxx_LEN=M*(TAPS*INTERP + (INTERP-1))
 *                |    |     |        |
 *                |    |     |        +- for the polyphase expansion over one sample
 *                |    |     |           sample
 *                |    |     +- interpolate rate for interp/decimate
 *                |    +- number of filter taps
 *                +- M is the oversample rate, OVERSAMPLE
 * We include OVERSAMPLE in the INTERP term and simplify to:
 *   RX_RCOSxxx_LEN=(TAPS*(INTERP+1) - OVERSAMPLE)
 *
 * Similarly, for transmit filter operation the polyphase expansion is over
 * DECIMATE samples:
 *   TX_RCOSxxx_LEN=(TAPS*INTERP + DECIMATE-OVERSAMPLE)
 */

#if defined(V34_DEMO)  // for v.34 only
#if defined(V34_COMMON_INTERP)
#define V34OVERSAMPLE 				64
#else /* V34_COMMON_INTERP */
#define V34OVERSAMPLE 				8
#endif /* V34_COMMON_INTERP */

#define ROLL600 					0.75f
#define OVERSAMPLE600 				1
#define INTERP600 					(3*OVERSAMPLE600)
#define DEC600 						(20*OVERSAMPLE600)
#define TAPS600		 				60
#define RX_RCOS600_LEN 				((TAPS600+1)*INTERP600-OVERSAMPLE600)
#define FS600						(8000.0*INTERP600)
#define TX_TAPS600		 			((short)(TAPS600*INTERP600/(2*DEC600)))
#define TX_RCOS600_LEN 				(TX_TAPS600*2*DEC600+INTERP600-OVERSAMPLE600)

#define ROLL1200 					0.75f
#define OVERSAMPLE1200 				4
#define INTERP1200 					(3*OVERSAMPLE1200)
#define DEC1200 					(10*OVERSAMPLE1200)
#define TAPS1200		 			18
#define RCOS1200_LEN 				((TAPS1200+1)*INTERP1200-OVERSAMPLE1200)
#define TX_TAPS1200		 			((short)(TAPS1200*INTERP1200/(2*DEC1200)))
#define FS1200						(8000.0*INTERP1200)

#define ROLL1600 					0.75f
#define OVERSAMPLE1600 				2
#define INTERP1600 					(6*OVERSAMPLE1600)
#define DEC1600 					(15*OVERSAMPLE1600)
#define TAPS1600		 			18
#define RCOS1600_LEN 				((TAPS1600+1)*INTERP1600-OVERSAMPLE1600)
#define TX_TAPS1600		 			((short)(TAPS1600*INTERP1600/(2*DEC1600)))
#define FS1600						(8000.0*INTERP1600)

#define ROLL2400 					0.50f
#define OVERSAMPLE2400 				V34OVERSAMPLE
#define INTERP2400 					(3*OVERSAMPLE2400)
#define DEC2400 					(5*OVERSAMPLE2400)
#if defined(V34_COMMON_INTERP)
#define TAPS2400		 			26
#define TX_TAPS2400		 			((short)(TAPS2400*INTERP2400/(2*DEC2400)))
#define RCOS2400_LEN 				1
#else /* V34_COMMON_INTERP */
#define TAPS2400		 			20
#define RCOS2400_LEN 				((TAPS2400+1)*INTERP2400-OVERSAMPLE2400)
#endif /* V34_COMMON_INTERP */
#define TX_TAPS2400		 			((short)(TAPS2400*INTERP2400/(2*DEC2400)))
#define FS2400						(8000.0*INTERP2400)

#define ROLL2743 					0.50f
#define OVERSAMPLE2743 				V34OVERSAMPLE
#define INTERP2743 					(24*OVERSAMPLE2743)
#define DEC2743 					(35*OVERSAMPLE2743)
#define TAPS2743		 			33
#define RCOS2743_LEN 				((TAPS2743+1)*INTERP2743-OVERSAMPLE2743)
#define TX_TAPS2743		 			((short)(TAPS2743*INTERP2743/(2*DEC2743)))
#define FS2743						(8000.0*INTERP2743)

#define ROLL2800 					0.50f
#define OVERSAMPLE2800 				V34OVERSAMPLE
#define INTERP2800 					(7*OVERSAMPLE2800)
#define DEC2800 					(10*OVERSAMPLE2800)
#define TAPS2800		 			33
#define RCOS2800_LEN 				((TAPS2800+1)*INTERP2800-OVERSAMPLE2800)
#define TX_TAPS2800		 			((short)(TAPS2800*INTERP2800/(2*DEC2800)))
#define FS2800						(8000.0*INTERP2800)

#define ROLL3000 					0.35f
#define OVERSAMPLE3000 				V34OVERSAMPLE
#define INTERP3000 					(3*OVERSAMPLE3000)
#define DEC3000 					(4*OVERSAMPLE3000)
#if defined(V34_COMMON_INTERP)
#define TAPS3000		 			24
#define RCOS3000_LEN 				1
#define TX_TAPS3000		 			((short)(TAPS3000*INTERP3000/(2*DEC3000)))
#else /* V34_COMMON_INTERP */
#define TAPS3000		 			24
#define RCOS3000_LEN 				((TAPS3000+1)*INTERP3000-OVERSAMPLE3000)
#define TX_TAPS3000		 			((short)(TAPS3000*INTERP3000/(2*DEC3000)))
#endif /* V34_COMMON_INTERP */
#define FS3000						(8000.0*INTERP3000)

#define ROLL3200 					0.20f
#define OVERSAMPLE3200 				((short)(0.75*V34OVERSAMPLE))
#define INTERP3200 					(4*OVERSAMPLE3200)
#define DEC3200 					(5*OVERSAMPLE3200)
#if defined(V34_COMMON_INTERP)
#define TAPS3200		 			38
#define RCOS3200_LEN 				1
#define TX_TAPS3200		 			((short)(TAPS3200*INTERP3200/(2*DEC3200)))
#else /* V34_COMMON_INTERP */
#define TAPS3200		 			38
#define RCOS3200_LEN 				((TAPS3200+1)*INTERP3200-OVERSAMPLE3200)
#define TX_TAPS3200		 			((short)(TAPS3200*INTERP3200/(2*DEC3200)))
#endif /* V34_COMMON_INTERP */
#define FS3200						(8000.0*INTERP3200)

#define ROLL3429 					0.20f
#define OVERSAMPLE3429 				((short)(0.5*V34OVERSAMPLE))
#define INTERP3429 					(6*OVERSAMPLE3429)
#define DEC3429 					(7*OVERSAMPLE3429)
#define TAPS3429		 			40
#if defined(V34_COMMON_INTERP)
#define RCOS3429_LEN 				((TAPS3429+1)*INTERP3429-OVERSAMPLE3429)
#else /* V34_COMMON_INTERP */
#define RCOS3429_LEN 				1
#endif /* V34_COMMON_INTERP */
#define TX_TAPS3429		 			((short)(TAPS3429*INTERP3429/(2*DEC3429)))
#define FS3429						(8000.0*INTERP3429)

#else /* V34DEMO */

#define ROLL600                     0.75f
#define OVERSAMPLE600               1
#define INTERP600                   (3*OVERSAMPLE600)
#define DEC600                      (20*OVERSAMPLE600)
#define TAPS600                     60
#define RX_RCOS600_LEN              (TAPS600*INTERP600+DEC600)
#define FS600                       (8000.0*INTERP600)
#define TX_TAPS600                  3
#define TX_RCOS600_LEN 				(TX_TAPS600*2*DEC600+INTERP600-OVERSAMPLE600)

#define ROLL1200                    0.75f
#define OVERSAMPLE1200              4
#define INTERP1200                  (3*OVERSAMPLE1200)
#define DEC1200                     (10*OVERSAMPLE1200)
#define TAPS1200                    18
#define RCOS1200_LEN				(TAPS1200*INTERP1200+DEC1200)
#define TX_TAPS1200					3
#define FS1200                      (8000.0*INTERP1200)

#define ROLL1600                    0.75f
#define OVERSAMPLE1600              2
#define INTERP1600                  (6*OVERSAMPLE1600)
#define DEC1600                     (15*OVERSAMPLE1600)
#define TAPS1600                    18
#define RCOS1600_LEN                (TAPS1600*INTERP1600+DEC1600)
#define TX_TAPS1600					3
#define FS1600                      (8000.0*INTERP1600)

#define ROLL2400					0.75f
#define OVERSAMPLE2400				8
#define INTERP2400                  (3*OVERSAMPLE2400)
#define DEC2400                     (5*OVERSAMPLE2400)
#define TAPS2400					16
#define RCOS2400_LEN                (TAPS2400*INTERP2400+DEC2400)
#define TX_TAPS2400					5
#define FS2400                      (8000.0*INTERP2400)

#define ROLL3000 					0.35f
#define OVERSAMPLE3000 				1
#define INTERP3000 					(3*OVERSAMPLE3000)
#define DEC3000 					(4*OVERSAMPLE3000)
#define TAPS3000		 			1 // placeholders to keep compiler happy
#define RCOS3000_LEN 				1 // placeholders to keep compiler happy
#define TX_TAPS3000		 			1 // placeholders to keep compiler happy
#define FS3000						(8000.0*INTERP3000)

#define ROLL3200 					0.20f
#define OVERSAMPLE3200 				1
#define INTERP3200 					(4*OVERSAMPLE3200)
#define DEC3200 					(5*OVERSAMPLE3200)
#define TAPS3200		 			1 // placeholders to keep compiler happy
#define RCOS3200_LEN 				1 // placeholders to keep compiler happy
#define TX_TAPS3200		 			1 // placeholders to keep compiler happy
#define FS3200						(8000.0*INTERP3200)

#define ROLL3429 					0.20f
#define OVERSAMPLE3429 				1
#define INTERP3429 					(6*OVERSAMPLE3429)
#define DEC3429 					(7*OVERSAMPLE3429)
#define TAPS3429		 			1 // placeholders to keep compiler happy
#define RCOS3429_LEN 				1 // placeholders to keep compiler happy
#define TX_TAPS3429		 			1 // placeholders to keep compiler happy
#define FS3429						(8000.0*INTERP3429)
#endif /* V34DEMO */

	/***********************************/
	/**** APSK modulator parameters ****/
	/***********************************/

#define SYM_CLK_THRESHOLD			16384

	/*************************************/
	/**** APSK demodulator parameters ****/
	/*************************************/

/*
 * WHAT_SCALE is used in the Phase Detector (PD) and Loss Of Signal
 * The SQRT_WHAT scale factor is 1/64 to permit all v34 values to be represented
 * in Q1.15 format.  
 */
#define WHAT_SCALE						2048	// (1/16)*(1/OP_POINT)
#define WHAT_SCALE_SHIFT				4		// 2^4=64
/*
 * PLL_SCALE is used in the PLL loop filter coefficients K1 and K2 to scale
 * them up to reasonable integer values. 
 */
#define PLL_SCALE						(32768.0/OP_POINT)
#define PLL_SCALE_SHIFT					2
#define EQ_MSE_SCALE					OP_POINT*16
#define EQ_MSE_SCALE_SHIFT				(OP_POINT_SHIFT+4)
#define LOS_SCALE						OP_POINT
#define LOS_SCALE_SHIFT					OP_POINT_SHIFT

/*
 * This behaves differently for OP_POINT=1/4 and needs to be re-visited.
 * This value was adjusted because v22 was not detecting S1 fast enough.
 */
#define COARSE_THR 						4096 // (32768*OP_POINT*0.5)
#define COARSE_ERROR_SHIFT				4
#define TIMING_TABLE_LEN				(4*4)
#define COEF_INCR 						(0*4)
#define COEF_DECR 						(1*4)
#define FIR_INCR 						(2*4)
#define FIR_DECR 						(3*4)
#if !defined(RX_SAMPLE_INCREMENT)
#define RX_SAMPLE_INCREMENT				1	// 1 for one real entity per sampling interval (2 for complex sample)
#endif /* RX_SAMPLE_INCREMENT */

	/***********************************************************************
	 * Rx->timing_threshold bit fields:
	 * TTTT TTTT TT II IIII IIII
	 * ------------ ------------
	 *      |            |_ bits0-9: increment
	 *      |_ bits9-15: threshold
	 ************************************************************************/

#define SYM_CLK_THRESHOLD_SHIFT			10
#define SYM_CLK_INCREMENT_FIELD 		((1<<SYM_CLK_THRESHOLD_SHIFT)-1)
#define SYM_CLK_THRESHOLD_FIELD			(~SYM_CLK_INCREMENT_FIELD)
#define SYM_CLK_INCREMENT				1		// default timing adjustment
#define ALTS_TIMING_ENABLE
#define ALTS_SYM_CLK_THRESHOLD			0
#define ALTS_SYM_CLK_INCREMENT_SHIFT	1		// increment=1<<0 = 1
#define ALTS_SYM_CLK_INCREMENT			(1<<ALTS_SYM_CLK_INCREMENT_SHIFT)
#define ALTS_NUM_REPLAY_SYMBOLS			6		// number of replay symbols (must be even)
#define EQ_ALTS_BACKUP_MASK				6	// mask to ensure even symbol backup

#define REV_CORR_LEN 					16
#define REV_CORR_DELAY 					(4*REV_CORR_LEN/4)	/* 2 symbols*2x baud*(REV_CORR_LEN/4)*/

	/***********************************************************************
	 * LOS detector (See 07-06-2004 for design details)
	 * Rx->LOS_monitor bit fields:
	 * L S GGGGGGGGGGG CCC
	 * - - ----------- ---
	 * | |      |       |_ bits 0-2: LOS counter
	 * | |      |_ bits3-13: Gap counter
	 * | |_ bit 14: LOS state bit, 0=no signal, 1=signal detected
	 * |_ bit 15: Lock monitor bit, 0=unlocked, 1=locked.
	 ************************************************************************/

#define LOS_THRESHOLD		  				(LOS_SCALE/4)	/* 6 dB drop */
//#define LOS_COUNT 						4
//#define UNLOCKED 						0
//#define LOCKED 							400
#define LOS_COUNTER_SHIFT					0
#define LOS_COUNTER_FIELD                   (0x0007u<<LOS_COUNTER_SHIFT)
#define LOS_COUNTER_INCREMENT				(0x0001u<<LOS_COUNTER_SHIFT)
#define GAP_COUNTER_SHIFT					3
#define GAP_COUNTER_FIELD                   (0x07ffu<<GAP_COUNTER_SHIFT)
#define GAP_COUNTER_INCREMENT				(0x0001u<<GAP_COUNTER_SHIFT)
#define LOS_STATE_SHIFT						14
#define LOS_STATE_FIELD                   	(0x0001u<<LOS_STATE_SHIFT)
#define LOS_UNDETECTED						0
#define LOS_DETECTED						LOS_STATE_FIELD
#define LOCK_STATE_SHIFT					15
#define LOCK_STATE_FIELD					(0x0001u<<LOCK_STATE_SHIFT)
#define LOCK_DETECTED						LOCK_STATE_FIELD
#define LOCK_UNDETECTED						0
#define LOS_GAP_DETECTED					(LOCK_STATE_FIELD+LOS_STATE_FIELD)

#define LOS_DETECT_COUNT					4	/* consec. counts for detecting loss-of-lock */
#define LOS_UNDETECT_COUNT					4	/* consec. counts for detecting lock */

	/**** internal buffering in Rx_fir[] ****/

/*
 * See 2-25-2004 for initial design details.
 * Rx_fir[] is enlarged and used to store intermediate signals for precoder,
 * Noise Predicter filter, and proposed polyphase filter. We also make provision
 * to store the demod T/2 delay line for Iprime, Inm1, Inm2, Inm3.
 *
 *
 *                                 Rx_fir[]
 *                           ~                       ~
 *                           |   :                   |
 *                           | I, Q                  |
 *              Rx->fir_ptr->|                       |
 *                           |   :                   |
 *                           |  Polyphase Buffer     |
 *                           |  (filtered by AEQ)    |
 *                           |   :                   |
 *                           | IEQ, QEQ              |
 *  POLYPHASE_BUFFER_OFFSET->|                       |
 *                           |   :                   |
 *                           |  IQ Delay Buffer      |
 *                           |  (rotated by VCO)     |
 *                           |   :                   |
 *                           | Inm3, Qnm3            |
 *                           | Inm2, Qnm2            |
 *                           | Inm1, Qnm1            |
 *                           | Iprime, Qprime        |
 *   IQ_DELAY_BUFFER_OFFSET->|                       |
 *                           |   not used (wasted)   |
 *                           | NPF_nm3               |
 *                           |   not used (wasted)   |
 *                           | NPF_nm2               |
 *                           |   not used (wasted)   |
 *                           | NPF_nm1               |
 *                           |   not used (wasted)   |
 *                           | NPF_prime             |
 *                           |   not used (wasted)   |
 *                           |                       |
 *                           |   :                   |
 *                           |  inverse precoder     |
 *                           |  (see v34.h)          |
 *                           |   :                   |
 *                           |                       |
 *                           ~                       ~
 *
 */

#define POLYPHASE_FIR_LEN			7
#if !defined(POLYPHASE_BUFFER_LEN)
#define POLYPHASE_BUFFER_LEN		0	// default to disable
#endif /* POLYPHASE_BUFFER_LEN */
#if !defined(IQ_DELAY_BUFFER_LEN)
#define IQ_DELAY_BUFFER_LEN			4	// IQprime, IQ_nm1, IQ_nm2, IQ_nm3
#endif /* IQ_DELAY_BUFFER_LEN */
#if POLYPHASE_BUFFER_LEN == 0
#define POLYPHASE_BUFFER_OFFSET		(2*IQ_DELAY_BUFFER_LEN)
#define IQ_DELAY_BUFFER_OFFSET		(2*IQ_DELAY_BUFFER_LEN)
#else /* POLYPHASE_BUFFER_LEN == 0 */
#define POLYPHASE_BUFFER_OFFSET		(2*POLYPHASE_BUFFER_LEN)
#define IQ_DELAY_BUFFER_OFFSET		(POLYPHASE_BUFFER_OFFSET + 2*IQ_DELAY_BUFFER_LEN)
#endif /* POLYPHASE_BUFFER_LEN == 0 */
#define IQ_IPRIME_OFFSET			(IQ_DELAY_BUFFER_OFFSET)
#define IQ_INM1_OFFSET				(IQ_DELAY_BUFFER_OFFSET-2)
#define IQ_INM2_OFFSET				(IQ_DELAY_BUFFER_OFFSET-4)
#define IQ_INM3_OFFSET				(IQ_DELAY_BUFFER_OFFSET-6)

	/**** Automatic Gain Control ****/

#define AGC_REF 					8192	/* OP_POINT */
#define AGC_EST_SEED 				14338	/* -41 dB*/
#define AGC_EST_STEP 				16423
#define AGC_UPDATE_DISABLED			0

#define COS_PI_BY_4 				23170	/* 32768*cos(pi/4)*/
#define TWENTY_SIX_DEGREES 			4836
#define FOURTY_FIVE_DEGREES			8192
#define NINETY_DEGREES 				16384
#define ONE_EIGHTY_DEGREES 			32768
#define RX_DET_LEN 					80

	/**** phase jitter resonators ****/

#define PJ_COEF_A                   (2<<OP_POINT_SHIFT)
#define PJ50_COEF600_B				-28378	/* 32768*cos(2*pi*50/600) */
#define PJ60_COEF600_B				-26510	/* 32768*cos(2*pi*60/600) */
#define PJ50_COEF1200_B				-31651	/* 32768*cos(2*pi*50/1200) */
#define PJ60_COEF1200_B				-31164	/* 32768*cos(2*pi*60/1200) */
#define PJ50_COEF1600_B				-32138	/* 32768*cos(2*pi*50/1600) */
#define PJ60_COEF1600_B				-31863	/* 32768*cos(2*pi*60/1600) */
#define PJ50_COEF2400_B				-32488	/* 32768*cos(2*pi*50/2400) */
#define PJ60_COEF2400_B				-32365	/* 32768*cos(2*pi*60/2400) */
#define PJ50_COEF3000_B				-32588  /* 32768*cos(2*pi*50/3000) */
#define PJ60_COEF3000_B				-32510  /* 32768*cos(2*pi*60/3000) */
#define PJ50_COEF3200_B				-32610  /* 32768*cos(2*pi*50/3200) */
#define PJ60_COEF3200_B				-32541  /* 32768*cos(2*pi*60/3200) */

	/**** tables and coefficients ****/

extern short Rx_timing600[];
extern short Rx_timing1200[];
extern short Rx_timing1600[];
extern short Rx_timing2400[];
extern short Rx_timing3000[];
extern short Rx_timing3200[];
extern short Tx_RCOS600_f1200[];                                              
extern short Tx_RCOS600_f2400[];                                              
extern short Rx_RCOS600_f1200[];                                              
extern short Rx_RCOS600_f2400[];                                              
extern short RCOS1200_f1800[];                                                
extern short RCOS1600_f1800[];                                                
extern short RCOS2400_f1800[];                                                
extern short RCOS3429_f1959[];

	/**** transmitter functions ****/

#if defined(XDAIS_API)
#else /* XDAIS_API */
extern void Tx_init_APSK(struct START_PTRS *);
extern short APSK_modulator(struct TX_BLOCK *);
#endif /* XDAIS_API */

	/**** receiver functions ****/

#if defined(XDAIS_API)
#else /* XDAIS_API */
extern void Rx_init_APSK(struct START_PTRS *);
extern short APSK_demodulator(struct RX_BLOCK *);
extern short no_timing(struct RX_BLOCK *);
extern short sgn_timing(struct RX_BLOCK *);
extern short APSK_timing(struct RX_BLOCK *);
#if defined(ALTS_TIMING_ENABLE)
extern short ALTs_timing_estimator(short, struct RX_BLOCK *);
#endif /* ALTS_TIMING_ENABLE */
extern short coarse_timing(struct RX_BLOCK *);
extern short Rx_train_loops(struct RX_BLOCK *);
extern short Rx_detect_EQ(struct RX_BLOCK *);
extern void agc_gain_estimator(struct RX_BLOCK *);
extern short Rx_fir_autocorrelator(struct RX_BLOCK *);
extern short IIR_resonator(short, short*);
extern short v21_inband_detector(struct RX_BLOCK *);
#endif /* XDAIS_API */

/****************************************************************************/

#endif /* APSK_INCLUSION_ */
