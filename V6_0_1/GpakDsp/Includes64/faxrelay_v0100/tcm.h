/***************************************************************************
* $RCSfile: tcm.h,v $                
* $Revision:   1.1  $              
* Revision $Author:   rfuchs  $                              
* Revision $Date:   Nov 29 2007 11:47:10  $            
* Author of Origin: Peter B. Miller             
* Date of Origin: 09-18-96 
*                   
* Company: MESi               
* Website: www.mesi.net             
* Description: Structure definitions and extern references for TCM			
* decoder.								
*                   
* Copyright (C) MESi 1996-2006, all rights reserved.         
***************************************************************************/

#if !defined(TCM_INCLUSION_)
#define TCM_INCLUSION_

#define TCM8_DELAY_STATES 			8	/* number of input points in v.32 viterbi decoder */
#define TCM8_TRACEBACK_LEN 			15	/* trace-back length for v.32 8-state viterbi decoder */

	/**** memory ****/

struct TCM_ENCODER_BLOCK {
	short spare[1];
	};

struct TCM8_DECODER_BLOCK {
	short path_metrics[TCM8_DELAY_STATES];
	short path_bits[TCM8_DELAY_STATES];
	short min_distance[TCM8_DELAY_STATES];
	short state_metrics[TCM8_DELAY_STATES];			
	};

	/**** functions ****/

#if defined(XDAIS_API)
extern void TCM_MESI_TxInitTCM(struct TX_BLOCK *);
extern short TCM_MESI_TCMencoder(short, struct TX_BLOCK *);
extern void TCM_MESI_RxInitTCM(struct RX_BLOCK *);
extern short TCM_MESI_TCMslicer(struct RX_BLOCK *);
extern short TCM_MESI_TCMdiffDecoder(struct RX_BLOCK *);
#else /* XDAIS_API */
extern void Tx_init_TCM(struct TX_BLOCK *);
extern short TCM_encoder(short, struct TX_BLOCK *);
extern void Rx_init_TCM(struct RX_BLOCK *);
extern short TCM_slicer(struct RX_BLOCK *);
extern short TCM_decoder(struct RX_BLOCK *);
#endif /* XDAIS_API */ 

/****************************************************************************/

#endif /* TCM_INCLUSION_ */

