/*************************************************************************
* $RCSfile: relay.h,v $
* $Revision:   1.0  $
* $Date:   Apr 23 2013 14:18:38  $
* $Author:   rfuchs  $
* Company: MESi
* Description: Top lelev relay interface header
*
* Copyright (C) MESi 1996-2006, all rights reserved.
***************************************************************************/
#if !defined(__RELAY__)
#define __RELAY__


//{  #include "FaxCommon.h
#if !defined(FAXCOMMON_INCLUSION_)
#define FAXCOMMON_INCLUSION_

#define CIRC                   short            
struct COMPLEX {short real,imag;};
/****************************************************************************/
#endif
//}
//{  #include "faxconfig.h"
#if !defined(FAXCFG_INCLUSION_)
#define FAXCFG_INCLUSION_
#undef ENABLED
#undef DISABLED
#undef COMPILER
#undef DUMP_BUFFER

#define DISABLED 0
#define ENABLED 1
#define DUMP_BUFFER  ENABLED
#define COMPILER     ENABLED

#define DFT_COEF               572   /* 32768*(sqrt(2)/ANALYSIS_LEN)*fudge */
#define DFT_COEF_LEN           256


//{ #include "rxtx.h"
#if !defined(RXTX_INCLUSION_)
#define RXTX_INCLUSION_

#define TX_SILENCE_ID            0x0100
#define RX_IDLE_ID               0x0100
#define RX_MEASURE_POWER_ID      0x0101

#define MESSAGE_ID               0x0040
#define CALL_DIRECTION_ID        0x0080

/**** Rx->status response messages ****/
#define STATUS_OK                 0x00
#define FEATURE_NOT_SUPPORTED     0x01
#define DETECT_FAILURE            0x10
#define ANS_TONE_DETECTED         0x11
#define SYNC_FAILURE              0x20
#define CHECKSUM_FAILURE          0x21
#define CRC_FAILURE               0x22
#define CID_DETECTED              0x23
#define TRAIN_LOOPS_FAILURE       0x30
#define START_EQ_FAILURE          0x30
#define TRAIN_EQ_FAILURE          0x31
#define SCR1_FAILURE              0x40
#define LOSS_OF_LOCK              0x50
#define GAIN_HIT_STATUS           0x51
#define EXCESSIVE_MSE_STATUS      0x52
#define EXCESSIVE_RTD_STATUS      0x53
#define RETRAIN                   0x60
#define RETRAIN_FAILURE           0x61
#define RENEGOTIATE               0x62
#define RENEGOTIATE_FAILURE       0x63
#define V22_USB1_DETECTED         0x70
#define V22_S1_DETECTED           0x71
#define V22_SB1_DETECTED          0x72
#define V22_F1M_DETECTED          0x73
#define V32_ANS_DETECTED          0x80
#define V32_AA_DETECTED           0x81
#define V32_AC_DETECTED           0x82
#define V32_SELFLOCK_DETECTED     0x83
#define GSTN_CLEARDOWN_REQUESTED  0x90

/**** Tx->mode bit field definitions ****/
#define TX_LONG_RESYNC_FIELD       0x0001
#define TX_V26_FAST_SYN_FIELD      0x0001
#define TX_V34_CONTROL_RESYNC      0x0001
#define TX_TEP_FIELD               0x0002
#define TX_CID_TYPE2_BIT           0x0002
#define TX_DIRECTION_BIT           0x0002   // 0=ANSWER, 1=CALL
#define TX_BELLCORE_MODE_BIT       0x0008
#define TX_V32TCM_MODE_BIT         0x0008
#define TX_SOURCE_BIT              0x0008   // 0=recipient, 1=source
#define TX_V32BIS_MODE_BIT         0x0010
#define TX_V32_SPECIAL_TRAIN_BIT   0x0020
#define TX_V34_PRIMARY_RESYNC      0x0020
#define TX_SCRAMBLER_DISABLE_BIT   0x0040
#define TX_V32BIS_MODE_BIT         0x0010
#define TX_V32_SPECIAL_TRAIN_BIT   0x0020
#define TX_V34_PRIMARY_RESYNC      0x0020
#define TX_SCRAMBLER_DISABLE_BIT   0x0040
#define TX_V26_ALT_A_BIT         0 x0080
#define INTERCEPT_MODE_BIT         0x0100

/**** Rx->mode bit field definitions ****/
#define RX_LONG_RESYNC_FIELD      0x0001
#define RX_V26_FAST_SYN_FIELD      0x0001
#define RX_V34_CONTROL_RESYNC      0x0001
#define RX_DETECTOR_DISABLE         0x0002
#define RX_DIRECTION_BIT         0x0002   // 0=ANSWER, 1=CALL
#define RX_CID_TYPE2_BIT         0x0004
#define RX_LOS_FIELD            0x0004
#define RX_BELLCORE_MODE_BIT      0x0008
#define RX_STU_III_BIT            0x0008
#define RX_SOURCE_BIT            0x0008   // 0=recipient, 1=source
#define RX_EC_COEF_SAVE_BIT         0x0010
#define RX_EQ_COEF_SAVE_BIT         0x0020
#define RX_V34_PRIMARY_RESYNC      0x0020
#define RX_DESCRAMBLER_DISABLE_BIT   0x0040
#define RX_EC_COEF_SAVE_BIT         0x0010
#define RX_EQ_COEF_SAVE_BIT         0x0020
#define RX_V34_PRIMARY_RESYNC      0x0020
#define RX_DESCRAMBLER_DISABLE_BIT   0x0040
#define RX_V26_ALT_A_BIT         0x0080
#define FAX_DEMOD_DISABLE_BIT      0x0080
#define V34_HALF_DUPLEX_SELECT_BIT   0x0080   // 0=full duplex, 1=half duplex
#define RX_EC_DISABLE_BIT         0x0100

/**** Tx->scale attenuation values ****/
#define ATTENUATE_2DB            26029   /* 32768*10exp(-2 dB/20)   */
#define ATTENUATE_3DB            23198   /* 32768*10exp(-3 dB/20)   */
#define ATTENUATE_6DB            16423   /* 32768*10exp(-6 dB/20)   */
#define ATTENUATE_7DB            14636   /* 32768*10exp(-7 dB/20)   */
#define ATTENUATE_9DB            11627   /* 32768*10exp(-9 dB/20)   */
#define ATTENUATE_10DB            10362   /* 32768*10exp(-10 dB/20)   */
#define ATTENUATE_12DB            8231   /* 32768*10exp(-12 dB/20)   */
#define ATTENUATE_16DB            5192   /* 32768*10exp(-16 dB/20)   */
#define ATTENUATE_20DB            3277   /* 32768*10exp(-20 dB/20)   */
#define ATTENUATE_28DB            1305   /* 32768*10exp(-28 dB/20)   */
#define ATTENUATE_30DB            1036   /* 32768*10exp(-30 dB/20)   */
            
/**** transmitter/receiver defaults ****/
#define TX_MINUS_16DBM0            32767
#define RX_MINUS_48DBM0            130      /* 32768*10exp(-48 dB/20) */   
#define RXTX_PBB_A1             31744   /* 1-3.0/PBB_LEN */

#if !defined NUM_SAMPLES
   #define NUM_SAMPLES             20      /* 20 samples per processing frame */                 
#endif /* NUM_SAMPLES */

#if !defined TX_NUM_SAMPLES
   #define TX_NUM_SAMPLES             NUM_SAMPLES        
#endif /* TX_NUM_SAMPLES */

#if !defined RX_NUM_SAMPLES
   #define RX_NUM_SAMPLES             NUM_SAMPLES        
#endif /* RX_NUM_SAMPLES */

#if !defined(TRANSMITTER)
   #define TRANSMITTER               ENABLED
#endif /* TRANSMITTER */

#if !defined(RECEIVER)
   #define RECEIVER               ENABLED
#endif /* RECEIVER */
          
   /**** struct START_PTRS members ****/

#define TRANSMITTER_START_PTRS \
   short *Tx_block_start; \
   CIRC *Tx_sample_start; \
   CIRC *Tx_data_start; \
   CIRC *Tx_fir_start

#define RECEIVER_START_PTRS \
   short *Rx_block_start; \
   CIRC *Rx_sample_start; \
   CIRC *Rx_data_start; \
   CIRC *Rx_fir_start; \
   short *EQ_coef_start; \
   short *EC_coef_start; \
   short *encoder_start; \
   short *decoder_start; \
   CIRC *trace_back_start

struct START_PTRS {
   TRANSMITTER_START_PTRS;
   RECEIVER_START_PTRS;
};

   /**** Tx_block control members ****/

#define TX_CONTROL_MEMBERS \
   struct START_PTRS *start_ptrs; \
   short (*state)(struct TX_BLOCK *); \
   unsigned short state_ID; \
   unsigned short rate; \
   short scale; \
   short system_delay; \
   short *sample_head; \
   short *sample_tail; \
   short sample_len; \
   short *data_head; \
   short *data_tail; \
   short data_len; \
   short sample_counter; \
   short symbol_counter; \
   short call_counter; \
   short num_samples; \
   short mode; \
   short terminal_count; \
   short Nbits;\
   short Nmask; \
   short bit_register; \
   short bit_register_low; \
   short bit_index

struct TX_BLOCK {
   TX_CONTROL_MEMBERS;
};

/**** Rx_block members ****/
#define RX_CONTROL_MEMBERS \
   struct START_PTRS *start_ptrs; \
   short (*state)(struct RX_BLOCK *); \
   unsigned short state_ID; \
   short status; \
   unsigned short rate; \
   short power; \
   short *sample_head; \
   short *sample_tail; \
   short *sample_stop; \
   short sample_len; \
   short *data_head; \
   short *data_tail; \
   short data_len; \
   short sample_counter; \
   short symbol_counter; \
   short call_counter; \
   short num_samples; \
   short mode; \
   short threshold; \
   short detector_mask; \
   short digit_CP_mask; \
   short temp0; \
   short temp1; \
   short Nbits; \
   short Nmask; \
   short bit_register; \
   short bit_register_low; \
   short bit_index

struct RX_BLOCK {
   RX_CONTROL_MEMBERS;
};

/****************************************************************************
 * Macros to access Tx_block and Rx_block control section members.         
 ****************************************************************************/

#define set_Tx_num_samples(A, PTR)    ((struct TX_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->num_samples=A
#define set_Tx_rate(A, PTR)           ((struct TX_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->rate=A
#define set_Tx_scale(A, PTR)          ((struct TX_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->scale=A
#define set_system_delay(A, PTR)      ((struct TX_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->system_delay=A
#define set_Tx_terminal_count(A, PTR) ((struct TX_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->terminal_count=A
#define set_Tx_mode(A, PTR)           ((struct TX_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->mode=A

#define get_Tx_state_ID(PTR)          ((struct TX_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->state_ID
#define get_Tx_terminal_count(PTR)    ((struct TX_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->terminal_count
#define get_Tx_mode(PTR)              ((struct TX_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->mode
#define get_Tx_sample_head(PTR)       ((struct TX_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->sample_head
#define get_Tx_sample_tail(PTR)       ((struct TX_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->sample_tail
#define get_Tx_data_head(PTR)         ((struct TX_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->data_head
#define get_Tx_data_tail(PTR)         ((struct TX_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->data_tail

#define set_Rx_num_samples(A, PTR)    ((struct RX_BLOCK *)((struct START_PTRS *)PTR)->Rx_block_start)->num_samples=A
#define set_Rx_rate(A, PTR)           ((struct RX_BLOCK *)((struct START_PTRS *)PTR)->Rx_block_start)->rate=A
#define set_Rx_mode(A, PTR)           ((struct RX_BLOCK *)((struct START_PTRS *)PTR)->Rx_block_start)->mode=A
#define get_Rx_state_ID(PTR)          ((struct RX_BLOCK *)((struct START_PTRS *)PTR)->Rx_block_start)->state_ID
#define get_Rx_mode(PTR)              ((struct RX_BLOCK *)((struct START_PTRS *)PTR)->Rx_block_start)->mode
#define get_Rx_status(PTR)            ((struct RX_BLOCK *)((struct START_PTRS *)PTR)->Rx_block_start)->status
#define get_Rx_sample_head(PTR)       ((struct RX_BLOCK *)((struct START_PTRS *)PTR)->Rx_block_start)->sample_head
#define get_Rx_sample_tail(PTR)       ((struct RX_BLOCK *)((struct START_PTRS *)PTR)->Rx_block_start)->sample_tail
#define get_Rx_data_head(PTR)         ((struct RX_BLOCK *)((struct START_PTRS *)PTR)->Rx_block_start)->data_head
#define get_Rx_data_tail(PTR)         ((struct RX_BLOCK *)((struct START_PTRS *)PTR)->Rx_block_start)->data_tail
#endif
//}
//{ #include "apsk.h"
#if !defined(APSK_INCLUSION_)
#define APSK_INCLUSION_

	/**** APSK modulator common structure ****/

#define TX_APSK_MEMBERS \
	short interpolate; \
	short decimate; \
	short coef_ptr; \
	short *coef_start; \
	short *fir_head;	\
	short *fir_tail;	\
	short fir_len; \
	short fir_taps; \
	short sym_clk_offset; \
	short sym_clk_memory; \
	short sym_clk_phase; \
	unsigned short carrier; \
	short *map_ptr; \
	short *amp_ptr; \
	unsigned short phase; \
	unsigned short Sreg; \
	unsigned short Sreg_low; \
	short fir_scale; \
	unsigned short Ereg

struct TX_APSK_BLOCK
	{
	TX_CONTROL_MEMBERS;
	TX_APSK_MEMBERS;
	};

	/**** APSK demodulator common structures ****/

struct IIR_RESONATOR_BLOCK {
	short coef;
	short dnm1;
	short dnm2;
};

#define RX_APSK_MEMBERS \
	short (*decoder_ptr)(struct RX_BLOCK *); \
	short (*slicer_ptr)(struct RX_BLOCK *); \
	short (*timing_ptr)(struct RX_BLOCK *); \
	short baud_counter; \
	short *data_ptr; \
	short *sample_ptr;	/* This member not used */ \
	short fir_taps; \
	short *coef_start; \
	short coef_ptr; \
	short sym_clk_phase; \
	short interpolate; \
	short decimate; \
	short oversample;	/* This member not used */ \
	short *timing_start; /* This member not used */ \
	short I; \
	short Q; \
	short IEQ; \
	short QEQ; \
	short Iprime; \
	short Qprime; \
	short Inm1; \
	short Qnm1; \
	short Inm2; \
	short Qnm2; \
	short Inm3; \
	short Qnm3; \
	short Ihat; \
	short Qhat; \
	short Ihat_nm2; \
	short Qhat_nm2; \
unsigned short What; \
	short *fir_ptr; \
	short IEQprime_error; \
	short QEQprime_error; \
unsigned short EQ_MSE; \
	short EQ_2mu; \
	short COS; \
	short SIN; \
	unsigned short LO_memory; \
	unsigned short LO_frequency; \
	short LO_phase; \
	unsigned short vco_memory; \
	short phase_error; \
	short loop_memory; \
	short loop_memory_low; \
	short loop_K1; \
	short loop_K2; \
	struct IIR_RESONATOR_BLOCK PJ1; \
	struct IIR_RESONATOR_BLOCK PJ2; \
	short agc_gain; \
	short agc_K; \
	short frequency_est; \
	short sym_clk_memory; \
	short timing_threshold; \
	short coarse_error; \
	short LOS_counter; /* This member not used */  \
	unsigned short LOS_monitor; \
	short map_shift; \
	short Phat; \
	short phase; \
	short EQ_taps; \
	unsigned short Dreg; \
	unsigned short Dreg_low; \
	unsigned short pattern_reg; \
	short *trace_back_ptr; \
	short *signal_map_ptr; \
	short *EC_fir_ptr; \
	short *EC_sample_ptr; \
	short EC_2mu;	\
	short EC_MSE;	\
	short EC_taps; \
	short EC_shift; \
	short RTD

struct RX_APSK_BLOCK
	{
	RX_CONTROL_MEMBERS;
	RX_APSK_MEMBERS;
	};

	/**** APSK demodulator operating point, OP_POINT ****/

#define OP_POINT 			        8192	/* 32768*(1/4) */
#define OP_POINT_SHIFT		        2

	/**** RCOS filter coefficients ****/

/*
 *  RCOS COEFFICIENT LENGTH:  The RCOS filter coefficient length is calculated
 * based on the specified number of taps, interpolate and decimate rates,
 * oversample rate, and the extension for polyphase operation. A standard FIR
 * filter requires TAPS coefficients for the specified number of taps. An
 * interpolator/decimator structure requires TAPS*INTERP coefficients. To
 * implement a polyphase filter we oversample by OVERSAMPLE, and implement
 * a method for adjusting the sample phase coincident with filter coefficient
 * phase. For modulator filter, the polyphase span is one symbol, or the
 * decimation rate. For the demodulator (sample-rate to symbol rate), the
 * polyphase span is one sample, or the interpolate rate. The RCOS coefficients
 * are shared by modulator and demodulator for a given symbol rate, and
 * also for several symbol rates for some types of modems. The length set for
 * RCOS coefficients is fixed by the demodulator since those filters must be
 * longer than the modulators so the RX_RCOSxxx_LEN is calculated for demodulators
 * as:
 * RX_RCOSxxx_LEN=M*(TAPS*INTERP + (INTERP-1))
 *                |    |     |        |
 *                |    |     |        +- for the polyphase expansion over one sample
 *                |    |     |           sample
 *                |    |     +- interpolate rate for interp/decimate
 *                |    +- number of filter taps
 *                +- M is the oversample rate, OVERSAMPLE
 * We add OVERSAMPLE in the INTERP term and simplify to:
 *   RX_RCOSxxx_LEN=(TAPS*(INTERP+1) - OVERSAMPLE)
 *
 * Similarly, for transmit filter operation the polyphase expansion is over
 * DECIMATE samples:
 *   TX_RCOSxxx_LEN=(TAPS*INTERP + DECIMATE-OVERSAMPLE)
 */

#if defined(V34_DEMO)  // for v.34 only
#if defined(V34_COMMON_INTERP)
#define V34OVERSAMPLE 				64
#else /* V34_COMMON_INTERP */
#define V34OVERSAMPLE 				8
#endif /* V34_COMMON_INTERP */

#define ROLL600 					0.75f
#define OVERSAMPLE600 				1
#define INTERP600 					(3*OVERSAMPLE600)
#define DEC600 						(20*OVERSAMPLE600)
#define TAPS600		 				60
#define RX_RCOS600_LEN 				((TAPS600+1)*INTERP600-OVERSAMPLE600)
#define FS600						(8000.0*INTERP600)
#define TX_TAPS600		 			((short)(TAPS600*INTERP600/(2*DEC600)))
#define TX_RCOS600_LEN 				(TX_TAPS600*2*DEC600+INTERP600-OVERSAMPLE600)

#define ROLL1200 					0.75f
#define OVERSAMPLE1200 				4
#define INTERP1200 					(3*OVERSAMPLE1200)
#define DEC1200 					(10*OVERSAMPLE1200)
#define TAPS1200		 			18
#define RCOS1200_LEN 				((TAPS1200+1)*INTERP1200-OVERSAMPLE1200)
#define TX_TAPS1200		 			((short)(TAPS1200*INTERP1200/(2*DEC1200)))
#define FS1200						(8000.0*INTERP1200)

#define ROLL1600 					0.75f
#define OVERSAMPLE1600 				2
#define INTERP1600 					(6*OVERSAMPLE1600)
#define DEC1600 					(15*OVERSAMPLE1600)
#define TAPS1600		 			18
#define RCOS1600_LEN 				((TAPS1600+1)*INTERP1600-OVERSAMPLE1600)
#define TX_TAPS1600		 			((short)(TAPS1600*INTERP1600/(2*DEC1600)))
#define FS1600						(8000.0*INTERP1600)

#define ROLL2400 					0.50f
#define OVERSAMPLE2400 				V34OVERSAMPLE
#define INTERP2400 					(3*OVERSAMPLE2400)
#define DEC2400 					(5*OVERSAMPLE2400)
#if defined(V34_COMMON_INTERP)
#define TAPS2400		 			26
#define TX_TAPS2400		 			((short)(TAPS2400*INTERP2400/(2*DEC2400)))
#define RCOS2400_LEN 				1
#else /* V34_COMMON_INTERP */
#define TAPS2400		 			20
#define RCOS2400_LEN 				((TAPS2400+1)*INTERP2400-OVERSAMPLE2400)
#endif /* V34_COMMON_INTERP */
#define TX_TAPS2400		 			((short)(TAPS2400*INTERP2400/(2*DEC2400)))
#define FS2400						(8000.0*INTERP2400)

#define ROLL2743 					0.50f
#define OVERSAMPLE2743 				V34OVERSAMPLE
#define INTERP2743 					(24*OVERSAMPLE2743)
#define DEC2743 					(35*OVERSAMPLE2743)
#define TAPS2743		 			33
#define RCOS2743_LEN 				((TAPS2743+1)*INTERP2743-OVERSAMPLE2743)
#define TX_TAPS2743		 			((short)(TAPS2743*INTERP2743/(2*DEC2743)))
#define FS2743						(8000.0*INTERP2743)

#define ROLL2800 					0.50f
#define OVERSAMPLE2800 				V34OVERSAMPLE
#define INTERP2800 					(7*OVERSAMPLE2800)
#define DEC2800 					(10*OVERSAMPLE2800)
#define TAPS2800		 			33
#define RCOS2800_LEN 				((TAPS2800+1)*INTERP2800-OVERSAMPLE2800)
#define TX_TAPS2800		 			((short)(TAPS2800*INTERP2800/(2*DEC2800)))
#define FS2800						(8000.0*INTERP2800)

#define ROLL3000 					0.35f
#define OVERSAMPLE3000 				V34OVERSAMPLE
#define INTERP3000 					(3*OVERSAMPLE3000)
#define DEC3000 					(4*OVERSAMPLE3000)
#if defined(V34_COMMON_INTERP)
#define TAPS3000		 			24
#define RCOS3000_LEN 				1
#define TX_TAPS3000		 			((short)(TAPS3000*INTERP3000/(2*DEC3000)))
#else /* V34_COMMON_INTERP */
#define TAPS3000		 			24
#define RCOS3000_LEN 				((TAPS3000+1)*INTERP3000-OVERSAMPLE3000)
#define TX_TAPS3000		 			((short)(TAPS3000*INTERP3000/(2*DEC3000)))
#endif /* V34_COMMON_INTERP */
#define FS3000						(8000.0*INTERP3000)

#define ROLL3200 					0.20f
#define OVERSAMPLE3200 				((short)(0.75*V34OVERSAMPLE))
#define INTERP3200 					(4*OVERSAMPLE3200)
#define DEC3200 					(5*OVERSAMPLE3200)
#if defined(V34_COMMON_INTERP)
#define TAPS3200		 			38
#define RCOS3200_LEN 				1
#define TX_TAPS3200		 			((short)(TAPS3200*INTERP3200/(2*DEC3200)))
#else /* V34_COMMON_INTERP */
#define TAPS3200		 			38
#define RCOS3200_LEN 				((TAPS3200+1)*INTERP3200-OVERSAMPLE3200)
#define TX_TAPS3200		 			((short)(TAPS3200*INTERP3200/(2*DEC3200)))
#endif /* V34_COMMON_INTERP */
#define FS3200						(8000.0*INTERP3200)

#define ROLL3429 					0.20f
#define OVERSAMPLE3429 				((short)(0.5*V34OVERSAMPLE))
#define INTERP3429 					(6*OVERSAMPLE3429)
#define DEC3429 					(7*OVERSAMPLE3429)
#define TAPS3429		 			40
#if defined(V34_COMMON_INTERP)
#define RCOS3429_LEN 				((TAPS3429+1)*INTERP3429-OVERSAMPLE3429)
#else /* V34_COMMON_INTERP */
#define RCOS3429_LEN 				1
#endif /* V34_COMMON_INTERP */
#define TX_TAPS3429		 			((short)(TAPS3429*INTERP3429/(2*DEC3429)))
#define FS3429						(8000.0*INTERP3429)

#else /* V34DEMO */

#define ROLL600                     0.75f
#define OVERSAMPLE600               1
#define INTERP600                   (3*OVERSAMPLE600)
#define DEC600                      (20*OVERSAMPLE600)
#define TAPS600                     60
#define RX_RCOS600_LEN              (TAPS600*INTERP600+DEC600)
#define FS600                       (8000.0*INTERP600)
#define TX_TAPS600                  3
#define TX_RCOS600_LEN 				(TX_TAPS600*2*DEC600+INTERP600-OVERSAMPLE600)

#define ROLL1200                    0.75f
#define OVERSAMPLE1200              4
#define INTERP1200                  (3*OVERSAMPLE1200)
#define DEC1200                     (10*OVERSAMPLE1200)
#define TAPS1200                    18
#define RCOS1200_LEN				(TAPS1200*INTERP1200+DEC1200)
#define TX_TAPS1200					3
#define FS1200                      (8000.0*INTERP1200)

#define ROLL1600                    0.75f
#define OVERSAMPLE1600              2
#define INTERP1600                  (6*OVERSAMPLE1600)
#define DEC1600                     (15*OVERSAMPLE1600)
#define TAPS1600                    18
#define RCOS1600_LEN                (TAPS1600*INTERP1600+DEC1600)
#define TX_TAPS1600					3
#define FS1600                      (8000.0*INTERP1600)

#define ROLL2400					0.75f
#define OVERSAMPLE2400				8
#define INTERP2400                  (3*OVERSAMPLE2400)
#define DEC2400                     (5*OVERSAMPLE2400)
#define TAPS2400					16
#define RCOS2400_LEN                (TAPS2400*INTERP2400+DEC2400)
#define TX_TAPS2400					5
#define FS2400                      (8000.0*INTERP2400)

#define ROLL3000 					0.35f
#define OVERSAMPLE3000 				1
#define INTERP3000 					(3*OVERSAMPLE3000)
#define DEC3000 					(4*OVERSAMPLE3000)
#define TAPS3000		 			1 // placeholders to keep compiler happy
#define RCOS3000_LEN 				1 // placeholders to keep compiler happy
#define TX_TAPS3000		 			1 // placeholders to keep compiler happy
#define FS3000						(8000.0*INTERP3000)

#define ROLL3200 					0.20f
#define OVERSAMPLE3200 				1
#define INTERP3200 					(4*OVERSAMPLE3200)
#define DEC3200 					(5*OVERSAMPLE3200)
#define TAPS3200		 			1 // placeholders to keep compiler happy
#define RCOS3200_LEN 				1 // placeholders to keep compiler happy
#define TX_TAPS3200		 			1 // placeholders to keep compiler happy
#define FS3200						(8000.0*INTERP3200)

#define ROLL3429 					0.20f
#define OVERSAMPLE3429 				1
#define INTERP3429 					(6*OVERSAMPLE3429)
#define DEC3429 					(7*OVERSAMPLE3429)
#define TAPS3429		 			1 // placeholders to keep compiler happy
#define RCOS3429_LEN 				1 // placeholders to keep compiler happy
#define TX_TAPS3429		 			1 // placeholders to keep compiler happy
#define FS3429						(8000.0*INTERP3429)
#endif /* V34DEMO */

	/***********************************/
	/**** APSK modulator parameters ****/
	/***********************************/

#define SYM_CLK_THRESHOLD			16384

	/*************************************/
	/**** APSK demodulator parameters ****/
	/*************************************/

/*
 * WHAT_SCALE is used in the Phase Detector (PD) and Loss Of Signal
 * The SQRT_WHAT scale factor is 1/64 to permit all v34 values to be represented
 * in Q1.15 format.  
 */
#define WHAT_SCALE						2048	// (1/16)*(1/OP_POINT)
#define WHAT_SCALE_SHIFT				4		// 2^4=64
/*
 * PLL_SCALE is used in the PLL loop filter coefficients K1 and K2 to scale
 * them up to reasonable integer values. 
 */
#define PLL_SCALE						(32768.0/OP_POINT)
#define PLL_SCALE_SHIFT					2
#define EQ_MSE_SCALE					OP_POINT*16
#define EQ_MSE_SCALE_SHIFT				(OP_POINT_SHIFT+4)
#define LOS_SCALE						OP_POINT
#define LOS_SCALE_SHIFT					OP_POINT_SHIFT

/*
 * This behaves differently for OP_POINT=1/4 and needs to be re-visited.
 * This value was adjusted because v22 was not detecting S1 fast enough.
 */
#define COARSE_THR 						4096 // (32768*OP_POINT*0.5)
#define COARSE_ERROR_SHIFT				4
#define TIMING_TABLE_LEN				(4*4)
#define COEF_INCR 						(0*4)
#define COEF_DECR 						(1*4)
#define FIR_INCR 						(2*4)
#define FIR_DECR 						(3*4)
#if !defined(RX_SAMPLE_INCREMENT)
#define RX_SAMPLE_INCREMENT				1	// 1 for one real entity per sampling interval (2 for complex sample)
#endif /* RX_SAMPLE_INCREMENT */

	/***********************************************************************
	 * Rx->timing_threshold bit fields:
	 * TTTT TTTT TT II IIII IIII
	 * ------------ ------------
	 *      |            |_ bits0-9: increment
	 *      |_ bits9-15: threshold
	 ************************************************************************/

#define SYM_CLK_THRESHOLD_SHIFT			10
#define SYM_CLK_INCREMENT_FIELD 		((1<<SYM_CLK_THRESHOLD_SHIFT)-1)
#define SYM_CLK_THRESHOLD_FIELD			(~SYM_CLK_INCREMENT_FIELD)
#define SYM_CLK_INCREMENT				1		// default timing adjustment
#define ALTS_TIMING_ENABLE
#define ALTS_SYM_CLK_THRESHOLD			0
#define ALTS_SYM_CLK_INCREMENT_SHIFT	1		// increment=1<<0 = 1
#define ALTS_SYM_CLK_INCREMENT			(1<<ALTS_SYM_CLK_INCREMENT_SHIFT)
#define ALTS_NUM_REPLAY_SYMBOLS			6		// number of replay symbols (must be even)
#define EQ_ALTS_BACKUP_MASK				6	// mask to ensure even symbol backup

#define REV_CORR_LEN 					16
#define REV_CORR_DELAY 					(4*REV_CORR_LEN/4)	/* 2 symbols*2x baud*(REV_CORR_LEN/4)*/

	/***********************************************************************
	 * LOS detector (See 07-06-2004 for design details)
	 * Rx->LOS_monitor bit fields:
	 * L S GGGGGGGGGGG CCC
	 * - - ----------- ---
	 * | |      |       |_ bits 0-2: LOS counter
	 * | |      |_ bits3-13: Gap counter
	 * | |_ bit 14: LOS state bit, 0=no signal, 1=signal detected
	 * |_ bit 15: Lock monitor bit, 0=unlocked, 1=locked.
	 ************************************************************************/

#define LOS_THRESHOLD		  				(LOS_SCALE/4)	/* 6 dB drop */
//#define LOS_COUNT 						4
//#define UNLOCKED 						0
//#define LOCKED 							400
#define LOS_COUNTER_SHIFT					0
#define LOS_COUNTER_FIELD                   (0x0007u<<LOS_COUNTER_SHIFT)
#define LOS_COUNTER_INCREMENT				(0x0001u<<LOS_COUNTER_SHIFT)
#define GAP_COUNTER_SHIFT					3
#define GAP_COUNTER_FIELD                   (0x07ffu<<GAP_COUNTER_SHIFT)
#define GAP_COUNTER_INCREMENT				(0x0001u<<GAP_COUNTER_SHIFT)
#define LOS_STATE_SHIFT						14
#define LOS_STATE_FIELD                   	(0x0001u<<LOS_STATE_SHIFT)
#define LOS_UNDETECTED						0
#define LOS_DETECTED						LOS_STATE_FIELD
#define LOCK_STATE_SHIFT					15
#define LOCK_STATE_FIELD					(0x0001u<<LOCK_STATE_SHIFT)
#define LOCK_DETECTED						LOCK_STATE_FIELD
#define LOCK_UNDETECTED						0
#define LOS_GAP_DETECTED					(LOCK_STATE_FIELD+LOS_STATE_FIELD)

#define LOS_DETECT_COUNT					4	/* consec. counts for detecting loss-of-lock */
#define LOS_UNDETECT_COUNT					4	/* consec. counts for detecting lock */

	/**** internal buffering in Rx_fir[] ****/

/*
 * See 2-25-2004 for initial design details.
 * Rx_fir[] is enlarged and used to store intermediate signals for precoder,
 * Noise Predicter filter, and proposed polyphase filter. We also make provision
 * to store the demod T/2 delay line for Iprime, Inm1, Inm2, Inm3.
 *
 *
 *                                 Rx_fir[]
 *                           ~                       ~
 *                           |   :                   |
 *                           | I, Q                  |
 *              Rx->fir_ptr->|                       |
 *                           |   :                   |
 *                           |  Polyphase Buffer     |
 *                           |  (filtered by AEQ)    |
 *                           |   :                   |
 *                           | IEQ, QEQ              |
 *  POLYPHASE_BUFFER_OFFSET->|                       |
 *                           |   :                   |
 *                           |  IQ Delay Buffer      |
 *                           |  (rotated by VCO)     |
 *                           |   :                   |
 *                           | Inm3, Qnm3            |
 *                           | Inm2, Qnm2            |
 *                           | Inm1, Qnm1            |
 *                           | Iprime, Qprime        |
 *   IQ_DELAY_BUFFER_OFFSET->|                       |
 *                           |   not used (wasted)   |
 *                           | NPF_nm3               |
 *                           |   not used (wasted)   |
 *                           | NPF_nm2               |
 *                           |   not used (wasted)   |
 *                           | NPF_nm1               |
 *                           |   not used (wasted)   |
 *                           | NPF_prime             |
 *                           |   not used (wasted)   |
 *                           |                       |
 *                           |   :                   |
 *                           |  inverse precoder     |
 *                           |  (see v34.h)          |
 *                           |   :                   |
 *                           |                       |
 *                           ~                       ~
 *
 */

#define POLYPHASE_FIR_LEN			7
#if !defined(POLYPHASE_BUFFER_LEN)
#define POLYPHASE_BUFFER_LEN		0	// default to disable
#endif /* POLYPHASE_BUFFER_LEN */
#if !defined(IQ_DELAY_BUFFER_LEN)
#define IQ_DELAY_BUFFER_LEN			4	// IQprime, IQ_nm1, IQ_nm2, IQ_nm3
#endif /* IQ_DELAY_BUFFER_LEN */
#if POLYPHASE_BUFFER_LEN == 0
#define POLYPHASE_BUFFER_OFFSET		(2*IQ_DELAY_BUFFER_LEN)
#define IQ_DELAY_BUFFER_OFFSET		(2*IQ_DELAY_BUFFER_LEN)
#else /* POLYPHASE_BUFFER_LEN == 0 */
#define POLYPHASE_BUFFER_OFFSET		(2*POLYPHASE_BUFFER_LEN)
#define IQ_DELAY_BUFFER_OFFSET		(POLYPHASE_BUFFER_OFFSET + 2*IQ_DELAY_BUFFER_LEN)
#endif /* POLYPHASE_BUFFER_LEN == 0 */
#define IQ_IPRIME_OFFSET			(IQ_DELAY_BUFFER_OFFSET)
#define IQ_INM1_OFFSET				(IQ_DELAY_BUFFER_OFFSET-2)
#define IQ_INM2_OFFSET				(IQ_DELAY_BUFFER_OFFSET-4)
#define IQ_INM3_OFFSET				(IQ_DELAY_BUFFER_OFFSET-6)

	/**** Automatic Gain Control ****/

#define AGC_REF 					8192	/* OP_POINT */
#define AGC_EST_SEED 				14338	/* -41 dB*/
#define AGC_EST_STEP 				16423
#define AGC_UPDATE_DISABLED			0

#define COS_PI_BY_4 				23170	/* 32768*cos(pi/4)*/
#define TWENTY_SIX_DEGREES 			4836
#define FOURTY_FIVE_DEGREES			8192
#define NINETY_DEGREES 				16384
#define ONE_EIGHTY_DEGREES 			32768
#define RX_DET_LEN 					80

	/**** phase jitter resonators ****/

#define PJ_COEF_A                   (2<<OP_POINT_SHIFT)
#define PJ50_COEF600_B				-28378	/* 32768*cos(2*pi*50/600) */
#define PJ60_COEF600_B				-26510	/* 32768*cos(2*pi*60/600) */
#define PJ50_COEF1200_B				-31651	/* 32768*cos(2*pi*50/1200) */
#define PJ60_COEF1200_B				-31164	/* 32768*cos(2*pi*60/1200) */
#define PJ50_COEF1600_B				-32138	/* 32768*cos(2*pi*50/1600) */
#define PJ60_COEF1600_B				-31863	/* 32768*cos(2*pi*60/1600) */
#define PJ50_COEF2400_B				-32488	/* 32768*cos(2*pi*50/2400) */
#define PJ60_COEF2400_B				-32365	/* 32768*cos(2*pi*60/2400) */
#define PJ50_COEF3000_B				-32588  /* 32768*cos(2*pi*50/3000) */
#define PJ60_COEF3000_B				-32510  /* 32768*cos(2*pi*60/3000) */
#define PJ50_COEF3200_B				-32610  /* 32768*cos(2*pi*50/3200) */
#define PJ60_COEF3200_B				-32541  /* 32768*cos(2*pi*60/3200) */

	/**** tables and coefficients ****/

extern short Rx_timing600[];
extern short Rx_timing1200[];
extern short Rx_timing1600[];
extern short Rx_timing2400[];
extern short Rx_timing3000[];
extern short Rx_timing3200[];
extern short Tx_RCOS600_f1200[];                                              
extern short Tx_RCOS600_f2400[];                                              
extern short Rx_RCOS600_f1200[];                                              
extern short Rx_RCOS600_f2400[];                                              
extern short RCOS1200_f1800[];                                                
extern short RCOS1600_f1800[];                                                
extern short RCOS2400_f1800[];                                                
extern short RCOS3429_f1959[];

	/**** transmitter functions ****/

#if defined(XDAIS_API)
#else /* XDAIS_API */
extern void Tx_init_APSK(struct START_PTRS *);
extern short APSK_modulator(struct TX_BLOCK *);
#endif /* XDAIS_API */

	/**** receiver functions ****/

#if defined(XDAIS_API)
#else /* XDAIS_API */
extern void Rx_init_APSK(struct START_PTRS *);
extern short APSK_demodulator(struct RX_BLOCK *);
extern short no_timing(struct RX_BLOCK *);
extern short sgn_timing(struct RX_BLOCK *);
extern short APSK_timing(struct RX_BLOCK *);
#if defined(ALTS_TIMING_ENABLE)
extern short ALTs_timing_estimator(short, struct RX_BLOCK *);
#endif /* ALTS_TIMING_ENABLE */
extern short coarse_timing(struct RX_BLOCK *);
extern short Rx_train_loops(struct RX_BLOCK *);
extern short Rx_detect_EQ(struct RX_BLOCK *);
extern void agc_gain_estimator(struct RX_BLOCK *);
extern short Rx_fir_autocorrelator(struct RX_BLOCK *);
extern short IIR_resonator(short, short*);
extern short v21_inband_detector(struct RX_BLOCK *);
#endif /* XDAIS_API */

/****************************************************************************/

#endif /* APSK_INCLUSION_ */
//}
//{ #include "v27.h"            /* has the largest Tx_spare[] and Rx_spare[] req't */
#if !defined(V27_INCLUSION_)
#define V27_INCLUSION_

	/**** state_ID definitions ****/

#define TX_V27_MOD_ID 				0x2700
#define TX_V27_SEGMENT1_ID 			0x2701
#define TX_V27_SEGMENT2_ID 			0x2702
#define TX_V27_SEGMENT3_ID 			0x2703
#define TX_V27_SEGMENT4_ID 			0x2704
#define TX_V27_SEGMENT5_ID 			0x2705
#define TX_V27_MESSAGE_ID 			(0x2700|MESSAGE_ID)
#define TX_V27_SEGMENTA_ID 			0x2707
#define TX_V27_SEGMENTB_ID 			0x2708

#define RX_V27_TRAIN_LOOPS_ID 		0x2701
#define RX_V27_DETECT_EQ_ID 		0x2702
#define RX_V27_TRAIN_EQ_ID 			0x2703
#define RX_V27_SCR1_ID 				0x2704
#define RX_V27_MESSAGE_ID 			(0x2700|MESSAGE_ID)

	/**** mode definitions ****/

#define STARTUP 					0
#define TURNAROUND 					1

	/**** memory ****/

#define TX_V27_MEMBERS \
	short Sguard; \
	short Sinv

struct TX_V27_BLOCK
	{
	TX_CONTROL_MEMBERS;
	TX_APSK_MEMBERS;
	TX_V27_MEMBERS;
	};

#define RX_V27_MEMBERS \
	short Dguard; \
	short Dinv; \
	short train_EQ_timeout

struct RX_V27_BLOCK
	{
	RX_CONTROL_MEMBERS;
	RX_APSK_MEMBERS;
	RX_V27_MEMBERS;
	};

	/**** functions ****/

#if defined(XDAIS_API)
extern void V27_MESI_TxInitV27(struct START_PTRS *);
extern void V27_MESI_RxInitV27(struct START_PTRS *);
#else /* XDAIS_API */
extern void Tx_init_v27(struct START_PTRS *);
extern void Rx_init_v27(struct START_PTRS *);
#endif /* XDAIS_API */ 

	/**** macros ****/

#define Tx_init_v27_2400(ptrs) \
				{set_Tx_rate(2400,ptrs); \
				enable_Tx_v27_TEP(ptrs); \
			 	Tx_init_v27(ptrs);}
#define Tx_init_v27_2400_TEP(ptrs) Tx_init_v27_2400(ptrs)
#define Tx_init_v27_4800(ptrs) \
				{set_Tx_rate(4800,ptrs); \
				enable_Tx_v27_TEP(ptrs); \
			 	Tx_init_v27(ptrs);}
#define Tx_init_v27_4800_TEP(ptrs) Tx_init_v27_4800(ptrs)

#define enable_Tx_v27_startup_seq(ptrs) \
	set_Tx_mode(~TX_LONG_RESYNC_FIELD&get_Tx_mode(ptrs),ptrs)
#define enable_Tx_v27_turnaround_seq(ptrs) \
	set_Tx_mode(TX_LONG_RESYNC_FIELD|get_Tx_mode(ptrs),ptrs)
#define enable_Tx_v27_TEP(ptrs) \
	{set_Tx_mode(TX_TEP_FIELD|get_Tx_mode(ptrs),ptrs); \
	Tx_init_v27(ptrs);}
#define disable_Tx_v27_TEP(ptrs) \
	{set_Tx_mode(~TX_TEP_FIELD&get_Tx_mode(ptrs),ptrs); \
	Tx_init_v27(ptrs);}

#define Rx_init_v27_2400(ptrs) \
				{set_Rx_rate(2400,ptrs); \
			 	Rx_init_v27(ptrs);}
#define Rx_init_v27_4800(ptrs) \
				{set_Rx_rate(4800,ptrs); \
			 	Rx_init_v27(ptrs);}

#define enable_Rx_v27_startup_seq(ptrs) \
	set_Rx_mode(~RX_LONG_RESYNC_FIELD&get_Rx_mode(ptrs),ptrs)
#define enable_Rx_v27_turnaround_seq(ptrs) \
	set_Rx_mode(RX_LONG_RESYNC_FIELD|get_Rx_mode(ptrs),ptrs)

#define enable_Tx_v27_resync(ptrs) enable_Tx_v27_turnaround_seq(ptrs)
#define enable_Rx_v27_resync(ptrs) enable_Rx_v27_turnaround_seq(ptrs)

/****************************************************************************/

#endif /* V27_INCLUSION_ */
//}
//{ #include "tcm.h"
#if !defined(TCM_INCLUSION_)
#define TCM_INCLUSION_

#define TCM8_DELAY_STATES 			8	/* number of input points in v.32 viterbi decoder */
#define TCM8_TRACEBACK_LEN 			15	/* trace-back length for v.32 8-state viterbi decoder */

	/**** memory ****/

struct TCM_ENCODER_BLOCK {
	short spare[1];
	};

struct TCM8_DECODER_BLOCK {
	short path_metrics[TCM8_DELAY_STATES];
	short path_bits[TCM8_DELAY_STATES];
	short min_distance[TCM8_DELAY_STATES];
	short state_metrics[TCM8_DELAY_STATES];			
	};

	/**** functions ****/

#if defined(XDAIS_API)
extern void TCM_MESI_TxInitTCM(struct TX_BLOCK *);
extern short TCM_MESI_TCMencoder(short, struct TX_BLOCK *);
extern void TCM_MESI_RxInitTCM(struct RX_BLOCK *);
extern short TCM_MESI_TCMslicer(struct RX_BLOCK *);
extern short TCM_MESI_TCMdiffDecoder(struct RX_BLOCK *);
#else /* XDAIS_API */
extern void Tx_init_TCM(struct TX_BLOCK *);
extern short TCM_encoder(short, struct TX_BLOCK *);
extern void Rx_init_TCM(struct RX_BLOCK *);
extern short TCM_slicer(struct RX_BLOCK *);
extern short TCM_decoder(struct RX_BLOCK *);
#endif /* XDAIS_API */ 

#endif /* TCM_INCLUSION_ */
//}

#if !defined(TX_BLOCK_LEN)
   #define TX_BLOCK_LEN            (sizeof(struct TX_V27_BLOCK)/sizeof(short))
#elif TX_BLOCK_LEN == 0
   #undef TX_BLOCK_LEN
#endif /* TX_BLOCK_LEN */

#if !defined(TX_SAMPLE_LEN)
   #define TX_SAMPLE_LEN            (2*8+1)
#endif /* TX_SAMPLE_LEN */

#if !defined(TX_DATA_LEN)
   #define TX_DATA_LEN               15
#endif /* TX_DATA_LEN */

#if !defined(TX_FIR_LEN)
   #define TX_FIR_LEN               12
#endif /* TX_FIR_LEN */

#if !defined(RX_BLOCK_LEN)
   #define RX_BLOCK_LEN            (sizeof(struct RX_V27_BLOCK)/sizeof(short))
#elif RX_BLOCK_LEN == 0
   #undef RX_BLOCK_LEN
#endif /* RX_BLOCK_LEN */

#if !defined(RX_SAMPLE_LEN)
   #define RX_SAMPLE_LEN            (80+8+1)
#endif /* RX_SAMPLE_LEN */

#if !defined(RX_DATA_LEN)
#if defined(VOICE)
   #define RX_DATA_LEN               RX_SAMPLE_LEN /* voice copies samples to Rx_data[]  */
#else /* VOICE */
   #define RX_DATA_LEN               4
#endif /* VOICE */
#endif /* RX_DATA_LEN */

#if !defined(RX_FIR_LEN)
   #define RX_FIR_LEN               (2*64)
#endif /* RX_FIR_LEN */

#if !defined(EQ_COEF_LEN)
   #define EQ_COEF_LEN               (2*64)
#endif /* EQ_COEF_LEN */

#if !defined(DECODER_BLOCK_LEN)
   #define DECODER_BLOCK_LEN         (sizeof(struct TCM8_DECODER_BLOCK)/sizeof(short))
#elif DECODER_BLOCK_LEN == 0
   #undef DECODER_BLOCK_LEN
#endif /* DECODER_BLOCK_LEN */

#if !defined(TRACEBACK_LEN)
   #define TRACEBACK_LEN              (TCM8_TRACEBACK_LEN*TCM8_DELAY_STATES)
#endif /* TRACEBACK_LEN */

#if !defined(RX_TIMING1200_TABLE)
   #define RX_TIMING1200_TABLE         ENABLED
#endif /* RX_TIMING1200_TABLE */          

#if !defined(RX_TIMING1600_TABLE)
   #define RX_TIMING1600_TABLE         ENABLED
#endif /* RX_TIMING1600_TABLE */          

#if !defined(RX_TIMING2400_TABLE)
   #define RX_TIMING2400_TABLE         ENABLED
#endif /* RX_TIMING2400_TABLE */          

#if !defined(RCOS1200_F1800_TABLE)
   #define RCOS1200_F1800_TABLE         ENABLED
#endif /* RCOS1200_F1800_TABLE */

#if !defined(RCOS1600_F1800_TABLE)
   #define RCOS1600_F1800_TABLE      ENABLED
#endif /* RCOS1600_F1800_TABLE */

#if !defined(RCOS2400_F1800_TABLE)
   #define RCOS2400_F1800_TABLE      ENABLED
#endif /* RCOS2400_F1800_TABLE */
              
#if !defined(TX_V17_MODEM)
   #define TX_V17_MODEM            (ENABLED & TRANSMITTER)
#endif /* TX_V17_MODEM */

#if !defined(RX_V17_MODEM)
   #define RX_V17_MODEM            (ENABLED & RECEIVER)
#endif /* RX_V17_MODEM */

#if !defined(TX_V21_MODEM)
   #define TX_V21_MODEM            (ENABLED & TRANSMITTER)
#endif /* TX_V21_MODEM */

#if !defined(RX_V21_MODEM)
   #define RX_V21_MODEM            (ENABLED & RECEIVER)
#endif /* RX_V21_MODEM */

#if !defined(TX_V27_2400_MODEM) && !defined(TX_V27_MODEM)
   #define TX_V27_2400_MODEM         (ENABLED & TRANSMITTER)
#endif /* TX_V27_2400_MODEM */

#if !defined(TX_V27_4800_MODEM) && !defined(TX_V27_MODEM)
   #define TX_V27_4800_MODEM         (ENABLED & TRANSMITTER)
#endif /* TX_V27_4800_MODEM */

#if !defined(TX_V27_MODEM)
   #define TX_V27_MODEM            (TX_V27_2400_MODEM | TX_V27_4800_MODEM)
#endif /* TX_V27_MODEM */

#if !defined(RX_V27_2400_MODEM) && !defined(RX_V27_MODEM)
   #define RX_V27_2400_MODEM         (ENABLED & RECEIVER)
#endif /* RX_V27_2400_MODEM */

#if !defined(RX_V27_4800_MODEM) && !defined(RX_V27_MODEM)
   #define RX_V27_4800_MODEM         (ENABLED & RECEIVER)
#endif /* RX_V27_4800_MODEM */

#if !defined(RX_V27_MODEM)
   #define RX_V27_MODEM            (RX_V27_2400_MODEM | RX_V27_4800_MODEM)
#endif /* RX_V27_MODEM */
      
#if !defined(TX_V29_MODEM)
   #define TX_V29_MODEM            (ENABLED & TRANSMITTER)
#endif /* TX_V29_MODEM */
#if !defined(RX_V29_MODEM)
   #define RX_V29_MODEM            (ENABLED & RECEIVER)
#endif /* RX_V29_MODEM */

#if !defined(TCM_7200)
   #define TCM_7200               ENABLED 
#endif /* TCM_7200 */

#if !defined(TCM_9600)
   #define TCM_9600               ENABLED 
#endif /* TCM_9600 */

#if !defined(TCM_12000)
   #define TCM_12000               ENABLED 
#endif /* TCM_12000 */

#if !defined(TCM_14400)
   #define TCM_14400               ENABLED 
#endif /* TCM_14400 */

#define TCM_ENCODER   ((TCM_7200|TCM_9600|TCM_12000|TCM_14400)   & TRANSMITTER)
#define TCM_DECODER   ((TCM_7200|TCM_9600|TCM_12000|TCM_14400)    & RECEIVER)
         
#if !defined(CNG_TONE)
   #define CNG_TONE               ENABLED
#endif /* CNG_TONE */

#if !defined(CED_TONE)
   #define CED_TONE               ENABLED
#endif /* CED_TONE */

#if !defined(TEP_1700_TONE)
   #define TEP_1700_TONE            ENABLED
#endif /* TEP_1700_TONE */

#if !defined(TEP_1800_TONE)
    #define TEP_1800_TONE            ENABLED
#endif /* TEP_1800_TONE */
             
#if !defined(TX_APSK_MODULATOR)
   #define TX_APSK_MODULATOR         (ENABLED & TRANSMITTER)
#endif /* TX_APSK_MODULATOR */

#if !defined(RX_APSK_DEMODULATOR)
   #define RX_APSK_DEMODULATOR         (ENABLED & RECEIVER)
#endif /* RX_APSK_DEMODULATOR */
#endif
//}
//{  #include "modemif.h"
#ifndef __MODEM_IF__
#define __MODEM_IF__
//{  #include "sequence.h"
#ifndef __SEQUENCE__
#define __SEQUENCE__

/*
* When changing the max values, check the bit field sizes in the structure
* below and adjust accordingly.
*/
#define MAX_STATE_MACHINE 		5
#define MAX_EVENTS 				128
#define LAST_ENTRY 				0x8000
#define ALLOW_USER_STATE 		0x4000
#define STATE_MACHINE_MASK 		0x3FFF
#define MAX_TIMERS 				16
#ifndef TRUE
#define TRUE (1==1)
#define FALSE !TRUE
#endif

typedef struct SeqStruct *SeqStructTypePointer;
typedef void (*PFI)(SeqStructTypePointer);

/* Event Table Struct */

struct ETS {
	int        event; 	// triggering event
	PFI        action;	// action routine to execute
	const struct ETS *nextstate; // next state to switch to
	};
/* state machine structure. One for each state machine */
struct sm{
	int ID;
	struct ETS *pCurrentState;
	};

struct tmr{
	int num;
	unsigned int value;
	};

	/**** Sequencer return codes ****/

#define SEQ_HEALTHY              	0x0000		// see sequence.c, relay.c
#define SEQ_BUF_POOL_EMPTY       	0x0001		// see bufmgr.c
#define SEQ_BUF_RETURN         		0x0002		// see bufmgr.c
#define SEQ_BUF_INTERNAL         	0x0004		// see bufmgr.c
#define SEQ_SM_FULL              	0x0008		// see sequence.c
#define SEQ_EVENT_OVERFLOW       	0x0010		// see sequence.c
#define SEQ_TIMER_OVERFLOW			0x0020 		// see sequence.c

struct SeqStruct{
    struct FaxStruct *FaxPtrs;
    unsigned int health;
	unsigned int MasterTimer;
	struct tmr Timer[MAX_TIMERS];
	struct sm StateMachine[MAX_STATE_MACHINE];
	unsigned int Events[MAX_EVENTS];
	unsigned int EventHead;
	unsigned int EventTail;
	unsigned int nTimers; // six bits used 
	unsigned int nStateMachines; // six bits used
	unsigned int ResidualSamples; // four bits used
	short *LastRxSampleHead;
	/* pRxSampleHead contains the address of the RxSampleHead pointer
	* This contains &START_PTRS->Rx_sample_block->sample_head
	*/
	short **pRxSampleHead;
	void *pBufMgr; 						/* pointer to BufStruct structure */
	struct ProtocolStruct *Protocol;	/* pointer to protocol structure */
#if defined(BUFLOG) | defined(SNOOP_CALL_LOG) | defined(NEED_REALTIME)
#if !defined(NEED_REALTIME)
#define NEED_REALTIME
#endif /* NEED_REALTIME */
	unsigned long RealTime;/* the count in ms since the call began */
#endif /* BUFLOG|SNOOP_CALL_LOG|NEED_REALTIME */
	short *DepBuffer;
    short *PartialIpFrame;
#ifdef INTERCEPT_SUPPORT
	int BitsPerBaud;
#endif /* INTERCEPT_SUPPORT */
#ifdef HOOK_SWITCH
	struct HookSwitchStruct *HookSwitch;
#endif
#ifdef _MSWINDOWS
	char DisplayState[80];
#endif /*_MSWINDOWS*/
    short AllowStateChange;
	};

extern void AddEvent(struct SeqStruct *q, unsigned int Event);
extern void DelEvent(struct SeqStruct *q, unsigned int Event);
extern int Sequencer(struct FaxStruct *f);
extern int AddStateMachine(struct SeqStruct *q, int StateID, struct ETS *StateTable);
extern int DelStateMachine(struct SeqStruct *q, int StateID);
extern int SeqTimeCheck(struct FaxStruct *f);
extern void StopTimer(struct SeqStruct *q, int TimeNum);
extern void StartTimer(struct SeqStruct *q, int TimeNum, unsigned int Nomst);
extern unsigned int ReadTimer(struct SeqStruct *q, int TimeNum);
extern void TimerTick(struct SeqStruct *q);
extern void InitSequencer(struct FaxStruct *);
extern void SeqBufDepEvent(struct SeqStruct *q,short *buf, int Event);
extern int SeqGetEventRemain(struct SeqStruct *q);

#define BlockStateChange(q) (((struct SeqStruct *)q)->AllowStateChange=FALSE)

/****************************************************************************/
#endif /* __SEQUENCE__ */
//}
//{  #include "packdata.h"
#ifndef __PACKDATA__
#define __PACKDATA__

#ifndef TRUE
#define TRUE (1==1)
#define FALSE !TRUE
#endif /* TRUE */
struct PackData{
    unsigned short data;
    unsigned short bits;
    unsigned short mask;
    unsigned short count;
    };

void PkdInit(struct PackData *p,short bits);
short PkdFlush(struct PackData *p);
short PkdPack(struct PackData *p,unsigned short data,unsigned short *retval);
short PkdNeedData(struct PackData *p);
void PkdNextOctet(struct PackData *p,short octet);
short PkdUnpack(struct PackData *p);
/****************************************************************************/
#endif /* __PACKDATA__ */
//}
//{  #include "hdlc.h"
#ifndef __HDLC__
#define __HDLC__
struct HdlcStruct {
    unsigned short fcs;
    unsigned short runningCount;
    unsigned short stuff;
    unsigned short onescount;
    unsigned short raw;
    unsigned short bitcount;
    unsigned short flags;
};
#define HDC_NO_DATA     	-1  
#define HDC_FLAG        	-2  
#define HDC_GOOD_CRC    	-3  
#define HDC_BAD_CRC     	-4  
#define HDC_FIVE_ONES   	1   
#define HDC_SIX_ONES    	2   
#define HDC_RX_DATA     	4   
#define HDC_FLAG_SEARCH 	8   

#define HdcGetCrc(h) ((h)->fcs)
#define HdcInvertCrc(h) ((h)->fcs^=0xffff)
#define HdcResetCrc(h) ((h)->fcs=0xffff)
#define HdcCheckCrc(h) ((h)->fcs == 0x1D0F?TRUE:FALSE)
#define HdcLastFrameSize(h) ((short)(h)->runningCount)
void HdcPartialFcs(short currentByte, struct HdlcStruct *h);
/*short HdcTxOctets(struct HdlcStruct *h,unsigned char *b,short n,unsigned char *ret);*/
short HdcTxOctet(struct HdlcStruct *h,unsigned char b,unsigned char *ret);
short HdcUnstuffOctet(struct HdlcStruct *h,short b);
short HdcUnstuffBit(struct HdlcStruct *h,short b);
short HdcTxCrc(struct HdlcStruct *h,unsigned char *ret);
void HdcTxFlag(struct HdlcStruct *h,unsigned char *ret);
void HdcInit(struct HdlcStruct *h);

/****************************************************************************/
#endif /* __HDLC__ */
//}
#ifdef VOICE
#ifndef VOICE_PACKET_SIZE
#define VOICE_PACKET_SIZE 		80
#endif /* VOICE_PACKET_SIZE */
#endif /* VOICE */

#if !defined(FAX_PACKET_SIZE)
#define FAX_PACKET_SIZE			10		/* packet length in msec. */
#endif /* FAX_PACKET_SIZE */

#define MIF_TX_IDLE				0
#define MIF_TX_FLAGS			1
#define MIF_TX_V21DATA			2
#define MIF_TX_UNDERRUN			3
#define MIF_TX_WAIT_EMPTY		4
#define MIF_TX_FAX_DATA			5
#ifdef VOICE
#define MIF_TX_AUDIO        	6
#endif /* VOICE */
#define MIF_TX_V21BYTE      	14	// must match T30_MSG_TYPE_V21BYTE

#define MIF_MOD_V27FB_2400		0x00	// T.30, table 2/T.30, DCS bits 9-14
#define MIF_MOD_V29_9600		0x20    // T.30, table 2/T.30, DCS bits 9-14
#define MIF_MOD_V27_4800		0x10    // T.30, table 2/T.30, DCS bits 9-14
#define MIF_MOD_V29_7200		0x30    // T.30, table 2/T.30, DCS bits 9-14
#define MIF_MOD_V17_14400		0x04    // T.30, table 2/T.30, DCS bits 9-14
#define MIF_MOD_V17_9600		0x24    // T.30, table 2/T.30, DCS bits 9-14
#define MIF_MOD_V17_12000		0x14    // T.30, table 2/T.30, DCS bits 9-14
#define MIF_MOD_V17_7200		0x34    // T.30, table 2/T.30, DCS bits 9-14

#define RX_ENERGY_DET_ID 		0x1000
#define RX_SIG_ANALYSIS_ID 		0x1100
#define V17_MODEM_ID 			0x1700
#define V27_MODEM_ID 			0x2700
#define V29_MODEM_ID 			0x2900
#define V21_MODEM_ID 			0x2100

#define MIF_MOD_NO_ECM			0
#define MIF_MOD_ECM				1
#define MIF_MOD_TRAINING		2

#define MIF_DIALTONE			1
#define MIF_BUSY				2
#define MIF_REORDER				3
#define MIF_ECSD				4
#define MIF_RINGBACK			5
#define MIF_RX_SILENCE          (0)
#define MIF_RX_OR_MASK          (0)
#define MIF_RX_AUTO_DETECT_MASK (AUTO_DETECT_MASK)
#define MIF_RX_V21_MASK         (V21_CH2_MASK|AUTO_DETECT_MASK)
#define MIF_RX_CNG_MASK         (CNG_MASK|AUTO_DETECT_MASK)
#define MIF_RX_CED_MASK         (CED_MASK|AUTO_DETECT_MASK)
#define MIF_RX_CALLPROG_MASK    (CALL_PROGRESS_MASK|AUTO_DETECT_MASK)

#define MIF_LOG_INPUT           0x01
#define MIF_8BIT_INPUT          0x02
#define MIF_16BIT_INPUT         0x04
#define MIF_MULAW_INPUT         0x08
#define MIF_TEXT_INPUT          0x10
#define MIF_SOUNDCARD_INPUT     0x20
#define MIF_SOUNDCARD_OUTPUT    0x40
#define MIF_SOUNDCARD_LOOPBACK  0x80
#define MIF_FILE_INPUT          0x100
#define MIF_FILE_OUTPUT         0x200

struct ModemIfStruct{
    struct PackData pack;
    struct PackData txPacked;
    struct HdlcStruct rxhdlc;
    struct HdlcStruct txhdlc;
	int OldRxState;
	short *RxBuf;		/* point to BufMgr buffer for rcvr */
	int RxCount;	/* count of bytes in current frame */
	int RxFlagCount;
	short *TxBuf;		/* pointer to buffer for the transmitter */
	int TxFlagCount;
	int TxState;
	int TxFlagReq;  /* number of flags that need to be modulated */
	int TxCount;	/* offset from TxOffsetMsg of next octet to tx */
	int TxFrameSize; /* number of octets in current frame */
	int SeqNr;
	int TxDataType;	/* ecm, non-ecm, training */
	int BitsPerBaud;
	int Residue;  /* number of bits of flag that were not transmitted. */
			/* tx data buffer to prevent underrunning */
    int RcvdFirstPacket;
#ifdef VOICE
    int SampleDefecit;
    int VoiceStatus;
    int voiceEnergy;
    int RxNeedsInit;
#endif
    int PacketLen;    /* duration of output frame in ms */
	};

extern void MifModemIf(struct FaxStruct *);
extern void MifInitModemIf(struct ModemIfStruct *);
/*****************************************************************************/
#endif /* __MODEM_IF__ */
//}
//{  #include "bufmgr.h"
#ifndef __BUFMGR__
#define __BUFMGR__
#ifdef BUFLOG
#include <stdio.h>
#endif /* BUFLOG */
/*
 * "BmrRwStruct" - structure to contain packet buffers
 */

struct BmrRwStruct {
    struct SeqStruct *Seq;
    short index;
    unsigned char *data;
    short len;
    short *buf;
};

#define BMR_SRC_CHAR 				0x01
#define BMR_SRC_INT  				0x02
#define BMR_SRC_BUF  				0x04
#define BMR_DST_CHAR 				0x10
#define BMR_DST_INT  				0X20
#define BMR_DST_BUF  				0X40
/*
* The number of buffer sections available is defined as NSECTIONS.  This can
* be used a one buffer of size NSECTIONS*OCTETS_PER_BUFFER or as
* NSECTIONS buffers each containing OCTETS_PER_BUFFER bytes.
*/
#ifndef NSECTIONS
#define NSECTIONS 					250
#endif /* NSECTIONS */

#ifndef OCTETS_PER_BUFFER
#define OCTETS_PER_BUFFER 			30
#endif /* OCTETS_PER_BUFFER */
/*
* set the size of block copy.  The stack is used for temp storage during
* the copy operation.  Therefore the stack must be atleast this large.
*/

#ifndef BMR_BLOCK_COPY_SIZE
#define BMR_BLOCK_COPY_SIZE 		64
#endif

/* The buffer manager maintains linked lists to pass and queue buffer from
* one process to another.  The number of linked lists if defined by
* NLLISTS and the index to each list element is defined below.
* The NET_IF linked lists are for the network interfaces.
*/
#define NLLISTS 					6
#define LL_TO_MODEM 				0
#define LL_FM_MODEM 				1
#define LL_TO_NET   				2
#define LL_FM_NET   				3
#define LL_TO_NET_IF				4
#define LL_FM_NET_IF				5

#define WORDS_PER_BUFFER ((OCTETS_PER_BUFFER+1)>>1)

struct BufLinkStruct {
    struct BufLinkStruct *pNextSect;
    short count;
    struct BufLinkStruct *pLinkNext;
    unsigned short data[WORDS_PER_BUFFER];
};

#define BUFFER_SECTION_OVERHEAD (((sizeof (struct BufLinkStruct)-sizeof(unsigned short)*WORDS_PER_BUFFER)/sizeof(short)))

#define BUF_COUNT_MASK	(short)0x7FFF
#define BUF_EMPTY_SECTION_MASK (short)0x8000

#define BMR_SIZE_BUFFER (WORDS_PER_BUFFER+BUFFER_SECTION_OVERHEAD)

struct BufStruct {
	short *(LinkedLists[NLLISTS]);
	short nSections;
	short nTotalSections;
	short MinSections;
	struct BufLinkStruct *EmptySections;
    /* when using 32-bit machines, the pointers must be on 32 bit boundaries
    *  To force a 32 bit alignment, the EmptySections pointer must be 
    *  located here, immediately before the buf array.
    * The buffers 'b' must be at the end of the structure if
    * the allocation is going to be dynamic
    */
	short buf[NSECTIONS][BMR_SIZE_BUFFER];
};

#define BmrGetSize(sect) ((sizeof(struct BufStruct))/sizeof(short)+\
    sect*(BMR_SIZE_BUFFER) -\
	NSECTIONS*BMR_SIZE_BUFFER)

	/**** functions ****/

extern  short *BmrGetBuffer(struct SeqStruct *q);
extern  void BmrReturnBuffer(struct SeqStruct *q,short *buf);
extern  int BmrLinkBuffer(struct SeqStruct *q,int ListNum,short *buf,int fTail);
extern  int BmrLinkBufferTail(struct SeqStruct *q,int ListNum,short *buf);
extern  int BmrLinkBufferHead(struct SeqStruct *q,int ListNum,short *buf);
extern  int BmrLinkLocalBuffer(struct SeqStruct *q,short **List,short *buf,int fTail);
extern  int BmrLinkLocalBufferTail(struct SeqStruct *q,short **List,short *buf);
extern  int BmrLinkLocalBufferHead(struct SeqStruct *q,short **List,short *buf);
extern  short *BmrUnlinkLocalBuffer(struct SeqStruct *q,short **List);
extern  short *BmrUnlinkBuffer(struct SeqStruct *q,int ListNum);
extern struct BufStruct *BmrInit(struct BufStruct *B,int nIntAvail);

extern  int BmrWriteOctet(struct SeqStruct *q,int index,int data,short *buf);
extern  int BmrWriteOctets(struct BmrRwStruct *b);
extern  int BmrWriteWord(struct SeqStruct *q,int index,int data,short *buf);
extern  int BmrWriteBigWord(struct SeqStruct *q,int index,int data,short *buf);
extern  int BmrReadOctet(int index,short *buf);
extern  int BmrReadOctets(struct BmrRwStruct *b);
extern  int BmrReadWord(int index,short *buf);

extern int BmrCopyData( struct SeqStruct *qdst,int idst,short *bufdst,
                 short *bufsrc,int isrc,int n);
extern  int BmrGetLen(short *buf);
extern  void BmrSetLen(short *buf,int length);

	/**** macros ****/

/****************************************************************************/
#endif /* __BUFMGR__ */
//}
//{  #include "t30.h"
#ifndef __T30__
#define __T30__

#ifdef SNOOP_CALL_LOG
	#include <stdio.h>
#endif

#define T30_TCP                 		1
#define T30_UDP                 		2
#define T30_REDUNDANT_UDP       		4
#define T30_NATIVE              		8
#define T30_T38                 		0x10

/*
 * Buffer offsets for the standard header as defined in our spec.
 */
#define T30_BUF_OFFSET_TYPE				0
#define T30_BUF_OFFSET_TIME_LSB			1
#define T30_BUF_OFFSET_TIME_MSB			2
#define T30_BUF_OFFSET_DATA				3

#define T30_LCL_UNTRAINED				0
#define T30_LCL_TRAINED_GOOD			1
#define T30_LCL_TRAINED_BAD				2
#define T30_SPOOF_IDLE 					0
#define T30_SPOOF_CFR  					1
#define T30_SPOOF_FTT  					2

/*
* "T30_SAMPLEBUF_DELAY" is the max delay from when a sample transmitted from
* the sample buffer will appear in the receiver buffer
* "T30_MAX_RT_DELAY_IN_MS" is the max external delay of the network echo; i.e
* a round trip satellite hop
*/
#define T30_MAX_RT_DELAY_IN_MS 			50
#define T30_SAMPLEBUF_DELAY 			500
#define T30_IO_DELAY 					125

#define RELAY 							0
#define INTERCEPT 						1
#define VON 							2

#ifdef INTERCEPT_SUPPORT
struct V21MsgStruct
	{
	int len;
	unsigned char data[128];
	};
#define EMBEDDED_LEN 					64
#define EMBEDDED_MSGS 					5
struct EmbeddedV21Struct	{
	int nmsg;
	int sizes[EMBEDDED_MSGS];
	unsigned char msgs[EMBEDDED_MSGS][EMBEDDED_LEN];
	};
#endif /* INTERCEPT_SUPPORT */

enum TcfProgress	{
	SearchZero, FirstZero, SecondZero, ThirdZero,
	FirstRun, SecondRun, ThirdRun, GoodTcf
	};

struct ProtocolStruct	{
	int ModType; /* modulation rate taken out of dcs message & 0x3c */
	int Mode;	/* ecm=1 or non ecm=0 from b27 of the dcs message */
	int Tep;	/* indicates if Tep was detected */
	short NsfDetectFlag; /* Non-standard message detected flag */
	int RcvdStartFaxData; /* flag indicates start of data was detected */
	short *DataPackets;	/* linked list of data packets */
	int TxKeyed;	/* flag set to tell protocol the tx is still keyed */
	int Cd;	/* flag set to tell protocol the receiver has CD */
	short *FmNetV21MsgQ; /* queue of v21 messages received from the
			   network when the local end was receiving a v21 msg*/
	short *V21Collision; /* dis dcs collision buffer pointer */
	int FirstFlagFound;	/* ecm flag indicating first flag detected */
	int DataContMsg[T30_BUF_OFFSET_DATA+4];
	int maxUnpackPerCall; /* max number of bytes unpacked per ms */
	int iUnpack;  /* index used during unpacking a frame */
	short *unpackQueue;	/* unpacked linked list */
	int LocalDelay;
	int TxWord;
	int BitsInTxWord;
	unsigned long Stream;
	short *FramingBuf;
	int FramingCount;
	int FrameNr;
	int TrainingState;
	int TrainingErrors;
	int TrainingBits;
	int Layer3;
	int TxStatusAtFlagDetect;
#ifdef SNOOP_CALL_LOG
	FILE *fhSnoopLog;
#endif
#ifdef INTERCEPT_SUPPORT
	int bitErrors;
	int TcfState;
	int RcvdPartialPage;
	unsigned char DcsData[8];
	unsigned char *faxData;	/* raw bits from the data pump */
	unsigned int sizeFaxData;
	unsigned int offsetFaxData;
	struct ecmstruct ecm;
	unsigned char LogData;
	unsigned char StartNewPage;
	unsigned char pc;  /* page count in pps frame */
	unsigned char bc;  /* block count in pps frame */
	unsigned int SnoopFileWrite;
	char *SnoopFileName;
	unsigned short *ecmblocks;
	int SnoopCount;
	void *Custom;
	struct V21MsgStruct Dis;		/* T.30 DIS message bytes */
	struct V21MsgStruct Csi;		/* T.30 CSI message bytes */
	struct V21MsgStruct Nsf;		/* T.30 NSF message bytes */
	struct V21MsgStruct Dtc;		/* T.30 DTC message bytes */
	struct V21MsgStruct Cig;		/* T.30 CIG message bytes */
	struct V21MsgStruct Nsc;		/* T.30 NSC message bytes */
	struct V21MsgStruct Dcs;		/* T.30 DCS message bytes */
	struct V21MsgStruct Tsi;		/* T.30 TSI message bytes */
	struct V21MsgStruct Nss;		/* T.30 NSS message bytes */
	int OnePagePerFile;
	int NssInBurst;
#endif /* INTERCEPT_SUPPORT */
	int UdpTxSeqNumber;
	int UdpRxSeqNumber;
	short TxSecondaryPacketA[BMR_SIZE_BUFFER];
	short TxSecondaryPacketB[BMR_SIZE_BUFFER];
#ifdef T38
	unsigned char T38V21Bytes[3];
	int FaxTep; // used in t38.c
	int FaxMode; // used in t38.c
	short *t38V21; /* V21 partial frame buffer */
	struct HdlcStruct t38TxHdlc;
	struct HdlcStruct t38RxHdlc;
	int t38ModType;/* modulation rate defined as a t38 indicator enum */
	unsigned int InitFlagNeeded;
	unsigned int bytesInFrame;/* number of byte in t38 hdlc frame*/
	unsigned int Ecm64;	/* false if ecm frames of 256, true for 64 */
	unsigned int t38StartSent; /*indicates if start of fax data has been sent*/
	int T38PreCorr;
#endif /* T38 */
#ifdef VOICE
	short *VoiceQueue;
	int VoiceState;
#endif /* VOICE */
#ifdef UNPACK_MODS
	unsigned long mask;
	unsigned long match;
#endif
	int FrameLen;
	int shift;
	};

extern void T30_init( struct SeqStruct *,short *,struct ProtocolStruct *);
extern int T30InitBuffer(struct SeqStruct *q,short **pbuf,int type);
extern int T30InitBufferwData(struct SeqStruct *q,short **pbuf,int type,int data);
#if (defined(BUFLOG) | defined(_MSWINDOWS) | defined(STDIO)) & defined(STATE_TRACE)
//#include "sequence.h"
extern void T30LogStateTransition(struct SeqStruct *, struct ETS *);
#endif /* (BUFLOG | _MSWINDOWS | STDIO) & STATE_TRACE */
#if defined(BUFLOG) & (defined(TX_V21MSG_TRACE)|defined(RX_V21MSG_TRACE))
extern void T30BufLogV21MsgDetails(short *);
#endif /* BUFLOG & (TX_V21MSG_TRACE|RX_V21MSG_TRACE) */

#define T30WriteHeader(q,loc,data,buf)  BmrWriteOctet(q,loc,data,buf)
#define T30WriteOctet(q,loc,data,buf)  BmrWriteOctet(q,loc+T30_BUF_OFFSET_DATA,data,buf)
#define T30WriteWord(q,loc,data,buf)  BmrWriteWord(q,loc+T30_BUF_OFFSET_DATA,data,buf)
#define T30ReadHeader(offset,buf) BmrReadOctet(offset,buf)
#define T30ReadOctet(offset,buf)  BmrReadOctet(offset+T30_BUF_OFFSET_DATA,buf)
#define T30ReadWord(offset,buf)  (BmrReadOctet(offset+T30_BUF_OFFSET_DATA,buf)|\
								  (BmrReadOctet(offset+T30_BUF_OFFSET_DATA+1,buf)<<8))

#define T30_MSG_STOP					0
#define T30_MSG_START					1

	/**** internal T30 type definitions (see Spec. section 5.2)  ****/

#define T30_MSG_TYPE_NO_SIGNAL			0	
#define T30_MSG_TYPE_CED        		1
#define T30_MSG_TYPE_CNG        		2
#define T30_MSG_TYPE_V21FLAG    		3
#define T30_MSG_TYPE_V21MSG     		4
#define T30_MSG_TYPE_DATA_START 		5
#define T30_MSG_TYPE_DATA_CONT  		6
#define T30_MSG_TYPE_FAX_CTRL   		7
#define T30_MSG_TYPE_MODEM_CHANGE 		8
#define T30_MSG_TYPE_RCV_CONF   		9
#define T30_MSG_TYPE_CALL_PROGRESS 		10
#define T30_MSG_TYPE_DIAL_NUMBER 		11
#define T30_MSG_TYPE_AUDIO      		12
#define T30_MSG_TYPE_GENDET_RESTART 	13
#define T30_MSG_TYPE_V21BYTE     		14 // must match MIF_TX_V21BYTE and T38

#define T30_MSG_CED_START				1
#define T30_MSG_CED_STOP				0
#define T30_MSG_CNG_START				1
#define T30_MSG_CNG_STOP				0
#define T30_MSG_TEP1700    				0x17
#define T30_MSG_TEP1800    				0x18
#define T30_MSG_MODE_TRAIN				2
#define T30_MSG_MODE_ECM				1
#define T30_MSG_MODE_NONECM				0
#define T30_MSG_TX_START				1
#define T30_MSG_TX_STOP					2
#define T30_MSG_CD_ON					3
#define T30_MSG_CD_OFF					4
#define T30_MSG_ABORT					1
#define T30_MODEM_RESET     			2

#define FCF_INIT 						0x80
#define FCF_RESP 						0x00
#define FCF_DTC  						0x81
#define FCF_CIG  						0x82
#define FCF_NSC  						0x84
#define FCF_DIS  						0x01
#define FCF_CSI  						0x02
#define FCF_PWD  						0x03
#define FCF_NSF  						0x04
#define FCF_CFR  						0x21
#define FCF_FTT  						0x22
#define FCF_CTR_T4  					0x23
#define FCF_CTR  						0x24
#define FCF_MCF  						0x31
#define FCF_RTP  						0x33
#define FCF_RTN  						0x32
#define FCF_PIP  						0x35
#define FCF_PIN  						0x34
#define FCF_RNR  						0x37
#define FCF_ERR  						0x38
#define FCF_PPR  						0x3D
#define FCF_FDM  						0x3F
#define FCF_DCS  						0x41
#define FCF_TSI  						0x42
#define FCF_SUB  						0x43
#define FCF_NSS  						0x44
#define FCF_SID  						0x45
#define FCF_CTC  						0x48
#define FCF_FNV  						0x53
#define FCF_CRP  						0x58
#define FCF_DCN  						0x5F
#define FCF_EOM  						0x71
#define FCF_MPS  						0x72
#define FCF_EOR  						0x73
#define FCF_EOP  						0x74
#define FCF_RR  						0x76
#define FCF_PRI_EOS 					0x78
#define FCF_PRI_EOM 					0x79
#define FCF_PRI_MPS 					0x7A
#define FCF_PRI_EOP 					0x7C
#define FCF_PPS  						0x7D
/*
#define FCF_EOM_T4  					0xF1
#define FCF_MPS_T4  					0xF2
#define FCF_EOP_T4  					0xF4
#define FCF_PRI_EOM_T4 					0xF9
#define FCF_PRI_MPS_T4 					0xFA
#define FCF_PRI_EOP_T4 					0xFC
*/
#define T30SetProtocol(f,p) ( ((struct FaxStruct*)f)->Seq->Protocol->Layer3=p)
#endif /* __T30__ */
//}
//{  #include "FaxStruct.h"
#if !defined(__FAXSTRUCT___)
#define __FAXSTRUCT___

struct FaxStruct
{
    struct ModemIfStruct *ModemIf;	/* address of Modem Interface structure */
    struct SeqStruct *Seq;          /* address of Sequencer structure */
    struct START_PTRS *start_ptrs;	/* modem channel memory address table */
    short RelayFunction;			/* relay type specifier */				
	short channel;					/* channel number */
#ifdef SEQ_COMM_LINK
    struct ippcStruct *comm;		/* for the external communications link */
#endif
};
/*****************************************************************************/
#endif /* __FAXSTRUCT___ */
//}

#define TONE1004HZ                  0x1000
#define V22BIS_MODEM                0x2200
#define V32BIS_MODEM                0x3200
#define V32_MODEM                   0x3201
#define V17_MODEM                   0x1700
#define V21_MODEM                   0x2100
#define V27_MODEM                   0x2700
#define V29_MODEM                   0x2900

#define SAMPLES_ONE_MS             8

enum TRelayFunction  { TFaxRelay, TFaxIntercept, TVoiceRelay, TDataRelay,TBentPipe };
struct RlyInitChannelStruct{
    int Layer3;
    int RelayFunction;
#ifdef HOOK_SWITCH
    struct HookSwitchStruct *hookswitch;
#endif
    };

/****************************************************************************
* Function: RlyRelayIf
* Input   : in         input samples or NULL if buffer copy if not needed
*           out        output sample or NULL if buffer copy is not needed
*           Length     Number of samples to produce/consume per itteration
*           fax        Relay Structure pointer
*           correction additional samples needed for transmitter
*                      this is for windows soundcard aps only
* Returns : status
* Purpose : Interface from sample interface to the modems
* This functions in two distinct modes that serve the same function.
* Case 1.  A high level process gathers "Length" samples (8, 20, 80,etc)
*  and places them in a linear buffer "in" and calls the RlyRelayIf.  It then
*  copies the sample from "out" and delivers them to the codec.
* Case 2.  "in" and "out" are NULL pointers the calling function manages the
*  placement of samples in/out of the relay internal structures.
****************************************************************************/
extern int RlyRelayIf(short *in,  short *out, int FillLength, struct FaxStruct *fax
#ifdef ASYNC_RX_TX
,  int correction
#endif
);

/****************************************************************************
* Function  RlyPacketToRelay
* Input:    f           Relay Structure pointer
*           msgbuf      pointer to msg for relay
*           length      number of bytes in buffer
* Returns   1           Packet accepted
*           0           Packet too large at present time, try again later
*           -1          Packet too large for current buffer pool (fail)
* Purpose   Attempt to send a packet to the relay
****************************************************************************/
extern int RlyPacketToRelay(struct FaxStruct *f,unsigned char *msgbuf, int length );
#define PKT_ACCEPTED               1
#define PKT_NOT_ACCEPTED          0
#define PKT_TOO_LARGE             -1

/****************************************************************************
* Function  RlyPacketFmRelay
* Input:    f           Relay Structure pointer
*           pMsgRet     point to array of chars where packet will be placed
*           MaxLen      max size of packet
* Returns   >0          length of packet stored at pMsgRet
*           0           no packets available
*           <0          Packet is greater than MaxLen.
*                         Packet requires -return bytes of memory.
* Purpose   Attempt to get a packet from the relay
****************************************************************************/
extern int RlyPacketFmRelay(struct FaxStruct *f,unsigned char *pMsgRet, int MaxLen );
#define NO_PKT_AVAIL             0

/****************************************************************************
* Function  RlyRelayInitChannel
* Input:    mem       address of channel memory
*           n         size of mem
*           p         pointer to channel init parameters
* Returns   FaxStruct pointer to FaxStruct
* Purpose   Initializes channel memory
****************************************************************************/
extern struct FaxStruct* RlyRelayInitChannel(unsigned char* mem,int n,struct RlyInitChannelStruct *p);

/****************************************************************************
* Macro:    RLY_CHANNEL_SIZE
* Purpose   Estimates channel memory size based on #define NSECTIONS
*           and is used for static memory models
*           (e.g.) unsigned char FaxMem[2 ][RLY_CHANNEL_SIZE];
*           would allocate two fax channels
****************************************************************************/

#if defined(CIRC_MEMORY_ALIGNMENT)
/*
 * The "modem memory"  = 6*128+80 is hard-wired for the C5400 CIRC alignment requirements.
 * DON'T touch this until you can verify the requirements on a C54xx target for correct 
 * operation!! (PBM 04-14-2006)
 */
#define RLY_CHANNEL_SIZE \
      ((6*128+80)*sizeof(short) + /* modem memory */ \
      sizeof(struct ProtocolStruct) + \
      sizeof(struct SeqStruct) + \
      BmrGetSize(NSECTIONS))
#else /* CIRC_MEMORY_ALIGNMENT */
#define FAX_MEM_ALIGNMENT             (8-1)   // align to 8 byte boundaries
#define RLY_CHANNEL_SIZE \
      ((sizeof(struct FaxStruct)+FAX_MEM_ALIGNMENT)&~FAX_MEM_ALIGNMENT) + \
      ((START_PTRS_LEN*sizeof(short)+FAX_MEM_ALIGNMENT)&~FAX_MEM_ALIGNMENT) + \
      ((RX_BLOCK_LEN*sizeof(short)+FAX_MEM_ALIGNMENT)&~FAX_MEM_ALIGNMENT) + \
      ((RX_FIR_LEN*sizeof(short)+FAX_MEM_ALIGNMENT)&~FAX_MEM_ALIGNMENT) + \
      ((EQ_COEF_LEN*sizeof(short)+FAX_MEM_ALIGNMENT)&~FAX_MEM_ALIGNMENT) + \
      ((TRACEBACK_LEN*sizeof(short)+FAX_MEM_ALIGNMENT)&~FAX_MEM_ALIGNMENT) + \
      ((RX_SAMPLE_LEN*sizeof(short)+FAX_MEM_ALIGNMENT)&~FAX_MEM_ALIGNMENT) + \
      ((DECODER_BLOCK_LEN*sizeof(short)+FAX_MEM_ALIGNMENT)&~FAX_MEM_ALIGNMENT) + \
      ((TX_BLOCK_LEN*sizeof(short)+FAX_MEM_ALIGNMENT)&~FAX_MEM_ALIGNMENT) + \
      ((TX_SAMPLE_LEN*sizeof(short)+FAX_MEM_ALIGNMENT)&~FAX_MEM_ALIGNMENT) + \
      ((TX_DATA_LEN*sizeof(short)+FAX_MEM_ALIGNMENT)&~FAX_MEM_ALIGNMENT) + \
      ((TX_FIR_LEN*sizeof(short)+FAX_MEM_ALIGNMENT)&~FAX_MEM_ALIGNMENT) + \
      ((RX_DATA_LEN*sizeof(short)+FAX_MEM_ALIGNMENT)&~FAX_MEM_ALIGNMENT) + \
      ((sizeof(struct ModemIfStruct)+FAX_MEM_ALIGNMENT)&~FAX_MEM_ALIGNMENT) + \
      ((sizeof(struct SeqStruct)+FAX_MEM_ALIGNMENT)&~FAX_MEM_ALIGNMENT) + \
      ((sizeof(struct ProtocolStruct)+FAX_MEM_ALIGNMENT)&~FAX_MEM_ALIGNMENT) + \
        BmrGetSize(NSECTIONS)
#endif /* CIRC_MEMORY_ALIGNMENT */
/*****************************************************************************/
#endif /* __RELAY__ */

//---------------------------------------
//{ #include "fsk.h"
#if !defined(FSK_INCLUSION_)
#define FSK_INCLUSION_
						 
#define RX_FSK_MARKS_ID 			0x1
#define RX_FSK_START_BIT_ID 		0x2
						 
	/**** FSK modulator common structure ****/
			  
#define TX_FSK_MEMBERS \
	short interpolate; \
	short decimate; \
	short coef_ptr; \
	short carrier; \
	short tone_scale; \
	unsigned short vco_memory; \
	short frequency;\
	short frequency_shift

struct TX_FSK_BLOCK	{                                                      
	TX_CONTROL_MEMBERS;                                      
	TX_FSK_MEMBERS;                                      
	};               

	/**** FSK demodulator common structure ****/
	
#define RX_FSK_MEMBERS \
	short mark_coef; \
	short space_coef; \
	short sym; \
	short coef_len; \
	short coef_ptr; \
	short interpolate; \
	short decimate; \
	short sym_nm1; \
	short sym_hat; \
	short sym_hat_nm2; \
	short sym_clk_memory; \
	short baud_counter; \
	short sym_level; \
	short MARKs_counter; \
	short LOS_threshold; \
	short LOS_memory

struct RX_FSK_BLOCK 	{                                                      
	RX_CONTROL_MEMBERS;                                      
	RX_FSK_MEMBERS;                                     
	};                                                     

	/**** FSK demodulator parameters ****/

#define FSK_LOS_LIMIT	 			4		/* 4 consecutive LOS events */
#define FSK_LOS_THRESHOLD			16384	/* 32768*10exp(-6 dB/20)	*/

	/**** functions ****/

#if defined(XDAIS_API)
extern void FSK_MESI_TxInitFSK(struct START_PTRS *);
extern short FSK_MESI_FSKmodulator(struct TX_BLOCK *);
extern void FSK_MESI_RxInitFSK(struct START_PTRS *);
extern short FSK_MESI_FSKdemodulator(struct RX_BLOCK *);
extern short FSK_MESI_MARKsDetect(struct RX_BLOCK *);
extern short FSK_MESI_StartBitDetect(struct RX_BLOCK *);
extern short FSK_MESI_RxComputeSymLevel(struct START_PTRS *);
#else /* XDAIS_API */
extern void Tx_init_FSK(struct START_PTRS *);
extern short FSK_modulator(struct TX_BLOCK *);
extern void Rx_init_FSK(struct START_PTRS *);
extern short FSK_demodulator(struct RX_BLOCK *);
extern short FSK_MARKs_detect(struct RX_BLOCK *);
extern short FSK_start_bit_detect(struct RX_BLOCK *);
extern short Rx_compute_sym_level(struct START_PTRS *);
#endif /* XDAIS_API */ 

/****************************************************************************/

#endif /* FSK_INCLUSION_ */
//}
//{  #include "v21.h"
#if !defined(V21_INCLUSION_)
#define V21_INCLUSION_

	/**** state_ID definitions ****/

#define	TX_V21_CH1_SILENCE_ID		0x2101
#define TX_V21_CH1_MESSAGE_ID 		(0x2100|MESSAGE_ID)

#define	TX_V21_CH2_SILENCE1_ID		0x2181
#define	TX_V21_CH2_ANS_ID			0x2182
#define	TX_V21_CH2_SILENCE2_ID		0x2183
#define TX_V21_CH2_MESSAGE_ID 		(0x2180|MESSAGE_ID)

#define RX_V21_CH1_ID 				0x2100
#define RX_V21_CH1_MARKS_ID 		0x2101
#define RX_V21_CH1_START_BIT_ID 	0x2102
#define RX_V21_CH1_MESSAGE_ID 		(0x2100|MESSAGE_ID)

#define RX_V21_CH2_ID 				0x2180
#define RX_V21_CH2_MARKS_ID 		0x2181
#define RX_V21_CH2_START_BIT_ID 	0x2182
#define RX_V21_CH2_MESSAGE_ID 		(0x2180|MESSAGE_ID)

	/**** FSK modulator common structure ****/

#define TX_V21_MEMBERS \
	unsigned short Sreg; \
	unsigned short Sreg_low

struct TX_V21_BLOCK	{
	TX_CONTROL_MEMBERS;
	TX_FSK_MEMBERS;
#if defined(V21_BER_MEASUREMENT)
	TX_V21_MEMBERS;
#endif /* V21_BER_MEASUREMENT */
	};

	/**** FSK demodulator common structure ****/

#define RX_V21_MEMBERS \
	unsigned short Dreg; \
	unsigned short Dreg_low

struct RX_V21_BLOCK	{
	RX_CONTROL_MEMBERS;
	RX_FSK_MEMBERS;
#if defined(V21_BER_MEASUREMENT)
	RX_V21_MEMBERS;
#endif /* V21_BER_MEASUREMENT */
	};

	/**** functions ****/

#if defined(XDAIS_API)
extern void V21_MESI_TxInitV21Ch1(struct START_PTRS *);
extern void V21_MESI_TxInitV21Ch2(struct START_PTRS *);
extern void V21_MESI_RxInitV21Ch1(struct START_PTRS *);
extern void V21_MESI_RxInitV21Ch2(struct START_PTRS *);
#else /* XDAIS_API */
extern void Tx_init_v21_ch1(struct START_PTRS *);
extern void Tx_init_v21_ch2(struct START_PTRS *);
extern void Rx_init_v21_ch1(struct START_PTRS *);
extern void Rx_init_v21_ch2(struct START_PTRS *);
#endif /* XDAIS_API */ 

/****************************************************************************/

#endif /* V21_INCLUSION_ */
//}
//{  #include "gendet.h"
#if !defined(GENDET_INCLUSION_)
#define GENDET_INCLUSION_

	/**** state_ID definitions ****/

#define TX_TONE_GEN_ID 				0x1200
#define TX_CNG_ID 					0x1211
#define TX_CED_ID 					0x1212
#define TX_ANS_ID 					0x1212
#define TX_ECSD_ID 					0x1213
#define TX_ANSAM_ID 				0x1214
#define TX_BELL_2225_ID 			0x1215
#define TX_CALL_PROGRESS_ID			0x1240
#define TX_DIALTONE_ID				0x1241
#define TX_RINGBACK_ID				0x1242
#define TX_REORDER_ID				0x1243
#define TX_BUSY_ID					0x1244

#define RX_ENERGY_DET_ID 			0x1000
#define RX_SIG_ANALYSIS_ID 			0x1100
#define RX_TONE_ID 					0x1200
#define RX_CNG_ID 					0x1211
#define RX_CED_ID 					0x1212
#define RX_ANS_ID 					0x1212
#define RX_ECSD_ID 					0x1213
#define RX_ANSAM_ID 				0x1214
#define RX_BELL_2225_ID 			0x1215
#define RX_TEP_1700_ID				0x1217
#define RX_TEP_1800_ID				0x1218
#define RX_CALL_PROGRESS_ID			0x1240
#define RX_DIALTONE_ID				0x1241
#define RX_RINGBACK_ID				0x1242
#define RX_REORDER_ID				0x1243
#define RX_BUSY_ID					0x1244

#define RX_V32_AUTOMODE_ID 			0x1500
#define RX_V32_CED_ID 				0x1501
#define RX_V32_ECSD_ID 				0x1502
#define RX_V32_AA_ID				0x1504
#define RX_V32_AC_ID				0x1508
#define RX_V32_USB1_ID				0x1510

#define RX_DETECT_V17_ID			0x1700			
#define RX_DETECT_V21_CH1_ID        0x2101
#define RX_DETECT_V21_CH2_ID        0x2102
#define RX_DETECT_V22C_ID			0x220C			
#define RX_DETECT_BELL_2225_ID		0x2225
#define RX_DETECT_V27_2400_ID       0x2724
#define RX_DETECT_V27_4800_ID       0x2748
#define RX_DETECT_V29_ID            0x2900

	/**** detector_mask mask bit definitions ****/

#define AUTO_DETECT_MASK 			0x0001
#define V21_CH2_MASK 				0x0004
#define V22_MASK 					0x0008
#define V27_2400_MASK 				0x0010
#define V27_4800_MASK 				0x0020
#define V29_MASK 					0x0040
#define V17_MASK 					0x0080
#define ANS_MASK 					0x0100
#define CED_MASK 					0x0100
#define BELL_2225_MASK 				0x0200
#define CNG_MASK 					0x0400
#define TEP_MASK 					0x0800
#define CALL_PROGRESS_MASK			0x1000
#define V32_AUTOMODE_MASK 			0x2000

#define FAX_DETECT_MASK (AUTO_DETECT_MASK|CED_MASK|CNG_MASK \
								|V21_CH2_MASK \
								|TEP_MASK \
								|V27_2400_MASK|V27_4800_MASK \
								|V17_MASK \
								|V29_MASK)
#define DATA_DETECT_MASK (AUTO_DETECT_MASK|V22_MASK)

	/**** digit_CP_mask mask bit definitions ****/

#define DTMF_MASK					0x0001
#define R1_MASK						0x0002
#define R2F_MASK					0x0004
#define R2B_MASK					0x0008
#define DIGIT_ID_MASK				0xFF00

	/**** tone generator constants ****/									
								
#define TX_GEN_SCALE 				7336	/* 32768*10exp(-13 dB/20) */
#define TX_GEN_DUALTONE_SCALE 		5193	/* 32768*10exp(-16 dB/20) */
#define AA_FREQUENCY				14746	/* 8.192*1800 Hz */
#define AC_FREQUENCY1				4915	/* 8.192*600 Hz */
#define AC_FREQUENCY2				24576	/* 8.192*3000 Hz */
#define CED_FREQUENCY				17203	/* 8.192*2100 Hz */
#define CNG_FREQUENCY				9011	/* 8.192*1100 Hz */
#define ECSD_FREQUENCY				17203	/* 8.192*2100 Hz */
#define ECSD_REV_PERIOD				3600u 	/* 8000*0.45 sec */

#define DIAL_FREQUENCY1				2867	/* 8.192*350 Hz	*/
#define DIAL_FREQUENCY2				3604	/* 8.192*440 Hz	*/
#define RINGBACK_FREQUENCY1			3604	/* 8.192*440 Hz	*/
#define RINGBACK_FREQUENCY2			3932	/* 8.192*480 Hz	*/
#define RINGBACK_PERIOD				48000u	/* 8000*6 sec */
#define RINGBACK_ON_TIME			16000u	/* 8000*2 sec */
#define REORDER_FREQUENCY1			3932	/* 8.192*480 Hz */
#define REORDER_FREQUENCY2			5079	/* 8.192*620 Hz	*/
#define REORDER_PERIOD				4000u	/* 8000*0.5 sec	*/
#define REORDER_ON_TIME				2000u	/* 8000*0.25 sec */
#define BUSY_FREQUENCY1				3932	/* 8.192*480 Hz	*/
#define BUSY_FREQUENCY2				5079	/* 8.192*620 Hz	*/
#define BUSY_PERIOD					8000u	/* 8000*1 sec */
#define BUSY_ON_TIME				4000u	/* 8000*0.5 sec	*/

/**** memory ****/

#define TX_GEN_MEMBERS \
	short frequency1; \
	unsigned short vco_memory1; \
	short scale1; \
	short frequency2; \
	unsigned short vco_memory2; \
	short scale2; \
	unsigned short cad_memory; \
	unsigned short cad_period; \
	unsigned short on_time; \
	unsigned short rev_memory;  \
	unsigned short rev_period; \
	short *digit_ptr

struct TX_GEN_BLOCK
	{
	TX_CONTROL_MEMBERS;
	TX_GEN_MEMBERS;
	};

#define RX_DET_MEMBERS \
	short SNR_est_coef; \
	short SNR_thr_coef; \
	short broadband_level; \
	short level_312; \
	short level_406; \
	short level_500; \
	short level_600; \
	short level_980; \
	short level_1000; \
	short level_1100; \
	short level_1180; \
	short level_1200; \
	short level_1375; \
	short level_1531; \
	short level_1650; \
	short level_1700; \
	short level_1750; \
	short level_1800; \
	short level_1850; \
	short level_2000; \
    short level_2100; \
	short level_2225; \
	short level_2250; \
	short level_2400; \
	short level_2600; \
	short level_2850; \
	short level_2900; \
	short level_3000; \
	short filter_mask_low; \
	short filter_mask_high; \
	unsigned short CP_detect_counter; \
	unsigned short CP_corr_register; \
	unsigned short CP_corr_mask; /* this is a spare used by NORTH_AMERICA_PSTN */  \
	short v32_automode_counter; \
	short (*digit_detector)(struct RX_BLOCK *); \
	short *digit_ptr;  \
	short Pbb; \
	short digit_ID; \
	short GF_len; \
	short GF_counter; \
	short num_filters; \
	short max_row; \
	short max_col; \
	short digit_threshold; \
	short Gk_scale

struct RX_DET_BLOCK
	{
	RX_CONTROL_MEMBERS;
	RX_DET_MEMBERS;
	};

/*
 * Create alias for "detect_counter" to use inactive member "v32_automode_counter"
 * This is associated with V8bis detector and is under development. Should
 * be named "v8bis_Dcounter" or similar.
 */
#define detect_counter		v32_automode_counter

	/**** functions ****/

extern void Tx_init_tone_gen(struct START_PTRS *);
extern void Tx_init_tone(short, struct START_PTRS *);
extern short Tx_digit_state(struct TX_BLOCK *);
extern short Tx_tone_gen(struct TX_BLOCK *);

extern void Rx_init_detector(struct START_PTRS *);
extern void set_Rx_detector_mask(short, struct START_PTRS *);
extern void set_Rx_digit_CP_mask(short, struct START_PTRS *);

	/**** macros ****/

#define Tx_init_CED(PTR) \
	Tx_init_tone_gen(PTR); \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->frequency1=CED_FREQUENCY; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->state_ID=TX_CED_ID
#define Tx_init_CNG(PTR) \
	Tx_init_tone_gen(PTR); \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->frequency1=CNG_FREQUENCY; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->state_ID=TX_CNG_ID
#define Tx_init_ECSD(PTR) \
	Tx_init_tone_gen(PTR); \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->frequency1=ECSD_FREQUENCY; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->rev_period=ECSD_REV_PERIOD; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->rev_memory=1; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->state_ID=TX_ECSD_ID
#define Tx_init_dialtone(PTR) \
	Tx_init_tone_gen(PTR); \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->frequency1=DIAL_FREQUENCY1; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->frequency2=DIAL_FREQUENCY2; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->scale1=TX_GEN_DUALTONE_SCALE; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->scale2=TX_GEN_DUALTONE_SCALE; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->state_ID=TX_DIALTONE_ID
#define Tx_init_ringback(PTR) \
	Tx_init_tone_gen(PTR); \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->frequency1=RINGBACK_FREQUENCY1; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->frequency2=RINGBACK_FREQUENCY2; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->scale1=TX_GEN_DUALTONE_SCALE; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->scale2=TX_GEN_DUALTONE_SCALE; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->cad_period=RINGBACK_PERIOD; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->on_time=RINGBACK_ON_TIME; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->cad_memory=0; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->state_ID=TX_RINGBACK_ID
#define Tx_init_reorder(PTR) \
	Tx_init_tone_gen(PTR); \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->frequency1=REORDER_FREQUENCY1; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->frequency2=REORDER_FREQUENCY2; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->scale1=TX_GEN_DUALTONE_SCALE; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->scale2=TX_GEN_DUALTONE_SCALE; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->cad_period=REORDER_PERIOD; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->on_time=REORDER_ON_TIME; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->cad_memory=0; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->state_ID=TX_REORDER_ID
#define Tx_init_busy(PTR) \
	Tx_init_tone_gen(PTR); \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->frequency1=BUSY_FREQUENCY1; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->frequency2=BUSY_FREQUENCY2; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->scale1=TX_GEN_DUALTONE_SCALE; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->scale2=TX_GEN_DUALTONE_SCALE; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->cad_period=BUSY_PERIOD; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->on_time=BUSY_ON_TIME; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->cad_memory=0; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->state_ID=TX_BUSY_ID

/****************************************************************************/
#endif /* GENDET_INCLUSION_ */
//}