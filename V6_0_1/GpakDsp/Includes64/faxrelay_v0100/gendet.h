/***************************************************************************
 * $RCSfile: gendet.h,v $                
 * $Revision:   1.1  $              
 * Revision $Author:   rfuchs  $                              
 * Revision $Date:   Nov 29 2007 11:47:10  $            
 * Author of Origin: Peter B. Miller             
 * Date of Origin: 09-18-96  
 *                   
 * Company: MESi               
 * Website: www.mesi.net             
 * Description: Structure definitions and extern references for gendet.								
 *                   
 * Copyright (C) MESi 1996-2006, all rights reserved.         
 ****************************************************************************/

#if !defined(GENDET_INCLUSION_)
#define GENDET_INCLUSION_

	/**** state_ID definitions ****/

#define TX_TONE_GEN_ID 				0x1200
#define TX_CNG_ID 					0x1211
#define TX_CED_ID 					0x1212
#define TX_ANS_ID 					0x1212
#define TX_ECSD_ID 					0x1213
#define TX_ANSAM_ID 				0x1214
#define TX_BELL_2225_ID 			0x1215
#define TX_CALL_PROGRESS_ID			0x1240
#define TX_DIALTONE_ID				0x1241
#define TX_RINGBACK_ID				0x1242
#define TX_REORDER_ID				0x1243
#define TX_BUSY_ID					0x1244

#define RX_ENERGY_DET_ID 			0x1000
#define RX_SIG_ANALYSIS_ID 			0x1100
#define RX_TONE_ID 					0x1200
#define RX_CNG_ID 					0x1211
#define RX_CED_ID 					0x1212
#define RX_ANS_ID 					0x1212
#define RX_ECSD_ID 					0x1213
#define RX_ANSAM_ID 				0x1214
#define RX_BELL_2225_ID 			0x1215
#define RX_TEP_1700_ID				0x1217
#define RX_TEP_1800_ID				0x1218
#define RX_CALL_PROGRESS_ID			0x1240
#define RX_DIALTONE_ID				0x1241
#define RX_RINGBACK_ID				0x1242
#define RX_REORDER_ID				0x1243
#define RX_BUSY_ID					0x1244

#define RX_V32_AUTOMODE_ID 			0x1500
#define RX_V32_CED_ID 				0x1501
#define RX_V32_ECSD_ID 				0x1502
#define RX_V32_AA_ID				0x1504
#define RX_V32_AC_ID				0x1508
#define RX_V32_USB1_ID				0x1510

#define RX_DETECT_V17_ID			0x1700			
#define RX_DETECT_V21_CH1_ID        0x2101
#define RX_DETECT_V21_CH2_ID        0x2102
#define RX_DETECT_V22C_ID			0x220C			
#define RX_DETECT_BELL_2225_ID		0x2225
#define RX_DETECT_V27_2400_ID       0x2724
#define RX_DETECT_V27_4800_ID       0x2748
#define RX_DETECT_V29_ID            0x2900

	/**** detector_mask mask bit definitions ****/

#define AUTO_DETECT_MASK 			0x0001
#define V21_CH2_MASK 				0x0004
#define V22_MASK 					0x0008
#define V27_2400_MASK 				0x0010
#define V27_4800_MASK 				0x0020
#define V29_MASK 					0x0040
#define V17_MASK 					0x0080
#define ANS_MASK 					0x0100
#define CED_MASK 					0x0100
#define BELL_2225_MASK 				0x0200
#define CNG_MASK 					0x0400
#define TEP_MASK 					0x0800
#define CALL_PROGRESS_MASK			0x1000
#define V32_AUTOMODE_MASK 			0x2000

#define FAX_DETECT_MASK (AUTO_DETECT_MASK|CED_MASK|CNG_MASK \
								|V21_CH2_MASK \
								|TEP_MASK \
								|V27_2400_MASK|V27_4800_MASK \
								|V17_MASK \
								|V29_MASK)
#define DATA_DETECT_MASK (AUTO_DETECT_MASK|V22_MASK)

	/**** digit_CP_mask mask bit definitions ****/

#define DTMF_MASK					0x0001
#define R1_MASK						0x0002
#define R2F_MASK					0x0004
#define R2B_MASK					0x0008
#define DIGIT_ID_MASK				0xFF00

	/**** tone generator constants ****/									
								
#define TX_GEN_SCALE 				7336	/* 32768*10exp(-13 dB/20) */
#define TX_GEN_DUALTONE_SCALE 		5193	/* 32768*10exp(-16 dB/20) */
#define AA_FREQUENCY				14746	/* 8.192*1800 Hz */
#define AC_FREQUENCY1				4915	/* 8.192*600 Hz */
#define AC_FREQUENCY2				24576	/* 8.192*3000 Hz */
#define CED_FREQUENCY				17203	/* 8.192*2100 Hz */
#define CNG_FREQUENCY				9011	/* 8.192*1100 Hz */
#define ECSD_FREQUENCY				17203	/* 8.192*2100 Hz */
#define ECSD_REV_PERIOD				3600u 	/* 8000*0.45 sec */

#define DIAL_FREQUENCY1				2867	/* 8.192*350 Hz	*/
#define DIAL_FREQUENCY2				3604	/* 8.192*440 Hz	*/
#define RINGBACK_FREQUENCY1			3604	/* 8.192*440 Hz	*/
#define RINGBACK_FREQUENCY2			3932	/* 8.192*480 Hz	*/
#define RINGBACK_PERIOD				48000u	/* 8000*6 sec */
#define RINGBACK_ON_TIME			16000u	/* 8000*2 sec */
#define REORDER_FREQUENCY1			3932	/* 8.192*480 Hz */
#define REORDER_FREQUENCY2			5079	/* 8.192*620 Hz	*/
#define REORDER_PERIOD				4000u	/* 8000*0.5 sec	*/
#define REORDER_ON_TIME				2000u	/* 8000*0.25 sec */
#define BUSY_FREQUENCY1				3932	/* 8.192*480 Hz	*/
#define BUSY_FREQUENCY2				5079	/* 8.192*620 Hz	*/
#define BUSY_PERIOD					8000u	/* 8000*1 sec */
#define BUSY_ON_TIME				4000u	/* 8000*0.5 sec	*/

/**** memory ****/

#define TX_GEN_MEMBERS \
	short frequency1; \
	unsigned short vco_memory1; \
	short scale1; \
	short frequency2; \
	unsigned short vco_memory2; \
	short scale2; \
	unsigned short cad_memory; \
	unsigned short cad_period; \
	unsigned short on_time; \
	unsigned short rev_memory;  \
	unsigned short rev_period; \
	short *digit_ptr

struct TX_GEN_BLOCK
	{
	TX_CONTROL_MEMBERS;
	TX_GEN_MEMBERS;
	};

#define RX_DET_MEMBERS \
	short SNR_est_coef; \
	short SNR_thr_coef; \
	short broadband_level; \
	short level_312; \
	short level_406; \
	short level_500; \
	short level_600; \
	short level_980; \
	short level_1000; \
	short level_1100; \
	short level_1180; \
	short level_1200; \
	short level_1375; \
	short level_1531; \
	short level_1650; \
	short level_1700; \
	short level_1750; \
	short level_1800; \
	short level_1850; \
	short level_2000; \
    short level_2100; \
	short level_2225; \
	short level_2250; \
	short level_2400; \
	short level_2600; \
	short level_2850; \
	short level_2900; \
	short level_3000; \
	short filter_mask_low; \
	short filter_mask_high; \
	unsigned short CP_detect_counter; \
	unsigned short CP_corr_register; \
	unsigned short CP_corr_mask; /* this is a spare used by NORTH_AMERICA_PSTN */  \
	short v32_automode_counter; \
	short (*digit_detector)(struct RX_BLOCK *); \
	short *digit_ptr;  \
	short Pbb; \
	short digit_ID; \
	short GF_len; \
	short GF_counter; \
	short num_filters; \
	short max_row; \
	short max_col; \
	short digit_threshold; \
	short Gk_scale

struct RX_DET_BLOCK
	{
	RX_CONTROL_MEMBERS;
	RX_DET_MEMBERS;
	};

/*
 * Create alias for "detect_counter" to use inactive member "v32_automode_counter"
 * This is associated with V8bis detector and is under development. Should
 * be named "v8bis_Dcounter" or similar.
 */
#define detect_counter		v32_automode_counter

	/**** functions ****/

extern void Tx_init_tone_gen(struct START_PTRS *);
extern void Tx_init_tone(short, struct START_PTRS *);
extern short Tx_digit_state(struct TX_BLOCK *);
extern short Tx_tone_gen(struct TX_BLOCK *);

extern void Rx_init_detector(struct START_PTRS *);
extern void set_Rx_detector_mask(short, struct START_PTRS *);
extern void set_Rx_digit_CP_mask(short, struct START_PTRS *);

	/**** macros ****/

#define Tx_init_CED(PTR) \
	Tx_init_tone_gen(PTR); \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->frequency1=CED_FREQUENCY; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->state_ID=TX_CED_ID
#define Tx_init_CNG(PTR) \
	Tx_init_tone_gen(PTR); \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->frequency1=CNG_FREQUENCY; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->state_ID=TX_CNG_ID
#define Tx_init_ECSD(PTR) \
	Tx_init_tone_gen(PTR); \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->frequency1=ECSD_FREQUENCY; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->rev_period=ECSD_REV_PERIOD; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->rev_memory=1; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->state_ID=TX_ECSD_ID
#define Tx_init_dialtone(PTR) \
	Tx_init_tone_gen(PTR); \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->frequency1=DIAL_FREQUENCY1; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->frequency2=DIAL_FREQUENCY2; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->scale1=TX_GEN_DUALTONE_SCALE; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->scale2=TX_GEN_DUALTONE_SCALE; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->state_ID=TX_DIALTONE_ID
#define Tx_init_ringback(PTR) \
	Tx_init_tone_gen(PTR); \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->frequency1=RINGBACK_FREQUENCY1; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->frequency2=RINGBACK_FREQUENCY2; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->scale1=TX_GEN_DUALTONE_SCALE; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->scale2=TX_GEN_DUALTONE_SCALE; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->cad_period=RINGBACK_PERIOD; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->on_time=RINGBACK_ON_TIME; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->cad_memory=0; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->state_ID=TX_RINGBACK_ID
#define Tx_init_reorder(PTR) \
	Tx_init_tone_gen(PTR); \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->frequency1=REORDER_FREQUENCY1; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->frequency2=REORDER_FREQUENCY2; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->scale1=TX_GEN_DUALTONE_SCALE; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->scale2=TX_GEN_DUALTONE_SCALE; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->cad_period=REORDER_PERIOD; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->on_time=REORDER_ON_TIME; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->cad_memory=0; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->state_ID=TX_REORDER_ID
#define Tx_init_busy(PTR) \
	Tx_init_tone_gen(PTR); \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->frequency1=BUSY_FREQUENCY1; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->frequency2=BUSY_FREQUENCY2; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->scale1=TX_GEN_DUALTONE_SCALE; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->scale2=TX_GEN_DUALTONE_SCALE; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->cad_period=BUSY_PERIOD; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->on_time=BUSY_ON_TIME; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->cad_memory=0; \
	((struct TX_GEN_BLOCK *)((struct START_PTRS *)PTR)->Tx_block_start)->state_ID=TX_BUSY_ID

/****************************************************************************/
#endif /* GENDET_INCLUSION_ */

