/*************************************************************************
 * $RCSfile: hdlc.h,v $
 * $Revision:   1.1  $
 * $Date:   Nov 29 2007 11:47:10  $
 * $Author:   rfuchs  $
 * Company: MESi
 * Description: Hdc IP Network Interface for fax relay
 *                   
 * Copyright (C) MESi 1996-2006, all rights reserved.         
 ***************************************************************************/
#ifndef __HDLC__
#define __HDLC__
struct HdlcStruct {
    unsigned short fcs;
    unsigned short runningCount;
    unsigned short stuff;
    unsigned short onescount;
    unsigned short raw;
    unsigned short bitcount;
    unsigned short flags;
};
#define HDC_NO_DATA     	-1  
#define HDC_FLAG        	-2  
#define HDC_GOOD_CRC    	-3  
#define HDC_BAD_CRC     	-4  
#define HDC_FIVE_ONES   	1   
#define HDC_SIX_ONES    	2   
#define HDC_RX_DATA     	4   
#define HDC_FLAG_SEARCH 	8   

#define HdcGetCrc(h) ((h)->fcs)
#define HdcInvertCrc(h) ((h)->fcs^=0xffff)
#define HdcResetCrc(h) ((h)->fcs=0xffff)
#define HdcCheckCrc(h) ((h)->fcs == 0x1D0F?TRUE:FALSE)
#define HdcLastFrameSize(h) ((short)(h)->runningCount)
void HdcPartialFcs(short currentByte, struct HdlcStruct *h);
/*short HdcTxOctets(struct HdlcStruct *h,unsigned char *b,short n,unsigned char *ret);*/
short HdcTxOctet(struct HdlcStruct *h,unsigned char b,unsigned char *ret);
short HdcUnstuffOctet(struct HdlcStruct *h,short b);
short HdcUnstuffBit(struct HdlcStruct *h,short b);
short HdcTxCrc(struct HdlcStruct *h,unsigned char *ret);
void HdcTxFlag(struct HdlcStruct *h,unsigned char *ret);
void HdcInit(struct HdlcStruct *h);

/****************************************************************************/
#endif /* __HDLC__ */

