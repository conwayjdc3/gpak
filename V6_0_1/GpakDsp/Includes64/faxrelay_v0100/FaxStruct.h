/*************************************************************************
* $RCSfile: FaxStruct.h,v $
* $Revision:   1.1  $
* $Date:   Nov 29 2007 11:47:10  $
* $Author:   rfuchs  $
* Company: MESi
* Description: Fax relay structure definition
*
* Copyright (C) MESi 1996-2006, all rights reserved.
***************************************************************************/
#if !defined(__FAXSTRUCT___)
#define __FAXSTRUCT___

struct FaxStruct
{
    struct ModemIfStruct *ModemIf;	/* address of Modem Interface structure */
    struct SeqStruct *Seq;          /* address of Sequencer structure */
    struct START_PTRS *start_ptrs;	/* modem channel memory address table */
    short RelayFunction;			/* relay type specifier */				
	short channel;					/* channel number */
#ifdef SEQ_COMM_LINK
    struct ippcStruct *comm;		/* for the external communications link */
#endif
};
/*****************************************************************************/
#endif /* __FAXSTRUCT___ */
