/*************************************************************************
 * $RCSfile: t30.h,v $
 * $Revision:   1.1  $
 * $Date:   Nov 29 2007 11:47:10  $
 * $Author:   rfuchs  $
 * Company: MESi
 * Description: Sequencer Code
 *                   
 * Copyright (C) MESi 1996-2006, all rights reserved.         
 ***************************************************************************/

#ifndef __T30__
#define __T30__

#ifdef SNOOP_CALL_LOG
	#include <stdio.h>
#endif

#include "bufmgr.h"
#include "hdlc.h"
#ifdef INTERCEPT_SUPPORT
	#include "ecm.h"
#endif /* INTERCEPT_SUPPORT */

#define T30_TCP                 		1
#define T30_UDP                 		2
#define T30_REDUNDANT_UDP       		4
#define T30_NATIVE              		8
#define T30_T38                 		0x10

/*
 * Buffer offsets for the standard header as defined in our spec.
 */
#define T30_BUF_OFFSET_TYPE				0
#define T30_BUF_OFFSET_TIME_LSB			1
#define T30_BUF_OFFSET_TIME_MSB			2
#define T30_BUF_OFFSET_DATA				3

#define T30_LCL_UNTRAINED				0
#define T30_LCL_TRAINED_GOOD			1
#define T30_LCL_TRAINED_BAD				2
#define T30_SPOOF_IDLE 					0
#define T30_SPOOF_CFR  					1
#define T30_SPOOF_FTT  					2

/*
* "T30_SAMPLEBUF_DELAY" is the max delay from when a sample transmitted from
* the sample buffer will appear in the receiver buffer
* "T30_MAX_RT_DELAY_IN_MS" is the max external delay of the network echo; i.e
* a round trip satellite hop
*/
#define T30_MAX_RT_DELAY_IN_MS 			50
#define T30_SAMPLEBUF_DELAY 			500
#define T30_IO_DELAY 					125

#define RELAY 							0
#define INTERCEPT 						1
#define VON 							2

#ifdef INTERCEPT_SUPPORT
struct V21MsgStruct
	{
	int len;
	unsigned char data[128];
	};
#define EMBEDDED_LEN 					64
#define EMBEDDED_MSGS 					5
struct EmbeddedV21Struct
	{
	int nmsg;
	int sizes[EMBEDDED_MSGS];
	unsigned char msgs[EMBEDDED_MSGS][EMBEDDED_LEN];
	};
#endif /* INTERCEPT_SUPPORT */

enum TcfProgress
	{
	SearchZero, FirstZero, SecondZero, ThirdZero,
	FirstRun, SecondRun, ThirdRun, GoodTcf
	};

struct ProtocolStruct
	{
	int ModType; /* modulation rate taken out of dcs message & 0x3c */
	int Mode;	/* ecm=1 or non ecm=0 from b27 of the dcs message */
	int Tep;	/* indicates if Tep was detected */
	short NsfDetectFlag; /* Non-standard message detected flag */
	int RcvdStartFaxData; /* flag indicates start of data was detected */
	short *DataPackets;	/* linked list of data packets */
	int TxKeyed;	/* flag set to tell protocol the tx is still keyed */
	int Cd;	/* flag set to tell protocol the receiver has CD */
	short *FmNetV21MsgQ; /* queue of v21 messages received from the
			   network when the local end was receiving a v21 msg*/
	short *V21Collision; /* dis dcs collision buffer pointer */
	int FirstFlagFound;	/* ecm flag indicating first flag detected */
	int DataContMsg[T30_BUF_OFFSET_DATA+4];
	int maxUnpackPerCall; /* max number of bytes unpacked per ms */
	int iUnpack;  /* index used during unpacking a frame */
	short *unpackQueue;	/* unpacked linked list */
	int LocalDelay;
	int TxWord;
	int BitsInTxWord;
	unsigned long Stream;
	short *FramingBuf;
	int FramingCount;
	int FrameNr;
	int TrainingState;
	int TrainingErrors;
	int TrainingBits;
	int Layer3;
	int TxStatusAtFlagDetect;
#ifdef SNOOP_CALL_LOG
	FILE *fhSnoopLog;
#endif
#ifdef INTERCEPT_SUPPORT
	int bitErrors;
	int TcfState;
	int RcvdPartialPage;
	unsigned char DcsData[8];
	unsigned char *faxData;	/* raw bits from the data pump */
	unsigned int sizeFaxData;
	unsigned int offsetFaxData;
	struct ecmstruct ecm;
	unsigned char LogData;
	unsigned char StartNewPage;
	unsigned char pc;  /* page count in pps frame */
	unsigned char bc;  /* block count in pps frame */
	unsigned int SnoopFileWrite;
	char *SnoopFileName;
	unsigned short *ecmblocks;
	int SnoopCount;
	void *Custom;
	struct V21MsgStruct Dis;		/* T.30 DIS message bytes */
	struct V21MsgStruct Csi;		/* T.30 CSI message bytes */
	struct V21MsgStruct Nsf;		/* T.30 NSF message bytes */
	struct V21MsgStruct Dtc;		/* T.30 DTC message bytes */
	struct V21MsgStruct Cig;		/* T.30 CIG message bytes */
	struct V21MsgStruct Nsc;		/* T.30 NSC message bytes */
	struct V21MsgStruct Dcs;		/* T.30 DCS message bytes */
	struct V21MsgStruct Tsi;		/* T.30 TSI message bytes */
	struct V21MsgStruct Nss;		/* T.30 NSS message bytes */
	int OnePagePerFile;
	int NssInBurst;
#endif /* INTERCEPT_SUPPORT */
	int UdpTxSeqNumber;
	int UdpRxSeqNumber;
	short TxSecondaryPacketA[BMR_SIZE_BUFFER];
	short TxSecondaryPacketB[BMR_SIZE_BUFFER];
#ifdef T38
	unsigned char T38V21Bytes[3];
	int FaxTep; // used in t38.c
	int FaxMode; // used in t38.c
	short *t38V21; /* V21 partial frame buffer */
	struct HdlcStruct t38TxHdlc;
	struct HdlcStruct t38RxHdlc;
	int t38ModType;/* modulation rate defined as a t38 indicator enum */
	unsigned int InitFlagNeeded;
	unsigned int bytesInFrame;/* number of byte in t38 hdlc frame*/
	unsigned int Ecm64;	/* false if ecm frames of 256, true for 64 */
	unsigned int t38StartSent; /*indicates if start of fax data has been sent*/
	int T38PreCorr;
#endif /* T38 */
#ifdef VOICE
	short *VoiceQueue;
	int VoiceState;
#endif /* VOICE */
#ifdef UNPACK_MODS
	unsigned long mask;
	unsigned long match;
#endif
	int FrameLen;
	int shift;
	};

extern void T30_init( struct SeqStruct *,short *,struct ProtocolStruct *);
extern int T30InitBuffer(struct SeqStruct *q,short **pbuf,int type);
extern int T30InitBufferwData(struct SeqStruct *q,short **pbuf,int type,int data);
#if (defined(BUFLOG) | defined(_MSWINDOWS) | defined(STDIO)) & defined(STATE_TRACE)
#include "sequence.h"
extern void T30LogStateTransition(struct SeqStruct *, struct ETS *);
#endif /* (BUFLOG | _MSWINDOWS | STDIO) & STATE_TRACE */
#if defined(BUFLOG) & (defined(TX_V21MSG_TRACE)|defined(RX_V21MSG_TRACE))
extern void T30BufLogV21MsgDetails(short *);
#endif /* BUFLOG & (TX_V21MSG_TRACE|RX_V21MSG_TRACE) */

#define T30WriteHeader(q,loc,data,buf)  BmrWriteOctet(q,loc,data,buf)
#define T30WriteOctet(q,loc,data,buf)  BmrWriteOctet(q,loc+T30_BUF_OFFSET_DATA,data,buf)
#define T30WriteWord(q,loc,data,buf)  BmrWriteWord(q,loc+T30_BUF_OFFSET_DATA,data,buf)
#define T30ReadHeader(offset,buf) BmrReadOctet(offset,buf)
#define T30ReadOctet(offset,buf)  BmrReadOctet(offset+T30_BUF_OFFSET_DATA,buf)
#define T30ReadWord(offset,buf)  (BmrReadOctet(offset+T30_BUF_OFFSET_DATA,buf)|\
								  (BmrReadOctet(offset+T30_BUF_OFFSET_DATA+1,buf)<<8))

#define T30_MSG_STOP					0
#define T30_MSG_START					1

	/**** internal T30 type definitions (see Spec. section 5.2)  ****/

#define T30_MSG_TYPE_NO_SIGNAL			0	
#define T30_MSG_TYPE_CED        		1
#define T30_MSG_TYPE_CNG        		2
#define T30_MSG_TYPE_V21FLAG    		3
#define T30_MSG_TYPE_V21MSG     		4
#define T30_MSG_TYPE_DATA_START 		5
#define T30_MSG_TYPE_DATA_CONT  		6
#define T30_MSG_TYPE_FAX_CTRL   		7
#define T30_MSG_TYPE_MODEM_CHANGE 		8
#define T30_MSG_TYPE_RCV_CONF   		9
#define T30_MSG_TYPE_CALL_PROGRESS 		10
#define T30_MSG_TYPE_DIAL_NUMBER 		11
#define T30_MSG_TYPE_AUDIO      		12
#define T30_MSG_TYPE_GENDET_RESTART 	13
#define T30_MSG_TYPE_V21BYTE     		14 // must match MIF_TX_V21BYTE and T38

#define T30_MSG_CED_START				1
#define T30_MSG_CED_STOP				0
#define T30_MSG_CNG_START				1
#define T30_MSG_CNG_STOP				0
#define T30_MSG_TEP1700    				0x17
#define T30_MSG_TEP1800    				0x18
#define T30_MSG_MODE_TRAIN				2
#define T30_MSG_MODE_ECM				1
#define T30_MSG_MODE_NONECM				0
#define T30_MSG_TX_START				1
#define T30_MSG_TX_STOP					2
#define T30_MSG_CD_ON					3
#define T30_MSG_CD_OFF					4
#define T30_MSG_ABORT					1
#define T30_MODEM_RESET     			2

#define FCF_INIT 						0x80
#define FCF_RESP 						0x00
#define FCF_DTC  						0x81
#define FCF_CIG  						0x82
#define FCF_NSC  						0x84
#define FCF_DIS  						0x01
#define FCF_CSI  						0x02
#define FCF_PWD  						0x03
#define FCF_NSF  						0x04
#define FCF_CFR  						0x21
#define FCF_FTT  						0x22
#define FCF_CTR_T4  					0x23
#define FCF_CTR  						0x24
#define FCF_MCF  						0x31
#define FCF_RTP  						0x33
#define FCF_RTN  						0x32
#define FCF_PIP  						0x35
#define FCF_PIN  						0x34
#define FCF_RNR  						0x37
#define FCF_ERR  						0x38
#define FCF_PPR  						0x3D
#define FCF_FDM  						0x3F
#define FCF_DCS  						0x41
#define FCF_TSI  						0x42
#define FCF_SUB  						0x43
#define FCF_NSS  						0x44
#define FCF_SID  						0x45
#define FCF_CTC  						0x48
#define FCF_FNV  						0x53
#define FCF_CRP  						0x58
#define FCF_DCN  						0x5F
#define FCF_EOM  						0x71
#define FCF_MPS  						0x72
#define FCF_EOR  						0x73
#define FCF_EOP  						0x74
#define FCF_RR  						0x76
#define FCF_PRI_EOS 					0x78
#define FCF_PRI_EOM 					0x79
#define FCF_PRI_MPS 					0x7A
#define FCF_PRI_EOP 					0x7C
#define FCF_PPS  						0x7D
/*
#define FCF_EOM_T4  					0xF1
#define FCF_MPS_T4  					0xF2
#define FCF_EOP_T4  					0xF4
#define FCF_PRI_EOM_T4 					0xF9
#define FCF_PRI_MPS_T4 					0xFA
#define FCF_PRI_EOP_T4 					0xFC
*/
#define T30SetProtocol(f,p) ( ((struct FaxStruct*)f)->Seq->Protocol->Layer3=p)
#endif /* __T30__ */
