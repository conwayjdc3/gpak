// The following constants and structures are used to internally size RTP data structures.
// This file must NOT be changed by the application developer.
#ifndef  __RTPUSER_H
#define __RTPUSER_H
//==========================================================================

//         File:    rtpuser.h
//  Description:    RTP/RTCP Application Program Interface header file.

//==========================================================================

#ifdef __BIOS__ 
   #include <adt_typedef_user.h>
#else
   #include <common/include/adt_typedef_user.h>
#endif

//==========================================================================
//   typedef and structure definitions
//==========================================================================
typedef enum { RTPRECEIVE, RTCPRECEIVE, RTPSEND, RTCPSEND} RTCPEVENTS;
typedef enum { PKT_ON_PROBATION =  0, PKT_PURGE_EXISTING,     PKT_VALID, 
               PKT_OUT_OF_RANGE =  3, PKT_OUT_OF_WINDOW,      PKT_SEQ_DUPLICATE, 
               PKT_OUT_OF_MEMORY=  6, PKT_SESSION_NOT_ACTIVE, PKT_VERSION, 
               PKT_PAYLOAD_SIZE =  9, PKT_FORMAT,             PKT_VOICE_IN_TONE_WINDOW, 
               PKT_LATE         = 12, PKT_TONE_ADJUST,        PKT_UNDERFLOW_ADJUST, 
               PKT_OVERFLOW_ADJUST = 15,
               PKT_INVALID = 0x1000 }
   PKTSTATUS;

typedef void NETHANDLE;

typedef ADT_UInt32 CLOCKFUNCTION (ADT_Int32 ChanId);
typedef ADT_Int16  NETFUNCTION   (NETHANDLE*, ADT_UInt32* data, ADT_Int32* len);
typedef PKTSTATUS  RTPVALIDATION (ADT_UInt32* data, ADT_UInt16 len, ADT_UInt16 type);
typedef ADT_UInt32 TONEDURATION  (ADT_Int32 ChanId, ADT_UInt16 payType, ADT_UInt16 *paybuff);

typedef struct RTPCONNECT RTPCONNECT;

//==========================================================================
//   External interfaces
//==========================================================================
extern ADT_UInt32 RTP_DROPOUT;   // maximum delta allowed for sequence # gap.
extern ADT_UInt32 RTP_MISORDER;  // maximum delta allowed for sequence # out of order.
extern const int RTP_ADT_DataI8;


#ifdef RTCP
   #ifdef __BIOS__ 
      #include "rtcpuser.h"
   #else
      #include "rtp/include/rtcpuser.h"
   #endif
#endif

#ifndef RTCP_STATE_I32
   #define RTCP_STATE_I32 0
#endif
#define RTP_STATE_I32   50    //  Size of struct RTPDATA without rtcp data
#define RTP_RTCP_STATE_I32 (RTP_STATE_I32 + RTCP_STATE_I32)


struct RTPCONNECT {
    ADT_UInt32 structure_data [RTP_RTCP_STATE_I32];
};   // RTPCONNECT Structure sized to internal RTPDATA structure

// RTP_ADT_Init return status codes:
typedef enum RTP_STAT {
   RTP_OPEN_STAT_SUCCESS = 0,       // open success
   RTP_OPEN_STAT_FAIL_SIZE = -1,    // open fails due to instance size error
   RTP_OPEN_STAT_FAIL_API  = -2     // open fails due to API mismatch
} RTP_STAT;


// configuration structure
typedef struct rtpConfig_APIV5_t {
    CLOCKFUNCTION   *ClockFunction;
    NETFUNCTION     *NetTransmit;
    RTPVALIDATION   *ProfileValidation;
    TONEDURATION    *ToneDurationFunction;
    ADT_UInt32      SSRC;
    ADT_UInt32      RandomSeed;
    ADT_UInt16      ProbationCount; // In packets
    ADT_UInt16      DelayTargetMin; // In RTP sample units
    ADT_UInt16      DelayTarget;    // In RTP sample units
    ADT_UInt16      DelayTargetMax; // In RTP sample units
    ADT_UInt16      JitterMode;     // 0 = static
                                    // 1 = 
    ADT_UInt16      JB_Max;         // Nax physical size of the jitter buffer in timestamps units
    ADT_UInt16      SampleRate;     // Frequency (Hz) of sample clock
    ADT_UInt16      StartSequence;  // First sequence number to transmit
    ADT_UInt16      ChanId;         // chan Id of this instance
    ADT_UInt16      APIVersion;     // API Version ID
    // Bit mapped fields
    unsigned int   Pad32:1;                  // 32-bit padding of payload
    int   Dummy1:15; 
    int   Dummy2:16;
} rtpConfig_APIV5_t;               
#define ADT_RTP_API_VERSION  5

//{   RTPSTATVars
#define RTPSTATVars   \
   ADT_UInt32 BaseSeq,  BaseCycles;   \
   ADT_UInt32 PktsSent, BytesSent;    \
   ADT_UInt32 PktsReceived;     /* Accepted from network and placed in jitter buffer   */ \
   ADT_UInt32 PktsPlayed;       /* Delivered for playout                               */ \
   ADT_UInt32 CurrentCnt;       /* Count of packets currently buffered                 */ \
                                                                                          \
   ADT_UInt32 OverflowCnt;      /* Count of buffer underflow adjustments               */ \
   ADT_UInt32 UnderflowCnt;     /* Count of buffer overflow adjustments                */ \
                                                                                          \
   ADT_UInt32 PurgedCnt;        /* Packets purged due to time resync or tone overlap   */ \
   ADT_UInt32 OldCnt;           /* Pakcets purged due to being too old                 */ \
                                                                                       \
   ADT_UInt32 InvSequenceCnt;   /* Packets dropped and not counted in PktsReceived     */ \
                                /* due to sequence number duplicates or discontinuities */ \
                                                                                       \
   ADT_UInt32 OutOfRangeCnt;    /* Packets dropped due to timestamp out of range       */ \
   ADT_UInt32 ToneOverlapCnt;   /* Packets dropped due to tone overlap                 */ \
   ADT_UInt32 AllocFailures;    /* Packets dropped due to memory allocation failures   */ \
   ADT_UInt32 LateCnt;          /* Packets dropped due to lateness                     */ \
//}

typedef struct RTPStatistics_t {
   /* This part comes directly from RTPStats */
   RTPSTATVars
   /* Additional statistics */
   ADT_Int32  JitterEstimate;    // current estimate of packet jitter
   ADT_Int16  JitterBufferDelay; // current playout delay in samples
   ADT_UInt8  fractionPktsLost;  // Fractional number (units of 1/256) of packets lost
   ADT_UInt32 extendedMaxSeq;    // Maximum (extended upon wrap) sequence number received
   ADT_UInt32 pktsExpected;      // Packets expected since last RTCP transmission
   ADT_UInt32 pktsReceived;      // Count of packets received since last RTCP transmission
   ADT_UInt32 cumPktsExpected;   // Calculation of total packets expected since start
   ADT_UInt32 cumPktsLost;       // Calculation of total number of packets lost since start

} RTPStatistics_t;


//------------------------------------------------------
//  Application supplied RTP support function prototypes
ADT_API void *RTPAlloc (ADT_UInt32 size);
ADT_API void RTPFree   (void *);
ADT_API void RTPLock   (ADT_Int32 instId);
ADT_API void RTPUnlock (ADT_Int32 instId);
ADT_API void RTPWait   (void);


//------------------------------------------------------
//  RTP API prototypes
ADT_API void RTP_ADT_Instance_Init (RTPCONNECT *RTPConnect, int instId);
ADT_API RTP_STAT RTP_ADT_Init (RTPCONNECT *RTPConnect, NETHANDLE *RTPHandle, rtpConfig_APIV5_t *pConfig);

ADT_API ADT_Int16 RTP_ADT_Send (RTPCONNECT *RTPConnect, ADT_UInt32 *PayldAddr, ADT_UInt16 PayldLenI8, 
              ADT_UInt16 PayldType, ADT_UInt32 TimeStamp, ADT_UInt16 Marker);

ADT_API ADT_Int16 RTP_ADT_SendExt (RTPCONNECT *RTPConnect, ADT_UInt32 *PyldAddr, ADT_UInt16 PyldILen8,
                 ADT_UInt16 PyldType,    ADT_UInt32 TimeStamp, ADT_UInt16 Marker,
                 ADT_UInt32 *CSRC,       ADT_UInt32 CSRCCnt,
                 ADT_UInt8  *ExtData,    ADT_UInt32 ExtDataI8Len);

ADT_API PKTSTATUS RTP_ADT_BufferPacket (RTPCONNECT* RTPConnect, ADT_UInt32* PktAddr, ADT_UInt16 PktLenI8);
ADT_API PKTSTATUS RTP_ADT_BufferPacketNc (RTPCONNECT* RTPConnect, ADT_UInt32* PktAddr, ADT_UInt16 PktLenI8, 
                                        void* FreeAddr);

ADT_API ADT_UInt16 RTP_ADT_Receive (RTPCONNECT* RTPConnect, ADT_UInt32* TimeStart, ADT_UInt32 TimeEnd,
                  ADT_UInt32* PktAddr, ADT_UInt16* PktSize, ADT_UInt16 *PayldType);


ADT_API ADT_UInt16 RTP_ADT_ReceiveExt (RTPCONNECT *RTPConnect, ADT_UInt32 *TimeStart, ADT_UInt32 TimeEnd,
                  ADT_UInt32 *PyldAddr, ADT_UInt16 *PyldLenI8, ADT_UInt16 *PyldType,
                  ADT_UInt16 *Marker,   ADT_UInt32 *SSCR,
                  ADT_UInt32 *CSRC,     ADT_UInt32 *CSRCCnt,
                  ADT_UInt8  *ExtData,  ADT_UInt32 *ExtDataI8Len);

ADT_API void RTP_ADT_GetStats(RTPCONNECT *RTPConnect, RTPStatistics_t *pStatistics);
ADT_API void RTPPurge (RTPCONNECT* RTPConnect);
ADT_API void RTP_ADT_Close (RTPCONNECT* RTPConnect);
ADT_API int  RTP_ADT_getInstanceI8 (void);

//------------------------------------------------------
// DLL Specific - used to configure callback functions for DLL 
typedef void *(RTPALLOC_FUNC) (ADT_UInt32 size);
typedef void RTPFREE_FUNC   (void *);
typedef void RTPLOCK_FUNC   (ADT_Int32 instId);
typedef void RTPUNLOCK_FUNC (ADT_Int32 instId);
typedef void RTPWAIT        (void);
typedef void PRINTF_FUNC    (const char *fmt, ...);

typedef struct rtpSupportConfig_t {
   RTPALLOC_FUNC  *rtpAllocFunc;
   RTPFREE_FUNC   *rtpFreeFunc;
   RTPLOCK_FUNC   *rtpLockFunc;
   RTPUNLOCK_FUNC *rtpUnlockFunc;
   RTPWAIT        *rtpWaitFunc;
} rtpSupportConfig_t;

ADT_API ADT_Int16 RTP_ADT_SystemCallbacks (rtpSupportConfig_t *pRtpSupportConfig);


//------------------------------------------------------
// Support for legacy API names
#define RTPInstanceInit  RTP_ADT_Instance_Init
#define RTPOpen          RTP_ADT_Init
#define RTPSend          RTP_ADT_Send
#define RTPSendExt       RTP_ADT_SendExt
#define RTPReceive       RTP_ADT_Receive
#define RTPReceiveExt    RTP_ADT_ReceiveExt
#define RTPBufferPacket  RTP_ADT_BufferPacket
#define RTPClose         RTP_ADT_Close
#define RTPGetStatistics RTP_ADT_GetStats
#define RTPSupportConfig RTP_ADT_SystemCallbacks
#endif
