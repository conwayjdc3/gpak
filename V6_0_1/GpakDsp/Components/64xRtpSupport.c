//==========================================================================
//          File:  64xRtpSupport.c
//   Description:  G.PAK Support routines for RTP and jitter buffer on \
//                 the C64x and 64plus platforms
//==========================================================================
#include <std.h>
//#include <hwi.h>
//#include <swi.h>
#include <string.h>

#include <adt_typedef.h>
#include "GpakDefs.h"
#include "GpakHpi.h"
#include "GpakErrs.h"
#include "GpakExts.h"

#ifdef _DEBUG_RTP_JITTER
//Global DEBUG RTP channel: RTPChan = RtpChanData[]
RTPDATA ADT_RTP_Chan;
RTPDATA *ADT_RTP_Ptr=0;
int firstRTPChan=0;
#endif

#ifdef SRTP_VALIDATE
extern far int srtp_validate;
#endif

#define MAX_U32  ((ADT_UInt64)4294967295U)

#ifdef SINGLE_CORE_STACK
// Check if a received network RTP packet is ready.
extern ADT_UInt32 isNetRtpRxPktReady(void);
#endif
extern int msgCore;

extern void (*RTPPayloadFree) (void *hndl, void*data, ADT_UInt16 dataI8);

#define HI_16(i32) (((i32) >> 16) & 0xffff)
#define LO_16(i32)  ((i32) & 0xffff)

#define SUM_SHIFT 5            // Utilization determination
#define TIME_AVG_CNT 32

ADT_UInt16 RTPScratchI8 = 0;    // Size of RTPScratch buffer
#pragma DATA_SECTION (RTPScratchI8, "PER_CORE_DATA:Scratch")

ADT_UInt32 HostSWIMaxTicks = 0x7fffffff;
#pragma DATA_SECTION (HostSWIMaxTicks, "NON_CACHED_DATA")

GpakTestMode far RTPLoopBack = DisableTest;
#pragma DATA_SECTION (RTPLoopBack, "NON_CACHED_DATA")

ADT_Bool far reportRTPUsage = ADT_FALSE;
#pragma DATA_SECTION (reportRTPUsage, "NON_CACHED_DATA")

static TONEDURATION toneDurationCallback;
extern NETFUNCTION netTransmit;
extern SWI_Handle SWI_Host;
extern int rtpPortValid (int rtpPort);
extern GPAK_RTPConfigStat_t processIPStackFields (ADT_UInt16 *pCmd, int chanId, void *RTPSession);
extern GPAK_RTPConfigStat_t processIPv6StackFields (ADT_UInt16 *pCmd, int chanId, void *RTPSession);
extern void* GetPktFromTransferBuffers (CircBufInfo_t *circ, NCHdr_t *NcHdr);
extern int  PurgeTransferBuffer (CircBufInfo_t* transferBuff);
extern void (*RTPPayloadFree) (void *hndl, void*data, ADT_UInt16 dataI8);

extern void InitG722RTPStat(ADT_UInt16 chanId); // jdc
extern g722RtpDebugStats_t g722RTPStats[];  // jdc
void saveRTPHeader(int rtp_sr, void *rtpBuffer, g722RTPSync_t *g722_rtpStats, ADT_UInt16 PktI8, ADT_UInt32 sr1_count);

static void HOSTSWI ();

// Statistics memory allocation
//#ifdef _DEBUG
#if 1
   typedef enum rtpEvt {
      PKTTONET      = 1,
      PKTTOJITTER   = 2,
      PKTFROMJITTER = 4,
      PKTDENIED     = 8
   } rtpEvt;
   #define RTPLOG_ELEMS (1<<10)
   #define STATS_INC(pool,field)  pool->stats->field++
   #define STATS_DEC(pool,field)  pool->stats->field--
   #define STATS(stmt)  { stmt }
   #define POOLSTATS
   typedef struct poolStats {
      int  total, avail, inUse, allocs, frees, empty, corrupt;
   } poolStats;

   typedef struct RTPModuleStats { 
      unsigned int StatID;
      poolStats smallJB;
      poolStats largeJB;
      poolStats hugeJB;
      struct tx {
         unsigned int total, voice, ulaw, alaw, tone, cng, t38, discards;
         unsigned int sendFailures;
      } tx;
      struct rx {
         unsigned int total, voice, ulaw, alaw, tone, cng, t38;
         unsigned int hdrDiscards;
         unsigned int payDiscards;
         unsigned int folDiscards;
         unsigned int cidDiscards;
         unsigned int inaDiscards;
         unsigned int invDiscards;
         unsigned int srtpDiscards;
         unsigned int JB, underflow, purges;
      } rx;
   } RTPModuleStats;
   RTPModuleStats RTPStats;
   #pragma DATA_SECTION (RTPStats, "PER_CORE_DATA:STATS")

   typedef struct RTPData {
      ADT_UInt16 chan;
      rtpEvt     operation;
      ADT_UInt16 pyldI8;
      ADT_UInt16 pyldType;
      ADT_UInt16 seq;
      ADT_UInt16 hole;
      ADT_UInt32 timeStamp;
      ADT_UInt32 pyld;
      ADT_UInt32 DmaStamp;
   } RTPData;

   typedef struct RTPLog {
      int idx, chan, filter, maxlen;
      RTPData data [RTPLOG_ELEMS];
   } RTPLog;
   RTPLog rtpLog = { 0, -1, 0, 40 };
   #pragma DATA_SECTION (rtpLog, "NON_CACHED_DATA")

void logRtp (int chan, rtpEvt operation, int pyldI8, ADT_UInt32 *rtpHdr) {
   RTPData *log;

   int idx;
   if ((rtpLog.chan != -1) && (rtpLog.chan != chan)) return;
   if ((rtpLog.filter & operation) == 0) return;
   if (rtpLog.maxlen < pyldI8) return;

   SWI_disable ();
   idx = rtpLog.idx++;
   rtpLog.idx = rtpLog.idx % RTPLOG_ELEMS;
   SWI_enable ();

   log = &rtpLog.data[idx % RTPLOG_ELEMS];
   log->chan = chan;
   log->operation = operation;
   log->pyldI8 = pyldI8;
   log->hole = 0x1111;
   if (operation == PKTFROMJITTER) {
      log->pyld      = 0;
      log->seq       = 0;
      log->pyldType  = (rtpHdr[0] >> 16) & 0xffff;
      log->timeStamp = rtpHdr[1];
   } else if (operation == PKTDENIED) {
      log->pyld      = 0;
      log->seq       = 0;
      log->pyldType  = 0;
      log->timeStamp = 0;
   } else {
      idx = 4;
      log->timeStamp = netI32 (rtpHdr, &idx);
      if (pyldI8) {
         idx = 1;
         log->pyldType = netI8 (rtpHdr, &idx);
         idx = 2;
         log->seq      = netI16 (rtpHdr, &idx);
         idx = 12;
         log->pyld     = netI32 (rtpHdr, &idx);
      } else {
         log->pyldType = 0x1111;
         log->seq      = 0x1111;
         log->pyld     = 0x11111111;
      }
   }
   log->DmaStamp = ApiBlock.DmaSwiCnt;
}
 
#else
   #define STATS_INC(pool,field)
   #define STATS_DEC(pool,field)
   #define STATS(stma)
   #undef  POOLSTATS
   #define logRtp(chan, operation, pyldI8, rtpHdr) 
#endif

ADT_UInt32 RTPReserveI32 = 0;   // Amount of I32 memory to reserve before RTPAlloc address.
#pragma DATA_SECTION (RTPReserveI32,  "PER_CORE_DATA")


static far ADT_Bool FrameOverloaded = ADT_FALSE;   // Frame overload indicator

SWI_Handle SWI_Host;  // RTP buffer to jitter buffer transfers
#pragma DATA_SECTION (SWI_Host, "PER_CORE_DATA:SWI_Host")

static SWI_Attrs swi_attr = { (SWI_Fxn) HOSTSWI, 0, 0, 0, 0};

//==========================================================================
//   RTP Buffer Allocation
//
//  smallJB, largeJB, hugeJB
//
//  RTPPoolInit
//  RTPMemInit  
//{
typedef struct memBlk {
   void *next;
   ADT_UInt32 data[1];
} memBlk;

typedef struct memPool {
   void *selfAddr;
   void *startAddr;
   void *endAddr;
   int  maxI8;
   memBlk *free;
#ifdef POOLSTATS 
   poolStats *stats;
#endif
} memPool;

memPool smallJB;
memPool largeJB;
memPool hugeJB;
#pragma DATA_SECTION (smallJB, "PER_CORE_DATA:jitterBuffer")
#pragma DATA_SECTION (largeJB, "PER_CORE_DATA:jitterBuffer")
#pragma DATA_SECTION (hugeJB,  "PER_CORE_DATA:jitterBuffer")

#ifdef _DEBUG  // RTPVars
// Structure to quickly locate RTP variables
typedef struct RTPVars_t {
   SWI_Handle *SWI_Host;
   GpakTestMode  *RTPLoopBack;

   CircBufInfo_t (*RTPCircBuffers)[];
   ADT_UInt16    *ToNetRTPBuffer, *ToDSPRTPBuffer;

   memPool    *smallJB, *largeJB, *hugeJB;
   ADT_UInt32 *SmallJBPool, *LargeJBPool, *HugeJBPool;

   RtpChanData_t    *(*RtpChanData)[];
   SrtpTxInstance_t *(*SrtpTxChanData)[];
   SrtpRxInstance_t *(*SrtpRxChanData)[];

 
   RTPModuleStats *RTPStats;
   RTPLog     *rtpLog;

   ADT_UInt32 *SrtpScratchPtr; 
} RTPVars_t;

#pragma DATA_SECTION (RTPVars, "NON_CACHED_DATA")
const RTPVars_t RTPVars = {
   &SWI_Host, &RTPLoopBack,

   &RTPCircBuffers, ToNetRTPBuffer, ToDSPRTPBuffer,

   &smallJB, &largeJB, &hugeJB, 
   SmallJBPool, LargeJBPool, HugeJBPool,

   &RtpChanData, &SrtpTxChanData, &SrtpRxChanData,

   &RTPStats, &rtpLog,
   SrtpScratchPtr
};
#endif




//
//--------------------------------------------------------------------
// Allocate jitter buffer memory blocks to memory pool's free list.
//--------------------------------------------------------------------
#pragma CODE_SECTION (RTPPoolInit, "SLOW_PROG_SECT")
static int RTPPoolInit (memPool *pool, int blkI32, int poolI32, ADT_UInt32 *mem) {

   memBlk *blk;
   int blkCnt, entries, maxI8;

   memset (pool, 0, sizeof (memPool));
   if (blkI32 == 0) return 0;

   maxI8 = blkI32 * 4;  // Maximum size that may be requested for allocation from pool

   // Calculate addition memory required for pre-RTP headers plus one 32-bit pool 
   // reference pointer for each allocated memory block
   blkI32 += RTPReserveI32 + 1;

   // Align RTP pools on cache boundaries for multi-core systems
   // Align core 0's RTP buffers on cache boundaries for multi-core systems
   if (1 < DSPTotalCores) {
      poolI32 -= 0x1f;
      mem      = (ADT_UInt32 *) (((ADT_Int32) mem) & ~0x7f);
      if (DSPCore == 0) {
         blkI32 = ((blkI32 + 0x1f) & ~0x1f);
         maxI8 = (blkI32 - RTPReserveI32 - 1) * 4;
      }
   }
   blkCnt = poolI32 / blkI32;
   entries = blkCnt;
#ifdef _DEBUG
   memset (mem, 0x11, poolI32 * (sizeof (ADT_UInt32)/sizeof (ADT_UInt8)));
#endif

   blk = (memBlk *) mem;
   pool->selfAddr = pool;
   pool->free = blk;
   pool->startAddr = blk;
   pool->maxI8 = maxI8;

   while (blkCnt--) {
      blk = (memBlk *) mem;
      mem += blkI32;
      blk->next = mem;
   }
   pool->endAddr = mem - 1;
   blk->next = NULL;
   return entries;
}


#pragma CODE_SECTION (RTPMemInit, "SLOW_PROG_SECT")
void RTPMemInit () {

   int smallBlkCnt,  largeBlkCnt,   hugeBlkCnt;
   int smallBlkI32,  largeBlkI32,   hugeBlkI32;
   int smallPoolI32, largePoolI32,  hugePoolI32;

   // Distribute jitter buffer memory across cores
   smallBlkCnt = sysConfig.maxJitterms;
   largeBlkCnt = (sysConfig.maxJitterms/10) + 1;
   hugeBlkCnt  = (sysConfig.maxJitterms/30) + 1;

   smallBlkI32 = sysConfig.smallJBBlockI32;
   largeBlkI32 = sysConfig.largeJBBlockI32;
   hugeBlkI32  = sysConfig.hugeJBBlockI32;


   smallBlkI32 = sysConfig.smallJBBlockI32;
   largeBlkI32 = sysConfig.largeJBBlockI32;
   hugeBlkI32  = sysConfig.hugeJBBlockI32;

   smallPoolI32 = (smallBlkCnt * sysConfig.maxNumChannels * smallBlkI32) / DSPTotalCores;
   largePoolI32 = (largeBlkCnt * sysConfig.maxNumChannels * largeBlkI32) / DSPTotalCores;
   hugePoolI32 = (hugeBlkCnt * sysConfig.maxNumChannels * hugeBlkI32) / DSPTotalCores;

   smallBlkCnt = RTPPoolInit (&smallJB, smallBlkI32, smallPoolI32, &SmallJBPool[DSPCore * smallPoolI32]);
   largeBlkCnt = RTPPoolInit (&largeJB, largeBlkI32, largePoolI32, &LargeJBPool[DSPCore * largePoolI32]);
   hugeBlkCnt  = RTPPoolInit (&hugeJB,  hugeBlkI32,  hugePoolI32,  &HugeJBPool[DSPCore * hugePoolI32]);

   STATS (memset (&RTPStats, 0, sizeof (RTPStats));)
   STATS (RTPStats.StatID = 0x4444;)
   STATS (smallJB.stats = &RTPStats.smallJB;)
   STATS (RTPStats.smallJB.total = smallBlkCnt;)
   STATS (RTPStats.smallJB.avail = smallBlkCnt;)

   STATS (largeJB.stats = &RTPStats.largeJB;)
   STATS (RTPStats.largeJB.total = largeBlkCnt;)
   STATS (RTPStats.largeJB.avail = largeBlkCnt;)

   STATS (hugeJB.stats = &RTPStats.hugeJB;)
   STATS (RTPStats.hugeJB.total = hugeBlkCnt;)
   STATS (RTPStats.hugeJB.avail = hugeBlkCnt;)

  // Enable software interrupt to transfer packets from RTP Buffer into jitter buffers
   swi_attr.priority = HostXferPriority;
   SWI_Host = SWI_create (&swi_attr);

   if (hugeJB.maxI8) {
      RTPScratchI8 = hugeJB.maxI8;
   } else if (largeJB.maxI8) {
      RTPScratchI8 = largeJB.maxI8;
      hugeJB.maxI8 = largeJB.maxI8;
   } else {
      RTPScratchI8  = smallJB.maxI8;
      largeJB.maxI8 = smallJB.maxI8;
      hugeJB.maxI8  = smallJB.maxI8;
   }

}

//}==========================================================================
//   RTP Application program interface functions
//
//   RTPAlloc  - memory allocation for jitter buffer storage
//   RTPFree   - memory release from jitter buffer storage
//   RTPTime   - current time in samples
//   RTPLock   - critical section lock
//   RTPUnlock - critical section unlock
//
//   toneDurationCallback - returns duration of tone event packets
//
//   RTP API support routines
//{==========================================================================

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ RTPAlloc
//
// FUNCTION
//  Allocates memory for RTP jitter buffer
//
// Inputs
//    sizeI8 - Number of bytes to allocate
//
// RETURNS
//  pointer to allocated memory
//
//}
#pragma CODE_SECTION (RTPAlloc, "PKT_PROG_SECT")
void *RTPAlloc (ADT_UInt32 sizeI8) {
   register memBlk  *blk;
   register memPool *pool;
   int mask;

   pool = NULL;
   if ((sizeI8 <= smallJB.maxI8) && (smallJB.free != NULL)) {
      pool = &smallJB;
   } else if ((sizeI8 <= largeJB.maxI8) && (largeJB.free != NULL)) {
      pool = &largeJB;
   } else if ((sizeI8 <= hugeJB.maxI8)  && (hugeJB.free != NULL)) {
      pool = &hugeJB;
   } else {
      AppErr ("Memory request too large", sizeI8);
      return NULL;
   }
   if (pool->free == NULL) {
      STATS_INC (pool, empty);
      return NULL;
   }

   mask = HWI_disable ();
   blk = pool->free;
   pool->free = blk->next;

   STATS_INC (pool, allocs);
   STATS_DEC (pool, avail);
   STATS_INC (pool, inUse);
   HWI_restore (mask);

   blk->next = pool;    // Identify pool for RTP free
   return &blk->data[RTPReserveI32];
}

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// RTPFree
//
// FUNCTION
//  Release previously allocated memory
//
// Inputs
//   Pointer to memory location
//
//}
#pragma CODE_SECTION (RTPFree, "PKT_PROG_SECT")
void RTPFree (void* mem) {
   register memBlk  *blk;
   register memPool *pool;
   int mask;

   // Obtain pool pointer from start of memory block.
   blk = (memBlk *) ((ADT_UInt32 *) mem - 1 - RTPReserveI32);
   pool = blk->next;

#ifdef _DEBUG
   if (pool->selfAddr != pool) {
      AppErr ("JB memory corruption", (int) blk);
   }

   if ((void *) blk < pool->startAddr || pool->endAddr < (void *) blk) {
      STATS_INC (pool, corrupt);
      AppErr ("RTP JB fault", (int) blk);
   }
   AppErr ("JB memory corruption", hugeJB.maxI8 < pool->maxI8);

   memset (mem, 0x11, pool->maxI8);
#endif

   mask = HWI_disable ();
   blk->next = pool->free;
   pool->free = blk;

   STATS_INC (pool, frees);
   STATS_INC (pool, avail);
   STATS_DEC (pool, inUse);
   HWI_restore (mask);
}

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// RTPTime
//
// FUNCTION
//   Current system time for packet arrival statistics
//
// RETURNS
//   Actual time scale to RTP sampling rate units
//
//}
//ADT_UInt32 GPAK_ADT_Timer = 0;
#pragma CODE_SECTION (RTPTime, "PKT_PROG_SECT")
ADT_UInt32 RTPTime (int RTPId) {
   RtpChanData_t *RTPChan;
#if 1
   ADT_UInt32 timestamp32;
   ADT_UInt64 ts64;

   // Convert millisecond to RTP samples
   RTPChan = RtpChanData [RTPId];
   ts64 = ApiBlock.DmaSwiCnt;
   ts64 *= (ADT_UInt64)(RTPChan->Instance.SampleRate / 1000);

   // handle 32-bit wrap
   if (ts64 > MAX_U32) {
        timestamp32	 = ts64 - MAX_U32 - 1;
   } else {
        timestamp32 = ts64;
   }
   return (timestamp32);
#else
   return (ApiBlock.DmaSwiCnt * (ADT_UInt32)(RTPChan->Instance.SampleRate / 1000));
#endif
}

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// RTPLock
//
// FUNCTION
//   Cricital section locking mechanism to RTP structures
//
//}
#pragma CODE_SECTION (RTPLock, "PKT_PROG_SECT")
void RTPLock (int RTPid) {
   SWI_disable();
}

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// RTPUnlock
//
// FUNCTION
//   Cricital section unlocking mechanism to RTP structures
//
//}
#pragma CODE_SECTION (RTPUnlock, "PKT_PROG_SECT")
void RTPUnlock (int RTPid) {
   SWI_enable();
}

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// RTPWait
//
// FUNCTION
//   Task suspension
//
//}
#pragma CODE_SECTION (RTPWait, "PKT_PROG_SECT")
void RTPWait () { }

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// RTCPStats
//
// FUNCTION
//   Gather statistics for RTCP
//
//}
#pragma CODE_SECTION (RTCPStats, "PKT_PROG_SECT")
void RTCPStats (RTCPEVENTS event, RTPCONNECT *RTPConnect, ADT_UInt8* PktAddr, ADT_UInt16 PktSize) { }

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// toneDurationCallback
//
// FUNCTION
//   RTP callback to extract the toneDuration from RTP 2833 tone payloads
//
// Inputs
//   hndl     -  Handle established at RTPOpen (set to channel Id)
//   paytype  -  RTP payload type
//   paybuff  -  pointer to start of payload data
//
// returns:   - Tone duration: if paytype is a tone packet
//              zero         : for all other packets types
//}
#pragma CODE_SECTION (toneDurationCallback, "FAST_PROG_SECT")
#if 0
static ADT_UInt32 toneDurationCallback (ADT_Int32 chanID, ADT_UInt16 payType, ADT_UInt16* paybuff) {
   int timeElapsed;
#else
static ADT_UInt32 toneDurationCallback (NETHANDLE* hndl, ADT_UInt16 payType, ADT_UInt16* paybuff) {
   int timeElapsed;
   ADT_Int32 chanID;
   chanID = (ADT_UInt32) hndl;
#endif
   if (sysConfig.rtpPktType == TonePkt)
      return 0;

   if (RtpChanData[chanID]->Profile.Tone == payType) {
       timeElapsed =  ntohs (paybuff[1]);
       return (ADT_UInt32) timeElapsed;
   }

   // return 0 if paytype is not tone
   return 0;
}


//} - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//   Common GPAK Frame work and messaging interfaces
//
//   ProcConfigRTPMsg  - Process RTP configuration message
//   ProcRTPStatusMsg  - Process RTP status message
//   getPacketFromRTP  - Retrieve packet from host transfer buffer
//   sendPacketToRTP   - Send packet to IP stack or host transfer buffers
//   HOSTSWI           - Transfer packets from host transfer buffer to jitter buffer
//   postRtpHostSWI    - Post HOSTSWI S/W interrupt
//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


//=====================================================
//{  RTP configuration message processing
//
//   Called by messaging task upon receipt of RTP configuration message
//
//
//    Word       Bits
//   Msg[0]     0xff00    - Commnand ID
//   Msg[0]     0x00ff    - Command tag

//   Msg[1]     0xff00    - Jitter mode
//   Msg[1]     0x00ff    - GPak channel

//   Msg[2]     0xffff    - SSRC High
//   Msg[3]     0xffff    - SSRC Low

//   Msg[4]     0xffff    - Start Sequence
//   Msg[5]     0xffff    - MinDelay (ms)
//   Msg[6]     0xffff    - TargetDelay (ms)
//   Msg[7]     0xffff    - MaxDelay (ms)

//   Msg[8]     0xff00    - Comfort noise payload type
//   Msg[8]     0x00ff    - Voice payload type
//   Msg[9]     0xff00    - FAX payload type
//   Msg[9]     0x00ff    - Tone payload type

//   Msg[10]    0xffff    - Destination UDP port
//   Msg[11]    0xffff    - Rx Destination IP High
//   Msg[12]    0xffff    - Rx Destination IP Low
//   Msg[13]    0xffff    - Bytes 0 and 1 MAC
//   Msg[14]    0xffff    - Bytes 2 and 3 MAC
//   Msg[15]    0xffff    - Bytes 4 and 5 MAC
//   Msg[16]    0xffff    - Source UDP port

//   Msg[17]    0xffff    - Destination SSRC High
//   Msg[18]    0xffff    - Destination SSRC Low
//   Msg[19]    0xffff    - Sampling rate (Hz)
//
//   Msg[20]    0xffff    - Tx Destination IP High
//   Msg[21]    0xffff    - Tx Destination IP Low
//   Msg[22]    0xff00    - VLAN Index
//   Msg[22]    0x00ff    - DSCP value.
//   Msg[23]    0xffff    - Tx Destination IP High
//   Msg[24]    0xffff    - Tx Destination IP Low
//
//    Rply[0]    0x00ff    - Command tag (filled in by msg task)
//    Rply[0]    0xff00    - Reply ID
//    Rply[1]    0x00ff    - Reply status (return code)
//    Rply[1]    0xff00    - Channel ID
//}
#pragma CODE_SECTION (ProcConfigRTPMsg, "SLOW_PROG_SECT")
GPAK_RTPConfigStat_t ProcConfigRTPMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {
   RTP_STAT rtpStat;
   GPAK_RTPConfigStat_t rply;
   rtpConfig_APIV5_t cfg;
   ProfileTypes profile;
   int         chanId;
   void        *RTPSession;

   chanId   = (pCmd [1] & 0x00FF);

   pReply[0] |= (MSG_RTP_CONFIG_REPLY << 8);
   pReply[1] = (chanId << 8);

   if (sysConfig.maxJitterms == 0)
      return RTPNotConfigured;

   // Validate configuration parameters
   if (sysConfig.maxNumChannels < chanId)
      return RTPChannelError;

   if (chanTable[chanId]->channelType != inactive)
      return RTPChannelActive;

   memset (&cfg, 0, sizeof (cfg));

   // Parse message for configuration parameters
   cfg.JitterMode       = (pCmd [1] & 0xFF00) >> 8;
   cfg.SSRC             = (((ADT_UInt32)pCmd[2])<<16) | (ADT_UInt32)pCmd[3];
   cfg.StartSequence    = pCmd [4];

   cfg.ChanId = (ADT_UInt16)chanId;
   cfg.APIVersion = ADT_RTP_API_VERSION;

   if (12 < cfg.JitterMode)
      return RTPJitterModeError;

   if (sysConfig.maxJitterms * sysConfig.samplesPerMs < cfg.DelayTargetMax  ||
       cfg.DelayTargetMax    < cfg.DelayTarget     ||
       cfg.DelayTarget       < cfg.DelayTargetMin)
      return RTPTargetError;

   CLEAR_INST_CACHE (RtpChanData[chanId], sizeof (RtpChanData_t));

   profile.Voice          = (pCmd [8] & 0x00FF);
   profile.CNG            = (pCmd [8] & 0xFF00) >> 8;
   profile.Tone           = (pCmd [9] & 0x00FF);
   profile.T38_PROFILE    = (pCmd [9] & 0xFF00) >> 8;

   FLUSH_wait ();

   removeFromIPSession (chanId);


   // Fill in GPAK constrained variables
   cfg.ClockFunction     = RTPTime;
   cfg.NetTransmit       = netTransmit;
   if (SysAlgs.toneRlyGenEnable)
      cfg.ToneDurationFunction = (TONEDURATION *) toneDurationCallback;
   else
      cfg.ToneDurationFunction = NULL;

   cfg.ProbationCount    = 3;
   cfg.JB_Max            = sysConfig.maxJitterms * sysConfig.samplesPerMs;
   cfg.Pad32             = ADT_TRUE;
   cfg.RandomSeed        = (ApiBlock.DmaSwiCnt << 16) | CLK_gethtime();

   // Sampling Rate (kHz)
   if (19 < ApiBlock.CmdMsgLength)
      cfg.SampleRate = pCmd[19];
   else
      cfg.SampleRate = 8000;

   cfg.DelayTargetMin   = pCmd [5] * (cfg.SampleRate / 1000);
   cfg.DelayTarget      = pCmd [6] * (cfg.SampleRate / 1000);
   cfg.DelayTargetMax   = pCmd [7] * (cfg.SampleRate / 1000);

   RTPSession = &RtpChanData[chanId]->Instance;
   if (SysAlgs.rtpNetDelivery)  {
      rply = processIPStackFields (pCmd, chanId, RTPSession);
      if (rply != RTPSuccess) return rply;
   }

   // Use channel id as network "handle" for callbacks
   memcpy  (&RtpChanData[chanId]->Profile, &profile, sizeof (ProfileTypes));

   // debug only InitG722RTPStat(chanId); // jdc

   rtpStat = RTP_ADT_Init (RTPSession, (void *) chanId, &cfg);

#ifdef RTCP_ENABLED
   if (25 < ApiBlock.CmdMsgLength) {
        if (rtcp_process_cfg_msg(pCmd, pReply, RTPSession) != 0) {
            if (rtpStat == RTP_OPEN_STAT_SUCCESS) { 
                removeFromIPSession (chanId);
                rtpStat = (RTP_STAT)-1;
            }
        }
   }
#endif

   FLUSH_INST_CACHE (RtpChanData[chanId], sizeof (RtpChanData_t));
   FLUSH_wait ();

   if (rtpStat == RTP_OPEN_STAT_SUCCESS) 
        return RTPSuccess;

   return  RTPChannelError;


}


//=====================================================
//{  RTP IpV6 configuration message processing
//
//   Called by messaging task upon receipt of RTP configuration message
//
//
//    Word       Bits
//   Msg[0]     0xff00    - Commnand ID
//   Msg[0]     0x00ff    - Command tag

//   Msg[1]     0xff00    - Jitter mode
//   Msg[1]     0x00ff    - GPak channel

//   Msg[2]     0xffff    - SSRC High
//   Msg[3]     0xffff    - SSRC Low

//   Msg[4]     0xffff    - Start Sequence
//   Msg[5]     0xffff    - MinDelay (ms)
//   Msg[6]     0xffff    - TargetDelay (ms)
//   Msg[7]     0xffff    - MaxDelay (ms)

//   Msg[8]     0xff00    - Comfort noise payload type
//   Msg[8]     0x00ff    - Voice payload type
//   Msg[9]     0xff00    - FAX payload type
//   Msg[9]     0x00ff    - Tone payload type

//   Msg[10]    0xffff    - Destination UDP port

//   Msg[11] = 0xffff    - Rx Destination IP XXXX:::::::
//   Msg[12] = 0xffff    - Rx Destination IP :XXXX::::::
//   Msg[13] = 0xffff    - Rx Destination IP ::XXXX:::::
//   Msg[14] = 0xffff    - Rx Destination IP :::XXXX::::
//   Msg[15] = 0xffff    - Rx Destination IP ::::XXXX:::
//   Msg[16] = 0xffff    - Rx Destination IP :::::XXXX::
//   Msg[17] = 0xffff    - Rx Destination IP ::::::XXXX:
//   Msg[18] = 0xffff    - Rx Destination IP :::::::XXXX

//   Msg[19]    0xffff    - Bytes 0 and 1 MAC
//   Msg[20]    0xffff    - Bytes 2 and 3 MAC
//   Msg[21]    0xffff    - Bytes 4 and 5 MAC
//   Msg[22]    0xffff    - Source UDP port

//   Msg[23]    0xffff    - Destination SSRC High
//   Msg[24]    0xffff    - Destination SSRC Low
//   Msg[25]    0xffff    - Sampling rate (Hz)
//
//   Msg[26] = 0xffff    - Tx Destination IP XXXX:::::::
//   Msg[27] = 0xffff    - Tx Destination IP :XXXX::::::
//   Msg[28] = 0xffff    - Tx Destination IP ::XXXX:::::
//   Msg[29] = 0xffff    - Tx Destination IP :::XXXX::::
//   Msg[30] = 0xffff    - Tx Destination IP ::::XXXX:::
//   Msg[31] = 0xffff    - Tx Destination IP :::::XXXX::
//   Msg[32] = 0xffff    - Tx Destination IP ::::::XXXX:
//   Msg[33] = 0xffff    - Tx Destination IP :::::::XXXX

//   Msg[34]    0xff00    - VLAN Index
//   Msg[34]    0x00ff    - DSCP value.

//   Msg[35] = 0xffff    - In Destination IP XXXX:::::::
//   Msg[36] = 0xffff    - In Destination IP :XXXX::::::
//   Msg[37] = 0xffff    - In Destination IP ::XXXX:::::
//   Msg[38] = 0xffff    - In Destination IP :::XXXX::::
//   Msg[39] = 0xffff    - In Destination IP ::::XXXX:::
//   Msg[40] = 0xffff    - In Destination IP :::::XXXX::
//   Msg[41] = 0xffff    - In Destination IP ::::::XXXX:
//   Msg[42] = 0xffff    - In Destination IP :::::::XXXX

//
//    Rply[0]    0x00ff    - Command tag (filled in by msg task)
//    Rply[0]    0xff00    - Reply ID
//    Rply[1]    0x00ff    - Reply status (return code)
//    Rply[1]    0xff00    - Channel ID
//}
#pragma CODE_SECTION (ProcConfigRTPv6Msg, "SLOW_PROG_SECT")
GPAK_RTPConfigStat_t ProcConfigRTPv6Msg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {
   RTP_STAT rtpStat;
   GPAK_RTPConfigStat_t rply;
   rtpConfig_APIV5_t cfg;
   ProfileTypes profile;
   int         chanId;
   void        *RTPSession;

   chanId   = (pCmd [1] & 0x00FF);

   pReply[0] |= (MSG_RTP_V6_CONFIG_REPLY << 8);
   pReply[1] = (chanId << 8);

   if (sysConfig.maxJitterms == 0)
      return RTPNotConfigured;

   // Validate configuration parameters
   if (sysConfig.maxNumChannels < chanId)
      return RTPChannelError;

   if (chanTable[chanId]->channelType != inactive)
      return RTPChannelActive;

   memset (&cfg, 0, sizeof (cfg));

   // Parse message for configuration parameters
   cfg.JitterMode       = (pCmd [1] & 0xFF00) >> 8;
   cfg.SSRC             = (((ADT_UInt32)pCmd[2])<<16) | (ADT_UInt32)pCmd[3];
   cfg.StartSequence    = pCmd [4];

   cfg.ChanId = (ADT_UInt16)chanId;
   cfg.APIVersion = ADT_RTP_API_VERSION;

   if (12 < cfg.JitterMode)
      return RTPJitterModeError;

   if (sysConfig.maxJitterms * sysConfig.samplesPerMs < cfg.DelayTargetMax  ||
       cfg.DelayTargetMax    < cfg.DelayTarget     ||
       cfg.DelayTarget       < cfg.DelayTargetMin)
      return RTPTargetError;

   CLEAR_INST_CACHE (RtpChanData[chanId], sizeof (RtpChanData_t));

   profile.Voice          = (pCmd [8] & 0x00FF);
   profile.CNG            = (pCmd [8] & 0xFF00) >> 8;
   profile.Tone           = (pCmd [9] & 0x00FF);
   profile.T38_PROFILE    = (pCmd [9] & 0xFF00) >> 8;

   FLUSH_wait ();

   removeFromIPSession (chanId);


   // Fill in GPAK constrained variables
   cfg.ClockFunction     = RTPTime;
   cfg.NetTransmit       = netTransmit;
   if (SysAlgs.toneRlyGenEnable)
      cfg.ToneDurationFunction = (TONEDURATION *) toneDurationCallback;
   else
      cfg.ToneDurationFunction = NULL;

   cfg.ProbationCount    = 3;
   cfg.JB_Max            = sysConfig.maxJitterms * sysConfig.samplesPerMs;
   cfg.Pad32             = ADT_TRUE;
   cfg.RandomSeed        = (ApiBlock.DmaSwiCnt << 16) | CLK_gethtime();

   // Sampling Rate (kHz)
   if (19 < ApiBlock.CmdMsgLength)
      cfg.SampleRate = pCmd[25];
   else
      cfg.SampleRate = 8000;

   cfg.DelayTargetMin   = pCmd [5] * (cfg.SampleRate / 1000);
   cfg.DelayTarget      = pCmd [6] * (cfg.SampleRate / 1000);
   cfg.DelayTargetMax   = pCmd [7] * (cfg.SampleRate / 1000);

   RTPSession = &RtpChanData[chanId]->Instance;
   if (SysAlgs.rtpNetDelivery)  {
      rply = processIPv6StackFields (pCmd, chanId, RTPSession);
      if (rply != RTPSuccess) return rply;
   }

   // Use channel id as network "handle" for callbacks
   memcpy  (&RtpChanData[chanId]->Profile, &profile, sizeof (ProfileTypes));

   // debug only InitG722RTPStat(chanId); // jdc

   rtpStat = RTP_ADT_Init (RTPSession, (void *) chanId, &cfg);
   FLUSH_INST_CACHE (RtpChanData[chanId], sizeof (RtpChanData_t));
   FLUSH_wait ();

#ifdef RTCP_ENABLED
   // IPv6 RTP config message with RTCP disabled is 43 words in length
   if (43 < ApiBlock.CmdMsgLength) {
        if (rtcp_process_cfg_msg_v6(pCmd, pReply, RTPSession) != 0) {
            if (rtpStat == RTP_OPEN_STAT_SUCCESS) {
                removeFromIPSession (chanId);
                rtpStat = (RTP_STAT)-1;
            }
        }
   }
#endif

   if (rtpStat == RTP_OPEN_STAT_SUCCESS)
        return RTPSuccess;

   return  RTPChannelError;


}
//{  RTP status messaage processing
//
//   Called by messaging task upon receipt of RTP status message
//
//     0:ff00    -  57 - RTP status request
//     0:00ff    -  Core index
//     1:ff00    -  Channel index
//
//  Word:Bits
//     0:ff00     -  58 - RTP Status response
//     0:00ff     -  DSP status value
//     1:ff00     -  channel index
//     1:00ff     -  samples per MS
//     2:ffff     -  Pkts sent (HI)
//     3:ffff     -            (LO)
//     4:ffff     -  Pkts recd (HI)
//     5:ffff     -            (LO)  
//     6:ffff     -  Loss %    (HI)
//     7:ffff     -            (LO)  
//     8:ffff     -  Pkts plyd (HI)
//     9:ffff     -            (LO)  
//    10:ffff     -  Pkts late (HI)
//    11:ffff     -            (LO)  
//    12:ffff     -  OutofRange(HI)
//    13:ffff     -            (LO)  
//    14:ffff     -  Overflow  (HI)
//    15:ffff     -            (LO)  
//    16:ffff     -  Underflow (HI)
//    17:ffff     -            (LO)  
//    18:ffff     -  JitterMin (HI)
//    19:ffff     -            (LO)  
//    20:ffff     -  JitterCurrent[HI]
//    21:ffff     -            (LO)  
//    22:ffff     -  JitterMax (HI)
//    23:ffff     -            (LO)  
//    24:ffff     -  JitterMode
//    25:ffff     -  MinDelay
//    26:ffff     -  Delay
//    27:ffff     -  MaxDelay
//}
#pragma CODE_SECTION (ProcRTPStatusMsg, "SLOW_PROG_SECT")
int ProcRTPStatusMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {
   int       chanId, jitterMin;
   ADT_UInt8 samplesPerMS;
   RTPDATA  *RTP;
   ADT_UInt32 PktsExpected, MaxSeqExtended, PktLossPercent;
   int MsgLen;
   chanInfo_t *chan;   

#ifdef RTCP_ENABLED   
   int rtcpRequest;
   rtcpRequest = pCmd [1] & 0x00FF;
#endif
   
   chanId   = (pCmd [1] >> 8) & 0x00FF;
   chan = chanTable[chanId];

   pReply[1] = PackBytes (chanId, 0);

   // Validate configuration parameters
   if (sysConfig.maxJitterms == 0) {
      pReply[0] = PackBytes (MSG_RTP_STATUS_REPLY, RTPStat_NotConfigured);
      return 2;
   }

   if (sysConfig.maxNumChannels < chanId) {
      pReply[0] = PackBytes (MSG_RTP_STATUS_REPLY, RTPStat_ChannelError);
      return 2;
   }

   RTP = (RTPDATA  *) &RtpChanData[chanId]->Instance;
   if (chan->CoreID != msgCore) 
   {
        CLEAR_INST_CACHE (RTP, sizeof (RTPDATA));
        FLUSH_wait ();
   }
   if ((RTP->State & SESSION_MASK) != SESSION_ACTIVE) {
      pReply[0] = PackBytes (MSG_RTP_STATUS_REPLY, RTPStat_ChannelInactive);
      return 2;
   }

   samplesPerMS = (ADT_UInt8) (RTP->SampleRate/1000);

   pReply[0] = PackBytes (MSG_RTP_STATUS_REPLY, RTPStat_Success);
   pReply[1] = PackBytes (chanId, samplesPerMS);

   if (RTP->stats.PktsReceived) {
      MaxSeqExtended = RTP->stats.BaseCycles + RTP->MaxSeq;
      PktsExpected   = MaxSeqExtended - RTP->stats.BaseSeq;
      if (RTP->stats.PktsReceived < PktsExpected)
         PktLossPercent = ((PktsExpected - RTP->stats.PktsReceived) * 1000) / PktsExpected;
      else
         PktLossPercent = 0;
   } else {
      PktLossPercent = 0;
   }
   if (1000 < PktLossPercent) PktLossPercent = 1000;

   // Packet counts
   pReply[2]  = HI_16 (RTP->stats.PktsSent);
   pReply[3]  = LO_16 (RTP->stats.PktsSent);
   pReply[4]  = HI_16 (RTP->stats.PktsReceived);
   pReply[5]  = LO_16 (RTP->stats.PktsReceived);
   pReply[6]  = HI_16 (PktLossPercent);
   pReply[7]  = LO_16 (PktLossPercent);
   pReply[8]  = HI_16 (RTP->stats.PktsPlayed);
   pReply[9]  = LO_16 (RTP->stats.PktsPlayed);
   pReply[10] = HI_16 (RTP->stats.LateCnt);
   pReply[11] = LO_16 (RTP->stats.LateCnt);
   pReply[12] = HI_16 (RTP->stats.OutOfRangeCnt);
   pReply[13] = LO_16 (RTP->stats.OutOfRangeCnt);
   pReply[14] = HI_16 (RTP->stats.OverflowCnt);
   pReply[15] = LO_16 (RTP->stats.OverflowCnt);
   pReply[16] = HI_16 (RTP->stats.UnderflowCnt);
   pReply[17] = LO_16 (RTP->stats.UnderflowCnt);

   // Jitter
   jitterMin = RTP->JitterMin;
   if (RTP->JitterMax < jitterMin) jitterMin = 0;

   pReply[18] = HI_16 (jitterMin);
   pReply[19] = LO_16 (jitterMin);
   pReply[20] = HI_16 (RTP->JitterEstimate);
   pReply[21] = LO_16 (RTP->JitterEstimate);
   pReply[22] = HI_16 (RTP->JitterMax);
   pReply[23] = LO_16 (RTP->JitterMax);

   // Jitter buffer depth
   pReply[24] = RTP->JitterMode;
   pReply[25] = RTP->DelayTargetMin;
   pReply[26] = RTP->JitterBufferDelay;
   pReply[27] = RTP->DelayTargetMax;
   MsgLen = 28;

#ifdef RTCP_ENABLED
    if (rtcpRequest) {
        MsgLen += rtcp_read_stats (chanId, &pReply[28]);
    }
#endif

   if (chan->CoreID != msgCore) 
   {
        CLEAR_INST_CACHE (RTP, sizeof (RTPDATA));
   }

   return MsgLen;  // Return number of I16 array elements of message
}


#pragma CODE_SECTION (GpakToRTPPayloadType, "PKT_PROG_SECT")
//  Convert GPAK class and type to RFC 3551 payload type
static ADT_UInt16 GpakToRTPPayloadType (ProfileTypes *Profile, GpakPayloadClass PayloadClass, ADT_UInt16 PayloadType) {

   switch (PayloadClass) {
   case PayClassAudio:
      switch (PayloadType) {
      case PCMU_64:
         STATS (RTPStats.tx.ulaw++;) return 0;
      case PCMA_64:   
         STATS (RTPStats.tx.alaw++;) return 8;
      case G722_64:   
         return 9;
      case G729AB:   
      case G729:   
         return 18;
      case G723_63:   
      case G723_53:   
      case G723_63A:   
      case G723A:   
         return 4;
      case G728_16:   
         return 15;
      }
      STATS (RTPStats.tx.voice++;) return Profile->Voice;

   case PayClassSilence: STATS (RTPStats.tx.cng++;)  return Profile->CNG;
   case PayClassTone:    
   case PayClassStartTone:
   case PayClassEndTone:
       STATS (RTPStats.tx.tone++;) return Profile->Tone;
   case PayClassT38:     STATS (RTPStats.tx.t38++;)  return Profile->T38_PROFILE;
   }
   return (ADT_UInt16) 0xffff;
}

#pragma CODE_SECTION (RTPToGPakPayloadType, "PKT_PROG_SECT")
// Convert RFC 3551 payload type to GPAK class and type
static void RTPToGPakPayloadType (ProfileTypes *Profile, ADT_UInt16 RTPPayloadType,
                           GpakPayloadClass *PayloadClass,  ADT_UInt16 *PayloadType) {

   if (RTPPayloadType == Profile->Voice) {          // Default primary codec
      *PayloadClass = PayClassAudio;
      *PayloadType  = DefaultCodec;
       STATS (RTPStats.rx.voice++;)
   } else if (RTPPayloadType == 0) {                // G.711 mu-law packets
      *PayloadClass = PayClassAudio;
      *PayloadType = PCMU_64;
       STATS (RTPStats.rx.ulaw++;)
   } else if (RTPPayloadType == 4) {                // G.723  packets
      *PayloadClass = PayClassAudio;
      *PayloadType = G723_53;
       STATS (RTPStats.rx.voice++;)
   } else if (RTPPayloadType == 8) {                // G.711 a-law packets
      *PayloadClass = PayClassAudio;
      *PayloadType = PCMA_64;
       STATS (RTPStats.rx.alaw++;)
   } else if (RTPPayloadType == 9) {                // G.722 packets
      *PayloadClass = PayClassAudio;
      *PayloadType = G722_64;
       STATS (RTPStats.rx.voice++;)
   } else if (RTPPayloadType == 15) {                // G.728 packets
      *PayloadClass = PayClassAudio;
      *PayloadType = G728_16;
   } else if (RTPPayloadType == 18) {                // G.729 packets
      *PayloadClass = PayClassAudio;
      *PayloadType = G729AB;
       STATS (RTPStats.rx.voice++;)
   } else if (RTPPayloadType == Profile->Tone) {    // tone relay packets
      *PayloadClass = PayClassTone;
      *PayloadType = sysConfig.rtpPktType;
       STATS (RTPStats.rx.tone++;)
   } else if (RTPPayloadType == Profile->CNG) {     // comfort noise packets
      *PayloadClass = PayClassSilence;
      *PayloadType = 0;
       STATS (RTPStats.rx.cng++;)
   } else if (RTPPayloadType == Profile->T38_PROFILE) {     // T.38 fax relay packets
      *PayloadClass = PayClassT38;
      *PayloadType = 0;
       STATS (RTPStats.rx.t38++;)
   } else {
      *PayloadClass = PayClassNone;
      *PayloadType = 0;
       STATS (RTPStats.rx.payDiscards++;)
   }
   return;

}

#pragma CODE_SECTION (ED137PTTMsg, "MEDIUM_PROG_SECT")
static int ED137PTTMsg (int channelId, GpakDeviceSide_t deviceSide, ADT_UInt8 *buf, ADT_UInt16 lenI8) {

   ADT_UInt16 byteLen, i16Len, payBytes;
   EventFifoED137Msg_t eventED137Msg;

   // Pad the payload if necessary to ensure multiples of 4-bytes to accomodate host transfer size.
   // Add 2 to account for the actual Non-Persistent msg length field (stored in the 1st 16-bit element of the event payload).
   payBytes = ((lenI8 + 2 + 3)>>2)<<2;
   byteLen = payBytes + (EVENT_HDR_I16LEN * 2);

   i16Len  = byteLen/2;

   // invalidate cache to force a refresh of pointers accessed by the host
   if (sysConfig.enab.Bits.apiCacheEnable) {
      CACHE_INV(ApiBlock.pRdBlock, CACHE_L2_LINE_SIZE);
      eventFIFOInfo.TakeIndex = ApiBlock.pRdBlock->EventFIFOTakeIndex;
      eventFIFOInfo.PutIndex  = ApiBlock.pWrBlock->EventFIFOPutIndex;
   }

   // Check if the Non-Persistent message will fit in the event fifo
   if (getFreeSpace (&eventFIFOInfo) - 2 < i16Len)
      return ED137_EVENT_MESSAGE_AVAILABLE_CODE;

   eventED137Msg.header.channelId   = channelId;
   eventED137Msg.header.eventCode   = customED137B_PTT_Event;
   eventED137Msg.header.eventLength = payBytes;
   eventED137Msg.header.deviceSide  = deviceSide;

   // the first element of rx cid payload is the Rx msg length itself
   eventED137Msg.payload[0] = lenI8;
   memcpy (&(eventED137Msg.payload[1]), buf, lenI8);
   writeEventIntoFifo ((EventFifoMsg_t *)&eventED137Msg);
   return ED137_EVENT_OK_CODE;
}

#pragma CODE_SECTION (ED137PersistentMsg, "MEDIUM_PROG_SECT")
static int ED137PersistentMsg (int channelId, GpakDeviceSide_t deviceSide, ADT_UInt8 TYPE, ADT_UInt8 VALUE, ADT_UInt16 *PT, ADT_UInt16 lenI8) {

   ADT_UInt16 byteLen, i16Len, payBytes;
   EventFifoED137Msg_t eventED137Msg;

   // Pad the payload if necessary to ensure multiples of 4-bytes to accomodate host transfer size.
   // Add 2 to account for the actual Non-Persistent msg length field (stored in the 1st 16-bit element of the event payload).
   payBytes = ((lenI8 + 2 + 3)>>2)<<2;
   byteLen = payBytes + (EVENT_HDR_I16LEN * 2);

   i16Len  = byteLen/2;

   // invalidate cache to force a refresh of pointers accessed by the host
   if (sysConfig.enab.Bits.apiCacheEnable) {
      CACHE_INV(ApiBlock.pRdBlock, CACHE_L2_LINE_SIZE);
      eventFIFOInfo.TakeIndex = ApiBlock.pRdBlock->EventFIFOTakeIndex;
      eventFIFOInfo.PutIndex  = ApiBlock.pWrBlock->EventFIFOPutIndex;
   }

   // Check if the Non-Persistent message will fit in the event fifo
   if (getFreeSpace (&eventFIFOInfo) - 2 < i16Len)
      return ED137_EVENT_MESSAGE_AVAILABLE_CODE;

   eventED137Msg.header.channelId   = channelId;
   eventED137Msg.header.eventCode   = customED137B_PersistentFeatureEvent;
   eventED137Msg.header.eventLength = payBytes;
   eventED137Msg.header.deviceSide  = deviceSide;

   // the first element of rx cid payload is the Rx msg length itself
   eventED137Msg.payload[0] = lenI8;
 //  memcpy (&(eventED137Msg.payload[1]), buf, lenI8/2);
   eventED137Msg.payload[1] = TYPE;
   eventED137Msg.payload[2] = VALUE;
   eventED137Msg.payload[3] = *PT;
   writeEventIntoFifo ((EventFifoMsg_t *)&eventED137Msg);
   return ED137_EVENT_OK_CODE;

}

#pragma CODE_SECTION (ED137NonPersistentMsg, "MEDIUM_PROG_SECT")
static int ED137NonPersistentMsg (int channelId, GpakDeviceSide_t deviceSide, ADT_UInt8 *buf, ADT_UInt16 lenI8) {

   ADT_UInt16 byteLen, i16Len, payBytes;
   EventFifoED137Msg_t eventED137Msg;

   // Pad the payload if necessary to ensure multiples of 4-bytes to accomodate host transfer size.
   // Add 2 to account for the actual Non-Persistent msg length field (stored in the 1st 16-bit element of the event payload).
   payBytes = ((lenI8 + 2 + 3)>>2)<<2;
   byteLen = payBytes + (EVENT_HDR_I16LEN * 2);

   i16Len  = byteLen/2;

   // invalidate cache to force a refresh of pointers accessed by the host
   if (sysConfig.enab.Bits.apiCacheEnable) {
      CACHE_INV(ApiBlock.pRdBlock, CACHE_L2_LINE_SIZE);
      eventFIFOInfo.TakeIndex = ApiBlock.pRdBlock->EventFIFOTakeIndex;
      eventFIFOInfo.PutIndex  = ApiBlock.pWrBlock->EventFIFOPutIndex;
   }

   // Check if the Non-Persistent message will fit in the event fifo
   if (getFreeSpace (&eventFIFOInfo) - 2 < i16Len)
      return ED137_EVENT_MESSAGE_AVAILABLE_CODE;

   eventED137Msg.header.channelId   = channelId;
   eventED137Msg.header.eventCode   = customED137B_NONPersistentFeatureEvent;
   eventED137Msg.header.eventLength = payBytes;
   eventED137Msg.header.deviceSide  = deviceSide;

   // the first element of rx cid payload is the Rx msg length itself
   eventED137Msg.payload[0] = lenI8;
   memcpy (&(eventED137Msg.payload[1]), buf, lenI8);
   writeEventIntoFifo ((EventFifoMsg_t *)&eventED137Msg);
   return ED137_EVENT_OK_CODE;

}

#pragma CODE_SECTION (ED137KeepAliveErrorMsg, "MEDIUM_PROG_SECT")
static int ED137KeepAliveErrorMsg (int channelId) {

    EventFifoMsg_t msg;

    msg.header.eventCode    = customED137B_Timeout_Event;
    msg.header.channelId    = channelId;
    msg.header.eventLength  = EVENT_FIFO_MSGLEN_ZERO;
    msg.header.deviceSide   = 0;
    writeEventIntoFifo (&msg);

    return ED137_EVENT_OK_CODE;
}


#pragma CODE_SECTION (RtpRxTimeoutMsg, "MEDIUM_PROG_SECT")
static int RtpRxTimeoutMsg (int channelId) {

    EventFifoMsg_t msg;

    msg.header.eventCode    = EventRtpRx_Timeout;
    msg.header.channelId    = channelId;
    msg.header.eventLength  = EVENT_FIFO_MSGLEN_ZERO;
    msg.header.deviceSide   = 0;
    writeEventIntoFifo (&msg);

    return 0;
}

ExtRXStatus_t ProcessHeaderExtension(chanInfo_t *pChan, ED137RxPkt_t  *ExtData, ADT_UInt16 *PayldType)
{
    ED137Ext_t ED137Ext;
    RTPRxExt_t RxExt;
    RTP_PersistentFeatures_t PF;
    GpakDeviceSide_t deviceSide  = NetToDSP;
    ExtRXStatus_t retval = EXT_RX_NO_DATA;
    ADT_UInt16 PTT_Ext;
    PF.PF_TYPE = 0;

    ED137Ext.VER    = ExtData->EXT_VER;
    ED137Ext.TYPE   = ExtData->EXT_TYPE;
    ED137Ext.LENGTH = ExtData->EXT_LENGTH;

    RxExt.PTT_TYPE  =  ExtData->PTT_VALUE_H >> PTT_TYPE_SHIFT;
    RxExt.SQU       = (ExtData->PTT_VALUE_H >> SQU_SHIFT) & SQU_MASK;
    RxExt.PTT_ID    = ((ExtData->PTT_VALUE_H << PTT_ID_SHIFT_H) &PTT_ID_MASK_H) | ((ExtData->PTT_VALUE_L >> PTT_ID_SHIFT_L) & PTT_ID_MASK_L);
    RxExt.PM        = (ExtData->PTT_VALUE_L >> PM_SHIFT) & PM_MASK;
    RxExt.PTTS      = (ExtData->PTT_VALUE_L >> PTTS_SHIFT) & PTTS_MASK;
    RxExt.SCT       = (ExtData->PTT_VALUE_L >> SCT_SHIFT) & SCT_MASK;
    RxExt.X         =  ExtData->PTT_VALUE_L  & X_MASK;
    if(RxExt.X)
    {
        if((VALID_ED137_PF_TYPE <= (ExtData->AF_VALUE[0] >> PF_TYPE_SHIFT) & PF_TYPE_MASK)
                && ((ExtData->AF_VALUE[0] & PF_LENGTH_MASK) == 1))
        {
            PF.PF_TYPE      = (ExtData->AF_VALUE[0] >> PF_TYPE_SHIFT) & PF_TYPE_MASK;
            PF.PF_LENGTH    =  ExtData->AF_VALUE[0] & PF_LENGTH_MASK;
            PF.PF_VALUE     =  ExtData->AF_VALUE[1];
        }
    }
    PTT_Ext = (ExtData->PTT_VALUE_H << 8) | ExtData->PTT_VALUE_L;
    if((ED137Ext.VER == ExtVer1) &&
            (ED137Ext.TYPE == ExtED137) &&
            (*PayldType == ExtED137_KEEP_ALIVE_PAYLOAD_TYPE))
    {
        if(pChan->ED137RxState.RxState == RX_KEEP_ALIVE)
        {
            pChan->ED137RxState.KeepAliveCount = KEEP_ALIVE_RECEIVED_RESET;
        }
        else
        {
            pChan->ED137RxState.RxState = RX_KEEP_ALIVE;
        }
        if(pChan->ED137B_GateAudioFeature.Enable_Mask)
        {
        retval = EXT_RX_NO_DATA;
        }
        else
        {
            retval = EXT_RX_XFER_DATA;
        }
    }
    if((ED137Ext.VER == ExtVer1) && (ED137Ext.TYPE == ExtED137) &&
            (MIN_ED137_EXT_LENGTH <= ED137Ext.LENGTH))
    {
        if((PTT_Ext & pChan->ED137BcfgParms.mask) !=  (pChan->ED137RxState.LAST_PTT_VALUE & pChan->ED137BcfgParms.mask))
        {
            //Post Event PTT change
            ED137PTTMsg (pChan->ChannelId, deviceSide, &ExtData->PTT_VALUE_H, 2);
        }
        switch(RxExt.PTT_TYPE)
        {
            case  PTT_OFF:
            {
                if( pChan->ED137RxState.RxState == RX_ACTIVE)
                {
                    if(pChan->ED137RxState.LAST_PTT_VALUE != PTT_OFF)
                    {
                        pChan->ED137RxState.RxState = RX_KEEP_ALIVE;
                    }
                }
                if(pChan->ED137B_GateAudioFeature.Enable_Mask)
                {
                retval = EXT_RX_NO_DATA;
                }
                else
                {
                    retval = EXT_RX_XFER_DATA;
                }
                break;
            }
            case  NORMAL_PTT_ON:
            case  COUPLING_PTT_ON:
            case  PRIORITY_PTT_ON:
            case  EMERGENCY_PTT_ON:
            {
                if(*PayldType != ExtED137_KEEP_ALIVE_PAYLOAD_TYPE)
                {
                    pChan->ED137RxState.RxState = RX_ACTIVE;
                    retval = EXT_RX_XFER_DATA;
                }
                else
                {
                    if(pChan->ED137B_GateAudioFeature.Enable_Mask)
                    {
                    retval = EXT_RX_NO_DATA;
                    }
                    else
                    {
                       retval = EXT_RX_XFER_DATA;
                    }
                }
                break;
            }
            default:
            {
                break;
            }
        }
        pChan->ED137RxState.LAST_PTT_VALUE = PTT_Ext;
        if(RxExt.X)
        {
            if(VALID_ED137_PF_TYPE <= PF.PF_TYPE)
            {
                if((PF.PF_VALUE & pChan->ED137B_PersistentFeature.PersistentFeature_Mask) != ( pChan->ED137RxState.LAST_PF_VALUE & pChan->ED137B_PersistentFeature.PersistentFeature_Mask))
                {
                    if(*PayldType == ExtED137_KEEP_ALIVE_PAYLOAD_TYPE)
                    {
                        pChan->ED137RxState.LAST_PF_VALUE = PF.PF_VALUE;
                        //Post Event Persistent Feature change
                        ED137PersistentMsg (pChan->ChannelId, deviceSide, PF.PF_TYPE, PF.PF_VALUE, PayldType, 4);
                    }
                }
                if(MIN_ED137_EXT_LENGTH < ED137Ext.LENGTH)
                {
                    //Post Event Non-Persistent Feature
                    ED137NonPersistentMsg (pChan->ChannelId, deviceSide, &ExtData->AF_VALUE[2], ((ED137Ext.LENGTH - MIN_ED137_EXT_LENGTH) * sizeof(ADT_UInt32)));
                }
            }
            else if(ExtData->AF_VALUE[0]!=0)
            {
                ED137NonPersistentMsg (pChan->ChannelId, deviceSide, &ExtData->AF_VALUE[0], ((ED137Ext.LENGTH - MIN_ED137_EXT_LENGTH) * sizeof(ADT_UInt32)));
            }
        }
    }
    return retval;
}

#pragma CODE_SECTION (getPacketFromRTP, "PKT_PROG_SECT")
// Called by framing task to retrieve next encoded packet (RTP) for decode and playout
packetStatus_t getPacketFromRTP (chanInfo_t *chan, PktHdr_t *PktHdr, ADT_UInt16 *pyld, ADT_UInt16 maxPayI8) {

   RtpChanData_t *RTPChan;
   int  statUpdate, rtpSamplingRate;
   GpakPayloadClass pyldClass;
   ADT_UInt40 rtpFrameStart40, TimeStart40, TimeEnd40;
   ADT_UInt32 rtpFrameStart, rtpTimeStart, rtpTimeEnd;
   ADT_UInt16 numPktsAvail;
   pkt2pcm_t *PktPcm;
   PktExtHdr_t PktExtHdr;
   ExtRXStatus_t ExtRXStatus;
   ADT_UInt16 R2SPayldType;
   ADT_UInt16 Frame_Period;

   // skip underflow stats if t38 fax is in progress
   statUpdate = !chan->fax.t38InProgress;

   // -------------------------------------------------------
   //   Read packet(s) from jitter buffer
   //
   //   No packet => exit without packet
   //   Non-t38 packet => exit with packet
   //   t38 packet => get next packet within time frame
   RTPChan = RtpChanData [chan->ChannelId];
   PktPcm = &chan->pktToPcm;

   TimeStart40 = PktPcm->FrameTimeStamp;
   TimeEnd40   = TimeStart40 + PktPcm->SamplesPerFrame;

   // Change TDM rate to RTP rate.
   rtpSamplingRate = RTPChan->Instance.SampleRate;
   if (TDMRate < rtpSamplingRate) {
      TimeStart40 *= (rtpSamplingRate / TDMRate);
      TimeEnd40   *= (rtpSamplingRate / TDMRate);
   } else if (rtpSamplingRate < TDMRate) {
      TimeStart40 /= (TDMRate / rtpSamplingRate);
      TimeEnd40   /= (TDMRate / rtpSamplingRate);
   }
   TimeEnd40--;
   rtpFrameStart40 = TimeStart40;
   rtpTimeStart = (ADT_UInt32) TimeStart40;
   rtpTimeEnd   = (ADT_UInt32) TimeEnd40;
   rtpFrameStart = rtpTimeStart;

   Frame_Period = PktPcm->SamplesPerFrame/(rtpSamplingRate/1000);

   if(chan->ED137RxState.RxState == RX_KEEP_ALIVE)
   {
       chan->ED137RxState.KeepAliveCount ++;
   }
   do {

PullNextPkt:

      // Read RTP packet from jitter buffer

      PktHdr->PayldBytes = maxPayI8;
      PktHdr->PayldType  = 0xff;
      PktHdr->PayldClass = PayClassNone;

      PktExtHdr.ExtData[0] = 0;     // Receive Extension API requires these parameters to be initialized.
      PktExtHdr.ExtDataI8Len = 1;

      numPktsAvail = RTP_ADT_ReceiveExt ((RTPCONNECT *) &RTPChan->Instance, &rtpTimeStart,
           rtpTimeEnd, (ADT_UInt32 *) pyld, &PktHdr->PayldBytes, &PktHdr->PayldType,
           &PktExtHdr.Marker, &PktExtHdr.SSCR,
           &PktExtHdr.CSRC[0], &PktExtHdr.CSRCCnt,
           &PktExtHdr.ExtData[0], &PktExtHdr.ExtDataI8Len);

      if ((numPktsAvail == 0) && (chan->R2SRxExT.EXT_LENGTH == 0))
      {
         if(chan->RtpRxTimeoutValue != 0)
         {
             chan->RtpRxTimeoutCount++;
             if( ((chan->RtpRxTimeoutValue)/Frame_Period) < (chan->RtpRxTimeoutCount))
             {
                 chan->RtpRxTimeoutCount = 0;
                 RtpRxTimeoutMsg (chan->ChannelId);
             }
         }
      }
      if(PktExtHdr.ExtData[0])    // Extension data available
      {
               ExtRXStatus  = ProcessHeaderExtension(chan, (ED137RxPkt_t *) &PktExtHdr.ExtData[0], &PktHdr->PayldType);
               if(ExtRXStatus == EXT_RX_NO_DATA)     // Extension specifies to not send data
               {
                   numPktsAvail = 0;
                   statUpdate = 0;
               }
      }
      else if(chan->R2SRxExT.EXT_LENGTH != 0)
      {
          R2SPayldType = ExtED137_KEEP_ALIVE_PAYLOAD_TYPE;
          ExtRXStatus  = ProcessHeaderExtension(chan, &chan->R2SRxExT, &R2SPayldType);
          chan->R2SRxExT.EXT_LENGTH = 0;
          if(ExtRXStatus == EXT_RX_NO_DATA)     // Extension specifies to not send data
          {
              numPktsAvail = 0;
              statUpdate = 0;
          }
      }

      if(chan->ED137RxState.RxState == RX_KEEP_ALIVE)
      {
          if((chan->ED137BcfgParms.R2SKeepAlivePeriod/Frame_Period) < chan->ED137RxState.KeepAliveCount)
          {
              if(chan->ED137RxState.KeepAliveErrorCount < chan->ED137BcfgParms.R2SKeepAliveMultiplier)
              {
                   chan->ED137RxState.KeepAliveErrorCount++;
                   chan->ED137RxState.KeepAliveCount = KEEP_ALIVE_RECEIVED_RESET;
              }
              else
              {
                  //Post Event Keep Alive Timeout
                  ED137KeepAliveErrorMsg (chan->ChannelId);
                  chan->ED137RxState.KeepAliveErrorCount = KEEP_ALIVE_RECEIVED_RESET;
                  chan->ED137RxState.KeepAliveCount = KEEP_ALIVE_RECEIVED_RESET;
              }
          }
      }
      if (numPktsAvail == 0) PktHdr->PayldBytes = 0;

      // Change RTP timestamp to TDM timestamp.
      TimeStart40 = rtpFrameStart40 + (rtpTimeStart - rtpFrameStart);

#ifdef _DEBUG   // For consistent time stamp logging
      PktHdr->TimeStamp = (ADT_UInt32) TimeStart40;
#endif
      logRtp (chan->ChannelId, PKTFROMJITTER, PktHdr->PayldBytes, (void *) &PktHdr->PayldClass);

      // Adjust time stamp to TDMRate for tone forwarding.
      if (TDMRate < rtpSamplingRate) {
         TimeStart40 *= (rtpSamplingRate / TDMRate);
         TimeEnd40   *= (rtpSamplingRate / TDMRate);
      } else if (rtpSamplingRate < TDMRate) {
         TimeStart40 /= (TDMRate / rtpSamplingRate);
         TimeEnd40   /= (TDMRate / rtpSamplingRate);
      }
      PktHdr->TimeStamp = (ADT_UInt32) TimeStart40;

      if (0 == numPktsAvail) {
         if (statUpdate) {
            chan->status.Bits.packetUnderflow = 1;
            chan->underflowCount++;
            STATS (RTPStats.rx.underflow++;)
         }
         logTime (0x04070000 | chan->ChannelId);
         return noPacketAvailable;
      }
      STATS (RTPStats.rx.JB++;)
      logTime (0x04070000 | (PktHdr->PayldBytes << 8) | chan->ChannelId);

      // Convert RTP payload type to G.PAK class and type
      RTPToGPakPayloadType (&RTPChan->Profile,    PktHdr->PayldType,
                            &pyldClass,          &PktHdr->PayldType);

      PktHdr->ChanId = chan->ChannelId;
      PktHdr->PayldClass = pyldClass;
      if (PktHdr->PayldType == DefaultCodec)
         PktHdr->PayldType = chan->pktToPcm.PreferredCoding;

      if (!verifyPacketHeader (PktHdr, chan)) {
         STATS (RTPStats.rx.hdrDiscards++;)
         return invalidPktHeader;
      }

      chan->fromHostPktCount++;

      // continue pulling packets that are available
      if ((1 < numPktsAvail) && (pyldClass != PayClassT38)) { 
         goto PullNextPkt;
      }

      if (pyldClass != PayClassT38)
         return nonT38PktAvailable;

      // Add packet to Fax Relay buffer.
      if (!T38_Add_Packet (chan, pyld, PktHdr->PayldBytes, rtpTimeStart))
         break;
   } while (TRUE);


   return relayFull;
}


ADT_UInt8 FormatHeaderExtension(chanInfo_t *pChan, ADT_UInt8  *ExtData,  ADT_UInt32  *ExtDataI8Len)
{
    ADT_UInt16 NPF_Cnt, NPF_value_cnt, NPF_value_index=0;
    ADT_UInt8 retval = 0;

    if( !(pChan->ED137TxState.TxState & TX_ACTIVE))
    {
        pChan->ED137TxExT.EXT_VER      = ExtVer1;                            // Initialize the ED137 Extension Structure
        pChan->ED137TxExT.EXT_TYPE     = ExtED137;
        pChan->ED137TxExT.RSVD1        = 0;
        pChan->ED137TxExT.EXT_LENGTH   = MIN_ED137_EXT_LENGTH;
        pChan->ED137TxExT.PTT_VALUE_H  = (pChan->ED137BcfgParms.Ptt_val >> 8) & 0xFF;
        pChan->ED137TxExT.PTT_VALUE_L  = (pChan->ED137BcfgParms.Ptt_val) & 0xFF;
        pChan->ED137TxExT.PF_VALUE_H   = (pChan->ED137B_PersistentFeature.PersistentFeature_Type << PF_TYPE_SHIFT) | ED137_PF_LENGTH ;
        pChan->ED137TxExT.PF_VALUE_L   = pChan->ED137B_PersistentFeature.PersistentFeature_Value;
        pChan->ED137TxState.TxState |= TX_ACTIVE;
    }

    if( pChan->ED137TxState.TxState & TX_UPDATE_PTT)
    {
        pChan->ED137TxExT.PTT_VALUE_H = (pChan->ED137BPTT_Parms.PTT_Type << PTT_TYPE_SHIFT) | (pChan->ED137BPTT_Parms.SQU << SQU_SHIFT) |(pChan->ED137BPTT_Parms.PTT_ID >> PTT_ID_SHIFT_H);
        pChan->ED137TxExT.PTT_VALUE_L = (((pChan->ED137BPTT_Parms.PTT_ID & PTT_ID_MASK_L) << PTT_ID_SHIFT_L)
                                                                      | ((pChan->ED137BPTT_Parms.PM & PM_MASK) << PM_SHIFT)
                                                                      | ((pChan->ED137BPTT_Parms.PTTS & PTTS_MASK) << PTTS_SHIFT)
                                                                      | ExtED137_EXTENSION_ENABLED_X);

        pChan->ED137TxState.TxState = pChan->ED137TxState.TxState & (TX_UPDATE_PTT ^ 0xFFFF);
    }
    if(pChan->ED137TxState.TxState & TX_UPDATE_PF_MSG)
    {
        pChan->ED137TxExT.PF_VALUE_H = (pChan->ED137B_PersistentFeature.PersistentFeature_Type << 4) | ED137_PF_LENGTH;  // Length 1
        pChan->ED137TxExT.PF_VALUE_L = pChan->ED137B_PersistentFeature.PersistentFeature_Value;

        pChan->ED137TxState.TxState = pChan->ED137TxState.TxState & (TX_UPDATE_PF_MSG ^ 0xFFFF);
    }
    if(pChan->ED137TxState.TxState & TX_NON_PF_MSG)
    {
        pChan->ED137TxExT.EXT_LENGTH = MIN_ED137_EXT_LENGTH;
        for(NPF_Cnt = 0; NPF_Cnt < pChan->ED137B_NPF.Num_NPF; NPF_Cnt++)
        {
            pChan->ED137TxExT.NPF_VALUE[NPF_value_index] = pChan->ED137B_NPF.ED137B_NONPersistentFeatureParms[NPF_Cnt].NONPersistentFeature_Type;
            NPF_value_index++;
            pChan->ED137TxExT.NPF_VALUE[NPF_value_index] =pChan->ED137B_NPF.ED137B_NONPersistentFeatureParms[NPF_Cnt].NONPersistentFeature_Length;
            NPF_value_index++;
            for(NPF_value_cnt = 0; NPF_value_cnt < pChan->ED137B_NPF.ED137B_NONPersistentFeatureParms[NPF_Cnt].NONPersistentFeature_Length;NPF_value_cnt++)
            {
                pChan->ED137TxExT.NPF_VALUE[NPF_value_cnt+NPF_value_index] = pChan->ED137B_NPF.ED137B_NONPersistentFeatureParms[NPF_Cnt].NONPersistentFeature_Value[NPF_value_cnt];
            }
            NPF_value_index += pChan->ED137B_NPF.ED137B_NONPersistentFeatureParms[NPF_Cnt].NONPersistentFeature_Length;
        }
        pChan->ED137TxExT.NPF_VALUE[NPF_value_index] = 0x00;  // Padding
        pChan->ED137TxExT.NPF_VALUE[NPF_value_index+1] = 0x00;
        pChan->ED137TxExT.NPF_VALUE[NPF_value_index+2] = 0x00;
        NPF_value_index++;
        pChan->ED137TxExT.EXT_LENGTH += (NPF_value_index+3)/sizeof(ADT_Int32);
        pChan->ED137TxState.TxState = pChan->ED137TxState.TxState & (TX_NON_PF_MSG ^ 0xFFFF);
        pChan->ED137TxState.TxState = pChan->ED137TxState.TxState & (TX_UPDATE_PF_MSG ^ 0xFFFF);
    }
    else
    {
        pChan->ED137TxExT.EXT_LENGTH = MIN_ED137_EXT_LENGTH;  // Default Length to 1
        memset(&pChan->ED137TxExT.NPF_VALUE[0],0,16);
    }
    if ((pChan->ED137TxExT.NPF_VALUE[0] != 0)
                           && !(VALID_ED137_PF_TYPE <= pChan->ED137B_PersistentFeature.PersistentFeature_Type))
    {
        memcpy(ExtData, &pChan->ED137TxExT,((ED137_EXT_HDR_LENGTH+2)*sizeof(ADT_Int16)));
        memcpy(&ExtData[6], &pChan->ED137TxExT.NPF_VALUE,((pChan->ED137TxExT.EXT_LENGTH)*sizeof(ADT_Int32)));
    }
    else if (VALID_ED137_PF_TYPE <= pChan->ED137B_PersistentFeature.PersistentFeature_Type)
    {
        memcpy(ExtData, &pChan->ED137TxExT,((pChan->ED137TxExT.EXT_LENGTH +ED137_EXT_HDR_LENGTH)*sizeof(ADT_Int32)));
    }
    else
    {
        memcpy(ExtData, &pChan->ED137TxExT,((ED137_EXT_HDR_LENGTH+2)*sizeof(ADT_Int16)));
    }

    /*
    * RTP/Voice packets being generated by the DSP , the Squelch bit will be set and not be set if it is silence (R2S) packets.
    */
    if(pChan->ED137BcfgParms.follow_squ_with_vad)
    {
       if(pChan->ED137TxState.TxState & TX_KEEP_ALIVE)
       {
           ExtData[4] &= (1 << SQU_SHIFT) ^ 0xFF;
       }
       else
       {
           ExtData[4] |= (1 << SQU_SHIFT);
       }
    }
    if(pChan->ED137TxState.TxState & TX_PTT_OFF)
    {
  //     pChan->ED137TxExT.EXT_LENGTH = MIN_ED137_EXT_LENGTH;  // Default Length to 1
 //      memcpy(ExtData, &pChan->ED137TxExT,((pChan->ED137TxExT.EXT_LENGTH + ED137_EXT_HDR_LENGTH)*sizeof(ADT_Int32)));
       ExtData[4] &= PTT_TYPE_MASK;                        // Update PTT Type to OFF
       ExtData[4] |=  (PTT_OFF << PTT_TYPE_SHIFT);
  //     *ExtDataI8Len = pChan->ED137TxExT.EXT_LENGTH;
  //     retval = TX_PTT_OFF;
 //      return retval;
     }
    *ExtDataI8Len = pChan->ED137TxExT.EXT_LENGTH;
    retval = pChan->ED137TxState.TxState;

    return retval;
}

#pragma CODE_SECTION (sendPacketToRTP, "PKT_PROG_SECT")
// Called by framing task to packetize (RTP) and send newly encoded payload to IP stack
int sendPacketToRTP (chanInfo_t *chan, PktHdr_t *Hdr, ADT_UInt16 *pyld) {
   RtpChanData_t *RTPChan;
   ADT_UInt16 payloadType, rtnVal;
   PktExtHdr_t PktExtHdr;
   ADT_UInt8 ExtStatus;

   RTPChan = RtpChanData[chan->ChannelId];

   STATS (RTPStats.tx.total++;)
   payloadType = GpakToRTPPayloadType (&RTPChan->Profile, (GpakPayloadClass) Hdr->PayldClass, Hdr->PayldType);
   if (payloadType == 0xffff) {
      STATS (RTPStats.tx.discards++;)
      return 0;
   }
   if((chan->ED137BcfgParms.rtp_ext_en) || (chan->ED137TxState.TxState & TX_KEEP_ALIVE))
   {
      ExtStatus  = FormatHeaderExtension(chan,
                                         (ADT_UInt8*)&PktExtHdr.ExtData[0],
                                         &PktExtHdr.ExtDataI8Len);
      if(ExtStatus & TX_KEEP_ALIVE)
      {
          payloadType = ExtED137_KEEP_ALIVE_PAYLOAD_TYPE;
          Hdr->PayldBytes = 0;
      }
      if(ExtStatus & TX_PTT_OFF)
      {
          Hdr->PayldBytes = 0;
      }

      PktExtHdr.ExtDataI8Len = (ED137_EXT_HDR_LENGTH*sizeof(ADT_Int32)) + (PktExtHdr.ExtDataI8Len*4);
      PktExtHdr.CSRCCnt = 0;
      PktExtHdr.CSRC[0] = 0;
      rtnVal = RTP_ADT_SendExt((RTPCONNECT *) &RtpChanData [chan->ChannelId]->Instance, (ADT_UInt32 *) pyld,
            Hdr->PayldBytes, payloadType, Hdr->TimeStamp, Hdr->PayldClass == PayClassStartTone,
            &PktExtHdr.CSRC[0],
            PktExtHdr.CSRCCnt,
            &PktExtHdr.ExtData[0],
            PktExtHdr.ExtDataI8Len);

      if(!(PktExtHdr.ExtData[4] & 0xE0) && !(chan->ED137TxState.TxState & TX_PTT_OFF) && !(ExtStatus & TX_KEEP_ALIVE))     // PTT OFF Sent via Command Not through VAD
      {
          chan->ED137TxState.TxState |= TX_PTT_OFF;
      }
   }
   else
   {
      rtnVal = RTP_ADT_Send ((RTPCONNECT *) &RtpChanData [chan->ChannelId]->Instance, (ADT_UInt32 *) pyld,
            Hdr->PayldBytes, payloadType, Hdr->TimeStamp, Hdr->PayldClass == PayClassStartTone);
   }
   STATS (if (!rtnVal) RTPStats.tx.sendFailures++;)
   FLUSH_INST_CACHE (&RtpChanData [chan->ChannelId]->Instance, sizeof (RTPCONNECT));
   return rtnVal;
}

#pragma CODE_SECTION (sendHostCoreMsg, "FAST_PROG_SECT")
int sendHostCoreMsg (ADT_UInt32* Data, int dataI8) {
   CircBufInfo_t *RTPCirc;
   RTPToHostHdr_t Hdr;
   int sent, mask;

   Hdr.ChanId = 0xfffe;
   Hdr.PktI8  = dataI8;

   // Place message in RTP circular buffer using signaling channel 0xfffe
   RTPCirc = &RTPCircBuffers[(DSPCore * 2) + 1];
   mask = HWI_disable ();
   sent = PayloadToCircCache (RTPCirc, &Hdr, RTP_TO_HOST_I16, (ADT_UInt8 *)Data, dataI8);
   HWI_restore (mask);
   return sent;  
}

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// HOSTSWI
//
// FUNCTION
//   Software interrupt to transfer RTP packets from host to DSP.  Invoked by HWI from
//   host or end of framing tasks.
//   
//   Remove all packets from RTP circular buffers and transfer to jitter buffers
//   SRTP is performed within this SWI because the RTP header needed by SRTP
//   is not maintained by the jitter buffer and thus is not available to the framing tasks.
//
//   HOSTSWI runs at a higher priority than the framing tasks.
//
//   Packets are discarded when framing MIPS overruns occur.
//
//   The amount of processing time that is allowed for this SWI is throttled to half the time 
//   allotted to the fastest configured frame task.
//
//}

//  Field positions within 1st 16 bits of RTP Packet
#define RTP_VERSION_MASK    0xC000  // Version number mask
#define RTP_CURRENT_VERSION 0x8000  // Current version
#define RTP_PADDING_BIT     0x2000  // Padding bit field
#define RTP_EXTEND_BIT      0x1000  // Extension bit field
#define RTP_CC_MASK         0x0f00  // SSRC count field
#define RTP_MARKER_BIT      0x0080  // Marker bit field
#define RTP_PAYLD_MASK      0x007f  // Payload type field

#pragma CODE_SECTION (HOSTSWI, "FAST_PROG_SECT")
static void HOSTSWI () {

   // Host packet transfers.
   CircBufInfo_t* RTPToDSPCirc;
   NCHdr_t        Hdr;
   void*          rtpBuffer;

   // Packet handling
   int        ChanId;
   ADT_UInt16 PktI8;
   ADT_UInt8  pyldType;

   int offsetI8 = 0;
   int offsetI8R2S = 0;
   int extI32LenR2S =0;

   // RTP buffering
   PKTSTATUS      rtpStat;
   RtpChanData_t* RTPChan;
   chanInfo_t*    chan;

   RTPDATA  *RTP;

   // Time management
   ADT_UInt32 startTime, TicksToComplete;
   FrameTaskData_t *RptTsk;

   logTime (0x80000010);

   RTPToDSPCirc = &RTPCircBuffers[DSPCore * 2];
   startTime = CLK_gethtime();

   // Pull RTP packets from host transfer buffer and forward to t38 or jitter buffers
   do {

      rtpBuffer = GetPktFromTransferBuffers (RTPToDSPCirc, &Hdr);
      if (rtpBuffer == NULL) break;

      STATS (RTPStats.rx.total++;)

      // Discard packets when frame overloading occurs to reduce overhead
      if (FrameOverloaded) {
         //RTPPayloadFree (Hdr.PktHndl, Hdr.RTPBuff, Hdr.PktI8);
         RTPFree (rtpBuffer);
         PktI8 = PurgeTransferBuffer (RTPToDSPCirc);
         STATS (RTPStats.rx.folDiscards += PktI8;)
         break;
      }

      rtpBuffer = customPktIn (&Hdr, rtpBuffer);
      ChanId = Hdr.ChanId;
      PktI8  = Hdr.PktI8;
      chan   = chanTable[ChanId];


      if (sysConfig.maxNumChannels <= ChanId) {
         AppErr ("RTP buffer error", ADT_TRUE);
         //RTPPayloadFree (Hdr.PktHndl, Hdr.RTPBuff, Hdr.PktI8);
         RTPFree (rtpBuffer);
         PktI8 = PurgeTransferBuffer (RTPToDSPCirc);
         STATS (RTPStats.rx.cidDiscards += PktI8;)
         break;
      }

      // Discard packets going to an inactive or non-receptive channel
      if ((chan->channelType == inactive) ||
          (chan->Flags & CF_RTP_RX_DISABLED) ||
          (chan->pktToPcm.Coding == NullCodec)) {
         //RTPPayloadFree (Hdr.PktHndl, Hdr.RTPBuff, Hdr.PktI8);
         RTPFree (rtpBuffer);
         STATS (RTPStats.rx.inaDiscards++;)
         continue;
      }
      chan->RtpRxTimeoutCount = 0;
      // consume RTCP packets  
	  #ifdef RTCP_ENABLED
      if (rtcp_rx_packet(ChanId, rtpBuffer, PktI8)) {
         RTPFree (rtpBuffer);
         continue;
      }
	  #endif

      if(chan->pktToPcm.Coding == G722) { // Sampling rate adjustment
	     ADT_UInt16 Sequence, BitFlds, extI32Len, rtpSamplingRate, pt, marker;
		 ADT_UInt32 deltaTimeStamp;
		 ADT_UInt32 TimeStamp;
         int hdrI8Len, pyldI8Len, paddingI8Len, CSRCCount;
         rtpSamplingRate = RtpChanData[ChanId]->Instance.SampleRate;

         if (chan->G722RtpSamplingRate == 0) { // don't know if RTPG722 use 16K(wrong) yet
            // first packet sequence
            offsetI8 = 0; 
            BitFlds   = netI16 (rtpBuffer, &offsetI8);
            marker = BitFlds & RTP_MARKER_BIT;
            offsetI8 = 2;
            Sequence  = netI16 (rtpBuffer, &offsetI8); // Convert header fields from network byte order
            offsetI8 = 4; 
            TimeStamp = netI32 (rtpBuffer, &offsetI8);
            chan->G722RtpSequence = Sequence;
			chan->G722RtpTimeStamp = TimeStamp;
            chan->G722Marker = marker;
            saveRTPHeader(chan->G722RtpSamplingRate, rtpBuffer, &g722RTPStats[ChanId].g722Sync[0], PktI8, g722RTPStats[ChanId].sr1_count);
			chan->G722RtpSamplingRate = 1; // state machine changed to 1
            g722RTPStats[ChanId].sr0_count++;
         } else if (chan->G722RtpSamplingRate == 1) { // don't know if RTPG722 use 16K(wrong) yet
            // second packet sequence
            g722RTPStats[ChanId].sr1_count++;
            offsetI8 = 0; 
            BitFlds   = netI16 (rtpBuffer, &offsetI8);
            pt = BitFlds & RTP_PAYLD_MASK;
            marker = BitFlds & RTP_MARKER_BIT;
            offsetI8 = 2;
            Sequence  = netI16 (rtpBuffer, &offsetI8); // Convert header fields from network byte order
            offsetI8 = 4; 
            TimeStamp = netI32 (rtpBuffer, &offsetI8);

            if ((pt == 9) && ((Sequence - chan->G722RtpSequence) == 1) && (marker == 0)  && (chan->G722Marker == 0)) { // consecutive g.722 packets detected, must be g.722 payload type - not CNG.
			   deltaTimeStamp = TimeStamp - chan->G722RtpTimeStamp;
               
			   // Get payload size
               // Convert CSRC fields from network byte order
               CSRCCount = (BitFlds & RTP_CC_MASK)>>8;
               offsetI8 = 12;
               offsetI8 += CSRCCount * 4;
               if (BitFlds & RTP_EXTEND_BIT) {
                  offsetI8 += 2;           // Skip ProfileVars
                  extI32Len = netI16 (rtpBuffer, &offsetI8);
                  offsetI8 += extI32Len*4; // Position offsetI8 to point beyond the extension data
               }
               hdrI8Len = offsetI8;
               // Calculate payload size by subtracting the header length
               // and padding length from the packet size
               pyldI8Len = PktI8 - hdrI8Len;
               if ((BitFlds & RTP_PADDING_BIT) != 0) {
                  offsetI8 = PktI8 - 1;
                  paddingI8Len = netI8 (rtpBuffer, &offsetI8);
                  pyldI8Len -= paddingI8Len;
               }

			   if(deltaTimeStamp > (ADT_UInt32)pyldI8Len) { // 160 > 80
			      chan->G722RtpSamplingRate = 16000; // state machine changed to 16000
			   } else {
			      chan->G722RtpSamplingRate = 8000; // state machine changed to 8000
               }
			} else {
               chan->G722RtpSequence = Sequence;
			   chan->G722RtpTimeStamp = TimeStamp;
               chan->G722Marker = marker;
			}
            saveRTPHeader(chan->G722RtpSamplingRate, rtpBuffer, &g722RTPStats[ChanId].g722Sync[0], PktI8, g722RTPStats[ChanId].sr1_count);
		 } else if (chan->G722RtpSamplingRate == 16000) {
            if(rtpSamplingRate == 8000) {
               g722RTPStats[ChanId].sr16_adj_count++;
               offsetI8 = 4;
               TimeStamp = netI32 (rtpBuffer, &offsetI8);
			   TimeStamp /= 2;
			   offsetI8 = 4;
			   I32net (rtpBuffer, &offsetI8, TimeStamp);
            } else {
               g722RTPStats[ChanId].sr16_nadj_count++;
            }
		 } else if (chan->G722RtpSamplingRate == 8000) {
            if(rtpSamplingRate == 16000) {
               g722RTPStats[ChanId].sr8_adj_count++;
               offsetI8 = 4;
               TimeStamp = netI32 (rtpBuffer, &offsetI8);
			   TimeStamp *= 2;
			   offsetI8 = 4;
			   I32net (rtpBuffer, &offsetI8, TimeStamp);
            } else {
               g722RTPStats[ChanId].sr8_nadj_count++;
            }
		 }
      } // end of 'if codec is G722'

      logTime (0x04080000 | ChanId);

      offsetI8 = 1;
      pyldType = netI8 (rtpBuffer, &offsetI8) & 0x7f;

#ifdef SRTP_VALIDATE
      if (!srtp_validate) {
#endif
      // Run SRTP if enabled and SRTP channel already configed
      if ((sysConfig.SRTP_Enable) && (SrtpRxKeyData[ChanId]->Configured)) {
         PktI8 = SrtpDecryptU8 (SrtpRxChanData[ChanId]->Instance, rtpBuffer, PktI8, rtpBuffer);
         if (PktI8 <= 0) {
            STATS (RTPStats.rx.srtpDiscards++;)
         }
      }
#ifdef SRTP_VALIDATE
     }
#endif
      // Pass T38 packets directly to T38, all others to jitter buffer
      RTPChan = RtpChanData[ChanId];

      if (pyldType == ExtED137_KEEP_ALIVE_PAYLOAD_TYPE)
      {
            offsetI8R2S = 14;// +(CSRCCountR2S * 4);
            extI32LenR2S = netI16 (rtpBuffer, &offsetI8R2S);
            offsetI8R2S -=4;
            if(0 < extI32LenR2S < 5)
            {
                chan->R2SRxExT.EXT_VER      = netI8 (rtpBuffer, &offsetI8R2S);
                chan->R2SRxExT.EXT_TYPE     = netI8 (rtpBuffer, &offsetI8R2S);
                chan->R2SRxExT.RSVD1        = netI8 (rtpBuffer, &offsetI8R2S);
                chan->R2SRxExT.EXT_LENGTH   = netI8 (rtpBuffer, &offsetI8R2S);
                chan->R2SRxExT.PTT_VALUE_H  = netI8 (rtpBuffer, &offsetI8R2S);
                chan->R2SRxExT.PTT_VALUE_L  = netI8 (rtpBuffer, &offsetI8R2S);
                chan->R2SRxExT.AF_VALUE[0]  = netI8 (rtpBuffer, &offsetI8R2S);
                chan->R2SRxExT.AF_VALUE[1]  = netI8 (rtpBuffer, &offsetI8R2S);
                if(1 < extI32LenR2S)
                {
                    for(offsetI8 = 2; offsetI8+4 < extI32LenR2S*4; offsetI8 += 4)
                    {
                        chan->R2SRxExT.AF_VALUE[offsetI8]    = netI8 (rtpBuffer, &offsetI8R2S);
                        chan->R2SRxExT.AF_VALUE[offsetI8+1]  = netI8 (rtpBuffer, &offsetI8R2S);
                        chan->R2SRxExT.AF_VALUE[offsetI8+2]  = netI8 (rtpBuffer, &offsetI8R2S);
                        chan->R2SRxExT.AF_VALUE[offsetI8+3]  = netI8 (rtpBuffer, &offsetI8R2S);
                    }
                }
            }

            RTP = (RTPDATA  *) &RtpChanData[ChanId]->Instance;
            RTP->stats.PktsReceived++;
            //RTPPayloadFree (Hdr.PktHndl, Hdr.RTPBuff, Hdr.PktI8);
            RTPFree (rtpBuffer);
       }
       else if (pyldType == RTPChan->Profile.T38_PROFILE) {
         T38_Add_Packet (chan, rtpBuffer, PktI8, 0);
         //RTPPayloadFree (Hdr.PktHndl, Hdr.RTPBuff, Hdr.PktI8);
         RTPFree (rtpBuffer);
       } else {
            logRtp (ChanId, PKTTOJITTER, PktI8, (ADT_UInt32 *) rtpBuffer);
            rtpStat = RTP_ADT_BufferPacketNc ((RTPCONNECT *) &RTPChan->Instance, 
                                           rtpBuffer, PktI8, Hdr.PktHndl);
#ifdef _DEBUG_RTP_JITTER
            ADT_RTP_Ptr = (RTPDATA *)&RTPChan->Instance;
	        if((ADT_RTP_Ptr->stats.UnderflowCnt > 0) && (firstRTPChan==0)) {
		        memcpy(&ADT_RTP_Chan, ADT_RTP_Ptr, sizeof(RtpChanData_t));
		        firstRTPChan = 1;
	        }
#endif //ifdef _DEBUG_RTP_JITTER
	        if (rtpStat & PKT_INVALID) {
                STATS (RTPStats.rx.invDiscards++;)
                logRtp (ChanId, PKTDENIED, rtpStat, 0);
                if(chan->pktToPcm.Coding == G722) { // Sampling rate adjustment
                    g722RTPStats[ChanId].discard_count++;
                }
            }

            logTime (0x04060000 | ChanId);
      }

      // Break if frame packet processing takes longer than 1/2 fastest frame rate
      if (HostSWIMaxTicks < (CLK_gethtime() - startTime))
      {
         break;
      }

   } while (rtpBuffer);

   FrameOverloaded = ADT_FALSE;
   logTime (0x80010010);

   if (!reportRTPUsage) return;

   TicksToComplete = CLK_gethtime() - startTime;

   RptTsk = &FrameTaskData[FRAME_TASK_CNT + DSPCore];

   // compute Max loading
   if (RptTsk->pkUsageTicks < TicksToComplete) {
      // Report Peak usage as 1/10 percent of total CPU MIPs
      RptTsk->pkUsageTicks = TicksToComplete;
      *(RptTsk->pCpuPkUsage) = (((ADT_UInt32)RptTsk->pkUsageTicks * 10L) / (HostSWIMaxTicks/100L)) + 1;
   }

   RptTsk->ProcTimeSum += (TicksToComplete >> SUM_SHIFT);
   if (TIME_AVG_CNT <= ++RptTsk->TimeAvgCnt) {
      // Report CPU usage as 1/10 percent
      *(RptTsk->pCpuUsage) = ((ADT_UInt32)RptTsk->ProcTimeSum * 10L) / (HostSWIMaxTicks/100L);

      RptTsk->ProcTimeSum = 0;
      RptTsk->TimeAvgCnt  = 0;
   }


}

#pragma CODE_SECTION (postRtpHostSWI, "PKT_PROG_SECT")
void postRtpHostSWI (ADT_Bool overloaded, ADT_UInt32 ticksPerFrame) {
   logTime (0x000F0001);
   if (SWI_Host == NULL) return;

   ticksPerFrame = ticksPerFrame / 2;
   FrameOverloaded |= overloaded;
   if (SysAlgs.rtpHostDelivery || SysAlgs.rtpNetDelivery) {

#ifdef SINGLE_CORE_STACK
      if (isNetRtpRxPktReady() != 0)
#else
      if (RTPCircBuffers[DSPCore * 2].TakeIndex != RTPCircBuffers[DSPCore * 2].PutIndex)
#endif

      {
         logTime (0x000F0002);
         SWI_post (SWI_Host);
         if (ticksPerFrame < HostSWIMaxTicks) HostSWIMaxTicks = ticksPerFrame;
      }
   }
   logTime (0x000F0003);
}

#pragma CODE_SECTION (loopBackPkts, "PKT_PROG_SECT")
void loopBackPkts (RTPToHostHdr_t *Hdr) {
   ADT_UInt16 chID;

   // This routine changes the channel to which the packet is directed according to the loopback mode

   //  PacketLoopback retains channel number to return packet to same channel
   //  PacketCrossover pairs even and odd channels
   //  PacketDaisyChain forms channel link as follows.  x.y are paired decode.encode channels
   //  
   //  PcmPkt                                                          PcmPkt
   //    0  <->  1:2  <->  3:4  <->  5:6  <->  ...  <->  n-2:n-1  <->    n
   // PCM to Pkt channels are at ends to terminate chains
   chID = Hdr->ChanId;
   if (RTPLoopBack == PacketCrossOver) {
      Hdr->ChanId = chID ^ 1;   // Perform odd/even channel crossover
   } else if (RTPLoopBack == PacketDaisyChain) {
      switch (chanTable[chID]->channelType) {
      case pcmToPacket:
      case packetToPacket: 
         if (chID & 1) chID -= 1;
         else          chID += 1;
         break;

      default:
         break;
      }
      Hdr->ChanId = chID;
   }
   return;
}
void saveRTPHeader(int rtp_sr, void *rtpBuffer, g722RTPSync_t *g722_rtpSync, ADT_UInt16 PktI8, ADT_UInt32 sr1_count) {

ADT_UInt16 Sequence, BitFlds, extI32Len;
ADT_UInt32 TimeStamp;
int hdrI8Len, pyldI8Len, paddingI8Len, CSRCCount, offsetI8;
g722RTPSync_t *sync;

    offsetI8 = 2;
    Sequence  = netI16 (rtpBuffer, &offsetI8); // Convert header fields from network byte order
    offsetI8 = 4; 
    TimeStamp = netI32 (rtpBuffer, &offsetI8);

	// Get payload size
    offsetI8 = 0; 
    BitFlds   = netI16 (rtpBuffer, &offsetI8);

    // Convert CSRC fields from network byte order
    CSRCCount = (BitFlds & RTP_CC_MASK)>>8;
    offsetI8 = 12;
    offsetI8 += CSRCCount * 4;
    if (BitFlds & RTP_EXTEND_BIT) {
       offsetI8 += 2;           // Skip ProfileVars
       extI32Len = netI16 (rtpBuffer, &offsetI8);
       offsetI8 += extI32Len*4; // Position offsetI8 to point beyond the extension data
    }
    hdrI8Len = offsetI8;

    // Calculate payload size by subtracting the header length
    // and padding length from the packet size
    pyldI8Len = PktI8 - hdrI8Len;
    if ((BitFlds & RTP_PADDING_BIT) != 0) {
       offsetI8 = PktI8 - 1;
       paddingI8Len = netI8 (rtpBuffer, &offsetI8);
       pyldI8Len -= paddingI8Len;
    }
    if (rtp_sr < 8000) {
        sync = &g722_rtpSync[0];
    } else {
        sync = &g722_rtpSync[1];
    }
    sync->rtp_sr     = rtp_sr;
    sync->Sequence   = Sequence;
    sync->TimeStamp  = TimeStamp;
    sync->BitFlds    = BitFlds;
    sync->hdrI8Len   = hdrI8Len;
    sync->PktI8      = PktI8;
    sync->pyldI8Len  = pyldI8Len;
    sync->sr1_count  = sr1_count;
}


