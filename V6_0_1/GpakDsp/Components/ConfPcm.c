/*
 * Copyright (c) 2004-2007, Adaptive Digital Technologies, Inc.
 *
 * File Name: ConfPcm.c
 *
 * Description:
 *   This file contains functions to support Conference PCM type channels.
 *
 * Version: 1.0
 *
 * Revision History:
 *   4/21/04 - Initial release.
 *   2/2007  - Merged C54 and C64
 *
 */
   
// Application related header files.
#include "GpakDefs.h"
#include "GpakErrs.h"
#include "GpakExts.h"
#include "GpakHpi.h"

extern ADT_Bool CheckAecMemory(ADT_UInt16 FrameSize, ADT_UInt32 SampleRate);


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ ProcCfgConfPcmMsg - Process a Configure Conference PCM Channel message.
//
// FUNCTION
//   This function performs Conference PCM channel type dependent processing for
//   a Configure Channel message.
//
// Inputs
//   pCmd - Command message buffer
//
//  Word:Bits
//     2:ff00    -  Conference ID
//     2:0020    -  AGC
//     2:0010    -  PCM Echo canceller
//     2:0008    -  AEC Echo canceller
//     3:0700    -  In Port ID
//     3:f8ff    -  In Slot ID
//     4:0700    -  Out Port ID
//     4:f8ff    -  Out Slot ID
//     5:FF00    -  McASP Input Pin (obsolete)
//     5:00FF    -  McASP Input Pin (obsolete)
//     6:FFE0    -  Tone Types
//
// Outputs
//   chan - Channel data structure pointer
//
// RETURNS
//  Return status for reply message.
//}
#pragma CODE_SECTION (ProcCfgConfPcmMsg, "SLOW_PROG_SECT")
GPAK_ChannelConfigStat_t ProcCfgConfPcmMsg (ADT_UInt16 *pCmd,  chanInfo_t *chan) {
   ConferenceInfo_t *cnfrInfo;
   ADT_UInt16 ConferenceId;   // Conference Id
   int toneDetectCnt;
   int mfTones, cedTones, cngTones, arbTones;
   GPAK_ChannelConfigStat_t toneStatus;

   pcm2pkt_t *PcmPkt = &(chan->pcmToPkt);
   pkt2pcm_t *PktPcm = &(chan->pktToPcm);

   channelAllocsNull (chan, PcmPkt, PktPcm);

   ConferenceId = Byte1 (pCmd[2]);
   if (sysConfig.maxNumConferences <= ConferenceId)
      return Cc_InvalidConference;
 
   cnfrInfo = GetGpakCnfr (chan->CoreID, ConferenceId);
   if (cnfrInfo->FrameSize == 0)
      return Cc_ConfNotConfigured;
      
   if (sysConfig.maxNumConfChans <= cnfrInfo->NumMembers)
      return Cc_ConferenceFull;

   chan->PairedChannelId = ConferenceId;
   PcmPkt->SamplesPerFrame = cnfrInfo->FrameSize;
   PktPcm->SamplesPerFrame = cnfrInfo->FrameSize;

   PcmPkt->Coding = NullCodec;
   PcmPkt->InSerialPortId  = (GpakSerialPort_t) UnpackPort (pCmd[3]);
   PcmPkt->InSlotId =                           UnpackSlot (pCmd[3]);
   PcmPkt->OutSerialPortId = SerialPortNull;

   PktPcm->Coding = NullCodec;
   PktPcm->InSerialPortId = SerialPortNull;
   PktPcm->OutSerialPortId = (GpakSerialPort_t) UnpackPort (pCmd[4]);
   PktPcm->OutSlotId =                          UnpackSlot (pCmd[4]);

   // Configure AGC

   //----------------------------------------------------------
   // API Version 402 and better.
   //    Caller ID and Gain settings
   if (ApiBlock.ApiVersionId <= 0x0401u) {
      if ((pCmd[2] & 0x0020) == 0) {                // Input and Output AGC
         PcmPkt->AGC = Disabled;
         PktPcm->AGC = Disabled;
      } else if (numAGCChansAvail < 2)
         return Cc_InsuffAgcResourcesAvail;
      else {
         PcmPkt->AGC = Enabled;
         PktPcm->AGC = Enabled;
      }
    } else {
      // Version 402+
      if ((pCmd[2] & 0x0020) == 0)                  // Input AGC
         PcmPkt->AGC = Disabled;
      else if (numAGCChansAvail < 1)
         return Cc_InsuffAgcResourcesAvail;
      else
         PcmPkt->AGC = Enabled;

      if ((pCmd[8] & 0x00001) == 0)                 // Output AGC
         PktPcm->AGC = Disabled;
      else if (numAGCChansAvail < 1)
         return Cc_InsuffAgcResourcesAvail;
      else
         PktPcm->AGC = Enabled;
   }
   //-----------------------------------------------------
   //
   // Select acoustic or PCM echo canceller giving priority to acoustic echo canceller
   //
   if ((pCmd[2] & 0x0008) != 0) {                // Acoustic Echo cancellation
      if (NumAECEcansUsed < sysConfig.AECInstances) {

         // Acoustic echo canceller selected. 
         pCmd[2] &= (~0x0010);                 // Turn off request for PCM canceller.
         if (!CheckAecMemory(PcmPkt->SamplesPerFrame, chan->ProcessRate))
            return Cc_InsuffBuffsAvail;
         PcmPkt->AECEchoCancel = Enabled;

      } else if ((pCmd[2] & 0x0010) == 0)
         return Cc_AECEchoCancelNotCfg;  // No acoustic echo canceller and no PCM request
   } else
      PcmPkt->AECEchoCancel = Disabled;

   // Determine Echo Canceller needs and availability.
   if ((pCmd[2] & 0x0010) != 0) {
      if (sysConfig.numPcmEcans <= NumPcmEcansUsed)
         return Cc_PcmEchoCancelNotCfg;
         
      PcmPkt->EchoCancel = Enabled;
   } else
      PcmPkt->EchoCancel = Disabled;

   chan->VoiceChannel = Enabled;

   // Verify allocation of tone detector and generation instances.
   PktPcm->toneTypes = (pCmd[6] & Tone_Generate);
   if ((PktPcm->toneTypes & Tone_Generate) && (numToneGenChansAvail == 0))
      return Cc_InsuffTGResourcesAvail;


   // Configure tone detection.  Conference sets PktPcm tones.  Channel sets PcmPkt tones.
   PcmPkt->toneTypes = (pCmd[6] & ~Tone_Generate);
   PktPcm->toneTypes |= cnfrInfo->InputToneTypes;
   
   mfTones = cedTones = cngTones = arbTones = 0;
   toneStatus = ValidToneTypes (PcmPkt->toneTypes, &mfTones, &cedTones, &cngTones, &arbTones);
   if (toneStatus != Cc_Success) return toneStatus;

   toneStatus = ValidToneTypes (PktPcm->toneTypes, &mfTones, &cedTones, &cngTones, &arbTones);
   if (toneStatus != Cc_Success) return toneStatus;

   toneDetectCnt = 0;
   if (PcmPkt->toneTypes & ToneDetect) toneDetectCnt++;
   if (PktPcm->toneTypes & ToneDetect) toneDetectCnt++;
   if (ToneDetectorsAvail < toneDetectCnt)
        return Cc_InsuffTDResourcesAvail;

   if (ApiBlock.ApiVersionId <= 0x0401u) return Cc_Success;

   //----------------------------------------------------------
   // API Version 402 and better.
   //   Gain settings
   PcmPkt->Gain.ToneGenGainG1  = pCmd[7];
   PcmPkt->Gain.OutputGainG2   = pCmd[9];
   PcmPkt->Gain.InputGainG3    = pCmd[10];
   ClampScaleGain (&PcmPkt->Gain);

   PktPcm->Gain.ToneGenGainG1  = PcmPkt->Gain.ToneGenGainG1;
   PktPcm->Gain.OutputGainG2   = PcmPkt->Gain.OutputGainG2; 
   PktPcm->Gain.InputGainG3    = PcmPkt->Gain.InputGainG3;     
//ss
#if 0
   PktPcm->SideTone = Disabled;
   PcmPkt->SideTone = Disabled;
#endif
   return Cc_Success;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ ProcConfPcmStatMsg - Process a Conference PCM Channel Status message.
//
// FUNCTION
//   This function prepares the Channel Status reply message for a Conference PCM
//   type channel.
//
// RETURNS
//   Length I16 of reply message.
//}
#pragma CODE_SECTION (ProcConfPcmStatMsg, "SLOW_PROG_SECT")
ADT_UInt16 ProcConfPcmStatMsg (ADT_UInt16 *pReply, chanInfo_t *chan) {
   pkt2pcm_t *PktPcm;
   pcm2pkt_t *PcmPkt;

   PcmPkt = &(chan->pcmToPkt);
   PktPcm = &(chan->pktToPcm);

   pReply[7]  = (ADT_UInt16) 
            ((chan->PairedChannelId << 8) |
             ((PcmPkt->AGC           << 5) & 0x0020) |    
             ((PcmPkt->EchoCancel    << 4) & 0x0010) |
             ((PcmPkt->AECEchoCancel << 3) & 0x0008) |
             ((chan->VoiceChannel   << 2) & 0x004));
             
   pReply[8]  = PackPortandSlot (PcmPkt->InSerialPortId,  PcmPkt->InSlotId);
   pReply[9]  = PackPortandSlot (PktPcm->OutSerialPortId, PktPcm->OutSlotId);
   pReply[10] = 0;  // Obsolete Pin IDs
   pReply[11] = (ADT_UInt16) ((PktPcm->toneTypes | PcmPkt->toneTypes) & 0xFFF);

   if (ApiBlock.ApiVersionId <= 0x0401u) return 12;

   //----------------------------------------------------------
   // API Version 402 and better.
   //   Gain settings
   pReply[12]   = (ADT_UInt16) (PktPcm->AGC & 0x1);
   if (PktPcm->Gain.ToneGenGainG1 != (ADT_Int16)ADT_GAIN_MUTE)
        pReply[13] = (ADT_Int16) PktPcm->Gain.ToneGenGainG1/10;
   else
        pReply[13] = (ADT_Int16) PktPcm->Gain.ToneGenGainG1;

   if (PktPcm->Gain.OutputGainG2 != (ADT_Int16)ADT_GAIN_MUTE)
        pReply[14] = (ADT_Int16) PktPcm->Gain.OutputGainG2/10; 
   else 
        pReply[14] = (ADT_Int16) PktPcm->Gain.OutputGainG2; 

   if (PcmPkt->Gain.InputGainG3 != (ADT_Int16)ADT_GAIN_MUTE)
        pReply[15] = (ADT_Int16) PcmPkt->Gain.InputGainG3/10;  
   else
        pReply[15] = (ADT_Int16) PcmPkt->Gain.InputGainG3;  
   return 16;

}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ SetupConfPcmChan - Setup a Conference PCM channel.
//
// FUNCTION
//   This function performs channel setup initialization for a Conference PCM
//   type channel.
//
// RETURNS
//   nothing
//}
#pragma CODE_SECTION (SetupConfPcmChan, "SLOW_PROG_SECT")
void SetupConfPcmChan (chanInfo_t *chan) {
   conf_Member_t    *pConfMember;   // pointer to Conference Member data
   pcm2pkt_t *PcmPkt;      // pointer to channel's PCM to Packet data
   pkt2pcm_t *PktPcm;      // pointer to channel's Packet to PCM data

   // Get pointers to the channel's PCM to Packet and Packet to PCM data.
   PcmPkt = &(chan->pcmToPkt);
   PktPcm = &(chan->pktToPcm);

   if (PcmPkt->EchoCancel)
      chan->ProcessRate = 8000;

   // Allocate Pcm, Pkt and BulkDelay Buffers
   AllocBuffs (chan);

   // Initialize the circular PCM buffers.
   PcmPkt->Coding = NullCodec;
   PcmPkt->activeInBuffer = &PcmPkt->inbuffer;
   memset (PcmPkt->inbuffer.pBufrBase, 0, PcmPkt->inbuffer.BufrSize * sizeof (ADT_PCM16));

#if VECTOR_TEST_BUILD
   if (CurrentTestMode == TestSyncPcmInput) {
      chan->Flags |= CF_VECTOR_TEST_MODE;
      PktPcm->OutSerialPortId = SerialPortNull;
      PktPcm->OutSlotId = CurrentTestParm;
      PcmPkt->EchoCancel = Disabled;
   } else
#endif
   {
      memset (PktPcm->outbuffer.pBufrBase, 0, PktPcm->outbuffer.BufrSize * sizeof (ADT_PCM16));
   }

   // Initialize enabled tone detectors
   if ((PcmPkt->toneTypes & ToneDetectAll) != Null_tone) {
      InitToneDetection (PcmPkt->toneTypes, &PcmPkt->TDInstances, PcmPkt->toneRelayPtr, PcmPkt->SamplesPerFrame);
   }

   if ((PktPcm->toneTypes & ToneDetectAll) != Null_tone) {
      InitToneDetection (PktPcm->toneTypes, &PktPcm->TDInstances, 0, PktPcm->SamplesPerFrame);
   }

   // Allocate enabled AGC instances
   AllocAGC (chan, ADevice, PcmPkt->AGC);
   AllocAGC (chan, BDevice, PktPcm->AGC);

   // Allocate enabled tone gen instances
   AllocToneGen (&(PcmPkt->TGInfo), Disabled);
   AllocToneGen (&(PktPcm->TGInfo), (PktPcm->toneTypes & Tone_Generate));

   // Initialize enabled echo cancellers
   PcmPkt->ecBulkDelay.SlipSamps = 0;
   PcmPkt->ecFarPtr.SlipSamps = 0;
   if (PcmPkt->AECEchoCancel == Enabled) {
      PcmPkt->pG168Chan =
         AllocEchoCanceller (ACOUSTIC, PcmPkt->SamplesPerFrame, &PcmPkt->EcIndex, chan->ProcessRate);
      if (PcmPkt->pG168Chan == NULL)
      {
	     PcmPkt->AECEchoCancel = Disabled;
      }
      else
      {
         PcmPkt->ecFarPtr.pBufrBase = pAECFarPool[PcmPkt->EcIndex & 0xFF];
         PcmPkt->ecFarPtr.BufrSize  = PcmPkt->SamplesPerFrame;
         memset (PcmPkt->ecFarPtr.pBufrBase, 0,  PcmPkt->SamplesPerFrame * sizeof (ADT_PCM16));
      }
   } else if (PcmPkt->EchoCancel == Enabled) {
      PcmPkt->ecFarPtr.pBufrBase = PktPcm->outbuffer.pBufrBase;
      PcmPkt->ecFarPtr.BufrSize  = PktPcm->outbuffer.BufrSize;
      PcmPkt->EcIndex = chan->CoreID;
      PcmPkt->pG168Chan =
         AllocEchoCanceller (G168_PCM, PcmPkt->SamplesPerFrame, &PcmPkt->EcIndex, chan->ProcessRate);
   }

   // Allocate and initialize sampling rate converter instances
   AllocEncoderSampleRateConv (chan);
   AllocDecoderSampleRateConv (chan);

   // Setup the channel's Conference Member structure.
   pConfMember  = ConfMemberData[chan->ChannelId];
   CLEAR_INST_CACHE (pConfMember, sizeof (conf_Member_t));
   FLUSH_wait ();

   pConfMember->pVAD       = ConfVadData[chan->ChannelId];
   pConfMember->pInPCM     = (ADT_Int16 *) PktPcm->inbuffer->pBufrBase;
   pConfMember->pOutPCM    = (ADT_Int16 *) PcmPkt->outbuffer->pBufrBase;
   pConfMember->malloc_ADT = NULL;
   pConfMember->pLink      = NULL;
   if (chan->ProcessRate == 8000)
      pConfMember->MemberType = MEMBER_TYPE_8K_B;
   else
      pConfMember->MemberType = MEMBER_TYPE_16K_B;

   pConfMember->Priority = 0;
   FLUSH_INST_CACHE (pConfMember, sizeof (conf_Member_t));
   return;
}
