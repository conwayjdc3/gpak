//==========================================================================
//          File:  64xRtpHostDelivery.c
//
//   Description:  Initialization and support routines for delivery of RTP packets to the host
//
//
//
//{==========================================================================
#include <std.h>
//#include <hwi.h>
//#include <swi.h>
#include <adt_typedef.h>
#include "GpakDefs.h"
#include "GpakExts.h"
#include "GpakHpi.h"

//} - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//   Frame work interfaces for packet delivery to host
//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

extern GpakTestMode RTPLoopBack;

extern void ProcessHostIsr ();
extern void RTPMemInit ();
extern void Enable_Interrupt_From_Host (Fxn HostIsr);

extern void (*RTPPayloadFree) (void *hndl, void*data, ADT_UInt16 dataI8);
static void PacketHndlFree (void* hndl, void* data, ADT_UInt16 dataI8);

extern ADT_UInt16 RTPScratchI8;    // Size of RTPScratch buffer
static void* RTPScratch;  // Host SWI scratch buffer
#pragma DATA_SECTION (RTPScratch,   "PER_CORE_DATA:Scratch")

#pragma CODE_SECTION (RTP_Init, "SLOW_PROG_SECT")
void RTP_Init () {

   int i, CoreIdx;

   ApiBlock.PktBufrMem      = RTPCircBuffers;

   for (i=0; i<sysConfig.maxNumChannels; i++)
      memset (RtpChanData[i], 0, sizeof (RtpChanData_t));

   RTPMemInit ();
   RTPPayloadFree = PacketHndlFree;

   RTPScratch = RTPAlloc (RTPScratchI8);

   CoreIdx = DSPCore * 2;
 
   RTPCircBuffers[CoreIdx].TakeIndex = 0;
   RTPCircBuffers[CoreIdx].PutIndex  = 0;
   RTPCircBuffers[CoreIdx].pBufrBase = ToDSPRTPBuffer;
   RTPCircBuffers[CoreIdx].BufrSize  = sysConfig.RTPBuffersI16;
   RTPCircBuffers[CoreIdx].SlipSamps = 0;

   CoreIdx++;
   RTPCircBuffers[CoreIdx].TakeIndex = 0;
   RTPCircBuffers[CoreIdx].PutIndex  = 0;
   RTPCircBuffers[CoreIdx].pBufrBase = ToNetRTPBuffer;
   RTPCircBuffers[CoreIdx].BufrSize  = sysConfig.RTPBuffersI16;
   RTPCircBuffers[CoreIdx].SlipSamps = 0;

   Enable_Interrupt_From_Host ((Fxn)ProcessHostIsr);

   return;

}


#ifdef _DEBUG
   #define PKTTONET      1
void logRtp (int chan, int operation, int pyldI8, ADT_UInt32 *rtpHdr);
#else
   #define logRtp(chan, operation, pyldI8, rtpHdr) 
#endif

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// netTransmit
//
// FUNCTION
//   RTP callback to send RTP packets to host. Will be called from framing tasks via RTPSend API.
//
// Inputs
//   hndl   -  Handle established at RTPOpen (pointer to channel structure.
//   data   -  RTP header and payload
//   dataI8 -  Size of data
//
//}
#pragma CODE_SECTION (netTransmit, "PKT_PROG_SECT")
short int netTransmit (NETHANDLE* hndl, ADT_UInt32* Data, int *dataI8) {
   RTPToHostHdr_t Hdr;
   ADT_UInt8 *data;
   int sent;
   void *RTPCirc;
   int mask;

   data = (ADT_UInt8 *) Data;

   Hdr.PktI8  = *dataI8;
   Hdr.ChanId = (ADT_UInt16) ((ADT_UInt32) hndl);

   // Run SRTP if enabled and SRTP channel already configed
   if ((sysConfig.SRTP_Enable) && (SrtpTxKeyData[Hdr.ChanId]->Configured)) {
      Hdr.PktI8 = SrtpEncryptU8 (SrtpTxChanData[Hdr.ChanId]->Instance, data, *dataI8, data);
      *dataI8 = Hdr.PktI8;
      if (*dataI8 == 0)
         return 0;
   }

   data = customPktOut (&Hdr, (ADT_UInt16 *) data);

   RTPCirc = &RTPCircBuffers[(DSPCore * 2) + 1];
   logRtp (Hdr.ChanId, PKTTONET, *dataI8, (ADT_UInt32 *) data);

   if (RTPLoopBack != DisableTest) {
      loopBackPkts (&Hdr);
      RTPCirc = &RTPCircBuffers[DSPCore * 2];   // Place packet in DSP's receive buffer
   }

   // Avoid simultaneous access to RTP Buffer from framing tasks
   logTime (0x04090000 | Hdr.ChanId);
   mask = HWI_disable ();
   if (SysAlgs.apiCacheEnable) {
      sent = PayloadToCircCache (RTPCirc, &Hdr, RTP_TO_HOST_I16, data, *dataI8);
   } else {
      sent = PayloadToCirc      (RTPCirc, &Hdr, RTP_TO_HOST_I16, data, *dataI8);
   }
   HWI_restore (mask);

   return sent;

}

int PurgeTransferBuffer (CircBufInfo_t* transferBuff) {
   int cntI16;

   cntI16 =  transferBuff->TakeIndex - transferBuff->PutIndex;
   if (cntI16 < 0) cntI16 += transferBuff->BufrSize;

   transferBuff->TakeIndex = transferBuff->PutIndex;
   return (cntI16 / NCHdrI16);
}

#pragma CODE_SECTION (GetPktFromTransferBuffers, "MEDIUM_PROG_SECT")
void* GetPktFromTransferBuffers (CircBufInfo_t *circ, NCHdr_t *NcHdr) {
   int takeIdx;
   void* pkt;

   takeIdx = GetPktFromCircBuffer (circ, &NcHdr, RTP_TO_HOST_I16,
                                   RTPScratch, RTPScratchI8);
   if (takeIdx < 0) return NULL;

   RemovePktFromCircBuffer (circ, &NcHdr, RTPScratch, takeIdx);

   pkt = RTPAlloc (NcHdr->PktI8);
   NcHdr->RTPBuff = pkt;
   NcHdr->PktHndl = pkt;
   if (pkt != NULL) mmCpy (pkt, RTPScratch, NcHdr->PktI8);
   return pkt;
}

//{  processIPStackFields  Only supported by IP stack builds.
//
//   Parse IP stack addresses and parameters from RTP configuration message.
//   Add IP mapping for this channel to system.
//}
#pragma CODE_SECTION (processIPStackFields, "SLOW_PROG_SECT")
GPAK_RTPConfigStat_t processIPStackFields (ADT_UInt16 *pCmd, int chanId, void *RTPSession) {
   AppErr ("IP stack not supported", TRUE);
   return RTPSuccess;
}


//{  processIPv6StackFields  Only supported by IP stack builds.
//
//   Parse IP stack addresses and parameters from RTP configuration message.
//   Add IP mapping for this channel to system.
//}
#pragma CODE_SECTION (processIPv6StackFields, "SLOW_PROG_SECT")
GPAK_RTPConfigStat_t processIPv6StackFields (ADT_UInt16 *pCmd, int chanId, void *RTPSession) {
   AppErr ("IP stack not supported", TRUE);
   return RTPSuccess;
}
//{  Set VLAN tag message.  Only supported by IP stack builds.
//
//  Word:Bits
//     0:ff00    -  SET_VLAN_TAG
//     0:00ff    -  VLAN Index (0-9)
//     1:ffff    -  VLAN ID (0-fff)
//     2:ff00    -  priority

//   Set VLAN tag response
//
//  Word:Bits
//     0:ff00    -  SET_VLAN_TAG_REPLY
//     0:00ff    -  Command tag (VLAN index filled in by msg task)
//     1:ff00    -  VLAN Index (0-9)
//}    1:00ff    -  DSP reply code
#pragma CODE_SECTION (ProcessSetVLANTagMsg, "SLOW_PROG_SECT")
int ProcessSetVLANTagMsg (ADT_UInt16 *cmd, ADT_UInt16 *reply) {
   ADT_UInt16 vlanIdx;
   vlanIdx  = Byte0 (cmd[0]);

   // Format reply
   reply[0] |= MSG_SET_VLAN_TAG_REPLY << 8;
   reply[1]  = (vlanIdx << 8) | vlanUnAvailable;
   return 4;
} 

//{  Get VLAN tag message.
//
//  Word:Bits
//     0:ff00    -  GET_VLAN_TAG
//     0:00ff    -  VLAN Index (0-9)
//     1:ffff    -  VLAN ID (0-fff)
//     2:ff00    -  priority

//   Set VLAN tag response
//
//  Word:Bits
//     0:ff00    -  SET_VLAN_TAG_REPLY
//     0:00ff    -  Command tag (VLAN Index filled in by msg task)
//     1:ff00    -  VLAN Index (0-9)
//     1:00ff    -  DSP reply code
//     2:e000    -  priority
//}    2:0fff    -  vlan tag
#pragma CODE_SECTION (ProcessGetVLANTagMsg, "SLOW_PROG_SECT")
int ProcessGetVLANTagMsg (ADT_UInt16 *cmd, ADT_UInt16 *reply) {

   ADT_UInt16 vlanIdx;

   vlanIdx  = Byte0 (cmd[0]);

   // Format reply
   reply[0] |= MSG_GET_VLAN_TAG_REPLY << 8;
   reply[1]  = (vlanIdx << 8) | vlanUnAvailable;
   return 4;
}


//-------------------------------------------------------------------------
//  If IP Stack is enabled with RTP packets delivered to host. 
#pragma CODE_SECTION (rtpGetPacket, "MEDIUM_PROG_SECT")
int rtpGetPacket (void *IPBuff, int IPBuffI8, ADT_UInt16 *ChanID, int coreID) {
   return 0;
}
#pragma CODE_SECTION (storeInboundRTPPacket, "MEDIUM_PROG_SECT")
int storeInboundRTPPacket (ADT_UInt16 chanID, void *RTPBuff, int pktI8, void *PktHndl) {
   return 1;
}

#pragma CODE_SECTION (freePktHandles, "MEDIUM_PROG_SECT")
int freePktHandles (int coreID) {   return 1; }

#pragma CODE_SECTION (PacketHndlFree, "PKT_PROG_SECT")
void PacketHndlFree (void* hndl, void* data, ADT_UInt16 dataI8) {
   RTPFree (data);
   return;
}
