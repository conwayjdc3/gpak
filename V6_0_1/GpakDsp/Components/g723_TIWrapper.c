#include <std.h>
// TI's implementation of G723

/* XDAIS header files */
#include <ti/xdais/xdas.h>

/* XDM header files */
#include <ti/xdais/dm/xdm.h>
#include <ti/xdais/dm/ispeech1.h>
#include <ti/xdais/dm/isphenc1.h>
#include <ti/xdais/dm/isphdec1.h>

/* G723A Interface header files */
#include <g7231aenc_tii.h>
#include <g7231adec_tii.h>
#include <ig7231a.h>
#include <string.h>

#ifndef CONTROLDEF_IN_H
#define CONTROLDEF_IN_H
typedef struct
{
	XDAS_Int16 WrkMode ;
	XDAS_Int16 WrkRate ;
	XDAS_Int8   UseHp ;
	XDAS_Int8   UsePf ;
	XDAS_Int8   UseVx ;

} CONTROLDEF ;	// sizeof(DECCNGDEF) = 10

/* Coder rate */
//enum Crate { Rate63, Rate53 } ;
#define Rate63			0
#define Rate53			1
#endif

#define G723ENC_INSTANCE_SIZEI8 1568
#define G723ENC_SCRATCH_SIZEI8 3314
#define G723DEC_INSTANCE_SIZEI8 440
#define G723DEC_SCRATCH_SIZEI8 1672

// --------------------------offset          length  post-pad
#define ENC_MEM0_BYTE_OFFSET  0           // 176      0
#define ENC_MEM1_BYTE_OFFSET  176          // 936      0
#define ENC_MEM2_BYTE_OFFSET  1112          // 456     0 // total 1568

#define ENC_SCR_MEM0_BYTE_OFFSET  0         // 960      0  //scr
#define ENC_SCR_MEM1_BYTE_OFFSET  960         // 1488     0  // scr
#define ENC_SCR_MEM2_BYTE_OFFSET  2448         //  866     0 //scr total 3314

// --------------------------offset         length  post-pad
#define DEC_MEM0_BYTE_OFFSET  0              // 440      0 // total 440
#define DEC_SCR_MEM0_BYTE_OFFSET  0          // 1024     0
#define DEC_SCR_MEM1_BYTE_OFFSET  1024       // 648      0  //scr total 1672

int G723_ADT_initEnc (void *chan, void *scratch) {
   IALG_MemRec         memTab[6];
   ISPHENC1_Params     params;

   IALG_Fxns           *fxns = (IALG_Fxns *) &G7231AENC_TII_IG7231AENC;
   IALG_Fxns 			*fxnsPtr;
   Int                 n;
   XDAS_Int8           *pMem = (XDAS_Int8 *)chan, *pScrMem = (XDAS_Int8 *)scratch;
   IALG_Handle          alg;

    params.size                 = sizeof (ISPHENC1_Params);
    params.tablesPtr            = NULL;
    memset(memTab, 0, sizeof(memTab));
    n = fxns->algNumAlloc != NULL ? fxns->algNumAlloc() : IALG_DEFMEMRECS;

    if (n == 6) {
       n = fxns->algAlloc((IALG_Params *)&params, &fxnsPtr, memTab);
       if (n <= 0) return -1;

        memTab[0].base = &pMem[ENC_MEM0_BYTE_OFFSET];
        memTab[1].base = &pMem[ENC_MEM1_BYTE_OFFSET];
        memTab[2].base = &pMem[ENC_MEM2_BYTE_OFFSET];
        memTab[3].base = &pScrMem[ENC_SCR_MEM0_BYTE_OFFSET];
        memTab[4].base = &pScrMem[ENC_SCR_MEM1_BYTE_OFFSET];
        memTab[5].base = &pScrMem[ENC_SCR_MEM2_BYTE_OFFSET];

        alg = (IALG_Handle)memTab[0].base;
        alg->fxns = fxns;
        if (fxns->algInit(alg, memTab, NULL, (IALG_Params *)&params) == IALG_EOK) {
            return 0;
        }
    }
    return -1;
}

int G723_ADT_encode (void *chan, CONTROLDEF *ConTrol, void *pcm, void *data) {
  /* Base Class Structures                                                    */
  ISPHENC1_DynamicParams  dynparams;
  ISPHENC1_Status         status;
  ISPHENC1_InArgs         inargs;
  ISPHENC1_OutArgs        outargs;
  /* Algorithm specific handle                                                */
  ISPHENC1_Handle         handle;
  /* Input/Output Buffer Descriptors                                          */
  XDM1_SingleBufDesc      inBufs;
  XDM1_SingleBufDesc      outBufs;
  XDAS_Int32              retVal;

   //logTime (0x400A1600ul);

   handle =  (ISPHENC1_Handle)chan;

   dynparams.size = sizeof(ISPHENC1_DynamicParams);
   if (ConTrol->WrkRate == Rate53) { /* lOWER RATE 5.3 */
      dynparams.bitRate              = ISPEECH1_G723_BITRATE_5P3;
   } else if (ConTrol->WrkRate == Rate63) { /* HIGHER RATE 6.3 */
      dynparams.bitRate              = ISPEECH1_G723_BITRATE_6P3;
   }
   // Enable/disable DTX
   if (ConTrol->UseVx) //(Vad)
      dynparams.vadFlag = ISPEECH1_VADFLAG_ON;
   else
      dynparams.vadFlag = ISPEECH1_VADFLAG_OFF;
   if (ConTrol->UseHp == 1) {
      dynparams.noiseSuppressionMode = ISPEECH1_G723_NOISEPREPROC_ON;
   } else {
      dynparams.noiseSuppressionMode = ISPEECH1_G723_NOISEPREPROC_OFF;
   }  
   retVal = handle->fxns->control (handle, XDM_SETPARAMS, &dynparams, &status);
   if (retVal == ISPHENC1_EFAIL)
      return -1;

   // perform Encode function
   inBufs.buf      = (XDAS_Int8   *)pcm;
   inBufs.bufSize  = 240*2;
   outBufs.buf     = (XDAS_Int8   *)data;
   //outBufs.bufSize  = 24;
   /* Set the Input Arguments structure */
   inargs.size                 = sizeof (ISPHENC1_InArgs);
   /* Set the Output Arguments structure */
   outargs.size                = sizeof (ISPHENC1_OutArgs);
   retVal = handle->fxns->process (handle, &inBufs, &outBufs, &inargs, &outargs);
   if(retVal == XDM_EFAIL)
      return -1;

	//logTime (0x400A1700ul);
   return 0;
}

int G723_ADT_initDec (void *chan, void *scratch) {
   IALG_MemRec         memTab[3];
   ISPHDEC1_Params     params;
   IALG_Fxns    *fxns = (IALG_Fxns *) &G7231ADEC_TII_IG7231ADEC;
   IALG_Fxns    *fxnsPtr;
   Int                 n;
   XDAS_Int8           *pMem = (XDAS_Int8 *)chan, *pScrMem = (XDAS_Int8 *)scratch;
   IALG_Handle          alg;

    params.size                 = sizeof (ISPHDEC1_Params);
    params.tablesPtr            = NULL;
    memset(memTab, 0, sizeof(memTab));
    n = fxns->algNumAlloc != NULL ? fxns->algNumAlloc() : IALG_DEFMEMRECS;

    if (n == 3) {
        n = fxns->algAlloc((IALG_Params *)&params, &fxnsPtr, memTab);
        if (n <= 0)
           return -1;

        memTab[0].base = &pMem[DEC_MEM0_BYTE_OFFSET];
        memTab[1].base = &pScrMem[DEC_SCR_MEM0_BYTE_OFFSET];
        memTab[2].base = &pScrMem[DEC_SCR_MEM1_BYTE_OFFSET];

        alg = (IALG_Handle)memTab[0].base;
        alg->fxns = fxns;
        if (fxns->algInit(alg, memTab, NULL, (IALG_Params *)&params) == IALG_EOK) {
            return 0;
        }
    }
    return -1;
}

int G723_ADT_decode (void *chan, CONTROLDEF *ConTrol, void *pyld, void *pcm, short FrameErase) {
   /* Base Class Structures                                                    */
   ISPHDEC1_DynamicParams  dParams;
   ISPHDEC1_Status         status;
   ISPHDEC1_InArgs         inargs;
   ISPHDEC1_OutArgs        outargs;

   /* Algorithm specific handle                                                */
   ISPHDEC1_Handle         handle;
   /* Input/Output Buffer Descriptors                                          */
   XDM1_SingleBufDesc      inBufs;
   XDM1_SingleBufDesc      outBufs;

   XDAS_Int32              retVal;
   XDAS_Int32 BitCount;
   
   handle =  (ISPHDEC1_Handle)chan;

   dParams.size                 = sizeof (ISPHDEC1_DynamicParams);
   if (ConTrol->UsePf)
      dParams.postFilter  = 1;
   else
      dParams.postFilter  = 0;

   inargs.size     = sizeof (ISPHDEC1_InArgs);
   inargs.frameType = ISPHDEC1_FTYPE_SPEECHGOOD;
   outargs.size    = sizeof (ISPHDEC1_OutArgs);

   inBufs.buf      = (XDAS_Int8  *)pyld;

   outBufs.buf     = (XDAS_Int8  *)pcm;
   outBufs.bufSize    = 240*2;

   BitCount = inBufs.buf[0] & 0x0003;
   if (BitCount == 0) { /* Bitrate = 6.3 kbps */
      inargs.frameType = ISPHDEC1_FTYPE_SPEECHGOOD;
      inBufs.bufSize = 24;
   } else if (BitCount == 1) {   /* Bitrate = 5.3 kbps */
      inargs.frameType = ISPHDEC1_FTYPE_SPEECHGOOD;
      inBufs.bufSize = 20;
   } else if (BitCount == 2) {
        inargs.frameType = ISPHDEC1_FTYPE_SIDUPDATE;
        inBufs.bufSize = 4;
   } else {
      inargs.frameType = ISPHDEC1_FTYPE_NODATA;
      inBufs.bufSize = 0;
   }

   if (FrameErase) {
      inargs.frameType = ISPHDEC1_FTYPE_SPEECHLOST;
      inBufs.bufSize = 0;
   }

   /* Set the control parameters at frame boundary */
   retVal = handle->fxns->control (handle, XDM_SETPARAMS, &dParams, &status);
   if (retVal != XDM_EOK)
      return -1;

   retVal = handle->fxns->process (handle, &inBufs, &outBufs, &inargs, &outargs);
   if (retVal == XDM_EFAIL)
      return -1;

   return 0;
}

//#pragma CODE_SECTION(G723_ADT_getSize4Decoder, ".G723_DEC")
XDAS_Int16 G723_ADT_getSize4Decoder (void *LineIn) {
   XDAS_Int16 Info, Size;
   XDAS_Int16 *Line = (XDAS_Int16 *)LineIn;

   Info = Line[0] & (XDAS_Int16)0x0003 ;

   /* Check frame type and rate informations */
   switch (Info) {
   case 0 : Size  = 23;   break;    // Active frame, high rate 
   case 1 : Size  = 10;   break;    // Active frame, low rate 
   case 2 : Size  = 3;    break;    // Sid Frame 
   default : Size  = 0;   break;    // DTX
   }
   return Size;

}
