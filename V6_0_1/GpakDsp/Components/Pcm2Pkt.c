/*
 * Copyright (c) 2004-2007, Adaptive Digital Technologies, Inc.
 *
 * File Name: Pcm2Pkt.c
 *
 * Description:
 *   This file contains functions to support PCM To Packet type channels.
 *
 * Version: 1.0
 *
 * Revision History:
 *   4/21/04 - Initial release.
 *   2/2007  - Merged C54 and C64
 */

// Application related header files.
#include "GpakDefs.h"
#include "GpakErrs.h"
#include "GpakExts.h"
#include "GpakHpi.h"
#include "sysconfig.h"
#include "GpakDma.h"

extern ADT_Bool CheckAecMemory(ADT_UInt16 FrameSize, ADT_UInt32 SampleRate);
extern void processFaxRelay (chanInfo_t *pChan, void *pcmInBuff, void *pcmOut, int FrameSize);
extern void waveFilePlayback (wavefileInfo_t *pInfo, ADT_Int16 *buff,  int sampsPerFrame); 


#ifndef _DEBUG
#define captureFaxOutput(a,b,c)
#endif

#ifdef VECTOR_TEST_BUILD
static ADT_Bool findTestVector (chanInfo_t *ChnInf, ADT_PCM16 *pcmInBuf,  
                                pkt2pcm_t *PktToPcm, ADT_UInt16 SampsPerFrame) {

   int i;                     // loop index / counter

   // Preprocess if Vector test mode is active.
   if ((ChnInf->Flags & CF_VECTOR_TEST_MODE) == 0)
      return TRUE;
         
   if ((ChnInf->Flags & CF_VECTOR_SYNC_OK)) 
      // Start of test vector found in previous frame. Copy entire frame to outbuffer.
      copyLinearToCirc ((ADT_UInt16 *) pcmInBuf, &PktToPcm->outbuffer, SampsPerFrame);
   else   {
      // Check for start of test vector.  If found copy partial frame to outbuffer
      for (i = 0; i < SampsPerFrame; i++)  {
         if (pcmInBuf[i] == PktToPcm->OutSlotId)
            continue;
                 
         ChnInf->Flags |= CF_VECTOR_SYNC_OK;
         copyLinearToCirc ((ADT_UInt16 *) &pcmInBuf[i], &PktToPcm->outbuffer, SampsPerFrame - i);
         if (i != 0) return FASLE;
         break;                  
      }
   }
   
   // Full frame of data available in outbuffer. 
   copyCircToLinear (&PktToPcm->outbuffer, (ADT_UInt16 *) pcmInBuf, SampsPerFrame);
   return TRUE;
}
#else
#define findTestVector(ChnInf,pcmInBuf,PktToPcm,SampsPerFrame) TRUE
#endif




// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ ProcCfgPcm2PktMsg - Process a Configure PCM To Packet Channel message.
//
// FUNCTION
//   Performs PCM To Packet channel processing for a Configure Channel message.
//
//  Word:Bits
//     2:0700    -  In Port ID
//     2:f8ff    -  In Slot ID
//     3:0700    -  Out Port ID
//     3:f8ff    -  Out Slot ID
//     4:ff00    -  In Codec
//     4:00ff    -  In Frame Size
//     5:ff00    -  Out Codec
//     5:00ff    -  Out Frame Size
//     6:8000    -  PCM Echo Cancel
//     6:4000    -  Packet Echo Cancel
//     6:2000    -  pcm2Pkt VAD/Comfort noise
//     6:1000    -  pcmToPkt AGC enable
//     6:0800    -  Acoustic Echo Cancel
//     6:0400    -  VoiceChannel enable
//     6:0200    -  DTX enable
//     6:0100    -  packetToPcm AGC enable
//     6:0080    -  pkt2Pcm VAD
//     7:FF00    -  McASP Input Pin  (obsolete)
//     7:00FF    -  McASP Output Pin (obsolete)
//     8:C000    -  fax relay mode bits
//     8:3000    -  fax relay transport bits
//     8:07FF    -  Tone type bits
//     9:FFFF    -  CallerID
//     10:FFFF   -  ToneGenGainG1
//     11:FFFF   -  OutputGainG2
//     13:FFFF   -  InputGainG3
//
// Inputs
//       pCmd  - Command message buffer
//
// Outputs
//     chan - Channel data structure pointer
//
// RETURNS
//  Return status for reply message.
//}
GPAK_ChannelConfigStat_t ProcCfgPcm2PktMsg (ADT_UInt16 *pCmd, chanInfo_t *chan) {


   pcm2pkt_t *PcmPkt;   // pointer to channel's PCM to Packet data
   pkt2pcm_t *PktPcm;   // pointer to channel's Packet to PCM data

   GPAK_ChannelConfigStat_t toneStatus;
   GpakFaxMode_t faxMode;
   GpakToneTypes toneTypes, toneTypesPktPcm;
   int mfTones, cedTones, cngTones, arbTones;

   PcmPkt = &(chan->pcmToPkt);
   PktPcm = &(chan->pktToPcm);

   channelAllocsNull (chan, PcmPkt, PktPcm);


   // Frame rate in samples before api version 603
   // Frame rate in 1/2 ms after api version 603
   if (ApiBlock.ApiVersionId < 0x0603u) {
      PcmPkt->SamplesPerFrame = Byte0 (pCmd[5]);
      PktPcm->SamplesPerFrame = Byte0 (pCmd[4]);
   } else {
      PcmPkt->SamplesPerFrame = halfMSToSamps (Byte0 (pCmd[5]));
      PktPcm->SamplesPerFrame = halfMSToSamps (Byte0 (pCmd[4]));
   }

   //
   //  Get TDM and vocoding parameters
   //
   PcmPkt->InSerialPortId  = (GpakSerialPort_t) UnpackPort (pCmd[2]);
   PcmPkt->InSlotId =                           UnpackSlot (pCmd[2]);
   PcmPkt->Coding =                (GpakCodecs) Byte1 (pCmd[5]);
   if (PcmPkt->InSerialPortId == SerialPortNull) PcmPkt->Coding = NullCodec;
   if (PcmPkt->Coding == NullCodec) PcmPkt->InSerialPortId = SerialPortNull;
   
   if (!ValidFrameSize (PcmPkt->SamplesPerFrame))
      return Cc_InvalidPktOutSizeB;
   
   if (!ValidCoding (PcmPkt->Coding, sampsToHalfMS (PcmPkt->SamplesPerFrame)))
      return Cc_InvalidPktOutCodingB;
      
   PcmPkt->OutSerialPortId = SerialPortNull;
   PcmPkt->OutSlotId_MuxFact = 0;

   PktPcm->OutSerialPortId = (GpakSerialPort_t) UnpackPort (pCmd[3]);
   PktPcm->OutSlotId =                          UnpackSlot (pCmd[3]);
   PktPcm->Coding    =             (GpakCodecs) Byte1 (pCmd[4]);
   if (PktPcm->OutSerialPortId == SerialPortNull) PktPcm->Coding = NullCodec;
   if (PktPcm->Coding == NullCodec) PktPcm->OutSerialPortId = SerialPortNull;

   PktPcm->InSerialPortId = SerialPortNull;
   PktPcm->InSlotId_MuxFact = 0;
   
   if (!ValidFrameSize (PktPcm->SamplesPerFrame))
      return Cc_InvalidPktInSizeB;

   if (!ValidCoding (PktPcm->Coding, sampsToHalfMS (PktPcm->SamplesPerFrame)))
      return Cc_InvalidPktInCodingB;

   //
   // Data channel. All algorithm processing is ignored.
   //
   if ((pCmd[6] & 0x0400) == 0) {
      chan->VoiceChannel = Disabled;
      if ((PktPcm->Coding != L8) && (PktPcm->Coding != L16))
         return Cc_InvalidPktInCodingB;

      if ((PcmPkt->Coding != L8) && (PcmPkt->Coding != L16))
         return Cc_InvalidPktOutCodingB;

      return Cc_Success;
   } 
   chan->VoiceChannel = Enabled;

   //
   // Select acoustic or PCM echo canceller giving priority to acoustic echo canceller
   //
   if ((pCmd[6] & 0x0800) == 0) {                // Acoustic Echo cancellation
      PcmPkt->AECEchoCancel = Disabled;
   } else if (NumAECEcansUsed < sysConfig.AECInstances) {
      // Acoustic echo canceller selected. 
      pCmd[6] &= 0x7fff;                 // Turn off request for PCM canceller.
      if (!CheckAecMemory(PcmPkt->SamplesPerFrame, chan->ProcessRate))
         return Cc_InsuffBuffsAvail;
      PcmPkt->AECEchoCancel = Enabled;

      // Ensure that the TDM in and TDM out rates are the same when AEC is turned on
      if (PktPcm->SamplesPerFrame != PcmPkt->SamplesPerFrame)
           return Cc_InvalidPktOutSizeB;

   } else if ((pCmd[6] & 0x8000) == 0) 
      return Cc_AECEchoCancelNotCfg;     // Acoustic and PCM echo cancellers not available


   if ((pCmd[6] & 0x8000) == 0)   {             // PCM Echo cancellation
      PcmPkt->EchoCancel = Disabled;
   } else if (NumPcmEcansUsed < sysConfig.numPcmEcans) {
      PcmPkt->EchoCancel = Enabled;
   } else
      return Cc_PcmEchoCancelNotCfg;

   PktPcm->AECEchoCancel = Disabled;
   if ((pCmd[6] & 0x4000) == 0)   {             // Pkt Echo cancellation
      PktPcm->EchoCancel = Disabled;
   } else if (NumPktEcansUsed < sysConfig.numPktEcans) {
      PktPcm->EchoCancel = Enabled;
   } else
      return Cc_PktEchoCancelNotCfg;

   //
   // VAD / Comfort noise
   //
   if ((pCmd[6] & 0x2000) != 0)   {
      PcmPkt->VAD = SetVADMode (PcmPkt->Coding);
      if (PcmPkt->VAD == VadDisabled)
         return Cc_VadNotConfigured;
      if (sysConfig.VadReport == Enabled)
        PcmPkt->VAD |= VadNotifyEnabled;
   } else
      PcmPkt->VAD = VadDisabled;

   PktPcm->ForwardCNGPkts = Disabled;
   PktPcm->CNG = SetVADMode (PktPcm->Coding);

   if ((pCmd[6] & 0x0080) != 0)   {
      PktPcm->VAD = (GpakVADMode)VadCngEnabled; //SetVADMode (PcmPkt->Coding);
      //if (PktPcm->VAD == VadDisabled)
      //   return Cc_VadNotConfigured;
      if (sysConfig.VadReport == Enabled)
        PktPcm->VAD |= VadNotifyEnabled;
      PktPcm->PrevVadState = VAD_INIT_RELEASE;
   } else
      PktPcm->VAD = VadDisabled;
   //
   //  AGC
   //
   if ((pCmd[6] & 0x1000) == 0)
        PcmPkt->AGC = Disabled;
   else if (numAGCChansAvail == 0)
        return Cc_InsuffAgcResourcesAvail;
   else
        PcmPkt->AGC = Enabled;

   if ((pCmd[6] & 0x0100) == 0)
        PktPcm->AGC = Disabled;
   else if (numAGCChansAvail == 0)
        return Cc_InsuffAgcResourcesAvail;
   else
        PktPcm->AGC = Enabled;
   //
   // DTX
   //
   if ((pCmd[6] & 0x200) != 0) {
        if ((PcmPkt->VAD & VadMask) != VadCngEnabled)
            return Cc_VadNotConfigured;
        else {
            PcmPkt->DtxEnable = Enabled;
            PcmPkt->PrevVadState = VAD_INIT_RELEASE;
        }
   } else {
        PcmPkt->DtxEnable = Disabled;
   }

   //
   // Tone detection/generation paramters
   //
   // detection only on tdm to packet side.
   // Regular Gpak generation only on packet to tdm side.
   // Trilogy asks for tone gen on the tdm to packet side.
   // Cornet asks for Tone Det packet to tdm side for C66x build
   PcmPkt->tonePktGet = 0;
   PktPcm->tonePktPut = 0;
   PktPcm->ForwardTonePkts = Disabled;

   toneTypes = (GpakToneTypes) (pCmd[8] & 0xFFF);
   PcmPkt->toneTypes = toneTypes & (~(Tone_Regen));

   mfTones = cedTones = cngTones = arbTones = 0;
   toneStatus = ValidToneTypes (toneTypes, &mfTones, &cedTones, &cngTones, &arbTones);
   if (toneStatus != Cc_Success) return toneStatus;

   if ((toneTypes & Tone_Generate) && (numToneGenChansAvail == 0))
        return Cc_InsuffTGResourcesAvail;

   if ((toneTypes & ToneDetect) && (ToneDetectorsAvail == 0))
        return Cc_InsuffTDResourcesAvail;

   toneTypesPktPcm = (GpakToneTypes) (pCmd[13] & 0xFFF);
   PktPcm->toneTypes = toneTypesPktPcm & (Tone_Generate | Tone_Regen | Notify_Host | ToneDetect);
   //mfTones = cedTones = cngTones = arbTones = 0;
   toneStatus = ValidToneTypes (toneTypes, &mfTones, &cedTones, &cngTones, &arbTones);
   if (toneStatus != Cc_Success) return toneStatus;

   if ((toneTypes & Tone_Generate) && (numToneGenChansAvail == 0))
        return Cc_InsuffTGResourcesAvail;

   if ((toneTypes & ToneDetect) && (ToneDetectorsAvail == 0))
        return Cc_InsuffTDResourcesAvail;
   //
   //  T.38 FAX parameters
   //
   chan->fax.faxMode = disabled;
   faxMode = (GpakFaxMode_t)((pCmd[8] >> 14) & 0x0003);
   chan->fax.faxTransport = (GpakFaxTransport_t)((pCmd[8] >> 12) & 3);
   if (faxMode != disabled)   { 
      if (sysConfig.numFaxChans <= numFaxChansUsed)
         return Cc_FaxRelayNotConfigured;

      if ((faxMode != faxOnly) && (faxMode != faxVoice))
         return Cc_FaxRelayInvalidMode;

      if (chan->fax.faxTransport != faxRtp) 
         return Cc_FaxRelayInvalidTransport;

      chan->fax.faxMode = faxMode;

   }
   if (CheckIfBuffsAvail (PktPcm->EchoCancel) == 0)
        return Cc_InsuffBuffsAvail;

   if (ApiBlock.ApiVersionId <= 0x0401u) return Cc_Success;

   //----------------------------------------------------------
   // API Version 402 and better.
   //   CID and gains
   if ((pCmd[9] & CidReceive) && (numRxCidChansAvail == 0))  return Cc_RxCIDNotAvail;
   if ((pCmd[9] & CidTransmit) && (numTxCidChansAvail == 0)) return Cc_TxCIDNotAvail;

   PcmPkt->CIDMode = CidDisabled;
   PktPcm->CIDMode = CidDisabled;
   if (pCmd[9] & CidReceive)  PcmPkt->CIDMode |= CidReceive;

   if (pCmd[9] & CidTransmit) PktPcm->CIDMode |= CidTransmit;

   PcmPkt->Gain.ToneGenGainG1  = pCmd[10];
   PcmPkt->Gain.OutputGainG2   = pCmd[11];
   PcmPkt->Gain.InputGainG3    = pCmd[12];
   ClampScaleGain(&PcmPkt->Gain);

   PktPcm->Gain.ToneGenGainG1  = PcmPkt->Gain.ToneGenGainG1;
   PktPcm->Gain.OutputGainG2   = PcmPkt->Gain.OutputGainG2; 
   PktPcm->Gain.InputGainG3    = PcmPkt->Gain.InputGainG3;         

   // Return with an indication of success.
   return Cc_Success;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ ProcPcm2PktStatMsg - Process a PCM To Packet Channel Status message.
//
// FUNCTION
//   This function prepares the Channel Status reply message for a PCM To Packet
//   type channel.
//
// RETURNS
//   Length I16 of reply message.
//}
ADT_UInt16 ProcPcm2PktStatMsg (ADT_UInt16 *pReply, chanInfo_t *chan) {

   pkt2pcm_t *PktPcm;
   pcm2pkt_t *PcmPkt;
   ADT_UInt16 ATone;
   int VAD;
   

   if (chan->pcmToPkt.VAD & VadEnabled)
      VAD = 1;
   else 
      VAD = 0;

   PcmPkt = &(chan->pcmToPkt);
   PktPcm = &(chan->pktToPcm);

   pReply[7] = PackPortandSlot (PcmPkt->InSerialPortId, PcmPkt->InSlotId);
   pReply[8] = PackPortandSlot (PktPcm->OutSerialPortId, PktPcm->OutSlotId);
   if (ApiBlock.ApiVersionId < 0x0603u) {
      pReply[9] = (ADT_UInt16) PackBytes (PktPcm->Coding,  PktPcm->SamplesPerFrame);
      pReply[10] = (ADT_UInt16) PackBytes (PcmPkt->Coding, PcmPkt->SamplesPerFrame);
   } else {
      pReply[9] = (ADT_UInt16) PackBytes (PktPcm->Coding,  sampsToHalfMS (PktPcm->SamplesPerFrame));
      pReply[10] = (ADT_UInt16) PackBytes (PcmPkt->Coding, sampsToHalfMS (PcmPkt->SamplesPerFrame));
   }

   ATone    = PcmPkt->toneTypes | PktPcm->toneTypes;
   pReply[11] = (ADT_UInt16)
      ((PcmPkt->EchoCancel << 15)           | ((PktPcm->EchoCancel << 14) & 0x4000) |
       ((VAD               << 13) & 0x2000) | ((PcmPkt->AGC        << 12) & 0x1000) |
       ((PcmPkt->AECEchoCancel << 11) & 0x0800) | 
       ((chan->VoiceChannel << 10) & 0x0400) | ((PcmPkt->DtxEnable << 9) & 0x0200));

   pReply[12] = 0;  // Obsolete pin IDs
   pReply[13] = (ADT_UInt16) ((chan->fax.faxMode << 14) | 
                           ((chan->fax.faxTransport << 12) & 0x3000) | (ATone & 0x0FFF));

   if (ApiBlock.ApiVersionId <= 0x0401u) return 14;

   //----------------------------------------------------------
   // API Version 402 and better.
   pReply[14] = (ADT_UInt16) ((PcmPkt->CIDMode & CidReceive) | (PktPcm->CIDMode & CidTransmit)) ;

   if (PktPcm->Gain.ToneGenGainG1 != (ADT_Int16)ADT_GAIN_MUTE)
        pReply[15] = (ADT_Int16) PktPcm->Gain.ToneGenGainG1/10;
   else
        pReply[15] = (ADT_Int16) PktPcm->Gain.ToneGenGainG1;

   if (PktPcm->Gain.OutputGainG2 != (ADT_Int16)ADT_GAIN_MUTE)
        pReply[16] = (ADT_Int16) PktPcm->Gain.OutputGainG2/10; 
   else 
        pReply[16] = (ADT_Int16) PktPcm->Gain.OutputGainG2; 

   if (PcmPkt->Gain.InputGainG3 != (ADT_Int16)ADT_GAIN_MUTE)
        pReply[17] = (ADT_Int16) PcmPkt->Gain.InputGainG3/10;  
   else
        pReply[17] = (ADT_Int16) PcmPkt->Gain.InputGainG3;  
   pReply[18] = (ADT_UInt16) chan->fromHostPktCount;
   pReply[19] = (ADT_UInt16) chan->toHostPktCount;

   return 20;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ FramePktB2PcmA - Decode (B-side) framing for a PCM To Packet type channel.
//
// FUNCTION
//   Reads incoming packet (chan->PktToPCM.inbuffer) from the host and 
//   decodes to PCM (chan->PktToPCM.outbuffer) 
//
// Transfers
//
//      Host Transfer
//          pkt ==>  pktPcm.inbuffer [pkt]
//                  (PktBufferPool[0][chan])
//          
//      ParsePacket
//          pktPcm.inbuffer [pkt] --> Task.InWork [parameters]
//            (PktBufferPool[0][chan])
//
//      VoiceDecode
//          Task.InWork [parameters] --> Task.OutWork [PCM]
//
//      pktPcm.EchoCancel == ENABLED
//          pktPcm.EcFar [PCM] ==> Task.Scratch [PCM]
//          (BulkDelayPool[chan])
//
//          Task.OutWork [PCM] + Task.Scratch [PCM] --> Task.OutWork [PCM] 
//                                                  --> Task.InWork [PCM for DTMF]
//      AGC == ENABLED
//          Task.OutWork [PCM] --> Task.OutWork [PCM]
//
//      
//      Task.OutWork[PCM] --> pktPcm.outbuffer [PCM]
//                               (PcmOutBufferPool[chan])
//
//      DMACopy
//          pktPcm.outbuffer [PCM] --> Serial port [PCM]
//            (PcmOutBufferPool[chan])
//
// Inputs
//        chan -  Channel data structure pointer
//    SampsPerFrame -  Samples per frame
//
// Work buffers
//   pktBuff -  Temporary linear buffer (Task.InWork) for packet data
//   pcmBuff -  Temporary linear buffer (Task.OutWork) for pcm data
//   ecFarWork - Temporary linear buffer (Task.Scratch) for far side echo cancellation
//
// RETURNS
//   nothing
//}
void FramePktB2PcmA (chanInfo_t *chan, void  *pktBuff, ADT_PCM16 *pcmBuff,
                      ADT_PCM16 *ecFarWork, int SampsPerFrame, int pcmBuffI8) {

   GpakPayloadClass pyldclass;      // packet's class
   pkt2pcm_t *PktPcm = &chan->pktToPcm;
   pcm2pkt_t *PcmPkt = &chan->pcmToPkt;
   CircBufInfo_t pcmAInfo;
   ADT_PCM16 * pktBuffST;
   short *DTMFBuff;     // pointer to PCM data for DTMF processing

   int sampsPerFrame, paramsI8;

   // Copy sidetone audio if wanted
    if (Enabled == PcmPkt->SideTone) { // A- enabled?
         memcpy (&pcmAInfo, PcmPkt->activeInBuffer, sizeof(CircBufInfo_t));
    }

   if (PktPcm->OutSerialPortId == SerialPortNull) 
      return;

   logTime (0x400B0000ul | chan->ChannelId);
   if (PktPcm->Coding != NullCodec) {
      // Read packet from host interface into pktBuff using pcmBuff as scratch for unpacking
      paramsI8 = pcmBuffI8;
      pyldclass = parsePacket (chan, pktBuff, &paramsI8, pcmBuff, pcmBuffI8);
   } else {
      memset (pcmBuff, 0, SampsPerFrame * sizeof(ADT_UInt16));
      pyldclass = PayClassNone;
   }

   // When T38 Fax relay is in progess PCM output will be generated by the PCM to packet processing thread.
   if (chan->fax.t38InProgress)  {
      logTime (0x400B0F00ul | chan->ChannelId);
      return;
   }

   // Decode packets (pktBuff) by playload class and type into PCM (pcmBuff) samples.
   sampsPerFrame = DecodePayload (chan, PktPcm, pyldclass, pktBuff, paramsI8, pcmBuff, pcmBuffI8);
   AppErr ("Buffer size error", SampsPerFrame < sampsPerFrame);

   DTMFBuff = (short *) pktBuff;
   i16cpy (DTMFBuff, pcmBuff, SampsPerFrame);
   ProcessVadToneOnDecodePath(chan, pcmBuff, DTMFBuff, SampsPerFrame);
   
   if (Enabled == PktPcm->EchoCancel)   {
     logTime (0x400B0200ul | chan->ChannelId);
     // Perform echo cancellation on far channel. Obtain far end samples from bulk delay buffer.
     copyCircToLinear (&(PktPcm->ecFarPtr), ecFarWork, sampsPerFrame);
     g168Capture (PktPcm->EcIndex, pcmBuff, ecFarWork, sampsPerFrame);
     LEC_ADT_g168Cancel (PktPcm->pG168Chan,   (short *) pcmBuff,
                        (short *) ecFarWork, (short *) pktBuff);

     g168PostCapture (PktPcm->EcIndex, pcmBuff, ecFarWork, sampsPerFrame);
   }

   // generate tones if enabled
   logTime (0x400B0800ul | chan->ChannelId);

   ToneGenerate (&PktPcm->TGInfo, pcmBuff, sampsPerFrame);

   // Apply Tonegen Gain G1 when generator is active
   if (PktPcm->TGInfo.active && PktPcm->Gain.ToneGenGainG1)
      ADT_GainBlockWrapper (PktPcm->Gain.ToneGenGainG1, pcmBuff, pcmBuff, sampsPerFrame);

   // Apply Pcm Output Gain G2
   if (PktPcm->Gain.OutputGainG2)
      ADT_GainBlockWrapper (PktPcm->Gain.OutputGainG2, pcmBuff, pcmBuff, sampsPerFrame);

   // Apply voice play back
   if (PktPcm->playA.state & PlayBack)
      voicePlayback (&PktPcm->playA, pcmBuff, ecFarWork, sampsPerFrame, ADevice, chan->ChannelId);

   waveFilePlayback (&(PktPcm->waveInfo), pcmBuff, sampsPerFrame); 

   if (Enabled == PcmPkt->SideTone) { // A- enabled?
  	  int i;
  	  pktBuffST = (ADT_PCM16 *) pktBuff;
  	  copyCircToLinear (&pcmAInfo, pktBuffST, SampsPerFrame);
  	  ADT_GainBlockWrapper (PcmPkt->SideToneDB, pktBuffST, pktBuffST, SampsPerFrame);
  	  for (i = 0; (i < SampsPerFrame); i ++) {
  		  pcmBuff[ i ] = (_sadd2(pcmBuff[ i ], pktBuffST[ i ]) & 0xffff);
  	  }
    }

   // Apply caller ID
   if (PktPcm->CIDMode & CidTransmit)
      txCidProcess (chan->ChannelId, ADevice, &(PktPcm->cidInfo), pcmBuff, sampsPerFrame);

	// Type 2 CID, on Rx path, seding the Ack tone to CO side
	if(sysConfig.type2CID && PcmPkt->cidInfo.type2CID && (PcmPkt->CIDMode & CidReceive)) {
	   rxCid2SndProcess(chan->ChannelId, ADevice, &(PcmPkt->cidInfo), pcmBuff, SampsPerFrame);
	}

   // playback B-buffer capture or vector to PCM output
   play_record(chan->ChannelId, pcmBuff, SampsPerFrame, (PLAYREC_B | PLAYREC_PLAY));

   if (PktPcm->AGC == Enabled) {
        // Perform AGC on pcm buffer
        PktPcm->AGCPower = AGC_ADT_run (PktPcm->agcPtr, (short *) pcmBuff, SampsPerFrame, 0, 0, (short *) pcmBuff, NULL);
   }

   // copy the linear pcm buffer into the circular pcm buffer
   if (Enabled == PcmPkt->AECEchoCancel) {
      // Store pcm for output by PCM to PKT side
      i16cpy (PcmPkt->ecFarPtr.pBufrBase, pcmBuff, sampsPerFrame);
   } else {
      ecSigTrace (PktPcm->OutSerialPortId, PktPcm->OutSlotId, 1, (ADT_Int16 *)pcmBuff, NULL, sampsPerFrame);

      sampsPerFrame = toTDMCirc (chan, BDevice, pcmBuff, sampsPerFrame, &PktPcm->rateCvt);
   }

   // B-side output recording
   if (PktPcm->recB.state & Record) {
      voiceRecord (&PktPcm->recB, pcmBuff, ecFarWork, sampsPerFrame, BDevice, chan->ChannelId);
   }

   captureFaxOutput (chan, pcmBuff, sampsPerFrame);
   logTime (0x400B0F00ul | chan->ChannelId);
   return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ FramePcmA2PktB - Encode (A-side) framing for a PCM To Packet type channel.
//
// FUNCTION
//   Reads PCM data (chan->PcmToPkt.activeInBuffer), encodes it into a packet
//   and sends it to the host (chan->PcmToPkt.outBuffer)
//
// Transfers
//          
//      DMACopy
//         Serial port [PCM] ==> pcmPkt.activeInBuffer [PCM]
//                                (PcmInBufferPool[chan])
//
//      pcmPkt.activeInBuffer [PCM] ==> Task.InWork [PCM]
//         (PcmInBufferPool[chan])
//
//         
//         Task.InWork [PCM] ==>  Task.InWork [PCM]
//
//      pcmPkt.EchoCancel == ENABLED
//          pcmPkt.EcFar [PCM] --> Task.Scratch [PCM]
//           (PcmOutBufferPool[chan])
//
//          Task.InWork [PCM] + Task.Scratch[PCM] --> Task.InWork [PCM]
//                                                --> Task.OutWork [PCM for DTMF]
//
//      fax = ENABLED
//          Task.InWork [PCM] + Task.Scratch[PCM] --> Task.InWork [PCM]
//           
//
//      AGC == ENABLED
//          Task.InWork [PCM] --> Task.InWork [PCM]
//
//      VoiceRecord
//          Task.InWork [PCM] --> Task.InWork [PCM]
//                            --> pcmPkt.recA->Circ
//
//      pktPcm.EchoCancel == ENABLED
//          Task.InWork [PCM] --> pcmPkt.ecBulkDelay
//                                (BulkDelayPool[chan])
//
//      ProcessVadToneEncode
//          Tone:  Task.OutWork [PCM] --> pcmPkt.outbuffer [pkt]
//                                        (PktBufferPool[1][chan])
//          Voice: Task.InWork [PCM] --> Task.OutWork [parameters]
//                 Task.OutWork [parameters] --> pcmPkt.outbuffer [pkt]
//                                               (PktBufferPool[1][chan])
//
//      Host Transfer
//          pcmPkt.outbuffer [pkt]  --> pkt
//          (PktBufferPool[1][chan])
//
// Inputs
//        chan -  Channel data structure pointer
//    SampsPerFrame -  Samples per frame
//
// Work buffers
//   pcmBuff -  Temporary linear buffer (Task.InWork) for pcm data
//   pktBuff -  Temporary linear buffer (Task.OutWork) for packet data
//   ecFarWork - Temporary linear buffer (Task.Scratch) for far side echo cancellation
//
// RETURNS
//   nothing
//}
void FramePcmA2PktB (chanInfo_t *chan, ADT_PCM16 *pcmBuff, void *pktBuff,
                     ADT_PCM16 *ecFarWork, int SampsPerFrameTDM) {
                     
   short *DTMFBuff;     // pointer to PCM data for DTMF processing

   pcm2pkt_t *PcmPkt = &chan->pcmToPkt;
   pkt2pcm_t *PktPcm = &chan->pktToPcm;
   int slipCnt, SampsPerFrame;

   if (PcmPkt->InSerialPortId == SerialPortNull)
      return;

   SampsPerFrame = SampsPerFrameTDM;
   if (!findTestVector (chan, pcmBuff, PktPcm, SampsPerFrame))
      return;

  logTime (0x400A0000ul | chan->ChannelId);

   // copy the input pcm data from circular buffer to linear work buffer
   slipCnt = fromTDMCirc (chan, ADevice, pcmBuff, &SampsPerFrame, &PcmPkt->rateCvt);

   // playback A-buffer capture or vector to PCM input
   play_record(chan->ChannelId, pcmBuff, SampsPerFrame, (PLAYREC_A | PLAYREC_PLAY));

   if (slipCnt && Enabled == PcmPkt->EchoCancel) 
      addSlipToCirc (&(PcmPkt->ecFarPtr), slipCnt);

	// Type 2CID, on Rcv path, Tx CID detects Ack tone from other from TE
	if(sysConfig.type2CID && PktPcm->cidInfo.type2CID && (PktPcm->CIDMode & CidTransmit)) {
	   txCid2RcvProcess(chan->ChannelId, ADevice, &PktPcm->cidInfo, pcmBuff, SampsPerFrame);
	}

   if (PcmPkt->CIDMode & CidReceive)
      rxCidProcess (chan->ChannelId, ADevice, &PcmPkt->cidInfo, pcmBuff, SampsPerFrame);

   if (Enabled == PcmPkt->AECEchoCancel) {
      DTMFBuff = (short *) pcmBuff;
      runAEC (chan, pcmBuff, ecFarWork, SampsPerFrame, &PktPcm->rateCvt);
   } else if (Enabled == PcmPkt->EchoCancel) {

      // Perform pcm echo cancellation; DTMF contains pre-NLP for DTMF detection.
      DTMFBuff = (short *) pktBuff;

      // Perform echo cancellation on pcmBuff, Obtain far data from far end's TDM output buffer.   Todo.  DownConversion
      SampsPerFrame = SampsPerFrameTDM;
      fromTDMCirc (chan, ASideECFar, ecFarWork, &SampsPerFrame, NULL);

      ecSigTrace (PcmPkt->InSerialPortId, PcmPkt->InSlotId, 4, (ADT_Int16 *)pcmBuff, (ADT_Int16  *)ecFarWork, SampsPerFrame);
      g168Capture (PcmPkt->EcIndex, pcmBuff, ecFarWork, SampsPerFrame);
      LEC_ADT_g168Cancel (PcmPkt->pG168Chan, (short *) pcmBuff, (short *) ecFarWork, DTMFBuff);
      g168PostCapture (PcmPkt->EcIndex, pcmBuff, ecFarWork, SampsPerFrame);

   } else {
      DTMFBuff = (short *) pktBuff;
      i16cpy (DTMFBuff, pcmBuff, SampsPerFrame);
   }

   if (chan->fax.faxMode != disabled) {
      // call the relay PCM interface
      logTime (0x400A1200ul | chan->ChannelId);
      processFaxRelay (chan, pcmBuff, ecFarWork, SampsPerFrame);

      // skip the remaining processing if fax relay is active
      if (chan->fax.t38InProgress)
         return;
   }

   // Apply Pcm Input Gain G3
   if (PcmPkt->Gain.InputGainG3)
      ADT_GainBlockWrapper (PcmPkt->Gain.InputGainG3, pcmBuff, pcmBuff, SampsPerFrame);

   if (PcmPkt->AGC == Enabled) {
      // Perform AGC on pcm buffer
      logTime (0x400A0500ul | chan->ChannelId);
      PcmPkt->AGCPower = AGC_ADT_run (PcmPkt->agcPtr, (short *) pcmBuff, SampsPerFrame, 0, 0, (short *) pcmBuff, NULL);
   }
   
   // copy pcm samples into scratch buffer ecFarWork for voice recording
   if (PcmPkt->recA.state & Record) {
      i16cpy (ecFarWork, pcmBuff, SampsPerFrame);
      voiceRecord (&PcmPkt->recA, ecFarWork, pktBuff, SampsPerFrame, ADevice, chan->ChannelId);
   }

   if (PcmPkt->playB.state & PlayBack)
      voicePlayback (&PcmPkt->playB, pcmBuff, ecFarWork, SampsPerFrame, BDevice, chan->ChannelId);

   waveFilePlayback (&(PcmPkt->waveInfo), pcmBuff, SampsPerFrame); 

   // Store pcm data for far end echo canceller in bulk delay buffer.
   if (Enabled == chan->pktToPcm.EchoCancel) {
      logTime (0x400A0B00ul | chan->ChannelId);
      copyLinearToCirc (pcmBuff, &chan->pktToPcm.ecFarPtr, SampsPerFrame);
   }

   // generate tones if enabled
   ToneGenerate (&PcmPkt->TGInfo, pcmBuff, SampsPerFrame);
   
   // Perform VAD, Tone Detection, and Encode functions.
   ProcessVadToneEncode (chan, (ADT_PCM16 *) pcmBuff, DTMFBuff, pktBuff, SampsPerFrame);
   
   logTime (0x400A0F00ul | chan->ChannelId);
   return;
}



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ SetupPcm2PktChan - Setup a PCM To Packet channel.
//
// FUNCTION
//   This function performs channel setup initialization for a PCM To Packet
//   type channel.
//
// RETURNS
//   nothing
//}
void SetupPcm2PktChan (chanInfo_t *chan) {
   pcm2pkt_t *PcmPkt;   // pointer to channel's PCM to Packet data
   pkt2pcm_t *PktPcm;   // pointer to channel's Packet to PCM data

   // Get pointers to the channel's A and B Sides
   PcmPkt = &chan->pcmToPkt;
   PktPcm = &chan->pktToPcm;

   // Modes that force narrow band processing
   if (PcmPkt->CIDMode || PcmPkt->EchoCancel ||
       PktPcm->CIDMode || PktPcm->EchoCancel ||
      chan->fax.faxMode)
      chan->ProcessRate = 8000;


   // Allocate Pcm, Pkt and BulkDelay Buffers
   AllocBuffs (chan);

   // Initialize PCM buffers
   PcmPkt->activeInBuffer = &PcmPkt->inbuffer;
   memset (PcmPkt->inbuffer.pBufrBase,  0, (PcmPkt->inbuffer.BufrSize)*sizeof(ADT_PCM16));

   // tonePkt put/get pointers not used in pcm/pkt mode
   PcmPkt->tonePktGet = 0;
   PktPcm->tonePktPut = 0;
   PktPcm->ForwardTonePkts = Disabled;
   PktPcm->ForwardCNGPkts = Disabled;

   // Initialize the PCM output buffer based on Vector test mode.
#if VECTOR_TEST_BUILD
   if (CurrentTestMode == TestSyncPcmInput)   {
      chan->Flags |= CF_VECTOR_TEST_MODE;
      PktPcm->OutSerialPortId = SerialPortNull;
      PktPcm->OutSlotId = CurrentTestParm;
      PcmPkt->EchoCancel = Disabled;
   }   else
#endif
   {
      memset (PktPcm->outbuffer.pBufrBase, 0, (PktPcm->outbuffer.BufrSize)*sizeof(ADT_PCM16));
   }

   // Initialize the PCM Echo Canceller if enabled.
   PcmPkt->ecFarPtr.SlipSamps = 0;
   PcmPkt->ecBulkDelay.SlipSamps = 0;
   if (PcmPkt->AECEchoCancel == Enabled) {
      PcmPkt->pG168Chan =
         AllocEchoCanceller (ACOUSTIC , PcmPkt->SamplesPerFrame, &PcmPkt->EcIndex, chan->ProcessRate);
      if (PcmPkt->pG168Chan == NULL)
      {
	     PcmPkt->AECEchoCancel = Disabled;
      }
      else
      {
         PcmPkt->ecFarPtr.pBufrBase = pAECFarPool[PcmPkt->EcIndex & 0xFF];
         PcmPkt->ecFarPtr.BufrSize  = PcmPkt->SamplesPerFrame;
         memset (PcmPkt->ecFarPtr.pBufrBase, 0,  PcmPkt->SamplesPerFrame * sizeof (ADT_PCM16));
      }

   } else if (PcmPkt->EchoCancel == Enabled) {
      PcmPkt->ecFarPtr.pBufrBase = PktPcm->outbuffer.pBufrBase;
      PcmPkt->ecFarPtr.BufrSize  = PktPcm->outbuffer.BufrSize;
      PcmPkt->EcIndex = chan->CoreID;
      PcmPkt->pG168Chan =
         AllocEchoCanceller (G168_PCM, PcmPkt->SamplesPerFrame, &PcmPkt->EcIndex, chan->ProcessRate);
   }

   // Initialize the Packet Echo Canceller if enabled.
   if (PktPcm->EchoCancel == Enabled) {
      memset (PcmPkt->ecBulkDelay.pBufrBase, 0,
             (PcmPkt->ecBulkDelay.BufrSize)*sizeof(ADT_UInt16));
      
      PktPcm->ecFarPtr.pBufrBase = PcmPkt->ecBulkDelay.pBufrBase;
      PktPcm->ecFarPtr.BufrSize  = PcmPkt->ecBulkDelay.BufrSize;
      PktPcm->EcIndex = chan->CoreID;
      PktPcm->pG168Chan =
         AllocEchoCanceller (G168_PKT, PktPcm->SamplesPerFrame, &PktPcm->EcIndex, chan->ProcessRate);
   }


   // Initialize tone detection and tone relay instances
   if ((PcmPkt->toneTypes & ToneDetectAll) != Null_tone) {
      InitToneDetection (PcmPkt->toneTypes,   &PcmPkt->TDInstances, 
                         PcmPkt->toneRelayPtr, PcmPkt->SamplesPerFrame);
   }
   if ((PktPcm->toneTypes & ToneDetectAll) != Null_tone) {
      InitToneDetection (PktPcm->toneTypes,   &PktPcm->TDInstances,
                         0, PktPcm->SamplesPerFrame);
   }

   // Initialize VAD and Comfort noise generation
   if ((PcmPkt->VAD & VadCng) || (PktPcm->CNG & VadCng)) {
      InitVadCng (PcmPkt->vadPtr, &PcmPkt->VadParms, PcmPkt->SamplesPerFrame, PktPcm->SamplesPerFrame, chan->ProcessRate);
   }
   // Initialize VAD
   if (PktPcm->VAD & VadCng) {
      InitVadCng (PktPcm->vadPtr, &PktPcm->VadParms, PcmPkt->SamplesPerFrame, PktPcm->SamplesPerFrame, chan->ProcessRate);
   }

   // Allocate AGC and initialize instances
   AllocAGC (chan, ADevice, PcmPkt->AGC);
   AllocAGC (chan, BDevice, PktPcm->AGC);

   // Allocate tone generation instances
   // Trilogy's request for tone gen on Pkt side
   AllocToneGen (&PcmPkt->TGInfo, PcmPkt->toneTypes); //AllocToneGen (&PcmPkt->TGInfo, 0);
   AllocToneGen (&PktPcm->TGInfo, PktPcm->toneTypes);

   // Initialize tone relay generation instance.
   if (PktPcm->TGInfo.toneRlyGenPtr) {
      InitToneRelayGen (&PktPcm->TGInfo);
   }

   // Allocate and initialize T.38 fax relay
   AllocFaxRelay (chan);

   // Initialize vocoders
   initEncoder (PcmPkt, 0, chan->ProcessRate);
   initDecoder (PktPcm, 0, chan->ProcessRate);

   // Initialize Caller ID
   NullCID (&(PcmPkt->cidInfo));
   NullCID (&(PktPcm->cidInfo));
   AllocCID (chan, ADevice, PcmPkt->CIDMode, PktPcm->CIDMode);
   
   // Initialize DMA info structures
   dmaInitInfoStruct (chan, PcmPkt->EcIndex, PktPcm->EcIndex);

   // Allocate and initialize sampling rate converter instances
   AllocEncoderSampleRateConv (chan);
   AllocDecoderSampleRateConv (chan);

   return;
}
