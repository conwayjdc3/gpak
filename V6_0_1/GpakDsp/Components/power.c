// power.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include "power.h"
#ifndef WIN32
#define	norm_l(L_var1)_norm(L_var1)
#else
static Word16 norm_l(Word32 L_var1)
  {
   Word16 var_out;

   if (L_var1 == 0)
     {
      var_out = 0;
//      var_out = 31;
     }
   else
     {
      if (L_var1 == (Word32)0xffffffffL)
        {
         var_out = 31;
        }
      else
        {
         if (L_var1 < 0)
           {
            L_var1 = ~L_var1;
           }

         for(var_out = 0;L_var1 < (Word32)0x40000000L;var_out++)
           {
            L_var1 <<= 1;
           }
        }
     }

   return(var_out);
  }
#endif

/*
 *****************************************************************************************
 * Function: PowerTodBm10
 * Purpose: Converts from power to dBm10 (dBm*10)
 *****************************************************************************************
 */
#define ABS(x) ((x>=0)? x:-x )
#define MAX_32 0x7fffffff
#define MIN_32 0x80000000
static ADT_Int16 Fine_dBPowerTable[] = 
{
	-30,
	-29,
	-28,
	-28,
	-27,
	-26,
	-26,
	-25,
	-24,
	-24,
	-23,
	-23,
	-22,
	-22,
	-21,
	-20,
	-20,
	-19,
	-19,
	-18,
	-18,
	-17,
	-17,
	-16,
	-16,
	-15,
	-15,
	-14,
	-14,
	-13,
	-13,
	-12,
	-12,
	-12,
	-11,
	-11,
	-10,
	-10,
	-9,
	-9,
	-9,
	-8,
	-8,
	-7,
	-7,
	-6,
	-6,
	-6,
	-5,
	-5,
	-5,
	-4,
	-4,
	-3,
	-3,
	-3,
	-2,
	-2,
	-2,
	-1,
	-1,
	-1,
	0,
	0
};

static ADT_Int16 PowerTodBm10(ADT_Int64 PPower)
{
	Int16 Mantissa,Exponent;
	Int16 Shift;
	Int16 dBmCoarse;
	Int16 dBmFine;
	Int16 dBm;
	ADT_Int32 Power;
	ADT_Int16 Offset = 0;
	ADT_Int64 Power64;

	Power64 = (ADT_Int64) PPower;
	if (ABS(Power64) > MAX_32)
	{
		Power64 >>= 8;
		Offset = 240;	//24 dB shift = 1/256
	}
	if (ABS(Power64) > MAX_32)
	{
		Power64 >>= 8;
		Offset += 240;
	}
	Power = (ADT_Int32) Power64;

	if (Power == 0)
		return(-960);
	Exponent = norm_l(Power);
	Shift = (Exponent - 16);
	if (Shift > 0)
		Mantissa = (Int16) (Power << Shift);
	else
		Mantissa = (Int16) (Power >> -Shift);
	dBmCoarse = ((-16-Shift)*3 + 3) * 10;	//*10 for tenths of dB
	dBmFine = Fine_dBPowerTable[(Mantissa>>(15-7)) - 64];
	dBm = dBmCoarse + dBmFine+Offset;
	return(dBm);
}


void PWR_ADT_initObj(Power_t *pChannel, ADT_Int32 Interval)
{
	pChannel->SampleCount = 0;
	pChannel->Interval = Interval;
	pChannel->PowerSum = 0LL;
	pChannel->CurrentPowerdBm10 = -960;
}

ADT_Int16 PWR_ADT_update(Power_t *pChannel, ADT_Int16 *pSamples, ADT_Int16 NSamples, ADT_Int16 *pUpdateFlag)
{
	ADT_Int16 PowerdBm10;
	ADT_Int16 i;
	*pUpdateFlag = 0;
	PowerdBm10 = pChannel->CurrentPowerdBm10;
	for (i = 0; i < NSamples; i++)
	{
		pChannel->PowerSum += (ADT_Int32) pSamples[i] * (ADT_Int32) pSamples[i];
	}
	pChannel->SampleCount += NSamples;
	if (pChannel->SampleCount >= pChannel->Interval)
	{
		pChannel->PowerSum /= pChannel->SampleCount;
		PowerdBm10 = PowerTodBm10(pChannel->PowerSum)+60;
		pChannel->CurrentPowerdBm10 = PowerdBm10;
		*pUpdateFlag = 1;
		pChannel->SampleCount = 0;
		pChannel->PowerSum = 0LL;
	}
	return(PowerdBm10);
}

