/*
 * Copyright (c) 2004-2007, Adaptive Digital Technologies, Inc.
 *
 * File Name: LbCoder.c
 *
 * Description:
 *   This file contains functions to support Loopback Coder type channels.
 *
 * Version: 1.0
 *
 * Revision History:
 *   5/27/09 - Initial release.
 */
   
// Application related header files.
#include "GpakDefs.h"
#include "GpakErrs.h"
#include "GpakExts.h"
#include "sysconfig.h"


extern ADT_UInt32 *lbScratch; 
extern ADT_UInt16 lbScratchI16; 

extern chanInfo_t *LoopbackList[];
extern int teardownLBPending[];
extern int teardownLBCoder[];
extern int numLbsInUse;


#pragma CODE_SECTION (lbCoderInit , "SLOW_PROG_SECT")
void lbCoderInit () {
   int i;

   numLbsInUse = 0;
   for (i=0; i< sysConfig.maxNumLbCoders; i++) {
      LoopbackList[i] = NULL;
      teardownLBPending[i] = 0;            
      teardownLBCoder[i] = 0;
   }
   return;
}

// getGBScratch obtains scratch buffers needed to initialize the vocoders for background processing
#pragma CODE_SECTION (getBGScratch , "SLOW_PROG_SECT")
void *getBGScratch (ADT_UInt16 *bgScratchI16) {
   *bgScratchI16 = lbScratchI16;
   return (void *) lbScratch;
}


//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ProcCfgLpbkMsg - Process a Configure Loopback Coder Channel message.
//
// FUNCTION
//   Performs Loopback Coder channel processing for a Configure Channel message.
//
//  Word:Bits
//     2:ff00    -  Input Coder type
//     2:00ff    -  Output Coder type
//     3:ff00    -  Frame size (1/2 ms).
//
// Inputs
//       pCmd  - Command message buffer
//
// Outputs
//     pChan - Channel data structure pointer
//
// RETURNS
//  Return status for reply message.
//}
#pragma CODE_SECTION (ProcCfgLpbkMsg , "SLOW_PROG_SECT")
GPAK_ChannelConfigStat_t ProcCfgLpbkMsg (ADT_UInt16 *pCmd, chanInfo_t *chan) {
   int frameSizeTdmMS;

   pcm2pkt_t *PcmPkt;   // pointer to channel's PCM to Packet data
   pkt2pcm_t *PktPcm;   // pointer to channel's Packet to PCM data

   PcmPkt = &chan->pcmToPkt;
   PktPcm = &chan->pktToPcm;

    if (sysConfig.maxNumLbCoders <= numLbsInUse)
        return Cc_InsuffLbResourcesAvail;

   //
   //  Get Input vocoding parameters
   //
   PktPcm->Coding  =  (GpakCodecs) Byte1 (pCmd[2]);
   if (!ValidCoding (PktPcm->Coding, PktPcm->SamplesPerFrame))
      return Cc_InvalidPktInCodingA;

   //
   //  Get Output vocoding parameters
   //
   PcmPkt->Coding  =  (GpakCodecs) Byte0 (pCmd[2]);
   if (!ValidCoding (PcmPkt->Coding, PcmPkt->SamplesPerFrame))
      return Cc_InvalidPktOutCodingA;

   // Set sample size
   if (ApiBlock.ApiVersionId < 0x0603u) {
      PcmPkt->SamplesPerFrame = FrameSize_10_ms;
      PktPcm->SamplesPerFrame = FrameSize_10_ms;
   } else {
      frameSizeTdmMS = (Byte1 (pCmd[3]) / 2) * (TDMRate / 1000);
      PcmPkt->SamplesPerFrame = frameSizeTdmMS;
      PktPcm->SamplesPerFrame = frameSizeTdmMS;
   }

   channelAllocsNull (chan, PcmPkt, PktPcm);

   // Return with an indication of success.
   return Cc_Success;

}

#pragma CODE_SECTION (ProcCfgLpbkStatMsg, "SLOW_PROG_SECT")
ADT_UInt16 ProcCfgLpbkStatMsg  (ADT_UInt16 *pReply, chanInfo_t *pChan) {

   pReply[7] = (ADT_UInt16) PackBytes (pChan->pktToPcm.Coding, pChan->pcmToPkt.Coding);
   pReply[8] = (ADT_UInt16) pChan->fromHostPktCount;
   pReply[9] = (ADT_UInt16) pChan->toHostPktCount;

   return 10;
}

#pragma CODE_SECTION (SetupLoopbackCoder, "SLOW_PROG_SECT")
void SetupLoopbackCoder (chanInfo_t *chan) {
   int i;

   // add the channel to the Loopback coder list
   for (i=0; i< sysConfig.maxNumLbCoders; i++) {
      if (LoopbackList[i] != 0) continue;
      LoopbackList[i] = chan;
      numLbsInUse++;

      // Allocate Pcm, Pkt and BulkDelay Buffers
      AllocBuffs (chan);
      break;
   }
   chan->Flags |= (CF_REINIT_ENCODE | CF_REINIT_DECODE);

   // Allocate and initialize sampler rate converter instances
   AllocEncoderSampleRateConv (chan);
   AllocDecoderSampleRateConv (chan);

}

#pragma CODE_SECTION (DeactivateLpbkCoder, "SLOW_PROG_SECT")
void DeactivateLpbkCoder (chanInfo_t *pChan) {
   chanInfo_t *pActChan;
   int i;

   // check for pending loopback channel teardowns
   for (i=0; i<sysConfig.maxNumLbCoders; i++) {
      pActChan = LoopbackList[i];
      if (pActChan == NULL) continue;
      if (pActChan != pChan) continue;

      teardownLBCoder[i] = 1;
      break;
   }
}

// called from higher priority SWI to complete pending loopback teardowns
#pragma CODE_SECTION (teardownLoopBack, "SLOW_PROG_SECT")
void teardownLoopBack () {
   int i;

   chanInfo_t *pChan;
   EventFifoMsg_t msg;

   // check for pending loopback channel teardowns
   for (i=0; i<sysConfig.maxNumLbCoders; i++) {
      pChan = LoopbackList[i];
      if (pChan == 0) continue;

      if (!teardownLBPending[i]) continue;
      LoopbackList[i] = 0;
      numLbsInUse--;
      teardownLBPending[i] = 0;            
      teardownLBCoder[i] = 0;

      // DeAllocate Pcm, Pkt and BulkDelay Buffers
      DeAllocBuffs(pChan);

      // send event to host indicating teardown is complete
      msg.header.channelId = pChan->ChannelId;
      msg.header.deviceSide = ADevice;
      msg.header.eventCode = EventLoopbackTeardownComplete;
      msg.header.eventLength = EVENT_FIFO_MSGLEN_ZERO;
      writeEventIntoFifo (&msg);
      pChan->channelType = inactive;
   }
}

#pragma CODE_SECTION (processLoopbackCoder, "SLOW_PROG_SECT")
void processLoopbackCoder() {
   chanInfo_t *pChan;
   pcm2pkt_t *PcmPkt;
   pkt2pcm_t *PktPcm;
   ADT_UInt16 *paramBuff;
   ADT_PCM16  *pcmBuff;
   GpakPayloadClass pyldclass;
   int i, paramsI8, pcmI8, sampleCnt;


    // check for teardown
   for (i=0; i<sysConfig.maxNumLbCoders; i++) {
      pChan = LoopbackList[i];
      if (pChan == 0) continue;

      // skip channels that are pending teardown
      if (teardownLBPending[i]) continue;

      if (teardownLBCoder[i]) {
         teardownLBPending[i] = 1;
         break;
      }
      PcmPkt = &pChan->pcmToPkt;
      PktPcm = &pChan->pktToPcm;

      if (pChan->Flags & CF_REINIT_DECODE) {
         initDecoder (PktPcm, 1, pChan->ProcessRate);
         pChan->Flags &= ~CF_REINIT_DECODE;
      }

      if ((pChan->Flags & CF_REINIT_ENCODE) != 0)   {
         initEncoder (PcmPkt, 1, pChan->ProcessRate);
         pChan->Flags &= ~CF_REINIT_ENCODE;
      }

      paramsI8  = sysConfig.pcmInBufferSize*2;
      paramBuff = PcmPkt->inbuffer.pBufrBase;

      pcmI8     = sysConfig.pcmOutBufferSize*2;
      pcmBuff   = (ADT_PCM16 *) PktPcm->outbuffer.pBufrBase;

      pyldclass = parsePacket (pChan, paramBuff, &paramsI8, pcmBuff, pcmI8);
      if (pyldclass == PayClassAudio) {
         sampleCnt = DecodePayload (pChan, PktPcm, pyldclass, paramBuff, paramsI8, pcmBuff, pcmI8);
         voiceEncode (pChan, pcmBuff, paramBuff, sampleCnt);
      }
   }
}
