unpackMSB4(unsigned char *pU8, unsigned short *pOut, unsigned short num)
{
int loopCount, i;

    // 2 output values per input byte
    loopCount = num/2;

    for (i=0; i<loopCount; i++)
    {
        *pOut++ = *pU8 >> 4;
        *pOut++ = *pU8++ & 0x0f;
    }
    return;
}    
packMSB4(unsigned char *pU8, unsigned short *pIn, unsigned short num)
{
int loopCount, i;

    // one output byte per two input values
    loopCount = num/2;
    for (i=0; i<loopCount; i++)
    {
        *pU8 = *pIn++ << 4;
        *pU8 |= (*pIn++ & 0x0f);
        pU8++;
    }
    return;
}    
