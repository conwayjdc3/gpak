/*
 * Copyright (c) 2004-2007, Adaptive Digital Technologies, Inc.
 *
 * File Name: GpakConf.c
 *
 * Description:
 *   This file contains functions to support G.PAK conferences.
 *
 * Version: 1.0
 *
 * Revision History:
 *   5/07/04 - Initial release.
 *
 */

// Application related header files.
#include "GpakDefs.h"
#include "GpakExts.h"
#include "GpakErrs.h"
#include "GpakHpi.h"



//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// GetGpakCnfr - Get the address of a G.PAK conference
//
// FUNCTION
//   This function determines the address of the conference member queue
//   head associated with the specified conference.
//
// RETURNS
//  Pointer to framing rate queue head.
//}
#pragma CODE_SECTION (GetGpakCnfr, "SLOW_PROG_SECT")
ConferenceInfo_t *GetGpakCnfr (int CoreID, int Cnfr) {

   int idx;

   if ((sysConfig.maxNumConferences + sysConfig.customCnfrCnt) < Cnfr) return NULL;
   idx = (CoreID * (sysConfig.maxNumConferences  + sysConfig.customCnfrCnt)) + Cnfr;
   return ConferenceInfo[idx];
}

#pragma CODE_SECTION (ProcConfToneGenMsg, "SLOW_PROG_SECT")
ADT_UInt16 ProcConfToneGenMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) { 

   ConferenceInfo_t *cnfr;
   ADT_UInt16 CnfrID;             // tone conference identifier
   ADT_UInt16 CoreID;

   GpakToneGenCmd_t ToneCmd;      // tone command
   GpakToneGenType_t ToneType;
   ADT_Int16 Frequency[4], OnTime[4], OffTime[4], Level[4];

   TGInfo_t     *toneInfo;
   TGParams_1_t *tgParams;
   short numFreqs, i, numOnOffs;

   // Get the Conf Id from the command message.
   CnfrID = pCmd[1] & 0xff;
   CoreID = ((pCmd[1] >> 8) & 0xff);

   // Verify that the Core ID is valid
   if (DSPTotalCores <= CoreID) 
      return Tg_InvalidCore;

   // Prepare the reply message.
   pReply[0] |= (MSG_CONFTONEGEN_REPLY << 8);
   pReply[1] = pCmd[1];

   if (sysConfig.maxNumConferences <= CnfrID)
      return Tg_InvalidChannel;

   cnfr = GetGpakCnfr (CoreID, CnfrID);
   if (cnfr->MemberList == NULL)
      return Tg_InactiveChannel;

   // Get the tone command, and tone parameters from
   // the request message.
   ToneCmd  = (GpakToneGenCmd_t)  (pCmd[2] & 1);
   ToneType = (GpakToneGenType_t) ((pCmd[2] >> 2) & 0x0003);

   // Verify the channel supports tone generation.
   toneInfo  = &(cnfr->TG_NB);
   if (toneInfo->index == VOID_INDEX)
       return Tg_toneGenNotEnabledA;

   // Validate the tone command
   if ((ToneType != TgCadence) && (ToneType != TgContinuous) && (ToneType != TgBurst))
      return Tg_InvalidToneGenType;

   if (ToneCmd == ToneGenStart) {
      Frequency[0] = (ADT_Int16) (pCmd[3]*10);
      Frequency[1] = (ADT_Int16) (pCmd[4]*10);
      Frequency[2] = (ADT_Int16) (pCmd[5]*10);
      Frequency[3] = (ADT_Int16) (pCmd[6]*10);

      Level[0]  = (ADT_Int16)  (pCmd[7]*10);
      Level[1]  = (ADT_Int16) (pCmd[10]*10);
      Level[2]  = (ADT_Int16) (pCmd[11]*10);
      Level[3]  = (ADT_Int16) (pCmd[12]*10);

      OnTime[0] = (ADT_Int16) pCmd[8];
      OnTime[1] = (ADT_Int16) pCmd[13];
      OnTime[2] = (ADT_Int16) pCmd[14];
      OnTime[3] = (ADT_Int16) pCmd[15];

      OffTime[0] = (ADT_Int16) pCmd[9];
      OffTime[1] = (ADT_Int16) pCmd[16];
      OffTime[2] = (ADT_Int16) pCmd[17];
      OffTime[3] = (ADT_Int16) pCmd[18];


      // Validate the tone parameters.
      numFreqs = 0;
      numOnOffs = 0;
      for (i=0; i<4; i++)  {
         if (Frequency[i] != 0)
            numFreqs++;
         if (OnTime[i] != 0)
            numOnOffs++;
      }

      if (numFreqs == 0) 
         return Tg_InvalidFrequency;

      for (i=0; i<numFreqs; i++) {
         if (Frequency[i] == 0)
            return Tg_InvalidFrequency;
         if ((Level[i] < (MIN_TONEGEN_LEVEL*10)) || (Level[i] > (MAX_TONEGEN_LEVEL*10)))
            return Tg_InvalidLevel;
      }

      if (ToneType != TgContinuous) {
         if (numOnOffs == 0)
             return Tg_InvalidOnTime;
         for (i=0; i<numOnOffs; i++) {
            if (OnTime[i] == 0)
               return Tg_InvalidOnTime;
         }
      }
   }

   // Protect framing task from operating on parameters while change is being made
   toneInfo->update = Disabled;
   if (ToneCmd == ToneGenStart) {
      tgParams = toneInfo->toneGenParmPtr;
      CLEAR_INST_CACHE (tgParams, sizeof (TGParams_1_t));
      FLUSH_wait ();

      tgParams->ToneType = ToneType; 
      tgParams->Level[0] = Level[0];
      tgParams->Level[1] = Level[1];
      tgParams->Level[2] = Level[2];
      tgParams->Level[3] = Level[3];
      tgParams->NOnOffs  = numOnOffs;
      tgParams->NFreqs   = numFreqs;  

      tgParams->OnTime[0]   = OnTime[0];
      tgParams->OnTime[1]   = OnTime[1];
      tgParams->OnTime[2]   = OnTime[2];
      tgParams->OnTime[3]   = OnTime[3];
      tgParams->OffTime[0]  = OffTime[0];
      tgParams->OffTime[1]  = OffTime[1];
      tgParams->OffTime[2]  = OffTime[2];
      tgParams->OffTime[3]  = OffTime[3];
      tgParams->Freq[0]  = Frequency[0];
      tgParams->Freq[1]  = Frequency[1];
      tgParams->Freq[2]  = Frequency[2];
      tgParams->Freq[3]  = Frequency[3];

      FLUSH_INST_CACHE (tgParams, sizeof (TGParams_1_t));
   }

   toneInfo->cmd   = ToneCmd;
   toneInfo->update = Enabled;
   FLUSH_wait ();


   toneInfo  = &(cnfr->TG_WB);
   if (toneInfo->toneGenPtr == NULL)    return Tg_Success;

   toneInfo->update = Disabled;
   if (ToneCmd == ToneGenStart) {
      tgParams = toneInfo->toneGenParmPtr;
      CLEAR_INST_CACHE (tgParams, sizeof (TGParams_1_t));
      FLUSH_wait ();

      tgParams->ToneType = ToneType; 
      tgParams->Level[0] = Level[0];
      tgParams->Level[1] = Level[1];
      tgParams->Level[2] = Level[2];
      tgParams->Level[3] = Level[3];
      tgParams->NOnOffs  = numOnOffs;
      tgParams->NFreqs   = numFreqs;  

      tgParams->OnTime[0]   = OnTime[0] * 2;
      tgParams->OnTime[1]   = OnTime[1] * 2;
      tgParams->OnTime[2]   = OnTime[2] * 2;
      tgParams->OnTime[3]   = OnTime[3] * 2;
      tgParams->OffTime[0]  = OffTime[0] * 2;
      tgParams->OffTime[1]  = OffTime[1] * 2;
      tgParams->OffTime[2]  = OffTime[2] * 2;
      tgParams->OffTime[3]  = OffTime[3] * 2;
      tgParams->Freq[0]  = Frequency[0] / 2;
      tgParams->Freq[1]  = Frequency[1] / 2;
      tgParams->Freq[2]  = Frequency[2] / 2;
      tgParams->Freq[3]  = Frequency[3] / 2;

      FLUSH_INST_CACHE (tgParams, sizeof (TGParams_1_t));
   }

   toneInfo->cmd    = ToneCmd;
   toneInfo->update = Enabled;
   FLUSH_wait ();


   return Tg_Success;

} 

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ProcConfigConferenceMsg - Process a Configure Conference message.
//
// FUNCTION
//   Performs processing for a host's Configure Conference message and builds the reply message.
// 
// Inputs
//  pCmd  -  Commad buffer pointer
//
// RETURNS
//  Return status for reply message.
//}
#pragma CODE_SECTION (ProcConfigConferenceMsg, "SLOW_PROG_SECT")
GPAK_ConferConfigStat_t ProcConfigConferenceMsg (ADT_UInt16 *pCmd) {
   GpakFrameSizes SamplesPerFrame;
   ADT_UInt8  CoreId;
   ADT_UInt16 CnfrId;         // Conference Id
   int mfTones, cedTones, cngTones, arbTones;
   TGInfo_t *pInfo;

   ConferenceInfo_t *gpakCnfr;
   int cnfrIdx;

   GPAK_ChannelConfigStat_t toneStatus;
   GpakToneTypes InputToneTypes;   // Input Tone Detection types
   ConfParams_t ConfParms;         // conference parameters
   ConfScratch_t *lclConfScratch; 

   // Get the Conference Id, Frame Size, and Input Tone Detection type from the
   // command message.
   CoreId  = Byte0 (pCmd[0]);
   CnfrId  = Byte1 (pCmd[1]);

   // Verify the frame size is valid.
   if (ApiBlock.ApiVersionId < 0x0603u)
      SamplesPerFrame = (GpakFrameSizes) Byte0 (pCmd[1]);
   else
      SamplesPerFrame = (GpakFrameSizes) halfMSToSamps (Byte0 (pCmd[1]));

   if (!ValidFrameSize (SamplesPerFrame))
      return Cf_InvalidFrameSize;


   InputToneTypes = (GpakToneTypes) (pCmd[2] & 0x0FFF);

   // Verify the Conference Id is valid.
   if ((sysConfig.maxNumConferences + sysConfig.customCnfrCnt) <= CnfrId)
      return Cf_InvalidConference;

   if (DSPTotalCores <= CoreId) 
      return Cf_InvalidCore;

   // Verify the conference is inactive.
   gpakCnfr = GetGpakCnfr (CoreId, CnfrId);
   if ((gpakCnfr->MemberList != NULL) ||  (gpakCnfr->CompositeList != NULL))
      return Cf_ConferenceActive;

   // Verify the input tone detection types are valid.
   mfTones = cedTones = cngTones = arbTones = 0;
   toneStatus = ValidToneTypes (InputToneTypes, &mfTones, &cedTones, &cngTones, &arbTones);
   if ((toneStatus != Cc_Success) || cedTones || cngTones) return Cf_InvalidToneTypes;

   // Setup and initialize the conference.
   cnfrIdx = (CoreId * sysConfig.maxNumConferences) + CnfrId;
   gpakCnfr->Inst           = ConferenceInst [cnfrIdx];
   gpakCnfr->FrameSize      = SamplesPerFrame;
   gpakCnfr->InputToneTypes = InputToneTypes;
   gpakCnfr->AgcInstance    = ConfAgcInstance[cnfrIdx];
   CLEAR_INST_CACHE (gpakCnfr->Inst,        sizeof (ConfInstance_t));  
   CLEAR_INST_CACHE (gpakCnfr->AgcInstance, sizeof (AGCInstance_t));  

#if 0
   if ((TDMRate == 16000) || SysAlgs.wbEnable)
      ConfParms.ConfType =  CONF_TYPE_MIXED_16K;
   else
      ConfParms.ConfType =  CONF_TYPE_8K_ONLY;
#else
   if (TDMRate == 8000)
      ConfParms.ConfType =  CONF_TYPE_8K_ONLY;
   else
      ConfParms.ConfType =  CONF_TYPE_MIXED_16K;
#endif

   ConfParms.MaxConferenceChannels = sysConfig.maxNumConfChans;
   ConfParms.FrameSizeMSecQ4 = (SamplesPerFrame * 16) / sysConfig.samplesPerMs;
   ConfParms.DominantN = sysConfig.numConfDominant;

   ConfParms.MaxNoiseSuppression = sysConfig.maxConfNoiseSuppress;
#if (DSP_TYPE == 64)
   // VAD run internally for G.PAK conferences, externally for custom conferences
   ConfParms.RunVADExternally    =  (sysConfig.maxNumConferences <= CnfrId);
#endif
   ConfParms.VADNoiseFloorThresh = sysConfig.vadNoiseFloorZThresh;
   ConfParms.VADHangMSec         = sysConfig.vadHangOverTime;
   ConfParms.VADWindowSizeMSec   = sysConfig.vadWindowMs;

   ConfParms.AGCTargetPowerIn    = sysConfig.agcTargetPower;
   ConfParms.AGCMaxLossLimitIn   = sysConfig.agcMinGain;
   ConfParms.AGCMaxGainLimitIn   = sysConfig.agcMaxGain;
   ConfParms.AGCLowSigThreshdBm  = sysConfig.agcLowSigThreshDBM;

   // Initialize the Conference NB Tone Generator Info Structures
   pInfo = &(gpakCnfr->TG_NB);
   pInfo->index    = CnfrId;
   pInfo->cmd      = ToneGenStop; 
   pInfo->active   = Disabled;
   pInfo->update   = Disabled;    
   pInfo->toneGenPtr     = ConfToneGenInstance[cnfrIdx];
   pInfo->toneGenParmPtr = ConfToneGenParams  [cnfrIdx];

   // Initialize the Conference WB Tone Generator Info Structures
   pInfo = &(gpakCnfr->TG_WB);
   if (ConfParms.ConfType == CONF_TYPE_MIXED_16K) {
      cnfrIdx += sysConfig.maxNumConferences;
      pInfo->index    = CnfrId;
      pInfo->cmd      = ToneGenStop; 
      pInfo->active   = Disabled;
      pInfo->update   = Disabled;    
      pInfo->toneGenPtr     = ConfToneGenInstance[cnfrIdx];
      pInfo->toneGenParmPtr = ConfToneGenParams  [cnfrIdx];
   } else {
      pInfo->toneGenPtr = NULL;
   }

   // Determine the scratch memory to use
   lclConfScratch = (ConfScratch_t *) getConfScratch (SamplesPerFrame);

   FLUSH_wait ();
   CONF_ADT_init (&ConfParms, gpakCnfr->Inst, lclConfScratch, gpakCnfr->AgcInstance);
   FLUSH_INST_CACHE (gpakCnfr->Inst,        sizeof (ConfInstance_t));  
   FLUSH_INST_CACHE (gpakCnfr->AgcInstance, sizeof (AGCInstance_t));  

   // Return with an indication of success.
   return Cf_Success;
}

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ProcessConference - Process a conference and all associated channels.
//
// FUNCTION
//   This function performs all framing operations for a particular conference
//   and all channels associated with the conference.
//
//  ---------------------------- Preconference transfers ------------------------
//  PCM Channels  {pCnfMbr->pInPCM == pktPcm.inbuffer}
//          
//      DMACopy
//         Serial port [PCM] ==> pcmPkt.activeInBuffer [PCM]
//                                (PcmInBufferPool[chan])
//
//      pcmPkt.activeInBuffer [PCM] ==> pktPcm.inbuffer [PCM]
//         (PcmInBufferPool[chan])      (PktBufferPool[chan*2])
//
//  Pkt Channels  [pCnfMbr->pInPCM == pcmPkt.inbuffer}
//
//      Host Transfer
//          pkt ==>  pktPcm.inbuffer [pkt]
//                  (PktBufferPool[0][chan])
//
//      ParsePacket
//          pktPcm.inbuffer [pkt] --> Task.InWork [parameters]
//            (PktBufferPool[0][chan])
//                                --> pktPcm.toneRly
//                                --> pcmPkt.inbuffer [temp work]
//                                    (PcmInBufferPool [chan])
//
//      VoiceDecode
//          Task.InWork [parameters] --> pcmPkt.inbuffer [PCM]
//                                      (PcmInBufferPool [chan])
//
//  All channels
//
//      pcmPkt.EchoCancel == ENABLED
//          pcmPkt.EcFar [PCM]                       --> Task.InWork [Far PCM]
//          (PcmOutBufferPool[chan])
//          
//          Task.InWork [Far PCM] +  pcmInBuf [PCM] --> pcmInBuf [PCM]
//                                                  --> Task.OutWork [PCM for DTMF]
//
//      AGC == ENABLED
//          Task.InWork [PCM]                         --> pcmInBuf [PCM]
//
//
//  ---------------------------- Postconference transfers ------------------------
//
//  Pcm Channels   {pCnfMbr->pOutPCM == pcmPkt.outbuffer}
//      LinToCirc
//          pcmPkt.outbuffer [PCM]                    --> pktPcm->outbuffer [PCM]
//
//      DMACopy
//          pktPcm.outbuffer [PCM]                    --> Serial port [PCM]
//          (PcmOutBufferPool[chan])
//
//  Pkt Channels   {pCnfMbr->pOutPCM == pcmPkt.inbuffer[SampsPerFrame]}
//      Transfer to bulk delay buffer
//      pktPcm.EchoCancel == ENABLED
//         pcmPkt.inbuffer [PCM]                    --> pcmPkt.ecBulkDelay
//         pktBuffStructTable[ch*2]                     (BulkDelayPool[chan])
//
//      ProcessVadToneEncode
//          Voice: pcmPkt.inbuffer [PCM]            --> Task.OutWork [parameters]
//                 Task.OutWork [parameters]        --> pcmPkt.outbuffer [pkt]
//                                                     (PktBufferPool[1][chan])
//
//      Host Transfer
//          pcmPkt.outbuffer [pkt]  --> pkt
//          (PktBufferPool[1][chan])
//
//  Composite Channels (composite PCM == Task.InWork)
//      LinToCirc
//          Task.InWork [PCM]                         --> pktPcm->outbuffer [PCM]
//
//      DMACopy
//          pktPcm.outbuffer [PCM]                    --> Serial port [PCM]
//          (PcmOutBufferPool[chan])
//
//
//      ProcessVadToneEncode
//          Voice: Task.InWork [PCM]                --> Task.OutWork [parameters]
//                 Task.OutWork [parameters]        --> pcmPkt.outbuffer [pkt]
//                                                     (PktBufferPool[1][chan])
//
//      Host Transfer
//          pcmPkt.outbuffer [pkt]  --> pkt
//          (PktBufferPool[1][chan])
//
//
// Inputs
//    Cnfr     -  Conference control structure pointer
//    CnfrInst -  Conference instance pointer
//
// Work buffers
//    pWork1   -  work buffer (Task.InWork) holding parameter data and composite pcm data
//    scratch  -  scratch work buffer (Task.OutWork)
//
// RETURNS
//  nothing
//}
#pragma CODE_SECTION (ProcessConference, "MEDIUM_PROG_SECT")
void ProcessConference (ConferenceInfo_t *Cnfr, ConfInstance_t *CnfrInst,
                       ADT_PCM16 *pWork1, void *scratch, void *scratch2, ADT_UInt16 workI8) {

   chanInfo_t *chan;         // pointer to Channel Info
   ADT_PCM16 *pcmInBuf;        // Conference channel's PCM Input Buffer
   ADT_PCM16 *pcmOutBuf;       // Confernece channel's PCM Output data buffer
   ADT_PCM16 *pcmDTMF;         // Buffer to use for DTMF checking (pre-NLP)
   ADT_PCM16 *compPcm;         // Composite PCM output
   ADT_PCM16 *compositeOut;

   ADT_Bool   InputsReady, AddMember;
   int slipCnt, paramsI8;
   int SampsPerFrame, SampsPerFrame8k;

   GpakPayloadClass PayClass;      // packet payload class

   conf_Member_t *pConfMember;      // pointer to Conference Member data
   pcm2pkt_t *PcmPkt;   // Channel's PCM to Packet control info
   pkt2pcm_t *PktPcm;   // Channel's Packet to PCM control info
   toneDetInfo_t toneInf;
   CircBufInfo_t pcmAInfo;
   ADT_PCM16 *dtmfWork;

   //---------------------------------------------------------------------------
   // Stage 1: Member channel preconference processing (decode, echo canc, tonedetect ...)
   //---------------------------------------------------------------------------
   // initialize the dma control structure for group B (decode) processing and
   // start the first DMA load outside of the processing loop
   InputsReady = TRUE;
   for (chan = Cnfr->MemberList; chan != NULL; chan = chan->nextPcmToPkt) {

      while ((chan != NULL) && (AlgorithmControl (&chan, CnfrMembers) == ALG_CTRL_DEQUEUE_CMD));
      if (chan == NULL) break;

      // Determine count of samples at channel's processing rate

      //
      //---  Member addition
      //
      pConfMember = ConfMemberData[chan->ChannelId];
      AddMember = chan->Flags & CF_CNFR_ADD;
      if (AddMember) {
         CLEAR_INST_CACHE (pConfMember, sizeof (conf_Member_t));
         chan->Flags &= ~CF_CNFR_ADD;
         FLUSH_wait ();
         CONF_ADT_addMember (CnfrInst, pConfMember);
      }

      pcmInBuf = pConfMember->pInPCM;
      PcmPkt = &chan->pcmToPkt;
      PktPcm = &chan->pktToPcm;

      // 
      //---   PCM - S/W Expand
      //      PKT - Parsing and decoding
      if (chan->channelType == conferencePcm) {
    	  // Copy sidetone audio if wanted
		 if (Enabled == PcmPkt->SideTone) { // A- enabled?
			  memcpy (&pcmAInfo, PcmPkt->activeInBuffer, sizeof(CircBufInfo_t));
		 }

         // Conference PCM -> copy circular PCM buffer to linear pcmInBuf.
         SampsPerFrame = Cnfr->FrameSize;
         slipCnt = fromTDMCirc (chan, ADevice, pcmInBuf, &SampsPerFrame, &PktPcm->rateCvt);
         if (slipCnt && Enabled == PcmPkt->EchoCancel) 
            addSlipToCirc (&PcmPkt->ecFarPtr, slipCnt);

      } else if (chan->channelType == conferenceMultiRate) {

         // Conference Pkt (mismatched rates) -> copy pkt-pkt circular PCM buffer to linear pcmInBuf.
         if (chan->ProcessRate < TDMRate)
            SampsPerFrame = Cnfr->FrameSize / (TDMRate / chan->ProcessRate);
         else
            SampsPerFrame = Cnfr->FrameSize * (chan->ProcessRate / TDMRate);

         copyCircToLinear (&PcmPkt->inbuffer, (ADT_UInt16 *) pcmInBuf, SampsPerFrame);

      } else if (chan->channelType == conferencePacket)  {

         // Conference Pkt (matched rates) -> parse packet and decode data into linear pcmInBuf
         paramsI8 = workI8;
         PayClass = parsePacket (chan, pWork1, &paramsI8, pcmInBuf, workI8);
         if (PayClass == PayClassTone)   PayClass = PayClassNone;
         SampsPerFrame = DecodePayload (chan, PktPcm, PayClass, (ADT_UInt16 *) pWork1, paramsI8, pcmInBuf, workI8);         
      }
      AppErr ("Buffer size error", (workI8 / 2) < SampsPerFrame);
      //
      //---   Perform echo cancellation
      //       pcmInBuff points to modified PCM
      //
      play_record(chan->ChannelId, pcmInBuf, SampsPerFrame, (PLAYREC_B | PLAYREC_PLAY));
      chan->FrameCount++;
      pcmDTMF = pcmInBuf;

      if (Enabled == PcmPkt->AECEchoCancel) {
         runAEC (chan, pcmInBuf, pWork1, SampsPerFrame, &PcmPkt->rateCvt);
      } else if (Enabled == PcmPkt->EchoCancel) {

         logTime (0x400B0200ul | chan->ChannelId);
         // Perform echo cancellation using near end pcm (pcmInBuf), far end pcm (pWork1) and nearPreNLP (scratch)
         // Far end data is from near end output on PCM-Conference
         //                 from bulk delay on others
         pcmDTMF = (ADT_PCM16 *) scratch;
         if (chan->channelType == conferencePcm) {
            SampsPerFrame = Cnfr->FrameSize;
            slipCnt = fromTDMCirc (chan, ASideECFar, pWork1, &SampsPerFrame, &PcmPkt->rateCvt);
         } else
            copyCircToLinear (&PcmPkt->ecFarPtr, pWork1, SampsPerFrame);

         g168Capture (PcmPkt->EcIndex, pcmInBuf, pWork1, SampsPerFrame);
         LEC_ADT_g168Cancel (PcmPkt->pG168Chan, pcmInBuf, (ADT_Int16 *) pWork1, pcmDTMF);
         g168PostCapture (PcmPkt->EcIndex, pcmInBuf, pWork1, SampsPerFrame);
      }

      // Apply Pcm Input Gain G3
      ADT_GainBlockWrapper (PcmPkt->Gain.InputGainG3, pcmInBuf, pcmInBuf, SampsPerFrame);

      //
      //---   Perform tone detection
      //
      // Down sample as necessary for tones
      SampsPerFrame8k = SampsPerFrame;
      if ((chan->ProcessRate == 16000) && (Cnfr->InputToneTypes & ToneDetectAll)) {
         pcmDTMF = (ADT_PCM16 *) scratch;
         logTime (0x400B1900ul | chan->ChannelId);
         AppErr ("Rate err", PktPcm->rateCvt.DwnFrame != SampsPerFrame);
         SampsPerFrame8k = downConvert (PktPcm->rateCvt.DwnInst, pcmInBuf, SampsPerFrame, pcmDTMF);
      }

      if (Cnfr->InputToneTypes & ToneDetectAll) {
         toneInf.ChannelId   = chan->ChannelId;
         toneInf.deviceSide  = BDevice;
         toneInf.FrameSize8k = SampsPerFrame8k;
         toneInf.toneTypes   = Cnfr->InputToneTypes | Notify_Host;

         toneInf.TDInstances = &PktPcm->TDInstances;
         toneInf.DTMFBuff = pcmDTMF;
         toneInf.pcmBuff  = pcmDTMF;  
         toneInf.Rec = &PcmPkt->recA;
         logTime (0x400B0300ul | chan->ChannelId);
         toneDetect (&toneInf, PktPcm->FrameTimeStamp, chan->ProcessRate, Cnfr->FrameSize); 
      }

      //
      //---   Perform AGC
      //
      if (PcmPkt->AGC == Enabled) {
         // Perform AGC using same buffer (pcmInBuf) for input and output
         logTime (0x400B0500ul | chan->ChannelId);
         PcmPkt->AGCPower = AGC_ADT_run (PcmPkt->agcPtr, (short *) pcmInBuf, SampsPerFrame, 0, 0, (short *) pcmInBuf, NULL);
      }

      //
      //---   Perform voice recording
      //
      if (PcmPkt->recA.state & Record) {
         // copy pcm samples into scratch buffer pWork1 for voice recording
         i16cpy (pWork1, pcmInBuf, SampsPerFrame);
         voiceRecord (&PcmPkt->recA, (ADT_UInt16 *) pWork1, scratch, SampsPerFrame, ADevice, chan->ChannelId);
      }
      if (chan->channelType != conferenceMultiRate) {
         // NOTE: FrameTimeStamps for multirate conferences are updated by packet processing
         PktPcm->FrameTimeStamp += Cnfr->FrameSize;
      }

      if ((chan->channelType == conferenceMultiRate) || (chan->channelType == conferencePacket))  {
         captureDecoderOutput (chan, pcmInBuf, SampsPerFrame);
      }

   }

   // Skip all other conference processing if all inputs aren't ready 
   if (!InputsReady)
      return;

   //---------------------------------------------------------------------------
   // Stage 2: Perform the conferencing and conference wide tone generation
   //---------------------------------------------------------------------------

   // Use work1 to hold composite out
   compositeOut = (ADT_PCM16 *) pWork1;

   logTime (0x400C0000ul);
   SampsPerFrame = Cnfr->FrameSize;

   if (Cnfr->MemberList) {
      int SampsPerToneFrame;

      CONF_ADT_run (CnfrInst, compositeOut);

      // Perform Conference-wide tone generation.

      // Over-ride each conference member's output with generated tone data
      // Perform this step here before scratch buffer is used again.
      SampsPerToneFrame = Cnfr->FrameSize;
      if (TDMRate == 16000) SampsPerToneFrame /= 2;
      AppErr ("Buffer size error", (workI8 / 2) < SampsPerToneFrame);

      //  Narrowband tone generation.
      ToneGenerate (&Cnfr->TG_NB, scratch, SampsPerToneFrame);
      if (Cnfr->TG_NB.active) {
         for (chan = Cnfr->MemberList; chan != NULL; chan = chan->nextPcmToPkt) {
            pConfMember = ConfMemberData[chan->ChannelId];
            if (pConfMember->MemberType != MEMBER_TYPE_8K_B) continue;
            i16cpy (pConfMember->pOutPCM, scratch, SampsPerToneFrame);
         }
      }

      //  Wideband tone generation.
      if (Cnfr->TG_WB.toneGenPtr != NULL) {
         SampsPerToneFrame = Cnfr->FrameSize;
         if (TDMRate == 8000) SampsPerToneFrame *= 2;
         AppErr ("Buffer size error", (workI8 / 2) < SampsPerToneFrame);
         ToneGenerate (&Cnfr->TG_WB, scratch, SampsPerToneFrame);
         if (Cnfr->TG_WB.active) {
            for (chan = Cnfr->MemberList; chan != NULL; chan = chan->nextPcmToPkt) {
               pConfMember = ConfMemberData[chan->ChannelId];
               if (pConfMember->MemberType != MEMBER_TYPE_16K_B) continue;
               i16cpy (pConfMember->pOutPCM, scratch, SampsPerToneFrame);
            }
         }
      }
   } else {
      memset (compositeOut, 0, SampsPerFrame * sizeof(ADT_PCM16));
   }

   logTime (0x400CFF00ul);

   //---------------------------------------------------------------------------
   // Stage 3: Member channel post processing (encode, agc, tone gen, playback, copy output)
   //---------------------------------------------------------------------------
   for (chan = Cnfr->MemberList; chan != NULL; chan = chan->nextPcmToPkt)   {

      // Determine count of samples at channel's processing rate
      if (TDMRate < chan->ProcessRate)
         SampsPerFrame = Cnfr->FrameSize * (chan->ProcessRate / TDMRate);
      else
         SampsPerFrame = Cnfr->FrameSize / (TDMRate / chan->ProcessRate);
      AppErr ("Buffer size error", (workI8 / 2) < SampsPerFrame);

      //
      //---  Perform encoding and outbound transfers
      //
      pConfMember = ConfMemberData[chan->ChannelId];
      pcmOutBuf   = pConfMember->pOutPCM;
      PcmPkt = &chan->pcmToPkt;
      PktPcm = &chan->pktToPcm;

      if ((chan->channelType == conferenceMultiRate) || (chan->channelType == conferencePacket))  {
         captureEncoderInput (chan, pcmOutBuf, SampsPerFrame);
      }

      //
      //---  Perform outbound AGC
      //
      if (PktPcm->AGC == Enabled) {
         // Perform AGC using same buffer (pcmOutBuf) for input and output
         logTime (0x400A0500ul | chan->ChannelId);
         PktPcm->AGCPower = AGC_ADT_run (PktPcm->agcPtr, (short *) pcmOutBuf, SampsPerFrame, 0, 0, (short *) pcmOutBuf, NULL);
      }

      //
      //---  Perform tone generation with gain
      //
      logTime (0x400A0800ul | chan->ChannelId);

      ToneGenerate (&PktPcm->TGInfo, pcmOutBuf, SampsPerFrame);
      if (PktPcm->TGInfo.active)
         ADT_GainBlockWrapper (PktPcm->Gain.ToneGenGainG1, pcmOutBuf, pcmOutBuf, SampsPerFrame);

      // Apply Pcm Output Gain G2
      ADT_GainBlockWrapper (PktPcm->Gain.OutputGainG2, pcmOutBuf, pcmOutBuf, SampsPerFrame);


      //
      //---  Perform voice playback
      //
      if (PktPcm->playA.state & PlayBack)
         voicePlayback (&PktPcm->playA, pcmOutBuf, scratch, SampsPerFrame, ADevice, chan->ChannelId);

      //---  Perform voice compression
      //          Pcm - SW compression, rate conversion, copy to circ TDM
      //          Pkt - Bulk copy, Encoding
      if (chan->channelType == conferencePcm) {

         if (PktPcm->OutSerialPortId != SerialPortNull)  {
            if (Enabled == PcmPkt->AECEchoCancel) {
               // Store AEC data for output on next frame
               i16cpy (PcmPkt->ecFarPtr.pBufrBase, pcmOutBuf, SampsPerFrame);
            } else {
            	if (Enabled == PcmPkt->SideTone) { // A- enabled?
					int i;
					copyCircToLinear (&pcmAInfo, dtmfWork, SampsPerFrame);
					ADT_GainBlockWrapper (PcmPkt->SideToneDB, dtmfWork, dtmfWork, SampsPerFrame);
					for (i = 0; (i < SampsPerFrame); i ++) {
						pcmOutBuf[ i ] = (_sadd2(pcmOutBuf[ i ], dtmfWork[ i ]) & 0xffff);
					}
				}
               toTDMCirc (chan, BDevice, pcmOutBuf, SampsPerFrame, &PcmPkt->rateCvt);
            }
         }  
      } else if (chan->channelType == conferenceMultiRate) {
         if (PcmPkt->EchoCancel == Enabled) {
            logTime (0x400A0C00ul | chan->ChannelId);
            copyLinearToCirc ((ADT_UInt16 *) pcmOutBuf, &PcmPkt->ecFarPtr, SampsPerFrame);
         }

         logTime (0x400A0B00ul | chan->ChannelId);
         copyLinearToCirc ((ADT_UInt16 *) pcmOutBuf, &PktPcm->outbuffer, SampsPerFrame);
         // NOTE: FrameTimeStamps for multirate conferences are updated by packet processing
      } else if (chan->channelType == conferencePacket) {

         if (PcmPkt->EchoCancel == Enabled) {
            logTime (0x400A0C00ul | chan->ChannelId);
            copyLinearToCirc ((ADT_UInt16 *) pcmOutBuf, &PcmPkt->ecFarPtr, SampsPerFrame);
         }
         ProcessVadToneEncode (chan, pcmOutBuf, pcmOutBuf, scratch, SampsPerFrame);
         PcmPkt->FrameTimeStamp += Cnfr->FrameSize;
      }
   }


   //---------------------------------------------------------------------------
   // Stage 4: Composite Channel Post processing (copy output to PCM or packet)
   //---------------------------------------------------------------------------

   compPcm = (ADT_PCM16 *) scratch;             // Use scratch for channel's pcm buffer
   for (chan = Cnfr->CompositeList; chan != NULL;  chan = chan->nextPcmToPkt) {
      chan->FrameCount++;

      while ((chan != NULL) && (AlgorithmControl (&chan, CnfrComposites) == ALG_CTRL_DEQUEUE_CMD));
      if (chan == NULL) break;

      //
      // Determine count of samples at channel's processing rate
      if (TDMRate < chan->ProcessRate)
         SampsPerFrame = Cnfr->FrameSize * (chan->ProcessRate / TDMRate);
      else
         SampsPerFrame = Cnfr->FrameSize / (TDMRate / chan->ProcessRate);
      AppErr ("Buffer size error", (workI8 / 2) < SampsPerFrame);

      //
      //---  Perform encoding and outbound transfers for composites
      //
      PktPcm = &(chan->pktToPcm);
      PcmPkt = &(chan->pcmToPkt);

      // -  Copy unmodified composite signal (compositeOut) into channel's pcm work buffer (compPcm)
      i16cpy (compPcm, compositeOut, SampsPerFrame);
      if (PktPcm->playA.state & PlayBack)
         voicePlayback (&PktPcm->playA, compPcm, scratch2, SampsPerFrame, ADevice, chan->ChannelId);

      // Copy composite output into channel's circular PCM output buffer.
      if (PktPcm->OutSerialPortId != SerialPortNull) {
         toTDMCirc (chan, BDevice, compPcm, SampsPerFrame, &PcmPkt->rateCvt);
      } else if (chan->pcmToPkt.Coding != NullCodec) {
         ProcessVadToneEncode (chan, compPcm, compPcm, scratch2, SampsPerFrame);
      }
      PktPcm->FrameTimeStamp += Cnfr->FrameSize;
      PcmPkt->FrameTimeStamp += Cnfr->FrameSize;
   }
   
}
