extern packLSB2(unsigned char *pU8, unsigned short *pIn, unsigned short num);
extern unpackLSB2(unsigned char *pU8, unsigned short *pIn, unsigned short num);
extern packLSB3(unsigned char *pU8, unsigned short *pIn, unsigned short num);
extern unpackLSB3(unsigned char *pU8, unsigned short *pIn, unsigned short num);
extern packLSB4(unsigned char *pU8, unsigned short *pIn, unsigned short num);
extern unpackLSB4(unsigned char *pU8, unsigned short *pIn, unsigned short num);
extern packLSB5(unsigned char *pU8, unsigned short *pIn, unsigned short num);
extern unpackLSB5(unsigned char *pU8, unsigned short *pIn, unsigned short num);


packLSB7(unsigned char *pU8, unsigned short *pIn, unsigned short num)
{
int loopCount, inCount, i;

    loopCount = num/8;
    for (i=0; i<loopCount; i++)
    {
        // seven output bytes per 8 input values
        *pU8 = (*pIn++ & 0x7F);
        *pU8 |= (*pIn << 7);
        pU8++;

        *pU8 = (*pIn++ & 0x7F) >> 1;
        *pU8 |= (*pIn << 6);
        pU8++;

        *pU8 = (*pIn++ & 0x7F) >> 2;
        *pU8 |= (*pIn << 5);
        pU8++;

        *pU8 = (*pIn++ & 0x7F) >> 3;
        *pU8 |= (*pIn << 4);
        pU8++;

        *pU8 = (*pIn++ & 0x7F) >> 4;
        *pU8 |= (*pIn << 3);
        pU8++;

        *pU8 = (*pIn++ & 0x7F) >> 5;
        *pU8 |= (*pIn << 2);
        pU8++;

        *pU8 = (*pIn++ & 0x7F) >> 6;
        *pU8 |= (*pIn++ << 1);
        pU8++;
    }

    // remainder loop
    if (inCount = num % 8)
    {
        for (i=0; i<1; i++)
        {
            *pU8 = (*pIn++ & 0x7F);
            if (--inCount == 0)
                break;
            *pU8 |= (*pIn << 7);
            pU8++;

            *pU8 = (*pIn++ & 0x7F) >> 1;
            if (--inCount == 0)
                break;
            *pU8 |= (*pIn << 6);
            pU8++;

            *pU8 = (*pIn++ & 0x7F) >> 2;
            if (--inCount == 0)
                break;
            *pU8 |= (*pIn << 5);
            pU8++;

            *pU8 = (*pIn++ & 0x7F) >> 3;
            if (--inCount == 0)
                break;
            *pU8 |= (*pIn << 4);
            pU8++;

            *pU8 = (*pIn++ & 0x7F) >> 4;
            if (--inCount == 0)
                break;
            *pU8 |= (*pIn << 3);
            pU8++;

            *pU8 = (*pIn++ & 0x7F) >> 5;
            if (--inCount == 0)
                break;
            *pU8 |= (*pIn << 2);
            pU8++;

            *pU8 = (*pIn++ & 0x7F) >> 6;
        }
    } // if remainder
    return;
} 
unpackLSB7(unsigned char *pU8, unsigned short *pOut, unsigned short num)
{
int loopCount, outCount, i;

    loopCount = num/8;
    for (i=0; i<loopCount; i++)
    {
        // eight output values per 7 input bytes
        *pOut++ = *pU8 & 0x7F;              

        *pOut = *pU8++ >> 7;                
        *pOut |= ((*pU8 & 0x3F) << 1);
        pOut++;

        *pOut = *pU8++ >> 6;                
        *pOut |= (*pU8 & 0x1F) << 2;
        pOut++;

        *pOut = *pU8++ >> 5;                
        *pOut |= ((*pU8 &0x0F) << 3);
        pOut++;

        *pOut = *pU8++ >> 4;                
        *pOut |= ((*pU8 & 0x07) << 4);
        pOut++;

        *pOut = *pU8++ >> 3;                
        *pOut |= ((*pU8 & 0x03) << 5);
        pOut++;

        *pOut = *pU8++ >> 2;               
        *pOut |= ((*pU8 & 0x01) << 6);
        pOut++;

        *pOut++ = *pU8++ >> 1;             
    }

    // remainder loop
    if (outCount = num % 8)
    {
        for (i=0; i<1; i++)
        {
            *pOut++ = *pU8 & 0x7F;              
            if (--outCount == 0)
                break;

            *pOut = *pU8++ >> 7;                
            *pOut |= ((*pU8 & 0x3F) << 1);
            if (--outCount == 0)
                break;
            pOut++;

            *pOut = *pU8++ >> 6;                
            *pOut |= (*pU8 & 0x1F) << 2;
            if (--outCount == 0)
                break;
            pOut++;

            *pOut = *pU8++ >> 5;                
            *pOut |= ((*pU8 &0x0F) << 3);
            if (--outCount == 0)
                break;
            pOut++;

            *pOut = *pU8++ >> 4;                
            *pOut |= ((*pU8 & 0x07) << 4);
            if (--outCount == 0)
                break;
            pOut++;

            *pOut = *pU8++ >> 3;                
            *pOut |= ((*pU8 & 0x03) << 5);
            if (--outCount == 0)
                break;
            pOut++;

            *pOut = *pU8++ >> 2;               
            *pOut |= ((*pU8 & 0x01) << 6);
        }
    } // if remainder
}

packLSB6(unsigned char *pU8, unsigned short *pIn, unsigned short num)
{
int loopCount, inCount, i;

    loopCount = num/4;
    for (i=0; i<loopCount; i++)
    {
        // 3 output bytes per 4 input values
        *pU8 = *pIn++ & 0x3F;                  

        *pU8 |= ((*pIn & 0x03) << 6);             
        pU8++;
        *pU8 = (*pIn++ >> 2) & 0x0F;

        *pU8 |= ((*pIn & 0x0F) << 4);     
        pU8++;
        *pU8 = (*pIn++ >> 4) & 0x03;

        *pU8 |= ((*pIn++ & 0x3F) << 2);     
        pU8++;
    }
    
    // remainder loop
    if (inCount = num % 4)
    {
        for (i=0; i<1; i++)
        {
            *pU8 = *pIn++ & 0x3F;                  
            if (--inCount == 0)
                break;
                                                      
            *pU8 |= ((*pIn & 0x03) << 6);             
            pU8++;

            *pU8 = (*pIn++ >> 2) & 0x0F;
            if (--inCount == 0)
                break;
            *pU8 |= ((*pIn & 0x0F) << 4);     
            pU8++;
        
            *pU8 = (*pIn++ >> 4) & 0x03;
        }
    }
}
unpackLSB6(unsigned char *pU8, unsigned short *pOut, unsigned short num)
{
int loopCount, outCount, i;

    loopCount = num/4;
    for (i=0; i<loopCount; i++)
    {
        // 4 output values per 3 input bytes
        *pOut++ = *pU8 & 0x3F;
    
        *pOut = *pU8++ >> 6;
        *pOut |= ((*pU8 & 0x0F) << 2);
        pOut++;

        *pOut = *pU8++ >> 4;
        *pOut |= ((*pU8 & 0x03) << 4);
        pOut++;

        *pOut++ = *pU8++ >> 2;
    }

    // remainder loop
    if (outCount = num % 4)
    {
        for (i=0; i<1; i++)
        {
            *pOut++ = *pU8 & 0x3F;
            if (--outCount == 0)
                break;
    
            *pOut = *pU8++ >> 6;
            *pOut |= ((*pU8 & 0x0F) << 2);
            pOut++;
            if (--outCount == 0)
                break;

            *pOut = *pU8++ >> 4;
            *pOut |= ((*pU8 & 0x03) << 4);
        }
    }
}



void packLSB(unsigned short *inword,   // input buffer: unpacked data
         unsigned short *outword,   // output buffer: packed data
         unsigned short num,        // number of codewords to pack
         unsigned short width)      // width (in bits) of each codeword to pack
{

unsigned char *pU8;
unsigned short  *pIn;
int i;

    pU8 = (unsigned char *)outword;
    pIn = inword;

    switch (width)
    {
        case 8:
            for (i=0; i<num; i++)
                *pU8++ = *pIn++;
            break;

        case 7:  packLSB7(pU8, pIn, num);          break;
        case 6:  packLSB6(pU8, pIn, num);          break;
        case 5:  packLSB5(pU8, pIn, num);          break;
        case 4:  packLSB4(pU8, pIn, num);          break;
        case 3:  packLSB3(pU8, pIn, num);          break;
        case 2:  packLSB2(pU8, pIn, num);          break;
        
        default:
            break;
    };
}
void unpackLSB(unsigned short *inword,   // input buffer: packed data
         unsigned short *outword,   // output buffer: unpacked data
         unsigned short num,        // number of codewords to unpack
         unsigned short width)      // width (in bits) of each codeword to unpack
{

unsigned char *pU8;
unsigned short  *pOut;
int i;

    pU8 = (unsigned char *)inword;
    pOut = outword;


    switch (width)
    {
        case 8:
            for (i=0; i<num; i++)
                *pOut++ = *pU8++;
            break;

        case 7:    unpackLSB7(pU8, pOut, num);            break;
        case 6:    unpackLSB6(pU8, pOut, num);            break;
        case 5:    unpackLSB5(pU8, pOut, num);            break;
        case 4:    unpackLSB4(pU8, pOut, num);            break;
        case 3:    unpackLSB3(pU8, pOut, num);            break;
        case 2:    unpackLSB2(pU8, pOut, num);            break;

        default:
            break;
    };
}
