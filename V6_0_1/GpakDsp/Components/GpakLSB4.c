packLSB4(unsigned char *pU8, unsigned short *pIn, unsigned short num)
{
int i;

    // one output byte per 2 input values
    for (i=0; i<num/2; i++)
    {
        *pU8 = *pIn++ & 0x0f;
        *pU8 |= ((*pIn++ & 0x0f)<<4);
        pU8++;
    }
    return;
}
unpackLSB4(unsigned char *pU8, unsigned short *pOut, unsigned short num)
{
int loopCount, i;

    // 2 output values per input byte
    loopCount = num/2;

    for (i=0; i<loopCount; i++)
    {
        *pOut++ = *pU8 & 0x0f;
        *pOut++ = *pU8++ >> 4;
    }
    return;
}    
