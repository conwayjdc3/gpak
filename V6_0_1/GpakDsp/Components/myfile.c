#include "GpakDefs.h"
#include "sysconfig.h"
#include "GpakExts.h"
#include "GpakHpi.h"

void jdc_func() { }
void nullCID (cidInfo_t *pCid) {
    memset(pCid, 0, sizeof(cidInfo_t));
    pCid->RxCIDIndex = VOID_INDEX;
    pCid->TxCIDIndex = VOID_INDEX;
}