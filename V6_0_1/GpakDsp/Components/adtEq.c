#include "adt_typedef.h"
#include "adtEq.h"
#include <gsm.h>	/* Use TI's header file */

#pragma CODE_SECTION (eqInit, "SLOW_PROG_SECT")
void eqInit(EqChannel_t *pChannel, EqParams_t *pParams)
{
	ADT_Int16 i;

	pChannel->coefLength = pParams->coefLength;
	pChannel->pCoef = pParams->pCoef;
	pChannel->pState = pParams->pState;
	for (i = 0; i < (pParams->coefLength + pParams->frameSize); i++)
		pChannel->pState[i] = 0;
}

#pragma CODE_SECTION (eqApply, "SLOW_PROG_SECT")
void eqApply(EqChannel_t *pChannel, ADT_Int16 *pIn, ADT_Int16 *pOut, ADT_Int16 NSamples)
{
	ADT_Int16 i,j;
	ADT_Int32 Sum;

	//Update the state
	for (i = 0; i < pChannel->coefLength; i++)
	{
		pChannel->pState[i] = pChannel->pState[i + NSamples];
	}
	for (i = 0; i < NSamples; i++)
	{
		pChannel->pState[i+pChannel->coefLength] = pIn[i];
	}
	for (i = 0; i < NSamples; i++)
	{
		Sum = 0;
		for (j = 0; j < pChannel->coefLength; j++)
			Sum = L_mac(Sum, pChannel->pState[i+j], pChannel->pCoef[j]);
		Sum = L_add(Sum, 1L << 15);
		Sum >>= 16;
		pOut[i] = (ADT_Int16) Sum;
	}
}

