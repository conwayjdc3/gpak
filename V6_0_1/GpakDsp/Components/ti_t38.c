//
// G.PAK module for controlling TI's T.38 FAX
//
// This module supports transitioning between voice codecs,
// modem (g.711) and t.38 fax relay modes.
//
// Current mode    Event            New mode
//   codec         CNG              modem
//                 CED              modem
//                 V.21             t.38
//                 t38 pkt          t.38

//   modem         timeout          codec
//                 signal loss      codec
//                 V.21             t.38
//                 t38 pkt          t.38

//   t.38         cleardown         cleardown
//                voice pkt         cleardown
//                CED               modem
//                timeout           codec

//  cleardown     timeout           codec

//  in-bound t.38 packets are tossed during cleardown interval

//{  Externals and pre-processor defines
#include <stdio.h>
#include "GpakDefs.h"
#include "GpakErrs.h"
#include "GpakExts.h"

//#include "ti_t38_user_v0102.h"

#define MODEM_DETECT_MS      125   // Milliseconds CED must be active before G711
#define MODEM_TIMEOUT_MS   15000   // Modem active keep alive
#define FAX_TIMEOUT_MS     60000   // FAX active keep alive
#define FAX_TEARDOWN_MS    10000   // Time between cleardown and actual shutdown

//  Channel flags for FAX signaling
#define CF_FAX_TONE_DETECT      0x00800   // Set when fax relay wants notification of FAX tones (CED/CNG)
#define CF_MODEM_ACTIVE         0x01000   // Set while modem active
#define CF_FAX_ACTIVE           0x02000   // Set while fax is active
#define CF_NEW_FAX              0x04000   // For indicating new fax on captures
#define CF_FAX_TEARDOWN_ACTIVE  0x08000   // Set while fax is in teardown mode
#define CF_FAX_INSTANCES_OPEN   0x10000   // FAX instance
#define CF_ALL_FAX_FLAGS       (CF_MODEM_ACTIVE | CF_FAX_ACTIVE | CF_FAX_TEARDOWN_ACTIVE |  \
                                CF_FAX_TONE_DETECT | CF_NEW_FAX)

#define faxDisabled  disabled

#define EVT_ERRORS              0x0010
#define EVT_TRANSITIONS         0x0020
#define EVT_DATA                0x0040
#define EVT_FUNCTIONS           0x0080
#define EVT_PCM_CAPTURE         0x0100
#define EVT_PCM_CAPTURE_ACTIVE  0x0200
#define EVT_TEXT_CAPTURE        0x0400
#define EVT_TEXT_CAPTURE_ACTIVE 0x0800
#define EVT_FIU_CAPTURE         0x1000
#define EVT_FIU_CAPTURE_ACTIVE  0x2000
#define EVT_FM_CAPTURE          0x4000
#define EVT_FM_CAPTURE_ACTIVE   0x8000

// Set FAX capture flags to default values
#define EVT_CAPTURE     (EVT_PCM_CAPTURE | EVT_TEXT_CAPTURE | EVT_FM_CAPTURE | EVT_FIU_CAPTURE)
#define EVT_PRE_START   (EVT_DATA | EVT_FUNCTIONS)
typedef enum t38_trace {

   // Errors
   //   0/5                       1/6                    2/7                  3/8                   4/9
   EVT_HOST_FULL=EVT_ERRORS,   EVT_RLY_FULL,          EVT_PKT_OUT_ERR,   

   // Transitions 
   EVT_CED=EVT_TRANSITIONS,    EVT_CNG,               EVT_V21,                EVT_FAX_START,         EVT_FAX_END,
   EVT_MODEM_DETECT,           EVT_MODEM_ACTIVE,      EVT_MODEM_END,          EVT_DETECT_TO,         EVT_FAX_TO,
   EVT_XFER_TO_VOICE,

   // Data transfers
   EVT_PCM_IN=EVT_DATA,        EVT_PCM_OUT,           EVT_PKT_IN,             EVT_PKT_OUT,

   // Function calls
   EVT_FUNC=EVT_FUNCTIONS,     EVT_FAX_INIT_FIN,      EVT_FAX_INIT_FOUT,      EVT_PKT_FROM_RLY_FIN,  EVT_PKT_FROM_RLY_FOUT,
   EVT_PKT_TO_RLY_FIN,         EVT_PKT_TO_RLY_FOUT
   
   // CAPTURES
} t38_trace;

static inline void modemStart (chanInfo_t *pChan);
static inline void modemEnd (chanInfo_t *pChan);
static inline void faxStart (chanInfo_t *pChan);
static inline void faxIdle (chanInfo_t *pChan);
static inline void faxEnd (chanInfo_t *pChan);

void captureFaxOutput (chanInfo_t *pChan, void *data, int FrameSize);
void T38_Check_Timeout (chanInfo_t *pChan, int SamplesPerFrame);
void stopFaxCapture  (int condition);

extern int adtToTITone [];

#ifndef _DEBUG
   #define ModemTraceEnable 
   #define T38TraceEnable 
   #define T38TraceDisable 
   #define T38_TRACE(chanID, code) 
   #define captureFaxInput(chan,data,frameSize)
   #define AddToBufLog(str,chan,value)
	#define AddPktToBufLog(str,chan,value, pkt, len)
   #define startFaxCapture(cond,flags)
#else
#include <string.h>
#undef CaptureText
#pragma DATA_SECTION (textLog, "SLOW_DATA_SECT:logging")
#pragma DATA_SECTION (startCapBuff, "SLOW_DATA_SECT:logging")
#pragma DATA_SECTION (capBuffI8, "SLOW_DATA_SECT:logging")
struct textLog {
   int logLen;
   char *capBuff;
} textLog = { 0, (void *) NULL };
volatile char *startCapBuff = NULL;
volatile int capBuffI8 = 0;

#define FIXED_LEN 64
char fixedStr[FIXED_LEN];
static void CaptureText (char *s) {
   int len; 
   if(textLog.capBuff == NULL) return;
   len = strlen (s); 
	memset(fixedStr, 0, FIXED_LEN);
	if(len < FIXED_LEN) {
		memcpy(fixedStr, s, len);
	} else {
		memcpy(fixedStr, s, (FIXED_LEN-1));
	}
	len = FIXED_LEN;
   if (capBuffI8 < textLog.logLen + len) return;   //if (0x400000 < textLog.logLen) return;

   textLog.logLen += len;

   memcpy (textLog.capBuff, fixedStr, len-1);
   textLog.capBuff+=len;
   *textLog.capBuff = 0;

}
static int TextCaptureI8 () {
   return textLog.logLen;
}

static void *TextLogReset () {
   void *end;

   end = (void *)startCapBuff;
   textLog.capBuff = (void *)startCapBuff;
   textLog.logLen = 0;

   //memset (textLog.capBuff, ' ', capBuffI8);
   return end;
}

   static char *ToneStr [] = { toneStrMapping };
   #define TRACE_LEN 1024
#pragma DATA_SECTION (t38Trace, "SLOW_DATA_SECT:logging")
#pragma DATA_SECTION (t38, "SLOW_DATA_SECT:logging")

   struct trace {
      ADT_UInt16 id;
      ADT_UInt16 event;
      ADT_UInt16 time;
   } t38Trace[TRACE_LEN];
   
   struct t38_Debug {
      ADT_UInt32 TraceEnable;
      int TraceIdx;
      int StartCnt;
      ADT_UInt32 StartMs;
   } t38 = 
   { (EVT_PCM_CAPTURE_ACTIVE | EVT_TRANSITIONS | EVT_MODEM_ACTIVE | EVT_MODEM_END | EVT_TEXT_CAPTURE_ACTIVE | EVT_CAPTURE |EVT_FIU_CAPTURE_ACTIVE | EVT_FM_CAPTURE_ACTIVE), 0, 0, 0
   };

   #define ModemTraceEnable if (t38.StartCnt == 0) t38.TraceIdx = 0; t38.TraceEnable &= ~(EVT_PRE_START);  t38.StartCnt=1;
   #define T38TraceEnable   t38.TraceEnable &= ~(EVT_PRE_START); 
   #define T38TraceDisable  t38.TraceEnable &= ~(EVT_PRE_START);

   #define T38_TRACE(chanID, code) \
      if ((t38.TraceIdx < TRACE_LEN) && (t38.TraceEnable & code)) { \
          t38Trace[t38.TraceIdx].id   = chanID;                    \
          t38Trace[t38.TraceIdx].event = code;                     \
          t38Trace[t38.TraceIdx].time = ApiBlock.DmaSwiCnt;        \
          t38.TraceIdx = (t38.TraceIdx + 1) & 0xff;               \
          t38Trace[t38.TraceIdx].event = 0xdead; }

   static void captureFaxInput (chanInfo_t *pChan, void *data, int FrameSize);
   static void AddToBufLog (char *string, chanInfo_t *pChan, int data);
   static void AddPktToBufLog (char *string, chanInfo_t *pChan, int data, void *pkt, int len);
   void startFaxCapture (int condition, int traceFlags);
#endif

#pragma DATA_SECTION (t38Inited, "SHARED_DATA_SECT")
far ADT_Bool t38Inited = FALSE;
#pragma DATA_SECTION (faxActiveCnt, "SHARED_DATA_SECT")
far int faxActiveCnt = 0;

static void OpenRelayInst  (chanInfo_t *pChan);
static void CloseRelayInst (chanInfo_t *pChan, int *TxPkts,int *RxPkts);

static FaxDetectParams_t detectParams = {
   15,                    // Max frequency deviation (.1 %)
  -20,                    // Minimum level dB
  MODEM_DETECT_MS * 8     // Min duration (samples)
};

//}

typedef enum {
  xferpkt_TYPE_NULL=-1,      /**< Don't care */
  xferpkt_TYPE_AAL1=17,      /**< AAL1 packets */
  xferpkt_TYPE_AAL2=18,      /**< AAL2 packets */
  xferpkt_TYPE_FRF11=-2,     /**< FRF11 packets */
  xferpkt_TYPE_S3=-3,        /**< S3 packets */
  xferpkt_TYPE_XPT=-4,       /**< XPT packets */
  xferpkt_TYPE_RTP=16,       /**< RTP packets */
  xferpkt_TYPE_RTCP=-5,      /**< RTCP packets */
  xferpkt_TYPE_HDLC=9,       /**< HDLC packets */
  xferpkt_TYPE_ANNOUNCE=10,  /**< Announce packets */
  xferpkt_TYPE_FAX=16,       /**< FAX packets */
  xferpkt_TYPE_CLEARDATA=16, /**< Digital Data packets */
  xferpkt_TYPE_ALIVE=11      /**< keep alive packets */
} xferpktType_t;
typedef struct {
  xferpktType_t type;       /**< Defines the packet type being transported (see @ref xferpktType_t 
                                 for the list of valid packet types) */
  ADT_UInt16         srcPort;    /**< Defines the source port where the packet originated from */      
  ADT_Int16          npkts;      /**< Defines the number transmission segments being trasnported */
  ADT_Int16          *pktSize;   /**< Pointer to segment size array. */
  void          **pktIn;    /**< Pointer to segment entry array */
  void          *suppInfo;  /**< Pointer to optional supplemental data 
                             *   may be uninitialized between modules that don抰 need it;
                             *   must be initialized for modules that need it.
                             */


} xferpktInfo_t;

//====================================================
//{ Modem switch over management
//===================================================================================
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// modemStart - Switch to G711
static inline void modemStart (chanInfo_t *pChan) {

   startFaxCapture (faxActiveCnt == 0, t38.TraceEnable & EVT_CAPTURE);

   if (pChan->Flags & (CF_FAX_ACTIVE | CF_MODEM_ACTIVE)) return;
 
   ModemTraceEnable;

   AddToBufLog ("MODEM ACTIVE", pChan, 0);

   //pChan->pcmToPkt.Coding = PCMU_64;
   pChan->Flags |= CF_MODEM_ACTIVE;
   T38_TRACE (pChan->ChannelId, EVT_MODEM_ACTIVE);
   pChan->fax.msBeforeTimeout = MODEM_TIMEOUT_MS;
   
   // trigger fax, looked like TI T38 pass the Tone packet in the T.38 packets
   faxStart(pChan);
   return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// modemEnd - Switch back to vocoder after signal loss
static inline void modemEnd (chanInfo_t *pChan) {
   int       TxPkts, RxPkts;

   if (!(pChan->Flags & CF_MODEM_ACTIVE)) return;
 
   CloseRelayInst (pChan, &TxPkts, &RxPkts);

   AddToBufLog ("MODEM INACTIVE", pChan, 0);
   pChan->pcmToPkt.Coding = pChan->pcmToPkt.PreferredCoding;
   pChan->Flags |= CF_REINIT_ENCODE | CF_REINIT_DECODE;
   pChan->Flags &= ~CF_MODEM_ACTIVE;
   pChan->fax.msBeforeTimeout = 0;


   T38_TRACE (pChan->ChannelId, EVT_MODEM_END);
   T38TraceDisable;
   return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// faxIdle - Idle fax state
static inline void faxIdle (chanInfo_t *pChan) {

   faxInfo_t *faxData = &pChan->fax;

   pChan->Flags &= ~(CF_ALL_FAX_FLAGS);
   pChan->pcmToPkt.Coding = pChan->pcmToPkt.PreferredCoding;
   pChan->fax.t38InProgress = FALSE;//(pChan->fax.faxMode == faxOnly); 
   faxData->faxPktRcvd    = FALSE;
   faxData->msBeforeTimeout = 0;
   return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// faxStart - Switch to T.38
static inline void faxStart (chanInfo_t *pChan) {
   faxInfo_t *faxData;
   TI_T38    *ti_t38;

   if (pChan->Flags & CF_FAX_ACTIVE) return;
 
   T38TraceEnable;
   T38_TRACE (pChan->ChannelId, EVT_FAX_START);

   // Restart capture with current flags
   startFaxCapture (faxActiveCnt == 0, t38.TraceEnable & EVT_CAPTURE);
   AddToBufLog ("FAX ACTIVE", pChan, pChan->ChannelId);
	
   faxData = &pChan->fax;
   ti_t38 = (TI_T38 *) faxData->pFax;

   faxData->t38InProgress = TRUE;
   faxData->faxPktRcvd    = FALSE;
   faxData->msBeforeTimeout = FAX_TIMEOUT_MS;


   pChan->Flags |= (CF_MODEM_ACTIVE | CF_FAX_ACTIVE | CF_NEW_FAX);

   ActivateTIRelayInst (ti_t38->fiuInst, ti_t38->fmInst);
   FLUSH_INST_CACHE (ti_t38->fiuInst, fiuInstI8);
   FLUSH_INST_CACHE (ti_t38->fmInst,  fmInstI8);
   FLUSH_wait();

   faxActiveCnt++;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// faxEnd - Close instances and switch to back to vocoder
static inline void faxEnd (chanInfo_t *pChan) {
   int       TxPkts, RxPkts;
   faxInfo_t *faxData;

   faxData = &pChan->fax;
   if (!(pChan->Flags & CF_FAX_ACTIVE)) return;

   CloseRelayInst (pChan, &TxPkts, &RxPkts);

   faxIdle (pChan);
   pChan->Flags |= CF_REINIT_ENCODE | CF_REINIT_DECODE;

   T38_TRACE (pChan->ChannelId, EVT_FAX_END);
   T38TraceDisable;

   AddToBufLog ("FAX INACTIVE", pChan, 0);
   AddToBufLog ("      RCVD", pChan, RxPkts);
   AddToBufLog ("      SENT", pChan, TxPkts);
   
   faxActiveCnt--;

   stopFaxCapture (faxActiveCnt == 0);

   // Reinitialize t38 instance data
	AddToBufLog ("FAX ReOPEN", pChan, pChan->ChannelId);
   OpenRelayInst (pChan);
   if (faxData->faxMode == faxOnly) {
      //faxInfo_t *faxData;
      //TI_T38    *ti_t38;
      //faxData = &pChan->fax;
      //ti_t38 = (TI_T38 *) faxData->pFax;
 		//ActivateTIRelayInst (ti_t38->fiuInst, ti_t38->fmInst);//
		faxStart (pChan);
      //FLUSH_INST_CACHE (ti_t38->fiuInst, fiuInstI8);
      //FLUSH_INST_CACHE (ti_t38->fmInst,  fmInstI8);
	   //FLUSH_wait();
	   // pChan->fax.t38InProgress = pChan->fax.faxMode == faxOnly;
      AddToBufLog ("FAX ONLY RESTART", pChan, 0);
   }
   return;
}
//}

//====================================================
//{  TI T.38 callback routines
#pragma CODE_SECTION (T38_Get_Time_Ms, "FAX_PROG_SECT")
ADT_UInt32 T38_Get_Time_Ms (void) {
   return ApiBlock.DmaSwiCnt;
}

// T38 events (cleardown, change overs)
#pragma CODE_SECTION (T38_Event, "FAX_PROG_SECT")
void T38_Event (chanInfo_t *pChan, int clearDown, int evtCode) {
   faxInfo_t *faxData = &pChan->fax;

   if (clearDown) {
      AddToBufLog ("CLEAR_DOWN", pChan, 0);
      pChan->Flags &= ~CF_FAX_TONE_DETECT;
      pChan->Flags |= CF_FAX_TEARDOWN_ACTIVE;
      faxData->msBeforeTimeout = FAX_TEARDOWN_MS;
   } else {
      AppErr ("Unknown T38 event %d", evtCode);
   }
   return; 
}

// Turn on tone generation (ToneOnCmd callback)
#pragma CODE_SECTION (T38_Tone_On, "FAX_PROG_SECT")
void T38_Tone_On (chanInfo_t *pChan, int dBm, ADT_UInt32 power, int frequency, int on_duration, int off_duration) {
   TGInfo_t     *genInfo;
   TGParams_1_t *genParams;

   genInfo   = &(pChan->pktToPcm.TGInfo);
   genParams = genInfo->toneGenParmPtr;

   CLEAR_INST_CACHE (genParams, sizeof (TGParams_1_t));
   CLEAR_INST_CACHE (genInfo->toneGenPtr, sizeof (TGInstance_t));

   if      (on_duration == 0)  genParams->ToneType = TgContinuous;
   else if (off_duration == 0) genParams->ToneType = TgBurst;
   else                        genParams->ToneType = TgCadence;

   genParams->NFreqs      = 1;
   genParams->NOnOffs     = 1;
   genParams->Freq[0]     = frequency;
   genParams->Freq[1]     = 0;
   genParams->Freq[2]     = 0;
   genParams->Freq[3]     = 0;
   genParams->Level[0]    = dBm;
   genParams->Level[1]    = 0;
   genParams->Level[2]    = 0;
   genParams->Level[3]    = 0;
   genParams->OnTime[0]   = on_duration;
   genParams->OnTime[1]   = 0;
   genParams->OnTime[2]   = 0;
   genParams->OnTime[3]   = 0;
   genParams->OffTime[0]  = off_duration;
   genParams->OffTime[1]  = 0;
   genParams->OffTime[2]  = 0;
   genParams->OffTime[3]  = 0;

   genInfo->cmd = ToneGenStart;
   genInfo->active = Enabled;
   genInfo->update = Disabled;

   FLUSH_wait ();
   TG_ADT_init_1 (genInfo->toneGenPtr, genParams);

   FLUSH_INST_CACHE (genParams, sizeof (TGParams_1_t));
   FLUSH_wait ();

   //AddToBufLog ("TONE GEN ON", pChan, frequency);
}

// Turn off tone generation (ToneOffCmd callback)
#pragma CODE_SECTION (T38_Tone_Off, "FAX_PROG_SECT")
void T38_Tone_Off (chanInfo_t *pChan) {
   TGInfo_t *genInfo;
   genInfo = &(pChan->pktToPcm.TGInfo);

   genInfo->cmd = ToneGenStop;
   genInfo->update = Disabled;
   genInfo->active = Disabled;

   //AddToBufLog ("TONE GEN OFF", pChan, 0);
}

// Enable/disable notification of T.38 on CED / CNG detection (ToneDetCtrl callback)
#pragma CODE_SECTION (T38_Tone_Detect_Ctrl, "FAX_PROG_SECT")
void T38_Tone_Detect_Ctrl (chanInfo_t *pChan, unsigned int enable) {
   if (enable) pChan->Flags |= CF_FAX_TONE_DETECT;
   else        pChan->Flags &= ~CF_FAX_TONE_DETECT;
   //AddToBufLog ("TONE DETECT RQST", pChan, enable);

}

// Outbound TDM delivery (fmReceiveOut callback) = triggered by each call to fmSendIn
#pragma CODE_SECTION (T38_PCM_Available, "FAX_PROG_SECT")
void T38_PCM_Available (chanInfo_t *pChan, void *pcmOut) {
   TGInfo_t       *genInfo;
   GpakActivation *toneGenActive;

   T38_TRACE (pChan->ChannelId, EVT_PCM_OUT);

   // Generate the tone data for this frame and update active state
   genInfo = &(pChan->pktToPcm.TGInfo);
   toneGenActive = &(genInfo->active);
   if (*toneGenActive) {
      //AddToBufLog ("TONE Gen running", pChan, genInfo->toneGenParmPtr->Freq[0]);
      *toneGenActive = (GpakActivation) TG_ADT_generate (genInfo->toneGenPtr, 80, pcmOut);
   }

   // --- Copy out-bound PCM to the TDM outbuffer
	if(!pChan->fax.t38InProgress) return;
   //if (pChan->Flags & CF_FAX_ACTIVE) {
      captureFaxOutput (pChan, pcmOut, 80);
      swCompress (pChan->VoiceChannel, pChan->pktToPcm.OutSerialPortId, pcmOut, pcmOut, 80, pChan->pktToPcm.OutCompandingMode);
      copyLinearToCirc (pcmOut, &(pChan->pktToPcm.outbuffer), 80);
   //}
}

// Outbound packet delivery (fiuSendOut callback)
#pragma CODE_SECTION (T38_Pkt_To_Net, "FAX_PROG_SECT")
int T38_Pkt_To_Net (chanInfo_t *pChan, void *pktBuff, int pktSize, int type) {

   PktHdr_t   hdr;
   faxInfo_t *faxData;
   ADT_UInt32 tempTimeStamp;
   int wordsFree, pktI16;
   int offsetI8;

   faxData = &pChan->fax;

   //CLEAR_INST_CACHE (pktBuff, pktSize);

   // Retreive timestamp from packet
   //offsetI8 = 2;
   //hdr.PayldType  = netI16 (pktBuff, &offsetI8); // Seq in PayldType for other purposes
   offsetI8 = 4;
   hdr.TimeStamp = netI32 (pktBuff, &offsetI8);

   T38_TRACE (pChan->ChannelId, EVT_PKT_FROM_RLY_FIN);

   if (!(pChan->Flags & CF_FAX_ACTIVE)) {
      // If modem already active but fax not active yet
      // FIU packet -> start fax
      //if (pChan->Flags & CF_MODEM_ACTIVE)
      //   faxStart(pChan);
      //else {
      // Drop outbound t.38 packets if fax is no longer active
         AddToBufLog ("OUT PKT DROPPED. FAX NOT ACTIVE.", pChan, type);
         return 1;
      //}
   }

	// Teardown timeout process, still send the rest of the packet out.
   if (pChan->Flags & CF_FAX_TEARDOWN_ACTIVE) {
      return 0;
   }

   wordsFree = getFreeSpace (pChan->pcmToPkt.outbuffer);

   hdr.ChanId = pChan->ChannelId;
   hdr.PayldClass = PayClassT38;
   hdr.PayldType  = 0;

   // copy packet to the host buffer
   if (faxData->faxTransport == faxRtp) {
      pktSize -= 12;            // Remove TI's RTP header so that we can add our own
      pktBuff = ((ADT_UInt8 *) pktBuff) + 12;
   }

   hdr.PayldBytes = pktSize;
   pktI16 = ((hdr.PayldBytes + 1) / 2) + PKT_HDR_I16LEN;
   if (wordsFree < pktI16) {
      // no space in host buffer - dump packet
      T38_TRACE (pChan->ChannelId, EVT_HOST_FULL);
      AddToBufLog ("OUT PKT DROPPED. BUFF FULL.", pChan, type);
      return 1;
   }

   // Force TI's time stamp on out-bound packets
   tempTimeStamp = pChan->pcmToPkt.FrameTimeStamp;
   pChan->pcmToPkt.FrameTimeStamp = hdr.TimeStamp;
   SendPktToHost (pChan, &hdr, pktBuff);
   pChan->pcmToPkt.FrameTimeStamp = tempTimeStamp;
   T38_TRACE (pChan->ChannelId, EVT_PKT_IN);

   //AddToBufLog ("TxPKT", pChan, hdr.PayldType);
	// Teardown timeout process, still send the rest of the packet out.
   if (!(pChan->Flags & CF_FAX_TEARDOWN_ACTIVE)) {
      faxData->msBeforeTimeout = FAX_TIMEOUT_MS;
	}
   T38_TRACE (pChan->ChannelId, EVT_PKT_FROM_RLY_FOUT);
   return 0;
}

// Intermediary between FM and FIU modules  (fmSendOut intermediary)
#pragma CODE_SECTION (T38_Pass_Through, "FAX_PROG_SECT")
void T38_Pass_Through (chanInfo_t *pChan, void *msg) {
   faxInfo_t *faxData;
   TI_T38    *ti_t38;

   faxData = &pChan->fax;
   ti_t38 = (TI_T38 *) faxData->pFax;

   // FAX Active, pass data on to FIU
   if (pChan->Flags & CF_FAX_ACTIVE) {
		CLEAR_INST_CACHE (ti_t38->fiuInst, fiuInstI8);
      fiuSendIn (ti_t38->fiuInst, msg);
		FLUSH_INST_CACHE (ti_t38->fiuInst, fiuInstI8);
		FLUSH_wait();
      return;
   }

   // FAX Inactive, check for carrier or signal loss to shut down modem operation
   if (t38CarrierLost (msg)) {
      AddToBufLog ("FAX SGNL/CAR LOSS", pChan, 0);
      modemEnd (pChan);
   } else {
      AddToBufLog ("FM to FIU MSG", pChan, 0);
      faxData->msBeforeTimeout = MODEM_TIMEOUT_MS;
   }
   return;
}
//}

//====================================================
//{  G.PAK framework T.38 interface routines

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// OpenRelayInst.  Called initially upon channel setup.
//                 Also called at end of FAX after previous
//                 instance was closed.
#pragma CODE_SECTION (OpenRelayInst, "FAX_PROG_SECT")
static void OpenRelayInst (chanInfo_t *pChan) {
   TI_T38    *ti_t38;
   TGInfo_t  *genInfo;
   char      *failure;
   RtpChanData_t *RTPChan;

   T38_TRACE (pChan->ChannelId, EVT_FAX_INIT_FIN);
   ti_t38 = (TI_T38 *) pChan->fax.pFax;

   CLEAR_INST_CACHE (ti_t38->fiuInst, fiuInstI8);
   CLEAR_INST_CACHE (ti_t38->fmInst,  fmInstI8);
   //FLUSH_wait ();

   //====================================================
   // TI's t.38 initialization
   RTPChan   = RtpChanData[pChan->ChannelId];
   failure = OpenTIRelayInst (ti_t38->fiuInst, ti_t38->fmInst, pChan, RTPChan->Profile.T38);
   AppErr (failure, failure != NULL);
   if (failure != NULL) {
      pChan->fax.faxMode = faxDisabled;
      return;
   }

   //====================================================
   // FAX tones detection initialization
   FDET_ADT_init (&ti_t38->toneInst, &detectParams);
   T38_TRACE (pChan->ChannelId, EVT_FAX_INIT_FOUT);

   FLUSH_INST_CACHE (ti_t38->fiuInst, fiuInstI8);
   FLUSH_INST_CACHE (ti_t38->fmInst,  fmInstI8);

   //====================================================
   // Tone generation initialization
   genInfo = &(pChan->pktToPcm.TGInfo);
   genInfo->cmd = ToneGenStop;
   genInfo->active = Disabled;
   genInfo->update = Disabled;

   pChan->Flags |= CF_FAX_TONE_DETECT | CF_FAX_INSTANCES_OPEN;
   AddToBufLog ("T38 OPENNED", pChan, 0);

   FLUSH_wait ();

   return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// CloseRelayInst. Called at end of FAX and when channel is deallocated
#pragma CODE_SECTION (CloseRelayInst, "FAX_PROG_SECT")
static void CloseRelayInst (chanInfo_t *pChan, int *TxPkts, int *RxPkts) {
   faxInfo_t   *faxData;
   TI_T38      *ti_t38;

   faxData = &pChan->fax;
   if (!(pChan->Flags & CF_FAX_INSTANCES_OPEN)) return;

   ti_t38  = (TI_T38 *) faxData->pFax;


   CloseTIRelayInst (ti_t38->fiuInst, ti_t38->fmInst, TxPkts, RxPkts);

   CLEAR_INST_CACHE (ti_t38->fiuInst, fiuInstI8);
   CLEAR_INST_CACHE (ti_t38->fmInst,  fmInstI8);
   //FLUSH_INST_CACHE (ti_t38->fiuInst, fiuInstI8);
   //FLUSH_INST_CACHE (ti_t38->fmInst,  fmInstI8);
   //FLUSH_wait ();

   pChan->Flags &= ~CF_FAX_INSTANCES_OPEN;
   AddToBufLog ("T38 CLOSED", pChan, 0);

}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//  T38_ToneNotificaiton  -  Notify T.38 of detected fax tones
#pragma CODE_SECTION (lcl_ToneNotification, "FAX_PROG_SECT")
static void lcl_ToneNotification (chanInfo_t *pChan, int toneType) {
   faxInfo_t *faxData;
   TI_T38    *ti_t38;

   faxData = &pChan->fax;
   ti_t38 = (TI_T38 *) faxData->pFax;

   // Ignore tones during cleardown interval
   if (pChan->Flags & CF_FAX_TEARDOWN_ACTIVE) {
      return;
   }

   // Ignore tone end
   if (toneType == FDET_NULL) {
      return;
   }

   // V21 - start FAX if necessary
   if (toneType == FDET_V21) {
      T38_TRACE (pChan->ChannelId, EVT_V21);
      if (!(pChan->Flags & CF_FAX_ACTIVE)) {
         AddToBufLog ("DETECTED V21", pChan, toneType);
         //faxStart (pChan);
      }
      return;
   }

   if (pChan->Flags & CF_FAX_ACTIVE) {
      // FAX Active.  Restart FAX on CED
      if ((toneType == FDET_CED) && (faxData->faxMode == faxVoice) && (faxData->faxPktRcvd)) {
         AddToBufLog ("CED RESTART", pChan, toneType);
         // Temporarily remove the switch-out, Huafeng June26, 2012
	      //faxEnd (pChan);
         modemStart (pChan);
      }
   } else if (pChan->Flags & CF_MODEM_ACTIVE) {
      // Modem Active.  Refresh timeout
      faxData->msBeforeTimeout = MODEM_TIMEOUT_MS;
   } else {
      // Modem inactive.  Mark active
      modemStart (pChan);
		AddToBufLog ("ToneKickModem", pChan, toneType);
   }

   // Report
   if (toneType == FDET_CED) {
      AddToBufLog ("FDET_CED TONE", pChan, toneType);
      T38_TRACE (pChan->ChannelId, EVT_CED);
   } else if (toneType == FDET_CNG) {
      AddToBufLog ("FDET_CNG TONE", pChan, toneType);
      T38_TRACE (pChan->ChannelId, EVT_CNG);
   } else if (toneType != FDET_NULL) {
      AddToBufLog ("UNKNOWN TONE", pChan, toneType);
      return;
   }

   AddToBufLog (ToneStr[toneType], pChan, pChan->Flags & CF_FAX_TONE_DETECT);
   if (pChan->Flags & CF_FAX_TONE_DETECT) {
      fmToneDetect (ti_t38->fmInst, adtToTITone[toneType], 0, 0);  // Notify T.38 of tones  ??
   }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// AllocFaxRelay -  Allocate fax relay instance from pool
#pragma CODE_SECTION (AllocFaxRelay, "SLOW_PROG_SECT")
void AllocFaxRelay (chanInfo_t *pChan) {
   char *Error;
   int i;
   TI_T38 *ti_t38;

   if ((pChan->fax.faxMode == faxDisabled) || (sysConfig.numFaxChans == 0))
      return;

	//startFaxCapture (faxActiveCnt == 0, t38.TraceEnable & EVT_CAPTURE);
   AddToBufLog ("FAX ALLOC", pChan, pChan->ChannelId);

   if (!t38Inited) {
      Error = InitTIRelayStructures (sysConfig.numFaxChans);
      AppErr (Error, Error != NULL);
      t38Inited = TRUE;
      (void) Error;   // Avoid compiler warning
   }

   // get the index of the next available fax instance
   for (i=0; i<sysConfig.numFaxChans; i++) {
      if (faxChanInUse[i] != 0) continue;
      pChan->fax.faxIndex = i;
      faxChanInUse[i] = 1;
      numFaxChansUsed++;
      break;
   }

   ti_t38 = &TI_Fax[pChan->fax.faxIndex];
   ti_t38->channelId = pChan->ChannelId;
   pChan->fax.pFax = (void *) ti_t38;

   // initialize local state flagw
   pChan->Flags &= ~(CF_ALL_FAX_FLAGS | CF_FAX_INSTANCES_OPEN);
   faxIdle (pChan);

   // initialize the fax relay instances
   OpenRelayInst (pChan);
   if (pChan->fax.faxMode == faxOnly) {
      //faxInfo_t *faxData;
      //TI_T38    *ti_t38;
      //faxData = &pChan->fax;
      //ti_t38 = (TI_T38 *) faxData->pFax;
 		//ActivateTIRelayInst (ti_t38->fiuInst, ti_t38->fmInst);//faxStart (pChan);
      //FLUSH_INST_CACHE (ti_t38->fiuInst, fiuInstI8);
      //FLUSH_INST_CACHE (ti_t38->fmInst,  fmInstI8);
	   //FLUSH_wait ();

      faxStart (pChan);
	   //pChan->fax.t38InProgress = (pChan->fax.faxMode == faxOnly);
      AddToBufLog ("FAX ONLY", pChan, 0);
   }

}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// invalidateFaxRelay -  Restore fax relay instance to pool
#pragma CODE_SECTION (DeallocFaxRelay, "SLOW_PROG_SECT")
void invalidateFaxRelay (chanInfo_t *pChan) {
   int TxPkts, RxPkts;

   if (pChan->fax.faxMode == faxDisabled) return;

   CloseRelayInst (pChan, &TxPkts, &RxPkts);
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// DeallocFaxRelay -  Restore fax relay instance to pool
#pragma CODE_SECTION (DeallocFaxRelay, "SLOW_PROG_SECT")
void DeallocFaxRelay (chanInfo_t *pChan) {
   int TxPkts, RxPkts;

   if (pChan->fax.faxMode == faxDisabled) return;

   if (pChan->fax.t38InProgress) faxActiveCnt--;

   CloseRelayInst (pChan, &TxPkts, &RxPkts);
   faxChanInUse[pChan->fax.faxIndex] = 0;
   numFaxChansUsed--;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//  T38_Add_Packet  -  Add fax packet received from host into fax relay buffers
//
//  Return TRUE to indicate packet should be removed from packet buffer
#pragma CODE_SECTION (T38_Add_Packet, "FAX_PROG_SECT")
int T38_Add_Packet (chanInfo_t *pChan, void *pyld, int PayldI8, ADT_UInt32 timeStamp) {

   int        status;
   faxInfo_t *faxData;
   TI_T38    *ti_t38;

   faxData = &pChan->fax;
   ti_t38  = (TI_T38 *) faxData->pFax;

   // Toss packet if t.38 is faxDisabled or in teardown state
   if ((faxData->faxMode == faxDisabled) || (pChan->Flags & CF_FAX_TEARDOWN_ACTIVE)) {
      return TRUE;
   }

   //if (!(pChan->Flags & CF_FAX_ACTIVE)) {
   if (!faxData->t38InProgress) {
      AddToBufLog ("NEW T38 PKT", pChan, 0);
      //faxStart (pChan); // moved to the processFaxRelay
   }
   // Teardown timeout process, still send the rest of the packet out.
   if (!(pChan->Flags & CF_FAX_TEARDOWN_ACTIVE)) {
      faxData->msBeforeTimeout = FAX_TIMEOUT_MS;    // reset time for no packets sent or received
   }
   faxData->faxPktRcvd = TRUE;                   // Indicate fax packet has been received
   //captureFaxInput (pChan, pyld, (PayldI8 + 1) / 2);
   //AddPktToBufLog("RxPKT:", pChan, timeStamp, pyld, PayldI8);
#ifdef _DEBUG
   //seq = netI16 (pyld, &offsetI8);
   //AddToBufLog ("RxPKT", pChan, (PayldI8-12));
#endif

   CLEAR_INST_CACHE (ti_t38->fiuInst, fiuInstI8);
   // Pass in-bound t.38 packets to the relay processor
   T38_TRACE (pChan->ChannelId, EVT_PKT_TO_RLY_FIN);
   status = t38ReceiveIn (ti_t38->fiuInst, pyld, PayldI8);
   T38_TRACE (pChan->ChannelId, EVT_PKT_TO_RLY_FOUT);
   FLUSH_INST_CACHE (ti_t38->fiuInst, fiuInstI8);
   FLUSH_wait();

   if (status == 0) {
      // packet accepted by the relay. 
      T38_TRACE (pChan->ChannelId, EVT_PKT_OUT);
      return TRUE;
   } 

   // packet to dsp error 
   T38_TRACE (pChan->ChannelId, EVT_PKT_OUT_ERR);
   AddToBufLog ("IN PKT DROPPED. RLY REJECT", pChan, status);
   return FALSE;
    
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// processFaxRelay  - Pass inbound pcm to fax/relay, kick off fax/relay processing
//{
// Inputs
//     pChan - Channel data structure pointer
//     pcmInBuff - buffer holding input pcm samples
//     pcmOut - scratch buffer 
//     FrameSize - number of samples in this frame
//
// Outputs
//      nothing
//
// RETURNS
//  nothing
//}
#pragma CODE_SECTION (processFaxRelay, "FAX_PROG_SECT")
void processFaxRelay (chanInfo_t *pChan, ADT_PCM16 *pcmInBuff, ADT_PCM16 *pcmOut, int FrameSize) {
   ADT_PCM16 *pcmIn;
   ADT_Int16  faxTone;
   faxInfo_t *faxData;
   TI_T38    *ti_t38;
   int        Avail;

   faxData = &pChan->fax;
   ti_t38 = (TI_T38 *) faxData->pFax;
   if ((FrameSize % 80) != 0) return;

   // --- Copy in-bound PCM to circular buffer for debug
   captureFaxInput (pChan, pcmInBuff, FrameSize);
   T38_TRACE (pChan->ChannelId, EVT_PCM_IN);

   //CLEAR_INST_CACHE (ti_t38->fiuInst, fiuInstI8);
   //CLEAR_INST_CACHE (ti_t38->fmInst,  fmInstI8);
	
	if ((!pChan->fax.t38InProgress) && (faxData->faxPktRcvd)) {
		faxStart(pChan);
	}
   // Pass inbound PCM packets (80 samples per call) to t38 for processing
   pcmIn = pcmInBuff;
   Avail = FrameSize;
   while (80 <= Avail) {
      // ---  Process in-bound PCM (pcmIn) to generate out-bound PCM (pcmOut)
      //      Note: each call to fmSendIn generates complimentary call to t38PCMAvailable
      fmSendIn (ti_t38->fmInst, pcmIn);
      fiuTask  (ti_t38->fiuInst);
      pcmIn += 80;
      Avail -= 80;
   }

   FLUSH_INST_CACHE (ti_t38->fiuInst, fiuInstI8);
   FLUSH_INST_CACHE (ti_t38->fmInst,  fmInstI8);
   // Detect fax tone transitions and notify the t38 fax relay module of the detected tone
   if (FDET_ADT_detect (&ti_t38->toneInst, pcmInBuff, FrameSize, &faxTone)) {
      lcl_ToneNotification (pChan, faxTone);
   }

   // Check for modem timeout
   if ((!pChan->fax.t38InProgress) && (pChan->Flags & CF_MODEM_ACTIVE)){
      T38_Check_Timeout (pChan, FrameSize);
   }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// T38_Check_Timeout - Turn off fax mode and reestablish voice when no 
//                     T.38 packets are received or transmitted during timeout duration
#pragma CODE_SECTION (T38_Check_Timeout, "FAX_PROG_SECT")
void T38_Check_Timeout (chanInfo_t *pChan, int SamplesPerFrame) {

   int msPerFrame = SamplesPerFrame / sysConfig.samplesPerMs;

   if (msPerFrame < pChan->fax.msBeforeTimeout) {
      pChan->fax.msBeforeTimeout -=  msPerFrame;
   } else {
      if (pChan->Flags & (CF_FAX_ACTIVE | CF_FAX_TEARDOWN_ACTIVE)) {
         // Timeout - Restore codec and remove flags
         AddToBufLog ("FAX TIMEOUT", pChan, 0);
         faxEnd (pChan);
         T38_TRACE (pChan->ChannelId, EVT_FAX_TO);
         T38TraceDisable;
      } else if (pChan->Flags & CF_MODEM_ACTIVE) {
         AddToBufLog ("MODEM TIMEOUT", pChan, 0);
			stopFaxCapture (1);
         //if (pChan->fax.faxMode == faxOnly) return;
         // Timeout - Restore codec and remove flags
         modemEnd (pChan);
         T38_TRACE (pChan->ChannelId, EVT_FAX_TO);
         T38TraceDisable;
      }
   }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//  T38_Restart_Voice - Turn off fax mode and reestablish voice when a
//                      voice packet is received
//{
//  NOTE: Initiating channel will expect to receive voice packets
//        until far end responds with a packet.  
//
//        FAX packets may also be received during the cleardown state.
//
//        Voice packets should be ignored under these circumstances and
//        the channel should remain as faxInProgress
//}
#pragma CODE_SECTION (T38_Restart_Voice, "FAX_PROG_SECT")
void T38_Restart_Voice (chanInfo_t *pChan) {

   if ((pChan->fax.faxMode == faxOnly) || (!pChan->fax.faxPktRcvd) || 
       (pChan->Flags & CF_FAX_TEARDOWN_ACTIVE))
      return;

   AddToBufLog ("VOICE RESTART", pChan, 0);
   T38_TRACE (pChan->ChannelId, EVT_XFER_TO_VOICE);
   pChan->Flags |= CF_FAX_TEARDOWN_ACTIVE;
   pChan->fax.msBeforeTimeout =  FAX_TEARDOWN_MS;
}
//}

//====================================================
//{  T.38 FAX capture and logging routines
// Debug routines

#pragma CODE_SECTION (captureFaxOutput, "SLOW_PROG_SECT")
#pragma CODE_SECTION (T38_Fiu_Capture, "FAX_PROG_SECT")
#pragma CODE_SECTION (T38_Fm_Capture,  "FAX_PROG_SECT")
#pragma CODE_SECTION (startFaxCapture, "SLOW_PROG_SECT")
#pragma CODE_SECTION (stopFaxCapture,  "SLOW_PROG_SECT")


#ifndef _DEBUG
void captureFaxOutput (chanInfo_t *pChan, void *data, int FrameSize) { return; }
void T38_Fiu_Capture (void *id, ADT_UInt16 msgType, ADT_UInt16 msgCode, ADT_UInt16  msgI8, ADT_UInt16 *msgData) { return; }
void T38_Fm_Capture  (void *id, ADT_UInt16 msgType, ADT_UInt16 msgCode, ADT_UInt16  msgI8, ADT_UInt16 *msgData) { return; }

#undef startFaxCapture
void startFaxCapture (int condition, int flags) { return; }
void stopFaxCapture (int condition)  { return; }
#else
//void startFaxCapture (int condition, int flags) { return; }
//void stopFaxCapture (int condition)  { return; }

static char tempBuffer[500];

extern CircBufInfo_t captureA;
extern CircBufInfo_t captureB;
extern CircBufInfo_t captureC;
extern CircBufInfo_t captureD;
//extern void *TextLogReset ();
//extern int   TextCaptureI8 ();

#pragma CODE_SECTION (FaxBufrReset, "SLOW_PROG_SECT")
static void *FaxBufrReset () {
   void *end;

   end = TextLogReset ();
   return end;
}

#pragma CODE_SECTION (indent, "SLOW_PROG_SECT")
static void indent (int id) {
   if (id != 1) return;
   memset (tempBuffer, ' ', 40);
   tempBuffer[40]=0;
   CaptureText (tempBuffer);
}

#pragma CODE_SECTION (AddToBufLog, "SLOW_PROG_SECT")
static void AddToBufLog (char *string, chanInfo_t *pChan, int data) {
   if(pChan->ChannelId > 1) return;
   if (!(t38.TraceEnable & EVT_TEXT_CAPTURE_ACTIVE)) return;

   //indent (pChan->ChannelId);
   sprintf (tempBuffer, "%1d) [%8d] %s %5d\n",  pChan->ChannelId,
              ApiBlock.DmaSwiCnt - t38.StartMs, string, data);
   CaptureText (tempBuffer);
}
/*
#pragma CODE_SECTION (AddPktToBufLog, "SLOW_PROG_SECT")
static void AddPktToBufLog (char *string, chanInfo_t *pChan, int data, void *pkt, int len) {
char tempBuf[40];
char tempBuf2[40];
ADT_UInt8 *pPkt = (ADT_UInt8 *)pkt;
int i;
	if(pChan->ChannelId > 1) return;
   if (!(t38.TraceEnable & EVT_TEXT_CAPTURE_ACTIVE)) return;
	
	if(len > 12) { // removed the RTp header
	   len  -= 12;
		pPkt += 12;
	}
   if(len >8) len=8;
   //indent (pChan->ChannelId);
	strcpy(tempBuf, string);
   for(i=0;i<len;i++) {
	   sprintf(tempBuf2, "%2x",pPkt[i]);
		strcat(tempBuf, tempBuf2);
	}
   sprintf (tempBuffer, "%1d) [%8d] %s %5d\n",  pChan->ChannelId,
              ApiBlock.DmaSwiCnt - t38.StartMs, tempBuf, len);
   CaptureText (tempBuffer);
}
*/
// Capture fax signals in capture circular buffers if pcm capture marked active
#pragma CODE_SECTION (captureFaxInput, "SLOW_PROG_SECT")
static void captureFaxInput (chanInfo_t *pChan, void *data, int FrameSize) {
   CircBufInfo_t *cBuff;
   if (pChan->ChannelId == 0) { 
      cBuff = &captureA;
   } else if (pChan->ChannelId == 1) { 
      cBuff = &captureC;
   } else {
      return;
   }

   if ((t38.StartMs == 0) && (t38.TraceEnable & EVT_PCM_CAPTURE_ACTIVE))
       t38.StartMs = ApiBlock.DmaSwiCnt;

   if (pChan->Flags & CF_NEW_FAX) {
      AddToBufLog ("NEW FAX ACTIVE", pChan, 0);
      pChan->Flags &= ~CF_NEW_FAX;
     
      //memset (data, 0x55, 16);
   }


   if (!(t38.TraceEnable & EVT_PCM_CAPTURE_ACTIVE)) return;
   captureSignal (cBuff, data, FrameSize);
   
   return;
}

#pragma CODE_SECTION (captureFaxOutput, "SLOW_PROG_SECT")
void captureFaxOutput (chanInfo_t *pChan, void *data, int FrameSize) {
   CircBufInfo_t *cBuff;

   if (!(t38.TraceEnable & EVT_PCM_CAPTURE_ACTIVE)) return;

   if (pChan->ChannelId == 0) { 
      cBuff = &captureB;
   } else if (pChan->ChannelId == 1) { 
      cBuff = &captureD;
   } else {
      return;
   }

   captureSignal (cBuff, data, FrameSize);
}

#pragma CODE_SECTION (T38_Fiu_Capture, "FAX_PROG_SECT")
void T38_Fiu_Capture (void *id, ADT_UInt16 msgType, ADT_UInt16 msgCode,
                                ADT_UInt16 msgI8,   ADT_UInt16 *msgData) {
   int fatal;
   if((int) id > 1) return;
   if (!(t38.TraceEnable & EVT_FIU_CAPTURE_ACTIVE)) return;

   //indent ((int) id);
   sprintf (tempBuffer, "%1d) [%8d]", id, ApiBlock.DmaSwiCnt - t38.StartMs);
   fatal = fiuDebugTraceParser (msgType, msgCode, *msgData, &tempBuffer[13]);
   CaptureText (tempBuffer);
   AppErr ("FIU Fatal Error", fatal);

}

#pragma CODE_SECTION (T38_Fm_Capture, "FAX_PROG_SECT")
void T38_Fm_Capture  (void *id, ADT_UInt16 msgType, ADT_UInt16 msgCode,
                                ADT_UInt16 msgI8,   ADT_UInt16 *msgData) {
   int fatal;
   if((int) id > 1) return;
   if (!(t38.TraceEnable & EVT_FM_CAPTURE_ACTIVE)) return;

   //indent ((int) id);
   sprintf (tempBuffer, "%1d) [%8d]", id, ApiBlock.DmaSwiCnt - t38.StartMs);
   fatal = fmDebugTraceParser (msgType, msgCode, *msgData, &tempBuffer[13]);
   CaptureText (tempBuffer);
   AppErr ("FIU Fatal Error", fatal);
}
#if 1
// Enable captures according to enableFlags
// Activate the capture if not yet active and startCondition is true
#pragma CODE_SECTION (startFaxCapture, "SLOW_PROG_SECT")
void startFaxCapture (int startCondition, int enableFlags) {
   int activeFlags;

   // Get current active flags and check if capture is already active
   activeFlags = (t38.TraceEnable & EVT_CAPTURE) << 1; 
   if (!startCondition && ((t38.TraceEnable & activeFlags) == activeFlags)) return;

   // Remove current capture enable and active flags with new flags
   t38.TraceEnable &= ~(EVT_CAPTURE | (EVT_CAPTURE << 1));
   t38.TraceEnable |= (enableFlags | (enableFlags << 1));

   t38.StartMs =    0;
   captureA.PutIndex = 0;
   captureB.PutIndex = 0;
   captureC.PutIndex = 0;
   captureD.PutIndex = 0;
   FaxBufrReset();

   AddToBufLog ("CAPTURE STARTED", chanTable[0], activeFlags);

}

// Disable active captures and send capture complete message to host when
// endCondition is true
#pragma CODE_SECTION (stopFaxCapture, "SLOW_PROG_SECT")
void stopFaxCapture (int endCondition) {
   int activeFlags;

   struct faxCompleteMsg {
      EventFifoHdr_t  header;    
      ADT_Int32 aDataAddr, aDataLenI32;
      ADT_Int32 bDataAddr, bDataLenI32;
      ADT_Int32 textAddr,  textLenI32;
   } msg;

   activeFlags = (t38.TraceEnable & EVT_CAPTURE) << 1;

   if (!endCondition || !activeFlags) return;

   AddToBufLog ("CAPTURE STOPPED", chanTable[0], 0);
   if (faxActiveCnt == 0) t38.TraceEnable &= ~activeFlags;

   CACHE_wbAll ();

   // Send FaxComplete Message (inbound capture)
   msg.header.channelId  = 0;
   msg.header.deviceSide = 0;
   
   msg.header.eventCode = EventFaxComplete;
   msg.header.eventLength =  sizeof (msg) - sizeof (EventFifoHdr_t);

   msg.aDataLenI32  = captureA.PutIndex / 2;
   msg.aDataAddr    = (((ADT_Int32) captureA.pBufrBase) >> 2) << 2;

   msg.bDataLenI32 = captureC.PutIndex / 2;
   msg.bDataAddr    = (((ADT_Int32) captureC.pBufrBase) >> 2) << 2;

   msg.textAddr   = (((ADT_Int32) startCapBuff) >> 2) << 2;
   msg.textLenI32 = TextCaptureI8 () / 4;
   writeEventIntoFifo ((void *) &msg);

   // Send FaxComplete Message (outbound capture)
   msg.header.channelId  = 1;
   msg.header.deviceSide = 0;

   msg.header.eventCode = EventFaxComplete;
   msg.header.eventLength =  sizeof (msg) - sizeof (EventFifoHdr_t);

   msg.aDataLenI32  = captureB.PutIndex / 2;
   msg.aDataAddr    = (((ADT_Int32) captureB.pBufrBase) >> 2) << 2;

   msg.bDataLenI32 = captureD.PutIndex / 2;
   msg.bDataAddr    = (((ADT_Int32) captureD.pBufrBase) >> 2) << 2;

   msg.textAddr   = 0;
   msg.textLenI32 = 0;
   writeEventIntoFifo ((void *) &msg);
   return;
}
#endif
#endif
//}
