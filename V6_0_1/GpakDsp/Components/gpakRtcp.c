#include <stdio.h>
#include "GpakDefs.h"
#include "GpakExts.h"
#include "GpakHpi.h" 
#include <ti/sysbios/hal/SecondsClock.h>
#ifdef RTCP_ENABLED

//#define NTP_DEBUG
//#define RTCP_DEBUG


#ifdef inc_ipStackUser          // compile only if IP stack support exists
#include inc_ipStackUser


#define htonl(a) ((((a) & 0xff000000) >> 24) | (((a) & 0x00ff0000) >> 8) | \
                  (((a) & 0x0000ff00) << 8)  | (((a) & 0x000000ff) << 24) )
#define ntohl(a) htonl(a)

#define HI_16(i32) (((i32) >> 16) & 0xffff)
#define LO_16(i32)  ((i32) & 0xffff)
#define HI_8(i16) (((i16) >> 8) & 0xff)
#define LO_8(i16)  ((i16) & 0xff)
#define Pack16(Hi,Lo)            ((ADT_UInt16) ((Hi)<<8) | ((Lo) & 0x00ff))
#define Pack32(hi,lo)       ((ADT_UInt32) ((((ADT_UInt32) (hi)) << 16) | (((ADT_UInt32) (lo)) & 0xffff)))

// NTP clock time resolution is specified as 1/(2^32) seconds (approximately 0.232 nanoseconds per tick)
// Use the following scale factor to convert a clock with 1 ms resolution to a clock with NTP resoution 
#define NTP_ONE_MSEC (4294967.296)  // the number of ntp-resolution ticks in 1 ms
#define NTP_ONE_NSEC (4.294967296)  // the number of ntp-resolution ticks in 1 nanosec
#define NTP_OFFSET 0x83AA7E80       // convert UNIX (seconds since 1970) epoch to NTP

#define DEBUG(...) printf(__VA_ARGS__)
#define currTimeMS() ApiBlock.DmaSwiCnt
extern ADT_Bool fastRTPMap (int chanID, IPToSess_t **ipSess);
extern ADT_Bool fastRTCPMap(int chanID, IPToSess_t **ipSess);
extern int rtpPortValid (int rtpPort);
extern int addToIPSessionRTCP (IPToSess_t *new, void *RTPSession, ADT_UInt16 ChanId);
extern int addToIPSessionRTCP6 (IPToSess_t *new, void *RTPSession, ADT_UInt16 ChanId);
extern int fastRTPsend(void *rtpBuf, int rtpI8, IPToSess_t *IPMapping);
extern IPToSess_t *IPSessionLookupRTCP (IPN rmtIP, ADT_UInt16 rmtPort, ADT_UInt32 rmtSSRC, ADT_Bool new);
extern IPToSess_t *IPSessionLookupRTCP6 (ADT_UInt8 *rmtIP, ADT_UInt16 rmtPort, ADT_UInt32 rmtSSRC, ADT_Bool new);

extern GpakTestMode RTPLoopBack;
extern rtcpdata_t* const rtcpData[]; 
extern ADT_UInt16 numRtcpChannels;
extern int msgCore;

// Send an RTP packet to a channel
extern void sendRtpPktToChan(
	ADT_UInt16 channelId,		// channel Id
	ADT_UInt8 *pPktData,		// pointer to packet data
	ADT_UInt16 length			// length of packet data (bytes)
	);
#ifdef PRINT_RTCP_NOTIFICATIONS
static void  SR_notify (ADT_UInt16 chanId, ADT_UInt32 SSRC, senderReport_t *SR);
static void  RR_notify (ADT_UInt16 chanId, ADT_UInt32 SSRC, receiverReport_t *RR);
static void  SDES_notify (ADT_UInt16 chanId, ADT_UInt32 SSRC, sdesReport_t *SDES);
static void  BYE_notify (ADT_UInt16 chanId, ADT_UInt32 SSRC, byeReport_t *BYE);
static void  APP_notify (ADT_UInt16 chanId, ADT_UInt32 SSRC, appReport_t *APP);
static void  EXT_notify (ADT_UInt16 chanId, ADT_UInt32 SSRC, appReport_t *APP);
#else
#define SR_notify(a,b,c);   
#define RR_notify(a,b,c);      
#define SDES_notify(a,b,c);    
#define BYE_notify(a,b,c);     
#define APP_notify(a,b,c);     
#define EXT_notify(a,b,c);     
#endif

#define RR_VALID   0x0001
#define SR_VALID   0x0002
#define SDES_VALID 0x0004
#define BYE_VALID  0x0008
#define APP_VALID  0x0010
#define EXT_VALID  0x0020


typedef struct rtcpconfig_t {
   ADT_UInt16      transportOverheadBytes;    // Overhead (in bytes) added to RTCP packets due to transport layer headers
   ADT_UInt16      timeoutMultiplier;         // See RFC 3550; section 6.3.5; M
                                              //         - Multiplier to allocate for timeout 
                                              //         - Default is 5
   ADT_UInt32      minRTCPPktPeriodMs;        // See RFC 3550; section 6.3.1; Tmin
                                              //         - Minimum period (milliseconds) between RTCP transmissions
                                              //         - Default is 5000
   ADT_UInt32      rtcpBandwidthBytesPerSec;  // See RFC 3550; section 6.3; rtcp_bw * 'session bandwidth'
                                              //         - Bandwidth (bytes per second) to allocate to RTCP traffic
   ADT_Float32     senderBandwidthFraction;   // See RFC 3550; section 6.2; S / (S + R)
                                              //         - Fraction (0.0 to 1.0) of RTCP bandwidth to allocate to senders
                                              //         - Default is .25
   SDES_CFG        sdes;                      // Source descriptor information.
   ADT_UInt16      localRtcpPort;             // the local UDP port to receive rtcp pkts on (don't care when using PJSIP)
   ADT_UInt16      remoteRtcpPort;            // the remote UDP port to send rtcp pkts to (don't care when using PJSIP)
} rtcpconfig_t;

void* (*rtcpNetIn) (ADT_UInt32 ip, ADT_UInt16 port, ADT_UInt8 *payload) = NULL;
void* (*rtcpNetIn6) (ADT_UInt8 *ip, ADT_UInt16 port, ADT_UInt8 *payload) = NULL;
#pragma DATA_SECTION (rtcpNetIn, "NON_CACHED_DATA")
#pragma DATA_SECTION (rtcpNetIn6, "NON_CACHED_DATA")

far int rtcp_cached = 0;
#pragma DATA_SECTION (rtcp_cached, "NON_CACHED_DATA")

extern IPToSess_t RTCPIPToSess [0x100];
extern IPToSess_t *RTCPchnToIPSess [0x100];

int rtcp_bad_pkt_count = 0;
int rtcp_add_pkt_count = 0;
int rtcp_tx_pkt_count = 0;


//#define RTCP_DEBUG
#ifdef RTCP_DEBUG

#define RTCP_LOG_SCHED_CB   0x00000001
#define RTCP_LOG_EXSCHED_CB 0x00000002
#define RTCP_LOG_NETIN_CB   0x00000004
#define RTCP_LOG_ADD_RX     0x00000008
#define RTCP_LOG_NETTX_CB   0x00000010
#define RTCP_LOG_SR_CB      0x00010000
#define RTCP_LOG_RR_CB      0x00020000
#define RTCP_LOG_SD_CB      0x00040000

#define RTCP_DEBUG_LEN 128
typedef struct rtcp_debug_t {
    ADT_UInt32 ntp_time32;
    ADT_UInt32 code;
    ADT_UInt32 parm[4]; 
} rtcp_debug_t;
rtcp_debug_t rtcp_debug[RTCP_DEBUG_LEN];

ADT_UInt32 rtcp_debug_idx = 0;
ADT_UInt32 rtcp_debug_filter = (RTCP_LOG_NETTX_CB | RTCP_LOG_NETIN_CB | RTCP_LOG_RR_CB);



void rtcp_debug_log(ADT_UInt32 code, ADT_UInt32 p1, ADT_UInt32 p2, ADT_UInt32 p3, ADT_UInt32 p4) {
rtcp_debug_t *dbg, *next;
ADT_UInt64 NTPClock = 0;
SecondsClock_Time tv;

    if ((code & rtcp_debug_filter) == 0) return;
    SWI_disable();
    if( SecondsClock_getTime(&tv) == 0xffffffff ) return;
    dbg = &rtcp_debug[rtcp_debug_idx++];
    rtcp_debug_idx &= (RTCP_DEBUG_LEN-1);

    next = &rtcp_debug[rtcp_debug_idx];
    next->ntp_time32 = 0;
    next->code = 0;
    next->parm[0] = 0;
    next->parm[1] =0;
    next->parm[2] =0;
    next->parm[3] =0;
    SWI_enable();

    NTPClock = ((ADT_UInt64) tv.secs)<<32;
    NTPClock |= (ADT_UInt64)tv.nsecs;
    dbg->ntp_time32 = (NTPClock >> 16) & 0xffffffff;  
    dbg->code = code;
    dbg->parm[0] = p1;
    dbg->parm[1] = p2;
    dbg->parm[2] = p3;
    dbg->parm[3] = p4;
}
#else
#define rtcp_debug_log(...)
#endif



static rtcpdata_t *rtcp_get_instance_ptr(ADT_UInt16 chanId) {
   if (chanId > numRtcpChannels) {
        return (rtcpdata_t *)0;
   }
   return (rtcpData[chanId]);

} 

//
// callback functions used by RTCP algorithm: ----------------------------------
//
static ADT_UInt64 rtcp_ntptime_callback (void) { 
ADT_UInt64 NTPClock = 0;
SecondsClock_Time tv;
double ntp_low;

    if( SecondsClock_getTime(&tv) == 0xffffffff ) {
		DEBUG ("NTPTime clock gettime error\n");
		return (0);
    }

    NTPClock = ((ADT_UInt64) tv.secs)<<32;
    ntp_low =  (double)tv.nsecs * NTP_ONE_NSEC; // jdc convert bios 1 nsec resolution to NTP resolution
    NTPClock |= (ADT_UInt64)ntp_low & 0x00000000ffffffff;
//    NTPClock |= (ADT_UInt64)tv.nsecs;
    return NTPClock; 
}
	
// RTCP callback function: 
// 1. loads the function pointer for the next RTCP function to be executed
// 2. Schedules when the next function should execute by initializing a SW timer. 
// The gpak rtcp function: "rtcp_execute_scheduled_callbacks()" runs at the frame rate
// and calls the next scheduled callback when the SW timer expires.
static void rtcp_scheduler_callback (RTCP_CALLBACK_FXN fxn, ADT_UInt32 timeMS, void *sess) { 
   rtcpdata_t *rtcp;
   int chanId;
   RTPDATA *rtpData;
   
   rtpData = (RTPDATA *)sess;
   if (rtpData == NULL) return;

   chanId = (int)rtpData->rtcp.RTCPHandle;

   rtcp = rtcp_get_instance_ptr(chanId);
   if (rtcp == NULL)
        return;
   //DEBUG ("SchedulerCB: rtcpChan %d \n",chanId);

   rtcp->NextTime = currTimeMS() + timeMS;
   rtcp_debug_log(RTCP_LOG_SCHED_CB, currTimeMS(), timeMS, rtcp->NextTime, (ADT_UInt32)fxn);

   rtcp->Fxn = *fxn;
   rtcp->Sess = sess;
   return;
}

static short int rtcp_nettransmit_callback (NETHANDLE* hndl, ADT_UInt32* Data, ADT_Int32 *dataI8) {
   RTPToHostHdr_t Hdr;
   IPToSess_t  *IPMapping;
   ADT_Bool macValid;
   ADT_UInt8* data;
   int sent, mask;
   void *RTPCirc;

   data = (ADT_UInt8 *) Data;
   Hdr.PktI8  = *dataI8;
   Hdr.ChanId = (ADT_UInt16) ((ADT_UInt32) hndl);

   macValid = fastRTCPMap ((int) hndl, &IPMapping);

#ifdef SINGLE_CORE_STACK
   
   if (RTPLoopBack != DisableTest) {
      // for RTP loopback mode
      loopBackPkts(&Hdr);
      sendRtpPktToChan(Hdr.ChanId, data, *dataI8);
      return 1;
   }

   if (macValid) {
      rtcp_debug_log(RTCP_LOG_NETTX_CB, 0, 0, 0, 0);
      if (fastRTPsend((void *)data, *dataI8, IPMapping)) {
         rtcp_tx_pkt_count++;
         return 1;
      }
   }
#endif

   // Destination mac is not available - forward to core 0 for ARP
   RTPCirc = &RTPCircBuffers[(DSPCore * 2) + 1];
   mask = HWI_disable ();
   sent = PayloadToCircCache (RTPCirc, &Hdr, RTP_TO_HOST_I16, data, Hdr.PktI8);
   HWI_restore (mask);

   return sent;
}

static void rtcp_notify_callback (ADT_UInt16 chanId, ADT_UInt32 SSRC, RTCP_BLOCK_TYPES type, reportData_t *Data) { 
ADT_UInt64 ntp_time64;
ADT_UInt32 ntp_time32;
#ifdef RTCP_DEBUG
//float rtt_ms;
ADT_UInt32 ntp_time64H, ntp_time64L;
#endif
rtcpdata_t *rtcp;
senderReport_t   *SR;      // Parsed sender report fields
receiverReport_t *RR;      // Parsed receiver report fields
sdesReport_t     *SDES;    // Parsed source descriptor fields
byeReport_t      *BYE;     // Parsed bye report fields     
appReport_t      *APP;     // Pointer/length of application defined report
sdesReportItem_t *src, *dst;
ADT_UInt8 i;

    rtcp = rtcp_get_instance_ptr(chanId);
    if (rtcp == NULL)
        return;

   switch (type) {
       default:
            rtcp->RtcpStats.rcvERRCount++;
       break;

       case RTCP_SR:
            SR = &(rtcp->RtcpStats.sndrRpt);   
            SWI_disable();
            *SR = Data->SR;
            SWI_enable();
            rtcp->RtcpStats.rcvSRCount++;
            rtcp->RtcpStats.sndrRptSSRC = SSRC;
#ifdef RTCP_DEBUG
            ntp_time64 = Data->SR.NTPTime;
            ntp_time32 = (ADT_UInt32)((ntp_time64 >> 16) & 0xffffffff);
            ntp_time64H = ntp_time64 >> 32;
            ntp_time64L = ntp_time64 & 0xffffffff;
            rtcp_debug_log(RTCP_LOG_SR_CB, ntp_time64H, ntp_time64L, ntp_time32, SR->RTPTime);
#endif
            SR_notify   (chanId, SSRC, SR);    
            rtcp->RtcpStats.valid |= SR_VALID;
            break;
       case RTCP_RR:   
            ntp_time64 = rtcp_ntptime_callback();
            RR = &(rtcp->RtcpStats.rcvrRpt);   
            SWI_disable();
            *RR = Data->RR;
            SWI_enable();
            // compute Round-Trip Time:
            //ntp_time64 = rtcp->arrivalTime;
            ntp_time32 = (ADT_UInt32)((ntp_time64 >> 16) & 0xffffffff);
            rtcp->RtcpStats.RTT = ntp_time32 - RR->DLSR - RR->LSR;
            if (rtcp->RtcpStats.RTT & 0x8000000) {
                rtcp->RtcpStats.rttNegCount++;
            }
            rtcp->RtcpStats.rcvRRCount++;
            rtcp->RtcpStats.rcvrRptSSRC = SSRC;
#ifdef RTCP_DEBUG
            //rtt_ms = (float)rtcp->RTT/(float)65.536;
            ntp_time64H = ntp_time64 >> 32;
            ntp_time64L = ntp_time64 & 0xffffffff;
            rtcp_debug_log(RTCP_LOG_RR_CB, rtcp->RtcpStats.RTT, ntp_time32-RR->LSR, RR->DLSR, RR->LSR);
#endif
            RR_notify   (chanId, SSRC, RR);    
            rtcp->RtcpStats.valid |= RR_VALID;
            break;
       case RTCP_SDES: 
            rtcp->RtcpStats.rcvSDESCount++;
            SDES = &(rtcp->RtcpStats.sdesRpt);
            SWI_disable();
            SDES->itemCnt = Data->SDES.itemCnt;
            for (i=0; i<SDES->itemCnt; i++) {
                src =  &(Data->SDES.item[i]); 
                dst =  &(SDES->item[i]);
                dst->data = rtcp->RtcpStats.sdesData[i];
                dst->dataI8 = src->dataI8;
                dst->type = src->type;
                memcpy(dst->data, src->data, dst->dataI8);
            }
            SWI_enable();
            rtcp_debug_log(RTCP_LOG_SD_CB, 0, 0, 0, 0);
            SDES_notify (chanId, SSRC, SDES);
            rtcp->RtcpStats.valid |= SDES_VALID;
            break;
       case RTCP_APP:  
            rtcp->RtcpStats.rcvAPPCount++;
            APP = &(rtcp->RtcpStats.appRpt);   
            APP->block = rtcp->RtcpStats.appData;
            SWI_disable();
            APP->blockI8 = Data->APP.blockI8;
            memcpy(APP->block, Data->APP.block, APP->blockI8);
            SWI_enable(); 
            APP_notify  (chanId, SSRC, APP);   
            rtcp->RtcpStats.valid |= APP_VALID;   
            break;
       case RTCP_EXT:  
            rtcp->RtcpStats.rcvEXTCount++;
            EXT_notify  (chanId, SSRC, &Data->APP);   
            rtcp->RtcpStats.valid |= EXT_VALID;   
            break;
       case RTCP_BYE:  
            rtcp->RtcpStats.rcvBYECount++;
            BYE = &(rtcp->RtcpStats.byeRpt);   
            BYE->reason = rtcp->RtcpStats.byeData;
            BYE->reasonI8 = Data->BYE.reasonI8;
            SWI_disable();
            memcpy(BYE->reason, Data->BYE.reason, BYE->reasonI8); 
            SWI_enable();
            BYE_notify  (chanId, SSRC, &Data->BYE);   
            rtcp->RtcpStats.valid |= BYE_VALID;   
            break;
   }

   return; 
}

#ifdef PRINT_RTCP_NOTIFICATIONS
static void dumpStr (ADT_UInt8 *str, int lenI8) {  
   while (lenI8--) DEBUG ("%c", *str++);
}
static void dumpHex (void *data, int lenI8) {  
   int i;
   ADT_UInt8 *Data = data;

   for (i=0; i<lenI8; i++) {
      DEBUG ("%02x ", *Data++);
      if (i%16 == 15) DEBUG ("\n");
   }
   DEBUG ("\n");
}
//  SDES notificaton callbacks
static void   SR_notify (ADT_UInt16 chanId, ADT_UInt32 SSRC, senderReport_t *SR) {


   DEBUG ("  SR for %d.%x  ", chanId, SSRC);
   DEBUG ("Pkts: %u   Bytes: %u   RTPTime: %u   NTP: %8x%08x\n", SR->PktsSent, SR->BytesSent,
                 SR->RTPTime, (ADT_UInt32) (SR->NTPTime >> 32), (ADT_UInt32) (SR->NTPTime & 0x00000000ffffffff)); 
}

static void   RR_notify (ADT_UInt16 chanId, ADT_UInt32 SSRC, receiverReport_t *RR) {

   ADT_Float32 delayedSeconds;

   delayedSeconds = (ADT_Float32) (RR->DLSR / 65536.0);

   DEBUG ("  RR for %d.%x", chanId, SSRC);
   DEBUG ("  Lost:  %d%%, cumLost: %d", 
           ((RR->fractionPktsLost * 100) / 255), RR->cumPktsLost);
   DEBUG ("  MaxSeq: %d, Jitter: %d,  LSR: %x,   DLSR: %6.3f\n", 
             RR->extendedMaxSeq, RR->JitterEstimate, RR->LSR, delayedSeconds);

}

static void SDES_notify (ADT_UInt16 chanId, ADT_UInt32 SSRC, sdesReport_t *SDES) {
   int i;
   sdesReportItem_t *item;


   item = SDES->item;
   for (i=0; i<SDES->itemCnt; i++, item++) {

      DEBUG ("SDES for %d.%x.  Item: %3d ", chanId, SSRC, item->type);
      dumpStr (item->data, item->dataI8);
      DEBUG ("\n");
   }
}

static void  BYE_notify (ADT_UInt16 chanId, ADT_UInt32 SSRC, byeReport_t *BYE) {


   DEBUG ("BYE for %d.%x  ", chanId, SSRC);
   dumpStr (BYE->reason, BYE->reasonI8);
   DEBUG ("\n");
}

static void  APP_notify (ADT_UInt16 chanId, ADT_UInt32 SSRC, appReport_t *APP) {


   DEBUG ("APP for %d.%x ", chanId, SSRC);
   dumpStr (APP->block, APP->blockI8);
   DEBUG ("\n            ");
   dumpHex (APP->block, APP->blockI8);
}

static void  EXT_notify (ADT_UInt16 chanId, ADT_UInt32 SSRC, appReport_t *APP) {

   DEBUG ("EXT for %d.%x ", chanId, SSRC);
   dumpStr (APP->block, APP->blockI8);
   DEBUG ("\n            ");
   dumpHex (APP->block, APP->blockI8);
}

#endif

int rtcp_is_valid_header(ADT_UInt8 *payload) {
    // check if a standard RTCP header and valid rtcp SR or RR payload type
    if ( ((payload[0] & 0xc0) == 0x80) &&  
         ((payload[1] == 200) || (payload[1] == 201)) ) {
        return 1;
    }

    return 0;
}

static int rtcp_channel_init (ADT_UInt16 chanId, rtcpdata_t *rtcp, rtcpconfig_t  *cfg) {
rtcpConfig_APIV5_t  *rtcpCfg;
RTCP_STAT rtcpStat;
IPToSess_t *ipSess;
IPToSess_t  IPMapping;
int i;

    rtcpCfg = &(rtcp->cfg);

    rtcpCfg->APIVersion = ADT_RTP_API_VERSION;
    rtcpCfg->Scheduler = rtcp_scheduler_callback;
    rtcpCfg->NTPTime   = rtcp_ntptime_callback;
    rtcpCfg->RTCP_Notify = rtcp_notify_callback;
    rtcpCfg->NetTransmit = rtcp_nettransmit_callback;
    rtcpCfg->rtcpBandwidthBytesPerSec = cfg->rtcpBandwidthBytesPerSec;
    rtcpCfg->sdes.itemCnt = cfg->sdes.itemCnt;
    rtcpCfg->sdes.pktsPerCycle = cfg->sdes.pktsPerCycle;

    // copy sdes items into local copies. 
    // initialize configuration structure's textData pointer to static buffer in gpak rtcp channel structure.
    // The sdes text data from the API message has already been copied there.
    for (i=0; i<rtcpCfg->sdes.itemCnt; i++) {
        rtcpCfg->sdes.item[i] = cfg->sdes.item[i];
        rtcpCfg->sdes.item[i].textData  = &(rtcp->sdesText[i][0]); // point to local buffer
    }
    rtcp->localRtcpPort = cfg->localRtcpPort;
    rtcp->remoteRtcpPort = cfg->remoteRtcpPort;
                   
    // read the ipSess settings for this Gpak channel to obtain the RTP session and IP addresses.
    fastRTPMap(chanId, &ipSess);
    if ((ipSess->ChanId != chanId) || (ipSess->RTPSession == NULL) || ((void *)rtcp->sess != ipSess->RTPSession)) { 
        DEBUG("rtcp_init: rtp session NULL\n");
        return -1;
    }

    memset (&IPMapping, 0, sizeof (IPMapping));
    IPMapping.vlanIndex  = 0xff;

    if (ipSess->ipver != 6) {
        // use remote IP address from RTP session (was already converted to network byte order)
        IPMapping.rmt.IP = ntohl (ipSess->rmt.IP);
        IPMapping.rmt.dstIP =  IPMapping.rmt.IP;

        // use remote's MAC address from RTP session if already available as indicated by lcl.SSRC==1
        if (ipSess->lcl.SSRC == 1) {
          memcpy (IPMapping.rmt.MAC, ipSess->rmt.MAC, 6);
        }
        IPMapping.rmt.Port = rtcp->remoteRtcpPort;
        IPMapping.lcl.Port = rtcp->localRtcpPort; 
        if (IPMapping.lcl.Port != 0 && !rtpPortValid (IPMapping.lcl.Port)) {
            DEBUG("rtcp_init: invalid port \n");
            return -1;
        }
        if (addToIPSessionRTCP (&IPMapping, (void *)rtcp->sess, chanId) < 0) {
            DEBUG("rtcp_init: add Session failure \n");
            return -1;
        }
    } 
#ifdef _INCLUDE_IPv6_CODE
    else {
        // use remote IP address from RTP session
        memcpy(&IPMapping.rmt6.IP, &ipSess->rmt6.IP, 16);
        memcpy(&IPMapping.rmt6.dstIP, &ipSess->rmt6.IP, 16);

        // use remote's MAC address from RTP session if already available as indicated by lcl.SSRC==1
        if (ipSess->lcl6.SSRC == 1) {
          memcpy (IPMapping.rmt6.MAC, ipSess->rmt6.MAC, 6);
        }
        IPMapping.rmt6.Port = rtcp->remoteRtcpPort;
        IPMapping.lcl6.Port = rtcp->localRtcpPort; 
        if (IPMapping.lcl6.Port != 0 && !rtpPortValid (IPMapping.lcl6.Port)) {
            DEBUG("rtcp_init: invalid port \n");
            return -1;
        }
        if (addToIPSessionRTCP6 (&IPMapping, (void *)rtcp->sess, chanId) < 0) {
            DEBUG("rtcp_init: add Session failure \n");
            return -1;
        }
    }
#endif

#if 0
    rtcpStat = RTCP_ADT_Init (rtcp->sess, (void *)chanId, rtcpCfg);
    if (rtcpStat != RTCPSuccess) {
        DEBUG("RTCP Init error: code %d \n", rtcpStat);
        return -1;
    }
    rtcp->enabled = 1;
#else
    rtcp->initPending = 1;
#endif
    return 0;
}

// "rtcpNetIn" callback from UDP rx packet processing in network stack. Used to determine whether
// received packet is an RTCP packet, and if so, determine the associated RTCP IPSession
static void *rtcpNetInCallback(ADT_UInt32 rmtIP, ADT_UInt16 rmtPort, ADT_UInt8 *payload) {
   IPToSess_t *ipSess;
   ADT_UInt32 rmtSSRC;
   rtcpdata_t *rtcp;


    if (rtcp_is_valid_header(payload) == 0)
        return (void *)0;

    // extract remote SSRC from RTCP header
    rmtSSRC = _mem4(&(payload[4]));
    ipSess = IPSessionLookupRTCP (rmtIP, rmtPort, rmtSSRC, FALSE);
    if (ipSess == NULL)
        return (void *)0;
 
    rtcp = rtcp_get_instance_ptr(ipSess->ChanId);
    if (rtcp == NULL)
        return (void *)0;
    
#ifdef SINGLE_CORE_STACK
    if (rtcp->enabled) {
        rtcp->arrivalTime = rtcp_ntptime_callback();
        rtcp_debug_log( RTCP_LOG_NETIN_CB, 0, 0, 0, 0);
    } else {
        return (void *)0;
    }
#else
    if (rtcp_cached) {
        CLEAR_INST_CACHE (rtcp, sizeof (rtcpdata_t));
        FLUSH_wait ();
    }
    if (rtcp->enabled == 0) {
        return (void *)0;
    }
#endif
    return (void *)ipSess;
}

#ifdef _INCLUDE_IPv6_CODE
// "rtcpNetIn6" callback from UDP rx packet processing in network stack. Used to determine whether
// received packet is an RTCP packet, and if so, determine the associated RTCP IPSession
static void *rtcpNetInCallback6(ADT_UInt8 *rmtIP, ADT_UInt16 rmtPort, ADT_UInt8 *payload) {
   IPToSess_t *ipSess;
   ADT_UInt32 rmtSSRC;
   rtcpdata_t *rtcp;

    if (rtcp_is_valid_header(payload) == 0)
        return (void *)0;

    // extract remote SSRC from RTCP header
    rmtSSRC = _mem4(&(payload[4]));
    ipSess = IPSessionLookupRTCP6 (rmtIP, rmtPort, rmtSSRC, FALSE);
    if (ipSess == NULL)
        return (void *)0;

    rtcp = rtcp_get_instance_ptr(ipSess->ChanId);
    if (rtcp == NULL)
        return (void *)0;

#ifdef SINGLE_CORE_STACK
    if (rtcp->enabled) {
        rtcp->arrivalTime = rtcp_ntptime_callback();
        rtcp_debug_log( RTCP_LOG_NETIN_CB, 0, 0, 0, 0);
    } else {
        return (void *)0;
    }
#else
    if (rtcp_cached) {
        CLEAR_INST_CACHE (rtcp, sizeof (rtcpdata_t));
        FLUSH_wait ();
    }
    if (rtcp->enabled == 0) {
        return (void *)0;
    }
#endif

    return (void *)ipSess;
}
#endif

//
// This function is called by gpak framework at the framerate. It executes the
// current RTCP-internal callback function that is scheduled to execute when the 
// callback's execution-timer has expired.  
//
void rtcp_execute_scheduled_callbacks(ADT_UInt16 chanId) {
rtcpdata_t *rtcp;
ADT_Int32 DeltaTime;
chanInfo_t *chan;

   rtcp = rtcp_get_instance_ptr(chanId);
   if (rtcp == NULL)
        return;

   if (rtcp->initPending) {
        RTCP_STAT rtcpStat;
        rtcpStat = RTCP_ADT_Init (rtcp->sess, (void *)chanId, &(rtcp->cfg));
        if (rtcpStat == RTCPSuccess) {
            rtcp->enabled = 1;
        }
        rtcp->initPending = 0;
   }

   if ((rtcp->enabled == 0) || (rtcp->sess == 0))
        return;

   if ((rtcp->Fxn == 0) || (rtcp->Sess == 0))
       return;

   if (rtcp->sess != rtcp->Sess) {
       //DEBUG("rtcp schedCB: rtcp->sess=%x,  rtcp->Sess=%x, chan=%d\n",rtcp->sess, rtcp->Sess, i);
       return;
   }

   chan = chanTable[chanId];
   if (chan->rtcpBye.byePending && (chan->rtcpBye.byeI8 > 0) && (chan->rtcpBye.byeI8 <= MAX_RTCP_BYE_MSGI8))  {
        chan->rtcpBye.byePending = 0;
        chan->rtcpBye.byeI8 = 0;
        if (RTCP_ADT_Bye(rtcp->sess, chan->rtcpBye.byeMsg, chan->rtcpBye.byeI8) != RTCPSuccess) {
            //chan->status.Bits.rtcpByeFailure;
            ;
        }
    }

   DeltaTime = rtcp->NextTime - currTimeMS();
   if (DeltaTime < 0) {
        rtcp_debug_log(RTCP_LOG_EXSCHED_CB, currTimeMS(),  currTimeMS() - rtcp->NextTime, (ADT_UInt32)rtcp->Fxn, 0);

       //DEBUG("rtcp schedCB: rtcp->sess=%x,  rtcp->Sess=%x, chan=%d\n",rtcp->sess, rtcp->Sess, i);
       rtcp->Fxn (rtcp->Sess);
       //DEBUG("rtcp schedCB: return Fxn\n");
      return;
   }
       
}

int rtcp_process_bye_msg(ADT_UInt16 *pCmd, ADT_UInt16 *pReply) {
rtcpdata_t *rtcp;
ADT_UInt16 chanId;
ADT_UInt8 *byeMsg, byeI8, j, k;
ADT_UInt16 MsgLenI16;
chanInfo_t *chan;


    chanId = pCmd[0] & 0xff;
    byeI8 = (ADT_UInt8)LO_8(pCmd[1]);

    pReply[0] |= (MSG_RTCP_BYE_REPLY << 8);
    pReply[1]  = chanId << 8;

    if (chanId >= sysConfig.maxNumChannels) {
        pReply[1] |= RTCPBye_InvalidChannel;
        goto byeReturn;
    }

    rtcp = rtcp_get_instance_ptr(chanId);
    if (rtcp == NULL) {
        pReply[1] |= RTCPBye_NotConfigured;
        goto byeReturn;
    }

    chan = chanTable[chanId];
    byeMsg = chan->rtcpBye.byeMsg;
    
    if (chan->CoreID != (ADT_UInt16)msgCore) 
    {
        if (rtcp_cached) {
            CLEAR_INST_CACHE (rtcp, sizeof (rtcpdata_t));
            FLUSH_wait ();
        }
    }

    if (rtcp->enabled == 0) {
        pReply[1] |= RTCPBye_ChannelInactive;
        goto byeReturn;
    }

    MsgLenI16 = 2;
    for (j=0, k=0; j<(int)byeI8/2; j++, k+=2, MsgLenI16++) 
    {
         byeMsg[k]   = HI_8(pCmd[MsgLenI16]);
         byeMsg[k+1] = LO_8(pCmd[MsgLenI16]);
     }
     // unpack the remaining byte if odd number of dataBytes
     if (byeI8 & 1) {
         byeMsg[k]   = HI_8(pCmd[MsgLenI16]);
         MsgLenI16++;
     }
    chan->rtcpBye.byeI8 = byeI8;
    chan->rtcpBye.byePending = 1;
    pReply[1] |= RTCPBye_Success;

byeReturn:
    return (2*2);
}

// clear out the gpak rtcp instance
void rtcp_channel_null (ADT_UInt16 chanId) {
rtcpdata_t *rtcp;

    rtcp = rtcp_get_instance_ptr(chanId);
    if (rtcp == NULL)
        return;
	memset(rtcp, 0, sizeof(rtcpdata_t));
}


// indicate whether RTCP is enabled on this channel
int rtcp_channel_is_enabled(ADT_UInt16 chanId) {
rtcpdata_t *rtcp;

    rtcp = rtcp_get_instance_ptr(chanId);
    if (rtcp == NULL)
        return 0;

    return (rtcp->enabled);
} 

// Called from Gpak HOSTSWI to add packets
int rtcp_rx_packet(ADT_UInt16 chanId, ADT_UInt8 *pktBuffer, ADT_UInt16 pktLenI8) {
rtcpdata_t *rtcp;
RTCP_STAT status;


    rtcp = rtcp_get_instance_ptr(chanId);
    if (rtcp == NULL)
        return 0;

    if ((rtcp->enabled == 0) || (rtcp->sess == 0))
        return 0;

    // check if RTCP payload is RR or SR, if so, add packet to RTCP receive alg.
    if (rtcp_is_valid_header(pktBuffer) == 0)
        return 0;

    rtcp_debug_log(RTCP_LOG_ADD_RX, 0, 0, 0, 0);
#ifndef SINGLE_CORE_STACK
    rtcp->arrivalTime = rtcp_ntptime_callback();
#endif
    if ((status = RTCP_ADT_OnReceive (rtcp->sess, (void *) pktBuffer, (int)pktLenI8)) != RTCPSuccess) {
        DEBUG("Error with rtcp To Engine: %d %d\n", status, chanId);
        rtcp_bad_pkt_count++;
        return 0;
    }

    rtcp_add_pkt_count++;

    return 1;
}

void rtcp_system_init() { 
int i;           
rtcpdata_t *rtcp;

    for (i=0; i<sysConfig.maxNumChannels; i++) {
        rtcp = rtcp_get_instance_ptr(i);
        if (rtcp_cached) {
            CLEAR_INST_CACHE (rtcp, sizeof (rtcpdata_t));
            FLUSH_wait ();
        }
        if (DSPCore == 0) {
            memset(rtcp, 0, sizeof(rtcpdata_t));
            if (rtcp_cached) {
                FLUSH_INST_CACHE (rtcp, sizeof (rtcpdata_t));
                FLUSH_wait ();
            }
        }
    }

    SecondsClock_set(NTP_OFFSET);
    rtcpNetIn = rtcpNetInCallback;
#ifdef _INCLUDE_IPv6_CODE
    rtcpNetIn6 = rtcpNetInCallback6;
#endif
}


int rtcp_process_cfg_msg(ADT_UInt16 *pCmd, ADT_UInt16 *pReply, void *RTPSession) {
rtcpconfig_t rtcp_parms;
rtcpdata_t *rtcp;

ADT_UInt16 chanId;
int status;
int i, j, k;
ADT_UInt32 MsgLenI16; 
SDES_CFG_ITEM *sdes;
chanInfo_t *chan;

    chanId   = (pCmd [1] & 0x00FF);
    rtcp = rtcp_get_instance_ptr(chanId);
    if (rtcp == NULL)
        return -1;

    if (rtcp_cached) {
        CLEAR_INST_CACHE (rtcp, sizeof (rtcpdata_t));
        FLUSH_wait ();
    }
    rtcp_channel_null(chanId);

    memset(&rtcp_parms, 0, sizeof(rtcpconfig_t));
    chan = chanTable[chanId];
    chan->rtcpBye.byeI8 = 0;
    chan->rtcpBye.byePending = 0;

    rtcp_parms.localRtcpPort  = pCmd[25];
    rtcp_parms.remoteRtcpPort = pCmd[26];
    rtcp_parms.transportOverheadBytes = pCmd[27];
    rtcp_parms.timeoutMultiplier = pCmd[28];
                           
    rtcp_parms.minRTCPPktPeriodMs = Pack32(pCmd[29], pCmd[30]);
    rtcp_parms.rtcpBandwidthBytesPerSec = Pack32(pCmd[31], pCmd[32]);
    rtcp_parms.senderBandwidthFraction  = (ADT_Float32)(Pack32(pCmd[33], pCmd[34]));
    rtcp_parms.sdes.itemCnt = HI_8(pCmd[35]);
    rtcp_parms.sdes.pktsPerCycle = LO_8(pCmd[35]);

    if (rtcp_parms.sdes.itemCnt > MAX_SDES_ITEMS)
        return -1;

    MsgLenI16 = 36;

    for (i=0; i<rtcp_parms.sdes.itemCnt; i++) {
        sdes = &rtcp_parms.sdes.item[i];
        sdes->textData = &(rtcp->sdesText[i][0]);  // sdes text buffer is allocated in channel instance structure
        sdes->type            = HI_8(pCmd[MsgLenI16]);
        sdes->firstPktInCycle = LO_8(pCmd[MsgLenI16]);
        MsgLenI16++;

        sdes->txInterval = HI_8(pCmd[MsgLenI16]);
        sdes->dataBytes  = LO_8(pCmd[MsgLenI16]);
        MsgLenI16++;

       for (j=0, k=0; j<(int)sdes->dataBytes/2; j++, k+=2, MsgLenI16++) 
       {
            sdes->textData[k]   = HI_8(pCmd[MsgLenI16]);
            sdes->textData[k+1] = LO_8(pCmd[MsgLenI16]);
        }
        // unpack the remaining byte if odd number of dataBytes
        if (sdes->dataBytes & 1) {
            sdes->textData[k]   = HI_8(pCmd[MsgLenI16]);
            MsgLenI16++;
        }
    }
    rtcp->sess = (RTPCONNECT  *)RTPSession;
    status = rtcp_channel_init (chanId, rtcp, &rtcp_parms);

    if (rtcp_cached) {
        FLUSH_INST_CACHE (rtcp, sizeof (rtcpdata_t));
        FLUSH_wait ();
    }
    return status;
}


int rtcp_process_cfg_msg_v6(ADT_UInt16 *pCmd, ADT_UInt16 *pReply, void *RTPSession) {
rtcpconfig_t rtcp_parms;
rtcpdata_t *rtcp;
chanInfo_t *chan;

ADT_UInt16 chanId;
int status;
int i, j, k;
ADT_UInt32 MsgLenI16; 
SDES_CFG_ITEM *sdes;

    chanId   = (pCmd [1] & 0x00FF);
    rtcp = rtcp_get_instance_ptr(chanId);
    if (rtcp == NULL)
        return -1;

    if (rtcp_cached) {
        CLEAR_INST_CACHE (rtcp, sizeof (rtcpdata_t));
        FLUSH_wait ();
    }

    rtcp_channel_null(chanId);

    memset(&rtcp_parms, 0, sizeof(rtcpconfig_t));
    chan = chanTable[chanId];
    chan->rtcpBye.byeI8 = 0;
    chan->rtcpBye.byePending = 0;

    rtcp_parms.localRtcpPort  = pCmd[43];
    rtcp_parms.remoteRtcpPort = pCmd[44];
    rtcp_parms.transportOverheadBytes = pCmd[45];
    rtcp_parms.timeoutMultiplier = pCmd[46];
                           
    rtcp_parms.minRTCPPktPeriodMs = Pack32(pCmd[47], pCmd[48]);
    rtcp_parms.rtcpBandwidthBytesPerSec = Pack32(pCmd[49], pCmd[50]);
    rtcp_parms.senderBandwidthFraction  = (ADT_Float32)(Pack32(pCmd[51], pCmd[52]));
    rtcp_parms.sdes.itemCnt = HI_8(pCmd[53]);
    rtcp_parms.sdes.pktsPerCycle = LO_8(pCmd[53]);

    if (rtcp_parms.sdes.itemCnt > MAX_SDES_ITEMS)
        return -1;

    MsgLenI16 = 54;

    for (i=0; i<rtcp_parms.sdes.itemCnt; i++) {
        sdes = &rtcp_parms.sdes.item[i];
        sdes->textData = &(rtcp->sdesText[i][0]);  // sdes text buffer is allocated in channel instance structure
        sdes->type            = HI_8(pCmd[MsgLenI16]);
        sdes->firstPktInCycle = LO_8(pCmd[MsgLenI16]);
        MsgLenI16++;

        sdes->txInterval = HI_8(pCmd[MsgLenI16]);
        sdes->dataBytes  = LO_8(pCmd[MsgLenI16]);
        MsgLenI16++;

       for (j=0, k=0; j<(int)sdes->dataBytes/2; j++, k+=2, MsgLenI16++) 
       {
            sdes->textData[k]   = HI_8(pCmd[MsgLenI16]);
            sdes->textData[k+1] = LO_8(pCmd[MsgLenI16]);
        }
        // unpack the remaining byte if odd number of dataBytes
        if (sdes->dataBytes & 1) {
            sdes->textData[k]   = HI_8(pCmd[MsgLenI16]);
            MsgLenI16++;
        }
    }
    rtcp->sess = (RTPCONNECT  *)RTPSession;
    status = rtcp_channel_init (chanId, rtcp, &rtcp_parms);
    if (rtcp_cached) {
        FLUSH_INST_CACHE (rtcp, sizeof (rtcpdata_t));
        FLUSH_wait ();
    }
    return status;
}

static void pack8To16(ADT_UInt8 *src8, ADT_UInt16 *dst16, ADT_UInt16 lenI8, ADT_UInt32 *lenI16) {
int j,k;
ADT_UInt32 MsgLenI16 = *lenI16;

    for (j=0, k=0; j<(int)lenI8/2; j++, k+=2) {
        dst16[MsgLenI16++] = PackBytes(src8[k], src8[k+1]);
    }
        // pack the remaining byte if odd number of dataBytes
    if (lenI8 & 1) {
        dst16[MsgLenI16++] = PackBytes(src8[k], 0);
    }

    *lenI16 = MsgLenI16;
}

// format rtcp stats message for host report
int rtcp_read_stats (ADT_UInt16 chanId,  ADT_UInt16 *pReply) {
rtcpdata_t *rtcp;
chanInfo_t *chan;
RtcpStats_t *stats;
int i;
ADT_UInt32 replyLen;

    rtcp = rtcp_get_instance_ptr(chanId);
    if (rtcp == NULL)
        return 0;

    chan = chanTable[chanId];
    if (chan->CoreID != (ADT_UInt16)msgCore) 
    {
        if (rtcp_cached) {
            CLEAR_INST_CACHE (rtcp, sizeof (rtcpdata_t));
            FLUSH_wait ();
        }
     }

    if (rtcp->enabled == 0) {
        for (i=0; i<16; i++)
            pReply[i] = 0;
        return 16;
    }

    stats = &(rtcp->RtcpStats);

    pReply[0] = stats->valid;
 
    if (stats->valid & RR_VALID) {
        pReply[1] = HI_16(stats->RTT);
        pReply[2] = LO_16(stats->RTT);
        pReply[3] = HI_16(stats->rcvrRpt.JitterEstimate);
        pReply[4] = LO_16(stats->rcvrRpt.JitterEstimate);
        pReply[5] = HI_16(stats->rcvrRpt.cumPktsLost);
        pReply[6] = LO_16(stats->rcvrRpt.cumPktsLost);
        pReply[7] = HI_16(stats->rcvrRptSSRC);
        pReply[8] = LO_16(stats->rcvrRptSSRC);
        pReply[9] = (ADT_UInt16)(stats->rcvrRpt.fractionPktsLost);
    }

    if (stats->valid & SR_VALID) {
        pReply[10] = HI_16(stats->sndrRpt.PktsSent);
        pReply[11] = LO_16(stats->sndrRpt.PktsSent);
        pReply[12] = HI_16(stats->sndrRpt.BytesSent);
        pReply[13] = LO_16(stats->sndrRpt.BytesSent);
        pReply[14] = HI_16(stats->sndrRptSSRC);
        pReply[15] = LO_16(stats->sndrRptSSRC);
    }
    replyLen = 16;

    // send sdes report
    if (stats->valid & SDES_VALID) {
        sdesReport_t   *rpt = &stats->sdesRpt;
        int i;

        pReply[replyLen++] = (ADT_UInt16)rpt->itemCnt;
        for (i=0; i<rpt->itemCnt; i++) {
            sdesReportItem_t *item;

            item = &(rpt->item[i]);
            pReply[replyLen++] = PackBytes(item->type, item->dataI8);
            pack8To16(item->data, pReply, item->dataI8, &replyLen);
        }
    }

    // send bye report
    if (stats->valid & BYE_VALID) {
        byeReport_t  *rpt = &stats->byeRpt;

        pReply[replyLen++] = (ADT_UInt16)rpt->reasonI8;
        pack8To16(rpt->reason, pReply, rpt->reasonI8, &replyLen);
    }

    // send app report
    if (stats->valid & APP_VALID) {
        appReport_t  *rpt = &stats->appRpt;

        pReply[replyLen++] = (ADT_UInt16)rpt->blockI8;
        pack8To16(rpt->block, pReply, rpt->blockI8, &replyLen);
    }

   if (chan->CoreID != msgCore) 
   {
        if (rtcp_cached) {
            CLEAR_INST_CACHE (rtcp, sizeof (rtcpdata_t));
        }
   }
    return (int)replyLen;
}

#endif // end if IP stack support exists
#endif // end if RTCP_ENABLED

// needed to resolve missing symbol from rtp/rtcp library
void MemCopy8(ADT_Int8 *pDest, ADT_Int8 *pSrc, ADT_Int32 Length)
{
	int i;
	for (i = 0; i < Length; i++)
		pDest[i] = pSrc[i];
}


#ifdef NTP_DEBUG
#define NTP_LEN 128
ADT_UInt32 ntp_history[NTP_LEN][3];
SecondsClock_Time prev_tv;
int ntp_state = 0;
Uint32 ntp_idx = 0;

void rtcp_debug_ntptime() { 
SecondsClock_Time tv;
Uint32 htime;

    if( SecondsClock_getTime(&tv) == 0xffffffff ) {
		DEBUG ("NTPTime clock gettime error\n");
		return;
    }
    htime = CLK_gethtime();
    if (ntp_state == 1) {
        // start a capture, snapshot the current 'SecondClock' value
        ntp_idx = 0;
        prev_tv = tv;
        ntp_state = 2;
    }
    if (ntp_state == 2) {
        // save samples while waiting for 'second' value to increment twice
        ntp_history[ntp_idx][0] = tv.secs;
        ntp_history[ntp_idx][1] = tv.nsecs;
        ntp_history[ntp_idx][2] = htime;
        ntp_idx++;
        ntp_idx &= (NTP_LEN-1);
        if (tv.secs > (prev_tv.secs+1))
            ntp_state = 3;
    }
    else if (ntp_state >= 3) {
        // save first 20 samples after the 'second' value incremented
        ntp_history[ntp_idx][0] = tv.secs;
        ntp_history[ntp_idx][1] = tv.nsecs;
        ntp_history[ntp_idx][2] = htime;
        ntp_idx++;
        ntp_idx &= (NTP_LEN-1);
        ntp_state++;
        if (ntp_state >= 20) {
            ntp_state = 0;
            ntp_history[ntp_idx][0] = 0;
            ntp_history[ntp_idx][1] = 0;
            ntp_history[ntp_idx][2] = 0;
        }
    }
}
#else
void rtcp_debug_ntptime() { }
#endif
