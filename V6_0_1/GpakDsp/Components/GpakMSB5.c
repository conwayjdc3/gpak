unpackMSB5(unsigned char *pU8, unsigned short *pOut, unsigned short num)
{
int loopCount, outCount, i;

    loopCount = num/8;
    for (i=0; i<loopCount; i++)
    {
        // 8 output values per 5 input bytes
        *pOut++ = *pU8 >> 3;                    

        *pOut = (*pU8++ & 0x07) << 2;           
        *pOut |= (*pU8 >> 6);
        pOut++;

        *pOut++ = (*pU8 >> 1) & 0x1F;           

        *pOut = (*pU8++ & 0x01) << 4;           
        *pOut |= (*pU8 >> 4);
        pOut++;

        *pOut = (*pU8++ & 0x0F) << 1;           
        *pOut |= (*pU8 >> 7);
        pOut++;

        *pOut++ = (*pU8 >> 2) & 0x1F;           

        *pOut = (*pU8++ & 0x03) << 3;           
        *pOut |= (*pU8 >> 5);
        pOut++;

        *pOut++ = *pU8++ & 0x1F;                
    }                                           

    // remainder loop
    if (outCount = num % 8)
    {
        for (i=0; i<1; i++)
        {
            *pOut++ = *pU8 >> 3;                    
            if (--outCount == 0)
                break;

            *pOut = (*pU8++ & 0x07) << 2;           
            *pOut |= (*pU8 >> 6);
            if (--outCount == 0)
                break;
            pOut++;

            *pOut++ = (*pU8 >> 1) & 0x1F;           
            if (--outCount == 0)
                break;

            *pOut = (*pU8++ & 0x01) << 4;           
            *pOut |= (*pU8 >> 4);
            if (--outCount == 0)
                break;
            pOut++;

            *pOut = (*pU8++ & 0x0F) << 1;           
            *pOut |= (*pU8 >> 7);
            if (--outCount == 0)
                break;
            pOut++;

            *pOut++ = (*pU8 >> 2) & 0x1F;           
            if (--outCount == 0)
                break;

            *pOut = (*pU8++ & 0x03) << 3;           
            *pOut |= (*pU8 >> 5);
        } 
    }
}
packMSB5(unsigned char *pU8, unsigned short *pIn, unsigned short num)
{
int loopCount, inCount, i;

    loopCount = num/8;
    for (i=0; i<loopCount; i++)
    {
        // five output bytes per 8 input values
        *pU8 = *pIn++ << 3;   
        *pU8 |= ((*pIn & 0x1F) >> 2);         
        pU8++;
                                                
        *pU8 = *pIn++ << 6;            
        *pU8 |= ((*pIn++ & 0x1F) << 1);         
        *pU8 |= ((*pIn & 0x1F) >> 4);         
        pU8++;
                                                
        *pU8 = *pIn++ << 4;            
        *pU8 |= ((*pIn & 0x1F) >> 1);         
        pU8++;
                                                
        *pU8 = *pIn++ << 7;            
        *pU8 |= ((*pIn++ & 0x1F) << 2);         
        *pU8 |= ((*pIn & 0x1F) >> 3);         
        pU8++;
                                                
        *pU8 = *pIn++ << 5;            
        *pU8 |= (*pIn++ & 0x1F);
        pU8++;
    }                                           

    // remainder loop
    if (inCount = num % 8)
    {
        for (i=0; i<1; i++)
        {
            *pU8 = *pIn++ << 3;   
            if (--inCount == 0)
                break;
            *pU8 |= ((*pIn & 0x1F) >> 2);         
            pU8++;
                                                
            *pU8 = *pIn++ << 6;            
            if (--inCount == 0)
                break;
            *pU8 |= ((*pIn++ & 0x1F) << 1);         
            if (--inCount == 0)
                break;
            *pU8 |= ((*pIn & 0x1F) >> 4);         
            pU8++;
                                                
            *pU8 = *pIn++ << 4;            
            if (--inCount == 0)
                break;
            *pU8 |= ((*pIn & 0x1F) >> 1);         
            pU8++;
                                                
            *pU8 = *pIn++ << 7;            
            if (--inCount == 0)
                break;
            *pU8 |= ((*pIn++ & 0x1F) << 2);         
            if (--inCount == 0)
                break;
            *pU8 |= ((*pIn & 0x1F) >> 3);         
            pU8++;
                                                
            *pU8 = *pIn++ << 5;            
        } 
    }
}
