/*
 * Copyright (c) 2004-2007, Adaptive Digital Technologies, Inc.
 *
 * File Name: Pcm2Pcm.c
 *
 * Description:
 *   This file contains functions to support PCM To PCM type channels.
 *
 * Version: 1.0
 *
 * Revision History:
 *
 *   5/2005  - Add noise suppression
 *   4/21/04 - Initial release.
 *   2/2007  - Merged C54 and C64
 *
 */
   
// Application related header files.
#include "GpakDefs.h"
#include "GpakErrs.h"
#include "GpakExts.h"
#include "GpakHpi.h"
#include "sysconfig.h"
#include "GpakDma.h"


extern ADT_Bool CheckAecMemory(ADT_UInt16 FrameSize, ADT_UInt32 SampleRate);

extern NCAN_Channel_t  *NoiseSuppressInstance[][2];

extern ADT_UInt16 NoiseSuppress (NSuppress_t *Noise, ADT_PCM16 **workPtr,
                          CircBufInfo_t *outBuffer, int SampsPerFrame);

#define AEchoBit  0x8000
#define BEchoBit  0x4000
#define ANoiseBit 0x2000
#define BNoiseBit 0x1000

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ ProcCfgPcm2PcmMsg - Process a Configure PCM To PCM Channel message.
//
// FUNCTION
//   This function performs PCM To PCM channel type dependent processing for a
//   Configure Channel message.
//
//  Word:Bits
//     0:ff00    -  9 - Channel setup
//     1:ff00    -  Channel index
//     1:00ff    -  2 channel type PCM to PCM
//     2:0700    -  A- In Port ID
//     2:f8ff    -  A- In Slot ID
//     3:0700    -  A- Out Port ID  (host A, DSP B)
//     3:f8ff    -  A- Out Slot ID  (host A, DSP B)
//     4:0700    -  B- In Port ID
//     4:f8ff    -  B- In Slot ID
//     5:0700    -  B- Out Port ID  (host B, DSP A)
//     5:f8ff    -  B- Out Slot ID  (host B, DSP A)
//     6:8000    -  A- PCM Echo Cancel
//     6:4000    -  B- PCM Echo Cancel
//     6:2000    -  A- Acoustic Echo Cancel
//     6:1000    -  B- Acoustic Echo Cancel
//     6:0800    -  A- AGC enable
//     6:0400    -  B- AGC enable
//     6:0200    -  VoiceChannel enable
//     6:00FF    -  Frame rate (half MS units)
//     7:FF00    -  A- McASP Input Pin   (obsolete)
//     7:00FF    -  B- McASP Input Pin   (obsolete)
//     8:FF00    -  A- McASP Output Pin  (obsolete)
//     8:00FF    -  B- McASP Output Pin  (obsolete)
//     9:FFE0    -  A- tone type
//     9:0010    -  A- NoiseSuppress
//     10:FFE0   -  B- tone type
//     10:0010   -  B- NoiseSuppress
//
// RETURNS
//  Return status for reply message.
//}
GPAK_ChannelConfigStat_t ProcCfgPcm2PcmMsg (ADT_UInt16 *pCmd, chanInfo_t *chan) {

   int PcmAvailable, AECAvailable;      // number of Echo Cancellers available
   pcm2pkt_t *ASide;
   pkt2pcm_t *BSide;

   GPAK_ChannelConfigStat_t toneStatus;
   int toneDetectCnt, toneGenCnt;
   int mfTones, cedTones, cngTones, arbTones;
   ADT_UInt16 agcInstanceCount;
   ADT_UInt16 ASideGen, BSideGen;
   ADT_UInt16 *pCmd_ = pCmd;

   ASide = &(chan->pcmToPkt);
   BSide = &(chan->pktToPcm);

   channelAllocsNull (chan, ASide, BSide);

   pCmd = &pCmd[2];
   ASide->Coding = NullCodec;
   ASide->InSerialPortId  = (GpakSerialPort_t) UnpackPort (*pCmd);
   ASide->InSlotId        =                    UnpackSlot (*pCmd);

   pCmd++;  // [3]
   BSide->OutSerialPortId = (GpakSerialPort_t) UnpackPort (*pCmd);
   BSide->OutSlotId       =                    UnpackSlot (*pCmd);

   pCmd++;  // [4]
   BSide->Coding = NullCodec;
   BSide->InSerialPortId  = (GpakSerialPort_t) UnpackPort (*pCmd);
   BSide->InSlotId_MuxFact =                   UnpackSlot (*pCmd);

   pCmd++;  // [5]
   ASide->OutSerialPortId = (GpakSerialPort_t) UnpackPort (*pCmd);
   ASide->OutSlotId_MuxFact =                  UnpackSlot (*pCmd);


   //
   // Select acoustic or PCM echo canceller giving priority to acoustic echo canceller
   //
   pCmd++;     // [6]
   // Frame rate in samples before api version 603
   // Frame rate in 1/2 ms after api version 603
   if (ApiBlock.ApiVersionId < 0x0603u)
      ASide->SamplesPerFrame = Byte0 (*pCmd);
   else
      ASide->SamplesPerFrame = halfMSToSamps (Byte0 (*pCmd));

   if (!ValidFrameSize (ASide->SamplesPerFrame))
      return Cc_InvalidFrameSize;

   BSide->SamplesPerFrame = ASide->SamplesPerFrame;

   if ((*pCmd & 0x0200) == 0) {               // Voice channel
      chan->VoiceChannel = Disabled;
      return Cc_Success;
   }
   chan->VoiceChannel = Enabled;

   PcmAvailable = (int) (sysConfig.numPcmEcans - NumPcmEcansUsed);
   AECAvailable = (int) (sysConfig.AECInstances - NumAECEcansUsed);

   if ((*pCmd & 0x2000) == 0) {               // Acoustic Echo cancellation
      ASide->AECEchoCancel = Disabled;
   } else if (0 < AECAvailable) {
      // Acoustic echo canceller selected and available.
      *pCmd &= 0x7fff;                        // Turn off request for PCM canceller.
      if (!CheckAecMemory(ASide->SamplesPerFrame, chan->ProcessRate))
         return Cc_InsuffBuffsAvail;
      ASide->AECEchoCancel = Enabled;
      AECAvailable--;
   } else if ((*pCmd & 0x8000) == 0)
      return Cc_AECEchoCancelNotCfg;          // No acoustic echo canceller available

   if ((*pCmd & 0x8000) == 0) {               // PCM Line echo cancellation
      ASide->EchoCancel = Disabled;
   } else if (0 < PcmAvailable) {
      ASide->EchoCancel = Enabled;
      PcmAvailable--;
   } else
      return Cc_PcmEchoCancelNotCfg;

   if ((*pCmd & 0x1000) == 0) {               // Acoustic Echo cancellation
      BSide->AECEchoCancel = Disabled;        
   } else {
      return Cc_AECEchoCancelNotCfg;          // B-Side AEC not permitted.
   } 
   
   if ((*pCmd & 0x4000) == 0) {               // PCM Line echo cancellation
      BSide->EchoCancel = Disabled;
   } else if (0 < PcmAvailable) {
      BSide->EchoCancel = Enabled;
   } else
      return Cc_PcmEchoCancelNotCfg;

   agcInstanceCount = 0;
   if ((*pCmd & 0x0800) == 0)                // A-side AGC
      ASide->AGC = Disabled;
   else {
      agcInstanceCount++;
      ASide->AGC = Enabled;
   }

   if ((*pCmd & 0x0400) == 0)                // B-side AGC
      BSide->AGC = Disabled;
   else {
      agcInstanceCount++;
      BSide->AGC = Enabled;
   }

   if (numAGCChansAvail < agcInstanceCount) {
      ASide->AGC = Disabled;
      BSide->AGC = Disabled;
      return Cc_InsuffAgcResourcesAvail;
   }


   pCmd++;  // [7]  obsolete
   pCmd++;  // [8]  obsolete
   pCmd++;  // [9]
   ASide->toneTypes = (GpakToneTypes) (*pCmd & 0x0FFF);
   mfTones = cedTones = cngTones = arbTones = 0;
   toneStatus = ValidToneTypes (ASide->toneTypes, &mfTones, &cedTones, &cngTones, &arbTones);
   if (toneStatus != Cc_Success) return toneStatus;
   if (ASide->toneTypes & (Tone_Relay | Tone_Regen)) return Cc_InvalidToneTypesA;

   ASide->NoiseSuppress = (GpakActivation)((*pCmd >> 12) & 0x0001);
   if (ASide->NoiseSuppress) ASide->Noise.Bufr = NoiseSuppressInstance [chan->ChannelId][0];
   else                      ASide->Noise.Bufr = NULL;

   pCmd++;  // [10]
   BSide->toneTypes = (GpakToneTypes) (*pCmd & 0x0FFF);
   toneStatus = ValidToneTypes (BSide->toneTypes, &mfTones, &cedTones, &cngTones, &arbTones);
   if (toneStatus == Cc_InvalidToneTypesA) return Cc_InvalidToneTypesB;
   if (toneStatus != Cc_Success)           return toneStatus;
   if (BSide->toneTypes & (Tone_Relay | Tone_Regen)) return Cc_InvalidToneTypesB;

   BSide->NoiseSuppress = (GpakActivation)((*pCmd >> 12) & 0x0001);
   if (BSide->NoiseSuppress) BSide->Noise.Bufr = NoiseSuppressInstance [chan->ChannelId][1];
   else                      BSide->Noise.Bufr = NULL;

   if ((BSide->NoiseSuppress || BSide->NoiseSuppress) && !SysAlgs.noiseSuppressEnable)
      return Cc_NoiseSuppressionNotConfigured;

   toneGenCnt = 0;
   if (ASide->toneTypes & Tone_Generate) toneGenCnt++;
   if (BSide->toneTypes & Tone_Generate) toneGenCnt++;
   if (numToneGenChansAvail < toneGenCnt)
        return Cc_InsuffTGResourcesAvail;

   toneDetectCnt = 0;
   if (ASide->toneTypes & ToneDetect) toneDetectCnt++;
   if (BSide->toneTypes & ToneDetect) toneDetectCnt++;
   if (ToneDetectorsAvail < toneDetectCnt)
        return Cc_InsuffTDResourcesAvail;

   // put the tonegen bits in the variable corresponding to the device Side
   ASideGen = ASide->toneTypes & Tone_Generate;
   BSideGen = BSide->toneTypes & Tone_Generate;
   ASide->toneTypes &= ~Tone_Generate;
   BSide->toneTypes &= ~Tone_Generate;
   BSide->toneTypes |= ASideGen;
   ASide->toneTypes |= BSideGen;

   if (ApiBlock.ApiVersionId <= 0x0401u) return Cc_Success;

   //----------------------------------------------------------
   // API Version 402 and better.
   //    Caller ID and Gain settings
   pCmd = pCmd_;

   ASide->CIDMode = CidDisabled;
   BSide->CIDMode = CidDisabled;

   if (((pCmd[11] >> 2) & CidReceive) && (numRxCidChansAvail == 0))
      return Cc_RxCIDNotAvail;

   if (((pCmd[11] >> 2) & CidTransmit) && (numTxCidChansAvail == 0))
      return Cc_TxCIDNotAvail;

   if ((pCmd[11] & CidReceive) && (numRxCidChansAvail == 0))
      return Cc_RxCIDNotAvail;

   if ((pCmd[11] & CidTransmit) && (numTxCidChansAvail == 0))
      return Cc_TxCIDNotAvail;

   // A-side Cid Rx
   if ((pCmd[11] >> 2) & CidReceive)
      ASide->CIDMode  |= CidReceive;

   // A-side Cid Tx
   if ((pCmd[11] >> 2) & CidTransmit)
      BSide->CIDMode  |= CidTransmit;

   // B-side Cid Rx
   if (pCmd[11] & CidReceive)
      BSide->CIDMode  |= CidReceive;

   // B-side Cid Tx
   if (pCmd[11] & CidTransmit)
      ASide->CIDMode  |= CidTransmit;

   ASide->Gain.ToneGenGainG1  = pCmd[12];
   ASide->Gain.OutputGainG2   = pCmd[13];
   ASide->Gain.InputGainG3    = pCmd[14];
   ClampScaleGain(&ASide->Gain);

   BSide->Gain.ToneGenGainG1  = pCmd[15];
   BSide->Gain.OutputGainG2   = pCmd[16];
   BSide->Gain.InputGainG3    = pCmd[17];
   ClampScaleGain (&BSide->Gain);

   ASide->SideTone = Disabled;
   BSide->SideTone = Disabled;

   // Return with an indication of success.
   return Cc_Success;
}

ADT_UInt16 ProcPcm2PcmStatMsg (ADT_UInt16 *pReply, chanInfo_t *chan) {

   pkt2pcm_t *BSide;
   pcm2pkt_t *ASide;
   ADT_UInt16 ATone, BTone, AToneGen, BToneGen;
   ADT_UInt16 CidA; 
   ADT_UInt16 CidB; 

   ASide = &(chan->pcmToPkt);
   BSide = &(chan->pktToPcm);

   pReply[7]  = PackPortandSlot (ASide->InSerialPortId,  ASide->InSlotId);
   pReply[8]  = PackPortandSlot (BSide->OutSerialPortId, BSide->OutSlotId);
   pReply[9]  = PackPortandSlot (BSide->InSerialPortId,  BSide->InSlotId_MuxFact);
   pReply[10] = PackPortandSlot (ASide->OutSerialPortId, ASide->OutSlotId_MuxFact);
   pReply[11] = (ADT_UInt16)
               ((ASide->EchoCancel    << 15)           | ((BSide->EchoCancel    << 14) & 0x4000) |
               ((ASide->AECEchoCancel << 13) & 0x2000) | ((BSide->AECEchoCancel << 12) & 0x1000) |
               ((ASide->AGC           << 11) & 0x0800) | ((BSide->AGC           << 10) & 0x0400) |
               ((chan->VoiceChannel   << 9) & 0x0200) | (sampsToHalfMS (ASide->SamplesPerFrame) & 0xFF));
   pReply[12] = 0;  // Obsolte pin IDs
   pReply[13] = 0;  // Obsolte pin IDs

   ATone = ASide->toneTypes;
   BTone = BSide->toneTypes;
   AToneGen = BTone & Tone_Generate;
   BToneGen = ATone & Tone_Generate;
   ATone &= ~Tone_Generate;
   BTone &= ~Tone_Generate;
   ATone |= AToneGen;
   BTone |= BToneGen;

   pReply[14] = (ADT_UInt16) ((ATone & 0xFFF) | ((ASide->NoiseSuppress << 12) & 0x1000));
   pReply[15] = (ADT_UInt16) ((BTone & 0xFFF) | ((BSide->NoiseSuppress << 12) & 0x1000));

   if (ApiBlock.ApiVersionId <= 0x0401u) return 16;


   // API Version 402 and better.
   //    Caller ID and Gain settings
   CidA = (ADT_UInt16) ((ASide->CIDMode & CidReceive) | (BSide->CIDMode & CidTransmit));
   CidB = (ADT_UInt16) ((BSide->CIDMode & CidReceive) | (ASide->CIDMode & CidTransmit));
   pReply[16]   = ((CidA << 2)  | CidB);

   if (ASide->Gain.ToneGenGainG1 != (ADT_Int16)ADT_GAIN_MUTE)
        pReply[17] = (ADT_Int16) ASide->Gain.ToneGenGainG1/10;
   else
        pReply[17] = (ADT_Int16) ASide->Gain.ToneGenGainG1;

   if (ASide->Gain.OutputGainG2 != (ADT_Int16)ADT_GAIN_MUTE)
        pReply[18] = (ADT_Int16) ASide->Gain.OutputGainG2/10; 
   else 
        pReply[18] = (ADT_Int16) ASide->Gain.OutputGainG2; 

   if (ASide->Gain.InputGainG3 != (ADT_Int16)ADT_GAIN_MUTE)
        pReply[19] = (ADT_Int16) ASide->Gain.InputGainG3/10;  
   else
        pReply[19] = (ADT_Int16) ASide->Gain.InputGainG3;  

   if (BSide->Gain.ToneGenGainG1 != (ADT_Int16)ADT_GAIN_MUTE)
        pReply[20] = (ADT_Int16) BSide->Gain.ToneGenGainG1/10;
   else
        pReply[20] = (ADT_Int16) BSide->Gain.ToneGenGainG1;

   if (BSide->Gain.OutputGainG2 != (ADT_Int16)ADT_GAIN_MUTE)
        pReply[21] = (ADT_Int16) BSide->Gain.OutputGainG2/10; 
   else 
        pReply[21] = (ADT_Int16) BSide->Gain.OutputGainG2; 

   if (BSide->Gain.InputGainG3 != (ADT_Int16)ADT_GAIN_MUTE)
        pReply[22] = (ADT_Int16) BSide->Gain.InputGainG3/10;  
   else
        pReply[22] = (ADT_Int16) BSide->Gain.InputGainG3;  
   return 23;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ FramePcmB2PcmA - Side B to side A framing for a PCM To PCM type channel.
//
// FUNCTION
//   Performs a PCM To PCM channel's transfer in 'forward' direction
//
// Transfers
//          
//      DMACopy
//         Serial port [PCM] --> BSide.inbuffer [PCM]
//                                (PktBufferPool[0][chan])
//
//      BSide.inbuffer [PCM] --> Task.InWork [PCM]
//      (PktBufferPool[0][chan])
//
//      pktToPcm.EchoCancel == ENABLED
//          BSide.EcFar [PCM] --> Task.Scratch [PCM]
//          (BulkDelayPool[chan])
//
//          Task.InWork [PCM] + Task.Scratch[PCM] --> Task.InWork [PCM] 
//      
//      Task.InWork[PCM] --> BSide.outbuffer [PCM]
//                             (PcmOutBufferPool[chan])
//
//      DMACopy
//          BSide.outbuffer [PCM] --> Serial port [PCM]
//            (PcmOutBufferPool[chan])
//
// Inputs
//       chan - Channel data structure pointer
//   SampsPerFrame - Samples per frame
//
// Work buffers
//    pcmBuff    - Linear pcm work buffer (Task.InWork)
//    Dummy      - Not used  (Task,OutWork)
//    ecFarWork - echo cancellation work buffer  (Task.Scratch)
//
// RETURNS
//   nothing
//}
void FramePcmB2PcmA (chanInfo_t *chan, ADT_PCM16 *pcmBuff, ADT_PCM16 *dtmfWork, 
                     ADT_PCM16 *ecFarWork, int SampsPerFrame) {

   short *DTMFBuff;     // pointer to PCM data for DTMF processing

   pkt2pcm_t *BSide;
   pcm2pkt_t *ASide;
   toneDetInfo_t toneInf;
   int SampsPerFrame8k;   // narrow band sample count
   CircBufInfo_t pcmAInfo;

   BSide = &chan->pktToPcm;
   ASide = &chan->pcmToPkt;

   SampsPerFrame8k = SampsPerFrame;

   logTime (0x400B0000ul | chan->ChannelId);

   // B-->A processing
   // Copy sidetone audio if wanted
   if (Enabled == BSide->SideTone) { // B- enabled?
        memcpy (&ASide->savedPktPcmInBuffer,BSide->inbuffer, sizeof(CircBufInfo_t));
   }
   if (Enabled == ASide->SideTone) { // A- enabled?
        memcpy (&pcmAInfo, ASide->activeInBuffer, sizeof(CircBufInfo_t));
   }

   //-----------------------------------------------------------
   //  Input.   TDM to linear, PCM to L16, Downconvert.
   fromTDMCirc (chan, BDevice, pcmBuff, &SampsPerFrame, &BSide->rateCvt);

   play_record(chan->ChannelId, pcmBuff, SampsPerFrame, (PLAYREC_C | PLAYREC_PLAY));

	// Type 2CID, on Rcv path, Tx CID detects Ack tone from other from TE
	if(sysConfig.type2CID && BSide->cidInfo.type2CID && (BSide->CIDMode & CidTransmit)) {
	   txCid2RcvProcess(chan->ChannelId, BDevice, &BSide->cidInfo, pcmBuff, SampsPerFrame);
	}

   if (BSide->CIDMode & CidReceive)
      rxCidProcess (chan->ChannelId, BDevice, &BSide->cidInfo, pcmBuff, SampsPerFrame);

   if (Enabled == BSide->EchoCancel) {
      logTime (0x400B0200ul | chan->ChannelId);

      // Perform echo cancellation for far side. Far buffer is bulk delay.
      DTMFBuff = (short *) dtmfWork;
      copyCircToLinear (&BSide->ecFarPtr, ecFarWork, SampsPerFrame);
      g168Capture (BSide->EcIndex, pcmBuff, ecFarWork, SampsPerFrame);
      LEC_ADT_g168Cancel (BSide->pG168Chan, (short *) pcmBuff, (short *) ecFarWork, DTMFBuff);
      g168PostCapture (BSide->EcIndex, pcmBuff, ecFarWork, SampsPerFrame);
   }

   // Apply Pcm Input Gain G3
   ADT_GainBlockWrapper (BSide->Gain.InputGainG3, pcmBuff, pcmBuff, SampsPerFrame);

   // Down convert to DTMF buffer for tone detection
   if ((chan->ProcessRate == 16000) && (BSide->toneTypes & ToneDetectAll)) {
      DTMFBuff = (short *) dtmfWork;
      AppErr ("Rate err", BSide->rateCvt.DwnFrame != SampsPerFrame);
      SampsPerFrame8k = downConvert (BSide->rateCvt.DwnInst, pcmBuff, SampsPerFrame, DTMFBuff);
   } else if (Enabled != BSide->EchoCancel) {
      DTMFBuff = (short *) pcmBuff;
      SampsPerFrame8k = SampsPerFrame;
   }

   // perform tone detection
   if (BSide->toneTypes & ToneDetectAll) {
      toneInf.ChannelId   = chan->ChannelId;
      toneInf.deviceSide  = BDevice;
      toneInf.toneTypes   = BSide->toneTypes;
      toneInf.FrameSize8k = SampsPerFrame8k;

      toneInf.TDInstances = &BSide->TDInstances;
      toneInf.DTMFBuff    = DTMFBuff;
      if (chan->ProcessRate == 8000)
         toneInf.pcmBuff = pcmBuff;   // For tone squelch
      else
         toneInf.pcmBuff = DTMFBuff;

      toneInf.Rec = &(BSide->recB);
      logTime (0x400B0300ul | chan->ChannelId);
      toneDetect (&toneInf, BSide->FrameTimeStamp, chan->ProcessRate, SampsPerFrame);
  }

   if (BSide->AGC == Enabled) {
      // Perform AGC on pcm buffer
      logTime (0x400B0500ul | chan->ChannelId);
      BSide->AGCPower = AGC_ADT_run (BSide->agcPtr, (short *) pcmBuff, SampsPerFrame, 0, 0, (short *) pcmBuff, NULL);
   }

   // Perform noise suppression on the linear buffer (pInWork) 
   if (BSide->Noise.Bufr != NULL) {
      logTime (0x400B0600ul | chan->ChannelId);
      SampsPerFrame = NoiseSuppress (&(BSide->Noise), (short **) &pcmBuff, &(BSide->outbuffer), SampsPerFrame);
   }

   // copy pcm samples into scratch buffer (ecFarWork) for voice recording
   if (BSide->recB.state & Record) {
      i16cpy (ecFarWork, pcmBuff, SampsPerFrame);
      voiceRecord (&BSide->recB, ecFarWork, dtmfWork, SampsPerFrame, BDevice, chan->ChannelId);
   }

   // generate tones and apply gain
   logTime (0x400B0800ul | chan->ChannelId);
   ToneGenerate (&BSide->TGInfo, pcmBuff, SampsPerFrame);
   if (BSide->TGInfo.active)
      ADT_GainBlockWrapper (ASide->Gain.ToneGenGainG1, pcmBuff, pcmBuff, SampsPerFrame);


   //-----------------------------------------------------------
   //  Output
   if (BSide->OutSerialPortId == SerialPortNull) return;

   // Apply Pcm Output Gain G2
   ADT_GainBlockWrapper (ASide->Gain.OutputGainG2, pcmBuff, pcmBuff, SampsPerFrame);

   if (BSide->playA.state & PlayBack)
      voicePlayback (&BSide->playA, pcmBuff, dtmfWork, SampsPerFrame, ADevice, chan->ChannelId);

   if (Enabled == ASide->SideTone) { // A- enabled?
		int i;
	    copyCircToLinear (&pcmAInfo, dtmfWork, SampsPerFrame);
		ADT_GainBlockWrapper (ASide->SideToneDB, dtmfWork, dtmfWork, SampsPerFrame);
		for (i = 0; (i < SampsPerFrame); i ++) {
			pcmBuff[ i ] = (_sadd2(pcmBuff[ i ], dtmfWork[ i ]) & 0xffff);
		}
   }

   if (ASide->CIDMode & CidTransmit)
      txCidProcess (chan->ChannelId, ADevice, &ASide->cidInfo, pcmBuff, SampsPerFrame);

	// Type 2 CID, on Rx path, seding the Ack tone to CO side
	if(sysConfig.type2CID && ASide->cidInfo.type2CID && (ASide->CIDMode & CidReceive)) {
	   rxCid2SndProcess(chan->ChannelId, ADevice, &(ASide->cidInfo), pcmBuff, SampsPerFrame);
	}

   play_record(chan->ChannelId, pcmBuff, SampsPerFrame, (PLAYREC_B | PLAYREC_PLAY));

   if (Enabled == ASide->AECEchoCancel) {
      // Copy AEC output to FarData for delayed output by PcmA2PcmB.
      i16cpy (ASide->ecFarPtr.pBufrBase, pcmBuff, SampsPerFrame);
   } else {
      //  TDM output
      toTDMCirc (chan, BDevice, pcmBuff, SampsPerFrame, &BSide->rateCvt);
    } 

   logTime (0x400B0F00ul | chan->ChannelId);
   return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ FramePcmA2PcmB - PCM to Host framing for a PCM To PCM type channel.
//
// FUNCTION
//   Performs a PCM To PCM channel's transfer in 'reverse' direction
//
//
// Transfers
//          
//      DMACopy
//         Serial port [PCM] --> ASide.currentInBuffer [PCM]
//                                (PcmInBufferPool[chan])
//
//      ASide.currentInBuffer [PCM] --> Task.InWork [PCM]
//         (PcmInBufferPool[chan])        
//
//      ASide.EchoCancel == ENABLED
//          ASide.EcFar [PCM] --> Task.Scratch [PCM]
//          (PcmOutBufferPool[chan])
//
//          Task.InWork [PCM] + Task.Scratch [PCM] --> Task.InWork [PCM]
//
//      BSide.EchoCancel == ENABLED
//          Task.InWork [PCM] --> ASide.ecBulkDelay
//                                 (BulkDelayPool[chan])
//      
//      Task.InWork [PCM] --> ASide.outbuffer [PCM]
//                             (PktBufferPool[1][chan])
//
//      DMACopy
//          ASide.outbuffer [PCM] --> Serial port [PCM]
//            (PktBufferPool[1][chan])
//
//
// Inputs
//       chan - Channel data structure pointer
//   SampsPerFrame - Samples per frame
//
// Work buffers
//    pcmBuff    - Linear pcm work buffer (Task.InWork)
//    Dummy      - Unused (Task.OutWork)
//    ecFarWork - echo cancellation work buffer (Task.Scratch)
//
// RETURNS
//   nothing
//}
void FramePcmA2PcmB (chanInfo_t *chan, ADT_PCM16 *pcmBuff, ADT_PCM16 *dtmfWork,
                      ADT_PCM16 *ecFarWork, int SampsPerFrameTDM) {

   short *DTMFBuff;             // pointer to PCM data for DTMF processing

   pcm2pkt_t *ASide;
   pkt2pcm_t *BSide;
   toneDetInfo_t toneInf;
   int slipCnt;
   int SampsPerFrame8k, SampsPerFrame;

   ASide = &chan->pcmToPkt;
   BSide = &chan->pktToPcm;

   logTime (0x400A0000ul | chan->ChannelId);

   SampsPerFrame = SampsPerFrameTDM;

   // A-->B processing
   //-----------------------------------------------------------
   //   TDM Input

   // If extra samples were added by TDM, adjust ecFar accordingly
   slipCnt = fromTDMCirc (chan, ADevice, pcmBuff, &SampsPerFrame, &ASide->rateCvt);
   if (slipCnt && Enabled == ASide->EchoCancel) {
      addSlipToCirc (&ASide->ecFarPtr, slipCnt);
   }

   play_record(chan->ChannelId, pcmBuff, SampsPerFrame, (PLAYREC_A | PLAYREC_PLAY));

	// Type 2CID, on Rcv path, Tx CID detects Ack tone from other from TE
	if(sysConfig.type2CID && ASide->cidInfo.type2CID && (ASide->CIDMode & CidTransmit)) {
	   txCid2RcvProcess(chan->ChannelId, ADevice, &ASide->cidInfo, pcmBuff, SampsPerFrame);
	}
   if (ASide->CIDMode & CidReceive) 
      rxCidProcess (chan->ChannelId, ADevice, &ASide->cidInfo, pcmBuff, SampsPerFrame);

   DTMFBuff = NULL;
   if (Enabled == ASide->EchoCancel) {
      DTMFBuff = (short *) dtmfWork;

      logTime (0x400A0200ul | chan->ChannelId);

      // Perform echo cancellation for far side. Far buffer is TDM outbuffer.
      SampsPerFrame = SampsPerFrameTDM;
      fromTDMCirc (chan, ASideECFar, ecFarWork, &SampsPerFrame, &BSide->rateCvt);
      g168Capture (ASide->EcIndex, pcmBuff, ecFarWork, SampsPerFrame);
      LEC_ADT_g168Cancel (ASide->pG168Chan, (short *) pcmBuff, (short *) ecFarWork, (short *) DTMFBuff);
      g168PostCapture (ASide->EcIndex, pcmBuff, ecFarWork, SampsPerFrame);

   } else if (Enabled == ASide->AECEchoCancel) {
      runAEC (chan, pcmBuff, ecFarWork, SampsPerFrame, &BSide->rateCvt);
   }

   // Apply Pcm Input Gain G3
   ADT_GainBlockWrapper (ASide->Gain.InputGainG3, pcmBuff, pcmBuff, SampsPerFrame);

   // Down convert to DTMF buffer for tone detection
   if ((chan->ProcessRate == 16000) && (ASide->toneTypes & ToneDetectAll)) {
      DTMFBuff = (short *) dtmfWork;
      AppErr ("Rate err", ASide->rateCvt.DwnFrame != SampsPerFrame);
      SampsPerFrame8k = downConvert (ASide->rateCvt.DwnInst, pcmBuff, SampsPerFrame, DTMFBuff);
   } else {
      DTMFBuff = (short *) pcmBuff;
      SampsPerFrame8k = SampsPerFrame;
   }

   // perform tone detection on narrow band DTMF buffer
   if (ASide->toneTypes & ToneDetectAll) {
      toneInf.ChannelId   = chan->ChannelId;
      toneInf.deviceSide  = ADevice;
      toneInf.FrameSize8k = SampsPerFrame8k;
      toneInf.toneTypes   = ASide->toneTypes;

      toneInf.TDInstances = &ASide->TDInstances;
      toneInf.DTMFBuff    = DTMFBuff;
      if (chan->ProcessRate == 8000)
         toneInf.pcmBuff = pcmBuff;   // For tone squelch
      else
         toneInf.pcmBuff = DTMFBuff;

      toneInf.Rec = &ASide->recA;
      logTime (0x400A0300ul | chan->ChannelId);
      toneDetect (&toneInf, ASide->FrameTimeStamp, chan->ProcessRate, SampsPerFrame);
   }


   if (ASide->AGC == Enabled) {
      // Perform AGC on pcm buffer
      logTime (0x400A0500ul | chan->ChannelId);
      ASide->AGCPower = AGC_ADT_run (ASide->agcPtr, (short *) pcmBuff, SampsPerFrame, 0, 0, (short *) pcmBuff, NULL);
   }

   // Perform noise suppression on the linear buffer (pInWork) 
   //
   // First noise frame copied directly into outbuffer if remnant exists
   if (ASide->Noise.Bufr != NULL) {
      logTime (0x400A0600ul | chan->ChannelId);
      SampsPerFrame = NoiseSuppress (&(ASide->Noise), (short **) &pcmBuff, ASide->outbuffer, SampsPerFrame);
   }

   // Voice record - copy pcm samples into scratch buffer ecFarWork
   if (ASide->recA.state & Record) {
      i16cpy (ecFarWork, pcmBuff, SampsPerFrame);
      voiceRecord (&ASide->recA, ecFarWork, dtmfWork, SampsPerFrame, ADevice, chan->ChannelId);
   }

   // generate tones and apply gain
   logTime (0x400A0800ul | chan->ChannelId);
   ToneGenerate (&ASide->TGInfo, pcmBuff, SampsPerFrame);
   if (ASide->TGInfo.active)
      ADT_GainBlockWrapper (BSide->Gain.ToneGenGainG1, pcmBuff, pcmBuff, SampsPerFrame);

   //-----------------------------------------------------------
   //  Output
   if (ASide->OutSerialPortId == SerialPortNull) return;

   // Apply Pcm Output Gain G2
   ADT_GainBlockWrapper (BSide->Gain.OutputGainG2, pcmBuff, pcmBuff, SampsPerFrame);

   if (ASide->playB.state & PlayBack)
      voicePlayback (&ASide->playB, pcmBuff, dtmfWork, SampsPerFrame, BDevice, chan->ChannelId);

   if (Enabled == BSide->SideTone) {
	   int i;
	   copyCircToLinear (&ASide->savedPktPcmInBuffer, dtmfWork, SampsPerFrame);
	   ADT_GainBlockWrapper (BSide->SideToneDB, dtmfWork, dtmfWork, SampsPerFrame);
	   for (i = 0; (i < SampsPerFrame); i ++) {
			pcmBuff[ i ] = (_sadd2(pcmBuff[ i ], dtmfWork[ i ]) & 0xffff);
	   }
   }

   if (BSide->CIDMode & CidTransmit)
      txCidProcess (chan->ChannelId, BDevice, &BSide->cidInfo, pcmBuff, SampsPerFrame);

	// Type 2 CID, on Tx path, RxCid seding the Ack tone to CO side
	if(sysConfig.type2CID && BSide->cidInfo.type2CID && (BSide->CIDMode & CidReceive)) {
	   rxCid2SndProcess(chan->ChannelId, BDevice, &(BSide->cidInfo), pcmBuff, SampsPerFrame);
	}

   play_record(chan->ChannelId, pcmBuff, SampsPerFrame, (PLAYREC_D | PLAYREC_PLAY));

   // Copy to echo canceller's bulk delay buffer
   if (Enabled == BSide->EchoCancel) {
      logTime (0x400A0B00ul | chan->ChannelId);
      copyLinearToCirc (pcmBuff, &BSide->ecFarPtr, SampsPerFrame);
   }

   // Upconvert for TDM output
   toTDMCirc (chan, ADevice, pcmBuff, SampsPerFrame, &ASide->rateCvt);

   logTime (0x400A0F00ul | chan->ChannelId);
   return;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ SetupPcm2PcmChan - Setup a PCM To PCM channel.
//
// FUNCTION
//   This function performs channel setup initialization for a PCM To PCM type
//   channel.
//
// RETURNS
//   nothing
//}
void SetupPcm2PcmChan (chanInfo_t *chan) {

   pcm2pkt_t *ASide;
   pkt2pcm_t *BSide;   // pointer to channel's PCM B to PCM A data
   int SampsPerFrame8k;  // Narrowband samples per frame;

   // Get pointers to the channel's PCM to Packet and Packet to PCM data.
   ASide = &chan->pcmToPkt;
   BSide = &chan->pktToPcm;

   // Algorithms requiring 8000 kHz
   if (ASide->CIDMode || ASide->NoiseSuppress || ASide->EchoCancel ||
       BSide->CIDMode || BSide->NoiseSuppress || BSide->EchoCancel)
      chan->ProcessRate = 8000;

   SampsPerFrame8k = ASide->SamplesPerFrame;
   if (sysConfig.samplesPerMs == 16)
      SampsPerFrame8k /= 2;

    // Allocate Pcm, Pkt and BulkDelay Buffers
   AllocBuffs (chan);

   // Initialize PCM circular buffers
   ASide->activeInBuffer = &ASide->inbuffer;
   memset (ASide->inbuffer.pBufrBase,   0, ASide->inbuffer.BufrSize * sizeof (ADT_PCM16)); 
   memset (ASide->outbuffer->pBufrBase, 0, ASide->outbuffer->BufrSize * sizeof (ADT_PCM16)); 

   memset (BSide->outbuffer.pBufrBase, 0, BSide->outbuffer.BufrSize * sizeof (ADT_PCM16)); 
   memset (BSide->inbuffer->pBufrBase, 0, BSide->inbuffer->BufrSize * sizeof (ADT_PCM16)); 


   // Initialize enabled echo Cancellers.
   ASide->ecBulkDelay.SlipSamps = 0;
   ASide->ecFarPtr.SlipSamps = 0;
   BSide->ecFarPtr.SlipSamps = 0;

   if (ASide->AECEchoCancel)   {
      ASide->pG168Chan = AllocEchoCanceller (ACOUSTIC, ASide->SamplesPerFrame,
                                             &ASide->EcIndex, chan->ProcessRate);
      if (ASide->pG168Chan == NULL)
      {
	     ASide->AECEchoCancel = Disabled;
      }
      else
      {
         ASide->ecFarPtr.pBufrBase = pAECFarPool[ASide->EcIndex & 0xFF];
         ASide->ecFarPtr.BufrSize  = ASide->SamplesPerFrame;
         memset (ASide->ecFarPtr.pBufrBase, 0,  ASide->SamplesPerFrame * sizeof (ADT_PCM16));
      }

   } else if (ASide->EchoCancel)   {
      ASide->EcIndex = chan->CoreID;
      ASide->ecFarPtr.pBufrBase = BSide->outbuffer.pBufrBase;
      ASide->ecFarPtr.BufrSize  = BSide->outbuffer.BufrSize;
      ASide->pG168Chan = AllocEchoCanceller (G168_PCM, ASide->SamplesPerFrame,
                                             &ASide->EcIndex, chan->ProcessRate);
   }

   if (BSide->EchoCancel)   {
      memset (ASide->ecBulkDelay.pBufrBase, 0, ASide->ecBulkDelay.BufrSize * sizeof (ADT_PCM16));

      BSide->EcIndex = chan->CoreID;
      BSide->ecFarPtr.pBufrBase = ASide->ecBulkDelay.pBufrBase;
      BSide->ecFarPtr.BufrSize  = ASide->ecBulkDelay.BufrSize;
      BSide->pG168Chan = AllocEchoCanceller (G168_PCM, BSide->SamplesPerFrame,
                                             &BSide->EcIndex, chan->ProcessRate);
   }

   //  Set the initial noise offset to always force a full vocoder frame of output
   //  when the noise frames do not align with the vocoder frames
   if (ASide->Noise.Bufr != NULL) {
       ASide->Noise.Offset = NCAN_FRAME_SIZE - (SampsPerFrame8k % NCAN_FRAME_SIZE);
       if (ASide->Noise.Offset == NCAN_FRAME_SIZE) ASide->Noise.Offset = 0;

       InitNoiseSuppression (ASide->Noise.Bufr, &NCAN_Params);
   }

   if (BSide->Noise.Bufr != NULL) {
       BSide->Noise.Offset = NCAN_FRAME_SIZE - (SampsPerFrame8k % NCAN_FRAME_SIZE);
       if (BSide->Noise.Offset == NCAN_FRAME_SIZE)  BSide->Noise.Offset = 0;

       InitNoiseSuppression (BSide->Noise.Bufr, &NCAN_Params);
   }
    
   // init ASide tone detect
   if ((ASide->toneTypes & ToneDetectAll) != Null_tone) {
      InitToneDetection (ASide->toneTypes, &ASide->TDInstances, 0, ASide->SamplesPerFrame);
   }
   // init BSide tone detect
   if ((BSide->toneTypes & ToneDetectAll) != Null_tone) {
      InitToneDetection (BSide->toneTypes, &BSide->TDInstances, 0, ASide->SamplesPerFrame);
   }

   // Allocate and int AGC
   AllocAGC (chan, ADevice, ASide->AGC);
   AllocAGC (chan, BDevice, BSide->AGC);

   // allocate a tone gen instance if enabled
   AllocToneGen (&(ASide->TGInfo), (ASide->toneTypes & Tone_Generate));
   AllocToneGen (&(BSide->TGInfo), (BSide->toneTypes & Tone_Generate));

   // Allocate CID instances if enabled
   NullCID (&(ASide->cidInfo));
   NullCID (&(BSide->cidInfo));
   AllocCID (chan, ADevice, ASide->CIDMode, BSide->CIDMode);
   AllocCID (chan, BDevice, BSide->CIDMode, ASide->CIDMode);


   dmaInitInfoStruct (chan, ASide->EcIndex, BSide->EcIndex);

   // Allocate sampling rate and initialize sampler rate converter instances
   AllocEncoderSampleRateConv (chan);
   AllocDecoderSampleRateConv (chan);
   return;
}
