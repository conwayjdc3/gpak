//==========================================================================
//          File:  64xRtpNetDelivery.c
//
//   Description:  Initialization and support routines for delivery of RTP packets to the network stack
//
//{==========================================================================
extern void RTPMemInit ();

#include <std.h>
#include <string.h>
#include <stdio.h>
//#include <swi.h>
#include <adt_typedef.h>
#include "GpakDefs.h"
#include "GpakExts.h"
#include "GpakHpi.h"
extern far int fast_rtp_enabled;
extern void   mmCopy( void* pDst, void* pSrc, uint32_t Size );

#ifdef inc_ipStackUser          // compile only if IP stack support exists

#include inc_ipStackUser
extern void NDK_recvncfree( void *hFrag );

#ifdef SINGLE_CORE_STACK

// Get the next RTP packet received on the network.
extern void * getNetRtpRxPkt(
	ADT_UInt16 *pPktLength,		// pointer to packet length variable
	ADT_UInt16 *pChannelId		// pointer to channel Id variable
	);

// Send an RTP packet to a channel.
extern void sendRtpPktToChan(
	ADT_UInt16 channelId,		// channel Id
	ADT_UInt8 *pPktData,		// pointer to packet data
	ADT_UInt16 length			// length of packet data (bytes)
	);
#endif

extern ADT_Bool fastRTPMap(int chanID, IPToSess_t **ipSess);
extern int fastRTPsend(void *rtpBuf, int rtpI8, IPToSess_t *IPMapping);


//} - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//   Frame work interfaces for packet delivery to network statck
//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

extern void RTPMemInit ();
extern int addToIPSession6 (IPToSess_t *new, void *RTPSession, ADT_UInt16 ChanId);
extern int addToIPSession (IPToSess_t *new, void *RTPSession, ADT_UInt16 ChanId);
extern int GetPktFromCircBufferCache (CircBufInfo_t *circ, void *Hdr, int HdrI16, 
                          ADT_UInt16 *Payload, ADT_UInt16 maxPayloadI8);
extern int rtpPortValid (int rtpPort);
extern GpakTestMode RTPLoopBack;
extern void (*RTPPayloadFree) (void *hndl, void*data, ADT_UInt16 dataI8);

//  Allow RTP memory to be used for IP stack
extern void* (*appAlloc) (ADT_UInt32 memI8);
extern void  (*appFree)  (void* mem);
extern int   appMemI8;
extern       ADT_UInt16 RTPScratchI8;

extern CircBufInfo_t RTPFreeBuffers[];

#define RTPFreeBuffer ToDSPRTPBuffer

#ifdef _DEBUG
   #define PKTTONET  1
   #define STATS(cmd)  cmd
   void logRtp (int chan, int operation, int pyldI8, ADT_UInt32 *rtpHdr);
   struct NCTransferStats {
     int pktsToCore0;
     int pktsToCoreN;
     int pktsFromCore0;
     int pktsFromCoreN;
     int purged;
     int purges;
   } NCTransferStats;
   #pragma DATA_SECTION (NCTransferStats, "PER_CORE_DATA:STATS")
#else
   #define STATS(cmd)
   #define logRtp(chan, operation, pyldI8, rtpHdr) 
#endif


//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// netTransmit
//
// FUNCTION
//   RTP callback to send RTP packets to host. Will be called from framing tasks via RTPSend API.
//
// Inputs
//   hndl   -  Handle established at RTPOpen (pointer to channel structure.
//   data   -  RTP header and payload
//   dataI8 -  Size of data
//
//}
#ifdef SRTP_VALIDATE
ADT_UInt8 tempEncIn[600];
ADT_UInt8 tempDecOut[600];
volatile far int srtp_validate = 0;
#pragma DATA_SECTION (srtp_validate, "NON_CACHED_DATA")
#endif

#pragma CODE_SECTION (netTransmit, "FAST_PROG_SECT")
short int netTransmit (NETHANDLE* hndl, ADT_UInt32* Data, int *dataI8) {
   RTPToHostHdr_t Hdr;
   IPToSess_t  *IPMapping;
   ADT_Bool macValid;
   ADT_UInt8* data;
   int sent;
   void *RTPCirc;
   int mask;

#ifdef SRTP_VALIDATE
   int decOutI8, i;
   int encInI8 = *dataI8;

    // Save unencrypted SRTP encode for later decrypt
    if (srtp_validate && (encInI8 > 0) && (encInI8 < sizeof(tempEncIn)))
        memcpy(tempEncIn, Data, encInI8);
#endif


   data = (ADT_UInt8 *) Data;
   Hdr.PktI8  = *dataI8;
   Hdr.ChanId = (ADT_UInt16) ((ADT_UInt32) hndl);

   // Run SRTP if enabled and SRTP channel already configed
   if ((sysConfig.SRTP_Enable) && (SrtpTxKeyData[Hdr.ChanId]->Configured)) {
      Hdr.PktI8 = SrtpEncryptU8 (SrtpTxChanData[Hdr.ChanId]->Instance, data, *dataI8, data);
      *dataI8 = Hdr.PktI8;
      if (*dataI8 == 0) return 0;
   }
#ifdef SRTP_VALIDATE
      // Decrypt and compare with un-encrypted signal
      if (srtp_validate && (sysConfig.SRTP_Enable) && (SrtpRxKeyData[Hdr.ChanId]->Configured)) {
         decOutI8 = SrtpDecryptU8 (SrtpRxChanData[Hdr.ChanId]->Instance, data, *dataI8, tempDecOut);
         if ((encInI8 > 0) && (decOutI8 != encInI8)) {
            SrtpTxKeyData[Hdr.ChanId]->lenErrs++;
         } else {
            for (i=0; i<decOutI8; i++) {
                if (tempEncIn[i] != tempDecOut[i])
                    SrtpTxKeyData[Hdr.ChanId]->dataErrs++;
            }
            // restore un-encrypted sign for RTP transmit
            *dataI8 = encInI8;
            Hdr.PktI8  = *dataI8;
            memcpy(Data, tempEncIn, encInI8);
         }
      }
#endif

   data = customPktOut (&Hdr, (ADT_UInt16 *) data);
   *dataI8 = Hdr.PktI8; // customPktOut may change the size of the RTP packet
   logRtp (Hdr.ChanId, PKTTONET, *dataI8, (ADT_UInt32 *) data);

   macValid = fastRTPMap ((int) hndl, &IPMapping);

#ifdef SINGLE_CORE_STACK

   if (RTPLoopBack != DisableTest)
   {
      loopBackPkts(&Hdr);
      sendRtpPktToChan(Hdr.ChanId, data, *dataI8);
      return 1;
   }

   if (macValid)
   {
      if (fastRTPsend(data, *dataI8, IPMapping))
      {
         return 1;
      }
   }
#else

   if (fast_rtp_enabled && macValid && (RTPLoopBack == DisableTest)) {
      // Destination mac is available - use fastRTPsend to write packet to EMAC
      mask = HWI_disable ();
      fastRTPsend (data, *dataI8, IPMapping);
      HWI_restore (mask);
      return 1;
   }

#endif

   // Destination mac is not available - forward to core 0 for ARP
   RTPCirc = &RTPCircBuffers[(DSPCore * 2) + 1];
   if ((RTPLoopBack != DisableTest) && (DSPTotalCores == 1)) {
      loopBackPkts (&Hdr);
      RTPCirc = &RTPCircBuffers[DSPCore * 2];   // Place packet in DSP's receive buffer
   }

   // Avoid simultaneous access to RTP Buffer from framing tasks
   logTime (0x04090000 | Hdr.ChanId);
   mask = HWI_disable ();
   sent = PayloadToCircCache (RTPCirc, &Hdr, RTP_TO_HOST_I16, data, Hdr.PktI8);
   HWI_restore (mask);

   return sent;

   
}

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// rtpGetPacket
//
// FUNCTION
//   Called by RTP Stack to read a packet from a core's RTP buffer for delivery to the network.
//
// Outputs
//   IPBuff   - Buffer for RTP packet
//   IPBuffI8 - Size of IPBuff in bytes
//   ChanID   - G.PAK channel associated with packet
//
// Inputs
//   coreID   - Identifies which core's RTP buffer is read
//
// Returns -  Size of RTP packet in bytes
//
//}
//  Get RTP packet from RTP transfer buffer
#pragma CODE_SECTION (rtpGetPacket, "MEDIUM_PROG_SECT")
int rtpGetPacket (void *IPBuff, int IPBuffI8, ADT_UInt16 *ChanID, int coreID) {
   RTPToHostHdr_t Hdr;
   CircBufInfo_t *RTPToHostCirc;
   int takeIdx;
   //==============================================================
   // Obtain pointer to core's out bound RTP circular buffer
   RTPToHostCirc = (CircBufInfo_t *) &RTPCircBuffers[(coreID * 2) + 1];

   takeIdx = GetPktFromCircBufferCache (RTPToHostCirc, &Hdr, RTP_TO_HOST_I16, IPBuff, IPBuffI8);
   if (takeIdx < 0) return 0;
   *ChanID = Hdr.ChanId;

   RemovePktFromCircBuffer (RTPToHostCirc, &Hdr, IPBuff, takeIdx);
   if ((Hdr.ChanId != 0xfffe) && (chanTable[Hdr.ChanId]->channelType == inactive))
      *ChanID = 0xffff;

   logTime (0x04080000 | Hdr.ChanId);
   return Hdr.PktI8;
}

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// GetNcPktFromCircBufferCache - Get a packet from the host and return Hdr and Payload buffers
//
//   The packet handle and RTP address is stored in the host transfer buffer.  This routine
//   is responsible for:
//
//        reading the header from the host transfer buffer,
//        copying the packet from the packet buffer that is 'owned' by core 0
//        maintaining the packet's cache coherency with core 0
//        'adding' the header to core 0's free list (updating the free list's put pointer) 
//
//
//  IPStack calls this routine with Payload pointer set to null when freeing packet buffers
// 
//} Note:  Requires that that first I16 element in header defines the payload size in bytes
#pragma CODE_SECTION (GetHdrFromTransferBuffers, "FAST_PROG_SECT")
static ADT_Bool GetHdrFromTransferBuffers (CircBufInfo_t *circ, NCHdr_t *NcHdr) {

   // Circular buffer info
   register int PutIndex, TakeIndex, BufrSize;

   // Sizes in 32-bit integer units
   unsigned int WdsReady, WdsTillEnd, WdsRemaining;
   unsigned int HdrWds;
   
   register ADT_Word *pHdr;
   register ADT_Word *pBufr; 

   PutIndex  = SzInt16ToHost (circ->PutIndex);
   TakeIndex = SzInt16ToHost (circ->TakeIndex);
   BufrSize  = SzInt16ToHost (circ->BufrSize);
   HdrWds    = SzInt16ToHost (NCHdrI16);

   if (TakeIndex <= PutIndex) WdsReady = PutIndex - TakeIndex;
   else                       WdsReady = BufrSize - (TakeIndex - PutIndex);
   if (WdsReady < HdrWds) return ADT_FALSE;

   WdsTillEnd = BufrSize - TakeIndex;

   // Set circular buffer's current read position
   pHdr   = (ADT_Word *) NcHdr;
   pBufr  = (ADT_Word *) circ->pBufrBase;
   pBufr += TakeIndex;

   //--------------------------------
   // Copy NcHdr from circular buffer to app's Hdr
   if (WdsTillEnd < HdrWds)   {

      CACHE_INV (pBufr, SzHostToBytes(WdsTillEnd));
      wdcpy (pHdr, pBufr, WdsTillEnd);

      pHdr += WdsTillEnd;
      WdsRemaining = HdrWds - WdsTillEnd;
      pBufr = (ADT_Word *) circ->pBufrBase;  // Wrap circular buffer to start
      CACHE_INV (pBufr, SzHostToBytes(WdsRemaining));
      wdcpy (pHdr, pBufr, WdsRemaining);

   } else {
      CACHE_INV (pBufr, SzHostToBytes(HdrWds));
      wdcpy (pHdr, pBufr, HdrWds);
   }

   //--------------------------------
   // Remove entry from circular buffer
   TakeIndex += HdrWds;
   if (BufrSize <= TakeIndex) TakeIndex -= BufrSize;
   circ->TakeIndex = SzHostToInt16 (TakeIndex);

   //--------------------------------
   // Invalidate RTP buffer memory so copy will read from external memory
   BCACHE_inv (NcHdr->RTPBuff, NcHdr->PktI8, FALSE);
   return ADT_TRUE;
}

#pragma CODE_SECTION (GetPktFromTransferBuffers, "FAST_PROG_SECT")
void* GetPktFromTransferBuffers (CircBufInfo_t *circ, NCHdr_t *NcHdr) {

#ifdef SINGLE_CORE_STACK

	NcHdr->RTPBuff = getNetRtpRxPkt(&(NcHdr->PktI8), &(NcHdr->ChanId));
	if (NcHdr->RTPBuff == NULL)
	{
		return NULL;
	}
	NcHdr->PktHndl = NcHdr->RTPBuff;
	STATS(NCTransferStats.pktsFromCore0++;)
	return NcHdr->RTPBuff;

#else

   void* pkt;

   // Retrieve header from transfer buffers
   if (!GetHdrFromTransferBuffers (circ, NcHdr)) return NULL;
   STATS (NCTransferStats.pktsFromCore0++;)

   // Allocate RTP buffer, copy data, free original buffer.
   pkt = RTPAlloc (NcHdr->PktI8);
   if (pkt != NULL) {
      BCACHE_wait ();
      mmCopy (pkt, NcHdr->RTPBuff, NcHdr->PktI8);
   }
   STATS (NCTransferStats.pktsToCore0++;)
   RTPFreeBuffers[DSPCore].PutIndex = circ->TakeIndex;
   return pkt;

#endif
}

#pragma CODE_SECTION (PurgeTransferBuffer, "FAST_PROG_SECT")
int PurgeTransferBuffer (CircBufInfo_t* circ) {

#ifdef _DEBUG
   int purgeCnt;

   purgeCnt = circ->PutIndex - circ->TakeIndex;
   if (purgeCnt < 0) purgeCnt += circ->BufrSize;
   purgeCnt /= NCHdrI16;
   NCTransferStats.purges++;
   NCTransferStats.purged += purgeCnt;
#endif

   circ->TakeIndex = circ->PutIndex;
   RTPFreeBuffers[DSPCore].PutIndex = circ->TakeIndex;
   return 1;
}

#pragma CODE_SECTION (PacketHndlFree, "PKT_PROG_SECT")
void PacketHndlFree (void* hndl, void* data, ADT_UInt16 dataI8) {
   RTPFree (data);
   return;
}


//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// freePktHandles
//
// FUNCTION
//   Called by RTP Stack to free handles to processed packets.
//   Handles are obtained from the RTPFreeBuffers circular buffer.
//
// Inputs
//   coreID   - Identifies which core's RTPFreeBuffer is read
//
//
//}
#pragma CODE_SECTION (freePktHandles, "MEDIUM_PROG_SECT")
int freePktHandles (int coreID) {
   NCHdr_t NcHdr;

   int cnt=0;

   //==============================================================
   // Free packet handles that have already been processed by core
   do {
      if (!GetHdrFromTransferBuffers (&RTPFreeBuffers[coreID], &NcHdr)) break;
      recvncfree (NcHdr.PktHndl);
      STATS (NCTransferStats.pktsFromCoreN++);
      cnt++;
   } while (ADT_TRUE);
   return cnt;
}

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// storeInboundRTPPacket
//
// FUNCTION
//   Called by RTP Stack to store a packet into a core's RTP buffer for delivery to the framing task.
//
// Outputs
//   ChanID    - G.PAK channel associated with packet
//   RTPBuff   - Buffer for RTP packet
//   pktI8     - Size of RTPBuff in bytes
//
// Returns -  ADT_TRUE if packet was placed in buffer
//
//}
#pragma CODE_SECTION (storeInboundRTPPacket, "MEDIUM_PROG_SECT")
int storeInboundRTPPacket (ADT_UInt16 chanID, void *RTPBuff, int pktI8, void *PktHndl) {

   NCHdr_t NcHdr;
   CircBufInfo_t *RTPToDSPCirc;
   int sent;
   int mask;

   // Drop packets marked for channel types that do not support in-bound RTP packets
   switch (chanTable[chanID]->channelType) {
   default: return 0;   // Drop packets

   case customChannel:
   case pcmToPacket:
   case packetToPacket:
   case conferencePacket:
   case conferenceMultiRate: break;  // Accept packets for these channel types
   }

   //---------------------------------------------------------------------------
   // Place RTP packet into host transfer buffer
   NcHdr.PktI8   = pktI8;
   NcHdr.ChanId  = chanID;
   NcHdr.RTPBuff = RTPBuff;
   NcHdr.PktHndl = PktHndl;
   CACHE_INV (RTPBuff, pktI8);

   // Obtain pointer to the appropriate core's inbound RTP circular buffer
   RTPToDSPCirc = &RTPCircBuffers[chanTable[chanID]->CoreID * 2];

   mask = HWI_disable ();
   sent = PayloadToCircCache (RTPToDSPCirc, &NcHdr, NCHdrI16, NULL, 0);
   HWI_restore (mask);
   STATS (if (sent) NCTransferStats.pktsToCoreN++);

   logTime (0x04090000 | chanID);
   return sent;
}


//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//   Initialization of RTP channel instances, jitter buffer memory, IP stack and tasks
//} - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

#pragma CODE_SECTION (RTP_Init, "SLOW_PROG_SECT")
void RTP_Init () {

   int i, CoreIdx, RTPBuffI16;

   // Setup callback to free 
   RTPPayloadFree = PacketHndlFree;

   // Initialize RTP jitter buffer memory pools
   RTPMemInit ();

   // Provide IP stack with access to RTP memory
   appAlloc = RTPAlloc;
   appFree  = RTPFree;
   appMemI8 = RTPScratchI8;

   // Distribute RTP buffer memory (cache boundary aligned) across cores
   RTPBuffI16 = (sysConfig.RTPBuffersI16 / DSPTotalCores) & (~0x7f);

   CoreIdx = DSPCore * 2;

   // Configure RTP packet buffers
   RTPCircBuffers[CoreIdx].TakeIndex = 0;
   RTPCircBuffers[CoreIdx].PutIndex  = 0;
   RTPCircBuffers[CoreIdx].pBufrBase = &ToDSPRTPBuffer[DSPCore * RTPBuffI16];
   RTPCircBuffers[CoreIdx].BufrSize  = RTPBuffI16;
   RTPCircBuffers[CoreIdx].SlipSamps = 0;

   CoreIdx++;
   RTPCircBuffers[CoreIdx].TakeIndex = 0;
   RTPCircBuffers[CoreIdx].PutIndex  = 0;
   RTPCircBuffers[CoreIdx].pBufrBase = &ToNetRTPBuffer[DSPCore * RTPBuffI16];
   RTPCircBuffers[CoreIdx].BufrSize  = RTPBuffI16;
   RTPCircBuffers[CoreIdx].SlipSamps = 0;

   RTPFreeBuffers[DSPCore].TakeIndex = 0;
   RTPFreeBuffers[DSPCore].PutIndex  = 0;
   RTPFreeBuffers[DSPCore].pBufrBase = &RTPFreeBuffer[DSPCore * RTPBuffI16];
   RTPFreeBuffers[DSPCore].BufrSize  = RTPBuffI16;
   RTPFreeBuffers[DSPCore].SlipSamps = 0;

   STATS (memset (&NCTransferStats, 0, sizeof (NCTransferStats)));

   // Clear out RTP Instance structures
   // Start up tasks for IP stack, RTP packets and Messaging packets
   for (i=0; i<sysConfig.maxNumChannels; i++) {
#if 1
      CLEAR_INST_CACHE (RtpChanData[i], sizeof (RtpChanData_t));
      FLUSH_wait ();
      if (sysConfig.SRTP_Enable) {
         CLEAR_INST_CACHE (SrtpTxChanData[i], srtpTxI8);
         FLUSH_wait ();
         CLEAR_INST_CACHE (SrtpTxKeyData[i],  sizeof (GpakSrtpKeyInfo_t));
         FLUSH_wait ();
         CLEAR_INST_CACHE (SrtpRxChanData[i], srtpRxI8);
         FLUSH_wait ();
         CLEAR_INST_CACHE (SrtpRxKeyData[i],  sizeof (GpakSrtpKeyInfo_t));
         FLUSH_wait ();
      }
      if (DSPCore == 0) {
        memset (RtpChanData[i], 0, sizeof (RtpChanData_t));
        FLUSH_INST_CACHE (RtpChanData[i], sizeof (RtpChanData_t));
        if (sysConfig.SRTP_Enable) {
            memset(SrtpTxChanData[i], 0, srtpTxI8);
            memset(SrtpTxKeyData[i], 0, sizeof (GpakSrtpKeyInfo_t)); 
            memset(SrtpRxChanData[i], 0, srtpRxI8);
            memset(SrtpRxKeyData[i], 0, sizeof (GpakSrtpKeyInfo_t)); 
            FLUSH_INST_CACHE (SrtpTxChanData[i], srtpTxI8);
            FLUSH_wait ();
            FLUSH_INST_CACHE (SrtpTxKeyData[i],  sizeof (GpakSrtpKeyInfo_t));
            FLUSH_wait ();
            FLUSH_INST_CACHE (SrtpRxChanData[i], srtpRxI8);
            FLUSH_wait ();
            FLUSH_INST_CACHE (SrtpRxKeyData[i],  sizeof (GpakSrtpKeyInfo_t));
            FLUSH_wait ();
        }
      }
#else
       memset (RtpChanData[i], 0, sizeof (RtpChanData_t));

#endif
   }

   memset (VLANTags, 0, sizeof (VLAN_TAG) * 10);
   for (i=0; i<10; i++) VLANTags[i].ifIdx = -1;

}

//{  processIPStackFields
//
//   Parse IP stack addresses and parameters from RTP configuration message.
//   Add IP mapping for this channel to system.
//}
#pragma CODE_SECTION (processIPStackFields, "SLOW_PROG_SECT")
GPAK_RTPConfigStat_t processIPStackFields (ADT_UInt16 *pCmd, int chanId, void *RTPSession) {
   ADT_UInt16  *i16MAC;
   IPToSess_t  IPMapping;
   int i, offset;

   memset (&IPMapping.rmt, 0, sizeof (IPMapping.rmt));
   // Remote addresses (MAC, IP, UDP Port, SSRC)
   for (i = 0, offset = 0, i16MAC = (ADT_UInt16 *) &IPMapping.rmt.MAC; i < 3; i++)
      i16MAC[i] =  netI16 (&pCmd[13], &offset);

   IPMapping.rmt.IP = ((ADT_UInt32)pCmd[11] << 16) | (ADT_UInt32)pCmd[12];
   IPMapping.rmt.Port = pCmd[10];
   if (18 < ApiBlock.CmdMsgLength)
      IPMapping.rmt.SSRC = ((ADT_UInt32)pCmd[17] << 16) | (ADT_UInt32)pCmd[18];

   //-----------------------------------
   // Local addresses (UDP port)
    IPMapping.lcl.IP  = 0;
   if (16 < ApiBlock.CmdMsgLength)
      IPMapping.lcl.Port = pCmd[16];
   else
      IPMapping.lcl.Port = 0;

   if (IPMapping.lcl.Port != 0 && !rtpPortValid (IPMapping.lcl.Port))
      return RTPSrcPortError;

   if (22 <= ApiBlock.CmdMsgLength) {
      IPMapping.rmt.dstIP  = ((ADT_UInt32)pCmd[20] << 16) | (ADT_UInt32)pCmd[21];
      IPMapping.lcl.dstIP  = ((ADT_UInt32)pCmd[23] << 16) | (ADT_UInt32)pCmd[24];
      IPMapping.vlanIndex = Byte1 (pCmd[22]);
      IPMapping.DSCP      = Byte0 (pCmd[22]) << 2;
      if (IPMapping.vlanIndex != 0xff) {
         if (10 <= IPMapping.vlanIndex)  return RTPVlanError;
         if (VLANTags[IPMapping.vlanIndex].vlanDeviceHndl == NULL) return RTPVlanError;
      }
   } else {
      IPMapping.rmt.dstIP  = 0;
      IPMapping.lcl.dstIP  = 0;
      IPMapping.vlanIndex  = 0xff;
      IPMapping.DSCP       = 0;
   }
   if (addToIPSession (&IPMapping, RTPSession, chanId) < 0)
     return RTPDestinationError;
   return RTPSuccess;
}

//{  processIPv6StackFields
//
//   Parse IP stack addresses and parameters from RTP configuration message.
//   Add IP mapping for this channel to system.
//}
#pragma CODE_SECTION (processIPv6StackFields, "SLOW_PROG_SECT")
GPAK_RTPConfigStat_t processIPv6StackFields (ADT_UInt16 *pCmd, int chanId, void *RTPSession) {
#ifndef _INCLUDE_IPv6_CODE
   return  RTPNotConfigured;
#else
   ADT_UInt16  *i16MAC;
   IPToSess_t  IPMapping;
   int i, offset;

   memset (&IPMapping.rmt6, 0, sizeof (IPMapping.rmt6));
   // Remote addresses (MAC, IP, UDP Port, SSRC)
   for (i = 0, offset = 0, i16MAC = (ADT_UInt16 *) &IPMapping.rmt6.MAC; i < 3; i++)
      i16MAC[i] =  netI16 (&pCmd[19], &offset);

    for (i=0; i<8; i++) {
        IPMapping.rmt6.IP[2*i]   = (ADT_UInt8) (pCmd[11+i]>>8);
        IPMapping.rmt6.IP[2*i+1] = (ADT_UInt8)  pCmd[11+i];
    }

   IPMapping.rmt6.Port = pCmd[10];
   if (18 < ApiBlock.CmdMsgLength)
      IPMapping.rmt6.SSRC = ((ADT_UInt32)pCmd[23] << 16) | (ADT_UInt32)pCmd[24];

   //-----------------------------------
   // Local addresses (UDP port)
   memset (&IPMapping.lcl6.IP, 0, 16);

   if (16 < ApiBlock.CmdMsgLength)
      IPMapping.lcl6.Port = pCmd[22];
   else
      IPMapping.lcl6.Port = 0;

   if (IPMapping.lcl6.Port != 0 && !rtpPortValid (IPMapping.lcl6.Port))
      return RTPSrcPortError;

    for (i=0; i<8; i++) {
        IPMapping.rmt6.dstIP[2*i]   = (ADT_UInt8) (pCmd[26+i]>>8);
        IPMapping.rmt6.dstIP[2*i+1] = (ADT_UInt8)  pCmd[26+i];
        IPMapping.lcl6.dstIP[2*i]   = (ADT_UInt8) (pCmd[35+i]>>8);
        IPMapping.lcl6.dstIP[2*i+1] = (ADT_UInt8)  pCmd[35+i];
    }

   IPMapping.vlanIndex = Byte1 (pCmd[34]);
   IPMapping.DSCP      = Byte0 (pCmd[34]) << 2;
   if (IPMapping.vlanIndex != 0xff) {
      if (10 <= IPMapping.vlanIndex)  return RTPVlanError;
      if (VLANTags[IPMapping.vlanIndex].vlanDeviceHndl == NULL) return RTPVlanError;
   }
   if (addToIPSession6 (&IPMapping, RTPSession, chanId) < 0)
     return RTPDestinationError;
   return RTPSuccess;
#endif
}
//  Send vlan ID change request via host core messaging
#pragma CODE_SECTION (queueVlanDeviceRequest, "SLOW_PROG_SECT")
static void queueVlanDeviceRequest (ADT_UInt16 vlanIdx, ADT_UInt16 vlanID, ADT_UInt8 priority) {
   ADT_UInt32 Data[4];
   Data[0] = UPDATE_VLAN_TAG;
   Data[1] = (ADT_UInt32) vlanIdx;
   Data[2] = (ADT_UInt32) vlanID;
   Data[3] = (ADT_UInt32) priority;
   sendHostCoreMsg (Data, sizeof (Data));
   return;
}

//{  Process set VLAN tag message from host.
//
//  Word:Bits
//     0:ff00    -  SET_VLAN_TAG
//     0:00ff    -  VLAN Index (0-9)
//     1:ffff    -  VLAN ID (0-fff)
//     2:ff00    -  priority

//   Set VLAN tag response
//
//  Word:Bits
//     0:ff00    -  SET_VLAN_TAG_REPLY
//     0:00ff    -  Command tag (VLAN index filled in by msg task)
//     1:ff00    -  VLAN Index (0-9)
//}    1:00ff    -  DSP reply code
#pragma CODE_SECTION (ProcessSetVLANTagMsg, "SLOW_PROG_SECT")
int ProcessSetVLANTagMsg (ADT_UInt16 *cmd, ADT_UInt16 *reply) {
   ADT_UInt16 vlanIdx, vlanID, vlanTag;
   ADT_UInt8  priority;
   GPAK_VlanStat_t rplyValue;
   int i, offset;
   IPToSess_t  *IPMapping;

   vlanIdx  = Byte0 (cmd[0]);
   vlanID   = cmd[1];
   priority = Byte1 (cmd[2]);
   vlanTag  = (priority << 13) | vlanID;
   offset = 0;
   I16net (&vlanTag, &offset, vlanTag);

   // Format reply
   reply[0] |= MSG_SET_VLAN_TAG_REPLY << 8;
   reply[1]  = vlanIdx << 8;

   // Verify parameteres
   rplyValue = vlanSuccess;
   if (10 <= vlanIdx)          rplyValue = vlanIdxOutOfRange;
   else if (0x1000 <= vlanID)  rplyValue = vlanIDOutOfRange;
   else if (8 <= priority)     rplyValue = vlanPriorityOutOfRange;
   else if (VLANTags[vlanIdx].vlanDeviceHndl != NULL) {
      for (i=0, IPMapping=IPToSess; i < 0x100; i++, IPMapping++) {
         if (IPMapping->vlanIndex != vlanIdx) continue;
         rplyValue = vlanActive;
         break;
      }
   }
   if (rplyValue == vlanSuccess) {
      for (i = 0; i < 10; i++) {
         if ((vlanTag != VLANTags[i].Tag) || (i == vlanIdx)) continue;
         rplyValue = vlanDuplicate;
         break;
      }
   }
   reply[1] |= rplyValue;
   if (rplyValue != vlanSuccess) return 4;

   queueVlanDeviceRequest (vlanIdx, vlanID, priority);
   return 4;
}

//{  Get VLAN tag message.
//
//  Word:Bits
//     0:ff00    -  GET_VLAN_TAG
//     0:00ff    -  VLAN Index (0-9)
//     1:ffff    -  VLAN ID (0-fff)
//     2:ff00    -  priority

//   Set VLAN tag response
//
//  Word:Bits
//     0:ff00    -  SET_VLAN_TAG_REPLY
//     0:00ff    -  Command tag (VLAN Index filled in by msg task)
//     1:ff00    -  VLAN Index (0-9)
//     1:00ff    -  DSP reply code
//     2:e000    -  priority
//}    2:0fff    -  vlan tag
#pragma CODE_SECTION (ProcessGetVLANTagMsg, "SLOW_PROG_SECT")
int ProcessGetVLANTagMsg (ADT_UInt16 *cmd, ADT_UInt16 *reply) {

   ADT_UInt16 vlanIdx;
   GPAK_VlanStat_t rplyValue;

   vlanIdx  = Byte0 (cmd[0]);

   // Format reply
   reply[0] |= MSG_GET_VLAN_TAG_REPLY << 8;
   reply[1]  = vlanIdx << 8;

   rplyValue = vlanSuccess;
   if (10 <= vlanIdx) { 
        rplyValue = vlanIdxOutOfRange;
   } else {
        reply[2]  = (ADT_UInt16)VLANTags[vlanIdx].Tag;
   }
   reply[1] |= rplyValue;
   return 6;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// allocRtpBufrForNetPkt - Allocate an RTP buffer for a network stack packet.
//
// FUNCTION
//  This function allocates an RTP buffer for a network stack packet.
//
// RETURNS
//  Address of G.PAK RTP buffer or NULL if none available.
//
void * allocRtpBufrForNetPkt(
	ADT_UInt16 length			// length of packet (bytes)
	)
{
	void *pRtpPktBufr;			// pointer to allocated RTP packet buffer

	pRtpPktBufr = RTPAlloc(length);

	return pRtpPktBufr;
}

#endif                          // compile only if IP stack support exists
