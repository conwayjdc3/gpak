/*
 * Copyright (c) 2004-2007, Adaptive Digital Technologies, Inc.
 *
 * File Name: Pkt2Pkt.c
 *
 * Description:
 *   This file contains functions to support Packet To Packet type channels.
 *
 * Version: 1.0
 *
 * Revision History:
 *   4/21/04 - Initial release.
 *   2/2007  - Merged C54 and C64
 *
 */
   
// Application related header files.
#include "GpakDefs.h"
#include "GpakErrs.h"
#include "GpakExts.h"
#include "sysconfig.h"
//#include "GpakPragma.h"
extern volatile int skipErrors;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ ProcCfgPkt2PktMsg - Process a Configure Packet To Packet Channel message.
//
// FUNCTION
//   Performs Packet To Packet channel setup message parsing and 
//   setup channel data structure
//
// Inputs
//   pCmd   - Command message buffer
//
//  Word:Bits
//     2:ff00    -  A In Codec
//     2:00ff    -  A In Framesize (1/2 ms)
//     3:ff00    -  A Out Codec
//     3:00ff    -  A Out Framesize (1/2 ms)
//     4:8000    -  A Packet Echo Cancel
//     4:4000    -  A VAD
//     4:2000    -  B Packet Echo Cancel
//     4:1000    -  B VAD
//     4:0800    -  A AGC
//     4:0400    -  B AGC
//     4:0200    -  A DTX
//     4:0100    -  B DTX
//     4:00FF    -  B Channel ID
//     5:ff00    -  B In Codec
//     5:00ff    -  B In Framesize (1/2 ms)
//     6:ff00    -  B Out Codec
//     6:00ff    -  B Out Framesize (1/2 ms)
//     7:0FFF    -  A Tone types
//     8:0FFF    -  B Tone types
//
//    Version 402+
//     9:ffff    -  A OutGain
//    10:ffff    -  A InGain
//    11:ffff    -  B OutGain
//    12:ffff    -  B InGain

// Outputs
//   pChan  - Channel data structure pointer
//
// RETURNS
//  Return status for reply message.
//}
GPAK_ChannelConfigStat_t ProcCfgPkt2PktMsg (ADT_UInt16 *pCmd, chanInfo_t *pChanA) {

   ADT_UInt16 ChannelIdB;      // Channel Id B
   chanInfo_t *pChanB;         // pointer to second Channel structure
   ADT_UInt16 NumEcansNeeded;  // number of Echo Cancellers needed
   ADT_UInt16 flags;

   GPAK_ChannelConfigStat_t toneStatus;
   int toneDetectCnt;
   int mfTones, cedTones, cngTones, arbTones;
   int agcCnt;
   GpakToneTypes toneTypes;

   pkt2pcm_t *PktPcmA, *PktPcmB;
   pcm2pkt_t *PcmPktA, *PcmPktB;

   PcmPktA = &pChanA->pcmToPkt;
   PktPcmA = &pChanA->pktToPcm;

   //------------- Parse and validate the first channel's parameters.
   pCmd = &pCmd[2];   // A InCodec | A InFrameSize
   if (ApiBlock.ApiVersionId < 0x0603u)
      PktPcmA->SamplesPerFrame = Byte0 (*pCmd);
   else
      PktPcmA->SamplesPerFrame = halfMSToSamps (Byte0 (*pCmd));

   if (!ValidFrameSize (PktPcmA->SamplesPerFrame))
      return (Cc_InvalidPktInSizeA);

   PktPcmA->Coding = (GpakCodecs) Byte1 (*pCmd);  // Input codec
   if (!ValidCoding (PktPcmA->Coding, sampsToHalfMS (PktPcmA->SamplesPerFrame)))
      return (Cc_InvalidPktInCodingA);

   pCmd++;        // [3]  A OutCodec | A OutFrameSize
   if (ApiBlock.ApiVersionId < 0x0603u)
      PcmPktA->SamplesPerFrame = Byte0 (*pCmd);
   else
      PcmPktA->SamplesPerFrame = halfMSToSamps (Byte0 (*pCmd));

   if (!ValidFrameSize (PcmPktA->SamplesPerFrame))
      return Cc_InvalidPktOutSizeA;

   PcmPktA->Coding = (GpakCodecs) Byte1 (*pCmd);  // Output codec
   if (!ValidCoding (PcmPktA->Coding, sampsToHalfMS (PcmPktA->SamplesPerFrame)))
      return Cc_InvalidPktOutCodingA;

   PcmPktA->InSerialPortId  = SerialPortNull;
   PcmPktA->OutSerialPortId = SerialPortNull;
   PktPcmA->InSerialPortId  = SerialPortNull;
   PktPcmA->OutSerialPortId = SerialPortNull;

   channelAllocsNull (pChanA, PcmPktA, PktPcmA);

   pCmd++;      // [4]  A-PktEC | A-VAD | B-PktEC | B-VAD | A-AGC | B-AGC | A-DTX | B-DTX | B-ChanID
   flags = *pCmd;
   if ((flags & 0x8000) != 0)  {                 // Echo cancellation
      if (NumPktEcansUsed >= sysConfig.numPktEcans)
         return (Cc_PktEchoCancelNotCfg);

      PktPcmA->EchoCancel = Enabled;
      NumEcansNeeded = 1;
   }   else   {
      PktPcmA->EchoCancel = Disabled;
      NumEcansNeeded = 0;
   }


   if ((flags & 0x4000) != 0) {                  // VAD/Comfort noise
      PcmPktA->VAD = SetVADMode (PcmPktA->Coding);
      if (PcmPktA->VAD == VadDisabled)
         return Cc_VadNotConfigured;
      if (sysConfig.VadReport == Enabled)
        PcmPktA->VAD |= VadNotifyEnabled;
   } else
      PcmPktA->VAD = VadDisabled;

   PktPcmA->CNG = SetVADMode (PktPcmA->Coding);

   //
   //  AGC A
   //
   agcCnt = ((flags >> 11) & 1) + ((flags >> 10) & 1);
   if ((flags & 0x0800 ) == 0)
        PcmPktA->AGC = Disabled;
   else if (numAGCChansAvail < agcCnt)
        return Cc_InsuffAgcResourcesAvail;
   else
        PcmPktA->AGC = Enabled;

   //
   // DTX
   //
   if ((flags & 0x200) != 0) {
        if ((PcmPktA->VAD & VadMask) != VadCngEnabled)
            return Cc_VadNotConfigured;
        else {
            PcmPktA->DtxEnable = Enabled;
            PcmPktA->PrevVadState = VAD_INIT_RELEASE;
        }
   } else {
        PcmPktA->DtxEnable = Disabled;
   }

   //---------- Parse and validate the second channel's parameters.

   // Verify the second Channel Id is valid and the channel is not active.
   ChannelIdB = Byte0 (*pCmd);
   if (ChannelIdB >= sysConfig.maxNumChannels)
      return (Cc_InvalidChannelB);

   pChanB = chanTable[ChannelIdB];
   if (pChanB->channelType != inactive)
      return (Cc_ChannelActiveB);

   PcmPktB = &pChanB->pcmToPkt;
   PktPcmB = &pChanB->pktToPcm;

   channelAllocsNull (pChanB, PcmPktB, PktPcmB);

   if ((flags & 0x2000) != 0)  {                  // Echo cancellation
      if (NumPktEcansUsed >= (sysConfig.numPktEcans - NumEcansNeeded))
         return (Cc_PktEchoCancelNotCfg);

      PktPcmB->EchoCancel = Enabled;
   }   else
      PktPcmB->EchoCancel = Disabled;

   
   if ((flags & 0x1000) != 0)   {                 //  VAD/Comfort noise
      PcmPktB->VAD = SetVADMode (PcmPktB->Coding);
      if (PcmPktB->VAD == VadDisabled)
         return Cc_VadNotConfigured;
      if (sysConfig.VadReport == Enabled)
        PcmPktB->VAD |= VadNotifyEnabled;
   } else
      PcmPktB->VAD = VadDisabled;

   PktPcmB->CNG = SetVADMode (PktPcmB->Coding);
  
   //
   //  AGC
   //
   if ((flags & 0x0400 ) == 0)
        PcmPktB->AGC = Disabled;
   else if (numAGCChansAvail < agcCnt)
        return Cc_InsuffAgcResourcesAvail;
   else
        PcmPktB->AGC = Enabled;

   //
   // DTX
   //
   if ((flags & 0x100) != 0) {
        if ((PcmPktB->VAD & VadMask) != VadCngEnabled)
            return Cc_VadNotConfigured;
        else {
            PcmPktB->DtxEnable = Enabled;
            PcmPktB->PrevVadState = VAD_INIT_RELEASE;
        }
   } else {
        PcmPktB->DtxEnable = Disabled;
   }


   pCmd++;          // [5]  B-InCodec | B-InFrameSize
   if (ApiBlock.ApiVersionId < 0x0603u)
      PktPcmB->SamplesPerFrame = Byte0 (*pCmd);
   else
      PktPcmB->SamplesPerFrame = halfMSToSamps (Byte0 (*pCmd));

   if (!ValidFrameSize (PktPcmB->SamplesPerFrame))
      return Cc_InvalidPktInSizeB;

   PktPcmB->Coding = (GpakCodecs) Byte1 (*pCmd);  // Input codec
   if (!ValidCoding (PktPcmB->Coding, sampsToHalfMS (PktPcmB->SamplesPerFrame)))
      return Cc_InvalidPktInCodingB;
      
   pCmd++;       // [6] B-OutCodec | B-OutFrameSize
   if (ApiBlock.ApiVersionId < 0x0603u)
      PcmPktB->SamplesPerFrame = Byte0 (*pCmd);
   else
      PcmPktB->SamplesPerFrame = halfMSToSamps (Byte0 (*pCmd));

   if (!ValidFrameSize (PcmPktB->SamplesPerFrame))
      return Cc_InvalidPktOutSizeB;

   PcmPktB->Coding = (GpakCodecs) Byte1 (*pCmd);  // Output codec
   if (!ValidCoding (PcmPktB->Coding, sampsToHalfMS (PcmPktB->SamplesPerFrame)))
      return Cc_InvalidPktOutCodingB;

   PcmPktB->InSerialPortId  = SerialPortNull;
   PcmPktB->OutSerialPortId = SerialPortNull;
   PktPcmB->InSerialPortId  = SerialPortNull;
   PktPcmB->OutSerialPortId = SerialPortNull;

   // NOTE: Tone generate is disallowed because of conflicts with tone relay.
   pCmd++;  // [7]   A-Tones
   toneTypes = (GpakToneTypes) (*pCmd & 0xFFF);  // Tone detect
   if (toneTypes & Tone_Generate)
      return Cc_InvalidToneTypesA;

   PcmPktA->toneTypes = toneTypes;
   PktPcmA->toneTypes = toneTypes & (Tone_Regen | Tone_Relay | Notify_Host);

   pCmd++;  // [8]   B-Tones
   toneTypes = (GpakToneTypes) (*pCmd & 0xFFF);  // Tone detect
   if (toneTypes & Tone_Generate)
      return Cc_InvalidToneTypesB;

   PcmPktB->toneTypes = toneTypes;
   PktPcmB->toneTypes = toneTypes & (Tone_Regen | Tone_Relay | Notify_Host);

   mfTones = cedTones = cngTones = arbTones = 0;
   toneStatus = ValidToneTypes (PcmPktA->toneTypes, &mfTones, &cedTones, &cngTones, &arbTones);
   if (toneStatus != Cc_Success) return toneStatus;

   toneStatus = ValidToneTypes (PcmPktB->toneTypes, &mfTones, &cedTones, &cngTones, &arbTones);
   if (toneStatus == Cc_InvalidToneTypesA) return Cc_InvalidToneTypesB;
   if (toneStatus != Cc_Success) return toneStatus;

   toneDetectCnt = 0;
   if (PcmPktA->toneTypes & ToneDetect) toneDetectCnt++;
   if (PcmPktB->toneTypes & ToneDetect) toneDetectCnt++;
   if (ToneDetectorsAvail < toneDetectCnt)
        return Cc_InsuffTDResourcesAvail;

   pChanA->PairedChannelId = ChannelIdB;             // Channel pairing
   pChanB->PairedChannelId = pChanA->ChannelId;
   pChanB->channelType = packetToPacket;

   PcmPktA->Gain.ToneGenGainG1 =  0; // not used yet 
   PcmPktA->Gain.OutputGainG2 = 0;
   PcmPktA->Gain.InputGainG3 =  0;
   PcmPktB->Gain.ToneGenGainG1 =  0; // not used yet
   PcmPktB->Gain.OutputGainG2 = 0;
   PcmPktB->Gain.InputGainG3 =  0;

   if (ApiBlock.ApiVersionId <= 0x0401u) return Cc_Success;

   //----------------------------------------------------------
   // API Version 402 and better.
   //    Caller ID and Gain settings
   pCmd++;
   PcmPktA->Gain.OutputGainG2 = *pCmd++;  //Msg[9]
   PcmPktA->Gain.InputGainG3 =  *pCmd++;  //Msg[10]
   PcmPktB->Gain.OutputGainG2 = *pCmd++;  //Msg[11]
   PcmPktB->Gain.InputGainG3 =  *pCmd++;  //Msg[12]

   ClampScaleGain (&PcmPktA->Gain);
   ClampScaleGain (&PcmPktB->Gain);

   return Cc_Success;
}



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ ProcPkt2PktStatMsg - Process a Packet To Packet Channel Status message.
//
// FUNCTION
//   Prepares Channel Status reply message for a Packet To Packet type channel.
//
//  Inputs
//   pChan  - Channel data structure pointer
//
//  Outputs
//    pReply - Channel status reply buffer
//
//
// RETURNS
//   Length (octets) of reply message.
//}
ADT_UInt16 ProcPkt2PktStatMsg (ADT_UInt16 *pReply, chanInfo_t *pChanA) {

   chanInfo_t *pChanB; 

   pkt2pcm_t *PktPcmA, *PktPcmB;
   pcm2pkt_t *PcmPktA, *PcmPktB;
   int VADA, VADB;
   

   PcmPktA = &pChanA->pcmToPkt;
   PktPcmA = &pChanA->pktToPcm;

   pChanB = chanTable[pChanA->PairedChannelId];
   PcmPktB = &(pChanB->pcmToPkt);
   PktPcmB = &(pChanB->pktToPcm);

   if (PcmPktA->VAD & VadEnabled)
      VADA = 1;
   else 
      VADA = 0;

   if (PcmPktB->VAD & VadEnabled)
      VADB = 1;
   else 
      VADB = 0;


   pReply[9] = (ADT_UInt16)
            ((PktPcmA->EchoCancel << 15)  & 0x8000) |
            ((VADA         << 14)         & 0x4000) |
            ((PktPcmB->EchoCancel << 13)  & 0x2000) |
            ((VADB         << 12)         & 0x1000) |
            ((PcmPktA->AGC << 11)         & 0x0800) |
            ((PcmPktB->AGC << 10)         & 0x0400) |
            ((PcmPktA->DtxEnable << 9)    & 0x0200) |
            ((PcmPktB->DtxEnable << 8)    & 0x0100) |
             (pChanA->PairedChannelId & 0xFF);
             
   
   if (ApiBlock.ApiVersionId < 0x0603u) {
      pReply[7]  = (ADT_UInt16) PackBytes (PktPcmA->Coding, PktPcmA->SamplesPerFrame);
      pReply[8]  = (ADT_UInt16) PackBytes (PcmPktA->Coding, PcmPktA->SamplesPerFrame);
      pReply[10] = (ADT_UInt16) PackBytes (PktPcmB->Coding, PktPcmB->SamplesPerFrame);
      pReply[11] = (ADT_UInt16) PackBytes (PcmPktB->Coding, PcmPktB->SamplesPerFrame);
   } else {
      pReply[7]  = (ADT_UInt16) PackBytes (PktPcmA->Coding, sampsToHalfMS (PktPcmA->SamplesPerFrame));
      pReply[8]  = (ADT_UInt16) PackBytes (PcmPktA->Coding, sampsToHalfMS (PcmPktA->SamplesPerFrame));
      pReply[10] = (ADT_UInt16) PackBytes (PktPcmB->Coding, sampsToHalfMS (PktPcmB->SamplesPerFrame));
      pReply[11] = (ADT_UInt16) PackBytes (PcmPktB->Coding, sampsToHalfMS (PcmPktB->SamplesPerFrame));
   }

   pReply[12] = (ADT_UInt16) ((PcmPktA->toneTypes | PktPcmA->toneTypes) & 0xFFF);
   pReply[13] = (ADT_UInt16) ((PcmPktB->toneTypes | PktPcmB->toneTypes) & 0xFFF);

   if (ApiBlock.ApiVersionId <= 0x0401u) return 14;

   //----------------------------------------------------------
   // API Version 402 and better.
   //    Caller ID and Gain settings
   pReply[14] = (ADT_UInt16) pChanA->fromHostPktCount;
   pReply[15] = (ADT_UInt16) pChanA->toHostPktCount;
   pReply[16] = (ADT_UInt16) pChanB->fromHostPktCount;
   pReply[17] = (ADT_UInt16) pChanB->toHostPktCount;
   pReply[18] = (ADT_Int16) PcmPktA->Gain.OutputGainG2/10;
   pReply[19] = (ADT_Int16) PcmPktA->Gain.InputGainG3/10;
   pReply[20] = (ADT_Int16) PcmPktB->Gain.OutputGainG2/10;
   pReply[21] = (ADT_Int16) PcmPktB->Gain.InputGainG3/10;
   return 22;
}



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ PktPktDecode - Decode (B-side) processing for a Packet To Packet type channel.
//
// FUNCTION
//   Reads incoming packet (pChan->PktToPCM.inbuffer) from the host and 
//   decodes to PCM (pChan->PktToPCM.outbuffer) 
//
//
// Transfers
//
//      Host Transfer
//          pkt -->  pktPcm.inbuffer [pkt]
//                  (PktBufferPool[0][chan])
//
//      ParsePacket
//          pktPcm.inbuffer [pkt] --> Task.InWork [parameters]
//            (PktBufferPool[0][chan])
//
//      ToneRelayGenerate
//          pktPcm.toneRly  --> Task.OutWork [PCM]
//
//      VoiceDecode
//          Task.InWork [parameters] --> Task.OutWork [PCM]
//      
//      Task.OutWork[PCM] --> pktPcm.outbuffer [PCM]
//                               (PcmOutBufferPool[chan])
//
//      ENCODE
//          pktPcm.outbuffer [PCM] --> Encoder
//            (PcmOutBufferPool[chan])
//
// Inputs
//        pChan   -  Channel data structure pointer
//        pktBuff -  Linear work buffer (Task.InWork) for temporary packet data
//        pcmBuff -  Linear work buffer (Task.OutWork) for temporary PCM data
//     pECFarWork -  Linear work buffer (Task.EcFarWork) for far size PCM for echo cancellation
//      SampsPerFrame -  Samples per frame
//
// RETURNS
//   nothing
//}
void PktPktDecode (chanInfo_t *chan,  void *paramsBuff, ADT_PCM16 *pcmBuff, 
                      ADT_PCM16 *pECFarWork, int SampsPerFrame, int pcmBuffI8) {

   //  Decoding operation

   GpakPayloadClass class; 
   pkt2pcm_t       *PktPcm;
   int processSampsPerFrame, paramsI8;
   ADT_Bool   BActive;

   PktPcm = &chan->pktToPcm;
   if (PktPcm->Coding == NullCodec) return;

   logTime (0x400B0000ul | chan->ChannelId);

   // Read packet from host interface into pktBuff using pcmBuff as scratch unpacking
   // Decode packets (pktBuff) by class into PCM  (pcmBuff)
   paramsI8 = pcmBuffI8;
   class = parsePacket (chan, paramsBuff, &paramsI8, pcmBuff, pcmBuffI8);
   processSampsPerFrame = DecodePayload (chan, PktPcm, class, paramsBuff, paramsI8, pcmBuff, pcmBuffI8);
   AppErr ("Buffer size error", pcmBuffI8 / 2 < processSampsPerFrame);

   // copy the linear pcm buffer into the paired channel's circular pcm output buffer
   captureDecoderOutput (chan, pcmBuff, processSampsPerFrame);

   BActive = ((chanTable[chan->PairedChannelId]->Flags & CF_ACTIVE) && 
              (chanTable[chan->PairedChannelId]->pcmToPkt.Coding != NullCodec));

   if (BActive)
      copyLinearToCirc (pcmBuff, &PktPcm->outbuffer, processSampsPerFrame);

   logTime (0x400B0F00ul | chan->ChannelId);
   return;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ PktPktEncode - Encode (A-side) processing for a Packet To Packet type channel.
//
// FUNCTION
//   Reads PCM data (pChan->PcmPkt.currentInBuffer), encodes it into a packet
//   and sends it to the host (pChan->PcmPkt.outBuffer)
// Transfers
//          
//      Packet Decode
//         Decoder [PCM] --> pcmPkt.currentInBuffer [PCM]
//                              (PcmOutBufferPool[pairedChan])
//
//      pcmPkt.currentInBuffer [PCM] --> Task.InWork [PCM]
//         (PcmInBufferPool[pairedChan])
//
//      pcmPkt.EchoCancel == ENABLED
//          pcmPkt.EcFar [PCM] --> Task.EcFarWork [PCM]
//           (BulkDelay[pairedChan])
//
//          Task.InWork [PCM] + Task.EcFarWork[PCM] --> Task.InWork [PCM]
//
//      pairedChannel.pcmPkt.EchoCancel == ENABLED
//          Task.InWork [PCM] --> pcmPkt.ecBulkDelay
//                                (BulkDelayPool[chan])
//
//      ProcessVadToneEncode 
//          Tone:  Task.InWork [PCM] --> pcmPkt.outbuffer [pkt]
//                                        (PktBufferPool[1][chan])
//          Voice: Task.InWork [PCM] --> Task.OutWork [parameters]
//                 Task.OutWork [parameters] --> pcmPkt.outbuffer [pkt]
//                                               (PktBufferPool[1][chan])
//
//      Host Transfer
//          pktPcm.inbuffer [pkt]  --> pkt
//          (PktBufferPool[0][chan])

//
// Inputs
//        pChan   -  Channel data structure pointer
//        pcmBuff -  Linear work buffer (Task.InWork) for temporary PCM data
//        pktBuff -  Linear work buffer (Task.OutWork) for temporary packet data
//     pECFarWork -  Linear work buffer (Task.EcFarWork) for far size PCM for echo cancellation
//      SampsPerFrame -  Samples per frame
//
//
// RETURNS
//   nothing
//}
void PktPktEncode (chanInfo_t *chan, ADT_PCM16 *pcmBuff, void *pktBuff,
                     ADT_PCM16 *pECFarWork, int SampsPerFrame)  {

   //  Encoding operation
   ADT_PCM16 *DTMFBuff;  // Buffer to be used for 
   pcm2pkt_t *PcmPkt = &chan->pcmToPkt;
   pkt2pcm_t *PktPcm = &chan->pktToPcm;

   chanInfo_t *chanB = chanTable[chan->PairedChannelId];
   pkt2pcm_t *PktPcmB = &chanB->pktToPcm;
   int processSampsPerFrame;
   ADT_Bool   BActive;

   if (PcmPkt->Coding == NullCodec) return;

   logTime (0x400A0000ul | chan->ChannelId);

   processSampsPerFrame = PcmPkt->SamplesPerFrame;
   if (chan->ProcessRate < TDMRate)
      processSampsPerFrame /= (TDMRate / chan->ProcessRate);
   else if (TDMRate < chan->ProcessRate)
      processSampsPerFrame *= (chan->ProcessRate / TDMRate);

   // copy the input pcm data from the paired channel's circular output buffer to linear work buffer (pcmBuff)
   BActive = ((chanB->Flags & CF_ACTIVE) && (chanB->pktToPcm.Coding != NullCodec));
   if (BActive)
      copyCircToLinear (PcmPkt->activeInBuffer, pcmBuff, processSampsPerFrame);
   else
      memset (pcmBuff, 0, processSampsPerFrame / sizeof (ADT_UInt16));

   captureEncoderInput (chan, pcmBuff, processSampsPerFrame);
   DTMFBuff = (short *) pktBuff;
   if ((Enabled == PktPcm->EchoCancel) && BActive)  {
      // pktBuff will contain PCM samples with echo cancellation, without NLP.  This will be used as
      // input into DTMF detection.
      // Perform echo cancellation on pcmBuff. Obtain far end data from bulk delay buffer.
      copyCircToLinear (&PktPcm->ecFarPtr, pECFarWork, processSampsPerFrame);

      g168Capture (PktPcm->EcIndex, pcmBuff, pECFarWork, processSampsPerFrame);
      LEC_ADT_g168Cancel (PktPcm->pG168Chan, (short *) pcmBuff, (short *) pECFarWork, DTMFBuff);
      g168PostCapture (PktPcm->EcIndex, pcmBuff, pECFarWork, processSampsPerFrame);

   } else {
      // No echo cancellation, perform DTMF on original PCM buffer.
      i16cpy (DTMFBuff, pcmBuff, processSampsPerFrame);
   }
   // Apply Tone Buffer with Input Gain G3, separate from Pcm buffer
   ADT_GainBlockWrapper (PcmPkt->Gain.InputGainG3, DTMFBuff, DTMFBuff, processSampsPerFrame);
   // Apply Pcm Input Gain G3
   ADT_GainBlockWrapper (PcmPkt->Gain.InputGainG3, pcmBuff, pcmBuff, processSampsPerFrame);

   if (PcmPkt->AGC == Enabled)
      PcmPkt->AGCPower = AGC_ADT_run (PcmPkt->agcPtr, (short *) pcmBuff, processSampsPerFrame, 0, 0, (short *) pcmBuff, NULL);

   // Apply Pcm Output Gain G2 before Encoder
   ADT_GainBlockWrapper (PcmPkt->Gain.OutputGainG2, pcmBuff, pcmBuff, processSampsPerFrame);

   // Apply voice play back
   if (PktPcm->playA.state & PlayBack)
      voicePlayback (&PktPcm->playA, pcmBuff, pECFarWork, processSampsPerFrame, ADevice, chan->ChannelId);

   if ((Enabled == PktPcmB->EchoCancel) && BActive) {
      // Store pcm data to bulk delay for echo cancellation
      logTime (0x400A0B00ul | chan->ChannelId);
      copyLinearToCirc (pcmBuff, &PktPcmB->ecFarPtr, processSampsPerFrame);
   }

   // perform VAD, Tone Detection, and Encode. Copy result to host packet buffer
   ProcessVadToneEncode (chan, (ADT_PCM16 *) pcmBuff, DTMFBuff, pktBuff, processSampsPerFrame);
   logTime (0x400A0F00ul | chan->ChannelId);
   return;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ SetupPkt2PktChan - Setup a Packet To Packet channel.
//
// FUNCTION
//   This function performs channel setup initialization for a Packet To Packet
//   type channel.
//
// RETURNS
//   nothing
//}
#define ToneRelayEnabled(a, b) \
   ((GpakActivation) ((b->toneTypes & Tone_Relay) != Null_tone))

#define CNGEnabled(a,b)  ((PcmPktB->VAD & VadCng) ? Enabled : Disabled)


void InstanceInit (chanInfo_t *chan, pcm2pkt_t *PcmPkt,  pkt2pcm_t *PktPcm) {

   // Initialize PCM buffers
   memset (PktPcm->outbuffer.pBufrBase, 0, PktPcm->outbuffer.BufrSize*sizeof(ADT_PCM16));

   // Initial tone detection and tone relay instances
   if ((PcmPkt->toneTypes & ToneDetectAll) != Null_tone) {
      InitToneDetection (PcmPkt->toneTypes,   &PcmPkt->TDInstances,
                         PcmPkt->toneRelayPtr, PcmPkt->SamplesPerFrame);
   }

   // Initialize tone relay generation instance.
   AllocToneGen (&PktPcm->TGInfo, PktPcm->toneTypes);
   if (PktPcm->TGInfo.toneRlyGenPtr) {
      InitToneRelayGen (&PktPcm->TGInfo);
   }

   // Initialize VAD and Comfort noise gen
   if ((PcmPkt->VAD & VadCng) || (PktPcm->CNG & VadCng)) {
      InitVadCng (PcmPkt->vadPtr, &PcmPkt->VadParms, PcmPkt->SamplesPerFrame, PktPcm->SamplesPerFrame, chan->ProcessRate);
   }

   // Initialize vocoders
   initEncoder (PcmPkt, 0, chan->ProcessRate);
   initDecoder (PktPcm, 0, chan->ProcessRate);

   // Allocate sampling rate and initialize sampler rate converter instances
   AllocEncoderSampleRateConv (chan);
   AllocDecoderSampleRateConv (chan);

   return;
}

void SetupPkt2PktChan (chanInfo_t *chanA) {

   pcm2pkt_t *PcmPktA, *PcmPktB; 
   pkt2pcm_t *PktPcmA, *PktPcmB; 
   chanInfo_t *chanB;  

   // Get pointers to the both channel's PCM to Packet and Packet to PCM data.
   PcmPktA = &chanA->pcmToPkt;
   PktPcmA = &chanA->pktToPcm;
   
   chanB = chanTable[chanA->PairedChannelId];
   PcmPktB = &chanB->pcmToPkt;
   PktPcmB = &chanB->pktToPcm;

   if (PktPcmB->EchoCancel || PktPcmA->EchoCancel) {
      chanA->ProcessRate = 8000;
      chanB->ProcessRate = 8000;
   }

   // Allocate Pcm, Pkt and BulkDelay Buffers
   AllocBuffs (chanA);
   AllocBuffs (chanB);

   PktPcmA->ForwardTonePkts = ToneRelayEnabled (PktPcmA, PcmPktB); 
   PktPcmB->ForwardTonePkts = ToneRelayEnabled (PktPcmB, PcmPktA);
   
   PktPcmA->ForwardCNGPkts = CNGEnabled (PktPcmA, PcmPktB);
   PktPcmB->ForwardCNGPkts = CNGEnabled (PktPcmB, PcmPktA);

   // Active inbuffer is paired channel's outbuffer
   PcmPktA->activeInBuffer = &PktPcmB->outbuffer;
   PcmPktB->activeInBuffer = &PktPcmA->outbuffer;

   PcmPktA->ecBulkDelay.SlipSamps = 0;
   PcmPktB->ecBulkDelay.SlipSamps = 0;

   // Initialize enabled channel's echo canceller
   if (PktPcmB->EchoCancel == Enabled) {
      memset (PcmPktB->ecBulkDelay.pBufrBase, 0, PcmPktB->ecBulkDelay.BufrSize * sizeof (ADT_PCM16)); 
      PktPcmB->ecFarPtr.SlipSamps = 0;
      PktPcmB->ecFarPtr.pBufrBase = PcmPktB->ecBulkDelay.pBufrBase;
      PktPcmB->ecFarPtr.BufrSize  = PcmPktB->ecBulkDelay.BufrSize;
      PktPcmB->EcIndex = chanB->CoreID; 
      PktPcmB->pG168Chan =
         AllocEchoCanceller (G168_PKT, PktPcmB->SamplesPerFrame, &PktPcmB->EcIndex, chanB->ProcessRate);
   }

   if (PktPcmA->EchoCancel == Enabled) {
      memset (PcmPktA->ecBulkDelay.pBufrBase, 0, PcmPktA->ecBulkDelay.BufrSize * sizeof (ADT_PCM16)); 
      PktPcmA->ecFarPtr.SlipSamps = 0;
      PktPcmA->ecFarPtr.pBufrBase = PcmPktA->ecBulkDelay.pBufrBase;
      PktPcmA->ecFarPtr.BufrSize  = PcmPktA->ecBulkDelay.BufrSize;
      PktPcmA->EcIndex = chanA->CoreID; 
      PktPcmA->pG168Chan =
         AllocEchoCanceller (G168_PKT, PktPcmA->SamplesPerFrame, &PktPcmA->EcIndex, chanA->ProcessRate);
   }

   // Allocate AGC instances
   AllocAGC (chanA, ADevice,  PcmPktA->AGC);
   AllocAGC (chanB, ADevice,  PcmPktB->AGC);

   // Initialize tone packet getbuffer/putbuffer pointers
   if (PktPcmA->ForwardTonePkts || PktPcmA->ForwardCNGPkts) {
      PcmPktB->tonePktGet = PcmPktB->inbuffer.pBufrBase;
      PktPcmA->tonePktPut = PcmPktB->inbuffer.pBufrBase;
      memset (PcmPktB->inbuffer.pBufrBase, 0, PcmPktB->inbuffer.BufrSize * sizeof (ADT_UInt16));
    }
    
   if (PktPcmB->ForwardTonePkts || PktPcmB->ForwardCNGPkts) {
      PcmPktA->tonePktGet = PcmPktA->inbuffer.pBufrBase;
      PktPcmB->tonePktPut = PcmPktA->inbuffer.pBufrBase;
      memset (PcmPktA->inbuffer.pBufrBase, 0, PcmPktA->inbuffer.BufrSize * sizeof (ADT_UInt16));
   }

   InstanceInit (chanA, PcmPktA,  PktPcmA);
   InstanceInit (chanB, PcmPktB,  PktPcmB);

   return;
}
