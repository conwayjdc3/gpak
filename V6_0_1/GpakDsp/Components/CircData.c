/*
 * Copyright (c) 2004-2007, Adaptive Digital Technologies, Inc.
 *
 * File Name: CircData.c
 *
 * Description:
 *   The type is no longer supported
 *
 * Version: 1.0
 *
 * Revision History:
 *   4/21/04 - Initial release.
 *
 */
   
// Application related header files.
#include "GpakDefs.h"
#include "GpakErrs.h"
#include "GpakExts.h"
#include "sysconfig.h"


// Local function prototypes.


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ProcCfgCircDataMsg - Process a Configure Circuit Data Channel message.
//
// FUNCTION
//   Performs Circuit Data channel processing for a   Configure Channel message.
//
// Inputs
//       pCmd  - Command message buffer
//
// Outputs
//     pChan - Channel data structure pointer

// RETURNS
//  Return status for reply message.
//
GPAK_ChannelConfigStat_t ProcCfgCircDataMsg (ADT_UInt16 *pCmd, chanInfo_t *pChan) {
   return Cc_InvalidCktMuxFactor;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ProcCircDataStatMsg - Process a Circuit Data Channel Status message.
//
// FUNCTION
//   Prepares the Channel Status reply message for a Circuit Data channel.
//
// Inputs
//     pChan - Channel data structure pointer
//
// Outputs
//     pReply - Reply message
//
// RETURNS
//   Length (octets) of reply message.
//
ADT_UInt16 ProcCircDataStatMsg (ADT_UInt16 *pReply, chanInfo_t *pChan) {

   return 22;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// FrameOutCircData - Host to PCM framing for a Circuit Data type channel.
//
// FUNCTION
//   Performs framing operations for Circuit Data channel's Host to PCM direction.
//
// Inputs
//        pChan -  Channel data structure pointer
//    sampsPerFrame -  Samples per frame
//
// Work buffers
//   pktBuff -  Temporary linear buffer for packet data
//   pcmBuff -  Temporary linear buffer for pcm data
//   pECFarWork - Temporary linear buffer for far side echo cancellation
//
// RETURNS
//   nothing
//
void FrameOutCircData (chanInfo_t *pChan, void *pcmBuff, ADT_PCM16 *pktBuff,   
                       ADT_PCM16 *pECFarWork, int sampsPerFrame, int pcmBuffBytes) {
   

   return;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// FrameInCircData - PCM to Host framing for a Circuit Data type channel.
//
// FUNCTION
//   Performs framing operations for a Circuit Data channel's PCM to Host direction.
//
// Inputs
//        pChan -  Channel data structure pointer
//    sampsPerFrame -  Samples per frame
//
// Work buffers
//   pktBuff -  Temporary linear buffer for packet data
//   pcmBuff -  Temporary linear buffer for pcm data
//   pECFarWork - Temporary linear buffer for far side echo cancellation
//
// RETURNS
//   nothing
//
void FrameInCircData (chanInfo_t *pChan, ADT_PCM16 *pcmBuff, void *pktBuff,
                      ADT_PCM16 *pECFarWork, int sampsPerFrame) {


   return;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// SetupCircDataChan - Setup a Circuit Data channel.
//
// FUNCTION
//   Performs channel setup initialization for a Circuit Data channel.
//
// Inputs
//        pChan -  Channel data structure pointer
//
// RETURNS
//   nothing
//
void SetupCircDataChan (chanInfo_t *pChan) {

   return;
}


