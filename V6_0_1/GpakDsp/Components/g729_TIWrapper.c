#include <std.h>
// TI's implementation of G729

/* XDAIS header files */
#include <ti/xdais/xdas.h>

/* XDM header files */
#include <ti/xdais/dm/xdm.h>
#include <ti/xdais/dm/ispeech1.h>
#include <ti/xdais/dm/isphenc1.h>
#include <ti/xdais/dm/isphdec1.h>

/* G729AB Interface header files */
#include <g729abenc_tii.h>
#include <g729abdec_tii.h>
#include <ig729ab.h>
#include <adt_typedef.h>
#include "GpakExts.h"

// --------------------------offset          length  post-pad
#define ENC_MEM0_BYTE_OFFSET  0           // 48      0
#define ENC_MEM1_BYTE_OFFSET  48          // 32      0
#define ENC_MEM2_BYTE_OFFSET  80          // 272     0
#define ENC_MEM3_BYTE_OFFSET  352         // 48      0
#define ENC_MEM4_BYTE_OFFSET  400         // 308     4
#define ENC_MEM5_BYTE_OFFSET  712         // 608     0
#define ENC_MEM6_BYTE_OFFSET  1320        // 12      4
#define ENC_MEM7_BYTE_OFFSET  1336        // 172     -
#define ENC_INSTANCE_I8       1508
#define ENC_SCRATCH_I8                    // 2616

// --------------------------offset         length  post-pad
#define DEC_MEM0_BYTE_OFFSET  0           // 52      4
#define DEC_MEM1_BYTE_OFFSET  56          // 40      0
#define DEC_MEM2_BYTE_OFFSET  96          // 44      4
#define DEC_MEM3_BYTE_OFFSET  144         // 32      0
#define DEC_MEM4_BYTE_OFFSET  176         // 12      4
#define DEC_MEM5_BYTE_OFFSET  192         // 120     0
#define DEC_MEM6_BYTE_OFFSET  312         // 308     4
#define DEC_MEM7_BYTE_OFFSET  624         // 600     0
#define DEC_MEM8_BYTE_OFFSET  1224        // 10      6
#define DEC_MEM9_BYTE_OFFSET  1240        // 6       2
#define DEC_INSTANCE_I8       1246
#define DEC_SCRATCH_I8                    // 2048

#include "CircBuff.h"
int g729EncInit (void *chan, void *scratch, short Vad) {
   ISPHENC1_DynamicParams  dynparams;
   ISPHENC1_Params     params;
   ISPHENC1_Handle     dAlg;

   IALG_MemRec         memTab[9];
   IALG_Fxns          *fxns = (IALG_Fxns *) &G729ABENC_TII_IG729ABENC;
   IALG_Fxns          *fxnsPtr;
   IALG_Handle         alg;

   XDAS_Int8          *pMem = (XDAS_Int8 *)chan;
   Int                 n;

   // Verify memory requirements
   n = fxns->algNumAlloc != NULL ? fxns->algNumAlloc() : IALG_DEFMEMRECS;
   if (n != 9) {
      AppErr ("G729Alloc", ADT_TRUE);
      return -1;
   }

   memset (memTab, 0, sizeof(memTab));
   params.size      = sizeof (ISPHENC1_Params);
   params.tablesPtr = NULL; /* Default Table */

   n = fxns->algAlloc ((IALG_Params *)&params, &fxnsPtr, memTab);
   if (n <= 0) {
      AppErr ("G729Alloc", ADT_TRUE);
      return -1;
   }

   // Identify pointers into instance structure and initialize instance data
   memTab[0].base = &pMem[ENC_MEM0_BYTE_OFFSET];
   memTab[1].base = &pMem[ENC_MEM1_BYTE_OFFSET];
   memTab[2].base = &pMem[ENC_MEM2_BYTE_OFFSET];
   memTab[3].base = &pMem[ENC_MEM3_BYTE_OFFSET];
   memTab[4].base = &pMem[ENC_MEM4_BYTE_OFFSET];
   memTab[5].base = &pMem[ENC_MEM5_BYTE_OFFSET];
   memTab[6].base = &pMem[ENC_MEM6_BYTE_OFFSET];
   memTab[7].base = &pMem[ENC_MEM7_BYTE_OFFSET];
   memTab[8].base = (XDAS_Int8 *) scratch;

   alg = (IALG_Handle) memTab[0].base;
   alg->fxns = fxns;
   if (fxns->algInit (alg, memTab, NULL, (IALG_Params *)&params) != IALG_EOK) {
      AppErr ("G729Init", ADT_TRUE);
      return -1;
   }

   // Configure VAD
   dynparams.size = sizeof(ISPHENC1_DynamicParams);
   if (Vad)  dynparams.vadFlag = ISPEECH1_VADFLAG_ON;
   else      dynparams.vadFlag = ISPEECH1_VADFLAG_OFF;
   dAlg = (ISPHENC1_Handle) alg;
   if (dAlg->fxns->control (dAlg, XDM_SETPARAMS, &dynparams, NULL) == ISPHENC1_EFAIL) {
      AppErr ("G729", ADT_TRUE);
      return -1;
   }

   return 0;
}

int g729Encode (void *chan, void *pcm, void *data, short Vad, short *FrameType) {
   ISPHENC1_Handle         handle;
   ISPHENC1_InArgs         inargs;
   ISPHENC1_OutArgs        outargs;

   XDM1_SingleBufDesc      inBufs;
   XDM1_SingleBufDesc      outBufs;

   handle =  (ISPHENC1_Handle)chan;

   // perform Encode function
   inBufs.buf      = (XDAS_Int8 *) pcm;
   inBufs.bufSize  = 160;
   outBufs.buf     = (XDAS_Int8 *) data;
   outBufs.bufSize  = 10;
   inargs.size     = sizeof (ISPHENC1_InArgs); // No input arguments for encoder
   outargs.size    = sizeof (ISPHENC1_OutArgs);

   if (handle->fxns->process (handle, &inBufs, &outBufs, &inargs, &outargs) == XDM_EFAIL) {
      AppErr ("G729", ADT_TRUE);
      *FrameType = 0;
      return -1;
   }

   switch (outargs.frameType) {
   case ISPHENC1_FTYPE_SPEECH:     *FrameType = 1;    break;
   case ISPHENC1_FTYPE_SIDFRAME:   *FrameType = 2;    break;
   default:                        *FrameType = 0;    break;
   }
   return 0;
}

int g729DecInit (void *chan, void *scratch) {
   ISPHDEC1_Params     params;
   ISPHDEC1_DynamicParams  dynparams;
   ISPHDEC1_Handle      dAlg;

   IALG_MemRec         memTab[11];
   IALG_Fxns           *fxns = (IALG_Fxns *) &G729ABDEC_TII_IG729ABDEC;
   IALG_Fxns           *fxnsPtr;
   IALG_Handle          alg;

   XDAS_Int8           *pMem = (XDAS_Int8 *)chan;
   Int                 n;

   // Verify memory requirements
   n = fxns->algNumAlloc != NULL ? fxns->algNumAlloc() : IALG_DEFMEMRECS;
   if (n != 11) {
      AppErr ("G729Init", ADT_TRUE);
      return -1;
   }

   params.size       = sizeof (ISPHDEC1_Params);
   params.tablesPtr  = NULL;
   memset (memTab, 0, sizeof(memTab));
   n = fxns->algAlloc ((IALG_Params *) &params, &fxnsPtr, memTab);
   if (n <= 0) {
      AppErr ("G729Alloc", ADT_TRUE);
      return -1;
   }

   // Identify pointers into instance structure and initialize instance data
   memTab[0].base = &pMem[DEC_MEM0_BYTE_OFFSET];
   memTab[1].base = &pMem[DEC_MEM1_BYTE_OFFSET];
   memTab[2].base = &pMem[DEC_MEM2_BYTE_OFFSET];
   memTab[3].base = &pMem[DEC_MEM3_BYTE_OFFSET];
   memTab[4].base = &pMem[DEC_MEM4_BYTE_OFFSET];
   memTab[5].base = &pMem[DEC_MEM5_BYTE_OFFSET];
   memTab[6].base = &pMem[DEC_MEM6_BYTE_OFFSET];
   memTab[7].base = &pMem[DEC_MEM7_BYTE_OFFSET];
   memTab[8].base = &pMem[DEC_MEM8_BYTE_OFFSET];
   memTab[9].base = &pMem[DEC_MEM9_BYTE_OFFSET];
   memTab[10].base = (XDAS_Int8 *)scratch;

   alg = (IALG_Handle)memTab[0].base;
   alg->fxns = fxns;
   if (fxns->algInit (alg, memTab, NULL, (IALG_Params *)&params) != IALG_EOK) {
      AppErr ("G729Init", ADT_TRUE);
      return -1;
   }

   // enable post filter
   dynparams.size = sizeof(ISPHDEC1_DynamicParams);
   dynparams.postFilter = ISPEECH1_POSTFILTER_ON;
   dAlg = (ISPHDEC1_Handle) alg;
   if (dAlg->fxns->control (dAlg, XDM_SETPARAMS, &dynparams, NULL) == ISPHDEC1_EFAIL) {
      AppErr ("G729Init", ADT_TRUE);
      return -1;
   }

   return 0;
}

int g729Decode (void *chan, void *pyld, void *pcm, short FrameErase, short PktType) {
   ISPHDEC1_InArgs         inargs;
   ISPHDEC1_OutArgs        outargs;
   ISPHDEC1_Handle         handle;

   XDM1_SingleBufDesc      inBufs;
   XDM1_SingleBufDesc      outBufs;

   handle =  (ISPHDEC1_Handle)chan;

   inargs.size     = sizeof (ISPHDEC1_InArgs);
   outargs.size    = sizeof (ISPHDEC1_OutArgs);

   inBufs.buf      = (XDAS_Int8  *)pyld;
   inBufs.bufSize  = 0;
   outBufs.buf     = (XDAS_Int8  *)pcm;
   outBufs.bufSize = 160;

   if (FrameErase) {
      inargs.frameType    = ISPHDEC1_FTYPE_SPEECHLOST;
   } else {
      switch (PktType) {
      default:  inargs.frameType = ISPHDEC1_FTYPE_SPEECHLOST;    inBufs.bufSize  = 0;  break;
      case 0:   inargs.frameType = ISPHDEC1_FTYPE_NODATA;        inBufs.bufSize  = 0;  break;
      case 1:   inargs.frameType = ISPHDEC1_FTYPE_SPEECHGOOD;    inBufs.bufSize  = 10; break;
      case 2:   inargs.frameType = ISPHDEC1_FTYPE_SIDUPDATE;     inBufs.bufSize  = 2;  break;
      }
   }

   if (handle->fxns->process (handle, &inBufs, &outBufs, &inargs, &outargs) == XDM_EFAIL) {
     AppErr ("G729", ADT_TRUE);
     return -1;
   }

   return 0;
}


