extern packMSB2  (unsigned char *pU8, unsigned short *pIn, unsigned short num);
extern unpackMSB2(unsigned char *pU8, unsigned short *pIn, unsigned short num);
extern packMSB3  (unsigned char *pU8, unsigned short *pIn, unsigned short num);
extern unpackMSB3(unsigned char *pU8, unsigned short *pIn, unsigned short num);
extern packMSB4  (unsigned char *pU8, unsigned short *pIn, unsigned short num);
extern unpackMSB4(unsigned char *pU8, unsigned short *pIn, unsigned short num);
extern packMSB5  (unsigned char *pU8, unsigned short *pIn, unsigned short num);
extern unpackMSB5(unsigned char *pU8, unsigned short *pIn, unsigned short num);
extern void packMSB10 (unsigned char *pyld, unsigned short *param, unsigned short paramCnt);
extern void unpackMSB10 (unsigned char *pyld, unsigned short *param, unsigned short paramCnt);

void unpackMSB7(unsigned char *pU8, unsigned short *pOut, unsigned short num)
{
int loopCount, outCount, i;

    loopCount = num/8;
    for (i=0; i<loopCount; i++)
    {
        // eight output values per 7 input bytes
        *pOut++ = *pU8 >> 1;                // A

        *pOut = (*pU8++ & 0x01) << 6;       // B
        *pOut |= (*pU8 >> 2);
        pOut++;

        *pOut = (*pU8++ & 0x03) << 5;       // C
        *pOut |= (*pU8 >> 3);
        pOut++;

        *pOut = (*pU8++ & 0x07) << 4;       // D
        *pOut |= (*pU8 >> 4);
        pOut++;

        *pOut = (*pU8++ & 0x0f) << 3;       // E
        *pOut |= (*pU8 >> 5);
        pOut++;

        *pOut = (*pU8++ & 0x1f) << 2;       // F
        *pOut |= (*pU8 >> 6);
        pOut++;

        *pOut = (*pU8++ & 0x3f) << 1;       // G
        *pOut |= (*pU8 >> 7);
        pOut++;

        *pOut++ = *pU8++ & 0x7F;            // H
    }

    // remainder loop
    if (outCount = num % 8)
    {
        for (i=0; i<1; i++)
        {
            *pOut++ = *pU8 >> 1;                // A
            if (--outCount == 0)
                break;

            *pOut = (*pU8++ & 0x01) << 6;       // B
            *pOut |= (*pU8 >> 2);
            if (--outCount == 0)
                break;
            pOut++;

            *pOut = (*pU8++ & 0x03) << 5;       // C
            *pOut |= (*pU8 >> 3);
            if (--outCount == 0)
                break;
            pOut++;

            *pOut = (*pU8++ & 0x07) << 4;       // D
            *pOut |= (*pU8 >> 4);
            if (--outCount == 0)
                break;
            pOut++;

            *pOut = (*pU8++ & 0x0f) << 3;       // E
            *pOut |= (*pU8 >> 5);
            if (--outCount == 0)
                break;
            pOut++;

            *pOut = (*pU8++ & 0x1f) << 2;       // F
            *pOut |= (*pU8 >> 6);
            if (--outCount == 0)
                break;
            pOut++;

            *pOut = (*pU8++ & 0x3f) << 1;       // G
            *pOut |= (*pU8 >> 7);
        }
    } // if remainder
}
void packMSB7(unsigned char *pU8, unsigned short *pIn, unsigned short num)
{
int loopCount, inCount, i;

    loopCount = num/8;
    for (i=0; i<loopCount; i++)
    {
        // seven output bytes per 8 input values
        *pU8 = (*pIn++ << 1);
        *pU8 |= ((*pIn & 0x7F) >> 6);
        pU8++;

        *pU8 = (*pIn++ << 2);
        *pU8 |= ((*pIn & 0x7F) >> 5);
        pU8++;

        *pU8 = (*pIn++ << 3);
        *pU8 |= ((*pIn & 0x7F) >> 4);
        pU8++;

        *pU8 = (*pIn++ << 4);
        *pU8 |= ((*pIn  & 0x7F) >> 3);
        pU8++;

        *pU8 = (*pIn++ << 5);
        *pU8 |= ((*pIn & 0x7F) >> 2);
        pU8++;

        *pU8 = (*pIn++ << 6);
        *pU8 |= ((*pIn & 0x7F) >> 1);
        pU8++;

        *pU8 = (*pIn++ << 7);
        *pU8 |= (*pIn++ & 0x7F);
        pU8++;

    }

    // remainder loop
    if (inCount = num % 8)
    {
        for (i=0; i<1; i++)
        {
        // seven output bytes per 8 input values
            *pU8 = (*pIn++ << 1);
            if (--inCount == 0)
                break;
            *pU8 |= ((*pIn & 0x7F) >> 6);
            pU8++;

            *pU8 = (*pIn++ << 2);
            if (--inCount == 0)
                break;
            *pU8 |= ((*pIn & 0x7F) >> 5);
            pU8++;

            *pU8 = (*pIn++ << 3);
            if (--inCount == 0)
                break;
            *pU8 |= ((*pIn & 0x7F) >> 4);
            pU8++;

            *pU8 = (*pIn++ << 4);
            if (--inCount == 0)
                break;
            *pU8 |= ((*pIn  & 0x7F) >> 3);
            pU8++;

            *pU8 = (*pIn++ << 5);
            if (--inCount == 0)
                break;
            *pU8 |= ((*pIn & 0x7F) >> 2);
            pU8++;

            *pU8 = (*pIn++ << 6);
            if (--inCount == 0)
                break;
            *pU8 |= ((*pIn & 0x7F) >> 1);
            pU8++;

            *pU8 = (*pIn++ << 7);
        }
    } // if remainder
}
void unpackMSB6(unsigned char *pU8, unsigned short *pOut, unsigned short num)
{
int loopCount, outCount, i;

    loopCount = num/4;
    for (i=0; i<loopCount; i++)
    {
        // 4 output values per 3 input bytes
        *pOut++ = *pU8 >> 2;

        *pOut = (*pU8++ & 0x03) << 4;
        *pOut |= (*pU8 >> 4);
        pOut++;

        *pOut = (*pU8++ & 0x0F) << 2;
        *pOut |= (*pU8 >> 6);
        pOut++;

        *pOut++ = *pU8++ & 0x3F;
    }

    // remainder loop
    if (outCount = num % 4)
    {
        for (i=0; i<1; i++)
        {
            *pOut++ = *pU8 >> 2;
            if (--outCount == 0)
                break;

            *pOut = (*pU8++ & 0x03) << 4;
            *pOut |= (*pU8 >> 4);
            if (--outCount == 0)
                break;
            pOut++;

            *pOut = (*pU8++ & 0x0F) << 2;
            *pOut |= (*pU8 >> 6);
        }
    }
}
void packMSB6(unsigned char *pU8, unsigned short *pIn, unsigned short num)
{
int loopCount, inCount, i;

    loopCount = num/4;
    for (i=0; i<loopCount; i++)
    {
        // 3 output bytes per 4 input values
        *pU8 = (*pIn++ & 0x3F) << 2;                  
        *pU8 |= ((*pIn >> 4) & 0x03);
        pU8++;

        *pU8 = (*pIn++ & 0x0F) << 4;
        *pU8 |= ((*pIn >> 2) & 0x0F);     
        pU8++;
        
        *pU8 = (*pIn++ & 0x03) << 6;
        *pU8 |= (*pIn++ & 0x3F);
        pU8++;
    }

    // remainder loop
    if (inCount = num % 4)
    {
        for (i=0; i<1; i++)
        {
            *pU8 = (*pIn++ & 0x3F) << 2;                  
            if (--inCount == 0)
                break;
            *pU8 |= ((*pIn >> 4) & 0x03);
            pU8++;

            *pU8 = (*pIn++ & 0x0F) << 4;
            if (--inCount == 0)
                break;
            *pU8 |= ((*pIn >> 2) & 0x0F);     
            pU8++;
        
            *pU8 = (*pIn++ & 0x03) << 6;
        }
    }
}

void packMSB(unsigned short *inword,   // input buffer: unpacked data
         unsigned short *outword,      // output buffer: packed data
         unsigned short paramCnt,           // number of codewords to pack
         unsigned short width)         // width (in bits) of each codeword to pack
{

    unsigned char *pyld;
    unsigned short  *param;
    int i;

    pyld = (unsigned char *)outword;
    param = inword;

    switch (width) {
        case 8:
            for (i=0; i<paramCnt; i++) *pyld++ = *param++;
            break;

        case 10: packMSB10 (pyld, param, paramCnt); break;
        case 7:  packMSB7 (pyld, param, paramCnt);  break;
        case 6:  packMSB6 (pyld, param, paramCnt);  break;
        case 5:  packMSB5 (pyld, param, paramCnt);  break;
        case 4:  packMSB4 (pyld, param, paramCnt);  break;
        case 3:  packMSB3 (pyld, param, paramCnt);  break;
        case 2:  packMSB2 (pyld, param, paramCnt);  break;

        default:
            break;
    };
}

void unpackMSB(unsigned short *inword,   // input buffer: packed data
         unsigned short *outword,   // output buffer: unpacked data
         unsigned short paramCnt,        // number of codewords to unpack
         unsigned short width,      // width (in bits) of each codeword to unpack
         unsigned short outLS)    // number of left shifts to apply to output data
      
{

   unsigned char *pyld;
   unsigned short *param;
   int i;

    pyld = (unsigned char *)inword;
    param = outword;

    switch (width) {
        case 8:
            for (i=0; i<paramCnt; i++) *param++ = *pyld++;
            break;

        case 10: unpackMSB10 (pyld, param, paramCnt);  break;
        case 7:  unpackMSB7  (pyld, param, paramCnt);  break;
        case 6:  unpackMSB6  (pyld, param, paramCnt);  break;
        case 5:  unpackMSB5  (pyld, param, paramCnt);  break;
        case 4:  unpackMSB4  (pyld, param, paramCnt);  break;
        case 3:  unpackMSB3  (pyld, param, paramCnt);  break;
        case 2:  unpackMSB2  (pyld, param, paramCnt);  break;

        default:
            return;
    };

    // Shift outputs
    if (outLS)
    {
        param = outword;
        for (i=0; i<paramCnt; i++)
        {
            *param <<= outLS;
            param++;
        }
    }
}
#if 0
#define PACK_NUM 57
void testPack()
{

unsigned short inData[PACK_NUM];
unsigned short outData[PACK_NUM];
unsigned short unpackOutData[PACK_NUM];
unsigned short num=PACK_NUM;
unsigned short width=6;
unsigned short mask;

int i, j;
int count=0;
unsigned int packErrors = 0;

    while(1)
    {

        for (j=2; j <= 8; j++)
        //j = 7;
        {
            if (j == 4)
                num = PACK_NUM/4;
            else
            if (j == 2)
                num = PACK_NUM/2;
            else
                num = PACK_NUM;
            
            width = j;

            for (i=0; i<num; i++)
            {
                unpackOutData[i] = 0;
                outData[i] = 0;        
                mask =  (1<<width)-1;
                inData[i] =  rand() & mask;
                //inData[i] =  mask;

            }

            //packMSB(inData,outData,num,width);
            //unpackMSB(outData,unpackOutData,num,width);


            packLSB(inData,outData,num,width);
            unpackLSB(outData,unpackOutData,num,width);

            for (i=0; i<num; i++)
            {
                if (unpackOutData[i] != inData[i])
                    packErrors++;
            }

            count++;
        }
    }
}


#endif
