unpackMSB2(unsigned char *pU8, unsigned short *pOut, unsigned short num)
{
int loopCount, i;

    // 4 output values per input byte
    loopCount = num/4;

    for (i=0; i<loopCount; i++)
    {
        *pOut++ = *pU8 >> 6;
        *pOut++ = (*pU8 >> 4) & 0x03;
        *pOut++ = (*pU8 >> 2) & 0x03;
        *pOut++ = *pU8++ & 0x03;
    }                         
    return;
}
packMSB2(unsigned char *pU8, unsigned short *pIn, unsigned short num)
{
int loopCount, i;

    loopCount = num/4;
    // one output byte per four input values
    for (i=0; i<loopCount; i++)
    {
        *pU8 = *pIn++ << 6;
        *pU8 |= ((*pIn++ & 0x03)<<4);
        *pU8 |= ((*pIn++ & 0x03)<<2);
        *pU8 |= (*pIn++ & 0x03);
        pU8++;
    }
    return;
}
