/*
 * Copyright (c) 2007, Adaptive Digital Technologies, Inc.
 *
 * File Name: AMRFormat.c
 *
 * Description:
 *   This file contains functions convert AMR parameters to RTP payload
 *   format as specified by documents:
 *
 *          RFC 3267
 *          ETS 300 726
 *          TS 101 318
 *
 *
 *   NOTE:  Payloads are restricted to single frames of data packed
 *          in 'band-width efficient' (bit packed no gaps) mode.
 *
 * Version: 1.0
 *
 * External functions defined:
 *
 *
 * Revision History:
 *   02/2007 - Initial release.
 *   08/2012 - fixed the packing/unpacking for the C64X version
 */

#include "GpakExts.h"
//
// AMR mode is used to index into this table to 
// determine the payload's packet (hdr + data) size in I16
//
static ADT_UInt16 BitsPerPacket [] = {
  105, 113, 128, 144, 158, 169, 214, 254, 45
};

//================================================================================
//
//  Function: parseAMRPayload 
//  
//       This routine extracts AMR parameters from an RTP payload.
//
//   Inputs:
//     payload  -  word pointer of RTP payload containing AMR data
//
//   Outputs:
//     AMRParams   -  Pointer to 16-bit array to receive AMR parameter values.
//
//
//================================================================================
static void extractAMRData (ADT_UInt16 *Payload, int I8Cnt, ADT_UInt8 *AMRData) {

   ADT_UInt16 h1, h2, l1, l2;
   ADT_UInt16 highByte, lowByte;
   ADT_UInt16 temp;

   temp = *Payload++;
   highByte = (temp >> 8) & 0x00ff;

   do {
      h1 = (highByte << 2) & 0x00fc;

      temp = *Payload++;
      lowByte  =  temp & 0x00ff;
      highByte = (temp >> 8) & 0x00ff;

      l1 = (lowByte >> 6) & 0x0003;
      h2 = (lowByte << 2) & 0x00fc;

      *AMRData++ = h1 | l1;
      if (--I8Cnt <= 0) break;

      l2 = (highByte >> 6) & 0x0003;
      *AMRData++ = h2 | l2;

   } while (0 < --I8Cnt);
}

void parseAMRPayload (ADT_UInt16 *Payload, ADT_UInt16 *AMRData) {

    ADT_UInt16 temp, i8Cnt, frmRate;

    temp = ntohs (*Payload);

    AMRData[0] = (temp >> 12) & 0x000f;  // Requested rate
    AMRData[1] = frmRate = (temp >>  7) & 0x000f;  // Actual rate
    if (MRDTX < frmRate) return;

    i8Cnt = ((BitsPerPacket[frmRate]- 10) + 7) / 8;

    extractAMRData (Payload, i8Cnt, (ADT_UInt8 *)&AMRData[2]);
    //memcpy ((ADT_UInt8 *)&AMRData[2], &Payload[1], i8Cnt);
    return;

}


//================================================================================
//
//  Function: formatAMRPayload
//  
//       This routine formats AMR parameters into an RTP payload.
//
//   Inputs:
//     AMRParams -  Pointer to 16-bit array of AMR parameter values.
//
//   Outputs:
//     RtpPayload  -  word pointer of RTP payload to receive AMR data
//
//================================================================================
//
static void appendAMRData (ADT_UInt8 *AMRData, int I16Cnt, ADT_UInt16 *Payload) {
   ADT_UInt16 h1, h2, h3, l1, l2;
   ADT_UInt16 temp, highByte, lowByte;

   temp = *AMRData++;
   h1  = ((temp & 0x00fc) >> 2) << 8;
   l1  = (temp & 0x0003) << 6;

   *Payload++ |= h1;

   while (0 < --I16Cnt) {
      temp = *AMRData++;
      h2 = (temp & 0x00fc) >> 2;
      l2 = (temp & 0x0003) << 6;
      lowByte = l1 | h2;

      temp = *AMRData++;
      h3 = (temp & 0x00fc) >> 2;
      l1 = (temp & 0x0003) << 6;

      highByte = l2 | h3;
     *Payload++ = (highByte << 8) | lowByte;

   }
}


int formatAMRPayload (ADT_UInt16 *AMRData, ADT_UInt16 *Payload) {

    ADT_UInt16 rqstMode, frmRate, i16Cnt;
    ADT_UInt16 temp;
    
    rqstMode = AMRData[0] & 0x000f;
    frmRate = AMRData[1] & 0x000f;

    if (MRDTX < frmRate) return 0;

    temp = (rqstMode << 12) | (frmRate << 7) |  0x0040;  // Single frame specified by mode.
    Payload[0] = ntohs (temp);

    i16Cnt = (BitsPerPacket[frmRate] + 15) / 16;

    appendAMRData ((ADT_UInt8 *) &AMRData[2], i16Cnt, Payload);
    //memcpy(&Payload[1], &AMRData[2], (BitsPerPacket[frmRate] -10+7) / 8);

    return ( (BitsPerPacket[frmRate] -10+7) / 8 +2);
}
