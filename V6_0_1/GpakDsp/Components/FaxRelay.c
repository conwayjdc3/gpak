/*
 * Copyright (c) 2007, Adaptive Digital Technologies, Inc.
 *
 * File Name: FaxRelay.c
 *
 * Description:
 *   This file contains G.PAK Fax relay functions to interface
 *   with the MESi fax relay software.
 *
 * Version: 1.0
 *
 * Revision History:
 *   10/17/07 - Initial release.
 *
 *
 * Modem on is detected by CED tone
 * Fax on is detected by V21 Channel 2 indicator
 * Fax off is detected by EOM or timeout
 * Modem off is detected by signal level drop
 */
#include "GpakDefs.h"
#include "GpakErrs.h"
#include "GpakExts.h"


#include "./faxrelay_v0200/fsk.h"
#include "./faxrelay_v0200/v21.h"
#include "./faxrelay_v0200/gendet.h"

//#include "GpakPragma.h"

#define CF_FAX_CAPTURE     0x00800
#define CF_MODEM_ACTIVE    0x01000
#define CF_MODEM_DETECT    0x02000
#define CF_MODEM_ACTIVATED 0x04000
#define CF_MODEM_DETECTED  0x08000
#define CF_FAX_ACTIVATED   0x10000
int t38TooBig = 0;


#define MODEM_DETECT_TIMEOUT 125                      // Milliseconds CED must be active before G711
//#define MODEM_ACTIVE_TIMEOUT END_OF_FAX_TIMEOUT_MS    // Modem active keep alive
//#define FAX_TIMEOUT          END_OF_FAX_TIMEOUT_MS    // Fax active keep alive

#define MODEM_ACTIVE_TIMEOUT 15000                    // Modem active keep alive
#define FAX_TIMEOUT          15000                    // FAX active keep alive


typedef enum {
   EVT_V21=0x20,      EVT_FAX_START,         EVT_FAX_TO,      // 20 - 22   
   EVT_XFER_TO_VOICE, EVT_HOST_FULL,         EVT_RLY_FULL,    // 23 - 25
   EVT_PKT_RETRY,     EVT_RLY_TOO_SMALL,    EVT_BUFF_TOO_SMALL,   // 26 - 28
   EVT_RLY_PKT_OUT,   EVT_RLY_PKT_IN,

   EVT_MODEM_DETECT=0x40, EVT_MODEM_ACTIVE,  EVT_MODEM_END,    // 40 - 42
   EVT_DETECT_TO,         EVT_MODEM_TO,      EVT_PKT_DISCARD,  // 43 - 44

   EVT_FUNC=0x80,     EVT_FAX_INIT_IN,   EVT_FAX_INIT_OUT,       // 80 - 82
   EVT_FAX_PCM_IN,    EVT_FAX_PCM_OUT,   EVT_PKT_FROM_RLY_IN,    // 83 - 85
   EVT_PKT_FROM_RLY_OUT,  EVT_PKT_TO_RLY_IN,  EVT_PKT_TO_RLY_OUT // 86 - 88
} t38_trace;




#ifndef _DEBUG
   #define T38TraceEnable 
   #define T38TraceDisable 
   #define ModemTraceEnable 
   #define T38_TRACE(chan, code) 
   #define STATS_INC(strt,field)
   #define STATS_MAX(strt,field,value) 
   #define STATS_OR(strt,field,value) 
   #define STATS_TRACE(strt,field,chan,code) 
   #define captureFaxInput(chan,data,frameSize)
   #define captureFaxOutput(chan,data,frameSize)
   #define maxPktUpdate(pktSize)
   #define packetTooLarge(pktSize)
   #define BUFLEN MAX_FAX_PKT
   #define AddToBufLog(str,chan,value)
#else
   
   #include <stdio.h>
   #define TRACE_LEN 256
   #define ModemTraceEnable if (startCnt == 0) t38TraceIndex = 0; t38TraceEnable &= 0x60;  startCnt++;
   #define T38TraceEnable   if (t38TraceEnable & 0xC0) t38TraceEnable &= 0x60; 
   #define T38TraceDisable  t38TraceEnable = 0x60;
   int BUFLEN = MAX_FAX_PKT;
   int startCnt = 0;
   int maxPktLen = 0;

   struct {
      int nsections;
      int chanSize;
      int bufrSize;
   } faxSizes = { NSECTIONS, RLY_CHANNEL_SIZE, BMR_SIZE_BUFFER };

   struct {
      ADT_UInt16 id;
      ADT_UInt16 event;
      ADT_UInt16 rx_state;
      ADT_UInt16 tx_state;
      ADT_UInt16 time;
   } t38Trace[TRACE_LEN];
   ADT_UInt16 t38TraceIndex = 0;
   ADT_UInt16 t38TraceEnable = 0xE0;

   #define STATS_INC(strt,field)        strt->field++
   #define STATS_MAX(strt,field,value) if (strt->field < value) strt->field = value
   #define STATS_OR(strt,field,value)  strt->field |= value
   
   #define T38_TRACE(chan, code) \
      if ((t38TraceIndex < TRACE_LEN) && (t38TraceEnable & code)) { \
          t38Trace[t38TraceIndex].id   = (chan)->faxIndex;          \
          t38Trace[t38TraceIndex].event = code;                     \
          t38Trace[t38TraceIndex].rx_state = ( (struct RX_BLOCK *) ((chan)->pFax->start_ptrs)->Rx_block_start)->state_ID; \
          t38Trace[t38TraceIndex].tx_state = ( (struct TX_BLOCK *) ((chan)->pFax->start_ptrs)->Tx_block_start)->state_ID; \
          t38Trace[t38TraceIndex].time = ApiBlock.DmaSwiCnt;     \
          t38TraceIndex = (t38TraceIndex + 1) & 0xff; \
          t38Trace[t38TraceIndex].event = 0xdead; }



   #define STATS_TRACE(strt,field,chan,code) if ((strt->field % 1) == 0) T38_TRACE(chan,code)
   void stopFaxCapture ();
   void captureFaxInput (chanInfo_t *pChan, void *data, int FrameSize);
   void captureFaxOutput (chanInfo_t *pChan, void *data, int FrameSize);
   void maxPktUpdate (int pktLen);
   void packetTooLarge (int pktLen);   
   void AddToBufLog (char *string, chanInfo_t *pChan, int data);
#endif


//===================================================================================

// FAX relay pool management

//===================================================================================
                                                                             
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// AllocFaxRelay -  Allocate fax relay instance from pool
void InitRelay (chanInfo_t *pChan) {
   struct RlyInitChannelStruct RlyInit;

   T38_TRACE (&pChan->fax, EVT_FAX_INIT_IN);

   RlyInit.Layer3 = T30_T38;
   if (pChan->fax.faxTransport == faxUdptl)     RlyInit.Layer3 |= T30_REDUNDANT_UDP;
   else if (pChan->fax.faxTransport == faxRtp)  RlyInit.Layer3 |= T30_UDP;
   else if (pChan->fax.faxTransport == faxTcp)  RlyInit.Layer3 |= T30_TCP;
   RlyInit.RelayFunction=FAX_RELAY_FUNCTION;
   
   pChan->fax.pFax = RlyRelayInitChannel (pFaxChan[pChan->fax.faxIndex], RLY_CHANNEL_SIZE, &RlyInit);
   FLUSH_INST_CACHE (pFaxChan[pChan->fax.faxIndex], RLY_CHANNEL_SIZE);
   pChan->fax.pFax->channel = pChan->ChannelId;
   pChan->fax.pFax->ModemIf->PacketLen = (pChan->pcmToPkt.SamplesPerFrame + 7) / 8;
   T38_TRACE (&pChan->fax, EVT_FAX_INIT_OUT);

}

void AllocFaxRelay (chanInfo_t *pChan) {
   ADT_Int16 i;
    t38Packet_t *pT38Pkt;

   // init fax state variables
   pChan->fax.t38InProgress = (pChan->fax.faxMode == faxOnly);  // T.38 in progress only when faxOnly mode
   pChan->fax.faxPktRcvd    = 0;
   pChan->fax.msBeforeTimeout = 0; 

   if ((pChan->fax.faxMode == disabled) || (sysConfig.numFaxChans == 0))
      return;


   // get the index of the next available fax instance
   for (i=0; i<sysConfig.numFaxChans; i++) {
      if (faxChanInUse[i] == 0) {
         pChan->fax.faxIndex = i;
         faxChanInUse[i] = 1;
         numFaxChansUsed++;
         break;
      }
   }
   
   pT38Pkt = pT38Packets[pChan->fax.faxIndex];
   pT38Pkt->pktLen = 0;
   memset (&pT38Pkt->pktStats, 0, sizeof (FAXStats_t));

   // initialize the fax channel
   InitRelay (pChan);

}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// invalidateFaxRelay -  Restore fax relay instance to pool
void invalidateFaxRelay (chanInfo_t *pChan) {
   if (pChan->fax.faxMode != disabled) {
      CLEAR_INST_CACHE (pFaxChan[pChan->fax.faxIndex], RLY_CHANNEL_SIZE * 2);
   }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// DeallocFaxRelay -  Restore fax relay instance to pool
void DeallocFaxRelay (chanInfo_t *pChan) {
   if (pChan->fax.faxMode != disabled) {
      CLEAR_INST_CACHE (pFaxChan[pChan->fax.faxIndex], RLY_CHANNEL_SIZE * 2);
      faxChanInUse[pChan->fax.faxIndex] = 0;
      numFaxChansUsed--;
   }
}


//===================================================================================

// Modem switch over management

//===================================================================================

inline void faxStart (chanInfo_t *pChan, t38Packet_t *t38Storage) {
   unsigned char *pktBuff;
   int purgeCnt = 0;

   pChan->fax.t38InProgress = TRUE;
   pChan->fax.faxPktRcvd = 0;
   pChan->fax.msBeforeTimeout = FAX_TIMEOUT;    
   
   if (!(pChan->Flags & CF_MODEM_ACTIVE))
      memset ((void *)&(t38Storage->pktStats), 0, sizeof (FAXStats_t));

   pChan->Flags |= (CF_MODEM_DETECT | CF_MODEM_ACTIVE | CF_FAX_CAPTURE);

   // Strip any packets that may be lingering from previous fax call
   pktBuff = (unsigned char *) t38Storage->t38Packet;
   while (0 < RlyPacketFmRelay (pChan->fax.pFax, pktBuff, BUFLEN)) purgeCnt++;
   if (purgeCnt != 0) AddToBufLog ("FAX PURGE", pChan, purgeCnt);
   
}

inline void faxEnd (chanInfo_t *pChan) {

   pChan->fax.t38InProgress = pChan->fax.faxMode == faxOnly;
   pChan->pcmToPkt.Coding = pChan->pcmToPkt.PreferredCoding;
   pChan->fax.msBeforeTimeout = 0;

   pChan->Flags |= CF_REINIT_ENCODE | CF_REINIT_DECODE;
   pChan->Flags &= ~(CF_MODEM_DETECT | CF_MODEM_ACTIVE);
   
   AddToBufLog ("TOTAL RCVD", pChan, pT38Packets[pChan->fax.faxIndex]->pktStats.ToRelay);
   AddToBufLog ("TOTAL SENT", pChan, pT38Packets[pChan->fax.faxIndex]->pktStats.FromRelay);

}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// modemStart - Switch to G711
inline void modemStart (chanInfo_t *pChan, t38Packet_t *t38Storage) {

   FAXStats_t  *pktStats;
   int frameMs;
  
   pktStats = &t38Storage->pktStats;
   if (!(pChan->Flags & CF_MODEM_DETECT)) {
      ModemTraceEnable;
      memset (pktStats, 0, sizeof (FAXStats_t));

      pChan->Flags |= CF_MODEM_DETECT | CF_FAX_CAPTURE;
      T38_TRACE (&pChan->fax, EVT_MODEM_DETECT);
      pChan->fax.msBeforeTimeout = MODEM_DETECT_TIMEOUT;
      return;
   }

   // Count down until declaring modem active
   frameMs = pChan->pcmToPkt.SamplesPerFrame >> 3;
   if (frameMs < pChan->fax.msBeforeTimeout) {
      pChan->fax.msBeforeTimeout = pChan->fax.msBeforeTimeout - frameMs;
      return;
   }

   pChan->pcmToPkt.Coding = PCMU_64;
   pChan->Flags |= CF_MODEM_ACTIVE;
   T38_TRACE (&pChan->fax, EVT_MODEM_ACTIVE);
   pChan->fax.msBeforeTimeout = MODEM_ACTIVE_TIMEOUT;
   
   return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// modemFalseDetect -  modem false detection when CED/CNG signal does not last long enough
inline void modemFalseDetect (chanInfo_t *pChan, int state_ID) {

   AddToBufLog ("FALSE DETECT", pChan, state_ID);
   pChan->Flags &= ~(CF_MODEM_DETECT | CF_MODEM_ACTIVE);
   T38_TRACE (&pChan->fax, EVT_DETECT_TO);
   pChan->fax.msBeforeTimeout = 0;
   return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// modemEnd - Switch back to vocoder after signal loss
inline void modemEnd (chanInfo_t *pChan, int state_ID) {
   int frameMs;

   // Count down until declaring modem inactive
   frameMs = pChan->pcmToPkt.SamplesPerFrame >> 3;
   if (frameMs < pChan->fax.msBeforeTimeout) {
      pChan->fax.msBeforeTimeout = pChan->fax.msBeforeTimeout - frameMs;
      return;
   }
   AddToBufLog ("MODEM END", pChan, state_ID);
   pChan->Flags |= CF_REINIT_ENCODE | CF_REINIT_DECODE;
   pChan->Flags &= ~(CF_MODEM_DETECT | CF_MODEM_ACTIVE);
   pChan->pcmToPkt.Coding = pChan->pcmToPkt.PreferredCoding;
   T38_TRACE (&pChan->fax, EVT_MODEM_END);
   T38TraceDisable;
   pChan->fax.msBeforeTimeout = 0;
   return;
}

// 
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// processFaxRelay  - fax relay processing
//
// FUNCTION
// processFaxRelay  - runs the fax relay PCM interface function and attempts to
//                    copy as many packets as possible that are available from 
//                    the relay to the host packet interface. When the host buffer
//                    is full, the last packet remains in the channel's packet buffer
//                    until the host buffer has room
//
// Inputs
//     pChan - Channel data structure pointer
//     pcmInBuff - buffer holding input pcm samples
//     pcmOut - scratch buffer 
//     FrameSize - number of samples in this frame
//
// Outputs
//      nothing
//
// RETURNS
//  nothing
//

void processFaxRelay (chanInfo_t *pChan, void *pcmInBuff, void *pcmOut, int FrameSize) {
   ADT_Int16 healthVal;
   int pktLen, wordsFree, pktsPerFrame = 0;
   PktHdr_t hdr;
   struct RX_BLOCK *Rx; // pointer to faxrelay Rx block
   t38Packet_t *t38Storage;
   FAXStats_t  *pktStats;
   faxInfo_t   *faxData;
   unsigned char *pktBuff;
  #ifndef TDM_INTERFACE_AVAIL 
   short outSamples_temp[480];
  #endif

   // ---  Process in-bound PCM (pcmInBuff) and packets (previously stored in pChan->fax.pFax) 
   //      to generate out-bound PCM (pcmOut) and packets (stored in pChan->fax.pFax)
   faxData = &pChan->fax;

   t38Storage = pT38Packets[faxData->faxIndex];
   pktBuff  = (unsigned char *) t38Storage->t38Packet;
   pktStats = &t38Storage->pktStats;

   // --- Copy in-bound PCM to circular buffer for debug
   captureFaxInput (pChan, pcmInBuff, FrameSize);

   T38_TRACE (faxData, EVT_FAX_PCM_IN);
   healthVal = RlyRelayIf (pcmInBuff, pcmOut, FrameSize, pChan->fax.pFax);
   T38_TRACE (faxData, EVT_FAX_PCM_OUT);
   
   // --- Copy out-bound PCM to the TDM outbuffer
   if (faxData->t38InProgress) {

            copyLinearToCirc (pcmOut, &(pChan->pktToPcm.outbuffer), FrameSize);
#ifndef TDM_INTERFACE_AVAIL
           // read samples to simulate DMACopy read
           copyCircToLinear (&(pChan->pktToPcm.outbuffer), outSamples_temp, FrameSize);

           // write directly into encode pcm input circ buffer for tdm loopback
           copyLinearToCirc (pcmOut, pChan->pcmToPkt.activeInBuffer, FrameSize);
#endif
      captureFaxOutput (pChan, pcmOut, FrameSize);
   }


   if (healthVal != SEQ_HEALTHY) {
      STATS_INC (pktStats, healthErrors);
      STATS_OR  (pktStats, health, healthVal);
      AddToBufLog ("HEALTH ERROR", pChan, healthVal);

      faxEnd (pChan);
      InitRelay (pChan);
      return;      
   }

    // --- Copy out-bound packets (from faxData->pFax) to internal buffer (t38PpktBuff) for 
    //     delivery to host buffer via SendPktToHost
    while (pktStats != NULL)  {
        // Use existing packet from channel's packet buffer or get new one from FAX relay instance buffers
        if (t38Storage->pktLen != 0)  {
           pktLen = t38Storage->pktLen;
           T38_TRACE (faxData, EVT_PKT_RETRY);
        } else {
           T38_TRACE (faxData, EVT_PKT_FROM_RLY_IN);
           pktLen = RlyPacketFmRelay (faxData->pFax, pktBuff, BUFLEN);
           T38_TRACE (faxData, EVT_PKT_FROM_RLY_OUT);

           Rx = (struct RX_BLOCK *) (faxData->pFax->start_ptrs)->Rx_block_start;
           if (!faxData->t38InProgress) {
               // Identify start of modem via CED or CNG
               //          start of FAX via V.21
               //          end of modem via RX_ENERGY_DET_ID or RX_SIG_ANALYSIS_ID
               //          false modem detect via timeout before state change
               switch  (Rx->state_ID) {
               case RX_CED_ID:
//               case RX_CNG_ID:
                  if (pChan->Flags & CF_MODEM_ACTIVE)
                     faxData->msBeforeTimeout = MODEM_ACTIVE_TIMEOUT;
                  else {
                     modemStart (pChan, t38Storage);
                     AddToBufLog ("CED", pChan, Rx->state_ID);
                  }
                  return;

               case RX_V21_CH2_MESSAGE_ID:
                  T38TraceEnable;
                  T38_TRACE (faxData, EVT_V21);
                  AddToBufLog ("V21", pChan, 0);
                  faxStart (pChan, t38Storage);
                  break;

               case RX_ENERGY_DET_ID:
               case RX_SIG_ANALYSIS_ID:
                  if (pChan->Flags & CF_MODEM_ACTIVE) {
                     modemEnd (pChan, Rx->state_ID);
                  } else if (pChan->Flags & CF_MODEM_DETECT) {
                     modemFalseDetect (pChan, Rx->state_ID);
                  }
                  return;

               default:
                  if (pChan->Flags & CF_MODEM_ACTIVE)
                     faxData->msBeforeTimeout = MODEM_ACTIVE_TIMEOUT;
                  else if (pChan->Flags & CF_MODEM_DETECT) {
                     modemFalseDetect (pChan, Rx->state_ID);
                  }
                  return;
              }
           } else {
               // Identify end of fax / modem restart via CED
               switch  (Rx->state_ID) {
               case RX_CED_ID:
                  AddToBufLog ("CED", pChan, Rx->state_ID);
                  faxEnd (pChan);
                  modemStart (pChan, t38Storage);
                  break;
               }
           }
        }

        if (pktLen == 0) {
           // No packet to process - exit
           t38Storage->pktLen = 0;
           STATS_MAX (pktStats, MaxPerFrame, pktsPerFrame);
           return;
        } else if (0 < pktLen) {
           // Packet available - continue processing
           STATS_INC (pktStats, FromRelay);

           maxPktUpdate (pktLen);

           pktsPerFrame++;
        } else {
           // Packet too large for the packet buffer - discard and exit
           t38TooBig++;
           
           packetTooLarge (pktLen);
           t38Storage->pktLen = 0;
           
           T38_TRACE (faxData, EVT_RLY_TOO_SMALL);
           return;
        }

        // --- Copy packet to host buffer 
        wordsFree = getFreeSpace (pChan->pcmToPkt.outbuffer);
        if (wordsFree < ((pktLen+1)/2 + PKT_HDR_I16LEN)) {
           // no space in host buffer - leave packet in channel packet buffer
           t38Storage->pktLen = pktLen;
           STATS_INC (pktStats, HostFull);
           STATS_MAX (pktStats, MaxPerFrame,pktsPerFrame);
           T38_TRACE (faxData, EVT_HOST_FULL);
           return;
        }

        // copy packet to the host buffer
        t38Storage->pktLen = 0;
        STATS_INC (pktStats, ToNetwork);

        hdr.ChanId = pChan->ChannelId;
        hdr.PayldClass = PayClassT38;
        hdr.PayldType  = 0;
        hdr.PayldBytes = pktLen;

        SendPktToHost (pChan, &hdr, (ADT_UInt16 *)pktBuff);
        faxData->msBeforeTimeout = FAX_TIMEOUT;
//        AddToBufLog ("PKT SENT", pChan, hdr.TimeStamp);
        STATS_TRACE (pktStats, ToNetwork, faxData, EVT_RLY_PKT_OUT);

    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//  T38_Add_Packet  -  Add fax packet received from host into fax relay buffers
int T38_Add_Packet (chanInfo_t *pChan, void *pyld, int PayldBytes, ADT_UInt32 timeStamp) {

   int status;
   faxInfo_t   *faxData;
   FAXStats_t  *pktStats;
   t38Packet_t *t38Storage;


   faxData = &pChan->fax;
   t38Storage = pT38Packets[faxData->faxIndex];
   pktStats = &t38Storage->pktStats;
   STATS_INC (pktStats, FromNetwork);

   if ((faxData->faxMode == disabled) || (pktStats == NULL)) {
      // T38 packet was received while fax relay is disabled.
      return TRUE;
   }

   faxData->msBeforeTimeout = FAX_TIMEOUT;    // reset time for no packets sent or received
   faxData->faxPktRcvd++;                     // Indicate fax packet has been received

   T38TraceEnable;

   if (!faxData->t38InProgress) {

      T38_TRACE (faxData, EVT_FAX_START);
      faxStart (pChan, t38Storage);

   }
//   AddToBufLog ("PKT RCVD", pChan, timeStamp);

   // Add input packet to the relay
   T38_TRACE (faxData, EVT_PKT_TO_RLY_IN);
   status = RlyPacketToRelay (faxData->pFax, (unsigned char *)pyld, PayldBytes);
   T38_TRACE (faxData, EVT_PKT_TO_RLY_OUT);
   if (0 < status) {
      // packet accepted by the relay. 
      STATS_INC (pktStats, ToRelay);
      STATS_TRACE (pktStats, ToRelay, faxData, EVT_RLY_PKT_IN);
      return TRUE;
   } else if (status == 0) {
      // internal relay queue is full.
       STATS_INC (pktStats, RelayFull);
       T38_TRACE (faxData, EVT_RLY_FULL);
       return FALSE;
   } 
   // packet too large for fax buffer. 
   STATS_INC (pktStats, TooLarge);
   T38_TRACE (faxData, EVT_RLY_TOO_SMALL);
   return TRUE;
    
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// T38_Check_Timeout - Turn off fax mode and reestablish voice when no 
//                     packets are received or transmitted during timeout duration
void T38_Check_Timeout (chanInfo_t *pChan, int SamplesPerFrame) {

  if (pChan->fax.faxMode == faxOnly) return;

  if ((SamplesPerFrame >> 3) < pChan->fax.msBeforeTimeout) {
     pChan->fax.msBeforeTimeout -= (SamplesPerFrame>>3);
  } else if (pChan->Flags & CF_MODEM_DETECT) {
     // Timeout - Restore codec and remove flags
     AddToBufLog ("TIMEOUT", pChan, 0);
     faxEnd (pChan);
     T38_TRACE (&pChan->fax, EVT_FAX_TO);
     T38TraceDisable;
  }
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//  T38_Restart_Voice - Turn off fax mode and reestablish voice when a
//                      voice packet is received
//
//  NOTE: Initiating channel will expect to receive voice packets
//        until far end responds.  FAX should stay up under these 
//        circumstances
//
void T38_Restart_Voice (chanInfo_t *pChan) {

   if ((pChan->fax.faxMode == faxOnly) || (pChan->fax.faxPktRcvd == 0))
      return;

   AddToBufLog ("VOICE RESTART", pChan, 0);
   faxEnd (pChan);
   T38_TRACE (&pChan->fax, EVT_XFER_TO_VOICE);
   T38TraceDisable;
}

char bufLogString[128];
#ifdef _DEBUG
#define CACHE_NOWAIT 0

void startFaxCapture ();
void forceFaxCapture ();
void stopFaxCapture ();
extern CircBufInfo_t captureA;
extern CircBufInfo_t captureB;
extern CircBufInfo_t captureC;
extern CircBufInfo_t captureD;
extern void *TextLogReset ();
extern int   TextCaptureI8 ();

#define MesiLog 1
#define ADTLog  2

int faxFilter = MesiLog | ADTLog;


void BmrBufLog (char *s) {
   if (!(faxFilter & MesiLog)) return;
   
   CaptureText (s);
   return;
}

void AddToBufLog (char *string, chanInfo_t *pChan, int data) {
   if (!(faxFilter & ADTLog)) return;

   sprintf (bufLogString, "Time=%ld chan: %1d %-15s %5d\n", 
      pChan->fax.pFax->Seq->RealTime, pChan->ChannelId, string, data);
   CaptureText (bufLogString);
}

void *FaxBufrReset () {
   void *end;

   end = TextLogReset ();
   return end;
}


// Capture fax signal in captureC circular buffer
void captureFaxInput (chanInfo_t *pChan, void *data, int FrameSize) {
   CircBufInfo_t *cBuff;


   if (pChan->ChannelId == 0) { 
      cBuff = &captureA;
   } else if (pChan->ChannelId == 1) { 
      cBuff = &captureB;
   } else {
      return;
   }

   if ((pChan->Flags & CF_MODEM_DETECT) &&
      !(pChan->Flags & CF_MODEM_DETECTED)) {

      if (((chanTable[0]->Flags | chanTable[1]->Flags) & CF_MODEM_DETECTED) == 0)
#if 1 // todo RMF
      forceFaxCapture ();
#else
      startFaxCapture ();
#endif
      AddToBufLog ("MODEM DETECT", pChan, 0);
      pChan->Flags |= CF_MODEM_DETECTED | CF_FAX_CAPTURE;
      
   }

   if ((pChan->Flags & CF_MODEM_ACTIVE) &&
      !(pChan->Flags & CF_MODEM_ACTIVATED)) {
      AddToBufLog ("MODEM ACTIVE", pChan, 0);
      pChan->Flags |= CF_MODEM_ACTIVATED;
   }

   if (pChan->fax.t38InProgress && !(pChan->Flags & CF_FAX_ACTIVATED)) {
      AddToBufLog ("FAX ACTIVE", pChan, 0);
      pChan->Flags |= CF_FAX_ACTIVATED | CF_FAX_CAPTURE;
      
      memset (data, 0x55, 16);
   }

   if (!pChan->fax.t38InProgress && (pChan->Flags & CF_FAX_ACTIVATED)) {
      AddToBufLog ("FAX INACTIVE", pChan, 0);
      pChan->Flags &= ~CF_FAX_ACTIVATED;
   }
   
   
   if (!(pChan->Flags & CF_MODEM_ACTIVE) &&
        (pChan->Flags & CF_MODEM_ACTIVATED)) {
      AddToBufLog ("MODEM INACTIVE", pChan, 0);
      pChan->Flags &= ~(CF_MODEM_DETECTED | CF_MODEM_ACTIVATED);
   }

   if ((pChan->Flags & CF_FAX_CAPTURE) == 0) return;
   captureSignal (cBuff, data, FrameSize);
   return;
}


void captureFaxOutput (chanInfo_t *pChan, void *data, int FrameSize) {
   CircBufInfo_t *cBuff;

   if ((pChan->Flags & CF_FAX_CAPTURE) == 0) return;

   if (pChan->ChannelId == 0) { 
      cBuff = &captureC;
   } else if (pChan->ChannelId == 1) { 
      cBuff = &captureD;
   } else {
      return;
   }
   
   captureSignal (cBuff, data, FrameSize);
}

void startFaxCapture () {
   captureA.PutIndex = 0;
   captureB.PutIndex = 0;
   captureC.PutIndex = 0;
   captureD.PutIndex = 0;
   FaxBufrReset();
}


void stopFaxCapture () {

   struct {
      EventFifoHdr_t  header;    
      ADT_Int32 aDataAddr, aDataLenI32;
      ADT_Int32 bDataAddr, bDataLenI32;
      ADT_Int32 textAddr,  textLenI32;
   } msg;

  //CACHE_wbInvAllL2 (CACHE_NOWAIT);
   chanTable[0]->Flags &= ~CF_FAX_CAPTURE;
   chanTable[1]->Flags &= ~CF_FAX_CAPTURE;

   // Send FaxComplete Message (inbound capture)
   msg.header.channelId  = 0;
   msg.header.deviceSide = 0;
   
   msg.header.eventCode = EventFaxComplete;
   msg.header.eventLength =  sizeof (msg) - sizeof (EventFifoHdr_t);

   msg.aDataLenI32  = captureA.PutIndex / 2;
   msg.aDataAddr    = (((ADT_Int32) captureA.pBufrBase) >> 2) << 2;

   msg.bDataLenI32 = captureB.PutIndex / 2;
   msg.bDataAddr    = (((ADT_Int32) captureB.pBufrBase) >> 2) << 2;

   msg.textAddr   = (((ADT_Int32) PlayRecStartAddr) >> 2) << 2;
   msg.textLenI32 = TextCaptureI8 () / 4;
   writeEventIntoFifo ((void *) &msg);

   // Send FaxComplete Message (outbound capture)
   msg.header.channelId  = 0;
   msg.header.deviceSide = 1;
  
   msg.header.eventCode = EventFaxComplete;
   msg.header.eventLength =  sizeof (msg) - sizeof (EventFifoHdr_t);

   msg.aDataLenI32  = captureC.PutIndex / 2;
   msg.aDataAddr    = (((ADT_Int32) captureC.pBufrBase) >> 2) << 2;

   msg.bDataLenI32 = captureD.PutIndex / 2;
   msg.bDataAddr    = (((ADT_Int32) captureD.pBufrBase) >> 2) << 2;

   msg.textAddr   = 0;
   msg.textLenI32 = 0;
   writeEventIntoFifo ((void *) &msg);
   
   return;
}
void forceFaxCapture () {

   chanInfo_t *pChan;

   captureA.PutIndex = 0;
   captureB.PutIndex = 0;
   captureC.PutIndex = 0;
   captureD.PutIndex = 0;
   FaxBufrReset();

   pChan = chanTable[0];
   pChan->Flags |= CF_FAX_CAPTURE;
   pChan->fax.pFax->Seq->RealTime = 0;

   pChan = chanTable[1];
   pChan->Flags |= CF_FAX_CAPTURE;
   pChan->fax.pFax->Seq->RealTime = 0;
}

void maxPktUpdate (int pktLen) {
   if (pktLen <= maxPktLen) return;
   
   maxPktLen = pktLen;
   AddToBufLog ("MAX PKT LEN", chanTable[0], pktLen);
}

void packetTooLarge (int pktLen) {
   AddToBufLog ("PKT SIZE ERROR", chanTable[0], 0-pktLen);
   AppErr ("Packet too large", TRUE);
}

#else
void BmrBufLog (char *s) { return; }
void *FaxBufrReset () { return (void *) 0x9000000; }
#endif

void BmrAddLink (short **addr, char *name) { return; }

char *NullString = "";
char *BmrAddr2Txt (short **addr) { return NullString; }

