#include "GpakDefs.h"
// ADT's Implementation of G729

int g729EncInit(void *chan, void *scratch, short vadEnable) {
    G729AB_Encode_Init ((G729ENC_ADT_Instance_t *)chan, (G729ENC_ADT_Scratch_t *) scratch);
    return 0;
}


int g729Encode (void *chan, void *pcm, void *data, short Vad, short *FrameType) {
    if (Vad) Vad = 1;
    G729AB_Encode ((G729ENC_ADT_Instance_t *)chan, (short *)pcm, (char *)data, Vad, FrameType);
    return 0;
}

int g729DecInit(void *chan, void *scratch) {
    G729AB_Decode_Init ((G729DEC_ADT_Instance_t *)chan, (G729DEC_ADT_Scratch_t *) scratch);
    return 0;    
}

int g729Decode (void *chan, void *pyld, void *pcm, short FrameErase, short VadFlag) {
    G729AB_Decode ((G729DEC_ADT_Instance_t *)chan, (char *)pyld, (short *)pcm, FrameErase, VadFlag); 
    return 0;
}


