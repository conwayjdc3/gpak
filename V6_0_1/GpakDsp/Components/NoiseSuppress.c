/*
 * Copyright (c) 2005, Adaptive Digital Technologies, Inc.
 *
 * File Name: NoiseSuppress.c
 *
 * Description:
 *   This file contains functions to align vocoder framing with noise
 *   suppression framing.
 *
 * Version: 1.0
 *
 * Revision History:
 *
 *   5/2005  - Initial release
 *
 */
   
// Application related header files.
#include "GpakDefs.h"
#include "GpakErrs.h"
#include "GpakExts.h"
#include "GpakHpi.h"
#include "sysconfig.h"

#define inRange(lo,val,hi)  ((lo <= val) && (val <= hi)) 

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// NoiseSuppress
//
// FUNCTION
//   This function groups voice samples from a linear input buffer (**workPtr) into
// noise frames and then performs the noise suppression.  Extra samples from the
// linear input buffer are stored in the remnant buffer to be used during the next
// vocoder frame.
//
//   
//
//
//
//        Input buffer                                       Output Buffer   
//    Vocoder       Noise                              Vocoder        Noise
//    Framing       Framing      Remnant buffer        Framing        Framing
//                        
//         |-------|-----         |-------|
//         |  1 A  |              |  1 A  |
//     ----|-------|          ----|-------|             -----|-------|-----
//         |  1 B  |              |  1 B  |                  |  1 A  |
//         |-------|-----         |-------|                  |-------|
//         |   2   |                                         |  1 B  |
//         |-------|-----                                    |-------|-----
//         |   3   |                                         |   2   |
//         |-------|-----                                    |-------|-----
//         |  ...  |  ...                                    |   3   |
//         |-------|-----         |-------|                  |-------|-----
//         |  N A  |              |  N A  |                  |  ...  |
//     ----|-------|          ----|-------|             -----|-------|-----
//         |  N B  |              |  N B  |
//         |-------|-----         |-------|
//
//
//    Noise suppression of noise frames that span vocoder frame boundaries
//    (noise frames 1 and N in above example) is performed using the remnant 
//    buffer as input and output to NCAN_ADT_cancel.  NoiseSuppress performs the copy
//    of these frames from the remnant buffer into the final output buffer.
//
//    Noise suppression of noise frames that are contained within vocoder frame boundaries
//    (noise frames 2, 3, ... in above example) is performed using the input buffer as input
//    and output to NCAN_ADT_cancel.  NoiseSuppress does NOT perform the copy of these
//    frames into the final output buffer.
//
//    NOTE: inBufr is updated to point to the next sample within the input buffer 
//    that needs to be copied to the output buffer.
//   
//    The returned value identifies the number of samples that are ready for copy from
//    the circular input buffer to the output buffer
//
//
ADT_UInt16 NoiseSuppress (NSuppress_t *Noise,        ADT_PCM16 **inBufr, 
                          CircBufInfo_t *outBuffer,  ADT_UInt16 FrameSize) {

   ADT_UInt16 copySize;
   ADT_UInt16 samplesNotTransferred = 0;
   ADT_PCM16 *pInWork;

   pInWork = *inBufr;
    
   // If data exists in noise remnant buffer, fill with data from vocoder buffer to process noise frame
   if (Noise->Offset != 0) {
        copySize = NCAN_FRAME_SIZE - Noise->Offset;
        if (FrameSize < copySize) copySize = FrameSize;

        i16cpy (Noise->Remnant + Noise->Offset, pInWork, copySize);
        Noise->Offset += copySize;

        if (Noise->Offset < NCAN_FRAME_SIZE) return 0;

        // Suppress noise using remnant buffer
        // NOTE: copy from remnant buffer directly into circular out buffer reduces memory and processing
        NCAN_ADT_cancel (Noise->Bufr, Noise->Remnant, 0, Noise->Remnant);

        copyLinearToCirc ((ADT_UInt16 *) Noise->Remnant, outBuffer, NCAN_FRAME_SIZE);

        Noise->Offset = 0;
        pInWork   += copySize;
        FrameSize -= copySize;
        *inBufr = pInWork;
   }

   // Suppress noise using input buffer until there are insufficient samples to make a noise frame
   // NOTE: final linear to circular copy done by caller
   while (FrameSize >= NCAN_FRAME_SIZE) {
      NCAN_ADT_cancel (Noise->Bufr, pInWork, 0, pInWork);
      samplesNotTransferred += NCAN_FRAME_SIZE;

      FrameSize -= NCAN_FRAME_SIZE;
      pInWork   += NCAN_FRAME_SIZE;
   }

   // Copy any lingering data into NoiseRemnant for processing during next vocoder frame
   if (FrameSize > 0) {
      i16cpy (Noise->Remnant + Noise->Offset, pInWork, FrameSize);
      Noise->Offset = FrameSize;
   }
   return samplesNotTransferred;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ProcWriteSystemParmsMsg - Process a Write System Parameters message.
//
// FUNCTION
//   This function performs all processing for a host's Write System Parameters
//  message and builds the reply message.
//
// RETURNS
//  Return status for reply message.
//
GPAK_SysParmsStat_t ProcWriteNoiseParmsMsg (ADT_Int16 *pCmd, ADT_UInt16 *pReply) {


   ADT_Int16 MaxAttenuation;
   ADT_Int16 VadLoThreshold;  // VAD Noise Floor threshold
   ADT_Int16 VadHiThreshold;  // VAD Noise Floor threshold
   ADT_Int16 VadHangTime;     // VAD Hang Over time
   ADT_Int16 VadWindowSize;   // VAD Analysis Window size

   // Prepare the reply message.
   pReply[0] |= (MSG_WRITE_NOISE_PARAMS_REPLY << 8);

   MaxAttenuation = pCmd[1];
   VadLoThreshold = pCmd[2];
   VadHiThreshold = pCmd[3];
   VadHangTime    = pCmd[4];
   VadWindowSize  = pCmd[5];

   if (!inRange (MIN_NOISE_ATTENUATION, MaxAttenuation, MAX_NOISE_ATTENUATION))
         return Sp_BadNoiseAttenuation;
         
   if (!inRange (MIN_VAD_NOISE_FLOOR, VadLoThreshold, MAX_VAD_NOISE_FLOOR-2))
       return Sp_BadVadNoiseFloor;

   if (!inRange (VadLoThreshold + 2, VadHiThreshold, MAX_VAD_NOISE_FLOOR))
       return Sp_BadVadNoiseCeiling;

   if (!inRange (MIN_VAD_HANG_TIME, VadHangTime, MAX_VAD_HANG_TIME))
          return Sp_BadVadHangTime;

   if (!inRange (MIN_NOISE_WINDOW, VadWindowSize, MAX_NOISE_WINDOW))
       return Sp_BadVadWindowSize;
   
   NCAN_Params.NC_Params.MaxAttendB    = MaxAttenuation;
   NCAN_Params.VAD_Params.LowThreshdB  = VadLoThreshold;
   NCAN_Params.VAD_Params.HighThreshdB = VadHiThreshold;
   NCAN_Params.VAD_Params.HangMSec     = VadHangTime;
   NCAN_Params.VAD_Params.WindowSize   = VadWindowSize;


   // Return with the reply's return status.
   return (Sp_Success);
}



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ProcReadNoiseParamsMsg - Process a Read System Noise Parameters message.
//
// FUNCTION
//   This function performs all processing for a host's Read System Noise Parameters
//  message and builds the reply message.
//
// RETURNS
//  Length (16-bit words) of reply message.
//
ADT_UInt16 ProcReadNoiseParmsMsg (ADT_Int16 *pCmd, ADT_Int16 *pReply) {

   // Prepare the reply message.
   pReply[0] |= (MSG_READ_NOISE_PARAMS_REPLY << 8);

   // Store the Sytem Parameters information in the reply message.
   pReply[1] = NCAN_Params.NC_Params.MaxAttendB;
   pReply[2] = NCAN_Params.VAD_Params.LowThreshdB;
   pReply[3] = NCAN_Params.VAD_Params.HighThreshdB;
   pReply[4] = NCAN_Params.VAD_Params.HangMSec;
   pReply[5] = NCAN_Params.VAD_Params.WindowSize;
   return 6;
}



