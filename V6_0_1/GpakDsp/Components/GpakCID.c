#include "GpakDefs.h"
#include "sysconfig.h"
#include "GpakExts.h"
#include "GpakHpi.h"

#define REPORT_CID2_CODES

//#define CID_TEST // ENABLE this for standalone test in alg_test 
#ifdef CID_TEST
extern far ADT_Int32 *getCidTestScratchPtr (int SamplesPerFrame);
#endif

extern CircBufInfo_t eventFIFOInfo;
// Macro to limit processing rates to 11.25 milliseconds.
// This allows tone detection to work correctly.
#define setCallRate(sampsPerCall,sampsPerFrame)    \
   sampsPerCall = sampsPerFrame;                   \
   while (90 < sampsPerCall) sampsPerCall >>= 1;

#ifndef _DEBUG
#pragma CODE_SECTION (cidCapt, "SLOW_PROG_SECT")
   static inline void cidCapt (int chan, ADT_PCM16 *pcmBuff, int samps, int ready)  { return; }
   #define logCid(chan,side,type,prevstate,state,len)
#else
#define CID_CAPT_LEN (8000 * 5)
                          
static int cidCaptChan = 0;
static int cidCaptActive = 0;
static int initCidCapt = 1;
static CircBufInfo_t cidCaptCirc;
static ADT_UInt16 cidCaptBuf[CID_CAPT_LEN];
#pragma DATA_SECTION (cidCaptBuf, "SLOW_DATA_SECT")
#pragma DATA_ALIGN   (cidCaptBuf, CACHE_L2_LINE_SIZE)

#define CIDRX 0
#define CIDTX 1

#define CID_LOG_SIZE 0x100
int cidLogIdx = 0;
typedef struct cidLog {
   ADT_UInt32 time;
   ADT_UInt8 chan;
   ADT_UInt8 side;
   ADT_UInt8 type;
   ADT_UInt8 state;
   ADT_UInt8 samps;
} cidLog;
cidLog cidLogData[CID_LOG_SIZE];

#pragma CODE_SECTION (logCid, "SLOW_PROG_SECT")
void logCid (int chan, GpakDeviceSide_t side, int type, int prevState, int state, int samps) {
   cidLog *cidData;
   int cidIdx;

   if (prevState == state) return;
   SWI_disable ();
   cidIdx = cidLogIdx;
   cidLogIdx++;
   if (CID_LOG_SIZE < cidLogIdx) cidLogIdx = 0;
   SWI_enable ();

   cidData = &cidLogData[cidIdx];
   cidData->time  = ApiBlock.DmaSwiCnt;
   cidData->chan = (ADT_UInt8) chan;
   cidData->side = (ADT_UInt8) side;
   cidData->type = (ADT_UInt8) type;
   cidData->state = (ADT_UInt8) state;
   cidData->samps = (ADT_UInt8) samps;
}

#pragma CODE_SECTION (cidCapt, "SLOW_PROG_SECT")
void cidCapt (int chan, ADT_PCM16 *pcmBuff, int samps, int ready) {

    if (cidCaptChan != chan) return;
    if (cidCaptActive == 0) return;
    if (initCidCapt) return;

    copyLinearToCirc (pcmBuff, &cidCaptCirc, samps);
    cidCaptCirc.TakeIndex = cidCaptCirc.PutIndex;

}

#ifndef CID_LOOP
   #define CidLoopWr(chanId, buf)
   #define CidLoopRd(chanId, buf)
#else
ADT_PCM16 cidSamples[2][161];
#pragma DATA_SECTION (silenceBuf, "SLOW_DATA_SECT")
#pragma DATA_ALIGN   (silenceBuf, CACHE_L2_LINE_SIZE)

ADT_PCM16 silenceBuf[80];
#pragma DATA_SECTION (cidSamples, "SLOW_DATA_SECT")
#pragma DATA_ALIGN   (cidSamples, CACHE_L2_LINE_SIZE)

int rxToTxLink[2] = {1, 0};
CircBufInfo_t cidCirc[2] = {
    {(void *) cidSamples[0], 161, 0, 0, 0},
    {(void *) cidSamples[1], 161, 0, 0, 0}};

int loopActive = 1;
int loopReset = 1;

#pragma CODE_SECTION (CidLoopWr, "SLOW_PROG_SECT")
void CidLoopWr (int chanId, ADT_PCM16 *buf) {
   CircBufInfo_t *pCirc;
   ADT_Word I16Avail;

    if (loopReset) {
        loopReset = 0;
        cidCirc[0].PutIndex = 0;
        cidCirc[0].TakeIndex = 0;
        cidCirc[1].PutIndex = 0;
        cidCirc[1].TakeIndex = 0;
    }

    if ((chanId > 1)  || (loopActive == 0)) return;

    pCirc = &cidCirc[chanId];
    I16Avail = getFreeSpace(pCirc);
    if (I16Avail >= 80)
        copyLinearToCirc(buf, pCirc, 80);
}

#pragma CODE_SECTION (CidLoopRd, "SLOW_PROG_SECT")
void CidLoopRd (int chanId, ADT_PCM16 *buf) {
   CircBufInfo_t *pCirc;
   ADT_Word I16Avail;
   int txCircIdx;

   txCircIdx = rxToTxLink[chanId];
    if ((chanId > 1) || (loopActive == 0) || (txCircIdx > 1)) return;

    pCirc = &cidCirc[txCircIdx];

    I16Avail = getFreeSpace(pCirc);
    if ((pCirc->BufrSize - I16Avail) >= 80)
        copyCircToLinear (pCirc, buf, 80);
}
#endif

//====================================================================
// Channel setup for Called ID
#pragma CODE_SECTION (NullCID, "SLOW_PROG_SECT")
void NullCID (cidInfo_t *pCid) {
    memset(pCid, 0, sizeof(cidInfo_t));
    pCid->RxCIDIndex = VOID_INDEX;
    pCid->TxCIDIndex = VOID_INDEX;
}

#pragma CODE_SECTION (getRxCidPtrs, "SLOW_PROG_SECT")
void getRxCidPtrs (int i, CIDRX_Inst_t **ppRxCidChan, ADT_UInt8 **pRxCidBuf) {
    *ppRxCidChan = rxCidPool[i];
    *pRxCidBuf   = &(rxCidBuffer[i * rxCidBufferI8]);
}

#pragma CODE_SECTION (getTxCidPtrs, "SLOW_PROG_SECT")
void getTxCidPtrs (int i, CIDTX_Inst_t **ppTxCidChan, ADT_UInt8 **pTxCidMsgBuf, ADT_UInt8 **pTxCidBrstBuf) {
    *ppTxCidChan = txCidPool[i];
    *pTxCidMsgBuf   = &(txCidMsgBuffer [i * txCidBufferI8]);
    *pTxCidBrstBuf  = &(txCidBrstBuffer[i * txCidBurstI8]);
}


//====================================================================
// Transmitted caller ID status and processing
//{ Message
//  Word:Bits
//     0:ff00 - Message ID
//     0:00ff - Core ID
//     1:ff00 - Channel ID 
//     2:ffff - Payload length I8
//     3:ff00 - CID Message type
//     3:0001 - Channel side (A or B)
//     n:ffff - CID Message

//   Response
//  Word:Bits
//     0:ff00 - Reply ID
//     0:00ff - Core ID
//     1:ff00 - Channel ID 
//     1:00ff - Reply status
//}
#pragma CODE_SECTION (ProcTxCallerIDMsg, "SLOW_PROG_SECT")
ADT_UInt16 ProcTxCallerIDMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply) { 
   ADT_UInt16      ChannelId;      // channel identifier
   ADT_UInt16      PaylenI8;
   ADT_UInt8       MsgType;
   chanInfo_t      *pChan;          // pointer to Channel structure
   cidInfo_t       *cidInfo;
   GpakDeviceSide_t side;
   GpakCidMode_t *CIDMode;
   GpakCIDTypes type2CID;

   // Get the Channel Id and Control Code from the command message.
   ChannelId = (pCmd[1] >> 8) & 0x00FF;
   type2CID   = (GpakCIDTypes) (pCmd[1]  & 0x00FF);
   PaylenI8  = pCmd[2];

   // Prepare the reply message.
   pReply[0] |= (MSG_WRITE_TXCID_DATA_REPLY << 8);
   pReply[1]  = (ChannelId << 8);

   if (ChannelId >= sysConfig.maxNumChannels)
      return Scp_InvalidChannel;

    // Verify that the Channel is active
   pChan = chanTable[ChannelId];
   if (pChan->channelType == inactive) {
      return Scp_InactiveChannel;
   }

   side = (GpakDeviceSide_t) (pCmd[3] & 1);
   MsgType = (ADT_UInt8) (pCmd[3] >> 8);
   if (side == ADevice) {
      cidInfo = &(pChan->pktToPcm.cidInfo);
      CIDMode = &pChan->pktToPcm.CIDMode;
   } else {
      cidInfo = &(pChan->pcmToPkt.cidInfo);
      CIDMode = &pChan->pcmToPkt.CIDMode;
   }

    // Verify that Tx CID is enabled
   if (!(*CIDMode & CidTransmit))
      return Scp_TxCidNotEnabled;

   if ((sysConfig.type2CID == 0) && (type2CID)) // type 2 CID not supported in Gpak build
      return Scp_Cid2NotEnabled;

   if (type2CID) { // checking the seizure and mark bits range
      if ((sysConfig.numMarkBytes < 7) || (13 < sysConfig.numMarkBytes))
         return Scp_TxCid2MarkInvalid;
   }

   // Verify that the CID payload fits in the CID Msg buffer
   if (NUM_BURST_DATA_BYTES < PaylenI8 + 1)
      return Scp_TxPayloadTooBig;

   // Protect framing task from operating on parameters while change is being made
   CLEAR_INST_CACHE (cidInfo->pTxCidMsgBuff, PaylenI8 + 1);
   *CIDMode &= ~CidTransmit;
   FLUSH_wait ();

   cidInfo->pTxCidMsgBuff[0] = MsgType;
   i8cpy (&cidInfo->pTxCidMsgBuff[1], &pCmd[4], PaylenI8);
   FLUSH_INST_CACHE (cidInfo->pTxCidMsgBuff, PaylenI8 + 1);
   FLUSH_wait ();

   cidInfo->type2CID = type2CID;
   cidInfo->txBurstState = CID_TX_IDLE_CODE;
   cidInfo->numTxBurstBytes = PaylenI8;
   *CIDMode |= CidTransmit;

   return Scp_Success;
}

#pragma CODE_SECTION (reportTxCidStatus, "MEDIUM_PROG_SECT")
static void reportTxCidStatus (int channelId, GpakDeviceSide_t deviceSide) {
    EventFifoMsg_t eventMsg;

    eventMsg.header.channelId   = channelId;
    eventMsg.header.eventCode   = EventTxCidMessageComplete;
    eventMsg.header.eventLength = EVENT_FIFO_MSGLEN_ZERO; 
    eventMsg.header.deviceSide  = deviceSide;
    writeEventIntoFifo (&eventMsg);
    return ;
}

#ifndef REPORT_CID2_CODES
#pragma CODE_SECTION (reportTxCid2Code, "MEDIUM_PROG_SECT")
static inline void reportTxCid2Code (int channelId, GpakDeviceSide_t deviceSide, ADT_Int16 Status) { return; }
#else
#pragma CODE_SECTION (reportTxCid2Code, "MEDIUM_PROG_SECT")
static void reportTxCid2Code (int channelId, GpakDeviceSide_t deviceSide, ADT_Int16 Status) {
   EventFifoMsg_t eventMsg;
   ADT_UInt16 eventData;
   switch (Status) {
   case CID_TX_SENDING_FSK_CODE: eventData = EventTxCidSendingFsk;     break;
   case CID_TX_SENDING_TAS_CODE: eventData = EventTxCidSendingTas;     break;
   case CID_TX_WAITING_ACK_CODE: eventData = EventTxCidWaitingAck;     break;
   case CID_TX_FAILED_ACK_CODE:  eventData = EventTxCidFailedAck;      break;

   default:
      eventData = (1000 + Status);
      break;
   }

   eventMsg.header.channelId   = channelId;
   eventMsg.header.eventCode   = EventCID2Tx;
   eventMsg.header.eventLength = 6; 
   eventMsg.header.deviceSide  = deviceSide;
   eventMsg.payload[0] = eventData;

   eventMsg.payload[1] = (ADT_UInt16)(ApiBlock.DmaSwiCnt&0xFFFF);
   eventMsg.payload[2] = (ADT_UInt16)((ApiBlock.DmaSwiCnt >> 16)&0xFFFF);
   writeEventIntoFifo (&eventMsg);
   return ;
}
#endif

#pragma CODE_SECTION (txCidProcess, "MEDIUM_PROG_SECT")
void txCidProcess (int chanId, GpakDeviceSide_t side, cidInfo_t *pInfo, ADT_PCM16 *pcmBuff, int sampsPerFrame) {
   CIDTX_Params_t parms;
   ADT_UInt16 prevState;
   int sampsPerCall;

   setCallRate (sampsPerCall,sampsPerFrame);

   // state = inactive and numTxBurstBytes = 0 indicates Tx Cid is inactive.
   // return with no processing performed.
   if ((pInfo->numTxBurstBytes <= 0) && (pInfo->txBurstState == CID_TX_IDLE_CODE)) return;

   // state = inactive and 0 < numTxBurstBytes indicates start of new burst.
   // Initialize Tx CID algorithm.
   if (pInfo->txBurstState == CID_TX_IDLE_CODE) {
      CLEAR_INST_CACHE (pInfo->pTxCidMsgBuff, pInfo->numTxBurstBytes + 1);

      parms.formatBurstFlag     = 1;   // CID formats seizure bytes, marker bytes, and checksum.
      parms.numSeizureBytes     = sysConfig.numSeizureBytes;

      parms.numMarkBytes        = sysConfig.numMarkBytes;   
      parms.numPostambleBytes   = sysConfig.numPostambleBytes;
      parms.fskType             = sysConfig.fskType;  
      parms.frameSize           = sampsPerCall;
      parms.fskMarkLevel        = sysConfig.fskLevel;
      parms.fskSpaceLevel       = sysConfig.fskLevel;

      parms.msgType             = pInfo->pTxCidMsgBuff[0];  
      parms.msgLen              = pInfo->numTxBurstBytes;   

      // For the type 2, more init parameters 
      if (sysConfig.type2CID) {
         CIDTX2_Inst_t *pTxCID2Inst = (CIDTX2_Inst_t *)pInfo->pTxCIDInst;
         parms.TAS_Params.TAS_Level = -10;
         parms.TAS_Params.toneTasHandler = &pTxCID2Inst->tasGenInstance;
         parms.TAS_Params.toneAckHandler = &pTxCID2Inst->ackDetInstance;
         parms.TAS_Params.toneAckSctrach = getCidScratchPtr(sampsPerFrame);
         if (pInfo->type2CID) {
            parms.type2Enable = 1;
            parms.numSeizureBytes = 0;
         } else {
            parms.type2Enable = 0;
         }
      }
      FLUSH_wait ();
      CIDTX_ADT_init (pInfo->pTxCIDInst, &parms, &pInfo->pTxCidMsgBuff[1], 
                      pInfo->pTxCidBrstBuff, &pInfo->numTxBurstBytes);
      CLEAR_INST_CACHE (pInfo->pTxCidMsgBuff, pInfo->numTxBurstBytes + 1);
      logCid (chanId, side, CIDTX, -1, pInfo->txBurstState, pInfo->numTxBurstBytes);
   }

   // state active - transmit
   do {

#ifdef CID_LOOP
      if ((pInfo->numTxBurstBytes == 0) && (pInfo->txBurstState == CID_TX_IDLE_CODE)) {
         memset (silenceBuf, 0, sizeof(ADT_PCM16) * 80);
         CidLoopWr (chanId, silenceBuf);
         return;
      }
#endif
      // state = active indicates burst in progress. Run the Tx CID algorithm.
      prevState = pInfo->txBurstState;
      pInfo->txBurstState = CIDTX_ADT_transmit (pInfo->pTxCIDInst, pcmBuff);
      
      CidLoopWr (chanId, pcmBuff);
      sampsPerFrame -= sampsPerCall;
      pcmBuff       += sampsPerCall;

      logCid (chanId, side, CIDTX, prevState, pInfo->txBurstState, pInfo->numTxBurstBytes);

      // The burst just finished.  Report status and exit 
      if (pInfo->txBurstState == CID_TX_IDLE_CODE) {
         pInfo->numTxBurstBytes = 0;
         reportTxCidStatus (chanId, side);
         break;
      }

      // Report caller ID type 2 status transitions
      if (!sysConfig.type2CID) continue;

      if (pInfo->txBurstState != prevState)
         reportTxCid2Code (chanId, side, pInfo->txBurstState);
      if (pInfo->txBurstState == CID_TX_FAILED_ACK_CODE)
         pInfo->numTxBurstBytes = 0;

   } while (sampsPerCall <= sampsPerFrame);
}

//====================================================================
// Received caller ID status and processing


#pragma CODE_SECTION (rxCidInit, "SLOW_PROG_SECT")
void rxCidInit (cidInfo_t *pInfo, int sampsPerFrame) {

   CIDRX_Params_t  params;
   ADT_Int32 *pScr;
   int sampsPerCall;

   // Processing must be divided into frames of 11.25 milliseconds
   // or less to allow detection of transitions in tones
   sampsPerCall = sampsPerFrame;
   while (88 < sampsPerCall) sampsPerCall >>= 1;
   params.frameSize = sampsPerCall; //sampsPerFrame;

   params.fskType = sysConfig.fskType;
#ifdef CID_TEST
   pScr = getCidTestScratchPtr(sampsPerFrame);
#else
   pScr = getCidScratchPtr (sampsPerFrame);
#endif
    
   // For the type 2, more init parameters 
   if (sysConfig.type2CID) {
      CIDRX2_Inst_t *pRxCID2Inst = (CIDRX2_Inst_t *)pInfo->pRxCIDInst;
      params.TAS_Params.ACK_Level = -10;
      params.TAS_Params.toneGenNotify = NULL;
      params.TAS_Params.toneGenHandler = &pRxCID2Inst->ackGenInstance;
      params.TAS_Params.toneTasHandler = &pRxCID2Inst->tasDetInstance;

      if (pInfo->type2CID) {
         params.type2Enable = 1;
      } else {
         params.type2Enable = 0;
      }
   }
    
   CIDRX_ADT_init (pInfo->pRxCIDInst, &params, pInfo->pRxCidBuff, pScr);
   pInfo->rxCidState = CID_RX_OK_CODE;
   logCid (-1, ADevice, CIDRX, CID_RX_MESSAGE_AVAILABLE_CODE, pInfo->rxCidState, pInfo->rxMsgLenI8);

#ifdef _DEBUG
    if (initCidCapt) {
        initCidCapt = 0;
        cidCaptCirc.pBufrBase = cidCaptBuf;
        cidCaptCirc.BufrSize = CID_CAPT_LEN;
        cidCaptCirc.PutIndex = 0;
        cidCaptCirc.TakeIndex = 0;
        cidCaptCirc.SlipSamps = 0;
        cidCaptActive = 1;
    }
#endif
}
#pragma CODE_SECTION (reportRxCid2Code, "MEDIUM_PROG_SECT")

#ifndef REPORT_CID2_CODES
static inline void reportRxCid2Code(int channelId, GpakDeviceSide_t deviceSide, ADT_Int16 Status) {   return; }
#else
static void  reportRxCid2Code(int channelId, GpakDeviceSide_t deviceSide, ADT_Int16 Status) {
   EventFifoMsg_t eventMsg;
   ADT_UInt16 eventData;
   switch (Status) {
   case CID_RX_WAIT_TAS_CODE:      eventData = EventRxCidWaitingTas;      break;
   case CID_RX_TAS_DETECTED_CODE:  eventData = EventRxCidTasDetected;     break;
   case CID_RX_SENDING_ACK_CODE:   eventData = EventRxCidSendingAck;      break;
   case CID_RX_WAIT_MARK_CODE:     eventData = EventRxCidWaitMark;        break;
   case CID_RX_TAS_TOO_SHORT_CODE: eventData = EventRxCidTasTooShort;     break;
   case CID_RX_FAILED_MARK_CODE:   eventData = EventRxCidFailedMark;      break;

   default:
      eventData = (1000 + Status);
      break;
   }

   eventMsg.header.channelId   = channelId;
   eventMsg.header.eventCode   = EventCID2Rx;
   eventMsg.header.eventLength = 6; 
   eventMsg.header.deviceSide  = deviceSide;
   eventMsg.payload[0] = eventData;
   
   eventMsg.payload[1] = (ADT_UInt16)ApiBlock.DmaSwiCnt&0xFFFF;
   eventMsg.payload[2] = (ADT_UInt16)((ApiBlock.DmaSwiCnt >> 16)&0xFFFF);
   writeEventIntoFifo (&eventMsg);
   return ;
}
#endif

#pragma CODE_SECTION (reportRxCidLocked, "MEDIUM_PROG_SECT")
static void reportRxCidLocked (int channelId, GpakDeviceSide_t deviceSide) {
   EventFifoMsg_t eventMsg;

   eventMsg.header.channelId   = channelId;
   eventMsg.header.eventCode   = EventRxCidCarrierLock;
   eventMsg.header.eventLength = EVENT_FIFO_MSGLEN_ZERO; 
   eventMsg.header.deviceSide  = deviceSide;
   writeEventIntoFifo (&eventMsg);
   return ;
}

#pragma CODE_SECTION (reportRxCidMsgReady, "MEDIUM_PROG_SECT")
static int reportRxCidMsgReady (int channelId, GpakDeviceSide_t deviceSide, ADT_UInt8 *buf, ADT_UInt16 lenI8) {
   ADT_UInt16 byteLen, i16Len, payBytes;
   EventFifoCIDMsg_t eventCidMsg;

   // Pad the payload if necessary to ensure multiples of 4-bytes to accomodate host transfer size.
   // Add 2 to account for the actual Rx Cid msg length field (stored in the 1st 16-bit element of the event payload).
   payBytes = ((lenI8 + 2 + 3)>>2)<<2;
   byteLen = payBytes + (EVENT_HDR_I16LEN * 2);   

   i16Len  = byteLen/2;

   // invalidate cache to force a refresh of pointers accessed by the host
   if (sysConfig.enab.Bits.apiCacheEnable) {
      CACHE_INV(ApiBlock.pRdBlock, CACHE_L2_LINE_SIZE);
      eventFIFOInfo.TakeIndex = ApiBlock.pRdBlock->EventFIFOTakeIndex;
      eventFIFOInfo.PutIndex  = ApiBlock.pWrBlock->EventFIFOPutIndex;
   }

   // Check if the Rx CID message will fit in the event fifo
   if (getFreeSpace (&eventFIFOInfo) - 2 < i16Len) 
      return CID_RX_MESSAGE_AVAILABLE_CODE;

   eventCidMsg.header.channelId   = channelId;
   eventCidMsg.header.eventCode   = EventRxCidMessageComplete;
   eventCidMsg.header.eventLength = payBytes; 
   eventCidMsg.header.deviceSide  = deviceSide;

   // the first element of rx cid payload is the Rx msg length itself
   eventCidMsg.payload[0] = lenI8;
   memcpy (&(eventCidMsg.payload[1]), buf, lenI8);
   writeEventIntoFifo ((EventFifoMsg_t *)&eventCidMsg);
   return CID_RX_OK_CODE;
}

#pragma CODE_SECTION (rxCidProcess, "MEDIUM_PROG_SECT")
void rxCidProcess (int chanId, GpakDeviceSide_t side, cidInfo_t *pInfo, ADT_PCM16 *pcmBuff, int sampsPerFrame) {
   ADT_UInt16 prevState;
   int sampsPerCall;

   setCallRate (sampsPerCall,sampsPerFrame);

   do {

      prevState = pInfo->rxCidState;

      CidLoopRd (chanId, pcmBuff);
      pInfo->rxCidState = CIDRX_ADT_receive (pInfo->pRxCIDInst, pcmBuff, &pInfo->rxMsgLenI8);
      logCid (chanId, side, CIDRX, prevState, pInfo->rxCidState, pInfo->rxMsgLenI8);

      if ((prevState != pInfo->rxCidState) && (pInfo->rxCidState == CID_RX_DECODE_MESSSAGE_CODE)) {
         reportRxCidLocked (chanId, side);
      }

      cidCapt (chanId, pcmBuff, sampsPerFrame, (pInfo->rxCidState == CID_RX_MESSAGE_AVAILABLE_CODE));
      sampsPerFrame -= sampsPerCall;
      pcmBuff       += sampsPerCall;

      if ((pInfo->rxCidState == CID_RX_MESSAGE_AVAILABLE_CODE) || 
          ((prevState == CID_RX_DECODE_MESSSAGE_CODE) && (pInfo->rxCidState == CID_STOPBIT_ERROR_CODE) && (pInfo->rxMsgLenI8 > 2) ) ||
          ((prevState == CID_RX_DECODE_MESSSAGE_CODE) && (pInfo->rxCidState == CID_STARTBIT_ERROR_CODE) && (pInfo->rxMsgLenI8 > 2) ) ){
         pInfo->rxCidState = reportRxCidMsgReady (chanId, side, pInfo->pRxCidBuff, pInfo->rxMsgLenI8);
         logCid (chanId, side, CIDRX, prevState, pInfo->rxCidState, -1);
      }
      if (prevState == pInfo->rxCidState) continue;
      if (sysConfig.type2CID && pInfo->type2CID)
         reportRxCid2Code (chanId, side, pInfo->rxCidState);

   } while (sampsPerCall <= sampsPerFrame);
}

#pragma CODE_SECTION (rxCid2SndProcess, "MEDIUM_PROG_SECT")
void rxCid2SndProcess(int chanId, GpakDeviceSide_t side, cidInfo_t *pInfo, ADT_PCM16 *pcmBuff, int sampsPerFrame) {
   int sampsPerCall;
   setCallRate (sampsPerCall,sampsPerFrame);
   do {
      CIDRX_ADT_genACK (pInfo->pRxCIDInst, pcmBuff);
      sampsPerFrame -= sampsPerCall;
      pcmBuff       += sampsPerCall;
   } while (sampsPerCall <= sampsPerFrame);
}

//int ackTest = 0; // for the debug purpose
#pragma CODE_SECTION (txCid2RcvProcess, "MEDIUM_PROG_SECT")
void txCid2RcvProcess(int chanId, GpakDeviceSide_t side, cidInfo_t *pInfo, ADT_PCM16 *pcmBuff, int sampsPerFrame) {
   int sampsPerCall;

   setCallRate (sampsPerCall,sampsPerFrame);
   do {
      CIDTX_ADT_detACK (pInfo->pTxCIDInst, pcmBuff);
      sampsPerFrame -= sampsPerCall;
      pcmBuff       += sampsPerCall;
   } while (sampsPerCall <= sampsPerFrame);
}
#endif
