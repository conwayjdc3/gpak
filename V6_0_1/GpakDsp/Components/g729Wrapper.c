#ifdef TI_G729

// TI's implementation of G729

#include <std.h>

/* XDAIS header files */
#include <ti/xdais/xdas.h>

/* XDM header files */
#include <ti/xdais/dm/xdm.h>
#include <ti/xdais/dm/ispeech1.h>
#include <ti/xdais/dm/isphenc1.h>
#include <ti/xdais/dm/isphdec1.h>

/* G729AB Interface header files */
#include <ti/xdais/dm/g729abenc_tii.h>
#include <ti/xdais/dm/g729abdec_tii.h>
#include <ti/xdais/dm/ig729ab.h>

// --------------------------offset          length  post-pad
#define ENC_MEM0_BYTE_OFFSET  0           // 48      0
#define ENC_MEM1_BYTE_OFFSET  48          // 32      0
#define ENC_MEM2_BYTE_OFFSET  80          // 272     0
#define ENC_MEM3_BYTE_OFFSET  352         // 48      0
#define ENC_MEM4_BYTE_OFFSET  400         // 308     4
#define ENC_MEM5_BYTE_OFFSET  712         // 608     0
#define ENC_MEM6_BYTE_OFFSET  1320        // 12      4
#define ENC_MEM7_BYTE_OFFSET  1336        // 172     -

// --------------------------offset         length  post-pad
#define DEC_MEM0_BYTE_OFFSET  0           // 52      4
#define DEC_MEM1_BYTE_OFFSET  56          // 40      0
#define DEC_MEM2_BYTE_OFFSET  96          // 44      4
#define DEC_MEM3_BYTE_OFFSET  144         // 32      0
#define DEC_MEM4_BYTE_OFFSET  176         // 12      4
#define DEC_MEM5_BYTE_OFFSET  192         // 120     0
#define DEC_MEM6_BYTE_OFFSET  312         // 308     4
#define DEC_MEM7_BYTE_OFFSET  624         // 600     0
#define DEC_MEM8_BYTE_OFFSET  1224        // 10      6
#define DEC_MEM9_BYTE_OFFSET  1240        // 6       -

int g729EncInit(void *chan, void *scratch) {
IALG_MemRec         memTab[9];
ISPHENC1_Params     params;
IALG_Fxns           *fxns = (IALG_Fxns *) &G729ABENC_TII_IG729ABENC;
IALG_Fxns 			*fxnsPtr;
Int                 n;
XDAS_Int8           *pMem = (XDAS_Int8 *)chan;
IALG_Handle          alg;

    params.size                 = sizeof (ISPHENC1_Params);
    params.tablesPtr            = NULL;
    memset(memTab, 0, sizeof(memTab));
    n = fxns->algNumAlloc != NULL ? fxns->algNumAlloc() : IALG_DEFMEMRECS;

    if (n == 9) {
       n = fxns->algAlloc((IALG_Params *)&params, &fxnsPtr, memTab);
		if (n <= 0)
			return -1;

        memTab[0].base = &pMem[ENC_MEM0_BYTE_OFFSET];
        memTab[1].base = &pMem[ENC_MEM1_BYTE_OFFSET];
        memTab[2].base = &pMem[ENC_MEM2_BYTE_OFFSET];
        memTab[3].base = &pMem[ENC_MEM3_BYTE_OFFSET];
        memTab[4].base = &pMem[ENC_MEM4_BYTE_OFFSET];
        memTab[5].base = &pMem[ENC_MEM5_BYTE_OFFSET];
        memTab[6].base = &pMem[ENC_MEM6_BYTE_OFFSET];
        memTab[7].base = &pMem[ENC_MEM7_BYTE_OFFSET];
        memTab[8].base = (XDAS_Int8 *)scratch;

        alg = (IALG_Handle)memTab[0].base;
        alg->fxns = fxns;
        if (fxns->algInit(alg, memTab, NULL, (IALG_Params *)&params) == IALG_EOK) {
            return 0;
        }
    }
    return -1;
}

int g729Encode (void *chan, void *pcm, void *data, short Vad, short *FrameType) {
  ISPHENC1_DynamicParams  dynparams;
  ISPHENC1_Handle         handle;
  ISPHENC1_InArgs         inargs;
  ISPHENC1_OutArgs        outargs;
  XDM1_SingleBufDesc      inBufs;
  XDM1_SingleBufDesc      outBufs;
  XDAS_Int32              retVal;

    handle =  (ISPHENC1_Handle)chan;

    // Use the control function to enable/disable DTX
    dynparams.size = sizeof(ISPHENC1_DynamicParams);
    switch (Vad) {
        case 0:
        dynparams.vadFlag = ISPEECH1_VADFLAG_OFF;
        break;

        case 1:
        dynparams.vadFlag = ISPEECH1_VADFLAG_ON;
        break;
    }
  
    retVal = handle->fxns->control (handle, XDM_SETPARAMS, &dynparams, NULL);
    if (retVal == ISPHENC1_EFAIL)
        return -1;

    // perform Encode function
    inBufs.buf      = (XDAS_Int8   *)pcm;
    inBufs.bufSize  = 160;
    outBufs.buf     = (XDAS_Int8   *)data;
    outBufs.bufSize  = 10;
    inargs.size     = sizeof (ISPHENC1_InArgs); // No input arguments for encoder
    outargs.size    = sizeof (ISPHENC1_OutArgs);

    retVal = handle->fxns->process (handle, &inBufs, &outBufs, &inargs, &outargs);
    if(retVal == XDM_EFAIL)
        return -1;

    switch (outargs.frameType) {
        case ISPHENC1_FTYPE_SPEECH:
        *FrameType = 1;
        break;
        case ISPHENC1_FTYPE_SIDFRAME:
        *FrameType = 2;
        break;
        case ISPHENC1_FTYPE_NODATA:
        *FrameType = 0;
        break;
    }

    return 0;
}

int g729DecInit(void *chan, void *scratch) {
IALG_MemRec         memTab[11];
ISPHDEC1_Params     params;
IALG_Fxns           *fxns = (IALG_Fxns *) &G729ABDEC_TII_IG729ABDEC;
IALG_Fxns 			*fxnsPtr;
Int                 n;
XDAS_Int8           *pMem = (XDAS_Int8 *)chan;
IALG_Handle          alg;
ISPHDEC1_DynamicParams  dynparams;
ISPHDEC1_Handle      dAlg;

    params.size                 = sizeof (ISPHDEC1_Params);
    params.tablesPtr            = NULL;
    memset(memTab, 0, sizeof(memTab));
    n = fxns->algNumAlloc != NULL ? fxns->algNumAlloc() : IALG_DEFMEMRECS;

    if (n == 11) {
        n = fxns->algAlloc((IALG_Params *)&params, &fxnsPtr, memTab);
		if (n <= 0)
			return -1;

        memTab[0].base = &pMem[DEC_MEM0_BYTE_OFFSET];
        memTab[1].base = &pMem[DEC_MEM1_BYTE_OFFSET];
        memTab[2].base = &pMem[DEC_MEM2_BYTE_OFFSET];
        memTab[3].base = &pMem[DEC_MEM3_BYTE_OFFSET];
        memTab[4].base = &pMem[DEC_MEM4_BYTE_OFFSET];
        memTab[5].base = &pMem[DEC_MEM5_BYTE_OFFSET];
        memTab[6].base = &pMem[DEC_MEM6_BYTE_OFFSET];
        memTab[7].base = &pMem[DEC_MEM7_BYTE_OFFSET];
        memTab[8].base = &pMem[DEC_MEM8_BYTE_OFFSET];
        memTab[9].base = &pMem[DEC_MEM9_BYTE_OFFSET];
        memTab[10].base = (XDAS_Int8 *)scratch;

        alg = (IALG_Handle)memTab[0].base;
        alg->fxns = fxns;
        if (fxns->algInit(alg, memTab, NULL, (IALG_Params *)&params) == IALG_EOK) {
            // enable post filter with control function
            dynparams.size = sizeof(ISPHDEC1_DynamicParams);
            dynparams.postFilter = ISPEECH1_POSTFILTER_ON;
            dAlg =  (ISPHDEC1_Handle)alg;
            if (dAlg->fxns->control (dAlg, XDM_SETPARAMS, &dynparams, NULL) == ISPHDEC1_EFAIL)
                return -1;
            return 0;
        }
    }
    return -1;
}

int g729Decode (void *chan, void *pyld, void *pcm, short FrameErase, short PktType) {
  ISPHDEC1_InArgs         inargs;
  ISPHDEC1_OutArgs        outargs;
  ISPHDEC1_Handle         handle;
  XDM1_SingleBufDesc      inBufs;
  XDM1_SingleBufDesc      outBufs;
  XDAS_Int32              retVal;

    handle =  (ISPHDEC1_Handle)chan;

    inargs.size     = sizeof (ISPHDEC1_InArgs);
    outargs.size    = sizeof (ISPHDEC1_OutArgs);

    inBufs.buf      = (XDAS_Int8  *)pyld;
    inBufs.bufSize     = 0;
    outBufs.buf     = (XDAS_Int8  *)pcm;
    outBufs.bufSize    = 160;

    if (FrameErase) {
        inargs.frameType    = ISPHDEC1_FTYPE_SPEECHLOST;
    } else {
        switch (PktType) {
            case 0:
            inargs.frameType = ISPHDEC1_FTYPE_NODATA;
            break;

            case 1:
            inargs.frameType = ISPHDEC1_FTYPE_SPEECHGOOD;
            inBufs.bufSize      = 10;
            break;

            case 2:
            inargs.frameType = ISPHDEC1_FTYPE_SIDUPDATE;
            inBufs.bufSize      = 2;
            break;
        };
    }

    retVal = handle->fxns->process (handle, &inBufs, &outBufs, &inargs, &outargs);
    if(retVal == XDM_EFAIL)
        return -1;

    return 0;
}
#else  // =================================================================================

#include "GpakDefs.h"
// ADT's Implementation of G729

int g729EncInit(void *chan, void *scratch) {
    G729AB_Encode_Init ((G729ENC_ADT_Instance_t *)chan, (G729ENC_ADT_Scratch_t *) scratch);
    return 0;
}


int g729Encode (void *chan, void *pcm, void *data, short Vad, short *FrameType) {
    G729AB_Encode ((G729ENC_ADT_Instance_t *)chan, (short *)pcm, (char *)data, Vad, FrameType);
    return 0;
}

int g729DecInit(void *chan, void *scratch) {
    G729AB_Decode_Init ((G729DEC_ADT_Instance_t *)chan, (G729DEC_ADT_Scratch_t *) scratch);
    return 0;    
}

int g729Decode (void *chan, void *pyld, void *pcm, short FrameErase, short VadFlag) {
    G729AB_Decode ((G729DEC_ADT_Instance_t *)chan, (char *)pyld, (short *)pcm, FrameErase, VadFlag); 
    return 0;
}

#endif

