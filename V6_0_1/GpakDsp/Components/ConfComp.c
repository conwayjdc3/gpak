/*
 * Copyright (c) 2004-2007, Adaptive Digital Technologies, Inc.
 *
 * File Name: ConfComp.c
 *
 * Description:
 *   This file contains functions to support Conference Composite type channels.
 *
 * Version: 1.0
 *
 * Revision History:
 *   4/21/04 - Initial release.
 *   2/2007  - Merged C54 and C64
 */

// Application related header files.
#include "GpakDefs.h"
#include "GpakErrs.h"
#include "GpakExts.h"
#include "GpakHpi.h"
#include "sysconfig.h"


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ ProcCfgConfCompMsg - Process a Configure Conference Composite Channel msg.
//
// FUNCTION
//   Performs Conference Composite channel processing for a Configure Channel message.
//
// Inputs
//   pCmd  - Command message buffer
//
//  Word:Bits
//     2:ff00    -  Conference ID
//     2:0080    -  VAD
//     2:0040    -  DTX 
//     3:0700    -  Out Port ID
//     3:f8ff    -  Out Slot ID
//     4:ff00    -  Out Codec
//     4:00fc    -  Obsolete
//     5:00FF    -  McASP Output Pin (Obsolete)
//
// Updates
//   pChan - Channel data structure pointer
//
// RETURNS
//  Return status for reply message.
//}
#pragma CODE_SECTION (ProcCfgConfCompMsg, "SLOW_PROG_SECT")
GPAK_ChannelConfigStat_t ProcCfgConfCompMsg (ADT_UInt16 *pCmd, chanInfo_t *pChan) {

   ConferenceInfo_t *cnfrInfo;
   ADT_UInt16 ConferenceId;   // Conference Id

   pcm2pkt_t *PcmPkt;   // Channel's PCM to Packet control info
   pkt2pcm_t *PktPcm;   // Channel's Packet to PCM control info

   GPAK_ChannelConfigStat_t toneStatus;
   int mfTones, cedTones, cngTones, arbTones;
   GpakToneTypes ToneTypes;   // Tone Detection types

   pCmd = &pCmd[2];
   ConferenceId = Byte1 (*pCmd);
   if (sysConfig.maxNumConferences <= ConferenceId)
      return Cc_InvalidConference;

   cnfrInfo = GetGpakCnfr (pChan->CoreID, ConferenceId);
   if (cnfrInfo->FrameSize == 0)
      return Cc_ConfNotConfigured;

   PcmPkt = &(pChan->pcmToPkt);
   PktPcm = &(pChan->pktToPcm);

   channelAllocsNull (pChan, PcmPkt, PktPcm);

   pChan->PairedChannelId = ConferenceId;
   PcmPkt->SamplesPerFrame = cnfrInfo->FrameSize;
   PktPcm->SamplesPerFrame = cnfrInfo->FrameSize;

   if ((*pCmd & 0x0080) != 0)   {  // VAD
      PcmPkt->VAD = SetVADMode (PcmPkt->Coding);
      if (PcmPkt->VAD == VadDisabled)
         return Cc_VadNotConfigured;
      if (sysConfig.VadReport == Enabled)
        PcmPkt->VAD |= VadNotifyEnabled;
   } else
      PcmPkt->VAD = VadDisabled;

   //
   // DTX
   //
   if ((*pCmd & 0x40) != 0) {
        if ((PcmPkt->VAD & VadMask) != VadCngEnabled)
            return Cc_VadNotConfigured;
        else {
            PcmPkt->DtxEnable = Enabled;
            PcmPkt->PrevVadState = VAD_INIT_RELEASE;
        }
   } else {
        PcmPkt->DtxEnable = Disabled;
   }

   pCmd++;
   PktPcm->OutSerialPortId = (GpakSerialPort_t) UnpackPort (*pCmd);
   PktPcm->OutSlotId = UnpackSlot (*pCmd);
   
   pCmd++;
   PcmPkt->Coding    = (GpakCodecs) Byte1 (*pCmd);

   if ((PcmPkt->Coding != NullCodec) &&
      !ValidCoding (PcmPkt->Coding, sampsToHalfMS (PcmPkt->SamplesPerFrame)))
      return Cc_InvalidPktOutCodingA;

   // Verify allocation of tone detector instances.

   // NOTE:  Tone detector types are specified by the conference and
   //        are removed from the channel control structure.
   PcmPkt->toneTypes = (GpakToneTypes) 0;
   ToneTypes = cnfrInfo->InputToneTypes;

   mfTones = cedTones = cngTones = arbTones = 0;
   toneStatus = ValidToneTypes (ToneTypes, &mfTones, &cedTones, &cngTones, &arbTones);
   if (toneStatus != Cc_Success) return toneStatus;

   if (((ToneTypes & ToneDetect) != Null_tone) && (ToneDetectorsAvail == 0))
      return Cc_InsuffTDResourcesAvail;

   // Store the input(s) tone report enable in the AGC enable.
   PktPcm->Coding = NullCodec;
   PcmPkt->InSerialPortId = SerialPortNull;
   PktPcm->InSerialPortId = SerialPortNull;
   PcmPkt->OutSerialPortId = SerialPortNull;

   // Return with an indication of success.
   return Cc_Success;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ ProcConfCompStatMsg - Process a Conference Composite Channel Status message.
//
// FUNCTION
//   Prepares the Channel Status reply message for a Conference Composite channel.
//
// Inputs
//   pChan - Channel data structure pointer
//
// Outputs
//   pReply  - Reply buffer
//
// RETURNS
//   Length (octets) of reply message.
//}
#pragma CODE_SECTION (ProcConfCompStatMsg, "SLOW_PROG_SECT")
ADT_UInt16 ProcConfCompStatMsg (ADT_UInt16 *pReply, chanInfo_t *pChan) {


   pReply[7] = PackBytes (pChan->PairedChannelId, 
            ((pChan->pcmToPkt.VAD & 0x0080)       << 7) |
            ((pChan->pcmToPkt.DtxEnable & 0x0040) << 6) );
   pReply[8] = PackPortandSlot (pChan->pktToPcm.OutSerialPortId,  pChan->pktToPcm.OutSlotId);
   pReply[9] = PackBytes (pChan->pcmToPkt.Coding, 0);
   pReply[10]   = (ADT_UInt16)  pChan->toHostPktCount;

   return 11;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//{ SetupConfCompChan - Setup a Conference Composite channel.
//
// FUNCTION
//   Performs channel setup initialization for a Conference Composite channel.
//
// Updates
//   pChan - Channel data structure pointer
//
// RETURNS
//   nothing
//}
#pragma CODE_SECTION (SetupConfCompChan, "SLOW_PROG_SECT")
void SetupConfCompChan (chanInfo_t *pChan) {
   pcm2pkt_t *PcmPkt;
   pkt2pcm_t *PktPcm;

   // Get pointers to the channel's PCM to Packet and Packet to PCM data.
   PcmPkt = &(pChan->pcmToPkt);
   PktPcm = &(pChan->pktToPcm);

   // Allocate Pcm, Pkt and BulkDelay Buffers
   AllocBuffs (pChan);

   // Initialize the circular PCM output buffer.
   memset (PktPcm->outbuffer.pBufrBase, 0, (PktPcm->outbuffer.BufrSize)*sizeof(ADT_UInt16));

   // Initialize VAD and Comfort noise generation
   if (PcmPkt->VAD & VadCng) {
      InitVadCng (PcmPkt->vadPtr, &PcmPkt->VadParms, PcmPkt->SamplesPerFrame, PktPcm->SamplesPerFrame, pChan->ProcessRate);
   }

   // Initialize the encoder.
   initEncoder (PcmPkt, 0, pChan->ProcessRate);

   // Allocate and initialize sampling rate converter instances
   AllocEncoderSampleRateConv (pChan);

   return;
}
