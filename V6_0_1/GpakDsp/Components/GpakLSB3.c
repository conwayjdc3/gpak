packLSB3(unsigned char *pU8, unsigned short *pIn, unsigned short num)
{
int loopCount, inCount, i;

    loopCount = num/8;
    for (i=0; i<loopCount; i++)
    {
        // three output words per 8 input values
        *pU8 = *pIn++ & 0x07;                 
        *pU8 |= ((*pIn++ & 0x07)<<3);         
        *pU8 |= (*pIn <<6);         
        pU8++;
                                                
        *pU8 = (*pIn++ & 0x07)>>2;                 
        *pU8 |= ((*pIn++ & 0x07)<<1);         
        *pU8 |= ((*pIn++ & 0x07)<<4);         
        *pU8 |= (*pIn <<7);         
        pU8++;

        *pU8 = (*pIn++ & 0x07)>>1;                 
        *pU8 |= ((*pIn++ & 0x07)<<2);         
        *pU8 |= (*pIn++ <<5);         
        pU8++;
    }

    // remainder loop
    if (inCount = num % 8)
    {
        for (i=0; i<1; i++)
        {
            *pU8 = *pIn++ & 0x07;                 
            if (--inCount == 0)
                break;
            *pU8 |= ((*pIn++ & 0x07)<<3);         
            if (--inCount == 0)
                break;
            *pU8 |= (*pIn <<6);         
            pU8++;
                                                
            *pU8 = (*pIn++ & 0x07)>>2;                 
            if (--inCount == 0)
                break;
            *pU8 |= ((*pIn++ & 0x07)<<1);         
            if (--inCount == 0)
                break;
            *pU8 |= ((*pIn++ & 0x07)<<4);         
            if (--inCount == 0)
                break;
            *pU8 |= (*pIn <<7);         
            pU8++;

            *pU8 = (*pIn++ & 0x07)>>1;                 
            if (--inCount == 0)
                break;
            *pU8 |= (*pIn++ <<2);         
        }
    }
}
unpackLSB3(unsigned char *pU8, unsigned short *pOut, unsigned short num)
{
int loopCount, outCount, i;

    loopCount = num/8;
    for (i=0; i<loopCount; i++)
    {
        // 8 output values per 3 input bytes
        *pOut++ = *pU8 & 0x07;                 

        *pOut++ = (*pU8 >> 3) & 0x07;          

        *pOut = *pU8++ >> 6;                   
        *pOut |= ((*pU8 & 0x01) << 2);
        pOut++;

        *pOut++ = (*pU8 >> 1) & 0x07;          

        *pOut++ = (*pU8 >> 4) & 0x07;          

        *pOut = *pU8++ >> 7;                   
        *pOut |= ((*pU8 & 0x03) << 1);
        pOut++;

        *pOut++ = (*pU8 >> 2) & 0x07;          

        *pOut++ = *pU8++ >> 5;                 
    }

    // remainder loop
    if (outCount = num % 8)
    {
        for (i=0; i<1; i++)
        {
            *pOut++ = *pU8 & 0x07;                 
            if (--outCount == 0)
                break;

            *pOut++ = (*pU8 >> 3) & 0x07;          
            if (--outCount == 0)
                break;

            *pOut = *pU8++ >> 6;                   
            *pOut |= ((*pU8 & 0x01) << 2);
            if (--outCount == 0)
                break;
            pOut++;

            *pOut++ = (*pU8 >> 1) & 0x07;          
            if (--outCount == 0)
                break;

            *pOut++ = (*pU8 >> 4) & 0x07;          
            if (--outCount == 0)
                break;

            *pOut = *pU8++ >> 7;                   
            *pOut |= ((*pU8 & 0x03) << 1);
            if (--outCount == 0)
                break;
            pOut++;

            *pOut++ = (*pU8 >> 2) & 0x07;          
        }
    }
}
