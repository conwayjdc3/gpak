packLSB2(unsigned char *pU8, unsigned short *pIn, unsigned short num)
{
int i;

    // one output byte per four input values
    for (i=0; i<num/4; i++)
    {
        *pU8 = *pIn++ & 0x03;
        *pU8 |= ((*pIn++ & 0x03)<<2);
        *pU8 |= ((*pIn++ & 0x03)<<4);
        *pU8 |= ((*pIn++ & 0x03)<<6);
        pU8++;
    }
    return;
}
unpackLSB2(unsigned char *pU8, unsigned short *pOut, unsigned short num)
{
int loopCount, i;

// 4 output values per input byte
    loopCount = num/2;

    for (i=0; i<loopCount; i++)
    {
        *pOut++ = *pU8 & 0x03;
        *pOut++ = (*pU8 >> 2) & 0x03;
        *pOut++ = (*pU8 >> 4) & 0x03;
        *pOut++ = (*pU8++ >> 6) & 0x03;
    }                         
    return;
}
