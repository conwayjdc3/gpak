void packMSB10 (unsigned char *pyld, unsigned short *param, unsigned short paramCnt) {
    int loopCount;

    // five payload bytes per four parameters
    loopCount = paramCnt/4;
    while (loopCount--)  {
        *pyld = *param >> 2;               //  8 bits param 0
        pyld++;

        *pyld = *param++ << 6;             //  2 bits param 0
        *pyld |= ((*param >> 4) & 0x3F);   //  6 bits param 1
        pyld++;
                                                
        *pyld = *param++ << 4;             //  4 bits param 1
        *pyld |= ((*param >> 6) & 0x0F);   //  4 bits param 2
        pyld++;

        *pyld = *param++ << 2;             //  6 bits param 2
        *pyld |= ((*param >> 8) & 0x03);   //  2 bits param 3
        pyld++;

        *pyld = *param++ & 0xff;           //  8 bits param 3
        pyld++;
    }                                           

    // remainder 
    paramCnt = paramCnt % 4;
    if (paramCnt == 0) return;

    *pyld = *param >> 2;               //  8 bits param 0
    pyld++;
    if (--paramCnt == 0) return;

    *pyld = *param++ << 6;             //  2 bits param 0
    *pyld |= ((*param >> 4) & 0x3F);   //  6 bits param 1
    pyld++;
    if (--paramCnt == 0) return;
                                                
    *pyld = *param++ << 4;             //  4 bits param 1
    *pyld |= ((*param >> 6) & 0x0F);   //  4 bits param 2
    pyld++;
    if (--paramCnt == 0) return;

    *pyld = *param++ << 2;             //  6 bits param 2
    *pyld |= ((*param >> 8) & 0x03);   //  2 bits param 3
    pyld++;
}

void unpackMSB10 (unsigned char *pyld, unsigned short *param, unsigned short paramCnt) {

   int loopCount;

    // four parameters per five payload bytes
    loopCount = paramCnt/4;
    while (loopCount--)  {
        *param =  ((*pyld++ << 2) & 0x3FC);  //  8 bits payload 0
        *param |= ((*pyld   >> 6) & 0x003);  //  2 bits payload 1
        param++;

        *param =  ((*pyld++ << 4) & 0x3F0);  //  6 bits payload 1
        *param |= ((*pyld   >> 4) & 0x00F);  //  4 bits payload 2
        param++;

        *param =  ((*pyld++ << 6) & 0x3C0);  //  4 bits payload 2
        *param |= ((*pyld   >> 2) & 0x03F);  //  6 bits payload 3
        param++;

        *param = ((*pyld++ << 8) & 0x300);   //  2 bits payload 3
        *param |= (*pyld++       & 0x0FF);   //  8 bits payload 4
        param++;
    }                                           

    // remainder 
    paramCnt = paramCnt % 4;
    if (paramCnt == 0) return;

    *param =  ((*pyld++ << 2) & 0x3FC);  //  8 bits payload 0
    *param |= ((*pyld   >> 6) & 0x003);  //  2 bits payload 1
    param++;
    if (--paramCnt == 0) return;

    *param =  ((*pyld++ << 4) & 0x3F0);  //  6 bits payload 1
    *param |= ((*pyld   >> 4) & 0x00F);  //  4 bits payload 2
    param++;
    if (--paramCnt == 0) return;

    *param =  ((*pyld++ << 6) & 0x3C0);  //  4 bits payload 2
    *param |= ((*pyld   >> 2) & 0x03F);  //  6 bits payload 3
     param++;

}
