/*
 * Copyright (c) 2004-2007, Adaptive Digital Technologies, Inc.
 *
 * File Name: ConfPkt.c
 *
 * Description:
 *   This file contains functions to support Conference Packet type channels.
 *
 * Version: 1.0
 *
 * Revision History:
 *   4/21/04 - Initial release.
 *   2/2007  - Merged C54 and C64
 *
 */
   
// Application related header files.
#include "GpakDefs.h"
#include "GpakErrs.h"
#include "GpakExts.h"
#include "sysconfig.h"


//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ProcCfgConfPktMsg - Process a Configure Conference Packet Channel message.
//
// FUNCTION
//   This function performs Conference Packet channel type dependent processing
//   for   a Configure Channel message.
//
//
//       Msg[2] = ConferenceId,
//                ((VadEnable  & 1) << 7) |
//                ((InAgcEnable  & 1) << 5) |
//                ((EcanEnable & 1) << 4) );
//       Msg[3] = PackBytes (PktInCoding, PktOutCoding);
//       Msg[4] = ToneTypes << 5;
//       Msg[5] = ((OutAgcEnable  & 1) << 0) |
//                ((EcanEnable & 1) << 4) );
//       Msg[6] = ToneGenGain
//       Msg[7] = OutputGain
//       Msg[8] = InputGain
//       Msg[9] = Packet frame size
//
// RETURNS
//  Return status for reply message.
//}
#pragma CODE_SECTION (ProcCfgConfPktMsg, "SLOW_PROG_SECT")
GPAK_ChannelConfigStat_t ProcCfgConfPktMsg (ADT_UInt16 *pCmd, chanInfo_t *chan) {

   int toneDetectCnt;
   ConferenceInfo_t *cnfrInfo;
   ADT_UInt16 CnfrId;   // Conference Id

   ADT_UInt32 SampsPerPktFrame;
   ADT_UInt32 SampsPerCnfrFrame;
   int mfTones, cedTones, cngTones, arbTones;
   GPAK_ChannelConfigStat_t toneStatus;
   
   pcm2pkt_t *PcmPkt;   // Channel's PCM to Packet control info
   pkt2pcm_t *PktPcm;   // Channel's Packet to PCM control info


   CnfrId = Byte1 (pCmd[2]);
   if (sysConfig.maxNumConferences <= CnfrId)
      return Cc_InvalidConference;
 
   cnfrInfo = GetGpakCnfr (chan->CoreID, CnfrId);
   SampsPerCnfrFrame = cnfrInfo->FrameSize;
   if (SampsPerCnfrFrame == 0)
      return Cc_ConfNotConfigured;
      
   if (sysConfig.maxNumConfChans <= cnfrInfo->NumMembers)
      return Cc_ConferenceFull;
      
   chan->PairedChannelId = CnfrId;

   // Check for multi-rate conferencing (packet rate and conference rate do not match)
   if (10 <= ApiBlock.CmdMsgLength) {
      if (ApiBlock.ApiVersionId < 0x0603u)
         SampsPerPktFrame = Byte0 (Byte0 (pCmd[9]));
      else
         SampsPerPktFrame = halfMSToSamps (Byte0 (pCmd[9]));

      // Multi-rate conferencing is not allowed when RTP is not configured.
      // This is due to not having an extra buffer for use by conferencing task
      if ((SampsPerPktFrame != SampsPerCnfrFrame) && (sysConfig.maxJitterms == 0))
         return Cc_InvalidPktInCodingA;
   } else
      SampsPerPktFrame = SampsPerCnfrFrame;

   PcmPkt = &(chan->pcmToPkt);
   PktPcm = &(chan->pktToPcm);

   channelAllocsNull (chan, PcmPkt, PktPcm);

   PcmPkt->SamplesPerFrame = SampsPerPktFrame;
   PktPcm->SamplesPerFrame = SampsPerPktFrame;

   PcmPkt->InSerialPortId = PcmPkt->OutSerialPortId = SerialPortNull;
   PktPcm->InSerialPortId = PktPcm->OutSerialPortId = SerialPortNull;

   PktPcm->Coding = (GpakCodecs) Byte1 (pCmd[3]);
   if (!ValidCoding (PktPcm->Coding, sampsToHalfMS (SampsPerPktFrame)))
      return Cc_InvalidPktInCodingA;

   PcmPkt->Coding = (GpakCodecs) Byte0 (pCmd[3]);
   if (!ValidCoding (PcmPkt->Coding, sampsToHalfMS (SampsPerPktFrame)))
      return Cc_InvalidPktOutCodingA;

   // Configure VAD
   if ((pCmd[2] & 0x0080) != 0){
      PcmPkt->VAD = SetVADMode (PcmPkt->Coding);
      if (PcmPkt->VAD == VadDisabled)
         return Cc_VadNotConfigured;
      if (sysConfig.VadReport == Enabled)
        PcmPkt->VAD |= VadNotifyEnabled;
   } else
      PcmPkt->VAD = VadDisabled;

   PktPcm->CNG = SetVADMode (PktPcm->Coding);

   //
   // DTX
   //
   if ((pCmd[2] & 0x08) != 0) {
        if ((PcmPkt->VAD & VadMask) != VadCngEnabled)
            return Cc_VadNotConfigured;
        else {
            PcmPkt->DtxEnable = Enabled;
            PcmPkt->PrevVadState = VAD_INIT_RELEASE;
        }
   } else {
        PcmPkt->DtxEnable = Disabled;
   }

   // Configure AGC
   if (ApiBlock.ApiVersionId <= 0x0401u) {
        if ((pCmd[2] & 0x0020) == 0) {                // Input and Output AGC
            PcmPkt->AGC = Disabled;
            PktPcm->AGC = Disabled;
        } else if (numAGCChansAvail < 2)
            return Cc_InsuffAgcResourcesAvail;
        else {
            PcmPkt->AGC = Enabled;
            PktPcm->AGC = Enabled;
        }
    } else {
        // 4_2+ -----------------------------------------------
        if ((pCmd[2] & 0x0020) == 0)                  // Input AGC
            PcmPkt->AGC = Disabled;
        else if (numAGCChansAvail < 1)
            return Cc_InsuffAgcResourcesAvail;
        else
            PcmPkt->AGC = Enabled;

        if ((pCmd[5] & 0x00001) == 0)                 // Output AGC
            PktPcm->AGC = Disabled;
        else if (numAGCChansAvail < 1)
            return Cc_InsuffAgcResourcesAvail;
        else
            PktPcm->AGC = Enabled;
    //-----------------------------------------------------

   }
   // Determine Echo Canceller needs and availability.
   if ((pCmd[2] & 0x0010) != 0)   {
      if (sysConfig.numPktEcans <= NumPktEcansUsed)
         return Cc_PktEchoCancelNotCfg;
         
      PcmPkt->EchoCancel = Enabled;
   } else
      PcmPkt->EchoCancel = Disabled;

   // Configure tone generation.
   PktPcm->toneTypes = (pCmd[4] & Tone_Generate);
   if ((PktPcm->toneTypes & Tone_Generate) && (numToneGenChansAvail == 0))
      return Cc_InsuffTGResourcesAvail;

   // Configure tone detection.  Conference sets PktPcm tones.  Channel sets PcmPkt tones.
   PcmPkt->toneTypes = (pCmd[4] & ~Tone_Generate);
   PktPcm->toneTypes |= cnfrInfo->InputToneTypes;
   
   mfTones = cedTones = cngTones = arbTones = 0;
   toneStatus = ValidToneTypes (PcmPkt->toneTypes, &mfTones, &cedTones, &cngTones, &arbTones);
   if (toneStatus != Cc_Success) return toneStatus;

   toneStatus = ValidToneTypes (PktPcm->toneTypes, &mfTones, &cedTones, &cngTones, &arbTones);
   if (toneStatus != Cc_Success) return toneStatus;

   toneDetectCnt = 0;
   if (PcmPkt->toneTypes & ToneDetect) toneDetectCnt++;
   if (PktPcm->toneTypes & ToneDetect) toneDetectCnt++;
   if (ToneDetectorsAvail < toneDetectCnt)
        return Cc_InsuffTDResourcesAvail;

   // Configure gains
   if (ApiBlock.ApiVersionId <= 0x0401u) return Cc_Success;

   // 4_2+ -----------------------------------------------
   PcmPkt->Gain.ToneGenGainG1  = pCmd[6];
   PcmPkt->Gain.OutputGainG2   = pCmd[7];
   PcmPkt->Gain.InputGainG3    = pCmd[8];
   ClampScaleGain (&PcmPkt->Gain);

   PktPcm->Gain.ToneGenGainG1  = PcmPkt->Gain.ToneGenGainG1;
   PktPcm->Gain.OutputGainG2   = PcmPkt->Gain.OutputGainG2; 
   PktPcm->Gain.InputGainG3    = PcmPkt->Gain.InputGainG3;         

    return Cc_Success;
}


//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ProcConfPktStatMsg - Process a Conference Packet Channel Status message.
//
// FUNCTION
//   This function prepares the Channel Status reply message for a Conference
//   Packet type channel.
//
// RETURNS
//   Length I16 of reply message.
//}
#pragma CODE_SECTION (ProcConfPktStatMsg, "SLOW_PROG_SECT")
ADT_UInt16 ProcConfPktStatMsg (ADT_UInt16 *pReply, chanInfo_t *chan) {
   int VAD;
   ADT_UInt16 samps;
   pcm2pkt_t *PcmPkt;   // Channel's PCM to Packet control info
   pkt2pcm_t *PktPcm;   // Channel's Packet to PCM control info

   PcmPkt = &(chan->pcmToPkt);
   PktPcm = &(chan->pktToPcm);

   if (PcmPkt->VAD & VadEnabled)
      VAD = 1;
   else 
      VAD = 0;

   pReply[7] = (ADT_UInt16)
            ((chan->PairedChannelId << 8) |
             ((VAD                  << 7) & 0x0080) |
             ((PcmPkt->AGC          << 5) & 0x0020) |
             ((PcmPkt->EchoCancel   << 4) & 0x0010) |
             ((PcmPkt->DtxEnable    << 3) & 0x0008) );

   pReply[8] = (ADT_UInt16) PackBytes (PktPcm->Coding, PcmPkt->Coding);

   pReply[9] = (ADT_UInt16) ((PktPcm->toneTypes | 
      GetGpakCnfr (chan->CoreID, chan->PairedChannelId)->InputToneTypes) & 0xFFF);

   if (ApiBlock.ApiVersionId <= 0x0401u) return 10;

   // 4_2+ -----------------------------------------------
   pReply[10]   = (ADT_UInt16) (PktPcm->AGC & 0x1);
   if (PktPcm->Gain.ToneGenGainG1 != (ADT_Int16)ADT_GAIN_MUTE)
        pReply[11] = (ADT_Int16) PktPcm->Gain.ToneGenGainG1/10;
   else
        pReply[11] = (ADT_Int16) PktPcm->Gain.ToneGenGainG1;

   if (PktPcm->Gain.OutputGainG2 != (ADT_Int16)ADT_GAIN_MUTE)
        pReply[12] = (ADT_Int16) PktPcm->Gain.OutputGainG2/10; 
   else 
        pReply[12] = (ADT_Int16) PktPcm->Gain.OutputGainG2; 

   if (PcmPkt->Gain.InputGainG3 != (ADT_Int16)ADT_GAIN_MUTE)
        pReply[13] = (ADT_Int16) PcmPkt->Gain.InputGainG3/10;  
   else
        pReply[13] = (ADT_Int16) PcmPkt->Gain.InputGainG3;  
   pReply[14]   = (ADT_UInt16)  chan->fromHostPktCount;
   pReply[15]   = (ADT_UInt16)  chan->toHostPktCount;

   if (chan->channelType == conferenceMultiRate)
     samps = (ADT_UInt16) PcmPkt->SamplesPerFrame;
   else
     samps = GetGpakCnfr (chan->CoreID, chan->PairedChannelId)->FrameSize;

   if (ApiBlock.ApiVersionId < 0x0603u) 
     pReply[16] = samps;
   else
     pReply[16] = sampsToHalfMS (samps);
   
   return 17;
}


//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// SetupConfPktChan - Setup a Conference Packet channel.
//
// FUNCTION
//   This function performs channel setup initialization for a Conference Packet
//   type channel.
//
// RETURNS
//   nothing
//}
#pragma CODE_SECTION (SetupConfPktChan, "SLOW_PROG_SECT")
void SetupConfPktChan (chanInfo_t *chan) {
   conf_Member_t    *pConfMember;   // pointer to Conference Member data

   pcm2pkt_t *PcmPkt;      // pointer to channel's PCM to Packet data
   pkt2pcm_t *PktPcm;      // pointer to channel's Packet to PCM data

   // Get pointers to the channel's PCM to Packet and Packet to PCM data.
   PcmPkt = &(chan->pcmToPkt);
   PktPcm = &(chan->pktToPcm);

   if (PcmPkt->EchoCancel)
      chan->ProcessRate = 8000;

   // Allocate Pcm, Pkt and BulkDelay Buffers
   AllocBuffs (chan);
   PcmPkt->activeInBuffer = &PcmPkt->inbuffer;

   // Indicate that Silence and Tone packet forwarding is disabled.
   PktPcm->ForwardTonePkts = Disabled;
   PktPcm->ForwardCNGPkts = Disabled;

   // Initialize tone detection if enabled for the conference or the channel.
   if ((PcmPkt->toneTypes & ToneDetectAll) != Null_tone) {
      InitToneDetection (PcmPkt->toneTypes, &PcmPkt->TDInstances, PcmPkt->toneRelayPtr, PcmPkt->SamplesPerFrame);
   }

   if ((PktPcm->toneTypes & ToneDetectAll) != Null_tone) {
      InitToneDetection (PktPcm->toneTypes, &PktPcm->TDInstances, 0, PktPcm->SamplesPerFrame);
   }
   if ((PcmPkt->toneTypes & Tone_Relay) && (SysAlgs.rtpHostDelivery || SysAlgs.rtpNetDelivery)) {
      PktPcm->ForwardTonePkts = Enabled;
      PktPcm->tonePktPut = PcmPkt->outbuffer->pBufrBase;
      PcmPkt->tonePktGet = PcmPkt->outbuffer->pBufrBase;
   }
   memset (PcmPkt->outbuffer->pBufrBase, 0, PcmPkt->outbuffer->BufrSize * sizeof (ADT_UInt16));

   // Initialize VAD and Comfort noise gen
   if ((PcmPkt->VAD & VadCng) || (PktPcm->CNG & VadCng)) {
      InitVadCng (PcmPkt->vadPtr, &PcmPkt->VadParms, PcmPkt->SamplesPerFrame, PktPcm->SamplesPerFrame, chan->ProcessRate);
   }

   // Allocate AGC instances
   AllocAGC (chan, ADevice, PcmPkt->AGC);
   AllocAGC (chan, BDevice, PktPcm->AGC);

   // Allocate tone generation instances
   AllocToneGen (&PktPcm->TGInfo, PktPcm->toneTypes);
   AllocToneGen (&PcmPkt->TGInfo, Disabled);

   // Initialize tone relay generation instance.
   if (PktPcm->TGInfo.toneRlyGenPtr) {
      InitToneRelayGen (&PktPcm->TGInfo);
   }

   // Initialize the Echo Canceller if enabled.
   PcmPkt->ecBulkDelay.SlipSamps = 0;
   PcmPkt->ecFarPtr.SlipSamps = 0;
   if (PcmPkt->EchoCancel == Enabled) {
      PcmPkt->ecFarPtr.pBufrBase = PktPcm->outbuffer.pBufrBase;
      PcmPkt->ecFarPtr.BufrSize  = PktPcm->outbuffer.BufrSize;
      PcmPkt->EcIndex = chan->CoreID;
      PcmPkt->pG168Chan = AllocEchoCanceller (G168_PKT, 
                   PcmPkt->SamplesPerFrame, &PcmPkt->EcIndex, chan->ProcessRate);
      memset (PcmPkt->ecFarPtr.pBufrBase, 0, PcmPkt->ecFarPtr.BufrSize*sizeof(ADT_PCM16));
   }

   // Initialize vocoders.
   initEncoder (PcmPkt, 0, chan->ProcessRate);
   initDecoder (PktPcm, 0, chan->ProcessRate);

   // Allocate and initialize sampling rate converter instances
   AllocEncoderSampleRateConv (chan);
   AllocDecoderSampleRateConv (chan);

   // Setup the channel's Conference Member structure.
   pConfMember       = ConfMemberData[chan->ChannelId];
   CLEAR_INST_CACHE (pConfMember, sizeof (conf_Member_t));
   FLUSH_wait ();

   pConfMember->pVAD = ConfVadData[chan->ChannelId];

   if (chan->channelType == conferenceMultiRate) {
      pConfMember->pInPCM  = (ADT_Int16 *)    PktPcm->inbuffer->pBufrBase;
      pConfMember->pOutPCM = (ADT_Int16 *)  &(PktPcm->inbuffer->pBufrBase[PktPcm->inbuffer->BufrSize/2]);
   } else {
      pConfMember->pInPCM  = (ADT_Int16 *)    PcmPkt->inbuffer.pBufrBase;
      pConfMember->pOutPCM = (ADT_Int16 *)  &(PcmPkt->inbuffer.pBufrBase[PcmPkt->inbuffer.BufrSize/2]);
   }

   pConfMember->malloc_ADT = NULL;
   pConfMember->pLink = NULL;
   if (chan->ProcessRate == 8000)
      pConfMember->MemberType = MEMBER_TYPE_8K_B;
   else
      pConfMember->MemberType = MEMBER_TYPE_16K_B;

   pConfMember->Priority = 0;
   FLUSH_INST_CACHE (pConfMember, sizeof (conf_Member_t));
   return;
}


//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// CnfrMultiRate - Host to PCM framing for a Packet To Packet type channel.
//
// FUNCTION
//   Reads incoming packet (RTPInbuffer) from the host and 
//   decodes to PCM (chan->PktToPCM.outbuffer) 
//
//
// Transfers
//
//   DECODE
//      Host Transfer
//          pkt -->  RTPInBuffer
//                  (PktBufferPool[0][chan])
//
//      ParsePacket
//          JitterBuffer --> Task.InWork [parameters]
//
//      VoiceDecode
//          Task.InWork [parameters] --> Task.OutWork [PCM]
//      
//      Task.OutWork[PCM] --> pktPcm.outbuffer [PCM]
//                               (PcmOutBufferPool[chan])
//
//   ENCODE
//      pcmPkt.inbuffer [PCM] --> Task.InWork [PCM]
//         (PcmInBufferPool[pairedChan])
//
//
//      ProcessVadToneEncode 
//          Tone:  Task.InWork [PCM] --> RTPOutBuffer
//          Voice: Task.InWork [PCM] --> Task.OutWork [parameters]
//                 Task.OutWork [parameters] --> RTPOutBuffer
//
//      Host Transfer
//          RTPOutBuffer->network

//
// Inputs
//        chan   -  Channel data structure pointer
//        pktBuff -  Linear work buffer (Task.InWork) for temporary packet data
//        pcmBuff -  Linear work buffer (Task.OutWork) for temporary PCM data
//     pECFarWork -  Linear work buffer (Task.EcFarWork) for far size PCM for echo cancellation
//      SampsPerFrame -  Samples per frame
//
// RETURNS
//   nothing
//}

#pragma CODE_SECTION (MultiRateCnfrFrame,"MEDIUM_PROG_SECT")
void MultiRateCnfrFrame (chanInfo_t *chan,  void *pktBuff, ADT_PCM16 *pcmBuff, 
                    ADT_PCM16 *pECFarWork, int SampsPerFrame, int pcmBuffI8) {
                      
   GpakPayloadClass class; 
   int sampsPerFrame;
   int paramsI8;

   pcm2pkt_t *PcmPkt = &(chan->pcmToPkt);
   pkt2pcm_t *PktPcm = &(chan->pktToPcm);

   //===================================================================
   // Decode packet from Jitter buffer to conference (PcmPkt.inbuffer)

   // Read packet from host interface into pktBuff using pcmBuff as scratch unpacking
   // Decode packets (pktBuff) by class into PCM (pcmBuff)
   paramsI8 = pcmBuffI8;
   class = parsePacket (chan, pktBuff, &paramsI8, pcmBuff, pcmBuffI8);
   sampsPerFrame = DecodePayload (chan, PktPcm, class, pktBuff, paramsI8, pcmBuff, pcmBuffI8);

   // copy the linear pcm buffer into the circular pcm buffer (PcmPkt.inbuffer)
   copyLinearToCirc (pcmBuff, &PcmPkt->inbuffer, sampsPerFrame);

   //===================================================================
   // Encode packet from conference (PktPcm.outbuffer) to RTPOutBuffer

   // copy the input pcm data from circular buffer (PktPcm.outbuffer) to linear work buffer (pcmBuff)
   copyCircToLinear (&PktPcm->outbuffer, pcmBuff, sampsPerFrame);

   // perform VAD, Tone Detection, and Encode. Copy result to host packet buffer
   ProcessVadToneEncode (chan, (ADT_PCM16 *) pcmBuff, pcmBuff, pktBuff, sampsPerFrame);
   return;
}

