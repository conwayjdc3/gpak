//==========================================================================
//          File:  64xRtpNetDelivery.c
//
//   Description:  Initialization and support routines for delivery of RTP packets to the network stack
//
//{==========================================================================


#include <std.h>
#include <string.h>
#include <stdio.h>
//#include <swi.h>
#include <adt_typedef.h>
#include "GpakDefs.h"
#include "GpakExts.h"
#include "GpakHpi.h"
//} - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

//   Frame work interfaces for packet delivery to network statck
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#ifdef inc_ipStackUser          // compile only if IP stack support exists
#include inc_ipStackUser

extern char HostName[];
extern void initEMAC();
extern coreLock ipMapLock;
coreLock *pipMapLock = &ipMapLock;
//#pragma DATA_SECTION (ipMapLock, "SHARED_LOCK_SECT:ipMapLock") 
//#pragma DATA_ALIGN   (ipMapLock, 64) 

static ADT_Bool msgSocketState = ADT_FALSE;

// Linear copy of event buffer for network transmission
struct evtBuff {
   ADT_UInt32 tag;
   union {
      EventFifoMsg_t    evtMsg;
      EventFifoCIDMsg_t evtCidMsg;
   } u;
} evtBuff;
#pragma DATA_SECTION (evtBuff, "IPStack")

//{
// Verify that connect message from host is correctly formatted
//}
#pragma CODE_SECTION (verifyConnectionTag, "SLOW_PROG_SECT")
int verifyConnectionTag (int msgI8) {
   ADT_UInt32 *msgPtr, *hostNamePtr;
   ADT_UInt32 mask, versionID;
   ADT_UInt16 *rplyPtr;
   int i;

   msgPtr = (ADT_UInt32 *) ApiBlock.CmdMsgPointer;

   hostNamePtr = (ADT_UInt32 *) HostName;
   mask = *msgPtr++;
   versionID = *msgPtr++ ^ mask;
   if (msgI8 != 32) return msgSocketState;

   for (i=0; i<1; i++, msgPtr++) {  // 22 bytes name + 2 byte zero padding
      if ((*msgPtr ^ mask) != *hostNamePtr++) return msgSocketState;
   }

   for (; i<6; i++, msgPtr++) {  // 22 bytes name + 2 byte zero padding
      if ((*msgPtr ^ mask) != *hostNamePtr++) return 0;
   }

   // Fill in reply buffer with MaxCmdMsgI8 and MaxChanCnt values
   rplyPtr  = ApiBlock.ReplyMsgPointer;
   *rplyPtr++ = 0xaa55;
   *rplyPtr++ = 0xaa55;
   *rplyPtr++ = ApiBlock.MaxCmdMsgLen;
   *rplyPtr++ = sysConfig.maxNumChannels;

   ApiBlock.ReplyMsgLength = 8;
   ApiBlock.ApiVersionId = versionID;
   msgSocketState = ADT_TRUE;
   return 0;
}


//{
// Close host messaging socket
//}
#pragma CODE_SECTION (closeMsgConnection, "SLOW_PROG_SECT")
void closeMsgConnection () {
   msgSocketState = ADT_FALSE;
   return;
}

//{
// Called by RTP task to verify message buffer available
//}
#pragma CODE_SECTION (msgBuffInUse, "MEDIUM_PROG_SECT")
int msgBuffInUse () {
   return ApiBlock.CmdMsgLength || ApiBlock.ReplyMsgLength;
}

//{
// Called by RTP task to verify host connection handshake
//}
#pragma CODE_SECTION (acceptMsg, "MEDIUM_PROG_SECT")
int acceptMsg (int msgI8) {
   int connection;

   connection = verifyConnectionTag (msgI8);
   if (!connection) return msgSocketState;

   ApiBlock.CmdMsgLength = (msgI8 + 1) / 2;
   activateMsgHandler ();
   return 1;

}


//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// getMsgBuffers
//
// FUNCTION
//   Called from RTP processing task to obtain addresses of messaging buffers.
//
// Outputs
//   msgBuff  - Address that will point to the API Message Buffer
//   rplyBuff - Address that will point to the API Reply Buffer
//
// Return
//   Size of ApiBlock Command Message Buffer in bytes
//}
#pragma CODE_SECTION (getMsgBuffers, "SLOW_PROG_SECT")
int getMsgBuffers (void **msgBuff, void **rplyBuff, void **_evtBuff) {
   *msgBuff  = ApiBlock.CmdMsgPointer;
   *rplyBuff = ApiBlock.ReplyMsgPointer;
   *_evtBuff = &evtBuff;
   return ApiBlock.MaxCmdMsgLen;
}

#pragma CODE_SECTION (IPMap_lock, "FAST_PROG_SECT")
int IPMap_lock () {
   //return  ADDRESS_lock (&ipMapLock.Inst);
   return  ADDRESS_lock (&pipMapLock->Inst);
}

#pragma CODE_SECTION (IPMap_unlock, "FAST_PROG_SECT")
void IPMap_unlock (int mask) {
   //ADDRESS_unlock (&ipMapLock.Inst, mask);
   ADDRESS_unlock (&pipMapLock->Inst, mask);
}

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// msgReplyI8
//
// FUNCTION
//   Called from RTP processing task to determine size of pending messge reply for delivery to the network.
//
// Returns -  Size of Reply message in bytes
//
//}
#pragma CODE_SECTION (msgReplyI8, "MEDIUM_PROG_SECT")
int msgReplyI8 () {
   int RplyI8;

   RplyI8 = ApiBlock.ReplyMsgLength;
   ApiBlock.ReplyMsgLength = 0;
   return RplyI8;

}

//{ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// evtReplyI8
//
// FUNCTION
//   Called from RTP processing task to determine size of event for delivery to the network.
//
// Returns -  Size of Reply message in bytes
//
//}
#pragma CODE_SECTION (evtReplyI8, "MEDIUM_PROG_SECT")
int evtReplyI8 () {
   int hdrI8, pyldI8;
   ADT_Word pyldI16;

   // Get event header
   if (getAvailable (&eventFIFOInfo) < EVENT_HDR_I16LEN) return 0;

   evtBuff.tag = 0xaaaaaaaa;
   hdrI8 = EVENT_HDR_I16LEN * 2;
   copyCircToLinear (&eventFIFOInfo, &evtBuff.u.evtMsg.header, EVENT_HDR_I16LEN);

   // Get event payload
   pyldI8 = evtBuff.u.evtMsg.header.eventLength; 
   pyldI16 = (pyldI8 + 1) / 2;
   if (getAvailable (&eventFIFOInfo) < pyldI16) {
      eventFIFOInfo.TakeIndex = eventFIFOInfo.PutIndex;
      return 0;
   }

   if ((0 < pyldI8) && (pyldI8 <= sizeof (evtBuff.u) - 4))
       copyCircToLinear (&eventFIFOInfo, &evtBuff.u.evtMsg.payload, Int16PadForHost(pyldI16));
   return hdrI8 + pyldI8 + sizeof (evtBuff.tag);

}


#pragma CODE_SECTION (ipStackInit, "SLOW_PROG_SECT")
void ipStackInit () {

   // Initialize ipToSession lock mechanism
   //ipMapLock.Inst = 0;
   pipMapLock->Inst = 0;
#ifndef SINGLE_CORE_STACK
   if (DSPCore != 0) {
      initEMAC ();
      return;
   }
#endif
   GpakStackInit ();   
}

#endif                          // compile only if IP stack support exists
