// Common prototype to access two variants (dm642) and (evm6424) of spectrum
// digital's PCI interface dll.
//
#ifndef SD_PCI_LIB_H
#define SD_PCI_LIB_H

#include "adt_typedef.h"
#ifdef _WIN32
    #if (_MSC_VER >= 900)
       #define   WIN32_LEAN_AND_MEAN  1
       #define   INC_OLE2
       #define   NOSERVICE
    #endif 

    #include <windows.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif


typedef  void *PCI64_HANDLE;


typedef ADT_Int32 (*PCIOpen) (int BoardNum, PCI64_HANDLE *Hndl);
typedef ADT_Int32 (*PCIClose) (PCI64_HANDLE Hndl);

typedef ADT_Int32 (*PCIXfer) (PCI64_HANDLE Hndl,  ADT_UInt32  Taddr,     
                              ADT_UInt32   Cnt32, ADT_UInt32 *pData);

typedef ADT_Int32 (*PCILoadCoff) (PCI64_HANDLE hPci, char *pCoffName);

typedef HANDLE (*PCICbHndl) (PCI64_HANDLE Hndl);
typedef ADT_Int32 (*PCICbEvent) (PCI64_HANDLE Hndl, void *callbackFunction);


// Spectrum digtital PCI dll loader
// dspType = 6424 opens 6424 PCI dll
//           642  opens DM642 PCI dll.
HMODULE PCILibOpen (int dspType);

// Spectrum digital interface functions
PCIOpen     PCI64_Open;
PCIClose    PCI64_Close;
PCIXfer     PCI64_MemFill32, PCI64_MemWrite32, PCI64_MemRead32, PCI64_RegWrite32, PCI64_RegRead32;
PCILoadCoff PCI64_LoadCoff;
PCICbHndl   PCI64_GetCallbackEvent;
PCICbEvent  PCI64_SetupCallbackEvent;


#ifdef __cplusplus
}       // Balance extern "C" above
#endif


/*F***************************************************************************
* NAME:  PCI64_Open()
*
* DESCRIPTION:  Open access to DM642 EVM at BoardNum
*
* PARMETERS:
*   BoardNum       - Board number 0-3
*   pHndl          - Pointer to return handle
*     
* NOTES: The return handle can be valid even if PCI64_Open fails.  In this 
*        case you should immediately call PCI64_Close.
*
* RETURN VALUE:
*    0 Success, non-0 for Fail
*   
*F***************************************************************************/

/*F***************************************************************************
* NAME:  PCI64_Close()
*
* DESCRIPTION:  Close access to DM642 EVM.  Should always close
*   
* PARMETERS:
*   Hndl           - Handle returned from PCI64_Open
*
* NOTES: Always call PCI64_Close() even if PCI64_Open fails.
*
* RETURN VALUE:
*    0 Success, non-0 for Fail
*
*F***************************************************************************/

/*F***************************************************************************
* NAME:  PCI64_MemWrite32()
*
* DESCRIPTION:  Write Count 32 bit values to Taddr.
*         
* PARMETERS:
*   Hndl           - Handle returned from PCI64_Open
*   Taddr          - C64x target address  
*   Count          - Number of 32 bit data values
*   pData          - Pointer to array of 32 bit data types  
*
* NOTES: Taddr must be aligned to 32 bit address boundary.  
*        Count must be less then 8M bytes or the basic 64xx PCI memory 
*          window.
*        Taddr+Count must not cross a 8M byte PCI page boundary.
*
*        Function maps PCI region 4 to the base of the address space
*        specified by Taddr and then performs the write operation.
*
* RETURN VALUE:
*    0 Success, non-0 for Fail
*
*F***************************************************************************/

/*F***************************************************************************
* NAME:  PCI64_MemFill32()
*
* DESCRIPTION:  Fill Count 32 bit values to Taddr.
*
* PARMETERS:
*   Hndl           - Handle returned from PCI64_Open
*   Taddr          - C64x target address  
*   Count          - Number of 32 bit data values
*   pData          - Pointer to array of 32 bit data types  
*         
* NOTES: Taddr must be aligned to 32 bit address boundary.  
*        Count must be less then 8M bytes or the basic 64xx PCI memory 
*          window.
*        Taddr+Count must not cross a 8M byte PCI page boundary.
*
*        Function maps PCI region 4 to the base of the address space
*        specified by Taddr and then performs the memfill operation.
*
* RETURN VALUE:
*    0 Success, non-0 for Fail
*
*F***************************************************************************/

/*F***************************************************************************
* NAME:  PCI64_MemRead32()
*
* DESCRIPTION:  Read Count 32 bit values from Taddr.
*   
* PARMETERS:
*   Hndl           - Handle returned from PCI64_Open
*   Taddr          - C64x target address  
*   Count          - Number of 32 bit data values
*   pData          - Pointer to array of 32 bit data types  
*      
* NOTES: Taddr must be aligned to 32 bit address boundary.  
*        Count must be less then 8M bytes or the basic 64xx PCI memory 
*          window.
*        Taddr+Count must not cross a 8M byte PCI page boundary.
*
*        Function maps PCI region 4 to the base of the address space
*        specified by Taddr and then performs the read operation.
*
* RETURN VALUE:
*    0 Success, non-0 for Fail
*
*F***************************************************************************/

/*F***************************************************************************
* NAME:  PCI64_RegWrite32()
*
* DESCRIPTION:  Write Count 32 bit values to Taddr.
*
* PARMETERS:
*   Hndl           - Handle returned from PCI64_Open
*   Taddr          - C64x target address  
*   Count          - Number of 32 bit data values
*   pData          - Pointer to array of 32 bit data types  
*         
* NOTES: Taddr must be aligned to 32 bit address boundary.  
*        Count must be less then 1024.
*        
*        Function uses Base 1 address of 8M byte non-prefecthable region.
*
* RETURN VALUE:
*    0 Success, non-0 for Fail
*
*F***************************************************************************/

/*F***************************************************************************
* NAME:  PCI64_RegRead32()
*
* DESCRIPTION:  Read Count 32 bit values from Taddr.
* 
* PARMETERS:
*   Hndl           - Handle returned from PCI64_Open
*   Taddr          - C64x target address  
*   Count          - Number of 32 bit data values
*   pData          - Pointer to array of 32 bit data types  
*        
* NOTES: Taddr must be aligned to 32 bit address boundary.  
*        Count must be less then 1024.
*        
*        Function uses the PCI region 2 default address mapping at DSP
*        address 0x01C00000 with a range of 4MBytes.  It can only address
*        registers in that range.
*
* RETURN VALUE:
*    0 Success, non-0 for Fail
*
*F***************************************************************************/

/*F***************************************************************************
* NAME:  PCI64_LoadCoff()
*
* DESCRIPTION:  Load Coff image to PCI target board
*    
* PARMETERS:
*   Hndl           - Handle returned from PCI64_Open
*   pCoffName      - Pointer to coff name string 
*     
* NOTES: The following restrictions apply:
*        -C64xx is held in reset.
*        -C64xx EMIF has been setup if loading off chip
*        -Non-relocatable coff file.
*        -All COFF sections must begin on a 32 bit boundary.  Beware this
*          may require link command file changes to ensure alignment. 
*          If a COFF ends on a non-32 bit boundary then the remaining
*          1-3 bytes will be filled with zero(s). 
*        -C64xx will start execution from address 0x00000000 per C64xx
*          PCI boot mode.
*
* RETURN VALUE:
*    0 Success, non-0 for Fail
*
*F***************************************************************************/

/*F***************************************************************************
* NAME:  PCI64_GetCallbackEvent()
*
* DESCRIPTION:  Get the handle used for callback events
*    
* PARMETERS:
*   Hndl  - Handle returned from PCI64_Open
*
* NOTES: This call is only valid after a successfull PCI64_Open call.
*
* RETURN VALUE:
*     HANDLE for Success; NULL for Fail
*
*F***************************************************************************/

/*F***************************************************************************
* NAME:  PCI64_SetupCallbackEvent()
*
* DESCRIPTION:  Setup a callback function to respond to PCI interrupts
*    
* PARMETERS:
*   Hndl                  - Handle returned from PCI64_Open
*   callbackFunction      - Pointer to callback function of type
*                             DWORD WINAPI Callback(LPVOID dummy)
*
* NOTES:  This function creates a thread to run the Callback function.
*
*
* RETURN VALUE:
*    0 Success, non-0 for Fail
*
*F***************************************************************************/


#endif