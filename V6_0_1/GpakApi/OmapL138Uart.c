//-----------------------------------------------------------------------------
// \file    evmomapl138_uart.c
// \brief   implementation of a uart driver for OMAP-L138.
//
//-----------------------------------------------------------------------------
#include <stdio.h>
#include <ti/sysbios/knl/Semaphore.h>
#include "adt_typedef.h"

//-----------------------------------------------------------------------------
// Private Defines and Macros
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// Register Structure & Defines
//-----------------------------------------------------------------------------
typedef struct uart_regs_t {
    volatile ADT_UInt32 RBR;             // 0x0000
    volatile ADT_UInt32 IER;             // 0x0004
    volatile ADT_UInt32 IIR;             // 0x0008
    volatile ADT_UInt32 LCR;             // 0x000C
    volatile ADT_UInt32 MCR;             // 0x0010
    volatile ADT_UInt32 LSR;             // 0x0014
    volatile ADT_UInt32 MSR;             // 0x0018
    volatile ADT_UInt32 SCR;             // 0x001C
    volatile ADT_UInt32 DLL;             // 0x0020
    volatile ADT_UInt32 DLH;             // 0x0024
    volatile ADT_UInt32 REV_ID1;         // 0x0028
    volatile ADT_UInt32 REV_ID2;         // 0x002C
    volatile ADT_UInt32 PWREMU_MGMT;     // 0x0030
    volatile ADT_UInt32 MDR;             // 0x0034
} uart_regs_t;

// define all the available uart peripherals for the processor.
uart_regs_t *UART0 = (uart_regs_t *) 0x01C42000;
uart_regs_t *UART1 = (uart_regs_t *) 0x01D0C000;
uart_regs_t *UART2 = (uart_regs_t *) 0x01D0D000;

uart_regs_t *uart[] = {
   (uart_regs_t *) 0x01C42000,
   (uart_regs_t *) 0x01D0C000,
   (uart_regs_t *) 0x01D0D000
};

//{ UART bit masks
#define THR RBR
#define FCR IIR


// bitmask defines for FCR.
#define RXFIFTL      (0x000000C0)   // bit 6,7
#define DMAMODE1     (0x00000008)   // bit 3
#define TXCLR        (0x00000004)   // bit 2
#define RXCLR        (0x00000002)   // bit 1
#define FIFOEN       (0x00000001)   // bit 0

// bitmask defines for LCR.
#define DLAB         (0x00000080)   // bit 7
#define BC           (0x00000040)   // bit 6
#define SP           (0x00000020)   // bit 5
#define EPS          (0x00000010)   // bit 4
#define PEN          (0x00000008)   // bit 3
#define LCR_STB      (0x00000004)   // bit 2
#define WLS          (0x00000003)   // bit 0,1
#define WLS_6        (0x00000001)   // bit 0
#define WLS_7        (0x00000002)   // bit 1
#define WLS_8        (0x00000003)   // bit 0,1

// bitmask defines for LSR.
#define RXFIFOE      (0x00000080)   // bit 7
#define TEMT         (0x00000040)   // bit 6
#define THRE         (0x00000020)   // bit 5
#define BI           (0x00000010)   // bit 4
#define FE           (0x00000008)   // bit 3
#define PE           (0x00000004)   // bit 2
#define OE           (0x00000002)   // bit 1
#define DR           (0x00000001)   // bit 0

// bitmask defines for PWREMU_MGMT.
#define UTRST        (0x00004000)   // bit 14
#define URRST        (0x00002000)   // bit 13

#define SOFT         (0x00000002)   // bit 1
#define FREE         (0x00000001)   // bit 0
//}

//{ Pin mux defines.
#define SYS_CFG_BASE     (0x01C14000)
#define UART0_CFG_MUX_CTL ((volatile ADT_UInt32*)(SYS_CFG_BASE + 0x12C))
#define UART0_CFG_MUX_DAT ((volatile ADT_UInt32*)(SYS_CFG_BASE + 0x12C))
#define UART0_CTL_MASK   (0xFF000000)
#define UART0_CTL_VAL    (0x22000000)
#define UART0_DAT_MASK   (0x00FF0000)
#define UART0_DAT_VAL    (0x00220000)

#define UART1_CFG_MUX_CTL ((volatile ADT_UInt32*)(SYS_CFG_BASE + 0x120))
#define UART1_CFG_MUX_DAT ((volatile ADT_UInt32*)(SYS_CFG_BASE + 0x130))
#define UART1_CTL_MASK   (0x00FF0000)
#define UART1_CTL_VAL    (0x00440000)
#define UART1_DAT_MASK   (0xFF000000)
#define UART1_DAT_VAL    (0x22000000)

#define UART2_CFG_MUX_CTL ((volatile ADT_UInt32*)(SYS_CFG_BASE + 0x120))
#define UART2_CFG_MUX_DAT ((volatile ADT_UInt32*)(SYS_CFG_BASE + 0x130))
#define UART2_CTL_MASK   (0xFF000000)
#define UART2_CTL_VAL    (0x44000000)
#define UART2_DAT_MASK   (0x00FF0000)
#define UART2_DAT_VAL    (0x00220000)
//}

//{ Power and sleep control
#define PSC0_BASE  0x01C10000
#define PSC0_CMD   ((volatile ADT_UInt32 *) (PSC0_BASE + 0x120))  // PTCMD
#define PSC0_STAT  ((volatile ADT_UInt32 *) (PSC0_BASE + 0x128))  // PTSTAT
#define PSC0_REG   ((volatile ADT_UInt32 *) (PSC0_BASE + 0xA00))  // MDCTLn

#define PSC1_BASE  0x01E27000
#define PSC1_CMD   ((volatile ADT_UInt32 *) (PSC1_BASE + 0x120))  // PTCMD
#define PSC1_STAT  ((volatile ADT_UInt32 *) (PSC1_BASE + 0x128))  // PTSTAT
#define PSC1_REG   ((volatile ADT_UInt32 *) (PSC1_BASE + 0xA00))  // MDCTLn

#define PSC_OFF 2
#define PSC_ON  3

//}

#define LPSC_UART0         (9)
#define LPSC_UART1         (12)
#define LPSC_UART2         (13)


//-----------------------------------------------------------------------------
// Private Function Prototypes
//-----------------------------------------------------------------------------
static ADT_UInt32 getStringLength (char *in_string);

void PowerOnDevice (int DeviceCode, int Group) {

   volatile ADT_UInt32 *PSC_CMD;
   volatile ADT_UInt32 *PSC_STAT;
   volatile ADT_UInt32 *PSC_REG;

   if (Group == 0) {
      PSC_CMD  = PSC0_CMD;
      PSC_STAT = PSC0_STAT;
      PSC_REG  = PSC0_REG;
   } else {
      PSC_CMD  = PSC1_CMD;
      PSC_STAT = PSC1_STAT;
      PSC_REG  = PSC1_REG;
   }


    // 1. Wait for previous power transition to complete
    while (*PSC_STAT & 1);

    // 2. Enable clock domains for device code
    PSC_REG[DeviceCode]  = PSC_ON;

    // 3. Transition power
    *PSC_CMD = 1;

    // 4. Wait for power transition to complete
    while (*PSC_STAT & 1);
}

ADT_UInt32 UART_init (ADT_UInt32 port, ADT_UInt32 baud_rate) {
   ADT_UInt32 divisor;
   uart_regs_t *uart;

   // enable the psc and config pinmux for the given uart port.
   switch (port) {
   case 0:
      uart = UART0;
      PowerOnDevice (LPSC_UART0, 0);
      *UART0_CFG_MUX_CTL &= UART0_CTL_MASK;
      *UART0_CFG_MUX_CTL |= UART0_CTL_VAL;

      *UART0_CFG_MUX_DAT &= UART0_DAT_MASK;
      *UART0_CFG_MUX_DAT |= UART0_DAT_VAL;
      break;
      
   case 1:
      uart = UART1;
      PowerOnDevice (LPSC_UART1, 1);
      *UART1_CFG_MUX_CTL &= UART1_CTL_MASK;
      *UART1_CFG_MUX_CTL |= UART1_CTL_VAL;

      *UART1_CFG_MUX_DAT &= UART1_DAT_MASK;
      *UART1_CFG_MUX_DAT |= UART1_DAT_VAL;
      break;
      
   case 2:
      uart = UART2;
      PowerOnDevice (LPSC_UART2, 1);
      *UART2_CFG_MUX_CTL &= UART2_CTL_MASK;
      *UART2_CFG_MUX_CTL |= UART2_CTL_VAL;

      *UART2_CFG_MUX_DAT &= UART2_DAT_MASK;
      *UART2_CFG_MUX_DAT |= UART2_DAT_VAL;
      break;

   default:
      return 0;
   }

   // put xmtr/rcvr in reset.
   uart->PWREMU_MGMT = 0;

   // set baud rate...assumes default 16x oversampling.
   divisor = (150000000) / (baud_rate * 16);
   uart->DLH = (divisor & 0x0000FF00) >> 8;
   uart->DLL = divisor & 0x000000FF;

   // enable xmtr/rcvr fifos.
   uart->FCR = 0;
   uart->FCR |= FIFOEN;
   uart->FCR |= (RXCLR | TXCLR | DMAMODE1);

   // disable interrupts, flow control, and loop back.
   uart->IER = 0;
   uart->MCR = 0;
   uart->MDR = 0;

   // config LCR for no parity, one stop bit, 8 data bits, no flow control.
   uart->LCR = 0;
   uart->LCR |= WLS_8;

   // take xmtr/rcvr out of reset.
   uart->PWREMU_MGMT |= (UTRST | URRST | FREE);
   return 1;
}

ADT_UInt32 UART_txByte (ADT_UInt32 port, ADT_UInt8 data) {

   // wait until there is room in the FIFO;  then send character
   if (2 < port) return 0;

   while (!(uart[port]->LSR & THRE)) {}
   uart[port]->THR = data;

   if (data != '\n') return 1;

   // Add return on newline
   while (!(uart[port]->LSR & THRE)) {}
   uart[port]->THR = '\r';

   return 1;
}

ADT_UInt32 UART_txArray (ADT_UInt32 port, ADT_UInt8 *data, ADT_UInt32 in_length) {
   ADT_UInt32 rtn = 0;
   ADT_UInt32 i;
   
   if (2 < port) return 0;
   if (data == NULL) return 0;

   for (i = 0; i < in_length; i++) {
      rtn += UART_txByte (port, data[i]);
   }
   return rtn;
}

ADT_UInt32 UART_txString (ADT_UInt32 port, char *data) {
   
   ADT_UInt32 numBytes;

   if (2 < port) return 0;
   if (data == NULL) return 0;
   numBytes = getStringLength (data);

   return UART_txArray (port, (ADT_UInt8 *) data, numBytes);
}

ADT_UInt32 UART_txHex8 (ADT_UInt32 port, ADT_UInt8 data) {
   ADT_UInt8 tmpArray[5];

   if (2 < port) return 0;

   sprintf ((char *)tmpArray, "0x%02X", data);

   return UART_txArray (port, tmpArray, 4);
}

ADT_UInt32 UART_txHex32 (ADT_UInt32 port, ADT_UInt32 data) {
   ADT_UInt8 tmpArray[12];

   if (2 < port) return 0;

   sprintf ((char *)tmpArray, "0x%08X", data);

   return UART_txArray (port, tmpArray, 10);
}

ADT_UInt32 UART_rxByte (ADT_UInt32 port, ADT_UInt8 *data) {
   if (2 < port) return 0;

   if (uart[port]->LSR & DR) {
      *data = (ADT_UInt8) uart[port]->RBR;
      return 1;
   }
   return 0;
}

static ADT_UInt32 getStringLength (char *in_string) {
   ADT_UInt32 numBytes = 0;

   while (in_string[numBytes] != 0) {
      numBytes++;
   }

   return numBytes;
}
