// GpakApiCannedTests.c
//
//  Canned tests of G.PAK APIs
#ifdef BIOS
   #include <xdc/std.h>
#endif

#define _CRT_SECURE_NO_WARNINGS 1

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "GpakApiTestSupport.h"
#include "sysconfig.h"

#define Frame_2ms ((GpakFrameHalfMS) 4)
#define ESC 'x'
#define PRMPT ((ADT_UInt16) -1)

#define THIRD_VOICE_BUFFER 0

#define NLPoff      0
#define GaindB0     0
#define AllTonesOff ((GpakToneTypes) ToneDetectAll)

#define WBEnableBit    0x00000001
#define PCMECEnableBit 0x00004000
#define PKTECEnableBit 0x00008000
#define DTMFEnableBit  0x10000000
#define VadEnableBit   0x40000000
#define AGCEnableBit   0x80000000

#define WBEnabled (sysCfg.CfgEnableFlags & 1)
#define NoTearDown   (GpakToneTypes) 0xffff

extern GpakSystemConfig_t sysCfg;
extern GpakSystemParms_t  sysParams;
extern char *prompt;
extern char *chanType[];
extern int cyclesPerDisplay;
extern int cyclesSinceDisplay;
extern void toneAlgControl(ADT_UInt32 DspId);

// To reset statistics
const resetSsMask fullStatsReset = { 1, 1, 1 };
chanDef *testSuite;
static ADT_UInt16 daisyChainStart      = (ADT_UInt16) -1;  // Daisy chain start
static ADT_UInt16 daisyChainTerminator = (ADT_UInt16) -1;  // Daisy chain terminator

// Canned tone insertion tones
typedef struct RTPTones_t {
   int digit, volumeNegDB, durationSamps;
} RTPTones_t;

RTPTones_t RTPTones[] = {
   { 0, 2, 1000 },  // 125 ms
   { 1, 4, 2000 },  // 250 ms
   { 2, 6, 4000 },  // 500 ms
   { 3, 8, 8000 },  // 1 sec
   { 4, 2, 1000 },  // 125 ms

   { 5, 4, 2000 },  // 250 ms
   { 6, 6, 4000 },  // 500 ms
   { 7, 8, 8000 },  // 1 sec
   { 8, 2, 1000 },  // 125 ms
   { 9, 4, 2000 },  // 250 ms

   { -1 }
};

// Canned tone generation tones
GpakToneGenParms_t CannedTones [] = {
   {  ToneGenStart, ADevice, TgBurst, {  697, 1209, 0, 0 }, {  -5,  -5, 0, 0 }, { 50, 0, 0, 0 }, { 50, 0, 0, 0 } },
   {  ToneGenStart, ADevice, TgBurst, {  697, 1336, 0, 0 }, { -10, -10, 0, 0 }, { 60, 0, 0, 0 }, { 60, 0, 0, 0 } },
   {  ToneGenStart, ADevice, TgBurst, {  697, 1477, 0, 0 }, { -15, -15, 0, 0 }, { 70, 0, 0, 0 }, { 70, 0, 0, 0 } },
   {  ToneGenStart, ADevice, TgBurst, {  697, 1633, 0, 0 }, { -19, -19, 0, 0 }, { 80, 0, 0, 0 }, { 80, 0, 0, 0 } },

   {  ToneGenStart, ADevice, TgBurst, {  770, 1209, 0, 0 }, {  -5,  -5, 0, 0 }, { 50, 0, 0, 0 }, { 50, 0, 0, 0 } },
   {  ToneGenStart, ADevice, TgBurst, {  770, 1336, 0, 0 }, { -10, -10, 0, 0 }, { 60, 0, 0, 0 }, { 60, 0, 0, 0 } },
   {  ToneGenStart, ADevice, TgBurst, {  770, 1477, 0, 0 }, { -15, -15, 0, 0 }, { 70, 0, 0, 0 }, { 70, 0, 0, 0 } },
   {  ToneGenStart, ADevice, TgBurst, {  770, 1633, 0, 0 }, { -19, -19, 0, 0 }, { 80, 0, 0, 0 }, { 80, 0, 0, 0 } },

   {  ToneGenStart, ADevice, TgBurst, {  852, 1209, 0, 0 }, {  -5,  -5, 0, 0 }, { 50, 0, 0, 0 }, { 50, 0, 0, 0 } },
   {  ToneGenStart, ADevice, TgBurst, {  852, 1336, 0, 0 }, { -10, -10, 0, 0 }, { 60, 0, 0, 0 }, { 60, 0, 0, 0 } },
   {  ToneGenStart, ADevice, TgBurst, {  852, 1477, 0, 0 }, { -15, -15, 0, 0 }, { 70, 0, 0, 0 }, { 70, 0, 0, 0 } },
   {  ToneGenStart, ADevice, TgBurst, {  852, 1633, 0, 0 }, { -19, -19, 0, 0 }, { 80, 0, 0, 0 }, { 80, 0, 0, 0 } },
   
   {  ToneGenStart, ADevice, TgBurst, {  941, 1209, 0, 0 }, {  -5,  -5, 0, 0 }, { 50, 0, 0, 0 }, { 50, 0, 0, 0 } },
   {  ToneGenStart, ADevice, TgBurst, {  941, 1336, 0, 0 }, { -10, -10, 0, 0 }, { 60, 0, 0, 0 }, { 60, 0, 0, 0 } },
   {  ToneGenStart, ADevice, TgBurst, {  941, 1477, 0, 0 }, { -15, -15, 0, 0 }, { 70, 0, 0, 0 }, { 70, 0, 0, 0 } },
   {  ToneGenStart, ADevice, TgBurst, {  941, 1633, 0, 0 }, { -19, -19, 0, 0 }, { 80, 0, 0, 0 }, { 80, 0, 0, 0 } },


   {  ToneGenStart, BDevice, TgBurst, {  697, 1209, 0, 0 }, {  -5,  -5, 0, 0 }, { 50, 0, 0, 0 }, { 50, 0, 0, 0 } },
   {  ToneGenStart, BDevice, TgBurst, {  697, 1336, 0, 0 }, { -10, -10, 0, 0 }, { 60, 0, 0, 0 }, { 60, 0, 0, 0 } },
   {  ToneGenStart, BDevice, TgBurst, {  697, 1477, 0, 0 }, { -15, -15, 0, 0 }, { 70, 0, 0, 0 }, { 70, 0, 0, 0 } },
   {  ToneGenStart, BDevice, TgBurst, {  697, 1633, 0, 0 }, { -19, -19, 0, 0 }, { 80, 0, 0, 0 }, { 80, 0, 0, 0 } },

   {  ToneGenStart, BDevice, TgBurst, {  770, 1209, 0, 0 }, {  -5,  -5, 0, 0 }, { 50, 0, 0, 0 }, { 50, 0, 0, 0 } },
   {  ToneGenStart, BDevice, TgBurst, {  770, 1336, 0, 0 }, { -10, -10, 0, 0 }, { 60, 0, 0, 0 }, { 60, 0, 0, 0 } },
   {  ToneGenStart, BDevice, TgBurst, {  770, 1477, 0, 0 }, { -15, -15, 0, 0 }, { 70, 0, 0, 0 }, { 70, 0, 0, 0 } },
   {  ToneGenStart, BDevice, TgBurst, {  770, 1633, 0, 0 }, { -19, -19, 0, 0 }, { 80, 0, 0, 0 }, { 80, 0, 0, 0 } },

   {  ToneGenStart, BDevice, TgBurst, {  852, 1209, 0, 0 }, {  -5,  -5, 0, 0 }, { 50, 0, 0, 0 }, { 50, 0, 0, 0 } },
   {  ToneGenStart, BDevice, TgBurst, {  852, 1336, 0, 0 }, { -10, -10, 0, 0 }, { 60, 0, 0, 0 }, { 60, 0, 0, 0 } },
   {  ToneGenStart, BDevice, TgBurst, {  852, 1477, 0, 0 }, { -15, -15, 0, 0 }, { 70, 0, 0, 0 }, { 70, 0, 0, 0 } },
   {  ToneGenStart, BDevice, TgBurst, {  852, 1633, 0, 0 }, { -19, -19, 0, 0 }, { 80, 0, 0, 0 }, { 80, 0, 0, 0 } },

   {  ToneGenStart, BDevice, TgBurst, {  941, 1209, 0, 0 }, {  -5,  -5, 0, 0 }, { 50, 0, 0, 0 }, { 50, 0, 0, 0 } },
   {  ToneGenStart, BDevice, TgBurst, {  941, 1336, 0, 0 }, { -10, -10, 0, 0 }, { 60, 0, 0, 0 }, { 60, 0, 0, 0 } },
   {  ToneGenStart, BDevice, TgBurst, {  941, 1477, 0, 0 }, { -15, -15, 0, 0 }, { 70, 0, 0, 0 }, { 70, 0, 0, 0 } },
   {  ToneGenStart, BDevice, TgBurst, {  941, 1633, 0, 0 }, { -19, -19, 0, 0 }, { 80, 0, 0, 0 }, { 80, 0, 0, 0 } },

   {  ToneGenStop }
 };
GpakToneGenParms_t StopTones = {  ToneGenStop };

char dummySetupChannels (ADT_UInt32 DspId) { return ' '; }
 
char (*setupChannels) (ADT_UInt32 DspId) = dummySetupChannels;

//-----------------------------------------------------------------------------------
//{  Canned channel setup table configurations
//{ TDM-TDM tests
chanDef TdmTdm1[]= {
NO_LOOPBACK_LIST_START("TT1.  TDM to TDM 5ms channel"),
{   {0,  0,   pcmToPcm },  LFT_TDM, RGT_TDM, NO_PKT_5ms, ALL_DISABLES },
LIST_END
};
chanDef TdmTdm2[]= {
NO_LOOPBACK_LIST_START("TT1.  TDM to TDM 10ms channel"),
{   {0,  0,   pcmToPcm },  LFT_TDM, RGT_TDM, NO_PKT_10ms, ALL_DISABLES },
LIST_END
};
chanDef TdmTdm3[]= {
NO_LOOPBACK_LIST_START("TT1.  TDM to TDM 20ms channel"),
{   {0,  0,   pcmToPcm },  LFT_TDM, RGT_TDM, NO_PKT_20ms, ALL_DISABLES },
LIST_END
};
chanDef TdmTdm4[]= {
NO_LOOPBACK_LIST_START("TT1.  TDM to TDM 22.5 ms channel"),
{   {0,  0,   pcmToPcm },  LFT_TDM, RGT_TDM, NO_PKT_22ms, ALL_DISABLES },
LIST_END
};
chanDef TdmTdm5[]= {
NO_LOOPBACK_LIST_START("TT1.  TDM to TDM 30 ms channel"),
{   {0,  0,   pcmToPcm },  LFT_TDM, RGT_TDM, NO_PKT_30ms, ALL_DISABLES },
LIST_END
};
//}

//{ TDM-PKT tests use channel cross over setup

// ID, Cnfr, Type,    {   portA,  inslotA,   outslotA }, { portB, inslotB,  outslotB    },    
//                    {   decoder,     decodeFrame,      encoder,     encodeFrame,   RTP    },
//                    {   AGC,             VAD,           PCM_EC,      PKT_EC,        AEC   }
#define TDM_PKT_SETUP(E1,F1,ID)  \
CROSSOVER_LIST_START(ID),                                                                       \
{   {0,  0,   pcmToPacket},       LFT_TDM, NULL_TDM, { E1,  F1,  E1,  F1,  E1  }, ALL_DISABLES }, \
{   {1,  0,   pcmToPacket},       RGT_TDM, NULL_TDM, { E1,  F1,  E1,  F1,  E1  }, ALL_DISABLES }, \
LIST_END
// Narrow band vocoder tests
chanDef TdmPkt1[]  = {  TDM_PKT_SETUP (PCMU_64, Frame_1ms,    "TP1.  PCMU:1ms") };
chanDef TdmPkt2[]  = {  TDM_PKT_SETUP (PCMA_64, Frame_5ms,    "TP2.  PCMA:5ms") };
chanDef TdmPkt3[]  = {  TDM_PKT_SETUP (L16,     Frame_10ms,   "TP3.  L16:10ms") };
chanDef TdmPkt4[]  = {  TDM_PKT_SETUP (G729,    Frame_10ms,   "TP4.  G729:10ms") };
chanDef TdmPkt5[]  = {  TDM_PKT_SETUP (G729D,   Frame_10ms,   "TP5.  G729D:10ms") };
chanDef TdmPkt6[]  = {  TDM_PKT_SETUP (G729E,   Frame_10ms,   "TP6.  G729E:10ms") };
chanDef TdmPkt7[]  = {  TDM_PKT_SETUP (G729AB,  Frame_10ms,   "TP7.  G729AB:10ms") };
chanDef TdmPkt8[]  = {  TDM_PKT_SETUP (G729AB,  Frame_20ms,   "TP8.  G729AB:20ms") };
chanDef TdmPkt9[]  = {  TDM_PKT_SETUP (L8,      Frame_20ms,   "TP9.  L8:20ms") };
chanDef TdmPkt10[] = {  TDM_PKT_SETUP (Speex,   Frame_20ms,   "TP10. Speex:20ms") };
chanDef TdmPkt11[] = {  TDM_PKT_SETUP (MELP,    Frame_22_5ms, "TP11. MELP:22.5ms") };
chanDef TdmPkt12[] = {  TDM_PKT_SETUP (MELP_E,  Frame_22_5ms, "TP12. MELP_E:22.5ms") };

// Wide band vocoder tests
chanDef TdmPkt13[] = {  TDM_PKT_SETUP (PCMU_WB, Frame_5ms,  "TP13. PCMU_WB:5ms") };
chanDef TdmPkt14[] = {  TDM_PKT_SETUP (PCMA_WB, Frame_5ms,  "TP14. PCMA_WB:5ms") };
chanDef TdmPkt15[] = {  TDM_PKT_SETUP (L16_WB,  Frame_10ms, "TP15. L16_WB:10ms") };
chanDef TdmPkt16[] = {  TDM_PKT_SETUP (SpeexWB, Frame_20ms, "TP16. Speex_WB:20ms") };
chanDef TdmPkt17[] = {  TDM_PKT_SETUP (G722_64, Frame_10ms, "TP17. G722:10ms") };

// Custom frame rate tests
chanDef TdmPkt18[] = {  TDM_PKT_SETUP (L16,     Frame_2ms, "TP18. L16:2ms") };
chanDef TdmPkt19[] = {  TDM_PKT_SETUP (PCMU_64, Frame_2ms, "TP19. PCMU_64:2ms") };
chanDef TdmPkt20[] = {  TDM_PKT_SETUP (PCMA_64, Frame_2ms, "TP120 PCMA_64:2ms") };
//}

//{ PKT-PKT tests use channel daisy chain setup

// ID, Cnfr, Type,    { portA,  inslotA,   outslotA }, { portB, inslotB,  outslotB  },    
//                    {   decoder,     decodeFrame,      encoder,     encodeFrame,   RTP      },
//                    {   AGC,             VAD,           PCM_EC,      PKT_EC,        AEC     }
#define TRANSCODING_SETUP(E1,F1,E2,F2,ID)  \
DAISY_CHAIN_LIST_START(ID),                                                                       \
{   {0,  0,   pcmToPacket},       LFT_TDM, NULL_TDM, { E1,  F1,  E1,  F1,  E1  }, ALL_DISABLES }, \
{   {1,  0,   packetToPacket},   NULL_TDM, NULL_TDM, { E1,  F1,  E2,  F2,  E2  }, ALL_DISABLES }, \
{   {3,  0,   pcmToPacket},       RGT_TDM, NULL_TDM, { E2,  F2,  E2,  F2,  E2  }, ALL_DISABLES }, \
LIST_END
// Narrowband to narrowband same rate
chanDef PktPkt1[]  = {  TRANSCODING_SETUP (L16, Frame_5ms,    PCMU_64, Frame_5ms,    "PP1. L16:5ms<->PCMU:5ms")  };
chanDef PktPkt2[]  = {  TRANSCODING_SETUP (L16, Frame_5ms,    PCMA_64, Frame_5ms,    "PP2. L16:5ms<->PCMA:5ms")  };
chanDef PktPkt3[]  = {  TRANSCODING_SETUP (L16, Frame_10ms,   G729AB,  Frame_10ms,   "PP3. L16:10ms<->G729AB:10ms") };
chanDef PktPkt4[]  = {  TRANSCODING_SETUP (L16, Frame_20ms,   G729AB,  Frame_20ms,   "PP4. L16:20ms<->G729AB:20ms") };
chanDef PktPkt5[]  = {  TRANSCODING_SETUP (L16, Frame_20ms,   Speex,   Frame_20ms,   "PP5. L16:20ms<->Speex:20ms") };
chanDef PktPkt6[]  = {  TRANSCODING_SETUP (L16, Frame_22_5ms, MELP,    Frame_22_5ms, "PP6. L16:22.5ms<->MELP:22.5ms") };

// Narrowband to narrowband different rates
chanDef PktPkt7[]  = {  TRANSCODING_SETUP (L16, Frame_10ms,   PCMU_64, Frame_5ms,    "PP7.  L16:10ms<->PCMU:5ms")  };
chanDef PktPkt8[]  = {  TRANSCODING_SETUP (L16, Frame_5ms,    PCMA_64, Frame_10ms,   "PP8.  L16:5ms<->PCMA:10ms")  };
chanDef PktPkt9[]  = {  TRANSCODING_SETUP (L16, Frame_5ms,    G729AB,  Frame_20ms,   "PP9.  L16:5ms<->G729AB:20ms") };
chanDef PktPkt10[] = {  TRANSCODING_SETUP (L16, Frame_10ms,   MELP,    Frame_22_5ms, "PP10. L16:10ms<->MELP:22.5ms") };

// Wideband to wideband same rates
chanDef PktPkt11[] = {  TRANSCODING_SETUP (L16_WB, Frame_5ms,    PCMU_WB, Frame_5ms,    "PP11. L16WB:5ms<->PCMU_WB:5ms")  };
chanDef PktPkt12[] = {  TRANSCODING_SETUP (L16_WB, Frame_10ms,   PCMA_WB, Frame_10ms,   "PP12. L16WB:10ms<->PCMA_WB:10ms")  };
chanDef PktPkt13[] = {  TRANSCODING_SETUP (L16_WB, Frame_20ms,   SpeexWB, Frame_20ms,   "PP13. L16WB:20ms<->SpeexWB:20ms") };

// Wideband to wideband different rates
chanDef PktPkt14[] = {  TRANSCODING_SETUP (L16_WB, Frame_5ms,    PCMU_WB, Frame_10ms,   "PP14. L16WB:5ms<->PCMU_WB:10ms")  };
chanDef PktPkt15[] = {  TRANSCODING_SETUP (L16_WB, Frame_20ms,   PCMA_WB, Frame_5ms,    "PP15. L16WB:20ms<->PCMA_WB:5ms")  };
chanDef PktPkt16[] = {  TRANSCODING_SETUP (L16_WB, Frame_10ms,   SpeexWB, Frame_20ms,   "PP16. L16WB:10ms<->SpeexWB:20ms") };

// Narrowband to wideband same rates
chanDef PktPkt17[] = {  TRANSCODING_SETUP (L16,     Frame_5ms,    PCMU_WB, Frame_5ms,    "PP17. L16:5ms<->PCMU_WB:5ms")  };
chanDef PktPkt18[] = {  TRANSCODING_SETUP (L16,     Frame_10ms,   PCMA_WB, Frame_10ms,   "PP18. L16:10ms<->PCMA_WB:10ms")  };
chanDef PktPkt19[] = {  TRANSCODING_SETUP (L16,     Frame_20ms,   SpeexWB, Frame_20ms,   "PP19. L16:20ms<->SpeexWB:20ms") };
chanDef PktPkt20[] = {  TRANSCODING_SETUP (PCMU_WB, Frame_5ms,    L16, Frame_5ms,        "PP20. L16:5ms<->PCMU_WB:5ms")  };
chanDef PktPkt21[] = {  TRANSCODING_SETUP (PCMA_WB, Frame_10ms,   L16, Frame_10ms,       "PP21. L16:10ms<->PCMA_WB:10ms")  };
chanDef PktPkt22[] = {  TRANSCODING_SETUP (SpeexWB, Frame_20ms,   L16, Frame_20ms,       "PP22. L16:20ms<->SpeexWB:20ms") };

// Narrowband to wideband different rates
chanDef PktPkt23[] = {  TRANSCODING_SETUP (L16, Frame_5ms,    PCMU_WB, Frame_10ms,   "PP23. L16:5ms<->PCMU_WB:10ms")  };
chanDef PktPkt24[] = {  TRANSCODING_SETUP (L16, Frame_20ms,   PCMA_WB, Frame_10ms,   "PP24. L16:10ms<->PCMA_WB:10ms")  };
chanDef PktPkt25[] = {  TRANSCODING_SETUP (L16, Frame_10ms,   SpeexWB, Frame_20ms,   "PP25. L16:20ms<->SpeexWB:20ms") };
chanDef PktPkt26[] = {  TRANSCODING_SETUP (L16_WB, Frame_5ms, PCMA_64, Frame_10ms,   "PP26. L16_WB:5ms<->PCMA:10ms")  };
//}

//{ Conference test channels
//-----------------------------------------------------------------------------------
//  PKT -> PKT will playback voice for PKT -> Cnfr
//  0  PKT->Cnfr (Spkr1)  <->  1 PKT->PKT
//  5  TDM->Cnfr (Spkr2)
//  7  TDM->Cnfr (Spkr3)

#define TDM_PKT_SETUP(E1,F1,ID)  \
CROSSOVER_LIST_START(ID),                                                                       \
{   {0,  0,   pcmToPacket}, LFT_TDM,  NULL_TDM, {   E1,  F1,  E1,  F1,  E1  }, ALL_DISABLES }, \
{   {1,  0,   pcmToPacket}, RGT_TDM,  NULL_TDM, {   E1,  F1,  E1,  F1,  E1  }, ALL_DISABLES }, \
LIST_END
//chanDef Cnfr0[] = { TDM_PKT_SETUP(L16_WB, Frame_5ms, "Voice recording for use by conferencing")  };

chanDef Cnfr1[] = {
CROSSOVER_CNFR_START("CF1.   5ms:  2 TDMCnfr  1 L16WB PktCnfr", 0, Frame_5ms),
{   {0,  0,   conferencePacket},  NULL_TDM, NULL_TDM, {  L16_WB,    Frame_5ms, L16_WB,  Frame_5ms }, ALL_DISABLES  },
{   {1,  0,   packetToPacket},    NULL_TDM, NULL_TDM, {  L16_WB,    Frame_5ms, L16_WB,  Frame_5ms }, ALL_DISABLES  },
{   {3,  0,   conferencePcm},     LFT_TDM,  NULL_TDM,  NO_PKT_5ms, ALL_DISABLES  }, 
{   {4,  0,   conferencePcm},     RGT_TDM,  NULL_TDM,  NO_PKT_5ms, ALL_DISABLES  }, 
LIST_END
};

#define PKT_CNFR_SETUP(CNFR,E1,E2,E3,RATE,ID) \
CROSSOVER_CNFR_START(ID,CNFR,RATE),                                                                          \
{   {0,  CNFR,   conferencePacket},  NULL_TDM, NULL_TDM, {  E1,  RATE,  E1,  RATE,  E1   }, ALL_DISABLES  }, \
{   {1,  CNFR,   packetToPacket},    NULL_TDM, NULL_TDM, {  E1,  RATE,  E1,  RATE,  E1   }, ALL_DISABLES  }, \
{   {4,  CNFR,   pcmToPacket},       LFT_TDM,  NULL_TDM, {  E2,  RATE,  E2,  RATE,  E2   }, ALL_DISABLES  }, \
{   {5,  CNFR,   conferencePacket},  NULL_TDM, NULL_TDM, {  E2,  RATE,  E2,  RATE,  E2   }, ALL_DISABLES  }, \
{   {6,  CNFR,   pcmToPacket},       RGT_TDM,  NULL_TDM, {  E3,  RATE,  E3,  RATE,  E3   }, ALL_DISABLES  }, \
{   {7,  CNFR,   conferencePacket},  NULL_TDM, NULL_TDM, {  E3,  RATE,  E3,  RATE,  E3   }, ALL_DISABLES  }, \
LIST_END
chanDef Cnfr2[]= { PKT_CNFR_SETUP (0, L16, L16_WB, G729AB, Frame_5ms,    "CF2     5ms:   L16  L16WB  G729AB") };
chanDef Cnfr3[]= { PKT_CNFR_SETUP (1, L16, L16_WB, G729AB, Frame_10ms,   "CF3.   10ms:   L16  L16WB  G729AB") };
chanDef Cnfr4[]= { PKT_CNFR_SETUP (2, L16, L16_WB, MELP_E, Frame_22_5ms, "CF4. 22.5ms:   L16  L16WB MELP_E") };
//}

//{ Tone detection test channels
#define MIXED_CNFR_SETUP(CNFR,RATEC,E1,RATE1,ID)                                                      \
CROSSOVER_CNFR_START(ID, CNFR, RATEC),                                                                \
{   {0,  CNFR,   conferencePcm},     LFT_TDM,  NULL_TDM, { NullCodec, RATEC },      ALL_DISABLES  },  \
{   {2,  CNFR,   pcmToPacket},       RGT_TDM,  NULL_TDM, {  E1, RATE1, E1, RATE1 }, ALL_DISABLES  },  \
{   {3,  CNFR,   conferencePacket},  NULL_TDM, NULL_TDM, {  E1, RATE1, E1, RATE1 }, ALL_DISABLES  },  \
LIST_END
chanDef ToneCfr1[]  = { MIXED_CNFR_SETUP (0, Frame_10ms,   L16,    Frame_10ms,   "TC1.  TDM + L16:10ms     10ms conference") };
chanDef ToneCfr2[]  = { MIXED_CNFR_SETUP (1, Frame_10ms,   L16_WB, Frame_10ms,   "TC2.  TDM + L16WB:10ms   10ms conference") };
chanDef ToneCfr3[]  = { MIXED_CNFR_SETUP (0, Frame_20ms,   L16,    Frame_10ms,   "TC3.  TDM + L16:10ms     20ms conference") };
chanDef ToneCfr4[]  = { MIXED_CNFR_SETUP (1, Frame_20ms,   L16_WB, Frame_10ms,   "TC4.  TDM + L16WB:10ms   20ms conference") };
chanDef ToneCfr5[]  = { MIXED_CNFR_SETUP (0, Frame_22_5ms, L16,    Frame_10ms,   "TC5.  TDM + L16:10ms     22.5ms conference") };
chanDef ToneCfr6[]  = { MIXED_CNFR_SETUP (1, Frame_22_5ms, L16_WB, Frame_22_5ms, "TC6.  TDM + L16WB:22.5ms 22.5ms conference") };

chanDef ToneCfr7[]  = { MIXED_CNFR_SETUP (0, Frame_5ms,    L16,    Frame_10ms,   "TC7.  TDM + L16:10ms      5ms conference") };
chanDef ToneCfr8[]  = { MIXED_CNFR_SETUP (0, Frame_5ms,    L16,    Frame_10ms,   "TC8.  TDM + L16:10ms      5ms conference") };
chanDef ToneCfr9[]  = { MIXED_CNFR_SETUP (1, Frame_10ms,   L16_WB, Frame_5ms,    "TC9.  TDM + L16WB: 5ms   10ms conference") };
chanDef ToneCfr10[] = { MIXED_CNFR_SETUP (1, Frame_10ms,   L16_WB, Frame_5ms,    "TC10. TDM + L16WB: 5ms   10ms conference") };
chanDef ToneCfr11[] = { MIXED_CNFR_SETUP (2, Frame_5ms,    L16_WB, Frame_2ms,    "TC11. TDM + L16WB: 5ms    2ms conference") };
chanDef ToneCfr12[] = { MIXED_CNFR_SETUP (2, Frame_2ms,    L16_WB, Frame_5ms,    "TC12. TDM + L16WB: 2ms    5ms conference") };

chanDef ToneCfr13[] = { MIXED_CNFR_SETUP (2, Frame_10ms,   G729AB, Frame_10ms,   "TC13. TDM + G729:10ms    10ms conference") };
chanDef ToneCfr14[] = { MIXED_CNFR_SETUP (2, Frame_20ms,   G729AB, Frame_10ms,   "TC14. TDM + G729:10ms    20ms conference") };
chanDef ToneCfr15[] = { MIXED_CNFR_SETUP (3, Frame_20ms,   MELP_E, Frame_22_5ms, "TC15. TDM + MELPE:22.5ms 20ms conference") };
chanDef ToneCfr16[] = { MIXED_CNFR_SETUP (2, Frame_22_5ms, G729AB, Frame_20ms,   "TC16. TDM + G729:20ms    22.5ms conference") };
chanDef ToneCfr17[] = { MIXED_CNFR_SETUP (3, Frame_22_5ms, MELP_E, Frame_22_5ms, "TC17. TDM + MELPE:22.5ms 22.5ms conference") };
//}

//{ VAD test channels
#define TP_VAD_SETUP(E1,F1,ID)  \
CROSSOVER_LIST_START(ID),                                                                          \
{   {0,  0,   pcmToPacket},       LFT_TDM, NULL_TDM, { E1,  F1,  E1,  F1,  E1  }, VADCNG_TESTS },  \
{   {1,  0,   pcmToPacket},       RGT_TDM, NULL_TDM, { E1,  F1,  E1,  F1,  E1  }, ALL_DISABLES },  \
LIST_END
//  VAD testing
chanDef TdmPkt101[] = {  TP_VAD_SETUP (G729AB,  Frame_20ms,   "TP101. G729AB:20ms   VAD") };
chanDef TdmPkt102[] = {  TP_VAD_SETUP (L16,     Frame_20ms,   "TP102. L16:20ms      VAD") };
chanDef TdmPkt103[] = {  TP_VAD_SETUP (L16_WB,  Frame_10ms,   "TP103. L16_WB:10ms   VAD") };

#define TT_VAD_SETUP(E1,F1,E2,F2,ID)  \
DAISY_CHAIN_LIST_START(ID),                                                                        \
{   {0,  0,   pcmToPacket},       LFT_TDM, NULL_TDM, { E1,  F1,  E1,  F1,  E1  }, VADCNG_TESTS },  \
{   {1,  0,   packetToPacket},   NULL_TDM, NULL_TDM, { E1,  F1,  E2,  F2,  E2  }, VADCNG_TESTS },  \
{   {3,  0,   pcmToPacket},       RGT_TDM, NULL_TDM, { E2,  F2,  E2,  F2,  E2  }, ALL_DISABLES },  \
LIST_END
chanDef PktPkt101[] = {  TT_VAD_SETUP (L16,    Frame_5ms,    PCMU_64, Frame_5ms,    "PP101. L16:5ms<->PCMU:5ms  VAD")  };
chanDef PktPkt102[] = {  TT_VAD_SETUP (L16,    Frame_10ms,   PCMU_64, Frame_5ms,    "PP102. L16:10ms<->PCMU:5ms VAD")  };
chanDef PktPkt103[] = {  TT_VAD_SETUP (L16_WB, Frame_5ms,    PCMU_WB, Frame_5ms,    "PP103. L16WB:5ms<->PCMU_WB:5ms  VAD")  };
chanDef PktPkt104[] = {  TT_VAD_SETUP (L16_WB, Frame_5ms,    PCMU_WB, Frame_10ms,   "PP104. L16WB:5ms<->PCMU_WB:10ms VAD")  };
chanDef PktPkt105[] = {  TT_VAD_SETUP (L16,    Frame_5ms,    PCMU_WB, Frame_5ms,    "PP105. L16:5ms<->PCMU_WB:5ms    VAD")  };
chanDef PktPkt106[] = {  TT_VAD_SETUP (L16,    Frame_20ms,   PCMA_WB, Frame_10ms,   "PP106. L16:20ms<->PCMA_WB:10ms  VAD")  };

#define VAD_CNFR_SETUP(CNFR,RATEC,E1,RATE1,ID)                                                       \
CROSSOVER_CNFR_START(ID, CNFR, RATEC),                                                               \
{   {0,  CNFR,   conferencePacket},  NULL_TDM, NULL_TDM, {  E1, RATE1, E1, RATE1 }, VADCNG_TESTS },  \
{   {1,  CNFR,   pcmToPacket},       RGT_TDM,  NULL_TDM, {  E1, RATE1, E1, RATE1 }, VADCNG_TESTS },  \
{   {3,  CNFR,   conferencePcm},     LFT_TDM,  NULL_TDM, { NullCodec, RATEC },      ALL_DISABLES },  \
LIST_END
chanDef ToneCfr101[] = { VAD_CNFR_SETUP (0, Frame_10ms,   L16,    Frame_10ms,   "TC101.  TDM + L16:10ms     10ms conference  VAD") };
chanDef ToneCfr102[] = { VAD_CNFR_SETUP (1, Frame_10ms,   L16_WB, Frame_10ms,   "TC102.  TDM + L16WB:10ms   10ms conference  VAD") };
chanDef ToneCfr103[] = { VAD_CNFR_SETUP (2, Frame_10ms,   G729AB, Frame_10ms,   "TC103.  TDM + G729:10ms    10ms conference  VAD") };
chanDef ToneCfr104[] = { VAD_CNFR_SETUP (0, Frame_20ms,   L16,    Frame_10ms,   "TC104.  TDM + L16:10ms     20ms conference  VAD") };
//}

//{ AGC test channels
chanDef TdmTdm151[]= {
NO_LOOPBACK_LIST_START("TT151.  TDM to TDM channel.   AGC"),
{   {0,  0,   pcmToPcm },  LFT_TDM, RGT_TDM, NO_PKT_5ms, AGC_TESTS },
LIST_END
};

#define TP_AGC_SETUP(E1,F1,ID)  \
CROSSOVER_LIST_START(ID),                                                                          \
{   {0,  0,   pcmToPacket},       LFT_TDM, NULL_TDM, { E1,  F1,  E1,  F1,  E1  }, AGC_TESTS }, \
{   {1,  0,   pcmToPacket},       RGT_TDM, NULL_TDM, { E1,  F1,  E1,  F1,  E1  }, ALL_DISABLES },  \
LIST_END
chanDef TdmPkt151[] = {  TP_AGC_SETUP (G729AB,  Frame_20ms,   "TP151. G729AB:20ms   AGC") };
chanDef TdmPkt152[] = {  TP_AGC_SETUP (L16,     Frame_20ms,   "TP152. L16:20ms      AGC") };
chanDef TdmPkt153[] = {  TP_AGC_SETUP (L16_WB,  Frame_10ms,   "TP153. L16_WB:10ms   AGC") };

#define TT_AGC_SETUP(E1,F1,E2,F2,ID)  \
DAISY_CHAIN_LIST_START(ID),                                                                        \
{   {0,  0,   pcmToPacket},       LFT_TDM, NULL_TDM, { E1,  F1,  E1,  F1,  E1  }, ALL_DISABLES },  \
{   {1,  0,   packetToPacket},   NULL_TDM, NULL_TDM, { E1,  F1,  E2,  F2,  E2  }, AGC_TESTS }, \
{   {3,  0,   pcmToPacket},       RGT_TDM, NULL_TDM, { E2,  F2,  E2,  F2,  E2  }, ALL_DISABLES },  \
LIST_END
chanDef PktPkt151[] = {  TT_AGC_SETUP (L16,    Frame_5ms,    PCMU_64, Frame_5ms,    "PP151. L16:5ms<->PCMU:5ms  AGC")  };
chanDef PktPkt152[] = {  TT_AGC_SETUP (L16,    Frame_10ms,   PCMU_64, Frame_5ms,    "PP152. L16:10ms<->PCMU:5ms AGC")  };
chanDef PktPkt153[] = {  TT_AGC_SETUP (L16_WB, Frame_5ms,    PCMU_WB, Frame_5ms,    "PP153. L16WB:5ms<->PCMU_WB:5ms  AGC")  };
chanDef PktPkt154[] = {  TT_AGC_SETUP (L16_WB, Frame_5ms,    PCMU_WB, Frame_10ms,   "PP154. L16WB:5ms<->PCMU_WB:10ms AGC")  };
chanDef PktPkt155[] = {  TT_AGC_SETUP (L16,    Frame_5ms,    PCMU_WB, Frame_5ms,    "PP155. L16:5ms<->PCMU_WB:5ms    AGC")  };
chanDef PktPkt156[] = {  TT_AGC_SETUP (L16,    Frame_20ms,   PCMA_WB, Frame_10ms,   "PP156. L16:20ms<->PCMA_WB:10ms  AGC")  };

#define AGC_CNFR_SETUP(CNFR,RATEC,E1,RATE1,ID)                                                       \
CROSSOVER_CNFR_START(ID, CNFR, RATEC),                                                                   \
{   {0,  CNFR,   conferencePacket},  NULL_TDM, NULL_TDM, {  E1, RATE1, E1, RATE1 }, AGC_TESTS },  \
{   {1,  CNFR,   pcmToPacket},       RGT_TDM,  NULL_TDM, {  E1, RATE1, E1, RATE1 }, AGC_TESTS },  \
{   {3,  CNFR,   conferencePcm},     LFT_TDM,  NULL_TDM, { NullCodec, RATEC },      ALL_DISABLES  },     \
LIST_END
chanDef ToneCfr151[] = { AGC_CNFR_SETUP (0, Frame_10ms,   L16,    Frame_10ms,   "TC151.  TDM + L16:10ms     10ms conference  AGC") };
chanDef ToneCfr152[] = { AGC_CNFR_SETUP (1, Frame_10ms,   L16_WB, Frame_10ms,   "TC152.  TDM + L16WB:10ms   10ms conference  AGC") };
chanDef ToneCfr153[] = { AGC_CNFR_SETUP (2, Frame_10ms,   G729AB, Frame_10ms,   "TC153.  TDM + G729:10ms    10ms conference  AGC") };
chanDef ToneCfr154[] = { AGC_CNFR_SETUP (0, Frame_20ms,   L16,    Frame_10ms,   "TC154.  TDM + L16:10ms     20ms conference  AGC") };


//}

//{ Echo canceller test channels
chanDef TdmTdm201[]= {
NO_LOOPBACK_LIST_START("TT201.  TDM to TDM channel.   AEC"),
{   {0,  0,   pcmToPcm },  LFT_TDM, RGT_TDM, NO_PKT_10ms, AEC_TESTS },
LIST_END
};
chanDef TdmTdm202[]= {
NO_LOOPBACK_LIST_START("TT202.  TDM to TDM channel.   AEC"),
{   {0,  0,   pcmToPcm },  LFT_TDM, RGT_TDM, NO_PKT_5ms, AEC_TESTS },
LIST_END
};
chanDef TdmTdm203[]= {
NO_LOOPBACK_LIST_START("TT203.  TDM to TDM channel.   PEC"),
{   {0,  0,   pcmToPcm },  LFT_TDM, RGT_TDM, NO_PKT_5ms, PEC_TESTS },
LIST_END
};

#define TP_EC_SETUP(EC_TEST,E1,F1,ID)  \
CROSSOVER_LIST_START(ID),                                                                          \
{   {0,  0,   pcmToPacket},       LFT_TDM, NULL_TDM, { E1,  F1,  E1,  F1,  E1  }, EC_TEST      },  \
{   {1,  0,   pcmToPacket},       RGT_TDM, NULL_TDM, { E1,  F1,  E1,  F1,  E1  }, ALL_DISABLES },  \
LIST_END
chanDef TdmPkt201[] = {  TP_EC_SETUP (AEC_TESTS, L16,     Frame_20ms,   "TP201. L16:20ms      AEC") };
chanDef TdmPkt202[] = {  TP_EC_SETUP (AEC_TESTS, L16_WB,  Frame_10ms,   "TP202. L16_WB:10ms   AEC") };
chanDef TdmPkt203[] = {  TP_EC_SETUP (PEC_TESTS, L16,     Frame_20ms,   "TP203. L16:20ms      PEC") };
chanDef TdmPkt204[] = {  TP_EC_SETUP (PEC_TESTS, L16_WB,  Frame_10ms,   "TP204. L16_WB:10ms   PEC") };
chanDef TdmPkt205[] = {  TP_EC_SETUP (NEC_TESTS, G729AB,  Frame_20ms,   "TP205. G729AB:20ms   NEC") };
chanDef TdmPkt206[] = {  TP_EC_SETUP (NEC_TESTS, L16_WB,  Frame_10ms,   "TP206. L16_WB:10ms   NEC") };

#define TT_EC_SETUP(EC_TEST,E1,F1,E2,F2,ID)  \
DAISY_CHAIN_LIST_START(ID),                                                                        \
{   {0,  0,   pcmToPacket},       LFT_TDM, NULL_TDM, { E1,  F1,  E1,  F1,  E1  }, ALL_DISABLES },  \
{   {1,  0,   packetToPacket},   NULL_TDM, NULL_TDM, { E1,  F1,  E2,  F2,  E2  }, EC_TEST      },  \
{   {3,  0,   pcmToPacket},       RGT_TDM, NULL_TDM, { E2,  F2,  E2,  F2,  E2  }, ALL_DISABLES },  \
LIST_END
chanDef PktPkt201[] = {  TT_EC_SETUP (NEC_TESTS, L16,    Frame_5ms,    PCMU_64, Frame_5ms,    "PP201. L16:5ms<->PCMU:5ms        NEC")  };
chanDef PktPkt202[] = {  TT_EC_SETUP (NEC_TESTS, L16,    Frame_10ms,   PCMU_64, Frame_5ms,    "PP202. L16:10ms<->PCMU:5ms       NEC")  };
chanDef PktPkt203[] = {  TT_EC_SETUP (NEC_TESTS, L16_WB, Frame_10ms,   PCMU_WB, Frame_10ms,   "PP203. L16WB:10ms<->PCMU_WB:10ms NEC")  };
chanDef PktPkt204[] = {  TT_EC_SETUP (NEC_TESTS, L16_WB, Frame_5ms,    PCMU_WB, Frame_10ms,   "PP204. L16WB:5ms<->PCMU_WB:10ms  NEC")  };
chanDef PktPkt205[] = {  TT_EC_SETUP (NEC_TESTS, L16,    Frame_20ms,   PCMU_WB, Frame_20ms,   "PP205. L16:20ms<->PCMU_WB:20ms   NEC")  };
chanDef PktPkt206[] = {  TT_EC_SETUP (NEC_TESTS, L16,    Frame_10ms,   PCMA_WB, Frame_20ms,   "PP206. L16:10ms<->PCMA_WB:20ms   NEC")  };

#define EC_PCM_CNFR_SETUP(EC_TEST,CNFR,RATEC,E1,RATE1,ID)                                            \
CROSSOVER_CNFR_START(ID, CNFR, RATEC),                                                               \
{   {0,  CNFR,   conferencePcm},     LFT_TDM,  NULL_TDM, { NullCodec, RATEC },      EC_TEST      },  \
{   {2,  CNFR,   conferencePacket},  NULL_TDM, NULL_TDM, {  E1, RATE1, E1, RATE1 }, ALL_DISABLES },  \
{   {3,  CNFR,   pcmToPacket},       RGT_TDM,  NULL_TDM, {  E1, RATE1, E1, RATE1 }, ALL_DISABLES },  \
LIST_END
chanDef Cfr201[] = { EC_PCM_CNFR_SETUP (AEC_TESTS, 0, Frame_10ms,   L16,    Frame_10ms,   "C201.  TDM + L16:10ms     10ms conference  AEC") };
chanDef Cfr202[] = { EC_PCM_CNFR_SETUP (PEC_TESTS, 0, Frame_10ms,   L16_WB, Frame_10ms,   "C202.  TDM + L16_WB:10ms  10ms conference  PEC") };
chanDef Cfr203[] = { EC_PCM_CNFR_SETUP (AEC_TESTS, 0, Frame_10ms,   L16,    Frame_20ms,   "C203.  TDM + L16:20ms     10ms conference  AEC") };

#define EC_PKT_CNFR_SETUP(CNFR,RATEC,E1,RATE1,ID)                                            \
CROSSOVER_CNFR_START(ID, CNFR, RATEC),                                                      \
{   {0,  CNFR,   conferencePcm},     LFT_TDM,  NULL_TDM, { NullCodec, RATEC },      ALL_DISABLES },  \
{   {2,  CNFR,   conferencePacket},  NULL_TDM, NULL_TDM, {  E1, RATE1, E1, RATE1 }, NEC_TESTS    },  \
{   {3,  CNFR,   pcmToPacket},       RGT_TDM,  NULL_TDM, {  E1, RATE1, E1, RATE1 }, ALL_DISABLES },  \
LIST_END
chanDef Cfr204[] = { EC_PKT_CNFR_SETUP (0, Frame_10ms,   L16,    Frame_10ms,   "C204.  TDM + L16:10ms     10ms conference  NEC") };
chanDef Cfr205[] = { EC_PKT_CNFR_SETUP (1, Frame_20ms,   L16_WB, Frame_20ms,   "C205.  TDM + L16WB:20ms   20ms conference  NEC") };
chanDef Cfr206[] = { EC_PKT_CNFR_SETUP (2, Frame_20ms,   L16,    Frame_10ms,   "C206.  TDM + L16:10ms     20ms conference  NEC") };
chanDef Cfr207[] = { EC_PKT_CNFR_SETUP (0, Frame_10ms,   L16_WB, Frame_20ms,   "C207.  TDM + L16_WB:20ms  10ms conference  NEC") };
//}

//{ MIPs loading tests
#define DAISY_CHAIN_SETUP(C1,F1,C2,F2,ID)  \
DAISY_CHAIN_LIST_START(ID),                                                                   \
{  { 0,  0,    pcmToPacket},     LFT_TDM,  NULL_TDM, {  C1, F1, C1, F1, C1  }, ALL_DISABLES  }, \
{  { 1, PRMPT, packetToPacket},  NULL_TDM, NULL_TDM, {  C1, F1, C2, F2, C1  }, ALL_DISABLES  }, \
{  { 0,  0,    pcmToPacket},     RGT_TDM,  NULL_TDM, {  C1, F1, C1, F1, C1  }, ALL_DISABLES  }, \
LIST_END

chanDef DC1[] = { DAISY_CHAIN_SETUP (L16_WB, Frame_5ms,  PCMU_64, Frame_10ms, "DC1 L16 WB/5  <-> PCMU  * ?") };
chanDef DC2[] = { DAISY_CHAIN_SETUP (L16_WB, Frame_5ms,  G729AB,  Frame_20ms, "DC2 L16 WB/5  <-> G729AB/20 * ?") };
chanDef DC3[] = { DAISY_CHAIN_SETUP (L16_WB, Frame_10ms, G729AB,  Frame_30ms, "DC3 L16 WB/10 <-> G729AB/30 * ?") };
chanDef DC4[] = { DAISY_CHAIN_SETUP (L16_WB, Frame_5ms,  PCMU_64, Frame_5ms,  "DC4 L16 WB/5  <-> PCMU/5  * ?") };

#define CNFR_LOADING_SETUP(CNFR,C1,F1,C2,F2,ID) \
CROSSOVER_CNFR_START(ID,CNFR,F1),                                                                          \
{ {0,  CNFR,  conferencePcm},     LFT_TDM,  NULL_TDM, {  C1,  F1, C1, F1, C1  }, ALL_DISABLES  }, \
{ {2,  CNFR,  conferencePacket},  NULL_TDM, NULL_TDM, {  C1,  F1, C1, F1, C1  }, ALL_DISABLES  }, \
{ {3,  CNFR,  packetToPacket},    NULL_TDM, NULL_TDM, {  C1,  F1, C1, F1, C1  }, ALL_DISABLES  }, \
{ {6,  CNFR,  conferencePacket},  NULL_TDM, NULL_TDM, {  C1,  F1, C1, F1, C1  }, ALL_DISABLES  }, \
{ {7, PRMPT,  packetToPacket},    NULL_TDM, NULL_TDM, {  C1,  F1, C2, F2, C1  }, ALL_DISABLES  }, \
{ {0,  0,     pcmToPacket},       RGT_TDM,  NULL_TDM, {  C1,  F1, C1, F1, C1  }, ALL_DISABLES  }, \
LIST_END
chanDef DC_CF1[]= { CNFR_LOADING_SETUP (0, L16_WB, Frame_5ms, G729AB, Frame_5ms,    "DC_CF1     5ms:   L16WB  G729AB") };
chanDef DC_CF2[]= { CNFR_LOADING_SETUP (1, L16_WB, Frame_5ms, G729AB, Frame_10ms,   "DC_CF2.   10ms:   L16WB  G729AB") };
chanDef DC_CF3[]= { CNFR_LOADING_SETUP (2, L16_WB, Frame_5ms, MELP_E, Frame_22_5ms, "DC_CF3. 22.5ms:   L16WB  MELP_E") };
//}

//{  Background transcoding tests
#define BG_TRANSCODING_SETUP(C1,C2,RATE,ID) \
{ { DisableTest,  NULL_CNFR,   inactive, ID, RATE },  NULL_TDM, NULL_TDM, NO_PKT_5ms, ALL_DISABLES }, \
{ {0,  0,  backgroundTranscoder},  NULL_TDM, NULL_TDM, { C1, RATE, C2, RATE, C1 }, ALL_DISABLES  }, \
{ {1,  0,  backgroundTranscoder},  NULL_TDM, NULL_TDM, { C2, RATE, C1, RATE, C1 }, ALL_DISABLES  }, \
LIST_END
chanDef BG_TC1[]= { BG_TRANSCODING_SETUP (L16, PCMU_64, Frame_5ms,  "BG_TC1   5ms L16 G711u") };
chanDef BG_TC2[]= { BG_TRANSCODING_SETUP (L16, G722,    Frame_5ms,  "BG_TC2   5ms L16 G722") };
chanDef BG_TC3[]= { BG_TRANSCODING_SETUP (L16, G722,    Frame_10ms, "BG_TC3  10ms L16 G722") };
chanDef BG_TC4[]= { BG_TRANSCODING_SETUP (L16, G722,    Frame_30ms, "BG_TC4  30ms L16 G722") };
chanDef BG_TC5[]= { BG_TRANSCODING_SETUP (L16, G729AB,  Frame_10ms, "BG_TC5  10ms L16 G729AB") };
chanDef BG_TC6[]= { BG_TRANSCODING_SETUP (L16, G729AB,  Frame_20ms, "BG_TC6  20ms L16 G729AB") };
chanDef BG_TC7[]= { BG_TRANSCODING_SETUP (L16, G723_63, Frame_10ms, "BG_TC7  10ms L16 G723") };
//}
//}

void toneAlgControl(ADT_UInt32 DspId) {
ADT_UInt16 chanId;
GpakToneTypes deact, act;
GPAK_AlgControlStat_t dspStat;
gpakAlgControlStat_t  apiStat;
int deactSq, actSq;

    deactSq = actSq = 0;
    chanId  = ADT_getint ("\nChannel ");
    deact   = ADT_getint ("\nDeact Mask ");
    act     = ADT_getint ("\nAct Mask ");

   if (deact & Tone_Squelch) {
        deactSq = 1;
        deact &= ~Tone_Squelch;
   }

   if (act & Tone_Squelch) {
        actSq = 1;
        act &= ~Tone_Squelch;
   }

   if (deactSq) {
        apiStat = gpakAlgControl (DspId, chanId, BypassToneSuppress, 0, 0, ADevice, 0, 0, &dspStat);
        if (apiStat !=  GpakApiSuccess || dspStat != Cc_Success) {
            ADT_printf ("Channel %3d AlgCtrl Failure %d:%d:%d\n\n", chanId, apiStat, dspStat, DSPError [DspId]);
        }
   }

   if (actSq) {
        apiStat = gpakAlgControl (DspId, chanId, EnableToneSuppress, 0, 0, ADevice, 0, 0, &dspStat);
        if (apiStat !=  GpakApiSuccess || dspStat != Cc_Success) {
            ADT_printf ("Channel %3d AlgCtrl Failure %d:%d:%d\n\n", chanId, apiStat, dspStat, DSPError [DspId]);
        }
   }

   if (deact || act) {
        apiStat = gpakAlgControl (DspId, chanId, ModifyToneDetector, deact, act, ADevice, 0, 0, &dspStat);
        if (apiStat !=  GpakApiSuccess || dspStat != Cc_Success) {
            ADT_printf ("Channel %3d AlgCtrl Failure %d:%d:%d\n\n", chanId, apiStat, dspStat, DSPError [DspId]);
        }
   }

}
//-----------------------------------------------
//{ Channel setup routines using chanDef tables
void setTDMTDMCfg  (GpakChannelConfig_t *chCfgData, chanDef *chDef, GpakToneTypes tone) {

   PcmPcmCfg_t *PcmPcm = &chCfgData->PcmPcm;

   memset (PcmPcm, 0, sizeof (PcmPcmCfg_t));

   PcmPcm->AECEcanEnableA = Disabled;
   PcmPcm->AECEcanEnableB = Disabled;
   PcmPcm->EcanEnableA    = Disabled;
   PcmPcm->EcanEnableB    = Disabled;
   if ((chDef->Components.EC == AEC) && (sysCfg.AECInstances != 0)) {
      PcmPcm->AECEcanEnableA = Enabled;
   } else if ((chDef->Components.EC == PCM_EC) && (sysCfg.CfgEnableFlags & PCMECEnableBit)) {
      PcmPcm->EcanEnableA    = Enabled;
      PcmPcm->EcanEnableB    = Enabled;
   }

   PcmPcm->AgcEnableA = Disabled;
   PcmPcm->AgcEnableB = Disabled;
   if (chDef->Components.AGC && (sysCfg.CfgEnableFlags & AGCEnableBit)) {
      PcmPcm->AgcEnableA = Enabled;
      PcmPcm->AgcEnableB = Enabled;
   }

   PcmPcm->InPortA    = chDef->TDMA.port;
   PcmPcm->OutPortA   = chDef->TDMA.port;
   PcmPcm->InSlotA    = *chDef->TDMA.inSlot;
   PcmPcm->OutSlotA   = *chDef->TDMA.outSlot;
   PcmPcm->InPortB    = chDef->TDMB.port;
   PcmPcm->OutPortB   = chDef->TDMB.port;
   PcmPcm->InSlotB    = *chDef->TDMB.inSlot;
   PcmPcm->OutSlotB   = *chDef->TDMB.outSlot;

   PcmPcm->NoiseSuppressA = Disabled;
   PcmPcm->CIdModeA       = CidDisabled;
   PcmPcm->ToneTypesA     = tone;
   PcmPcm->ToneGenGainG1A = 0;
   PcmPcm->OutputGainG2A  = 0;
   PcmPcm->InputGainG3A   = 0;

   PcmPcm->NoiseSuppressB = Disabled;
   PcmPcm->CIdModeB       = CidDisabled;
   PcmPcm->ToneTypesB     = tone;
   PcmPcm->ToneGenGainG1B = 0;
   PcmPcm->OutputGainG2B  = 0;
   PcmPcm->InputGainG3B   = 0;

   PcmPcm->FrameHalfMS   = chDef->Pkt.decodeHalfMS;
   PcmPcm->Coding        = GpakVoiceChannel;

   ADT_printf ("Chan: %3d  TDM[%d.%d.%dms]_TDM[%d.%d.%dms]\n", chDef->ID.Chan,
            PcmPcm->InPortA,  PcmPcm->InSlotA,  PcmPcm->FrameHalfMS/2,
            PcmPcm->OutPortA, PcmPcm->OutSlotA, PcmPcm->FrameHalfMS/2);
   ADT_printf ("           TDM[%d.%d.%dms]_TDM[%d.%d.%dms]\n",
            PcmPcm->InPortB,  PcmPcm->InSlotB,  PcmPcm->FrameHalfMS/2,
            PcmPcm->OutPortB, PcmPcm->OutSlotB, PcmPcm->FrameHalfMS/2);
}
void setTDMPktCfg  (GpakChannelConfig_t *chCfgData, chanDef *chDef, GpakToneTypes tone) {

   PcmPktCfg_t *PcmPkt = &chCfgData->PcmPkt;

   memset (PcmPkt, 0, sizeof (PcmPktCfg_t));

   PcmPkt->AECEcanEnable = Disabled;
   PcmPkt->PcmEcanEnable = Disabled;
   PcmPkt->PktEcanEnable = Disabled;
   if ((chDef->Components.EC == AEC) && (sysCfg.AECInstances != 0)) {
      PcmPkt->AECEcanEnable = Enabled;
   } else if ((chDef->Components.EC == PCM_EC) && (sysCfg.CfgEnableFlags & PCMECEnableBit)) {
      PcmPkt->PcmEcanEnable = Enabled;
   } else if ((chDef->Components.EC == PKT_EC) && (sysCfg.CfgEnableFlags & PKTECEnableBit)) {
      PcmPkt->PktEcanEnable = Enabled;
   }

   PcmPkt->AgcEnable = Disabled;
   if (chDef->Components.AGC && (sysCfg.CfgEnableFlags & AGCEnableBit)) {
      PcmPkt->AgcEnable = Enabled;
   }

   PcmPkt->VadEnable = Disabled;
   if (chDef->Components.VAD && (sysCfg.CfgEnableFlags & VadEnableBit)) {
      PcmPkt->VadEnable = Enabled;
   }

   PcmPkt->PcmInPort    = chDef->TDMA.port;
   PcmPkt->PcmOutPort   = chDef->TDMA.port;
   PcmPkt->PcmInSlot    = *chDef->TDMA.inSlot;
   PcmPkt->PcmOutSlot   = *chDef->TDMA.outSlot;

   PcmPkt->PktInCoding       = chDef->Pkt.decode;
   PcmPkt->PktInFrameHalfMS  = getCodecHalfMS (chDef->Pkt.decode, chDef->Pkt.decodeHalfMS);
   PcmPkt->PktOutCoding      = chDef->Pkt.encode;
   PcmPkt->PktOutFrameHalfMS = getCodecHalfMS (chDef->Pkt.encode, chDef->Pkt.encodeHalfMS);
   PcmPkt->CIdMode       = CidDisabled;
   PcmPkt->FaxMode       = disabled;
   PcmPkt->FaxTransport  = faxUdptl;
   PcmPkt->Coding        = GpakVoiceChannel;
   PcmPkt->ToneTypes     = tone;

   PcmPkt->ToneGenGainG1 = 0;
   PcmPkt->OutputGainG2  = 0;
   PcmPkt->InputGainG3   = 0;
   daisyChainTerminator = chDef->ID.Chan;

   ADT_printf ("Chan: %3d  TDM[%d.%d.%dms]_PKT[%2d.%dms:%s]\n", chDef->ID.Chan,
            PcmPkt->PcmInPort,    PcmPkt->PcmInSlot,       PcmPkt->PktOutFrameHalfMS/2, 
            PcmPkt->PktOutCoding, PcmPkt->PktOutFrameHalfMS/2, txIPFromChan(chDef->ID.Chan));
   ADT_printf ("           PKT[%2d.%dms:%s]_TDM[%d.%d.%dms]\n", 
            PcmPkt->PktInCoding, PcmPkt->PktInFrameHalfMS/2, rxIPFromChan((ADT_UInt16) (chDef->ID.Chan+1)),
            PcmPkt->PcmOutPort,  PcmPkt->PcmOutSlot, PcmPkt->PktInFrameHalfMS/2);

}
void setPktPktCfg  (GpakChannelConfig_t *chCfgData, chanDef *chDef, GpakToneTypes tone) {

   PktPktCfg_t *PktPkt = &chCfgData->PktPkt;

   memset (PktPkt, 0, sizeof (PktPktCfg_t));

   PktPkt->EcanEnableA = Disabled;
   PktPkt->EcanEnableB = Disabled;
   if ((chDef->Components.EC == PKT_EC) && (sysCfg.CfgEnableFlags & PKTECEnableBit)) {
      PktPkt->EcanEnableA = Enabled;
      PktPkt->EcanEnableB = Enabled;
   }

   PktPkt->AgcEnableA = Disabled;
   PktPkt->AgcEnableB = Disabled;
   if (chDef->Components.AGC && (sysCfg.CfgEnableFlags & AGCEnableBit)) {
      PktPkt->AgcEnableA = Enabled;
      PktPkt->AgcEnableB = Enabled;
   }

   PktPkt->VadEnableA = Disabled;
   PktPkt->VadEnableB = Disabled;
   if (chDef->Components.VAD && (sysCfg.CfgEnableFlags & VadEnableBit)) {
      PktPkt->VadEnableA = Enabled;
      PktPkt->VadEnableB = Enabled;
   }


   PktPkt->PktInCodingA       = chDef->Pkt.decode;
   PktPkt->PktInFrameHalfMSA  = getCodecHalfMS (chDef->Pkt.decode, chDef->Pkt.decodeHalfMS);
   PktPkt->PktOutCodingA      = chDef->Pkt.decode;
   PktPkt->PktOutFrameHalfMSA = getCodecHalfMS (chDef->Pkt.decode, chDef->Pkt.decodeHalfMS);
   PktPkt->ToneTypesA         = tone & (~Tone_Generate);

   PktPkt->ChannelIdB         = chDef->ID.Chan + 1;
   PktPkt->PktInCodingB       = chDef->Pkt.encode;
   PktPkt->PktInFrameHalfMSB  = getCodecHalfMS (chDef->Pkt.encode, chDef->Pkt.encodeHalfMS);
   PktPkt->PktOutCodingB      = chDef->Pkt.encode;
   PktPkt->PktOutFrameHalfMSB = getCodecHalfMS (chDef->Pkt.encode, chDef->Pkt.encodeHalfMS);
   PktPkt->ToneTypesB         = tone & (~Tone_Generate);

   // Special daisy chain case to allow half duplex operation
   if (chDef->Pkt.decode == NullCodec) {
      PktPkt->PktInCodingA  = chDef->Pkt.encode;
      PktPkt->PktOutCodingA = chDef->Pkt.encode;
   }

   if (chDef->Pkt.encode == NullCodec) {
      PktPkt->PktOutCodingA = chDef->Pkt.decode;
      PktPkt->PktInCodingB  = chDef->Pkt.decode;

      PktPkt->PktInCodingA  = NullCodec;
      PktPkt->PktOutCodingB = NullCodec;
   }

   ADT_printf ("Chan: %3d  --> PKT[%3d.%2dms]                 \\/  PKT[%3d.%2dms]   <-- %3d\n",
          chDef->ID.Chan, PktPkt->PktInCodingA,  PktPkt->PktInFrameHalfMSA/2,  
                          PktPkt->PktInCodingB,  PktPkt->PktInFrameHalfMSB/2,  chDef->ID.Chan+1);
   ADT_printf ("           <-- PKT[%3d.%2dms:%s] /\\  PKT[%3d.%2dms:%s]\n",
                     PktPkt->PktOutCodingA, PktPkt->PktOutFrameHalfMSA/2, rxIPFromChan (chDef->ID.Chan),
                     PktPkt->PktOutCodingB, PktPkt->PktOutFrameHalfMSB/2, txIPFromChan ((ADT_UInt16) (chDef->ID.Chan+1))  );
}
void setTDMCnfrCfg (GpakChannelConfig_t *chCfgData, chanDef *chDef, GpakToneTypes tone) {
   CnfPcmCfg_t *PcmCnf = &chCfgData->ConferPcm;
   int cnfrMs;

   cnfrMs = ActiveCnfr[chDef->ID.Cnfr].cnfrHalfMS/2;

   memset (PcmCnf, 0, sizeof (CnfPcmCfg_t));

   PcmCnf->ConferenceId = chDef->ID.Cnfr;

   PcmCnf->PcmInPort    = chDef->TDMA.port;
   PcmCnf->PcmOutPort   = chDef->TDMA.port;
   PcmCnf->PcmInSlot    = *chDef->TDMA.inSlot;
   PcmCnf->PcmOutSlot   = *chDef->TDMA.outSlot;

   PcmCnf->AECEcanEnable = Disabled;
   PcmCnf->EcanEnable = Disabled;
   if ((chDef->Components.EC == AEC) && (sysCfg.AECInstances != 0)) {
      PcmCnf->AECEcanEnable = Enabled;
   } else if ((chDef->Components.EC == PCM_EC) && (sysCfg.CfgEnableFlags & PCMECEnableBit)) {
      PcmCnf->EcanEnable = Enabled;
   }

   PcmCnf->AgcInEnable   = Disabled;
   PcmCnf->AgcOutEnable  = Disabled;
   if (chDef->Components.AGC && (sysCfg.CfgEnableFlags & AGCEnableBit)) {
      PcmCnf->AgcInEnable   = Enabled;
      PcmCnf->AgcOutEnable  = Enabled;
   }

   PcmCnf->ToneTypes     = tone;

   PcmCnf->ToneGenGainG1 = 0;
   PcmCnf->OutputGainG2  = 0;
   PcmCnf->InputGainG3   = 0;

   ADT_printf ("Chan: %3d  TDM[%d.%d]_CNF[%d.%dms]\n", chDef->ID.Chan,
            PcmCnf->PcmInPort,  PcmCnf->PcmInSlot, 
            PcmCnf->ConferenceId, cnfrMs);
   ADT_printf ("           CNF[%d.%dms]_TDM[%d.%d]\n", 
            PcmCnf->ConferenceId, cnfrMs,
            PcmCnf->PcmOutPort, PcmCnf->PcmOutSlot);

}
void setPktCnfrCfg (GpakChannelConfig_t *chCfgData, chanDef *chDef, GpakToneTypes tone) {

   CnfPktCfg_t *CnfPkt = &chCfgData->ConferPkt;
   int cnfrMs;

   cnfrMs = ActiveCnfr[chDef->ID.Cnfr].cnfrHalfMS/2;
   memset (CnfPkt, 0, sizeof (CnfPktCfg_t));

   CnfPkt->EcanEnable = Disabled;
   if ((chDef->Components.EC == PKT_EC) && (sysCfg.CfgEnableFlags & PKTECEnableBit)) {
      CnfPkt->EcanEnable = Enabled;
   }

   CnfPkt->AgcInEnable  = Disabled;
   CnfPkt->AgcOutEnable = Disabled;
   if (chDef->Components.AGC && (sysCfg.CfgEnableFlags & AGCEnableBit)) {
      CnfPkt->AgcInEnable  = Enabled;
      CnfPkt->AgcOutEnable = Enabled;
   }

   CnfPkt->VadEnable = Disabled;
   if (chDef->Components.VAD && (sysCfg.CfgEnableFlags & VadEnableBit)) {
      CnfPkt->VadEnable = Enabled;
   }

   CnfPkt->ConferenceId = chDef->ID.Cnfr;
   CnfPkt->PktInCoding  = chDef->Pkt.decode;
   CnfPkt->PktOutCoding = chDef->Pkt.encode;
   CnfPkt->ToneTypes    = tone;
   CnfPkt->PktFrameHalfMS = getCodecHalfMS (chDef->Pkt.decode, chDef->Pkt.encodeHalfMS);

   CnfPkt->ToneGenGainG1 = 0;
   CnfPkt->OutputGainG2  = 0;
   CnfPkt->InputGainG3   = 0;

   ADT_printf ("Chan: %3d  PKT[%2d.%dms:%s]_CNF[%d.%dms]\n", chDef->ID.Chan,
            CnfPkt->PktInCoding,  CnfPkt->PktFrameHalfMS/2, rxIPFromChan (chDef->ID.Chan),
            CnfPkt->ConferenceId, cnfrMs);

   ADT_printf ("           CNF[%d.%dms]_PKT[%2d.%dms:%s]\n", 
            CnfPkt->ConferenceId, cnfrMs,
            CnfPkt->PktOutCoding, CnfPkt->PktFrameHalfMS/2, txIPFromChan (chDef->ID.Chan));
}
void setCmpConfCfg (GpakChannelConfig_t *chCfgData, chanDef *chDef) {

   CnfCmpCfg_t *CnfCmp = &chCfgData->ConferComp;
   int cnfrMs;

   cnfrMs = ActiveCnfr[chDef->ID.Cnfr].cnfrHalfMS/2;

   memset (CnfCmp, 0, sizeof (CnfCmpCfg_t));

   CnfCmp->VadEnable = Disabled;
   if (chDef->Components.VAD && (sysCfg.CfgEnableFlags & VadEnableBit)) {
      CnfCmp->VadEnable = Enabled;
   }

   CnfCmp->ConferenceId     = chDef->ID.Cnfr;
   CnfCmp->PcmOutPort       = SerialPortNull;
   CnfCmp->PcmOutSlot       = 0;
   CnfCmp->PcmOutPin        = 0;
   CnfCmp->PktOutCoding     = chDef->Pkt.encode;

   ADT_printf ("           CNF[%d.%dms]_CMP[%2d:%s]\n", 
            CnfCmp->ConferenceId, cnfrMs,
            CnfCmp->PktOutCoding, txIPFromChan (chDef->ID.Chan));

}
void setTranscodeCfg (GpakChannelConfig_t *chCfgData, chanDef *chDef) {
   LbCdrCgf_t *TransCode = &chCfgData->LpbkCoder;
   int frameMs;

   frameMs = getCodecHalfMS (chDef->Pkt.decode, chDef->Pkt.decodeHalfMS) / 2;
   memset (TransCode, 0, sizeof (LbCdrCgf_t));

   TransCode->PktFrameHalfMS   = chDef->Pkt.decodeHalfMS;
   TransCode->PktInCoding      = chDef->Pkt.decode;
   TransCode->PktOutCoding     = chDef->Pkt.encode;

   ADT_printf ("           TRN[%2d.%dms]_TRN[%2d:%dms]\n", 
            TransCode->PktInCoding,  frameMs,
            TransCode->PktOutCoding, frameMs);
}

void setupChannelsViaTable (ADT_UInt32 DspId, chanDef *chDef, GpakToneTypes tone) {

   // Channel configuration variables
   GpakApiStatus_t          ApiStat;
   GpakChannelConfig_t      chCfgData;
   GPAK_ChannelConfigStat_t chDspStat = Cc_Success;
   GpakCodecs encode, decode;

   ADT_UInt16 chanID, chnCnt, chanStart;
   GpakFrameHalfMS encodeHalfMS, decodeHalfMS;

   if (tone != (GpakToneTypes) 0xffff) dspTearDown (DspId);
   else tone = Null_tone;

   testSuite = chDef;
   for (; chDef->ID.Chan != ((ADT_UInt16) -1); chDef++) {
      chanID = chDef->ID.Chan;

      //  Setup channel configuration structure from table entry.
      switch (chDef->ID.ChanType) {
      case inactive:
         switch (chanID) {
         case PacketCrossOver:  setDSPCrossover  (DspId); break;
         case PacketDaisyChain: setDSPDaisyChain (DspId); break;
         case PacketLoopBack:   setDSPLoopBack   (DspId); break;
         default:               setDSPNoLoopBack (DspId); break;
         }
         if (chDef->ID.Cnfr == NULL_CNFR) continue;
         setupConference ( DspId, chDef->ID.Cnfr, chDef->ID.CnfrHalfMS, Null_tone);
         continue;

      case pcmToPcm: // Configure PCM to PCM channel
         if ((sysCfg.CfgFramesChans & PCM2PCM_CFG_BIT) == 0) {
            ADT_printf ("TDM-TDM channels are not configured\n");
            continue;
         }
         setTDMTDMCfg (&chCfgData, chDef, tone);
         break;

      case pcmToPacket:
         if ((sysCfg.CfgFramesChans & PCM2PKT_CFG_BIT) == 0) {
            ADT_printf ("TDM-Pkt channels are not configured\n");
            continue;
         }

         setTDMPktCfg (&chCfgData, chDef, tone);
         sendRTPConfigMsg (DspId, chanID, chDef->Pkt.encode, encodeHalfMS);
         if (daisyChainStart == ((ADT_UInt16) -1)) daisyChainTerminator = chanID;
         break;

      case packetToPacket:
         if ((sysCfg.CfgFramesChans & PKT2PKT_CFG_BIT) == 0) {
            ADT_printf ("Pkt-Pkt channels are not configured\n");
            continue;
         }
         chnCnt = chDef->ID.Cnfr;
         if (chnCnt == PRMPT) {

            chanDef *pcmPktDef;
            chnCnt = (ADT_UInt16) ADT_getint ("Pkt-Pkt channel count: ");
            chnCnt &= 0xfe;
            if (chnCnt == 0) return;
            pcmPktDef = chDef + 1;
            if (pcmPktDef->ID.ChanType != pcmToPacket) return;
            daisyChainStart = chanID;
            pcmPktDef->ID.Chan = chanID + (2 * chnCnt);
         } else if (chnCnt == 0) {
            chnCnt = 1;
         }

         // Alternate encode/decode
         chanStart = chanID;
         encode = chDef->Pkt.encode;
         encodeHalfMS = chDef->Pkt.encodeHalfMS;
         decode = chDef->Pkt.decode;
         decodeHalfMS = chDef->Pkt.decodeHalfMS;
         do {
            setPktPktCfg (&chCfgData, chDef, tone);
            // Special case for null codec in daisy chain
            if (chDef->Pkt.decode == NullCodec)
               sendRTPConfigMsg (DspId, chanID, chDef->Pkt.encode, encodeHalfMS);
            else
               sendRTPConfigMsg (DspId, chanID, chDef->Pkt.decode, decodeHalfMS);

            if (chDef->Pkt.encode == NullCodec)
               sendRTPConfigMsg (DspId, (ADT_UInt16) (chanID+1), chDef->Pkt.decode, decodeHalfMS);
            else
               sendRTPConfigMsg (DspId, (ADT_UInt16) (chanID+1), chDef->Pkt.encode, encodeHalfMS);

            ApiStat = gpakConfigureChannel (DspId, chanID, chDef->ID.ChanType, &chCfgData, &chDspStat);
            if (ApiStat !=  GpakApiSuccess || chDspStat != Cc_Success) {
               ADT_printf ("Channel %3d setup failure %d:%d:%d\n\n", chanID, ApiStat, chDspStat, DSPError [DspId]);
               break;
            } else {
               ADT_printf ("Channel %3d setup success\n\n", chanID);
              sysCfg.ActiveChannels += 2;
            }
            if (chnCnt & 1) {
               chDef->Pkt.encode = encode;
               chDef->Pkt.encodeHalfMS = encodeHalfMS;
               chDef->Pkt.decode = decode;
               chDef->Pkt.decodeHalfMS = decodeHalfMS;
            } else {
               chDef->Pkt.encode = decode;
               chDef->Pkt.encodeHalfMS = decodeHalfMS;
               chDef->Pkt.decode = encode;
               chDef->Pkt.decodeHalfMS = encodeHalfMS;
             }

            chanID += 2;
            chDef->ID.Chan = chanID;
            gpakHostDelay ();
         } while (--chnCnt);
         // Return orignal codecs
         chDef->ID.Chan = chanStart;
         chDef->Pkt.encode = encode;
         chDef->Pkt.encodeHalfMS = encodeHalfMS;
         chDef->Pkt.decode = decode;
         chDef->Pkt.decodeHalfMS = decodeHalfMS;
         continue;

      case conferencePcm:
         if ((sysCfg.CfgFramesChans & CONFPCM_CFG_BIT) == 0) {
            ADT_printf ("TDM conference channels are not configured\n");
            continue;
         }
         setTDMCnfrCfg (&chCfgData, chDef, tone);
         break;

      case conferencePacket:
         if ((sysCfg.CfgFramesChans & CONFPKT_CFG_BIT) == 0) {
            ADT_printf ("Packet conference channels are not configured\n");
            continue;
         }
         setPktCnfrCfg (&chCfgData, chDef, tone);
         sendRTPConfigMsg (DspId, chanID, chDef->Pkt.encode, encodeHalfMS);
         break;

      case conferenceComposite:
         if ((sysCfg.CfgFramesChans & CONFCOMP_CFG_BIT) == 0) {
            ADT_printf ("Conference composite channels are not configured\n");
         }
         setCmpConfCfg (&chCfgData, chDef);
         sendRTPConfigMsg (DspId, chanID, chDef->Pkt.encode, encodeHalfMS);
         break;

      case backgroundTranscoder:
         setTranscodeCfg (&chCfgData, chDef);
         break;
      }

      //  Issue API call to setup channels.
      ApiStat = gpakConfigureChannel (DspId, chanID, chDef->ID.ChanType, &chCfgData, &chDspStat);
      if (ApiStat !=  GpakApiSuccess || chDspStat != Cc_Success) {
         ADT_printf ("Channel %3d setup failure %d:%d:%d\n\n", chanID, ApiStat, chDspStat, DSPError [DspId]);
      } else {
         ADT_printf ("Channel %3d setup success\n\n", chanID);
         if (chDef->ID.ChanType == packetToPacket) sysCfg.ActiveChannels ++;
         sysCfg.ActiveChannels++;
      }
      gpakHostDelay ();
   }

}
//}

//------------------------------------------------------------------
//                     CANNED TESTS
//------------------------------------------------------------------
//------------------------------------------------------------------
char processTestKeys (ADT_UInt32 DspId, chanDef *chDef) {
   char key;
   ADT_printf ("<CR> for next, '%c' to teardown, 'q' to exit without teardown, '?' for more options\n", ESC);
   do {
      switch (key = ADT_getchar ()) {
      default: return key;
      case '-': gpakResetSystemStats (DspId, fullStatsReset); break;  // reset stats
      case 'c': sendTestMsg (DspId, "Chan capture", StartChanCapt);  break;
      case 'g': toneGenRequest (DspId);             break;

      case '+': ADT_printf ("%s\n", testSuite->ID.Test);  setupChannels (DspId); break;
      case 's': autoStats = !autoStats; cyclesSinceDisplay = cyclesPerDisplay - 2;  break; // Channel stats
      case 'r': autoRTP   = !autoRTP;   cyclesSinceDisplay = cyclesPerDisplay - 2;  break; // RTP stats
      case 'u': autoUsage = !autoUsage; cyclesSinceDisplay = cyclesPerDisplay - 2;  break; // CPU usage
      case 'v': displayChannelOpts (DspId); break;

      case 't': toneAlgControl(DspId);
      case 'q': return ESC;
      case 'Q': autoRTP = autoUsage = autoStats = ADT_FALSE; break;
      case ESC: dspTearDown (DspId);  return ESC;
      case '\r':
      case '\n':
      case 'n':
         if (chDef != NULL) dspTearDown (DspId);
         return ' ';

      case '?': 
         ADT_printf ("%s\n", testSuite->ID.Test);
         ADT_printf ("-: reset stats,   c: channel capture,  g: tone generation\n");
         ADT_printf ("r: toggle RTP,    s: toggle stats,     u: toggle usage\n");
         ADT_printf ("+: more channels, v: channel opts,     Q: auto off\n");
         ADT_printf ("t: tone Alg control\n");
         break;
      
      }
   } while (ADT_TRUE);
}

//------------------------------------------------------------------
//{ TdmTdm canned tests.
char tdmTdmTest (ADT_UInt32 DspId, chanDef *chDef) {

   ADT_printf ("\nTDM-TDM: %s\n", chDef->ID.Test);
   dspTearDown (DspId);

   setupChannelsViaTable (DspId, chDef, Null_tone);
   
   return processTestKeys(DspId, chDef);
}
void tdmTdmTests (ADT_UInt32 DspId) {
   if (tdmTdmTest (DspId, TdmTdm1) == ESC) return;
   if (tdmTdmTest (DspId, TdmTdm2) == ESC) return;
   if (tdmTdmTest (DspId, TdmTdm3) == ESC) return;
   if (tdmTdmTest (DspId, TdmTdm4) == ESC) return;
   if (tdmTdmTest (DspId, TdmTdm5) == ESC) return;
   dspTearDown (DspId);
   ADT_printf ("\nTDM to TDM test suite complete\n\n");
}
//}
//------------------------------------------------------------------
//{ TdmPkt canned tests.
char tdmPktTest (ADT_UInt32 DspId, chanDef *chDef) {

   ADT_printf ("\nTDM-Pkt: %s\n", chDef->ID.Test);
   dspTearDown (DspId);

   setupChannelsViaTable (DspId, chDef, Null_tone);
   
   return processTestKeys(DspId, chDef);
}
void tdmPktTests (ADT_UInt32 DspId) {
   if (tdmPktTest (DspId, TdmPkt1) == ESC) return;
   if (tdmPktTest (DspId, TdmPkt2) == ESC) return;
   if (tdmPktTest (DspId, TdmPkt3) == ESC) return;
   if (tdmPktTest (DspId, TdmPkt4) == ESC) return;
   if (tdmPktTest (DspId, TdmPkt5) == ESC) return;
   if (tdmPktTest (DspId, TdmPkt6) == ESC) return;
   if (tdmPktTest (DspId, TdmPkt7) == ESC) return;
   if (tdmPktTest (DspId, TdmPkt8) == ESC) return;
   if (tdmPktTest (DspId, TdmPkt9) == ESC) return;
   if (tdmPktTest (DspId, TdmPkt10) == ESC) return;
   if (tdmPktTest (DspId, TdmPkt11) == ESC) return;
   if (tdmPktTest (DspId, TdmPkt12) == ESC) return;
   if (tdmPktTest (DspId, TdmPkt13) == ESC) return;
   if (tdmPktTest (DspId, TdmPkt14) == ESC) return;
   if (tdmPktTest (DspId, TdmPkt15) == ESC) return;
   if (tdmPktTest (DspId, TdmPkt16) == ESC) return;
   if (tdmPktTest (DspId, TdmPkt17) == ESC) return;
   if (tdmPktTest (DspId, TdmPkt18) == ESC) return;
   if (tdmPktTest (DspId, TdmPkt19) == ESC) return;
   if (tdmPktTest (DspId, TdmPkt20) == ESC) return;
   dspTearDown (DspId);
   ADT_printf ("\nTDM to PKT test suite complete\n\n");
}
//}
//------------------------------------------------------------------
//{ PktPkt canned tests.
char pktPktTest (ADT_UInt32 DspId, chanDef *chDef) {

   ADT_printf ("\nPkt-Pkt: %s\n", chDef->ID.Test);
   dspTearDown (DspId);

   setupChannelsViaTable (DspId, chDef, Null_tone);

   return processTestKeys(DspId, chDef);
}
void pktPktTests (ADT_UInt32 DspId) {
   if (pktPktTest (DspId, PktPkt1) == ESC) return;
   if (pktPktTest (DspId, PktPkt2) == ESC) return;
   if (pktPktTest (DspId, PktPkt3) == ESC) return;
   if (pktPktTest (DspId, PktPkt4) == ESC) return;
   if (pktPktTest (DspId, PktPkt5) == ESC) return;
   if (pktPktTest (DspId, PktPkt6) == ESC) return;
   if (pktPktTest (DspId, PktPkt7) == ESC) return;
   if (pktPktTest (DspId, PktPkt8) == ESC) return;
   if (pktPktTest (DspId, PktPkt9) == ESC) return;
   if (pktPktTest (DspId, PktPkt10) == ESC) return;
   if (pktPktTest (DspId, PktPkt11) == ESC) return;
   if (pktPktTest (DspId, PktPkt12) == ESC) return;
   if (pktPktTest (DspId, PktPkt13) == ESC) return;
   if (pktPktTest (DspId, PktPkt14) == ESC) return;
   if (pktPktTest (DspId, PktPkt15) == ESC) return;
   if (pktPktTest (DspId, PktPkt16) == ESC) return;
   if (pktPktTest (DspId, PktPkt17) == ESC) return;
   if (pktPktTest (DspId, PktPkt18) == ESC) return;
   if (pktPktTest (DspId, PktPkt19) == ESC) return;
   if (pktPktTest (DspId, PktPkt20) == ESC) return;
   if (pktPktTest (DspId, PktPkt21) == ESC) return;
   if (pktPktTest (DspId, PktPkt22) == ESC) return;
   if (pktPktTest (DspId, PktPkt23) == ESC) return;
   if (pktPktTest (DspId, PktPkt24) == ESC) return;
   if (pktPktTest (DspId, PktPkt25) == ESC) return;
   if (pktPktTest (DspId, PktPkt26) == ESC) return;
   dspTearDown (DspId);
   ADT_printf ("\nPKT to PKT test suite complete\n\n");
}
//}
//------------------------------------------------------------------
//{ Conferencing canned tests.
char cnfrTest (ADT_UInt32 DspId, chanDef *chDef, ADT_Bool DTMFDetect) {

   ADT_printf ("\nConference %d tests: %s\n", chDef->ID.Cnfr, chDef->ID.Test);
   dspTearDown (DspId);

   if (setupConference (DspId, chDef->ID.Cnfr, chDef->ID.CnfrHalfMS, DTMFDetect) == ESC) return ESC;

   setupChannelsViaTable (DspId, chDef, Null_tone);

   //  Play third voice on pkt->pkt channel 1
   playbackVoice (DspId, THIRD_VOICE_BUFFER, 1, ADevice, StartContinuousPlayBack);

   return processTestKeys (DspId, chDef);
}
void cnfrTests (ADT_UInt32 DspId) {
   ADT_Int16 dominant = 3;

   ADT_UpdateInt16Value ("Max dominants", &dominant);
   setMaxDominant (DspId, dominant);
   if (recordVoice (DspId, "Record third voice", THIRD_VOICE_BUFFER, NULL_CHAN, ADevice, StartRecording, tdsNoToneDetected) == ESC) return;

   if (cnfrTest (DspId, Cnfr1, ADT_FALSE) == ESC) return;
   if (cnfrTest (DspId, Cnfr2, ADT_FALSE) == ESC) return;
   if (cnfrTest (DspId, Cnfr3, ADT_FALSE) == ESC) return;
   if (cnfrTest (DspId, Cnfr4, ADT_FALSE) == ESC) return;
   dspTearDown (DspId);
   ADT_printf ("\nConferencing test suite complete\n\n");
}
//}
//------------------------------------------------------------------
//{ Background transcoding canned tests.
static RTPPkt rtpPkt;       //   Temporary RTP packet buffer
static FILE *infile, *outfile;
int transcodedPktCnt;
char *transcodingTest;

ADT_UInt16 transcodeI8;

void transcodeRTPPackets (ADT_UInt32 Dsp) {
   ADT_printf ("transcodeRTPPackets not implemented\n");
}

void transcodeRTPPayloads (ADT_UInt32 Dsp) {

   ADT_UInt16 PktI8;

   GpakApiStatus_t  pktRcvStat;
   GpakApiStatus_t  pktSndStat;
   GpakPayloadClass PayloadClass;
   ADT_Int16        PayloadType;
   ADT_UInt32       TimeStamp;

   //  1) Read raw PCM data from file.
   PktI8 = fread ((ADT_UInt8 *) &rtpPkt, 2, (transcodeI8 / 2), infile) * 2;
   if (PktI8 != transcodeI8) {
      ADT_printf ("Transcoding file %s completed. %d packets\n", transcodingTest, transcodedPktCnt);
      rtpProcess = NULL;
      return;
   }

   //  2) Use channel 0 to encode L16 PCM data to non-PCM format.
   PayloadClass = PayClassAudio; 
   PayloadType  = L16;
   pktSndStat = gpakSendPayloadToDsp (Dsp, 0, PayloadClass, PayloadType,
                                        (ADT_UInt8 *) &rtpPkt, transcodeI8);
   if (pktSndStat != GpakApiSuccess) {
      ADT_printf ("SndPktToDSP error %d\n", pktSndStat);    
   }

   //  3) Read encoded data from channel 0 
   do {
      PktI8 = sizeof (rtpPkt);
      pktRcvStat =  gpakGetPayloadFromDsp (Dsp, 0, &PayloadClass, &PayloadType,
                                        (ADT_UInt8 *) &rtpPkt, &PktI8, &TimeStamp);
      gpakHostDelay();
      if (pktRcvStat != GpakApiNoPayload) break;
   } while (pktRcvStat == GpakApiNoPayload);
   if (pktRcvStat != GpakApiSuccess) {
      ADT_printf ("GetPktFromDSP error %d\n", pktRcvStat);
      return;
   }

   //  4) pass encoded data to channel 1 for decoding back to L16 PCM.
   pktSndStat = gpakSendPayloadToDsp (Dsp, 1, PayloadClass, PayloadType,
                                        (ADT_UInt8 *) &rtpPkt, PktI8);
   if (pktSndStat != GpakApiSuccess) {
      ADT_printf ("SndPktToDSP error %d\n", pktSndStat);    
   }

   //  5) Write decoded L16 PCM to file for verification.
   do {
      PktI8 = sizeof (rtpPkt);
      pktRcvStat =  gpakGetPayloadFromDsp (Dsp, 1, &PayloadClass, &PayloadType,  
                                        (ADT_UInt8 *) &rtpPkt, &PktI8, &TimeStamp);
      gpakHostDelay();
      if (pktRcvStat != GpakApiNoPayload) break;
   } while (pktRcvStat == GpakApiNoPayload);
   if (pktRcvStat != GpakApiSuccess) {
      ADT_printf ("GetPktFromDSP error %d\n", pktRcvStat);
      return;
   }
   PktI8 = fwrite ((ADT_UInt8 *) &rtpPkt, 1, PktI8, outfile);
   (void) pktSndStat;
   transcodedPktCnt++;
}

char bgTest (ADT_UInt32 DspId, chanDef *chDef, ADT_Bool DTMFDetect) {
   int key;
   char outfileName[80];

   ADT_printf ("\nBackground transcoding tests: %s\n", chDef->ID.Test);
   transcodingTest = chDef->ID.Test;
   dspTearDown (DspId);

   sprintf (outfileName, "%s.pcm", chDef->ID.Test);
   infile  = fopen ("Raw.pcm", "rb");
   if (infile == NULL) {
      ADT_printf ("Unable to locate Raw.pcm file\n");
   }
   outfile = fopen (outfileName, "wb");

   // Calculate number of bytes in frame of data for L16 format.
   transcodeI8 = chDef->ID.CnfrHalfMS * 8;

   rtpProcess = NULL;

   setupChannelsViaTable (DspId, chDef, Null_tone);

   if (DSPPerformsRTP (DspId)) rtpProcess = NULL;
   else                        rtpProcess = transcodeRTPPayloads;
   transcodedPktCnt = 0;
  
   key = processTestKeys (DspId, chDef);


   fclose (infile);
   fclose (outfile);
   return key;
}

void bgTests (ADT_UInt32 DspId) {
   if (bgTest (DspId, BG_TC1, ADT_FALSE) == ESC) return;
   if (bgTest (DspId, BG_TC2, ADT_FALSE) == ESC) return;
   if (bgTest (DspId, BG_TC3, ADT_FALSE) == ESC) return;
   if (bgTest (DspId, BG_TC4, ADT_FALSE) == ESC) return;
   if (bgTest (DspId, BG_TC5, ADT_FALSE) == ESC) return;
   if (bgTest (DspId, BG_TC6, ADT_FALSE) == ESC) return;
   if (bgTest (DspId, BG_TC7, ADT_FALSE) == ESC) return;
   dspTearDown (DspId);
   ADT_printf ("\nBackground test suite complete\n\n");
}
//}
//------------------------------------------------------------------
//{ Tone detection canned tests.
GPAK_AlgControlStat_t setupToneDetector (ADT_UInt32 DspId, chanDef *chDef, int side, GpakToneTypes activateTone) {
   GPAK_AlgControlStat_t DspStat = Ac_Success;
   GpakApiStatus_t       ApiStat = GpakApiSuccess;
   char toneString [20];
   int chType;
   ADT_UInt16 chID;

   chID   = chDef->ID.Chan;
   chType = chDef->ID.ChanType;

   toneString[0] = 0;
   appendToneTypes (activateTone, toneString, sizeof (toneString));
   if (activateTone != Null_tone) 
      activateTone |= Notify_Host;
   ApiStat = gpakAlgControl (DspId, chID, ModifyToneDetector, AllTonesOff, activateTone,
                             (GpakDeviceSide_t) side, NLPoff, GaindB0, &DspStat);
   if (ApiStat != GpakApiSuccess && ApiStat != GpakApiParmError) {
      ADT_printf ("%s request failure %d:%d:%d on %d.%d\n\n", toneString, ApiStat, DspStat, DSPError [DspId], chID, side);
      return DspStat;
   }
   if ((DspStat == Ac_Success) && (activateTone != Null_tone))
      ADT_printf ("\nSearching for %s tones on %s. Channel %d.%c %s", toneString, testSuite->ID.Test, chID, side + 'A', chanType[chType]);
   return DspStat;
}

char testForTones (ADT_UInt32 DspId, chanDef *chDef) {
   int side;
   int tones, toneMask, activateTone;
   char toneString [20];

   chDef++;   // Skip header
   while (chDef->ID.Chan != ((ADT_UInt16) -1)) {

      for (side=0; side < 2; side++) {
         if ((chDef->ID.ChanType == conferencePcm)    && (side == 0)) continue;
         if ((chDef->ID.ChanType == conferencePacket) && (side == 0)) continue;

         tones = ToneDetectAll;   // All tone detection types
         if (chDef->ID.ChanType == conferencePcm)    tones = DTMF_tone;
         if (chDef->ID.ChanType == conferencePacket) tones = DTMF_tone;

         toneMask = 1;

         // Loop through all tone detection types
         while (tones) {
            activateTone = tones & toneMask;
            tones &= ~toneMask;
            toneMask <<= 1;
            if (activateTone == Null_tone) continue;

            if (setupToneDetector (DspId, chDef, side, (GpakToneTypes) activateTone) != Ac_Success)
               continue;

            // Tone setup - play tone file to verify results.
            toneString[0] = 0;
            appendToneTypes ((GpakToneTypes) activateTone, toneString, sizeof (toneString));
            ADT_printf ("\nPlay %s tone file.  ", toneString);
            if (processTestKeys (DspId, NULL) == ESC) return ESC;

         }
         setupToneDetector (DspId, chDef, side, (GpakToneTypes) Null_tone);

         // Tone detection on side B is not available to packet 
         if (chDef->ID.ChanType == pcmToPacket) break;
         if (chDef->ID.ChanType == packetToPacket) break;
      }
      chDef++;
   }

   dspTearDown (DspId);  
   ADT_printf ("\nTone tests for %s complete.\n", testSuite->ID.Test);
   return processTestKeys (DspId, NULL);
}
char toneDetectTest (ADT_UInt32 DspId, chanDef *chanDefs) {
   char *oldPrompt;
   char key;

   chanDef *chDef;
   ADT_printf ("\nTone detection tests: %s\n", chanDefs->ID.Test);
   dspTearDown (DspId);

   // Disable event notification from printing prompts during tests
   oldPrompt = prompt;
   prompt = NULL;

   if (setupConference (DspId, chanDefs->ID.Cnfr, chanDefs->ID.CnfrHalfMS, ADT_TRUE) == ESC) {
      prompt = oldPrompt;
      return ESC;
   }

   setupChannelsViaTable (DspId, chanDefs, DTMF_tone);

   // Disable tone detection on all channels before test starts
   chDef = chanDefs;
   chDef++;
   while (chDef->ID.Chan != ((ADT_UInt16) -1)) {
      setupToneDetector (DspId, chDef, ADevice, Null_tone);
      ADT_sleep (20);
      setupToneDetector (DspId, chDef, BDevice, Null_tone);
      ADT_sleep (20);
      chDef++;
   }
   key = testForTones (DspId, chanDefs);
   prompt = oldPrompt;
   return key;
}
void toneDetectTests (ADT_UInt32 DspId) {

   setMaxDominant (DspId, 3);

   // TDM-TDM channels
   if (toneDetectTest (DspId, TdmTdm1) == ESC) return;

   // TDM-PKT and PKT-PKT channels
   if (toneDetectTest (DspId, PktPkt1) == ESC) return;
   if (toneDetectTest (DspId, PktPkt7) == ESC) return;
   if (toneDetectTest (DspId, PktPkt11) == ESC) return;
   if (toneDetectTest (DspId, PktPkt14) == ESC) return;
   if (toneDetectTest (DspId, PktPkt17) == ESC) return;
   if (toneDetectTest (DspId, PktPkt12) == ESC) return;
   if (toneDetectTest (DspId, PktPkt22) == ESC) return;
   if (toneDetectTest (DspId, PktPkt25) == ESC) return;

   // TDM-Cnfr channels
   if (toneDetectTest (DspId, ToneCfr1) == ESC) return;
   if (toneDetectTest (DspId, ToneCfr2) == ESC) return;
   if (toneDetectTest (DspId, ToneCfr3) == ESC) return;
   if (toneDetectTest (DspId, ToneCfr4) == ESC) return;
   if (toneDetectTest (DspId, ToneCfr5) == ESC) return;
   if (toneDetectTest (DspId, ToneCfr6) == ESC) return;
   if (toneDetectTest (DspId, ToneCfr7) == ESC) return;
   if (toneDetectTest (DspId, ToneCfr8) == ESC) return;
   if (toneDetectTest (DspId, ToneCfr9) == ESC) return;
   if (toneDetectTest (DspId, ToneCfr10) == ESC) return;
   if (toneDetectTest (DspId, ToneCfr11) == ESC) return;
   if (toneDetectTest (DspId, ToneCfr12) == ESC) return;
   if (toneDetectTest (DspId, ToneCfr13) == ESC) return;
   if (toneDetectTest (DspId, ToneCfr14) == ESC) return;
   if (toneDetectTest (DspId, ToneCfr15) == ESC) return;
   if (toneDetectTest (DspId, ToneCfr16) == ESC) return;
   if (toneDetectTest (DspId, ToneCfr17) == ESC) return;

   ADT_printf ("\nTone detection test suite complete\n\n");

}
//}
//------------------------------------------------------------------
//{ Tone generation canned tests.
char generateTestTones (ADT_UInt32 DspId, chanDef *chanDefs) {
   GpakToneGenParms_t *toneParams;
   GpakApiStatus_t    ApiStat;
   GPAK_ToneGenStat_t DspStat;
   chanDef *chDef;

   // A Device (Encode) tone generation is only available for TDM-TDM channels
   for (chDef = chanDefs + 1; chDef->ID.Chan != ((ADT_UInt16) -1); chDef++) {
      if (chDef->ID.ChanType == packetToPacket) continue;

      ADT_printf ("Playing tones on channel %d\n", chDef->ID.Chan);
      for (toneParams = CannedTones; toneParams->ToneCmd != ToneGenStop; toneParams++) {
         if ((chDef->ID.ChanType != pcmToPcm) && (toneParams->Device == BDevice)) continue;
 
         DspStat = Tg_Success;
         ApiStat = gpakToneGenerate (DspId, chDef->ID.Chan, toneParams, &DspStat);
         if ((ApiStat != GpakApiSuccess) && (DspStat != Tg_Success)) {
            ADT_printf ("Tone generation error: %d.%d.%d on channel %d.%c\n", ApiStat, DspStat,
                        DSPError [DspId], chDef->ID.Chan, toneParams->Device + 'A');
            break;
         }
         ADT_sleep (250);
         ApiStat = gpakToneGenerate (DspId, chDef->ID.Chan, &StopTones, &DspStat);
      }
      if (processTestKeys (DspId, NULL) == ESC) return ESC;
   }
   return 'n';
}
char toneGenTest (ADT_UInt32 DspId, chanDef *chanDefs) {

   ADT_Bool DTMFDetect;

   ADT_printf ("\nTone generation tests: %s\n", chanDefs->ID.Test);
   dspTearDown (DspId);

   DTMFDetect = (sysCfg.CfgEnableFlags & DTMFEnableBit) != 0;
   if (setupConference (DspId, chanDefs->ID.Cnfr, chanDefs->ID.CnfrHalfMS, DTMFDetect) == ESC) {
      return ESC;
   }

   setupChannelsViaTable (DspId, chanDefs, Tone_Generate);

   return generateTestTones (DspId, chanDefs);
}
void toneGenTests (ADT_UInt32 DspId) {

   setMaxDominant (DspId, 3);

   // TDM-TDM channels
   if (toneGenTest (DspId, TdmTdm1) == ESC) return;

   // TDM-PKT and PKT-PKT channels
   if (toneGenTest (DspId, PktPkt1) == ESC) return;
   if (toneGenTest (DspId, PktPkt7) == ESC) return;
   if (toneGenTest (DspId, PktPkt11) == ESC) return;
   if (toneGenTest (DspId, PktPkt14) == ESC) return;
   if (toneGenTest (DspId, PktPkt17) == ESC) return;
   if (toneGenTest (DspId, PktPkt12) == ESC) return;
   if (toneGenTest (DspId, PktPkt22) == ESC) return;
   if (toneGenTest (DspId, PktPkt25) == ESC) return;

   // TDM-Cnfr channels
   if (toneGenTest (DspId, ToneCfr1) == ESC) return;
   if (toneGenTest (DspId, ToneCfr2) == ESC) return;
   if (toneGenTest (DspId, ToneCfr3) == ESC) return;
   if (toneGenTest (DspId, ToneCfr4) == ESC) return;
   if (toneGenTest (DspId, ToneCfr5) == ESC) return;
   if (toneGenTest (DspId, ToneCfr6) == ESC) return;
   if (toneGenTest (DspId, ToneCfr7) == ESC) return;
   if (toneGenTest (DspId, ToneCfr8) == ESC) return;
   if (toneGenTest (DspId, ToneCfr9) == ESC) return;
   if (toneGenTest (DspId, ToneCfr10) == ESC) return;
   if (toneGenTest (DspId, ToneCfr11) == ESC) return;

   if (toneGenTest (DspId, ToneCfr12) == ESC) return;
   if (toneGenTest (DspId, ToneCfr13) == ESC) return;
   if (toneGenTest (DspId, ToneCfr14) == ESC) return;
   if (toneGenTest (DspId, ToneCfr15) == ESC) return;
   if (toneGenTest (DspId, ToneCfr16) == ESC) return;
   if (toneGenTest (DspId, ToneCfr17) == ESC) return;
   dspTearDown (DspId);
   ADT_printf ("\nTone generation test suite complete\n\n");

}
//}
//------------------------------------------------------------------
//{ Tone relay canned tests.
char toneRelayTest (ADT_UInt32 DspId, chanDef *chanDefs) {

   ADT_printf ("\nTone relay tests: %s\n", chanDefs->ID.Test);
   dspTearDown (DspId);

   if (setupConference (DspId, chanDefs->ID.Cnfr, chanDefs->ID.CnfrHalfMS, ADT_TRUE) == ESC) {
      return ESC;
   }

   setupChannelsViaTable (DspId, chanDefs, DTMF_tone | Tone_Relay | Tone_Regen | Tone_Generate);

   ADT_printf ("\nPlay DTMF tones and verify tones are reproduced correctly   ");
   return  processTestKeys (DspId, NULL);
}
void toneRelayTests (ADT_UInt32 DspId) {

   setMaxDominant (DspId, 3);

   // TDM-PKT channels
   if (toneRelayTest (DspId, TdmPkt3) == ESC) return;
   if (toneRelayTest (DspId, TdmPkt4) == ESC) return;
   if (toneRelayTest (DspId, TdmPkt13) == ESC) return;
   if (toneRelayTest (DspId, TdmPkt15) == ESC) return;
   if (toneRelayTest (DspId, TdmPkt17) == ESC) return;
   if (toneRelayTest (DspId, TdmPkt19) == ESC) return;

   // TDM-PKT and PKT-PKT channels
   if (toneRelayTest (DspId, PktPkt1) == ESC) return;
   if (toneRelayTest (DspId, PktPkt7) == ESC) return;
   if (toneRelayTest (DspId, PktPkt11) == ESC) return;
   if (toneRelayTest (DspId, PktPkt14) == ESC) return;
   if (toneRelayTest (DspId, PktPkt17) == ESC) return;
   if (toneRelayTest (DspId, PktPkt12) == ESC) return;
   if (toneRelayTest (DspId, PktPkt22) == ESC) return;
   if (toneRelayTest (DspId, PktPkt25) == ESC) return;
   dspTearDown (DspId);

   ADT_printf ("\nTone relay test suite complete\n\n");

}
//}
//------------------------------------------------------------------
//{ Tone insertion canned tests.
char insertTestTones (ADT_UInt32 DspId, ADT_UInt16 chID) {
   char tonePyld[4];
   GpakApiStatus_t        apiStat;
   GPAK_InsertEventStat_t dspStat;

   RTPTones_t *RTPTone;
   ADT_printf ("\n\nInserting RTP tones on channel %d\n", chID);
   for (RTPTone = RTPTones; RTPTone->digit != -1; RTPTone++) {
      tonePyld [0] = (char) RTPTone->digit;
      tonePyld [1] = (char) (0x80 | RTPTone->volumeNegDB);
      tonePyld [2] = (char) (RTPTone->durationSamps >> 8);
      tonePyld [3] = (char) (RTPTone->durationSamps & 0xff);

      apiStat = gpakRTPInsertTone (DspId, chID, (ADT_UInt16) sizeof (tonePyld), (ADT_UInt16 *) tonePyld, &dspStat);
      if (apiStat != GpakApiSuccess) {
         ADT_printf ("Insert tone failure %d.%d.%d\n", apiStat, dspStat, DSPError[DspId]);
         return ESC;
      }
      ADT_sleep (1500);  // Allow 1.5 seconds to playout before next tone
   }
   return 'n';
}
char toneInsertTest (ADT_UInt32 DspId, chanDef *chanDefs) {

   chanDef *chDef;

   ADT_printf ("\n\n\nTone insertion tests: %s\n", chanDefs->ID.Test);
   dspTearDown (DspId);

   if (setupConference (DspId, chanDefs->ID.Cnfr, chanDefs->ID.CnfrHalfMS, ADT_TRUE) == ESC) {
      return ESC;
   }

   setupChannelsViaTable (DspId, chanDefs, Tone_Relay | Tone_Regen | Tone_Generate);

   for (chDef = chanDefs + 1; chDef->ID.Chan != ((ADT_UInt16) -1); chDef++) {
      if ((chDef->ID.ChanType != packetToPacket) && (chDef->ID.ChanType != conferencePacket)) continue;
          
      if (insertTestTones (DspId, chDef->ID.Chan) == ESC) return ESC;
      if (chDef->ID.ChanType != packetToPacket) continue;

      if (insertTestTones (DspId, (ADT_UInt16) (chDef->ID.Chan+1)) == ESC) return ESC;

      if (processTestKeys (DspId, NULL) == ESC) return ESC;

   }
   return 'n';

}
void toneInsertTests (ADT_UInt32 DspId) {

   setMaxDominant (DspId, 3);
   if (toneInsertTest (DspId, Cnfr2) == ESC) return;
   if (toneInsertTest (DspId, Cnfr3) == ESC) return;

   // TDM-PKT and PKT-PKT channels
   if (toneInsertTest (DspId, PktPkt1) == ESC) return;
   if (toneInsertTest (DspId, PktPkt7) == ESC) return;
   if (toneInsertTest (DspId, PktPkt11) == ESC) return;
   if (toneInsertTest (DspId, PktPkt14) == ESC) return;
   if (toneInsertTest (DspId, PktPkt17) == ESC) return;
   if (toneInsertTest (DspId, PktPkt12) == ESC) return;
   if (toneInsertTest (DspId, PktPkt22) == ESC) return;
   if (toneInsertTest (DspId, PktPkt25) == ESC) return;

   dspTearDown (DspId);
   ADT_printf ("\nTone generation test suite complete\n\n");

}
//}
//------------------------------------------------------------------
//{  Fixed gain canned tests
GPAK_AlgControlStat_t setupGain (ADT_UInt32 DspId, ADT_UInt16 chID, int side, GpakAlgCtrl_t setting, ADT_Int16 gaindB) {
   GPAK_AlgControlStat_t DspStat = Ac_Success;
   GpakApiStatus_t       ApiStat = GpakApiSuccess;
   char *gainString;

   ApiStat = gpakAlgControl (DspId, chID, setting, AllTonesOff, AllTonesOff,
                             (GpakDeviceSide_t) side, NLPoff, gaindB, &DspStat);
   if (ApiStat != GpakApiSuccess && ApiStat != GpakApiParmError) {
      ADT_printf (" request failure %d:%d:%d on %d.%d\n\n",  ApiStat, DspStat, DSPError [DspId], chID, side);
      return DspStat;
   }
   if (setting == UpdateTdmInGain) gainString = "Input";
   else                            gainString = "Output";

   if (DspStat == Ac_Success)
      ADT_printf ("%s %s gain set for %3ddB. Channel %d.%c\n", testSuite->ID.Test, gainString, gaindB, chID, side + 'A');
   return DspStat;
}
char channelGainTest (ADT_UInt32 DspId, ADT_UInt16 chID, GpakChanType chType) {
   int side;
   ADT_Int16 gain;

   // Loop through input gain values.
   // A side available for all channel types.
   // B side available for only for TDM-TDM channels.
   for (side=0; side < 2; side++) {
      for (gain = -40; gain <= 40; gain += 10) {
         if (setupGain (DspId, chID, side, UpdateTdmInGain, gain) != Ac_Success)
            continue;
         if (processTestKeys (DspId, NULL) == ESC) return ESC;
      }
      setupGain (DspId, chID, side, UpdateTdmInGain, 0);
      if (chType != pcmToPcm) break;
   }

   // Loop through output gain values
   // A side available for all channels except PKT-PKT channels.
   // B side available for only for TDM-TDM and PKT-PKT.
   for (side=0; side < 2; side++) {
      if ((chType == packetToPacket) && (side == 0)) continue;
      for (gain = -40; gain <= 40; gain += 10) {
         if (setupGain (DspId, chID, side, UpdateTdmOutGain, gain) != Ac_Success)
            continue;
         if (processTestKeys (DspId, NULL) == ESC) return ESC;
      }
      setupGain (DspId, chID, side, UpdateTdmOutGain, 0);

      if ((chType != pcmToPcm) && (chType != packetToPacket))
         break;
   }
   return 'n';
}
char testGainRange (ADT_UInt32 DspId, chanDef *chDef) {

   chDef++;   // Skip header
   while (chDef->ID.Chan != ((ADT_UInt16) -1)) {
      if (channelGainTest (DspId, chDef->ID.Chan, chDef->ID.ChanType) == ESC) return ESC;
      chDef++;
   }

   dspTearDown (DspId);  
   ADT_printf ("\nGain tests for %s complete.\n", testSuite->ID.Test);
   return processTestKeys (DspId, NULL);
}
char gainTest (ADT_UInt32 DspId, chanDef *chanDefs) {
   char key;

   ADT_printf ("\nFixed gain range tests: %s\n", chanDefs->ID.Test);
   dspTearDown (DspId);

   if (setupConference (DspId, chanDefs->ID.Cnfr, chanDefs->ID.CnfrHalfMS, ADT_TRUE) == ESC) {
      return ESC;
   }

   setupChannelsViaTable (DspId, chanDefs, DTMF_tone);

   key = testGainRange (DspId, chanDefs);
   return key;
}
void gainTests (ADT_UInt32 DspId) {

   // TDM-TDM channels
   if (gainTest (DspId, TdmTdm1) == ESC) return;

   // TDM-PKT and PKT-PKT channels
   if (gainTest (DspId, PktPkt2) == ESC) return;
   if (gainTest (DspId, PktPkt8) == ESC) return;
   if (gainTest (DspId, PktPkt12) == ESC) return;
   if (gainTest (DspId, PktPkt15) == ESC) return;
   if (gainTest (DspId, PktPkt18) == ESC) return;
   if (gainTest (DspId, PktPkt22) == ESC) return;
   if (gainTest (DspId, PktPkt24) == ESC) return;
   if (gainTest (DspId, PktPkt25) == ESC) return;

   // TDM-Cnfr channels
   if (gainTest (DspId, ToneCfr1) == ESC) return;
   if (gainTest (DspId, ToneCfr2) == ESC) return;
   if (gainTest (DspId, ToneCfr3) == ESC) return;
   if (gainTest (DspId, ToneCfr4) == ESC) return;
   if (gainTest (DspId, ToneCfr5) == ESC) return;
   if (gainTest (DspId, ToneCfr6) == ESC) return;
   if (gainTest (DspId, ToneCfr7) == ESC) return;
   if (gainTest (DspId, ToneCfr8) == ESC) return;
   if (gainTest (DspId, ToneCfr9) == ESC) return;
   if (gainTest (DspId, ToneCfr10) == ESC) return;
   if (gainTest (DspId, ToneCfr11) == ESC) return;
   ADT_printf ("\nFixed gain test suite complete\n\n");

}
//}
//------------------------------------------------------------------
//{  Automatic gain canned tests
void setAGCSettings (ADT_UInt32 DspId, GpakAGCParms_t *AGC_A, GpakAGCParms_t *AGC_B) {

   GpakApiStatus_t     ApiStat = GpakApiSuccess;
   GPAK_SysParmsStat_t DspStat;
   GpakSystemParms_t   Params;

   Params = sysParams;
   Params.AGC_A = *AGC_A;
   Params.AGC_B = *AGC_B;

   // Update AGC parameters                         AGC      VAD       PCM-EC     PKT-EC     Cnfr      CID
   ApiStat = gpakWriteSystemParms (DspId, &Params, 3, ADT_FALSE, ADT_FALSE, ADT_FALSE,  ADT_FALSE, ADT_FALSE, &DspStat);
   if (ApiStat != GpakApiSuccess) {
      ADT_printf ("Write system configuration failure %d:%d:%d\n", ApiStat, DspStat, DSPError [DspId]);
   }
   return;
}

char testAGCRanges (ADT_UInt32 DspId, chanDef *chanDefs) {
   GpakAGCParms_t      AGC_A, AGC_B;
   int targetPower, lossLimit, gainLimit, lowSignal;

   // Loop through target power settings
   AGC_A = sysParams.AGC_A;
   AGC_B = sysParams.AGC_B;
   for (targetPower = -30; targetPower <= 0; targetPower += 10) {
      AGC_A.TargetPower = targetPower;
      setAGCSettings (DspId, &AGC_A, &AGC_B);
      setupChannelsViaTable (DspId, chanDefs, Null_tone);

      ADT_printf ("%s. A Side AGC target power = %d dBm\n", chanDefs->ID.Test, targetPower);
      if (processTestKeys (DspId, NULL) == ESC) return ESC;
   }
   AGC_A = sysParams.AGC_A;

   // Loop through loss limit settings
   for (lossLimit = -23; lossLimit <= 0; lossLimit += 6) {
      AGC_A.LossLimit = lossLimit;
      setAGCSettings (DspId, &AGC_A, &AGC_B);
      setupChannelsViaTable (DspId, chanDefs, Null_tone);

      ADT_printf ("%s. A Side AGC loss limit = %d dB\n", chanDefs->ID.Test, lossLimit);
      if (processTestKeys (DspId, NULL) == ESC) return ESC;
   }
   AGC_A = sysParams.AGC_A;

   // Loop through gain limit settings
   for (gainLimit = 0; gainLimit <= 23; gainLimit += 6) {
      AGC_B.GainLimit = gainLimit;
      setAGCSettings (DspId, &AGC_A, &AGC_B);
      setupChannelsViaTable (DspId, chanDefs, Null_tone);

      ADT_printf ("%s. B Side AGC gain limit = %d dB\n", chanDefs->ID.Test, gainLimit);
      if (processTestKeys (DspId, NULL) == ESC) return ESC;
   }
   AGC_B = sysParams.AGC_B;

   // Loop through low signal settings
   for (lowSignal = -65; lowSignal <= -20; lowSignal += 9) {
      AGC_B.LowSignal = lowSignal;
      setAGCSettings (DspId, &AGC_A, &AGC_B);
      setupChannelsViaTable (DspId, chanDefs, Null_tone);

      ADT_printf ("%s. B Side AGC low signal = %d dBm\n", chanDefs->ID.Test, lowSignal);
      if (processTestKeys (DspId, NULL) == ESC) return ESC;
   }
   AGC_B = sysParams.AGC_B;
   setAGCSettings (DspId, &AGC_A, &AGC_B);

   dspTearDown (DspId);  
   ADT_printf ("\nAGC tests for %s complete.\n", testSuite->ID.Test);
   
   return processTestKeys (DspId, NULL);
}
char AGCTest (ADT_UInt32 DspId, chanDef *chanDefs) {
   char key;

   ADT_printf ("\nAGC range tests: %s\n", chanDefs->ID.Test);
   dspTearDown (DspId);

   if (setupConference (DspId, chanDefs->ID.Cnfr, chanDefs->ID.CnfrHalfMS, ADT_TRUE) == ESC) {
      return ESC;
   }
   key = testAGCRanges (DspId, chanDefs);
   return key;
}
void AGCTests (ADT_UInt32 DspId) {

   setCaptureSize  (DspId, 6); // AGC test signal runs for 4 seconds

   // TDM-TDM channels
   if (AGCTest (DspId, TdmTdm151) == ESC) return;

   // TDM-PKT channels
   if (AGCTest (DspId, TdmPkt151) == ESC) return;
   if (AGCTest (DspId, TdmPkt152) == ESC) return;
   if (AGCTest (DspId, TdmPkt153) == ESC) return;

   // PKT-PKT channels
   if (AGCTest (DspId, PktPkt151) == ESC) return;
   if (AGCTest (DspId, PktPkt152) == ESC) return;
   if (AGCTest (DspId, PktPkt153) == ESC) return;
   if (AGCTest (DspId, PktPkt154) == ESC) return;
   if (AGCTest (DspId, PktPkt155) == ESC) return;


   // TDM-Cnfr channels
   if (AGCTest (DspId, ToneCfr151) == ESC) return;
   if (AGCTest (DspId, ToneCfr152) == ESC) return;
   if (AGCTest (DspId, ToneCfr153) == ESC) return;
   if (AGCTest (DspId, ToneCfr154) == ESC) return;
   dspTearDown (DspId);

   ADT_printf ("\nAGC test suite complete\n\n");

}
//}
//------------------------------------------------------------------
//{  Voice activity canned tests
void setVADNotify  (ADT_UInt32 DspId, ADT_UInt16 chID, ADT_Bool enableVAD) {
   GpakTestData_t  Params;
   GpakApiStatus_t apiStat;
   GPAK_TestStat_t dspStat;
   ADT_UInt16 Reply [5];

   Params.u.TestParm = (chID << 8) | enableVAD;
   apiStat = gpakTestMode (DspId, (GpakTestMode) 4, &Params, Reply, &dspStat);
   if (apiStat == GpakApiSuccess) return;

   ADT_printf ("VAD notify failure %d.5%d.%d\n", apiStat, dspStat, DSPError[DspId]);
}
char VADTest (ADT_UInt32 DspId, chanDef *chanDefs, ADT_UInt16 chID, ADT_Bool VADNotify) {

   ADT_printf ("\nVAD tests: %s\n", chanDefs->ID.Test);
   dspTearDown (DspId);

   if (setupConference (DspId, chanDefs->ID.Cnfr, chanDefs->ID.CnfrHalfMS, ADT_TRUE) == ESC) {
      return ESC;
   }
   setupChannelsViaTable (DspId, chanDefs, Null_tone);
   setVADNotify  (DspId, chID, VADNotify);

   return processTestKeys (DspId, NULL);;
}
void VADTests (ADT_UInt32 DspId) {

   GpakAGCParms_t AGC_A, AGC_B;

   // Disable affect of AGC
   AGC_A.LossLimit = 0;
   AGC_A.GainLimit = 0;
   AGC_B.LossLimit = 0;
   AGC_B.GainLimit = 0;

   setAGCSettings (DspId, &AGC_A, &AGC_B);

   setCaptureSize  (DspId, 6); // VAD test signal runs for 4 seconds

   // TDM-PKT channels
   if (VADTest (DspId, TdmPkt101, 1, VAD_ON) == ESC) return;
   if (VADTest (DspId, TdmPkt102, 1, VAD_OFF) == ESC) return;
   if (VADTest (DspId, TdmPkt103, 1, VAD_OFF) == ESC) return;

   // PKT-PKT channels
   if (VADTest (DspId, PktPkt101, 0, VAD_ON)  == ESC) return;
   if (VADTest (DspId, PktPkt102, 1, VAD_ON)  == ESC) return;
   if (VADTest (DspId, PktPkt103, 1, VAD_OFF) == ESC) return;
   if (VADTest (DspId, PktPkt104, 1, VAD_OFF) == ESC) return;
   if (VADTest (DspId, PktPkt105, 1, VAD_OFF) == ESC) return;

   // PKT-Cnfr channels
   if (VADTest (DspId, ToneCfr101, 1, VAD_OFF) == ESC) return;
   if (VADTest (DspId, ToneCfr102, 1, VAD_OFF) == ESC) return;
   if (VADTest (DspId, ToneCfr103, 1, VAD_OFF) == ESC) return;
   if (VADTest (DspId, ToneCfr104, 1, VAD_OFF) == ESC) return;
   dspTearDown (DspId);

   ADT_printf ("\nVAD test suite complete\n\n"); 
}
//}
//------------------------------------------------------------------
//{ Record and playback canned tests.
char recordAndPlayback (ADT_UInt32 DspId, chanDef *chanDefs) {
   GpakApiStatus_t       ApiStat;
   GPAK_PlayRecordStat_t DspStat;
   chanDef *chDef;
   int side, bufrI8;

   bufrI8 = 5 * 15000;  // Five seconds of record and playback

   if (WBEnabled) bufrI8 *= 2;

   for (chDef = chanDefs + 1; chDef->ID.Chan != ((ADT_UInt16) -1); chDef++) {
      for (side = 0; side < 2; side++) {

        
         if (((side == 0) && (chDef->ID.ChanType != packetToPacket) && (chDef->ID.ChanType != conferenceComposite)) ||
             (chDef->ID.ChanType == pcmToPcm)) {
            ADT_printf ("Press any key to start recording on channel %d.%c\n", chDef->ID.Chan, side + 'A');
            if (processTestKeys (DspId, NULL) == ESC) return ESC;
            ApiStat = gpakSendPlayRecordMsg (DspId, chDef->ID.Chan, (GpakDeviceSide_t) side, StartRecording,
                                             playRecBuffers[side], bufrI8, RecordingL16, tdsNoToneDetected, &DspStat);
            if ((ApiStat != GpakApiSuccess) || (DspStat != PrSuccess)) {
               ADT_printf ("Start record failure = %d.%d.%d\n", ApiStat, DspStat, DSPError[DspId]);
               return ESC;
            }
            ADT_sleep (6000);
         }

         ADT_printf ("Press any key to start continuous playback on channel %d.%c\n", chDef->ID.Chan, side + 'A');
         if (processTestKeys (DspId, NULL) == ESC) return ESC;
         ApiStat = gpakSendPlayRecordMsg (DspId, chDef->ID.Chan, (GpakDeviceSide_t) side, StartContinuousPlayBack,
                                          playRecBuffers[side], bufrI8, RecordingL16, tdsNoToneDetected, &DspStat);
         if ((ApiStat != GpakApiSuccess) || (DspStat != PrSuccess)) {
            ADT_printf ("Start record failure = %d.%d.%d\n", ApiStat, DspStat, DSPError[DspId]);
            return ESC;
         }
         ADT_printf ("Press any key to stop playback on channel %d.%c\n", chDef->ID.Chan, side + 'A');
         if (processTestKeys (DspId, NULL) == ESC) return ESC;

         ApiStat = gpakSendPlayRecordMsg (DspId, chDef->ID.Chan, (GpakDeviceSide_t) side, StopPlayBack,
                                          playRecBuffers[side], bufrI8, RecordingL16, tdsNoToneDetected, &DspStat);
         if ((ApiStat != GpakApiSuccess) || (DspStat != PrSuccess)) {
            ADT_printf ("Start record failure = %d.%d.%d\n", ApiStat, DspStat, DSPError[DspId]);
            return ESC;
         }

         // B-Side only TDM-TDM and TDM-PKT channels
         if ((chDef->ID.ChanType != pcmToPcm) && (chDef->ID.ChanType != pcmToPacket))
            break;
      }
      ADT_printf ("---Channel %d complete ", chDef->ID.Chan);
      if (processTestKeys (DspId, NULL) == ESC) return ESC;
   }
   return 'n';
}
char recordPlayTest (ADT_UInt32 DspId, chanDef *chanDefs) {
   char key;
   char *oldPrompt;

   ADT_printf ("\nRecord and playback tests: %s\n", chanDefs->ID.Test);
   dspTearDown (DspId);

   if (setupConference (DspId, chanDefs->ID.Cnfr, chanDefs->ID.CnfrHalfMS, ADT_TRUE) == ESC) {
      return ESC;
   }

   oldPrompt = prompt;
   prompt = "";
   setupChannelsViaTable (DspId, chanDefs, Null_tone);

   key = recordAndPlayback (DspId, chanDefs);
   ADT_printf ("Record and playback tests: %s complete\n\n\n", chanDefs->ID.Test);
   prompt = oldPrompt;
   return key;
}
void recordPlayTests (ADT_UInt32 DspId) {

   setMaxDominant (DspId, 3);

   // TDM-TDM channels
   if (recordPlayTest (DspId, TdmTdm1) == ESC) return;

   // Conference channels
   if (recordPlayTest (DspId, ToneCfr2) == ESC) return;
   if (recordPlayTest (DspId, ToneCfr3) == ESC) return;
   if (recordPlayTest (DspId, ToneCfr4) == ESC) return;
   if (recordPlayTest (DspId, ToneCfr5) == ESC) return;
   if (recordPlayTest (DspId, ToneCfr6) == ESC) return;
   if (recordPlayTest (DspId, ToneCfr7) == ESC) return;
   if (recordPlayTest (DspId, ToneCfr8) == ESC) return;
   if (recordPlayTest (DspId, ToneCfr9) == ESC) return;
   if (recordPlayTest (DspId, ToneCfr10) == ESC) return;
   if (recordPlayTest (DspId, ToneCfr11) == ESC) return;



   // TDM-PKT and PKT-PKT channels
   if (recordPlayTest (DspId, PktPkt1) == ESC) return;
   if (recordPlayTest (DspId, PktPkt7) == ESC) return;
   if (recordPlayTest (DspId, PktPkt11) == ESC) return;
   if (recordPlayTest (DspId, PktPkt14) == ESC) return;
   if (recordPlayTest (DspId, PktPkt17) == ESC) return;
   if (recordPlayTest (DspId, PktPkt12) == ESC) return;
   if (recordPlayTest (DspId, PktPkt22) == ESC) return;
   if (recordPlayTest (DspId, PktPkt25) == ESC) return;

   // TDM-Cnfr channels
   if (recordPlayTest (DspId, ToneCfr1) == ESC) return;

   ADT_printf ("\nRecord playback test suite complete\n\n");

}
//}
//------------------------------------------------------------------
//{ Echo canceller canned tests.
char echoTest (ADT_UInt32 DspId, chanDef *chanDefs) {

   ADT_printf ("\nEcho canceller tests: %s\n", chanDefs->ID.Test);
   dspTearDown (DspId);

   if (setupConference (DspId, chanDefs->ID.Cnfr, chanDefs->ID.CnfrHalfMS, ADT_FALSE) == ESC) {
      return ESC;
   }

   setupChannelsViaTable (DspId, chanDefs, Null_tone);

   ADT_printf ("Echo canceller test: %s set up\n\n\n", chanDefs->ID.Test);
   return processTestKeys (DspId, NULL);
}
void echoTests (ADT_UInt32 DspId) {

   setMaxDominant (DspId, 3);

   if (echoTest (DspId, TdmPkt202) == ESC) return;
   if (echoTest (DspId, TdmPkt203) == ESC) return;
   if (echoTest (DspId, TdmPkt204) == ESC) return;
   if (echoTest (DspId, TdmPkt205) == ESC) return;
   if (echoTest (DspId, TdmPkt206) == ESC) return;
 
   // PKT-PKT channels
   if (echoTest (DspId, PktPkt201) == ESC) return;
   if (echoTest (DspId, PktPkt202) == ESC) return;
   if (echoTest (DspId, PktPkt203) == ESC) return;
   if (echoTest (DspId, PktPkt204) == ESC) return;
   if (echoTest (DspId, PktPkt205) == ESC) return;
   if (echoTest (DspId, PktPkt206) == ESC) return;

   // TDM-Cnfr channels
   if (echoTest (DspId, Cfr201) == ESC) return;
   if (echoTest (DspId, Cfr202) == ESC) return;
   if (echoTest (DspId, Cfr203) == ESC) return;

   // PKT-Cnfr channels
   if (echoTest (DspId, Cfr204) == ESC) return;
   if (echoTest (DspId, Cfr205) == ESC) return;
   if (echoTest (DspId, Cfr206) == ESC) return;
   if (echoTest (DspId, Cfr207) == ESC) return;

   // TDM-TDM channels
   if (echoTest (DspId, TdmTdm201) == ESC) return;
   if (echoTest (DspId, TdmTdm202) == ESC) return;
   if (echoTest (DspId, TdmTdm203) == ESC) return;

   // TDM-PKT channels
   if (echoTest (DspId, TdmPkt201) == ESC) return;

   ADT_printf ("\nEcho canceller test suite complete\n\n");

}
//}

//{ Loading tests
void addTranscoderChannels (ADT_UInt32 DspId, chanDef *chDef, int cnt) {
   chanDef *pcmPktDef;
	// Find transcoder channel definition.
   ADT_UInt16 chnID, cnfr;

   while (chDef->ID.Chan != ((ADT_UInt16) -1)) chDef++;
   chDef--;
   pcmPktDef = chDef--;

   // Set channel ID and count
   chnID = chDef->ID.Chan;
   cnfr  = chDef->ID.Cnfr;
   chDef->ID.Chan = daisyChainTerminator;
   chDef->ID.Cnfr = cnt;
   pcmPktDef->ID.Chan = daisyChainTerminator + (2 * cnt);
   setupChannelsViaTable (DspId, chDef, NoTearDown);
   chDef->ID.Chan = chnID;
   chDef->ID.Cnfr = cnfr;
   pcmPktDef->ID.Chan = 0;
  
}
void addTerminationChannel (ADT_UInt32 DspId, chanDef *chDef) {
   ADT_UInt16 chnID;
   // Find termination channel definition.
   while (chDef->ID.Chan != ((ADT_UInt16) -1)) chDef++;
   chDef--;

   // Set new termination channel based upon active channel count
   chnID = chDef->ID.Chan;
   chDef->ID.Chan = daisyChainTerminator;
   setupChannelsViaTable (DspId, chDef, NoTearDown);
   chDef->ID.Chan = chnID;
}

char daisyTest  (ADT_UInt32 DspId, chanDef *chDef) {
   chanDef *pcmPktDef;
   ADT_UInt16 chnID;

   ADT_printf ("\nDaisy chain transcoding test: %s\n", chDef->ID.Test);
   dspTearDown (DspId);

   pcmPktDef = chDef;
   while (pcmPktDef->ID.Chan != ((ADT_UInt16) -1)) pcmPktDef++;
   pcmPktDef--; 
   chnID = pcmPktDef->ID.Chan;

   setupChannelsViaTable (DspId, chDef, Null_tone);
   pcmPktDef->ID.Chan = chnID;
   

   ADT_printf ("'+' for more, '-' for less, 'x' to stop, 'q' to exit\n");
   do {
      switch (ADT_getchar ()) {
      case 'q': return ESC;
      case 'x': dspTearDown (DspId);  return ESC;
      case '-': 
         tearDownChannels (DspId, 5, daisyChainTerminator);
         daisyChainTerminator -= 4;
         addTerminationChannel (DspId, chDef);
         break;

      case '+':
         tearDownChannels (DspId, 1, daisyChainTerminator);
         addTranscoderChannels (DspId, chDef, 2);
         break;

      case '\r':
      case '\n':
      case 'n':
         if (chDef != NULL) dspTearDown (DspId);
         return ' ';
      }
ADT_printf ("Start/End:  %d %d", daisyChainStart, daisyChainTerminator);
   } while (ADT_TRUE);
}
void loadingTests (ADT_UInt32 DspId) {
   // Pkt-Pkt
   if (daisyTest (DspId, DC1) == ESC) return;
   if (daisyTest (DspId, DC2) == ESC) return;
   if (daisyTest (DspId, DC3) == ESC) return;
   if (daisyTest (DspId, DC4) == ESC) return;

   // Conferencing
   if (daisyTest (DspId, DC_CF1) == ESC) return;
   if (daisyTest (DspId, DC_CF2) == ESC) return;
   if (daisyTest (DspId, DC_CF3) == ESC) return;

}
//}

// Tests with problems that need to be debugged
void debugTests (ADT_UInt32 DspId) {
   if (tdmPktTest (DspId, TdmPkt17) == ESC) return;
}

void cannedGpakTests (ADT_UInt32 DspId) {

   ADT_printf ("\n\nCanned Tests\n");
   ADT_printf ("1=TDM<->TDM\n");
   ADT_printf ("2=TDM<->PKT\n");
   ADT_printf ("3=PKT<->PKT\n");
   ADT_printf ("4=Conferences\n\n");

   ADT_printf ("5=Tone detection\n");
   ADT_printf ("6=Tone generation\n");
   ADT_printf ("7=Tone relay\n");
   ADT_printf ("8=Gains\n");
   ADT_printf ("9=AGC\n");
   ADT_printf ("10=VAD\n");
   ADT_printf ("11=Echo canceller\n\n");

   ADT_printf ("12=Record playback\n");
   ADT_printf ("13=Tone insertion\n\n");
   
   ADT_printf ("14=Loading\n");
   ADT_printf ("15=Background transcoding\n");

   // Restore AGC parameters to system defaults.
   setAGCSettings (DspId, &sysParams.AGC_A, &sysParams.AGC_B);

   // Establish default RTP packet processing
   if (DSPPerformsRTP (DspId)) rtpProcess = loopbackRTPPackets;
   else                        rtpProcess = loopbackRTPPayloads;

   switch (ADT_getint ("\nTest ID: ")) {
   case 0: debugTests      (DspId); break;
   case 1: tdmTdmTests     (DspId); break;
   case 2: tdmPktTests     (DspId); break;
   case 3: pktPktTests     (DspId); break;
   case 4: cnfrTests       (DspId); break;

   case 5: toneDetectTests (DspId); break;
   case 6: toneGenTests    (DspId); break;
   case 7: toneRelayTests  (DspId); break;
   case 8: gainTests       (DspId); break;
   case 9: AGCTests        (DspId); break;

   case 10: VADTests        (DspId); break;
   case 11: echoTests       (DspId); break;
   case 12: recordPlayTests (DspId); break;
   case 13: toneInsertTests (DspId); break;
   case 14: loadingTests    (DspId); break;
   case 15: bgTests         (DspId); break;
   }

   // Restore default RTP packet processing routines.
   if (DSPPerformsRTP (DspId)) rtpProcess = loopbackRTPPackets;
   else                        rtpProcess = loopbackRTPPayloads;

   return;
}
