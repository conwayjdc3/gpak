/*
 * Copyright (c) 2009, Adaptive Digital Technologies, Inc.
 *
 * File Name: GpakPCI.c
 *
 * Description:
 *   This file contains host system dependent functions to support generic
 *   G.PAK API functions on the TI DM642 and EVM6424 DSPs. The file is integrated into 
 *   the host processor  connected to G.PAK DSPs via a Host Port Interface.
 *
 *   Note: This file needs to be modified by the G.PAK system integrator.
 *
 * Version: 1.0
 *
 * Revision History:
 *   10/17/01 - Initial release.
 *
 */
#ifdef _WIN32
    #if (_MSC_VER >= 900)
       #define   WIN32_LEAN_AND_MEAN  1
       #define   INC_OLE2
       #define   NOSERVICE
    #endif 

    #include <windows.h>
    #include <stdio.h>
    #include <conio.h>   
#endif

#include "time.h"

#include "process.h"
#include "objbase.h"
#include "64.h"
#include "6424.h"
#include "sd_pci_lib.h"
#include "gpakcust.h"

#include "adt_typedef.h"

static PCI64_HANDLE pciHandle  = NULL;  // pci handle for main thread
static PCI64_HANDLE intHandle  = NULL;  // pci handle for interrupt thread
static HANDLE hCallbackEvent = NULL;

static char *DDR2Init (PCI64_HANDLE hPci, int freq, int bit32);
static char *EmifInit (PCI64_HANDLE hPci);
static char *EnableInterruptsFromDSP  (PCI64_HANDLE hPci);
static char *DisableInterruptsFromDSP (PCI64_HANDLE hPci);

static char *ReleaseDSP     (PCI64_HANDLE hPci);
static char *IsPciBootMode  (PCI64_HANDLE hPci);
static char *AssertDspReset (PCI64_HANDLE hPci);

static int Locked = FALSE;
static HANDLE mutex = NULL;

int PCIDsp = 0;                      // DSP type PCI is connected to
int ifBlkAddress = 0;                // Interface block address
unsigned int MaxCoresPerDSP   = CORES_PER_DSP;

void (*ProcessDSPInterrupts)(void) = NULL;   // User function to handle interrupts

int DSPInterrupts = 0;
int dspToHostIntrCnt = 0;

//{==========================================================
//
//  System error trap
//
//}===========================================================
#ifdef _DEBUG
void SystemError () {
   printf ("System Error\n");
   while (1) Sleep (1);
}
#else
void SystemError () {
   return;
}
#endif



/****************************************************************************
* NAME:  PCILibOpen
*
* DESCRIPTION:  Opens dll library according to the dspType 
*         
* Inputs
*     dspType  -  642 or 6424
*
* RETURN VALUE:
*    dllHandle - NULL if failure
*
*F***************************************************************************/
HMODULE PCILibOpen (int dspType) {
   HMODULE dll = NULL;

   if (dspType == 6424) {
      dll =  LoadLibrary ("sd_pci_6424.dll");
      ifBlkAddress = 0x10800000;
   } else {
      dll =  LoadLibrary ("sd_pci_dm642.dll");
      ifBlkAddress =0x0200;
   }       
   if (dll == NULL) {
      printf ("PCI load library failed\n");
      return NULL;
   }

   PCIDsp = dspType;

   PCI64_Open  = (PCIOpen)  GetProcAddress (dll, "PCI64_Open");
   PCI64_Close = (PCIClose) GetProcAddress (dll, "PCI64_Close");

   PCI64_MemFill32  = (PCIXfer) GetProcAddress (dll, "PCI64_MemFill32");
   PCI64_MemWrite32 = (PCIXfer) GetProcAddress (dll, "PCI64_MemWrite32");
   PCI64_MemRead32  = (PCIXfer) GetProcAddress (dll, "PCI64_MemRead32");
   PCI64_RegWrite32 = (PCIXfer) GetProcAddress (dll, "PCI64_RegWrite32");
   PCI64_RegRead32  = (PCIXfer) GetProcAddress (dll, "PCI64_RegRead32"); 

   PCI64_LoadCoff           = (PCILoadCoff) GetProcAddress (dll, "PCI64_LoadCoff"); 
   PCI64_GetCallbackEvent   = (PCICbHndl)   GetProcAddress (dll, "PCI64_GetCallbackEvent"); 
   PCI64_SetupCallbackEvent = (PCICbEvent)  GetProcAddress (dll, "PCI64_SetupCallbackEvent");
   return dll;
}

/****************************************************************************
* NAME:  PCILibClose
*
* DESCRIPTION:  Closes PCI dll library
*         
* Inputs
*     dll  -  Handle for PCI library
*
*F***************************************************************************/
PCILibClose (HMODULE dll) {
   FreeLibrary (dll);
   PCIDsp = 0;
}


/****************************************************************************
* NAME:  PCI64_RegRead ()
*
* DESCRIPTION:  Handles register reads that are outside of the PCI area 
*               (01c0 0000 .. 01ff ffff) the Spectrum Digital has mapped out
*               for registers 
*         
* Inputs
*     hPci  -  Handle for PCI access
*     reg   -  Register address
*    value  -  pointer to store register value
*
* RETURN VALUE:
*    0 Success, non-0 for Fail
*
*F***************************************************************************/
static ADT_Int32 PCI64_RegRead (PCI64_HANDLE hPci, ADT_UInt32 reg, ADT_UInt32 *value) {

    if (hPci == NULL) return -1;

    if (0x01c00000 <= reg && reg < 0x02000000)
        return PCI64_RegRead32 (hPci, reg, 1, value);
    else
        return PCI64_MemRead32 (hPci, reg, 1, value);
}

/****************************************************************************
* NAME:  PCI64_RegWrite ()
*
* DESCRIPTION:  Handles register writes that are outside of the PCI area 
*               (01c0 0000 .. 01ff ffff) the Spectrum Digital has mapped out
*               for registers 
*         
* Inputs
*     hPci  -  Handle for PCI access
*     reg   -  Register address
*    value  -  pointer to store register value
*
* RETURN VALUE:
*    0 Success, non-0 for Fail
*
*F***************************************************************************/
static ADT_Int32 PCI64_RegWrite (PCI64_HANDLE hPci, ADT_UInt32 reg, ADT_UInt32 *value) {

    if (hPci == NULL) return -1;

    if (0x01c00000 <= reg && reg < 0x02000000)
        return PCI64_RegWrite32 (hPci, reg, 1, value);
    else
        return PCI64_MemWrite32 (hPci, reg, 1, value);
}


//=============================================================================
//
//  Interrupt Support
//
//==============================================================================

//{===============================================================
//
//  Callback on DSP interrupt
//
//}===============================================================
static DWORD WINAPI DSPCallback (LPVOID dummy) {
    ADT_UInt32 RegRst, Hsr;
    ADT_Int32  Error;


    if (hCallbackEvent == NULL)
        return (-1);


    while (hCallbackEvent != NULL) {

       if (WaitForSingleObject (hCallbackEvent, INFINITE)) continue;
            
       dspToHostIntrCnt++;

       // Clear interrupt
       if (pciHandle == NULL) continue;

       if (PCIDsp == 642) {
          RegRst = RSTSRC_INTRST;    // Remove interrupt signal
          Error = PCI64_RegWrite32 (intHandle, PCI64_RSTSRC, 1, &RegRst);
          if (Error) continue;

          Error = PCI64_RegRead32 (intHandle, PCI64_HSR, 1, &Hsr);
          if (Error) continue;
          if (Hsr & HSR_INTSRC) {
             Hsr |= HSR_INTSRC;  // Clear pending interrupt
             PCI64_RegWrite32 (intHandle, PCI64_HSR,1, &Hsr);
         }
      }
      if (ProcessDSPInterrupts)  (*ProcessDSPInterrupts)();
   }

   return 0;
}

/****************************************************************************
* NAME:  EnableInterruptFrom64 ()
*
* DESCRIPTION:  Enable interrupts from DSP via PCI 
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/
static char *EnableInterruptsFrom64 (PCI64_HANDLE hPci) {

   ADT_UInt32          Hsr, Pciis;

    if (hPci == NULL) return "DSP Release failure. Null Handle";

 
    if (PCI64_RegRead32 (hPci, PCI64_HSR, 1, &Hsr))
       return "Unable to read HSR";

    if (PCI64_RegRead32 (hPci, PCI64_PCIIS, 1, &Pciis))
       return "Unable to read PCIIS";

    Hsr |= HSR_INTSRC;   // Clear pending interrupts
    if (PCI64_RegWrite32 (hPci, PCI64_HSR, 1, &Hsr))
       return "Unable to clear pending interrupts";


    Hsr &= ~HSR_INTAM;   // Enable interrupt
    if (PCI64_RegWrite32 (hPci, PCI64_HSR, 1, &Hsr))
       return "Unable to enable DSP interrupts";

    if (PCI64_RegWrite32 (hPci, PCI64_PCIIS, 1, &Pciis))
       return "Unable to clear PCIIS";

    return NULL;
}

/****************************************************************************
* NAME:  EnableInterruptFrom6424 ()
*
* DESCRIPTION:  Enable interrupts from DSP via PCI 
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/
static char *EnableInterruptsFrom6424 (PCI64_HANDLE hPci) {

   ADT_UInt32          Hsr;

    if (hPci == NULL) return "DSP Release failure. Null Handle";


    Hsr = HOST_INTS;   // Enable interrupts
    if (PCI64_RegWrite (hPci, PCI64_HINT_CLR, &Hsr))
       return "Unable to enable DSP interrupts";

    Hsr = HOST_INTS;   // Enable interrupts
    if (PCI64_RegWrite (hPci, PCI64_HINT_SET, &Hsr))
       return "Unable to enable DSP interrupts";

    return NULL;
}

/****************************************************************************
* NAME:  EnableInterruptFromCDSP ()
*
* DESCRIPTION:  Enable interrupts from DSP via PCI 
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/
static char *EnableInterruptsFromDSP (PCI64_HANDLE hPci) {

   if (PCIDsp == 642)
      return EnableInterruptsFrom64 (hPci);
   else if (PCIDsp == 6424);
      return EnableInterruptsFrom6424 (hPci);
}


/****************************************************************************
* NAME:  DisableInterruptFrom64 ()
*
* DESCRIPTION:  Disable interrupts from DSP via PCI 
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/
static char *DisableInterruptsFrom64 (PCI64_HANDLE hPci) {

    ADT_UInt32          Hsr;

    if (hPci == NULL) return "DSP Release failure. Null Handle";

 
    if (PCI64_RegRead32 (hPci, PCI64_HSR, 1, &Hsr))
       return "Unable to read HSR";

    Hsr |= HSR_INTAM;    // Disable interrupt
    if (PCI64_RegWrite32 (hPci, PCI64_HSR, 1, &Hsr))
       return "Unable to disable interrupts";

    return NULL;
}

/****************************************************************************
* NAME:  DisableInterruptFrom6424 ()
*
* DESCRIPTION:  Disable interrupts from DSP via PCI 
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/
static char *DisableInterruptsFrom6424 (PCI64_HANDLE hPci) {

    ADT_UInt32          Hsr;

    if (hPci == NULL) return "DSP Release failure. Null Handle";

 
    Hsr = ALL_INTS;   // Enable interrupts
    if (PCI64_RegWrite (hPci, PCI64_HINT_CLR, &Hsr))
       return "Unable to disable interrupts";

    return NULL;
}

/****************************************************************************
* NAME:  DisableInterruptFromDSP ()
*
* DESCRIPTION:  Disable interrupts from DSP via PCI 
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/
static char *DisableInterruptsFromDSP (PCI64_HANDLE hPci) {

   if (PCIDsp == 642)
      return DisableInterruptsFrom64 (hPci);
   else if (PCIDsp == 6424);
      return DisableInterruptsFrom6424 (hPci);
}



//=============================================================================
//
//  Startup Support
//
//==============================================================================

/****************************************************************************
* NAME:  AssertDspReset63 ()
*
* DESCRIPTION:  Put DSP in reset
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    0 Success, non-0 for Fail
*
*F***************************************************************************/
static char *AssertDspReset64 (PCI64_HANDLE hPci) {
    ADT_UInt32 RegHdcr;
    ADT_Int32  Error;

    if (hPci == NULL) return "DSP reset failure. PCI not open";

    Error = PCI64_RegRead32 (hPci, PCI64_HDCR, 1, &RegHdcr);
    if (Error)
        return "DSP reset failure. HDCR register read failure";

    RegHdcr |= HDCR_WARMRESET;  // Place DSP in warm reset
    Error = PCI64_RegWrite32 (hPci, PCI64_HDCR, 1, &RegHdcr);
    if (Error)
        return "DSP reset failure. HDCR register write failure";
    
    Sleep (2);

    EmifInit (hPci);
    return NULL;
}

/****************************************************************************
* NAME:  AssertDspReset6424 ()
*
* DESCRIPTION:  Put DSP in reset
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    0 Success, non-0 for Fail
*
*F***************************************************************************/
static char *AssertDspReset6424 (PCI64_HANDLE hPci) {
    ADT_UInt32 Value;
    ADT_Int32  Error;

    if (hPci == NULL) return "DSP reset failure. PCI not open";

    Error = PCI64_RegRead (hPci, BOOTCOMPLT, &Value);
    if (Error)
        return "DSP reset failure. BOOT register read failure";

    if (Value & 1)
        return "C6424 does not support host reset";
        
    
    Sleep (2);

    DDR2Init (hPci, 162, 1);
    return NULL;
}

/****************************************************************************
* NAME:  AssertDspReset ()
*
* DESCRIPTION:  Put DSP in reset
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    0 Success, non-0 for Fail
*
*F***************************************************************************/
static char *AssertDspReset (PCI64_HANDLE hPci) {
   if (PCIDsp == 642)
      return  AssertDspReset64 (hPci);
   else if (PCIDsp == 6424);
      return  AssertDspReset6424 (hPci);
}



/****************************************************************************
* NAME: Release64 () 
*
* DESCRIPTION:  Release DSP from  reset and start code exection from address 0x00000000.
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    NULL Success,  Failure message
*
*F***************************************************************************/
static char *Release64 (PCI64_HANDLE hPci) {
    ADT_UInt32 RegHdcr;
    ADT_Int32  Error;

    if (hPci == NULL) return "DSP Release failure. Null Handle";

    Error = PCI64_RegRead32 (hPci, PCI64_HDCR, 1, &RegHdcr);
    if (Error)
        return "DSP Read HDCR Register failure";

    RegHdcr |= HDCR_DSPINT;   //  Generate DSP interrupt to release DSP
    Error = PCI64_RegWrite32 (hPci, PCI64_HDCR, 1, &RegHdcr);
    if (Error) 
       return "DSP Write HDCR Register failure";

    return NULL;
}

/****************************************************************************
* NAME: Release6424 () 
*
* DESCRIPTION:  Release DSP from  reset and start code exection from address 0x00000000.
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    NULL Success,  Failure message
*
*F***************************************************************************/
static char *Release6424 (PCI64_HANDLE hPci) {
    ADT_UInt32 Value;
    ADT_Int32  Error;

    if (hPci == NULL) return "DSP Release failure. Null Handle";

    Error = PCI64_RegRead (hPci, BOOTCOMPLT, &Value);
    if (Error)
        return "DSP Read BOOT Register failure";

    Value |= 1;   //  Generate DSP interrupt to release DSP
    Error = PCI64_RegWrite (hPci, BOOTCOMPLT, &Value);
    if (Error) 
       return "DSP Write BOOT Register failure";

    return NULL;
}

/****************************************************************************
* NAME: ReleaseDSP () 
*
* DESCRIPTION:  Release DSP from  reset and start code exection from address 0x00000000.
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    NULL Success,  Failure message
*
*F***************************************************************************/
static char *ReleaseDSP (PCI64_HANDLE hPci) {
   if (PCIDsp == 642)
      return  Release64 (hPci);
   else if (PCIDsp == 6424);
      return  Release6424 (hPci);
}



/****************************************************************************
* NAME: IsPciBootMode 
*
* DESCRIPTION:  Tests for PCI boot mode.  If not in PCI boot mode then
*               should NOT attempt doing PCI boot.
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*  NULL - PCI Boot mode enabled
*  Other - reason not enabled
*
****************************************************************************/
static char *IsPciBootMode (PCI64_HANDLE hPci) {

    ADT_UInt32 RegHdcr = 0;

    if (hPci == NULL) return "NULL Handle";

    if (PCI64_RegRead32 (hPci, PCI64_HDCR, 1, &RegHdcr))
       return "HDCR Register Read Error";

    if (RegHdcr == HDCR_PCIBOOT) return NULL;
    return "DSP not in PCI boot mode";
}

/****************************************************************************
* NAME:  EmifInit ()
*
* DESCRIPTION:  Init the EMIF.  Just handles EMIF-A.
*
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/
static char *EmifInit (PCI64_HANDLE hPci) {

    ADT_Bool success;

    ADT_UInt32 EmifA[12] = { 0x00052078UL,     // GCTL     - 0x01800000
                         0x73A28E01UL,        // CE1      - 0x01800004
                         0xFFFFFFD3UL,        // CE0      - 0x01800008
                         // Address split 3
                         0x22a28a22UL,        // CE2      - 0x01800010
                         0x22a28a42UL,        // CE3      - 0x01800014
                         0x57115000UL,        // SDRAMCTL - 0x01800018
                         0x0000081bUL,        // SDRAMTIM - 0x0180001c
                         0x001faf4dUL,        // SDRAMEXT - 0x01800020
                         // Address split 9
                         0x00000002UL,        // CE1ECCTL - 0x01800044
                         0x00000002UL,        // CE0ECCTL - 0x01800048
                         // Address split 
                         0x00000002UL,        // CE2ECCTL - 0x01800050
                         0x00000073UL,        // CE3ECCTL - 0x01800054
                        };

    if (hPci == NULL) return "EMIF Init failure. PCI Bus not open";

    success  = !PCI64_MemWrite32 (hPci, 0x01800000UL, 3, &EmifA[0]);
    success &= !PCI64_MemWrite32 (hPci, 0x01800010UL, 5, &EmifA[3]);
    success &= !PCI64_MemWrite32 (hPci, 0x01800048UL, 2, &EmifA[8]);
    success &= !PCI64_MemWrite32 (hPci, 0x01800050UL, 2, &EmifA[10]);
    if (success) return NULL;

    return "EMIF Init failure. Write error.";
}

/****************************************************************************
 *                                                                          *
 *  psc_change_state (id, state)                                            *
 *      id    = Domain #ID                                                  *
 *      state = (ENABLE, DISABLE, SYNCRESET, RESET)                         *
 *              (  =3  ,   =2   ,    =1    ,   =0 )                         *
 *                                                                          *
 ************************************************************************** */
void psc_change_state (PCI64_HANDLE hPci, int id, int state) {

    ADT_UInt32 Value;
    ADT_UInt32 mdstat = 0x01c41800UL + (4 * id);
    ADT_UInt32 mdctl  = 0x01c41a00UL + (4 * id);
    int try = 0;

    
    //  Step 0 - Ignore request if the state is already set as is
    PCI64_RegRead  (hPci, mdstat, &Value);
    if ((Value & 0x1f) == (ADT_UInt32) state)
        return;

    //  Step 1 - Wait for PTSTAT.GOSTAT to clear
    do {
        PCI64_RegRead  (hPci, PSC_PTSTAT, &Value);
    } while ((Value & 1) && try++<1000);
    if (1000 <= try) {
      printf ("\nPower module [%d] state change failure ", id);
      return;
    }

    //  Step 2 - Set MDCTLx.NEXT to new state
    PCI64_RegRead  (hPci, mdctl, &Value);
    Value &= ~0x1f;
    Value |= state;
    PCI64_RegWrite (hPci, mdctl, &Value);

    //  Step 3 - Start power transition (set PTCMD.GO to 1)
    Value = 1;
    PCI64_RegWrite (hPci, PSC_PTCMD, &Value);

    //  Step 4 - Wait for PTSTAT.GOSTAT to clear
    try=0;
    do {
        PCI64_RegRead  (hPci, PSC_PTSTAT, &Value);
    } while ((Value & 1) && try++<1000);
    if (1000 <= try) {
      printf ("\nPower module [%d] state change failure ", id);
      return;
    }

    //  Step 5 - Verify state changed
    try=0;
    do {
        PCI64_RegRead  (hPci, mdstat, &Value);
    } while (((Value & 0x1f) != (ADT_UInt32) state) && try++<10000);
    if (10000 <= try) {
      printf ("\nPower module [%d] state change failure ", id);
      return;
    }
}

/****************************************************************************
* NAME:  DDR2Init ()
*
* DESCRIPTION:  Init the DDR2.  
*
* Inputs
*     hPci  -  Handle for PCI access
*     freq  -  Memory speed
*     wide  -  0 = 16 bit,  1 = 32 bit
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/
static char *DDR2Init (PCI64_HANDLE hPci, int freq, int wide) {
    ADT_UInt32 Value;
    ADT_UInt32 pch_nch;

    int widthBit;

	if (wide) {
	   widthBit = 0;
    } else {
	   widthBit = 0x4000;
    }
    //  Step 1 - Setup PLL2
    //  Step 2 - Enable DDR2 PHY
    psc_change_state (hPci, 13, 3);

    //  Step 3 - DDR2 Initialization
    Value = 0x50006405;
    PCI64_RegWrite (hPci, DDR_DDRPHYCR, &Value);     // DLL powered, ReadLatency=5

    Value = 0x00138822 | widthBit;
    PCI64_RegWrite (hPci, DDR_SDBCR, &Value) ;         // DDR Bank: xx-bit bus, CAS=4,
                                                       // 4 banks, 1024-word pg
      // DDR Timing
    if (freq == 135)  {
       Value = 0x14492148;
       PCI64_RegWrite (hPci, DDR_SDTIMR, &Value);
       Value = 0x000bc702;
       PCI64_RegWrite (hPci, DDR_SDTIMR2, &Value);
    } else { // Default to 162 MHz
       Value = 0x000bc702;
       PCI64_RegWrite (hPci, DDR_SDTIMR, &Value);
       Value = 0x000cc702;
       PCI64_RegWrite (hPci, DDR_SDTIMR2, &Value);
    }
    
    Value = 0x00130822 | widthBit;
    PCI64_RegWrite (hPci, DDR_SDBCR, &Value);      // DDR Bank: cannot modify
    Value = (ADT_UInt32) freq * 7.8;
    PCI64_RegWrite (hPci, DDR_SDRCR, &Value);      // Refresh Control [ 7.8 usec * freq ]

    //  Step 4 - Dummy Read from DDR2
    PCI64_RegRead (hPci, 0x80000000UL, &Value);

    //  Step 5 - Soft Reset (SYNCRESET followed by ENABLE) of DDR2 PHY
    psc_change_state (hPci, 13, 1);
    psc_change_state (hPci, 13, 3);
      
    //  Step 6 - Enable VTP calibration
    //  Step 7 - Wait for VTP calibration (33 VTP cycles)
    Value = 0x201f;
    PCI64_RegWrite (hPci, DDR_VTPIOCR, &Value);
    Value = 0xa01f;
    PCI64_RegWrite (hPci, DDR_VTPIOCR, &Value);
    Sleep (1500);

    //  Step 8 - Enable access to DDR VTP reg
    //  Step 9 - Reat P & N channels
    //  Step 10 - Set VTP fields PCH & NCH
    Value = 1;
    PCI64_RegWrite (hPci, DDR_DDRVTPER, &Value);
    PCI64_RegRead  (hPci, DDR_DDRVTPR, &pch_nch);
    pch_nch &= 0x3ff;
    pch_nch |= 0xa000;
    PCI64_RegWrite (hPci, DDR_VTPIOCR, &Value);

    //  Step 11 - Disable VTP calibaration
    //          - Disable access to DDR VTP register
    PCI64_RegWrite (hPci, DDR_VTPIOCR, &Value);
    Value &= ~0x2000;
    PCI64_RegWrite (hPci, DDR_VTPIOCR, &Value);

    Value = 0;
    PCI64_RegWrite (hPci, DDR_DDRVTPER, &Value);
    return NULL;
}



/****************************************************************************
* NAME:  GenerateInterruptTo64 ()
*
* DESCRIPTION:  Generate interrupts to DSP via PCI 
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    Nothing
*
****************************************************************************/
static void GenerateInterruptTo64 (ADT_Int32 DspId) {

   ADT_UInt32          Hsr;

    if ((pciHandle == NULL) || (DspId != 0)) return;


    Hsr = HDCR_DSPINT;   // Enable interrupts
    PCI64_RegWrite32 (pciHandle, PCI64_HDCR, 1, &Hsr);

    DSPInterrupts++;

    return;
}

/****************************************************************************
* NAME:  GenerateInterruptTo6424 ()
*
* DESCRIPTION:  Generate interrupts to DSP via PCI 
*         
* Inputs
*     hPci  -  Handle for PCI access
*
*
****************************************************************************/
static void GenerateInterruptTo6424 (ADT_Int32 DspId) {

   ADT_UInt32          Hsr;

    if ((pciHandle == NULL) || (DspId != 0)) return;


    Hsr = DSP_INTS;   // Enable interrupts
    PCI64_RegWrite (pciHandle, PCI64_INT_GEN, &Hsr);

    return;
}

/****************************************************************************
* NAME:  GenerateInterruptToDSP ()
*
* DESCRIPTION:  Generate interrupts to DSP via PCI 
*         
* Inputs
*     hPci  -  Handle for PCI access
*
****************************************************************************/
void GenerateInterruptToDSP (ADT_Int32 DspId) {

   if (PCIDsp == 642)
      GenerateInterruptTo64 (DspId);
   else if (PCIDsp == 6424);
      GenerateInterruptTo6424 (DspId);
}


/****************************************************************************
* NAME:  PCIConnect ()
*
* DESCRIPTION:  Establish communication to DSP via PCI 
*         
* Inputs
*     Dsp  -  Identifier for multiple DSPs
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/
char *PCIConnect (ADT_Int32 Dsp) {


   if (intHandle != NULL)  {    // Close old PCI connection
      PCI64_Close (intHandle);
      intHandle = NULL;
   }

   if (pciHandle != NULL)  {    // Close old PCI connection
      PCI64_Close (pciHandle);
      pciHandle = NULL;
   }

	if (PCI64_Open (Dsp, &intHandle))     // Open new interrupt PCI connection
 	   return "PCI bus open failure";

   if (PCI64_Open (Dsp, &pciHandle)) {    // Open new PCI connection
      PCI64_Close (intHandle);
      intHandle = NULL;
 	   return "PCI bus open failure";
   }

   hCallbackEvent = PCI64_GetCallbackEvent (pciHandle);
   PCI64_SetupCallbackEvent (pciHandle, DSPCallback);

   return EnableInterruptsFromDSP (pciHandle);

   
}

/****************************************************************************
* NAME:  PCIDisconnect ()
*
* DESCRIPTION:  Release DSP Connection
*         
* Inputs
*     Dsp  -  Identifier for multiple DSPs
*
*
****************************************************************************/
void PCIDisconnect (ADT_Int32 Dsp) {

   char *Error = NULL;

   if (pciHandle == NULL) return;

   DisableInterruptsFromDSP (pciHandle);

   PCI64_Close (pciHandle);
   pciHandle = NULL;

   PCI64_Close (intHandle);
   intHandle = NULL;
   
   hCallbackEvent = NULL;
}


/****************************************************************************
* NAME:  PCIInitEmif ()
*
* DESCRIPTION:  Init the EMIF.  Just handles EMIF-A.
*
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/
char *PCIInitEMIF (ADT_Int32 Dsp) {

    return EmifInit (pciHandle);
}

/****************************************************************************
* NAME:  PCIInitDDR2 ()
*
* DESCRIPTION:  Init the DDR.
*
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/
char *PCIInitDDR2 (PCI64_HANDLE hPci, int freq, int width) {

    return DDR2Init (hPci, freq, width);
}


/****************************************************************************
* NAME:  PCIReset64 ()
*
* DESCRIPTION:  Reset and release DSP after establishing communications to DSP via PCI 
*         
* Inputs
*     Dsp  -  Identifier for multiple DSPs
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/
static char *PCIReset64 (ADT_Int32 Dsp) {

	char *Error = NULL;

   Error = IsPciBootMode (pciHandle);  // Verify board allows PCI Boot
   if (Error != NULL) {
      PCI64_Close (pciHandle);
      PCI64_Close (intHandle);
      pciHandle = NULL;
      intHandle = NULL;
      return Error;
   }

   Error = AssertDspReset (pciHandle);  // Reset DSP
   if (Error != NULL) {
      PCI64_Close (pciHandle);
      PCI64_Close (intHandle);
      pciHandle = NULL;
      intHandle = NULL;
      return Error;
   }

   Error = EmifInit (pciHandle);     // Initialize external memory I/F registers
   if (Error != NULL) {
      PCI64_Close (pciHandle);
      PCI64_Close (intHandle);
      pciHandle = NULL;
      intHandle = NULL;
      return Error;
   }

   // Release DSP from reset
   Error = ReleaseDSP (pciHandle);
   if (Error != NULL) {
      PCI64_Close (pciHandle);
      PCI64_Close (intHandle);
      pciHandle = NULL;
      intHandle = NULL;
      return Error;
   }
   return NULL;

}

/****************************************************************************
* NAME:  PCIReset6424 ()
*
* DESCRIPTION:  Reset and release DSP after establishing communications to DSP via PCI 
*         
* Inputs
*     Dsp  -  Identifier for multiple DSPs
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/
static char *PCIReset6424 (ADT_Int32 Dsp) {

	char *Error = NULL;

   Error = AssertDspReset (pciHandle);  // Reset DSP
   if (Error != NULL) {
      PCI64_Close (pciHandle);
      PCI64_Close (intHandle);
      pciHandle = NULL;
      intHandle = NULL;
      hCallbackEvent = NULL;
      return Error;
   }

   Error = DDR2Init (pciHandle, 162, 1);     // Initialize external memory I/F registers
   if (Error != NULL) {
      PCI64_Close (pciHandle);
      PCI64_Close (intHandle);
      pciHandle = NULL;
      intHandle = NULL;
      hCallbackEvent = NULL;
      return Error;
   }

   // Release DSP from reset
   Error = ReleaseDSP (pciHandle);
   if (Error != NULL) {
      PCI64_Close (pciHandle);
      PCI64_Close (intHandle);
      pciHandle = NULL;
      intHandle = NULL;
      hCallbackEvent = NULL;
      return Error;
   }
   return NULL;

}

/****************************************************************************
* NAME:  PCIReset ()
*
* DESCRIPTION:  Reset and release DSP after establishing communications to DSP via PCI 
*         
* Inputs
*     Dsp  -  Identifier for multiple DSPs
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/
char *PCIReset (ADT_Int32 Dsp) {
   if (PCIDsp == 642)
      return PCIReset64 (Dsp);
   else if (PCIDsp == 6424);
      return PCIReset6424 (Dsp);
}


/****************************************************************************
* NAME:  PCIHalt ()
*
* DESCRIPTION:  Halt DSP
*         
* Inputs
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/
char *PCIHalt (ADT_Int32 Dsp) {
   return AssertDspReset (pciHandle);  // Reset DSP
}

/****************************************************************************
* NAME:  PCIRelease ()
*
* DESCRIPTION:  Release DSP
*         
* Inputs
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/
char *PCIRelease (ADT_Int32 Dsp) {
   return ReleaseDSP (pciHandle);  // Release DSP from reset
}


/****************************************************************************
* NAME:  PCIDownloadCoff ()
*
* DESCRIPTION:  Download coff file to DSP 
*         
* Inputs
*     Dsp  -  Identifier for multiple DSPs
*    Filename - path and name of Coff file
*
* RETURN VALUE:
*    GdlSuccess      - Success
*    GdlInvalidFile  - Failure
*
****************************************************************************/
int PCIDownloadCoff (char *Filename) {

   
   if (!PCI64_LoadCoff (pciHandle, Filename))
      return 0;
   
   return 9;

}



//=============================================================================
//
//  Support for GPAK APIs
//
//==============================================================================

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadDspMemory32 - Read DSP memory.
 *
 * FUNCTION
 *  Reads a contiguous block of 32-bit values from DSP memory 
 *  starting at the specified address.
 *
 * Inputs
 *     Dsp  -  Identifier for multiple DSPs
 *    DspAddress - DSP memory starting address
 *    LongCnt       - Number of 32-bit memory values to transfer
 *
 * Outputs
 *    Buff       - Buffer to receive values
 *
 * RETURNS
 *  nothing
 *
 */
#define PGSZ 0x100000

void gpakReadDspMemory32 (ADT_Int32 DspId, DSP_Address DspAddress, ADT_UInt32 LongCnt, ADT_UInt32 *Buff) {
    int Error;
    ADT_UInt32 I32ToPageEnd;
    ADT_UInt32 XferCnt;
    ADT_UInt32 XferAddr;
    
    if ((pciHandle == NULL) || (DspId != 0)) return;

    XferAddr = (ADT_UInt32) DspAddress;
    
    while (LongCnt) {
       I32ToPageEnd = (PGSZ - (XferAddr & (PGSZ-1))) >> 2;
       if (I32ToPageEnd < LongCnt) XferCnt = I32ToPageEnd;
       else                        XferCnt = LongCnt;
       Error = PCI64_MemRead32 (pciHandle, XferAddr, XferCnt, Buff);
       if (Error) SystemError ();
       Buff    += XferCnt;
       XferAddr+= XferCnt * 4;
       LongCnt -= XferCnt;
    }
    return;

}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakWriteDspMemory32 - Write DSP memory.
 *
 * FUNCTION
 *  Writes a contiguous block of 32-bit values to DSP memory  starting at the specified address.
 *
 * Inputs
 *     Dsp  -  Identifier for multiple DSPs
 *    DspAddress - DSP memory starting address
 *    LongCnt        - Number of 32-bit memory values to transfer
 *    Buff       - Buffer of values to write
 *
 * RETURNS
 *  nothing
 *
 */
void gpakWriteDspMemory32 (ADT_Int32 DspId, DSP_Address DspAddress, ADT_UInt32 LongCnt,  ADT_UInt32 *Buff) {
    int Error;
    ADT_UInt32 I32ToPageEnd;
    ADT_UInt32 XferCnt;
    ADT_UInt32 XferAddr;
    
    if ((pciHandle == NULL) || (DspId != 0)) return;

    XferAddr = (ADT_UInt32) DspAddress;
    
    while (LongCnt) {
       I32ToPageEnd = (PGSZ - (XferAddr & (PGSZ-1))) >> 2;
       if (I32ToPageEnd < LongCnt) XferCnt = I32ToPageEnd;
       else                        XferCnt = LongCnt;
       Error = PCI64_MemWrite32 (pciHandle, XferAddr, XferCnt, Buff);
       if (Error) SystemError ();
       Buff    += XferCnt;
       XferAddr+= XferCnt * 4;
       LongCnt -= XferCnt;
    }
    return;
}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadDspNoSwap32 - Read DSP memory.
 *
 * FUNCTION
 *  Reads a contiguous block of 32-bit values from DSP memory 
 *  starting at the specified address.
 *
 * Inputs
 *     Dsp  -  Identifier for multiple DSPs
 *    DspAddress - DSP memory starting address
 *    LongCnt       - Number of 32-bit memory values to transfer
 *
 * Outputs
 *    Buff       - Buffer to receive values
 */
void gpakReadDspNoSwap32 (ADT_Int32 DspId, DSP_Address DspAddress, ADT_UInt32 LongCnt, ADT_UInt32 *Buff) {

    gpakReadDspMemory32 (DspId, DspAddress, LongCnt, Buff);
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakWriteDspNoSwap32 - Write DSP memory.
 *
 * FUNCTION
 *  Writes a contiguous block of 32-bit values to DSP memory  starting at the specified address.
 *
 * Inputs
 *     Dsp  -  Identifier for multiple DSPs
 *    DspAddress - DSP memory starting address
 *    LongCnt        - Number of 32-bit memory values to transfer
 *    Buff       - Buffer of values to write
 *
 */
void gpakWriteDspNoSwap32 (ADT_Int32 DspId, DSP_Address DspAddress, ADT_UInt32 LongCnt,  ADT_UInt32 *Buff) {
    gpakWriteDspMemory32 (DspId, DspAddress, LongCnt,  Buff);
}


void gpakReadDspMemory16 (ADT_Int32 DspId, DSP_Address DspAddress, ADT_UInt32 WordCnt, ADT_UInt16 *Buff) {
	printf ("Read16 unimplemented %x [%d]\n", DspAddress, WordCnt);
   return ;
}

void gpakWriteDspMemory16 (ADT_Int32 DspId, DSP_Address DspAddress, ADT_UInt32 WordCnt,  ADT_UInt16 *Buff) {
	printf ("Write16 unimplemented  %x [%d]\n",  DspAddress, WordCnt);

   return;
}



/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakHostDelay - Delay for a fixed time interval.
 *
 * FUNCTION
 *  Delays for a fixed time interval before returning. The time
 *  interval is the Host Port Interface sampling period when polling a DSP for
 *  replies to command messages.
 *
 */
void gpakHostDelay (void) {
/* This function needs to be implemented by the G.PAK system integrator. */
   Sleep (2);
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakLockAccess - Lock access to the specified DSP.
 *
 * FUNCTION
 *  Aquires exclusive access to the specified DSP.
 *
 */
void gpakLockAccess (ADT_Int32 DspId) {

   if (DspId != 0) return;

   if (mutex == NULL)
      mutex = CreateMutex (NULL, FALSE, "Data available");

   WaitForSingleObject (mutex, INFINITE);

   while (Locked);
   Locked = TRUE;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakUnlockAccess - Unlock access to the specified DSP.
 *
 * FUNCTION
 *  Releases exclusive access to the specified DSP.
 *
 */
void gpakUnlockAccess (ADT_Int32 DspId) {
   
   if (DspId != 0) return;
   
   while (!Locked);
   
   Locked = FALSE;
   if (mutex) ReleaseMutex (mutex);
}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadFile - Read a block of bytes from a G.PAK Download file.
 *
 * FUNCTION
 *  Reads a contiguous block of bytes from a G.PAK Download file
 *  starting at the current file position.
 *
 * Inputs
 *   FileId -  Download file identifier
 *   pBuffer - Byte storage buffer
 *   ByteCnt     - Number of bytes to transfer
 *
 * RETURNS
 *  The number of bytes read from the file.
 *   -1 indicates an error occurred.
 *    0 indicates all bytes have been read (end of file)
 *
 */
int gpakReadFile (GPAK_FILE_ID FileId, ADT_UInt8 *pBuffer, ADT_UInt32 ByteCnt) {

    return fread (pBuffer, sizeof(char), ByteCnt, (FILE *) FileId);
}


