// GpakApiTestSupport.c
//
//  API Test Support
#ifndef GpakApiTestSupport_h
#define GpakApiTestSupport_h

#include <stdio.h>

#if (ADT_DEVELOPMENT)
   #include "GpakCustWin32.h"
#else
   #include "GpakCust.h"
#endif

#include "GpakApi.h"
#include "sysconfig.h"

extern GpakSystemParms_t  sysParams;
extern GpakSystemConfig_t sysCfg;

typedef enum loopBack {
  NO_LOOP = 1, LOOPBACK, CROSSOVER, DSP_LOOP
} LoopbackMode;

extern LoopbackMode TDM_loopback;
extern LoopbackMode hostLoopBack;
extern int TDM_crossoverbit;

#define VAD_ON   ADT_TRUE
#define VAD_OFF  ADT_FALSE

//------------------------------
//  Per channel configuration structures
typedef struct TDMDef {
   GpakSerialPort_t port;
   ADT_UInt16 *inSlot;
   ADT_UInt16 *outSlot;
} TDMDef;

typedef struct PKTDef {
   GpakCodecs      decode;
   GpakFrameHalfMS decodeHalfMS;
   GpakCodecs      encode;
   GpakFrameHalfMS encodeHalfMS;
   GpakCodecs      RTP;
} PKTDef;

typedef enum ECTypes  { PCM_EC, PKT_EC, AEC, NO_EC } ECTypes;
#define NO_VAD  Disabled
#define VAD_CNG Enabled

typedef struct CompDef {
   GpakActivation AGC;
   GpakActivation VAD;
   ECTypes  EC;
} CompDef;

// Channel definition for channel setup tables
typedef struct chanDef {
   struct ID { 
      ADT_UInt16      Chan;
      ADT_UInt16      Cnfr;
      GpakChanType    ChanType;
      char*           Test;
      GpakFrameHalfMS CnfrHalfMS;
   } ID;

   TDMDef TDMA;   // TDM Details
   TDMDef TDMB;

   PKTDef Pkt;          // Pkt details
   CompDef Components;  // Component definitions
} chanDef;

#define LEFT_SLOT  &leftSlt
#define RIGHT_SLOT &rightSlt
#define MIC_SLOT   &leftSlt

extern ADT_UInt16 leftSlt, rightSlt;

#define NULL_CNFR ((ADT_UInt16) -1)
#define NULL_CHAN ((ADT_UInt16) -1)

void setVADNotify  (ADT_UInt32 DspId, ADT_UInt16 chID, ADT_Bool enableVAD);


// -----------------------------------------------------------------------------
//{ Macros for channel definition tables
#define NO_PKT_5ms   { NullCodec, Frame_5ms }
#define NO_PKT_10ms  { NullCodec, Frame_10ms }
#define NO_PKT_20ms  { NullCodec, Frame_20ms }
#define NO_PKT_22ms  { NullCodec, Frame_22_5ms }
#define NO_PKT_30ms  { NullCodec, Frame_30ms }
#define NULL_TDM     { SerialPortNull, LEFT_SLOT, LEFT_SLOT }
#define LFT_TDM      { SerialPort1, LEFT_SLOT,  LEFT_SLOT }
#define RGT_TDM      { SerialPort1, RIGHT_SLOT, RIGHT_SLOT }
#define ALL_DISABLES { Disabled, NO_VAD, NO_EC }
#define PEC_TESTS    { Disabled, NO_VAD, PCM_EC }
#define NEC_TESTS    { Disabled, NO_VAD, PKT_EC }
#define AEC_TESTS    { Disabled, NO_VAD, AEC }
#define AGC_TESTS    { Enabled,  NO_VAD, NO_EC }
#define VADCNG_TESTS { Disabled, VAD_CNG, NO_EC }

#define DAISY_CHAIN_CNFR_START(ID,CNFR,RATE) { { PacketDaisyChain,  CNFR,   inactive, ID, RATE }, NULL_TDM, NULL_TDM, NO_PKT_5ms, ALL_DISABLES }  // Even/odd cross over setup
#define CROSSOVER_CNFR_START(ID,CNFR,RATE)   { { PacketCrossOver,   CNFR,   inactive, ID, RATE }, NULL_TDM, NULL_TDM, NO_PKT_5ms, ALL_DISABLES }  // Even/odd cross over setup
#define CROSSOVER_LIST_START(ID)       { PacketCrossOver,   NULL_CNFR,   inactive, ID }  // Even/odd cross over setup
#define DAISY_CHAIN_LIST_START(ID)     { PacketDaisyChain,  NULL_CNFR,   inactive, ID }  // Daisy chain seutp
#define LOOP_BACK_LIST_START(ID)       { PacketLoopBack,    NULL_CNFR,   inactive, ID }  // Same channel loop back setup
#define NO_LOOPBACK_LIST_START(ID)     { DisableTest,       NULL_CNFR,   inactive, ID }  // No DSP loop back
#define LIST_END                       { ((ADT_UInt16) -1), NULL_CNFR,   inactive     }  // Same channel loop back setup

//}

// ========================================
//  Channel configuration defaults
typedef struct chanCfg {
   LoopbackMode  loopback;
   GpakActivation PCM_EC;
   GpakActivation PKT_EC;
   GpakActivation AEC;
   GpakActivation AGC;
   GpakActivation VAD;
   GpakActivation NOISE;
   GpakFaxMode_t  FAX;

   GpakFrameHalfMS ENCSZ;
   GpakFrameHalfMS DECSZ;

   int TONEA;
   int TONEB;
   GpakCodecs ENCODE;
   GpakCodecs DECODE;
   GpakCodecs RTP_CODEC_PYLD;

   GpakCidMode_t CID;
   ADT_Int16 TG_GAIN;
   ADT_Int16 IN_GAIN;
   ADT_Int16 OUT_GAIN;
   
   GpakSerialPort_t TDM_PORT;
   GpakActivation NOISEB;
   GpakActivation AGCB;
} chanCfg;


extern chanCfg ActiveChan, DefaultChan;

// ========================================
// Conference configuration defaults
typedef struct cnfrCfg {
   GpakFrameHalfMS cnfrHalfMS;
   GpakToneTypes  InputToneTypes;
} cnfrCfg;
extern cnfrCfg ActiveCnfr[5], DefaultCnfr[5];

typedef struct chanCnts {
   int tdmTdm;
   int tdmPkt;
   int pktPkt;
   int tdmCnfr;
   int pktCnfr;
   int cmpCnfr;
} chanCnts;
extern chanCnts chns;

extern GpakSRTPCfg_t  SRTPCfg;    // SRTP configuration parameters

extern int         PlayRecI8;           // Total length of recording memory segment
extern DSP_Address PlayRecAddress;      // Base address of recording memory segment
extern DSP_Address playRecBuffers[5];   // Address of individual recording buffers

extern ADT_Int32 dspIP;
extern ADT_Int32 FirehoseIP;
extern ADT_Int32 BaseDestIP;
extern ADT_Int32 BaseMCastIP;
extern ADT_Int32 BaseSSRC;
extern ADT_Bool autoRTP, autoUsage, autoStats;

typedef struct netEnables {
   ADT_Bool vlan;         // Assign vlan index
   ADT_Bool multicastTx;  // Assign multicast IP address for transmissions
   ADT_Bool multicastRx;  // Assign multicast IP address of receptions
   ADT_Bool ARP;          // Force ARP requests (zero remote mac addresses)
   ADT_Bool PortCrossover;  // Assign IP address of DSP and port address to paired port.
   ADT_Bool disableRTPTx;
   ADT_Bool fireHoseEnable;
   ADT_Bool stereo;
} netEnables_t;
extern netEnables_t NetEnables;

// G.PAK custom API routine for initialization
extern void  initGpakStructures (void);

//-----------------------------------------
//{ Interactive channel setup support routines called by host application
extern int ADT_UpdateInt16Value (char *name, ADT_Int16 *value);

extern void dspTDMInit (ADT_UInt32 DspId);
extern gpakRtpStatus_t sendRTPConfigMsg (ADT_UInt32 DspId, ADT_UInt16 chID, GpakCodecs codec, GpakFrameHalfMS frameHalfMS);
extern void setSRTPCfgDefaults ();

extern void setTDMTDMDefaults  (GpakChannelConfig_t *chCfgData, ADT_UInt16 chID);
extern void setTDMPktDefaults  (GpakChannelConfig_t *chCfgData, ADT_UInt16 chID, ADT_UInt32 DspId);
extern void setPktPktDefaults  (GpakChannelConfig_t *chCfgData, ADT_UInt16 chID, ADT_UInt32 DspId);
extern void setTDMCnfrDefaults (GpakChannelConfig_t *chCfgData, ADT_UInt16 cfrId, ADT_UInt16 chID);
extern void setPktCnfrDefaults (GpakChannelConfig_t *chCfgData, ADT_UInt16 cfrID, ADT_UInt16 chID, GpakFrameHalfMS FrameHalfMS, ADT_UInt32 DspId);
extern void setCmpConfDefaults (GpakChannelConfig_t *chCfgData, ADT_UInt16 CfrID, ADT_UInt16 chID, ADT_UInt32 DspId);

extern void initTdmTdmChannels (ADT_UInt32 DspId, int count);
extern void initTdmPktChannels (ADT_UInt32 DspId, int count);
extern void initPktPktChannels (ADT_UInt32 DspId, int count);

extern void initChannels (ADT_UInt32 DspId);

extern void dspTearDown (ADT_UInt32 DspId);
extern void tearDownChannels (ADT_UInt32 DspId, int chanCnt, int encChannel);
extern void sendCIDMsg (ADT_UInt32 DspId);
extern void toneGenRequest (ADT_UInt32 DspId);
//}

//{  Main processing support routines
extern void processEvent (ADT_UInt32 Dsp, ADT_UInt16 chId, GpakAsyncEventCode_t EvtCode, GpakAsyncEventData_t *eventData);
extern void periodicStatusDisplay (ADT_UInt32 Dsp);
extern void userInterface (void);
extern void displayNetworkStats (void);
extern char *txIPFromChan (ADT_UInt16 Chan);
extern char *rxIPFromChan (ADT_UInt16 Chan);

extern ADT_Bool DSPPerformsRTP  (ADT_UInt32 Dsp);
extern void loopbackRTPPackets  (ADT_UInt32 Dsp);
extern void loopbackRTPPayloads (ADT_UInt32 Dsp);
extern void processDSPInterrupt ();
//}

//{  API wrappers
extern void startFilePlayback (ADT_UInt32 Dsp, ADT_UInt16 chId, int dev, int bufrID, unsigned int buffI8);
extern void setToneMode (ADT_UInt32 DspId, ADT_UInt16 chID, GpakDeviceSide_t device, GpakToneTypes toneFlags);
extern char recordVoice (ADT_UInt32 DspId, char *instructions, int bufrID, ADT_UInt16 chID, GpakDeviceSide_t device, 
                        GpakPlayRecordCmd_t mode, GpakToneCodes_t stopDigit);
extern void playbackVoice (ADT_UInt32 DspId, int bufrID, ADT_UInt16 chID, GpakDeviceSide_t device, GpakPlayRecordCmd_t mode);

extern void setMaxDominant (ADT_UInt32 DspId, int dominant);

extern void dspGetCoreUsage (ADT_UInt32 DspId);

extern void setDSPCrossover  (ADT_Int32 DspId);
extern void setDSPDaisyChain (ADT_Int32 DspId);
extern void setDSPLoopBack   (ADT_Int32 DspId);
extern void setDSPNoLoopBack (ADT_Int32 DspId);

extern void sendTestMsg (ADT_Int32 DspId, char *ID, GpakTestMode Mode);
extern void setCaptureSize  (ADT_Int32 DspId, ADT_UInt16 secs);

extern char setupConference (ADT_UInt32 DspId, ADT_UInt16 CnfrID, GpakFrameHalfMS HalfMS, ADT_Bool DTMFEnabled);
//}

//{  Canned tests for G.PAK APIs
extern void setupChannelsViaTable (ADT_UInt32 DspId, chanDef *chDef, GpakToneTypes tone);
extern void cannedGpakTests (ADT_UInt32 DspId);

extern char toneDetectTest (ADT_UInt32 DspId, chanDef *chanDefs);
extern char AGCTest (ADT_UInt32 DspId, chanDef *chanDefs);
extern char gainTest (ADT_UInt32 DspId, chanDef *chanDefs);
extern char toneGenTest (ADT_UInt32 DspId, chanDef *chanDefs);
extern char VADTest (ADT_UInt32 DspId, chanDef *chanDefs, ADT_UInt16 chID, ADT_Bool vadNotify);
extern char recordPlayTest (ADT_UInt32 DspId, chanDef *chanDefs);
extern char echoTest (ADT_UInt32 DspId, chanDef *chanDefs);
extern char toneRelayTest (ADT_UInt32 DspId, chanDef *chanDefs);
extern char insertTestTones (ADT_UInt32 DspId, ADT_UInt16 chID);
extern char channelGainTest (ADT_UInt32 DspId, ADT_UInt16 chID, GpakChanType chType);
extern void setAGCSettings (ADT_UInt32 DspId, GpakAGCParms_t *AGC_A, GpakAGCParms_t *AGC_B);
extern char processTestKeys (ADT_UInt32 DspId, chanDef *chDef);
extern char recordVoice (ADT_UInt32 DspId, char *instructions, int bufrID, ADT_UInt16 chID, GpakDeviceSide_t device, 
                  GpakPlayRecordCmd_t mode, GpakToneCodes_t stopDigit);

extern char pktPktTest (ADT_UInt32 DspId, chanDef *chDef);
extern char cnfrTest (ADT_UInt32 DspId, chanDef *chDef, ADT_Bool DTMFDetect);
extern char daisyTest  (ADT_UInt32 DspId, chanDef *chDef);
extern char bgTest (ADT_UInt32 DspId, chanDef *chDef, ADT_Bool DTMFDetect);
//}

extern GpakFrameHalfMS getCodecHalfMS (GpakCodecs Coding, GpakFrameHalfMS FrameHalfMS);
extern void appendChannelCodecs (GpakCodecs Coding, GpakFrameHalfMS FrameHalfMS, char *Buffer, size_t BufferI8);
extern void appendToneTypes (GpakToneTypes toneTypes, char *Buffer, size_t BufferI8);
extern void restoreChannelDefaults (void);
extern char getParams (void);
extern void displayChannelOpts (ADT_UInt32 DspId);

//-----------------------------------------
//{ Routines that host needs to provide to support
extern int  getSlot  (GpakSerialPort_t port); 
extern void freeSlot (GpakSerialPort_t port, int slot);
extern void freeAllSlots (GpakSerialPort_t port);
extern void appErr (int exitCode);
extern void displayNetworkStats (void);

extern int  ADT_printf (char *fmt, ...);
extern void ADT_sleep (int ms);  // One second between keyboard polling
extern char ADT_getchar (void);
extern void ADT_putchar (char key);
extern void ADT_gets (char *buff, int buffI8);
extern int  ADT_getint (char *prompt);

extern void (*customDiagnostics)(void);
extern void (*rtpProcess)(ADT_UInt32 DSP);
//}


// -----------------------------------------------------------------------------
// DSP interface
typedef struct RTP {
   struct rtpHdr {
      int vers:8;
      int pyldType:8;
      int sequence:16;
      ADT_UInt32 timeStamp;
      ADT_UInt32 SSRC;
   } RTP;
   ADT_UInt8 data[512];  // 30 ms * 160 samples + RTP header
} RTPPkt;

#endif
