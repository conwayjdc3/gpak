/* File Name: TrilogyDiag.c
 *
 * Description:
 *   This file contains functions to support host testing of Trilogy DSP code.
 *
 *
 */

// Application related header files.
#include <string.h>
#include <stdio.h>
#include "GpakApiTestSupport.h"

#include "GpakHpi.h"
#include "GpakErrs.h"
#include "GpakApi.h"
#include "RCApi.h"
#define BASE_MCAST_SSRC 0xE1000000
#define BASE_UCAST_SSRC 0x09020000

#define TEST_DEF(id) ADT_printf ("\n----------------------\n%s", id); TrilogyTests.ID.Test = id
#define ESC 'x'
extern void dspVLANDump (ADT_UInt32 DspId);
extern char (*setupChannels) (ADT_UInt32 DspId);
extern void displayToneString(int tone);

extern chanDef *testSuite;
chanDef TrilogyTests;
extern ADT_Int32  BaseMCastIP;
extern int UseCustomDestIP;
extern ADT_Int32  BaseSSRC;
extern ADT_UInt32 CustomDestSSRC;
extern ADT_UInt32 CustomSSRC;
extern int UseCustomSSRC;
extern ADT_UInt32 RemoteIP [];

ADT_UInt8 rxHdr[6] = { 9, 2, 0, 0, 0x7b, 0 };
ADT_UInt8 txHdr[6] = { 9, 2, 0, 0, 0x7b, 0 };

ADT_UInt8 mcRxHdr[6] = { 0, 0, 0, 0, 0xe, 0 };
ADT_UInt8 mcTxHdr[6] = { 0, 0, 0, 0, 0xe, 0 };
int rxMcastChan = 0;

int startChan, mcStartChan, endChan;

void dspVLANInit (ADT_UInt32 DspId) {
   GpakApiStatus_t apiStat;
   GPAK_VlanStat_t dspStat;
   ADT_UInt16 idx;

   apiStat = gpakSetVlanTag (DspId, 10, 1, 0, &dspStat);
   if ((apiStat != GpakApiParmError) || (dspStat != vlanIdxOutOfRange))
      ADT_printf ("VLAN index out of range check error %d.%d.%d\n", apiStat, dspStat, DSPError[DspId]);

   apiStat = gpakSetVlanTag (DspId, 0, 2, 8, &dspStat);
   if ((apiStat != GpakApiParmError) || (dspStat != vlanPriorityOutOfRange))
      ADT_printf ("VLAN priority out of range check error %d.%d.%d\n", apiStat, dspStat, DSPError[DspId]);

   apiStat = gpakSetVlanTag (DspId, 0, 0x1000, 0, &dspStat);
   if ((apiStat != GpakApiParmError) || (dspStat != vlanIDOutOfRange))
      ADT_printf ("VLAN ID out of range check error %d.%d.%d\n", apiStat, dspStat, DSPError[DspId]);

   for (idx = 0; idx < 10; idx++) {
      apiStat = gpakSetVlanTag (DspId, idx, (ADT_UInt16) ((idx+1)<<8), (ADT_UInt8) ((idx+1)&7), &dspStat);
      if ((apiStat != GpakApiSuccess) || (dspStat != vlanSuccess))
         ADT_printf ("VLAN.%d Setup error %d.%d.%d\n", idx, apiStat, dspStat, DSPError[DspId]);
   }

   apiStat = gpakSetVlanTag (DspId, 5, 2<<8, 2, &dspStat);
   if ((apiStat != GpakApiParmError) || (dspStat != vlanDuplicate))
      ADT_printf ("VLAN duplicate error %d.%d.%d\n", apiStat, dspStat, DSPError[DspId]);

}
void dspVLANDump (ADT_UInt32 DspId) {
   GpakApiStatus_t apiStat;
   GPAK_VlanStat_t dspStat;
   ADT_UInt16 idx, vlanID;
   ADT_UInt8  priority;

   ADT_printf ("\nIdx  prior  ID\n");
   for (idx = 0; idx < 10; idx++) {
      apiStat = gpakGetVlanTag (DspId, idx, &vlanID, &priority, &dspStat);
      if ((apiStat != GpakApiSuccess) || (dspStat != vlanSuccess))
         ADT_printf ("VLAN get tag error %d.%d.%d\n", apiStat, dspStat, DSPError[DspId]);
      else
         ADT_printf ("%3u %4u   %03x\n", idx, priority, vlanID);
   }
   ADT_printf ("\n");
   return;
}

void setupPreHeaders (ADT_UInt32 DspId) {
   GpakApiStatus_t apiStat;
   preHdrStat_t    dspStat;
   ADT_UInt16 i;

   // Setup unicast channels
   for (i=0; i<endChan; i++) {
      txHdr[3] = (ADT_UInt8) (i + 2);
      rxHdr[3] = (ADT_UInt8) (i + 2) ^ 1;
      dspStat = preHdrSuccess;
      apiStat = gpakPreRTPHeader (DspId, i, sizeof (rxHdr), rxHdr, txHdr, &dspStat);
      if ((apiStat == GpakApiSuccess) && (dspStat == preHdrSuccess)) continue;

      ADT_printf ("Channel %u pre-header failure %u:%u:%u\n", i, apiStat, dspStat, DSPError [DspId]);
      break;
   }
}

void setupMCPreHeaders (ADT_UInt32 DspId) {
   GpakApiStatus_t apiStat;
   preHdrStat_t    dspStat;
   ADT_UInt16 i;

   // Setup multicast channels
   for (i=startChan; i<endChan; i++) {
      mcTxHdr[3] = (ADT_UInt8) (i + 2);
      mcRxHdr[3] = (ADT_UInt8) (i + 2) ^ 1;
      dspStat = preHdrSuccess;
      apiStat = gpakPreRTPHeader (DspId, i, (ADT_UInt16) sizeof (rxHdr), mcRxHdr, mcTxHdr, &dspStat);
      if ((apiStat == GpakApiSuccess) && (dspStat == preHdrSuccess)) continue;

      ADT_printf ("\nChannel %u pre-header failure %u:%u:%u\n", i, apiStat, dspStat, DSPError [DspId]);
      return;
   }

}

void clearPreHeaders (ADT_UInt32 DspId) {
   GpakApiStatus_t apiStat;
   preHdrStat_t    dspStat;
   ADT_UInt16 i;

   //  channels
   for (i=startChan; i<endChan; i++) {
      dspStat = preHdrSuccess;
      apiStat = gpakPreRTPHeader (DspId, i, 0, mcRxHdr, mcTxHdr, &dspStat);
      if ((apiStat == GpakApiSuccess) && (dspStat == preHdrSuccess)) continue;

      ADT_printf ("\nChannel %u pre-header failure %u:%u:%u\n", i, apiStat, dspStat, DSPError [DspId]);
      return;
   }
}

char trilogyPktChannelSetup (ADT_UInt32 DspId) {
   int chanCnt;

   chanCnt = ADT_getint ("\nChannel count: ");
   ADT_printf ("\n");

   if (chanCnt == 0) return ESC;
   if (chanCnt < 0) {
      dspTearDown (DspId);
      chanCnt = -1*chanCnt;
      sysCfg.ActiveChannels = 0;
   }
   startChan = sysCfg.ActiveChannels;
   endChan   = startChan + chanCnt;

   clearPreHeaders (DspId);

   chns.tdmTdm  = 0;
   chns.tdmPkt  = chanCnt;
   chns.pktPkt  = 0;
   chns.tdmCnfr = 0;
   chns.pktCnfr = 0;
   chns.cmpCnfr = 0;

   initChannels (DspId);
   return ' ';
}

char trilogyTdmChannelSetup (ADT_UInt32 DspId) {
   int chanCnt;

   chanCnt = ADT_getint ("\nChannel count: ");
   ADT_printf ("\n");

   if (chanCnt == 0) return ESC;
   if (chanCnt < 0) {
      dspTearDown (DspId);
      chanCnt = -1*chanCnt;
      sysCfg.ActiveChannels = 0;
   }
   startChan = sysCfg.ActiveChannels;
   endChan   = startChan + chanCnt;

   chns.tdmTdm  = chanCnt;
   chns.tdmPkt  = 0;
   chns.pktPkt  = 0;
   chns.tdmCnfr = 0;
   chns.pktCnfr = 0;
   chns.cmpCnfr = 0;

   initChannels (DspId);
   return ' ';
}

void algorithmTests (ADT_UInt32 DspId) {
   char key;
   dspTearDown (DspId);

   TEST_DEF ("AGC testing.\n");
   restoreChannelDefaults();
   ActiveChan.DECODE = G729AB; 
   ActiveChan.DECSZ  = Frame_30ms;
   ActiveChan.ENCODE = G729AB;
   ActiveChan.ENCSZ  = Frame_30ms;
   ActiveChan.AGC    = Enabled;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;
   dspTearDown (DspId);

   TEST_DEF ("VAD/CNG testing.\n");
   restoreChannelDefaults();
   ActiveChan.DECODE = PCMU_64; 
   ActiveChan.DECSZ  = Frame_20ms;
   ActiveChan.ENCODE = PCMU_64;
   ActiveChan.ENCSZ  = Frame_20ms;
   ActiveChan.VAD    = Enabled;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;
   dspTearDown (DspId);

   TEST_DEF ("Tone generation");
   restoreChannelDefaults();
   ActiveChan.DECODE = G729AB; 
   ActiveChan.DECSZ  = Frame_30ms;
   ActiveChan.ENCODE = G729AB;
   ActiveChan.ENCSZ  = Frame_30ms;
   ActiveChan.TONEA  = Tone_Generate | DTMF_tone | Notify_Host;
   ActiveChan.TONEB  = Tone_Generate | DTMF_tone | Notify_Host;
   if (setupChannels (DspId) == ESC) return;
   do { toneGenRequest (DspId); } while ((key = processTestKeys (DspId, NULL)) == '+');
   if (key == ESC) return;

   TEST_DEF ("Tone detection.  Play DTMF tones and verify host receives notification.\n");
   if (processTestKeys (DspId, NULL) == ESC) return;
   dspTearDown (DspId);
}

void callerIDTests (ADT_UInt32 DspId) {
   char key;
   int  frameMs;

   frameMs = ADT_getint ("\nFrame size(ms): ");
   if (frameMs == 0) frameMs = 10;
   
   restoreChannelDefaults();
   ActiveChan.DECODE = PCMU_64; 
   ActiveChan.DECSZ  =  (GpakFrameHalfMS) (frameMs * 2);
   ActiveChan.ENCODE = PCMU_64;
   ActiveChan.ENCSZ  =  (GpakFrameHalfMS) (frameMs * 2);
   ActiveChan.CID    = (GpakCidMode_t) (CidReceive | CidTransmit);

   if (setupChannels (DspId) == ESC) return;

   TEST_DEF ("Caller ID transmission testing.\n");
   do { sendCIDMsg (DspId); } while ( (key = processTestKeys (DspId, NULL)) == '+');
   if (key == ESC) return;
   ActiveChan.CID    = CidDisabled;
   dspTearDown (DspId);
}

void vocoderTests (ADT_UInt32 DspId) {
   TEST_DEF ("L16 channels");
   ActiveChan.DECODE = L16;
   ActiveChan.DECSZ  = Frame_10ms;
   ActiveChan.ENCODE = L16; 
   ActiveChan.ENCSZ  = Frame_10ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;

   TEST_DEF ("L16_WB channels");
   ActiveChan.DECODE = L16_WB;
   ActiveChan.DECSZ  = Frame_10ms;
   ActiveChan.ENCODE = L16_WB; 
   ActiveChan.ENCSZ  = Frame_10ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;

   TEST_DEF ("G723_63 channels");
   ActiveChan.DECODE = G723_63; 
   ActiveChan.DECSZ  = Frame_30ms;
   ActiveChan.ENCODE = G723_63; 
   ActiveChan.ENCSZ  = Frame_30ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;

   TEST_DEF ("G723_63A channels");
   ActiveChan.DECODE = G723_63A; 
   ActiveChan.DECSZ  = Frame_30ms;
   ActiveChan.ENCODE = G723_63A; 
   ActiveChan.ENCSZ  = Frame_30ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;

   TEST_DEF ("G723_53A (VAD) channels");
   ActiveChan.DECODE = G723_53A; 
   ActiveChan.DECSZ  = Frame_30ms;
   ActiveChan.ENCODE = G723_53A; 
   ActiveChan.ENCSZ  = Frame_30ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;

   TEST_DEF ("G723_53 channels");
   ActiveChan.DECODE = G723_53; 
   ActiveChan.DECSZ  = Frame_30ms;
   ActiveChan.ENCODE = G723_53; 
   ActiveChan.ENCSZ  = Frame_30ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;

   TEST_DEF ("G726-16 kb channels");
   ActiveChan.DECODE = G726_16; 
   ActiveChan.DECSZ  = Frame_10ms;
   ActiveChan.ENCODE = G726_16; 
   ActiveChan.ENCSZ  = Frame_10ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;

   TEST_DEF ("G726-24 kb channels");
   ActiveChan.DECODE = G726_24; 
   ActiveChan.DECSZ  = Frame_10ms;
   ActiveChan.ENCODE = G726_24; 
   ActiveChan.ENCSZ  = Frame_10ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;

   TEST_DEF ("G726-32 kb channels");
   ActiveChan.DECODE = G726_32; 
   ActiveChan.DECSZ  = Frame_10ms;
   ActiveChan.ENCODE = G726_32; 
   ActiveChan.ENCSZ  = Frame_10ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;

   ADT_printf ("\nG726-40 kb channels");
   ActiveChan.DECODE = G726_40; 
   ActiveChan.DECSZ  = Frame_10ms;
   ActiveChan.ENCODE = G726_40; 
   ActiveChan.ENCSZ  = Frame_10ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;

   TEST_DEF ("PCMU channels");
   ActiveChan.DECODE = PCMU_64;
   ActiveChan.DECSZ  = Frame_10ms;
   ActiveChan.ENCODE = PCMU_64; 
   ActiveChan.ENCSZ  = Frame_10ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;

   TEST_DEF ("G729 channels");
   ActiveChan.DECODE = G729; 
   ActiveChan.DECSZ  = Frame_10ms;
   ActiveChan.ENCODE = G729; 
   ActiveChan.ENCSZ  = Frame_10ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;

   TEST_DEF ("G722 channels");
   restoreChannelDefaults();
   ActiveChan.DECODE = G722; 
   ActiveChan.DECSZ  = Frame_10ms;
   ActiveChan.ENCODE = G722; 
   ActiveChan.ENCSZ  = Frame_10ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;
 
   TEST_DEF ("G729AB channels");
   ActiveChan.DECODE = G729AB; 
   ActiveChan.DECSZ  = Frame_30ms;
   ActiveChan.ENCODE = G729AB; 
   ActiveChan.ENCSZ  = Frame_30ms;
   setupChannels (DspId);

   TEST_DEF ("G729 channels");
   ActiveChan.DECODE = G729; 
   ActiveChan.DECSZ  = Frame_20ms;
   ActiveChan.ENCODE = G729; 
   ActiveChan.ENCSZ  = Frame_20ms;
   setupChannels (DspId);
   return;
}

void packetRateTests (ADT_UInt32 DspId) {
{  // G729
   TEST_DEF ("G729 10ms channels");
   ActiveChan.DECODE = G729;
   ActiveChan.DECSZ  = Frame_10ms;
   ActiveChan.ENCODE = G729;
   ActiveChan.ENCSZ  = Frame_10ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;

   TEST_DEF ("G729 20ms channels");
   ActiveChan.DECODE = G729;
   ActiveChan.DECSZ  = Frame_20ms;
   ActiveChan.ENCODE = G729;
   ActiveChan.ENCSZ  = Frame_20ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;

   TEST_DEF ("G729 30ms channels");
   ActiveChan.DECODE = G729;
   ActiveChan.DECSZ  = Frame_20ms;
   ActiveChan.ENCODE = G729;
   ActiveChan.ENCSZ  = Frame_20ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;

   TEST_DEF ("G729 60ms channels");
   ActiveChan.DECODE = G729;
   ActiveChan.DECSZ  = Frame_60ms;
   ActiveChan.ENCODE = G729;
   ActiveChan.ENCSZ  = Frame_60ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;

   TEST_DEF ("G729 80ms channels");
   ActiveChan.DECODE = G729;
   ActiveChan.DECSZ  = Frame_80ms;
   ActiveChan.ENCODE = G729;
   ActiveChan.ENCSZ  = Frame_80ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;
}
{  // G729AB
   TEST_DEF ("G729AB 10ms channels");
   ActiveChan.DECODE = G729AB;
   ActiveChan.DECSZ  = Frame_10ms;
   ActiveChan.ENCODE = G729AB;
   ActiveChan.ENCSZ  = Frame_10ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;

   TEST_DEF ("G729AB 20ms channels");
   ActiveChan.DECODE = G729AB;
   ActiveChan.DECSZ  = Frame_20ms;
   ActiveChan.ENCODE = G729AB;
   ActiveChan.ENCSZ  = Frame_20ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;

   TEST_DEF ("G729AB 60ms channels");
   ActiveChan.DECODE = G729AB;
   ActiveChan.DECSZ  = Frame_60ms;
   ActiveChan.ENCODE = G729AB;
   ActiveChan.ENCSZ  = Frame_60ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;

   TEST_DEF ("G729AB 80ms channels");
   ActiveChan.DECODE = G729AB;
   ActiveChan.DECSZ  = Frame_80ms;
   ActiveChan.ENCODE = G729AB;
   ActiveChan.ENCSZ  = Frame_80ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;

}
{  // PCMU_64
   TEST_DEF ("PCM_U 1ms channels");
   ActiveChan.DECODE = PCMU_64;
   ActiveChan.DECSZ  = Frame_1ms;
   ActiveChan.ENCODE = PCMU_64;
   ActiveChan.ENCSZ  = Frame_1ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;

   TEST_DEF ("PCM_U 2.5ms channels");
   ActiveChan.DECODE = PCMU_64;
   ActiveChan.DECSZ  = Frame_2_5ms;
   ActiveChan.ENCODE = PCMU_64;
   ActiveChan.ENCSZ  = Frame_2_5ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;

   TEST_DEF ("PCM_U 5ms channels");
   ActiveChan.DECODE = PCMU_64;
   ActiveChan.DECSZ  = Frame_5ms;
   ActiveChan.ENCODE = PCMU_64;
   ActiveChan.ENCSZ  = Frame_5ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;

   TEST_DEF ("PCM_U 10ms channels");
   ActiveChan.DECODE = PCMU_64;
   ActiveChan.DECSZ  = Frame_10ms;
   ActiveChan.ENCODE = PCMU_64;
   ActiveChan.ENCSZ  = Frame_10ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;

   TEST_DEF ("PCM_U 20ms channels");
   ActiveChan.DECODE = PCMU_64;
   ActiveChan.DECSZ  = Frame_20ms;
   ActiveChan.ENCODE = PCMU_64;
   ActiveChan.ENCSZ  = Frame_20ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;

   TEST_DEF ("PCM_U 30ms channels");
   ActiveChan.DECODE = PCMU_64;
   ActiveChan.DECSZ  = Frame_30ms;
   ActiveChan.ENCODE = PCMU_64;
   ActiveChan.ENCSZ  = Frame_30ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;

   TEST_DEF ("PCM_U 60ms channels");
   ActiveChan.DECODE = PCMU_64;
   ActiveChan.DECSZ  = Frame_60ms;
   ActiveChan.ENCODE = PCMU_64;
   ActiveChan.ENCSZ  = Frame_60ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;

   TEST_DEF ("PCM_U 80ms channels");
   ActiveChan.DECODE = PCMU_64;
   ActiveChan.DECSZ  = Frame_80ms;
   ActiveChan.ENCODE = PCMU_64;
   ActiveChan.ENCSZ  = Frame_80ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;
}
{  // G722
   TEST_DEF ("G722 10ms channels");
   ActiveChan.DECODE = G722; 
   ActiveChan.DECSZ  = Frame_10ms;
   ActiveChan.ENCODE = G722; 
   ActiveChan.ENCSZ  = Frame_10ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;
 
   TEST_DEF ("G722 20ms channels");
   ActiveChan.DECODE = G722; 
   ActiveChan.DECSZ  = Frame_20ms;
   ActiveChan.ENCODE = G722; 
   ActiveChan.ENCSZ  = Frame_20ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;
 
   TEST_DEF ("G722 30ms channels");
   ActiveChan.DECODE = G722; 
   ActiveChan.DECSZ  = Frame_30ms;
   ActiveChan.ENCODE = G722; 
   ActiveChan.ENCSZ  = Frame_30ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;
 
   TEST_DEF ("G722 60ms channels");
   restoreChannelDefaults();
   ActiveChan.DECODE = G722; 
   ActiveChan.DECSZ  = Frame_60ms;
   ActiveChan.ENCODE = G722; 
   ActiveChan.ENCSZ  = Frame_60ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;
   
   TEST_DEF ("G722 80ms channels");
   ActiveChan.DECODE = G722; 
   ActiveChan.DECSZ  = Frame_80ms;
   ActiveChan.ENCODE = G722; 
   ActiveChan.ENCSZ  = Frame_80ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;
}
{  // G726
   TEST_DEF ("G726-16 80ms kb channels");
   ActiveChan.DECODE = G726_16; 
   ActiveChan.DECSZ  = Frame_80ms;
   ActiveChan.ENCODE = G726_16; 
   ActiveChan.ENCSZ  = Frame_80ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;

   TEST_DEF ("G726-40 kb 60ms channels");
   ActiveChan.DECODE = G726_40; 
   ActiveChan.DECSZ  = Frame_60ms;
   ActiveChan.ENCODE = G726_40; 
   ActiveChan.ENCSZ  = Frame_60ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;

   TEST_DEF ("G726-24 5ms kb channels");
   ActiveChan.DECODE = G726_24; 
   ActiveChan.DECSZ  = Frame_5ms;
   ActiveChan.ENCODE = G726_24; 
   ActiveChan.ENCSZ  = Frame_5ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;

   TEST_DEF ("G726-32 2.5ms kb channels");
   ActiveChan.DECODE = G726_32; 
   ActiveChan.DECSZ  = Frame_2_5ms;
   ActiveChan.ENCODE = G726_32; 
   ActiveChan.ENCSZ  = Frame_2_5ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;
}
{  // L16
   TEST_DEF ("L16 30ms channels");
   restoreChannelDefaults();
   ActiveChan.DECODE = L16; 
   ActiveChan.DECSZ  = Frame_30ms;
   ActiveChan.ENCODE = L16; 
   ActiveChan.ENCSZ  = Frame_30ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;
 
   TEST_DEF ("L16 60ms channels");
   restoreChannelDefaults();
   ActiveChan.DECODE = L16; 
   ActiveChan.DECSZ  = Frame_60ms;
   ActiveChan.ENCODE = L16; 
   ActiveChan.ENCSZ  = Frame_60ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;
 
   TEST_DEF ("L16 80ms channels");
   restoreChannelDefaults();
   ActiveChan.DECODE = L16; 
   ActiveChan.DECSZ  = Frame_80ms;
   ActiveChan.ENCODE = L16; 
   ActiveChan.ENCSZ  = Frame_80ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;
}
{  // L16_WB
   TEST_DEF ("L16_WB 30ms channels");
   restoreChannelDefaults();
   ActiveChan.DECODE = L16_WB; 
   ActiveChan.DECSZ  = Frame_30ms;
   ActiveChan.ENCODE = L16_WB; 
   ActiveChan.ENCSZ  = Frame_30ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;
 
   TEST_DEF ("L16_WB 60ms channels");
   restoreChannelDefaults();
   ActiveChan.DECODE = L16_WB; 
   ActiveChan.DECSZ  = Frame_60ms;
   ActiveChan.ENCODE = L16_WB; 
   ActiveChan.ENCSZ  = Frame_60ms;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;
 
}
   return;
}

void SRTPTests (ADT_UInt32 DspId) {
   restoreChannelDefaults();
   ActiveChan.DECODE = PCMU_64; 
   ActiveChan.DECSZ  = Frame_1ms;
   ActiveChan.ENCODE = PCMU_64;
   ActiveChan.ENCSZ  = Frame_1ms;

   TEST_DEF ("AES_CM. AUTH_NONE. PCMU_64 x 1ms\n");
   setSRTPCfgDefaults();
   SRTPCfg.Encode.EncryptionScheme     = GPAK_ENCRYPT_CM;
   SRTPCfg.Decode.EncryptionScheme     = GPAK_ENCRYPT_CM;
   SRTPCfg.Encode.AuthenticationScheme = GPAK_AUTH_NONE;
   SRTPCfg.Decode.AuthenticationScheme = GPAK_AUTH_NONE;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;

   ActiveChan.DECSZ  = Frame_20ms;
   ActiveChan.ENCSZ  = Frame_20ms;

   TEST_DEF ("AES_CM. AUTH_NONE. PCMU_64 x 20ms\n");
   setSRTPCfgDefaults();
   SRTPCfg.Encode.EncryptionScheme     = GPAK_ENCRYPT_CM;
   SRTPCfg.Decode.EncryptionScheme     = GPAK_ENCRYPT_CM;
   SRTPCfg.Encode.AuthenticationScheme = GPAK_AUTH_NONE;
   SRTPCfg.Decode.AuthenticationScheme = GPAK_AUTH_NONE;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;

   ActiveChan.DECODE = G729; 
   ActiveChan.DECSZ  = Frame_20ms;
   ActiveChan.ENCODE = G729;
   ActiveChan.ENCSZ  = Frame_20ms;

   TEST_DEF ("AES_CM. AUTH_SHA. G729 x 20ms\n");
   setSRTPCfgDefaults();
   SRTPCfg.Encode.EncryptionScheme     = GPAK_ENCRYPT_CM;
   SRTPCfg.Decode.EncryptionScheme     = GPAK_ENCRYPT_CM;
   SRTPCfg.Encode.AuthenticationScheme = GPAK_AUTH_HMAC_SHA;
   SRTPCfg.Decode.AuthenticationScheme = GPAK_AUTH_HMAC_SHA;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;

   TEST_DEF ("AES_CM.  AUTH_NONE.\n");
   setSRTPCfgDefaults();
   SRTPCfg.Encode.EncryptionScheme     = GPAK_ENCRYPT_CM;
   SRTPCfg.Decode.EncryptionScheme     = GPAK_ENCRYPT_CM;
   SRTPCfg.Encode.AuthenticationScheme = GPAK_AUTH_NONE;
   SRTPCfg.Decode.AuthenticationScheme = GPAK_AUTH_NONE;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;

   TEST_DEF ("AES-F8.  AUTH_NONE.\n");
   setSRTPCfgDefaults();
   SRTPCfg.Encode.EncryptionScheme     = GPAK_ENCRYPT_F8;
   SRTPCfg.Decode.EncryptionScheme     = GPAK_ENCRYPT_F8;
   SRTPCfg.Encode.AuthenticationScheme = GPAK_AUTH_NONE;
   SRTPCfg.Decode.AuthenticationScheme = GPAK_AUTH_NONE;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;

   TEST_DEF ("ENCRYPT_NONE. AUTH_SHA.\n");
   setSRTPCfgDefaults();
   SRTPCfg.Encode.EncryptionScheme     = GPAK_ENCRYPT_NONE;
   SRTPCfg.Decode.EncryptionScheme     = GPAK_ENCRYPT_NONE;
   SRTPCfg.Encode.AuthenticationScheme = GPAK_AUTH_HMAC_SHA;
   SRTPCfg.Decode.AuthenticationScheme = GPAK_AUTH_HMAC_SHA;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;

   TEST_DEF ("ENCRYPT_NONE. AUTH_MD5.\n");
   setSRTPCfgDefaults();
   SRTPCfg.Encode.EncryptionScheme     = GPAK_ENCRYPT_NONE;
   SRTPCfg.Decode.EncryptionScheme     = GPAK_ENCRYPT_NONE;
   SRTPCfg.Encode.AuthenticationScheme = GPAK_AUTH_MD5;
   SRTPCfg.Decode.AuthenticationScheme = GPAK_AUTH_MD5;
   if (setupChannels (DspId) == ESC) return;
   if (processTestKeys (DspId, NULL) == ESC) return;

}

void loadTests (ADT_UInt32 DspId) {
   GpakApiStatus_t apiStat;
   GPAK_AlgControlStat_t  algStat;

   TEST_DEF ("1ms load testing with firehose.  Target = 32 Channels.\n");
   dspTearDown (DspId);
   restoreChannelDefaults();

#ifdef ADT_DEVELOPMENT
   NetEnables.fireHoseEnable = ADT_TRUE;
#endif
   TDM_loopback = NO_LOOP;

   ActiveChan.DECODE = PCMU_64; 
   ActiveChan.DECSZ  = Frame_1ms;
   ActiveChan.ENCODE = PCMU_64; 
   ActiveChan.ENCSZ  = Frame_1ms;
   setupChannels (DspId);
   apiStat = gpakAlgControl (DspId, 0, RTPTransmitEnable,
	                         Null_tone, Null_tone, NullDevice,
	                          0, 0, &algStat);
   printf ("Transmit enable status %d.%d\n", apiStat, algStat);
   if (processTestKeys (DspId, NULL) == ESC) return;

   
   TEST_DEF ("1ms load testing with RTP loopback.  Target = 32 Channels.\n");
   dspTearDown (DspId);
   restoreChannelDefaults();

#ifdef ADT_DEVELOPMENT
   NetEnables.fireHoseEnable = ADT_FALSE;
#endif
   TDM_loopback = CROSSOVER;
   ActiveChan.DECODE = L16; 
   ActiveChan.DECSZ  = Frame_1ms;
   ActiveChan.ENCODE = L16; 
   ActiveChan.ENCSZ  = Frame_1ms;
   setupChannels (DspId);
   if (processTestKeys (DspId, NULL) == ESC) return;
   return;
}

void DTXTests (ADT_UInt32 DspId) {
   GpakApiStatus_t apiStat;
   GPAK_AlgControlStat_t  algStat;

   TEST_DEF ("DTX Testing.\n");
   dspTearDown (DspId);
   restoreChannelDefaults();

#ifdef ADT_DEVELOPMENT
   NetEnables.fireHoseEnable = ADT_TRUE;
#endif
   TDM_loopback = NO_LOOP;
   setupChannels (DspId);
   if (processTestKeys (DspId, NULL) == ESC) return;

   apiStat = gpakAlgControl (DspId, 0, RTPTransmitDisable,
	                         Null_tone, Null_tone, NullDevice,
	                          0, 0, &algStat);
   printf ("Transmit disable status %d.%d\n", apiStat, algStat);
   if (processTestKeys (DspId, NULL) == ESC) return;

   apiStat = gpakAlgControl (DspId, 0, RTPReceiveDisable,
	                         Null_tone, Null_tone, NullDevice,
	                          0, 0, &algStat);
   printf ("Receive disable status %d.%d\n", apiStat, algStat);
   if (processTestKeys (DspId, NULL) == ESC) return;

   apiStat = gpakAlgControl (DspId, 0, RTPReceiveEnable,
	                         Null_tone, Null_tone, NullDevice,
	                          0, 0, &algStat);
   printf ("Receive enable status %d.%d\n", apiStat, algStat);
   if (processTestKeys (DspId, NULL) == ESC) return;

   apiStat = gpakAlgControl (DspId, 0, RTPTransmitEnable,
	                         Null_tone, Null_tone, NullDevice,
	                          0, 0, &algStat);
   printf ("Transmit enable status %d.%d\n", apiStat, algStat);
   if (processTestKeys (DspId, NULL) == ESC) return;
}

void problemTests (ADT_UInt32 DspId) {
   return;
}

GpakArbTdParams_t arbParams = {
     4,   // num frequencies
     4,   // num of tones
     0,   // min dBm to detect tone
     0    // allowable frequency deviation (% times 10)
};

ADT_UInt16 arbFreqs[7][4] = {
    {350, 440, 480, 620},
    {400, 490, 530, 670},
    {450, 540, 580, 720},
    {500, 590, 630, 770},
    {550, 640, 680, 820},
    {600, 690, 730, 870},
    {650, 740, 780, 920}};

#define IDX0 101
#define IDX1 101
#define IDX2 101
#define IDX3 101
#define IDX4 101
#define IDX5 101
#define IDX6 101

GpakArbTdToneIdx_t arbTones[7][4] = {
{ {350, 440, IDX0}, {440, 480, (IDX0+1)}, {480, 620, (IDX0+2)}, {350, 620, (IDX0+3)} },  // config 1: 350, 440, 480, 620:
{ {400, 490, IDX1}, {490, 530, (IDX1+1)}, {530, 670, (IDX1+2)}, {400, 670, (IDX1+3)} },  // config 2: 400, 490, 530, 670:
{ {450, 540, IDX2}, {540, 580, (IDX2+1)}, {580, 720, (IDX2+2)}, {450, 720, (IDX2+3)} },  // config 3: 450, 540, 580, 720:
{ {500, 590, IDX3}, {590, 630, (IDX3+1)}, {630, 770, (IDX3+2)}, {500, 770, (IDX3+3)} },  // config 4: 500, 590, 630, 770:
{ {550, 640, IDX4}, {640, 680, (IDX4+1)}, {680, 820, (IDX4+2)}, {550, 820, (IDX4+3)} },  // config 5: 550, 640, 680, 820:
{ {600, 690, IDX5}, {690, 730, (IDX5+1)}, {730, 870, (IDX5+2)}, {600, 870, (IDX5+3)} },  // config 6: 600, 690, 730, 870:
{ {650, 740, IDX6}, {740, 780, (IDX6+1)}, {780, 920, (IDX6+2)}, {650, 920, (IDX6+3)} }   // config 7: 650, 740, 780, 920:
};



extern void configCustomToneDetection (ADT_UInt32 DspId, ADT_UInt16 CfgId, GpakArbTdParams_t *pParams, GpakArbTdToneIdx_t *pTones, ADT_UInt16 *pFreqs);
extern void activateCustomToneDetection (ADT_UInt32 DspId, ADT_UInt16 CfgId);

void TDMTDM_ToneDetTests (ADT_UInt32 DspId) {
    ADT_UInt16 i = 0;
    int rate, valid;
    setupChannels             = trilogyTdmChannelSetup;

    restoreChannelDefaults();
    ActiveChan.TONEA = Arbit_tone | Notify_Host | DTMF_tone | Tone_Squelch;
    ActiveChan.TONEB = Arbit_tone | Notify_Host | DTMF_tone | Tone_Squelch;
    for (i=0; i<7; i++) {
        ADT_printf("Arb Config %d\n",i);
        configCustomToneDetection(DspId, i, &arbParams,  arbTones[i], arbFreqs[i]);
    }

    do {
        rate   = ADT_getint ("\n Enter rate: <5, 10, 20, 30, 60, 80>  ");
        ADT_printf ("\n");
        valid = 1;         
        switch (rate)  {
            case 5:
            ActiveChan.DECSZ  = Frame_5ms;
            ActiveChan.ENCSZ  = Frame_5ms;
            break;

            case 10:
            ActiveChan.DECSZ  = Frame_10ms;
            ActiveChan.ENCSZ  = Frame_10ms;
            break;

            case 20:
            ActiveChan.DECSZ  = Frame_20ms;
            ActiveChan.ENCSZ  = Frame_20ms;
            break;

            case 30:
            ActiveChan.DECSZ  = Frame_30ms;
            ActiveChan.ENCSZ  = Frame_30ms;
            break;

            case 60:
            ActiveChan.DECSZ  = (60*2);
            ActiveChan.ENCSZ  = (60*2);
            break;

            case 80:
            ActiveChan.DECSZ  = (80*2);
            ActiveChan.ENCSZ  = (80*2);
            break;
    
            default:
            valid = 0;
            break;
        }
        if (!valid) break;

        ADT_printf ("\n%d ms frame rate\n",rate);
        ADT_printf ("A-SIDE Tones: ");
        displayToneString(ActiveChan.TONEA);
        ADT_printf ("B-SIDE Tones: ");
        displayToneString(ActiveChan.TONEB);
        for (i=0; i<7; i++) {
            ADT_printf("Arb Config %d\n",i);
            activateCustomToneDetection(DspId, i);
            if (setupChannels (DspId) == ESC) return;
            if (processTestKeys (DspId, NULL) == ESC) return;
            dspTearDown (DspId);
            rate   = ADT_getint ("\n More ARB Configurations ? (1==yes, 0 == no) ");
            ADT_printf ("\n");
            if (!rate) break;
        }
    } while (1);       

   // restore to pkt channel setup default 
   setupChannels             = trilogyPktChannelSetup;
}

int fixedCore = 0;
int halfDuplex=0; // for tone relay debugging
void ToneRelayTests (ADT_UInt32 DspId) {
   int  frameMs;

    fixedCore = 1;

    frameMs = ADT_getint ("\nFrame size(ms): ");
    if (frameMs == 0) frameMs = 10;

    halfDuplex = ADT_getint ("\nHalf Duplex? (0 == no, 1 == chan0-->chan1: ");
    ADT_printf ("\n");

   
    restoreChannelDefaults();
    ActiveChan.DECODE = PCMU_64; 
    ActiveChan.DECSZ  =  (GpakFrameHalfMS) (frameMs * 2);
    ActiveChan.ENCODE = PCMU_64;
    ActiveChan.ENCSZ  =  (GpakFrameHalfMS) (frameMs * 2);
    ActiveChan.TONEA = Notify_Host | DTMF_tone | Tone_Relay | Tone_Regen | Tone_Generate;
    setDSPCrossover (DspId);

    if (setupChannels (DspId) == ESC) return;
    if (processTestKeys (DspId, NULL) == ESC) return;

}

void ToneGenDbg (ADT_UInt32 DspId) {

    restoreChannelDefaults();
    ActiveChan.DECODE = PCMU_64; 
    ActiveChan.DECSZ  =  (GpakFrameHalfMS) (10 * 2);
    ActiveChan.ENCODE = PCMU_64;
    ActiveChan.ENCSZ  =  (GpakFrameHalfMS) (10 * 2);
    ActiveChan.TONEA = Tone_Generate;

    if (setupChannels (DspId) == ESC) return;
    if (processTestKeys (DspId, NULL) == ESC) return;

}

void UCChanSetup (ADT_UInt32 DspId) {
   int  frameMs;
   GpakApiStatus_t apiStat;
   preHdrStat_t    dspStat;
   GpakChannelConfig_t      chCfgData;
   GPAK_ChannelConfigStat_t chDspStat  = Cc_Success;
   GPAK_AlgControlStat_t    algDspStat = Ac_Success;
   ADT_UInt16 chanId, coreId, remoteChanId;
   int cross;

    frameMs = 10;
    restoreChannelDefaults();
    ActiveChan.DECODE = PCMU_64; 
    ActiveChan.DECSZ  =  (GpakFrameHalfMS) (frameMs * 2);
    ActiveChan.ENCODE = PCMU_64;
    ActiveChan.ENCSZ  =  (GpakFrameHalfMS) (frameMs * 2);
    ActiveChan.TONEA = 0;

    chanId   = ADT_getint ("\n Unicast channelID: ");
    ADT_printf ("\n");

    remoteChanId   = ADT_getint ("\n Remote Unicast channelID: ");
    ADT_printf ("\n");

    NetEnables.multicastTx = ADT_FALSE;
    NetEnables.multicastRx = ADT_FALSE;

    cross   = ADT_getint ("\n Port CrossOver ? : <0,1>");
    ADT_printf ("\n");
    if (cross)  {
        setDSPCrossover (DspId);
        NetEnables.PortCrossover = ADT_TRUE;
    } else {
        NetEnables.PortCrossover = ADT_FALSE;
    }


    startChan = chanId;
    endChan   = chanId + 1;

    clearPreHeaders (DspId);

    chns.tdmTdm = 0;
    chns.tdmPkt = 1;
    chns.pktPkt = 0;
    chns.tdmCnfr = 0;
    chns.pktCnfr = 0;
    chns.cmpCnfr = 0;

    txHdr[3] = (ADT_UInt8) chanId;
    rxHdr[3] = (ADT_UInt8) remoteChanId;
    CustomSSRC     =  BASE_UCAST_SSRC + (ADT_UInt32)chanId;
    CustomDestSSRC =  BASE_UCAST_SSRC + (ADT_UInt32)remoteChanId;
    UseCustomSSRC  = 1;

    dspStat = preHdrSuccess;
    apiStat = gpakPreRTPHeader (DspId, startChan, (ADT_UInt16) sizeof (rxHdr), rxHdr, txHdr, &dspStat);
    if ((apiStat != GpakApiSuccess) || (dspStat != preHdrSuccess))
        ADT_printf ("\nChannel %u pre-header failure %u:%u:%u\n", startChan, apiStat, dspStat, DSPError [DspId]);

    setTDMPktDefaults (&chCfgData,  chanId, DspId);
    coreId = 2;
    chanId = (coreId << 8) + chanId;
    apiStat = gpakConfigureChannel (DspId, chanId, pcmToPacket, &chCfgData, &chDspStat);
    if (apiStat !=  GpakApiSuccess || chDspStat != Cc_Success) {
       ADT_printf ("Core %d Channel %3d setup failure %d:%d:%d\n\r", coreId, chanId, apiStat, chDspStat, DSPError [DspId]);
       freeSlot (chCfgData.PcmPkt.PcmOutPort, chCfgData.PcmPkt.PcmOutSlot);
    } else {
       ADT_printf ("Core %d Channel %3d setup success\n\r", coreId, chanId);
       sysCfg.ActiveChannels++;
    }

    if (processTestKeys (DspId, NULL) == ESC) return;

}

void MCTxSetup (ADT_UInt32 DspId) {
   int  frameMs;
   GpakApiStatus_t apiStat;
   preHdrStat_t    dspStat;
   ADT_UInt8 *pBaseMCastIP = (ADT_UInt8 *)&BaseMCastIP;
   GpakChannelConfig_t      chCfgData;
   GPAK_ChannelConfigStat_t chDspStat  = Cc_Success;
   GPAK_AlgControlStat_t    algDspStat = Ac_Success;
   ADT_UInt16 chanId, coreId;
   int cross;

    frameMs = 10;
    restoreChannelDefaults();


    ActiveChan.DECODE = NullCodec; 
    ActiveChan.DECSZ  =  (GpakFrameHalfMS) (frameMs * 2);
    ActiveChan.ENCODE = PCMU_64;
    ActiveChan.ENCSZ  =  (GpakFrameHalfMS) (frameMs * 2);
    ActiveChan.TONEA = 0;

    NetEnables.multicastRx = ADT_FALSE;
    NetEnables.multicastTx = ADT_TRUE;
    chanId   = ADT_getint ("\n TxMcast channelID: ");
    ADT_printf ("\n");

    cross   = ADT_getint ("\n Port CrossOver ? : <0,1>");
    ADT_printf ("\n");
    if (cross)  NetEnables.PortCrossover = ADT_TRUE;
    else        NetEnables.PortCrossover = ADT_FALSE;

    startChan = chanId;
    endChan   = chanId + 1;

    clearPreHeaders (DspId);

    chns.tdmTdm = 0;
    chns.tdmPkt = 1;
    chns.pktPkt = 0;
    chns.tdmCnfr = 0;
    chns.pktCnfr = 0;
    chns.cmpCnfr = 0;

    mcTxHdr[0]  = pBaseMCastIP[3];
    mcTxHdr[1]  = pBaseMCastIP[2];
    mcTxHdr[2]  = pBaseMCastIP[1];
    mcTxHdr[3] = (ADT_UInt8) chanId;

    mcRxHdr[0]  = pBaseMCastIP[3];
    mcRxHdr[1]  = pBaseMCastIP[2];
    mcRxHdr[2]  = pBaseMCastIP[1];
    mcRxHdr[3] = (ADT_UInt8) chanId; // this is a don't care
    CustomSSRC     =  BASE_MCAST_SSRC + (ADT_UInt32)chanId;
    CustomDestSSRC =  BASE_MCAST_SSRC + (ADT_UInt32)chanId;
    UseCustomSSRC  = 1;

    dspStat = preHdrSuccess;
    apiStat = gpakPreRTPHeader (DspId, startChan, (ADT_UInt16) sizeof (rxHdr), mcRxHdr, mcTxHdr, &dspStat);
    if ((apiStat != GpakApiSuccess) || (dspStat != preHdrSuccess))
        ADT_printf ("\nChannel %u pre-header failure %u:%u:%u\n", startChan, apiStat, dspStat, DSPError [DspId]);

    setTDMPktDefaults (&chCfgData,  chanId, DspId);
    coreId = 2;
    chanId = (coreId << 8) + chanId;
    apiStat = gpakConfigureChannel (DspId, chanId, pcmToPacket, &chCfgData, &chDspStat);
    if (apiStat !=  GpakApiSuccess || chDspStat != Cc_Success) {
       ADT_printf ("Core %d Channel %3d setup failure %d:%d:%d\n\r", coreId, chanId, apiStat, chDspStat, DSPError [DspId]);
       freeSlot (chCfgData.PcmPkt.PcmOutPort, chCfgData.PcmPkt.PcmOutSlot);
    } else {
       ADT_printf ("Core %d Channel %3d setup success\n\r", coreId, chanId);
       sysCfg.ActiveChannels++;
    }

    if (processTestKeys (DspId, NULL) == ESC) return;
}
void MCRxSetup (ADT_UInt32 DspId) {
   int  frameMs;
   GpakApiStatus_t apiStat;
   preHdrStat_t    dspStat;
   ADT_UInt8 *pBaseMCastIP = (ADT_UInt8 *)&BaseMCastIP;
   GpakChannelConfig_t      chCfgData;
   GPAK_ChannelConfigStat_t chDspStat  = Cc_Success;
   GPAK_AlgControlStat_t    algDspStat = Ac_Success;
   ADT_UInt16 chanId, coreId;
   int cross;

    frameMs = 10;
    restoreChannelDefaults();


    ActiveChan.DECODE = PCMU_64; 
    ActiveChan.DECSZ  =  (GpakFrameHalfMS) (frameMs * 2);
    ActiveChan.ENCODE = NullCodec;
    ActiveChan.ENCSZ  =  (GpakFrameHalfMS) (frameMs * 2);
    ActiveChan.TONEA = 0;

    NetEnables.multicastRx = ADT_TRUE;
    NetEnables.multicastTx = ADT_FALSE;

    chanId   = ADT_getint ("\n RxMcast channelID : ");
    ADT_printf ("\n");

    rxMcastChan   = ADT_getint ("\n mcast channelID to listen on: ");
    ADT_printf ("\n");

    cross   = ADT_getint ("\n Port CrossOver ? : <0,1> ");
    ADT_printf ("\n");
    if (cross)  NetEnables.PortCrossover = ADT_TRUE;
    else        NetEnables.PortCrossover = ADT_FALSE;

    startChan = chanId;
    endChan   = chanId + 1;

    clearPreHeaders (DspId);

    chns.tdmTdm = 0;
    chns.tdmPkt = 1;
    chns.pktPkt = 0;
    chns.tdmCnfr = 0;
    chns.pktCnfr = 0;
    chns.cmpCnfr = 0;

    mcTxHdr[0]  = pBaseMCastIP[3];
    mcTxHdr[1]  = pBaseMCastIP[2];
    mcTxHdr[2]  = pBaseMCastIP[1];
    mcTxHdr[3] = (ADT_UInt8) rxMcastChan; // this is a don't care

    mcRxHdr[0]  = pBaseMCastIP[3];
    mcRxHdr[1]  = pBaseMCastIP[2];
    mcRxHdr[2]  = pBaseMCastIP[1];
    mcRxHdr[3] = (ADT_UInt8) rxMcastChan;

    CustomSSRC     =  BASE_MCAST_SSRC + (ADT_UInt32)chanId;
    CustomDestSSRC =  BASE_MCAST_SSRC + (ADT_UInt32)chanId;
    UseCustomSSRC  = 1;

    dspStat = preHdrSuccess;
    apiStat = gpakPreRTPHeader (DspId, startChan, (ADT_UInt16) sizeof (rxHdr), mcRxHdr, mcTxHdr, &dspStat);
    if ((apiStat != GpakApiSuccess) || (dspStat != preHdrSuccess))
        ADT_printf ("\nChannel %u pre-header failure %u:%u:%u\n", startChan, apiStat, dspStat, DSPError [DspId]);


    setTDMPktDefaults (&chCfgData,  chanId, DspId);
    coreId = 2;
    chanId = (coreId << 8) + chanId;
    apiStat = gpakConfigureChannel (DspId, chanId, pcmToPacket, &chCfgData, &chDspStat);
    if (apiStat !=  GpakApiSuccess || chDspStat != Cc_Success) {
       ADT_printf ("Core %d Channel %3d setup failure %d:%d:%d\n\r", coreId, chanId, apiStat, chDspStat, DSPError [DspId]);
       freeSlot (chCfgData.PcmPkt.PcmOutPort, chCfgData.PcmPkt.PcmOutSlot);
    } else {
       ADT_printf ("Core %d Channel %3d setup success\n\r", coreId, chanId);
       sysCfg.ActiveChannels++;
    }

    if (processTestKeys (DspId, NULL) == ESC) return;
}

// Setup 2 channels:
// Ch0 is Tx mcast, Rx unicast from Alwx Test IPphone
// Ch1 is Rx mcast, Tx unicast to Alwx Test IPphone
#define AWX_IP   0x0A00003B  // 10.0.0.59 allworkx test ip phone
#define MYPC_IP  0x0A000037  // 10.0.0.55 my desktop
#define MCAST_IP 0xE1000000

extern int getSlotAndPort ();

void McUcSetup (ADT_UInt32 DspId) {
    GpakApiStatus_t          apiStat;
    preHdrStat_t             dspStat;
    GPAK_ChannelConfigStat_t chDspStat;
    GPAK_RTPConfigStat_t     rtpDspStat;
    GpakRTPCfg_t             rtpCfg;
    GpakChannelConfig_t      chCfgData;
    PcmPktCfg_t              *PcmPkt;
    ADT_UInt16               chanId, coreId, listenChanId;
    int                      slotPort, slot, port;
    int                      frameSizeSamps, frameSizeHalfMS;
    GpakCodecs               codec;
    ADT_UInt8                inIP[4], outIP[4];
 

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
    // Channel 0 Configuration:  Tx Multicast, Rx Unicast
    coreId = 2;
    chanId = 0;
    listenChanId = chanId;
    codec  = PCMU_64;
    frameSizeSamps  = FrameSize_20_0ms;
    frameSizeHalfMS =  Frame_20ms;

    // Setup PreRTP Headers ----------------------------------------------------
    memset(mcTxHdr,0,4);
    memset(mcRxHdr,0,4);
    mcTxHdr[0]  = 0xE1;
    mcTxHdr[3] = (ADT_UInt8) chanId;
    dspStat = preHdrSuccess;
    apiStat = gpakPreRTPHeader (DspId, chanId, (ADT_UInt16) sizeof (rxHdr), mcRxHdr, mcTxHdr, &dspStat);
    if ((apiStat != GpakApiSuccess) || (dspStat != preHdrSuccess)) {
        ADT_printf ("\nChannel %u pre-header failure %u:%u:%u\n", chanId, apiStat, dspStat, DSPError [DspId]);
        return;
    }
 
    // Setup RTP stream -------------------------------------------------------
    rtpCfg.SamplingHz = 8000;
    rtpCfg.DelayTargetMinMS = frameSizeHalfMS / 2;
    rtpCfg.DelayTargetMS    = frameSizeHalfMS;
    rtpCfg.DelayTargetMaxMS = frameSizeHalfMS * 2;
    rtpCfg.JitterMode       = 6;
    rtpCfg.TonePyldType     = 97;
    rtpCfg.T38PyldType      = 98;
    rtpCfg.CNGPyldType      = 99;
    rtpCfg.VoicePyldType    = 100;
    rtpCfg.StartSequence    = 1;
    rtpCfg.SrcPort          = 6334;
    rtpCfg.DestPort         = 6334;
    rtpCfg.DestIP           = 0;
    rtpCfg.DestTxIP         = MCAST_IP + chanId;
    rtpCfg.InDestIP         = RemoteIP[DspId]; //AWX_IP;
    rtpCfg.DSCP             = chanId & 0x3f;
    rtpCfg.VlanIdx          = 0xFF;
    rtpCfg.SSRC             = 0; // BASE_MCAST_SSRC + (ADT_UInt32)chanId;
    rtpCfg.DestSSRC         = 0;
    memset (rtpCfg.DestMAC, 0, sizeof (rtpCfg.DestMAC));
    outIP[0] = (rtpCfg.DestTxIP>>24) & 0xFF; 
    outIP[1] = (rtpCfg.DestTxIP>>16) & 0xFF; 
    outIP[2] = (rtpCfg.DestTxIP>>8)  & 0xFF; 
    outIP[3] = (rtpCfg.DestTxIP)     & 0xFF; 
    inIP[0]  = (rtpCfg.InDestIP>>24) & 0xFF; 
    inIP[1]  = (rtpCfg.InDestIP>>16) & 0xFF; 
    inIP[2]  = (rtpCfg.InDestIP>>8)  & 0xFF; 
    inIP[3]  = (rtpCfg.InDestIP)     & 0xFF; 

    rtpDspStat   = RTPSuccess;
    apiStat = gpakSendRTPMsg (DspId, chanId, &rtpCfg, &rtpDspStat);
    if (apiStat != GpakApiSuccess || rtpDspStat != RTPSuccess) {
        ADT_printf ("RTP channel %d setup failure %d:%d:%d\n", chanId, apiStat, rtpDspStat, DSPError [DspId]);
        return;
    }

    // Setup G.PAK channel ----------------------------------------------------- 
    slotPort = getSlotAndPort ();
    slot = slotPort & 0xffff;
    port = slotPort >> 16;
    PcmPkt = &chCfgData.PcmPkt;
    memset (PcmPkt, 0, sizeof (PcmPktCfg_t));
    PcmPkt->PcmInPort    = port;
    PcmPkt->PcmOutPort   = port;
    PcmPkt->PcmInSlot    = slot;
    PcmPkt->PcmOutSlot   = slot;
    PcmPkt->PktInCoding       = codec;
    PcmPkt->PktInFrameHalfMS  = frameSizeHalfMS;
    PcmPkt->PktOutCoding      = codec;
    PcmPkt->PktOutFrameHalfMS = frameSizeHalfMS;
    PcmPkt->PcmEcanEnable = 0;
    PcmPkt->PktEcanEnable = 0;
    PcmPkt->AECEcanEnable = 0;
    PcmPkt->VadEnable     = 0;
    PcmPkt->AgcEnable     = 0;
    PcmPkt->ToneTypes     = 0;
    PcmPkt->CIdMode       = 0;
    PcmPkt->FaxMode       = 0;
    PcmPkt->FaxTransport  = 0;
    PcmPkt->Coding        = GpakVoiceChannel;
    PcmPkt->ToneGenGainG1 = 0;
    PcmPkt->OutputGainG2  = 0;
    PcmPkt->InputGainG3   = 0;


    chDspStat    = Cc_Success;
    apiStat = gpakConfigureChannel (DspId, ((coreId << 8) + chanId), pcmToPacket, &chCfgData, &chDspStat);
    if (apiStat !=  GpakApiSuccess || chDspStat != Cc_Success) {
       ADT_printf ("Core %d Channel %3d setup failure %d:%d:%d\n\r", coreId, chanId, apiStat, chDspStat, DSPError [DspId]);
       freeSlot (chCfgData.PcmPkt.PcmOutPort, chCfgData.PcmPkt.PcmOutSlot);
        return;
    } else {
       ADT_printf ("Core %d Channel %3d setup success:\n\r", coreId, chanId);
       ADT_printf ("Encode: TDM[port %d : slot %d] --> PKT[%d.%d.%d.%d : %d]\n\r", port, slot,outIP[0],outIP[1],outIP[2],outIP[3],rtpCfg.DestPort);
       ADT_printf ("Decode: PKT[%d.%d.%d.%d : %d]  --> TDM[port %d : slot %d]\n\r", inIP[0],inIP[1],inIP[2],inIP[3], rtpCfg.DestPort, port, slot);
       sysCfg.ActiveChannels++;
    }

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
    // Channel 1 Configuration:  Rx Multicast, Tx Unicast
    coreId = 2;
    chanId = 1;
    codec  = PCMU_64;
    frameSizeSamps  = FrameSize_20_0ms;
    frameSizeHalfMS =  Frame_20ms;

    // Setup PreRTP Headers ----------------------------------------------------
    memset(mcTxHdr,0,4);
    memset(mcRxHdr,0,4);
    mcRxHdr[0]  = 0xE1; // Rx mcast listens on channel 0 (i.e. mcRxHdr[3] = 0)
    dspStat      = preHdrSuccess;
    apiStat = gpakPreRTPHeader (DspId, chanId, (ADT_UInt16) sizeof (rxHdr), mcRxHdr, mcTxHdr, &dspStat);
    if ((apiStat != GpakApiSuccess) || (dspStat != preHdrSuccess)) {
       ADT_printf ("\nChannel %u pre-header failure %u:%u:%u\n", chanId, apiStat, dspStat, DSPError [DspId]);
        return;
    }

    // Setup RTP stream -------------------------------------------------------
    rtpCfg.SamplingHz = 8000;
    rtpCfg.DelayTargetMinMS = frameSizeHalfMS / 2;
    rtpCfg.DelayTargetMS    = frameSizeHalfMS;
    rtpCfg.DelayTargetMaxMS = frameSizeHalfMS * 2;
    rtpCfg.JitterMode       = 6;
    rtpCfg.TonePyldType     = 97;
    rtpCfg.T38PyldType      = 98;
    rtpCfg.CNGPyldType      = 99;
    rtpCfg.VoicePyldType    = 100;
    rtpCfg.StartSequence    = 1;
    rtpCfg.SrcPort          = 6334;
    rtpCfg.DestPort         = 6334;
    rtpCfg.DestIP           = 0;
    rtpCfg.DestTxIP         = RemoteIP[DspId]; //AWX_IP;
    rtpCfg.InDestIP         = MCAST_IP + listenChanId;
    rtpCfg.DSCP             = chanId & 0x3f;
    rtpCfg.VlanIdx          = 0xFF;
    rtpCfg.SSRC             = 0;
    rtpCfg.DestSSRC         = 0; // MCAST_IP + listenChanId;
    memset (rtpCfg.DestMAC, 0, sizeof (rtpCfg.DestMAC));
    outIP[0] = (rtpCfg.DestTxIP>>24) & 0xFF; 
    outIP[1] = (rtpCfg.DestTxIP>>16) & 0xFF; 
    outIP[2] = (rtpCfg.DestTxIP>>8)  & 0xFF; 
    outIP[3] = (rtpCfg.DestTxIP)     & 0xFF; 
    inIP[0]  = (rtpCfg.InDestIP>>24) & 0xFF; 
    inIP[1]  = (rtpCfg.InDestIP>>16) & 0xFF; 
    inIP[2]  = (rtpCfg.InDestIP>>8)  & 0xFF; 
    inIP[3]  = (rtpCfg.InDestIP)     & 0xFF; 

    rtpDspStat   = RTPSuccess;
    apiStat = gpakSendRTPMsg (DspId, chanId, &rtpCfg, &rtpDspStat);
    if (apiStat != GpakApiSuccess || rtpDspStat != RTPSuccess) {
        ADT_printf ("RTP channel %d setup failure %d:%d:%d\n", chanId, apiStat, rtpDspStat, DSPError [DspId]);
        return;
    }

    // Setup G.PAK channel ----------------------------------------------------- 
    slotPort = getSlotAndPort ();
    slot = slotPort & 0xffff;
    port = slotPort >> 16;
    PcmPkt = &chCfgData.PcmPkt;
    memset (PcmPkt, 0, sizeof (PcmPktCfg_t));
    PcmPkt->PcmInPort    = port;
    PcmPkt->PcmOutPort   = port;
    PcmPkt->PcmInSlot    = slot;
    PcmPkt->PcmOutSlot   = slot;
    PcmPkt->PktInCoding       = codec;
    PcmPkt->PktInFrameHalfMS  = frameSizeHalfMS;
    PcmPkt->PktOutCoding      = codec;
    PcmPkt->PktOutFrameHalfMS = frameSizeHalfMS;
    PcmPkt->PcmEcanEnable = 0;
    PcmPkt->PktEcanEnable = 0;
    PcmPkt->AECEcanEnable = 0;
    PcmPkt->VadEnable     = 0;
    PcmPkt->AgcEnable     = 0;
    PcmPkt->ToneTypes     = 0;
    PcmPkt->CIdMode       = 0;
    PcmPkt->FaxMode       = 0;
    PcmPkt->FaxTransport  = 0;
    PcmPkt->Coding        = GpakVoiceChannel;
    PcmPkt->ToneGenGainG1 = 0;
    PcmPkt->OutputGainG2  = 0;
    PcmPkt->InputGainG3   = 0;
 
    chDspStat    = Cc_Success;
    apiStat = gpakConfigureChannel (DspId, ((coreId << 8) + chanId), pcmToPacket, &chCfgData, &chDspStat);
    if (apiStat !=  GpakApiSuccess || chDspStat != Cc_Success) {
       ADT_printf ("Core %d Channel %3d setup failure %d:%d:%d\n\r", coreId, chanId, apiStat, chDspStat, DSPError [DspId]);
       freeSlot (chCfgData.PcmPkt.PcmOutPort, chCfgData.PcmPkt.PcmOutSlot);
        return;
    } else {
       ADT_printf ("Core %d Channel %3d setup success:\n\r", coreId, chanId);
       ADT_printf ("Encode: TDM[port %d : slot %d] --> PKT[%d.%d.%d.%d : %d]\n\r", port, slot,outIP[0],outIP[1],outIP[2],outIP[3],rtpCfg.DestPort);
       ADT_printf ("Decode: PKT[%d.%d.%d.%d : %d]  --> TDM[port %d : slot %d]\n\r", inIP[0],inIP[1],inIP[2],inIP[3], rtpCfg.DestPort, port, slot);
       sysCfg.ActiveChannels++;
    }

    if (processTestKeys (DspId, NULL) == ESC) return;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
}

void UcUcSetup (ADT_UInt32 DspId, ADT_UInt32 destip, ADT_UInt32 indestip, ADT_UInt32 desttxip) {
    GpakApiStatus_t          apiStat;
    preHdrStat_t             dspStat;
    GPAK_ChannelConfigStat_t chDspStat;
    GPAK_RTPConfigStat_t     rtpDspStat;
    GpakRTPCfg_t             rtpCfg;
    GpakChannelConfig_t      chCfgData;
    PcmPktCfg_t              *PcmPkt;
    ADT_UInt16               chanId, coreId;
    int                      slotPort, slot, port;
    int                      frameSizeSamps, frameSizeHalfMS;
    GpakCodecs               codec;
 

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
    // Channel 0 Configuration:  Tx Unicast, Rx Unicast
    coreId = 2;
    chanId = 0;
    codec  = PCMU_64;
    frameSizeSamps  = FrameSize_20_0ms;
    frameSizeHalfMS =  Frame_20ms;

    // Disable PreRTP Headers --------------------------------------------------
    memset(mcTxHdr,0,4);
    memset(mcRxHdr,0,4);
    dspStat = preHdrSuccess;
    apiStat = gpakPreRTPHeader (DspId, chanId, (ADT_UInt16) 0, mcRxHdr, mcTxHdr, &dspStat);
    if ((apiStat != GpakApiSuccess) || (dspStat != preHdrSuccess)) {
        ADT_printf ("\nChannel %u pre-header failure %u:%u:%u\n", chanId, apiStat, dspStat, DSPError [DspId]);
        return;
    }
 
    // Setup RTP stream -------------------------------------------------------
    rtpCfg.SamplingHz = 8000;
    rtpCfg.DelayTargetMinMS = frameSizeHalfMS / 2;
    rtpCfg.DelayTargetMS    = frameSizeHalfMS;
    rtpCfg.DelayTargetMaxMS = frameSizeHalfMS * 2;
    rtpCfg.JitterMode       = 6;
    rtpCfg.TonePyldType     = 97;
    rtpCfg.T38PyldType      = 98;
    rtpCfg.CNGPyldType      = 99;
    rtpCfg.VoicePyldType    = 100;
    rtpCfg.StartSequence    = 1;
    rtpCfg.SrcPort          = 6334;
    rtpCfg.DestPort         = 6334;
    rtpCfg.DestIP           = destip;
    rtpCfg.DestTxIP         = desttxip;
    rtpCfg.InDestIP         = indestip;
    rtpCfg.DSCP             = chanId & 0x3f;
    rtpCfg.VlanIdx          = 0xFF;
    rtpCfg.SSRC             = 0;
    rtpCfg.DestSSRC         = 0;
    memset (rtpCfg.DestMAC, 0, sizeof (rtpCfg.DestMAC));

    rtpDspStat   = RTPSuccess;
    apiStat = gpakSendRTPMsg (DspId, chanId, &rtpCfg, &rtpDspStat);
    if (apiStat != GpakApiSuccess || rtpDspStat != RTPSuccess) {
        ADT_printf ("RTP channel %d setup failure %d:%d:%d\n", chanId, apiStat, rtpDspStat, DSPError [DspId]);
        return;
    }

    // Setup G.PAK channel ----------------------------------------------------- 
    slotPort = getSlotAndPort ();
    slot = slotPort & 0xffff;
    port = slotPort >> 16;
    PcmPkt = &chCfgData.PcmPkt;
    memset (PcmPkt, 0, sizeof (PcmPktCfg_t));
    PcmPkt->PcmInPort    = port;
    PcmPkt->PcmOutPort   = port;
    PcmPkt->PcmInSlot    = slot;
    PcmPkt->PcmOutSlot   = slot;
    PcmPkt->PktInCoding       = codec;
    PcmPkt->PktInFrameHalfMS  = frameSizeHalfMS;
    PcmPkt->PktOutCoding      = codec;
    PcmPkt->PktOutFrameHalfMS = frameSizeHalfMS;
    PcmPkt->PcmEcanEnable = 0;
    PcmPkt->PktEcanEnable = 0;
    PcmPkt->AECEcanEnable = 0;
    PcmPkt->VadEnable     = 0;
    PcmPkt->AgcEnable     = 0;
    PcmPkt->ToneTypes     = 0;
    PcmPkt->CIdMode       = 0;
    PcmPkt->FaxMode       = 0;
    PcmPkt->FaxTransport  = 0;
    PcmPkt->Coding        = GpakVoiceChannel;
    PcmPkt->ToneGenGainG1 = 0;
    PcmPkt->OutputGainG2  = 0;
    PcmPkt->InputGainG3   = 0;


    chDspStat    = Cc_Success;
    apiStat = gpakConfigureChannel (DspId, ((coreId << 8) + chanId), pcmToPacket, &chCfgData, &chDspStat);
    if (apiStat !=  GpakApiSuccess || chDspStat != Cc_Success) {
       ADT_printf ("Core %d Channel %3d setup failure %d:%d:%d\n\r", coreId, chanId, apiStat, chDspStat, DSPError [DspId]);
       freeSlot (chCfgData.PcmPkt.PcmOutPort, chCfgData.PcmPkt.PcmOutSlot);
        return;
    } else {
       ADT_printf ("Core %d Channel %3d setup success:\n\r", coreId, chanId);
       ADT_printf ("TDM[port %d:slot %d], PKT[destip 0x%08x, indestip 0x%08x, desttxip 0x%08x:udpport %d\n\r",port,slot,destip,indestip,desttxip,rtpCfg.DestPort);
       sysCfg.ActiveChannels++;
    }

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
    if (processTestKeys (DspId, NULL) == ESC) return;
}

int CodecTests (ADT_UInt32 DspId) {
   int  frameMs, i, cross;
   char *c;
   char cmd[80];

    frameMs = ADT_getint ("\nFrame size(ms): ");
    if (frameMs == 0) frameMs = 10;
   
    restoreChannelDefaults();
    setDSPNoLoopBack (DspId); 
    hostLoopBack = NO_LOOP;
    ADT_printf ("\nCODECS:\tu=PCM_mu\t35=723_53kbps\t36=723A_63kbps\n\t9=729\t9a=729AB\t2=722_64kbps\n\t61=726_16kbps\t62=726_24kbps\t63=726_32kbps\t64=726_40kbps\t");

    memset(cmd, 0, sizeof(cmd));
    ADT_gets (cmd, sizeof (cmd));
    c = cmd;

    switch (*c++) {
        case '\n': return 0;
    
        case 'u': 
        ActiveChan.DECODE = PCMU_64;
        ActiveChan.ENCODE = PCMU_64; 
        break;
    
        case '2': 
        ActiveChan.DECODE = G722_64;
        ActiveChan.ENCODE = G722_64; 
        break;
    
        case '3': 
        if (*c == '5') {
            ActiveChan.DECODE = G723_53;
            ActiveChan.ENCODE = G723_53; 
        } else {
            ActiveChan.DECODE = G723_63;
            ActiveChan.ENCODE = G723_63; 
        }
        break;
    
        case '9': 
        if (*c == 'a') {
            ActiveChan.DECODE = G729AB;
            ActiveChan.ENCODE = G729AB; 
        } else {
            ActiveChan.DECODE = G729;
            ActiveChan.ENCODE = G729; 
        }
        break;
    
        case '6': 
        if (*c == '1') {
            ActiveChan.DECODE = G726_16;
            ActiveChan.ENCODE = G726_16; 
        } else if (*c == '2') {
            ActiveChan.DECODE = G726_24;
            ActiveChan.ENCODE = G726_24; 
        } else if (*c == '3') {
            ActiveChan.DECODE = G726_32;
            ActiveChan.ENCODE = G726_32; 
        } else {
            ActiveChan.DECODE = G726_40;
            ActiveChan.ENCODE = G726_40; 
        }
        break;
    
        default: 
        ActiveChan.DECODE = PCMU_64;
        ActiveChan.ENCODE = PCMU_64; 
        break;
    };

    ActiveChan.DECSZ  =  (GpakFrameHalfMS) (frameMs * 2);
    ActiveChan.ENCSZ  =  (GpakFrameHalfMS) (frameMs * 2);
    // ActiveChan.TONEA = Notify_Host | Arbit_tone | DTMF_tone | Tone_Relay | Tone_Regen | Tone_Generate | Tone_Squelch;
    ActiveChan.TONEA = Tone_Generate;
    ActiveChan.TONEB = Tone_Generate;
    ActiveChan.AGC = Enabled;

#if 0
    for (i=0; i<7; i++)
        configCustomToneDetection(DspId, i, &arbParams,  arbTones[i], arbFreqs[i]);
    activateCustomToneDetection(DspId, 0);
#endif

    cross = ADT_getint ("\nCrossOver ? <0,1>: ");
	if (cross)  {
		NetEnables.PortCrossover = ADT_TRUE;
        setDSPCrossover (DspId); 
	}
        
    
    UseCustomDestIP = ADT_getint ("\nCustom IP ? <0,1>: ");

    if (setupChannels (DspId) == ESC) return 0;
    if (processTestKeys (DspId, NULL) == ESC) return 0;

    return 0;
}
void RCDiagnostics () {

   ADT_UInt32 DspId = 0;
   ADT_Bool fireHoseEnable;
   ADT_Bool stereo, crossover;
   int testID;

   // Teardown any active channels.
   fireHoseEnable = NetEnables.fireHoseEnable;
   stereo         = NetEnables.stereo;
   crossover      = NetEnables.PortCrossover;

   memset (&NetEnables, 0, sizeof (NetEnables));
   setDSPNoLoopBack (DspId);
   NetEnables.fireHoseEnable = fireHoseEnable;
   NetEnables.stereo         = stereo;
   NetEnables.PortCrossover  = crossover;
   setupChannels             = trilogyPktChannelSetup;
   
   ADT_printf ("\n\nTrilogy Diagnostics\n");
   ADT_printf ("\n 0 - TSIP/VLAN setup");
   ADT_printf ("\n 1 - Problem tests");
   ADT_printf ("\n 2 - Standard TDM-PKT testing");
   ADT_printf ("\n 3 - Preheader TDM-PKT testing");
   ADT_printf ("\n 4 - VLAN/TDM-PKT testing");
   ADT_printf ("\n 5 - Multicast Tx/TDM-PKT testing");
   ADT_printf ("\n 6 - Multicast Rx/TDM-PKT testing");
   ADT_printf ("\n 7 - Multicast loopback testing");
   ADT_printf ("\n 8 - Vocoder testing");
   ADT_printf ("\n 9 - Frame rate testing");
   ADT_printf ("\n10 - Algorithm testing");
   ADT_printf ("\n11 - SRTP testing");
   ADT_printf ("\n12 - Caller ID testing");
   ADT_printf ("\n13 - RTP DTX testing");
   ADT_printf ("\n14 - TDM-TDM Tone Detect tests");
   ADT_printf ("\n15 - Tone Relay tests");
   ADT_printf ("\n16 - Codec tests");
   ADT_printf ("\n17 - PreHeader Unicast Chan Setup");
   ADT_printf ("\n18 - PreHeader Tx Multicast Chan Setup");
   ADT_printf ("\n19 - PreHeader Rx Multicast Chan Setup");
   ADT_printf ("\n20 - McUc--AwxIP  2 Chan Setup");
   ADT_printf ("\n21 - UcUc--AwxIP  1 Chan Setup");
   ADT_printf ("\n90 - Load testing");

   testID = ADT_getint ("\nTrilogy Diagnostics\n\nTest ID: ");

   if (NetEnables.PortCrossover == ADT_TRUE) {
      setDSPCrossover (DspId);
   }

   testSuite = &TrilogyTests;

   ADT_printf ("\n");
   switch (testID) {
   case 0: { //   TSIP setup
      dspTDMInit (DspId);
      ADT_printf ("\n");
      dspVLANInit (DspId);
      dspVLANDump (DspId);
      break; }

   case 1:   problemTests (DspId);     break;

   case 2: { //   Standard RTP format.
      restoreChannelDefaults();
      setupChannels (DspId);
      break; }

   case 3: { //   Preheader setup.  TDM-PKT
      endChan   = ADT_getint ("\n  Unicast channels: ");
      ADT_printf ("\n");
      dspTearDown (DspId);
      setupPreHeaders (DspId);

      restoreChannelDefaults();

      chns.tdmTdm = 0;
      chns.tdmPkt = endChan;
      chns.pktPkt = 0;
      chns.tdmCnfr = 0;
      chns.pktCnfr = 0;
      chns.cmpCnfr = 0;
      BaseSSRC = BASE_UCAST_SSRC ;

      initChannels (DspId);
      if (processTestKeys (DspId, NULL) == ESC) break;


      endChan   = ADT_getint ("\nMulticast channel: ");
      ADT_printf ("\n");
      dspTearDown (DspId);
      setupMCPreHeaders (DspId);

      restoreChannelDefaults();
      chns.tdmTdm = 0;
      chns.tdmPkt = endChan;
      chns.pktPkt = 0;
      chns.tdmCnfr = 0;
      chns.pktCnfr = 0;
      chns.cmpCnfr = 0;

      initChannels (DspId);
      if (processTestKeys (DspId, NULL) == ESC) return;

      break; }

   case 4: { //   VLAN enabled
      NetEnables.vlan = ADT_TRUE;
      restoreChannelDefaults();
      setupChannels (DspId);
      break; }

   case 5: { //   Mutlticast TX enable
      NetEnables.multicastTx = ADT_TRUE;
      restoreChannelDefaults();
      setupChannels (DspId);
      break; }

   case 6: { //   Mutlticast RX enable
      NetEnables.multicastRx = ADT_TRUE;
      restoreChannelDefaults();
      setupChannels (DspId);
      break; }

   case 7: { //   Mutlticast loopbackenable
      NetEnables.multicastRx = ADT_TRUE;
      NetEnables.multicastTx = ADT_TRUE;
      NetEnables.multicastRx = ADT_TRUE;
      NetEnables.PortCrossover = ADT_TRUE;
      restoreChannelDefaults();
      setupChannels (DspId);    
      break; }

   case 8: //   Vocoder testing.
      vocoderTests (DspId);
      break;

   case 9: //   Packet rate testing.
      packetRateTests (DspId);   break; 

   case 10: //   Algorithm testing.
      algorithmTests (DspId);    break; 

   case 11: //   SRTP testing.
      SRTPTests (DspId);         break; 

   case 12: //   Caller ID testing.
      dspTearDown (DspId);
      TDM_loopback = CROSSOVER;
      dspTDMInit    (DspId);
      callerIDTests (DspId);
      break;

   case 13:  //  DTX testing
      DTXTests (DspId);
      break;

   case 14:  //  TDM-TDM testing
      TDMTDM_ToneDetTests (DspId);
      break;

   case 15:  //  Tone Relay testing
      ToneRelayTests  (DspId);
      break;

   case 16:  //  Codec testing
      CodecTests  (DspId);
      break;

   case 17: 
      UCChanSetup  (DspId);
      break;

   case 18: 
      MCTxSetup  (DspId);
      break;

   case 19: 
      MCRxSetup  (DspId);
      break;

   case 20: 
      McUcSetup  (DspId);
      break;

   case 21: 
      //                 destip     indestip    desttxip
      UcUcSetup  (DspId, AWX_IP,    0,          0);
      UcUcSetup  (DspId, AWX_IP,    0,          AWX_IP);
      UcUcSetup  (DspId, AWX_IP,    AWX_IP,     0);
      UcUcSetup  (DspId, AWX_IP,    MYPC_IP,    AWX_IP);
      UcUcSetup  (DspId, 0,         AWX_IP,     AWX_IP);

      UcUcSetup  (DspId, AWX_IP,    MYPC_IP,    MCAST_IP);
      UcUcSetup  (DspId, 0,         AWX_IP,     MCAST_IP);
      UcUcSetup  (DspId, AWX_IP,    0,          MCAST_IP);
      UcUcSetup  (DspId, AWX_IP,    MCAST_IP,   AWX_IP);
      UcUcSetup  (DspId, 0,         MCAST_IP,   AWX_IP);
      UcUcSetup  (DspId, AWX_IP,    MCAST_IP,   0);
      UcUcSetup  (DspId, AWX_IP,    MCAST_IP,   MCAST_IP);
      UcUcSetup  (DspId, 0,         MCAST_IP,   MCAST_IP);
      break;

   case 90: { //   Load testing
      loadTests (DspId);
      break; }

   case 99: { //
       ADT_UInt32 data;
       gpakReadDspMemory32 (DspId, 0xEE000000, 1, &data);
       ADT_printf ("Data at 0xEE000000: %x\n", data);
       break;
       }
   }
      
   // Restore to default environment
   NetEnables.fireHoseEnable = fireHoseEnable;
   NetEnables.stereo         = stereo;
   TDM_loopback = NO_LOOP;
   testID = 0;
   UseCustomDestIP = 0;
   UseCustomSSRC  = 0;
   fixedCore = 0;

}
