/*
 * Copyright (c) 2019, Adaptive Digital Technologies, Inc.
 *
 * File Name: GpakApi.c
 *
 * Description:
 *   This file contains user API functions to communicate with DSPs executing
 *   G.PAK software. 
 *
 * Version: 5.1
 *
 * Revision History:
 *   10/17/01 - Initial release.
 *   06/15/04 - Tone type updates.
 *    1/2007  - Modified to allow both C54 and C64 access
 *
 */

#include <stdio.h>
#include "GpakHpi.h"
#include "GpakApi.h"
#include "GpakEnum.h"
#include "ED137BGpakApi.h"

#define NO_ID_CHK 0

extern ADT_UInt32 TransactCmd (ADT_UInt32 DspId,         ADT_UInt32 *pMsgBufr,   ADT_UInt32 I16Cnt,     
                        ADT_UInt8  ExpectedReply, ADT_UInt32 MaxRplyI16,  ADT_UInt32 IdI8,
                        ADT_UInt16 IdValue);

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakConfigureED137B
 *
 * FUNCTION
 *  This function configures a DSP's ED137B Support.  Each channel shall be 
 *  independently configured for ED-137B support. The fields below depict the initial
 *  channel configurations. If the first field rtp_ext_en is set to zero then the 
 *  channel does not utilize the ED-137B header extension.
 *
 * Inputs
 *      DspId         - Dsp identifier 
 *      ED137B        - Configuration params 
 *
 * Outputs
 *      Status  - DSP return status
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */

static ADT_UInt32 gpakFormatED137BcfgWriteParmsMsg (ADT_UInt16 *Msg, GpakED137BcfgParms *dspED137B) 
{
	/* Build the Configure ED137B Message. */
    Msg[0]	= MSG_CONFIGURE_ED137B_PARMS << 8;
	Msg[1]	= (ADT_UInt16) dspED137B->rtp_ext_en;													
    Msg[2]	= (ADT_UInt16) dspED137B->device;											
	Msg[3]	= (ADT_UInt16) dspED137B->txrxmode;
    Msg[4]	= (ADT_UInt16) dspED137B->follow_squ_with_vad;
	Msg[5]	= (ADT_UInt16) dspED137B->ptt_rep;		
	Msg[6]	= (ADT_UInt16) dspED137B->R2SKeepAlive;	
	Msg[7]	= (ADT_UInt16) dspED137B->R2SKeepAliveMultiplier;	
	Msg[8]	= (ADT_UInt16) dspED137B->Ptt_val;																			
	Msg[9]	= (ADT_UInt16) dspED137B->mask;	    
    return 10;
} 

GpakApiStatus_t gpakConfigureED137B (ADT_UInt32 DspId, ADT_UInt16 ChannelId, GpakED137BcfgParms *ED137BcfgParms,
											GPAK_ED137BcfgStat_t *Status) 
{
    ADT_UInt32 MsgLenI16;					/* message length */
    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];	/* message buffer */
    ADT_UInt16 *Msg;
	ADT_UInt32 length;

	if (MaxDsps <= DspId)
        return GpakApiInvalidDsp;
	Msg =(ADT_UInt16 *) &MsgBuffer[0];
	MsgLenI16 = gpakFormatED137BcfgWriteParmsMsg(Msg, ED137BcfgParms);
    Msg[0] |= ChannelId;

	length = TransactCmd(DspId, MsgBuffer, MsgLenI16, MSG_CONFIGURE_ED137B_PARMS_REPLY, 2,  NO_ID_CHK, (ADT_UInt16) ChannelId);
    if (!length)
        return GpakApiCommFailure;

	/* Return success or failure . */

    *Status = (GPAK_ED137BcfgStat_t) Byte0 (Msg[1]);
    if (*Status == ED137Bcfg_Success)	return GpakApiSuccess;
    else								return GpakApiParmError;

}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakPTT_Update
 *
 * FUNCTION
 *  This function supports dynamic ED137B updates of the PTT-Type field used to key 
 *  and un-key the GRS. The PTT-ID field will be unaltered and repeated with the value
 *  set when the channel was initialized.
 *
 * Inputs
 *      DspId         - Dsp identifier 
 *      ED137B        - PTT Update params 
 *
 * Outputs
 *      Status  - DSP return status
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */

static ADT_UInt32 gpakFormatED137B_PTTupdateParmsMsg (ADT_UInt16 *Msg, GpakED137B_PTTupdateParms   *dspED137B) 
{
	/* Build the ED137B Update PTT Message. */
    Msg[0]	= MSG_UPDATE_ED137B_PTT_PARMS << 8;
	Msg[1]	= (ADT_UInt16) dspED137B->PTT_Type;	
	Msg[2]  = (ADT_UInt16) dspED137B->SQU;
	Msg[3]  = (ADT_UInt16) dspED137B->PTT_ID;			
    Msg[4]	= (ADT_UInt16) dspED137B->PM;											
	Msg[5]	= (ADT_UInt16) dspED137B->PTTS;		
    return 10;
} 

GpakApiStatus_t gpakPTT_Update (ADT_UInt32 DspId, ADT_UInt16 ChannelId, GpakED137B_PTTupdateParms *ED137B_PTTupdateParms,
											GPAK_ED137B_PTTupdateStat_t *Status) 
{
	ADT_UInt32 MsgLenI16;					/* message length */
    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];	/* message buffer */
    ADT_UInt16 *Msg;
	ADT_UInt32 length;

	if (MaxDsps <= DspId)
        return GpakApiInvalidDsp;
	Msg =(ADT_UInt16 *) &MsgBuffer[0];
	MsgLenI16 = gpakFormatED137B_PTTupdateParmsMsg(Msg, ED137B_PTTupdateParms);
	Msg[0] |= ChannelId;

	length = TransactCmd(DspId, MsgBuffer, MsgLenI16, MSG_UPDATE_ED137B_PTT_PARMS_REPLY, 2,  NO_ID_CHK, (ADT_UInt16) ChannelId);
    if (!length)
        return GpakApiCommFailure;
    
	/* Return success or failure . */
    *Status = (GPAK_ED137B_PTTupdateStat_t) Byte0 (Msg[1]);
    if (*Status == ED137B_PTTupdate_Success)	return GpakApiSuccess;
    else								return GpakApiParmError;

}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakPersistentFeature
 *
 * FUNCTION
 * This function configures the Persistent Feature parameters.  The persistence 
 * feature shall be included in every RTP packet. This feature is used to
 * dynamically change the Value field. The Rx mask is used to trigger events
 * when unmasked bits change state. 
 *
 * Inputs
 *      DspId         - Dsp identifier 
 *      ED137B        - Persistent Feature params 
 *
 * Outputs
 *      Status  - DSP return status
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */

static ADT_UInt32 gpakFormatED137B_PersistentFeatureParmsMsg (ADT_UInt16 *Msg, GpakED137B_PersistentFeatureParms *dspED137B) 
{
	/* Build the ED137B Configure Persistent Featurre Message. */
    Msg[0]	= MSG_CONFIGURE_ED137B_PERSISTENT_FEATURES_PARMS << 8;
	Msg[1]	= (ADT_UInt16) dspED137B->PersistentFeature_Type;													
    Msg[2]	= (ADT_UInt16) dspED137B->PersistentFeature_Mask;											
	Msg[3]	= (ADT_UInt16) dspED137B->PersistentFeature_Value;		
    return 8;
} 

GpakApiStatus_t gpakPersistentFeature (ADT_UInt32 DspId, ADT_UInt16 ChannelId, GpakED137B_PersistentFeatureParms *ED137B_PersistentFeatureParms,
											GPAK_ED137B_PersistentFeatureStat_t *Status) 
{
    ADT_UInt32 MsgLenI16;					/* message length */
    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];	/* message buffer */
    ADT_UInt16 *Msg;
	ADT_UInt32 length;

	if (MaxDsps <= DspId)
        return GpakApiInvalidDsp;
	Msg =(ADT_UInt16 *) &MsgBuffer[0];
	MsgLenI16 = gpakFormatED137B_PersistentFeatureParmsMsg(Msg, ED137B_PersistentFeatureParms);
	Msg[0] |= ChannelId;

	length = TransactCmd(DspId, MsgBuffer, MsgLenI16, MSG_CONFIGURE_ED137B_PERSISTENT_FEATURES_PARMS_REPLY, 2,  NO_ID_CHK, (ADT_UInt16) ChannelId);
    if (!length)
        return GpakApiCommFailure;
    
	/* Return success or failure . */
    *Status = (GPAK_ED137B_PersistentFeatureStat_t) Byte0 (Msg[1]);
    if (*Status == ED137B_PersistentFeature_Success)	return GpakApiSuccess;
    else								return GpakApiParmError;

}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakPersistentFeatureUpdate
 *
 * FUNCTION
 * This function allows dynamic updates of the value field of the persistent packet 
 * identified by the DSP channel and Type field (B-F).
 *
 * Inputs
 *      DspId         - Dsp identifier 
 *      ED137B        - Persistent Feature Type and Value params 
 *
 * Outputs
 *      Status  - DSP return status
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */

static ADT_UInt32 gpakFormatED137B_PersistentFeatureUpdateParmsMsg (ADT_UInt16 *Msg, GpakED137B_PersistentFeatureUpdateParms   *dspED137B) 
{
	/* Build the ED137B Configure Persistent Feature Message. */
    Msg[0]	= MSG_ED137B_PERSISTENT_FEATURES_UPDATE_PARMS << 8;
	Msg[1]	= (ADT_UInt16) dspED137B->PersistentFeature_Type;																							
	Msg[2]	= (ADT_UInt16) dspED137B->PersistentFeature_Value;		
    return 6;
}

GpakApiStatus_t gpakPersistentFeatureUpdate (ADT_UInt32 DspId, ADT_UInt16 ChannelId, GpakED137B_PersistentFeatureUpdateParms *ED137B_PersistentFeatureUpdateParms,
											GPAK_ED137B_PersistentFeatureUpdateStat_t *Status) 
{
    ADT_UInt32 MsgLenI16;					/* message length */
    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];	/* message buffer */
    ADT_UInt16 *Msg;
	ADT_UInt32 length;

	if (MaxDsps <= DspId)
        return GpakApiInvalidDsp;
	Msg =(ADT_UInt16 *) &MsgBuffer[0];
	MsgLenI16 = gpakFormatED137B_PersistentFeatureUpdateParmsMsg(Msg, ED137B_PersistentFeatureUpdateParms);
	Msg[0] |= ChannelId;

	length = TransactCmd(DspId, MsgBuffer, MsgLenI16, MSG_CONFIGURE_ED137B_PERSISTENT_FEATURES_UPDATE_PARMS_REPLY, 2,  NO_ID_CHK, (ADT_UInt16) ChannelId);
    if (!length)
        return GpakApiCommFailure;
    
	/* Return success or failure . */
    *Status = (GPAK_ED137B_PersistentFeatureUpdateStat_t) Byte0 (Msg[1]);
    if (*Status == ED137B_PersistentFeatureUpdate_Success)	return GpakApiSuccess;
    else								return GpakApiParmError;

}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakNONPersistentFeature
 *
 * FUNCTION
 * This function configures the NON Persistent Feature parameters.The non-persistent
 * feature type can be appended to any active ED-137B channel. 
 * These packets are only sent once but can include one or more feature types. 
 * Note that the total additional features total length must be reflected in the 
 * ED-137B Header Extension length field as specified in the ED-137B specification
 *
 * Inputs
 *      DspId         - Dsp identifier 
 *      ED137B        - NON Persistent Feature params 
 *
 * Outputs
 *      Status  - DSP return status
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */

static ADT_UInt32 gpakFormatED137B_NONPersistentFeatureParmsMsg (ADT_UInt16 *Msg, ADT_UInt32 Num_Features, GpakED137B_NONPersistentFeatureParms   *dspED137B) 
{
	ADT_UInt8 NPF_value_cnt, NPF_cnt, NPF_index = 1;
  
	 /* Build the ED137B Configure Persistent Featurre Message. */
    Msg[0]	= MSG_CONFIGURE_ED137B_NON_PERSISTENT_FEATURES_PARMS << 8;
	Msg[1]  = (ADT_UInt16)Num_Features;
	for(NPF_cnt = 0; NPF_cnt < Num_Features; NPF_cnt++)
	{
	    Msg[NPF_index+1]	= (ADT_UInt16) dspED137B[NPF_cnt].NONPersistentFeature_Type;													
	    Msg[NPF_index+2]	= (ADT_UInt16) dspED137B[NPF_cnt].NONPersistentFeature_Length;	
	    for(NPF_value_cnt = 0; NPF_value_cnt < dspED137B[NPF_cnt].NONPersistentFeature_Length; NPF_value_cnt += 2) 
	    {
		    Msg[NPF_index+NPF_value_cnt+3]   = (ADT_UInt16) dspED137B[NPF_cnt].NONPersistentFeature_Value[NPF_value_cnt];
		    Msg[NPF_index+NPF_value_cnt+3+1] = (ADT_UInt16) dspED137B[NPF_cnt].NONPersistentFeature_Value[NPF_value_cnt+1];
	    }
		NPF_index += (dspED137B[NPF_cnt].NONPersistentFeature_Length)+2;
	}
	return (NPF_index+1);
} 

GpakApiStatus_t gpakNONPersistentFeature (ADT_UInt32 DspId, ADT_UInt16 ChannelId, ADT_UInt32 Num_Features, GpakED137B_NONPersistentFeatureParms *ED137B_NONPersistentFeatureParms,
											GPAK_ED137B_NONPersistentFeatureStat_t *Status) 
{
    ADT_UInt32 MsgLenI16;					/* message length */
    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];	/* message buffer */
    ADT_UInt16 *Msg;
	ADT_UInt32 length;

	if (MaxDsps <= DspId)
        return GpakApiInvalidDsp;
	Msg =(ADT_UInt16 *) &MsgBuffer[0];
	MsgLenI16 = gpakFormatED137B_NONPersistentFeatureParmsMsg(Msg, Num_Features, ED137B_NONPersistentFeatureParms);
	Msg[0] |= ChannelId;

	length = TransactCmd(DspId, MsgBuffer, MsgLenI16, MSG_CONFIGURE_ED137B_NON_PERSISTENT_FEATURES_PARMS_REPLY, 2,  NO_ID_CHK, (ADT_UInt16) ChannelId);
    if (!length)
        return GpakApiCommFailure;
    
	/* Return success or failure . */
    *Status = (GPAK_ED137B_NONPersistentFeatureStat_t) Byte0 (Msg[1]);
    if (*Status == ED137B_NONPersistentFeature_Success)	return GpakApiSuccess;
    else								return GpakApiParmError;

}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakGateAudioFeatureUpdate
 *
 * FUNCTION
 * This function allows updates of the Gate Audio to PTT feature field.
 *
 * Inputs
 *      DspId         - Dsp identifier 
 *      ED137B        - Enable Mask 
 *
 * Outputs
 *      Status  - DSP return status
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */

static ADT_UInt32 gpakFormatED137B_GateAudioFeatureParmsMsg (ADT_UInt16 *Msg, ADT_UInt32  Enable_Mask) 
{
	
     /* Build the ED137B Gate Audio Featurre Message. */
    Msg[0]	= MSG_CONFIGURE_ED137B_GATE_AUDIO_FEATURE_PARMS << 8;
	Msg[1]  = (ADT_UInt16)Enable_Mask;
    return 4;
}

GpakApiStatus_t gpakGateAudioFeature (ADT_UInt32 DspId, ADT_UInt16 ChannelId, ADT_UInt32 Enable_Mask, GpakED137B_GateAudioFeatureStat_t *Status) 
{
    ADT_UInt32 MsgLenI16;					/* message length */
    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];	/* message buffer */
    ADT_UInt16 *Msg;
	ADT_UInt32 length;

	if (MaxDsps <= DspId)
        return GpakApiInvalidDsp;
	Msg =(ADT_UInt16 *) &MsgBuffer[0];
	MsgLenI16 = gpakFormatED137B_GateAudioFeatureParmsMsg(Msg, Enable_Mask);
	Msg[0] |= ChannelId;

	length = TransactCmd(DspId, MsgBuffer, MsgLenI16, MSG_CONFIGURE_ED137B_GATE_AUDIO_FEATURE_PARMS_REPLY, 2,  NO_ID_CHK, (ADT_UInt16) ChannelId);
    if (!length)
        return GpakApiCommFailure;
    
	/* Return success or failure . */
    *Status = (GpakED137B_GateAudioFeatureStat_t) Byte0 (Msg[1]);
    if (*Status == ED137B_GateAudioFeature_Success)	return GpakApiSuccess;
    else								return GpakApiParmError;

}
