/*
 * Copyright (c) 2010, Adaptive Digital Technologies, Inc.
 *
 * File Name: GpakCust.c
 *
 * Description:
 *   This file contains host system dependent functions to support generic
 *   G.PAK API functions. The file is integrated into the host processor
 *   connected to G.PAK DSPs via a Host Port Interface.
 *
 *   Note: This file needs to be modified by the G.PAK system integrator.
 *
 * Version: 1.0
 *
 * Revision History:
 *   10/17/01 - Initial release.
 *   01/21/10 - Modified for DSPlink environment.
 *   03/02/10 - Added Coff file name argument to gpakDspStartup.
 *
 */
#include <unistd.h>
#include <dsplink.h>
#include <proc.h>
#include <string.h>
#include "GpakCust.h"
#include "GpakApi.h"

/* DSPlink related definitions. */
#define LINK_ID 0                  /* DSP processor Id */

unsigned int MaxCoresPerDSP   = CORES_PER_DSP;
unsigned int MaxDsps          = MAX_DSP_CORES;
DSP_Address  ifBlkAddress     = DSP_IFBLK_ADDRESS;                // Interface block address

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadDspMemory8 - Read DSP memory.
 *
 * FUNCTION
 *  This function reads a contiguous block of bytes from DSP memory starting at
 *  the specified address.
 *
 * RETURNS
 *  nothing
 *
 */
void gpakReadDspMemory8(
    ADT_Int32   DspId,         /* DSP Identifier (0 to MAX_DSP_CORES-1) */
    DSP_Address DspAddress,    /* DSP's memory address of first word */
    ADT_UInt32  I8Cnt,         /* number of contiguous bytes to read */
    ADT_UInt8  *I8Array       /* pointer to array of byte values variable */
    )
{
   DSP_STATUS lnkStat;   /* DSPlink API status */

   lnkStat = PROC_read (LINK_ID, DspAddress, I8Cnt, (Pvoid) I8Array);
   if (lnkStat != DSP_SOK) {
      memset (I8Array, 0, I8Cnt);
   }
   return;
}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadDspMemory16 - Read DSP memory.
 *
 * FUNCTION
 *  This function reads a contiguous block of 16-bit word from DSP memory starting at
 *  the specified address.
 *
 * RETURNS
 *  nothing
 *
 */
void gpakReadDspMemory16(
    ADT_Int32   DspId,           /* DSP Identifier (0 to MAX_DSP_CORES-1) */
    DSP_Address DspAddress,      /* DSP's memory address of first word */
    ADT_UInt32  I16Cnt,          /* number of contiguous short to read */
    ADT_UInt16  *I16Array        /* pointer to array of 16-bit values variable */
    )
{
   DSP_STATUS lnkStat;   /* DSPlink API status */

   lnkStat = PROC_read (LINK_ID, DspAddress, I16Cnt << 1, (Pvoid) I16Array);
   if (lnkStat != DSP_SOK) {
      memset (I16Array, 0, I16Cnt << 1);
   }
   return;
}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadDspNoSwap16 - Read DSP packet memory without byte swapping.
 *
 * FUNCTION
 *  This function reads a contiguous block of 16-bit values from DSP memory 
 *  starting at the specified address.  DSP packet memory is in big-endian order
 *
 * RETURNS
 *  nothing
 *
 */
void gpakReadDspNoSwap16(
    ADT_Int32   DspId,        /* DSP Identifier (0 to MAX_DSP_CORES-1) */
    DSP_Address DspAddress,   /* DSP's memory address of first word */
    ADT_UInt32  I16Cnt,       /* number of contiguous 16-bit vals to read */
    ADT_UInt32 *I16Array      /* pointer to array of 16-bit values variable */
    )
{
   DSP_STATUS lnkStat;   /* DSPlink API status */
   ADT_UInt32 i;            /* loop index / counter */

   lnkStat = PROC_read(LINK_ID, DspAddress, I16Cnt << 1, (Pvoid) I16Array);
   if (lnkStat != DSP_SOK){
      memset (I16Array, 0, I16Cnt << 1);
   } else {
      for (i = 0; i < I16Cnt; i++) {
         I16Array[i] = (I16Array[i] << 8) | (I16Array[i] >> 8);
      }
   }
   return;
}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadDspMemory32 - Read DSP memory.
 *
 * FUNCTION
 *  This function reads a contiguous block of 32-bit values from DSP memory 
 *  starting at the specified address. 
 *
 * RETURNS
 *  nothing
 *
 */
void gpakReadDspMemory32 (
    ADT_Int32   DspId,        /* DSP Identifier (0 to MAX_DSP_CORES-1) */
    DSP_Address DspAddress,   /* DSP's memory address of first word */
    ADT_UInt32  I32Cnt,       /* number of contiguous 32-bit vals to read */
    ADT_UInt32 *I32Array      /* pointer to array of 32-bit values variable */
    )
{
   DSP_STATUS lnkStat;   /* DSPlink API status */

   lnkStat = PROC_read (LINK_ID, DspAddress, I32Cnt << 2, (Pvoid) I32Array);
   if (lnkStat != DSP_SOK) {
      memset (I32Array, 0, I32Cnt << 1);
   }
   return;
}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadDspNoSwap32 - Read DSP packet memory without byte swapping.
 *
 * FUNCTION
 *  This function reads a contiguous block of 32-bit values from DSP memory 
 *  starting at the specified address.  DSP packet memory is in big-endian order
 *
 * RETURNS
 *  nothing
 *
 */
void gpakReadDspNoSwap32(
    ADT_Int32   DspId,        /* DSP Identifier (0 to MAX_DSP_CORES-1) */
    DSP_Address DspAddress,   /* DSP's memory address of first word */
    ADT_UInt32  I32Cnt,       /* number of contiguous 32-bit vals to read */
    ADT_UInt32 *I32Array      /* pointer to array of 32-bit values variable */
    )
{
   DSP_STATUS lnkStat;   /* DSPlink API status */

   lnkStat = PROC_read (LINK_ID, DspAddress, I32Cnt << 2, (Pvoid) I32Array);
   if (lnkStat != DSP_SOK) {
      memset(I32Array, 0, I32Cnt << 2);
   }
   return;
}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakWriteDspMemory8 - Write DSP memory.
 *
 * FUNCTION
 *  This function writes a contiguous block of bytes to DSP memory starting at
 *  the specified address.
 *
 * RETURNS
 *  nothing
 *
 */
void gpakWriteDspMemory8 (
    ADT_Int32   DspId,          /* DSP Identifier (0 to MAX_DSP_CORES-1) */
    DSP_Address DspAddress,     /* DSP's memory address of first word */
    ADT_UInt32  I8Cnt,       /* number of contiguous bytes to write */
    ADT_UInt8  *I8Array     /* pointer to array of byte values to write */
    )
{
   DSP_STATUS lnkStat;   /* DSPlink API status */

   lnkStat = PROC_write (LINK_ID, DspAddress, I8Cnt, (Pvoid) I8Array);
   return;
}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakWriteDspMemory16 - Write DSP memory.
 *
 * FUNCTION
 *  This function writes a contiguous block of words to DSP memory starting at
 *  the specified address.
 *
 * RETURNS
 *  nothing
 *
 */
void gpakWriteDspMemory16 (
    ADT_Int32 DspId,         /* DSP Identifier (0 to MAX_DSP_CORES-1) */
    DSP_Address DspAddress,  /* DSP's memory address of first word */
    ADT_UInt32 I16Cnt,       /* number of contiguous 16-bit values to write */
    ADT_UInt16 *I16Array     /* pointer to array of 16-bit values to write */
    )
{
   DSP_STATUS lnkStat;   /* DSPlink API status */

   lnkStat = PROC_write (LINK_ID, DspAddress, I16Cnt << 1, (Pvoid) I16Array);
   return;
}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakWriteDspNoSwap16 - Write DSP packet memory without byte swapping.
 *
 * FUNCTION
 *  This function writes a contiguous block of words to DSP memory starting at
 *  the specified address.
 *
 * RETURNS
 *  nothing
 *
 */
void gpakWriteDspNoSwap16 (
    ADT_Int32 DspId,         /* DSP Identifier (0 to MAX_DSP_CORES-1) */
    DSP_Address DspAddress,  /* DSP's memory address of first word */
    ADT_UInt32 I16Cnt,       /* number of contiguous 16-bit values to write */
    ADT_UInt16 *I16Array     /* pointer to array of 16-bit values to write */
    )
{
   ADT_UInt32 i;            /* loop index / counter */
   DSP_STATUS lnkStat;   /* DSPlink API status */

   for (i = 0; i < I16Cnt; i++) {
      I16Array[i] = (I16Array[i] << 8) | (I16Array[i] >> 8);
   }
   lnkStat = PROC_write(LINK_ID, DspAddress, I16Cnt << 1, (Pvoid) I16Array);
   return;
}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakWriteDspMemory32 - Write DSP memory.
 *
 * FUNCTION
 *  This function writes a contiguous block of 32-bit values to DSP memory 
 *  starting at the specified address.
 *
 * RETURNS
 *  nothing
 *
 */
void gpakWriteDspMemory32 (
    ADT_Int32 DspId,         /* DSP Identifier (0 to MAX_DSP_CORES-1) */
    DSP_Address DspAddress,  /* DSP's memory address of first word */
    ADT_UInt32 I32Cnt,       /* number of contiguous 32-bit vals to write */
    ADT_UInt32 *I32Array     /* pointer to array of 32-bit values to write */
    )
{
   DSP_STATUS lnkStat;   /* DSPlink API status */

   lnkStat = PROC_write (LINK_ID, DspAddress, I32Cnt << 2, (Pvoid) I32Array);
   return;
}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakWriteDspNoSwap32 - Write DSP packet memory without byte swapping.
 *
 * FUNCTION
 *  This function writes a contiguous block of words to DSP memory starting at
 *  the specified address.
 *
 * RETURNS
 *  nothing
 *
 */
void gpakWriteDspNoSwap32 (
    ADT_Int32 DspId,         /* DSP Identifier (0 to MAX_DSP_CORES-1) */
    DSP_Address DspAddress,  /* DSP's memory address of first word */
    ADT_UInt32 I32Cnt,       /* number of contiguous 32-bit values to write */
    ADT_UInt16 *I32Array     /* pointer to array of 32-bit values to write */
    )
{
   DSP_STATUS lnkStat;   /* DSPlink API status */
   lnkStat = PROC_write (LINK_ID, DspAddress, I32Cnt << 2, (Pvoid) I32Array);
   return;
}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakHostDelay - Delay for a fixed time interval.
 *
 * FUNCTION
 *  This function delays for a fixed time interval before returning. The time
 *  interval is the Host Port Interface sampling period when polling a DSP for
 *  replies to command messages.
 *
 * RETURNS
 *  nothing
 *
 */
void gpakHostDelay (void)
{

   usleep(500);

   return;
}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakLockAccess - Lock access to the specified DSP.
 *
 * FUNCTION
 *  This function aquires exclusive access to the specified DSP.
 *
 * RETURNS
 *  nothing
 *
 */
void gpakLockAccess (
    ADT_Int32 DspId                   /* DSP Identifier (0 to MAX_DSP_CORES-1) */
    )
{

   return;
}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakUnlockAccess - Unlock access to the specified DSP.
 *
 * FUNCTION
 *  This function releases exclusive access to the specified DSP.
 *
 * RETURNS
 *  nothing
 *
 */
void gpakUnlockAccess (
    ADT_Int32 DspId                   /* DSP Identifier (0 to MAX_DSP_CORES-1) */
    )
{

   return;
}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadFile - Read a block of bytes from a G.PAK Download file.
 *
 * FUNCTION
 *  This function reads a contiguous block of bytes from a G.PAK Download file
 *  starting at the current file position.
 *
 * RETURNS
 *  The number of bytes read from the file.
 *   -1 indicates an error occurred.
 *    0 indicates all bytes have been read (end of file)
 *
 */
int gpakReadFile (
    GPAK_FILE_ID FileId,        /* G.PAK Download File Identifier */
    ADT_UInt8 *pBuffer,   /* pointer to buffer for storing bytes */
    ADT_UInt32 I8Cnt       /* number of bytes to read */
    )
{

    return -1;
}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakDspStartup - Startup the G.PAK DSP.
 *
 * FUNCTION
 *  This function loads and starts execution of the G.PAK DSP and prepares for
 *  Host-DSP communication.
 *
 * RETURNS
 *  An indication of success or failure.
 *   -1 indicates an error occurred
 *    0 indicates success
 *
 */
int gpakDspStartup (
    ADT_Int32 DspId,            /* DSP Identifier (0 to MAX_DSP_CORES-1) */
    char *pCoffFileName         /* pointer to Coff file name */
    )
{
    DSP_STATUS lnkStat;   /* DSPlink API status */

    /* Create and initialize the DSPlink PROC object. */
    lnkStat = PROC_setup (NULL);
    if (lnkStat != DSP_SOK) {
        return -1;
    }

    /* Attach to the DSP. */
    lnkStat = PROC_attach (LINK_ID, NULL);
    if (lnkStat != DSP_SOK) {
        PROC_destroy();
        return -1;
    }

    /* Load the DSP. */
    lnkStat = PROC_load (LINK_ID, pCoffFileName, 0, NULL);
    if (lnkStat != DSP_SOK) {
        PROC_detach (LINK_ID);
        PROC_destroy ();
        return -1;
    }

    /* Start execution on the DSP. */
    lnkStat = PROC_start (LINK_ID);
    if (lnkStat != DSP_SOK) {
        PROC_detach (LINK_ID);
        PROC_destroy ();
        return -1;
    }

    return 0;
}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakDspShutdown - Shutdown the G.PAK DSP.
 *
 * FUNCTION
 *  This function stops execution of the G.PAK DSP and terminates Host-DSP
 *  communication.
 *
 * RETURNS
 *  An indication of success or failure.
 *   -1 indicates an error occurred
 *    0 indicates success
 *
 */
int gpakDspShutdown(
    ADT_Int32 DspId             /* DSP Identifier (0 to MAX_DSP_CORES-1) */
    )
{

    /* Stop execution on the DSP. */
    if (PROC_stop (LINK_ID) != DSP_SOK) {
        PROC_detach (LINK_ID);
        PROC_destroy ();
        return -1;
    }

    /* Detach from the DSP. */
    if (PROC_detach (LINK_ID) != DSP_SOK)  {
        PROC_destroy ();
        return -1;
    }

    /* Destroy the PROC object. */
    if (PROC_destroy () != DSP_SOK) {
        return -1;
    }

    return 0;
}


int customChannelConfig (ADT_UInt16 *Msg, void *chanCfg, ADT_UInt16 ChannelId) {
   return 0;
}

void customChannelStatusParse (ADT_UInt16 *Msg, void *pChanStatus) { 
   return;
}
