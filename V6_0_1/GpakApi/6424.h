/*H***************************************************************************
*
* $Revision::   1.1                                                          $
* $Date::   Mar 30 2010 18:00:48                                             $
* $Author::   rfuchs                                                         $
*
* DESCRIPTION: Register defines for 6424 . 
*
*H***************************************************************************/

#ifndef c_6424_h
#define c_6424_h



//  DDR2 Registers
#define DDR_SDBCR     0x20000008UL
#define DDR_SDRCR     0x2000000cUL
#define DDR_SDTIMR    0x20000010UL
#define DDR_SDTIMR2   0x20000014UL
#define DDR_DDRPHYCR  0x200000e4UL

#define DDR_VTPIOCR   0x200000f0UL
#define DDR_DDRVTPR   0x01c42038UL
#define DDR_DDRVTPER  0x01c4004cUL

// Power control registers
#define PSC_PTCMD     0x01c41120UL
#define PSC_PTSTAT    0x01c41128UL

#define PCICSR     0x1C1A104UL
#define PCISLVCNTL 0x1C1A180UL

// PLL control registers
#define pll1_ctl    0x01c40900UL
#define pll1_pllm   0x01c40910UL
#define pll1_div1   0x01c40918UL  // SYSCLK1 DIV  CLKDIV1 DOMAIN  DSP
#define pll1_div2   0x01c4091cUL  // SYSCLK2 DIV  CLKDIV3 DOMAIN  SCR/EDMA/PCI/DDR2
#define pll1_div3   0x01c40920UL  // SYSCLK3 DIV  CLKDIV6 DOMAIN  HPI/EMAC/EMIF/McASP/McBSP/GPIO
#define pll1_bpdiv  0x01c4092cUL
#define pll1_cmd    0x01c40938UL
#define pll1_stat   0x01c4093cUL

#define pll2_ctl    0x01c40d00UL
#define pll2_pllm   0x01c40d10UL
#define pll2_div1   0x01c40d18UL  // SYSCLK1 DIV         DDR2 PHY
#define pll2_bpdiv  0x01c40d2cUL  // SYSCLK-BYPASS DIV   DDR2 VTP
#define pll2_cmd    0x01c40d38UL
#define pll2_stat   0x01c40d3cUL



//  PCI control registers
#define PCI64_INT_GEN  0x01C1A010UL
#define PCI64_HINT_SET 0x01C1A020UL
#define PCI64_HINT_CLR 0x01C1A024UL
#define HOST_INTS      0x01000000UL
#define DSP_INTS       0x02000000UL
#define ALL_INTS       0x0F000000UL


// Boot sequence control registers
#define DSPBOOTADDR   0x01C40008UL
#define BOOTCOMPLT    0x01C4000CUL
#define BOOTCFG       0x01c40014UL

#define mdstat39 (0x01c41800UL + (4 * 39))
#define mdctl39  (0x01c41a00UL + (4 * 39))


#endif
