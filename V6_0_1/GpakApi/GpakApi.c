/*
 * Copyright (c) 2002 - 2007, Adaptive Digital Technologies, Inc.
 *
 * File Name: GpakApi.c
 *
 * Description:
 *   This file contains user API functions to communicate with DSPs executing
 *   G.PAK software. 
 *
 * Version: 5.1
 *
 * Revision History:
 *   10/17/01 - Initial release.
 *   06/15/04 - Tone type updates.
 *    1/2007  - Modified to allow both C54 and C64 access
 *
 */

#include <stdio.h>
#include <string.h>
#include "GpakHpi.h"
#include "GpakApi.h"
#include "GpakEnum.h"

#if !defined (__TMS320C55XX__) && !defined (__TMS470__)
   #include <memory.h>
#endif

#if defined (__BIOS__) || defined (__TMS470__)
   #include <string.h>
#endif

/* DSP Related values */
extern DSP_Address *getPktInBufrAddr  (int DspID, int channel);
extern DSP_Address *getPktOutBufrAddr (int DspID, int channel);


int  (*customChannelConfig) (ADT_UInt16 *Msg, void *chanCfg, ADT_UInt16 ChannelId) = NULL;
void (*customChannelStatusParse) (ADT_UInt16 *Msg, void *pChanStatus) = NULL;


#define HighWord(a)            ((ADT_UInt16) ((a >> 16) & 0xffff))
#define LowWord(a)             ((ADT_UInt16) ( a        & 0xffff))
#define PackWords(hi,lo)       ((ADT_UInt32) ((((ADT_UInt32) (hi)) << 16) | (((ADT_UInt32) (lo)) & 0xffff)))
#define Pack(Hi,Lo)            ((ADT_UInt16) ((Hi)<<8) | ((Lo) & 0x00ff))
#define PackWordsSigned(hi,lo) ((ADT_Int32) ((((ADT_UInt32) (hi)) << 16) | (((ADT_UInt32) (lo)) & 0xffff)))


// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
// Macro to reconstruct a 32-bit value from two 16-bit values.
// Parameter p32: 32-bit-wide destination
// Parameter p16: 16-bit-wide source array of length 2 words
#define RECONSTRUCT_LONGWORD(p32, p16) p32 = (DSP_Address)p16[0]<<16; \
                                       p32 |= (DSP_Address)p16[1]

#define CORE_ID(chan) ((chan >> 8) & 0xFF)
#define CHAN_ID(chan) (chan & 0xFF)
#define CNFR_ID(cnfr) (cnfr & 0xFF)

#if (DSP_WORD_I8 == 4)
   #define EndianSwap(word) swapI32(word)
#elif (DSP_WORD_I8 == 2)
   #define EndianSwap(word) swapI16(word)
#endif

#define NO_ID_CHK 0
#define BYTE_ID 1
#define WORD_ID 2


//===================================================================================
//{                  Messaging support routines
//===================================================================================
// Swap adjacent elements of a 16-bit integer array
static void gpakI8Swap (ADT_UInt16 *in, ADT_UInt16 *out, int len) {

    ADT_UInt16 i;

    for (i=0; i<len; i++) *out++ = *in++;
}

void gpakI16Swap (ADT_UInt32 *Buff, int BuffI8)  { 
   ADT_UInt16 temp;
   ADT_UInt16 *buffHi = (ADT_UInt16 *) Buff; 
   ADT_UInt16 *buffLo = ((ADT_UInt16 *) (Buff))+1; 
   int i;

   BuffI8 += 3;
   BuffI8 /= 4;
   for (i=0; i<BuffI8; i++) {
      temp = *buffHi;
      *buffHi = *buffLo;
      *buffLo = temp;
      buffHi += 2;
      buffLo += 2;
   }
}

// Endianess conversion on 32 bit integer
ADT_UInt32 swapI32 (ADT_UInt32 word) {
  return ((word >> 24) & 0x000000ff) |   
         ((word >>  8) & 0x0000ff00) |   
         ((word <<  8) & 0x00ff0000) |
         ((word << 24) & 0xff000000);
}

// Endianess conversion on 16 bit integer
ADT_UInt16 swapI16 (ADT_UInt16 word) {
  return ((word >>  8) & 0x00ff)  |
         ((word <<  8) & 0xff00);
}

// Endianess conversion on 32 bit integer array
void gpakEndianI32Convert (ADT_UInt32 *Buff, int BuffI8) {
   int i;
   int temp;

#if (DSP_WORD_I8 == 4) 
   for (i=0; i< (BuffI8 + 3)/4; i++, Buff++) {
      temp = *Buff;
      *Buff++ = EndianSwap (temp);
   }
#else
   ADT_UInt16 *i16Buff = (ADT_UInt16 *) Buff;
   for (i=0; i< (BuffI8 + 1)/2; i++, i16Buff++) {
      temp = *i16Buff;
      *i16Buff++ = EndianSwap (temp);
   }
#endif
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * VerifyReply - Verify the reply message is correct for the command sent.
 *
 * FUNCTION
 *  Verifies correct reply message content for the command that was just sent.
 *
 * Inputs
 *   pMsgBufr - Reply message buffer
 *   IdValue  - Expected id
 *   IdMode   -  NO_ID_CHK  = No id checking
 *               BYTE_ID    = ID in High byte of reply[1]
 *               WORD_ID    = ID in reply[1]
 *
 * RETURNS
 *  0 = Incorrect
 *  1 = Correct
 *
 */
int VerifyReply (ADT_UInt16 *pMsgBufr, ADT_UInt32 IdMode, ADT_UInt16 IdValue) {

   ADT_UInt16 ReturnID;

   if (IdMode == NO_ID_CHK) return 1;

   ReturnID = pMsgBufr[1];
   
   if (IdMode == BYTE_ID) ReturnID = (ADT_UInt16) Byte1 (ReturnID);
   return (ReturnID == IdValue);
}


#ifdef NETWORK_MESSAGING  // Command transactions
int CheckDspReset (ADT_UInt32 DspId) { return 0; }

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * TransactCmd - Send a command to the DSP and receive it's reply via TCP/IP
 *
 * FUNCTION
 *  Sends the specified command to the DSP and receives the DSP's  reply.
 *
 * Inputs
 *   DspId       - Dsp identifier 
 *   I16Cnt      - Size of command in 16-bit words
 *   ReplyType   - Expected reply type (ChannelStatus, SystemParameterStatus, ...)
 *   ReplyI16Cnt - Expected size of reply in 16-bit words
 *   IdI8  -   0 = No id in reply
 *             1 = ID in High byte of reply[1]
 *             2 = ID in reply[1]
 *   IdValue - Expected value for returned ID (channel number, port number, etc.)
 *
 * Updates
 *    pMsgBufr - Before. Cmd buffer
 *               After.  Reply buffer
 *
 * RETURNS
 *  Length (16-bit words) of reply message (0 = Failure)
 *
 */
ADT_UInt32 TransactCmd (ADT_UInt32 DspId,         ADT_UInt32 *pMsgBufr,   ADT_UInt32 I16Cnt,     
                        ADT_UInt8  ExpectedReply, ADT_UInt32 MaxRplyI16,  ADT_UInt32 IdI8,
                        ADT_UInt16 IdValue) {

   int FuncStatus;              /* function status */

   ADT_UInt32 ReplyI16Cnt;      /* Reply message length (16-bits words) */
   ADT_UInt8  ReplyValue;       /* Reply type code */

   ADT_UInt16 *pWordBufr;       /* Message buffer */

   /* Default the return value to indicate a failure. */
   DSPError [DspId] = DSPSuccess;
   gpakLockAccess (DspId);

   /* Check if the DSP was reset and is ready. */
   if (CheckDspMsgConnect (DspId) == -1) {
      gpakUnlockAccess (DspId);
      return 0;  // Failure
   }
   /* Send the command message to the DSP. */
   FuncStatus = SendDspCmdMessage (DspId, pMsgBufr, I16Cnt);
   if (FuncStatus != 1)  {  // Unsuccessful command transfer to DSP
      gpakUnlockAccess (DspId);
      return 0;
   }

   // Receive the reply from the DSP.
retryRcv:
   ReplyI16Cnt = MaxRplyI16;
   FuncStatus = RecvDspReplyMessage (DspId, pMsgBufr, &ReplyI16Cnt);
   if (FuncStatus != 1)  {   // Unsuccessful reply from DSP
      gpakUnlockAccess (DspId);
      return 0;
   }

   // Verify reply size, expected msg reply value (expectedReply) and expected reply tag
   pWordBufr = (ADT_UInt16 *) pMsgBufr;
   ReplyValue = Byte1 (pWordBufr[0]);
   if (ReplyValue != ExpectedReply) {
      DSPError[DspId] = DSPRplyValue;
      goto retryRcv;                         // Reply for wrong message;
   } if (!VerifyReply (pWordBufr, IdI8, IdValue)) {
      DSPError[DspId] = DSPRplyValue;        // Not the expected reply value
      ReplyI16Cnt = 0;
   } else {
      DSPError [DspId] = DSPSuccess;
   }

    gpakUnlockAccess (DspId);
    return ReplyI16Cnt;  // Length of reply message
}
void bytesToDSP (ADT_UInt16 *Msg, ADT_UInt8 *pPayloadData, ADT_UInt16 PaylenI16) {
   ADT_UInt8 *msg = (ADT_UInt8 *) Msg;
   int i;
   for (i = 0; i < PaylenI16 * 2; i++) {
       *msg++ = *pPayloadData++;
   }
}


#else

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * CheckDspReset - Check if the DSP was reset.
 *
 * FUNCTION
 *  Determines if the DSP was reset and is ready. If reset
 *  occurred, it reads interface parameters and calculates DSP addresses.
 *
 * Inputs
 *   DspId - Dsp identifier 
 *
 * RETURNS
 *  -1 = DSP is not ready.
 *   0 = Reset did not occur.
 *   1 = Reset occurred.
 *
 */
int CheckDspReset (ADT_UInt32 DspId) {

   DSP_Address IfBlockPntr;   /* Interface Block pointer */
   DSP_Address PktBufrMem;    /* address of Packet Buffer */

   DSP_Word    DspChannels;   /* number of DSP channels */
   DSP_Word    DspStatus;     /* DSP initialization status */
   DSP_Word    VersionId;     /* The version ID */
   DSP_Word    Temp;

   ADT_UInt16 i;

   /* Read Interface Block Address */
   IfBlockPntr = pDspIfBlk[DspId];
   if (IfBlockPntr == 0)
      gpakReadDspAddr (DspId, ifBlkAddress, 1, &IfBlockPntr);

   /* Pointer is zero -> return not ready. */
   if (IfBlockPntr == 0 || MAX_DSP_ADDRESS < IfBlockPntr) {
      DSPError [DspId] = DSPNullIFAddr;
      pDspIfBlk[DspId] = 0;
      MaxChannels[DspId] = 0;
      return -1;  // DSP not ready
   }

   /* Read the DSP's Status. from the Interface block */
   gpakReadDsp (DspId, IfBlockPntr + DSP_STATUS_OFFSET, 1, &DspStatus);

   if ((DspStatus == DSP_INIT_STATUS) ||
      ((DspStatus == HOST_INIT_STATUS) && (pDspIfBlk[DspId] == 0))) {

      /* DSP was reset, read the DSP's interface parameters (CmdMsgLen and MaxChannels)
         and calculate DSP addresses. (per channel PacketIn and PacketOut) */
      pDspIfBlk[DspId] = IfBlockPntr;

      gpakReadDsp (DspId, IfBlockPntr + MAX_CMD_MSG_LEN_OFFSET, 1, &Temp);
      MaxCmdMsgLen[DspId] = Temp;
      gpakReadDspAddr (DspId, IfBlockPntr + EVENT_MSG_PNTR_OFFSET,  1, &(pEventFifoAddress[DspId]));
      gpakReadDsp     (DspId, IfBlockPntr + NUM_CHANNELS_OFFSET,    1, &DspChannels);
      if (DspChannels > MaxChannelAlloc)
         MaxChannels[DspId] = MaxChannelAlloc;
      else
         MaxChannels[DspId] = DspChannels;

      // Per channel packet in and packet out buffers
      gpakReadDspAddr (DspId, IfBlockPntr + PKT_BUFR_MEM_OFFSET, 1, &PktBufrMem);
      for (i = 0; i < MaxChannels[DspId]; i++) {
#if (DSP_WORD_I8 == 4)
         *getPktOutBufrAddr (DspId, i) = PktBufrMem;
         *getPktInBufrAddr  (DspId, i) = PktBufrMem + CB_INFO_STRUCT_LEN*4;
         PktBufrMem           += (CB_INFO_STRUCT_LEN*8);
#else
         *getPktOutBufrAddr (DspId, i) = PktBufrMem;
         *getPktInBufrAddr  (DspId, i) = PktBufrMem + CB_INFO_STRUCT_LEN;
         PktBufrMem           += (CB_INFO_STRUCT_LEN*2);
#endif

      }

      //  -----------------------------------------------------------------
      /* write host's API Version ID to DSP.  */
      gpakReadDsp (DspId, IfBlockPntr + VERSION_ID_OFFSET, 1, &VersionId);
      if (MIN_DSP_VERSION_ID < VersionId) {
         VersionId = API_VERSION_ID;
         gpakWriteDsp (DspId, IfBlockPntr + API_VERSION_ID_OFFSET, 1, &VersionId);
      }
      // -------------------------------------------------------------------

      /* Set the DSP Status to indicate the host recognized the reset. */
      DspStatus = HOST_INIT_STATUS;
      gpakWriteDsp (DspId, IfBlockPntr + DSP_STATUS_OFFSET, 1, &DspStatus);
      return 1;  // Dsp ready, reset occured
   }
    
   if (DspStatus != HOST_INIT_STATUS) {
      DSPError [DspId] = DSPHostStatusError;
      pDspIfBlk[DspId] = 0;
      MaxChannels[DspId] = 0;
      return -1;  // DSP not ready
   }

   if (pDspIfBlk[DspId] == 0) {
      DSPError [DspId] = DSPNullIFAddr;
      MaxChannels[DspId] = 0;
      return -1;  // DSP not ready
   }

   return 0;   // DSP ready
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * WriteDspCmdMessage - Write a Host Command/Request message to DSP.
 *
 * FUNCTION
 *  Writes a Host Command/Request message into DSP memory and
 *  informs the DSP of the presence of the message.
 *
 * Inputs
 *   DspId    - Dsp identifier 
 *   pMessage - Message buffer (16-bit words, 32-bit aligned)
 *   ElemCnt  - Number of 16-bit words
 *
 * RETURNS
 *  -1 = Unable to write message (msg len or DSP Id invalid or DSP not ready)
 *   0 = Temporarily unable to write message (previous Cmd Msg busy)
 *   1 = Message written successfully
 *
 */
int WriteDspCmdMessage (ADT_UInt32 DspId, ADT_UInt32 *pMessage, ADT_UInt32 ElemCnt_I16) {

   DSP_Word    ByteCnt;
   DSP_Word    CmdMsgLength;  /* Cmd message length (Dsp words) */
   DSP_Address MsgBuf;        /* message buffer pointer */
   DSP_Address DspOffset;   

   /* Check if the DSP was reset and is ready. */
   if (CheckDspReset(DspId) == -1)
      return (-1);  // Failure

  ByteCnt = (DSP_Word) (ElemCnt_I16 * 2);

   /* Make sure the message length is valid. */
   if (ByteCnt < 1 || MaxCmdMsgLen[DspId] < ByteCnt) {
      DSPError[DspId] = DSPCmdLengthError;
      return (-1);  // Failure
   }

   DspOffset = pDspIfBlk[DspId];

   /* Verify Command message is not pending */
   gpakReadDsp (DspId, DspOffset + CMD_MSG_LEN_OFFSET, 1,  &CmdMsgLength);
   if (CmdMsgLength != 0)
      return (0);  // DSP busy

   /* Purge previous reply message.  NOTE: CmdMessageLength = 0 at this point */
   gpakWriteDsp (DspId, DspOffset + REPLY_MSG_LEN_OFFSET, 1, &CmdMsgLength);

   /* Copy the Command message into DSP memory. */
   gpakReadDspAddr  (DspId, DspOffset + CMD_MSG_PNTR_OFFSET, 1, &MsgBuf);

#if (DSP_WORD_I8 == 4)
   if (HostBigEndian)
      gpakI16Swap (pMessage, ByteCnt);
#endif

   gpakWriteDsp (DspId, MsgBuf, BytesToTxUnits (ByteCnt),  (DSP_Word *) pMessage);
 
   /* Notify DSP of Command message */
   CmdMsgLength = (DSP_Word) ElemCnt_I16;
   gpakWriteDsp (DspId, DspOffset + CMD_MSG_LEN_OFFSET, 1, &CmdMsgLength);

   return (1);  // Success
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * ReadDspReplyMessage - Read a DSP Reply message from DSP.
 *
 * FUNCTION
 *  Reads a DSP Reply message from DSP memory.
 *
 * Inputs
 *   DspId    - Dsp identifier 
 *
 *  Outputs
 *   pMessage - Message buffer (16-bit words, 32-bit aligned)
 *
 *  Update
 *   pElemCnt  - Pointer to number of 16-bit words 
 *               Before = buffer size
 *               After  = actual reply message
 *
 * RETURNS
 *  -1 = Unable to write message (msg len or DSP Id invalid or DSP not ready)
 *   0 = No message available (DSP Reply message empty)
 *   1 = Message read successfully (message and length stored in variables)
 *
 */
int ReadDspReplyMessage (ADT_UInt32 DspId, ADT_UInt32 *pMessage, ADT_UInt32 *pElemCnt) {

   DSP_Word MsgBytes;        /* message length */
   DSP_Address MsgBuf;     /* message buffer pointer */
   DSP_Address DspOffset;   


   /* Read transfer count (in DSP bytes) */
   DspOffset = pDspIfBlk[DspId];
   MsgBytes = 0;
   gpakReadDsp (DspId, DspOffset + REPLY_MSG_LEN_OFFSET, 1,  &MsgBytes);
   if (MsgBytes == 0)
      return (0);

    
   /* Verify reply buffer is large enough  */
   if (*pElemCnt * 2 < MsgBytes) {  // counts converted to bytes
      DSPError[DspId] = DSPRplyLengthError;
      return (-1);
   }

   /* Copy Reply message from DSP memory. */
   gpakReadDspAddr (DspId, DspOffset + REPLY_MSG_PNTR_OFFSET, 1, &MsgBuf);
   gpakReadDsp     (DspId, MsgBuf, BytesToTxUnits (MsgBytes), (DSP_Word *) pMessage);

#if (DSP_WORD_I8 == 4)
   if (HostBigEndian)
      gpakI16Swap (pMessage, MsgBytes);
#endif

   /* return message length in 16-bit words */
   *pElemCnt = (ADT_UInt32) (MsgBytes / 2);

   /* Notify DSP that reply buffer is free */
   MsgBytes = 0;
   gpakWriteDsp (DspId, DspOffset + REPLY_MSG_LEN_OFFSET, 1, &MsgBytes);

   /* Return with an indication the message was read. */
   return (1);
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * TransactCmd - Send a command to the DSP and receive it's reply via HPI or PCI
 *
 * FUNCTION
 *  Sends the specified command to the DSP and receives the DSP's  reply.
 *
 * Inputs
 *   DspId       - Dsp identifier 
 *   ElemCnt     - Size of command in 16-bit words
 *   ReplyType   - Expected reply type (ChannelStatus, SystemParameterStatus, ...)
 *   ReplyElemCnt - Expected size of reply in 16-bit words
 *   IdMode  - 0 = No id in reply
 *             1 = ID in High byte of reply[1]
 *             2 = ID in reply[1]
 *   IdValue - Expected value for returned ID
 *
 * Updates
 *    pMsgBufr - Before. Cmd buffer
 *               After.  Reply buffer
 *
 * RETURNS
 *  Length (16-bit words) of reply message (0 = Failure)
 *
 */
ADT_UInt32  TransactCmd (ADT_UInt32 DspId,        ADT_UInt32 *pMsgBufr,
                         ADT_UInt32 ElemCnt,      ADT_UInt8   ExpectedReply,
                         ADT_UInt32 MaxRplyElem,  ADT_UInt32 IdMode,      ADT_UInt16 IdValue) {

   int FuncStatus;              /* function status */
   unsigned int LoopCount;               /* wait loop counter */

   ADT_UInt32 ReplyElemCnt;     /* Reply message length (16-bits words) */
   ADT_UInt8  ReplyValue;       /* Reply type code */

   ADT_UInt16 *pWordBufr;
   ADT_UInt32 RetValue;         /* return status value */

   /* Default the return value to indicate a failure. */
   RetValue = 0;
   DSPError [DspId] = DSPSuccess;
   gpakLockAccess (DspId);

   /* Write the command message to the DSP. */
   LoopCount = 0;
   while ((FuncStatus = WriteDspCmdMessage (DspId, pMsgBufr, ElemCnt)) != 1)   {
      if (FuncStatus == -1) break;
      if (++LoopCount > MaxWaitLoops) {
         DSPError[DspId] = DSPCmdTimeout;
         break;
      }
      gpakHostDelay();
   }

   if (FuncStatus == 1)  {  // Successful command transfer to DSP

      // Read the reply from the DSP.  Keep trying until successful or too many tries.
      for (LoopCount = 0; LoopCount < MaxWaitLoops; LoopCount++)  {
         ReplyElemCnt = MSG_BUFFER_ELEMENTS;
         FuncStatus = ReadDspReplyMessage (DspId, pMsgBufr, &ReplyElemCnt);
         if (FuncStatus == 0)  {
            gpakHostDelay();
            continue;  // Retry
         }
            
         if (FuncStatus == -1) break;

         pWordBufr = (ADT_UInt16 *) pMsgBufr;

         ReplyValue = Byte1 (pWordBufr[0]);
         if (ReplyValue == MSG_NULL_REPLY) 
            DSPError[DspId] = DSPRplyValue;
               
         else if (MaxRplyElem < ReplyElemCnt)
            DSPError[DspId] = DSPRplyLengthError;

         else if (!VerifyReply (pWordBufr, IdMode, IdValue) || (ReplyValue != ExpectedReply))
            DSPError[DspId] = DSPRplyValue;
            
         else {
            DSPError [DspId] = DSPSuccess;

            RetValue = ReplyElemCnt;
         }
         break;
      }
      if (FuncStatus == 0) 
         DSPError[DspId] = DSPRplyTimeout;
   }

   gpakUnlockAccess (DspId);

   return RetValue;  // Length of reply message
}
static void gpakPackCidPayload_be (ADT_UInt16 *Msg, ADT_UInt8 *pPayloadData, ADT_UInt16 PaylenI16);
static void gpakPackCidPayload_le (ADT_UInt16 *Msg, ADT_UInt8 *pPayloadData, ADT_UInt16 PaylenI16);

void bytesToDSP (ADT_UInt16 *Msg, ADT_UInt8 *pPayloadData, ADT_UInt16 PaylenI16) {
   if (HostBigEndian)
      gpakPackCidPayload_be (Msg, pPayloadData, PaylenI16);
   else
      gpakPackCidPayload_le (Msg, pPayloadData, PaylenI16);
}


#endif

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * DSPSync
 *
 * FUNCTION
 *  Checks to see if communications has been established with DSP.  If not, attempts to open the socket
 *  Called by functions that do not call TransactCmd to establish a connection with the DSP.
 *
 * Inputs
 *   DspId       - Dsp identifier 
 *
 *
 */
void DSPSync (ADT_UInt32 DspId) {

   if (MaxDsps <= DspId)
      return;

   gpakLockAccess   (DspId);
   CheckDspReset    (DspId);
   gpakUnlockAccess (DspId);
   return;
}
//}===================================================================================
//===================================================================================
//{                  Circular buffer access routines
//===================================================================================
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * ReadCircStruct 
 *
 * FUNCTION
 *  Transfers circular buffer structure from DSP to host memory 
 *
 * INPUTS
 *   DspId       - Dsp identifier 
 *   CircBufrAddr - Pointer to DSP memory circular buffer structure
 *
 * OUTPUT
 *   cBuff        - Pointer to host memory circular buffer structure
 *   I16Ready      - Size (16-bit words, DSP word padded) of data available for transfer
 *   I16Free       - Size (16-bit words, DSP word padded) of free space available for transfer
 *
 *   NOTE: Transfers are aligned to DSP word boundaries.  16-bit word sizes are
 *         rounded down to number of available 16-bit words available for transfer
 */
static void ReadCircStruct (ADT_UInt32 DspId, DSP_Address CircBufrAddr, CircBufr_t *cBuff,
                     DSP_Word *I16sFree, DSP_Word *I16sReady) {

    DSP_Word    ElemReady;


   // Read circular buffer structure into host memory
   gpakReadDsp (DspId, CircBufrAddr,  CB_INFO_STRUCT_LEN, (DSP_Word *) cBuff);

   // Calculate elements (16-bit) available
   ElemReady = cBuff->PutIndex - cBuff->TakeIndex;
   if (cBuff->PutIndex < cBuff->TakeIndex)
      ElemReady += (cBuff->BufrSize);

   // Adjust
   *I16sReady = CircAvail (ElemReady);
   *I16sFree  = CircAvail (cBuff->BufrSize - (ElemReady + 2));
   return;
}
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * PurgeCircStruct
 *
 * FUNCTION
 *  Called on errors to purge circular buffer structure (TAKE = PUT)
 *
 * INPUTS
 *   DspId       - Dsp identifier 
 *   cBuff        - Pointer to host memory circular buffer structure
 *
 * OUTPUT
 *   CircBufrAddr - Pointer to DSP memory circular buffer structure
 *
 */

static void PurgeCircStruct (ADT_UInt32 DspId, DSP_Address CircBufrAddr, CircBufr_t *cBuff) {
   
    // Set take index to put index
    cBuff->TakeIndex = cBuff->PutIndex;
    gpakWriteDsp (DspId, CircBufrAddr + CB_BUFR_TAKE_INDEX, 1, &cBuff->TakeIndex);
   return;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * WriteCircBuffer - Write to a DSP circular buffer.
 *
 * FUNCTION
 *  Transfers block of 16 bits words from linear buffer to DSP circular buffer. 
 *  Put address is incremented by the number of 16-bit words written adjusting for buffer wrap.
 *
 * Inputs
 *   DspId       - Dsp identifier 
 *   cBuff       - Pointer to circular buffer structure
 *   hostBuff    - Address of host linear buffer
 *   I16Cnt      - Number of 16-bit words to transfer
 *   cBuffAddr   - non-Null update circular buffer on DSP
 *
 * Updates
 *   cBuff   - Before = Current state of circular buffer
 *           - After  = Put index updated
 *
 * RETURNS
 *  nothing
 *
 */
void WriteCircBuffer (ADT_UInt32 DspId, CircBufr_t *cBuff, DSP_Word *hostBuff, 
                      DSP_Word  I16Cnt, DSP_Address cBuffAddr) {

    DSP_Word    I16sBeforeWrap, I16sAfterWrap;
    DSP_Address PutAddr;
    ADT_UInt16 *pHostBuff;

    // Ensure integrity of circular buffer pointers
    if ((I16Cnt <= 0) || (cBuff->BufrSize <= I16Cnt) || (cBuff->BufrSize <= cBuff->TakeIndex)) return;
    if (cBuff->BufrSize <= cBuff->PutIndex) cBuff->PutIndex = cBuff->TakeIndex;

    pHostBuff = (ADT_UInt16 *) hostBuff;
    PutAddr = CircAddress (cBuff->BufrBase, cBuff->PutIndex);
 
    //  If buffer wraps, divide into two transfers, else single transfer
    I16sBeforeWrap = cBuff->BufrSize - cBuff->PutIndex;   

    if (I16sBeforeWrap < I16Cnt) {
       I16sAfterWrap  = I16Cnt - I16sBeforeWrap;

       gpakWriteDsp (DspId, PutAddr,         CircTxSize (I16sBeforeWrap), (DSP_Word *) pHostBuff);
       gpakWriteDsp (DspId, cBuff->BufrBase, CircTxSize (I16sAfterWrap),  (DSP_Word *) &(pHostBuff[I16sBeforeWrap]));
       cBuff->PutIndex = I16sAfterWrap;
    } else {

        gpakWriteDsp (DspId, PutAddr,        CircTxSize (I16Cnt), (DSP_Word *) pHostBuff);
        if (I16Cnt == I16sBeforeWrap)
            cBuff->PutIndex = 0;  // At exact end of buffer. Next write at beginning
        else
            cBuff->PutIndex += I16Cnt;
    }
    if (cBuffAddr)
      gpakWriteDsp (DspId, cBuffAddr + CB_BUFR_PUT_INDEX,  1, &cBuff->PutIndex);

    return;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * ReadCircBuffer - Read from a DSP circular buffer.
 *
 * FUNCTION
 *  Tranfers block of 16-bit words from a DSP circular buffer to a linear buffer. The Take
 *  address is incremented by the number of bytes read adjusting for buffer wrap.
 *
 * Inputs
 *   DspId       - Dsp identifier 
 *   cBuff       - Pointer to circular buffer structure
 *   hostBuff    - Address of host linear buffer
 *   I16Cnt      - Number of 16-bit words to transfer
 *   cBuffAddr   - non-Null update circular buffer on DSP
 *
 * Updates
 *   cBuff   - Before = Current state of circular buffer
 *           - After  = Take index updated
 *
 * RETURNS
 *  nothing
 *
 */
void ReadCircBuffer (ADT_UInt32 DspId,  CircBufr_t *cBuff, DSP_Word *hostBuff,
                DSP_Word I16Cnt, DSP_Address cBuffAddr) {

    ADT_UInt16 *pHostBuff;
    DSP_Word    I16sBeforeWrap, I16sAfterWrap;
    DSP_Address TakeAddr;

    // Ensure integrity of circular buffer pointers
    if ((I16Cnt <= 0) || (cBuff->BufrSize <= I16Cnt) || (cBuff->BufrSize <= cBuff->PutIndex)) return;
    if (cBuff->BufrSize <= cBuff->TakeIndex) cBuff->TakeIndex = cBuff->PutIndex;


    pHostBuff = (ADT_UInt16 *) hostBuff;
    TakeAddr = CircAddress (cBuff->BufrBase, cBuff->TakeIndex);

    //  If buffer wraps, divide into two transfers, else single transfer
    I16sBeforeWrap = cBuff->BufrSize - cBuff->TakeIndex;
    if (I16sBeforeWrap < I16Cnt) {
       I16sAfterWrap  = I16Cnt - I16sBeforeWrap;

       gpakReadDsp (DspId, TakeAddr,        CircTxSize (I16sBeforeWrap), (DSP_Word *) pHostBuff);
       gpakReadDsp (DspId, cBuff->BufrBase, CircTxSize (I16sAfterWrap),  (DSP_Word *) &(pHostBuff[I16sBeforeWrap]));
       cBuff->TakeIndex = I16sAfterWrap;

    } else   {

       gpakReadDsp (DspId, TakeAddr,        CircTxSize (I16Cnt), (DSP_Word *) pHostBuff);
       if (I16Cnt == I16sBeforeWrap)
           cBuff->TakeIndex = 0;  // At exact end of buffer. Next write at beginning
       else
           cBuff->TakeIndex += I16Cnt;
    }
    if (cBuffAddr)
       gpakWriteDsp (DspId, cBuffAddr + CB_BUFR_TAKE_INDEX,  1, &cBuff->TakeIndex);

    return;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * WriteCircBufferNoSwap - Write to a DSP circular buffer without byte swapping.
 *
 * FUNCTION
 *  Transfers block of 16 bits words from linear buffer to DSP circular buffer. 
 *  Put address is incremented by the number of 16-bit words written adjusting for buffer wrap.
 *
 * Inputs
 *   DspId       - Dsp identifier 
 *   cBuff       - Pointer to circular buffer structure
 *   hostBuff    - Address of host linear buffer
 *   I16Cnt      - Number of 16-bit words to transfer
 *   cBuffAddr   - non-Null update circular buffer on DSP
 *
 * Updates
 *   cBuff   - Before = Current state of circular buffer
 *           - After  = Put index updated
 *
 * RETURNS
 *  nothing
 *
 */
static void WriteCircBufferNoSwap (ADT_UInt32 DspId, CircBufr_t *cBuff, DSP_Word *hostBuff, 
            DSP_Word  I16Cnt, DSP_Address cBuffAddr) {
 
    DSP_Word    I16sBeforeWrap, I16sAfterWrap;
    DSP_Address PutAddr;
    ADT_UInt16 *pHostBuff;
 
    // Ensure integrity of circular buffer pointers
    if ((I16Cnt <= 0) || (cBuff->BufrSize <= I16Cnt) || (cBuff->BufrSize <= cBuff->TakeIndex)) return;
    if (cBuff->BufrSize <= cBuff->PutIndex) cBuff->PutIndex = cBuff->TakeIndex;

    pHostBuff = (ADT_UInt16 *) hostBuff;
    PutAddr = CircAddress (cBuff->BufrBase, cBuff->PutIndex);
 
    //  If buffer wraps, divide into two transfers, else single transfer
    I16sBeforeWrap = cBuff->BufrSize - cBuff->PutIndex;   
    if (I16sBeforeWrap < I16Cnt) {
       I16sAfterWrap  = I16Cnt - I16sBeforeWrap;
 
       gpakWriteDspNoSwap (DspId, PutAddr,         CircTxSize (I16sBeforeWrap), (DSP_Word *) pHostBuff);
       gpakWriteDspNoSwap (DspId, cBuff->BufrBase, CircTxSize (I16sAfterWrap),  (DSP_Word *) &(pHostBuff[I16sBeforeWrap]));
       cBuff->PutIndex = I16sAfterWrap;
    } else {
 
        gpakWriteDspNoSwap (DspId, PutAddr,        CircTxSize (I16Cnt), (DSP_Word *) pHostBuff);
        if (I16Cnt == I16sBeforeWrap)
            cBuff->PutIndex = 0;  // At exact end of buffer. Next write at beginning
        else
            cBuff->PutIndex += I16Cnt;
    }
    if (cBuffAddr)
        gpakWriteDsp (DspId, cBuffAddr + CB_BUFR_PUT_INDEX,  1, &cBuff->PutIndex);
 
    return;
}
 
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * ReadCircBufferNoSwap - Read from a DSP circular buffer with swapping.
 *
 * FUNCTION
 *  Tranfers block of 16-bit words from a DSP circular buffer to a linear buffer. The Take
 *  address is incremented by the number of bytes read adjusting for buffer wrap.
 *
 * Inputs
 *   DspId       - Dsp identifier 
 *   cBuff       - Pointer to circular buffer structure
 *   hostBuff    - Address of host linear buffer
 *   I16Cnt      - Number of 16-bit words to transfer
 *   cBuffAddr   - non-Null update circular buffer on DSP
 *
 * Updates
 *   cBuff   - Before = Current state of circular buffer
 *           - After  = Take index updated
 *
 * RETURNS
 *  nothing
 *
 */
static void ReadCircBufferNoSwap (ADT_UInt32 DspId,  CircBufr_t *cBuff, DSP_Word *hostBuff,
                DSP_Word I16Cnt, DSP_Address cBuffAddr) {

    ADT_UInt16 *pHostBuff;
    DSP_Word    I16sBeforeWrap, I16sAfterWrap;
    DSP_Address TakeAddr;

    // Ensure integrity of circular buffer pointers
    if ((I16Cnt <= 0) || (cBuff->BufrSize <= I16Cnt) || (cBuff->BufrSize <= cBuff->PutIndex)) return;
    if (cBuff->BufrSize <= cBuff->TakeIndex) cBuff->TakeIndex = cBuff->PutIndex;

    pHostBuff = (ADT_UInt16 *) hostBuff;
    TakeAddr = CircAddress (cBuff->BufrBase, cBuff->TakeIndex);

    //  If buffer wraps, divide into two transfers, else single transfer
    I16sBeforeWrap = cBuff->BufrSize - cBuff->TakeIndex;
    if (I16sBeforeWrap < I16Cnt) {
       I16sAfterWrap  = I16Cnt - I16sBeforeWrap;

       gpakReadDspNoSwap (DspId, TakeAddr,        CircTxSize (I16sBeforeWrap), (DSP_Word *) pHostBuff);
       gpakReadDspNoSwap (DspId, cBuff->BufrBase, CircTxSize (I16sAfterWrap),  (DSP_Word *) &(pHostBuff[I16sBeforeWrap]));
       cBuff->TakeIndex = I16sAfterWrap;

    } else   {

       gpakReadDspNoSwap (DspId, TakeAddr,        CircTxSize (I16Cnt), (DSP_Word *) pHostBuff);
       if (I16Cnt == I16sBeforeWrap)
           cBuff->TakeIndex = 0;  // At exact end of buffer. Next write at beginning
       else
           cBuff->TakeIndex += I16Cnt;
    }
    if (cBuffAddr)
      gpakWriteDsp (DspId, cBuffAddr + CB_BUFR_TAKE_INDEX,  1, &cBuff->TakeIndex);
    return;
}

//}==================================================================================
//===================================================================================
//{                  System configuration and status APIs
//===================================================================================

// -----------------------   IP stack
#ifdef DSP_IPCFG_ADDRESS
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakCfgIPStack 
 *
 * FUNCTION
 *  Configures a DSP's IP stack
 *
 * Inputs
 *   DspId     - Dsp identifier 
 *   IPConfig  - IP stack configuration data structure pointer
 *
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
gpakIPStack_t gpakCfgIPStack (ADT_Int32 DspId, GpakIPCfg_t *IPConfig) {
   GpakIPCfg_t ipCfg;

   if (MaxDsps <= (unsigned int) DspId)
        return IPInvalidDSP;

   /* Read the IP configuration from the DSP. */
   gpakReadDspNoSwap32 (DspId, DSP_IPCFG_ADDRESS, BytesToTxUnits (byteSize (ipCfg)), (void *) &ipCfg);

   // Convert RTP defaults to network byte order
   if (HostBigEndian) {
      ipCfg.RTPSrcIP = IPConfig->RTPSrcIP;
   } else
      ipCfg.RTPSrcIP = swapI32 (IPConfig->RTPSrcIP);

   memcpy (&ipCfg.TxMac, IPConfig->TxMac, sizeof (ipCfg.TxMac));

   // If first time, convert IP stack values to network byte order
   if ((ipCfg.RxMac[0] + ipCfg.RxMac[1] + ipCfg.RxMac[2] + 
        ipCfg.RxMac[3] + ipCfg.RxMac[4] + ipCfg.RxMac[5]) == 0) {
      memcpy (&ipCfg.RxMac, IPConfig->RxMac, sizeof (ipCfg.RxMac));

      if (HostBigEndian) {
         ipCfg.IPAddress = IPConfig->IPAddress;
         ipCfg.IPGateway = IPConfig->IPGateway;
         ipCfg.IPMask    = IPConfig->IPMask;

         ipCfg.RTPPort   = IPConfig->RTPPort;
         ipCfg.MsgPort   = IPConfig->MsgPort;
         ipCfg.EvtPort   = IPConfig->EvtPort;
         ipCfg.PortRange = IPConfig->PortRange;
      } else {
         ipCfg.IPAddress = swapI32 (IPConfig->IPAddress);
         ipCfg.IPGateway = swapI32 (IPConfig->IPGateway);
         ipCfg.IPMask    = swapI32 (IPConfig->IPMask);

         ipCfg.RTPPort   = swapI16 (IPConfig->RTPPort);
         ipCfg.MsgPort   = swapI16 (IPConfig->MsgPort);
         ipCfg.EvtPort   = swapI16 (IPConfig->EvtPort);
         ipCfg.PortRange = swapI16 (IPConfig->PortRange);
      }
   }

   ipCfg.DHCPIP = 0;

   /* Write the IP parameters to the DSP. */
   gpakWriteDspNoSwap32 (DspId, DSP_IPCFG_ADDRESS, BytesToTxUnits (sizeof (ipCfg)), (void *) &ipCfg);
   return IPSuccess;

}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadIPStackCfg
 *
 * FUNCTION
 *  Reads a DSP's IP stack configuration
 *
 * Inputs
 *   DspId     - Dsp identifier 
 *   IPConfig  - IP stack configuration data structure pointer
 *
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
gpakIPStack_t gpakReadIPStackCfg (ADT_Int32 DspId, GpakIPCfg_t *IPConfig) {
   GpakIPCfg_t ipCfg;

   if (MaxDsps <= (unsigned int) DspId)
        return IPInvalidDSP;

   /* Read the IP configuration from the DSP. */
   gpakReadDspNoSwap32 (DspId, DSP_IPCFG_ADDRESS, BytesToTxUnits (byteSize (ipCfg)), (void *) &ipCfg);

   memcpy (IPConfig->TxMac, &ipCfg.TxMac, sizeof (ipCfg.TxMac));
   memcpy (IPConfig->RxMac, &ipCfg.RxMac, sizeof (ipCfg.RxMac));

   // Convert IP stack parameters from network byte order
   if (HostBigEndian) {
      IPConfig->RTPSrcIP  = ipCfg.RTPSrcIP;
      IPConfig->IPAddress = ipCfg.IPAddress;
      IPConfig->IPGateway = ipCfg.IPGateway;
      IPConfig->IPMask    = ipCfg.IPMask;

      IPConfig->RTPPort   = ipCfg.RTPPort;
      IPConfig->MsgPort   = ipCfg.MsgPort;
      IPConfig->EvtPort   = ipCfg.EvtPort;
      IPConfig->DHCPIP    = ipCfg.DHCPIP;
   } else {
      IPConfig->RTPSrcIP  = swapI32 (ipCfg.RTPSrcIP);
      IPConfig->IPAddress = swapI32 (ipCfg.IPAddress);
      IPConfig->IPGateway = swapI32 (ipCfg.IPGateway);
      IPConfig->IPMask    = swapI32 (ipCfg.IPMask);   

      IPConfig->RTPPort   = swapI16 (ipCfg.RTPPort);
      IPConfig->MsgPort   = swapI16 (ipCfg.MsgPort);
      IPConfig->EvtPort   = swapI16 (ipCfg.EvtPort);
      IPConfig->DHCPIP    = swapI32 (ipCfg.DHCPIP);
   }
   return IPSuccess;

}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakSetVlanTag
 *
 * FUNCTION
 *  Assigns a VLAN tag and priority to a VLAN index.
 *
 * INPUTS
 *  DspId    DSP identifier.
 *  VlanIdx  VLAN index [0-9]. Used to specify VLAN ID / priority pair.
 *  VlanID   VLAN ID [0-0xfff]. VLAN ID field of VLAN tag.
 *  priority Priority [0-7]. Priority field of VLAN tag.
 *
 * OUTPUTS
 *  dspStat    Dsp return values. See GpakErrs.h
 *
 * RETURNS
 *  GpakApiSuccess � The DSP memory image was downloaded successfully.
 *  GpakApiParmError � A parameter error occurred. The dspStat will provide details.
 *  GpakApiInvalidDsp � The value of DspId is not a valid DSP identifier.
 *
 */
ADT_UInt32 gpakFormatSetVlanTagMsg (ADT_Int16 *Msg, ADT_UInt16 VlanIdx, ADT_UInt16 VlanID, ADT_UInt8 priority) {
   Msg[0] = (ADT_UInt16) PackBytes (MSG_SET_VLAN_TAG, VlanIdx);
   Msg[1] = VlanID;    
   Msg[2] = (ADT_UInt16) PackBytes (priority, 0);
   return 3;
}
GpakApiStatus_t gpakSetVlanTag (ADT_UInt32 DspId, ADT_UInt16 VlanIdx, ADT_UInt16 VlanID, 
            ADT_UInt8 priority, GPAK_VlanStat_t *dspStat) {
    ADT_UInt32 MsgLenI16;                        /* message length */
    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_Int16 *Msg;

    if (MaxDsps <= DspId)
       return GpakApiInvalidDsp;

    Msg = (ADT_Int16 *) &MsgBuffer[0];

    MsgLenI16 = gpakFormatSetVlanTagMsg (Msg, VlanIdx, VlanID, priority);

    if (!TransactCmd (DspId, MsgBuffer, MsgLenI16, MSG_SET_VLAN_TAG_REPLY,  2, BYTE_ID, VlanIdx))
        return GpakApiCommFailure;

    *dspStat = (GPAK_VlanStat_t) Byte0 (Msg[1]);
    if (*dspStat == vlanSuccess)
       return GpakApiSuccess;

    return GpakApiParmError;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakGetVlanTag
 *
 * FUNCTION
 *  Retreives the VLAN tag and priority assigned to a VLAN index.
 *
 * INPUTS
 *  DspId    DSP identifier.
 *  VlanIdx  VLAN index [0-9]. Used to specify VLAN ID / priority pair.
 *
 * OUTPUTS
 *  VlanID   VLAN ID [0-0xfff]. VLAN ID field of VLAN tag.
 *  priority Priority [0-7].    Priority field of VLAN tag.
 *  dspStat    Dsp return values. See GpakErrs.h
 *
 * RETURNS
 *  GpakApiSuccess � The DSP memory image was downloaded successfully.
 *  GpakApiParmError � A parameter error occurred. The dspStat will provide details.
 *  GpakApiInvalidDsp � The value of DspId is not a valid DSP identifier.
 *
 */
GpakApiStatus_t gpakGetVlanTag (ADT_UInt32 DspId, ADT_UInt16 VlanIdx, 
                  ADT_UInt16 *VlanID, ADT_UInt8 *priority, GPAK_VlanStat_t *dspStat) {

   ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
   ADT_Int16 *Msg;

   if (MaxDsps <= DspId)
      return GpakApiInvalidDsp;

   Msg = (ADT_Int16 *) &MsgBuffer[0];
   Msg[0] = (ADT_UInt16) PackBytes (MSG_GET_VLAN_TAG, VlanIdx);

   if (!TransactCmd (DspId, MsgBuffer, 1, MSG_GET_VLAN_TAG_REPLY,  3, BYTE_ID, VlanIdx))
        return GpakApiCommFailure;

    *dspStat = (GPAK_VlanStat_t) Byte0 (Msg[1]);
    if (*dspStat != vlanSuccess)
       return GpakApiParmError;

   *VlanID   = Msg[2] & 0xfff;
   *priority = (Msg[2] >> 13) & 0x7;
   return GpakApiSuccess;
}
#endif


// ----------------------   System parameters
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakGetSystemConfig - Read a DSP's System Configuration.
 *
 * FUNCTION
 *  Reads a DSP's System Configuration information.
 *
 * Inputs
 *   DspId       - Dsp identifier 
 *  sysCfg   - System configuration data structure pointer
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
void                     gpakParseSystemConfig (ADT_UInt16 *Msg, GpakSystemConfig_t *sysCfg) {

   /* Extract the System Configuration information from the message. */
   sysCfg->GpakVersionId   = PackWords (Msg[1], Msg[2]);
   sysCfg->MaxChannels     = (ADT_UInt16) Byte1 (Msg[3]);
   sysCfg->CfgEnableFlags  = PackWords (Msg[4], Msg[5]);
   sysCfg->PacketProfile   = (GpakProfiles) Msg[6];
   sysCfg->RtpTonePktType  = (GpakRTPPkts) Msg[7];
   sysCfg->CfgFramesChans  = Msg[8];
   sysCfg->Port1NumSlots   = Msg[9];
   sysCfg->Port2NumSlots   = Msg[10];
   sysCfg->Port3NumSlots   = Msg[11];
   sysCfg->Port1SupSlots   = Msg[12];
   sysCfg->Port2SupSlots   = Msg[13];
   sysCfg->Port3SupSlots   = Msg[14];
   sysCfg->PcmMaxTailLen   = Msg[15];
   sysCfg->PcmMaxFirSegs   = Msg[16];
   sysCfg->PcmMaxFirSegLen = Msg[17];
   sysCfg->PktMaxTailLen   = Msg[18];
   sysCfg->PktMaxFirSegs   = Msg[19];
   sysCfg->PktMaxFirSegLen = Msg[20];
   sysCfg->MaxConferences  = (ADT_UInt16) Byte1 (Msg[21]);
   sysCfg->MaxNumConfChans = (ADT_UInt16) Byte0 (Msg[21]);
   sysCfg->SelfTestStatus  = Msg[22];
   sysCfg->NumPcmEcans     = Msg[23];
   sysCfg->NumPktEcans     = Msg[24];
   sysCfg->MaxToneDetTypes = (ADT_UInt16) Byte0 (Msg[25]);
   sysCfg->AECInstances    = Msg[26];
   sysCfg->AECTailLen      = Msg[27];
   sysCfg->ActiveChannels  = ((ADT_UInt16) Byte0 (Msg[3])) | ((ADT_UInt16) Byte1 (Msg[25]) << 8);
   if (sysCfg->MaxChannels == 0) sysCfg->MaxChannels = 256;
}

gpakGetSysCfgStatus_t    gpakGetSystemConfig (ADT_UInt32 DspId, GpakSystemConfig_t *sysCfg) {

    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 *Msg;



    if (MaxDsps <= DspId)
        return GpakApiInvalidDsp;

    Msg    = (ADT_UInt16 *) MsgBuffer;

    Msg[0] = MSG_SYS_CONFIG_RQST << 8;   // Request Message

    if (!TransactCmd (DspId, MsgBuffer, 1, MSG_SYS_CONFIG_REPLY, 28, NO_ID_CHK, 0))
        return GpakApiCommFailure;

    gpakParseSystemConfig (Msg, sysCfg);
    return GpakApiSuccess;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadSystemParms - Read a DSP's System Parameters.
 *
 * FUNCTION
 *  Reads a DSP's System Parameters information.
 *
 * Inputs
 *   DspId       - Dsp identifier 
 *  pSysParams   - System parameters data structure pointer
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
void                     gpakParseSystemParms (ADT_Int16 *Msg, GpakSystemParms_t *sysPrms) {

    ADT_Int16 temp;
   
    /* Extract the System Parameters information from the message. */
    sysPrms->AGC_A.TargetPower       = Msg[1];
    sysPrms->AGC_A.LossLimit         = Msg[2];
    sysPrms->AGC_A.GainLimit         = Msg[3];
    sysPrms->AGC_A.LowSignal         = Msg[34];

    sysPrms->VadNoiseFloor        = Msg[4];
    sysPrms->VadHangTime          = Msg[5];
    sysPrms->VadWindowSize        = Msg[6];

    temp                            = Msg[7]; 
    sysPrms->PcmEc.NlpType         = (ADT_Int16) (temp & 0x000F);
    sysPrms->PcmEc.AdaptEnable     = (ADT_Int16) ((temp >> 4) & 0x0001);
    sysPrms->PcmEc.G165DetEnable   = (ADT_Int16) ((temp >> 5) & 0x0001);
    sysPrms->PcmEc.MixedFourWireMode = (ADT_Int16) ((temp >> 6) & 0x0001);
    sysPrms->PcmEc.ReconvergenceCheckEnable = (ADT_Int16) ((temp >> 7) & 0x0001);

    sysPrms->PktEc.NlpType         = (ADT_Int16) ((temp >> 8) & 0x000F);
    sysPrms->PktEc.AdaptEnable     = (ADT_Int16) ((temp >> 12) & 0x0001);
    sysPrms->PktEc.G165DetEnable   = (ADT_Int16) ((temp >> 13) & 0x0001);
    sysPrms->PktEc.MixedFourWireMode = (ADT_Int16) ((temp >> 14) & 0x0001);
    sysPrms->PktEc.ReconvergenceCheckEnable = (ADT_Int16) ((temp >> 15) & 0x0001);

    sysPrms->PcmEc.TailLength      = Msg[8];
    sysPrms->PcmEc.DblTalkThresh   = Msg[9];
    sysPrms->PcmEc.NlpThreshold    = Msg[10];
    sysPrms->PcmEc.NlpUpperLimitThreshConv = Msg[11];
    sysPrms->PcmEc.NlpUpperLimitThreshPreconv= Msg[12];
    sysPrms->PcmEc.NlpMaxSupp        = Msg[13];
    sysPrms->PcmEc.CngThreshold    = Msg[14];
    sysPrms->PcmEc.AdaptLimit      = Msg[15];
    sysPrms->PcmEc.CrossCorrLimit  = Msg[16];
    sysPrms->PcmEc.NumFirSegments  = Msg[17];
    sysPrms->PcmEc.FirSegmentLen   = Msg[18];
    sysPrms->PcmEc.FirTapCheckPeriod = Msg[19];
    sysPrms->PcmEc.MaxDoubleTalkThres = Msg[46];
  
    sysPrms->PktEc.TailLength      = Msg[20];
    sysPrms->PktEc.DblTalkThresh   = Msg[21];
    sysPrms->PktEc.NlpThreshold    = Msg[22];
    sysPrms->PktEc.NlpUpperLimitThreshConv = Msg[23];
    sysPrms->PktEc.NlpUpperLimitThreshPreconv= Msg[24];
    sysPrms->PktEc.NlpMaxSupp        = Msg[25];
    sysPrms->PktEc.CngThreshold    = Msg[26];
    sysPrms->PktEc.AdaptLimit      = Msg[27];
    sysPrms->PktEc.CrossCorrLimit  = Msg[28];
    sysPrms->PktEc.NumFirSegments  = Msg[29];
    sysPrms->PktEc.FirSegmentLen   = Msg[30];
    sysPrms->PktEc.FirTapCheckPeriod = Msg[31];
    sysPrms->PktEc.MaxDoubleTalkThres = Msg[47];

    sysPrms->ConfNumDominant      = Msg[32];
    sysPrms->MaxConfNoiseSuppress = Msg[33];

    sysPrms->AGC_B.TargetPower       = Msg[35];
    sysPrms->AGC_B.LossLimit         = Msg[36];
    sysPrms->AGC_B.GainLimit         = Msg[37];
    sysPrms->AGC_B.LowSignal         = Msg[38];

    // 4_2 ---------------------------------------
    sysPrms->Cid.type2CID           = Msg[39] & 1;
    sysPrms->Cid.numSeizureBytes    = Msg[40];
    sysPrms->Cid.numMarkBytes       = Msg[41];
    sysPrms->Cid.numPostambleBytes  = Msg[42];
    sysPrms->Cid.fskType            = (GpakFskType) Msg[43];
    sysPrms->Cid.fskLevel           = Msg[44];
    sysPrms->Cid.msgType            = Msg[45];
    sysPrms->VadReport              = (Msg[39] & 2) >> 1;
    // -------------------------------------------

}

gpakReadSysParmsStatus_t gpakReadSystemParms (ADT_UInt32 DspId, GpakSystemParms_t *sysPrms) {

    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_Int16  *Msg;


    if (MaxDsps <= DspId)
        return RspInvalidDsp;

    Msg = (ADT_Int16 *) MsgBuffer;

    Msg[0] = MSG_READ_SYS_PARMS << 8;

    if (!TransactCmd (DspId, MsgBuffer, 1, MSG_READ_SYS_PARMS_REPLY, 48, NO_ID_CHK, 0))
        return GpakApiCommFailure;

    gpakParseSystemParms (Msg, sysPrms);

    
    /* Return with an indication that System Parameters info was obtained. */
    return GpakApiSuccess;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakWriteSystemParms - Write a DSP's System Parameters.
 *
 * FUNCTION
 *  Writes selective System Parameters to a DSP.
 *
 * Inputs
 *   DspId       - Dsp identifier 
 *  sysPrms    - System parameters data structure pointer
 *  AgcUpdate    - 1  ==  Write A-device AGC parameters, 2 == Write B-device AGC parameters
 *  VadUpdate    - Non-zero .  Write VAD parameters
 *  PcmEcUpdate  - Non-zero .  Write Pcm Echo canceller parameters
 *  PktEcUpdate  - Non-zero .  Write Pkt Echo canceller parameters
 *  ConfUpdate   - Non-zero .  Write Conference parameters
 *
 * Outputs
 *   pStatus     - System parms status from DSP
 * 
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
ADT_UInt32               gpakFormatSystemParms (ADT_Int16 *Msg, GpakSystemParms_t *sysPrms,
                                               ADT_UInt16 AgcUpdate,   ADT_UInt16 VadUpdate, ADT_UInt16 PcmEcUpdate,
                                               ADT_UInt16 PktEcUpdate, ADT_UInt16 ConfUpdate, ADT_UInt16 CIDUpdate) {

    ADT_UInt32 MsgLenI16, i;

    MsgLenI16 = 49;
    for (i=0; i<MsgLenI16; i++) Msg[i] = 0;

    /* Build the Write System Parameters message. */
    Msg[0] = MSG_WRITE_SYS_PARMS << 8;
    Msg[1] = 0;    

    if (AgcUpdate  & 1) {
        Msg[1] |= 1;
        Msg[2] = sysPrms->AGC_A.TargetPower;
        Msg[3] = sysPrms->AGC_A.LossLimit;
        Msg[4] = sysPrms->AGC_A.GainLimit;
        Msg[35] = sysPrms->AGC_A.LowSignal;
    }
    if (AgcUpdate  & 2) {
        Msg[1] |= 0x0020;
        Msg[36] = sysPrms->AGC_B.TargetPower;
        Msg[37] = sysPrms->AGC_B.LossLimit;
        Msg[38] = sysPrms->AGC_B.GainLimit;
        Msg[39] = sysPrms->AGC_B.LowSignal;
    }

    if (VadUpdate)    {
        Msg[1] |= 2;
        Msg[5] = sysPrms->VadNoiseFloor;
        Msg[6] = sysPrms->VadHangTime;
        Msg[7] = sysPrms->VadWindowSize;
    }
    if (PcmEcUpdate || PktEcUpdate)   {
        Msg[8] = (ADT_Int16)
                   ((sysPrms->PcmEc.NlpType & 0x000F) |
                   ((sysPrms->PcmEc.AdaptEnable << 4) & 0x0010) |
                   ((sysPrms->PcmEc.G165DetEnable << 5) & 0x0020) |
                   ((sysPrms->PcmEc.MixedFourWireMode << 6) & 0x0040) |
                   ((sysPrms->PcmEc.ReconvergenceCheckEnable << 7) & 0x0080) |
                   ((sysPrms->PktEc.NlpType << 8) & 0x0F00) |
                   ((sysPrms->PktEc.AdaptEnable << 12) & 0x1000) |
                   ((sysPrms->PktEc.G165DetEnable << 13) & 0x2000) |
                   ((sysPrms->PktEc.MixedFourWireMode << 14) & 0x4000) |
                   ((sysPrms->PktEc.ReconvergenceCheckEnable << 15) & 0x8000));
    }
    if (PcmEcUpdate)    {
        Msg[1] |= 4;
        Msg[9]  = sysPrms->PcmEc.TailLength;
        Msg[10] = sysPrms->PcmEc.DblTalkThresh;
        Msg[11] = sysPrms->PcmEc.NlpThreshold;
        Msg[12] = sysPrms->PcmEc.NlpUpperLimitThreshConv;
        Msg[13] = sysPrms->PcmEc.NlpUpperLimitThreshPreconv;
        Msg[14] = sysPrms->PcmEc.NlpMaxSupp;

        Msg[15] = sysPrms->PcmEc.CngThreshold;
        Msg[16] = sysPrms->PcmEc.AdaptLimit;
        Msg[17] = sysPrms->PcmEc.CrossCorrLimit;
        Msg[18] = sysPrms->PcmEc.NumFirSegments;
        Msg[19] = sysPrms->PcmEc.FirSegmentLen;
        Msg[20] = sysPrms->PcmEc.FirTapCheckPeriod;
        Msg[47] = sysPrms->PcmEc.MaxDoubleTalkThres;
    }
    if (PktEcUpdate)    {
        Msg[1] |= 8;
        Msg[21] = sysPrms->PktEc.TailLength;
        Msg[22] = sysPrms->PktEc.DblTalkThresh;
        Msg[23] = sysPrms->PktEc.NlpThreshold;
        Msg[24] = sysPrms->PktEc.NlpUpperLimitThreshConv;
        Msg[25] = sysPrms->PktEc.NlpUpperLimitThreshPreconv;
        Msg[26] = sysPrms->PktEc.NlpMaxSupp;

        Msg[27] = sysPrms->PktEc.CngThreshold;
        Msg[28] = sysPrms->PktEc.AdaptLimit;
        Msg[29] = sysPrms->PktEc.CrossCorrLimit;
        Msg[30] = sysPrms->PktEc.NumFirSegments;
        Msg[31] = sysPrms->PktEc.FirSegmentLen;
        Msg[32] = sysPrms->PktEc.FirTapCheckPeriod;
        Msg[48] = sysPrms->PktEc.MaxDoubleTalkThres;
    }
    if (ConfUpdate)  {
        Msg[1] |= 16;
        Msg[33] = sysPrms->ConfNumDominant;
        Msg[34] = sysPrms->MaxConfNoiseSuppress;
    }


    // 4_2 ---------------------------------------
    if (CIDUpdate) {
        Msg[1] |= 0x40;
        Msg[40] = sysPrms->Cid.type2CID;
        Msg[41] = sysPrms->Cid.numSeizureBytes;
        Msg[42] = sysPrms->Cid.numMarkBytes;
        Msg[43] = sysPrms->Cid.numPostambleBytes;
        Msg[44] = sysPrms->Cid.fskType;
        Msg[45] = sysPrms->Cid.fskLevel;
        Msg[46] = sysPrms->Cid.msgType;
    }

    if (sysPrms->VadReport)
        Msg[1] |= 0x80;

    return MsgLenI16;
}

gpakWriteSysParmsStatus_t gpakWriteSystemParms (ADT_UInt32 DspId, GpakSystemParms_t *sysPrms,
                                               ADT_UInt16 AgcUpdate,   ADT_UInt16 VadUpdate, ADT_UInt16 PcmEcUpdate,
                                               ADT_UInt16 PktEcUpdate, ADT_UInt16 ConfUpdate,  ADT_UInt16 CIDUpdate, 
                                               GPAK_SysParmsStat_t *pStatus) {

    ADT_UInt32 MsgLenI16;                        /* message length */
    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_Int16 *Msg;


    if (MaxDsps <= DspId)
        return WspInvalidDsp;

    Msg = (ADT_Int16 *) &MsgBuffer[0];

    MsgLenI16 = gpakFormatSystemParms (Msg, sysPrms, AgcUpdate,   VadUpdate,
                                  PcmEcUpdate,  PktEcUpdate, ConfUpdate, 
                                    CIDUpdate);

    if (!TransactCmd (DspId, MsgBuffer, MsgLenI16, MSG_WRITE_SYS_PARMS_REPLY,  2, NO_ID_CHK, 0))
        return GpakApiCommFailure;

    *pStatus = (GPAK_SysParmsStat_t) Byte1 (Msg[1]);
    if (*pStatus == Sp_Success)
        return GpakApiSuccess;
    else
        return GpakApiParmError;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakDtmfDetConfig - Write a DSP's DTMF detector configuration parameters.
 *
 * FUNCTION
 *  Writes selective DTMF Detector Parameters to a DSP. Calling this API can 
 *  over-ride the DTMF detector's default global settings. Changing these settings
 *  will affect all dtmf detect instances, current and future. It's recommended
 *  to call this API while no detectors are running.
 *
 * Inputs
 *  DspId               - Dsp identifier 
 *  dtmfPrms            - Dtmf parameters data structure pointer
 *  MinSigLevUpdate     - flag to update min. signal level param. (0 == mo update)
 *  MaxFreqDevUpdate    - flag to update max. frequency deviation param. (0 == mo update)
 *  MaxTwistUpdate      - flag to update max. twist param. (0 == mo update)
 *  RestoreDefaults     - flag to restore all settings to default. param. (0 == no restore)
 *
 * Outputs
 *   pStatus     - dtmf parms status from DSP
 * 
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
ADT_UInt32              gpakFormatDtmfDetConfigParms (ADT_Int16 *Msg, GpakDtmfDetParms_t *dtmfPrms, GpakDTMFCriteria_t CriteriaSet) {

    ADT_UInt32 MsgLenI16;
    ADT_UInt16 UnwaivedBits;

    memset (Msg, 0, 20);
    /* Build the Write System Parameters message. */
    Msg[0] = MSG_WRITE_DTMFCFG_PARMS << 8;
    Msg[1] = 0;

    if (CriteriaSet == DTMF_Waivers) Msg[1] |= 0x1000;

    if (dtmfPrms->UpdateMinSignalLevel) {
        Msg[1] |= 2;
        Msg[2] = dtmfPrms->MinDetectSignalLevDbm;
    }

    if (dtmfPrms->UpdateMaxFreqDev)    {
        Msg[1] |= 4;
        Msg[3] = dtmfPrms->MaxFreqDevPercent_x10;
    }

    if (dtmfPrms->UpdateMaxTwist)   {
        Msg[1] |= 8;
        Msg[4] = dtmfPrms->MaxFwdTwistDb;
        Msg[5] = dtmfPrms->MaxRevTwistDb;
    }

    if (dtmfPrms->UpdateToneQuality) {
       Msg[1] |= 0x10;
       Msg[6] = dtmfPrms->ToneQuality;
    }

    if (dtmfPrms->UpdateMaxWaivers) {
       Msg[1] |= 0x20;
       Msg[7] = dtmfPrms->MaxWaivers;
    }

    if (dtmfPrms->UpdateWaiverEnable) {
       Msg[1] |= 0x40;
       UnwaivedBits = 0;
       if (!dtmfPrms->WaiverEnable.Power)        UnwaivedBits |= 1;
       if (!dtmfPrms->WaiverEnable.FrequencyDev) UnwaivedBits |= 2;
       if (!dtmfPrms->WaiverEnable.Twist)        UnwaivedBits |= 4;
       if (!dtmfPrms->WaiverEnable.SNR)          UnwaivedBits |= 8;
       if (!dtmfPrms->WaiverEnable.ToneGap)      UnwaivedBits |= 0x10;
       Msg[8] = UnwaivedBits;
    }
    MsgLenI16 = 9;

    return MsgLenI16;
}

GpakApiStatus_t gpakDtmfDetConfig (ADT_UInt32 DspId, GpakDtmfDetParms_t *dtmfPrms, GpakDTMFCriteria_t CriteriaSet,
                                   GPAK_DtmfDetConfigStat_t *pStatus) {

    ADT_UInt32 MsgLenI16;                        /* message length */
    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_Int16 *Msg;


    if (MaxDsps <= DspId)
        return GpakApiInvalidDsp;;

    Msg = (ADT_Int16 *) &MsgBuffer[0];

    MsgLenI16 = gpakFormatDtmfDetConfigParms (Msg, dtmfPrms, CriteriaSet);

    if (!TransactCmd (DspId, MsgBuffer, MsgLenI16, MSG_WRITE_DTMFCFG_PARMS_REPLY,  2, NO_ID_CHK, 0))
        return GpakApiCommFailure;

    *pStatus = (GPAK_DtmfDetConfigStat_t) Byte1 (Msg[1]);
    if (*pStatus == Dc_Success)
        return GpakApiSuccess;
    else
        return GpakApiParmError;
}

ADT_UInt32  gpakFormatSpeexParams (ADT_Int16 *Msg, ADT_UInt8 quality, ADT_UInt8 complexity,
                                   ADT_Bool variableRate, ADT_Bool perceptualEnhancement) {

   /* Build the Speex Parameters message. */
   Msg[0] = MSG_WRITE_SPEEX_PARAMS << 8;
   Msg[1] = (ADT_UInt16) PackBytes (quality, complexity);
   if (variableRate)          Msg[0] |= 1;
   if (perceptualEnhancement) Msg[0] |= 2;

   return 2;
}

GpakApiStatus_t gpakConfigSpeex (ADT_UInt32 DspId, ADT_UInt8 quality, ADT_UInt8 complexity, ADT_Bool variableRate,
                                 ADT_Bool perceptualEnhancement, GPAK_SpeexParamsStat_t *dspStatus) {

   ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
   ADT_Int16 *Msg;
   ADT_UInt32 MsgI16;

   if (MaxDsps <= DspId)
     return WspInvalidDsp;

   Msg = (ADT_Int16 *) &MsgBuffer[0];

   MsgI16 = gpakFormatSpeexParams (Msg, quality, complexity, variableRate, perceptualEnhancement);
   if (!TransactCmd (DspId, MsgBuffer, MsgI16, MSG_WRITE_SPEEX_PARAMS_REPLY, 2, NO_ID_CHK, 0))
     return GpakApiCommFailure;

   *dspStatus = (GPAK_SpeexParamsStat_t) Byte0 (Msg[1]);
   if (*dspStatus == SpxSuccess)
     return GpakApiSuccess;

   return GpakApiParmError;

}

// ----------------------   Acoustic echo canceller

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadAECParms
 *
 * FUNCTION
 *  This function read the DSP's current Acoustic Echo Canceller settings.
 *
 * Inputs
 *      DspId         - Dsp identifier 
 *      AECParams     - Configuration params 
 *
 * Outputs
 *      Status  - DSP return status
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
#if (defined(AEC_LIB_VERSION) && (AEC_LIB_VERSION >= 0x0420))
GpakApiStatus_t gpakParseAecReadParmsResponse (ADT_UInt32 MsgLenI16, ADT_UInt16 *Msg, GpakAECParms_t   *AECParms) {

    if (MsgLenI16 == 1)
       return GpakApiNotConfigured;

    AECParms->activeTailLengthMSec    = (ADT_Int16) Msg[1];
    AECParms->totalTailLengthMSec     = (ADT_Int16) Msg[2];
    AECParms->txNLPAggressiveness     = (ADT_Int16) Msg[3];
    AECParms->maxTxLossSTdB           = (ADT_Int16) Msg[4];
    AECParms->maxTxLossDTdB           = (ADT_Int16) Msg[5];
    AECParms->maxRxLossdB             = (ADT_Int16) Msg[6];
    AECParms->initialRxOutAttendB     = (ADT_Int16) Msg[7];
    AECParms->targetResidualLeveldBm  = (ADT_Int16) Msg[8];
    AECParms->maxRxNoiseLeveldBm      = (ADT_Int16) Msg[9];
    AECParms->worstExpectedERLdB      = (ADT_Int16) Msg[10];
    AECParms->rxSaturateLeveldBm      = (ADT_Int16) Msg[11];
    AECParms->noiseReduction1Setting  = (ADT_Int16) Msg[12];
    AECParms->noiseReduction2Setting  = (ADT_Int16) Msg[13];
    AECParms->cngEnable               = (GpakActivation) Msg[14];
    AECParms->fixedGaindB10           = (ADT_Int16) Msg[15];
    AECParms->txAGCEnable             = (GpakActivation) Msg[16];
    AECParms->txAGCMaxGaindB          = (ADT_Int16) Msg[17];
    AECParms->txAGCMaxLossdB          = (ADT_Int16) Msg[18];
    AECParms->txAGCTargetLeveldBm     = (ADT_Int16) Msg[19];
    AECParms->txAGCLowSigThreshdBm    = (ADT_Int16) Msg[20];
    AECParms->rxAGCEnable             = (GpakActivation) Msg[21];
    AECParms->rxAGCMaxGaindB          = (ADT_Int16) Msg[22];
    AECParms->rxAGCMaxLossdB          = (ADT_Int16) Msg[23];
    AECParms->rxAGCTargetLeveldBm     = (ADT_Int16) Msg[24];
    AECParms->rxAGCLowSigThreshdBm    = (ADT_Int16) Msg[25];
    AECParms->rxBypassEnable          = (GpakActivation) Msg[26];
    AECParms->maxTrainingTimeMSec     = (ADT_Int16) Msg[27];
    AECParms->trainingRxNoiseLeveldBm = (ADT_Int16) Msg[28];
#if (AEC_LIB_VERSION >= 0x0430)
    AECParms->antiHowlEnable          = (GpakActivation) Msg[29];
#endif

    return GpakApiSuccess;
}

GpakApiStatus_t  gpakReadAECParms (ADT_UInt32 DspId, GpakAECParms_t   *AECParms) {

    ADT_UInt32  MsgLenI16;                        /* message length */
    ADT_UInt32  MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 *Msg;

    if (MaxDsps <= DspId)
        return AECInvalidDsp;

    if (pDspIfBlk[DspId] == 0)
       DSPSync (DspId);

    Msg = (ADT_UInt16 *) MsgBuffer;

    Msg[0] = MSG_READ_AEC_PARMS << 8;

    MsgLenI16 = TransactCmd (DspId, MsgBuffer, 2, MSG_READ_AEC_PARMS_REPLY, MSG_BUFFER_ELEMENTS, 
                        NO_ID_CHK, 0);
    if (!MsgLenI16)
        return GpakApiCommFailure;

    return gpakParseAecReadParmsResponse (MsgLenI16, Msg, AECParms);

}
#else
GpakApiStatus_t gpakParseAecReadParmsResponse (ADT_UInt32 MsgLenI16, ADT_UInt16 *Msg, GpakAECParms_t   *AECParms) {

    if (MsgLenI16 == 1)
       return GpakApiNotConfigured;

    AECParms->activeTailLengthMSec     = (ADT_Int16) Msg[1]; 
    AECParms->totalTailLengthMSec      = (ADT_Int16) Msg[2]; 
    AECParms->maxTxNLPThresholddB      = (ADT_Int16) Msg[3]; 
    AECParms->maxTxLossdB              = (ADT_Int16) Msg[4]; 
    AECParms->maxRxLossdB              = (ADT_Int16) Msg[5]; 
    AECParms->targetResidualLeveldBm   = (ADT_Int16) Msg[6]; 
    AECParms->maxRxNoiseLeveldBm       = (ADT_Int16) Msg[7]; 
    AECParms->worstExpectedERLdB       = (ADT_Int16) Msg[8]; 
    AECParms->noiseReductionEnable     = (GpakActivation) Msg[9]; 
    AECParms->cngEnable                = (GpakActivation) Msg[10];
    AECParms->agcEnable                = (GpakActivation) Msg[11];
    AECParms->agcMaxGaindB             = (ADT_Int16) Msg[12];
    AECParms->agcMaxLossdB             = (ADT_Int16) Msg[13];
    AECParms->agcTargetLeveldBm        = (ADT_Int16) Msg[14];
    AECParms->agcLowSigThreshdBm       = (ADT_Int16) Msg[15];
    AECParms->maxTrainingTimeMSec      = (ADT_UInt16) Msg[16];
    AECParms->rxSaturateLeveldBm       = (ADT_Int16) Msg[17];
    AECParms->trainingRxNoiseLeveldBm  = (ADT_Int16) Msg[18];  
    AECParms->fixedGaindB10            = (ADT_Int16) Msg[19];  

    return GpakApiSuccess;
}

GpakApiStatus_t  gpakReadAECParms (ADT_UInt32 DspId, GpakAECParms_t   *AECParms) {

    ADT_UInt32  MsgLenI16;                        /* message length */
    ADT_UInt32  MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 *Msg;

    if (MaxDsps <= DspId)
        return AECInvalidDsp;

    if (pDspIfBlk[DspId] == 0)
       DSPSync (DspId);

    Msg = (ADT_UInt16 *) MsgBuffer;

    Msg[0] = MSG_READ_AEC_PARMS << 8;

    MsgLenI16 = TransactCmd (DspId, MsgBuffer, 2, MSG_READ_AEC_PARMS_REPLY, MSG_BUFFER_ELEMENTS, 
                        NO_ID_CHK, 0);
    if (!MsgLenI16)
        return GpakApiCommFailure;

    return gpakParseAecReadParmsResponse (MsgLenI16, Msg, AECParms);

}
#endif

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakWriteAECParms
 *
 * FUNCTION
 *  This function configures a DSP's Acoustic Echo Canceller.  The parameter that
 *  are in effect at time of channel setup are used for the life time of the channel
 *
 * Inputs
 *      DspId         - Dsp identifier 
 *      ArbParams     - Configuration params 
 *
 * Outputs
 *      Status  - DSP return status
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
#if (defined(AEC_LIB_VERSION) && (AEC_LIB_VERSION >= 0x0420))
ADT_UInt32 gpakFormatAecWriteParmsMsg (ADT_UInt16 *Msg, GpakAECParms_t   *AECParms) {

    /* Build the Write AEC Parameters message. */
    Msg[0]   = MSG_WRITE_AEC_PARMS  << 8;
    Msg[1]   = (ADT_UInt16) AECParms->activeTailLengthMSec;
    Msg[2]   = (ADT_UInt16) AECParms->totalTailLengthMSec;
    Msg[3]   = (ADT_UInt16) AECParms->txNLPAggressiveness;
    Msg[4]   = (ADT_UInt16) AECParms->maxTxLossSTdB;
    Msg[5]   = (ADT_UInt16) AECParms->maxTxLossDTdB;
    Msg[6]   = (ADT_UInt16) AECParms->maxRxLossdB;
    Msg[7]   = (ADT_UInt16) AECParms->initialRxOutAttendB;
    Msg[8]   = (ADT_UInt16) AECParms->targetResidualLeveldBm;
    Msg[9]   = (ADT_UInt16) AECParms->maxRxNoiseLeveldBm;
    Msg[10]  = (ADT_UInt16) AECParms->worstExpectedERLdB;
    Msg[11]  = (ADT_UInt16) AECParms->rxSaturateLeveldBm;
    Msg[12]  = (ADT_UInt16) AECParms->noiseReduction1Setting;
    Msg[13]  = (ADT_UInt16) AECParms->noiseReduction2Setting;
    Msg[14]  = (ADT_UInt16) AECParms->cngEnable;
    Msg[15]  = (ADT_UInt16) AECParms->fixedGaindB10;
    Msg[16]  = (ADT_UInt16) AECParms->txAGCEnable;
    Msg[17]  = (ADT_UInt16) AECParms->txAGCMaxGaindB;
    Msg[18]  = (ADT_UInt16) AECParms->txAGCMaxLossdB;
    Msg[19]  = (ADT_UInt16) AECParms->txAGCTargetLeveldBm;
    Msg[20]  = (ADT_UInt16) AECParms->txAGCLowSigThreshdBm;
    Msg[21]  = (ADT_UInt16) AECParms->rxAGCEnable;
    Msg[22]  = (ADT_UInt16) AECParms->rxAGCMaxGaindB;
    Msg[23]  = (ADT_UInt16) AECParms->rxAGCMaxLossdB;
    Msg[24]  = (ADT_UInt16) AECParms->rxAGCTargetLeveldBm;
    Msg[25]  = (ADT_UInt16) AECParms->rxAGCLowSigThreshdBm;
    Msg[26]  = (ADT_UInt16) AECParms->rxBypassEnable;
    Msg[27]  = (ADT_UInt16) AECParms->maxTrainingTimeMSec;
    Msg[28]  = (ADT_UInt16) AECParms->trainingRxNoiseLeveldBm;
#if (AEC_LIB_VERSION >= 0x0430)
    Msg[29]  = (ADT_UInt16) AECParms->antiHowlEnable;
    return 30;
#else
    return 29;
#endif
}


GpakApiStatus_t gpakWriteAECParms (ADT_UInt32 DspId,  GpakAECParms_t  *AECParms, 
                                            GPAK_AECParmsStat_t *Status) {

    ADT_UInt32 MsgLenI16;                     /* message length */
    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 *Msg;
    ADT_UInt32 length;

    if (MaxDsps <= DspId)
        return GpakApiInvalidDsp;
    Msg =(ADT_UInt16 *) &MsgBuffer[0];
    MsgLenI16 = gpakFormatAecWriteParmsMsg (Msg, AECParms);
    length =
        TransactCmd(DspId, MsgBuffer, MsgLenI16, MSG_WRITE_AEC_PARMS_REPLY, 2,
                    NO_ID_CHK, (ADT_UInt16) 0);
    if (!length)
        return GpakApiCommFailure;

    /* Return success or failure . */
    *Status = (GPAK_AECParmsStat_t) Byte1 (Msg[1]);
    if (*Status == AEC_Success)  return GpakApiSuccess;
    else                         return GpakApiParmError;

}
#else
ADT_UInt32               gpakFormatAecWriteParmsMsg (ADT_UInt16 *Msg, GpakAECParms_t   *AECParms) {

    /* Build the Configure Serial Ports message. */
    Msg[0]   = MSG_WRITE_AEC_PARMS  << 8;
    Msg[1]   = (ADT_UInt16) AECParms->activeTailLengthMSec;
    Msg[2]   = (ADT_UInt16) AECParms->totalTailLengthMSec;
    Msg[3]   = (ADT_UInt16) AECParms->maxTxNLPThresholddB;
    Msg[4]   = (ADT_UInt16) AECParms->maxTxLossdB;
    Msg[5]   = (ADT_UInt16) AECParms->maxRxLossdB;
    Msg[6]   = (ADT_UInt16) AECParms->targetResidualLeveldBm;
    Msg[7]   = (ADT_UInt16) AECParms->maxRxNoiseLeveldBm;
    Msg[8]   = (ADT_UInt16) AECParms->worstExpectedERLdB;
    Msg[9]   = (ADT_UInt16) AECParms->noiseReductionEnable;
    Msg[10]  = (ADT_UInt16) AECParms->cngEnable;
    Msg[11]  = (ADT_UInt16) AECParms->agcEnable;
    Msg[12]  = (ADT_UInt16) AECParms->agcMaxGaindB;
    Msg[13]  = (ADT_UInt16) AECParms->agcMaxLossdB;
    Msg[14]  = (ADT_UInt16) AECParms->agcTargetLeveldBm;
    Msg[15]  = (ADT_UInt16) AECParms->agcLowSigThreshdBm;
    Msg[16]  = (ADT_UInt16) AECParms->maxTrainingTimeMSec;
    Msg[17]  = (ADT_UInt16) AECParms->rxSaturateLeveldBm;
    Msg[18]  = (ADT_UInt16) AECParms->trainingRxNoiseLeveldBm;
    Msg[19]  = (ADT_UInt16) AECParms->fixedGaindB10;
    return 20;
}


GpakApiStatus_t gpakWriteAECParms (ADT_UInt32 DspId,  GpakAECParms_t  *AECParms, 
                                            GPAK_AECParmsStat_t *Status) {

    ADT_UInt32 MsgLenI16;                     /* message length */
    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 *Msg;


    if (MaxDsps <= DspId)
        return GpakApiInvalidDsp;

    Msg =(ADT_UInt16 *) &MsgBuffer[0];

    MsgLenI16 = gpakFormatAecWriteParmsMsg (Msg, AECParms);
    if (!TransactCmd (DspId, MsgBuffer, MsgLenI16, MSG_WRITE_AEC_PARMS_REPLY, 2,
                      BYTE_ID, (ADT_UInt16) 0))
        return GpakApiCommFailure;

    /* Return success or failure . */
    *Status = (GPAK_AECParmsStat_t) Byte1 (Msg[1]);
    if (*Status == AEC_Success)  return GpakApiSuccess;
    else                         return GpakApiParmError;

}
#endif

// ----------------------   Tone detector

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakConfigArbToneDetect - Configure a DSP's Arbitrary Tone Detector.
 *
 * FUNCTION
 *  This function configures a DSP's Arbitrary Tone Detector and sets it as the
 *  active configuration for subsequent channels that run the detector.
 *
 * Inputs
 *      DspId          - Dsp identifier 
 *      ArbCfgId       - Arb Detector configuration identifier
 *      pArbParams     - Arb Detector configuration params 
 *      pArbTones      - tones used by this configuration 
 *
 * Outputs
 *    pStatus  - conference configuration status from DSP
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
GpakApiStatus_t gpakConfigArbToneDetect (ADT_UInt32 DspId, ADT_UInt16 ArbCfgId,
                                                   GpakArbTdParams_t *pArbParams, GpakArbTdToneIdx_t *pArbTones,
                                                   ADT_UInt16 *pArbFreqs, GPAK_ConfigArbToneStat_t *pStatus) {
    
    ADT_UInt32  MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 *Msg, *Msg1, i, lenI16;

    if (MaxDsps <= DspId)
        return CatInvalidDsp;

    Msg1    = (ADT_UInt16 *) MsgBuffer;
    Msg = Msg1;

    Msg[0] = MSG_CONFIG_ARB_DETECTOR << 8;
    Msg[1] = ArbCfgId;
 
    Msg[2] = (ADT_UInt16) PackBytes (pArbParams->numDistinctFreqs, pArbParams->numTones);
    Msg[3] = (ADT_UInt16) PackBytes (pArbParams->minPower, pArbParams->maxFreqDeviation);
    lenI16 = 4;

    // maximum of 32 distinct frequencies
    Msg  = &Msg1[lenI16];
    for (i=0; i<32; i++) {
        if (i < pArbParams->numDistinctFreqs) {
            Msg[i] = pArbFreqs[i];
        } else {
            Msg[i] = 0;
        }
    }
    lenI16 += 32;
    
    // maximum of 16 tones
    Msg  = &Msg1[lenI16];
    for (i=0; i<16; i++) {
        if (i < pArbParams->numTones) {
            Msg[3*i]   = pArbTones[i].f1;
            Msg[3*i+1] = pArbTones[i].f2;
            Msg[3*i+2] = pArbTones[i].index;
        } else {
            Msg[3*i]   = 0;
            Msg[3*i+1] = 0;
            Msg[3*i+2] = 0;
        }
    }
    lenI16 += 48;

    if (!TransactCmd (DspId, MsgBuffer, lenI16, MSG_CONFIG_ARB_DETECTOR_REPLY, 2, 
                    BYTE_ID, ArbCfgId))
        return GpakApiCommFailure;

    Msg  = Msg1;
    *pStatus = (GPAK_ConfigArbToneStat_t) Byte0 (Msg[1]);
    if (*pStatus == Cat_Success)
        return GpakApiSuccess;
    else
        return GpakApiParmError;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakActivateArbToneDetect - Activate a DSP's Arbitrary Tone Detector.
 *
 * FUNCTION
 *  This function activates a DSP's Arbitrary Tone Detector. Any channels
 *  that are configured to run arbitrary tone detection will automatically
 *  use the arbitrary detector configuration that is currently active.
 *
 * Inputs
 *      DspId          - Dsp identifier 
 *      ArbCfgId       - Arb Detector configuration identifier
 *
 * Outputs
 *    pStatus  - conference configuration status from DSP
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
GpakApiStatus_t gpakActiveArbToneDetect (ADT_UInt32 DspId, ADT_UInt16 ArbCfgId,  GPAK_ActiveArbToneStat_t *pStatus) {
    
    ADT_UInt32  MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 *Msg;

    if (MaxDsps <= DspId)
        return GpakApiInvalidDsp;

    Msg    = (ADT_UInt16 *) MsgBuffer;

    Msg[0] = MSG_ACTIVE_ARB_DETECTOR << 8;
    Msg[1] = ArbCfgId;
    if (!TransactCmd (DspId, MsgBuffer, 2, MSG_ACTIVE_ARB_DETECTOR_REPLY, 2, 
                    BYTE_ID, ArbCfgId))
        return AatDspCommFailure;

    *pStatus = (GPAK_ActiveArbToneStat_t) Byte0 (Msg[1]);
    if (*pStatus == Aat_Success)
        return GpakApiSuccess;
    else
        return GpakApiParmError;
}

// ----------------------   TDM ports

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakConfigurePorts - Configure a DSP's TDM ports.
 *
 * FUNCTION
 *  Configures a DSP's serial ports.
 *
 * Inputs
 *   DspId    - Dsp identifier 
 *   PortCfg  - Port configuration data structure pointer
 *
 * Outputs
 *    pStatus  - port configuration status from DSP
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */

#ifdef PORT_CFG_MCBSP_54
ADT_UInt32 gpakFormatPortConfig (ADT_UInt16 *MsgBuff, GpakPortConfig_t *PortCfg) {

   ADT_UInt8  Polarities, Delays;
    int port, wordCnt;
    ADT_UInt16 *Msg;

    /* Build the Configure Serial Ports message. */
    MsgBuff[0] = MSG_CONFIGURE_PORTS << 8;
    
   Msg = &MsgBuff[1];

   wordCnt = 1;
   for (port=0; port<3; port++, wordCnt += 6) {
      Polarities = 0;

       if (PortCfg->RxClockRise[port])     Polarities |= 1;
       if (PortCfg->RxClockInternal[port]) Polarities |= 2;

       if (PortCfg->TxClockFall[port])     Polarities |= 4;
       if (PortCfg->TxClockInternal[port]) Polarities |= 8;

       if (PortCfg->RxSyncLow[port])       Polarities |= 0x10;
       if (PortCfg->RxSyncInternal[port])  Polarities |= 0x20;

       if (PortCfg->TxSyncLow[port])       Polarities |= 0x40;
       if (PortCfg->TxSyncInternal[port])  Polarities |= 0x80;

      Delays = ((PortCfg->TxDelay[port] << 2) & 0xc)  | 
               (PortCfg->RxDelay[port]       & 0x3);

      *Msg++ = Pack (PortCfg->MultiMode[port], PortCfg->Compand[port]);
      *Msg++ = Pack (PortCfg->LowBlk[port],    PortCfg->HighBlk[port]);
      *Msg++ = Pack (Polarities, Delays);
      *Msg++ = PortCfg->LowMask[port];
      *Msg++ = PortCfg->HighMask[port];
      *Msg++ = Pack (PortCfg->ClkDiv[port], PortCfg->PulseWidth[port]);
   }
    return wordCnt;
}
#endif 

#ifdef PORT_CFG_MCBSP_55
ADT_UInt32 gpakFormatPortConfig (ADT_UInt16 *Msg, GpakPortConfig_t *PortCfg) {

    /* Build the Configure Serial Ports message. */
    Msg[0] = MSG_CONFIGURE_PORTS << 8;
    Msg[1] = 0;
    if (PortCfg->Port1Enable)        Msg[1] = 0x0001;
    if (PortCfg->Port2Enable)        Msg[1] |= 0x0002;
    if (PortCfg->Port3Enable)        Msg[1] |= 0x0004;
   //if (PortCfg->AudioPort1Enable)   Msg[1] |= 0x0008;
    //if (PortCfg->AudioPort2Enable)   Msg[1] |= 0x0010;

    Msg[2]  = (ADT_UInt16)((PortCfg->Port1SlotMask0_31  ) >> 16);  
    Msg[3]  = (ADT_UInt16)((PortCfg->Port1SlotMask0_31  ) & 0xffff);  
    Msg[4]  = (ADT_UInt16)((PortCfg->Port1SlotMask32_63 ) >> 16);  
    Msg[5]  = (ADT_UInt16)((PortCfg->Port1SlotMask32_63 ) & 0xffff);
    Msg[6]  = (ADT_UInt16)((PortCfg->Port1SlotMask64_95 ) >> 16);  
    Msg[7]  = (ADT_UInt16)((PortCfg->Port1SlotMask64_95 ) & 0xffff);
    Msg[8]  = (ADT_UInt16)((PortCfg->Port1SlotMask96_127) >> 16);  
    Msg[9]  = (ADT_UInt16)((PortCfg->Port1SlotMask96_127) & 0xffff);

    Msg[10] = (ADT_UInt16)((PortCfg->Port2SlotMask0_31  ) >> 16);  
    Msg[11] = (ADT_UInt16)((PortCfg->Port2SlotMask0_31  ) & 0xffff);
    Msg[12] = (ADT_UInt16)((PortCfg->Port2SlotMask32_63 ) >> 16);  
    Msg[13] = (ADT_UInt16)((PortCfg->Port2SlotMask32_63 ) & 0xffff);
    Msg[14] = (ADT_UInt16)((PortCfg->Port2SlotMask64_95 ) >> 16);  
    Msg[15] = (ADT_UInt16)((PortCfg->Port2SlotMask64_95 ) & 0xffff);
    Msg[16] = (ADT_UInt16)((PortCfg->Port2SlotMask96_127) >> 16);  
    Msg[17] = (ADT_UInt16)((PortCfg->Port2SlotMask96_127) & 0xffff);

    Msg[18] = (ADT_UInt16)((PortCfg->Port3SlotMask0_31  ) >> 16);  
    Msg[19] = (ADT_UInt16)((PortCfg->Port3SlotMask0_31  ) & 0xffff);
    Msg[20] = (ADT_UInt16)((PortCfg->Port3SlotMask32_63 ) >> 16);  
    Msg[21] = (ADT_UInt16)((PortCfg->Port3SlotMask32_63 ) & 0xffff);
    Msg[22] = (ADT_UInt16)((PortCfg->Port3SlotMask64_95 ) >> 16);  
    Msg[23] = (ADT_UInt16)((PortCfg->Port3SlotMask64_95 ) & 0xffff);
    Msg[24] = (ADT_UInt16)((PortCfg->Port3SlotMask96_127) >> 16);  
    Msg[25] = (ADT_UInt16)((PortCfg->Port3SlotMask96_127) & 0xffff);

    Msg[26] = (ADT_UInt16)(PortCfg->txClockPolarity1);  
    Msg[27] = (ADT_UInt16)(PortCfg->txClockPolarity2);
    Msg[28] = (ADT_UInt16)(PortCfg->txClockPolarity3);  
    Msg[29] = (ADT_UInt16)(PortCfg->rxClockPolarity1);
    Msg[30] = (ADT_UInt16)(PortCfg->rxClockPolarity2);  
    Msg[31] = (ADT_UInt16)(PortCfg->rxClockPolarity3);

    //Msg[32] = (ADT_UInt16)((PortCfg->AudioPort2TxSerMask) >> 16);  
    //Msg[33] = (ADT_UInt16)((PortCfg->AudioPort2TxSerMask) & 0xffff);
    //Msg[34] = (ADT_UInt16)((PortCfg->AudioPort2RxSerMask) >> 16);  
    //Msg[35] = (ADT_UInt16)((PortCfg->AudioPort2RxSerMask) & 0xffff);
    //Msg[36] = (ADT_UInt16)((PortCfg->AudioPort2SlotMask ) >> 16);  
    //Msg[37] = (ADT_UInt16)((PortCfg->AudioPort2SlotMask ) & 0xffff);

    Msg[38] = PackBytes (PortCfg->ClkDiv2,     PortCfg->ClkDiv1);
    Msg[39] = PackBytes (PortCfg->FrameWidth1, PortCfg->ClkDiv3);
    Msg[40] = PackBytes (PortCfg->FrameWidth3, PortCfg->FrameWidth2);

    Msg[41] = (ADT_UInt16)(((PortCfg->FramePeriod1 << 4) & 0xfff0) |
                         ((PortCfg->Compand1 << 1) & 0xE) | (PortCfg->SampRateGen1 & 1) );
    Msg[42] = (ADT_UInt16)(((PortCfg->FramePeriod2 << 4) & 0xfff0) |
                         ((PortCfg->Compand2 << 1) & 0xE) | (PortCfg->SampRateGen2 & 1) );

    Msg[43] = (ADT_UInt16)(((PortCfg->FramePeriod3 << 4) & 0xfff0) |
                         ((PortCfg->Compand3 << 1) & 0xE) | (PortCfg->SampRateGen3 & 1) );

    Msg[44] = (ADT_UInt16 )( (PortCfg->TxDataDelay1 & 0x0003)       | ((PortCfg->RxDataDelay1 << 2) & 0x000c) |
                          ((PortCfg->TxDataDelay2 << 4) & 0x0030) | ((PortCfg->RxDataDelay2 << 6) & 0x00c0) |
                          ((PortCfg->TxDataDelay3 << 8) & 0x0300) | ((PortCfg->RxDataDelay3 << 10) & 0x0c00) );
    return 45;
}
#endif

#ifdef PORT_CFG_MCBSP_64
ADT_UInt32 gpakFormatPortConfig (ADT_UInt16 *Msg, GpakPortConfig_t *PortCfg) {

    /* Build the Configure Serial Ports message. */
    Msg[0] = MSG_CONFIGURE_PORTS << 8;
    Msg[1] = 0;
    if (PortCfg->Port1Enable)        Msg[1] = 0x0001;
    if (PortCfg->Port2Enable)        Msg[1] |= 0x0002;
    if (PortCfg->Port3Enable)        Msg[1] |= 0x0004;
    if (PortCfg->AudioPort1Enable)   Msg[1] |= 0x0008;
    if (PortCfg->AudioPort2Enable)   Msg[1] |= 0x0010;

    Msg[2]  = (ADT_UInt16)((PortCfg->Port1SlotMask0_31  ) >> 16);  
    Msg[3]  = (ADT_UInt16)((PortCfg->Port1SlotMask0_31  ) & 0xffff);  
    Msg[4]  = (ADT_UInt16)((PortCfg->Port1SlotMask32_63 ) >> 16);  
    Msg[5]  = (ADT_UInt16)((PortCfg->Port1SlotMask32_63 ) & 0xffff);
    Msg[6]  = (ADT_UInt16)((PortCfg->Port1SlotMask64_95 ) >> 16);  
    Msg[7]  = (ADT_UInt16)((PortCfg->Port1SlotMask64_95 ) & 0xffff);
    Msg[8]  = (ADT_UInt16)((PortCfg->Port1SlotMask96_127) >> 16);  
    Msg[9]  = (ADT_UInt16)((PortCfg->Port1SlotMask96_127) & 0xffff);

    Msg[10] = (ADT_UInt16)((PortCfg->Port2SlotMask0_31  ) >> 16);  
    Msg[11] = (ADT_UInt16)((PortCfg->Port2SlotMask0_31  ) & 0xffff);
    Msg[12] = (ADT_UInt16)((PortCfg->Port2SlotMask32_63 ) >> 16);  
    Msg[13] = (ADT_UInt16)((PortCfg->Port2SlotMask32_63 ) & 0xffff);
    Msg[14] = (ADT_UInt16)((PortCfg->Port2SlotMask64_95 ) >> 16);  
    Msg[15] = (ADT_UInt16)((PortCfg->Port2SlotMask64_95 ) & 0xffff);
    Msg[16] = (ADT_UInt16)((PortCfg->Port2SlotMask96_127) >> 16);  
    Msg[17] = (ADT_UInt16)((PortCfg->Port2SlotMask96_127) & 0xffff);

    Msg[18] = (ADT_UInt16)((PortCfg->Port3SlotMask0_31  ) >> 16);  
    Msg[19] = (ADT_UInt16)((PortCfg->Port3SlotMask0_31  ) & 0xffff);
    Msg[20] = (ADT_UInt16)((PortCfg->Port3SlotMask32_63 ) >> 16);  
    Msg[21] = (ADT_UInt16)((PortCfg->Port3SlotMask32_63 ) & 0xffff);
    Msg[22] = (ADT_UInt16)((PortCfg->Port3SlotMask64_95 ) >> 16);  
    Msg[23] = (ADT_UInt16)((PortCfg->Port3SlotMask64_95 ) & 0xffff);
    Msg[24] = (ADT_UInt16)((PortCfg->Port3SlotMask96_127) >> 16);  
    Msg[25] = (ADT_UInt16)((PortCfg->Port3SlotMask96_127) & 0xffff);

    Msg[26] = (ADT_UInt16)((PortCfg->AudioPort1TxSerMask) >> 16);  
    Msg[27] = (ADT_UInt16)((PortCfg->AudioPort1TxSerMask) & 0xffff);
    Msg[28] = (ADT_UInt16)((PortCfg->AudioPort1RxSerMask) >> 16);  
    Msg[29] = (ADT_UInt16)((PortCfg->AudioPort1RxSerMask) & 0xffff);
    Msg[30] = (ADT_UInt16)((PortCfg->AudioPort1SlotMask ) >> 16);  
    Msg[31] = (ADT_UInt16)((PortCfg->AudioPort1SlotMask ) & 0xffff);

    Msg[32] = (ADT_UInt16)((PortCfg->AudioPort2TxSerMask) >> 16);  
    Msg[33] = (ADT_UInt16)((PortCfg->AudioPort2TxSerMask) & 0xffff);
    Msg[34] = (ADT_UInt16)((PortCfg->AudioPort2RxSerMask) >> 16);  
    Msg[35] = (ADT_UInt16)((PortCfg->AudioPort2RxSerMask) & 0xffff);
    Msg[36] = (ADT_UInt16)((PortCfg->AudioPort2SlotMask ) >> 16);  
    Msg[37] = (ADT_UInt16)((PortCfg->AudioPort2SlotMask ) & 0xffff);

    Msg[38] = PackBytes (PortCfg->ClkDiv2,     PortCfg->ClkDiv1);
    Msg[39] = PackBytes (PortCfg->FrameWidth1, PortCfg->ClkDiv3);
    Msg[40] = PackBytes (PortCfg->FrameWidth3, PortCfg->FrameWidth2);

    Msg[41] = (ADT_UInt16)(((PortCfg->FramePeriod1 << 4) & 0xfff0) |
                         ((PortCfg->Compand1 << 1) & 0xE) | (PortCfg->SampRateGen1 & 1) );
    Msg[42] = (ADT_UInt16)(((PortCfg->FramePeriod2 << 4) & 0xfff0) |
                         ((PortCfg->Compand2 << 1) & 0xE) | (PortCfg->SampRateGen2 & 1) );

    Msg[43] = (ADT_UInt16)(((PortCfg->FramePeriod3 << 4) & 0xfff0) |
                         ((PortCfg->Compand3 << 1) & 0xE) | (PortCfg->SampRateGen3 & 1) );

    Msg[44] = (ADT_UInt16 )( (PortCfg->TxDataDelay1 & 0x0003)       | ((PortCfg->RxDataDelay1 << 2) & 0x000c) |
                          ((PortCfg->TxDataDelay2 << 4) & 0x0030) | ((PortCfg->RxDataDelay2 << 6) & 0x00c0) |
                          ((PortCfg->TxDataDelay3 << 8) & 0x0300) | ((PortCfg->RxDataDelay3 << 10) & 0x0c00) );
    return 45;
}

#endif

#ifdef PORT_CFG_TSIP
ADT_UInt32 gpakFormatPortConfig (ADT_UInt16 *Msg, GpakPortConfig_t *PortCfg) {
   int i, idx;

    /* Build the Configure Serial Ports message. */
    Msg[0] = MSG_CONFIGURE_PORTS << 8;

    // Enable bits
    Msg[1] = 0;
    if (PortCfg->TsipEnable1)        Msg[1]  = 0x0001;
    if (PortCfg->TsipEnable2)        Msg[1] |= 0x0002;
    if (PortCfg->TsipEnable3)        Msg[1] |= 0x0004;

    //--------------------------------------------------------------------
    // TSIP 0
    // Slot masks
    for (i=0, idx=2; i<32; i++, idx+=2) {
        Msg[idx]    = (ADT_UInt16)((PortCfg->slotMask1[i]) >> 16);  
        Msg[idx+1]  = (ADT_UInt16)((PortCfg->slotMask1[i]) & 0xffff);  
    }

    // Control parameters
    Msg[66] = (ADT_UInt16) ( (PortCfg->singleClk1             & 0x0001) |
                            ((PortCfg->txFsPolarity1   << 1)  & 0x0002) |
                            ((PortCfg->txDataClkEdge1  << 2)  & 0x0004) |
                            ((PortCfg->txFsClkEdge1    << 3)  & 0x0008) |
                            ((PortCfg->txClkMode1      << 4)  & 0x0010) |
                            ((PortCfg->txClkFsSrc1     << 5)  & 0x0020) |
                            ((PortCfg->txOutputDly1    << 6)  & 0x0040) |
                            ((PortCfg->rxFsPolarity1   << 7)  & 0x0080) |
                            ((PortCfg->rxDataClkEdge1  << 8)  & 0x0100) |
                            ((PortCfg->rxFsClkEdge1    << 9)  & 0x0200) |
                            ((PortCfg->rxClkMode1      << 10) & 0x0400) |
                            ((PortCfg->rxClkFsSrc1     << 11) & 0x0800));

    Msg[67] = (ADT_UInt16) ( (PortCfg->txDriveState1      & 0x0003) |
                            ((PortCfg->txDataRate1  << 2) & 0x000C) |
                            ((PortCfg->rxDataRate1  << 4) & 0x0030) |
                            ((PortCfg->loopBack1    << 6) & 0x00C0));

    Msg[68] = (ADT_UInt16) PortCfg->txDatDly1;
    Msg[69] = (ADT_UInt16) PortCfg->rxDatDly1;

    //--------------------------------------------------------------------
    // TSIP 1
    // Slot masks
    for (i=0, idx=70; i<32; i++, idx+=2) {
        Msg[idx]    = (ADT_UInt16)((PortCfg->slotMask2[i]) >> 16);  
        Msg[idx+1]  = (ADT_UInt16)((PortCfg->slotMask2[i]) & 0xffff);  
    }

    // Control parameters
    Msg[134] = (ADT_UInt16) ( (PortCfg->singleClk2            & 0x0001) |
                            ((PortCfg->txFsPolarity2   << 1)  & 0x0002) |
                            ((PortCfg->txDataClkEdge2  << 2)  & 0x0004) |
                            ((PortCfg->txFsClkEdge2    << 3)  & 0x0008) |
                            ((PortCfg->txClkMode2      << 4)  & 0x0010) |
                            ((PortCfg->txClkFsSrc2     << 5)  & 0x0020) |
                            ((PortCfg->txOutputDly2    << 6)  & 0x0040) |
                            ((PortCfg->rxFsPolarity2   << 7)  & 0x0080) |
                            ((PortCfg->rxDataClkEdge2  << 8)  & 0x0100) |
                            ((PortCfg->rxFsClkEdge2    << 9)  & 0x0200) |
                            ((PortCfg->rxClkMode2      << 10) & 0x0400) |
                            ((PortCfg->rxClkFsSrc2     << 11) & 0x0800));

    Msg[135] = (ADT_UInt16) ( (PortCfg->txDriveState2     & 0x0003) |
                            ((PortCfg->txDataRate2  << 2) & 0x000C) |
                            ((PortCfg->rxDataRate2  << 4) & 0x0030) |
                            ((PortCfg->loopBack2    << 6) & 0x00C0));

    Msg[136] = (ADT_UInt16) PortCfg->txDatDly2;
    Msg[137] = (ADT_UInt16) PortCfg->rxDatDly2;

    //--------------------------------------------------------------------
    // TSIP 2
    // Slot masks
    for (i=0, idx=138; i<32; i++, idx+=2) {
        Msg[idx]    = (ADT_UInt16)((PortCfg->slotMask3[i]) >> 16);  
        Msg[idx+1]  = (ADT_UInt16)((PortCfg->slotMask3[i]) & 0xffff);  
    }

    // Control parameters
    Msg[202] = (ADT_UInt16) ( (PortCfg->singleClk3            & 0x0001) |
                            ((PortCfg->txFsPolarity3   << 1)  & 0x0002) |
                            ((PortCfg->txDataClkEdge3  << 2)  & 0x0004) |
                            ((PortCfg->txFsClkEdge3    << 3)  & 0x0008) |
                            ((PortCfg->txClkMode3      << 4)  & 0x0010) |
                            ((PortCfg->txClkFsSrc3     << 5)  & 0x0020) |
                            ((PortCfg->txOutputDly3    << 6)  & 0x0040) |
                            ((PortCfg->rxFsPolarity3   << 7)  & 0x0080) |
                            ((PortCfg->rxDataClkEdge3  << 8)  & 0x0100) |
                            ((PortCfg->rxFsClkEdge3    << 9)  & 0x0200) |
                            ((PortCfg->rxClkMode3      << 10) & 0x0400) |
                            ((PortCfg->rxClkFsSrc3     << 11) & 0x0800));

    Msg[203] = (ADT_UInt16) ( (PortCfg->txDriveState3     & 0x0003) |
                            ((PortCfg->txDataRate3  << 2) & 0x000C) |
                            ((PortCfg->rxDataRate3  << 4) & 0x0030) |
                            ((PortCfg->loopBack3    << 6) & 0x00C0));

    Msg[204] = (ADT_UInt16) PortCfg->txDatDly3;
    Msg[205] = (ADT_UInt16) PortCfg->rxDatDly3;

    //--------------------------------------------------------------------
    // TSIP 0
    // Companding modes
    for (i=0, idx=206; i<4; i++, idx+=2) {
        Msg[idx]    = (ADT_UInt16)((PortCfg->slotCompand1[i]) >> 16);  
        Msg[idx+1]  = (ADT_UInt16)((PortCfg->slotCompand1[i]) & 0xffff);  
    }

    //--------------------------------------------------------------------
    // TSIP 1
    // Companding modes
    for (i=0, idx=214; i<4; i++, idx+=2) {
        Msg[idx]    = (ADT_UInt16)((PortCfg->slotCompand2[i]) >> 16);  
        Msg[idx+1]  = (ADT_UInt16)((PortCfg->slotCompand2[i]) & 0xffff);  
    }

    //--------------------------------------------------------------------
    // TSIP 2
    // Companding modes
    for (i=0, idx=222; i<4; i++, idx+=2) {
        Msg[idx]    = (ADT_UInt16)((PortCfg->slotCompand3[i]) >> 16);  
        Msg[idx+1]  = (ADT_UInt16)((PortCfg->slotCompand3[i]) & 0xffff);  
    }

    return 230;
}

GpakApiStatus_t gpakSetTSIPMode (ADT_Int32 DspId, ADT_UInt16 sampleSizeI8, ADT_UInt16 samplingRatekHz,
                                 ADT_UInt16 compandedData) {
   struct {
      ADT_UInt16 sampleSizeI8;
      ADT_UInt16 samplingRatekHz;
      ADT_UInt16 compandedData;
   } TSIPParams;

   if (MaxDsps <= (unsigned int) DspId)
        return IPInvalidDSP;

   if (HostBigEndian) {
      TSIPParams.sampleSizeI8    = swapI16(sampleSizeI8);
      TSIPParams.samplingRatekHz = swapI16(samplingRatekHz);
      TSIPParams.compandedData   = swapI16(compandedData);

      /* Write the TSIP parameters to the DSP. */
      gpakWriteDspNoSwap (DspId, DSP_TSIPCFG_ADDRESS, BytesToTxUnits (sizeof TSIPParams), (void *) &TSIPParams);

   } else {
      TSIPParams.sampleSizeI8    = sampleSizeI8;
      TSIPParams.samplingRatekHz = samplingRatekHz;
      TSIPParams.compandedData   = compandedData;

      /* Write the TSIP parameters to the DSP. */
      gpakWriteDsp (DspId, DSP_TSIPCFG_ADDRESS, BytesToTxUnits (sizeof TSIPParams), (void *) &TSIPParams);
   }
   return GpakApiSuccess;
}

#endif

#ifdef PORT_CFG_MCASP_64
ADT_UInt32 gpakFormatPortConfig (ADT_UInt16 *Msg, GpakPortConfig_t *PortCfg) {

   // Build the Configure Serial Ports message.
   Msg[0] = MSG_CONFIGURE_PORTS << 8;
   Msg[1] = PortCfg->PortID;
   Msg[2] = PortCfg->InputPinMap;
   Msg[3] = PortCfg->OutputPinMap;
   Msg[4] = PortCfg->DataDelay << 14    | PortCfg->HiResClockDivisor;
   Msg[5] = PortCfg->BitsPerSlot << 8   | PortCfg->BitsPerSample;
   Msg[6] = PortCfg->SlotsPerFrame << 8 | PortCfg->ClockDivisor;

   Msg[7] = PortCfg->ActiveSlotMap >> 16;
   Msg[8] = PortCfg->ActiveSlotMap & 0xffff;

   Msg[9] = ((PortCfg->FsWord & 1)      << 15) | ((PortCfg->FsFalling & 1)        << 14) |
            ((PortCfg->ClkRxRising & 1) << 13) | ((PortCfg->HiResClockInvert & 1) << 12) |
            ((PortCfg->GenClocks & 1)   << 11) | ((PortCfg->SharedClk & 1) << 10);
   return 10;
}
#endif

GpakApiStatus_t gpakConfigurePorts (ADT_UInt32 DspId, GpakPortConfig_t *pPortConfig, 
                                           GPAK_PortConfigStat_t *pStatus) {

    ADT_UInt32 MsgLenI16;                        /* message length */
    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 *Msg;


    if (MaxDsps <= DspId)
        return GpakApiInvalidDsp;

    Msg =(ADT_UInt16 *) &MsgBuffer[0];

    MsgLenI16 = gpakFormatPortConfig (Msg, pPortConfig);

    if (!TransactCmd (DspId, MsgBuffer, MsgLenI16, MSG_CONFIG_PORTS_REPLY, 2, NO_ID_CHK, 0))
        return GpakApiCommFailure;

    /* Return success or failure . */
    *pStatus = (GPAK_PortConfigStat_t) Byte1 (Msg[1]);
    if (*pStatus == Pc_Success)
        return GpakApiSuccess;
    else
        return GpakApiParmError;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadMcBspStats - Read McBsp statistics
 * 
 * FUNCTION
 *  This function reads McBsp Statistics
 * INPUTS
 *      DspId,               DSP Identifier
 *      FrameStats           Pointer to store McBSP Statistics
 *
 * OUTPUTS 
 *      McBSP FrameStats   - McBSP Statistics from DSP
 *
 * RETURNS
 *  Status  code indicating success or a specific error.
 */
void             gpakParseMcBspStats (ADT_UInt16 *Msg, gpakMcBspStats_t *FrameStats) {

    FrameStats->Port1Status             = (GpakActivation) Msg[2];
    FrameStats->Port1RxIntCount         = PackWords(Msg[3],Msg[4]);
    FrameStats->Port1RxSlips            = Msg[5];
    FrameStats->Port1RxDmaErrors        = Msg[6];
    FrameStats->Port1TxIntCount         = PackWords(Msg[7], Msg[8]);
    FrameStats->Port1TxSlips            = Msg[9];
    FrameStats->Port1TxDmaErrors        = Msg[10];
    FrameStats->Port1FrameSyncErrors    = Msg[11];
    FrameStats->Port1RestartCount       = Msg[12];
    
    FrameStats->Port2Status             = (GpakActivation) Msg[13];
    FrameStats->Port2RxIntCount         = PackWords(Msg[14],Msg[15]);
    FrameStats->Port2RxSlips            = Msg[16];
    FrameStats->Port2RxDmaErrors        = Msg[17];
    FrameStats->Port2TxIntCount          = PackWords(Msg[18], Msg[19]);
    FrameStats->Port2TxSlips            = Msg[20];
    FrameStats->Port2TxDmaErrors        = Msg[21];
    FrameStats->Port2FrameSyncErrors    = Msg[22];
    FrameStats->Port2RestartCount       = Msg[23];
    
    FrameStats->Port3Status             = (GpakActivation) Msg[24];
    FrameStats->Port3RxIntCount         = PackWords(Msg[25],Msg[26]);
    FrameStats->Port3RxSlips            = Msg[27];
    FrameStats->Port3RxDmaErrors        = Msg[28];
    FrameStats->Port3TxIntCount          = PackWords(Msg[29], Msg[30]);
    FrameStats->Port3TxSlips            = Msg[31];
    FrameStats->Port3TxDmaErrors        = Msg[32];
    FrameStats->Port3FrameSyncErrors    = Msg[33];
    FrameStats->Port3RestartCount       = Msg[34];
}

GpakApiStatus_t  gpakReadMcBspStats ( ADT_UInt32 DspId, gpakMcBspStats_t *FrameStats) {

    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 *Msg;

    if (MaxDsps <= DspId)
        return GpakApiInvalidDsp;

    Msg =(ADT_UInt16 *) &MsgBuffer[0];

    Msg[0] = MSG_READ_MCBSP_STATS << 8;
    Msg[1] = 0;

    if (!TransactCmd (DspId, MsgBuffer, 2, MSG_READ_MCBSP_STATS_REPLY, 35, NO_ID_CHK, 0))
        return GpakApiCommFailure;

    gpakParseMcBspStats (Msg, FrameStats);

    return GpakApiSuccess;
}


// ----------------------   System Status 
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadCoreUsage - Read CPU usage statistics from a DSP.
 *
 * FUNCTION
 *  Reads the CPU usage statistics from a DSP's memory. The
 *  average CPU usage in units of .1 percent are obtained for each of the frame
 *  rates.
 *
 * Inputs
 *     DspId      - Dsp identifier 
 *     CoreCount  - Number of cores contained in the DSP
 *
 * Outputs
 *  pxxmsUsage - Per frame size pointers to receive CPU usage  (.1% granularity)
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
#ifndef NETWORK_MESSAGING   // Per core CPU Usage
GpakApiStatus_t gpakReadCoreUsage (ADT_UInt32 DspId, ADT_UInt32 *CoreCount, ADT_UInt32 *FrameCount,
                                   ADT_UInt32  AvgCpuUsage [CORES_PER_DSP][MAX_NUM_GPAK_FRAMES],
                                   ADT_UInt32  PeakCpuUsage[CORES_PER_DSP][MAX_NUM_GPAK_FRAMES]) {
    DSP_Address usageAddr;

    if (MaxDsps <= DspId)
        return GpakApiInvalidDsp;

    gpakLockAccess (DspId);
    if (CheckDspReset (DspId) == -1) {
       gpakUnlockAccess (DspId);
       return GpakApiCommFailure;
    }

    /* Read the CPU Usage statistics from the DSP. */
    gpakReadDsp (DspId, pDspIfBlk[DspId] + CPU_USAGE_OFFSET, 1, &usageAddr);
    gpakReadDsp (DspId, usageAddr, MAX_NUM_GPAK_FRAMES * (*CoreCount), &AvgCpuUsage[0][0]);


    /* Read the CPU peak Usage statistics from the DSP. */
    gpakReadDsp (DspId, pDspIfBlk[DspId] + CPU_PKUSAGE_OFFSET, 1, &usageAddr);
    gpakReadDsp (DspId, usageAddr, MAX_NUM_GPAK_FRAMES * (*CoreCount), &PeakCpuUsage[0][0]);

    gpakUnlockAccess(DspId);

    return GpakApiSuccess;
}
#else
GpakApiStatus_t gpakReadCoreUsage (ADT_UInt32 DspId, ADT_UInt32 *CoreCount, ADT_UInt32 *FrameCount,
                                   ADT_UInt32  AvgCpuUsage [CORES_PER_DSP][MAX_NUM_GPAK_FRAMES],
                                   ADT_UInt32  PeakCpuUsage[CORES_PER_DSP][MAX_NUM_GPAK_FRAMES]) {
    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 *Msg;
    ADT_UInt32 numReportedCores, numReportedFrames, i;
    ADT_UInt32 *pAvg, *pPk;



    if (MaxDsps <= DspId)
        return GpakApiInvalidDsp;

    Msg    = (ADT_UInt16 *) MsgBuffer;

    Msg[0] = MSG_READ_CPU_USAGE << 8;   // Request Message
    memset (AvgCpuUsage,  0, (*CoreCount) * (*FrameCount) * sizeof (ADT_UInt32));
    memset (PeakCpuUsage, 0, (*CoreCount) * (*FrameCount) * sizeof (ADT_UInt32));
    
    if (!TransactCmd (DspId, MsgBuffer, 1, MSG_READ_CPU_USAGE_REPLY, sizeof (MsgBuffer)/2, NO_ID_CHK, 0))
        return GpakApiCommFailure;

    numReportedCores  = (ADT_UInt32)(Msg[1] & 0xff);
    numReportedFrames = (ADT_UInt32)((Msg[1] >> 8) & 0xff);

    if (numReportedCores > CORES_PER_DSP)
        numReportedCores = CORES_PER_DSP;

    if (numReportedFrames > MAX_NUM_GPAK_FRAMES)
        numReportedFrames = MAX_NUM_GPAK_FRAMES; 

    *FrameCount = numReportedFrames;
    *CoreCount  = numReportedCores;

    for (i=0; i<numReportedCores; i++) {
        pAvg = &MsgBuffer[1 + 2*i*numReportedFrames];
        pPk  = &MsgBuffer[1 + (2*i+1)*numReportedFrames];
        memcpy(&AvgCpuUsage[i][0], pAvg, sizeof(ADT_UInt32)*numReportedFrames);  
        memcpy(&PeakCpuUsage[i][0], pPk, sizeof(ADT_UInt32)*numReportedFrames);
    }
    return GpakApiSuccess;
}
#endif

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadRTPClock
 *
 * FUNCTION
 *  Reads the DSP's current RTP clock time 
 *
 * Inputs
 *     DspId      - Dsp identifier 
 *
 *
 * RETURNS
 *  DSP's current RTP clock value (1/8 millisecond units)
 *
 */
#ifndef NETWORK_MESSAGING   // Read RTP clock
ADT_Int32            gpakReadRTPClock (ADT_UInt32 DspId) {
    
    ADT_Int32 RTPClock;

    if (MaxDsps <= DspId)
        return (0);

    gpakLockAccess (DspId);
    gpakReadDsp32 (DspId, pDspIfBlk[DspId] + RTP_CLOCK_OFFSET,  BytesToTxUnits (4), (ADT_UInt32 *) &RTPClock);
    gpakUnlockAccess (DspId);
    return RTPClock;
}
#endif

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadMaxChannels
 *
 * FUNCTION
 *  Reads the DSP's maximum allowed channels 
 *
 * Inputs
 *     DspId      - Dsp identifier 
 *
 * Outputs
 *     MaxChannels - DSP's maximum channel count
 *
 * RETURNS
 *  0 - Success
 * -1 - Failure
 */
#ifndef NETWORK_MESSAGING   // Read Max Channels
int                  gpakReadMaxChannels (ADT_UInt32 DspId, ADT_UInt32 *MaxChannels) {

    if (MaxDsps <= DspId)
        return (-1);

    gpakLockAccess (DspId);
    if (CheckDspReset(DspId) == -1) {
        gpakUnlockAccess (DspId);
        return (-1);
    }
    gpakUnlockAccess (DspId);

    *MaxChannels = (ADT_UInt32) MaxChannels[DspId];
    return (0);
}
#else
int                  gpakReadMaxChannels (ADT_UInt32 DspId, ADT_UInt32 *MaxChannels) {
    if (MaxDsps <= DspId)
        return (-1);

    *MaxChannels = (ADT_UInt32) MaxChannels[DspId];
    return (0);
}
#endif


//{ Returns maximum number of bytes of auxiliary data allowable for the event
//  -1 indicates invalid
//
// NOTE: The ToneDevice field is not included in the event header size field,
//       even though that field is included in the event data structure.
//       this is due to the event header not being an even multiple of 32-bit words
//
// Returns:  -1  Data length error
//            0  No payload
//}           Other max payload length for evtcode
int parseEventHdr (ADT_UInt16 *EvtHdr, int *EvtPayloadI8, ADT_Int16 *ChanId, 
                   GpakAsyncEventCode_t *EvtCode, GpakDeviceSide_t *DeviceSide) {
   int maxPayloadI8;

   // Convert little endianness to host endianess
#if (DSP_WORD_I8 == 4)
   if (HostBigEndian)  gpakI16Swap ((ADT_UInt32 *) EvtHdr, 8);
#endif

   *ChanId        = EvtHdr[0];
   *EvtCode       = (GpakAsyncEventCode_t) EvtHdr[1];
   *EvtPayloadI8  = EvtHdr[2];
   *DeviceSide    = (GpakDeviceSide_t) EvtHdr[3];

   switch (*EvtCode) {
   case EventToneDetect:           // 2 or 4 bytes aux data
      if ((*EvtPayloadI8 != 2) && (*EvtPayloadI8 != 4)) return -1;
      maxPayloadI8 = byteSize (struct toneEvent);
      break;

   case EventPlaybackComplete:
   case EventPlaybackHalfWay:
   case EventPlaybackAtEnd:
   case EventRecordingHalfWay:
   case EventRecordingAtEnd:
   case EventRecordingStopped:
   case EventRecordingBufferFull:  // 4 bytes aux data
      maxPayloadI8 = byteSize (struct recordEvent);
      break;

   case EventFaxComplete:
   case EventCaptureComplete:
   case EventEcCaptureComplete:
      maxPayloadI8 = byteSize (struct captureData);
      break;

   case EventRxCidMessageComplete:
      maxPayloadI8 = byteSize (struct rxCidEvent);
      break;
	case EventCID2Tx:
   case EventCID2Rx:
	   maxPayloadI8 = 3*byteSize(ADT_Int16);
      break;

   case EventNeedSRTPMasterKey:
      maxPayloadI8 = byteSize (struct srtpEvent);
      break;

   case EventWarning:
      maxPayloadI8 = byteSize (GpakAsyncEventData_t);
      break;

   default:
      if (CustomEventStart <= *EvtCode) maxPayloadI8 = byteSize (GpakAsyncEventData_t);
      else                              maxPayloadI8 = 0;
      break;
   }

   // Verify that actual data does not exceed allowable size of data structure
   if (maxPayloadI8 < *EvtPayloadI8) return -1;
   return maxPayloadI8;
}

ADT_Bool parseEvent (ADT_UInt16 *EvtPayload, ADT_UInt32 EvtPayloadI8, 
                     GpakAsyncEventCode_t EvtCode, GpakAsyncEventData_t *EvtData) {


   switch (EvtCode) {
   case EventToneDetect:
#if (DSP_WORD_I8 == 4)
      if (HostBigEndian) gpakI16Swap ((ADT_UInt32 *) EvtPayload, EvtPayloadI8);
#endif
      EvtData->aux.toneEvent.ToneCode = (GpakToneCodes_t)  EvtPayload[0];
      if (EvtPayloadI8 == 4)
         EvtData->aux.toneEvent.ToneDuration = EvtPayload[1];
      else
         EvtData->aux.toneEvent.ToneDuration = 0;
      break;

   case EventPlaybackHalfWay:
   case EventPlaybackAtEnd:
   case EventPlaybackComplete:
   case EventRecordingHalfWay:
   case EventRecordingAtEnd:
   case EventRecordingStopped:
   case EventRecordingBufferFull:
#if (DSP_WORD_I8 == 4)
      if (HostBigEndian) gpakI16Swap ((ADT_UInt32 *) EvtPayload, EvtPayloadI8);
#endif
      EvtData->aux.recordEvent.RecordLength = (EvtPayload[0] << 16) | EvtPayload[1] ;
      EvtData->aux.recordEvent.RecordAddr   = (EvtPayload[2] << 16) | EvtPayload[3] ;
      break;

   case EventFaxComplete:
   case EventCaptureComplete:
   case EventEcCaptureComplete:
      memset (&EvtData->aux.captureData, 0, sizeof (struct captureData));
#if (DSP_WORD_I8 == 4)
      if (HostBigEndian) gpakI16Swap ((ADT_UInt32 *) EvtPayload, EvtPayloadI8);
#endif
      memcpy (&EvtData->aux.captureData, EvtPayload, sizeof (struct captureData));
      break;

   case EventRxCidMessageComplete:
      // Restore the message to original order as stored by DSP (little endian length followed by big endian data)
#if (DSP_WORD_I8 == 4)
      if (HostBigEndian) {
         gpakEndianI32Convert ((ADT_UInt32 *) EvtPayload, EvtPayloadI8);
         gpakI8Swap (&EvtPayload [1], &EvtPayload [1], EvtData->aux.rxCidEvent.Length);
         EvtPayload[1] = EvtPayload[0];
      } else
#endif
      {
         EvtData->aux.rxCidEvent.Length = EvtPayload [0];
      }

      // Copy payload data into event structure
      memcpy (EvtData->aux.rxCidEvent.Payload, &EvtPayload[1], bytesToWords (EvtPayloadI8 - 2));
      break;

   case EventNeedSRTPMasterKey:
      EvtData->aux.srtpEvent.roc = ((ADT_UInt32)EvtPayload[1] << 16) | ((ADT_UInt32)EvtPayload[0]);
      EvtData->aux.srtpEvent.MkiValue = ((ADT_UInt32)EvtPayload[3] << 16) | ((ADT_UInt32)EvtPayload[2]);;
      EvtData->aux.srtpEvent.seq = EvtPayload[4];
      EvtData->aux.srtpEvent.MkiI8 = EvtPayload[5];
      break;

	// Debug purpose states
	case EventCID2Tx:
   case EventCID2Rx:
	   EvtData->aux.CID2StateCode[0] = EvtPayload[0];
	   EvtData->aux.CID2StateCode[1] = EvtPayload[1];
	   EvtData->aux.CID2StateCode[2] = EvtPayload[2];

      break;

   default:
      if (EvtCode < CustomEventStart)  return 0;

   case EventWarning:
#if (DSP_WORD_I8 == 4)
      if (HostBigEndian) {
         gpakI8Swap (&EvtPayload[0], &EvtPayload[0], EvtPayloadI8);
      }
#endif
      memcpy (EvtData->aux.customPayload, &EvtPayload[0], EvtPayloadI8);
      return 1;
   };

   return 1;
}

#ifdef NETWORK_MESSAGING    // Read events
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadEventFIFOMessage - read from the event fifo
 * 
 * FUNCTION
 *  This function reads a single event from the event fifo if one is available
 * 
 * RETURNS
 *  Status  code indicating success or a specific error.
 *
 * Notes: This function should be called in a loop until the return status 
 *        indicates that the fifo is empty.
 *      
 *        If the event code equals "EventLoopbackTeardownComplete", then the 
 *        contents of *pChannelId hold the coderBlockId that was assigned to
 *        the loopback coder that was torn down.
 */
GpakAsyncEventData_t EvtBuffer;       // Event buffer for data read
ADT_Int8* i8RcvBuffer = NULL;
ADT_Int32 EvtBufferI8 = 0;

gpakReadEventFIFOMessageStat_t gpakReadEventFIFOStream (ADT_UInt32 DspId,
                   ADT_Int16 *ChanId, GpakAsyncEventCode_t *EvtCode,
                   GpakAsyncEventData_t *EvtData) {


   // Read event message from TCP/IP
   EvtBufferI8 = gpakRecvDspEvt (DspId, &EvtBuffer, sizeof (EvtBuffer));
   if (EvtBufferI8 < 0) {       // Socket error
      gpakUnlockAccess (DspId);
      DSPError [DspId] = EvtBufferI8;
      return RefDspCommFailure; 
   }

   if (EvtBufferI8 == 0) {      // No event data
      return RefNoEventAvail;
   }
#if (DSP_WORD_I8 == 4)
   // Convert G.PAK DSP event messages from little endian to match host format
   if (HostBigEndian)
      gpakEndianI32Convert ((ADT_Int32*) &EvtBuffer, EvtBufferI8);
#endif

   i8RcvBuffer = (ADT_Int8*) &EvtBuffer;
   return RefEventAvail;
}

gpakReadEventFIFOMessageStat_t gpakReadEventFIFOMessage (ADT_UInt32 DspId,
                   ADT_Int16 *ChanId, GpakAsyncEventCode_t *EvtCode,
                   GpakAsyncEventData_t *EvtData) {
   gpakReadEventFIFOMessageStat_t ApiStat;
   ADT_UInt16* i16RcvBuff;
   ADT_UInt32* i32RcvBuff;
   ADT_Int32 EvtPayloadI8, maxPayloadI8;         // Length of returned event payload
   ADT_Bool EvtValid;

   if (MaxDsps <= DspId)
      return RefInvalidDsp;

   gpakLockAccess (DspId);
   if (CheckDspEvtConnect (DspId) == -1) {
      gpakUnlockAccess (DspId);
      return RefDspCommFailure;
   }

   // If additional events are in the event buffer, parse only
   // otherwise, read in more events
   if (EvtBufferI8 <= 0) {
      ApiStat = gpakReadEventFIFOStream (DspId, ChanId, EvtCode, EvtData);
      if (ApiStat != RefEventAvail) {
         EvtBufferI8 = 0;
      gpakUnlockAccess (DspId);
         return ApiStat;
   }
   }
   i16RcvBuff = (ADT_UInt16 *) i8RcvBuffer;
   i32RcvBuff = (ADT_UInt32 *) i8RcvBuffer;

   if (*i32RcvBuff != 0xaaaaaaaa) {
      EvtBufferI8 = 0;
      gpakUnlockAccess (DspId);
      return RefInvalidEvent;
   }


   // Parse event header (Skip four bytes of marker)
   maxPayloadI8 = parseEventHdr (&i16RcvBuff[2], &EvtPayloadI8, ChanId, EvtCode, &EvtData->ToneDevice);
   if (maxPayloadI8 == 0) {
      // No additional data
      EvtBufferI8 -= EvtPayloadI8 + 12;    // 
      i8RcvBuffer += EvtPayloadI8 + 12;
      gpakUnlockAccess (DspId);
      return RefEventAvail;
   }
   if (maxPayloadI8 < 0) {
      // Data exceeds structure size
      EvtBufferI8 = 0;
      gpakUnlockAccess (DspId);
      return RefInvalidEvent;
   }

   // Parse event data (Skip four bytes of marker and eight bytes of header)
   EvtValid = parseEvent (&i16RcvBuff[6], EvtPayloadI8, *EvtCode, EvtData);
   if (!EvtValid) {
      EvtBufferI8 = 0;
      gpakUnlockAccess (DspId);
      return RefInvalidEvent;
   }

   /* Unlock access to the DSP. */
   EvtBufferI8 -= EvtPayloadI8 + 12;    // 
   i8RcvBuffer += EvtPayloadI8 + 12;
   gpakUnlockAccess (DspId);
   return RefEventAvail;

}
#else
gpakReadEventFIFOMessageStat_t gpakReadEventFIFOMessage (ADT_UInt32 DspId,
                   ADT_Int16 *ChanId, GpakAsyncEventCode_t *EvtCode,
                   GpakAsyncEventData_t *EvtData) {

   DSP_Address DspEvtAddr;               // address of EventFIFO info structure 
   DSP_Word I16Free, I16Ready, TxHdrI16;
   int EvtDataI16;                       // Length of returned event payload
   ADT_Int32 EvtPayloadI8, maxPayloadI8;         // Length of returned event payload
   CircBufr_t cBuff;

   GpakAsyncEventData_t EvtBuffer;        // Event buffer for data read

   ADT_Bool EvtValid;                    // Bool indicating valid event message

   if (MaxDsps <= DspId)
      return RefInvalidDsp;

   gpakLockAccess (DspId);
   if (CheckDspReset(DspId) == -1) {
      gpakUnlockAccess (DspId);
      return RefDspCommFailure;
   }

   // Read circular buffer structure into host memory
   DspEvtAddr = pEventFifoAddress[DspId];
   if (DspEvtAddr == 0) {
      gpakUnlockAccess (DspId);
      return RefNoEventQue;
   }

   // Read circular buffer to determine if there is a complete event message
   TxHdrI16 = BytesToCircUnits (8);
   ReadCircStruct (DspId, DspEvtAddr, &cBuff, &I16Free, &I16Ready);
   if (I16Ready < TxHdrI16) {
      if (I16Ready != 0) PurgeCircStruct (DspId, DspEvtAddr, &cBuff);
      gpakUnlockAccess (DspId);
      return RefNoEventAvail;
   }

   // Read and parse event header
   ReadCircBuffer (DspId, &cBuff, (DSP_Word *) &EvtBuffer, TxHdrI16, DspEvtAddr);
   maxPayloadI8 = parseEventHdr ((ADT_UInt16 *) &EvtBuffer, (int *) &EvtPayloadI8, ChanId, EvtCode, &EvtData->ToneDevice);
   if (maxPayloadI8 == 0) {
      // No additional data
      gpakUnlockAccess (DspId);
      return RefEventAvail;
   }
   if (maxPayloadI8 < 0) {
      // Data exceeds structure size
      PurgeCircStruct (DspId, DspEvtAddr, &cBuff);
      gpakUnlockAccess (DspId);
      return RefInvalidEvent;
   }

   // Read and parse auxiliary data
   EvtDataI16 = BytesToCircUnits (EvtPayloadI8);
   ReadCircBuffer (DspId, &cBuff, (ADT_UInt32 *) &EvtBuffer, EvtDataI16, DspEvtAddr);
   EvtValid = parseEvent ((ADT_UInt16 *) &EvtBuffer, EvtPayloadI8, *EvtCode, EvtData);
   if (!EvtValid) {
      PurgeCircStruct (DspId, DspEvtAddr, &cBuff);
   };

   /* Unlock access to the DSP. */
   gpakUnlockAccess (DspId);

   if (!EvtValid) 
      return RefInvalidEvent;

   return RefEventAvail;

}
#endif

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakResetSystemStats - Reset system statistics
 * 
 * FUNCTION
 *  This function reset's the system statistics specified in the SelectMask
 * 
 * INPUTS
 *   DspId          - DSP Identifier
 *   SelectMask     - reset bit Mask
 * 
 * RETURNS
 *  Status  code indicating success or a specific error.
 */

gpakResetSystemStat_t       gpakResetSystemStats (ADT_UInt32 DspId, resetSsMask SelectMask) {

    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 *Msg;

    if (MaxDsps <= DspId)
        return RssInvalidDsp;

    Msg =(ADT_UInt16 *) &MsgBuffer[0];

    Msg[0] = MSG_RESET_SYS_STATS << 8;
    Msg[1] = 0;
    Msg[2] = 0;
    if (SelectMask.FramingStats)  Msg[2] = 1;
    if (SelectMask.CpuUsageStats) Msg[2] |= (1 << 1);

    if (!TransactCmd (DspId, MsgBuffer, 3, MSG_RESET_SYS_STATS_REPLY, 2, NO_ID_CHK, 0))
        return RssDspCommFailure;

    return RssSuccess;

}

//}==================================================================================
//===================================================================================
//{                  Conferencing APIs
//===================================================================================

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakConfigureConference - Configure a DSP's Conference.
 *
 * FUNCTION
 *  Configures a DSP's Conference.
 *
 * Inputs
 *      DspId     - Dsp identifier 
 * ConferenceId   - Conference identifier
 * FrameSize      - Samples per conference frame
 * InputToneTypes - Bit map of tone types for tone detection
 *
 * Outputs
 *    pStatus  - conference configuration status from DSP
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
GpakApiStatus_t gpakConfigureConference (ADT_UInt32 DspId, ADT_UInt16 ConferenceId,
                                         GpakFrameHalfMS FrameSizeHalfMS,  GpakToneTypes InputToneTypes,
                                         GPAK_ConferConfigStat_t *pStatus) {

    ADT_UInt32  MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 *Msg;

    if (MaxDsps <= DspId)
        return GpakApiInvalidDsp;

    Msg    = (ADT_UInt16 *) MsgBuffer;

    Msg[0] = (ADT_UInt16) PackBytes (MSG_CONFIG_CONFERENCE, CORE_ID (ConferenceId));
    Msg[1] = (ADT_UInt16) PackBytes (CNFR_ID (ConferenceId), FrameSizeHalfMS);
    Msg[2] = (ADT_UInt16) InputToneTypes & 0xFFF;

    if (!TransactCmd (DspId, MsgBuffer, 3, MSG_CONFIG_CONF_REPLY, 2, 
                    BYTE_ID, (ADT_UInt16) CNFR_ID (ConferenceId)))
        return GpakApiCommFailure;

    *pStatus = (GPAK_ConferConfigStat_t) Byte0 (Msg[1]);
    if (*pStatus == Cf_Success)
        return GpakApiSuccess;
    else
        return GpakApiParmError;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakConferenceGenerateTone - Control tone generation.
 *
 * FUNCTION
 *  This function controls the generation of tones on the specified conference.
 *
 * INPUTS
 *  DspId,         - DSP identifier
 *  ConfId,          - conference identifier
 *  *pToneParms,    - pointer to tone gen parameters
 *  *pStatus        - pointer to DSP status reply
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
ADT_UInt32 gpakFormatConfToneGenerator (ADT_UInt16 *Msg, ADT_UInt16 ConfId, 
                                        GpakToneGenParms_t *tone) {

   ADT_UInt32 MsgLenI16;

   /* Format the tone generation message */

    Msg[0] = (MSG_CONFTONEGEN << 8);
    Msg[1] = ConfId;
    Msg[2] = (ADT_UInt16) ( ((tone->ToneType  & 3) << 2) | 
                            ((tone->Device    & 1) << 1) | 
                            (tone->ToneCmd   & 1) );
    Msg[3] = (tone->Frequency[0]*10);
    Msg[4] = (tone->Frequency[1]*10);
    Msg[5] = (tone->Frequency[2]*10);
    Msg[6] = (tone->Frequency[3]*10);
    Msg[7] = (tone->Level[0]*10);
    Msg[8] = tone->OnDuration[0];
    Msg[9] = tone->OffDuration[0];
    MsgLenI16 = 10;

    // ----------------------------------
    Msg[10] = (tone->Level[1]*10);
    Msg[11] = (tone->Level[2]*10);
    Msg[12] = (tone->Level[3]*10);
    Msg[13] = tone->OnDuration[1];
    Msg[14] = tone->OnDuration[2];
    Msg[15] = tone->OnDuration[3];
    Msg[16] = tone->OffDuration[1];
    Msg[17] = tone->OffDuration[2];
    Msg[18] = tone->OffDuration[3];
    MsgLenI16 += 9;
    // ----------------------------------

    return MsgLenI16;
}

GpakApiStatus_t gpakConferenceGenerateTone (ADT_UInt32 DspId, ADT_UInt16 ConfId, 
                              GpakToneGenParms_t *Tone, GPAK_ToneGenStat_t *pStatus) {

   ADT_UInt32  MsgLenI16;                        /* message length */
   ADT_UInt32  MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
   ADT_UInt16 *Msg;

   if (MaxDsps <= DspId)
      return TgcInvalidDsp;

   Msg    = (ADT_UInt16 *) MsgBuffer;

   MsgLenI16 = gpakFormatConfToneGenerator (Msg, ConfId, Tone);

   if (!TransactCmd (DspId, MsgBuffer, MsgLenI16, MSG_CONFTONEGEN_REPLY,
                     MsgLenI16, WORD_ID, ConfId))
        return GpakApiCommFailure;

    *pStatus = (GPAK_ToneGenStat_t) Byte1 (Msg[2]);
    if (*pStatus == Tg_Success)  return GpakApiSuccess;
    else                         return GpakApiParmError;
}

ADT_UInt32 gpakFormatConfToneGeneratorHighRes (ADT_UInt16 *Msg, ADT_UInt16 ConfId, 
                                        GpakToneGenParms_t *tone) {

   ADT_UInt32 MsgLenI16;

   /* Format the tone generation message */

    Msg[0] = (MSG_CONFTONEGEN << 8);
    Msg[1] = ConfId;
    Msg[2] = (ADT_UInt16) ( ((tone->ToneType  & 3) << 2) | 
                            ((tone->Device    & 1) << 1) | 
                            (tone->ToneCmd   & 1) );
    Msg[3] = tone->Frequency[0];
    Msg[4] = tone->Frequency[1];
    Msg[5] = tone->Frequency[2];
    Msg[6] = tone->Frequency[3];
    Msg[7] = tone->Level[0];
    Msg[8] = tone->OnDuration[0];
    Msg[9] = tone->OffDuration[0];
    MsgLenI16 = 10;

    // ----------------------------------
    Msg[10] = tone->Level[1];
    Msg[11] = tone->Level[2];
    Msg[12] = tone->Level[3];
    Msg[13] = tone->OnDuration[1];
    Msg[14] = tone->OnDuration[2];
    Msg[15] = tone->OnDuration[3];
    Msg[16] = tone->OffDuration[1];
    Msg[17] = tone->OffDuration[2];
    Msg[18] = tone->OffDuration[3];
    MsgLenI16 += 9;
    // ----------------------------------

    return MsgLenI16;
}

GpakApiStatus_t gpakConferenceGenerateToneHighRes (ADT_UInt32 DspId, ADT_UInt16 ConfId, 
                              GpakToneGenParms_t *Tone, GPAK_ToneGenStat_t *pStatus) {

   ADT_UInt32  MsgLenI16;                        /* message length */
   ADT_UInt32  MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
   ADT_UInt16 *Msg;

   if (MaxDsps <= DspId)
      return TgcInvalidDsp;

   Msg    = (ADT_UInt16 *) MsgBuffer;

   MsgLenI16 = gpakFormatConfToneGeneratorHighRes (Msg, ConfId, Tone);

   if (!TransactCmd (DspId, MsgBuffer, MsgLenI16, MSG_CONFTONEGEN_REPLY,
                     MsgLenI16, WORD_ID, ConfId))
        return GpakApiCommFailure;

    *pStatus = (GPAK_ToneGenStat_t) Byte1 (Msg[2]);
    if (*pStatus == Tg_Success)  return GpakApiSuccess;
    else                         return GpakApiParmError;
}
//}==================================================================================
//===================================================================================
//{                  Channel setup and status APIs
//===================================================================================
#define PCMPkt   pChanStatus->ChannelConfig.PcmPkt
#define PCMPcm   pChanStatus->ChannelConfig.PcmPcm
#define PKTPkt   pChanStatus->ChannelConfig.PktPkt
#define CIRCData pChanStatus->ChannelConfig.Circuit
#define CNFPcm   pChanStatus->ChannelConfig.ConferPcm
#define CNFPkt   pChanStatus->ChannelConfig.ConferPkt
#define CNFCmp   pChanStatus->ChannelConfig.ConferComp
#define LPBCdr   pChanStatus->ChannelConfig.LpbkCoder

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakConfigureChannel - Configure a DSP's Channel.
 *
 * FUNCTION
 *  Configures a DSP's Channel.
 *
 * Inputs
 *   DspId     - Dsp identifier 
 * ChannelId   - Channel identifier
 * ChannelType - Type of channel,  PCM-PCM, PCK-PKT,,,
 *  pChan      - Channel data structure pointer
 *
 * Outputs
 *    pStatus  - channel configuration status from DSP
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
ADT_UInt32             gpakFormatChannelConfig (ADT_UInt16 *Msg, GpakChannelConfig_t *pChan,
                                                ADT_UInt16 ChannelId, GpakChanType ChannelType) {

    ADT_UInt32 MsgLenI16;                     /* message length */
    int        Voice;

    Msg[0] = PackBytes (MSG_CONFIGURE_CHANNEL, CORE_ID(ChannelId));
    Msg[1] = PackBytes (CHAN_ID(ChannelId), ChannelType);

    /* Build the Configure Channel message based on the Channel Type. */
    switch (ChannelType) {
    case pcmToPacket:
        if (pChan->PcmPkt.Coding == GpakVoiceChannel) Voice = 1;
        else                                          Voice = 0;
        Msg[2] = PackPortandSlot (pChan->PcmPkt.PcmInPort,    pChan->PcmPkt.PcmInSlot);
        Msg[3] = PackPortandSlot (pChan->PcmPkt.PcmOutPort,   pChan->PcmPkt.PcmOutSlot);
        Msg[4] = PackBytes (pChan->PcmPkt.PktInCoding,  pChan->PcmPkt.PktInFrameHalfMS);
        Msg[5] = PackBytes (pChan->PcmPkt.PktOutCoding, pChan->PcmPkt.PktOutFrameHalfMS);
        Msg[6] = (ADT_UInt16) ( ( pChan->PcmPkt.PcmEcanEnable         << 15) |
                              ((pChan->PcmPkt.PktEcanEnable &    1) << 14) |
                              ((pChan->PcmPkt.VadEnable     &    1) << 13) |
                              ((pChan->PcmPkt.AgcEnable     &    1) << 12) |
                              ((pChan->PcmPkt.AECEcanEnable &    1) << 11) |
                              ((Voice                       &    1) << 10) |
                              ((pChan->PcmPkt.DtxEnable     &    1) << 9) 
#ifdef DSP_66x
                              |
                              ((pChan->PcmPkt.AgcEnablePkt2Pcm & 1) << 8)
                              |
                              ((pChan->PcmPkt.VadEnablePkt & 1) << 7)
#endif
							  );
        Msg[7] = 0;  // PinIn/PinOut Deprecated
        Msg[8] = (ADT_UInt16) ( ((pChan->PcmPkt.FaxMode     &    3) << 14) | 
                              ((pChan->PcmPkt.FaxTransport  &    3) << 12) |
                              ((pChan->PcmPkt.ToneTypes     & 0x0FFF)) );
        MsgLenI16 = 9;

        // 4_2 ---------------------------------------
        Msg[9] = (ADT_UInt16) (pChan->PcmPkt.CIdMode & 3);
        Msg[10] = pChan->PcmPkt.ToneGenGainG1;
        Msg[11] = pChan->PcmPkt.OutputGainG2;
        Msg[12] = pChan->PcmPkt.InputGainG3;
        MsgLenI16 += 4;
#ifdef DSP_66x
        Msg[13] = (ADT_UInt16) (((pChan->PcmPkt.ToneTypesPkt & 0x0FFF)) );
        MsgLenI16 += 1;
#endif
        // ---------------------- 

        break;

    case pcmToPcm:
        if (pChan->PcmPcm.Coding == GpakVoiceChannel) Voice = 1;
        else                                          Voice = 0;

        Msg[2] = PackPortandSlot (pChan->PcmPcm.InPortA,  pChan->PcmPcm.InSlotA);
        Msg[3] = PackPortandSlot (pChan->PcmPcm.OutPortA, pChan->PcmPcm.OutSlotA);
        Msg[4] = PackPortandSlot (pChan->PcmPcm.InPortB,  pChan->PcmPcm.InSlotB);
        Msg[5] = PackPortandSlot (pChan->PcmPcm.OutPortB, pChan->PcmPcm.OutSlotB);
        Msg[6] = PackBytes ( 
                        ( ( pChan->PcmPcm.EcanEnableA         << 7) |
                          ((pChan->PcmPcm.EcanEnableB    & 1) << 6) |
                          ((pChan->PcmPcm.AECEcanEnableA & 1) << 5) |
                          ((pChan->PcmPcm.AECEcanEnableB & 1) << 4) |
                          ((pChan->PcmPcm.AgcEnableA     & 1) << 3) |
                          ((pChan->PcmPcm.AgcEnableB     & 1) << 2) |
                          ( Voice                             << 1) ),
                           pChan->PcmPcm.FrameHalfMS);
        Msg[7] = 0;  // PinIn/PinOut Deprecated
        Msg[8] = 0;  // PinIn/PinOut Deprecated
        Msg[9]   = (pChan->PcmPcm.ToneTypesA & 0xFFF) | ((pChan->PcmPcm.NoiseSuppressA << 12) & 0x1000);
        Msg[10]  = (pChan->PcmPcm.ToneTypesB & 0xFFF) | ((pChan->PcmPcm.NoiseSuppressB << 12) & 0x1000);
        MsgLenI16 = 11;

        // 4_2 ---------------------------------------
        Msg[11] = (ADT_UInt16) (((pChan->PcmPcm.CIdModeA & 3) << 2) |
                                 (pChan->PcmPcm.CIdModeB & 3));
        Msg[12] = pChan->PcmPcm.ToneGenGainG1A;
        Msg[13] = pChan->PcmPcm.OutputGainG2A;
        Msg[14] = pChan->PcmPcm.InputGainG3A;
        Msg[15] = pChan->PcmPcm.ToneGenGainG1B;
        Msg[16] = pChan->PcmPcm.OutputGainG2B;
        Msg[17] = pChan->PcmPcm.InputGainG3B;
        MsgLenI16 += 7;
        // ---------------------------------

        break;

    case packetToPacket:
        Msg[2] = PackBytes (pChan->PktPkt.PktInCodingA,  pChan->PktPkt.PktInFrameHalfMSA);
        Msg[3] = PackBytes (pChan->PktPkt.PktOutCodingA, pChan->PktPkt.PktOutFrameHalfMSA);
        Msg[4] = PackBytes (
                     (  ( pChan->PktPkt.EcanEnableA      << 7) |
                        ((pChan->PktPkt.VadEnableA  & 1) << 6) |
                        ((pChan->PktPkt.EcanEnableB & 1) << 5) |
                        ((pChan->PktPkt.VadEnableB  & 1) << 4) |
                        ((pChan->PktPkt.AgcEnableA  & 1) << 3) |
                        ((pChan->PktPkt.AgcEnableB  & 1) << 2) |
                        ((pChan->PktPkt.DtxEnableA  & 1) << 1) |
                        ((pChan->PktPkt.DtxEnableB  & 1) << 0)),
                          CHAN_ID(pChan->PktPkt.ChannelIdB));
        Msg[5] = PackBytes (pChan->PktPkt.PktInCodingB,  pChan->PktPkt.PktInFrameHalfMSB);
        Msg[6] = PackBytes (pChan->PktPkt.PktOutCodingB, pChan->PktPkt.PktOutFrameHalfMSB);
        Msg[7] = pChan->PktPkt.ToneTypesA & 0xFFF;
        Msg[8] = pChan->PktPkt.ToneTypesB & 0xFFF;
        Msg[9] = pChan->PktPkt.OutputGainG2A;
        Msg[10] = pChan->PktPkt.InputGainG3A;
        Msg[11] = pChan->PktPkt.OutputGainG2B;
        Msg[12] = pChan->PktPkt.InputGainG3B;
        MsgLenI16 = 13;
        break;

    case circuitData:
        Msg[2] = PackPortandSlot (pChan->Circuit.PcmInPort, pChan->Circuit.PcmInSlot);
        Msg[3] = PackPortandSlot (pChan->Circuit.PcmOutPort, pChan->Circuit.PcmOutSlot);
        Msg[4] = (ADT_UInt16) (pChan->Circuit.MuxFactor << 8);
        Msg[5] =  0;  // PinIn/PinOut Deprecated
        MsgLenI16 = 6;
        break;

    case conferencePcm:

        Msg[2] = PackBytes (pChan->ConferPcm.ConferenceId,
                        ( ((pChan->ConferPcm.AgcInEnable    & 1) << 5) |
                          ((pChan->ConferPcm.EcanEnable     & 1) << 4) |
                          ((pChan->ConferPcm.AECEcanEnable  & 1) << 3) ) 
                          );
        Msg[3] = PackPortandSlot (pChan->ConferPcm.PcmInPort, pChan->ConferPcm.PcmInSlot);
        Msg[4] = PackPortandSlot (pChan->ConferPcm.PcmOutPort, pChan->ConferPcm.PcmOutSlot);
        Msg[5] = 0;  // PinIn/PinOut Deprecated
        Msg[6]  = pChan->ConferPcm.ToneTypes & 0xFFF;
        MsgLenI16 = 7;

        // 4_2 ---------------------------------------
        Msg[7]  = pChan->ConferPcm.ToneGenGainG1;
        Msg[8]  = (ADT_UInt16) (pChan->ConferPcm.AgcOutEnable & 1);
        Msg[9] = pChan->ConferPcm.OutputGainG2;
        Msg[10] = pChan->ConferPcm.InputGainG3;
        MsgLenI16 += 4;
        // ------------------------------------

        break;

    case conferencePacket:
        Msg[2] = PackBytes (pChan->ConferPkt.ConferenceId,
                        ((pChan->ConferPkt.VadEnable  & 1)  << 7) |
                        ((pChan->ConferPkt.AgcInEnable & 1) << 5) |
                        ((pChan->ConferPkt.EcanEnable & 1)  << 4) |
                        ((pChan->ConferPkt.DtxEnable & 1)   << 3) );
        Msg[3] = PackBytes (pChan->ConferPkt.PktInCoding, pChan->ConferPkt.PktOutCoding);
        Msg[4] = pChan->ConferPkt.ToneTypes & 0xFFF;
        MsgLenI16 = 5;

        // 4_2 ---------------------------------------
        Msg[5]  = (ADT_UInt16) (pChan->ConferPkt.AgcOutEnable & 1);
        Msg[6]  = pChan->ConferPkt.ToneGenGainG1;
        Msg[7]  = pChan->ConferPkt.OutputGainG2;
        Msg[8]  = pChan->ConferPkt.InputGainG3;
        MsgLenI16 += 4;

        // 5_0 ------------------------------------
        Msg[9] = pChan->ConferPkt.PktFrameHalfMS;
        MsgLenI16 += 1;

        break;

    case conferenceComposite:
        Msg[2] = PackBytes (pChan->ConferComp.ConferenceId,
                       ((pChan->ConferComp.VadEnable       & 1) << 7) |
                       ((pChan->ConferComp.DtxEnable       & 1) << 6) );
        Msg[3] = PackPortandSlot (pChan->ConferComp.PcmOutPort,   pChan->ConferComp.PcmOutSlot);
        Msg[4] = PackBytes (pChan->ConferComp.PktOutCoding, 0); // pinOut Deprecated
        MsgLenI16 = 5;
        break;

    // 4_2 ---------------------------------------
    case loopbackCoder:
        Msg[2] = PackBytes (pChan->LpbkCoder.PktInCoding, pChan->LpbkCoder.PktOutCoding);
        Msg[3] = PackBytes (pChan->LpbkCoder.PktFrameHalfMS, 0);
        MsgLenI16 = 4;
        break;

    // --------------------------------------------
    case customChannel:
       if (customChannelConfig != NULL)
          MsgLenI16 = (*customChannelConfig) (Msg, pChan, ChannelId);
       else
          MsgLenI16 = 0;
          
      break;

    default:
        MsgLenI16 = 0;
        break;
    }
    return MsgLenI16;
}

GpakApiStatus_t gpakConfigureChannel (ADT_UInt32 DspId, ADT_UInt16 ChannelId, 
                             GpakChanType ChannelType, GpakChannelConfig_t *pChan, 
                             GPAK_ChannelConfigStat_t *pStatus) {

    ADT_UInt32  MsgLenI16;                     /* message length */
    ADT_UInt32  MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 *Msg;

    if (MaxDsps <= DspId)
        return CcsInvalidDsp;

    if (pDspIfBlk[DspId] == 0)
       DSPSync (DspId);

    if (MaxChannels[DspId] <= (DSP_Word) CHAN_ID(ChannelId))
        return GpakApiInvalidChannel;

    Msg    = (ADT_UInt16 *) &MsgBuffer[0];

    MsgLenI16 = gpakFormatChannelConfig (Msg, pChan, ChannelId, ChannelType);
    if (MsgLenI16 == 0) {
        *pStatus = Cc_InvalidChannelType;
        return CcsParmError;
    }

   if (!TransactCmd (DspId, MsgBuffer, MsgLenI16, MSG_CONFIG_CHAN_REPLY, 2, 
                     BYTE_ID, (ADT_UInt16) CHAN_ID(ChannelId)))
        return GpakApiCommFailure;

    /* Return success or failure  */
    *pStatus = (GPAK_ChannelConfigStat_t) Byte0 (Msg[1]);
    if (*pStatus == Cc_Success)  return GpakApiSuccess;
    else                         return GpakApiParmError;
}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakTearDownChannel - Tear Down a DSP's Channel.
 *
 * FUNCTION
 *  Tears down a DSP's Channel.
 *
 * Inputs
 *   DspId     - Dsp identifier 
 * ChannelId   - Channel identifier
 *
 * Outputs
 *    pStatus  - channel tear down status from DSP
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
GpakApiStatus_t   gpakTearDownChannel (ADT_UInt32 DspId, ADT_UInt16 ChannelId,
                                           GPAK_TearDownChanStat_t *pStatus) {

    ADT_UInt32  MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 *Msg;


    if (MaxDsps <= DspId)
        return GpakApiInvalidDsp;

    if (pDspIfBlk[DspId] == 0)
       DSPSync (DspId);

    if (MaxChannels[DspId] <= (DSP_Word) CHAN_ID(ChannelId))
        return GpakApiInvalidChannel;

    Msg = (ADT_UInt16 *) MsgBuffer;

    Msg[0] = PackBytes (MSG_TEAR_DOWN_CHANNEL, CORE_ID (ChannelId));
    Msg[1] = (ADT_UInt16) (CHAN_ID(ChannelId) << 8);

    if (!TransactCmd (DspId, MsgBuffer, 2, MSG_TEAR_DOWN_REPLY, 2,
                    BYTE_ID, (ADT_UInt16) CHAN_ID(ChannelId)))
        return GpakApiCommFailure;

    *pStatus = (GPAK_TearDownChanStat_t) Byte0 (Msg[1]);
    if (*pStatus == Td_Success)
        return GpakApiSuccess;
    else
        return GpakApiParmError;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakGetChannelStatus - Read a DSP's Channel Status.
 *
 * FUNCTION
 *  Reads a DSP's Channel Status information for a specified channel.
 *
 * Inputs
 *   DspId     - Dsp identifier 
 * ChannelId   - Channel identifier
 *
 * Outputs
 *    pChanStatus - Channel status data structure pointer
 *    pStatus  - channel status from DSP
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
GpakApiStatus_t  gpakParseChanStatusResponse (ADT_UInt32 ReplyLength, ADT_UInt16 *Msg, 
                                                      GpakChannelStatus_t *pChanStatus,
                                                      GPAK_ChannelStatusStat_t *pStatus) {

    *pStatus = (GPAK_ChannelStatusStat_t) Byte0 (Msg[1]);
    if (*pStatus != Cs_Success)
        return GpakApiParmError;

    pChanStatus->ChannelType = (GpakChanType) Byte1 (Msg[2]);
    if (pChanStatus->ChannelType == inactive) 
       return GpakApiSuccess;

      if (ReplyLength < 7)
         return GpakApiCommFailure;

    pChanStatus->BadPktHeaderCnt =  Msg[3];
    pChanStatus->PktOverflowCnt =   Msg[4];
    pChanStatus->PktUnderflowCnt =  Msg[5];
    pChanStatus->StatusFlags =      Msg[6];

    switch (pChanStatus->ChannelType)  {
    case pcmToPacket:
       if (ReplyLength < 14)
          return GpakApiCommFailure;


       PCMPkt.PcmInPort =      (GpakSerialPort_t) UnpackPort (Msg[7]);
       PCMPkt.PcmInSlot =                         UnpackSlot (Msg[7]);
       PCMPkt.PcmOutPort =     (GpakSerialPort_t) UnpackPort (Msg[8]);
       PCMPkt.PcmOutSlot =                        UnpackSlot (Msg[8]);
       PCMPkt.PktInCoding       = (GpakCodecs)         Byte1 (Msg[9]);
       PCMPkt.PktInFrameHalfMS  = (GpakFrameHalfMS)    Byte0 (Msg[9]);
       PCMPkt.PktOutCoding      = (GpakCodecs)         Byte1 (Msg[10]);
       PCMPkt.PktOutFrameHalfMS = (GpakFrameHalfMS)    Byte0 (Msg[10]);

       if (Msg[11] & 0x8000)  PCMPkt.PcmEcanEnable = Enabled;
       else                   PCMPkt.PcmEcanEnable = Disabled;

       if (Msg[11] & 0x4000)  PCMPkt.PktEcanEnable = Enabled;
       else                   PCMPkt.PktEcanEnable = Disabled;

       if (Msg[11] & 0x2000)  PCMPkt.VadEnable = Enabled;
       else                   PCMPkt.VadEnable = Disabled;

       if (Msg[11] & 0x1000)  PCMPkt.AgcEnable = Enabled;
       else                   PCMPkt.AgcEnable = Disabled;

       if (Msg[11] & 0x0800)  PCMPkt.AECEcanEnable = Enabled;
       else                   PCMPkt.AECEcanEnable = Disabled;

       if (Msg[11] & 0x0400)  PCMPkt.Coding = GpakVoiceChannel;
       else                   PCMPkt.Coding = GpakDataChannel;

       if (Msg[11] & 0x0200)  PCMPkt.DtxEnable = Enabled;
       else                   PCMPkt.DtxEnable = Disabled;
 


       PCMPkt.PcmInPin =  0;   // Deprecated
       PCMPkt.PcmOutPin = 0;  // Deprecated

       PCMPkt.FaxMode =       (GpakFaxMode_t) ((Msg[13] >> 14) & 0x0003);
       PCMPkt.FaxTransport =  (GpakFaxTransport_t) ((Msg[13] >> 12) & 0x0003);
       PCMPkt.ToneTypes =     (GpakToneTypes) (Msg[13] & 0x0FFF);

        // 4_2 ----------------------------------------------------
        PCMPkt.CIdMode       =   (GpakCidMode_t) (Msg[14] & 0x0003);
        PCMPkt.ToneGenGainG1 =   Msg[15];
        PCMPkt.OutputGainG2  =   Msg[16];
        PCMPkt.InputGainG3   =   Msg[17];
        pChanStatus->NumPktsToDsp    = Msg[18];
        pChanStatus->NumPktsFromDsp  = Msg[19];
        // ---------------------------------------------------------

       break;

    case pcmToPcm:
       if (ReplyLength < 16)
          return GpakApiCommFailure;

       PCMPcm.InPortA =   (GpakSerialPort_t) UnpackPort (Msg[7]);
       PCMPcm.InSlotA =                      UnpackSlot (Msg[7]);
       PCMPcm.OutPortA =  (GpakSerialPort_t) UnpackPort (Msg[8]);
       PCMPcm.OutSlotA =                     UnpackSlot (Msg[8]);
       PCMPcm.InPortB =   (GpakSerialPort_t) UnpackPort (Msg[9]);
       PCMPcm.InSlotB =                      UnpackSlot (Msg[9]);
       PCMPcm.OutPortB =  (GpakSerialPort_t) UnpackPort (Msg[10]);
       PCMPcm.OutSlotB =                     UnpackSlot (Msg[10]);

       if (Msg[11] & 0x8000)    PCMPcm.EcanEnableA = Enabled;
       else                     PCMPcm.EcanEnableA = Disabled;

       if (Msg[11] & 0x4000)    PCMPcm.EcanEnableB = Enabled;
       else                     PCMPcm.EcanEnableB = Disabled;

       if (Msg[11] & 0x2000)    PCMPcm.AECEcanEnableA = Enabled;
       else                     PCMPcm.AECEcanEnableA = Disabled;

       if (Msg[11] & 0x1000)    PCMPcm.AECEcanEnableB = Enabled;
       else                     PCMPcm.AECEcanEnableB = Disabled;

       if (Msg[11] & 0x0800)    PCMPcm.AgcEnableA = Enabled;
       else                     PCMPcm.AgcEnableA = Disabled;

       if (Msg[11] & 0x0400)    PCMPcm.AgcEnableB = Enabled;
       else                     PCMPcm.AgcEnableB = Disabled;

       if (Msg[11] & 0x0200)    PCMPcm.Coding = GpakVoiceChannel;
       else                     PCMPcm.Coding = GpakDataChannel;

       PCMPcm.FrameHalfMS =     (GpakFrameHalfMS) Byte0 (Msg[11]);

       PCMPcm.InPinA =  0;  // Deprecated
       PCMPcm.InPinB =  0;  // Deprecated
       PCMPcm.OutPinA = 0;  // Deprecated
       PCMPcm.OutPinB = 0;  // Deprecated

       PCMPcm.ToneTypesA =  (GpakToneTypes) (Msg[14] & 0xFFF);
       PCMPcm.ToneTypesB =  (GpakToneTypes) (Msg[15] & 0xFFF);
       PCMPcm.NoiseSuppressA = (GpakActivation) ((Msg[14] >> 12) & 0x0001);
       PCMPcm.NoiseSuppressB = (GpakActivation) ((Msg[15] >> 12) & 0x0001);

       // 4_2 ----------------------------------------------------
       PCMPcm.CIdModeA         = (GpakCidMode_t) ((Msg[16] >> 2) & 3);
       PCMPcm.CIdModeB         = (GpakCidMode_t) (Msg[16] & 3);
       PCMPcm.ToneGenGainG1A   = Msg[17];
       PCMPcm.OutputGainG2A    = Msg[18];
       PCMPcm.InputGainG3A     = Msg[19];
       PCMPcm.ToneGenGainG1B   = Msg[20];
       PCMPcm.OutputGainG2B    = Msg[21];
       PCMPcm.InputGainG3B     = Msg[22];
       pChanStatus->NumPktsToDsp    = 0;
       pChanStatus->NumPktsFromDsp  = 0;
       // --------------------------------------------------------
       break;

    case packetToPacket:
       if (ReplyLength < 14)
          return GpakApiCommFailure;

       PKTPkt.PktInCodingA =         (GpakCodecs)       Byte1 (Msg[7]);
       PKTPkt.PktInFrameHalfMSA =    (GpakFrameHalfMS) Byte0 (Msg[7]);
       PKTPkt.PktOutCodingA =        (GpakCodecs)       Byte1 (Msg[8]);
       PKTPkt.PktOutFrameHalfMSA =   (GpakFrameHalfMS) Byte0 (Msg[8]);

       if (Msg[9] & 0x8000)  PKTPkt.EcanEnableA = Enabled;
       else                  PKTPkt.EcanEnableA = Disabled;

       if (Msg[9] & 0x4000)  PKTPkt.VadEnableA = Enabled;
       else                  PKTPkt.VadEnableA = Disabled;

       if (Msg[9] & 0x2000)  PKTPkt.EcanEnableB = Enabled;
       else                  PKTPkt.EcanEnableB = Disabled;

       if (Msg[9] & 0x1000)  PKTPkt.VadEnableB = Enabled;
       else                  PKTPkt.VadEnableB = Disabled;

       if (Msg[9] & 0x0800)  PKTPkt.AgcEnableA = Enabled;
       else                  PKTPkt.AgcEnableA = Disabled;

       if (Msg[9] & 0x0400)  PKTPkt.AgcEnableB = Enabled;
       else                  PKTPkt.AgcEnableB = Disabled;

       if (Msg[9] & 0x0200)  PKTPkt.DtxEnableA = Enabled;
       else                  PKTPkt.DtxEnableA = Disabled;

       if (Msg[9] & 0x0100)  PKTPkt.DtxEnableB = Enabled;
       else                  PKTPkt.DtxEnableB = Disabled;

       PKTPkt.ChannelIdB =                              Byte0 (Msg[9]);
       PKTPkt.PktInCodingB =           (GpakCodecs)     Byte1 (Msg[10]);
       PKTPkt.PktInFrameHalfMSB =    (GpakFrameHalfMS) Byte0 (Msg[10]);
       PKTPkt.PktOutCodingB =          (GpakCodecs)     Byte1 (Msg[11]);
       PKTPkt.PktOutFrameHalfMSB =   (GpakFrameHalfMS) Byte0 (Msg[11]);

       PKTPkt.ToneTypesA = (GpakToneTypes) (Msg[12] & 0xFFF);
       PKTPkt.ToneTypesB = (GpakToneTypes) (Msg[13] & 0xFFF);

       // 4_2 and beyond ---------------------------------
       pChanStatus->NumPktsToDsp    = Msg[14];
       pChanStatus->NumPktsFromDsp  = Msg[15];
       pChanStatus->NumPktsToDspB    = Msg[16];
       pChanStatus->NumPktsFromDspB  = Msg[17];
       PKTPkt.OutputGainG2A  = Msg[18];
       PKTPkt.InputGainG3A   = Msg[19];
       PKTPkt.OutputGainG2B  = Msg[20];
       PKTPkt.InputGainG3B   = Msg[21];
       // -------------------------------------
       break;

    case circuitData:
       if (ReplyLength < 11)
           return GpakApiCommFailure;

       CIRCData.PcmInPort =  (GpakSerialPort_t) UnpackPort (Msg[7]);
       CIRCData.PcmInSlot =                     UnpackSlot (Msg[7]);
       CIRCData.PcmOutPort = (GpakSerialPort_t) UnpackPort (Msg[8]);
       CIRCData.PcmOutSlot =                    UnpackSlot (Msg[8]);
       CIRCData.MuxFactor =                     Byte1 (Msg[9]);
       CIRCData.PcmInPin  =   0;  // Deprecated
       CIRCData.PcmOutPin =   0;  // Deprecated
       break;

    case conferencePcm:
       if (ReplyLength < 12)
            return GpakApiCommFailure;

       CNFPcm.ConferenceId = Byte1 (Msg[7]);

       if (Msg[7] & 0x0020) CNFPcm.AgcInEnable = Enabled;
       else                 CNFPcm.AgcInEnable = Disabled;

       if (Msg[7] & 0x0010)  CNFPcm.EcanEnable = Enabled;
       else                  CNFPcm.EcanEnable = Disabled;

       if (Msg[7] & 0x0008)  CNFPcm.AECEcanEnable = Enabled;
       else                  CNFPcm.AECEcanEnable = Disabled;

       CNFPcm.PcmInPort  =  (GpakSerialPort_t) UnpackPort (Msg[8]);
       CNFPcm.PcmInSlot  =                     UnpackSlot (Msg[8]);
       CNFPcm.PcmOutPort =  (GpakSerialPort_t) UnpackPort (Msg[9]);
       CNFPcm.PcmOutSlot =                     UnpackSlot (Msg[9]);
       CNFPcm.PcmInPin   =  0;  // Deprecated
       CNFPcm.PcmOutPin  =  0;  // Deprecated

       CNFPcm.ToneTypes        =  (GpakToneTypes) (Msg[11] & 0xFFF);

       // 4_2 ----------------------------------------------------
       CNFPcm.AgcOutEnable  = (GpakActivation) (Msg[12] & 1);
       CNFPcm.ToneGenGainG1 = Msg[13];
       CNFPcm.OutputGainG2  = Msg[14];
       CNFPcm.InputGainG3   = Msg[15];
       // --------------------------------------------------------
       break;

    case conferenceMultiRate:
    case conferencePacket:
       if (ReplyLength < 10)
            return GpakApiCommFailure;

       CNFPkt.ConferenceId =   Byte1 (Msg[7]);

       if (Msg[7] & 0x0080) CNFPkt.VadEnable = Enabled;
       else                 CNFPkt.VadEnable = Disabled;

       if (Msg[7] & 0x0020) CNFPkt.AgcInEnable = Enabled;
       else                 CNFPkt.AgcInEnable = Disabled;

       if (Msg[7] & 0x0010) CNFPkt.EcanEnable = Enabled;
       else                 CNFPkt.EcanEnable = Disabled;

       if (Msg[7] & 0x0008) CNFPkt.DtxEnable = Enabled;
       else                 CNFPkt.DtxEnable = Disabled;

       CNFPkt.PktInCoding =  (GpakCodecs) Byte1 (Msg[8]);
       CNFPkt.PktOutCoding = (GpakCodecs) Byte0 (Msg[8]);
       CNFPkt.ToneTypes    = (GpakToneTypes) (Msg[9] & 0xFFF);

       // 4_2 ----------------------------------------------------
       CNFPkt.AgcOutEnable  = (GpakActivation) (Msg[10] & 1);
       CNFPkt.ToneGenGainG1 = Msg[11];
       CNFPkt.OutputGainG2  = Msg[12];
       CNFPkt.InputGainG3   = Msg[13];
       pChanStatus->NumPktsToDsp    = Msg[14];
       pChanStatus->NumPktsFromDsp  = Msg[15];
       // --------------------------------------------------------

       if (ReplyLength < 17) CNFPkt.PktFrameHalfMS = (GpakFrameHalfMS) 0;
       else                  CNFPkt.PktFrameHalfMS = (GpakFrameHalfMS) Msg[16];

       break;

    case conferenceComposite:
       if (ReplyLength < 10)
          return GpakApiCommFailure;

       CNFCmp.ConferenceId = Byte1 (Msg[7]);

       if (Msg[7] & 0x0080) CNFCmp.VadEnable = Enabled;
       else                 CNFCmp.VadEnable = Disabled;

       if (Msg[7] & 0x0040) CNFCmp.DtxEnable = Enabled;
       else                 CNFCmp.DtxEnable = Disabled;

       CNFCmp.PcmOutPort = (GpakSerialPort_t) UnpackPort (Msg[8]);
       CNFCmp.PcmOutSlot =                    UnpackSlot (Msg[8]);
       CNFCmp.PktOutCoding =     (GpakCodecs) Byte1 (Msg[9]);
       CNFCmp.PcmOutPin =     0;  // Deprecated
       pChanStatus->NumPktsToDsp    =  0;
       pChanStatus->NumPktsFromDsp  =  Msg[10];
       break;

    // 4_2 ---------------------------------------
    case loopbackCoder:
       if (ReplyLength < 8)
          return GpakApiCommFailure;

        LPBCdr.PktInCoding  = (GpakCodecs) Byte1 (Msg[7]);
        LPBCdr.PktOutCoding = (GpakCodecs) Byte0 (Msg[7]);
        pChanStatus->NumPktsToDsp    = Msg[8];
        pChanStatus->NumPktsFromDsp  = Msg[9];
        break;
    // --------------------------------------------

    case customChannel:
       if (customChannelStatusParse != NULL)
          (*customChannelStatusParse) (Msg, pChanStatus);
       break;

    default:
       return GpakApiCommFailure;
    }
    return GpakApiSuccess;
}

GpakApiStatus_t  gpakGetChannelStatus (ADT_UInt32 DspId, ADT_UInt16 ChannelId,
                                       GpakChannelStatus_t *pChanStatus, 
                                       GPAK_ChannelStatusStat_t *pStatus) {


    ADT_UInt32  MsgLenI16;                        /* message length */
    ADT_UInt32  MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 *Msg;

    if (MaxDsps <= DspId)
        return GpakApiInvalidDsp;

    if (pDspIfBlk[DspId] == 0)
       DSPSync (DspId);

    if (MaxChannels[DspId] <= (DSP_Word) CHAN_ID(ChannelId))
        return GpakApiInvalidChannel;

    Msg = (ADT_UInt16 *) MsgBuffer;

    Msg[0] = PackBytes (MSG_CHAN_STATUS_RQST, CORE_ID(ChannelId));
    Msg[1] = (ADT_UInt16) (CHAN_ID (ChannelId) << 8);

    MsgLenI16 = TransactCmd (DspId, MsgBuffer, 2, MSG_CHAN_STATUS_REPLY, MSG_BUFFER_ELEMENTS, 
                        BYTE_ID, (ADT_UInt16) CHAN_ID(ChannelId));
    if (!MsgLenI16)
        return GpakApiCommFailure;

    pChanStatus->ChannelId = ((Msg[0] & 0xff) << 8) | ChannelId;
    return gpakParseChanStatusResponse (MsgLenI16, Msg, pChanStatus, pStatus);

}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakSendRTPMsg 
 *
 * FUNCTION
 *    Configure a channel's RTP parameters.
 *
 * Inputs
 *      DspId  - Dsp identifier 
 * ChannelId   - Channel identifier
 * Cfg         - Pointer to structure containing RTP configuration parameters
 *
 * Outputs
 *    pStatus  - RTP configuration status from DSP
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */

ADT_UInt32                gpakFormatRTPMsg (ADT_UInt16 *Msg,  ADT_UInt16  ChannelId, GpakRTPCfg_t *Cfg) {

    /* Build the Configure RTP message. */
    Msg[0]  = PackBytes (MSG_RTP_CONFIG, CORE_ID(ChannelId));
    Msg[1]  = PackBytes (Cfg->JitterMode, CHAN_ID(ChannelId));

    Msg[2]  = (ADT_UInt16)((Cfg->SSRC >> 16) & 0xffff);
    Msg[3]  = (ADT_UInt16)(Cfg->SSRC & 0xffff);
    Msg[4]  = Cfg->StartSequence;

    Msg[5]  = Cfg->DelayTargetMinMS;
    Msg[6]  = Cfg->DelayTargetMS;
    Msg[7]  = Cfg->DelayTargetMaxMS;

    Msg[8]  = PackBytes (Cfg->CNGPyldType, Cfg->VoicePyldType);
    Msg[9]  = PackBytes (Cfg->T38PyldType, Cfg->TonePyldType);

    Msg[10] = Cfg->DestPort;
    Msg[11] = (ADT_UInt16)((Cfg->DestIP >> 16) & 0xffff);
    Msg[12] = (ADT_UInt16) (Cfg->DestIP & 0xffff);
    Msg[13] = PackBytes (Cfg->DestMAC[0], Cfg->DestMAC[1]);
    Msg[14] = PackBytes (Cfg->DestMAC[2], Cfg->DestMAC[3]);
    Msg[15] = PackBytes (Cfg->DestMAC[4], Cfg->DestMAC[5]);
    Msg[16] = Cfg->SrcPort;
    Msg[17] = (ADT_UInt16)((Cfg->DestSSRC >> 16) & 0xffff);
    Msg[18] = (ADT_UInt16) (Cfg->DestSSRC & 0xffff);
    Msg[19] = Cfg->SamplingHz;

    Msg[20] = (ADT_UInt16)((Cfg->DestTxIP >> 16) & 0xffff);
    Msg[21] = (ADT_UInt16) (Cfg->DestTxIP & 0xffff);
    Msg[22] = PackBytes (Cfg->VlanIdx, Cfg->DSCP);
    Msg[23] = (ADT_UInt16)((Cfg->InDestIP >> 16) & 0xffff);
    Msg[24] = (ADT_UInt16) (Cfg->InDestIP & 0xffff);
    return 25;
}

gpakRtpStatus_t           gpakSendRTPMsg (ADT_UInt32 DspId,  ADT_UInt16 ChannelId, 
                                          GpakRTPCfg_t *Cfg, GPAK_RTPConfigStat_t *dspStatus) {

    ADT_UInt32 MsgLenI16;                     /* message length */
    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 *Msg;


    if (MaxDsps <= DspId)
        return RtpInvalidDsp;

    Msg =(ADT_UInt16 *) &MsgBuffer[0];

    MsgLenI16 = gpakFormatRTPMsg (Msg, ChannelId, Cfg);
    if (!TransactCmd (DspId, MsgBuffer, MsgLenI16, MSG_RTP_CONFIG_REPLY, 2,
                      BYTE_ID, (ADT_UInt16) CHAN_ID(ChannelId)))
        return RtpDspCommFailure;

    /* Return success or failure . */
    *dspStatus = (GPAK_RTPConfigStat_t) Byte0 (Msg[1]);
    if (*dspStatus == RTPSuccess)  return RtpSuccess;
    else                           return RtpParmError;
}
ADT_UInt32                gpakFormatRTPv6Msg (ADT_UInt16 *Msg,  ADT_UInt16  ChannelId, GpakRTPv6Cfg_t *Cfg) {

    /* Build the Configure RTPv6 message. */
    Msg[0]  = PackBytes (MSG_RTP_V6_CONFIG, CORE_ID(ChannelId));
    Msg[1]  = PackBytes (Cfg->JitterMode, CHAN_ID(ChannelId));

    Msg[2]  = (ADT_UInt16)((Cfg->SSRC >> 16) & 0xffff);
    Msg[3]  = (ADT_UInt16)(Cfg->SSRC & 0xffff);
    Msg[4]  = Cfg->StartSequence;

    Msg[5]  = Cfg->DelayTargetMinMS;
    Msg[6]  = Cfg->DelayTargetMS;
    Msg[7]  = Cfg->DelayTargetMaxMS;

    Msg[8]  = PackBytes (Cfg->CNGPyldType, Cfg->VoicePyldType);
    Msg[9]  = PackBytes (Cfg->T38PyldType, Cfg->TonePyldType);

    Msg[10] = Cfg->DestPort;

    Msg[11] = (ADT_UInt16)((Cfg->DestIP[0]<< 8) | Cfg->DestIP[1]);
	Msg[12] = (ADT_UInt16)((Cfg->DestIP[2]<< 8) | Cfg->DestIP[3]);
	Msg[13] = (ADT_UInt16)((Cfg->DestIP[4]<< 8) | Cfg->DestIP[5]);
	Msg[14] = (ADT_UInt16)((Cfg->DestIP[6]<< 8) | Cfg->DestIP[7]);
    Msg[15] = (ADT_UInt16)((Cfg->DestIP[8]<< 8) | Cfg->DestIP[9]);
	Msg[16] = (ADT_UInt16)((Cfg->DestIP[10]<< 8) | Cfg->DestIP[11]);
	Msg[17] = (ADT_UInt16)((Cfg->DestIP[12]<< 8) | Cfg->DestIP[13]);
	Msg[18] = (ADT_UInt16)((Cfg->DestIP[14]<< 8) | Cfg->DestIP[15]);

    Msg[19] = PackBytes (Cfg->DestMAC[0], Cfg->DestMAC[1]);
    Msg[20] = PackBytes (Cfg->DestMAC[2], Cfg->DestMAC[3]);
    Msg[21] = PackBytes (Cfg->DestMAC[4], Cfg->DestMAC[5]);
    Msg[22] = Cfg->SrcPort;
    Msg[23] = (ADT_UInt16)((Cfg->DestSSRC >> 16) & 0xffff);
    Msg[24] = (ADT_UInt16) (Cfg->DestSSRC & 0xffff);
    Msg[25] = Cfg->SamplingHz;

	Msg[26] = (ADT_UInt16)((Cfg->DestTxIP[0]<< 8) | Cfg->DestTxIP[1]);
	Msg[27] = (ADT_UInt16)((Cfg->DestTxIP[2]<< 8) | Cfg->DestTxIP[3]);
	Msg[28] = (ADT_UInt16)((Cfg->DestTxIP[4]<< 8) | Cfg->DestTxIP[5]);
	Msg[29] = (ADT_UInt16)((Cfg->DestTxIP[6]<< 8) | Cfg->DestTxIP[7]);
    Msg[30] = (ADT_UInt16)((Cfg->DestTxIP[8]<< 8) | Cfg->DestTxIP[9]);
	Msg[31] = (ADT_UInt16)((Cfg->DestTxIP[10]<< 8) | Cfg->DestTxIP[11]);
	Msg[32] = (ADT_UInt16)((Cfg->DestTxIP[12]<< 8) | Cfg->DestTxIP[13]);
	Msg[33] = (ADT_UInt16)((Cfg->DestTxIP[14]<< 8) | Cfg->DestTxIP[15]);

    Msg[34] = PackBytes (Cfg->VlanIdx, Cfg->DSCP);

	Msg[35] = (ADT_UInt16)((Cfg->InDestIP[0]<< 8) | Cfg->InDestIP[1]);
	Msg[36] = (ADT_UInt16)((Cfg->InDestIP[2]<< 8) | Cfg->InDestIP[3]);
	Msg[37] = (ADT_UInt16)((Cfg->InDestIP[4]<< 8) | Cfg->InDestIP[5]);
	Msg[38] = (ADT_UInt16)((Cfg->InDestIP[6]<< 8) | Cfg->InDestIP[7]);
    Msg[39] = (ADT_UInt16)((Cfg->InDestIP[8]<< 8) | Cfg->InDestIP[9]);
	Msg[40] = (ADT_UInt16)((Cfg->InDestIP[10]<< 8) | Cfg->InDestIP[11]);
	Msg[41] = (ADT_UInt16)((Cfg->InDestIP[12]<< 8) | Cfg->InDestIP[13]);
	Msg[42] = (ADT_UInt16)((Cfg->InDestIP[14]<< 8) | Cfg->InDestIP[15]);

    return 43;
}

gpakRtpStatus_t           gpakSendRTPv6Msg (ADT_UInt32 DspId,  ADT_UInt16 ChannelId, 
                                          GpakRTPv6Cfg_t *Cfg, GPAK_RTPConfigStat_t *dspStatus) {

    ADT_UInt32 MsgLenI16;                     /* message length */
    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 *Msg;


    if (MaxDsps <= DspId)
        return RtpInvalidDsp;

    Msg =(ADT_UInt16 *) &MsgBuffer[0];

    MsgLenI16 = gpakFormatRTPv6Msg (Msg, ChannelId, Cfg);
    if (!TransactCmd (DspId, MsgBuffer, MsgLenI16, MSG_RTP_V6_CONFIG_REPLY, 2,
                      BYTE_ID, (ADT_UInt16) CHAN_ID(ChannelId)))
        return RtpDspCommFailure;

    /* Return success or failure . */
    *dspStatus = (GPAK_RTPConfigStat_t) Byte0 (Msg[1]);
    if (*dspStatus == RTPSuccess)  return RtpSuccess;
    else                           return RtpParmError;
}
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakSendRTP_RTCPMsg 
 *
 * FUNCTION
 *    Configure a channel's RTP and RTCP parameters.
 *
 * Inputs
 *      DspId  - Dsp identifier 
 * ChannelId   - Channel identifier
 * Cfg         - Pointer to structure containing RTP configuration parameters
 *
 * Outputs
 *    pStatus  - RTP configuration status from DSP
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */

ADT_UInt32                gpakFormatRTP_RTCPMsg (ADT_UInt16 *Msg,  ADT_UInt16  ChannelId, GpakRTPCfg_t *Cfg,  GpakRTCPCfg_t *RtcpCfg) {
int i, j, k;
ADT_UInt32 MsgLenI16, senderFract; 
SDES_CFG_ITEM *sdes;

    /* Build the RTP Configuration message. */
    Msg[0]  = PackBytes (MSG_RTP_CONFIG, CORE_ID(ChannelId));
    Msg[1]  = PackBytes (Cfg->JitterMode, CHAN_ID(ChannelId));

    Msg[2]  = (ADT_UInt16)((Cfg->SSRC >> 16) & 0xffff);
    Msg[3]  = (ADT_UInt16)(Cfg->SSRC & 0xffff);
    Msg[4]  = Cfg->StartSequence;

    Msg[5]  = Cfg->DelayTargetMinMS;
    Msg[6]  = Cfg->DelayTargetMS;
    Msg[7]  = Cfg->DelayTargetMaxMS;

    Msg[8]  = PackBytes (Cfg->CNGPyldType, Cfg->VoicePyldType);
    Msg[9]  = PackBytes (Cfg->T38PyldType, Cfg->TonePyldType);

    Msg[10] = Cfg->DestPort;
    Msg[11] = (ADT_UInt16)((Cfg->DestIP >> 16) & 0xffff);
    Msg[12] = (ADT_UInt16) (Cfg->DestIP & 0xffff);
    Msg[13] = PackBytes (Cfg->DestMAC[0], Cfg->DestMAC[1]);
    Msg[14] = PackBytes (Cfg->DestMAC[2], Cfg->DestMAC[3]);
    Msg[15] = PackBytes (Cfg->DestMAC[4], Cfg->DestMAC[5]);
    Msg[16] = Cfg->SrcPort;
    Msg[17] = (ADT_UInt16)((Cfg->DestSSRC >> 16) & 0xffff);
    Msg[18] = (ADT_UInt16) (Cfg->DestSSRC & 0xffff);
    Msg[19] = Cfg->SamplingHz;

    Msg[20] = (ADT_UInt16)((Cfg->DestTxIP >> 16) & 0xffff);
    Msg[21] = (ADT_UInt16) (Cfg->DestTxIP & 0xffff);
    Msg[22] = PackBytes (Cfg->VlanIdx, Cfg->DSCP);
    Msg[23] = (ADT_UInt16)((Cfg->InDestIP >> 16) & 0xffff);
    Msg[24] = (ADT_UInt16) (Cfg->InDestIP & 0xffff);

    // format the RTCP parameters
    Msg[25] = RtcpCfg->localRtcpPort;
    Msg[26] = RtcpCfg->remoteRtcpPort;
    Msg[27] = RtcpCfg->transportOverheadBytes;
    Msg[28] = RtcpCfg->timeoutMultiplier;

    Msg[29] = HighWord(RtcpCfg->minRTCPPktPeriodMs);
    Msg[30] = LowWord(RtcpCfg->minRTCPPktPeriodMs);
    Msg[31] = HighWord(RtcpCfg->rtcpBandwidthBytesPerSec);
    Msg[32] = LowWord(RtcpCfg->rtcpBandwidthBytesPerSec);
    senderFract = (ADT_UInt32)(RtcpCfg->senderBandwidthFraction);
    Msg[33] = HighWord(senderFract);
    Msg[34] = LowWord(senderFract);
    Msg[35] = PackBytes(RtcpCfg->sdes.itemCnt, RtcpCfg->sdes.pktsPerCycle);

    if (RtcpCfg->sdes.itemCnt > MAX_SDES_ITEMS)
        return 0;

    // Format the SDES items
    MsgLenI16 = 36;

    for (i=0; i<RtcpCfg->sdes.itemCnt; i++) {
        sdes = &RtcpCfg->sdes.item[i];
        Msg[MsgLenI16++] = PackBytes(sdes->type, sdes->firstPktInCycle);
        Msg[MsgLenI16++] = PackBytes(sdes->txInterval, sdes->dataBytes);

        // make sure the text data for this item fits in the message buffer
        if ((MsgLenI16 + (ADT_UInt32)(sdes->dataBytes+1)/2) > MSG_BUFFER_SIZE*2)
            return 0;

        for (j=0, k=0; j<(int)sdes->dataBytes/2; j++, k+=2) {
             Msg[MsgLenI16++] = PackBytes(sdes->textData[k], sdes->textData[k+1]);
        }
        // pack the remaining byte if odd number of dataBytes
        if (sdes->dataBytes & 1)
             Msg[MsgLenI16++] = PackBytes(sdes->textData[k], 0);
    }

    return MsgLenI16;
}

gpakRtpStatus_t           gpakSendRTP_RTCPMsg (ADT_UInt32 DspId,  ADT_UInt16 ChannelId, 
                                          GpakRTPCfg_t *Cfg,  GpakRTCPCfg_t *RtcpCfg, GPAK_RTPConfigStat_t *dspStatus) {

    ADT_UInt32 MsgLenI16;                     /* message length */
    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 *Msg;


    if (MaxDsps <= DspId)
        return RtpInvalidDsp;

    Msg =(ADT_UInt16 *) &MsgBuffer[0];

    MsgLenI16 = gpakFormatRTP_RTCPMsg (Msg, ChannelId, Cfg, RtcpCfg);
    if (MsgLenI16 == 0) return RtpParmError;

    if (!TransactCmd (DspId, MsgBuffer, MsgLenI16, MSG_RTP_CONFIG_REPLY, 2,
                      BYTE_ID, (ADT_UInt16) CHAN_ID(ChannelId)))
        return RtpDspCommFailure;

    /* Return success or failure . */
    *dspStatus = (GPAK_RTPConfigStat_t) Byte0 (Msg[1]);
    if (*dspStatus == RTPSuccess)  return RtpSuccess;
    else                           return RtpParmError;
}


ADT_UInt32                gpakFormatRTPv6_RTCPMsg (ADT_UInt16 *Msg,  ADT_UInt16  ChannelId, GpakRTPv6Cfg_t *Cfg,  GpakRTCPCfg_t *RtcpCfg) {
int i, j, k;
ADT_UInt32 MsgLenI16, senderFract; 
SDES_CFG_ITEM *sdes;

    /* Build the RTP Configuration message. */
    Msg[0]  = PackBytes (MSG_RTP_V6_CONFIG, CORE_ID(ChannelId));
    Msg[1]  = PackBytes (Cfg->JitterMode, CHAN_ID(ChannelId));

    Msg[2]  = (ADT_UInt16)((Cfg->SSRC >> 16) & 0xffff);
    Msg[3]  = (ADT_UInt16)(Cfg->SSRC & 0xffff);
    Msg[4]  = Cfg->StartSequence;

    Msg[5]  = Cfg->DelayTargetMinMS;
    Msg[6]  = Cfg->DelayTargetMS;
    Msg[7]  = Cfg->DelayTargetMaxMS;

    Msg[8]  = PackBytes (Cfg->CNGPyldType, Cfg->VoicePyldType);
    Msg[9]  = PackBytes (Cfg->T38PyldType, Cfg->TonePyldType);

    Msg[10] = Cfg->DestPort;

    Msg[11] = (ADT_UInt16)((Cfg->DestIP[0]<< 8) | Cfg->DestIP[1]);
	Msg[12] = (ADT_UInt16)((Cfg->DestIP[2]<< 8) | Cfg->DestIP[3]);
	Msg[13] = (ADT_UInt16)((Cfg->DestIP[4]<< 8) | Cfg->DestIP[5]);
	Msg[14] = (ADT_UInt16)((Cfg->DestIP[6]<< 8) | Cfg->DestIP[7]);
    Msg[15] = (ADT_UInt16)((Cfg->DestIP[8]<< 8) | Cfg->DestIP[9]);
	Msg[16] = (ADT_UInt16)((Cfg->DestIP[10]<< 8) | Cfg->DestIP[11]);
	Msg[17] = (ADT_UInt16)((Cfg->DestIP[12]<< 8) | Cfg->DestIP[13]);
	Msg[18] = (ADT_UInt16)((Cfg->DestIP[14]<< 8) | Cfg->DestIP[15]);

    Msg[19] = PackBytes (Cfg->DestMAC[0], Cfg->DestMAC[1]);
    Msg[20] = PackBytes (Cfg->DestMAC[2], Cfg->DestMAC[3]);
    Msg[21] = PackBytes (Cfg->DestMAC[4], Cfg->DestMAC[5]);
    Msg[22] = Cfg->SrcPort;
    Msg[23] = (ADT_UInt16)((Cfg->DestSSRC >> 16) & 0xffff);
    Msg[24] = (ADT_UInt16) (Cfg->DestSSRC & 0xffff);
    Msg[25] = Cfg->SamplingHz;

	Msg[26] = (ADT_UInt16)((Cfg->DestTxIP[0]<< 8) | Cfg->DestTxIP[1]);
	Msg[27] = (ADT_UInt16)((Cfg->DestTxIP[2]<< 8) | Cfg->DestTxIP[3]);
	Msg[28] = (ADT_UInt16)((Cfg->DestTxIP[4]<< 8) | Cfg->DestTxIP[5]);
	Msg[29] = (ADT_UInt16)((Cfg->DestTxIP[6]<< 8) | Cfg->DestTxIP[7]);
    Msg[30] = (ADT_UInt16)((Cfg->DestTxIP[8]<< 8) | Cfg->DestTxIP[9]);
	Msg[31] = (ADT_UInt16)((Cfg->DestTxIP[10]<< 8) | Cfg->DestTxIP[11]);
	Msg[32] = (ADT_UInt16)((Cfg->DestTxIP[12]<< 8) | Cfg->DestTxIP[13]);
	Msg[33] = (ADT_UInt16)((Cfg->DestTxIP[14]<< 8) | Cfg->DestTxIP[15]);

    Msg[34] = PackBytes (Cfg->VlanIdx, Cfg->DSCP);

	Msg[35] = (ADT_UInt16)((Cfg->InDestIP[0]<< 8) | Cfg->InDestIP[1]);
	Msg[36] = (ADT_UInt16)((Cfg->InDestIP[2]<< 8) | Cfg->InDestIP[3]);
	Msg[37] = (ADT_UInt16)((Cfg->InDestIP[4]<< 8) | Cfg->InDestIP[5]);
	Msg[38] = (ADT_UInt16)((Cfg->InDestIP[6]<< 8) | Cfg->InDestIP[7]);
    Msg[39] = (ADT_UInt16)((Cfg->InDestIP[8]<< 8) | Cfg->InDestIP[9]);
	Msg[40] = (ADT_UInt16)((Cfg->InDestIP[10]<< 8) | Cfg->InDestIP[11]);
	Msg[41] = (ADT_UInt16)((Cfg->InDestIP[12]<< 8) | Cfg->InDestIP[13]);
	Msg[42] = (ADT_UInt16)((Cfg->InDestIP[14]<< 8) | Cfg->InDestIP[15]);

    // format the RTCP parameters
    Msg[43] = RtcpCfg->localRtcpPort;
    Msg[44] = RtcpCfg->remoteRtcpPort;
    Msg[45] = RtcpCfg->transportOverheadBytes;
    Msg[46] = RtcpCfg->timeoutMultiplier;

    Msg[47] = HighWord(RtcpCfg->minRTCPPktPeriodMs);
    Msg[48] = LowWord(RtcpCfg->minRTCPPktPeriodMs);
    Msg[49] = HighWord(RtcpCfg->rtcpBandwidthBytesPerSec);
    Msg[50] = LowWord(RtcpCfg->rtcpBandwidthBytesPerSec);
    senderFract = (ADT_UInt32)(RtcpCfg->senderBandwidthFraction);
    Msg[51] = HighWord(senderFract);
    Msg[52] = LowWord(senderFract);
    Msg[53] = PackBytes(RtcpCfg->sdes.itemCnt, RtcpCfg->sdes.pktsPerCycle);

    if (RtcpCfg->sdes.itemCnt > MAX_SDES_ITEMS)
        return 0;

    // Format the SDES items
    MsgLenI16 = 54;

    for (i=0; i<RtcpCfg->sdes.itemCnt; i++) {
        sdes = &RtcpCfg->sdes.item[i];
        Msg[MsgLenI16++] = PackBytes(sdes->type, sdes->firstPktInCycle);
        Msg[MsgLenI16++] = PackBytes(sdes->txInterval, sdes->dataBytes);

        // make sure the text data for this item fits in the message buffer
        if ((MsgLenI16 + (ADT_UInt32)(sdes->dataBytes+1)/2) > MSG_BUFFER_SIZE*2)
            return 0;

        for (j=0, k=0; j<(int)sdes->dataBytes/2; j++, k+=2) {
             Msg[MsgLenI16++] = PackBytes(sdes->textData[k], sdes->textData[k+1]);
        }
        // pack the remaining byte if odd number of dataBytes
        if (sdes->dataBytes & 1)
             Msg[MsgLenI16++] = PackBytes(sdes->textData[k], 0);
    }

    return MsgLenI16;
}

gpakRtpStatus_t  gpakSendRTPv6_RTCPMsg (ADT_UInt32 DspId,  ADT_UInt16 ChannelId, 
                                          GpakRTPv6Cfg_t *Cfg,  GpakRTCPCfg_t *RtcpCfg, GPAK_RTPConfigStat_t *dspStatus) {

    ADT_UInt32 MsgLenI16;                     /* message length */
    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 *Msg;


    if (MaxDsps <= DspId)
        return RtpInvalidDsp;

    Msg =(ADT_UInt16 *) &MsgBuffer[0];

    MsgLenI16 = gpakFormatRTPv6_RTCPMsg (Msg, ChannelId, Cfg, RtcpCfg);
    if (MsgLenI16 == 0) return RtpParmError;

    if (!TransactCmd (DspId, MsgBuffer, MsgLenI16, MSG_RTP_V6_CONFIG_REPLY, 2,
                      BYTE_ID, (ADT_UInt16) CHAN_ID(ChannelId)))
        return RtpDspCommFailure;

    /* Return success or failure . */
    *dspStatus = (GPAK_RTPConfigStat_t) Byte0 (Msg[1]);
    if (*dspStatus == RTPSuccess)  return RtpSuccess;
    else                           return RtpParmError;
}
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakSendRTCPByeMsg 
 *
 * FUNCTION
 *    Send an RTCP bye message
 *
 * Inputs
 *      DspId  - Dsp identifier 
 * ChannelId   - Channel identifier
 * ByeMsg      - Pointer to buffer containing the bye msg data
 * ByeMsgI8    - Number of data bytes in the bye msg
 *
 * Outputs
 *    pStatus  - status from DSP
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */

ADT_UInt32                gpakFormatRTCPByeMsg (ADT_UInt16 *Msg,  ADT_UInt16  ChannelId, ADT_UInt8 *ByeMsg, ADT_UInt8 ByeMsgLenI8) {
int j, k;
ADT_UInt32 MsgLenI16; 

    /* Build the RTCP bye message. */
    Msg[0]  = PackBytes (MSG_RTCP_BYE, CORE_ID(ChannelId));
    Msg[1] =  Byte0(ByeMsgLenI8);
    MsgLenI16 = 2;

   for (j=0, k=0; j<(int)ByeMsgLenI8/2; j++, k+=2) {
        Msg[MsgLenI16++] = PackBytes(ByeMsg[k], ByeMsg[k+1]);
   }
   // pack the remaining byte if odd number of dataBytes
   if (ByeMsgLenI8 & 1)
        Msg[MsgLenI16++] = PackBytes(ByeMsg[k], 0);

    return MsgLenI16;
}
GpakApiStatus_t   gpakSendRTCPByeMsg (ADT_UInt32 DspId,  ADT_UInt16 ChannelId, 
                           ADT_UInt8 *ByeMsg, ADT_UInt8 ByeMsgLenI8, GPAK_RTCPBye_t *dspStatus) {

    ADT_UInt32 MsgLenI16;                     /* message length */
    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 *Msg;


    if (MaxDsps <= DspId)
        return RtpInvalidDsp;

    Msg =(ADT_UInt16 *) &MsgBuffer[0];

    MsgLenI16 = gpakFormatRTCPByeMsg (Msg, ChannelId, ByeMsg, ByeMsgLenI8);
    if (MsgLenI16 == 0) return GpakApiParmError;

    if (!TransactCmd (DspId, MsgBuffer, MsgLenI16, MSG_RTCP_BYE_REPLY, 2,
                      BYTE_ID, (ADT_UInt16) CHAN_ID(ChannelId)))
        return GpakApiCommFailure;

    /* Return success or failure . */
    *dspStatus = (GPAK_RTCPBye_t) Byte0 (Msg[1]);
    if (*dspStatus == RTCPBye_Success)  return GpakApiSuccess;
    else                           return GpakApiParmError;
}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakConfigSRTP 
 *
 * FUNCTION
 *    Configure a channel's SRTP parameters.
 *
 * Inputs
 *      DspId  - Dsp identifier 
 * ChannelId   - Channel identifier
 * Cfg         - Pointer to structure containing RTP configuration parameters
 *
 * Outputs
 *    pStatus  - RTP configuration status from DSP
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */

ADT_UInt32                gpakFormatSRTPCfg (ADT_UInt16 *Msg,  ADT_UInt16  ChannelId, GpakSRTPCfg_t *Cfg) {
   int i;

   /* Build the Configure Serial Ports message. */
   Msg[0]  = PackBytes (MSG_SRTP_CONFIG, CORE_ID(ChannelId));
   Msg[1]  = PackBytes (Cfg->Encode.EncryptionScheme, CHAN_ID(ChannelId));
   Msg[2]  = Cfg->Encode.AuthenticationScheme;
   Msg[3]  = Cfg->Encode.AuthenticationI8;
   for(i = 0; i< 16/2; i++)
      Msg[4+i] = PackBytes (Cfg->Encode.MasterKey[2*i], Cfg->Encode.MasterKey[2*i+1]);

   for(i = 0; i< 14/2; i++) 
      Msg[12+i] = PackBytes (Cfg->Encode.MasterSalt[2*i], Cfg->Encode.MasterSalt[2*i+1]);

   for(i = 0; i< 3; i++) 
      Msg[19+i] = Cfg->Encode.MasterKeyLife[i];

   for(i = 0; i< 4/2; i++) 
      Msg[22+i] = PackBytes (Cfg->Encode.MKI[2*i], Cfg->Encode.MKI[2*i+1]);

   Msg[24] = PackBytes (Cfg->Encode.MKI_I8, Cfg->Encode.KDR);

   Msg[25]  = Cfg->Decode.EncryptionScheme;
   Msg[26]  = Cfg->Decode.AuthenticationScheme;
   Msg[27]  = Cfg->Decode.AuthenticationI8;
   for(i = 0; i< 16/2; i++)
      Msg[28+i] = PackBytes (Cfg->Decode.MasterKey[2*i], Cfg->Decode.MasterKey[2*i+1]);

   for(i = 0; i< 14/2; i++) 
      Msg[36+i] = PackBytes (Cfg->Decode.MasterSalt[2*i], Cfg->Decode.MasterSalt[2*i+1]);

   for(i = 0; i< 3; i++) 
      Msg[43+i] = Cfg->Decode.MasterKeyLife[i];

   for(i = 0; i< 4/2; i++) 
      Msg[46+i] = PackBytes (Cfg->Decode.MKI[2*i], Cfg->Decode.MKI[2*i+1]);

   Msg[48] = PackBytes (Cfg->Decode.MKI_I8, Cfg->Decode.KDR);
   Msg[49] = Cfg->Decode.ROC;
   Msg[50] = Cfg->FrameHalfMS;
   Msg[51] = Cfg->Encode.KeySizeU8;
   Msg[52] = Cfg->Decode.KeySizeU8;

   for(i = 0; i< 16/2; i++)
      Msg[53+i] = PackBytes (Cfg->Encode.MasterKey[2*i+16], Cfg->Encode.MasterKey[2*i+1+16]);

   for(i = 0; i< 16/2; i++)
      Msg[61+i] = PackBytes (Cfg->Decode.MasterKey[2*i+16], Cfg->Decode.MasterKey[2*i+1+16]);

   return (51+1+8+1+8);
}

GpakApiStatus_t           gpakConfigSRTP (ADT_UInt32 DspId,  ADT_UInt16 ChannelId, 
                                          GpakSRTPCfg_t *Cfg, GpakSrtpStatus_t *dspStatus) {

    ADT_UInt32 MsgLenI16;                     /* message length */
    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 *Msg;


    if (MaxDsps <= DspId)
        return RtpInvalidDsp;

    if (pDspIfBlk[DspId] == 0)
       DSPSync (DspId);

    Msg =(ADT_UInt16 *) &MsgBuffer[0];

    MsgLenI16 = gpakFormatSRTPCfg (Msg, ChannelId, Cfg);
    if (!TransactCmd (DspId, MsgBuffer, MsgLenI16, MSG_SRTP_CONFIG_REPLY, 2,
                      BYTE_ID, (ADT_UInt16) CHAN_ID(ChannelId)))
        return RtpDspCommFailure;

    /* Return success or failure . */
    *dspStatus = (GpakSrtpStatus_t) Byte0 (Msg[1]);
    if (*dspStatus == GPAK_SRTP_SUCCESS)  return GpakApiSuccess;
    else                                  return GpakApiParmError;
}

ADT_UInt32 gpakFormatSRTPNewKey (
      ADT_UInt16 *Msg,               /* pointer to message buffer */
      ADT_UInt16  ChannelId,         /* Channel identifier */
      GpakSRTPKey_t *Cfg) {           /* Pointer to configuration structure */
int i;

    /* Build the Configure Serial Ports message. */
    Msg[0]  = PackBytes (MSG_SRTPNEWKEY_CONFIG, CORE_ID(ChannelId));
    Msg[1]  = PackBytes (Cfg->Direction, CHAN_ID(ChannelId) );
    Msg[2]  = Cfg->KeySizeU8; // KeySizeU8
    Msg[3]  = 0; // reserved
    for(i = 0; i< 16/2; i++)
      Msg[4+i] = PackBytes (Cfg->MasterKey[2*i], Cfg->MasterKey[2*i+1]);

    for(i = 0; i< 14/2; i++) 
      Msg[12+i] = PackBytes (Cfg->MasterSalt[2*i], Cfg->MasterSalt[2*i+1]);

    for(i = 0; i< 3; i++) 
      Msg[19+i] = Cfg->MasterKeyLife[i];

    for(i = 0; i< 4/2; i++) 
      Msg[22+i] = PackBytes (Cfg->MKI[2*i], Cfg->MKI[2*i+1]);

    Msg[24] = PackBytes (Cfg->MKI_I8, Cfg->KDR);

    for(i = 0; i< 16/2; i++)
      Msg[25+i] = PackBytes (Cfg->MasterKey[2*i+16], Cfg->MasterKey[2*i+1+16]);
    return 33;
}

GpakApiStatus_t gpakSRTPNewKey (
   ADT_UInt32       DspId,       /* DSP identifier */
   ADT_UInt16       ChannelId,   /* Channel identifier */
   GpakSRTPKey_t   *Cfg,         /* Pointer to RTP configuration data */
   GpakSrtpStatus_t *dspStatus)   /* Pointer to DSP's reply status */
{
    ADT_UInt32 MsgLenI16;                     /* message length */
    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 *Msg;


    if (MaxDsps <= DspId)
        return GpakApiInvalidDsp;

    if (pDspIfBlk[DspId] == 0)
       DSPSync (DspId);

    Msg =(ADT_UInt16 *) &MsgBuffer[0];

    MsgLenI16 = gpakFormatSRTPNewKey (Msg, ChannelId, Cfg);
    if (!TransactCmd (DspId, MsgBuffer, MsgLenI16, MSG_SRTPNEWKEY_CONFIG_REPLY, 2,
                      BYTE_ID, (ADT_UInt16) CHAN_ID(ChannelId)))
        return GpakApiCommFailure;

    /* Return success or failure . */
    *dspStatus = (GpakSrtpStatus_t) Byte0 (Msg[1]);
    if (*dspStatus == GPAK_SRTP_SUCCESS)  return GpakApiSuccess;
    else                                  return GpakApiParmError;

}
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakGetRTPStatistics 
 *
 * FUNCTION
 *    Obtain RTP st
 *
 * Inputs
 *      DspId  - Dsp identifier 
 * ChannelId   - Channel identifier
 *
 * Outputs
 * RTPStats    - Pointer to structure receiving RTP statistics data
 *  dspStatus  - RTP statistics reply rom DSP
 *
 * RETURNS
 *  API status code indicating success or a specific error.
 *
 */
GpakApiStatus_t gpakParseRTPStatistics (ADT_UInt32 ReplyI16, ADT_UInt16 *Rply, 
                                        GPAK_RTP_STATS *RTPStats, 
                                        GPAK_RTP_Stat_Rply_t *dspStatus) {

   float SamplesPerMS;

   memset (RTPStats, 0, sizeof (GPAK_RTP_STATS));
   *dspStatus = (GPAK_RTP_Stat_Rply_t) Byte0 (Rply[0]);
   if (*dspStatus != RTPStat_Success)
      return GpakApiParmError;

   if (ReplyI16 != 28) {
      return GpakApiCommFailure;
   }


   SamplesPerMS = (float) Byte0 (Rply[1]);
   RTPStats->PktsSent       = PackWords (Rply[2], Rply[3]);
   RTPStats->PktsRcvd       = PackWords (Rply[4], Rply[5]);
   RTPStats->LossPercentage = ((float) PackWords (Rply[6], Rply[7])) / 10.0f;
   RTPStats->PktsPlayed     = PackWords (Rply[8],  Rply[9]);
   RTPStats->PktsLate       = PackWords (Rply[10], Rply[11]);
   RTPStats->PktsOutOfRange = PackWords (Rply[12], Rply[13]);
   RTPStats->BufferOverflowCnt  = PackWords (Rply[14], Rply[15]);
   RTPStats->BufferUnderflowCnt = PackWords (Rply[16], Rply[17]);

   RTPStats->JitterMinMS     = ((float) PackWordsSigned (Rply[18], Rply[19])) / SamplesPerMS;
   RTPStats->JitterCurrentMS = ((float) PackWordsSigned (Rply[20], Rply[21])) / SamplesPerMS;
   RTPStats->JitterMaxMS     = ((float) PackWordsSigned (Rply[22], Rply[23])) / SamplesPerMS;
   
   RTPStats->JitterMode     = Rply[24];
   RTPStats->MinDelayMS     = (ADT_UInt16) (Rply[25] / SamplesPerMS);
   RTPStats->CurrentDelayMS = (ADT_UInt16) (Rply[26] / SamplesPerMS);
   RTPStats->MaxDelayMS     = (ADT_UInt16) (Rply[27] / SamplesPerMS);
   
   return GpakApiSuccess;
}


GpakApiStatus_t gpakGetRTPStatistics (ADT_UInt32 DspId, ADT_UInt16 ChannelId,
                                  GPAK_RTP_STATS *RTPStats,
                                  GPAK_RTP_Stat_Rply_t *dspStatus) {

   ADT_UInt32  RplyI16;                     /* message length */
   ADT_UInt32  MsgBuffer[MSG_BUFFER_SIZE];  /* message buffer */
   ADT_UInt16 *Msg;
   GpakApiStatus_t apiStatus;

   if (MaxDsps <= DspId)
      return GpakApiInvalidDsp;

   if (pDspIfBlk[DspId] == 0)
      DSPSync (DspId);

   if (MaxChannels[DspId] <= (DSP_Word) CHAN_ID(ChannelId))
      return GpakApiInvalidChannel;

   Msg = (ADT_UInt16 *) MsgBuffer;

   Msg[0] = PackBytes (MSG_RTP_STATUS_RQST, CORE_ID(ChannelId));
   Msg[1] = (ADT_UInt16) (CHAN_ID (ChannelId) << 8);

   RplyI16 = TransactCmd (DspId, MsgBuffer, 2, MSG_RTP_STATUS_REPLY, MSG_BUFFER_ELEMENTS, 
                          BYTE_ID, (ADT_UInt16) CHAN_ID(ChannelId));

   apiStatus = gpakParseRTPStatistics (RplyI16, Msg, RTPStats, dspStatus);
   if (apiStatus == GpakApiCommFailure) {
      DSPError[DspId] = DSPRplyLengthError;
   } else {
      RTPStats->ChannelId = ChannelId;
   }
   return apiStatus;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakGetRTCPStatistics 
 *
 * FUNCTION
 *    Obtain RTP + RTCP statistics
 *
 * Inputs
 *      DspId  - Dsp identifier 
 * ChannelId   - Channel identifier
 *
 * Outputs
 * RTPStats    - Pointer to structure receiving RTP statistics data
 * RTCPStats    - Pointer to structure receiving RTCP statistics data
 *  dspStatus  - RTP+RTCP statistics reply rom DSP
 *
 * RETURNS
 *  API status code indicating success or a specific error.
 *
 */
GpakApiStatus_t gpakParseRTCPStatistics (ADT_UInt32 ReplyI16, ADT_UInt16 *Rply, 
                                        GPAK_RTP_STATS *RTPStats, GPAK_RTCP_STATS *RTCPStats,
                                        GPAK_RTP_Stat_Rply_t *dspStatus) {

   float SamplesPerMS, fracLostQ8, RTT;
   ADT_UInt16 valid, indexI16;
   sdesReportItem_t *item;        

   memset (RTPStats, 0, sizeof (GPAK_RTP_STATS));
   //memset (RTCPStats, 0, sizeof (GPAK_RTCP_STATS));
   *dspStatus = (GPAK_RTP_Stat_Rply_t) Byte0 (Rply[0]);
   if (*dspStatus != RTPStat_Success)
      return GpakApiParmError;

   if (ReplyI16 <29) {
      return GpakApiCommFailure;
   }


   SamplesPerMS = (float) Byte0 (Rply[1]);
   RTPStats->PktsSent       = PackWords (Rply[2], Rply[3]);
   RTPStats->PktsRcvd       = PackWords (Rply[4], Rply[5]);
   RTPStats->LossPercentage = ((float) PackWords (Rply[6], Rply[7])) / 10.0f;
   RTPStats->PktsPlayed     = PackWords (Rply[8],  Rply[9]);
   RTPStats->PktsLate       = PackWords (Rply[10], Rply[11]);
   RTPStats->PktsOutOfRange = PackWords (Rply[12], Rply[13]);
   RTPStats->BufferOverflowCnt  = PackWords (Rply[14], Rply[15]);
   RTPStats->BufferUnderflowCnt = PackWords (Rply[16], Rply[17]);

   RTPStats->JitterMinMS     = ((float) PackWordsSigned (Rply[18], Rply[19])) / SamplesPerMS;
   RTPStats->JitterCurrentMS = ((float) PackWordsSigned (Rply[20], Rply[21])) / SamplesPerMS;
   RTPStats->JitterMaxMS     = ((float) PackWordsSigned (Rply[22], Rply[23])) / SamplesPerMS;
   
   RTPStats->JitterMode     = Rply[24];
   RTPStats->MinDelayMS     = (ADT_UInt16) (Rply[25] / SamplesPerMS);
   RTPStats->CurrentDelayMS = (ADT_UInt16) (Rply[26] / SamplesPerMS);
   RTPStats->MaxDelayMS     = (ADT_UInt16) (Rply[27] / SamplesPerMS);

   // Start of RTCP stats at reply element 28 ...
   valid = Rply[28];
   RTCPStats->validFlags = valid;
   if (!valid) {
        return GpakApiSuccess;
   }

   if (valid & RR_VALID) { 
        RTT = (float)PackWords (Rply[29], Rply[30]);    
        RTCPStats->roundTripTimeMs     = RTT/65.536;
        RTCPStats->rr_JitterEstimate   = PackWords (Rply[31], Rply[32]);  
        RTCPStats->rr_cumPktsLost      = PackWords (Rply[33], Rply[34]);  
        RTCPStats->rr_SSRC             = PackWords (Rply[35], Rply[36]);  
        fracLostQ8                     = (float) Byte0 (Rply[37]);
        RTCPStats->rr_fractionPktsLost =  fracLostQ8/256.0;
   }

   if (valid & SR_VALID) { 
        RTCPStats->sr_PktsSent         = PackWords (Rply[38], Rply[39]);        
        RTCPStats->sr_BytesSent        = PackWords (Rply[40], Rply[41]);       
        RTCPStats->sr_SSRC             = PackWords (Rply[42], Rply[43]);            
   }

   indexI16 = 44;

   // extract SDES data
   if (valid & SDES_VALID) {
        int i,j,k;

        RTCPStats->sdesReport.itemCnt = Rply[indexI16];
        indexI16++;
        if (RTCPStats->sdesReport.itemCnt > MAX_SDES_ITEMS)
            return  GpakApiParmError;

        for (i=0; i<RTCPStats->sdesReport.itemCnt; i++) {
            item = &(RTCPStats->sdesReport.item[i]);
            item->type   = Byte1(Rply[indexI16]); 
            item->dataI8 = Byte0(Rply[indexI16]); 
            indexI16++;
            for (j=0, k=0; j<(int)item->dataI8/2; j++, k+=2, indexI16++) 
            {
                if (item->data) {
                    item->data[k]   = Byte1(Rply[indexI16]);
                    item->data[k+1] = Byte0(Rply[indexI16]);
                }
            }
            // unpack the remaining byte if odd number of dataBytes
            if (item->dataI8 & 1) {
                if (item->data) {
                    item->data[k]   = Byte1(Rply[indexI16]);
                }
                indexI16++;
            }
        }
   }

   if (valid & BYE_VALID) {
        int j,k;
        byeReport_t *bye = &(RTCPStats->byeReport);

        bye->reasonI8 = Byte0(Rply[indexI16]);     
        indexI16++;
        for (j=0, k=0; j<(int)bye->reasonI8/2; j++, k+=2, indexI16++) 
        {
            if (bye->reason) {
                bye->reason[k]   = Byte1(Rply[indexI16]);
                bye->reason[k+1] = Byte0(Rply[indexI16]);
            }
        }
        // unpack the remaining byte if odd number of dataBytes
        if (bye->reasonI8 & 1) {
            if (bye->reason) {
                bye->reason[k]   = Byte1(Rply[indexI16]);
            }
            indexI16++;
        }
   }

   if (valid & APP_VALID) {
        int j,k;
        appReport_t *app = &(RTCPStats->appReport);

        app->blockI8 = Byte0(Rply[indexI16]);     
        indexI16++;
        for (j=0, k=0; j<(int)app->blockI8/2; j++, k+=2, indexI16++) 
        {
            if (app->block) {
                app->block[k]   = Byte1(Rply[indexI16]);
                app->block[k+1] = Byte0(Rply[indexI16]);
            }
        }
        // unpack the remaining byte if odd number of dataBytes
        if (app->blockI8 & 1) {
            if (app->block) {
                app->block[k]   = Byte1(Rply[indexI16]);
            }
            indexI16++;
        }
   }   
   return GpakApiSuccess;
}


GpakApiStatus_t gpakGetRTCPStatistics (ADT_UInt32 DspId, ADT_UInt16 ChannelId,
                                  GPAK_RTP_STATS *RTPStats, GPAK_RTCP_STATS *RTCPStats,
                                  GPAK_RTP_Stat_Rply_t *dspStatus) {

   ADT_UInt32  RplyI16;                     /* message length */
   ADT_UInt32  MsgBuffer[MSG_BUFFER_SIZE];  /* message buffer */
   ADT_UInt16 *Msg;
   GpakApiStatus_t apiStatus;

   if (MaxDsps <= DspId)
      return GpakApiInvalidDsp;

   if (pDspIfBlk[DspId] == 0)
      DSPSync (DspId);

   if (MaxChannels[DspId] <= (DSP_Word) CHAN_ID(ChannelId))
      return GpakApiInvalidChannel;

   Msg = (ADT_UInt16 *) MsgBuffer;

   Msg[0] = PackBytes (MSG_RTP_STATUS_RQST, CORE_ID(ChannelId));
//   Msg[1] = (ADT_UInt16) (CHAN_ID (ChannelId) << 8);
   Msg[1] = PackBytes(CHAN_ID (ChannelId), 1);  // 1 in lsb indicates RTCP request

   RplyI16 = TransactCmd (DspId, MsgBuffer, 2, MSG_RTP_STATUS_REPLY, MSG_BUFFER_ELEMENTS, 
                          BYTE_ID, (ADT_UInt16) CHAN_ID(ChannelId));

   apiStatus = gpakParseRTCPStatistics (RplyI16, Msg, RTPStats, RTCPStats, dspStatus);
   if (apiStatus == GpakApiCommFailure) {
      DSPError[DspId] = DSPRplyLengthError;
   } else {
      RTPStats->ChannelId = ChannelId;
   }
   return apiStatus;
}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakWriteRtpRxTimeoutMsg 
 *
 * FUNCTION
 *    Write the channels RTP RX Timeout value.
 *
 * Inputs
 *      DspId  - Dsp identifier 
 * ChannelId   - Channel identifier
 * Cfg         - Pointer to structure containing RTP configuration parameters
 *
 * Outputs
 *    pStatus  - RTP configuration status from DSP
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */

gpakRtpStatus_t gpakWriteRtpRxTimeoutMsg (ADT_UInt32 DspId,  ADT_UInt16 ChannelId, 
                                         ADT_UInt16 TimeoutValue, GPAK_RTP_Stat_Rply_t *dspStatus) {

    ADT_UInt32 MsgLenI16;                     /* message length */
    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 *Msg;


    if (MaxDsps <= DspId)
        return RtpInvalidDsp;

    Msg =(ADT_UInt16 *) &MsgBuffer[0];

	Msg[0]	= MSG_WRITE_RTP_TIMEOUT << 8;
	Msg[0] |= ChannelId;
    Msg[1]	= TimeoutValue;
    MsgLenI16 = 2;
    if (!TransactCmd (DspId, MsgBuffer, MsgLenI16, MSG_WRITE_RTP_TIMEOUT_REPLY, 2,
                      NO_ID_CHK, (ADT_UInt16) CHAN_ID(ChannelId)))
        return RtpDspCommFailure;

    /* Return success or failure . */
    *dspStatus = (GPAK_RTPConfigStat_t) Byte0 (Msg[1]);
    if (*dspStatus == RTPSuccess)  return RtpSuccess;
    else                           return RtpParmError;
}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakGetEcanState - Read an Echo Canceller's training state.
 *
 * FUNCTION
 *  This function reads an Echo Canceller's training state information.
 * INPUTS
 *   DspId -          DSP identifier
 *   ChannelId  -      channel identifier
 *  *pEcanState -     pointer to Ecan state variable
 * 
 * OUTPUTS
 *  *pEcanState -     Ecan state variable data
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
void                          gpakParseEcanState (ADT_UInt16 *Msg, GpakEcanState_t *pEcanState) {
    
    pEcanState->Size            = Msg[2];
    pEcanState->TapLength       = Msg[3];
    pEcanState->CoefQ           = Msg[4];
    pEcanState->BGCoefQ         = Msg[5];
    pEcanState->NumFirSegments  = Msg[6];
    pEcanState->FirSegmentLen   = Msg[7];

    memcpy (pEcanState->Data, &Msg[8], EC_STATE_DATA_LEN * sizeof (ADT_UInt16));
}

gpakGetEcanStateStat_t        gpakGetEcanState (ADT_UInt32  DspId, ADT_UInt16 ChannelId,
                                                  GpakEcanState_t *pEcanState, GpakDeviceSide_t AorB) {

    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 *Msg;
    ADT_UInt16 RplyLenI16;

    if (MaxDsps <= DspId)
        return GesInvalidDsp;

    if (MaxChannels[DspId] <= (DSP_Word) CHAN_ID (ChannelId))
        return GesInvalidChannel;

    Msg =(ADT_UInt16 *) &MsgBuffer[0];

    Msg[0] = PackBytes (MSG_READ_ECAN_STATE, CORE_ID(ChannelId));
    Msg[1] = (ADT_UInt16) (CHAN_ID (ChannelId) << 8);
    Msg[2] = (ADT_UInt16) AorB;

    RplyLenI16  = ECAN_STATE_LEN_I16 + 2;
                                                                                           
    if (!TransactCmd (DspId, MsgBuffer, 3, MSG_READ_ECAN_STATE_REPLY, RplyLenI16, WORD_ID, (ADT_UInt16) ChannelId))
        return GesDspCommFailure;

    gpakParseEcanState (Msg, pEcanState);

    return GesSuccess;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakSetEcanState - Read an Echo Canceller's training state.
 *
 * FUNCTION
 *  This function writes an Echo Canceller's training state information.
 * INPUTS
 *   DspId -          DSP identifier
 *   ChannelId  -      channel identifier
 *  *pEcanState -     Ecan state variable data
 * 
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
ADT_UInt32                    formatSetEcanState(ADT_UInt16 *Msg, ADT_UInt16 ChannelId, GpakEcanState_t *pEcanState, GpakDeviceSide_t AorB) {

    Msg[0] = PackBytes (MSG_WRITE_ECAN_STATE, CORE_ID(ChannelId));
    Msg[1] = (ADT_UInt16) (CHAN_ID (ChannelId) << 8);
    Msg[2] = (ADT_UInt16)AorB;

    Msg[3] = pEcanState->Size;
    Msg[4] = pEcanState->TapLength;
    Msg[5] = pEcanState->CoefQ;
    Msg[6] = pEcanState->BGCoefQ;
    Msg[7] = pEcanState->NumFirSegments;
    Msg[8] = pEcanState->FirSegmentLen;

    memcpy (&Msg[9], pEcanState->Data, EC_STATE_DATA_LEN * sizeof (ADT_UInt16));

    return  (ECAN_STATE_LEN_I16 + 3);
}

gpakSetEcanStateStat_t        gpakSetEcanState (ADT_UInt32  DspId, ADT_UInt16 ChannelId,
                                                GpakEcanState_t *pEcanState, GpakDeviceSide_t AorB) {

    ADT_UInt32 MsgLenI16;                     /* message length */
    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 *Msg;

    if (MaxDsps <= DspId)
        return SesInvalidDsp;

    if (MaxChannels[DspId] <= (DSP_Word)  CHAN_ID(ChannelId))
        return SesInvalidChannel;

    Msg =(ADT_UInt16 *) &MsgBuffer[0];

    MsgLenI16 = formatSetEcanState(Msg, ChannelId, pEcanState, AorB);

    if (!TransactCmd (DspId, MsgBuffer, MsgLenI16, MSG_WRITE_ECAN_STATE_REPLY, 2, WORD_ID, (ADT_UInt16) ChannelId))
        return SesDspCommFailure;

    return SesSuccess;
}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadChanAecStatus - Read a channel's AEC status.
 *
 * FUNCTION
 *  This function reads a channel's AEC status.
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 */
gpakGetAecStatusStat_t gpakReadChanAecStatus(
    ADT_UInt32 dspId,                   /* DSP identifier */
    ADT_UInt16 channelId,               /* channel identifier */
    gpakAecStatus_t *pAecStatus,        /* pointer to AEC status info */
    GPAK_AecChanReadStat_t *pDspStatus  /* pointer to DSP reply status */
    )
{
    ADT_UInt32 msgLength;                   /* message length (16 bit words) */
    ADT_UInt32 msgBufr[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 *pMsg;                       /* pointer to message */

    /* Default the DSP reply status. */
    memset(pAecStatus, 0, sizeof(gpakAecStatus_t));
    *pDspStatus = aecChanRead_NotEnabled;

    /* Validate the DSP Id and prepare for access. */
    if (dspId >= MaxDsps)
    {
        return GaecsInvalidDsp;
    }
    if (pDspIfBlk[dspId] == 0)
    {
        DSPSync(dspId);
    }

    /* Validate the Channel Id. */
    if (channelId >= MaxChannels[dspId])
    {
        return GaecsInvalidChannel;
    }

    /* Build the request message. */
    pMsg = (ADT_UInt16 *) msgBufr;
    pMsg[0] = MSG_READ_AEC_STATUS << 8;
    pMsg[1] = channelId << 8;

    /* Send the request message and get the reply message. */
    msgLength = TransactCmd(dspId, msgBufr, 2, MSG_READ_AEC_STATUS_REPLY,
                            MSG_BUFFER_ELEMENTS, NO_ID_CHK, 0);

    /* Process the reply message. */
    if (msgLength == 0)
    {
        return GaecsDspCommFailure;
    }
    if (msgLength < 2)
    {
        return GaecsParmError;
    }
    *pDspStatus = (GPAK_AecChanReadStat_t) Byte0(pMsg[1]);
    if (*pDspStatus != aecChanRead_Success)
    {
    	return GaecsParmError;
    }
#if (AEC_LIB_VERSION >= 0x0430)
    if (msgLength < 31)
#else
    if (msgLength < 29)
#endif
    {
        *pDspStatus = aecChanRead_NotEnabled;
        return GaecsParmError;
    }

    // Get the AEC status info from the reply message.
    pAecStatus->txInPowerdBm10 = (ADT_Int16) pMsg[2];
    pAecStatus->txOutPowerdBm10 = (ADT_Int16) pMsg[3];
    pAecStatus->rxInPowerdBm10 = (ADT_Int16) pMsg[4];
    pAecStatus->rxOutPowerdBm10 = (ADT_Int16) pMsg[5];
    pAecStatus->residualPowerdBm10 = (ADT_Int16) pMsg[6];
    pAecStatus->erlDirectdB10 = (ADT_Int16) pMsg[7];
    pAecStatus->erlIndirectdB10 = (ADT_Int16) pMsg[8];
    pAecStatus->erldB10BestEstimate = (ADT_Int16) pMsg[9];
    pAecStatus->worstPerBinERLdB10BestEstimate = (ADT_Int16) pMsg[10];
    pAecStatus->erledB10 = (ADT_Int16) pMsg[11];
    pAecStatus->shortTermERLEdB10 = (ADT_Int16) pMsg[12];
    pAecStatus->shadowERLEdB10 = (ADT_Int16) pMsg[13];
    pAecStatus->rxVADState = (ADT_Int16) pMsg[14];
    pAecStatus->txVADState = (ADT_Int16) pMsg[15];
    pAecStatus->rxVADStateLatched = (ADT_Int16) pMsg[16];
    pAecStatus->currentBulkDelaySamples = (ADT_Int16) pMsg[17];
    pAecStatus->txAttenuationdB10 = (ADT_Int16) pMsg[18];
    pAecStatus->rxAttenuationdB10 = (ADT_Int16) pMsg[19];
    pAecStatus->rxOutAttenuationdB10 = (ADT_Int16) pMsg[20];
    pAecStatus->nlpThresholddB10 = (ADT_Int16) pMsg[21];
    pAecStatus->nlpSaturateFlag = (ADT_Int16) pMsg[22];
    pAecStatus->aecState = (ADT_Int16) pMsg[23];
    pAecStatus->sbcngResidualPowerdBm10 = (ADT_Int16) pMsg[24];
    pAecStatus->sbcngCNGPowerdBm10 = (ADT_Int16) pMsg[25];
    pAecStatus->rxOutAttendB10 = (ADT_Int16) pMsg[26];
    pAecStatus->sbMaxAttendB10 = (ADT_Int16) pMsg[27];
    pAecStatus->sbMaxClipLeveldBm10 = (ADT_Int16) pMsg[28];
#if (AEC_LIB_VERSION >= 0x0430)
    pAecStatus->instantaneousERLEdB100 = (ADT_Int16) pMsg[29];
    pAecStatus->dynamicNLPAggressivenessAdjustdB10 = (ADT_Int16) pMsg[30];
#endif

    return GaecsSuccess;
}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadChanLecStatus - Read a channel's LEC status.
 *
 * FUNCTION
 *  This function reads a channel's LEC status.
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 */
gpakGetLecStatusStat_t gpakReadChanLecStatus(
    ADT_UInt32 dspId,                   /* DSP identifier */
    ADT_UInt16 channelId,               /* channel identifier */
    GpakDeviceSide_t AorB,              /* device side of canceller */
    gpakLecStatus_t *pLecStatus,        /* pointer to LEC status info */
    GPAK_LecChanReadStat_t *pDspStatus  /* pointer to DSP reply status */
    )
{
    ADT_UInt32 msgLength;                   /* message length (16 bit words) */
    ADT_UInt32 msgBufr[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 *pMsg;                       /* pointer to message */

    /* Default the DSP reply status. */
    memset(pLecStatus, 0, sizeof(gpakLecStatus_t));
    *pDspStatus = lecChanRead_NotEnabled;

    /* Validate the DSP Id and prepare for access. */
    if (dspId >= MaxDsps)
    {
        return GlecsInvalidDsp;
    }
    if (pDspIfBlk[dspId] == 0)
    {
        DSPSync(dspId);
    }

    /* Validate the Channel Id. */
    if (channelId >= MaxChannels[dspId])
    {
        return GlecsInvalidChannel;
    }

    /* Build the request message. */
    pMsg = (ADT_UInt16 *) msgBufr;
    pMsg[0] = MSG_READ_LEC_STATUS << 8;
    pMsg[1] = channelId << 8;
    pMsg[2] = AorB;

    /* Send the request message and get the reply message. */
    msgLength = TransactCmd(dspId, msgBufr, 3, MSG_READ_LEC_STATUS_REPLY,
                            MSG_BUFFER_ELEMENTS, NO_ID_CHK, 0);

    /* Process the reply message. */
    if (msgLength == 0)
    {
        return GlecsDspCommFailure;
    }
    if (msgLength < 2)
    {
        return GlecsParmError;
    }
    *pDspStatus = (GPAK_LecChanReadStat_t) Byte0(pMsg[1]);
    if (*pDspStatus != lecChanRead_Success)
    {
    	return GlecsParmError;
    }
    if (msgLength < 15)
    {
        *pDspStatus = lecChanRead_NotEnabled;
        return GlecsParmError;
    }

    // Get the LEC status info from the reply message.
    pLecStatus->AdaptReport = (ADT_Int16) pMsg[3];
    pLecStatus->CrossCorrReport = (ADT_Int16) pMsg[4];
    pLecStatus->G165Status = (ADT_UInt16) pMsg[5];
    pLecStatus->ConvergenceStat = (ADT_Int16) pMsg[6];
    pLecStatus->NLPFlag = (ADT_Int16) pMsg[7];
    pLecStatus->DoubleTalkFlag = (ADT_Int16) pMsg[8];
    pLecStatus->SmartPacketMode = (ADT_Int16) pMsg[9];
    pLecStatus->ERL1 = (ADT_Int16) pMsg[10];
    pLecStatus->ERL2 = (ADT_Int16) pMsg[11];
    pLecStatus->ERLE = (ADT_Int16) pMsg[12];
    pLecStatus->StatusFlags = (ADT_UInt16) pMsg[13];
    pLecStatus->EventFlags = (ADT_UInt16) pMsg[14];

    return GlecsSuccess;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadAgcInputPower - Read short term power
 * 
 * FUNCTION
 *  This function reads a channel's short term power computed by the AGC
 * 
 * INPUTS
 *   DspId,      - DSP identifier 
 *   ChannelId,    - channel identifier 
 *   *pPowerA,   - pointer to store A-side Input Power (dBm) 
 *   *pPowerB,   - pointer to store B-side Input Power (dBm) 
 *   *pStatus    - pointer to DSP status reply  
 * 
 * 
 * RETURNS
 *  Status  code indicating success or a specific error.
 */
gpakAGCReadInputPowerStat_t gpakReadAgcInputPower (ADT_UInt32 DspId,
                                                   ADT_UInt16 ChannelId, ADT_Int16 *pPowerA, ADT_Int16 *pPowerB,
                                                   GPAK_AGCReadStat_t *pStatus) {

    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 *Msg;

    if (MaxDsps <= DspId)
        return AprInvalidDsp;

    if (MaxChannels[DspId] <= (DSP_Word) CHAN_ID(ChannelId))
        return AprInvalidChannel;

    Msg = (ADT_UInt16 *) &MsgBuffer[0];
    Msg[0] = PackBytes (MSG_READ_AGC_POWER, CORE_ID(ChannelId));
    Msg[1] = (ADT_UInt16) (CHAN_ID(ChannelId) << 8);

    if (!TransactCmd (DspId, MsgBuffer, 2, MSG_READ_AGC_POWER_REPLY, 4, BYTE_ID, (ADT_UInt16) CHAN_ID(ChannelId)))
        return AprDspCommFailure;
    
    *pPowerA = Msg[2];
    *pPowerB = Msg[3];

    *pStatus = (GPAK_AGCReadStat_t) Byte0 (Msg[1]);
    if (*pStatus == Ar_Success)  return AprSuccess;
    else                         return AprParmError;

}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakResetChannelStats - Reset a channel's statistics
 * 
 * FUNCTION
 *  This function reset's a channel's statistics specified in the SelectMask
 * 
 * INPUTS
 *   DspId          - DSP Identifier
 *   ChannelId      - channel identifier
 *   SelectMask     - reset bit Mask
 * 
 * RETURNS
 *  Status  code indicating success or a specific error.
 */

gpakResetChannelStat_t      gpakResetChannelStats (ADT_UInt32 DspId, ADT_UInt16 ChannelId, resetCsMask SelectMask) {

    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 *Msg;

    if (MaxDsps <= DspId)
        return RcsInvalidDsp;

    if (MaxChannels[DspId] <= (DSP_Word) CHAN_ID (ChannelId))
        return RcsInvalidChannel;

    Msg =(ADT_UInt16 *) &MsgBuffer[0];

    Msg[0] = PackBytes (MSG_RESET_CHAN_STATS, CORE_ID(ChannelId));
    Msg[1] = (ADT_UInt16) (CHAN_ID(ChannelId) << 8);
    Msg[2] = 0;
    if (SelectMask.PacketStats) Msg[2] = 1;

    if (!TransactCmd (DspId, MsgBuffer, 3, MSG_RESET_CHAN_STATS_REPLY, 2, WORD_ID, (ADT_UInt16) ChannelId))
        return RcsDspCommFailure;

    return RcsSuccess;

}

//}==================================================================================
//===================================================================================
//{                  Channel control APIs
//===================================================================================

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakToneGenerate - Write tone generation paramaters to the DSP.
 *
 * FUNCTION
 *  This function writes the tone generation parameters into DSP memory 
 *  
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
ADT_UInt32 gpakFormatToneGenerator (ADT_UInt16 *Msg, ADT_UInt16 ChannelId, 
                                   GpakToneGenParms_t *tone) {

ADT_UInt32 MsgLenI16;

   /* Format the tone generation message */

    Msg[0] = PackBytes (MSG_TONEGEN, CORE_ID(ChannelId));
    Msg[1] = CHAN_ID(ChannelId);
    Msg[2] = (ADT_UInt16) ( ((tone->ToneType  & 3) << 2) | 
                            ((tone->Device    & 1) << 1) | 
                            (tone->ToneCmd   & 1) );
    Msg[3] = (tone->Frequency[0]*10);
    Msg[4] = (tone->Frequency[1]*10);
    Msg[5] = (tone->Frequency[2]*10);
    Msg[6] = (tone->Frequency[3]*10);
    Msg[7] = (tone->Level[0]*10);
    Msg[8] = tone->OnDuration[0];
    Msg[9] = tone->OffDuration[0];
    MsgLenI16 = 10;

    // 4_2 ----------------------------------
    Msg[10] = (tone->Level[1]*10);
    Msg[11] = (tone->Level[2]*10);
    Msg[12] = (tone->Level[3]*10);
    Msg[13] = tone->OnDuration[1];
    Msg[14] = tone->OnDuration[2];
    Msg[15] = tone->OnDuration[3];
    Msg[16] = tone->OffDuration[1];
    Msg[17] = tone->OffDuration[2];
    Msg[18] = tone->OffDuration[3];
    MsgLenI16 += 9;
    // ----------------------------------

    return MsgLenI16;
}

gpakGenToneStatus_t gpakToneGenerate (ADT_UInt32 DspId, ADT_UInt16 ChannelId, 
               GpakToneGenParms_t *Tone, GPAK_ToneGenStat_t *pStatus) {

    ADT_UInt32  MsgLenI16;                        /* message length */
    ADT_UInt32  MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 *Msg;

    if (MaxDsps <= DspId)
        return TgcInvalidDsp;

    if (pDspIfBlk[DspId] == 0)
       DSPSync (DspId);

    if (MaxChannels[DspId] <= (DSP_Word) CHAN_ID(ChannelId))
        return TgcInvalidChannel;

    Msg    = (ADT_UInt16 *) MsgBuffer;

    MsgLenI16 = gpakFormatToneGenerator (Msg, ChannelId, Tone);

    if (!TransactCmd (DspId, MsgBuffer, MsgLenI16, MSG_TONEGEN_REPLY, MsgLenI16, 
                    BYTE_ID, (ADT_UInt16)CHAN_ID(ChannelId)))
        return TgcDspCommFailure;

    *pStatus = (GPAK_ToneGenStat_t) Byte1 (Msg[2]);
    if (*pStatus == Tg_Success)  return TgcSuccess;
    else                         return TgcParmError;
}

ADT_UInt32 gpakFormatToneGeneratorHighRes (ADT_UInt16 *Msg, ADT_UInt16 ChannelId, 
                                   GpakToneGenParms_t *tone) {

ADT_UInt32 MsgLenI16;

   /* Format the tone generation message */

    Msg[0] = PackBytes (MSG_TONEGEN, CORE_ID(ChannelId));
    Msg[1] = CHAN_ID(ChannelId);
    Msg[2] = (ADT_UInt16) ( ((tone->ToneType  & 3) << 2) | 
                            ((tone->Device    & 1) << 1) | 
                            (tone->ToneCmd   & 1) );
    Msg[3] = tone->Frequency[0];
    Msg[4] = tone->Frequency[1];
    Msg[5] = tone->Frequency[2];
    Msg[6] = tone->Frequency[3];
    Msg[7] = tone->Level[0];
    Msg[8] = tone->OnDuration[0];
    Msg[9] = tone->OffDuration[0];
    MsgLenI16 = 10;

    // 4_2 ----------------------------------
    Msg[10] = tone->Level[1];
    Msg[11] = tone->Level[2];
    Msg[12] = tone->Level[3];
    Msg[13] = tone->OnDuration[1];
    Msg[14] = tone->OnDuration[2];
    Msg[15] = tone->OnDuration[3];
    Msg[16] = tone->OffDuration[1];
    Msg[17] = tone->OffDuration[2];
    Msg[18] = tone->OffDuration[3];
    MsgLenI16 += 9;
    // ----------------------------------

    return MsgLenI16;
}
gpakGenToneStatus_t gpakToneGenerateHighRes (ADT_UInt32 DspId, ADT_UInt16 ChannelId, 
               GpakToneGenParms_t *Tone, GPAK_ToneGenStat_t *pStatus) {

    ADT_UInt32  MsgLenI16;                        /* message length */
    ADT_UInt32  MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 *Msg;

    if (MaxDsps <= DspId)
        return TgcInvalidDsp;

    if (pDspIfBlk[DspId] == 0)
       DSPSync (DspId);

    if (MaxChannels[DspId] <= (DSP_Word) CHAN_ID(ChannelId))
        return TgcInvalidChannel;

    Msg    = (ADT_UInt16 *) MsgBuffer;

    MsgLenI16 = gpakFormatToneGeneratorHighRes (Msg, ChannelId, Tone);

    if (!TransactCmd (DspId, MsgBuffer, MsgLenI16, MSG_TONEGEN_REPLY, MsgLenI16, 
                    BYTE_ID, (ADT_UInt16)CHAN_ID(ChannelId)))
        return TgcDspCommFailure;

    *pStatus = (GPAK_ToneGenStat_t) Byte1 (Msg[2]);
    if (*pStatus == Tg_Success)  return TgcSuccess;
    else                         return TgcParmError;
}
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakDtmfDial - Write Dtmf dialing parameters to the DSP.
 *
 * FUNCTION
 *  This function writes the Dtmf dialing parameters into DSP memory and 
 *  starts dtmf dialing.
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
ADT_UInt32 gpakFormatDtmfDial (ADT_UInt16 *Msg, ADT_UInt16 ChannelId,
                               GpakDtmfParms_t *Parms) {

   /* Format the dtmf dial message */

    Msg[0] = PackBytes (MSG_DTMFDIAL, CORE_ID(ChannelId));
    Msg[1] = CHAN_ID(ChannelId);
    Msg[2] = PackBytes (Parms->NumDigits, Parms->Device);

    Msg[3] = Parms->OnTimeMs;
    Msg[4] = Parms->InterdigitTimeMs;
    Msg[5] = Parms->Level[0];
    Msg[6] = Parms->Level[1];

    Msg[7]  = PackBytes(Parms->DtmfDigits[1],  Parms->DtmfDigits[0]);
    Msg[8]  = PackBytes(Parms->DtmfDigits[3],  Parms->DtmfDigits[2]);
    Msg[9]  = PackBytes(Parms->DtmfDigits[5],  Parms->DtmfDigits[4]);
    Msg[10] = PackBytes(Parms->DtmfDigits[7],  Parms->DtmfDigits[6]);
    Msg[11] = PackBytes(Parms->DtmfDigits[9],  Parms->DtmfDigits[8]);
    Msg[12] = PackBytes(Parms->DtmfDigits[11], Parms->DtmfDigits[10]);
    Msg[13] = PackBytes(Parms->DtmfDigits[13], Parms->DtmfDigits[12]);
    Msg[14] = PackBytes(Parms->DtmfDigits[15], Parms->DtmfDigits[14]);

    return 15;
}

GpakApiStatus_t gpakDtmfDial ( ADT_UInt32 DspId, ADT_UInt16 ChannelId,
                        GpakDtmfParms_t *Parms, GPAK_DtmfDialStat_t *pStatus) {

    ADT_UInt32  MsgLenI16;                        /* message length */
    ADT_UInt32  MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 *Msg;

    if (MaxDsps <= DspId)
        return GpakApiInvalidDsp;

    if (pDspIfBlk[DspId] == 0)
       DSPSync (DspId);

    if (MaxChannels[DspId] <= (DSP_Word) CHAN_ID(ChannelId))
        return GpakApiInvalidChannel;

    Msg    = (ADT_UInt16 *) MsgBuffer;

    MsgLenI16 = gpakFormatDtmfDial (Msg, ChannelId, Parms);

    if (!TransactCmd (DspId, MsgBuffer, MsgLenI16, MSG_DTMFDIAL_REPLY, MsgLenI16, 
                    BYTE_ID, (ADT_UInt16)CHAN_ID(ChannelId)))
        return GpakApiCommFailure;

    *pStatus = (GPAK_DtmfDialStat_t) Byte1 (Msg[2]);
    if (*pStatus == Dd_Success)  return GpakApiSuccess;
    else                         return GpakApiParmError;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakSendCIDPayloadToDsp - Send a transmit payload to a G.PAK channel
 *
 * FUNCTION
 *  This function sends a CID transmit payload to the DSP.
 *
 * INPUTS
 *    DspId             - DSP identifier
 *    ChannelId         - channel identifier
 *    *pPayloadData     - pointer to Payload data
 *    PayloadI8        - length of Payload data (octets)
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
static void gpakPackCidPayload_be (ADT_UInt16 *Msg, ADT_UInt8 *pPayloadData, ADT_UInt16 PaylenI16) {  
    ADT_UInt16 i;

    for (i=0; i<PaylenI16; i++)
        Msg[i] = PackBytes (pPayloadData[2*i+1], pPayloadData[2*i]);
}
static void gpakPackCidPayload_le (ADT_UInt16 *Msg, ADT_UInt8 *pPayloadData, ADT_UInt16 PaylenI16) {  
    ADT_UInt16 i;

    for (i=0; i<PaylenI16; i++)
        Msg[i] = PackBytes (pPayloadData[2*i], pPayloadData[2*i+1]);
}
gpakSendCIDPayloadToDspStat_t gpakSendCIDPayloadToDsp (ADT_UInt32  DspId, ADT_UInt16 ChannelId, 
                                                       GpakCIDTypes CIDType, GpakCIDMsgTypes CIDMsgType,
                                                       ADT_UInt8 *pPayloadData, ADT_UInt16 PayloadI8,
                                                       GpakDeviceSide_t AorBSide, GPAK_TxCIDPayloadStat_t *pStatus) {

    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt32 MsgLenI16;                     /* message length */
    ADT_UInt16 PaylenI16;
    ADT_UInt16 *Msg;

    /* Make sure the DSP Id is valid. */
    if (DspId >= MaxDsps)
        return (ScpInvalidDsp);

    /* Make sure the Channel Id is valid. */
    if (MaxChannels[DspId] <= (DSP_Word) CHAN_ID(ChannelId))
        return (ScpInvalidChannel);

    Msg =(ADT_UInt16 *) &MsgBuffer[0];
    Msg[0] = PackBytes (MSG_WRITE_TXCID_DATA, CORE_ID(ChannelId));
    Msg[1] = (ADT_UInt16) ((CHAN_ID(ChannelId) << 8) | CIDType) ;
    Msg[2] = PayloadI8;
    Msg[3] = PackBytes (CIDMsgType, AorBSide);

    PaylenI16 = (PayloadI8+1)/2;
    MsgLenI16 = 4 + PaylenI16; 

    if (BytesToTxUnits(PayloadI8+8) > MSG_BUFFER_SIZE)
        return ScpInvalidPaylen;

    bytesToDSP (&Msg[4], pPayloadData, PaylenI16);

    if (!TransactCmd (DspId, MsgBuffer, MsgLenI16, MSG_WRITE_TXCID_DATA_REPLY, 2, BYTE_ID, (ADT_UInt16) CHAN_ID(ChannelId)))
        return ScpDspCommFailure;

    /* Return success or failure  */
    *pStatus = (GPAK_TxCIDPayloadStat_t) Byte0 (Msg[1]);
    if (*pStatus == Scp_Success)  return ScpSuccess;
    else                         return ScpParmError;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
* This function provides the mechanism for the host to insert an RTP tone event payload 
* into a channel's RTP stream with a proper RTP sequence number and timestamp.  
* The tone event will be sent over the channel's RTP stream with the payload type set to 
* the TonePyldType specified for the channel by the call to the gpakSendRTPMsg API.  
* The tone event payload must match the format for tone payloads specified in RFC 2833. 
*/

GpakApiStatus_t gpakRTPInsertTone (ADT_UInt32 DspId, 
                    ADT_UInt16 ChannelId, 
                    ADT_UInt16 TonePyldI8,
               ADT_UInt16 *TonePyld, 
               GPAK_InsertEventStat_t * dspStatus) {

    ADT_UInt32  MsgLenI16;                        /* message length */
    ADT_UInt32  MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 *Msg, PaylenI16, i;

    if (MaxDsps <= DspId)
        return TgcInvalidDsp;

    if (pDspIfBlk[DspId] == 0)
       DSPSync (DspId);

    if (MaxChannels[DspId] <= (DSP_Word) CHAN_ID(ChannelId))
        return TgcInvalidChannel;

    Msg    = (ADT_UInt16 *) MsgBuffer;

    Msg[0] = PackBytes (MSG_INRTPTONEPKTGEN, CORE_ID(ChannelId));
    Msg[1] = CHAN_ID(ChannelId);
    Msg[2] = TonePyldI8;

    PaylenI16 = (TonePyldI8+1)/2;
    MsgLenI16 = 3 + PaylenI16; 

    if (BytesToTxUnits(TonePyldI8+8) > MSG_BUFFER_SIZE)
        return GpakApiInvalidPaylen;

    // Swap to little endian before transfer.  Transfer will cause swap backe to big endian.
    for (i=0; i<PaylenI16; i++) {
#if ((DSP_WORD_I8 == 4) || (DSP_WORD_I8 == 2))
       if (HostBigEndian) Msg[3+1] = swapI16 (TonePyld[i]);
       else
#endif
       Msg[3+i] = TonePyld[i];
    }

    if (!TransactCmd (DspId, MsgBuffer, MsgLenI16, MSG_INRTPTONEPKTGEN_REPLY, MsgLenI16, 
                    WORD_ID, ChannelId))
        return TgcDspCommFailure;

    *dspStatus = (GPAK_InsertEventStat_t) Byte1 (Msg[2]);
    if (*dspStatus == Ti_Success)  return TgcSuccess;
    else                           return TgcParmError;
}
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakFormatTonePktGenerator - Write tone generation pkt info to the DSP.
 *
 * FUNCTION
 *  This function writes the tone pkt generation parameters into DSP memory 
 *  
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
ADT_UInt32 gpakFormatTonePktGenerator (ADT_UInt16 *Msg, ADT_UInt16 ChannelId, 
                                   GpakTonePktGenParms_t *tone) {

ADT_UInt32 MsgLenI16;

   /* Format the tone generation message */

    Msg[0] = PackBytes (MSG_TONEPKTGEN, CORE_ID(ChannelId));
    Msg[1] = CHAN_ID(ChannelId);
    Msg[2] = (ADT_UInt16) ( ((tone->ToneCode  & 0xFF) << 8) | 
                            ((tone->Device    & 1) << 1) | 
                            (tone->ToneCmd   & 1) );
    Msg[3] = tone->Frequency[0];
    Msg[4] = tone->Frequency[1];
    Msg[5] = tone->Level;
    Msg[6] = tone->Duration;
  
    MsgLenI16 = 7;


    return MsgLenI16;
}

gpakGenToneStatus_t gpakTonePktGenerate (ADT_UInt32 DspId, ADT_UInt16 ChannelId, 
               GpakTonePktGenParms_t *Tone, GPAK_ToneGenStat_t *pStatus) {

    ADT_UInt32  MsgLenI16;                        /* message length */
    ADT_UInt32  MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 *Msg;

    if (MaxDsps <= DspId)
        return TgcInvalidDsp;

    if (pDspIfBlk[DspId] == 0)
       DSPSync (DspId);

    if (MaxChannels[DspId] <= (DSP_Word) CHAN_ID(ChannelId))
        return TgcInvalidChannel;

    Msg    = (ADT_UInt16 *) MsgBuffer;

    MsgLenI16 = gpakFormatTonePktGenerator (Msg, ChannelId, Tone);

    if (!TransactCmd (DspId, MsgBuffer, MsgLenI16, MSG_TONEPKTGEN_REPLY, MsgLenI16, 
                    WORD_ID, ChannelId))
        return TgcDspCommFailure;

    *pStatus = (GPAK_ToneGenStat_t) Byte1 (Msg[2]);
    if (*pStatus == Tg_Success)  return TgcSuccess;
    else                         return TgcParmError;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakAlgControl - Control an Algorithm.
 *
 * FUNCTION
 *  This function controls an Algorithm
 * 
 * INPUTS
 *   DspId,          - DSP identifier 
 *   ChannelId,        - channel identifier 
 *   ControlCode,    - algorithm control code 
 *   DeactTonetype,  - tone detector to deactivate 
 *   ActTonetype,    - tone detector to activate 
 *   AorBSide,       - A or B device side 
 *   NLPsetting,     - NLP value 
 *   *pStatus        - pointer to return status 
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
ADT_UInt32                    formatAlgControl (ADT_UInt16 *Msg, ADT_UInt16 ChannelId,
                                                GpakAlgCtrl_t ControlCode, GpakToneTypes DeactTonetype,
                                                GpakToneTypes ActTonetype, GpakDeviceSide_t AorBSide, ADT_UInt16 NLPsetting,
                                               ADT_Int16 GaindB, GpakCodecs CodecType) {

    Msg[0] = PackBytes (MSG_ALG_CONTROL, CORE_ID(ChannelId));
    Msg[1] = (ADT_UInt16) (CHAN_ID (ChannelId) << 8);
    Msg[2] = ((ControlCode & 0xff)      |
              ((NLPsetting & 0xf) << 8) |
              ((AorBSide & 0x1) << 12));
    Msg[3] = DeactTonetype;
    Msg[4] = ActTonetype;
    Msg[5] = GaindB;
    Msg[6] = CodecType;
    return 7;
}

gpakAlgControlStat_t          gpakAlgControl (ADT_UInt32 DspId, ADT_UInt16 ChannelId,
                                               GpakAlgCtrl_t ControlCode, GpakToneTypes DeactTonetype,
                                               GpakToneTypes ActTonetype, GpakDeviceSide_t AorBSide, ADT_UInt16 NLPsetting,
                                               ADT_Int16 GaindB, GpakCodecs CodecType, GPAK_AlgControlStat_t *pStatus) {

    ADT_UInt32 MsgLenI16;                     /* message length */
    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 *Msg;

    if (MaxDsps <= DspId)
        return AcInvalidDsp;

    if (MaxChannels[DspId] <= (DSP_Word) CHAN_ID(ChannelId))
        return AcInvalidChannel;

    Msg =(ADT_UInt16 *) &MsgBuffer[0];

    MsgLenI16 = formatAlgControl(Msg, ChannelId, ControlCode, DeactTonetype,
                     ActTonetype, AorBSide, NLPsetting, GaindB, CodecType);  

    if (!TransactCmd (DspId, MsgBuffer, MsgLenI16, MSG_ALG_CONTROL_REPLY, 2, BYTE_ID, (ADT_UInt16) CHAN_ID(ChannelId)))
        return AcDspCommFailure;

    /* Return success or failure  */
    *pStatus = (GPAK_AlgControlStat_t) Byte0 (Msg[1]);
    if (*pStatus == Ac_Success)  return AcSuccess;
    else                         return AcParmError;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *   gpakSendPlayRecordMsg 
 *
 * FUNCTION
 *  This function is used to issue start/stop voice recording and start
 *  voice playback commands to the DSP.
 *
 *
 *
 *  Notes: The start playback command requires that the payload buffer 
 *         (after a reserved 32 byte header) is filled with voice data in the 
 *         format specified by the recording mode. 
 *
 *         Stop tone = tdsDtmfDigit<x> or tdsNoToneDetected  x=0..9
 *

 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
ADT_UInt32 gpakFormatPlayRecordMsg (ADT_UInt16 *Msg, ADT_UInt16 ChanId,
                GpakDeviceSide_t Device,       GpakPlayRecordCmd_t Cmd,  
                DSP_Address      BuffAddr,     ADT_UInt32 BuffLen, 
                GpakPlayRecordMode_t RecMode,  GpakToneCodes_t StopTone) {
   
   Msg[0] = PackBytes (MSG_PLAY_RECORD, (int) Cmd);
   Msg[1] = ChanId;
   Msg[2] = HighWord (BuffAddr);
   Msg[3] = LowWord  (BuffAddr);
   Msg[4] = HighWord (BuffLen);
   Msg[5] = LowWord  (BuffLen);
   Msg[6] = PackBytes (RecMode, StopTone);
   Msg[7] = PackBytes (0, Device & 3);
   return 8;
}

gpakPlayRecordStatus_t gpakSendPlayRecordMsg (ADT_UInt32 DspId, ADT_UInt16 ChanId, 
                                              GpakDeviceSide_t Device,  GpakPlayRecordCmd_t Cmd,  DSP_Address  BuffAddr,
                                              ADT_UInt32 BuffLen,       GpakPlayRecordMode_t RecMode,  GpakToneCodes_t StopTone, GPAK_PlayRecordStat_t *pStatus) {

    ADT_UInt32  MsgLenI16;                        /* message length */
    ADT_UInt32  MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 *Msg;

    if (MaxDsps <= DspId)
        return PrsInvalidDsp;

    if (pDspIfBlk[DspId] == 0)
       DSPSync (DspId);

    if (ChanId >= MaxChannels[DspId])
        return PrsInvalidChannel;

    Msg    = (ADT_UInt16 *) MsgBuffer;

    MsgLenI16 = gpakFormatPlayRecordMsg (Msg, ChanId, Device, Cmd, BuffAddr,
                                    BuffLen, RecMode, StopTone);

    if (!TransactCmd (DspId, MsgBuffer, MsgLenI16, MSG_PLAY_RECORD_RESPONSE, 3, 
                     WORD_ID, ChanId))
        return PrsDspCommFailure;

    *pStatus = (GPAK_PlayRecordStat_t) Byte1 (Msg[2]);
    if (*pStatus == PrSuccess)  return PrsSuccess;
    else                        return PrsParmError;
}
//}===================================================================================
//===================================================================================
//{                  Packet processing APIs
//===================================================================================

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakSendPayloadToDsp - Send a Payload to a DSP's Channel.
 *
 * FUNCTION
 *  Writes a Payload message into DSP memory for a specified channel.
 *
 * Inputs
 *   DspId      - Dsp identifier 
 * ChannelId    - Channel identifier
 * PayloadClass - Audio, silence, tone, ...
 * PayloadType  - G711, G729, ...
 * pData        - Pointer to payload data.  MUST be aligned on DSP word boundary
 * PayloadI8    - Byte count of payload data.
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
gpakSendPayloadStatus_t gpakSendPayloadToDsp (ADT_UInt32 DspId, ADT_UInt16 ChannelId,
    GpakPayloadClass PayloadClass, ADT_Int16 PayloadType, ADT_UInt8 *pData,
    ADT_UInt16 PayloadI8) {


    PktHdr_t Hdr;

    CircBufr_t cBuff;
 
    DSP_Address CircBufrAddr;      /* address of Pkt In buffer info */
    DSP_Word  I16Free, I16Ready, HdrI16Sz, Pyld16Sz;


    if (MaxDsps <= DspId)
        return SpsInvalidDsp;

    if (pDspIfBlk[DspId] == 0)
       DSPSync (DspId);

    if (MaxChannels[DspId] < ChannelId) {
        return SpsInvalidChannel;
    }

   // Read circular buffer structure into host memory
    gpakLockAccess (DspId);
    CircBufrAddr  = *getPktOutBufrAddr (DspId, ChannelId);
    ReadCircStruct (DspId, CircBufrAddr, &cBuff, &I16Free, &I16Ready);

   // Verify sufficient room with padding for non-word transfers
    HdrI16Sz = BytesToCircUnits (byteSize (PktHdr_t)); // Bytes to circular buffer units
    Pyld16Sz = BytesToCircUnits (PayloadI8);           // Bytes to circular buffer units
    if (I16Free < HdrI16Sz + Pyld16Sz)  {
        gpakUnlockAccess (DspId);
        return SpsBufferFull;
    }

    
    Hdr.ChannelId       = ChannelId;
    Hdr.PayloadClass    = (ADT_UInt16) PayloadClass;
    Hdr.PayloadType     = PayloadType;
    Hdr.OctetsInPayload = PayloadI8;
    Hdr.TimeStamp       = 0;


    // Write header and payload
#if (DSP_WORD_I8 == 4)
    if (HostBigEndian)
        gpakI16Swap ((ADT_UInt32 *) &Hdr, 8);
#endif

    WriteCircBuffer (DspId, &cBuff, (DSP_Word *) &Hdr,  HdrI16Sz,  0);
    WriteCircBufferNoSwap (DspId, &cBuff, (DSP_Word *) pData, Pyld16Sz, CircBufrAddr);

    gpakUnlockAccess (DspId);

    return SpsSuccess;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakGetPayloadFromDsp - Read a Payload from a DSP's Channel.
 *
 * FUNCTION
 *  Reads a Payload message from DSP memory for a specified channel.
 *
 *  Note: If the buffer is too short for the data, a failure indication is returned
 *        and the payload remains unread.
 * 
 * Inputs
 *     DspId      - Dsp identifier 
 *   ChannelId    - Channel identifier
 * 
 * Outputs
 *   PayloadClass - Audio, silence, tone, ...
 *   PayloadType  - G711, G729, ...
 *   pPayloadData - Pointer to payload data.  MUST be aligned on 32-bit boundary
 *
 * Updates
 *     PayloadI8  - Before. Byte count of payload buffer size
 *                    After,  Byte count of actual payload
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */

gpakGetPayloadStatus_t gpakGetPayloadFromDsp (ADT_UInt32 DspId, ADT_UInt16 ChannelId,
                GpakPayloadClass *pPayloadClass, ADT_Int16  *pPayloadType,  
                ADT_UInt8 *pPayloadData,         ADT_UInt16 *pPayloadBytes,
            ADT_UInt32 *pTimeStamp) {

    Packet_t Pkt;
    CircBufr_t cBuff;

    DSP_Address CircBufrAddr;
    DSP_Word    I16Ready, I16Free, HdrI16Sz, Pyld16Sz;

    if (MaxDsps <= DspId)
        return GpsInvalidDsp;

    if (pDspIfBlk[DspId] == 0)
       DSPSync (DspId);

    if (MaxChannels[DspId] < ChannelId) {
        return GpsInvalidChannel;
    }

   // Read circular buffer structure into host memory
    gpakLockAccess (DspId);
    CircBufrAddr  = *getPktInBufrAddr (DspId, ChannelId);
    ReadCircStruct (DspId, CircBufrAddr, &cBuff, &I16Free, &I16Ready);

    HdrI16Sz  = BytesToCircUnits (byteSize (PktHdr_t)); // Bytes to transfer units
    if (I16Ready < HdrI16Sz)  {
      if (I16Ready) PurgeCircStruct (DspId, CircBufrAddr, &cBuff);
        gpakUnlockAccess (DspId);
        return GpsNoPayload;
    }

    // Read payload header from DSP
    ReadCircBuffer (DspId, &cBuff, (DSP_Word *) &Pkt.Hdr, HdrI16Sz, 0);

#if (DSP_WORD_I8 == 4)
    if (HostBigEndian)
        gpakI16Swap ((ADT_UInt32 *) &Pkt.Hdr, 8);
#endif

    Pyld16Sz = BytesToCircUnits (Pkt.Hdr.OctetsInPayload);       // Bytes to transfer units
    if (I16Ready < HdrI16Sz + Pyld16Sz) {
       PurgeCircStruct (DspId, CircBufrAddr, &cBuff);
       gpakUnlockAccess (DspId);
       return GpsNoPayload;
    }

    if (*pPayloadBytes < Pkt.Hdr.OctetsInPayload) {
       PurgeCircStruct (DspId, CircBufrAddr, &cBuff);
       gpakUnlockAccess (DspId);
       return GpsBufferTooSmall;
    }

    // Read payload data from DSP
    if (Pyld16Sz != 0)
       ReadCircBufferNoSwap (DspId, &cBuff, (DSP_Word *) pPayloadData, Pyld16Sz, CircBufrAddr);

    *pPayloadClass = (GpakPayloadClass) Pkt.Hdr.PayloadClass;
    *pPayloadType  = Pkt.Hdr.PayloadType;
    *pPayloadBytes = Pkt.Hdr.OctetsInPayload;
    *pTimeStamp    = Pkt.Hdr.TimeStamp;

    gpakUnlockAccess (DspId);
    return GpsSuccess;
}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakSendPacketToDsp - Send an RTP packet to a DSP's Channel.
 *
 * FUNCTION
 *  Writes a Payload message into DSP memory for a specified channel.
 *
 * Inputs
 *   DspId      - Dsp identifier 
 * ChannelId    - Channel identifier
 * pData        - Pointer to RTP packet data.  MUST be aligned on DSP word boundary
 * PacketI8     - Byte count of RTP packet data.
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
typedef struct RTPHdr_t {
   ADT_UInt16 PktI8;
   ADT_UInt16 ChannelId;
} RTPHdr_t;

gpakSendPayloadStatus_t gpakSendPacketToDsp (ADT_UInt32 DspId, ADT_UInt16 ChannelId,
                                             ADT_UInt8 *pData,  ADT_UInt16 PacketI8) {


    RTPHdr_t Hdr;

    CircBufr_t cBuff;
 
    DSP_Address CircBufrAddr;      /* address of Pkt In buffer info */
    DSP_Word  I16Free, I16Ready, HdrI16Sz, PktI16Sz;


    if (MaxDsps <= DspId)
        return SpsInvalidDsp;

    if (pDspIfBlk[DspId] == 0)
       DSPSync (DspId);

    if (MaxChannels[DspId] < (DSP_Word) CHAN_ID(ChannelId)) {
        return SpsInvalidChannel;
    }

   // Read circular buffer structure into host memory
    CircBufrAddr  = *getPktOutBufrAddr (DspId, 0);
    gpakLockAccess (DspId);
    ReadCircStruct (DspId, CircBufrAddr, &cBuff, &I16Free, &I16Ready);

   // Verify sufficient room with padding for non-word transfers
    HdrI16Sz =  BytesToCircUnits (byteSize (RTPHdr_t)); // Bytes to circular buffer units
    PktI16Sz =  BytesToCircUnits (PacketI8);       // Bytes to circular buffer units
    if (I16Free < HdrI16Sz + PktI16Sz)  {
        gpakUnlockAccess (DspId);
        return SpsBufferFull;
    }
    
    Hdr.ChannelId  = ChannelId;
    Hdr.PktI8      = PacketI8;

    // Write header and payload
#if (DSP_WORD_I8 == 4)
    if (HostBigEndian)
        gpakI16Swap ((ADT_UInt32 *) &Hdr, 4);
#endif

    WriteCircBuffer (DspId, &cBuff, (DSP_Word *) &Hdr,  HdrI16Sz,  0);
    WriteCircBufferNoSwap (DspId, &cBuff, (DSP_Word *) pData, PktI16Sz, CircBufrAddr);

    gpakUnlockAccess (DspId);

    return SpsSuccess;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakGetPacketFromDsp - Read an RTP Packet from a DSP's Channel.
 *
 * FUNCTION
 *  Reads next available RTP packet from DSP memory.
 *
 *  Note: If the buffer is too short for the data, a failure indication is returned
 *        and all remaining packets are purged.
 * 
 * Inputs
 *     DspId      - Dsp identifier 
 * 
 * Outputs
 *   pChannelId   - Channel identifier
 *   pData        - Pointer to RTP packet data.  MUST be aligned on 32-bit boundary
 *
 * Updates
 *     PacketI8    - Before. Byte count of payload buffer size
 *                   After,  Byte count of actual payload
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */

gpakGetPayloadStatus_t gpakGetPacketFromDsp (ADT_UInt32 DspId, ADT_UInt16 *pChannelId,
                                             ADT_UInt8 *pData,  ADT_UInt16 *pPacketI8) {

    RTPHdr_t Hdr;
    CircBufr_t cBuff;

    DSP_Address CircBufrAddr;
    DSP_Word    I16Ready, I16Free, HdrI16Sz, PktI16Sz;

    if (MaxDsps <= DspId)
        return GpsInvalidDsp;


    if (pDspIfBlk[DspId] == 0)
       DSPSync (DspId);


    // Read circular buffer structure into host memory
    CircBufrAddr  = *getPktInBufrAddr (DspId, 0);
    gpakLockAccess (DspId);
    ReadCircStruct (DspId, CircBufrAddr, &cBuff, &I16Free, &I16Ready);

    HdrI16Sz  = BytesToCircUnits (byteSize (RTPHdr_t)); // Bytes to transfer units
    if (I16Ready < HdrI16Sz)  {
       if (I16Ready) PurgeCircStruct (DspId, CircBufrAddr, &cBuff);
       gpakUnlockAccess (DspId);
       return GpsNoPayload;
    }

    // Read RTP header from DSP
    ReadCircBuffer (DspId, &cBuff, (DSP_Word *) &Hdr, HdrI16Sz, 0);
#if (DSP_WORD_I8 == 4)
    if (HostBigEndian)
        gpakI16Swap ((ADT_UInt32 *) &Hdr, 4);
#endif

    PktI16Sz = BytesToCircUnits (Hdr.PktI8);       // Bytes to transfer units
    if (I16Ready < HdrI16Sz + PktI16Sz) {
       PurgeCircStruct (DspId, CircBufrAddr, &cBuff);
       gpakUnlockAccess (DspId);
       return GpsNoPayload;
    }

    if (*pPacketI8 < Hdr.PktI8) {
       PurgeCircStruct (DspId, CircBufrAddr, &cBuff);
       gpakUnlockAccess (DspId);
       return GpsBufferTooSmall;
    }

    // Read payload data from DSP
    if (PktI16Sz != 0)
       ReadCircBufferNoSwap (DspId, &cBuff, (DSP_Word *) pData, PktI16Sz, CircBufrAddr);

    *pChannelId = Hdr.ChannelId;
    *pPacketI8  = Hdr.PktI8;

    gpakUnlockAccess (DspId);
    return GpsSuccess;
}


//}===================================================================================
//===================================================================================
//{                  DSP download APIs
//===================================================================================
static void transferDlBlock_be (ADT_UInt32 DspId, DSP_Address Address, unsigned int NumWords) {
    ADT_UInt8 *DlByteBufr = (ADT_UInt8 *)DlByteBufr_;

    // Big endian mode does the swap in the Write and Read routines
    gpakWriteDsp (DspId, Address, NumWords, (DSP_Word *) DlByteBufr);
    gpakReadDsp  (DspId, Address, NumWords, (DSP_Word *) DlWordBufr);
}
static void transferDlBlock_le (ADT_UInt32 DspId, DSP_Address Address, unsigned int NumWords) {

   DSP_Word word,  *Buff;
   ADT_UInt8 *DlByteBufr = (ADT_UInt8 *)DlByteBufr_;
   unsigned int i;

    // Manually swaps the download data if little endian host
    for (i = 0, Buff = (DSP_Word *) &DlByteBufr[0]; i < NumWords; i++, Buff++) {
       word = *Buff;

      // Convert ENDIANESS
       DlWordBufr[i]  = EndianSwap(word);
        
    }
    gpakWriteDsp (DspId, Address, NumWords, (DSP_Word *) DlWordBufr);
    gpakReadDsp  (DspId, Address, NumWords, (DSP_Word *) DlByteBufr);
}
/*
 * gpakDownloadDsp - Download a DSP's Program and initialized Data memory.
 *
 * FUNCTION
 *  This function reads a DSP's Program and Data memory image from the
 *  specified file and writes the image to the DSP's memory.
 *
 * Inputs
 *    DspId  - Dsp to receive download
 *   FileId  - File Handle
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
gpakDownloadStatus_t          gpakDownloadDsp (ADT_UInt32 DspId, void *FileId) {

   gpakDownloadStatus_t RetStatus;     /* function return status */
   DSP_Address Address;                /* DSP address */
   DSP_Word BlockSize;
   DSP_Word word;
   ADT_UInt8 *DlByteBufr = (ADT_UInt8 *)DlByteBufr_;

   ADT_UInt32 WordCount;
   int          BytesRead, totalBytes;
   unsigned int NumWords;              /* number of words to read/write */
   unsigned int stat;

   if (MaxDsps <= DspId)
      return GdlInvalidDsp;

   gpakLockAccess(DspId);

   // Initialize DSP's status variables
   totalBytes = 0;
   DSPError[DspId]     = DSPSuccess;
   MaxChannels[DspId]  = 0;
   MaxCmdMsgLen[DspId] = 0;
   pEventFifoAddress[DspId] = 0;
   pDspIfBlk[DspId]    = 0;

   memset (getPktInBufrAddr (DspId, 0), 0, MaxChannelAlloc * sizeof (DSP_Address));
   memset (getPktOutBufrAddr(DspId, 0), 0, MaxChannelAlloc * sizeof (DSP_Address));

   // Disable DSP access
   word = 0;
   gpakWriteDsp (DspId, ifBlkAddress, 1, &word);

   RetStatus = GdlSuccess;
   while (RetStatus == GdlSuccess)  {

      /* Read a record header from the file. */
      BytesRead = gpakReadFile (FileId, DlByteBufr, DL_HDR_SIZE);
      totalBytes += BytesRead;
      if (BytesRead == -1)  {
         RetStatus = GdlFileReadError;
         break;
      }
      if (BytesRead != DL_HDR_SIZE)  {
         RetStatus = GdlInvalidFile;
         break;
      }

      /* Check for the End Of File record. */
      if (DlByteBufr[0] == (ADT_UInt8) 0xFF)
         break;

#if (DSP_DLADDR_SIZE_I8 == 3)
      /* Verify the record is for a valid memory type. */
      if ((DlByteBufr[0] != 0x00) && (DlByteBufr[0] != 0x01))  {
         RetStatus = GdlInvalidFile;
         break;
      }
      // 24-bit addressing on 54 and 55
      Address = (DSP_Address)(((ADT_UInt32) DlByteBufr[1] << 16) & 0x00ff0000) |       
                            (((ADT_UInt32) DlByteBufr[2] << 8)  & 0x0000ff00) |
                            ( (ADT_UInt32) DlByteBufr[3]        & 0x000000ff);

      WordCount =  (ADT_UInt16) (((ADT_UInt16) DlByteBufr[4] << 8) & 0xff00) |
                               ( (ADT_UInt16) DlByteBufr[5]       & 0x00ff);

#elif (DSP_DLADDR_SIZE_I8 == 4)
      /* Verify the record is for a valid memory type. */
      if (DlByteBufr[0] != 0x02)  {
         RetStatus = GdlInvalidFile;
         break;
      }

      Address = (DSP_Address) (((ADT_UInt32) DlByteBufr[1] << 24) & 0xff000000) |
                             (((ADT_UInt32) DlByteBufr[2] << 16) & 0x00ff0000) |       
                             (((ADT_UInt32) DlByteBufr[3] << 8)  & 0x0000ff00) |
                             ( (ADT_UInt32) DlByteBufr[4]        & 0x000000ff);

      WordCount =  (ADT_UInt16) (((ADT_UInt16) DlByteBufr[5] << 8) & 0xff00) |
                               ( (ADT_UInt16) DlByteBufr[6]       & 0x00ff);


#endif

      /* Read a block of words at a time from the file and write to the DSP's memory .*/
      while (WordCount != 0)      {
         if (WordCount < DownloadI8/sizeof (DSP_Word))
             NumWords = WordCount;
         else
             NumWords = DownloadI8/sizeof (DSP_Word);

         WordCount -= NumWords;
         BlockSize  = NumWords * sizeof (DSP_Word);
         BytesRead = gpakReadFile (FileId, DlByteBufr, BlockSize);
         totalBytes += BytesRead;
         if (BytesRead == -1)  {
             RetStatus = GdlFileReadError;
             break;
         }
         if (BytesRead != (int) BlockSize) {
             RetStatus = GdlInvalidFile;
             break;
         }

         if (HostBigEndian)
             transferDlBlock_be (DspId, Address, NumWords);
         else
             transferDlBlock_le (DspId, Address, NumWords);

         stat = memcmp (DlWordBufr, DlByteBufr, BlockSize);
         if (stat != 0) {
            RetStatus = GdlFileReadError;
            printf ("Address failure: %x\n", (unsigned int)Address);
         }

         Address += BytesToMAUs (BlockSize);
      }
   }

   /* Unlock access to the DSP. */
   gpakUnlockAccess (DspId);

   /* Return with an indication of success or failure. */
   return RetStatus;
}

//}===================================================================================
//===================================================================================
//{                  Test generation APIs
//===================================================================================

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakTestMode - Configure/perform a DSP's Test mode.
 *
 * FUNCTION
 *  Configures or performs a DSP's Test mode.
 *
 * Inputs
 *   DspId        - Dsp identifier 
 *   TestModeId   - Test identifier
 *   pTestParm    - pointer to TestMode-specific Test parameters
 *
 * Outputs
 *    pRespData - Response value pointer
 *    pStatus   - test mode status from DSP
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
ADT_UInt32 formatTestModeMsg(ADT_UInt16 *Msg, GpakTestMode TestModeId, 
                                       GpakTestData_t  *pTestParm, ADT_UInt16 buflenI16, ADT_UInt32 *ReplyLenI16) {

    ADT_UInt32 MsgLenI16, CustLenI16, i;
    ADT_UInt16 *pCustMsg;

   *ReplyLenI16 = 1;
    Msg[0] = MSG_TEST_MODE << 8;
    Msg[1] = (ADT_UInt16) TestModeId;

    switch (TestModeId) {
     case TdmFixedValue:
         Msg[2] = pTestParm->u.fixedVal.ChannelId;
         Msg[3] = pTestParm->u.fixedVal.DeviceSide;
         Msg[5] = pTestParm->u.fixedVal.Value;
         Msg[6] = pTestParm->u.fixedVal.State;
         MsgLenI16 = 7;
         break;

     case TdmLoopback:
         Msg[2] = pTestParm->u.tdmLoop.PcmOutPort;
         Msg[3] = pTestParm->u.tdmLoop.State;
         MsgLenI16 = 4;
         break;

     case CustomVarLen:
         MsgLenI16  = 2;
         pCustMsg = (ADT_UInt16 *)pTestParm;
         // Custom Variable length messages: pTestParm points to an array of
         // I16 elements pre-filled with the custom payload.
         // Custom Variable Length Message has following format:
         // Msg[0] == MSG_TEST_MODE << 8
         // Msg[1] == TestModeId
         // Msg[2] == pCustMsg[0] == custom Test identifier
         // Msg[3] == pCustMsg[1] == num I16 elements in the custom message, starting from pTestParam[0]
         // Msg[4] == pCustMsg[2] == expected number of I16 elements in the message response (excluding the 1st three I16 elements of a standard response)
         // Msg[5]... pCustMsg[3] ...remainder of the custom payload
         CustLenI16 = pCustMsg[1];
         *ReplyLenI16 = (ADT_UInt32) pCustMsg[2];
         if ((CustLenI16 + MsgLenI16) > buflenI16)
             return 0;
         for (i=0; i<CustLenI16; i++) {
             Msg[2+i] = pCustMsg[i];
         }
         MsgLenI16 += CustLenI16;
         break;
                 
     default:    
         Msg[2] = pTestParm->u.TestParm;
         MsgLenI16 = 3;
         break;
    };

    return MsgLenI16;
}
#define MAX_CMD_MSG_BYTES   2052  
gpakTestStatus_t gpakTestMode (ADT_UInt32 DspId, GpakTestMode TestModeId,
                               GpakTestData_t  *pTestParm, ADT_UInt16 *pRespData,
                               GPAK_TestStat_t *pStatus) {

    ADT_UInt32  MsgBuffer[MAX_CMD_MSG_BYTES/4];    /* message buffer */
    ADT_UInt16 *Msg;
    ADT_UInt32 MsgLenI16;
    ADT_UInt32 ReplyLenI16;
    ADT_UInt32 i;


    if (MaxDsps <= DspId)
        return CtmInvalidDsp;

    Msg    = (ADT_UInt16 *) MsgBuffer;

    MsgLenI16 = formatTestModeMsg(Msg, TestModeId, pTestParm, sizeof(MsgBuffer)/sizeof(ADT_UInt16), &ReplyLenI16);

    if ((ReplyLenI16+3) > (MAX_CMD_MSG_BYTES / 2)) return CtmCustomMsgError;
      
    if (MsgLenI16 == 0) return CtmCustomMsgError;

    ReplyLenI16 = TransactCmd (DspId, MsgBuffer, MsgLenI16, MSG_TEST_REPLY, MAX_CMD_MSG_BYTES / 2,
                    WORD_ID, (ADT_UInt16) TestModeId);
    if (ReplyLenI16 == 0)
       return CtmDspCommFailure;

    for (i=0; i<ReplyLenI16-3; i++)
       pRespData[i] = Msg[3+i];

    *pStatus = (GPAK_TestStat_t) Msg[2];
    if (*pStatus == Tm_Success)
        return CtmSuccess;
    else
        return CtmParmError;
}

#ifdef DSP_TDMCFG_ADDRESS
GpakApiStatus_t gpakSetTDMMode (ADT_Int32 DspId, ADT_UInt16 samplingRatekHz) {
   struct {
      ADT_UInt16 samplingRatekHz;
   } TDMParams;

   if (MaxDsps <= (unsigned int) DspId)
        return IPInvalidDSP;

   if (HostBigEndian) {
      TDMParams.samplingRatekHz = swapI16(samplingRatekHz);

      /* Write the TDM parameters to the DSP. */
      gpakWriteDspNoSwap (DspId, DSP_TDMCFG_ADDRESS, BytesToTxUnits (sizeof TDMParams), (void *) &TDMParams);

   } else {
      TDMParams.samplingRatekHz = samplingRatekHz;

      /* Write the TSIP parameters to the DSP. */
      gpakWriteDsp (DspId, DSP_TDMCFG_ADDRESS, BytesToTxUnits (sizeof TDMParams), (void *) &TDMParams);
   }
   return GpakApiSuccess;
}
#endif


GpakApiStatus_t gpakReadMemoryTestStatus ( 
	ADT_UInt32 DspId,
	memoryTestResults *dspMemoryTest,
    GPAK_MemoryTestResults_t *pStatus
	)
{
    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 *Msg;

	if (MaxDsps <= DspId)
        return GpakApiInvalidDsp;

	Msg    = (ADT_UInt16 *) MsgBuffer;

    Msg[0] = MSG_READ_MEMTEST_STATUS << 8;

	if (!TransactCmd(DspId, MsgBuffer, 1, MSG_READ_MEMTEST_STATUS_REPLY, 5,  NO_ID_CHK, (ADT_UInt16) 0))
        return GpakApiCommFailure;
   
	*pStatus = (GPAK_MemoryTestResults_t)Msg[1];	
	dspMemoryTest->TestPattern = Msg[2];
	dspMemoryTest->TestAddress = Msg[3] << 16;
	dspMemoryTest->TestAddress |= Msg[4];

	if (*pStatus != MT_Success)
        return GpakApiParmError;
	else
		return GpakApiSuccess;
}

GpakApiStatus_t gpakConfigureSideTone ( 
	ADT_UInt32 DspId,
	setSideTone dspSideTone
	)
{
    ADT_UInt32 MsgLenI16;                        /* message length */
    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 *Msg, *Msg1;
	ADT_UInt32 length;
	ADT_UInt8 Status;

	if (MaxDsps <= DspId)
        return GpakApiInvalidDsp;

	Msg1    = (ADT_UInt16 *) MsgBuffer;
    Msg = Msg1;

	 /* Build the Configure Side Tone Message. */
    Msg[0] = MSG_CONFIGURE_SIDETONE << 8;
    Msg[0] |= (ADT_UInt16)dspSideTone.ChannelId;
	Msg[1] = (ADT_UInt16)dspSideTone.Enable;
	Msg[2] = (ADT_UInt16)dspSideTone.Gain;
	MsgLenI16 = (ADT_UInt16)dspSideTone.MsgLen;

	length = TransactCmd(DspId, MsgBuffer, MsgLenI16, MSG_CONFIGURE_SIDETONE_REPLY, 2,  NO_ID_CHK, (ADT_UInt16) 0);
    if (!length)
        return GpakApiCommFailure;
    Msg  = Msg1;
    /* Return success or failure . */
		Status = Msg[0] & 0xFF;

	if(Status != 0)
	{
        return GpakApiParmError;
	}
	else
	{
		return GpakApiSuccess;
	}

}


#define TRANSFER_BLOCK_SIZE_I8 (MSG_BUFFER_SIZE-10)*4

typedef struct wavefileInfo_t {
    ADT_UInt32 dataLengthI8;
    ADT_UInt32 numBlocks;
    ADT_UInt16 numResidual;
    ADT_UInt16 coding;
    ADT_UInt16 sampleRate;
} wavefileInfo_t;

static WaveReadStatus_t read_wav_file_header (GPAK_FILE_ID fp, wavefileInfo_t *info) {
int n;
ADT_UInt32 Subchunk1Size, Subchunk2Size, SampleRate, NumBlocks, NumResidual;
ADT_UInt32 FactchunkSize;
ADT_UInt16 AudioFormat, NumChannels, BitsPerSample, ExtensionSize;
ADT_UInt8 buf[24];
ADT_UInt32 transfer_block_size_I8 =  TRANSFER_BLOCK_SIZE_I8;

    // read chunkID RIFF==little endian, RIFX== big-endian
    n = gpakReadFile(fp, buf, 4);
    if (n!=4) return WAVREAD_READ_ERR;
    buf[4]=0;
    if (strcmp(buf, "RIFF") != 0) return WAVREAD_FILETYPE_ERR;

    // skip chunk size
    n = gpakReadFile(fp, buf, 4);
    if (n!=4) return WAVREAD_READ_ERR;

    // read format
    n = gpakReadFile(fp, buf, 4);
    if (n!=4) return WAVREAD_READ_ERR;
    buf[4]=0;
    if (strcmp(buf, "WAVE") != 0) return WAVREAD_FILETYPE_ERR;

    // read subchunk1ID
    n = gpakReadFile(fp, buf, 4);
    if (n!=4) return WAVREAD_READ_ERR;
    buf[4]=0;
    if (strcmp(buf, "fmt ") != 0) return WAVREAD_FILETYPE_ERR;

    // read subchunk1 size
    n = gpakReadFile(fp, buf, 4);
    if (n!=4) return WAVREAD_READ_ERR;
    Subchunk1Size = buf[0] | (buf[1] << 8) | (buf[2] << 16) |  (buf[3] << 24);
    
    // read audio format (only PCM, U-LAW or A-LAW supported)
    n = gpakReadFile(fp, buf, 2);
    if (n!=2) return WAVREAD_READ_ERR;
    AudioFormat = buf[0] | (buf[1] << 8);
    if (AudioFormat == 1) 
        info->coding = L16;
    else  if (AudioFormat == 6)  
        info->coding = PCMA_64;
    else  if (AudioFormat == 7)  
        info->coding = PCMU_64;
    else
         return  WAVREAD_AUDIOFMT_ERR;

    // read number of channels                
    n = gpakReadFile(fp, buf, 2);
    if (n!=2) return WAVREAD_READ_ERR;
    NumChannels = buf[0] | (buf[1] << 8);
    if (NumChannels != 1) return WAVREAD_NUMCHAN_ERR;
    
    // read SampleRate
    n = gpakReadFile(fp, buf, 4);
    if (n!=4) return WAVREAD_READ_ERR;
    SampleRate = buf[0] | (buf[1] << 8) | (buf[2] << 16) |  (buf[3] << 24);
    if ((SampleRate != 8000) && (SampleRate != 16000)) return WAVREAD_SAMPLERATE_ERR;

    // read ByteRate
    n = gpakReadFile(fp, buf, 4);
    if (n!=4) return WAVREAD_READ_ERR;

    // read block align                
    n = gpakReadFile(fp, buf, 2);
    if (n!=2) return WAVREAD_READ_ERR;
                                        
    // read bits per sample, check against audio format                
    n = gpakReadFile(fp, buf, 2);
    if (n!=2) return WAVREAD_READ_ERR;
    BitsPerSample = buf[0] | (buf[1] << 8);
    if (BitsPerSample == 16) {
        if (AudioFormat != 1) return WAVREAD_BITSIZE_ERR;
    } else if  (BitsPerSample == 8) {
        if ((AudioFormat != 6) && (AudioFormat != 7)) return WAVREAD_BITSIZE_ERR;
    } else {
        return WAVREAD_BITSIZE_ERR;
    }

    if (Subchunk1Size != 16) {
        // read size of extension               
        n = gpakReadFile(fp, buf, 2);
        if (n!=2) return WAVREAD_READ_ERR;
        ExtensionSize= buf[0] | (buf[1] << 8);
        if ((ExtensionSize != 0) && (ExtensionSize != 22)) return WAVREAD_EXTENSION_ERR;
        // read the extension if present (always 22 bytes)
        if (ExtensionSize) { 
            n = gpakReadFile(fp, buf, 22);
            if (n!=22) return WAVREAD_READ_ERR;
        }
    }

    if ((AudioFormat == 6) || (AudioFormat == 7)) {
        // all compressed (NON-PCM) formats must have a "fact" chunk
        // read fact ID
        n = gpakReadFile(fp, buf, 4);
        if (n!=4) return WAVREAD_READ_ERR;
        buf[4]=0;
        if (strcmp(buf, "fact") != 0) return WAVREAD_FILETYPE_ERR;

        // read fact chunk size
        n = gpakReadFile(fp, buf, 4);
        if (n!=4) return WAVREAD_READ_ERR;
        FactchunkSize = buf[0] | (buf[1] << 8) | (buf[2] << 16) |  (buf[3] << 24);
        if (FactchunkSize != 4) return  WAVREAD_FILETYPE_ERR;

        // read the dwSampleLength
        n = gpakReadFile(fp, buf, 4);
        if (n!=4) return WAVREAD_READ_ERR;

    }

    // read subchunk2ID
    n = gpakReadFile(fp, buf, 4);
    if (n!=4) return WAVREAD_READ_ERR;
    buf[4]=0;
    if (strcmp(buf, "data") != 0) return WAVREAD_FILETYPE_ERR;

    // read subchunk2 size
    n = gpakReadFile(fp, buf, 4);
    if (n!=4) return WAVREAD_READ_ERR;
    Subchunk2Size = buf[0] | (buf[1] << 8) | (buf[2] << 16) |  (buf[3] << 24);

    NumBlocks = Subchunk2Size/transfer_block_size_I8;
    NumResidual = Subchunk2Size - NumBlocks*transfer_block_size_I8;    

    info->dataLengthI8 = Subchunk2Size;
    info->numBlocks = NumBlocks;
    info->numResidual = NumResidual;
    info->sampleRate = (ADT_UInt16)SampleRate;
    return WAVREAD_SUCCESS;
}

int msg_buffer_sizeI32 = MSG_BUFFER_SIZE;
void send_block(char *data, int lengthI8) {
    
}
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakLoadWavefile - Load a wavfile in DSP memory for future playback
 * 
 * FUNCTION
 *  This function loads a wavefile in DSP memory. Supported formats are:
 *     PCM 16-bit, U-LAW/A-LAW 8-bit, MONO
 * 
 * RETURNS
 *  None
 */
GpakApiStatus_t gpakLoadWavefile (
   ADT_UInt32       DspId,                  /* DSP Identifier */
   GPAK_FILE_ID     fp,                     /* Wavefile handle */
   ADT_UInt16       playbackId,             /* Id used to playback this file */
   WaveReadStatus_t *waveFileStatus,        /* .wav file parse status */
   GPAK_LoadWavefileParmsStat_t *pStatus)   /* DSP file download status */
{

    ADT_UInt32 MsgLenI16;                   /* message length */
    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];  /* message buffer */
    ADT_Int16 *Msg;
    ADT_UInt32 block, reply_block; 
    wavefileInfo_t info;
    int n;


    if (MaxDsps <= DspId)
        return GpakApiInvalidDsp;

    if (pDspIfBlk[DspId] == 0)
       DSPSync (DspId);

    if ((*waveFileStatus = read_wav_file_header (fp, &info)) != WAVREAD_SUCCESS) {
        return GpakApiParmError;
    }
        
    // First open the wavefile configuration. DSP will allocate memory to store the file.
    Msg = (ADT_Int16 *) &MsgBuffer[0];
    Msg[0] = MSG_NEW_WAVEFILE << 8;
    Msg[1] = playbackId;
    Msg[2] = (ADT_UInt16)info.dataLengthI8;
    Msg[3] = (ADT_UInt16)(info.dataLengthI8>>16);
    Msg[4] = info.coding;
    Msg[5] = info.sampleRate;
    MsgLenI16 = 6;

    if (!TransactCmd (DspId, MsgBuffer, MsgLenI16, MSG_NEW_WAVEFILE_REPLY,  2, NO_ID_CHK, 0))
        return GpakApiCommFailure;

    *pStatus = (GPAK_LoadWavefileParmsStat_t) Byte1 (Msg[1]);
    if (*pStatus != Lw_Success) {
        return GpakApiParmError;
    }

    // transfer full blocks of audio samples to the DSP
    for (block=1; block<=info.numBlocks; block++) {
        Msg[0] = MSG_LOAD_WAVEFILE << 8;
        Msg[1] = playbackId;
        Msg[2] = (ADT_UInt16)block;
        Msg[3] = (ADT_UInt16)(block>>16);
        Msg[4] = TRANSFER_BLOCK_SIZE_I8;
        Msg[5] = 0;   // last block==0
        if ((info.numResidual == 0) && (block == info.numBlocks))
            Msg[5] = 1;   // last block==1
        MsgLenI16 = 6 + TRANSFER_BLOCK_SIZE_I8/2;

        n = gpakReadFile(fp, (ADT_UInt8 *)&Msg[6], TRANSFER_BLOCK_SIZE_I8);    
        if (n!=TRANSFER_BLOCK_SIZE_I8) return WAVREAD_READ_ERR;

        if (!TransactCmd (DspId, MsgBuffer, MsgLenI16, MSG_LOAD_WAVEFILE_REPLY,  4, NO_ID_CHK, 0)) {
            return GpakApiCommFailure;
        }

        *pStatus = (GPAK_LoadWavefileParmsStat_t) Byte1 (Msg[1]);
        if (*pStatus != Lw_Success) {
            return GpakApiParmError;
        }
        reply_block = (ADT_UInt32)Msg[2] | (ADT_UInt32)Msg[3]<<16;
        if (reply_block != block) {
            return GpakApiParmError;
        }
    }

 
    // transfer final block of audio samples to the DSP
    if (info.numResidual) {
        Msg[0] = MSG_LOAD_WAVEFILE << 8;
        Msg[1] = playbackId;
        Msg[2] = (ADT_UInt16)block;
        Msg[3] = (ADT_UInt16)(block>>16);
        Msg[4] = info.numResidual;
        Msg[5] = 1;   // last block==1
        MsgLenI16 = 6 + (info.numResidual+1)/2;

        n = gpakReadFile(fp, (ADT_UInt8 *)&Msg[6], info.numResidual);    
        if (n!=info.numResidual) return WAVREAD_READ_ERR;

        if (!TransactCmd (DspId, MsgBuffer, MsgLenI16, MSG_LOAD_WAVEFILE_REPLY,  4, NO_ID_CHK, 0)) {
            return GpakApiCommFailure;
        }

        *pStatus = (GPAK_LoadWavefileParmsStat_t) Byte1 (Msg[1]);
        if (*pStatus != Lw_Success) {
            return GpakApiParmError;
        }

        reply_block = (ADT_UInt32)Msg[2] | (ADT_UInt32)Msg[3]<<16;
        if (reply_block != block) {
            return GpakApiParmError;
        }
    }
    return GpakApiSuccess;
}

#ifdef TEST_WAVELOAD
// Waveload test function. For debug only.
ADT_UInt16 transfer_block_size_I8 = 512;
static void testLoadWavefile ( )
{
    ADT_UInt32 MsgLenI16;                   /* message length */
    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];  /* message buffer */
    ADT_Int16 *Msg;
    ADT_UInt32 block, reply_block; 
    ADT_UInt16 i;
    volatile int keep_going = 1;
    GPAK_LoadWavefileParmsStat_t status;

    block = 1;
    while (keep_going) {
        Msg = (ADT_Int16 *) &MsgBuffer[0];
        Msg[0] = MSG_LOAD_WAVEFILE << 8;
        Msg[1] = 0;
        Msg[2] = (ADT_UInt16)block;
        Msg[3] = (ADT_UInt16)(block>>16);
        Msg[4] = transfer_block_size_I8;
        Msg[5]  = 0;
        MsgLenI16 = 6 + transfer_block_size_I8/2;
        for (i=0; i< transfer_block_size_I8/2; i++) {
            Msg[6+i] = i;
        }
        if (!TransactCmd (0, MsgBuffer, MsgLenI16, MSG_LOAD_WAVEFILE_REPLY,  4, NO_ID_CHK, 0)) {
            printf("transfer failure block %d\n",block);
        }
        status = (GPAK_LoadWavefileParmsStat_t) Byte1 (Msg[1]);
        if (status != Lw_Success) {
            printf("Dsp Status Error %d, block %d\n", status, block);
        }
        reply_block = (ADT_UInt32)Msg[2] | (ADT_UInt32)Msg[3]<<16;
        if (reply_block != block) {
            printf("Reply block Error %d, block %d, reply_block\n", block, reply_block);
        }
     block++;
    }
}
#endif


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakPlayWavefile - Play a wavfile that was loaded in DSP memory 
 * 
 * FUNCTION
 *  This function plays a wavefile in DSP memory. The file playbackId must match
 *  a file that was previously downloaded. Files can be played once, or looped
 *  continuously. They can be played towards the network interface or towards
 *  the TDM interface. The GPAK channel must be active. 
 * 
 * RETURNS
 *  None
 */
ADT_UInt32 gpakFormatWavePlayback ( ADT_UInt16 *Msg, ADT_UInt16 ChannelId,
                                    ADT_UInt16 playbackId, WavePlaybackCmd command,  GpakDeviceSide_t direction) {

    Msg[0] = PackBytes (MSG_PLAY_WAVEFILE, CORE_ID(ChannelId));
    Msg[1] = PackBytes (CHAN_ID(ChannelId), command);
    Msg[2] = playbackId;
    Msg[3] = direction;

    return 4;
}

GpakApiStatus_t gpakPlayWavefile (
   ADT_UInt32       DspId,              /* DSP Identifier */
   ADT_UInt16       ChannelId,          /* Channel Id to  play on */
   ADT_UInt16       playbackId,         /* Id used to playback this file */
   WavePlaybackCmd  command,            /* Stop, play once, loop */
   GpakDeviceSide_t direction,          /* (ASide or NetToDSP) == to TDM, (BSide or DSPToNet) == to Network */ 
   GPAK_PlayWavefileStat_t *pStatus)    /* DSP play wavefile status */
{

    ADT_UInt32 MsgLenI16;                   /* message length */
    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];  /* message buffer */
    ADT_Int16 *Msg;

    if (MaxDsps <= DspId)
        return GpakApiInvalidDsp;

    if (pDspIfBlk[DspId] == 0)
       DSPSync (DspId);

    if (MaxChannels[DspId] <= (DSP_Word) CHAN_ID(ChannelId))
        return GpakApiInvalidChannel;

    Msg    = (ADT_UInt16 *) &MsgBuffer[0];

    MsgLenI16 = gpakFormatWavePlayback (Msg, ChannelId, playbackId, command, direction);
    if (!TransactCmd (DspId, MsgBuffer, MsgLenI16, MSG_PLAY_WAVEFILE_REPLY, 2, 
                     BYTE_ID, (ADT_UInt16) CHAN_ID(ChannelId)))
        return GpakApiCommFailure;

    /* Return success or failure  */
    *pStatus = (GPAK_PlayWavefileStat_t) Byte0 (Msg[1]);
    if (*pStatus == Pw_Success)  return GpakApiSuccess;
    else                         return GpakApiParmError;

}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadVersionString - Read version string from DSP
 * 
 * FUNCTION
 *  This function reads the build version, date string
 * 
 * RETURNS
 *  None
 */
gpakTestStatus_t gpakReadVersionString (ADT_UInt32 DspId, gpakVersionString_t *verString) {
   gpakTestStatus_t apiStat;
   GPAK_TestStat_t  tstStat;
   ADT_UInt16       tstParam[3];
   ADT_UInt32       Buff[64];
   ADT_UInt8*       I8Buff = (ADT_UInt8 *)Buff;
   int i; 
 
   I8Buff = (ADT_UInt8*) Buff;

   tstParam [0] = 2<<8;   // custom Message ID==2 to read version ID
   tstParam [1] = 3;   // CMD Size
   tstParam [2] = 128; // REPLY SIZE I16

   apiStat = gpakTestMode (DspId, CustomVarLen, (GpakTestData_t*) &tstParam, (ADT_UInt16*)Buff, &tstStat);
   if (apiStat == GpakApiSuccess) {
        for (i=0; i<128; i++)
            verString->buf[i] = I8Buff[i];     
   }
   return apiStat;
}
