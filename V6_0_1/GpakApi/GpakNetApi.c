/*
 * Copyright (c) 2002 - 2007, Adaptive Digital Technologies, Inc.
 *
 * File Name: GpakNetApi.c
 *
 *
 * Description:
 *   This file contains host to DSP driver functions for network communications 
 *   with DSPs executing G.PAK software. The file is integrated into the host
 *   processor connected to G.PAK DSPs via network TCP messaging.
 *
 *
 */
#include <stdio.h>
#include <memory.h>

#include "GpakCustWin32.h"
#include "adt_typedef_user.h"
#include "GpakHpi.h"
#include "GpakApi.h"
#include "GpakEnum.h"


extern int uartPrintf(char *format, ...);
//#define ADT_printf uartPrintf
#define ADT_printf

extern void gpakEndianI32Convert (ADT_UInt32 *Buff, int BuffI8);


// Customer supplied data arrays/values
extern int ipver;
extern ADT_UInt32 LocalIP;                  // IP Address of host
extern ADT_UInt32 RemoteIP [];              // IP Addresses of DSPs
extern IP6N LocalIP6;                  // IPv6 Address of host
extern IP6N RemoteIP6 [];              // IPv6 Addresses of DSPs
extern void *msgSkt[];                      // DSPs TCP Messaging sockets
extern void *evtSkt[];                      // DSPs TCP event sockets
extern char gpakVerifyTag[][GPAK_TAG_I8];   // DSPs handshaking verification tags
extern char gpakDisconnectTag[][GPAK_TAG_I8];   // DSPs handshaking verification tags
extern void *invalid_socket;                // System identification of invalid socket
extern unsigned int MaxDsps;
extern unsigned short msg_tcp_port;
extern unsigned short evt_tcp_port;


//=================================================
//  NETWORK APIs
//================================================
static int SendDspConnectMessages (ADT_UInt32 DspId);
static int SendDspConnectEvents   (ADT_UInt32 DspId);
static int SendDspConnectMessagesUdp (ADT_UInt32 DspId);
static int SendDspConnectEventsUdp   (ADT_UInt32 DspId);

#ifdef UDP_MESSAGING
static int (*pSendDspConnectMessages) (ADT_UInt32 DspId) = SendDspConnectMessagesUdp;
static int (*pSendDspConnectEvents) (ADT_UInt32 DspId)   = SendDspConnectEventsUdp;
#else
static int (*pSendDspConnectMessages) (ADT_UInt32 DspId) = SendDspConnectMessages;
static int (*pSendDspConnectEvents) (ADT_UInt32 DspId)   = SendDspConnectEvents;
#endif

/**
 *  @b Description
 *  @n
 *      Utility Function which converts the IPv6 address to string format.
 *
 *  @param[in]   address
 *      The IPv6 Address in IP6N format.
 *  @param[out]  strIPAddress
 *      The IPv6 Address in String Format.
 *
 *  @retval
 *   Not Applicable.
 */
void IPv6IPAddressToString (IP6N address, char *strIPAddress)
{
    //InetNtopW(23, address, strIPAddress, 46);
    //RtlIpv6AddressToStringA(address, strIPAddress);
#if 1

    int     index              = 0;
    int     num_leading_zeroes = 0;
    int     zc                 = 0;
    char    tmpStr[60];
    char*   ptr_tmpStr;


    /* Special Cases: Check for UNSPECIFIED Address and LOOPBACK Address */
    if((address.u.addr16[0] == 0) && (address.u.addr16[1] == 0) && (address.u.addr16[2] == 0) &&
       (address.u.addr16[3] == 0) && (address.u.addr16[4] == 0) && (address.u.addr16[5] == 0) &&
       (address.u.addr16[6] == 0))
    {
        if (address.u.addr16[7] == 0) {
            sprintf (strIPAddress, "::");
        }
        else {
            /* Note64: check format strings for 64 bit */
            sprintf (strIPAddress, "::%x",ntohs(address.u.addr16[7]));
        }
        return;
    }

    /* Initialize the memory. */
    *strIPAddress = 0;

    /* Cycle through the entire address. */
    while (index < 8)
    {
        /* Initialize the temp string at the beginning of each loop iteration. */
        ptr_tmpStr  = &tmpStr[0];
        *ptr_tmpStr = 0;

        /* Check if the address is 0. */
        if (address.u.addr16[index] == 0)
        {
            /* YES. This is 0. Now check if we have done zero-compression or not? Remember
             * the RFC states that we are allowed only once to do zero-compression. */
            if (zc == 0)
            {
                /* Zero compression has not been done. Count the number of leading zeroes. */
                num_leading_zeroes++;
            }
            else
            {
                /* Zero compression has been done. Simply print the address on the console now */
                if (index == 7)
                    sprintf (tmpStr, "%x", ntohs(address.u.addr16[index]));
                else
                    sprintf (tmpStr, "%x:", ntohs(address.u.addr16[index]));
            }
        }
        else
        {
            /* NO. This is NON ZERO. Check if we got enough zeroes for compression and that we
             * have not already done the zero compression. */
            if ((num_leading_zeroes >= 2) && (zc == 0))
            {
                /* OK; we can do zero compression at this stage. We print only
                 * one ':' here because we have already done one while printing the initial
                 * non-zero down. */
                ptr_tmpStr += sprintf (ptr_tmpStr, ":");

                /* We are allowed only one zero compression. */
                zc = 1;
            }

            /* This is not a candidate for zero compression; so print out the contents.
             * For the last address index we dont want an additional ':' at the end. */
            if (index == 7)
                ptr_tmpStr += sprintf (ptr_tmpStr, "%x", ntohs(address.u.addr16[index]));
            else
                ptr_tmpStr += sprintf (ptr_tmpStr, "%x:", ntohs(address.u.addr16[index]));

            /* Reset the number of leading zeroes. */
            num_leading_zeroes = 0;
        }

        /* Get the next address. */
        index = index + 1;

        /* Concatenate to the final output. */
        strcat (strIPAddress, tmpStr);
    }

    /* This handles the trailing zeroes. */
    if ((num_leading_zeroes >= 4) && (zc == 0))
        strcat (strIPAddress, ":");
#endif
    /* Work has been completed. */
    return;
}

/**
 *  @b Description
 *  @n
 *      Utility Function which prints the IPv6 address. The IP address passed
 *      has to be specified in network order (IP6N).
 *
 *  @param[in]   address
 *      IPv6 Address to be displayed.
 *  @retval
 *   Not Applicable.
 */
void IPv6DisplayIPAddress (IP6N address)
{
    char    strIPAddress[40];

    /* Convert to string format. */
    IPv6IPAddressToString (address, &strIPAddress[0]);

    /* Print out the address on the console. */
    ADT_printf("%s\n", strIPAddress);

    return;
}

/**
 *  @b Description
 *  @n
 *      Utility Function that validates whether a given ASCII
 *      character is a valid hexadecimal digit.
 *
 *  @param[in]   ch
 *      The character that needs to be validated
 *
 *  @retval
 *      1   - Success, the character is a hexadecimal digit
 *
 *  @retval
 *      0   - Error, the character is not a valid hexadecimal digit.
 */
static int isValidHexDigit (int ch)
{
  /* Valid Hexadecimal char. return 1. RFC 2396. */
  if ((ch >= '0' && ch <= '9') ||
      (ch >= 'A' && ch <= 'F') ||
      (ch >= 'a' && ch <= 'f'))
    return 1;
  /* Invalid Hexadecimal char. return 0. */
  else
    return 0;
}

/**
 *  @b Description
 *  @n
 *      Utility function that converts a given ASCII character
 *      to its hexadecimal value.
 *
 *  @param[in]   ch
 *      The character that needs to be converted to hex.
 *
 *  @retval
 *      Hex equivalent - Success
 *
 *  @retval
 *      -1  - Error, the character is not a valid hexadecimal digit.
 */
static int GetHexValue (int ch)
{
    if (ch >= '0' && ch <= '9')
    {
        return (ch -'0');
    }
    else if (ch >= 'A' && ch <= 'F')
    {
        return (ch - 'A' + 10);
    }
    else if (ch >= 'a' && ch <= 'f')
    {
        return (ch - 'a' + 10);
    }
    else
    {
        /* Not a valid hexadecimal char, so return error */
        return -1;
    }
}
/**
 *  @b Description
 *  @n
 *      Utility Function which converts an IPv6 Address from CHAR Format to IP6N
 *
 *  @param[in]   StringIP
 *      The IPv6 Address in String Format
 *  @param[out]  address
 *      The IPv6 Address in IP6N format.
 *
 *  @retval
 *      0   -   Success
 *  @retval
 *      -1  -   Error
 */
int IPv6StringToIPAddress (char* StringIP, IP6N *address)
{
    int     num_colon_sep = 0;
    int     num_dcolon_sep = 0;
    int     index = 0, exp_index = 0;

    /* Basic Validations: */
    if ((StringIP == NULL) || (address == NULL))
        return -1;

    /* Initialize the IPv6 Address */
    memset(address, 0, sizeof(IP6N));

    /* Cycle through and verify if the address had zero compression or not?
     * We run through the Entire string and check the number of ':' and '::' separators.
     */
    while (StringIP[index] != 0)
    {
        /* Parse through the string, and when we encounter
         * a ':' increment the number of colons by 1 and
         * if we encounter another ':' following a ':', i.e.,
         * a '::', increment both number of colons and
         * number of double colons.
         * These numbers are used for validation purposes
         * once we step out of the loop.
         */
        if (StringIP[index] == ':')
        {
            num_colon_sep++;

            if (StringIP[index + 1] == ':')
                num_dcolon_sep++;
        }
        else if (!isValidHexDigit(StringIP[index]))
        {
            /* ASCII char not a valid hexadecimal int. return
             * error.
             */
            return -1;
        }

        index = index + 1;
    }

    /* A Valid IPv6 Address cannot have more than 8 16-bit hexadecimal peices
     * separated by ":" separator or more than one "::". Also, if it doesnt have
     * any "::" separated pieces, then it must exactly have 7 ":". Otherwise,
     * its an invalid IPv6 address.
     */
    if (num_colon_sep > 7 || num_dcolon_sep > 1 || (!num_dcolon_sep && num_colon_sep != 7))
        return -1;

    /* Iterate through the string and convert the characters to their hexadecimal value
     * to insert into IPv6 address.
     */
    index = 0;
    while (StringIP[index] != 0)
    {
        if (StringIP[index] == ':')
        {
            if (StringIP[index + 1] == ':')
                exp_index += (8 - num_colon_sep);
            else
                exp_index ++;
        }
        else
        {
            address->u.addr16[exp_index] = htons((ntohs(address->u.addr16[exp_index]) << 4) + GetHexValue(StringIP[index]));
        }

		index ++;
    }

    /* Address has been converted. */
    return 0;
}


//=================================================
//  Network stack intialization
int gpakServerInit (int ipVer) {
   int rslt, i;

   ipver = ipVer;
   LocalIP = 0;
   memset(&LocalIP6, 0, sizeof(IP6N));
   for (i=0; i<(int) MaxDsps ; i++) {
      msgSkt[i] = (void *) invalid_socket;
      evtSkt[i] = (void *) invalid_socket;
      RemoteIP[i] = 0;
      memset(&RemoteIP6[i], 0, sizeof(IP6N));
   }
   // Link to socket library
   if ((rslt = gpakNetworkStackInit (&LocalIP)) != 0) {
       ADT_printf ("Socket initialization failure %d\n", rslt);
   }
   return rslt;
}

//{  Make initial TCP/IP connections with G.PAK DSP
//
//  Returns:
//     TRUE - Both message and event connections made
//}    FALSE - otherwise
const char *gpak_disconnect_string="disconnect";
int gpakServerConnect (ADT_UInt32 DspId, char *gpakServer, void *remoteIP) {
   int i;
   ADT_UInt32 *tag, *pad, *vID, *IP, VersionId;
   IP6N *remoteIp6 = (IP6N *)remoteIP;

   // Create G.PAK verification tag
   IP  = (ADT_UInt32 *) &gpakVerifyTag[DspId][0];
   vID = (ADT_UInt32 *) &gpakVerifyTag[DspId][4];
   tag = (ADT_UInt32 *) &gpakVerifyTag[DspId][8];
   pad = (ADT_UInt32 *) &gpakVerifyTag[DspId][30];

   VersionId = API_VERSION_ID;
   if (ipver == 4)
        memcpy (IP,  &LocalIP, 4);
   else 
        memcpy(IP, &LocalIP6.u.addr32[3], 4);

   memcpy (vID, &VersionId, 4);
   memcpy (tag, gpakServer, 22);
   memset ((void *) pad, 0, 2);
   for (i=0; i<7; i++, vID++) *vID ^= *IP;

#ifdef UDP_MESSAGING
   // create disconnect tag
   memcpy(&gpakDisconnectTag[DspId][0], gpakServer, 22);
   memcpy(&gpakDisconnectTag[DspId][22], gpak_disconnect_string, 10);
    // Convert messages to match G.PAK DSP format
    if (HostBigEndian)
       gpakEndianI32Convert ((ADT_UInt32 *) gpakDisconnectTag[DspId], GPAK_TAG_I8);
#endif

   MaxCmdMsgLen[DspId] = 0;
   MaxChannels[DspId]  = 0;

   if (ipver == 4)
        RemoteIP[DspId] = *((ADT_UInt32 *)remoteIP);
   else 
   {
        for (i=0; i<4; i++) 
            RemoteIP6[DspId].u.addr32[i] = remoteIp6->u.addr32[i];
   }
   if (!(*pSendDspConnectMessages) (DspId))   ADT_printf ("\nFailed to connect to messaging");
   if (!(*pSendDspConnectEvents)   (DspId))   ADT_printf ("\nFailed to connect to events");
   return ((msgSkt[DspId] != invalid_socket) && (evtSkt[DspId] != invalid_socket));
}

//  Disconnect DSP connections
void gpakServerDisconnect (ADT_UInt32 DspId) {
#ifdef UDP_MESSAGING
int msgI8;

   if (msgSkt[DspId] != NULL) {
        // Send disconnect tag to G.PAK message server
        msgI8 = gpakSendDspMsg (DspId, msgSkt[DspId], gpakDisconnectTag[DspId], GPAK_TAG_I8);
        if (msgI8 != GPAK_TAG_I8) {
           // message disconnect send failure.
           ADT_printf ("Server disconnect send error %d\n", msgI8);
        }
   }

   if (evtSkt[DspId] != NULL) {
        // Send disconnect tag to G.PAK event server
        msgI8 = gpakSendDspConnectEvtUdp (DspId, evtSkt[DspId], gpakDisconnectTag[DspId], GPAK_TAG_I8);
        if (msgI8 != GPAK_TAG_I8) {
           // event disconnect send failure.
           ADT_printf ("Server disconnect send error %d\n", msgI8);
        }
   }
#endif
   gpakCloseTCPSocket (msgSkt[DspId]);
   gpakCloseTCPSocket (evtSkt[DspId]);
   msgSkt[DspId] = invalid_socket;
   evtSkt[DspId] = invalid_socket;
   RemoteIP[DspId] = 0;
   memset(&RemoteIP6[DspId], 0, sizeof(IP6N));
}


//{
// Receive event from DSP via socket
//  Input Parameters:
//     EvtBuffer   - Pointer to event buffer for reception
//     EvtBufferI8 - Number of bytes for buffer
//
//  Returns:
//     0.              No reply
//     Positive value. Number of bytes received.
//     Negative value. Error value.
//
//}
ADT_UInt32 gpakRecvDspEvt (ADT_UInt32 DspId, void *EvtBuffer, int EvtBufferI8) {
   ADT_Int32 EvtPayloadI8;

   EvtPayloadI8 = gpakRecvDspMsg (DspId, evtSkt[DspId], EvtBuffer, EvtBufferI8);
   if (EvtPayloadI8 < 0) {
      ADT_printf ("Event socket closed %d\n", EvtPayloadI8);
      gpakCloseTCPSocket (evtSkt[DspId]);
      evtSkt[DspId] = (void *) invalid_socket;
   }
   return EvtPayloadI8;
}

//===================================================================================
//===================================================================================
//
//  G.PAK API ethernet driver routines

//{  SendDspConnectMessages
//       1) sends tcp connection request to DSP's messaging port
//       2) sends connection verification tag of format:
//           encryption mask (host's IP address)
//           G.PAK API version ID
//           G.PAK server name
//           2 bytes of zero padding
//}       3) receives DSP's configuration info
static int SendDspConnectMessages (ADT_UInt32 DspId) {
   void *skt;
   int msgI8, rslt;
   struct gpakCfg {
      ADT_UInt32 MsgTag;
      ADT_UInt16 MaxCmdMsgI8;
      ADT_UInt16 MaxChanCnt;
   } gpakCfg;


   if (ipver == 4) {
        ADT_printf ("Connecting to msg DSP %d at %08x\n", DspId, RemoteIP[DspId]);
        skt = gpakOpenTCPSocket (RemoteIP[DspId], msg_tcp_port);
   } else {
        char ipStr[46];  
        IPv6IPAddressToString (RemoteIP6[DspId], ipStr);
        ADT_printf ("Connecting to msg DSP %d at %s\n", DspId, ipStr);
        skt = gpakOpenTCPSocket6 (RemoteIP6[DspId], msg_tcp_port);
   }
   if (skt == NULL) {
      msgSkt[DspId] = invalid_socket;
      return ADT_FALSE;
    }
    msgSkt[DspId] = skt;

   //-------------------------------------------------
   // Send verification tag to G.PAK server
   // Convert messages to match G.PAK DSP format
   if (HostBigEndian)
      gpakEndianI32Convert ((ADT_UInt32 *) gpakVerifyTag[DspId], GPAK_TAG_I8);

   msgI8 = gpakSendDspMsg (DspId, msgSkt[DspId], gpakVerifyTag[DspId], GPAK_TAG_I8);
   if (msgI8 != GPAK_TAG_I8) {
      // Connection failure.
      // Connection attempt will be made on next command to DSP
      ADT_printf ("Server connect send error %d\n", msgI8);
      gpakCloseTCPSocket (msgSkt[DspId]);
      msgSkt[DspId] = invalid_socket;
      DSPError[DspId] = msgI8;
      return ADT_FALSE;
   }

   //-------------------------------------------------
   // Read configuration response from server
   msgI8 = gpakRecvDspMsg (DspId, msgSkt[DspId], (void *) &gpakCfg, sizeof (gpakCfg));
   if (msgI8 != sizeof (gpakCfg)) {
      ADT_printf ("G.PAK server recv error %d\n", msgI8);
      gpakCloseTCPSocket (msgSkt[DspId]);
      msgSkt[DspId] = invalid_socket;
      DSPError[DspId] = msgI8;
      return ADT_FALSE;
   }

   // Convert G.PAK DSP messages from little endian to match host format
   if (HostBigEndian)
      gpakEndianI32Convert ((ADT_UInt32 *) &gpakCfg, msgI8);

   //-------------------------------------------------
   // Make messaging socket non-blocking
   rslt = gpakUnblockTCPSocket (msgSkt[DspId]);
   if (rslt != 0) {
      ADT_printf ("Socket unblocking failure %d\n", rslt);
      gpakCloseTCPSocket (msgSkt[DspId]);
      msgSkt[DspId] = invalid_socket;
      DSPError[DspId] = msgI8;
      return ADT_FALSE;
   }

   ADT_printf ("CmdMsgI8: %d   ChanCnt: %d\n", gpakCfg.MaxCmdMsgI8, gpakCfg.MaxChanCnt);
   MaxCmdMsgLen[DspId] = gpakCfg.MaxCmdMsgI8;
   MaxChannels[DspId]  = gpakCfg.MaxChanCnt;
   return ADT_TRUE;
}

//{  SendDspConnectEvents
//}       1) sends tcp connection request to DSP's event port
static int SendDspConnectEvents (ADT_UInt32 DspId) {
   void *skt;
   int rslt;

   ADT_printf ("Connecting to evt DSP %d\n", DspId);
   //-------------------------------------------------
   //  Open TCP socket for evt stream
   if (ipver == 4)
        skt = gpakOpenTCPSocket (RemoteIP[DspId], evt_tcp_port);
   else
        skt = gpakOpenTCPSocket6 (RemoteIP6[DspId], evt_tcp_port);
   if (skt == NULL) {
      evtSkt[DspId] = invalid_socket;
      return ADT_FALSE;
   }
   evtSkt[DspId] = skt;
 
   //-------------------------------------------------
   // Make event socket non-blocking
   rslt = gpakUnblockTCPSocket (evtSkt[DspId]);
   if (rslt != 0) {
      ADT_printf ("Event socket unblocking failure %d\n", rslt);
      gpakCloseTCPSocket (evtSkt[DspId]);
      evtSkt[DspId] = invalid_socket;
      DSPError[DspId] = rslt;
      return ADT_FALSE;
   }
   ADT_printf ("Connected\n");
   return ADT_TRUE;
}

static int SendDspConnectMessagesUdp (ADT_UInt32 DspId) {
   void *skt;
   int msgI8, rslt;
   struct gpakCfg {
      ADT_UInt32 MsgTag;
      ADT_UInt16 MaxCmdMsgI8;
      ADT_UInt16 MaxChanCnt;
   } gpakCfg;


   if (ipver == 4) {
        ADT_printf ("Connecting to msg DSP %d at %08x\n", DspId, RemoteIP[DspId]);
        skt = gpakOpenUDPSocket (RemoteIP[DspId], msg_tcp_port);
   } else {
        char ipStr[46];  
        IPv6IPAddressToString (RemoteIP6[DspId], ipStr);
        ADT_printf ("Connecting to msg DSP %d at %s\n", DspId, ipStr);
        skt = gpakOpenUDPSocket6 (RemoteIP6[DspId], msg_tcp_port);
   }
   if (skt == NULL) {
      msgSkt[DspId] = invalid_socket;
      return ADT_FALSE;
    }
    msgSkt[DspId] = skt;

   //-------------------------------------------------
   // Send verification tag to G.PAK server
   // Convert messages to match G.PAK DSP format
   if (HostBigEndian)
      gpakEndianI32Convert ((ADT_UInt32 *) gpakVerifyTag[DspId], GPAK_TAG_I8);

   msgI8 = gpakSendDspMsg (DspId, msgSkt[DspId], gpakVerifyTag[DspId], GPAK_TAG_I8);
   if (msgI8 != GPAK_TAG_I8) {
      // Connection failure.
      // Connection attempt will be made on next command to DSP
      ADT_printf ("Server connect send error %d\n", msgI8);
      gpakCloseTCPSocket (msgSkt[DspId]);
      msgSkt[DspId] = invalid_socket;
      DSPError[DspId] = msgI8;
      return ADT_FALSE;
   }

   //-------------------------------------------------
   // Read configuration response from server
   msgI8 = gpakRecvDspMsg (DspId, msgSkt[DspId], (void *) &gpakCfg, sizeof (gpakCfg));
   if (msgI8 != sizeof (gpakCfg)) {
      ADT_printf ("G.PAK server recv error %d\n", msgI8);
      gpakCloseTCPSocket (msgSkt[DspId]);
      msgSkt[DspId] = invalid_socket;
      DSPError[DspId] = msgI8;
      return ADT_FALSE;
   }

   // Convert G.PAK DSP messages from little endian to match host format
   if (HostBigEndian)
      gpakEndianI32Convert ((ADT_UInt32 *) &gpakCfg, msgI8);

   //-------------------------------------------------
   // Make messaging socket non-blocking
   rslt = gpakUnblockTCPSocket (msgSkt[DspId]);
   if (rslt != 0) {
      ADT_printf ("Socket unblocking failure %d\n", rslt);
      gpakCloseTCPSocket (msgSkt[DspId]);
      msgSkt[DspId] = invalid_socket;
      DSPError[DspId] = msgI8;
      return ADT_FALSE;
   }

   ADT_printf ("CmdMsgI8: %d   ChanCnt: %d\n", gpakCfg.MaxCmdMsgI8, gpakCfg.MaxChanCnt);
   MaxCmdMsgLen[DspId] = gpakCfg.MaxCmdMsgI8;
   MaxChannels[DspId]  = gpakCfg.MaxChanCnt;
   return ADT_TRUE;
}

static int SendDspConnectEventsUdp (ADT_UInt32 DspId) {
   void *skt;
   int msgI8, rslt;

   if (ipver == 4) {
        ADT_printf ("Connecting to Event DSP %d at %08x\n", DspId, RemoteIP[DspId]);
        skt = gpakOpenUDPSocket (RemoteIP[DspId], evt_tcp_port);
   } else {
        char ipStr[46];  
        IPv6IPAddressToString (RemoteIP6[DspId], ipStr);
        ADT_printf ("Connecting to Event DSP %d at %s\n", DspId, ipStr);
        skt = gpakOpenUDPSocket6 (RemoteIP6[DspId], evt_tcp_port);
   }

   if (skt == NULL) {
      evtSkt[DspId] = invalid_socket;
      return ADT_FALSE;
    }
    evtSkt[DspId] = skt;

   //-------------------------------------------------
   // Send verification tag to G.PAK server
   // Convert messages to match G.PAK DSP format
   if (HostBigEndian)
      gpakEndianI32Convert ((ADT_UInt32 *) gpakVerifyTag[DspId], GPAK_TAG_I8);

   msgI8 = gpakSendDspConnectEvtUdp (DspId, evtSkt[DspId], gpakVerifyTag[DspId], GPAK_TAG_I8);
   if (msgI8 != GPAK_TAG_I8) {
      // Connection failure.
      // Connection attempt will be made on next command to DSP
      ADT_printf ("Server connect send error %d\n", msgI8);
      gpakCloseTCPSocket (evtSkt[DspId]);
      evtSkt[DspId] = invalid_socket;
      DSPError[DspId] = msgI8;
      return ADT_FALSE;
   }
   // Make event socket non-blocking
   rslt = gpakUnblockTCPSocket (evtSkt[DspId]);
   if (rslt != 0) {
      ADT_printf ("Event socket unblocking failure %d\n", rslt);
      gpakCloseTCPSocket (evtSkt[DspId]);
      evtSkt[DspId] = invalid_socket;
      DSPError[DspId] = rslt;
      return ADT_FALSE;
   }
   return ADT_TRUE;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * SendDspCmdMessage - Send a DSP Command/Request message via TCP/IP
 *
 * Inputs
 *   DspId    - Dsp identifier 
 *   Msg     - Message buffer (16-bit words, 32-bit aligned)
 *   I16Cnt  - Number of 16-bit words
 *
 * RETURNS
 *  -1 = Unable to write message (msg len or DSP Id invalid or DSP not ready)
 *   0 = Temporarily unable to write message (previous Cmd Msg busy)
 *   1 = Message written successfully
 *
 */
int SendDspCmdMessage (ADT_UInt32 DspId, ADT_UInt32 *msg, ADT_UInt32 I16Cnt) {

   int msgI8, rslt;

   msgI8 = I16Cnt * 2;

   /* Make sure the message length is valid. */
   if ((msgI8 < 2) || (MaxCmdMsgLen[DspId] < (ADT_UInt16) msgI8)) {
      DSPError[DspId] = DSPCmdLengthError;
      return -1;  // Message length failure
   }
   if (msgSkt[DspId] == invalid_socket) {
      DSPError[DspId] = DSPNoConnection;
      return -1;
   }

   // Convert messages to match G.PAK DSP format
   if (HostBigEndian)
      gpakEndianI32Convert (msg, msgI8);

   if ((rslt = gpakSendDspMsg (DspId, msgSkt[DspId], msg, msgI8)) != msgI8) {
      // Connection failure. Connection must be re-established on next command
      ADT_printf ("Network send error %d\n", rslt);
      gpakCloseTCPSocket (msgSkt[DspId]);
      msgSkt[DspId] = invalid_socket;
      DSPError[DspId] = rslt;
      return -1;    // Connection failure
   }
   return 1;      // Success
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * RecvDspReplyMessage - Wait for DSP to reply with TCP/IP response
 *
 * FUNCTION
 *  Wait for re.
 *
 * Inputs
 *   DspId    - Dsp identifier 
 *
 *  Outputs
 *   Msg - Message buffer (16-bit words, 32-bit aligned)
 *
 *  Update
 *   pI16Cnt  - Pointer to number of 16-bit words 
 *               Before = buffer size
 *               After  = actual reply message
 *
 * RETURNS
 *  -1 = Unable to write message (msg len or DSP Id invalid or DSP not ready)
 *   0 = No message available (DSP Reply message empty)
 *   1 = Message read successfully (message and length stored in variables)
 *
 */
int RecvDspReplyMessage (ADT_UInt32 DspId, ADT_UInt32 *Msg, ADT_UInt32 *pI16Cnt) {
   int msgI8;      /* message length */

   int retryCnt = MaxWaitLoops * 3;

   // Wait for reply or time-out
   do {

      msgI8 = gpakRecvDspMsg (DspId, msgSkt[DspId], Msg, *pI16Cnt * 2);
      if (0 < msgI8) break;   // Message received
      if (0 == msgI8) {       // Socket timeout
         gpakHostDelay();
         continue;
      }

      // Connection failure. Connection must be re-established on next command
      gpakCloseTCPSocket (msgSkt[DspId]);
      msgSkt[DspId] = invalid_socket;
      DSPError[DspId] = msgI8;
      return -1;

   } while (0 < --retryCnt);

   if (*pI16Cnt * 2 < (ADT_UInt32) msgI8) {
      ADT_printf ("Length error\n");
      DSPError[DspId] = DSPRplyLengthError;
      return -1;
   }
   if (msgI8 == 0) {   // Timeout failure
      ADT_printf ("Receive timeout\n");
      DSPError[DspId] = DSPRplyTimeout;
      return 0;
   }

   // Convert G.PAK DSP messages from little endian to match host format
   if (HostBigEndian)
      gpakEndianI32Convert (Msg, msgI8);

   // Convert byte count to 16-bit count
   *pI16Cnt = (ADT_UInt32) (msgI8 / 2);
   return 1;           // Success
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * CheckDspMsgConnect - Check if the DSP is connnected to messaging socket.
 *
 * FUNCTION
 *  Determines if the DSP is connected to the host via TCP.  If not, try to
 *  re-establish a connection
 *
 * Inputs
 *   DspId - Dsp identifier 
 *
 * RETURNS
 *  -1 = DSP is not ready.
 *   0 = Reset did not occur.
 *   1 = Reset occurred.
 *
 */
int CheckDspMsgConnect (ADT_UInt32 DspId) {

   if (msgSkt[DspId] != (void *) invalid_socket) return 0;    // Connection already open
   (*pSendDspConnectMessages) (DspId);
   if (msgSkt[DspId] == (void *) invalid_socket)   
      return -1;                             // Connection failed

   return 1;                                 // Connection established with DSP
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * CheckDspEvtConnect - Check if the DSP is connnected to evebt socket.
 *
 * FUNCTION
 *  Determines if the DSP is connected to the host via TCP.  If not, try to
 *  re-establish a connection
 *
 * Inputs
 *   DspId - Dsp identifier 
 *
 * RETURNS
 *  -1 = DSP is not ready.
 *   0 = Reset did not occur.
 *   1 = Reset occurred.
 *
 */
int CheckDspEvtConnect (ADT_UInt32 DspId) {

   if (evtSkt[DspId] != (void *) invalid_socket) return 0;    // Connection already open
   (*pSendDspConnectEvents) (DspId);
   if (evtSkt[DspId] == (void *) invalid_socket)   
      return -1;                             // Connection failed

   return 1;                                 // Connection established with DSP
}



