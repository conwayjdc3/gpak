/*H***************************************************************************
*
* $Revision::   1.1                                                          $
* $Date::   Mar 30 2010 18:00:48                                             $
* $Author::   rfuchs                                                         $
*
* DESCRIPTION: Register defines for C64
*
*H***************************************************************************/

#ifndef c_64_H
#define c_64_H


#define IRAM_BASE            0x00000000UL
#define IRAM_LENGTH          0x00040000UL
#define IRAM_ADDR_BITS       16

#define SDRAM_BASE           0x80000000UL
#define SDRAM_LENGTH_SMALL   0x00100000UL
#define SDRAM_LENGTH_FULL    0x02000000UL
#define SDRAM_ADDR_BITS      24



//  Memory mapped PCI registers
#define PCI64_RSTSRC         0x01C00000UL
   #define RSTSRC_RSTN          0x00000001UL
   #define RSTSRC_PRSTN         0x00000002UL
   #define RSTSRC_WARMRST       0x00000004UL
   #define RSTSRC_INTREQ        0x00000008UL
   #define RSTSRC_INTRST        0x00000010UL
   #define RSTSRC_CFGDONE       0x00000020UL
   #define RSTSRC_CFGERR        0x00000040UL

#define PCI64_PMDCSR         0x01C00004UL // Not used on C64xx

#define PCI64_PCIIS          0x01C00008UL
   #define PCIIS_PWRMGMT        0x00000001UL
   #define PCIIS_PCITARGET      0x00000002UL
   #define PCIIS_PCIMASTER      0x00000004UL
   #define PCIIS_HOSTSW         0x00000008UL
   #define PCIIS_PWRLH          0x00000010UL
   #define PCIIS_PWRHL          0x00000020UL
   #define PCIIS_MASTEROK       0x00000040UL
   #define PCIIS_CFGDONE        0x00000080UL
   #define PCIIS_CFGERR         0x00000100UL
   #define PCIIS_EERDY          0x00000200UL
   #define PCIIS_PRSTN          0x00000800UL
   #define PCIIS_DMAHALTED      0x00001000UL

#define PCI64_PCIIEN         0x01C0000CUL
   #define PCIIEN_PWRMGMT       0x00000001UL
   #define PCIIEN_PCITARGET     0x00000002UL
   #define PCIIEN_PCIMASTER     0x00000004UL
   #define PCIIEN_HOSTSW        0x00000008UL
   #define PCIIEN_PWRLH         0x00000010UL
   #define PCIIEN_PWRHL         0x00000020UL
   #define PCIIEN_MASTEROK      0x00000040UL
   #define PCIIEN_CFGDONE       0x00000080UL
   #define PCIIEN_CFGERR        0x00000100UL
   #define PCIIEN_EERDY         0x00000200UL
   #define PCIIEN_PRSTN         0x00000800UL

#define PCI64_DSPMA          0x01C00010UL
#define PCI64_PCIMA          0x01C00014UL
#define PCI64_PCIMC          0x01C00018UL
#define PCI64_CDSPA          0x01C0001CUL
#define PCI64_CPCIA          0x01C00020UL
#define PCI64_CCNT           0x01C00024UL
#define PCI64_HALT           0x01C00028UL // Not used on C64xx


// PCI  I/O Registers
#define PCI64_HSR            0x01C1FFF0UL
   #define HSR_INTSRC           0x00000001UL
   #define HSR_INTAVAL          0x00000002UL
   #define HSR_INTAM            0x00000004UL
   #define HSR_CFGERR           0x00000008UL
   #define HSR_EEREAD           0x00000010UL

#define PCI64_HDCR           0x01C1FFF4UL
   #define HDCR_WARMRESET       0x00000001UL
   #define HDCR_DSPINT          0x00000002UL
   #define HDCR_PCIBOOT         0x00000004UL

#define PCI64_DSPP           0x01C1FFF8UL




#define PCI64_EEADD          0x01C20000UL
#define PCI64_EEDAT          0x01C20004UL
#define PCI64_EECTL          0x01C20008UL

#endif 

