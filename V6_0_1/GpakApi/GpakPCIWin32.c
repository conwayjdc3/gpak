/*
 * Copyright (c) 2009, Adaptive Digital Technologies, Inc.
 *
 * File Name: GpakPCIWin32.c
 *
 * Description:
 *   This file contains host system dependent functions to support generic
 *   G.PAK API functions on the TI DM642 and EVM6424 DSPs. The file is integrated into 
 *   the host processor connected to G.PAK DSPs via a PCI bus.
 *
 *
 *
 */
#ifdef _WIN32
    #if (_MSC_VER >= 900)
       #define   WIN32_LEAN_AND_MEAN  1
       #define   INC_OLE2
       #define   NOSERVICE
    #endif 

    #include <windows.h>
    #include <stdio.h>
    #include <conio.h>   
#endif

#include "time.h"

#include "process.h"
#include "objbase.h"
#include "64.h"
#include "6424.h"
#include "sd_pci_lib.h"
#include "gpakcust.h"

#include "adt_typedef.h"


#ifndef MULTIPLATFORM
#error MULTIPLATFORM must be defined when including this module in a project 
#endif
extern ADT_UInt32 ifBlkAddress;

static HMODULE dll = NULL;
static struct {
   int type;          // Board type
   ADT_UInt32 reg;    // Address with id unique to device
   ADT_UInt32 num;    // Unigue value
} pciBoards[] = { 
   { 642,  0x01B3F008, 0x0007902F },   // JTag part and manufacturer
   { 6424, 0x01C1A100, 0xB001104C },   // PCI vendor and device id
   { 0,     0 }
};  // DSP boards currently supported

static int NotInitialized = TRUE;

PCI64_HANDLE PCIHandle [MAX_DSP_CORES];

int PCIDsp = 0;                      // DSP type PCI is connected to
static ADT_UInt32 PCIBOARDREG = 0;


// Interrupt management
#define ONE_ONLY FALSE
void (*ProcessDSPInterrupts)(int DSP) = NULL;   // User function to handle interrupts
static HANDLE       CallbackEvents [MAX_DSP_CORES];
static ADT_Int32    DSPOwner       [MAX_DSP_CORES];
static DWORD        DSPCount = 0;  


long InterruptsIssued   = 0;
long InterruptsReceived = 0;

/* ------------------------------------------------------------------------ *
 *                                                                          *
 *  Setup_Cache( )                                                          *
 *      Invalidate old cache and setup cache for operation                  *
 *                                                                          *
 * ------------------------------------------------------------------------ */
void Setup_Cache6424(ADT_Int32 DspId)
{
	int Value;
	PCI64_HANDLE hPci = PCIHandle[DspId];
    #define CACHE_L2CFG         *( unsigned int* )( 0x01840000 )
    #define CACHE_L2INV         *( unsigned int* )( 0x01845008 )
    #define CACHE_L1PCFG        *( unsigned int* )( 0x01840020 )
    #define CACHE_L1PINV        *( unsigned int* )( 0x01845028 )
    #define CACHE_L1DCFG        *( unsigned int* )( 0x01840040 )
    #define CACHE_L1DINV        *( unsigned int* )( 0x01845048 )

    Value= 1; PCI64_RegWrite (hPci, CACHE_L1PINV, &Value); //CACHE_L1PINV = 1;           // L1P invalidated
    Value= 7; PCI64_RegWrite (hPci, CACHE_L1PCFG, &Value); //CACHE_L1PCFG = 7;           // L1P on, MAX size
    Value= 1; PCI64_RegWrite (hPci, CACHE_L1DINV, &Value); //CACHE_L1DINV = 1;           // L1D invalidated
    Value= 7; PCI64_RegWrite (hPci, CACHE_L1DCFG, &Value); //CACHE_L1DCFG = 7;           // L1D on, MAX size
    Value= 1; PCI64_RegWrite (hPci, CACHE_L2INV, &Value); //CACHE_L2INV  = 1;           // L2 invalidated
    Value= 0; PCI64_RegWrite (hPci, CACHE_L1PINV, &Value); //CACHE_L2CFG  = 0;           // L2 off, use as RAM
}

/* ------------------------------------------------------------------------ *
 *                                                                          *
 *  Disable_EDMA( )                                                         *
 *      Disabe EDMA events and interrupts, clear any pending events         *
 *                                                                          *
 * ------------------------------------------------------------------------ */
void Disable_EDMA6424(ADT_Int32 DspId )
{
	int Value;
	PCI64_HANDLE hPci = PCIHandle[DspId];
    #define EDMA_3CC_IECRH          *( int* )( 0x01C0105C )
    #define EDMA_3CC_EECRH          *( int* )( 0x01C0102C )
    #define EDMA_3CC_ICRH           *( int* )( 0x01C01074 )
    #define EDMA_3CC_ECRH           *( int* )( 0x01C0100C )

    #define EDMA_3CC_IECR           *( int* )( 0x01C01058 )
    #define EDMA_3CC_EECR           *( int* )( 0x01C01028 )
    #define EDMA_3CC_ICR            *( int* )( 0x01C01070 )
    #define EDMA_3CC_ECR            *( int* )( 0x01C01008 )

    Value= 0xFFFFFFFF; PCI64_RegWrite (hPci, EDMA_3CC_IECRH, &Value); //EDMA_3CC_IECRH  = 0xFFFFFFFF;   // IERH ( disable high interrupts )
    Value= 0xFFFFFFFF; PCI64_RegWrite (hPci, EDMA_3CC_EECRH, &Value); //EDMA_3CC_EECRH  = 0xFFFFFFFF;   // EERH ( disable high events )
    Value= 0xFFFFFFFF; PCI64_RegWrite (hPci, EDMA_3CC_ICRH, &Value); //EDMA_3CC_ICRH   = 0xFFFFFFFF;   // ICRH ( clear high interrupts )
    Value= 0xFFFFFFFF; PCI64_RegWrite (hPci, EDMA_3CC_ECRH, &Value); //EDMA_3CC_ECRH   = 0xFFFFFFFF;   // ICRH ( clear high events )

    Value= 0xFFFFFFFF; PCI64_RegWrite (hPci, EDMA_3CC_IECR, &Value); //EDMA_3CC_IECR   = 0xFFFFFFFF;   // IER  ( disable low interrupts )
    Value= 0xFFFFFFFF; PCI64_RegWrite (hPci, EDMA_3CC_EECR, &Value); //EDMA_3CC_EECR   = 0xFFFFFFFF;   // EER  ( disable low events )
    Value= 0xFFFFFFFF; PCI64_RegWrite (hPci, EDMA_3CC_ICR, &Value); //EDMA_3CC_ICR    = 0xFFFFFFFF;   // ICR  ( clear low interrupts )
    Value= 0xFFFFFFFF; PCI64_RegWrite (hPci, EDMA_3CC_ECR, &Value); //EDMA_3CC_ECR    = 0xFFFFFFFF;   // ICRH ( clear low events )
}

/****************************************************************************
* NAME:  PCILibOpen
*
* DESCRIPTION:  Opens dll library according to the dspType 
*         
* Inputs
*     dspType  -  642 or 6424
*
* RETURN VALUE:
*    dllHandle - NULL if failure
*
*F***************************************************************************/
static HMODULE PCILibOpen (int dspType) {

   HMODULE dll = NULL;

   if (dspType == 6424) {
      dll =  LoadLibrary ("sd_pci_6424.dll");
      ifBlkAddress = 0x10800000;
   } else {
      dll =  LoadLibrary ("sd_pci_dm642.dll");
      ifBlkAddress =0x0200;
   }       
   if (dll == NULL) {
      printf ("PCI load library failed\n");
      return NULL;
   }
   if (NotInitialized) {
      memset (PCIHandle,      0, sizeof (PCIHandle));
      memset (DSPOwner,       0, sizeof (DSPOwner));
      memset (CallbackEvents, 0, sizeof (CallbackEvents));
      NotInitialized = FALSE;
   }

   PCIDsp = dspType;

   PCI64_Open  = (PCIOpen)  GetProcAddress (dll, "PCI64_Open");
   PCI64_Close = (PCIClose) GetProcAddress (dll, "PCI64_Close");

   PCI64_MemFill32  = (PCIXfer) GetProcAddress (dll, "PCI64_MemFill32");
   PCI64_MemWrite32 = (PCIXfer) GetProcAddress (dll, "PCI64_MemWrite32");
   PCI64_MemRead32  = (PCIXfer) GetProcAddress (dll, "PCI64_MemRead32");
   PCI64_RegWrite32 = (PCIXfer) GetProcAddress (dll, "PCI64_RegWrite32");
   PCI64_RegRead32  = (PCIXfer) GetProcAddress (dll, "PCI64_RegRead32"); 

   PCI64_LoadCoff           = (PCILoadCoff) GetProcAddress (dll, "PCI64_LoadCoff"); 
   PCI64_GetCallbackEvent   = (PCICbHndl)   GetProcAddress (dll, "PCI64_GetCallbackEvent"); 
   PCI64_SetupCallbackEvent = (PCICbEvent)  GetProcAddress (dll, "PCI64_SetupCallbackEvent");
   return dll;
}

/****************************************************************************
* NAME:  PCILibClose
*
* DESCRIPTION:  Closes PCI dll library
*         
* Inputs
*     dll  -  Handle for PCI library
*
*F***************************************************************************/
static PCILibClose () {
   FreeLibrary (dll);
   PCIDsp = 0;
}

/****************************************************************************
* NAME:  PCI64_RegRead ()
*
* DESCRIPTION:  Handles register reads that are outside of the PCI area 
*               (01c0 0000 .. 01ff ffff) the Spectrum Digital has mapped out
*               for registers 
*         
* Inputs
*     hPci  -  Handle for PCI access
*     reg   -  Register address
*    value  -  pointer to store register value
*
* RETURN VALUE:
*    0 Success, non-0 for Fail
*
*F***************************************************************************/
static ADT_Int32 PCI64_RegRead  (PCI64_HANDLE hPci, ADT_UInt32 reg, ADT_UInt32 *value) {

    if (hPci == NULL) return -1;

    if (0x01c00000 <= reg && reg < 0x02000000)
        return PCI64_RegRead32 (hPci, reg, 1, value);
    else
        return PCI64_MemRead32 (hPci, reg, 1, value);
}

/****************************************************************************
* NAME:  PCI64_RegWrite ()
*
* DESCRIPTION:  Handles register writes that are outside of the PCI area 
*               (01c0 0000 .. 01ff ffff) the Spectrum Digital has mapped out
*               for registers 
*         
* Inputs
*     hPci  -  Handle for PCI access
*     reg   -  Register address
*    value  -  pointer to store register value
*
* RETURN VALUE:
*    0 Success, non-0 for Fail
*
*F***************************************************************************/
static ADT_Int32 PCI64_RegWrite (PCI64_HANDLE hPci, ADT_UInt32 reg, ADT_UInt32 *value) {

    if (hPci == NULL) return -1;

    if (0x01c00000 <= reg && reg < 0x02000000)
        return PCI64_RegWrite32 (hPci, reg, 1, value);
    else
        return PCI64_MemWrite32 (hPci, reg, 1, value);
}



//{ 642 specific routines

/****************************************************************************
* NAME:  EmifInit_642 ()
*
* DESCRIPTION:  Init the EMIF.  Just handles EMIF-A.
*
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/
static char *EmifInit642 (PCI64_HANDLE hPci) {

    ADT_Bool success;

    ADT_UInt32 EmifA[12] = { 0x00052078UL,     // GCTL     - 0x01800000
                         0x73A28E01UL,        // CE1      - 0x01800004
                         0xFFFFFFD3UL,        // CE0      - 0x01800008
                         // Address split 3
                         0x22a28a22UL,        // CE2      - 0x01800010
                         0x22a28a42UL,        // CE3      - 0x01800014
                         0x57115000UL,        // SDRAMCTL - 0x01800018
                         0x0000081bUL,        // SDRAMTIM - 0x0180001c
                         0x001faf4dUL,        // SDRAMEXT - 0x01800020
                         // Address split 9
                         0x00000002UL,        // CE1ECCTL - 0x01800044
                         0x00000002UL,        // CE0ECCTL - 0x01800048
                         // Address split 
                         0x00000002UL,        // CE2ECCTL - 0x01800050
                         0x00000073UL,        // CE3ECCTL - 0x01800054
                        };

    if (hPci == NULL) return "EMIF Init failure. PCI Bus not open";

    success  = !PCI64_MemWrite32 (hPci, 0x01800000UL, 3, &EmifA[0]);
    success &= !PCI64_MemWrite32 (hPci, 0x01800010UL, 5, &EmifA[3]);
    success &= !PCI64_MemWrite32 (hPci, 0x01800048UL, 2, &EmifA[8]);
    success &= !PCI64_MemWrite32 (hPci, 0x01800050UL, 2, &EmifA[10]);
    if (success) return NULL;

    return "EMIF Init failure. Write error.";
}

/****************************************************************************
* NAME: IsPciBootMode642 
*
* DESCRIPTION:  Tests for PCI boot mode.  If not in PCI boot mode then
*               should NOT attempt doing PCI boot.
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*  NULL - PCI Boot mode enabled
*  Other - reason not enabled
*
****************************************************************************/
static char *IsPciBootMode642 (PCI64_HANDLE hPci) {

    ADT_UInt32 RegHdcr = 0;

    if (hPci == NULL) return "NULL Handle";

    if (PCI64_RegRead32 (hPci, PCI64_HDCR, 1, &RegHdcr))
       return "HDCR Register Read Error";

    if (RegHdcr == HDCR_PCIBOOT) return NULL;
    return "DSP not in PCI boot mode";
}

/****************************************************************************
* NAME:  AssertDspReset642 ()
*
* DESCRIPTION:  Put DSP in reset
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    0 Success, non-0 for Fail
*
*F***************************************************************************/
static char *AssertDspReset642 (PCI64_HANDLE hPci) {
    ADT_UInt32 RegHdcr;
    ADT_Int32  Error;

    if (hPci == NULL) return "DSP reset failure. PCI not open";

    Error = PCI64_RegRead32 (hPci, PCI64_HDCR, 1, &RegHdcr);
    if (Error)
        return "DSP reset failure. HDCR register read failure";

    RegHdcr |= HDCR_WARMRESET;  // Place DSP in warm reset
    Error = PCI64_RegWrite32 (hPci, PCI64_HDCR, 1, &RegHdcr);
    if (Error)
        return "DSP reset failure. HDCR register write failure";
    
    Sleep (2);
    return NULL;
}

/****************************************************************************
* NAME: Release64 () 
*
* DESCRIPTION:  Release DSP from  reset and start code exection from address 0x00000000.
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    NULL Success,  Failure message
*
*F***************************************************************************/
static char *Release642 (PCI64_HANDLE hPci) {
    ADT_UInt32 RegHdcr;
    ADT_Int32  Error;

    if (hPci == NULL) return "DSP Release failure. Null Handle";

    Error = PCI64_RegRead32 (hPci, PCI64_HDCR, 1, &RegHdcr);
    if (Error)
        return "DSP Read HDCR Register failure";

    RegHdcr |= HDCR_DSPINT;   //  Generate DSP interrupt to release DSP
    Error = PCI64_RegWrite32 (hPci, PCI64_HDCR, 1, &RegHdcr);
    if (Error) 
       return "DSP Write HDCR Register failure";

    return NULL;
}

/****************************************************************************
* NAME:  GenerateInterruptTo642 ()
*
* DESCRIPTION:  Generate interrupts to DSP via PCI 
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    Nothing
*
****************************************************************************/
static void GenerateInterruptTo642 (PCI64_HANDLE hPci) {

   ADT_UInt32          Hsr;

   if (hPci == NULL)  return;

   Hsr = HDCR_DSPINT;   // Enable interrupts
   PCI64_RegWrite32 (hPci, PCI64_HDCR, 1, &Hsr);

   return;
}

/****************************************************************************
* NAME:  DisableInterruptFrom642 ()
*
* DESCRIPTION:  Disable interrupts from DSP via PCI 
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/
static char *DisableInterruptsFrom642 (PCI64_HANDLE hPci) {

    ADT_UInt32          Hsr;

    if (hPci == NULL) return "DSP Release failure. Null Handle";

 
    if (PCI64_RegRead32 (hPci, PCI64_HSR, 1, &Hsr))
       return "Unable to read HSR";

    Hsr |= HSR_INTAM;    // Disable interrupt
    if (PCI64_RegWrite32 (hPci, PCI64_HSR, 1, &Hsr))
       return "Unable to disable interrupts";

    return NULL;
}

/****************************************************************************
* NAME:  EnableInterruptFrom642 ()
*
* DESCRIPTION:  Enable interrupts from DSP via PCI 
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/
static char *EnableInterruptsFrom642 (PCI64_HANDLE hPci) {

   ADT_UInt32          Hsr, Pciis;

    if (hPci == NULL) return "DSP Release failure. Null Handle";

 
    if (PCI64_RegRead32 (hPci, PCI64_HSR, 1, &Hsr))
       return "Unable to read HSR";

    if (PCI64_RegRead32 (hPci, PCI64_PCIIS, 1, &Pciis))
       return "Unable to read PCIIS";

    Hsr |= HSR_INTSRC;   // Clear pending interrupts
    if (PCI64_RegWrite32 (hPci, PCI64_HSR, 1, &Hsr))
       return "Unable to clear pending interrupts";


    Hsr &= ~HSR_INTAM;   // Enable interrupt
    if (PCI64_RegWrite32 (hPci, PCI64_HSR, 1, &Hsr))
       return "Unable to enable DSP interrupts";

    if (PCI64_RegWrite32 (hPci, PCI64_PCIIS, 1, &Pciis))
       return "Unable to clear PCIIS";

    return NULL;
}

//}

//{ 6424 specific routines
/****************************************************************************
 *                                                                          *
 *  psc_change_state_6424 (id, state)                                            *
 *      id    = Domain #ID                                                  *
 *      state = (ENABLE, DISABLE, SYNCRESET, RESET)                         *
 *              (  =3  ,   =2   ,    =1    ,   =0 )                         *
 *                                                                          *
 ************************************************************************** */
void psc_change_state_6424 (PCI64_HANDLE hPci, int id, int state) {

    ADT_UInt32 Value;
    ADT_UInt32 mdstat = 0x01c41800UL + (4 * id);
    ADT_UInt32 mdctl  = 0x01c41a00UL + (4 * id);
    int try = 0;

    
    //  Step 0 - Ignore request if the state is already set as is
    PCI64_RegRead  (hPci, mdstat, &Value);
    if ((Value & 0x1f) == (ADT_UInt32) state)
        return;

    //  Step 1 - Wait for PTSTAT.GOSTAT to clear
    do {
        PCI64_RegRead  (hPci, PSC_PTSTAT, &Value);
        Sleep (10);
    } while ((Value & 1) && try++<10);
    if (10 <= try) {
      printf ("\nPower module [%d] status clear %x failure\n", id, Value);
      return;
    }

    //  Step 2 - Set MDCTLx.NEXT to new state
    PCI64_RegRead  (hPci, mdctl, &Value);
    Value &= ~0x1f;
    Value |= state;
    PCI64_RegWrite (hPci, mdctl, &Value);

    //  Step 3 - Start power transition (set PTCMD.GO to 1)
    Value = 1;
    PCI64_RegWrite (hPci, PSC_PTCMD, &Value);

    //  Step 4 - Wait for PTSTAT.GOSTAT to clear
    try=0;
    do {
        PCI64_RegRead  (hPci, PSC_PTSTAT, &Value);
        Sleep (10);
    } while ((Value & 1) && try++<10);
    if (10 <= try) {
      printf ("\nPower module [%d] status transition timeout\n", id);
      return;
    }

    //  Step 5 - Verify state changed
    try=0;
    do {
        PCI64_RegRead  (hPci, mdstat, &Value);
        Sleep (10);
    } while (((Value & 0x1f) != (ADT_UInt32) state) && try++<100);
    if (100 <= try) {
      printf ("\nPower module [%d] state change failure\n", id);
      return;
    }
}

/****************************************************************************
 *                                                                          *
 *  set_pll1_6424 ()                                                          *
 *                                                                          *
 *      clock_source    <- 0: Onchip Oscillator                             *
 *                         1: External Clock                                *
 *                                                                          *
 *      pll_mult        <- 21: 22x Multiplier * 27MHz Clk = 594 MHz         *
 *                                                                          *
 * ------------------------------------------------------------------------ */
static void set_pll1_6424 (PCI64_HANDLE hPci, ADT_UInt32 clock_source, ADT_UInt32 pll1_freq) {
   ADT_UInt32 Value, ctl_v, pllm_v, div1_v, div2_v, div3_v;
   ADT_Bool divisor_match;

   int power_up_pll, retries = 0;

   ADT_UInt32 pll_mult = (pll1_freq / 27) - 1;

   
   PCI64_RegRead (hPci, pll1_ctl,  &ctl_v);
   PCI64_RegRead (hPci, pll1_pllm, &pllm_v);
   PCI64_RegRead (hPci, pll1_div1, &div1_v);
   PCI64_RegRead (hPci, pll1_div2, &div2_v);
   PCI64_RegRead (hPci, pll1_div3, &div3_v);
   
   divisor_match = ((div1_v & 0x1f) == 0) &&
                   ((div2_v & 0x1f) == 2) &&
                   ((div3_v & 0x1f) == 5);
   if ((((ctl_v & 0x0100) >> 8) == clock_source)  &&
        ((pllm_v & 0x3f) == (pll_mult & 0x3f))    &&
        divisor_match) {
     return;
   }
   power_up_pll = (ctl_v & 0x0002) >> 1;

   //  Step 1 - Set clock mode
   if (power_up_pll == 1)   {
      if (clock_source == 0)
         ctl_v &= ~0x0100;    // Onchip Oscillator
      else
         ctl_v |= 0x0100;     // External Clock
      PCI64_RegWrite (hPci, pll1_ctl, &ctl_v);
   }

   //  Step 2 - Set PLL to bypass- Wait for PLL to stabilize (150 us)
   ctl_v &= ~0x0021;
   PCI64_RegWrite (hPci, pll1_ctl, &ctl_v);
   Sleep (1);

   //  Step 3 - Reset PLL
   ctl_v &= ~0x0008;
   PCI64_RegWrite (hPci, pll1_ctl, &ctl_v);

    
   //  Step 4 - Disable PLL
   //  Step 5 - Powerup PLL
   //  Step 6 - Enable PLL
   //  Step 7 - Wait for PLL to stabilize
   if (power_up_pll == 1) {
      ctl_v |= 0x0010;         // Disable PLL
      PCI64_RegWrite (hPci, pll1_ctl, &ctl_v);

      ctl_v &= ~0x0002;        // Power up PLL
      PCI64_RegWrite (hPci, pll1_ctl, &ctl_v);

      ctl_v &= ~0x0010;        // Enable PLL
      PCI64_RegWrite (hPci, pll1_ctl, &ctl_v);
   } else {
      ctl_v &= ~0x0010;        // Enable PLL
      PCI64_RegWrite (hPci, pll1_ctl, &ctl_v);
   }
   Sleep (1);             // Wait for PLL to stabilize (150 us)

   //  Step 8 - Load PLL multiplier 
   pllm_v = pll_mult & 0x3f;
   PCI64_RegWrite (hPci, pll1_pllm, &pllm_v);

   //  Step 9 - Load PLL dividers (must be in a 1/3/6 ratio)
   //           1:DSP, 2:SCR,EMDA, 3:Peripherals
   Value = 0x8000 | 0;
   PCI64_RegWrite (hPci, pll1_div1, &Value);

   Value = 0x8000 | 2;
   PCI64_RegWrite (hPci, pll1_div2, &Value);

   Value = 0x8000 | 5;
   PCI64_RegWrite (hPci, pll1_div3, &Value);
   
   PCI64_RegRead (hPci, pll1_cmd, &Value);
   Value |= 0x0001;             // Set phase alignment
   PCI64_RegWrite (hPci, pll1_cmd, &Value);

   do {
      PCI64_RegRead (hPci, pll1_stat, &Value);
      Sleep (1);
   } while (((Value & 1) != 0) & (retries++ < 100));// Wait for phase alignment

    
   //  Step 10 - Wait for PLL to reset (128 CLKIN cycles)
   Sleep (1);

   //  Step 11 - Release from reset
   PCI64_RegRead (hPci, pll1_ctl, &ctl_v);
   ctl_v |= 0x0008;
   PCI64_RegWrite (hPci, pll1_ctl, &ctl_v);
    
   //  Step 12 - Wait for PLL to re-lock (2000 CLKIN cycles)
   Sleep (1);

   //  Step 13 - Switch out of BYPASS mode
   PCI64_RegRead (hPci, pll1_ctl, &ctl_v);
   ctl_v |= 0x0001;
   PCI64_RegWrite (hPci, pll1_ctl, &ctl_v);

   PCI64_RegRead (hPci, pll1_pllm, &pllm_v);
   PCI64_RegRead (hPci, pll1_div1, &div1_v);
   PCI64_RegRead (hPci, pll1_div2, &div2_v);
   PCI64_RegRead (hPci, pll1_div3, &div3_v);

   pll1_freq = 27 * ((pllm_v & 0x3f) + 1);
   div1_v = (div1_v & 0x1f);
   div2_v = (div2_v & 0x1f);
   div3_v = (div3_v & 0x1f);

   printf ("DSP = %d MHz\n", pll1_freq / (div1_v + 1));
   printf ("SYSCLK2 = %d MHz\n", pll1_freq / (div2_v + 1));
   printf ("SYSCLK3 = %d MHz\n", pll1_freq / (div3_v + 1));
}

/****************************************************************************
 *                                                                          *
 *  set_pll2_6424 ()                                                          *
 *                                                                          *
 *      clock_source    <- 0: Onchip Oscillator                             *
 *                         1: External Clock                                *
 *                                                                          *
 *      pll_mult        <- PLL Multiplier                                   *
 *                         23: 24x Multiplier * 27MHz Clk = 648 MHz         *
 *                                                                          *
 *      ddr2_div        <- DDR2 divider (For PLL2)                          *
 *                         1: 648 MHz Clk / (2*2)x Divider = 162 MHz        *
 *                                                                          *
 * ------------------------------------------------------------------------ */
static void set_pll2_6424 (PCI64_HANDLE hPci, ADT_UInt32 clock_source, ADT_UInt32 pll2_freq, 
                                              ADT_UInt32 ddr2_freq) {

   ADT_UInt32 Value, ctl_v, pllm_v, div1_v;
   ADT_UInt32 pll_mult = (pll2_freq / 27) - 1;
   ADT_UInt32 ddr2_div = (pll2_freq / (ddr2_freq * 2)) - 1; 

   int power_up_pll, retries=0;
   
   
   //  Step 0 - Ignore request if the PLL is already set as is
   PCI64_RegRead (hPci, pll2_ctl,  &ctl_v);
   PCI64_RegRead (hPci, pll2_pllm, &pllm_v);
   PCI64_RegRead (hPci, pll2_div1, &div1_v);
   if ((((ctl_v & 0x0100) >> 8) == clock_source)  &&
        ((pllm_v & 0x3f) == (pll_mult & 0x3f))    &&
        ((div1_v & 0x1f) == (ddr2_div & 0x1f))) {
     return;
   }
   power_up_pll = (ctl_v & 0x0002) >> 1;

   //  Step 1 - Set clock mode
   if (power_up_pll == 1)   {
      if (clock_source == 0)
         ctl_v &= ~0x0100;    // Onchip Oscillator
      else
         ctl_v |= 0x0100;     // External Clock
      PCI64_RegWrite (hPci, pll2_ctl, &ctl_v);
   }

   //  Step 2 - Set PLL to bypass         - Wait for PLL to stabilize (150 us)
   ctl_v &= ~0x0021;
   PCI64_RegWrite (hPci, pll2_ctl, &ctl_v);
   Sleep (1);

   //  Step 3 - Reset PLL
   ctl_v &= ~0x0008;
   PCI64_RegWrite (hPci, pll2_ctl, &ctl_v);

   //  Step 4 - Disable PLL
   //  Step 5 - Powerup PLL
   //  Step 6 - Enable PLL
   //  Step 7 - Wait for PLL to stabilize (150 us)
   if (power_up_pll == 1) {
      ctl_v |= 0x0010;         // Disable PLL
      PCI64_RegWrite (hPci, pll2_ctl, &ctl_v);

      ctl_v &= ~0x0002;        // Power up PLL
      PCI64_RegWrite (hPci, pll2_ctl, &ctl_v);

      ctl_v &= ~0x0010;        // Enable PLL
      PCI64_RegWrite (hPci, pll2_ctl, &ctl_v);
   } else {
      ctl_v &= ~0x0010;        // Enable PLL
      PCI64_RegWrite (hPci, pll2_ctl, &ctl_v);
   }
   Sleep (1);             // Wait for PLL to stabilize  (150 us)
   
   //  Step 8 - Load PLL multiplier 
   pllm_v = pll_mult & 0x3f;
   PCI64_RegWrite (hPci, pll2_pllm, &pllm_v);

   //  Step 9 - Load PLL dividers 
   //           1: PHY    BP: VTP
   Value = 0x8000 | 1;
   PCI64_RegWrite (hPci, pll2_bpdiv, &Value);

   Value = 0x8000 | (ddr2_div & 0x1f);
   PCI64_RegWrite (hPci, pll2_div1, &Value);
    
   PCI64_RegRead (hPci, pll2_cmd, &Value);
   Value |= 0x0001;             // Set phase alignment
   PCI64_RegWrite (hPci, pll2_cmd, &Value);
    
   do {
      PCI64_RegRead (hPci, pll2_stat, &Value);
   } while (((Value & 1) != 0) & (retries++ < 1000));// Wait for phase alignment

   //  Step 10 - Wait for PLL to reset (128 CLKIN cycles)
   Sleep (1);

   //  Step 11 - Release from reset
   PCI64_RegRead (hPci, pll2_ctl, &ctl_v);
   ctl_v |= 0x0008;
   PCI64_RegWrite (hPci, pll2_ctl, &ctl_v);


   //  Step 12 - Wait for PLL to re-lock (2000 CLKIN cycles)
   Sleep (1);

   //  Step 13 - Switch out of BYPASS mode
   PCI64_RegRead (hPci, pll2_ctl, &ctl_v);
   ctl_v |= 0x0001;
   PCI64_RegWrite (hPci, pll2_ctl, &ctl_v);

   PCI64_RegRead (hPci, pll2_pllm, &pllm_v);
   PCI64_RegRead (hPci, pll2_div1, &div1_v);
   pll2_freq = 27 * ((pllm_v & 0x3f) + 1);
   ddr2_freq = pll2_freq / (2 * ((div1_v & 0x1f) + 1));

   printf ("PLL2 = %d MHz DDR2 = %d MHz\n", pll2_freq, ddr2_freq);
}

/****************************************************************************
* NAME:  DDR2Init_6424 ()
*
* DESCRIPTION:  Init the DDR2.  
*
* Inputs
*     hPci  -  Handle for PCI access
*     freq  -  Memory speed
*     wide  -  0 = 16 bit,  1 = 32 bit
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/
static char *DDR2Init_6424 (PCI64_HANDLE hPci, int freq, int wide) {
    ADT_UInt32 Value;
    ADT_UInt32 pch_nch;

    int widthBit;

	if (wide) {
	   widthBit = 0;
    } else {
	   widthBit = 0x4000;
    }
    //  Step 1 - Setup PLL2
    //  Step 2 - Power on DDR2 
    psc_change_state_6424 (hPci, 13, 3);

    //  Step 3 - DDR2 Initialization
    Value = 0x50006405;
    PCI64_RegWrite (hPci, DDR_DDRPHYCR, &Value);     // DLL out of reset, powered, ReadLatency=5

    Value = 0x00138822 | widthBit;
    PCI64_RegWrite (hPci, DDR_SDBCR, &Value) ;         // DDR Bank: xx-bit bus, CAS=4,
                                                       // 4 banks, 1024-word pg
      // DDR Timing
    if (freq == 135)  {
       Value = 0x14492148;
       PCI64_RegWrite (hPci, DDR_SDTIMR, &Value);
       Value = 0x000bc702;
       PCI64_RegWrite (hPci, DDR_SDTIMR2, &Value);
    } else { // Default to 162 MHz
       Value = 0x16492148;
       PCI64_RegWrite (hPci, DDR_SDTIMR, &Value);
       Value = 0x000cc702;
       PCI64_RegWrite (hPci, DDR_SDTIMR2, &Value);
    }
    
    Value = 0x00130822 | widthBit;
    PCI64_RegWrite (hPci, DDR_SDBCR, &Value);      // DDR Bank: cannot modify
    Value = (ADT_UInt32) (freq * 78) / 10;
    PCI64_RegWrite (hPci, DDR_SDRCR, &Value);      // Refresh Control [ 7.8 usec * freq ]

    //  Step 4 - Dummy Read from DDR2
    PCI64_RegRead (hPci, 0x80000000UL, &Value);

    //  Step 5 - Soft Reset (SYNCRESET followed by ENABLE) of DDR2 PHY
    psc_change_state_6424 (hPci, 13, 1);
    psc_change_state_6424 (hPci, 13, 3);
      
    //  Step 6 - Enable VTP calibration
    Value = 0x201f;
    PCI64_RegWrite (hPci, DDR_VTPIOCR, &Value);
    Value = 0xa01f;
    PCI64_RegWrite (hPci, DDR_VTPIOCR, &Value);

    //  Step 7 - Wait for VTP calibration (33 VTP cycles)
    Sleep (1);

    //  Step 8 - Enable access to DDR VTP reg
    Value = 1;
    PCI64_RegWrite (hPci, DDR_DDRVTPER, &Value);

    //  Step 9 - Read P & N channels
    PCI64_RegRead  (hPci, DDR_DDRVTPR, &pch_nch);

    //  Step 10 - Set VTP fields PCH & NCH
    pch_nch &= 0x03ff;
    pch_nch |= 0xa000;
    PCI64_RegWrite (hPci, DDR_VTPIOCR, &pch_nch);

    //  Step 11 - Disable VTP calibaration
    PCI64_RegRead  (hPci, DDR_VTPIOCR, &Value);
    Value &= ~0x2000;
    PCI64_RegWrite (hPci, DDR_VTPIOCR, &Value);

    //          - Disable access to DDR VTP register
    Value = 0;
    PCI64_RegWrite (hPci, DDR_DDRVTPER, &Value);
    return NULL;
}

/****************************************************************************
* NAME: IsPciBootMode6424 
*
* DESCRIPTION:  Tests for PCI boot mode.  If not in PCI boot mode then
*               should NOT attempt doing PCI boot.
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*  NULL - PCI Boot mode enabled
*  Other - reason not enabled
*
****************************************************************************/
static char *IsPciBootMode6424 (PCI64_HANDLE hPci) {

   ADT_UInt32 Value, BootMode;

   PCI64_RegRead (hPci, BOOTCFG, &Value);
   
   BootMode = Value & 0xf;

   if (((Value & 0xA0000) != 0xA0000) || ((BootMode != 1) && (BootMode != 2))) {
      return "Boot mode not set for PCI";
   }
   return NULL;
}

/****************************************************************************
* NAME:  AssertDspReset6424 ()
*
* DESCRIPTION:  Put DSP in reset
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    0 Success, non-0 for Fail
*
*F***************************************************************************/
static char *AssertDspReset6424 (PCI64_HANDLE hPci) {
   ADT_UInt32 Value, try = 0;
   ADT_UInt32 Error;

   if (hPci == NULL) return "DSP reset failure. PCI not open";

   //  Clear boot loader DSP start address and BOOTCOMPLT values
   Value = 0; 
   Error = PCI64_RegWrite (hPci, BOOTCOMPLT, &Value);

   Value = 0;
   Error = PCI64_RegWrite (hPci, DSPBOOTADDR, &Value);
   if (Error)
        return "DSP Start Address write failure";
    
   Value = 0x003;
   PCI64_RegWrite (hPci, mdctl39, &Value);  // CPU enabled and in reset

   PCI64_RegRead (hPci, mdstat39, &Value);  // Check for completion
   while (((Value & 0x200) == 0) && (try++ < 10)) {
      Sleep (1);
      PCI64_RegRead (hPci, mdstat39, &Value);
   }

   Value = 0x103;
   PCI64_RegWrite (hPci, mdctl39, &Value);  // CPU enabled and out of reset

   Sleep (10);
   return NULL;
}

/****************************************************************************
* NAME: Release6424 () 
*
* DESCRIPTION:  Release DSP from  reset and start code exection from address 0x00000000.
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    NULL Success,  Failure message
*
*F***************************************************************************/
static char *Release6424 (PCI64_HANDLE hPci, ADT_UInt32 StartAddress) {
   ADT_UInt32 Value;
   ADT_Int32  Error, try = 0;

   if (hPci == NULL) return "DSP Release failure. Null Handle";

   Error = PCI64_RegRead (hPci, BOOTCOMPLT, &Value);
   if (Error)
        return "DSP Read BOOT Register failure";

   Error = PCI64_RegWrite (hPci, DSPBOOTADDR, &StartAddress);
   if (Error)
        return "DSP Start Address write failure";
    
   Sleep (10);
   Value |= 1;   //  Notify boot loader to branch to DSP start address
   Error = PCI64_RegWrite (hPci, BOOTCOMPLT, &Value);
   if (Error) 
      return "DSP Write BOOT Register failure";

   Sleep (10);
   Value = 0x103;
   PCI64_RegWrite (hPci, mdctl39, &Value);  // CPU enabled and out of reset

   PCI64_RegRead (hPci, ifBlkAddress, &Value);  // Wait for DSP to set block pointer
   while ((Value == StartAddress) && (try++ < 100)) {
      Sleep (10);
      PCI64_RegRead (hPci, ifBlkAddress, &Value);
   }
   if (100 <= try)
      return "DSP not responding";

   return NULL;
}

/****************************************************************************
* NAME:  GenerateInterruptTo6424 ()
*
* DESCRIPTION:  Generate interrupts to DSP via PCI 
*         
* Inputs
*     hPci  -  Handle for PCI access
*
*
****************************************************************************/
static void GenerateInterruptTo6424 (PCI64_HANDLE hPci) {

   ADT_UInt32          Hsr;

   if (hPci == NULL)  return;

   Hsr = DSP_INTS;   // Enable interrupts
   PCI64_RegWrite (hPci, PCI64_INT_GEN, &Hsr);
   return;
}

/****************************************************************************
* NAME:  EnableInterruptFrom6424 ()
*
* DESCRIPTION:  Enable interrupts from DSP via PCI 
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/
static char *EnableInterruptsFrom6424 (PCI64_HANDLE hPci) {

   ADT_UInt32          Hsr;

    if (hPci == NULL) return "DSP Release failure. Null Handle";


    Hsr = HOST_INTS;   // Enable interrupts
    if (PCI64_RegWrite (hPci, PCI64_HINT_CLR, &Hsr))
       return "Unable to enable DSP interrupts";

    Hsr = HOST_INTS;   // Enable interrupts
    if (PCI64_RegWrite (hPci, PCI64_HINT_SET, &Hsr))
       return "Unable to enable DSP interrupts";

    return NULL;
}

/****************************************************************************
* NAME:  DisableInterruptFrom6424 ()
*
* DESCRIPTION:  Disable interrupts from DSP via PCI 
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/
static char *DisableInterruptsFrom6424 (PCI64_HANDLE hPci) {

    ADT_UInt32          Hsr;

    if (hPci == NULL) return "DSP Release failure. Null Handle";

 
    Hsr = ALL_INTS;   // Enable interrupts
    if (PCI64_RegWrite (hPci, PCI64_HINT_CLR, &Hsr))
       return "Unable to disable interrupts";

    return NULL;
}

//}
static DWORD WINAPI DSPCallbackDummy (LPVOID dummy) { return 0; }

static DWORD WINAPI DSPCallback (LPVOID dummy) {
   ADT_UInt32 RegRst, Hsr;
   ADT_Int32  Error, Dsp;
   int EvntIdx;


   while (DSPCount != 0) {

      EvntIdx  = WaitForMultipleObjects (DSPCount, CallbackEvents, ONE_ONLY, 100);
      if (EvntIdx == WAIT_FAILED) break;
      if (MAX_DSP_CORES <= EvntIdx) continue;
            
      InterruptsReceived++;

      Dsp = DSPOwner[EvntIdx];
      
      // Clear interrupt
      if (PCIHandle[Dsp] == NULL) continue;

      if (PCIDsp == 642) {
         RegRst = RSTSRC_INTRST;    // Remove interrupt signal
         Error = PCI64_RegWrite32 (PCIHandle[Dsp], PCI64_RSTSRC, 1, &RegRst);
         if (Error) continue;

         Error = PCI64_RegRead32 (PCIHandle[Dsp], PCI64_HSR, 1, &Hsr);
         if (Error) continue;
         if (Hsr & HSR_INTSRC) {
            Hsr |= HSR_INTSRC;       // Clear pending interrupt
            PCI64_RegWrite32 (PCIHandle[Dsp], PCI64_HSR,1, &Hsr);
        }
      }
      if (ProcessDSPInterrupts)  (*ProcessDSPInterrupts)(Dsp);
   }

   return 0;
}

/****************************************************************************
* NAME:  EnableInterruptFromDSP ()
*
* DESCRIPTION:  Enable interrupts from DSP via PCI 
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/
static char *EnableInterruptsFromDSP (PCI64_HANDLE hPci) {

   if (PCIDsp == 642)
      return EnableInterruptsFrom642 (hPci);
   else if (PCIDsp == 6424);
      return EnableInterruptsFrom6424 (hPci);
}

/****************************************************************************
* NAME:  DisableInterruptFromDSP ()
*
* DESCRIPTION:  Disable interrupts from DSP via PCI 
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/
static char *DisableInterruptsFromDSP (PCI64_HANDLE hPci) {

   if (PCIDsp == 642)
      return DisableInterruptsFrom642  (hPci);
   else if (PCIDsp == 6424);
      return DisableInterruptsFrom6424 (hPci);
}

/****************************************************************************
* NAME:  AssertDspReset ()
*
* DESCRIPTION:  Put DSP in reset
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    0 Success, non-0 for Fail
*
*F***************************************************************************/
static char *AssertDspReset (PCI64_HANDLE hPci) {
   if (PCIDsp == 642)
      return  AssertDspReset642  (hPci);
   else if (PCIDsp == 6424);
      return  AssertDspReset6424 (hPci);
}

/****************************************************************************
* NAME:  Reset642 ()
*
* DESCRIPTION:  Reset and release DSP after establishing communications to DSP via PCI 
*         
* Inputs
*     hPci  -  PCI handle
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/
static char *Reset642  (PCI64_HANDLE hPci) {

	char *Error = NULL;

   Error = IsPciBootMode642 (hPci);   // Verify board allows PCI Boot
   if (Error != NULL) return Error;

   Error = AssertDspReset642 (hPci);  // Reset DSP
   if (Error != NULL) return Error;

   Error = EmifInit642 (hPci);     // Initialize external memory I/F registers
   if (Error != NULL) return Error;

   // Release DSP from reset
   Error = Release642 (hPci);
   if (Error != NULL) return Error;
   return NULL;

}

/****************************************************************************
* NAME:  Reset6424 ()
*
* DESCRIPTION:  Reset and release DSP after establishing communications to DSP via PCI 
*         
* Inputs
*     hPci  -  PCI handle
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/
static char *Reset6424 (PCI64_HANDLE hPci) {

	char *Error;

   Error = IsPciBootMode6424 (hPci);
   if (Error != NULL) return Error;

   Error = AssertDspReset6424 (hPci);  // Reset DSP
   if (Error != NULL) return Error;

   set_pll1_6424 (hPci, 0, 594);       // DSP  @ 594 MHz, SYSCLK2 @ 198 MHz, SYSCLK3 @ 99 MHz, Onchip Oscillator
   set_pll2_6424 (hPci, 0, 648, 162);  // PLL2 @ 648 MHz, DDR2 @ 162 MHz, Onchip Oscillator
   DDR2Init_6424 (hPci, 162, 1);       // DDR2 @ 162 MHz - 32 bit width
   return NULL;

}

/****************************************************************************
* NAME: ReleaseDSP () 
*
* DESCRIPTION:  Release DSP from  reset and start code exection from address 0x00000000.
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    NULL Success,  Failure message
*
*F***************************************************************************/
static char *ReleaseDSP (PCI64_HANDLE hPci, ADT_UInt32 StartAddress) {
   if (PCIDsp == 642)  return  Release642  (hPci);
   if (PCIDsp == 6424) return  Release6424 (hPci, StartAddress);
   return NULL;
}


//=======================================================================
//===========================  APIS  ====================================

/****************************************************************************
* NAME:  PCIConnect (Dsp)
*
* DESCRIPTION:  Establish communication to DSP via PCI 
*         
* Inputs
*     Dsp  -  Target DSP
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/
char *PCIConnect (ADT_Int32 Dsp) {
   PCI64_HANDLE pciHandle;
   ADT_UInt32 Value;
   int i;

   if (PCIDsp != 0) {
      Value = PCI64_Open (0, &pciHandle);
      if (Value != 0) 
         return "PCI open failure";
      PCIHandle[Dsp] = pciHandle;

      if (MAX_DSP_CORES <= DSPCount) return NULL;
      CallbackEvents[DSPCount] = PCI64_GetCallbackEvent (pciHandle);
      DSPOwner[DSPCount++]     = Dsp;
      PCI64_SetupCallbackEvent (pciHandle, DSPCallbackDummy);

      return NULL;
   }

   for (i=0; pciBoards[i].type!=0; i++) {
      dll = PCILibOpen (pciBoards[i].type);
      pciHandle = NULL;

      if (PCI64_Open (0, &pciHandle)) {    // Open new PCI connection
         if (pciHandle != NULL) PCI64_Close (pciHandle);
         PCILibClose (dll);
         continue;
      }
      Value = 0;
      if (PCI64_MemRead32 (pciHandle, pciBoards[i].reg, 1UL, &Value)) {
         PCILibClose (dll);
         continue;
      }
      if (Value != pciBoards[i].num) {
         PCILibClose (dll);
         continue;
      }

      // Found matching board.  Save PCI handle.
      PCIDsp = pciBoards[i].type;
      PCIHandle[Dsp] = pciHandle;

      if (MAX_DSP_CORES <= DSPCount) return NULL;

      // Get callback event object that matches DSP interrupt
      CallbackEvents[DSPCount] = PCI64_GetCallbackEvent (pciHandle);
      DSPOwner[DSPCount++]     = Dsp;
      PCI64_SetupCallbackEvent (pciHandle, DSPCallback);

      return EnableInterruptsFromDSP (pciHandle);
   }
   return "PCI bus open failure";  
}

/****************************************************************************
* NAME:  PCIDisconnect (Dsp)
*
* DESCRIPTION:  Disconnect DSP from PCI interface
*         
* Inputs
*     Dsp  -  Target DSP
*
*
****************************************************************************/
void PCIDisconnect (ADT_Int32 Dsp) {

   PCI64_HANDLE pciHandle;
   char *Error = NULL;
   DWORD i;

   if (NotInitialized) return;
   pciHandle = PCIHandle[Dsp];
   if (pciHandle == NULL) return;

   DisableInterruptsFromDSP (pciHandle);
   
   // Remove dsp interrupt event from event arrays
   for (i=0; i<DSPCount; i++) {
      if (DSPOwner[i] != Dsp) continue;
   
      DSPOwner[i]       = DSPOwner[DSPCount];
      CallbackEvents[i] = CallbackEvents[DSPCount];

      DSPOwner[DSPCount]         = -1;
      CallbackEvents[DSPCount--] = 0;
   }
   PCI64_Close (pciHandle);
   PCIHandle[Dsp] = NULL;
}

/****************************************************************************
* NAME:  PCIReset ()
*
* DESCRIPTION:  Reset and release DSP to start boot up
*         
* Inputs
*     Dsp  -  Target DSP
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/
char *PCIReset (ADT_Int32 Dsp) {
   
   char *Error = NULL;
   if (NotInitialized) return "PCI Not Initialized";

   if (PCIDsp == 642)  Error = Reset642  (PCIHandle[Dsp]);
   if (PCIDsp == 6424) Error = Reset6424 (PCIHandle[Dsp]);
   if (Error == NULL) return NULL;
   
   PCI64_Close (PCIHandle[Dsp]);
   return Error;
}

/****************************************************************************
* NAME:  PCIHalt ()
*
* DESCRIPTION:  Halts DSP by place DSP in reset mode
*         
* Inputs
*     Dsp  -  Target DSP
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/
char *PCIHalt (ADT_Int32 Dsp) {
   if (NotInitialized) return "PCI Not Initialized";
   return AssertDspReset (PCIHandle[Dsp]);  // Reset DSP
}

/****************************************************************************
* NAME:  PCIRelease (DSP)
*
* DESCRIPTION:  Release DSP from reset and identify DSP's initial entry point
*         
* Inputs
*     Dsp          -  Target DSP
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/
char *PCIRelease (ADT_Int32 Dsp) {

   ADT_UInt32 StartAddress;

   if (NotInitialized) return "PCI Not Initialized";

   PCI64_RegRead (PCIHandle[Dsp], ifBlkAddress, &StartAddress);

   return ReleaseDSP (PCIHandle[Dsp], StartAddress);  // Release DSP from reset
}

/****************************************************************************
* NAME:  GenerateInterruptToDSP ()
*
* DESCRIPTION:  Generate interrupts to DSP via PCI 
*         
* Inputs
*     Dsp  -  Target DSP
*
****************************************************************************/
void GenerateInterruptToDSP (ADT_Int32 Dsp) {
   if (NotInitialized) return;

   if (PCIDsp == 642)   GenerateInterruptTo642  (PCIHandle[Dsp]);
   if (PCIDsp == 6424)  GenerateInterruptTo6424 (PCIHandle[Dsp]);
   InterruptsIssued++;

}

/****************************************************************************
* NAME:  PCIDownloadCoff (DSP, Filename)
*
* DESCRIPTION:  Download coff file to DSP 
*         
* Inputs
*     Dsp  -  Target DSP
*    Filename - path and name of DSP immage Coff file
*
* RETURN VALUE:
*    GdlSuccess      - Success
*    GdlInvalidFile  - Failure
*
****************************************************************************/
int PCIDownloadCoff (ADT_Int32 Dsp, char *Filename) {

   if (NotInitialized) 4;
   
   if (!PCI64_LoadCoff (PCIHandle[Dsp], Filename))
      return 0;
   
   return 9;

}
