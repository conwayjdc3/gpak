// OmaL138ApiExample.c
//    Runs on L138 ARM.
//
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef LINUX
#include "linux_thread.h"
#include <semaphore.h>
#include <stdarg.h>
#include <time.h>
#include <unistd.h>
#include "GpakCust.h"
#else

#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/family/arm/arm9/Mmu.h>

#endif

#include "GpakApiTestSupport.h"
#include "GpakEnum.h"
#include "OmapL138Uart.h"
#include "omapl138_bsl.h"

// G.PAK custom API routine for initialization
extern void  initGpakStructures (void);
extern int gpakDspStartup (ADT_UInt32 DspId, char *pCoffFileName);

// ========================================
//{  Interactive support structures
//
//  channel type count defaults
chanCnts chns = { 
   0,   // tdmTdm
  16,   // tdmPkt
   0,   // pktPkt
   0,   // tdmCnfr
   0,   // pktCnfr
   0    // cmpCnfr
};

//  Channel configuration defaults
chanCfg DefaultChan = {
   CROSSOVER,   //  LOOPBACKMODE 
   Disabled,    //  PCM_EC
   Disabled,    //  PKT_EC
   Disabled,    //  AEC
   Disabled,    //  AGC
   Disabled,    //  VAD
   Disabled,    //  NOISE
   disabled,    //  FAX

   Frame_10ms,  // ENCSZ 
   Frame_10ms,  // DECSZ 

   0,           //  TONEA
   0,           //  TONEB

   PCMU_64,     //  ENCODE
   PCMU_64,     //  DECODE
   PCMU_64,     //  RTP_CODEC_PYLD
   CidDisabled, //  CID
   
   0,          // TG_GAIN
   0,          // IN_GAIN
   0,          // OUT_GAIN

   SerialPort1  // TDM_PORT
};
chanCfg ActiveChan;

//  Conference configuration defaults
cnfrCfg DefaultCnfr[] = {
   {Frame_5ms,  (GpakToneTypes) Null_tone},     // Frame size (PCM samples), tones
   {Frame_10ms,  (GpakToneTypes) Null_tone},    // Frame size (PCM samples), tones
   {Frame_20ms,  (GpakToneTypes) Null_tone},    // Frame size (PCM samples), tones
   {Frame_Unknownms}                            // Zero terminate list
};
cnfrCfg ActiveCnfr[5];
//}

ADT_Int32 BaseDestIP=0x0A000005;   // Base IP address of VoIP test phones
ADT_Int32 BaseMCastIP= 0xE1000000; // Base Multicast IP address for tra
ADT_Int32 BaseSSRC   = 0;
ADT_Int32 dspIP;                // Discovered IP address of DSP


// ========================================
//  Record/playback buffer details
DSP_Address PlayRecAddress = 0xc2B00000;  // Base address of recording buffers
int         PlayRecI8      = 0x00500000;  // Total length of recording buffers

// ========================================
// Default bit map of configured and active TDM slots
ADT_UInt32 activeSlots[4] = {     0, 0, 0, 0 };
ADT_UInt32 cfgSlots[4]    = { 0x7F01, 0, 0, 0 };
ADT_UInt16 leftSlt = 0, rightSlt = 1;

// host loopback mode
LoopbackMode hostLoopBack = DSP_LOOP;
LoopbackMode TDM_loopback = NO_LOOP;
int TDM_crossoverbit = 1;



// ========================================
//    Host specific variables
//=========================================

Task_Handle      evtTask, rtpTask, uiTask;
Semaphore_Handle evtSem,  rtpSem,  uartTxSem, uartRxSem;

static void uiThread  ();
static void evtThread ();
static void rtpThread ();

void (*rtpProcess)(ADT_UInt32 DSP) = NULL;

static int exitFlag = ADT_FALSE;
int uiStack [4090];
int evtStack[4096];
int rtpStack[4096];

//----------------------------------------------------
//  TDM serial port initialization
#ifdef PORT_CFG_MCBSP_64
void dspTDMInit (ADT_UInt32 DspId) {

   // ActiveChan.TDM_PORT configuration variables
   GpakPortConfig_t       PortConfig;
   GpakApiStatus_t        PrtCfgStat = GpakApiSuccess;
   GPAK_PortConfigStat_t  PrtDspStat = Cf_Success;

   dspTearDown (DspId);

   memset (&PortConfig, 0, sizeof (PortConfig));

   // Configure McBSP1, Disable McBSP0 and McBSP2
   PortConfig.AudioPort1Enable = Disabled;
   PortConfig.AudioPort2Enable = Disabled;

   // ActiveChan.TDM_PORT 0 (Attached to daughter card)
   PortConfig.Port1Enable  = Disabled;
   PortConfig.SlotsSelect1 = Disabled;
   PortConfig.SampRateGen1     = Disabled; 
   PortConfig.Port1SlotMask0_31  = 0x00FFFFFF;  // One bit for each 'active slot'
   PortConfig.Compand1          = cmpPCMU;
   PortConfig.SampRateGen1      = Enabled; 

   // ActiveChan.TDM_PORT 1 (Attached to AIC)
   PortConfig.SlotsSelect2 = Disabled;
   PortConfig.SampRateGen2 = Disabled; 

   PortConfig.Port2Enable  = Enabled;
   PortConfig.Port2SlotMask0_31 = 3;
   PortConfig.TxDataDelay2 = 1;
   PortConfig.RxDataDelay2 = 1;
   PortConfig.Compand2  = cmpNone16;

   // ActiveChan.TDM_PORT 2 (Not available)
   PortConfig.Port3Enable  = Disabled;
   PortConfig.SlotsSelect3 = Disabled;

   PrtCfgStat = gpakConfigurePorts (DspId, &PortConfig, &PrtDspStat);
   if (PrtCfgStat != CfsSuccess || PrtDspStat != Cf_Success) {
      ADT_printf ("ActiveChan.TDM_PORT setup failure %d:%d:%d\n", PrtCfgStat, PrtDspStat, DSPError [DspId]);
      appErr (__LINE__);
   }
}
#endif

#ifdef PORT_CFG_MCASP_64
void dspTDMInit (ADT_UInt32 DspId) {

   // ActiveChan.TDM_PORT configuration variables
   GpakPortConfig_t       PortConfig;
   GpakApiStatus_t        PrtCfgStat = GpakApiSuccess;
   GPAK_PortConfigStat_t  PrtDspStat = Pc_Success;

   dspTearDown (DspId);

   memset (&PortConfig, 0, sizeof (PortConfig));

   PortConfig.PortID          = SerialPort1;
   PortConfig.SlotsPerFrame   = sysCfg.Port1NumSlots;
   PortConfig.ActiveSlotMap   = cfgSlots[0];

   PortConfig.BitsPerSample   = 16;
   PortConfig.BitsPerSlot     = 16;
   PortConfig.InputPinMap     = 1 << 12;
   PortConfig.OutputPinMap    = 1 << 11; 


   PortConfig.HiResClockDivisor = 1;
   PortConfig.ClockDivisor      = 1;
   PortConfig.DataDelay         = 1;

   PortConfig.FsWord            = ADT_FALSE;
   PortConfig.FsFalling         = ADT_FALSE;
   PortConfig.ClkRxRising       = ADT_TRUE;
   PortConfig.HiResClockInvert  = ADT_FALSE;
   PortConfig.GenClocks         = ADT_FALSE; 
   PortConfig.SharedClk         = ADT_TRUE;

   PrtCfgStat = gpakConfigurePorts (DspId, &PortConfig, &PrtDspStat);
   if (PrtCfgStat != CfsSuccess || PrtDspStat != Cf_Success) {
      ADT_printf ("TDM_PORT setup failure %d:%d:%d\n", PrtCfgStat, PrtDspStat, DSPError [DspId]);
      appErr (__LINE__);
   }

}
#endif


//========================================================================
//{   User interface support
//========================================================================
#define UART_PORT 2
extern int _printfi(char **_format, va_list _ap, void *_op,
                    int (*_outc)(char, void *), int (*_outs)(char *, void *));

static int _outc (char c, void *_op) {
    return *(*((char **)_op))++ = c;
}

static int _outs (char *s, void *_op) {
    size_t len = strlen(s);

    memcpy(*((char **)_op), s, len);
    *((char **)_op) += len;
    return len;
}

char outBuff[500];

void inline rxLock () {
   if (uartRxSem == NULL) return;
   Semaphore_pend (uartRxSem, BIOS_WAIT_FOREVER);
}
void inline rxUnlock () {
    if (uartRxSem == NULL) return;
    Semaphore_post (uartRxSem);
}

void inline txLock () {
   if (uartTxSem == NULL) return;
   Semaphore_pend (uartTxSem, BIOS_WAIT_FOREVER);
}
void inline txUnlock () {
   if (uartTxSem == NULL) return;
   Semaphore_post (uartTxSem);
}

int ADT_printf (char *format, ...) {
   int count;
   va_list  params;
   char *fmtPtr = (char *) format;
   char *outBuffPtr = outBuff;

   if (format == NULL) return 0;

   va_start (params, format);
   count = _printfi (&fmtPtr, params, &outBuffPtr, _outc, _outs);
   va_end (params);

   *outBuffPtr = '\0';

   txLock ();
   UART_txString (UART_PORT, outBuff); 
   txUnlock ();
   return count;
}

void ADT_putchar (char key) {
   txLock ();
   UART_txByte (UART_PORT, key);
   txUnlock ();
}

char ADT_getchar (void) {
   ADT_UInt8  data;

   txLock ();
   rxLock ();
   while (UART_rxByte (UART_PORT, &data) == 0) {
      txUnlock ();  // Allow higher priority tasks to gain access to transmit data
      Task_yield ();
      txLock ();
   }
   rxUnlock ();
   txUnlock ();
   return data;
}

void ADT_gets (char *buffer, int bufI8) {
   ADT_UInt8 *data;
   int i;

   data = (ADT_UInt8 *) buffer;
   txLock ();
   rxLock ();
   for (i=0; i<bufI8; i++) {
      while (UART_rxByte (UART_PORT, data) == 0) {
         txUnlock ();
         Task_sleep (2);
         txLock ();
      }
      if (*data == '\r')  break;
      if (*data == '\n')  break;
      if (*data == 8) {   // Backspace
         if (i==0) continue;
         data--;
         i--;
         continue;
      }
      data++;
   }
   data++;
   *data = 0;
   rxUnlock ();
   txUnlock ();
}

int ADT_getint (char *prompt) {
   char inputBuffer[80];
   if (prompt) ADT_printf ("\n\r%s", prompt);
   ADT_gets (inputBuffer, sizeof (inputBuffer));
   return atoi (inputBuffer);
}

int getSlot (GpakSerialPort_t port) {
   int i, j;
   ADT_UInt32 availSlots;

   for (i=0; i < 3; i++) {
      // Special case for OMAP EVM.  If configured for 16 slots, allocate slot 8 first.
      if (activeSlots[i] == 0 && cfgSlots[i] == 0xffff) {
         activeSlots[i] = 0x100;
         return (i * 32) + 8;
      }
      if (cfgSlots[i] == 0) continue;
      if (cfgSlots[i] == activeSlots[i]) continue;
      availSlots = ~activeSlots [i] & cfgSlots [i];
      j = 0;
      while ((availSlots & 1) == 0) {
         availSlots = availSlots >> 1;
         j++;
      }
      activeSlots[i] |= 1 << j;  
      return (i * 32) + j;
   }
   return -1;
}
void freeSlot (GpakSerialPort_t port, int slot) {
   int idx, slotMask;
   
   idx = slot >> 5;
   slot = slot & 0x1f;
   slotMask = 1 << slot;
   activeSlots[idx] &= ~slotMask;
}
void freeAllSlots (GpakSerialPort_t port) {
   int i;
   for (i = 0; i < 4; i++) activeSlots[i] = 0;
}
//}

volatile int sleepCnt;
void ADT_sleep (int ms) {
   if (Task_self() == NULL) {
      sleepCnt = 0;
      while (sleepCnt++ < (10000 * ms));
      return;
   }
   Task_sleep (ms);
}



//========================================================================
//   Host application 
//========================================================================
//----------------------------------------------
//{  Threads
static void threadInit (void) {

   Task_Params taskParams;

   Task_Params_init (&taskParams);
   taskParams.priority  = 3;
   taskParams.stack     = rtpStack;
   taskParams.stackSize = sizeof (rtpStack);
   taskParams.instance->name = "rtp";
   rtpTask = Task_create (rtpThread, &taskParams, NULL);

   Task_Params_init (&taskParams);
   taskParams.priority  = 2;
   taskParams.stack     = evtStack;
   taskParams.stackSize = sizeof (evtStack);
   taskParams.instance->name = "evt";
   evtTask = Task_create (evtThread, &taskParams, NULL);

   /* Create rtpThread, evtThread, and userIO tasks */
   Task_Params_init (&taskParams);
   taskParams.priority  = 1;
   taskParams.stack     = uiStack;
   taskParams.stackSize = sizeof (uiStack);
   taskParams.instance->name = "ui";
   uiTask = Task_create (uiThread, &taskParams, NULL);
}

//----------------------------------------------
//  Thread deletion
static void threadStop (void) {
   exitFlag = ADT_TRUE;
}

//-----------------------------------------------
// process interrupts from DSP
void DSPIntrHandler (void) {
   processDSPInterrupt ();

   if (rtpSem != NULL) Semaphore_post (rtpSem); 
   if (evtSem != NULL) Semaphore_post (evtSem); 
}

//-----------------------------------------------
// send interrupt to DSP
void InterruptDSP (ADT_UInt32 Dsp) {
   return;
}

//---------------------------------------
//  process user inputs
static void uiThread (void *args) {

   gpakDspStartup (0, NULL);
   uartRxSem = Semaphore_create (1, NULL, NULL);;
   uartTxSem = Semaphore_create (1, NULL, NULL);;

   userInterface ();
   threadStop ();
}

//-----------------------------------------------
// process events
static void evtThread (void *args) {
   ADT_UInt32 Dsp = 0;
   ADT_Int16  chanId;
   GpakApiStatus_t      fifoStat;
   GpakAsyncEventCode_t eventCode;
   GpakAsyncEventData_t eventData;

   evtSem = Semaphore_create (1, NULL, NULL);
   while (uartTxSem == NULL) gpakHostDelay ();

   while (!exitFlag && evtSem) {
      // 10 millisecond sleep
      Semaphore_pend (evtSem, 10);

      // Transfer events from DSP to host.
      do {
         fifoStat = gpakReadEventFIFOMessage (Dsp, &chanId, &eventCode, &eventData);
         if (fifoStat == RefNoEventAvail) break;
         else if (fifoStat != RefEventAvail) {
            ADT_printf ("readEvent error %d\n", fifoStat);
         }
         processEvent (Dsp, chanId, eventCode, &eventData);
      } while (fifoStat == RefEventAvail);

      // Display periodic status
      periodicStatusDisplay (Dsp);
   }
   if (evtSem != NULL) Semaphore_delete (&evtSem);
   evtSem = NULL;
}

//-----------------------------------------------
// process rtp packets
static void rtpThread (void *args) {
   ADT_UInt32 Dsp = 0;
   rtpSem = Semaphore_create (1, NULL, NULL);
   while (uartTxSem == NULL) gpakHostDelay ();

   while (!exitFlag && rtpSem) {
      // wait on semaphore with 5 millisecond wake up
      Semaphore_pend (rtpSem, 5);
      if (hostLoopBack == DSP_LOOP) continue;

      if (rtpProcess == NULL) continue;

      (*rtpProcess)(Dsp);

      // Notify DSP that packets are available
      InterruptDSP (Dsp);
   }
   if (rtpSem != NULL) Semaphore_delete (&rtpSem);
   rtpSem = NULL;
}

//}

//-----------------------------------------------
// Main processing loop.
//
//  Set up threads and call user interface
//
int main (int argc, char* argv[]) {

#ifdef AIC
   int slotCnt, TDMkHz;
#endif

   GpakApiStatus_t ApiStatus;

   Mmu_disable ();

   UART_init (2, 115200);

   ADT_printf ("\n\nStart DSP before continuing\n");
   ADT_getchar ();

   evtSem = NULL;
   rtpSem = NULL;
   uartTxSem = NULL;

   initGpakStructures ();

   // Read DSP configuration.
   memset (&sysCfg, 0, sizeof (sysCfg));
   ApiStatus = gpakGetSystemConfig (0, &sysCfg);
   if (ApiStatus != GpakApiSuccess)
      ADT_printf ("Get system configuration failure %d:%d\n", ApiStatus, DSPError [0]);

#ifdef AIC
   USTIMER_init ();    // AIC support
   I2C_init (I2C_PORT_AIC3106, I2C_CLK_100K);

   //===================================================
   // EVM AIC Initialization
   //     can only be configured for 2 or 16 slots. 
   slotCnt = 0;
   if (sysCfg.Port1NumSlots == 16) {
      cfgSlots[0] = 0xFFFF;
      leftSlt = 0; rightSlt = 8;
      slotCnt = 16;
   } else if (sysCfg.Port1NumSlots == 2) {
      cfgSlots[0] = 0x3;
      leftSlt = 0; rightSlt = 1;
      slotCnt = 2;
   }
   if (slotCnt) {
      TDMkHz = 16;
      ADT_printf ("Configuring AIC for: %d kHz, %d slots.\n", TDMkHz, slotCnt);
      AIC3106_init (TDMkHz * 1000, slotCnt);
      AIC3106_dump (TDMkHz * 1000);
   }
#endif

   // Initialize threads for packet transfers, event transfers, and user intefaces
   threadInit ();

   BIOS_start ();
   return 0;
}


void appErr (int exitCode) {
   ADT_printf ("\nApplication error %d\n", exitCode);
   while (ADT_TRUE);
}

