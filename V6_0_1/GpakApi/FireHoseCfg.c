/*
 * Copyright (c) 2009, Adaptive Digital Technologies, Inc.
 *
 * File Name: FireHosecfg.c
 *
 * Description:
 *
 * Version: 1.0
 *
 * Revision History:
 *   10/17/08 - Initial release.
 *
 */
#ifdef _WIN32
    #if (_MSC_VER >= 900)
       #define   WIN32_LEAN_AND_MEAN  1
       #define   INC_OLE2
       #define   NOSERVICE
    #endif 

    #include <windows.h>
    #include <stdio.h>
    #include <conio.h>   
#endif

#include <winsock2.h>
#include <ws2tcpip.h>


#include "GpakCustWin32.h"
#include "GpakApi.h"
#include "adt_typedef.h"

extern char *IPToStr (int IPAddr);

typedef struct rtpAddr {
   ADT_UInt32 SSRC;
   ADT_UInt32 IP;
   ADT_UInt16 Port;
   ADT_UInt8  MAC[6];
} rtpAddr;

struct fhCfg { 
   rtpAddr    rmt;        // Remote MAC, IP, Port and SSRC addresses

   ADT_UInt32 duplicates;
   ADT_UInt32 samplesPerFrame;
   ADT_UInt16 pyldI8;
   ADT_UInt16 pyldType;
} FHCfg;

SOCKET gpakOpenUDPSocket (ADT_UInt32 DspIP, ADT_UInt32 lclIP, ADT_UInt16 lclPort);
int getUPDMessage (SOCKET skt, char *msg, int msgI8, unsigned long *timeMS, ADT_UInt32 *FromIP);
int sendUPDMessage (SOCKET skt, ADT_UInt32 destIP, ADT_UInt16 destPort, char *msg, int msgI8);

ADT_UInt32 FireHoseLinkIP = 0;

ADT_UInt32 cfgFireHose (ADT_Int32 DspId, int chanCnt, int frameSize, int pyldI8,
                        int pyldType, int SSRC, short Port, int dspIP, char *MAC) {

   struct hostent     *localHost;
   int i, j, rslt;
   SOCKET udpSkt;
   unsigned long timeMS;
   char msg[100];
   ADT_UInt32 fireHoseIP;

   // Retrieve and report local IP address
   localHost = gethostbyname("");

   FHCfg.duplicates = chanCnt;
   FHCfg.samplesPerFrame = frameSize;
   FHCfg.pyldI8   = pyldI8;
   FHCfg.pyldType = pyldType;
   FHCfg.rmt.SSRC = htonl (SSRC);
   FHCfg.rmt.IP   = htonl (dspIP);
   FHCfg.rmt.Port = htons (Port);
   memcpy (FHCfg.rmt.MAC, MAC, 6);

   ADT_printf ("\nFirehose request (chans/frameSamps/rtpSamps/codec) %d/%d/%d/%d to IP %s\n", 
                 chanCnt, frameSize, pyldI8, pyldType, IPToStr (dspIP));

   // Poll for DSP's IP address.
   for (i=0; (i<2) & (FireHoseLinkIP != 0); i++) {
      ADT_printf ("Trying on IP %s\n", IPToStr (FireHoseLinkIP));

      // Send fire host cfg message.
      udpSkt = gpakOpenUDPSocket (DspId, FireHoseLinkIP, 9999);
      if (udpSkt == (SOCKET) NULL) continue;

      rslt = sendUPDMessage (udpSkt, 0xffffffff, 9998, (void *) &FHCfg, sizeof (FHCfg));
      if (rslt < 0) continue;

      timeMS = 5000;
      fireHoseIP = 0;
      rslt = getUPDMessage (udpSkt, msg, sizeof (msg), &timeMS, &fireHoseIP);
      if (0 <rslt) ADT_printf ("Firehose reply %s from %s\n", msg, IPToStr (fireHoseIP));
      else         ADT_printf ("No firehose reply\n");
      closesocket (udpSkt);

      if (0 <rslt) return fireHoseIP;

   }

   // Poll for DSP's IP address.
   for (i=0; i<2; i++) {
     
      // Search all sub-addresses for IP on same subnet as DSP
      for (j=0; j<localHost->h_length; j++) { 
         if (localHost->h_addr_list[j] == 0) break;
         FireHoseLinkIP = htonl (*((ADT_UInt32 *)localHost->h_addr_list[j]));
         ADT_printf ("Trying on IP %s\n", IPToStr (FireHoseLinkIP));

         // Send fire host cfg message.
         udpSkt = gpakOpenUDPSocket (DspId, FireHoseLinkIP, 9999);
         if (udpSkt == (SOCKET) NULL) continue;

         rslt = sendUPDMessage (udpSkt, 0xffffffff, 9998, (void *) &FHCfg, sizeof (FHCfg));
         if (rslt < 0) continue;

         timeMS = 1000;
         fireHoseIP = 0;
         rslt = getUPDMessage (udpSkt, msg, sizeof (msg), &timeMS, &fireHoseIP);
         if (0 <rslt) ADT_printf ("Firehose reply %s from %s\n", msg, IPToStr (fireHoseIP));
         else         ADT_printf ("No firehose reply\n");
         closesocket (udpSkt);

         if (0 <rslt) {
            return fireHoseIP;
         }

      }
      ADT_printf ("\nNo response from firehose\n");
   }
   FireHoseLinkIP = 0;
   return 0;
}
