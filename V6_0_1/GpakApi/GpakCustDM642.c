/*
 * Copyright (c) 2002, Adaptive Digital Technologies, Inc.
 *
 * File Name: GpakCustDM642.c
 *
 * Description:
 *   This file contains host system dependent functions to support generic
 *   G.PAK API functions on the TI DM642 DSP. The file is integrated into 
 *   the host processor  connected to G.PAK DSPs via a Host Port Interface.
 *
 *   Note: This file needs to be modified by the G.PAK system integrator.
 *
 * Version: 1.0
 *
 * Revision History:
 *   10/17/01 - Initial release.
 *
 */
#ifdef _WIN32
    #if (_MSC_VER >= 900)
       #define   WIN32_LEAN_AND_MEAN  1
       #define   INC_OLE2
       #define   NOSERVICE
    #endif 

    #include <windows.h>
    #include <stdio.h>
    #include <conio.h>   
#endif

#include "time.h"
#include "sd_pci64_dm642.h"

PCI64_HANDLE pciHandle  = NULL;  // pci handle for main thread
static PCI64_HANDLE intHandle  = NULL;  // pci handle for interrupt thread
static HANDLE hCallbackEvent = NULL;


#include "GpakCust.h"
#include "GpakApi.h"

static char *EmifInit (PCI64_HANDLE hPci);
static char *EnableInterruptsFromDSP (PCI64_HANDLE hPci);
static char *ReleaseDSP (PCI64_HANDLE hPci);
static char *DisableInterruptsFromDSP (PCI64_HANDLE hPci);
static char *IsPciBootMode (PCI64_HANDLE hPci);
static char *AssertDspReset (PCI64_HANDLE hPci);


static int Locked = FALSE;

int hostToDspIntrCnt = 0;
int dspToHostIntrCnt = 0;

//==========================================================
//
//  System error trap
//
//===========================================================
#ifdef _DEBUG
void SystemError () {
   while (1) 
      Sleep (1);
}
#else
void SystemError () {
   return;
}
#endif
//=============================================================================
//
//  Support for DSP control via PCI
//
//==============================================================================

//===============================================================
//
//  Callback on DSP interrupt
//
//===============================================================
long DSPInterrupts = 0;

void (*ProcessDSPInterrupts)(void) = NULL;

DWORD WINAPI DSPCallback (LPVOID dummy) {
    ADT_UInt32 RegRst, Hsr;
    ADT_Int32  Error;


    if (hCallbackEvent == NULL)
        return (-1);


    while (1) {
       if (WaitForSingleObject (hCallbackEvent, INFINITE)) continue;
            
       dspToHostIntrCnt++;

       // Clear interrupt
       if (pciHandle == NULL) continue;

       RegRst = RSTSRC_INTRST;    // Remove interrupt signal
       Error = PCI64_RegWrite32 (intHandle, PCI64_RSTSRC, 1, &RegRst);
       if (Error) continue;

       Error = PCI64_RegRead32 (intHandle, PCI64_HSR, 1, &Hsr);
       if (Error) continue;
       if (Hsr & HSR_INTSRC) {
           Hsr |= HSR_INTSRC;  // Clear pending interrupt
           PCI64_RegWrite32 (intHandle, PCI64_HSR,1, &Hsr);
       }
       if (ProcessDSPInterrupts)  (*ProcessDSPInterrupts)();

   }

   return 0;
}

/****************************************************************************
* NAME:  PCIConnect ()
*
* DESCRIPTION:  Establish communication to DSP via PCI 
*         
* Inputs
*     Dsp  -  Identifier for multiple DSPs
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/
char *PCIConnect (ADT_Int32 Dsp) {


   if (intHandle != NULL)  {    // Close old PCI connection
      PCI64_Close (intHandle);
      intHandle = NULL;
   }

   if (pciHandle != NULL)  {    // Close old PCI connection
      PCI64_Close (pciHandle);
      pciHandle = NULL;
   }

	if (PCI64_Open (Dsp, &intHandle))     // Open new interrupt PCI connection
 	   return "PCI bus open failure";

   if (PCI64_Open (Dsp, &pciHandle)) {    // Open new PCI connection
      PCI64_Close (intHandle);
      intHandle = NULL;
 	   return "PCI bus open failure";
   }

   hCallbackEvent = PCI64_GetCallbackEvent (pciHandle);
   PCI64_SetupCallbackEvent (pciHandle, DSPCallback);

   return EnableInterruptsFromDSP (pciHandle);

   
}

/****************************************************************************
* NAME:  PCIDisconnect ()
*
* DESCRIPTION:  Release DSP Connection
*         
* Inputs
*     Dsp  -  Identifier for multiple DSPs
*
*
****************************************************************************/
void PCIDisconnect () {

   char *Error = NULL;

   if (pciHandle == NULL) return;

   DisableInterruptsFromDSP (pciHandle);

   PCI64_Close (pciHandle);
   pciHandle = NULL;

   PCI64_Close (intHandle);
   intHandle = NULL;
}

/****************************************************************************
* NAME:  PCIReset ()
*
* DESCRIPTION:  Reset and release DSP after establishing communications to DSP via PCI 
*         
* Inputs
*     Dsp  -  Identifier for multiple DSPs
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/

char *PCIReset (ADT_Int32 Dsp) {

	char *Error = NULL;

   Error = IsPciBootMode (pciHandle);  // Verify board allows PCI Boot
   if (Error != NULL) {
      return Error;
   }

   Error = AssertDspReset (pciHandle);  // Reset DSP
   if (Error != NULL) {
      PCI64_Close (pciHandle);
      PCI64_Close (intHandle);
      pciHandle = NULL;
      intHandle = NULL;
      return "DSP Reset Failed.\n";
   }

   Error = EmifInit (pciHandle);     // Initialize external memory I/F registers
   if (Error != NULL) {
      PCI64_Close (pciHandle);
      PCI64_Close (intHandle);
      pciHandle = NULL;
      intHandle = NULL;
      return Error;
   }

   // Release DSP from reset
   Error = ReleaseDSP (pciHandle);
   if (Error != NULL) {
      PCI64_Close (pciHandle);
      PCI64_Close (intHandle);
      pciHandle = NULL;
      intHandle = NULL;
      return Error;
   }
   return NULL;

}

/****************************************************************************
* NAME:  PCIHalt ()
*
* DESCRIPTION:  Halt DSP
*         
* Inputs
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/

char *PCIHalt () {

   return AssertDspReset (pciHandle);  // Reset DSP

}



/****************************************************************************
* NAME:  PCIRelease ()
*
* DESCRIPTION:  Release DSP
*         
* Inputs
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/

char *PCIRelease () {

   return ReleaseDSP (pciHandle);  // Release DSP from reset

}

/****************************************************************************
* NAME:  PCIInitEmif ()
*
* DESCRIPTION:  Init the EMIF.  Just handles EMIF-A.
*
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/
char *PCIInitEMIF (ADT_Int32 Dsp) {

    return EmifInit (pciHandle);
}

/****************************************************************************
* NAME:  AssertDspReset ()
*
* DESCRIPTION:  Put DSP in reset
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    0 Success, non-0 for Fail
*
*F***************************************************************************/
static char *AssertDspReset (PCI64_HANDLE hPci) {
    ADT_UInt32 RegHdcr;
    ADT_Int32  Error;

    if (hPci == NULL) return "DSP reset failure. PCI not open";

    Error = PCI64_RegRead32 (hPci, PCI64_HDCR, 1, &RegHdcr);
    if (Error)
        return "DSP reset failure. HDCR register read failure";

    RegHdcr |= HDCR_WARMRESET;  // Place DSP in warm reset
    Error = PCI64_RegWrite32 (hPci, PCI64_HDCR, 1, &RegHdcr);
    if (Error)
        return "DSP reset failure. HDCR register write failure";
    
    Sleep (2);

    EmifInit (hPci);
    return NULL;
}

/****************************************************************************
* NAME: ReleaseDSP () 
*
* DESCRIPTION:  Release DSP from  reset and start code exection from address 0x00000000.
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    NULL Success,  Failure message
*
*F***************************************************************************/
static char *ReleaseDSP (PCI64_HANDLE hPci) {
    ADT_UInt32 RegHdcr;
    ADT_Int32  Error;

    if (hPci == NULL) return "DSP Release failure. Null Handle";

    Error = PCI64_RegRead32 (hPci, PCI64_HDCR, 1, &RegHdcr);
    if (Error)
        return "DSP Read HDCR Register failure";

    RegHdcr |= HDCR_DSPINT;   //  Generate DSP interrupt to release DSP
    Error = PCI64_RegWrite32 (hPci, PCI64_HDCR, 1, &RegHdcr);
    if (Error) 
       return "DSP Write HDCR Register failure";

    return NULL;
}

/****************************************************************************
* NAME: IsPciBootMode 
*
* DESCRIPTION:  Tests for PCI boot mode.  If not in PCI boot mode then
*               should NOT attempt doing PCI boot.
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*  NULL - PCI Boot mode enabled
*  Other - reason not enabled
*
****************************************************************************/
static char *IsPciBootMode (PCI64_HANDLE hPci) {

    ADT_UInt32 RegHdcr = 0;

    if (hPci == NULL) return "NULL Handle";

    if (PCI64_RegRead32 (hPci, PCI64_HDCR, 1, &RegHdcr))
       return "HDCR Register Read Error";

    if (RegHdcr == HDCR_PCIBOOT) return NULL;
    return "DSP not in PCI boot mode";
}

/****************************************************************************
* NAME:  EmifInit ()
*
* DESCRIPTION:  Init the EMIF.  Just handles EMIF-A.
*
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/
static char *EmifInit (PCI64_HANDLE hPci) {

    ADT_Bool success;

/*  Original values.  Change to match GEL values
    ADT_UInt32 EmifA[12] = {
      // GCTL          CE1              CE0
      0x00092078UL,  0xF3A88E02UL,  0xFFFFFFDEUL, 
                         
      //  CE2           CE3           SDRAMCTL         SDRAMTIM       SDRAMEXT 
      0x22a28a22UL,  0x22a28a42UL,   0x57115000UL,  0x0000081bUL,    0x000a8529UL, 
      
      //  CE1ECCTL     CE0ECCTL
      0x00000002UL,  0x00000002UL,

      //  CE2ECCTL     CE3ECCTL
      0x00000002UL,   0x00000073UL
    };

*/
    ADT_UInt32 EmifA[12] = { 0x00052078UL,     // GCTL     - 0x01800000
                         0x73A28E01UL,        // CE1      - 0x01800004
                         0xFFFFFFD3UL,        // CE0      - 0x01800008
                         // Address split 3
                         0x22a28a22UL,        // CE2      - 0x01800010
                         0x22a28a42UL,        // CE3      - 0x01800014
                         0x57115000UL,        // SDRAMCTL - 0x01800018
                         0x0000081bUL,        // SDRAMTIM - 0x0180001c
                         0x001faf4dUL,        // SDRAMEXT - 0x01800020
                         // Address split 9
                         0x00000002UL,        // CE1ECCTL - 0x01800044
                         0x00000002UL,        // CE0ECCTL - 0x01800048
                         // Address split 
                         0x00000002UL,        // CE2ECCTL - 0x01800050
                         0x00000073UL,        // CE3ECCTL - 0x01800054
                        };

    if (hPci == NULL) return "EMIF Init failure. PCI Bus not open";

    success  = !PCI64_MemWrite32 (hPci, 0x01800000UL, 3, &EmifA[0]);
    success &= !PCI64_MemWrite32 (hPci, 0x01800010UL, 5, &EmifA[3]);
    success &= !PCI64_MemWrite32 (hPci, 0x01800048UL, 2, &EmifA[8]);
    success &= !PCI64_MemWrite32 (hPci, 0x01800050UL, 2, &EmifA[10]);
    if (success) return NULL;

    return "EMIF Init failure. Write error.";
}


/****************************************************************************
* NAME:  EnableInterruptFromDSP ()
*
* DESCRIPTION:  Enable interrupts from DSP via PCI 
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/
static char *EnableInterruptsFromDSP (PCI64_HANDLE hPci) {

   ADT_UInt32          Hsr, Pciis;

    if (hPci == NULL) return "DSP Release failure. Null Handle";

 
    if (PCI64_RegRead32 (hPci, PCI64_HSR, 1, &Hsr))
       return "Unable to read HSR";

    if (PCI64_RegRead32 (hPci, PCI64_PCIIS, 1, &Pciis))
       return "Unable to read PCIIS";

    Hsr |= HSR_INTSRC;   // Clear pending interrupts
    if (PCI64_RegWrite32 (hPci, PCI64_HSR, 1, &Hsr))
       return "Unable to clear pending interrupts";


    Hsr &= ~HSR_INTAM;   // Enable interrupt
    if (PCI64_RegWrite32 (hPci, PCI64_HSR, 1, &Hsr))
       return "Unable to enable DSP interrupts";

    if (PCI64_RegWrite32 (hPci, PCI64_PCIIS, 1, &Pciis))
       return "Unable to clear PCIIS";

    return NULL;
}


/****************************************************************************
* NAME:  GenerateInterruptToDSP ()
*
* DESCRIPTION:  Generate interrupts to DSP via PCI 
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    Nothing
*
****************************************************************************/
void GenerateInterruptToDSP (ADT_Int32 DspId) {

   ADT_UInt32          Hsr;

    if ((pciHandle == NULL) || (DspId != 0)) return;


    Hsr = HDCR_DSPINT;   // Enable interrupts
    PCI64_RegWrite32 (pciHandle, PCI64_HDCR, 1, &Hsr);

    hostToDspIntrCnt++;

    return;
}

/****************************************************************************
* NAME:  DisableInterruptFromDSP ()
*
* DESCRIPTION:  Disable interrupts from DSP via PCI 
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/
static char *DisableInterruptsFromDSP (PCI64_HANDLE hPci) {

    ADT_UInt32          Hsr;

    if (hPci == NULL) return "DSP Release failure. Null Handle";

 
    if (PCI64_RegRead32 (hPci, PCI64_HSR, 1, &Hsr))
       return "Unable to read HSR";

    Hsr |= HSR_INTAM;    // Disable interrupt
    if (PCI64_RegWrite32 (hPci, PCI64_HSR, 1, &Hsr))
       return "Unable to disable interrupts";

    return NULL;
}

//=============================================================================
//
//  Support for GPAK APIs
//
//==============================================================================


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadDspMemory32 - Read DSP memory.
 *
 * FUNCTION
 *  Reads a contiguous block of 32-bit values from DSP memory 
 *  starting at the specified address.
 *
 * Inputs
 *     Dsp  -  Identifier for multiple DSPs
 *    DspAddress - DSP memory starting address
 *    LongCnt       - Number of 32-bit memory values to transfer
 *
 * Outputs
 *    Buff       - Buffer to receive values
 *
 * RETURNS
 *  nothing
 *
 */
#define PGSZ 0x100000

void gpakReadDspMemory32 (ADT_Int32 DspId, DSP_Address DspAddress, ADT_UInt32 LongCnt, ADT_UInt32 *Buff) {
    int Error;
    ADT_UInt32 I32ToPageEnd;
    ADT_UInt32 XferCnt;
    ADT_UInt32 XferAddr;
    
    if ((pciHandle == NULL) || (DspId != 0)) return;

    XferAddr = (ADT_UInt32) DspAddress;
    
    while (LongCnt) {
       I32ToPageEnd = (PGSZ - (XferAddr & (PGSZ-1))) >> 2;
       if (I32ToPageEnd < LongCnt) XferCnt = I32ToPageEnd;
       else                        XferCnt = LongCnt;
       Error = PCI64_MemRead32 (pciHandle, XferAddr, XferCnt, Buff);
       if (Error) SystemError ();
       Buff    += XferCnt;
       XferAddr+= XferCnt * 4;
       LongCnt -= XferCnt;
    }
    return;

}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakWriteDspMemory32 - Write DSP memory.
 *
 * FUNCTION
 *  Writes a contiguous block of 32-bit values to DSP memory  starting at the specified address.
 *
 * Inputs
 *     Dsp  -  Identifier for multiple DSPs
 *    DspAddress - DSP memory starting address
 *    LongCnt        - Number of 32-bit memory values to transfer
 *    Buff       - Buffer of values to write
 *
 * RETURNS
 *  nothing
 *
 */
void gpakWriteDspMemory32 (ADT_Int32 DspId, DSP_Address DspAddress, ADT_UInt32 LongCnt,  ADT_UInt32 *Buff) {
    int Error;
    ADT_UInt32 I32ToPageEnd;
    ADT_UInt32 XferCnt;
    ADT_UInt32 XferAddr;
    
    if ((pciHandle == NULL) || (DspId != 0)) return;

    XferAddr = (ADT_UInt32) DspAddress;
    
    while (LongCnt) {
       I32ToPageEnd = (PGSZ - (XferAddr & (PGSZ-1))) >> 2;
       if (I32ToPageEnd < LongCnt) XferCnt = I32ToPageEnd;
       else                        XferCnt = LongCnt;
       Error = PCI64_MemWrite32 (pciHandle, XferAddr, XferCnt, Buff);
       if (Error) SystemError ();
       Buff    += XferCnt;
       XferAddr+= XferCnt * 4;
       LongCnt -= XferCnt;
    }
    return;
}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadDspMemory32 - Read DSP memory.
 *
 * FUNCTION
 *  Reads a contiguous block of 32-bit values from DSP memory 
 *  starting at the specified address.
 *
 * Inputs
 *     Dsp  -  Identifier for multiple DSPs
 *    DspAddress - DSP memory starting address
 *    LongCnt       - Number of 32-bit memory values to transfer
 *
 * Outputs
 *    Buff       - Buffer to receive values
 *
 * RETURNS
 *  nothing
 *
 */

void gpakReadDspNoSwap32 (ADT_Int32 DspId, DSP_Address DspAddress, ADT_UInt32 LongCnt, ADT_UInt32 *Buff) {

    gpakReadDspMemory32 (DspId, DspAddress, LongCnt, Buff);
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakWriteDspMemory32 - Write DSP memory.
 *
 * FUNCTION
 *  Writes a contiguous block of 32-bit values to DSP memory  starting at the specified address.
 *
 * Inputs
 *     Dsp  -  Identifier for multiple DSPs
 *    DspAddress - DSP memory starting address
 *    LongCnt        - Number of 32-bit memory values to transfer
 *    Buff       - Buffer of values to write
 *
 * RETURNS
 *  nothing
 *
 */
void gpakWriteDspNoSwap32 (ADT_Int32 DspId, DSP_Address DspAddress, ADT_UInt32 LongCnt,  ADT_UInt32 *Buff) {
    gpakWriteDspMemory32 (DspId, DspAddress, LongCnt,  Buff);
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakHostDelay - Delay for a fixed time interval.
 *
 * FUNCTION
 *  Delays for a fixed time interval before returning. The time
 *  interval is the Host Port Interface sampling period when polling a DSP for
 *  replies to command messages.
 *
 * RETURNS
 *  nothing
 *
 */
void gpakHostDelay (void) {

/* This function needs to be implemented by the G.PAK system integrator. */
   Sleep (2);
}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakLockAccess - Lock access to the specified DSP.
 *
 * FUNCTION
 *  Aquires exclusive access to the specified DSP.
 *
 * RETURNS
 *  nothing
 *
 */
void gpakLockAccess (ADT_Int32 DspId) {

/* This function needs to be implemented by the G.PAK system integrator. */
   if (DspId != 0) return;

   while (Locked) gpakHostDelay ();
   Locked = TRUE;
}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakUnlockAccess - Unlock access to the specified DSP.
 *
 * FUNCTION
 *  Releases exclusive access to the specified DSP.
 *
 * RETURNS
 *  nothing
 *
 */
void gpakUnlockAccess (ADT_Int32 DspId)
{
   
/* This function needs to be implemented by the G.PAK system integrator. */
   if (DspId != 0) return;
   Locked = FALSE;
}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadFile - Read a block of bytes from a G.PAK Download file.
 *
 * FUNCTION
 *  Reads a contiguous block of bytes from a G.PAK Download file
 *  starting at the current file position.
 *
 * Inputs
 *   FileId -  Download file identifier
 *   pBuffer - Byte storage buffer
 *   ByteCnt     - Number of bytes to transfer
 *
 * RETURNS
 *  The number of bytes read from the file.
 *   -1 indicates an error occurred.
 *    0 indicates all bytes have been read (end of file)
 *
 */
int gpakReadFile (GPAK_FILE_ID FileId, ADT_UInt8 *pBuffer, ADT_UInt32 ByteCnt) {

    return fread (pBuffer, sizeof(char), ByteCnt, (FILE *) FileId);
}

/****************************************************************************
* NAME:  PCIDownloadCoff ()
*
* DESCRIPTION:  Download coff file to DSP 
*         
* Inputs
*     Dsp  -  Identifier for multiple DSPs
*    Filename - path and name of Coff file
*
* RETURN VALUE:
*    GdlSuccess      - Success
*    GdlInvalidFile  - Failure
*
****************************************************************************/
gpakDownloadStatus_t PCIDownloadCoff (char *Filename) {

   
   if (!PCI64_LoadCoff (pciHandle, Filename))
      return GdlSuccess;
   
   return GdlInvalidFile;

}



//=============================================================================
//
//  Support for RTP APIs
//
//==============================================================================
#include "rtpapi.h"
static int RtpLocked = FALSE;
static int txpkts = 0, rxpkts = 0;

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * RTCPStats
 *
 * FUNCTION
 *  Generates statistics for RTCP reports
 *
 *  Inputs
 *      event -  RTPRECEIVE or RTPSEND.   
 *      RTPConnect - RTP data structure
 *      PktAddr    - Pointer to RTP packet
 *      PktSize    - Size in bytes of packtet
 *
 * RETURNS
 *  nothing
 *
 */

void RTCPStats (RTCPEVENTS event, RTPCONNECT* RTPConnect, ADT_UInt8* PktAddr, ADT_UInt16 PktSize)
{ 
  (void) PktSize;
  (void) RTPConnect;
  (void) PktAddr;

  if (event == RTPRECEIVE) rxpkts++;
  if (event == RTPSEND)	   txpkts++;
  return; 
}
     
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * RTPAlloc
 *
 * FUNCTION
 *  Allocates memory for RTP jitter buffer
 *
 * Inputs
 *    size - Number of bytes to allocate
 *
 * RETURNS
 *  pointer to allocated memory
 *
 */

void *RTPAlloc(ULONG size) { return malloc (size); }

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * RTPFree
 *
 * FUNCTION
 *  Release previously allocated memory
 *
 * Inputs
 *   Pointer to memory location
 *
 * RETURNS
 *  nothing
 *
 */

void RTPFree(void *mem) {	free (mem);  }

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * RTPLock
 *
 * FUNCTION
 *  Aquires exclusive access to the RTP structures.
 *
 * RETURNS
 *  nothing
 *
 */
void RTPLock() { 
	glock ();
	RtpLocked = TRUE;
	return;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * RTPUnlock
 *
 * FUNCTION
 *  Releases exclusive access to the RTP structures.
 *
 * RETURNS
 *  nothing
 *
 */
void RTPUnlock() {
	RtpLocked = FALSE;
   gunlock ();
	return; 
}

ADT_UInt32 RTPTime () {
   ADT_Int64 temp;
   
   // Convert current time to RTP time stamp
   temp =  (((ADT_Int64) clock()) * 8000) / CLOCKS_PER_SEC;
   return (ADT_UInt32) temp;
}



void RTPWait () {
   Sleep (0);
   return;
}





void gpakReadDspMemory16 (ADT_Int32 DspId, DSP_Address DspAddress, ADT_UInt32 WordCnt, ADT_UInt16 *Buff) {
	printf ("Read %x [%d]\n", DspAddress, WordCnt);
    *Buff = 0x200;
	if (DspAddress == 0x100) *Buff = 0xaaaa;

    return ;

}
void gpakWriteDspMemory16 (ADT_Int32 DspId, DSP_Address DspAddress, ADT_UInt32 WordCnt,  ADT_UInt16 *Buff) {
	printf ("Write %x [%d]\n",  DspAddress, WordCnt);

   return;
}


