/*
 * Copyright (c) 2007, Adaptive Digital Technologies, Inc.
 *
 * File Name: PrintFax.c
 *
 * Description:
 *   This file contains routines to print out fax data on a G.PAK host
 *
 * Version: 1.0
 *
 * Revision History:
 *   1/22/09 - Initial release.
 *
 */
#include <stdio.h>
#include "GpakCust.h"
#include "GpakApi.h"

static ADT_Int32 buffer [256];

//  FAX log starts at location PlayRecStartAddr
static void printFaxBuffer (ADT_Int32 DspId, int Addr, int LengthI32) {
   FILE *file;
   int cntI32;

   if (LengthI32 == 0) return;
   file = fopen ("Faxlog.txt", "at");
   gpakLockAccess (DspId);
   while (0 < LengthI32) {
      cntI32 = sizeof (buffer)/4;
      if (LengthI32 < cntI32) cntI32 = LengthI32;

      gpakReadDspMemory32 (DspId, Addr, cntI32, buffer);

      fwrite (buffer, 1, cntI32 * 4, file);
      LengthI32 -= cntI32;
      Addr += (cntI32 * 4);
   } 
   gpakUnlockAccess (DspId);
   fclose (file);
   return;
}

//  FAX log starts at location PlayRecStartAddr
static char *direction[] = { "in", "out" };
static void printCaptureBuffer (ADT_Int32 DspId, int channel, int side, int Addr, int LengthI32) {
   FILE *file;
   int cntI32;

   if (LengthI32 == 0) return;
   sprintf ((char *) buffer, "faxChan%d%s.pcm", channel, direction[side]);
   file = fopen ((char *) buffer, "ab");
   gpakLockAccess (DspId);
   while (0 < LengthI32) {
      cntI32 = sizeof (buffer)/4;
      if (LengthI32 < cntI32) cntI32 = LengthI32;

      gpakReadDspMemory32 (DspId, Addr, cntI32, buffer);

      fwrite (buffer, 1, cntI32 * 4, file);
      LengthI32 -= cntI32;
      Addr += (cntI32 * 4);
   } 
   gpakUnlockAccess (DspId);

   fclose (file);
   return;
}

// startStop
//
//   0 - start FAX signal collection on modem active
//   1 - stop FAX signal collection and request buffer information
//   2 - force start of FAX signal collection immediately
//
void requestFaxDump (ADT_Int32 DspId, int startStop) {
   gpakTestStatus_t apiStatus;
   ADT_UInt16       respData;
   GPAK_TestStat_t  dspStatus;
   GpakTestData_t   testData;

   testData.u.TestParm = 100 + startStop;   
   apiStatus = gpakTestMode (DspId, UserDefined, &testData, &respData,  &dspStatus);

}

void dumpFaxBuffers (ADT_Int32 DspId, GpakAsyncEventData_t *EvtData) {
   printCaptureBuffer (DspId, 0, EvtData->ToneDevice, EvtData->aux.captureData.AAddr, EvtData->aux.captureData.ALenI32);
   printCaptureBuffer (DspId, 1, EvtData->ToneDevice, EvtData->aux.captureData.BAddr, EvtData->aux.captureData.BLenI32);
   printFaxBuffer     (DspId,    EvtData->aux.captureData.CAddr, EvtData->aux.captureData.CLenI32);
   if (EvtData->ToneDevice == 1) requestFaxDump (DspId, 0);
}


