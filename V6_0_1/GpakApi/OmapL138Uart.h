//-----------------------------------------------------------------------------
// \file    evmomapl138_uart.c
// \brief   implementation of a uart driver for OMAP-L138.
//
//-----------------------------------------------------------------------------
#ifndef UART_H
#define UART_H

typedef struct uart_regs_t {
    volatile ADT_UInt32 RBR;             // 0x0000
    volatile ADT_UInt32 IER;             // 0x0004
    volatile ADT_UInt32 IIR;             // 0x0008
    volatile ADT_UInt32 LCR;             // 0x000C
    volatile ADT_UInt32 MCR;             // 0x0010
    volatile ADT_UInt32 LSR;             // 0x0014
    volatile ADT_UInt32 MSR;             // 0x0018
    volatile ADT_UInt32 SCR;             // 0x001C
    volatile ADT_UInt32 DLL;             // 0x0020
    volatile ADT_UInt32 DLH;             // 0x0024
    volatile ADT_UInt32 REV_ID1;         // 0x0028
    volatile ADT_UInt32 REV_ID2;         // 0x002C
    volatile ADT_UInt32 PWREMU_MGMT;     // 0x0030
    volatile ADT_UInt32 MDR;             // 0x0034
} uart_regs_t;

// define all the available uart peripherals for the processor.
extern uart_regs_t *UART0;
extern uart_regs_t *UART1;
extern uart_regs_t *UART2;

ADT_UInt32 UART_init (ADT_UInt32 port, ADT_UInt32 baud_rate);
ADT_UInt32 UART_txByte (ADT_UInt32 port, ADT_UInt8 in_data) ;
ADT_UInt32 UART_txArray (ADT_UInt32 port, ADT_UInt8 *in_data, ADT_UInt32 in_length);
ADT_UInt32 UART_txString (ADT_UInt32 port, char *in_data) ;
ADT_UInt32 UART_txHex8 (ADT_UInt32 port, ADT_UInt8 in_data);
ADT_UInt32 UART_txHex32 (ADT_UInt32 port, ADT_UInt32 in_data);
ADT_UInt32 UART_rxByte (ADT_UInt32 port, ADT_UInt8 *data);
#endif
