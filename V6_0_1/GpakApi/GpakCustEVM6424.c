/*
 * Copyright (c) 2009, Adaptive Digital Technologies, Inc.
 *
 * File Name: GpakCustEVM6424.c
 *
 * Description:
 *   This file contains host system dependent functions to support generic
 *   G.PAK API functions on the TI EVM6424 DSP. The file is integrated into 
 *   the host processor  connected to G.PAK DSPs via a Host Port Interface.
 *
 *   Note: This file needs to be modified by the G.PAK system integrator.
 *
 * Version: 1.0
 *
 * Revision History:
 *   10/17/08 - Initial release.
 *
 */
#ifdef _WIN32
    #if (_MSC_VER >= 900)
       #define   WIN32_LEAN_AND_MEAN  1
       #define   INC_OLE2
       #define   NOSERVICE
    #endif 

    #include <windows.h>
    #include <stdio.h>
    #include <conio.h>   
#endif


#include "time.h"
#include "sd_pci64_evm6424.h"

PCI64_HANDLE pciHandle  = NULL;  // pci handle for main thread
static PCI64_HANDLE intHandle  = NULL;  // pci handle for interrupt thread
static HANDLE hCallbackEvent = NULL;


#include "GpakCust.h"
#include "adt_typedef.h"

static char *DDR2Init (PCI64_HANDLE hPci, int freq, int bit32);
static char *ReleaseDSP (PCI64_HANDLE hPci);
static char *AssertDspReset (PCI64_HANDLE hPci);
static char *EnableInterruptsFromDSP (PCI64_HANDLE hPci);
static char *DisableInterruptsFromDSP (PCI64_HANDLE hPci);


static int Locked = FALSE;


//==========================================================
//
//  System error trap
//
//===========================================================
#ifdef _DEBUG
void SystemError () {
   printf ("System Error\n");
   while (1) 
      Sleep (1);
}
#else
void SystemError () {
   return;
}
#endif
//=============================================================================
//
//  Support for DSP control via PCI
//
//==============================================================================

//===============================================================
//
//  Callback on DSP interrupt
//
//===============================================================
long DSPInterrupts = 0;
void (*ProcessDSPInterrupts)(void) = NULL;

DWORD WINAPI DSPCallback (LPVOID dummy) {

    if (hCallbackEvent == NULL)
        return (-1);

 
    while (hCallbackEvent != NULL) {
       if (WaitForSingleObject (hCallbackEvent, INFINITE)) continue;
            
       DSPInterrupts++;
       if (ProcessDSPInterrupts)  (*ProcessDSPInterrupts)();

   }

   return 0;
}

/****************************************************************************
* NAME:  PCI64_RegRead ()
*
* DESCRIPTION:  Handles register reads that are outside of the PCI area 
*               (01c0 0000 .. 01ff ffff) the Spectrum Digital has mapped out
*               for registers 
*         
* Inputs
*     hPci  -  Handle for PCI access
*     reg   -  Register address
*    value  -  pointer to store register value
*
* RETURN VALUE:
*    0 Success, non-0 for Fail
*
*F***************************************************************************/
static INT32 PCI64_RegRead (PCI64_HANDLE hPci, ADT_UInt32 reg, ADT_UInt32 *value) {

    if (hPci == NULL) return -1;

    if (0x01c00000 <= reg && reg < 0x02000000)
        return PCI64_RegRead32 (hPci, reg, 1, value);
    else
        return PCI64_MemRead32 (hPci, reg, 1, value);
}

/****************************************************************************
* NAME:  PCI64_RegWrite ()
*
* DESCRIPTION:  Handles register writes that are outside of the PCI area 
*               (01c0 0000 .. 01ff ffff) the Spectrum Digital has mapped out
*               for registers 
*         
* Inputs
*     hPci  -  Handle for PCI access
*     reg   -  Register address
*    value  -  pointer to store register value
*
* RETURN VALUE:
*    0 Success, non-0 for Fail
*
*F***************************************************************************/
static INT32 PCI64_RegWrite (PCI64_HANDLE hPci, ADT_UInt32 reg, ADT_UInt32 *value) {

    if (hPci == NULL) return -1;

    if (0x01c00000 <= reg && reg < 0x02000000)
        return PCI64_RegWrite32 (hPci, reg, 1, value);
    else
        return PCI64_MemWrite32 (hPci, reg, 1, value);
}


/****************************************************************************
* NAME:  PCIConnect ()
*
* DESCRIPTION:  Establish communication to DSP via PCI 
*         
* Inputs
*     Dsp  -  Identifier for multiple DSPs
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/
char *PCIConnect (ADT_Int32 Dsp) {


   if (intHandle != NULL)  {    // Close old PCI connection
      PCI64_Close (intHandle);
      intHandle = NULL;
   }

   if (pciHandle != NULL)  {    // Close old PCI connection
      PCI64_Close (pciHandle);
      pciHandle = NULL;
   }

	if (PCI64_Open (Dsp, &intHandle))     // Open new interrupt PCI connection
 	   return "PCI bus open failure";

   if (PCI64_Open (Dsp, &pciHandle)) {    // Open new PCI connection
      PCI64_Close (intHandle);
      intHandle = NULL;
 	   return "PCI bus open failure";
   }

   hCallbackEvent = PCI64_GetCallbackEvent (pciHandle);
   PCI64_SetupCallbackEvent (pciHandle, DSPCallback);

   return EnableInterruptsFromDSP (pciHandle);

   
}

/****************************************************************************
* NAME:  PCIDisconnect ()
*
* DESCRIPTION:  Release DSP Connection
*         
* Inputs
*     Dsp  -  Identifier for multiple DSPs
*
*
****************************************************************************/
void PCIDisconnect () {

   char *Error = NULL;

   if (pciHandle == NULL) return;

   DisableInterruptsFromDSP (pciHandle);
   hCallbackEvent = NULL;

   PCI64_Close (pciHandle);
   pciHandle = NULL;

   PCI64_Close (intHandle);
   intHandle = NULL;
}

/****************************************************************************
* NAME:  PCIReset ()
*
* DESCRIPTION:  Reset and release DSP after establishing communications to DSP via PCI 
*         
* Inputs
*     Dsp  -  Identifier for multiple DSPs
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/

char *PCIReset (ADT_Int32 Dsp) {

	char *Error = NULL;


   Error = AssertDspReset (pciHandle);  // Reset DSP
   if (Error != NULL) {
      PCI64_Close (pciHandle);
      PCI64_Close (intHandle);
      pciHandle = NULL;
      intHandle = NULL;
      hCallbackEvent = NULL;
      return Error;
   }

   Error = DDR2Init (pciHandle, 162, 1);     // Initialize external memory I/F registers
   if (Error != NULL) {
      PCI64_Close (pciHandle);
      PCI64_Close (intHandle);
      pciHandle = NULL;
      intHandle = NULL;
      hCallbackEvent = NULL;
      return Error;
   }

   // Release DSP from reset
   Error = ReleaseDSP (pciHandle);
   if (Error != NULL) {
      PCI64_Close (pciHandle);
      PCI64_Close (intHandle);
      pciHandle = NULL;
      intHandle = NULL;
      hCallbackEvent = NULL;
      return Error;
   }
   return NULL;

}

/****************************************************************************
* NAME:  PCIHalt ()
*
* DESCRIPTION:  Halt DSP
*         
* Inputs
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/

char *PCIHalt () {

   return AssertDspReset (pciHandle);  // Reset DSP

}



/****************************************************************************
* NAME:  PCIRelease ()
*
* DESCRIPTION:  Release DSP
*         
* Inputs
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/

char *PCIRelease () {

   return ReleaseDSP (pciHandle);  // Release DSP from reset

}

/****************************************************************************
* NAME:  PCIInitDDR2 ()
*
* DESCRIPTION:  Init the DDR.
*
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/
char *PCIInitDDR2 (PCI64_HANDLE hPci, int freq, int width) {

    return DDR2Init (hPci, freq, width);
}

/****************************************************************************
* NAME:  AssertDspReset ()
*
* DESCRIPTION:  Put DSP in reset
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    0 Success, non-0 for Fail
*
*F***************************************************************************/
static char *AssertDspReset (PCI64_HANDLE hPci) {
    ADT_UInt32 Value;
    ADT_Int32  Error;

    if (hPci == NULL) return "DSP reset failure. PCI not open";

    Error = PCI64_RegRead (hPci, BOOTCOMPLT, &Value);
    if (Error)
        return "DSP reset failure. BOOT register read failure";

    if (Value & 1)
        return "C6424 does not support host reset";
        
    
    Sleep (2);

    DDR2Init (hPci, 162, 1);
    return NULL;
}

/****************************************************************************
* NAME: ReleaseDSP () 
*
* DESCRIPTION:  Release DSP from  reset and start code exection from address 0x00000000.
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    NULL Success,  Failure message
*
*F***************************************************************************/
static char *ReleaseDSP (PCI64_HANDLE hPci) {
    ADT_UInt32 Value;
    ADT_Int32  Error;

    if (hPci == NULL) return "DSP Release failure. Null Handle";

    Error = PCI64_RegRead (hPci, BOOTCOMPLT, &Value);
    if (Error)
        return "DSP Read BOOT Register failure";

    Value |= 1;   //  Generate DSP interrupt to release DSP
    Error = PCI64_RegWrite (hPci, BOOTCOMPLT, &Value);
    if (Error) 
       return "DSP Write BOOT Register failure";

    return NULL;
}

/****************************************************************************
 *                                                                          *
 *  psc_change_state (id, state)                                            *
 *      id    = Domain #ID                                                  *
 *      state = (ENABLE, DISABLE, SYNCRESET, RESET)                         *
 *              (  =3  ,   =2   ,    =1    ,   =0 )                         *
 *                                                                          *
 ************************************************************************** */
void psc_change_state (PCI64_HANDLE hPci, int id, int state) {

    ADT_UInt32 Value;
    ADT_UInt32 mdstat = 0x01c41800UL + (4 * id);
    ADT_UInt32 mdctl  = 0x01c41a00UL + (4 * id);
    int try = 0;

    
    //  Step 0 - Ignore request if the state is already set as is
    PCI64_RegRead  (hPci, mdstat, &Value);
    if ((Value & 0x1f) == (ADT_UInt32) state)
        return;

    //  Step 1 - Wait for PTSTAT.GOSTAT to clear
    do {
        PCI64_RegRead  (hPci, PSC_PTSTAT, &Value);
    } while ((Value & 1) && try++<1000);
    if (1000 <= try) {
      printf ("\nPower module [%d] state change failure ", id);
      return;
    }

    //  Step 2 - Set MDCTLx.NEXT to new state
    PCI64_RegRead  (hPci, mdctl, &Value);
    Value &= ~0x1f;
    Value |= state;
    PCI64_RegWrite (hPci, mdctl, &Value);

    //  Step 3 - Start power transition (set PTCMD.GO to 1)
    Value = 1;
    PCI64_RegWrite (hPci, PSC_PTCMD, &Value);

    //  Step 4 - Wait for PTSTAT.GOSTAT to clear
    try=0;
    do {
        PCI64_RegRead  (hPci, PSC_PTSTAT, &Value);
    } while ((Value & 1) && try++<1000);
    if (1000 <= try) {
      printf ("\nPower module [%d] state change failure ", id);
      return;
    }

    //  Step 5 - Verify state changed
    try=0;
    do {
        PCI64_RegRead  (hPci, mdstat, &Value);
    } while (((Value & 0x1f) != (ADT_UInt32) state) && try++<10000);
    if (10000 <= try) {
      printf ("\nPower module [%d] state change failure ", id);
      return;
    }
}

/****************************************************************************
* NAME:  DDR2Init ()
*
* DESCRIPTION:  Init the DDR2.  
*
* Inputs
*     hPci  -  Handle for PCI access
*     freq  -  Memory speed
*     wide  -  0 = 16 bit,  1 = 32 bit
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/

static char *DDR2Init (PCI64_HANDLE hPci, int freq, int wide) {
    ADT_UInt32 Value;
    ADT_UInt32 pch_nch;

    int widthBit;

	if (wide) {
	   widthBit = 0;
    } else {
	   widthBit = 0x4000;
    }
    //  Step 1 - Setup PLL2
    //  Step 2 - Enable DDR2 PHY
    psc_change_state (hPci, 13, 3);

    //  Step 3 - DDR2 Initialization
    Value = 0x50006405;
    PCI64_RegWrite (hPci, DDR_DDRPHYCR, &Value);     // DLL powered, ReadLatency=5

    Value = 0x00138822 | widthBit;
    PCI64_RegWrite (hPci, DDR_SDBCR, &Value) ;         // DDR Bank: xx-bit bus, CAS=4,
                                                       // 4 banks, 1024-word pg
      // DDR Timing
    if (freq == 135)  {
       Value = 0x14492148;
       PCI64_RegWrite (hPci, DDR_SDTIMR, &Value);
       Value = 0x000bc702;
       PCI64_RegWrite (hPci, DDR_SDTIMR2, &Value);
    } else { // Default to 162 MHz
       Value = 0x000bc702;
       PCI64_RegWrite (hPci, DDR_SDTIMR, &Value);
       Value = 0x000cc702;
       PCI64_RegWrite (hPci, DDR_SDTIMR2, &Value);
    }
    
    Value = 0x00130822 | widthBit;
    PCI64_RegWrite (hPci, DDR_SDBCR, &Value);      // DDR Bank: cannot modify
    Value = (ADT_UInt32) freq * 7.8;
    PCI64_RegWrite (hPci, DDR_SDRCR, &Value);      // Refresh Control [ 7.8 usec * freq ]

    //  Step 4 - Dummy Read from DDR2
    PCI64_RegRead (hPci, 0x80000000UL, &Value);

    //  Step 5 - Soft Reset (SYNCRESET followed by ENABLE) of DDR2 PHY
    psc_change_state (hPci, 13, 1);
    psc_change_state (hPci, 13, 3);
      
    //  Step 6 - Enable VTP calibration
    //  Step 7 - Wait for VTP calibration (33 VTP cycles)
    Value = 0x201f;
    PCI64_RegWrite (hPci, DDR_VTPIOCR, &Value);
    Value = 0xa01f;
    PCI64_RegWrite (hPci, DDR_VTPIOCR, &Value);
    Sleep (1500);

    //  Step 8 - Enable access to DDR VTP reg
    //  Step 9 - Reat P & N channels
    //  Step 10 - Set VTP fields PCH & NCH
    Value = 1;
    PCI64_RegWrite (hPci, DDR_DDRVTPER, &Value);
    PCI64_RegRead  (hPci, DDR_DDRVTPR, &pch_nch);
    pch_nch &= 0x3ff;
    pch_nch |= 0xa000;
    PCI64_RegWrite (hPci, DDR_VTPIOCR, &Value);

    //  Step 11 - Disable VTP calibaration
    //          - Disable access to DDR VTP register
    PCI64_RegWrite (hPci, DDR_VTPIOCR, &Value);
    Value &= ~0x2000;
    PCI64_RegWrite (hPci, DDR_VTPIOCR, &Value);

    Value = 0;
    PCI64_RegWrite (hPci, DDR_DDRVTPER, &Value);
    return NULL;
}


/****************************************************************************
* NAME:  EnableInterruptFromDSP ()
*
* DESCRIPTION:  Enable interrupts from DSP via PCI 
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/
static char *EnableInterruptsFromDSP (PCI64_HANDLE hPci) {

   ADT_UInt32          Hsr;

    if (hPci == NULL) return "DSP Release failure. Null Handle";

 

    Hsr = HOST_INTS;   // Enable interrupts
    if (PCI64_RegWrite (hPci, PCI64_HINT_CLR, &Hsr))
       return "Unable to enable DSP interrupts";

    Hsr = HOST_INTS;   // Enable interrupts
    if (PCI64_RegWrite (hPci, PCI64_HINT_SET, &Hsr))
       return "Unable to enable DSP interrupts";

    return NULL;
}


/****************************************************************************
* NAME:  DisableInterruptFromDSP ()
*
* DESCRIPTION:  Disable interrupts from DSP via PCI 
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    Null      - Success
*    non-Null  - Failure text
*
****************************************************************************/
static char *DisableInterruptsFromDSP (PCI64_HANDLE hPci) {

    ADT_UInt32          Hsr;

    if (hPci == NULL) return "DSP Release failure. Null Handle";

 
    Hsr = ALL_INTS;   // Enable interrupts
    if (PCI64_RegWrite (hPci, PCI64_HINT_CLR, &Hsr))
       return "Unable to disable interrupts";

    return NULL;
}


/****************************************************************************
* NAME:  GenerateInterruptToDSP ()
*
* DESCRIPTION:  Generate interrupts to DSP via PCI 
*         
* Inputs
*     hPci  -  Handle for PCI access
*
* RETURN VALUE:
*    Nothing
*
****************************************************************************/
void GenerateInterruptToDSP (ADT_Int32 DspId) {

   ADT_UInt32          Hsr;

    if ((pciHandle == NULL) || (DspId != 0)) return;


    Hsr = DSP_INTS;   // Enable interrupts
    PCI64_RegWrite (pciHandle, PCI64_INT_GEN, &Hsr);

    return;
}

//=============================================================================
//
//  Support for GPAK APIs
//
//==============================================================================


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadDspMemory32 - Read DSP memory.
 *
 * FUNCTION
 *  Reads a contiguous block of 32-bit values from DSP memory 
 *  starting at the specified address.
 *
 * Inputs
 *     Dsp  -  Identifier for multiple DSPs
 *    DspAddress - DSP memory starting address
 *    LongCnt       - Number of 32-bit memory values to transfer
 *
 * Outputs
 *    Buff       - Buffer to receive values
 *
 * RETURNS
 *  nothing
 *
 */
#define PGSZ 0x100000

void gpakReadDspMemory32 (ADT_Int32 DspId, DSP_Address DspAddress, ADT_UInt32 LongCnt, ADT_UInt32 *Buff) {
    int Error;
    ADT_UInt32 I32ToPageEnd;
    ADT_UInt32 XferCnt;
    ADT_UInt32 XferAddr;
    
    if ((pciHandle == NULL) || (DspId != 0)) return;

    XferAddr = (ADT_UInt32) DspAddress;
    
    while (LongCnt) {
       I32ToPageEnd = (PGSZ - (XferAddr & (PGSZ-1))) >> 2;
       if (I32ToPageEnd < LongCnt) XferCnt = I32ToPageEnd;
       else                        XferCnt = LongCnt;
       Error = PCI64_MemRead32 (pciHandle, XferAddr, XferCnt, Buff);
       if (Error) {
          pciHandle = NULL;
          break;
       }
       Buff    += XferCnt;
       XferAddr+= XferCnt * 4;
       LongCnt -= XferCnt;
    }
    return;

}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakWriteDspMemory32 - Write DSP memory.
 *
 * FUNCTION
 *  Writes a contiguous block of 32-bit values to DSP memory  starting at the specified address.
 *
 * Inputs
 *     Dsp  -  Identifier for multiple DSPs
 *    DspAddress - DSP memory starting address
 *    LongCnt        - Number of 32-bit memory values to transfer
 *    Buff       - Buffer of values to write
 *
 * RETURNS
 *  nothing
 *
 */
void gpakWriteDspMemory32 (ADT_Int32 DspId, DSP_Address DspAddress, ADT_UInt32 LongCnt,  ADT_UInt32 *Buff) {
    int Error;
    ADT_UInt32 I32ToPageEnd;
    ADT_UInt32 XferCnt;
    ADT_UInt32 XferAddr;
    
    if ((pciHandle == NULL) || (DspId != 0)) return;

    XferAddr = (ADT_UInt32) DspAddress;
    
    while (LongCnt) {
       I32ToPageEnd = (PGSZ - (XferAddr & (PGSZ-1))) >> 2;
       if (I32ToPageEnd < LongCnt) XferCnt = I32ToPageEnd;
       else                        XferCnt = LongCnt;
       Error = PCI64_MemWrite32 (pciHandle, XferAddr, XferCnt, Buff);
       if (Error) SystemError ();
       Buff    += XferCnt;
       XferAddr+= XferCnt * 4;
       LongCnt -= XferCnt;
    }
    return;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadDspMemory32 - Read DSP memory.
 *
 * FUNCTION
 *  Reads a contiguous block of 32-bit values from DSP memory 
 *  starting at the specified address.
 *
 * Inputs
 *     Dsp  -  Identifier for multiple DSPs
 *    DspAddress - DSP memory starting address
 *    LongCnt       - Number of 32-bit memory values to transfer
 *
 * Outputs
 *    Buff       - Buffer to receive values
 *
 * RETURNS
 *  nothing
 *
 */

void gpakReadDspNoSwap32 (ADT_Int32 DspId, DSP_Address DspAddress, ADT_UInt32 LongCnt, ADT_UInt32 *Buff) {

    gpakReadDspMemory32 (DspId, DspAddress, LongCnt, Buff);
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakWriteDspMemory32 - Write DSP memory.
 *
 * FUNCTION
 *  Writes a contiguous block of 32-bit values to DSP memory  starting at the specified address.
 *
 * Inputs
 *     Dsp  -  Identifier for multiple DSPs
 *    DspAddress - DSP memory starting address
 *    LongCnt        - Number of 32-bit memory values to transfer
 *    Buff       - Buffer of values to write
 *
 * RETURNS
 *  nothing
 *
 */
void gpakWriteDspNoSwap32 (ADT_Int32 DspId, DSP_Address DspAddress, ADT_UInt32 LongCnt,  ADT_UInt32 *Buff) {
    gpakWriteDspMemory32 (DspId, DspAddress, LongCnt,  Buff);
}


void gpakReadDspMemory16 (ADT_Int32 DspId, DSP_Address DspAddress, ADT_UInt32 WordCnt, ADT_UInt16 *Buff) {
	printf ("Read16 unimplemented %x [%d]\n", DspAddress, WordCnt);

    return ;

}
void gpakWriteDspMemory16 (ADT_Int32 DspId, DSP_Address DspAddress, ADT_UInt32 WordCnt,  ADT_UInt16 *Buff) {
	printf ("Write16 unimplemented  %x [%d]\n",  DspAddress, WordCnt);

   return;
}



/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakHostDelay - Delay for a fixed time interval.
 *
 * FUNCTION
 *  Delays for a fixed time interval before returning. The time
 *  interval is the Host Port Interface sampling period when polling a DSP for
 *  replies to command messages.
 *
 * RETURNS
 *  nothing
 *
 */
void gpakHostDelay (void) {

/* This function needs to be implemented by the G.PAK system integrator. */
   Sleep (2);
}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakLockAccess - Lock access to the specified DSP.
 *
 * FUNCTION
 *  Aquires exclusive access to the specified DSP.
 *
 * RETURNS
 *  nothing
 *
 */
void gpakLockAccess (ADT_Int32 DspId) {

/* This function needs to be implemented by the G.PAK system integrator. */
   if (DspId != 0) return;

 	glock ();
   while (Locked);
   Locked = TRUE;
}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakUnlockAccess - Unlock access to the specified DSP.
 *
 * FUNCTION
 *  Releases exclusive access to the specified DSP.
 *
 * RETURNS
 *  nothing
 *
 */
void gpakUnlockAccess (ADT_Int32 DspId) {
   
/* This function needs to be implemented by the G.PAK system integrator. */
   if (DspId != 0) return;
   
   while (!Locked);
   
   Locked = FALSE;
   gunlock ();
}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadFile - Read a block of bytes from a G.PAK Download file.
 *
 * FUNCTION
 *  Reads a contiguous block of bytes from a G.PAK Download file
 *  starting at the current file position.
 *
 * Inputs
 *   FileId -  Download file identifier
 *   pBuffer - Byte storage buffer
 *   ByteCnt     - Number of bytes to transfer
 *
 * RETURNS
 *  The number of bytes read from the file.
 *   -1 indicates an error occurred.
 *    0 indicates all bytes have been read (end of file)
 *
 */
int gpakReadFile (GPAK_FILE_ID FileId, ADT_UInt8 *pBuffer, ADT_UInt32 ByteCnt) {

    return fread (pBuffer, sizeof(char), ByteCnt, (FILE *) FileId);
}

/****************************************************************************
* NAME:  PCIDownloadCoff ()
*
* DESCRIPTION:  Download coff file to DSP 
*         
* Inputs
*     Dsp  -  Identifier for multiple DSPs
*    Filename - path and name of Coff file
*
* RETURN VALUE:
*    GdlSuccess      - Success
*    GdlInvalidFile  - Failure
*
****************************************************************************/
int PCIDownloadCoff (char *Filename) {

   
   if (!PCI64_LoadCoff (pciHandle, Filename))
      return 0;
   
   return 9;

}



//=============================================================================
//
//  Support for RTP APIs
//
//==============================================================================
#include "rtpapi.h"
static int RtpLocked = FALSE;
static int txpkts = 0, rxpkts = 0;

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * RTCPStats
 *
 * FUNCTION
 *  Generates statistics for RTCP reports
 *
 *  Inputs
 *      event -  RTPRECEIVE or RTPSEND.   
 *      RTPConnect - RTP data structure
 *      PktAddr    - Pointer to RTP packet
 *      PktSize    - Size in bytes of packtet
 *
 * RETURNS
 *  nothing
 *
 */

void RTCPStats (RTCPEVENTS event, RTPCONNECT* RTPConnect, ADT_UInt8* PktAddr, ADT_UInt16 PktSize) { 
  (void) PktSize;
  (void) RTPConnect;
  (void) PktAddr;

  if (event == RTPRECEIVE) rxpkts++;
  if (event == RTPSEND)	   txpkts++;
  return; 
}
     
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * RTPAlloc
 *
 * FUNCTION
 *  Allocates memory for RTP jitter buffer
 *
 * Inputs
 *    size - Number of bytes to allocate
 *
 * RETURNS
 *  pointer to allocated memory
 *
 */

void *RTPAlloc(ULONG size) { return malloc (size); }

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * RTPFree
 *
 * FUNCTION
 *  Release previously allocated memory
 *
 * Inputs
 *   Pointer to memory location
 *
 * RETURNS
 *  nothing
 *
 */

void RTPFree(void *mem) {	free (mem);  }

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * RTPLock
 *
 * FUNCTION
 *  Aquires exclusive access to the RTP structures.
 *
 * RETURNS
 *  nothing
 *
 */
void RTPLock() { 
	glock ();
	RtpLocked = TRUE;
	return;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * RTPUnlock
 *
 * FUNCTION
 *  Releases exclusive access to the RTP structures.
 *
 * RETURNS
 *  nothing
 *
 */
void RTPUnlock() {
	RtpLocked = FALSE;
   gunlock ();
	return; 
}

ADT_UInt32 RTPTime () {
   ADT_Int64 temp;
   
   // Convert current time to RTP time stamp
   temp =  (((ADT_Int64) clock()) * 8000) / CLOCKS_PER_SEC;
   return (ADT_UInt32) temp;
}



void RTPWait () {
   Sleep (0);
   return;
}

