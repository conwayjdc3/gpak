/*
 * Copyright (c) 2012, Adaptive Digital Technologies, Inc.
 *
 * File Name: TrilogyApi.c
 *
 * Description:
 *   This file contains Trilogy specific API functions to communicate with DSPs executing
 *   G.PAK software. 
 **
 */
#include <stdio.h>
#include "GpakHpi.h"
#include "GpakApi.h"
#include "GpakEnum.h"

#include "TrilogyApi.h"


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakPreRTPHeader
 *
 * FUNCTION
 *    Configure a channel's pre RTP header values.
 *
 * Inputs
 *      DspId  - Dsp identifier 
 *  ChannelId  - Channel identifier
 *      HdrI8  - Size of the RTP header
 *      rxHdr  - Pointer to expected values for in-bound RTP packets with pre-headers
 *      txHdr  - Pointer to values to send as pre-RTP headers
 *
 * Outputs
 *  dspStatus  - return status from DSP
 *
 * RETURNS
 *  Status code indicating success or a specific error.
 *
 */
#define CORE_ID(chan) ((chan >> 8) & 0xFF)
#define CHAN_ID(chan) (chan & 0xFF)
#define CNFR_ID(cnfr) (cnfr & 0xFF)

#if (DSP_WORD_I8 == 4)
   #define EndianSwap(word) swapI32(word)
#elif (DSP_WORD_I8 == 2)
   #define EndianSwap(word) swapI16(word)
#endif

#define NO_ID_CHK 0
#define BYTE_ID 1
#define WORD_ID 2

ADT_UInt32 TransactCmd (ADT_UInt32 DspId,         ADT_UInt32 *pMsgBufr,   ADT_UInt32 I16Cnt,     
                        ADT_UInt8  ExpectedReply, ADT_UInt32 MaxRplyI16,  ADT_UInt32 IdI8,
                        ADT_UInt16 IdValue);
//{
//   N = number of bytes in pre-header
//   n = number of 16-bit words in pre-header (one byte padding if necessary)
//
//   Word:Bits
//
//     0:ff00    -  Msg ID
//     0:00ff    -  Channel ID
//     1:00ff    -  Pre-header length (bytes)
//
//     2:ff00    -  First byte rx
//     2:00ff    -  Second byte rx
//             .   .   .
//   n+2:ff00    -  Nth odd byte rx
//   n+2:00ff    -  Nth even byte rx or padding
//
//   n+3:ff00    -  First byte tx
//   n+3:00ff    -  Second byte tx
//             .   .   .
//  2n+2:ff00    -  Nth odd byte tx
//} 2n+2:00ff    -  Nth even byte tx or padding

static ADT_UInt32 gpakFormatPreRTPHeader (ADT_UInt16 *Msg,  ADT_UInt16  ChannelId,
                                          ADT_UInt16 HdrI8, ADT_UInt8 *rxHdr, ADT_UInt8 *txHdr) {
    int i, j;

    /* Build the Configure Serial Ports message. */
    Msg[0]  = PackBytes (MSG_PRE_RTP_HDR, CHAN_ID(ChannelId));
    Msg[1]  = PackBytes (0, HdrI8);

   j = 2;
    for (i = 0; i < HdrI8; i += 2)
      Msg[j++] = PackBytes (rxHdr[i], rxHdr[i+1]);

    for (i = 0; i < HdrI8; i += 2)
      Msg[j++] = PackBytes (txHdr[i], txHdr[i+1]);

    return j;
}

GpakApiStatus_t gpakPreRTPHeader (ADT_UInt32 DspId,  ADT_UInt16 ChannelId, 
                                 ADT_UInt16 HdrI8, ADT_UInt8 *rxHdr, ADT_UInt8 *txHdr, 
                                 preHdrStat_t *dspStatus) {

    ADT_UInt32 MsgI16;                       /* message length */
    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];   /* message buffer */
    ADT_UInt16 *Msg;


    if (MaxDsps <= DspId)
        return RtpInvalidDsp;
        
    if (16 < HdrI8) {
       *dspStatus = preHdrLengthErr;
       return GpakApiParmError;
    }

    Msg =(ADT_UInt16 *) &MsgBuffer[0];

    MsgI16 = gpakFormatPreRTPHeader (Msg, ChannelId, HdrI8, rxHdr, txHdr);
    if (!TransactCmd (DspId, MsgBuffer, MsgI16, MSG_PRE_RTP_HDR_REPLY, 2,
                      BYTE_ID, (ADT_UInt16) CHAN_ID(ChannelId)))
        return GpakApiCommFailure;

    /* Return success or failure . */
    *dspStatus = (GPAK_RTPConfigStat_t) Byte0 (Msg[1]);
    if (*dspStatus == GpakApiSuccess)  return GpakApiSuccess;
    else                               return GpakApiParmError;
}

ADT_UInt32 gpakFormatMcBspPortConfig (ADT_UInt16 *Msg, GpakMcBspPortConfig_t *PortCfg) {

    /* Build the Configure Serial Ports message. */
    Msg[0] = MSG_CONFIGURE_MCBSPPORTS << 8;
    Msg[1] = 0;
    if (PortCfg->Port1Enable)        Msg[1] = 0x0001;
    if (PortCfg->Port2Enable)        Msg[1] |= 0x0002;
    if (PortCfg->Port3Enable)        Msg[1] |= 0x0004;
    //if (PortCfg->AudioPort1Enable)   Msg[1] |= 0x0008;
    //if (PortCfg->AudioPort2Enable)   Msg[1] |= 0x0010;

    Msg[2]  = (ADT_UInt16)((PortCfg->Port1SlotMask0_31  ) >> 16);  
    Msg[3]  = (ADT_UInt16)((PortCfg->Port1SlotMask0_31  ) & 0xffff);  
    Msg[4]  = (ADT_UInt16)((PortCfg->Port1SlotMask32_63 ) >> 16);  
    Msg[5]  = (ADT_UInt16)((PortCfg->Port1SlotMask32_63 ) & 0xffff);
    Msg[6]  = (ADT_UInt16)((PortCfg->Port1SlotMask64_95 ) >> 16);  
    Msg[7]  = (ADT_UInt16)((PortCfg->Port1SlotMask64_95 ) & 0xffff);
    Msg[8]  = (ADT_UInt16)((PortCfg->Port1SlotMask96_127) >> 16);  
    Msg[9]  = (ADT_UInt16)((PortCfg->Port1SlotMask96_127) & 0xffff);

    Msg[10] = (ADT_UInt16)((PortCfg->Port2SlotMask0_31  ) >> 16);  
    Msg[11] = (ADT_UInt16)((PortCfg->Port2SlotMask0_31  ) & 0xffff);
    Msg[12] = (ADT_UInt16)((PortCfg->Port2SlotMask32_63 ) >> 16);  
    Msg[13] = (ADT_UInt16)((PortCfg->Port2SlotMask32_63 ) & 0xffff);
    Msg[14] = (ADT_UInt16)((PortCfg->Port2SlotMask64_95 ) >> 16);  
    Msg[15] = (ADT_UInt16)((PortCfg->Port2SlotMask64_95 ) & 0xffff);
    Msg[16] = (ADT_UInt16)((PortCfg->Port2SlotMask96_127) >> 16);  
    Msg[17] = (ADT_UInt16)((PortCfg->Port2SlotMask96_127) & 0xffff);

    Msg[18] = (ADT_UInt16)((PortCfg->Port3SlotMask0_31  ) >> 16);  
    Msg[19] = (ADT_UInt16)((PortCfg->Port3SlotMask0_31  ) & 0xffff);
    Msg[20] = (ADT_UInt16)((PortCfg->Port3SlotMask32_63 ) >> 16);  
    Msg[21] = (ADT_UInt16)((PortCfg->Port3SlotMask32_63 ) & 0xffff);
    Msg[22] = (ADT_UInt16)((PortCfg->Port3SlotMask64_95 ) >> 16);  
    Msg[23] = (ADT_UInt16)((PortCfg->Port3SlotMask64_95 ) & 0xffff);
    Msg[24] = (ADT_UInt16)((PortCfg->Port3SlotMask96_127) >> 16);  
    Msg[25] = (ADT_UInt16)((PortCfg->Port3SlotMask96_127) & 0xffff);
    //Msg[26] = (ADT_UInt16)((PortCfg->AudioPort1TxSerMask) >> 16);  
    //Msg[27] = (ADT_UInt16)((PortCfg->AudioPort1TxSerMask) & 0xffff);
    //Msg[28] = (ADT_UInt16)((PortCfg->AudioPort1RxSerMask) >> 16);  
    //Msg[29] = (ADT_UInt16)((PortCfg->AudioPort1RxSerMask) & 0xffff);
    //Msg[30] = (ADT_UInt16)((PortCfg->AudioPort1SlotMask ) >> 16);  
    //Msg[31] = (ADT_UInt16)((PortCfg->AudioPort1SlotMask ) & 0xffff);

    //Msg[32] = (ADT_UInt16)((PortCfg->AudioPort2TxSerMask) >> 16);  
    //Msg[33] = (ADT_UInt16)((PortCfg->AudioPort2TxSerMask) & 0xffff);
    //Msg[34] = (ADT_UInt16)((PortCfg->AudioPort2RxSerMask) >> 16);  
    //Msg[35] = (ADT_UInt16)((PortCfg->AudioPort2RxSerMask) & 0xffff);
    //Msg[36] = (ADT_UInt16)((PortCfg->AudioPort2SlotMask ) >> 16);  
    //Msg[37] = (ADT_UInt16)((PortCfg->AudioPort2SlotMask ) & 0xffff);

    Msg[38] = PackBytes (PortCfg->ClkDiv2,     PortCfg->ClkDiv1);
    Msg[39] = PackBytes (PortCfg->FrameWidth1, PortCfg->ClkDiv3);
    Msg[40] = PackBytes (PortCfg->FrameWidth3, PortCfg->FrameWidth2);

    Msg[41] = (ADT_UInt16)(((PortCfg->FramePeriod1 << 4) & 0xfff0) |
                         ((PortCfg->Compand1 << 1) & 0xE) | (PortCfg->SampRateGen1 & 1) );
    Msg[42] = (ADT_UInt16)(((PortCfg->FramePeriod2 << 4) & 0xfff0) |
                         ((PortCfg->Compand2 << 1) & 0xE) | (PortCfg->SampRateGen2 & 1) );

    Msg[43] = (ADT_UInt16)(((PortCfg->FramePeriod3 << 4) & 0xfff0) |
                         ((PortCfg->Compand3 << 1) & 0xE) | (PortCfg->SampRateGen3 & 1) );

    Msg[44] = (ADT_UInt16 )( (PortCfg->TxDataDelay1 & 0x0003)       | ((PortCfg->RxDataDelay1 << 2) & 0x000c) |
                          ((PortCfg->TxDataDelay2 << 4) & 0x0030) | ((PortCfg->RxDataDelay2 << 6) & 0x00c0) |
                          ((PortCfg->TxDataDelay3 << 8) & 0x0300) | ((PortCfg->RxDataDelay3 << 10) & 0x0c00) );
    Msg[45] =(ADT_UInt16)(((PortCfg->rxClockPolarity3   << 11) & 0x800) |
                         ((PortCfg->rxClockPolarity2    << 10) & 0x400) | 
                         ((PortCfg->rxClockPolarity1     << 9) & 0x200) | 
                         ((PortCfg->txClockPolarity3     << 8) & 0x100) | 
                         ((PortCfg->txClockPolarity2     << 7) & 0x80) | 
                         ((PortCfg->txClockPolarity1     << 6) & 0x40) | 
                         ((PortCfg->rxFrameSyncPolarity3 << 5) & 0x20) | 
                         ((PortCfg->rxFrameSyncPolarity2 << 4) & 0x10) | 
                         ((PortCfg->rxFrameSyncPolarity1 << 3) & 0x8) | 
                         ((PortCfg->txFrameSyncPolarity3 << 2) & 0x4) | 
                         ((PortCfg->txFrameSyncPolarity2 << 1) & 0x2) | 
						  (PortCfg->txFrameSyncPolarity1       & 0x1) ); 
    return 46;
}

GpakApiStatus_t gpakConfigureMcBspPorts (ADT_UInt32 DspId, GpakPortConfig_t *pPortConfig, 
                                           GPAK_PortConfigStat_t *pStatus) {

    ADT_UInt32 MsgLenI16;                        /* message length */
    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];    /* message buffer */
    ADT_UInt16 *Msg;


    if (MaxDsps <= DspId)
        return GpakApiInvalidDsp;

    Msg =(ADT_UInt16 *) &MsgBuffer[0];

    MsgLenI16 = gpakFormatMcBspPortConfig (Msg, pPortConfig);

    if (!TransactCmd (DspId, MsgBuffer, MsgLenI16, MSG_CONFIGURE_MCBSPPORTS_REPLY, 2, NO_ID_CHK, 0))
        return GpakApiCommFailure;

    /* Return success or failure . */
    *pStatus = (GPAK_PortConfigStat_t) Byte1 (Msg[1]);
    if (*pStatus == Pc_Success)
        return GpakApiSuccess;
    else
        return GpakApiParmError;
}

GpakApiStatus_t gpakParseG722RTPStats (ADT_UInt32 ReplyI16, ADT_UInt16 *Rply, 
                                       G722RtpDebugStats_t *RTPStats, 
                                       g722RtpStat_t *dspStatus) {

   memset (RTPStats, 0, sizeof (G722RtpDebugStats_t));
   *dspStatus = (g722RtpStat_t) Byte0 (Rply[0]);
   if (*dspStatus != g722RtpStatSuccess)
      return GpakApiParmError;

   if (ReplyI16 != 62) {
      return GpakApiCommFailure;
   }

   RTPStats->hdrDiscards        = PackWords (Rply[2 ],   Rply[3 ]);
   RTPStats->payDiscards        = PackWords (Rply[4 ],   Rply[5 ]);
   RTPStats->folDiscards        = PackWords (Rply[6 ],   Rply[7 ]);
   RTPStats->cidDiscards        = PackWords (Rply[8 ],   Rply[9 ]);
   RTPStats->inaDiscards        = PackWords (Rply[10],   Rply[11]);
   RTPStats->invDiscards        = PackWords (Rply[12],   Rply[13]);
   RTPStats->srtpDiscards       = PackWords (Rply[14],   Rply[15]);
   RTPStats->sr0_count          = PackWords (Rply[16],   Rply[17]);
   RTPStats->sr1_count          = PackWords (Rply[18],   Rply[19]);
   RTPStats->sr8_adj_count      = PackWords (Rply[20],   Rply[21]);
   RTPStats->sr8_nadj_count     = PackWords (Rply[22],   Rply[23]);
   RTPStats->sr16_adj_count     = PackWords (Rply[24],   Rply[25]);
   RTPStats->sr16_nadj_count    = PackWords (Rply[26],   Rply[27]);
   RTPStats->discard_count      = PackWords (Rply[28],   Rply[29]);

   RTPStats->g722Sync[0].rtp_sr     = PackWords (Rply[30],   Rply[31]);
   RTPStats->g722Sync[0].sr1_count  = PackWords (Rply[32],   Rply[33]);
   RTPStats->g722Sync[0].Sequence   = PackWords (Rply[34],   Rply[35]);
   RTPStats->g722Sync[0].TimeStamp  = PackWords (Rply[36],   Rply[37]);
   RTPStats->g722Sync[0].BitFlds    = PackWords (Rply[38],   Rply[39]);
   RTPStats->g722Sync[0].hdrI8Len   = PackWords (Rply[40],   Rply[41]);
   RTPStats->g722Sync[0].PktI8      = PackWords (Rply[42],   Rply[43]);
   RTPStats->g722Sync[0].pyldI8Len  = PackWords (Rply[44],   Rply[45]);

   RTPStats->g722Sync[1].rtp_sr     = PackWords (Rply[46],   Rply[47]);
   RTPStats->g722Sync[1].sr1_count  = PackWords (Rply[48],   Rply[49]);
   RTPStats->g722Sync[1].Sequence   = PackWords (Rply[50],   Rply[51]);
   RTPStats->g722Sync[1].TimeStamp  = PackWords (Rply[52],   Rply[53]);
   RTPStats->g722Sync[1].BitFlds    = PackWords (Rply[54],   Rply[55]);
   RTPStats->g722Sync[1].hdrI8Len   = PackWords (Rply[56],   Rply[57]);
   RTPStats->g722Sync[1].PktI8      = PackWords (Rply[58],   Rply[59]);
   RTPStats->g722Sync[1].pyldI8Len  = PackWords (Rply[60],   Rply[61]);

   return GpakApiSuccess;
}
GpakApiStatus_t gpakReadG722RTPStats (ADT_UInt32 DspId,  ADT_UInt16 ChanId, G722RtpDebugStats_t *g722RTPStats, 
                                 g722RtpStat_t *dspStatus) {

    ADT_UInt32 RplyI16;                       /* message length */
    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];   /* message buffer */
    ADT_UInt16 *Msg;
    GpakApiStatus_t apiStatus;


    if (MaxDsps <= DspId)
        return RtpInvalidDsp;
        
    Msg =(ADT_UInt16 *) &MsgBuffer[0];

    Msg[0] = PackBytes (MSG_G722_RTP_STATS, CORE_ID(ChanId));
    Msg[1] = (ADT_UInt16) (CHAN_ID (ChanId) << 8);

    RplyI16 = TransactCmd (DspId, MsgBuffer, 2, MSG_G722_RTP_STATS_REPLY, MSG_BUFFER_ELEMENTS, 
                          BYTE_ID, (ADT_UInt16) CHAN_ID(ChanId));

    apiStatus = gpakParseG722RTPStats (RplyI16, Msg, g722RTPStats, dspStatus);
    if (apiStatus == GpakApiCommFailure) {
       DSPError[DspId] =  DSPRplyLengthError;
    } 
    return apiStatus;
}

GpakApiStatus_t gpakReleaseTDMPortSlot (ADT_UInt32 DspId,  
                                 GpakSerialPort_t InPortId,  ADT_UInt16 InSlotId,
                                 GpakSerialPort_t OutPortId, ADT_UInt16 OutSlotId,
                                 ADT_UInt16 *dspStatus) {

    ADT_UInt32 MsgI16;                       /* message length */
    ADT_UInt32 MsgBuffer[MSG_BUFFER_SIZE];   /* message buffer */
    ADT_UInt16 *Msg;
    ADT_UInt16  status;


    if (MaxDsps <= DspId)
        return GpakApiInvalidDsp;
        
    Msg =(ADT_UInt16 *) &MsgBuffer[0];

    Msg[0]  = PackBytes (MSG_SLOT_RELEASE, 0);
    Msg[1]  = PackBytes (InPortId,  InSlotId);
    Msg[2]  = PackBytes (OutPortId, OutSlotId);
    MsgI16 = 3;

    if (!TransactCmd (DspId, MsgBuffer, MsgI16, MSG_SLOT_RELEASE_REPLY, 2,
                      NO_ID_CHK , 0))
        return GpakApiCommFailure;

    /* Return success or failure . */
    status = Byte0 (Msg[0]);
    *dspStatus = Msg[1];

    if (status == GpakApiSuccess)  return GpakApiSuccess;
    else                           return GpakApiParmError;
}
