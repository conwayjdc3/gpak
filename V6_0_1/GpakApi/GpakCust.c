/*
 * Copyright (c) 2002, Adaptive Digital Technologies, Inc.
 *
 * File Name: GpakCust.c
 *
 * Description:
 *   This file contains host system dependent functions to support generic
 *   G.PAK API functions. The file is integrated into the host processor
 *   connected to G.PAK DSPs via a Host Port Interface.
 *
 *   Note: This file needs to be modified by the G.PAK system integrator.
 *
 * Version: 1.0
 *
 * Revision History:
 *   10/17/01 - Initial release.
 *
 */

#include "GpakCust.h"
#include "GpakApi.h"


unsigned int MaxCoresPerDSP   = CORES_PER_DSP;
unsigned int MaxDsps          = MAX_DSP_CORES;
unsigned int MaxChannelAlloc  = MAX_CHANNELS;
unsigned int MaxWaitLoops     = MAX_WAIT_LOOPS;
unsigned int DownloadI8       = DOWNLOAD_BLOCK_I8;

#if defined (HOST_BIG_ENDIAN)
unsigned int HostBigEndian   = 1;
#elif defined (HOST_LITTLE_ENDIAN)
unsigned int HostBigEndian   = 0;
#else
   #error HOST_ENDIAN_MODE must be defined as either BIG or LITTLE
#endif

/* DSP Related values */
DSP_Address  ifBlkAddress = DSP_IFBLK_ADDRESS;                // Interface block address

GPAK_DspCommStat_t DSPError[MAX_DSP_CORES];

DSP_Word    MaxChannels[MAX_DSP_CORES]  = { 0, 0 };  /* max num channels */
DSP_Word    MaxCmdMsgLen[MAX_DSP_CORES] = { 0, 0 };  /* max Cmd msg length (octets) */

DSP_Address pDspIfBlk[MAX_DSP_CORES] = { 0, 0 };          /* DSP address of interface blocks */
DSP_Address pEventFifoAddress[MAX_DSP_CORES] = { 0, 0 };  /* Event circular buffers */


DSP_Address pPktInBufr [MAX_DSP_CORES][MAX_CHANNELS];     /* Pkt In circular buffers */
DSP_Address pPktOutBufr[MAX_DSP_CORES][MAX_CHANNELS];     /* Pkt Out circular buffers */

/* Download buffers */
ADT_UInt8  DlByteBufr_[(DOWNLOAD_BLOCK_I8 + sizeof(ADT_UInt8))/sizeof(ADT_UInt8)];    /* Dowload byte buf */
DSP_Word   DlWordBufr [(DOWNLOAD_BLOCK_I8 + sizeof(DSP_Word))/sizeof(DSP_Word)];      /* Dowload word buffer */

#ifdef NETWORK_MESSAGING
void *invalid_socket = INVALID_SOCKET;   // System definition of invalid socket handle

ADT_UInt32 LocalIP;                    // IP Address of host
ADT_UInt32 RemoteIP [MAX_DSP_CORES];   // IP Addresses of DSPs
void *msgSkt[MAX_DSP_CORES];           // DSPs TCP Messaging sockets
void *evtSkt[MAX_DSP_CORES];           // DSPs TCP event sockets
char gpakVerifyTag[MAX_DSP_CORES][GPAK_TAG_I8];   // DSPs handshaking verification tags

short msg_tcp_port = (short) 56765;
short evt_tcp_port = (short) 56766;

#endif


DSP_Address *getPktInBufrAddr  (int DspID, int channel) {
   return &pPktInBufr[DspID][channel];
}
DSP_Address *getPktOutBufrAddr (int DspID, int channel) {
   return &pPktOutBufr[DspID][channel];
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadDspMemory8 - Read DSP memory.
 *
 * FUNCTION
 *  This function reads a contiguous block of bytes from DSP memory starting at
 *  the specified address.
 *
 * RETURNS
 *  nothing
 *
 */
void gpakReadDspMemory8(
    ADT_UInt32   DspId,         /* DSP Identifier (0 to MAX_DSP_CORES-1) */
    DSP_Address DspAddress,    /* DSP's memory address of first word */
    ADT_UInt32  NumBytes,      /* number of contiguous bytes to read */
    ADT_UInt8  *pByteValues    /* pointer to array of byte values variable */
    )
{

/* This function needs to be implemented by the G.PAK system integrator. */

}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadDspMemory16 - Read DSP memory.
 *
 * FUNCTION
 *  This function reads a contiguous block of 16-bit word from DSP memory starting at
 *  the specified address.
 *
 * RETURNS
 *  nothing
 *
 */
void gpakReadDspMemory16(
    ADT_UInt32   DspId,           /* DSP Identifier (0 to MAX_DSP_CORES-1) */
    DSP_Address DspAddress,      /* DSP's memory address of first word */
    ADT_UInt32  NumShorts,       /* number of contiguous short to read */
    ADT_UInt16  *pShortValues    /* pointer to array of byte values variable */
    )
{

/* This function needs to be implemented by the G.PAK system integrator for C5x processors */
/*  Byte swapping is required for big endian hosts */
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadDspNoSwap16 - Read DSP packet memory in network byte order.
 *
 * FUNCTION
 *  This function reads a contiguous block of 16-bit values from DSP memory 
 *  starting at the specified address.  DSP packet memory is in big-endian order
 *
 * RETURNS
 *  nothing
 *
 */
void gpakReadDspNoSwap16(
    ADT_UInt32   DspId,           /* DSP Identifier (0 to MAX_DSP_CORES-1) */
    DSP_Address DspAddress,      /* DSP's memory address of first word */
    ADT_UInt32  NumShorts,       /* number of contiguous 32-bit vals to read */
    ADT_UInt32 *pShortValues     /* pointer to array of 32-bit values variable */
    )
{

/* This function needs to be implemented by the G.PAK system integrator for C5x processors */
/*  Packets are stored in DSP memory in network-byte order and must be transferred to
    network-byte order on the host.

    On C54 and C55 machines, the even bytes are stored in the low-byte and odd bytes are 
    stored in the high-byte of the DSP memory.
    
*/

}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadDspMemory32 - Read DSP memory.
 *
 * FUNCTION
 *  This function reads a contiguous block of 32-bit values from DSP memory 
 *  starting at the specified address. 
 *
 * RETURNS
 *  nothing
 *
 */
void gpakReadDspMemory32(
    ADT_UInt32   DspId,           /* DSP Identifier (0 to MAX_DSP_CORES-1) */
    DSP_Address DspAddress,      /* DSP's memory address of first word */
    ADT_UInt32  NumLongs,        /* number of contiguous 32-bit vals to read */
    ADT_UInt32 *pLongValues      /* pointer to array of 32-bit values variable */
    )
{

/* This function needs to be implemented by the G.PAK system integrator for C6x processors */
/*  Byte swapping is required for big endian hosts */

}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadDspNoSwap32 - Read DSP packet memory without byte swapping.
 *
 * FUNCTION
 *  This function reads a contiguous block of 32-bit values from DSP memory 
 *  starting at the specified address.  DSP packet memory is in big-endian order
 *
 * RETURNS
 *  nothing
 *
 */
void gpakReadDspNoSwap32(
    ADT_UInt32   DspId,           /* DSP Identifier (0 to MAX_DSP_CORES-1) */
    DSP_Address DspAddress,      /* DSP's memory address of first word */
    ADT_UInt32  NumLongs,        /* number of contiguous 32-bit vals to read */
    ADT_UInt32 *pLongValues      /* pointer to array of 32-bit values variable */
    )
{

/* This function needs to be implemented by the G.PAK system integrator for C6x processors */
/*  Packets are stored in DSP memory in network-byte order and must be transferred to
    network byte order on the host.

    All G.PAK C6x platforms are little endian format.  Network-byte order on the C6x platforms
    require the 1st network byte to be stored in the low byte of the 1st 32-bit word and the 4th
    network byte to be stored in the high byte of the 1st 32-bit word.
*/

}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakWriteDspMemory8 - Write DSP memory.
 *
 * FUNCTION
 *  This function writes a contiguous block of bytes to DSP memory starting at
 *  the specified address.
 *
 * RETURNS
 *  nothing
 *
 */
void gpakWriteDspMemory8(
    ADT_UInt32   DspId,          /* DSP Identifier (0 to MAX_DSP_CORES-1) */
    DSP_Address DspAddress,     /* DSP's memory address of first word */
    ADT_UInt32  NumBytes,       /* number of contiguous bytes to write */
    ADT_UInt8  *pByteValues     /* pointer to array of byte values to write */
    )
{

/* This function needs to be implemented by the G.PAK system integrator */

}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakWriteDspMemory16 - Write DSP memory.
 *
 * FUNCTION
 *  This function writes a contiguous block of words to DSP memory starting at
 *  the specified address.
 *
 * RETURNS
 *  nothing
 *
 */
void gpakWriteDspMemory16(
    ADT_UInt32 DspId,            /* DSP Identifier (0 to MAX_DSP_CORES-1) */
    DSP_Address DspAddress,     /* DSP's memory address of first word */
    ADT_UInt32 NumShorts,       /* number of contiguous words to write */
    ADT_UInt16 *pShortValues    /* pointer to array of word values to write */
    )
{

/* This function needs to be implemented by the G.PAK system integrator. */
/*  Byte swapping is required for big endian hosts */

}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakWriteDspNoSwap16 - Write DSP packet memory without byte swapping.
 *
 * FUNCTION
 *  This function writes a contiguous block of words to DSP memory starting at
 *  the specified address.
 *
 * RETURNS
 *  nothing
 *
 */
void gpakWriteDspNoSwap16(
    ADT_UInt32 DspId,            /* DSP Identifier (0 to MAX_DSP_CORES-1) */
    DSP_Address DspAddress,     /* DSP's memory address of first word */
    ADT_UInt32 NumShorts,       /* number of contiguous words to write */
    ADT_UInt16 *pShortValues    /* pointer to array of word values to write */
    )
{

/* This function needs to be implemented by the G.PAK system integrator. */
/*  Packets are stored in DSP memory in network-byte order and must be transferred to
    network-byte order on the host.

    On C54 and C55 machines, the even bytes are stored in the low-byte and odd bytes are 
    stored in the high-byte of the DSP memory.
    
*/

}
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakWriteDspMemory32 - Write DSP memory.
 *
 * FUNCTION
 *  This function writes a contiguous block of 32-bit values to DSP memory 
 *  starting at the specified address.
 *
 * RETURNS
 *  nothing
 *
 */
void gpakWriteDspMemory32(
    ADT_UInt32 DspId,            /* DSP Identifier (0 to MAX_DSP_CORES-1) */
    DSP_Address DspAddress,     /* DSP's memory address of first word */
    ADT_UInt32 NumLongs,        /* number of contiguous 32-bit vals to write */
    ADT_UInt32 *pLongValues     /* pointer to array of 32-bit values to write */
    )
{

/* This function needs to be implemented by the G.PAK system integrator. */
/*  Byte swapping is required for big endian hosts */

}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakWriteDspNoSwap32 - Write DSP packet memory without byte swapping.
 *
 * FUNCTION
 *  This function writes a contiguous block of words to DSP memory starting at
 *  the specified address.
 *
 * RETURNS
 *  nothing
 *
 */
void gpakWriteDspNoSwap32(
    ADT_UInt32 DspId,            /* DSP Identifier (0 to MAX_DSP_CORES-1) */
    DSP_Address DspAddress,     /* DSP's memory address of first word */
    ADT_UInt32 NumLongs,        /* number of contiguous longs to write */
    ADT_UInt32 *pLongValues     /* pointer to array of long values to write */
    )
{

/* This function needs to be implemented by the G.PAK system integrator. */
/*  Packets are stored in DSP memory in network-byte order and must be transferred to
    network byte order on the host.

    All G.PAK C6x platforms are little endian format.  Network-byte order on the C6x platforms
    require the 1st network byte to be stored in the low byte of the 1st 32-bit word and the 4th
    network byte to be stored in the high byte of the 1st 32-bit word.
*/


}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakHostDelay - Delay for a fixed time interval.
 *
 * FUNCTION
 *  This function delays for a fixed time interval before returning. The time
 *  interval is the Host Port Interface sampling period when polling a DSP for
 *  replies to command messages.
 *
 * RETURNS
 *  nothing
 *
 */
void gpakHostDelay(void)
{

/* This function needs to be implemented by the G.PAK system integrator. */

}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakLockAccess - Lock access to the specified DSP.
 *
 * FUNCTION
 *  This function aquires exclusive access to the specified DSP.
 *
 * RETURNS
 *  nothing
 *
 */
void gpakLockAccess(
    ADT_UInt32 DspId                   /* DSP Identifier (0 to MAX_DSP_CORES-1) */
    )
{

/* This function needs to be implemented by the G.PAK system integrator. */

}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakUnlockAccess - Unlock access to the specified DSP.
 *
 * FUNCTION
 *  This function releases exclusive access to the specified DSP.
 *
 * RETURNS
 *  nothing
 *
 */
void gpakUnlockAccess(
    ADT_UInt32 DspId                   /* DSP Identifier (0 to MAX_DSP_CORES-1) */
    )
{

/* This function needs to be implemented by the G.PAK system integrator. */

}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadFile - Read a block of bytes from a G.PAK Download file.
 *
 * FUNCTION
 *  This function reads a contiguous block of bytes from a G.PAK Download file
 *  starting at the current file position.
 *
 * RETURNS
 *  The number of bytes read from the file.
 *   -1 indicates an error occurred.
 *    0 indicates all bytes have been read (end of file)
 *
 */
int gpakReadFile(
    void*      FileId,        /* G.PAK Download File Identifier */
    ADT_UInt8 *pBuffer,	/* pointer to buffer for storing bytes */
    ADT_UInt32 NumBytes       /* number of bytes to read */
    )
{

/* This function needs to be implemented by the G.PAK system integrator. */

    return (0);
}


#ifdef NETWORK_MESSAGING
//  Customer supplied routines for TCP/IP communications with the DSP
//{
// Initialize the host network stack
//
// Output parameters:
//      LocalIP -  Local IP address used to communicate with DSPs in network byte order
//
// Returns:
//      0.             Success
//     Negative value. Error value.
//}
int gpakNetworkStackInit (ADT_UInt32 *LocalIP) {
/* This function needs to be implemented by the G.PAK system integrator. */
   return 0;
}

//{
// Open a TCP socket
//
//  Input Parameters:
//     DspIP    - Dsp's IP address
//     tcpPort  - Dsp's TCP port address
//
//  Returns:
//     NULL - open failure
//     Non-null - Socket handle cast as void pointer
//
//}
void *gpakOpenTCPSocket (ADT_UInt32 DspIP, ADT_UInt16 tcpPort) {
/* This function needs to be implemented by the G.PAK system integrator. */
   SOCKET skt;
   return (void *) skt;
}

//{
// Close a TCP socket
//
//  Input Parameters:
//     skt   - Socket handle cast as void pointer
//}
void gpakCloseTCPSocket (void *skt) {
/* This function needs to be implemented by the G.PAK system integrator. */
}

//{
//  Convert DSP socket to non-blocking
//
//  Input Parameters:
//     skt   - Socket handle cast as void pointer
//
//  Returns:
//     0.              Success
//     Negative value. Error value.
//
//}
int gpakUnblockTCPSocket (void *skt) {
   int error;
/* This function needs to be implemented by the G.PAK system integrator. */
   return error;
}

//{
// Send command message to DSP via socket.
//
//  Input Parameters:
//     skt   - Socket handle cast as void pointer
//     msgBuf   - Pointer to message buffer to be transmitted.  
//     msgBufI8 - Number of bytes to transmit
//
//  Returns:
//     Positive value. Number of bytes transmitted
//     Negative value. Error value.
//
//  NOTE: This routine should return only after message has been 
//        successfully sent or an error has been detected on the socket
//}
int gpakSendDspMsg (void *skt, void *msgBuf, int msgBufI8) {
   int rslt;

/* This function needs to be implemented by the G.PAK system integrator. */
   return rslt;
}

//{
// Receive command reply from DSP via socket
//  Input Parameters:
//     skt   - Socket handle cast as void pointerID.
//     msgBuf   - Pointer to message buffer to be transmitted.  
//     msgBufI8 - Number of bytes to transmit
//
//  Returns:
//     0.              No reply
//     Positive value. Number of bytes received.
//     Negative value. Error value.
//
//}
int gpakRecvDspMsg (void *skt, void *msgBuf, int msgBufI8) {

   int rcvI8;

/* This function needs to be implemented by the G.PAK system integrator. */
   return rcvI8;
}

#endif
