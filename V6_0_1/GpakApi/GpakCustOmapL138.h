/*
 * Copyright (c) 2001-2010, Adaptive Digital Technologies, Inc.
 *
 * File Name: GpakCustDspLink.h
 *
 * Description:
 *   This file contains host system dependent definitions and prototypes of
 *   functions to support generic G.PAK API functions. The file is used when
 *   integrating G.PAK API functions in a specific host processor environment.
 *
 *   Note: This file may need to be modified by the G.PAK system integrator.
 *
 * Version: 1.0
 *
 * Revision History:
 *   10/17/01 - Initial release.
 *
 */

#ifndef _GPAKCUST_H  /* prevent multiple inclusion */
#define _GPAKCUST_H


#define snprintf(str,sz,fmt,val)  sprintf (str, fmt, val)
#define _itoa_s(val,b,bI8,r) itoa (val,b,r)
#define strcat_s(b,bI8,s) strcat(b,s)
#define strcpy_s(b,bI8,s) strcpy(b,s)
#define fopen_s(f,fn,a)   *(f) = fopen(fn,a)


#define HOST_LITTLE_ENDIAN         /* un-comment this if little endian host */
//#define HOST_BIG_ENDIAN            /* un-comment this if big endian host */
#define _TI_STD_TYPES

#define MAX_DSP_CORES         1       /* maximum number of DSP messaging cores */
#define MAX_CHANNELS         32       /* maximum number of channels */
#define DOWNLOAD_BLOCK_I8   512        /* download block size (bytes) */
#define MAX_WAIT_LOOPS       15       /* max number of wait delay loops */

#endif  /* prevent multiple inclusion */


