// GpakApiExample.c
//
//

#include <winsock2.h>
#include <ws2tcpip.h>
#include <windows.h>
#include <process.h>
#include <conio.h>
#include <stdio.h>

#include "GpakApi.h"

// G.PAK custom API routine for initialization
extern void  initGpakStructures ();

// APIs to connect to DSPs via the PCI bus
extern void (*ProcessDSPInterrupts)(void);
extern char *PCIConnect (ADT_Int32 Dsp);
extern char *PCIReset   (ADT_Int32 Dsp);
extern char *PCIRelease (ADT_Int32 Dsp);
extern void  PCIDisconnect (ADT_Int32 Dsp);
extern void  GenerateInterruptToDSP (ADT_Int32 DspId);

extern traceMem  (int, int, int);
extern traceData (int *, int);

static void printStats ();
static void dspTearDown (ADT_UInt32 DspId);
static void gpakGenTone (ADT_UInt32 DspId, short int cmd);
static void gpakInsertTone (ADT_UInt32 DspId, short int cmd);
static void gpakGetChanStat(ADT_UInt32 DspId, short int cmd);

static void gpakSendSrtpKey(ADT_UInt32 DspId, int chId, GpakAsyncEventData_t *eventData) ;

#define DSP_LOOPBACK

#define MAX_CHANNELS 4 // 2  //
int num_Of_NeyKeys= 0;
// 
#define NO_LOOP       1
#define LOOPBACK      2
#define CROSSOVER     3
#define CROSSOVER_PKT 4

// ========================================
//  Channel configuration defaults
typedef struct chanCfg {
   int LOOPBACKMODE;
   int PCM_EC;
   int PKT_EC;
   int AEC;
   int AGC;
   int VAD;
   int TONE;
   int ENCODE;
   int DECODE;
   int RTP_CODEC_PYLD;
   int ENCSZ;
   int DECSZ;
   int CID;
   int FAX;
	int SRTP;
} chanCfg;

chanCfg DefaultChan = {
   CROSSOVER,   //  LOOPBACKMODE 
   Disabled,    //  PCM_EC
   Disabled,    //  PKT_EC
   Disabled,    //  AEC
   Disabled,    //  AGC
   Disabled,    //  VAD
   0,           //  TONE
   PCMU_64,     //  ENCODE
   PCMU_64,     //  DECODE
   PCMU_64,     //  RTP_CODEC_PYLD
   FrameSize_20_0ms,  // ENCSZ 
   FrameSize_20_0ms,  // DECSZ 
   CidDisabled, //  CID
   disabled,     // FAX faxVoice
	disabled
};

chanCfg ActiveChan;


#define NOISE  Disabled
#define McBSP  McBSPPort1


#define RTP_PORT  6334
#define TG_GAIN  0
#define IN_GAIN  0
#define OUT_GAIN 0
#define TDM_SLOT_OFFSET  1

#define TEST_ON_EVM 
#ifdef TEST_ON_EVM
//#define IMAGE_NAME "G:\\V6_0_1\\vtechSipGW\\vtechSipGWrtdxevm.bin"
//#define IMAGE_NAME "G:\\Deliver\\vtechsipgw_Aug17_2011\\vtechSipGWrtdxevm.bin"
#define IMAGE_NAME NULL
#else
//#define IMAGE_NAME "G:\\V6_0_1\\vtechSipGW\\vtechSipGW.bin"
#define IMAGE_NAME "G:\\Deliver\\vtechsipgw_Aug17_2011\\vtechSipGW.bin"
//#define IMAGE_NAME "G:\\V6_0_1\\vtechSipGW\\vtechSipGW.bin"
//#define IMAGE_NAME "G:\\Deliver\\vtech\\vtech.bin"
#define IMAGE_NAME NULL
#endif


static char *prompt = NULL;
static unsigned int threadHandles [2];
static int exitFlag = FALSE;

// ========================================
//  Network socket interface

typedef struct _SOCKET_LIST {
    ADT_Int32       iAddressCount;
    SOCKET_ADDRESS  Address[10];
} SOCKET_LIST;

typedef struct sockaddr_in  SktInAddr;


WSADATA wsa;
SOCKET rtpSkt = INVALID_SOCKET;

SktInAddr LocalIPAddr;         // Local RTP port/IP address
SktInAddr Dest[MAX_CHANNELS];  // List of destination RTP port/IP addresses
SktInAddr RemoteAddr;          // Source RTP port/IP address

FILE *rtpInFile0  = NULL;
FILE *rtpOutFile0 = NULL;
FILE *rtpInFile1  = NULL;
FILE *rtpOutFile1 = NULL;

// -----------------------------------------------------------------------------
// DSP interface
typedef struct RTP {
   struct rtpHdr {
      int vers:8;
      int pyldType:8;
      int sequence:16;
      ADT_UInt32 timeStamp;
      ADT_UInt32 SSRC;
   } RTP;
   ADT_UInt8 data[480];
} RTPPkt;

HANDLE rtpSemaphore = NULL;
HANDLE evtSemaphore = NULL;

RTPPkt rtpPkt; 
int toDSPCnt = 0, toDSPDropped = 0;
int toNetCnt = 0, toNetDropped = 0;
int DSPInterruptCnt = 0;


static void appExit (void) {
   PCIDisconnect (0);
   PCIDisconnect (1);
   if (kbhit ()) getch ();
   getch ();
}

static appErr (int exitCode) {
   printf ("Application error %d\n", exitCode);
   exit (exitCode);
}

//=================================================
//  DSP TDM serial port initialization
//
static void dspTDMInit (ADT_UInt32 DspId) {




   // McBSP configuration variables
   GpakPortConfig_t       PortConfig;
   gpakConfigPortStatus_t PrtCfgStat = 0;
   GPAK_PortConfigStat_t  PrtDspStat = 0;

   dspTearDown (DspId);

   memset (&PortConfig, 0, sizeof (PortConfig));

   // Configure McBSP1, Disable McBSP0 and McBSP2
   PortConfig.AudioPort1Enable = Disabled;
   PortConfig.AudioPort2Enable = Disabled;

   // McBSP 0 (Attached to daughter card)
   PortConfig.Port1Enable  = Disabled;
   PortConfig.SlotsSelect1 = Disabled;

   // McBSP 1 (Attached to AIC)
   PortConfig.SlotsSelect2 = Disabled;
   PortConfig.SampRateGen2 = Disabled; 

   PortConfig.Port2Enable  = Enabled;
   PortConfig.Port2SlotMask0_31 = 3;
   PortConfig.TxDataDelay2 = 1;
   PortConfig.RxDataDelay2 = 1;
   PortConfig.Compand2  = cmpNone16;

   // McBSP 2 (Not available)
   PortConfig.Port3Enable  = Disabled;
   PortConfig.SlotsSelect3 = Disabled;

   PrtCfgStat = gpakConfigurePorts (DspId, &PortConfig, &PrtDspStat);
   if (PrtCfgStat != CfsSuccess || PrtDspStat != Cf_Success) {
      printf ("McBSP setup failure %d:%d:%d\n", PrtCfgStat, PrtDspStat, DSPError [DspId]);
      appErr (__LINE__);
   }
   printf ("McBSP setup done\n");


}
static void gpakGenTone (ADT_UInt32 DspId, short int cmd) {
	gpakGenToneStatus_t  tgStat;
	GPAK_ToneGenStat_t tgRet;
	GpakTonePktGenParms_t Tone;
	Tone.Device = 1;
	Tone.Duration = 16000;
	Tone.Level = -3;
	Tone.ToneCmd = ToneGenStart;
	Tone.ToneCode = 2;


	tgStat = gpakTonePktGenerate (DspId, cmd, &Tone,&tgRet);
	return;
}

static void gpakInsertTone (ADT_UInt32 DspId, short int chanId) {
	GpakApiStatus_t  tiStat;
	GPAK_InsertEventStat_t tiRet;
	ADT_UInt8 TonePyld[4];
	GpakTonePktGenParms_t Tone;
	Tone.Device = 1;
	Tone.Duration = 32000;
	Tone.Level = -3;
	Tone.ToneCmd = ToneGenStart;
	Tone.ToneCode = 1;

      TonePyld[0] = Tone.ToneCode;
      TonePyld[1] = (Tone.Level & 0x003F) ; //<< 8;
      TonePyld[2] = Byte1 (Tone.Duration);
      TonePyld[3] = Byte0 (Tone.Duration);

	tiStat = gpakRTPInsertTone (DspId, chanId, 4, (ADT_UInt16 *)TonePyld, &tiRet);
	return;
}
static void gpakGetChanStat(ADT_UInt32 DspId, short int cmd) {
	GpakApiStatus_t gchanStStat;
	GpakChannelStatus_t ChanStatus;
	GPAK_ChannelStatusStat_t gchanStRet;

    gchanStStat = gpakGetChannelStatus (
       DspId,
       cmd, 
       &ChanStatus,
       &gchanStRet 
    );
	if(gchanStStat == GpakApiSuccess) {
		printf("\nChan Type:%d,\n\t", ChanStatus.ChannelType );
		if(ChanStatus.ChannelType == packetToPacket) {
		   printf("G2A:%d,\t", ChanStatus.ChannelConfig.PktPkt.OutputGainG2A);
		   printf("G3A:%d,\t", ChanStatus.ChannelConfig.PktPkt.InputGainG3A);
		   printf("G2B:%d,\t", ChanStatus.ChannelConfig.PktPkt.OutputGainG2B);
		   printf("G3B:%d,\t\n", ChanStatus.ChannelConfig.PktPkt.InputGainG3B);
		}

	} else {
		printf("\nGet Chan Stat failed!\n");
	}
	return;
}
static void gpakReadSystem(ADT_UInt32 DspId) {
gpakReadSysParmsStatus_t readStStat;
GpakSystemParms_t sysPrms;
	readStStat = gpakReadSystemParms (DspId, &sysPrms);
	if(readStStat == GpakApiSuccess) {
		printf("\n VadNoiseFloor:%d,\n", sysPrms.VadNoiseFloor );
	}
}

//=================================================
//  DSP TDM all channel tear down
//
static void dspTearDown (ADT_UInt32 DspId) {

   GPAK_TearDownChanStat_t  tdStat = 0;
   ADT_UInt16 i;

   // Tear down channels in case they were previously open
   for (i = 0; i < MAX_CHANNELS; i++)
      gpakTearDownChannel (DspId, i, &tdStat);
}

//=================================================
//  Network stack initialization
//
static void netStackInit () {
   unsigned long unblock;
   int rslt;
   struct hostent *localHost;

   memset (Dest, 0, sizeof (Dest));

   // Link to socket library
   if ((rslt = WSAStartup (MAKEWORD (2,2), &wsa)) != 0) {
      printf ("Socket initialization failure %d.%d\n", rslt, WSAGetLastError());
      appErr (__LINE__);
   }

   //  Open socket for RTP stream
   rtpSkt = socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP);
   if (rtpSkt == INVALID_SOCKET) {
      printf ("Unable to open RTP socket %d\n", WSAGetLastError());
      appErr (__LINE__);
   }
   
   // Retrieve and report local IP address
   localHost = gethostbyname("");
   memcpy (&LocalIPAddr, *localHost->h_addr_list, sizeof (LocalIPAddr));

   LocalIPAddr.sin_family = AF_INET;
   LocalIPAddr.sin_port = htons (RTP_PORT);
   printf ("Local socket: %u.%u.%u.%u:%d\n", 
            (int) LocalIPAddr.sin_addr.s_net,
            (int) LocalIPAddr.sin_addr.s_host,
            (int) LocalIPAddr.sin_addr.s_lh,
            (int) LocalIPAddr.sin_addr.s_impno, RTP_PORT);
   
   // Bind RTP port to RTP socket
   rslt = bind (rtpSkt, (struct sockaddr *) &LocalIPAddr, sizeof (LocalIPAddr));
   if (rslt != 0) {
      printf ("Bind failure %d\n", WSAGetLastError());
      appErr (__LINE__);
   }

   // Make socket receive calls unblocked
   unblock = TRUE;
   rslt = ioctlsocket (rtpSkt, FIONBIO, &unblock);
   if (rslt != 0) {
      printf ("Blocking failure %d\n", WSAGetLastError());
      appErr (__LINE__);
   }

   return;
}

//--------------------------------------
//  Locate port / IP addr in Dest array.  Add to array in non-existant
static ADT_UInt16 netAddrToChan (SktInAddr *addr) {

   ADT_UInt16 i;

   for (i=0; i<MAX_CHANNELS; i++) {
      if (Dest[i].sin_port == 0 && Dest[i].sin_addr.S_un.S_addr == 0) {
         memcpy (&Dest[i], addr, sizeof (SktInAddr));
         printf ("Mapping channel %d to %u.%u.%u.%u:%d\n", (int) i,
            (int) addr->sin_addr.s_net, (int) addr->sin_addr.s_host, 
            (int) addr->sin_addr.s_lh,  (int) addr->sin_addr.s_impno,
            ntohs (addr->sin_port));
            return i;
      }
      if (Dest[i].sin_port             != addr->sin_port) continue;
      if (Dest[i].sin_addr.S_un.S_addr != addr->sin_addr.S_un.S_addr) continue;
      return i;
   }
   return (ADT_UInt16) -1;   
}

//-----------------------------------------------
// set up default values for a TDM to conference channel
static void setTDMTDMDefaults (GpakChannelConfig_t *chCfgData, int cfrId, int chID) {
 
   PcmPcmCfg_t *PcmPcm = &chCfgData->PcmPcm;

   memset (PcmPcm, 0, sizeof (PcmPcmCfg_t));

   PcmPcm->InPortA    = McBSP;
   PcmPcm->OutPortA   = McBSP;
   PcmPcm->InSlotA    = chID;
   PcmPcm->OutSlotA   = chID;
   PcmPcm->InPortB    = McBSP;
   PcmPcm->OutPortB   = McBSP;
   PcmPcm->InSlotB    = chID + TDM_SLOT_OFFSET;
   PcmPcm->OutSlotB   = chID + TDM_SLOT_OFFSET;

   PcmPcm->EcanEnableA    = ActiveChan.PCM_EC;
   PcmPcm->AECEcanEnableA = ActiveChan.AEC;
   PcmPcm->AgcEnableA     = ActiveChan.AGC;
   PcmPcm->NoiseSuppressA = NOISE;
   PcmPcm->CIdModeA       = ActiveChan.CID;
   PcmPcm->ToneTypesA     = ActiveChan.TONE;
   PcmPcm->ToneGenGainG1A = TG_GAIN;
   PcmPcm->OutputGainG2A  = OUT_GAIN;
   PcmPcm->InputGainG3A   = IN_GAIN;

   PcmPcm->EcanEnableB    = ActiveChan.PCM_EC;
   PcmPcm->AECEcanEnableB = ActiveChan.AEC;
   PcmPcm->AgcEnableB     = ActiveChan.AGC;
   PcmPcm->NoiseSuppressB = NOISE;
   PcmPcm->CIdModeB       = ActiveChan.CID;
   PcmPcm->ToneTypesB     = ActiveChan.TONE;
   PcmPcm->ToneGenGainG1B = TG_GAIN;
   PcmPcm->OutputGainG2B  = OUT_GAIN;
   PcmPcm->InputGainG3B   = IN_GAIN;

   PcmPcm->FrameSize     = ActiveChan.ENCSZ;
   PcmPcm->Coding        = GpakVoiceChannel;

}
//-----------------------------------------------
// set up default values for a TDM to packet channel
static void setPcmPktDefaults (GpakChannelConfig_t *chCfgData, int chID) {

   PcmPktCfg_t *PcmPkt = &chCfgData->PcmPkt;

   memset (PcmPkt, 0, sizeof (PcmPktCfg_t));
   PcmPkt->PcmInPort    = McBSP;
   PcmPkt->PcmOutPort   = McBSP;
#ifdef TEST_ON_EVM
   PcmPkt->PcmInSlot    = (chID == 0)? 0: 1;
   PcmPkt->PcmOutSlot   = (chID == 0)? 0: 1;
#else
   PcmPkt->PcmInSlot    = chID;
   PcmPkt->PcmOutSlot   = chID;
#endif
   PcmPkt->PktInCoding     = ActiveChan.DECODE;
   PcmPkt->PktInFrameSize  = ActiveChan.DECSZ;
   PcmPkt->PktOutCoding    = ActiveChan.ENCODE;
   PcmPkt->PktOutFrameSize = ActiveChan.ENCSZ;

   PcmPkt->PcmEcanEnable = ActiveChan.PCM_EC;
   PcmPkt->PktEcanEnable = ActiveChan.PKT_EC;
   PcmPkt->AECEcanEnable = ActiveChan.AEC;
   PcmPkt->VadEnable     = ActiveChan.VAD;
   PcmPkt->AgcEnable     = ActiveChan.AGC;
   PcmPkt->ToneTypes     = (DTMF_tone | Notify_Host|Tone_Regen);//ActiveChan.TONE; //
   PcmPkt->CIdMode       = ActiveChan.CID;
   PcmPkt->FaxMode       = ActiveChan.FAX;
   PcmPkt->FaxTransport  = faxRtp;
   PcmPkt->Coding        = GpakVoiceChannel;

   PcmPkt->ToneGenGainG1 = TG_GAIN;
   PcmPkt->OutputGainG2  = OUT_GAIN;
   PcmPkt->InputGainG3   = IN_GAIN;
}
//-----------------------------------------------
// set up default values for a packet to packet channel
static void setPktPktDefaults (GpakChannelConfig_t *chCfgData, int chID) {

   PktPktCfg_t *PktPkt = &chCfgData->PktPkt;

   memset (PktPkt, 0, sizeof (PktPktCfg_t));

   PktPkt->PktInCodingA     = ActiveChan.DECODE;
   PktPkt->PktInFrameSizeA  = ActiveChan.DECSZ;
   PktPkt->PktOutCodingA    = ActiveChan.ENCODE;
   PktPkt->PktOutFrameSizeA = ActiveChan.ENCSZ;

   PktPkt->EcanEnableA      = ActiveChan.PKT_EC;
   PktPkt->VadEnableA       = ActiveChan.VAD;
   PktPkt->ToneTypesA       = (DTMF_tone | Tone_Relay|Notify_Host);//ActiveChan.TONE; //
   PktPkt->AgcEnableA       = ActiveChan.AGC;

   PktPkt->ChannelIdB       = chID+1; //chID ^ 1;
   PktPkt->PktInCodingB     = ActiveChan.ENCODE;
   PktPkt->PktInFrameSizeB  = ActiveChan.ENCSZ;
   PktPkt->PktOutCodingB    = ActiveChan.DECODE;
   PktPkt->PktOutFrameSizeB = ActiveChan.DECSZ;

   PktPkt->EcanEnableB      = ActiveChan.PKT_EC;
   PktPkt->VadEnableB       = ActiveChan.VAD;
   PktPkt->ToneTypesB       = (DTMF_tone | Tone_Relay|Notify_Host);//ActiveChan.TONE; //
   PktPkt->AgcEnableB       = ActiveChan.AGC;
   PktPkt->OutputGainG2A     = 3;
   PktPkt->InputGainG3A     = -3;
   PktPkt->OutputGainG2B     = 3;
   PktPkt->InputGainG3B     = -3;

}
//-----------------------------------------------
// set up default values for a TDM to conference channel
static void setTDMConfDefaults (GpakChannelConfig_t *chCfgData, int cfrId, int chID) {

   CnfPcmCfg_t *PcmCnf = &chCfgData->ConferPcm;

   memset (PcmCnf, 0, sizeof (CnfPcmCfg_t));

   PcmCnf->ConferenceId = cfrId;

   PcmCnf->PcmInPort    = McBSP;
   PcmCnf->PcmOutPort   = McBSP;
   PcmCnf->PcmInSlot    = chID;
   PcmCnf->PcmOutSlot   = chID;

   PcmCnf->EcanEnable    = ActiveChan.PCM_EC;
   PcmCnf->AECEcanEnable = ActiveChan.AEC;
   PcmCnf->AgcInEnable   = ActiveChan.AGC;
   PcmCnf->AgcOutEnable  = ActiveChan.AGC;
   PcmCnf->ToneTypes     = ActiveChan.TONE;

   PcmCnf->ToneGenGainG1 = TG_GAIN;
   PcmCnf->OutputGainG2  = OUT_GAIN;
   PcmCnf->InputGainG3   = IN_GAIN;


}
//-----------------------------------------------
// set up default values for a conf packet
static void setPktConfDefaults (GpakChannelConfig_t *chCfgData, int cfrID, int chID, int FrameSize) {

   CnfPktCfg_t *CnfPkt = &chCfgData->ConferPkt;

   memset (CnfPkt, 0, sizeof (CnfPktCfg_t));

   CnfPkt->ConferenceId = cfrID;
   CnfPkt->PktInCoding  = ActiveChan.DECODE;
   CnfPkt->PktOutCoding = ActiveChan.ENCODE;
   CnfPkt->EcanEnable   = ActiveChan.PKT_EC;
   CnfPkt->VadEnable    = ActiveChan.VAD;
   CnfPkt->AgcInEnable  = ActiveChan.AGC;
   CnfPkt->AgcOutEnable = ActiveChan.AGC;
   CnfPkt->ToneTypes    = ActiveChan.TONE;
   CnfPkt->PktFrameSize = FrameSize;

   CnfPkt->ToneGenGainG1 = TG_GAIN;
   CnfPkt->OutputGainG2  = OUT_GAIN;
   CnfPkt->InputGainG3   = IN_GAIN;

}
//-----------------------------------------------
// set up default values for a conf Composite
static void setCmpConfDefaults (GpakChannelConfig_t *chCfgData, int CfrID, int chID) {

   CnfCmpCfg_t *CnfCmp = &chCfgData->ConferComp;

   memset (CnfCmp, 0, sizeof (CnfCmpCfg_t));

   CnfCmp->ConferenceId     = CfrID;
   CnfCmp->PcmOutPort       = SerialPortNull;
   CnfCmp->PcmOutSlot       = 0;
   CnfCmp->PcmOutPin        = 0;
   CnfCmp->PktOutCoding     = ActiveChan.ENCODE;
   CnfCmp->VadEnable        = ActiveChan.VAD;
}

//SRTP Key stuff 
typedef struct optionParams {
   SrtpKeyScheme_t   KeyScheme;
   SrtpEncryptType_t EncryptType;
   SrtpAuthType_t    AuthType;
   ADT_UInt16        Kdr;
   ADT_UInt16        MkiI8;
} optionParams;

optionParams optTests [] = {
   {  KEY_PSK,    ENCRYPT_CM,     AUTH_HMAC_SHA,   25,  0 },
   {  KEY_PSK,    ENCRYPT_CM,     AUTH_HMAC_SHA,    8,  0 },
   {  KEY_PSK,    ENCRYPT_CM,     AUTH_MD5,        25,  0 },
   
   {  KEY_PSK,    ENCRYPT_F8,     AUTH_HMAC_SHA,   25,  0 },
   {  KEY_PSK,    ENCRYPT_F8,     AUTH_HMAC_SHA,    8,  0 },
   {  KEY_PSK,    ENCRYPT_F8,     AUTH_MD5,        25,  0 },

   {  KEY_MKI,    ENCRYPT_CM,     AUTH_HMAC_SHA,   25,  2 },
   {  KEY_MKI,    ENCRYPT_CM,     AUTH_HMAC_SHA,    8,  2 },
   {  KEY_MKI,    ENCRYPT_CM,     AUTH_HMAC_SHA,    8,  2 },
   {  KEY_MKI,    ENCRYPT_CM,     AUTH_HMAC_SHA,   25,  3 },
   {  KEY_MKI,    ENCRYPT_CM,     AUTH_HMAC_SHA,    8,  4 },

   
   { KEY_PSK,     ENCRYPT_NONE }   // Terminator
};

/* MKI only ket stuff */
typedef struct keySchedule {
   ADT_UInt32 roc;
   ADT_UInt16 seq;
   ADT_UInt32 mkiLife;
   ADT_UInt32 mkiValue;
   ADT_UInt16 key [8];
} keySchedule;

// Schedule is for key such that    roc + seq <= key <= roc + seq + key_life - 1
	/* Master
		HEX(e1, f9), HEX(7a, 0d), HEX(3e, 01), HEX(8b, e0),
		HEX(d6, 4f), HEX(a3, 2c), HEX(06, de), HEX(41, 39)
		Salt
		HEX(0e, c6), HEX(75, ad), HEX(49, 8a), HEX(fe, eb),
		HEX(b6, 96), HEX(0b, 3a), HEX(ab, e6)

	*/
keySchedule keySched [] = { 
  { 0x00000000, 0x0000, 0x100,       0x12345678,  { 0x1234, 0x2345, 0x3456, 0x4567, 0x5678, 0x6789, 0x789A, 0x89AB } },
  { 0x00000000, 0x0000, 0x100,       0x12345679,  { 0x4567, 0x5678, 0x6789, 0x789A, 0x89AB, 0x9ABC, 0xABCD, 0xDEF0 } },
  { 0x00000001, 0x0000, 0x100,       0x1234567a,  { 0x2345, 0x3456, 0x4567, 0x5678, 0x6789, 0x789A, 0x89AB, 0x9ABC } },
  { 0x00000001, 0x0000, 0x100,       0x1234567b,  { 0x2345, 0x3456, 0x4567, 0x5678, 0x6789, 0x789A, 0x89AB, 0x9ABC } },
  { 0x00000001, 0x0010, 0x200,       0x1234567c,  { 0x3456, 0x4567, 0x5678, 0x6789, 0x789A, 0x89AB, 0x9ABC, 0xABCD } },
  { 0x00000001, 0x0020, 0x200,       0x1234567d,  { 0x4567, 0x5678, 0x6789, 0x789A, 0x89AB, 0x9ABC, 0xABCD, 0xDEF0 } },
  { 0xffffffff, 0xffff, 0x800,      0x1234567e,           { 0xdddd, 0x4567, 0x5678, 0x6789, 0x789A, 0x89AB, 0x9ABC, 0xABCD } }
};

#define NUM_KEY_ON_RING 7

typedef struct keyRingChannel_t {
	int option[MAX_CHANNELS]; // record of the srtp types
	int keyIndex[MAX_CHANNELS];
} keyRingChannel_t;


keyRingChannel_t SRTP_keyRingEncode;
keyRingChannel_t SRTP_keyRingDecode;
init_keyRing = 0;

int SRTP_Key_Option= 0; // record of the srtp types

void srtpGetConfig(int ChanId, int opt, GpakSRTPCfg_t    *srtpCfgPtr)
{
   keySchedule *keyElement;
   optionParams *optTest;
   ADT_UInt32  mask = 0, mki = 0, *pMkiValue, MkiValue;
   ADT_UInt16  i, *mkey, *msalt, *lkey, *lsalt;
	int keyIndex = 0;
	
	if(!init_keyRing) {
		memset(&SRTP_keyRingEncode, 0, sizeof(keyRingChannel_t));
		memset(&SRTP_keyRingDecode, 0, sizeof(keyRingChannel_t));
		init_keyRing = 1;
		num_Of_NeyKeys= 0;
	}
	optTest = &optTests[opt];


   // Initialize SRTP processing for channels
   keyElement = keySched;
	keyIndex = 0;
   srtpCfgPtr->Encode.AuthenticationI8 = 10;
   srtpCfgPtr->Encode.EncryptionScheme = optTest->EncryptType; //ENCRYPT_CM;
   srtpCfgPtr->Encode.AuthenticationScheme = optTest->AuthType; //AUTH_HMAC_SHA;
   srtpCfgPtr->Encode.MKI_I8 = optTest->MkiI8;
   srtpCfgPtr->Encode.KDR = optTest->Kdr;

	if(optTest->KeyScheme == KEY_MKI) {
      switch (optTest->MkiI8) {
      case 2: mask = 0xffff;     break;
      case 3: mask = 0xffffff;   break;
      case 4: mask = 0xffffffff; break;
      }
      // Find key that matches mki value
      //while (((keyElement->mkiValue & mask) != pKeyInfo->u.Mki.MkiValue) && (pKeyInfo->u.Mki.MkiValue != 0)) keyElement++;
      //if (pKeyInfo->LookAhead) keyElement++;
		MkiValue = keyElement->mkiValue & mask;
		//pMkiValue = &srtpCfgPtr->Encode.MKI[0];
		//*pMkiValue =  = MkiValue;
		srtpCfgPtr->Encode.MKI[0] = (ADT_UInt8)((MkiValue>>24) & 0xff);
		srtpCfgPtr->Encode.MKI[1] = (ADT_UInt8)((MkiValue>>16) & 0xff);
		srtpCfgPtr->Encode.MKI[2] = (ADT_UInt8)((MkiValue>>8) & 0xff);
		srtpCfgPtr->Encode.MKI[3] = (ADT_UInt8)((MkiValue) & 0xff);

      srtpCfgPtr->Encode.MasterKeyLife[0] = 0;
      srtpCfgPtr->Encode.MasterKeyLife[1] = (keyElement->mkiLife >> 16) & 0xffff;
      srtpCfgPtr->Encode.MasterKeyLife[2] = keyElement->mkiLife & 0xffff;

	}


   mkey  = &srtpCfgPtr->Encode.MasterKey[0];
   lkey  = &keyElement->key[0];
   msalt = &srtpCfgPtr->Encode.MasterSalt[0];
   lsalt = &keyElement->key[1];
   

   for (i=0; i<7; i++) {
      *mkey++  = *lkey++;
      *msalt++ = *lsalt++;
   }
   *mkey++ = *lkey++;

	SRTP_keyRingEncode.option[ChanId] = opt;
	SRTP_keyRingEncode.keyIndex[ChanId] = keyIndex;
   

   srtpCfgPtr->Decode.AuthenticationI8 = 10;
   srtpCfgPtr->Decode.EncryptionScheme = optTest->EncryptType; //ENCRYPT_CM;
   srtpCfgPtr->Decode.AuthenticationScheme = optTest->AuthType; //AUTH_HMAC_SHA;
   srtpCfgPtr->Decode.MKI_I8 = optTest->MkiI8;
   srtpCfgPtr->Decode.KDR = optTest->Kdr;
	if(optTest->KeyScheme == KEY_MKI) {
      switch (optTest->MkiI8) {
      case 2: mask = 0xffff;     break;
      case 3: mask = 0xffffff;   break;
      case 4: mask = 0xffffffff; break;
      }
      // Find key that matches mki value
      //while (((keyElement->mkiValue & mask) != pKeyInfo->u.Mki.MkiValue) && (pKeyInfo->u.Mki.MkiValue != 0)) keyElement++;
      //if (pKeyInfo->LookAhead) keyElement++;
		MkiValue = keyElement->mkiValue & mask;
		srtpCfgPtr->Decode.MKI[0] = (ADT_UInt8)((MkiValue>>24) & 0xff);
		srtpCfgPtr->Decode.MKI[1] = (ADT_UInt8)((MkiValue>>16) & 0xff);
		srtpCfgPtr->Decode.MKI[2] = (ADT_UInt8)((MkiValue>>8) & 0xff);
		srtpCfgPtr->Decode.MKI[3] = (ADT_UInt8)((MkiValue) & 0xff);
      srtpCfgPtr->Decode.MasterKeyLife[0] = 0;
      srtpCfgPtr->Decode.MasterKeyLife[1] = (keyElement->mkiLife >> 16) & 0xffff;
      srtpCfgPtr->Decode.MasterKeyLife[2] = keyElement->mkiLife & 0xffff;

	}

   mkey  = &srtpCfgPtr->Decode.MasterKey[0];
   lkey  = &keyElement->key[0];
   msalt = &srtpCfgPtr->Decode.MasterSalt[0];
   lsalt = &keyElement->key[1];

   for (i=0; i<7; i++) {
      *mkey++  = *lkey++;
      *msalt++ = *lsalt++;
   }
   *mkey++ = *lkey++;

   srtpCfgPtr->Decode.ROC = keyElement->roc;

	srtpCfgPtr->FrameSize = ActiveChan.ENCSZ;

	SRTP_keyRingDecode.option[ChanId] = opt;
	SRTP_keyRingDecode.keyIndex[ChanId] = keyIndex;
}

 // eventData->aux.srtpEvent.roc, 
 // eventData->aux.srtpEvent.seq, 

static void gpakSendSrtpKey(ADT_UInt32 DspId, int chId, GpakAsyncEventData_t *eventData) {
	GpakApiStatus_t srtpRet;
	SrtpStatus_t    *dspStatus;
	GpakSRTPKey_t srtpKeyInfo;
   keySchedule *keyElement;
   optionParams *optTest;
   ADT_UInt32  mask = 0, mki = 0, *pMkiValue, MkiValue;
   ADT_UInt16  i, *mkey, *msalt, *lkey, *lsalt;
	int keyIndex;
	int opt;

	// get the record out of the storage
	if(eventData->Direction == DSPToNet) {
		opt = SRTP_keyRingEncode.option[chId];
	}else {
		opt = SRTP_keyRingDecode.option[chId];
	}

	optTest = &optTests[opt];

   // Initialize SRTP processing for channels

	srtpKeyInfo.Direction = eventData->Direction;
	srtpKeyInfo.MKI_I8 = eventData->aux.srtpEvent.MkiI8;
   srtpKeyInfo.KDR = optTest->Kdr;

	if(optTest->KeyScheme == KEY_MKI) {
      switch (optTest->MkiI8) {
      case 2: mask = 0xffff;     break;
      case 3: mask = 0xffffff;   break;
      case 4: mask = 0xffffffff; break;
      }
      // Find key that matches mki value
		if(eventData->Direction == DSPToNet) {
			// Serach the keyring for the current matached encoder key, then use the next key if there is any
			keyIndex = 0; //SRTP_keyRingEncode.keyIndex[chId];
			keyElement = &keySched[0];
			while (((keyElement->mkiValue & mask) != eventData->aux.srtpEvent.MkiValue) && (eventData->aux.srtpEvent.MkiValue != 0)) {
				if(keyIndex < NUM_KEY_ON_RING-1)  {
					// hasn't reached to the end of the key ring
					keyElement ++;
					keyIndex ++;
				}
				if(keyIndex == NUM_KEY_ON_RING-1)
					break;
			}
		   //if (eventData->aux.srtpEvent.seq) 
			if(keyIndex < NUM_KEY_ON_RING-1) {
				// hasn't reached to the end of the key ring
				keyElement++;
				keyIndex ++;
			}
			else {
				keyElement =&keySched[0];
				keyIndex =0;
			}
			SRTP_keyRingEncode.keyIndex[chId] = keyIndex;
		} else {
			if(eventData->aux.srtpEvent.MkiValue == 0) {
				keyIndex = SRTP_keyRingDecode.keyIndex[chId];
				keyElement = &keySched[keyIndex];
				// Lookahead for the decryption
				if(keyIndex < NUM_KEY_ON_RING-1) {
					// hasn't reached to the end of the key ring
					keyElement++;
					keyIndex ++;
				}
				else {
					keyElement =&keySched[0];
					keyIndex =0;
				}
			
			} else {
				keyIndex = 0;
				keyElement = &keySched[0];
				while ( (keyElement->mkiValue & mask) != eventData->aux.srtpEvent.MkiValue) {
					if(keyIndex < NUM_KEY_ON_RING-1)  {
						// hasn't reached to the end of the key ring
						keyElement ++;
						keyIndex ++;
					}
					if(keyIndex == NUM_KEY_ON_RING-1)
						break;
				}
				//if (eventData->aux.srtpEvent.seq) 
				if(keyIndex > NUM_KEY_ON_RING-1) {
					keyElement = &keySched[0];
					keyIndex =0;
					printf("Can't find the key\n");
					return;
				}
			}
			SRTP_keyRingDecode.keyIndex[chId] = keyIndex;
		}

		//*pMkiValue = keyElement->mkiValue & mask;
		MkiValue = keyElement->mkiValue & mask;
		srtpKeyInfo.MKI[0] = (ADT_UInt8)((MkiValue>>24) & 0xff);
		srtpKeyInfo.MKI[1] = (ADT_UInt8)((MkiValue>>16) & 0xff);
		srtpKeyInfo.MKI[2] = (ADT_UInt8)((MkiValue>>8) & 0xff);
		srtpKeyInfo.MKI[3] = (ADT_UInt8)((MkiValue) & 0xff);


      srtpKeyInfo.MasterKeyLife[0] = 0;
      srtpKeyInfo.MasterKeyLife[1] = (keyElement->mkiLife >> 16) & 0xffff;
      srtpKeyInfo.MasterKeyLife[2] = keyElement->mkiLife & 0xffff;
	}


   mkey  = &srtpKeyInfo.MasterKey[0];
   lkey  = &keyElement->key[0];
   msalt = &srtpKeyInfo.MasterSalt[0];
   lsalt = &keyElement->key[1];
   

   for (i=0; i<7; i++) {
      *mkey++  = *lkey++;
      *msalt++ = *lsalt++;
   }
   *mkey++ = *lkey++;

   
	srtpRet = gpakSRTPNewKey (DspId, chId, &srtpKeyInfo, &dspStatus);

}

static void initChannels (ADT_UInt32 DspId) {
   // SRTP configuration variables
   GpakSRTPCfg_t    srtpCfg;
   GpakApiStatus_t  srtpCfgStat = 0;
   SrtpStatus_t     srtpDspStat = 0;

   // RTP configuration variables
   GpakRTPCfg_t             rtpCfg;
   gpakRtpStatus_t          rtpCfgStat = 0;
   GPAK_RTPConfigStat_t     rtpDspStat = 0;

   // Channel configuration variables
   GpakChannelConfig_t      chCfgData;
   gpakConfigChanStatus_t   chCfgStat = 0;
   GPAK_ChannelConfigStat_t chDspStat = 0;

   ADT_UInt16 chanId, PktI8;
   gpakGetPayloadStatus_t   pktRcvStat;
   ADT_UInt16 i;

   // Purge any packets that may be waiting in DSP buffers
   do {
      PktI8 = sizeof (rtpPkt);
      pktRcvStat =  gpakGetPacketFromDsp (DspId, &chanId, (ADT_UInt8 *) &rtpPkt, &PktI8);
   } while (pktRcvStat == GpsSuccess);


   // Initialize RTP processing for channels
   rtpCfg.JitterMode       = 0;
   rtpCfg.DelayTargetMinMS = 60;
   rtpCfg.DelayTargetMS    = 80;
   rtpCfg.DelayTargetMaxMS = 100;
   rtpCfg.VoicePyldType    = ActiveChan.RTP_CODEC_PYLD;
   rtpCfg.TonePyldType     = 97;
   rtpCfg.T38PyldType      = 98;
   rtpCfg.StartSequence    =  1;

   
   for (i=0; i<MAX_CHANNELS; i++) {
      rtpCfg.SSRC = 0xAAAA0000 + i;
      rtpCfgStat = 0;
      rtpDspStat = 0;
      rtpCfgStat = gpakSendRTPMsg (DspId, i, &rtpCfg, &rtpDspStat);
      if (rtpCfgStat != RtpSuccess || rtpDspStat != RTPSuccess) {
         printf ("RTP channel %d setup failure %d:%d:%d\n", i, rtpCfgStat, rtpDspStat, DSPError [DspId]);
         appErr (__LINE__);
      }
      gpakHostDelay ();
   }
	if(ActiveChan.SRTP) {
		for (i=0; i<MAX_CHANNELS; i++) {
			srtpGetConfig(i, SRTP_Key_Option, &srtpCfg);
		   srtpCfgStat = 0;
			srtpDspStat = 0;
			srtpCfgStat = gpakConfigSRTP (DspId, i, &srtpCfg, &srtpDspStat);
			if (srtpCfgStat != GpakApiSuccess || srtpDspStat != SRTP_SUCCESS) {
				printf ("SRTP channel %d setup failure %d:%d:%d\n", i, srtpCfgStat, srtpDspStat, DSPError [DspId]);
				appErr (__LINE__);
			}
			gpakHostDelay ();
		}
   }
   // Open two TDM-Packet channels
#ifndef DSP_LOOPBACK

#ifdef TEST_ON_EVM
   for (i=0; i<2; i++) {
      setPcmPktDefaults (&chCfgData, i);
      chCfgStat = gpakConfigureChannel (DspId, i, pcmToPacket, &chCfgData, &chDspStat);
      if (chCfgStat != CcsSuccess || chDspStat != Cc_Success) {
         printf ("Channel %d setup failure %d:%d:%d\n", i, chCfgStat, chDspStat, DSPError [DspId]);
      }
      gpakHostDelay ();
   }
#else
	i = 2; // only setup pkt2pkt channels
#endif
   // Open remaining channels as Packet-Packet
   for (; i<MAX_CHANNELS; i+=2) {
      setPktPktDefaults (&chCfgData, i);

      chCfgStat = gpakConfigureChannel (DspId, i, packetToPacket, &chCfgData, &chDspStat);
      if (chCfgStat != CcsSuccess || chDspStat != Cc_Success) {
        printf ("Channel %d setup failure %d:%d:%d\n", i, chCfgStat, chDspStat, DSPError [DspId]);
      }
      gpakHostDelay ();
   }
#else
   for (i=0; i<1; i++) {
      setPcmPktDefaults (&chCfgData, i);
      chCfgStat = gpakConfigureChannel (DspId, i, pcmToPacket, &chCfgData, &chDspStat);
      if (chCfgStat != CcsSuccess || chDspStat != Cc_Success) {
         printf ("Channel %d setup failure %d:%d:%d\n", i, chCfgStat, chDspStat, DSPError [DspId]);
      }
      gpakHostDelay ();
   }
   for (i=1; i<3; i+=2) {
      setPktPktDefaults (&chCfgData, i);

      chCfgStat = gpakConfigureChannel (DspId, i, packetToPacket, &chCfgData, &chDspStat);
      if (chCfgStat != CcsSuccess || chDspStat != Cc_Success) {
        printf ("Channel %d setup failure %d:%d:%d\n", i, chCfgStat, chDspStat, DSPError [DspId]);
      }
      gpakHostDelay ();
   }

   for (i=3; i<4; i++) {
      setPcmPktDefaults (&chCfgData, i);
      chCfgStat = gpakConfigureChannel (DspId, i, pcmToPacket, &chCfgData, &chDspStat);
      if (chCfgStat != CcsSuccess || chDspStat != Cc_Success) {
         printf ("Channel %d setup failure %d:%d:%d\n", i, chCfgStat, chDspStat, DSPError [DspId]);
      }
      gpakHostDelay ();
   }
#endif
}


//----------------------------------------------------
// event notification/data capturing
static void dumpMono (char *filename, ADT_UInt32 dsp, int addr, int lenI32) {
   FILE *outFile;
   ADT_UInt32 value;

   if (lenI32 == 0) return;

   outFile = fopen (filename, "wb");
   if (outFile == NULL) { 
      printf ("Unable to open file %s\n", filename);
      return;
   }
   printf ("Writing %d bytes from dsp %d at %x to %s\n", lenI32*4, dsp, addr, filename);
   do {
   
      gpakLockAccess (dsp);
      gpakReadDspMemory32 (dsp, addr, 1, &value);
      gpakUnlockAccess (dsp);

      fwrite (&value, 1, 4, outFile);
      addr += 4;
      lenI32 -= 1;
   } while (0 < lenI32);

   fclose (outFile);
   return;
}
static void dumpStereo (char *filename, ADT_UInt32 dsp, int addr1, int len1I32, int addr2, int len2I32) {
   FILE *outFile;
   ADT_UInt16 value1[2], value2[2];
   int lenI32;

   if (len1I32 < len2I32) lenI32 = len1I32;
   else                   lenI32 = len2I32;

   if (lenI32 <= 0) return;

   outFile = fopen (filename, "wb");
   if (outFile == NULL) { 
      printf ("Unable to open file %s\n", filename);
      return;
   }
   printf ("Writing %d bytes from dsp %d at %x and %x to %s\n", lenI32*4, dsp, addr1, addr2, filename);
   do {
   
      gpakLockAccess (dsp);
      gpakReadDspMemory32 (dsp, addr1, 1, (ADT_UInt32 *) &value1);
      gpakReadDspMemory32 (dsp, addr2, 1, (ADT_UInt32 *) &value2);
      gpakUnlockAccess (dsp);

      fwrite (&value1[0], 1, 2, outFile);
      fwrite (&value2[0], 1, 2, outFile);
      fwrite (&value1[1], 1, 2, outFile);
      fwrite (&value2[1], 1, 2, outFile);
      addr1 += 4;
      addr2 += 4;
      lenI32 -= 1;
   } while (0 < lenI32);

   fclose (outFile);
   return;
}

static void dumpFaxCapture (ADT_UInt32 dsp, int chId, GpakAsyncEventData_t *eventData) {
   struct captureData *evt = (struct captureData *) &eventData->aux.captureData;

   
   if (chId==0) {
      dumpMono   ("T38Debug.txt",      dsp, evt->CAddr, evt->CLenI32);
      dumpStereo ("T38RawStereo.pcm",  dsp, evt->AAddr, evt->ALenI32, evt->BAddr, evt->BLenI32);
   } else {
      dumpStereo ("T38DecodedStereo.pcm",  dsp, evt->AAddr, evt->ALenI32, evt->BAddr, evt->BLenI32);
   }

   printf ("FAX capture complete on channel %d\n\n", chId);
   return;
}
static void dumpECCapture (ADT_UInt32 dsp, int chId, GpakAsyncEventData_t *eventData) {
   struct captureData *evt = (struct captureData *) &eventData->aux.captureData;

   dumpStereo ("ECInStereo.pcm",  dsp, evt->AAddr, evt->ALenI32, evt->BAddr, evt->BLenI32);
   dumpStereo ("ECOutStereo.pcm", dsp, evt->CAddr, evt->CLenI32, evt->DAddr, evt->DLenI32);

   printf ("EC capture complete on channel %d\n\n", chId);
   return;
}


static void printWarning (int warningID, int warningValue) {
   printf ("Warning: ");

   switch (warningID) {
   case WarnTxDmaSlip:       printf ("TDM slip on TX Buffer. Port %d\n", warningValue%100);  break;
   case WarnRxDmaSlip:       printf ("TDM slip on RX Buffer. Port %d\n", warningValue%100);  break;
   case WarnFrameTiming:     printf ("Framing task %d ms MIPS overload\n", warningValue/8);  break;
   case WarnFrameBuffers:    printf ("Framing task %d ms circular buffer reset\n", warningValue/8);  break;
   case WarnPortRestart:     printf ("TDM port restart\n");  break;
   case WarnFrameSyncError:  printf ("TDM port %d frame sync error\n", warningValue);  break;
   }
   return;
}

static void processEvent (ADT_UInt32 Dsp, int chId, GpakAsyncEventCode_t EvtCode, GpakAsyncEventData_t *eventData) {
    printf ("\r");
    switch (EvtCode) {
    case EventDspReady:             printf ("Dsp ready        \n");   break;
    case EventWarning:              printWarning (eventData->ToneDevice, chId);  break;
    case EventToneDetect:           printf ("Tone code %d Dur %d on chann %d Dir %d\n", eventData->aux.toneEvent.ToneCode, eventData->aux.toneEvent.ToneDuration, chId, eventData->Direction);    break;
    case EventPlaybackComplete:     printf ("Playback complete on channel %d\n", chId);            break;
    case EventRecordingStopped:     printf ("Recording stopped on channel %d\n", chId);            break;
    case EventRecordingBufferFull:  printf ("Recording buffer full on channel %d\n", chId);        break;
    case EventFaxComplete:          dumpFaxCapture (Dsp, chId, eventData);            break;
    case EventCaptureComplete:      dumpECCapture (Dsp, chId, eventData);             break;
    case EventVadStateVoice:        printf ("Voice on channel %d\n", chId);           break;
    case EventVadStateSilence:      printf ("Silence on channel %d\n", chId);         break;
    case EventTxCidMessageComplete: printf ("Cid sent on channel %d\n", chId);        break;
    case EventRxCidCarrierLock:     printf ("Cid detected on channel %d\n", chId);    break;
    case EventRxCidMessageComplete: printf ("Cid received on channel %d\n", chId);    break;
    case EventNeedSRTPMasterKey:
		 /*
		 if(eventData->aux.srtpEvent.MkiValue){
			printf ("SRTP ch %d dir %d Roc=%ld, seq=%d, MkiValue=%ld, MkiI8=%d \n",
													chId,
													eventData->Direction,
													eventData->aux.srtpEvent.roc, 
													eventData->aux.srtpEvent.seq, 
													eventData->aux.srtpEvent.MkiValue, 
													eventData->aux.srtpEvent.MkiI8 ); 
		 }
		 */
		 num_Of_NeyKeys++;
		 gpakSendSrtpKey(Dsp, chId, eventData);

       if(num_Of_NeyKeys %100 == 0) {
			printf ("SRTP send new key %d times \n", num_Of_NeyKeys);
			printf (prompt);
		 }
		 return;

		 break;
    default:     break;
    };
    printf (prompt);
}

//========================================================================
//   RTP packet processing
//
//   Runs as an independent thread.   Is awakened by a timeout or a Semaphore generated by
//   an interrupt handler that processes interrupts from the DSP.
//   Packets from the DSP are sent out over the network interface.  Packets from the 
//   network are sent to the DSP

//-----------------------------------------------
// process DSP interrupts
static void DSPIntrHandler (void) {
   DSPInterruptCnt++;

   gpakLockAccess (0);
   traceMem  (0xffffff3, DSPInterruptCnt, 0xf3);
   traceData (0, 0);
   gpakUnlockAccess (0);

   if (rtpSemaphore != NULL) ReleaseSemaphore (rtpSemaphore, 1, NULL);
   if (evtSemaphore != NULL) ReleaseSemaphore (evtSemaphore, 1, NULL);

}

static void evtThread (void *args) {
   ADT_UInt32 Dsp, *p32;
   ADT_UInt16 chanId;
   gpakReadEventFIFOMessageStat_t fifoStat;
   GpakAsyncEventCode_t eventCode;
   GpakAsyncEventData_t eventData;
   int status;

   p32 = args;
   Dsp = *p32;

   if (SetPriorityClass (GetCurrentProcess (), NORMAL_PRIORITY_CLASS) == 0) {
      printf ("Priority change failure %d\n", GetLastError ());
      appErr (__LINE__);
   }

   while (!exitFlag) {
      // wait on semaphore with 10 millisecond wake up

      status = WaitForSingleObject (evtSemaphore, 5);
      gpakLockAccess (0);
      traceMem (0xfffe, 0, 0xe1);
      traceData (&status, 1);
      gpakUnlockAccess (0);

      // Transfer events from DSP to network.
      do {
         gpakLockAccess (0);
         fifoStat = gpakReadEventFIFOMessage (Dsp, &chanId, &eventCode, &eventData);

         traceMem (0xfffe, 0, 0xe2);
         traceData (&fifoStat, 1);
         gpakUnlockAccess (0);


         if (fifoStat == RefNoEventAvail) break;
         else if (fifoStat != RefEventAvail) {
            printf ("readEvent error %d\n", fifoStat);
            appErr (__LINE__);
         }
         processEvent (Dsp, chanId, eventCode, &eventData);
      } while (fifoStat == RefEventAvail);
   }
}

static void rtpThread (void *args) {

   ADT_UInt32 Dsp, *p32;
   ADT_UInt16 chanId, PktI8, FillI8;
   gpakGetPayloadStatus_t   pktRcvStat;
   gpakSendPayloadStatus_t  pktSndStat;
   int status;

   int RemoteI8, rslt;
   
   p32 = args;
   Dsp = *p32;

   if (SetPriorityClass (GetCurrentProcess (), HIGH_PRIORITY_CLASS) == 0) {
      printf ("Priority change failure %d\n", GetLastError ());
      appErr (__LINE__);
   }

   //  Purge any buffered network packets
   do {
      RemoteI8 = sizeof (struct sockaddr);
      rslt = recvfrom (rtpSkt, (char *) &rtpPkt, sizeof (rtpPkt), 0, (struct sockaddr*) &RemoteAddr, &RemoteI8);
      if (rslt == SOCKET_ERROR) {
         if (WSAGetLastError () == WSAEWOULDBLOCK) break;
         if (WSAGetLastError () == WSAECONNRESET)  break;
         printf ("Network receive error %d\n", WSAGetLastError());
         appErr (__LINE__);
      }
   } while (TRUE);
#ifndef DSP_LOOPBACK
   while (!exitFlag) {
      // wait on semaphore with 5 millisecond wake up
      status = WaitForSingleObject (rtpSemaphore, 5);

         gpakLockAccess (0);
         traceMem (0xfff2, 0, 0xf1);
         traceData (&status, 1);
         gpakUnlockAccess (0);

      // Transfer RTP packets from DSP to network.
      do {
         PktI8 = sizeof (rtpPkt);
         pktRcvStat =  gpakGetPacketFromDsp (Dsp, &chanId, (ADT_UInt8 *) &rtpPkt, &PktI8);
         gpakLockAccess (0);
         traceMem (0xfff2, 0, 0xf2);
         traceData (&pktRcvStat, 1);
         gpakUnlockAccess (0);
         if (pktRcvStat == GpsNoPayload) break;
         else if (pktRcvStat != GpsSuccess) {
            printf ("GetPktFromDSP error %d\n", pktRcvStat);
            appErr (__LINE__);
         }

         if ((chanId == 0) && (rtpOutFile0 != NULL)) {
            FillI8 = (16 - PktI8) & 0xf;
            memset (((ADT_UInt8 *) &rtpPkt) + PktI8, 0xff, FillI8);
            fwrite (&rtpPkt, 1, PktI8 + FillI8, rtpOutFile0);
         }
         if ((chanId == 1) && (rtpOutFile1 != NULL)) {
            FillI8 = (16 - PktI8) & 0xf;
            memset (((ADT_UInt8 *) &rtpPkt) + PktI8, 0xff, FillI8);
            fwrite (&rtpPkt, 1, PktI8, rtpOutFile1);
         }

         if (ActiveChan.LOOPBACKMODE==LOOPBACK) {
            pktSndStat = gpakSendPacketToDsp (Dsp, chanId, (ADT_UInt8 *) &rtpPkt, PktI8);
            if (pktSndStat == SpsSuccess) toDSPCnt++;
            else if (pktSndStat != SpsSuccess) toDSPDropped++;
            else {
               printf ("SndPktToDSP error %d\n", pktSndStat);
               appErr (__LINE__);
            }
         } else if (ActiveChan.LOOPBACKMODE==CROSSOVER) {
			if(chanId>1) break;
            chanId ^= 1;
            pktSndStat = gpakSendPacketToDsp (Dsp, chanId, (ADT_UInt8 *) &rtpPkt, PktI8);
            if (pktSndStat == SpsSuccess) toDSPCnt++;
            else if (pktSndStat != SpsSuccess) toDSPDropped++;
            else {
               printf ("SndPktToDSP error %d\n", pktSndStat);
               appErr (__LINE__);
            }
         } else if (ActiveChan.LOOPBACKMODE == CROSSOVER_PKT) {
			ADT_UInt16 chanIdSendInd, chanIdSend, maxSendChannel;
		  if(MAX_CHANNELS <= 2) {
            chanId ^= 1;
            pktSndStat = gpakSendPacketToDsp (Dsp, chanId, (ADT_UInt8 *) &rtpPkt, PktI8);
            if (pktSndStat == SpsSuccess) toDSPCnt++;
            else if (pktSndStat != SpsSuccess) toDSPDropped++;
            else {
               printf ("SndPktToDSP error %d\n", pktSndStat);
               appErr (__LINE__);
            }
			
		  } else {

            if(chanId < 2) {
               chanIdSend = chanId + 2;
			   maxSendChannel = MAX_CHANNELS;
			} else if ((chanId == 2) || (chanId == 3)) {
               chanIdSend = chanId -2;
			   maxSendChannel = 2;
			} else {
			   chanIdSend = MAX_CHANNELS-1;
			   maxSendChannel = 0;
			}
            for(chanIdSendInd = chanIdSend; chanIdSendInd < maxSendChannel; chanIdSendInd += 2) {
               pktSndStat = gpakSendPacketToDsp (Dsp, chanIdSendInd, (ADT_UInt8 *) &rtpPkt, PktI8);

               if (pktSndStat == SpsSuccess) toDSPCnt++;
               else if (pktSndStat != SpsSuccess) toDSPDropped++;
               else {
			      printf ("SndPktToDSP error %d\n", pktSndStat);
			      appErr (__LINE__);
               }
			}
		  }
         } else {
            if (MAX_CHANNELS <= chanId || Dest[chanId].sin_port == 0) {
               toNetDropped++;
               continue;
            }
            rslt = sendto (rtpSkt, (char *) &rtpPkt, (int) PktI8, 0, (struct sockaddr*) &Dest[chanId], sizeof (SktInAddr));
            if (rslt != PktI8) {
               printf ("Network send error %d\n", WSAGetLastError());
               appErr (__LINE__);
            }
         }
         toNetCnt++;
      } while (pktRcvStat == GpsSuccess);

      // Transfer RTP packets from network to DSP
      do {
         RemoteI8 = sizeof (struct sockaddr);
         rslt = recvfrom (rtpSkt, (char *) &rtpPkt, sizeof (rtpPkt), 0, (struct sockaddr*) &RemoteAddr, &RemoteI8);
         if (rslt == SOCKET_ERROR) {
            if (WSAGetLastError () == WSAEWOULDBLOCK) break;
            if (WSAGetLastError () == WSAECONNRESET)  break;
            printf ("Network receive error %d\n", WSAGetLastError());
            appErr (__LINE__);
         }
         PktI8 = (ADT_UInt16) rslt;
         chanId = netAddrToChan (&RemoteAddr);
         pktSndStat = gpakSendPacketToDsp (Dsp, chanId, (ADT_UInt8 *) &rtpPkt, PktI8);
         if ((chanId == 0) && (rtpInFile0 != NULL)) {
            FillI8 = (16 - PktI8) & 0xf;
            memset (((ADT_UInt8 *) &rtpPkt) + PktI8, 0xff, FillI8);
            fwrite (&rtpPkt, 1, PktI8, rtpInFile0);
         }
         if ((chanId == 1) && (rtpInFile1 != NULL)) {
            FillI8 = (16 - PktI8) & 0xf;
            memset (((ADT_UInt8 *) &rtpPkt) + PktI8, 0xff, FillI8);
            fwrite (&rtpPkt, 1, PktI8, rtpInFile1);
         }

         if (pktSndStat == SpsSuccess) toDSPCnt++;
         else if (pktSndStat != SpsSuccess) toDSPDropped++;
         else {
            printf ("SndPktToDSP error %d\n", pktSndStat);
            appErr (__LINE__);
         }
      } while (TRUE);

      GenerateInterruptToDSP (Dsp);

   }
#endif
   ProcessDSPInterrupts = NULL;
}

static void threadInit (void *rtpArgs, void *evtArgs) {

   // Create Semaphore to signal RTP thread on interrupt
   rtpSemaphore = CreateSemaphore (NULL, 0, 1, "Pkt available");
   evtSemaphore = CreateSemaphore (NULL, 0, 1, "Evt available");
   ProcessDSPInterrupts = DSPIntrHandler;

   // Start up DSP transfer threads
   threadHandles[0] = _beginthread (rtpThread, 0, rtpArgs);
   threadHandles[1] = _beginthread (evtThread, 0, evtArgs);
   if (threadHandles[0]  == -1) goto threadErr;
   if (threadHandles[1] != -1) return;

threadErr:
   printf ("Thread startup error %d\n", errno);
   appErr (__LINE__);
}

static void threadStop () {

   exitFlag = TRUE;
   if (rtpSemaphore != NULL) ReleaseSemaphore (rtpSemaphore, 1, NULL);
   if (evtSemaphore != NULL) ReleaseSemaphore (evtSemaphore, 1, NULL);

   WaitForMultipleObjects (2, (void *) threadHandles, TRUE, INFINITE);
   CloseHandle (evtSemaphore);
   CloseHandle (rtpSemaphore);
   evtSemaphore = NULL;
   rtpSemaphore = NULL;


}

static void printStats () {

   printf ("\n\nPackets sent/dropped to net = %d/%d\n", toNetCnt, toNetDropped);
   printf ("Packets sent/dropped to DSP = %d/%d\n", toDSPCnt, toDSPDropped);
   printf ("Interrupts received %d\n", DSPInterruptCnt);
}

ADT_UInt32  AvgCpuUsage[MAX_NUM_GPAK_FRAMES];
ADT_UInt32  PeakCpuUsage[MAX_NUM_GPAK_FRAMES];
static void readUsage(ADT_UInt32 DspId) {
	gpakCpuUsageStatus_t usgStatus;      
    int i;
	usgStatus = gpakReadCpuUsage (DspId, AvgCpuUsage, PeakCpuUsage);
	if(usgStatus == GpakApiSuccess) {
	   printf("AvgCpuUsage: ");
       for (i=0; i<MAX_NUM_GPAK_FRAMES-1; i++)
	        printf("%d,\t", AvgCpuUsage[i]);
	   printf("%d\n", AvgCpuUsage[MAX_NUM_GPAK_FRAMES-1]);

	   printf("PeakCpuUsage: ");
       for (i=0; i<MAX_NUM_GPAK_FRAMES-1; i++)
	        printf("%d,\t", PeakCpuUsage[i]);
	   printf("%d\n", PeakCpuUsage[MAX_NUM_GPAK_FRAMES-1]);
	} else {
		printf("\nGet Chan Stat failed!\n");
	}
}
static void getParams () {
   char *c;
   char params[40] = { 30 };

   memset (params, 0, sizeof (params));
   params[0] = 30;

   ActiveChan = DefaultChan; 

   prompt = "\nEnter parameters: ";
   printf ("\nCODECS:\t9=729\t1=711Mu\t2=722\n");
   printf ("ALGS:\taa=AGC\tac=CID\taf=FAX+VOICE\taF=FAX ONLY\tav=VAD\tas=SRTP\n");
   printf ("ECS:\tea=Acoustic\tep=Packet\tet=TDM\n");
   printf ("TONES:\ttd=DTMF\ttp=PROG\ttg=GEN\ttr=RLY\tth=HOST\n\n");
   
   printf ("Loopback:\tln=None\tlc=Cross over\tll=Loopback\tlp=Cross over");
   printf (prompt);

   for (c = _cgets (params); *c != 0; c++) {
      switch (*c) {
      case '9':
         ActiveChan.ENCODE  = G729;
         ActiveChan.DECODE  = G729;
         ActiveChan.RTP_CODEC_PYLD = G729AB;
         break;
      case '1':
         ActiveChan.ENCODE  = PCMU_64;
         ActiveChan.DECODE  = PCMU_64;
         ActiveChan.RTP_CODEC_PYLD = 0;
         break;
      case '2':
         ActiveChan.ENCODE  = G722_64;
         ActiveChan.DECODE  = G722_64;
         ActiveChan.RTP_CODEC_PYLD = 0;
         break;

      case 'a':  // Algorithms
         c++;
         switch (*c) {
         case 'a': ActiveChan.AGC = Enabled;    break;
         case 'c': ActiveChan.CID = CidReceive; break;
         case 'f': ActiveChan.FAX = faxVoice;   ActiveChan.TONE |= Tone_Generate;   break;
         case 'F': ActiveChan.FAX = faxOnly;    ActiveChan.TONE |= Tone_Generate;   break;
         case 'v': ActiveChan.VAD = Enabled;    break;
         case 's': ActiveChan.SRTP = Enabled;
				c++;
	         switch (*c) {
	         case '0': SRTP_Key_Option = 0; break;
		      case '1': SRTP_Key_Option = 1; break;
			   case '2': SRTP_Key_Option = 2; break;
	         case '3': SRTP_Key_Option = 3; break;
		      case '4': SRTP_Key_Option = 4; break;
			   case '5': SRTP_Key_Option = 5; break;
	         case '6': SRTP_Key_Option = 6; break;
		      case '7': SRTP_Key_Option = 7; break;
			   case '8': SRTP_Key_Option = 8; break;
			   case '9': SRTP_Key_Option = 9; break;
				default:  SRTP_Key_Option = 0; break;
				}

				break;
         }
         break;

      case 'e':  // Echo cancellers
         c++;
         switch (*c) {
         case 'a': ActiveChan.AEC    = Enabled; break;
         case 'p': ActiveChan.PKT_EC = Enabled; break;
         case 't': ActiveChan.PCM_EC = Enabled; break;
         }
         break;

      case 'l':    // Loopback 
         c++;
         switch (*c) {
         case 'c': ActiveChan.LOOPBACKMODE = CROSSOVER; break;
         case 'l': ActiveChan.LOOPBACKMODE = LOOPBACK;  break;
         case 'n': ActiveChan.LOOPBACKMODE = NO_LOOP;   break;
         case 'p': ActiveChan.LOOPBACKMODE = CROSSOVER_PKT; break;
         }
         break;

      case 't':   // Tones
         c++;
         switch (*c) {
         case 'd': ActiveChan.TONE |= DTMF_tone; break;
         case 'g': ActiveChan.TONE |= Tone_Generate;   break;
         case 'h': ActiveChan.TONE |= Notify_Host;   break;
         case 'p': ActiveChan.TONE |= CallProg_tone; break;
         case 'r': ActiveChan.TONE |= (Tone_Relay | Tone_Regen); break;
         }
         break;
      }
   }
}
//-----------------------------------------------
// Main processing loop.
//
//  Setup channels.
//  Perform RTP packet transfers
//
int main (int argc, char* argv[]) {
   char *imageName;
   char key;

   FILE *imageFile;
   ADT_UInt32 Address, Value;
   ADT_Int32  DspId = 0;
   ADT_Int32  EvtDsp = 1;
   GpakTestData_t  tstParm;
   ADT_UInt16      RespData;
   GPAK_TestStat_t tstStatus;
   gpakDownloadStatus_t  DownloadStat = 0;
   char *Error;

   atexit (appExit);

   // Initialize modules
   netStackInit ();
   initGpakStructures ();

   // Initialize PCI interface to DSP
   Error = PCIConnect (DspId);
   if (Error != NULL) {
      printf ("PCI connect error %s\n", Error);
      appErr (__LINE__);
   }

   Error = PCIConnect (EvtDsp);
   if (Error != NULL) {
      printf ("PCI connect error (evt) %s\n", Error);
      appErr (__LINE__);
   }
   if (IMAGE_NAME == NULL)
      goto skipDownload;

download:
   // Perform DSP reset to place DSP in a known state for download
   Error = PCIReset (DspId);
   if (Error != NULL) {
      printf ("DSP reset error %s\n", Error);
      appErr (__LINE__);
   }
   // Clear EDMA parameters
   Value = 0;
   gpakLockAccess (DspId);
   for (Address = 0x01C04000; Address < 0x1C05000; Address+= 4)
      gpakWriteDspMemory32 (DspId, Address, 1UL, &Value);
   gpakUnlockAccess (DspId);

   // Download G.PAK image to DSP
   if (1 < argc) imageName = argv[1];
   else          imageName = IMAGE_NAME;

   imageFile = fopen (imageName, "rb");
   if (imageFile == NULL) {
      printf ("Unable to find %s\n", imageName);
      appErr (__LINE__);
   }

   DownloadStat = gpakDownloadDsp (DspId, (GPAK_FILE_ID) imageFile);
   if (DownloadStat != GdlSuccess) {
      printf ("Download failure %d\n", DownloadStat);
      appErr (__LINE__);
   }
   fclose (imageFile);
   printf ("%s downloaded\n", imageName);
	Sleep(1000L);
   // Read DSP's entry point and tell boot loader to start DSP
   Error = PCIRelease (DspId);
   if (Error != NULL) {
      printf ("DSP release error %s\n", Error);
      appErr (__LINE__);
   }

   // Initialize DSP's TDM interface
skipDownload:
#ifdef TEST_ON_EVM
   dspTDMInit (DspId);
#endif   
   // Initialize threads for packet and event transfers
   threadInit (&DspId, &EvtDsp);

channelInit:
   // Get channel initialization parameters
   getParams ();

   // Start EC capture
   // gpakTestMode (DspId, (GpakTestMode) (3), &tstParm,  &RespData, &tstStatus);

   // Initialize channels to defaults
   initChannels (DspId);
   
   // Wait until keyboard is pressed to exit
commandOptions:
   key = 0;
   printf ("\n\n1=start fax capture\t2=stop fax capture\n3=start ec capture\t4=stop ec capture\n");
   printf ("6=StartTonePkt\t7=InsertTonePkt\t8=Get Gain Info\n");
   printf ("q=quit\td=download\tr=reconfig\ts=stats\n");
   printf ("p=toggle RTP dump\n");
   prompt = "\nCmd: ";
   printf (prompt);
   do { 
      Sleep (1000L);  // One second between keyboard polling
      if (!_kbhit ()) continue;

      key = getch ();
      putch (key);
      switch (key) {
      case 'q':  break;
      case 'd':  // Download: teardown channels, stop all threads and restart with download
         dspTearDown (DspId);
         threadStop ();
         goto download;

      case 'r':  // Reconfigure: teardown channels, and restart with channel initialization
         dspTearDown (DspId);
         goto channelInit;

      case 's':  // Stats: print statistics
         printStats ();
			readUsage(DspId); 
         break;

      case 'p':
         if (rtpInFile0 != NULL) {
            fclose (rtpInFile0);
            fclose (rtpOutFile0);
            fclose (rtpInFile1);
            fclose (rtpOutFile1);
            
            rtpInFile0 = NULL;
            rtpOutFile0 = NULL;
            rtpInFile1 = NULL;
            rtpOutFile1 = NULL;
            printf ("RTP logging off\n");
         } else {
            rtpInFile0  = fopen ("Chan0In.rtp", "wb");
            rtpOutFile0 = fopen ("Chan0Out.rtp", "wb");
            rtpInFile1  = fopen ("Chan1In.rtp", "wb");
            rtpOutFile1 = fopen ("Chan1Out.rtp", "wb");
            printf ("RTP logging on\n");
         }
         break;

      case '1': case '2': case '3': case '4':  case '5':
         gpakTestMode (DspId, (GpakTestMode) (key - '0'), &tstParm, 
                       &RespData, &tstStatus);

         break;
      case '6':
         gpakGenTone (DspId, 2);

         break;
      case '7':
         gpakInsertTone (DspId, 3);

         break;
      case '8':
         gpakGetChanStat (DspId, 2);

         break;
      case '9':
			gpakReadSystem(DspId);
			break;
      default:
         goto commandOptions;
      }
   } while (key != 'q'); 

   // Teardown channels and stop all processing
   dspTearDown (DspId);
   threadStop ();

   printf ("\n\nProgram ended\n");
   printStats ();
   return 0;
}
