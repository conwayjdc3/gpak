// GpakNetApiExample.c
//
//
#define _CRT_SECURE_NO_WARNINGS

#include <winsock2.h>
#include <ws2tcpip.h>
#include <windows.h>
#include <process.h>
#include <conio.h>
#include <stdio.h>
#include <stdlib.h>

#include "GpakApiTestSupport.h"
#include "GpakEnum.h"

#include "GpakApi.h"
#include "sysconfig.h"


// ========================================
// G.PAK custom API routine for initialization
extern void initGpakStructures ();
extern int gpakDspStartup (ADT_UInt32 DspId, char *pCoffFileName);
extern char *IPToStr (ADT_UInt32 IPAddr);

static void appErr (int exitCode);
static void appExit (void);

// ========================================
//{ Network and firehose support data
#define FIREHOSE_CHANNELS    256

extern ADT_UInt32 pollForDSP (ADT_Int32 DspId, char *GPAKSERVER, ADT_UInt32 *hostIP, ADT_UInt32 *dspIP);

extern ADT_UInt32 cfgFireHose (ADT_Int32 DspId, int chanCnt, int frameSize, int pyldI8,
                        int pyldType, int SSRC, int port, int IP, char *MAC);

// EMAC address of EVM board
ADT_UInt8 GPAKSERVER[25];
//ADT_UInt8 *DefaultServer = "08.00.28.39.3d.f8";   // TI Tomahawk EVM.
ADT_UInt8 *DefaultServer = "00.08.ee.05.6e.2e";   // Logic PD EVM

ADT_UInt32 DrainIP     = 0x0A00000B;  // IP address to drain packets sent by DSP
ADT_Int32  BaseDestIP  = 0x0A000100;  // Base IP address of VoIP test phones
ADT_Int32  BaseMCastIP = 0xE1000000;  // Base Multicast IP address for tra
ADT_Int32  BaseSSRC    = 1;           // Value of 0 forces RTP to  assign ramdom SSRC on Tramsmit
ADT_Int32  dspIP;                     // Discovered IP address of DSP

ADT_Int32  CustomDestIP  = 0x0A00006A;  // Huafeng's VoIP test phones
int UseCustomDestIP = 0;
ADT_UInt32 CustomSSRC = 0;
ADT_UInt32 CustomDestSSRC = 0;
int UseCustomSSRC = 0;

ADT_UInt8 referenceMacAddrs[][6] = {
   { 0x00, 0x0a, 0xdd, 0x81, 0x24, 0xd9 },   // 10.0.0.11
   { 0x00, 0x0a, 0xdd, 0x80, 0x00, 0x29 },   // 10.0.0.6
   { 0x00, 0x0a, 0xdd, 0x81, 0x00, 0x44 },   // 10.0.0.7 (broken)
//   { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 },
};

ADT_UInt32 referenceIPAddrs[] = {
   0x0A000005,     // 10.0.0.5
   0x0A000006,     // 10.0.0.6
   0x0A000007,     // 10.0.0.7   (broke)
};

ADT_UInt8 fireHoseMac[6] = {   0x00, 0x24, 0xba, 0x45, 0x74, 0xec };


// MAC and IP addresses of DSP
ADT_UInt8 DSPMac[6];
ADT_Int32 FirehoseIP = 0;   // IP address of 'Firehose'

//}

// ========================================
//{  Interactive support structures.
//
//  channel type count defaults
chanCnts chns = { 
   0,   // tdmTdm
   4,   // tdmPkt
   0,   // pktPkt
   0,   // tdmCnfr
   0,   // pktCnfr
   0    // cmpCnfr
};

//  Channel configuration defaults
chanCfg DefaultChan = {
   CROSSOVER,   //  LOOPBACKMODE 
   Disabled,    //  PCM_EC
   Disabled,    //  PKT_EC
   Disabled,    //  AEC
   Disabled,    //  AGC
   Disabled,    //  VAD
   Disabled,    //  NOISE
   disabled,    //  FAX

   Frame_10ms,  // ENCSZ 
   Frame_10ms,  // DECSZ 

   0,           //  TONEA
   0,           //  TONEB

   PCMU_64,     //  ENCODE
   PCMU_64,     //  DECODE
   PCMU_64,     //  RTP_CODEC_PYLD
   CidDisabled, //  CID
   
   0,          // TG_GAIN
   0,          // IN_GAIN
   0,          // OUT_GAIN

   SerialPort1, // TDM_PORT
   Disabled,    //  NOISEB
   Disabled     // AGCB
};
chanCfg ActiveChan;

//  Conference configuration defaults
cnfrCfg DefaultCnfr[] = {
   160,  (GpakToneTypes) Null_tone,     // Frame size (PCM samples), tones
    80,  (GpakToneTypes) Null_tone,     // Frame size (PCM samples), tones
   160,  (GpakToneTypes) Null_tone,     // Frame size (PCM samples), tones
   0
};
cnfrCfg ActiveCnfr[5];
//}

// ========================================
//{  Canned test support
//  Record/playback buffer details
DSP_Address PlayRecAddress = 0xc2B00000;  // Base address of recording buffers
int         PlayRecI8      = 0x00500000;  // Total length of recording buffers

// Default bit map of configured and active TDM slots
ADT_UInt32 activeSlots[3][32];
ADT_UInt32 cfgSlots[3][32];
ADT_UInt16 leftSlt = 0, rightSlt = 1;

// host loopback mode
LoopbackMode hostLoopBack = DSP_LOOP;
LoopbackMode TDM_loopback = NO_LOOP;
int TDM_crossoverbit = 1;
//}

//=================================================
//{ Network and firehose support routines
static char MACAddrBuff [20];
static char IPAddrBuff [16];
char hextoc (char *hex) {
   char nybble;
   char value = 0;

   nybble = *hex++;
   if ('a' <= nybble) value |= (nybble - 'a' + 10) << 4;
   else               value |= (nybble - '0') << 4;

   nybble = *hex++;
   if ('a' <= nybble) value |= (nybble - 'a' + 10);
   else               value |= (nybble - '0');
   return value;
}

ADT_UInt32 strToIP (char *ipStr) {
   int i;
   union {
      ADT_UInt32 I32;
      ADT_UInt8  I8[4];
   } IPAdd;
   ADT_UInt32  I32[4];
   sscanf (ipStr, "%d.%d.%d.%d", &I32[3], &I32[2], &I32[1], &I32[0]);
 
   for (i=0; i<4; i++) IPAdd.I8[i] = (ADT_UInt8) I32[i];
   return IPAdd.I32;
}
char *macToStr (ADT_UInt8 *MAC) {
   sprintf (MACAddrBuff, "%02x.%02x.%02x.%02x.%02x.%02x", MAC[0], MAC[1], MAC[2], MAC[3], MAC[4], MAC[5]);
   return MACAddrBuff;
}


#ifdef ADT_DEVELOPMENT
void turnOnFireHose (int DspId) {
   short codecType;
   int pyldI8 = 20;
   int firehoseChans;
 
   codecType = ActiveChan.RTP_CODEC_PYLD;

   switch (ActiveChan.DECODE) {
   case PCMU_64:  case PCMA_64:   case L8:
   case G722_64:
      pyldI8 = ActiveChan.DECSZ * 4; break;
      
   case G729:     case G729AB:
      codecType = ActiveChan.DECODE;
      pyldI8 = ActiveChan.DECSZ / 2; break;
   }
   if (NetEnables.stereo) pyldI8 *= 2;


   gpakGetSystemConfig (DspId, &sysCfg);

   firehoseChans = sysCfg.ActiveChannels; 
   if (FIREHOSE_CHANNELS < firehoseChans) firehoseChans = FIREHOSE_CHANNELS;
   FirehoseIP = cfgFireHose (DspId, firehoseChans, ActiveChan.DECSZ * 4, pyldI8,
                   codecType, 1, 6334, dspIP, DSPMac);
}   
void turnOffFireHose (int DspId) {
   short codecType;
  
   codecType = ActiveChan.RTP_CODEC_PYLD;

   switch (ActiveChan.DECODE) {
   case G729:     case G729AB: codecType = ActiveChan.DECODE; break;
   }
   FirehoseIP = cfgFireHose (DspId, 0, 0, 0, codecType, 1, 6334, 
                             dspIP, DSPMac);
}
#endif
//}

//=================================================
//{  DSP TDM serial port initialization
//
#ifdef PORT_CFG_MCBSP_64
#define MAX_SLOTS 128
void dspTDMInit (ADT_UInt32 DspId) {

   // McBSP configuration variables
   GpakPortConfig_t       PortConfig;
   gpakConfigPortStatus_t PrtCfgStat = 0;
   GPAK_PortConfigStat_t  PrtDspStat = 0;

   dspTearDown (DspId);

   memset (&PortConfig, 0, sizeof (PortConfig));

   // Configure McBSP1, Disable McBSP0 and McBSP2
   PortConfig.AudioPort1Enable = Disabled;
   PortConfig.AudioPort2Enable = Disabled;

   // McBSP 0 (Attached to daughter card)
   PortConfig.Port1Enable  = Disabled;
   PortConfig.SlotsSelect1 = Disabled;
   PortConfig.SampRateGen1     = Disabled; 
   PortConfig.Port1SlotMask0_31  = 0x00FFFFFF;  // One bit for each 'active slot'
   PortConfig.Compand1          = cmpPCMU;
   PortConfig.SampRateGen1      = Enabled; 

   // McBSP 1 (Attached to AIC)
   PortConfig.SlotsSelect2 = Disabled;
   PortConfig.SampRateGen2 = Disabled; 

   PortConfig.Port2Enable  = Enabled;
   PortConfig.Port2SlotMask0_31 = 3;
   PortConfig.TxDataDelay2 = 1;
   PortConfig.RxDataDelay2 = 1;
   PortConfig.Compand2  = cmpNone16;

   // McBSP 2 (Not available)
   PortConfig.Port3Enable  = Disabled;
   PortConfig.SlotsSelect3 = Disabled;

   PrtCfgStat = gpakConfigurePorts (DspId, &PortConfig, &PrtDspStat);
   if (PrtCfgStat != CfsSuccess || PrtDspStat != Pc_Success) {
      printf ("McBSP setup failure %d:%d:%d\n", PrtCfgStat, PrtDspStat, DSPError [DspId]);
      appErr (__LINE__);
   }


}
#endif

#ifdef PORT_CFG_TSIP
#define MAX_SLOTS 1024
void PopulateSlotMask (ADT_UInt32 *slotMask, int slotCnt, int slotsPerSample) {
   ADT_UInt32 mask;
   int  slotsPerMask, bitsPerSlot;

   // Space out slots across the TDM bus.
   if (slotsPerSample == 8) {
      slotsPerMask = 4;
      mask = 0x01010101;
   } else if (slotsPerSample == 4) {
      slotsPerMask = 8;
      mask = 0x11111111;
   } else if (slotsPerSample == 2) {
      slotsPerMask = 16;
      mask = 0x55555555;
   } else {
      slotsPerMask = 32;
      mask = 0xffffffff;
      slotsPerSample = 1;
   }
   bitsPerSlot = 32 / slotsPerMask;

   // Populate slot mask array
   while (slotsPerMask <= slotCnt) {
      *slotMask++ = mask;
      slotCnt -= slotsPerMask;
   }
   *slotMask = (1 << (slotCnt * bitsPerSlot)) - 1;
   *slotMask &= mask;
}
void PopulateCompandingMask (ADT_UInt32 *slotMask, int slotCnt, int slotsPerSample, int compand_a) {
   ADT_UInt32 mask;
   int slotsPerMask;

   if (compand_a) mask = -1;
   else           mask =  0;
   slotsPerMask = 32 * 8;

   slotCnt *= slotsPerSample;

   // Populate slot mask array
   while (slotsPerMask <= slotCnt) {
      *slotMask++ = mask;
      slotCnt -= slotsPerMask;
   }
   *slotMask = (1 << (slotCnt / 8)) - 1;
   *slotMask &= mask;
}
void dspTDMInit (ADT_UInt32 DspId) {
   // TSIP configuration variables
   GpakApiStatus_t        apiStat;
   GpakPortConfig_t       PortConfig;
   gpakConfigPortStatus_t PrtCfgStat = 0;
   GPAK_PortConfigStat_t  PrtDspStat = 0;
   int tsipMask, attempts, slotCnt, slotsPerSample, compand_a;
   int loopback, slotsPerSamp;

   dspTearDown (DspId);

   apiStat = gpakGetSystemConfig (DspId, &sysCfg);
   if (apiStat != GpakApiSuccess) {
      ADT_printf ("Get system configuration failure %d:%d\n", apiStat, DSPError [DspId]);
   }
   
   tsipMask = ADT_getint ("\nEnter tsipActive mask[0, 7]: ");
   if (tsipMask != 0) {
      slotsPerSample = ADT_getint ("\nEnter slots per sample [1,2,4,8]: ");
      slotCnt   = ADT_getint ("\nEnter active slot count[1-1024]: ");
      compand_a = ADT_getint ("\nSelect companding mode[0=u-law, 1=a-law]: ");

      if (ADT_getint ("\nLoopback[0=None]: ") == 0) loopback = noLoopback;
      else                                          loopback = internalLoopback;

      if (1024 < slotCnt) slotCnt = 1024;
   } else {
      slotCnt  = 0;
      tsipMask = 7;
      slotsPerSample = 8;
      compand_a = 0;
      if (TDM_loopback == DSP_LOOP) loopback = internalLoopback;
      else                          loopback = noLoopback;

   }
#ifdef ADT_DEVELOPMENT //  Tell DSP to enable TSIP2 for testing.
   // TSIP2 not configured.  Send DSP message configure TSIP2 as port 0.
   if (sysCfg.Port3SupSlots == 0) {
      GpakTestData_t  tstParm;
      GPAK_TestStat_t tstStatus;
      ADT_UInt16      RespData;
      ADT_printf("\nSending Custom Message 200\n");
      tstParm.u.TestParm = 200;
      gpakTestMode (DspId, UserDefined, &tstParm, &RespData, &tstStatus);       
   }
#endif

   TDM_crossoverbit = slotsPerSample;
   memset (&PortConfig, 0, sizeof (PortConfig));
   PortConfig.TsipEnable1 = Disabled;
   PortConfig.TsipEnable2 = Disabled;
   PortConfig.TsipEnable3 = Disabled;

   // TSIP 1 configuration
   if ((sysCfg.Port1SupSlots == 0) || ((tsipMask & 1) == 0)) {
      ADT_printf ("\nPort 1. 0 slots.\n");
   } else {
      PortConfig.TsipEnable1 = Enabled;
      if (slotCnt != 0) sysCfg.Port1SupSlots = slotCnt;
      if (1024 < (sysCfg.Port1SupSlots * slotsPerSample)) {
         slotsPerSamp = 1024 / sysCfg.Port1SupSlots;
      } else {
         slotsPerSamp = slotsPerSample;
      }
      ADT_printf ("\nPort 1. %u slots spaced %u slots apart.\n", sysCfg.Port1SupSlots, slotsPerSamp);
      PopulateSlotMask (PortConfig.slotMask1, sysCfg.Port1SupSlots, slotsPerSamp);
      PortConfig.singleClk1       = Enabled;
      PortConfig.txFsPolarity1    = fsActLow;
      PortConfig.txDataClkEdge1   = clkRising;
      PortConfig.txFsClkEdge1     = clkRising;
      PortConfig.txClkMode1       = clkSingleRate;
      PortConfig.txDataRate1      = rate_8Mbps;
      PortConfig.txClkFsSrc1      = clkFsA;
      PortConfig.txDatDly1        = 1023;
      PortConfig.txDriveState1    = highz;
      PortConfig.txOutputDly1     = Disabled;
      PortConfig.rxFsPolarity1    = fsActLow;
      PortConfig.rxDataClkEdge1   = clkFalling;
      PortConfig.rxFsClkEdge1     = clkRising;
      PortConfig.rxClkMode1       = clkSingleRate;
      PortConfig.rxDataRate1      = rate_8Mbps;
      PortConfig.rxClkFsSrc1      = clkFsA;
      PortConfig.rxDatDly1        = 1023;
      PortConfig.loopBack1        = loopback; 
      PopulateCompandingMask (PortConfig.slotCompand1, sysCfg.Port1SupSlots, slotsPerSamp, compand_a);
   }

   // TSIP 2 configuration
   if ((sysCfg.Port2SupSlots == 0) || ((tsipMask & 2) == 0)) {
      ADT_printf ("Port 2. 0 slots.\n");
   } else {
      PortConfig.TsipEnable2 = Enabled;
      if (slotCnt != 0) sysCfg.Port2SupSlots = slotCnt;
      
      if (1024 < (sysCfg.Port2SupSlots * slotsPerSample)) {
         slotsPerSamp = 1024 / sysCfg.Port2SupSlots;
      } else {
         slotsPerSamp = slotsPerSample;
      }
      ADT_printf ("Port 2. %u slots spaced %u slots apart.\n", sysCfg.Port2SupSlots, slotsPerSamp);

      PopulateSlotMask (PortConfig.slotMask2, sysCfg.Port2SupSlots, slotsPerSamp);
      PortConfig.singleClk2       = Enabled;
      PortConfig.txFsPolarity2    = fsActLow;
      PortConfig.txDataClkEdge2   = clkRising;
      PortConfig.txFsClkEdge2     = clkRising;
      PortConfig.txClkMode2       = clkSingleRate;
      PortConfig.txDataRate2      = rate_8Mbps;
      PortConfig.txClkFsSrc2      = clkFsA;
      PortConfig.txDatDly2        = 1023;
      PortConfig.txDriveState2    = highz;
      PortConfig.txOutputDly2     = Disabled;
      PortConfig.rxFsPolarity2    = fsActLow;
      PortConfig.rxDataClkEdge2   = clkFalling;
      PortConfig.rxFsClkEdge2     = clkRising;
      PortConfig.rxClkMode2       = clkSingleRate;
      PortConfig.rxDataRate2      = rate_8Mbps;
      PortConfig.rxClkFsSrc2      = clkFsA;
      PortConfig.rxDatDly2        = 1023;
      PortConfig.loopBack2        = loopback;
      PopulateCompandingMask (PortConfig.slotCompand2, sysCfg.Port2SupSlots, slotsPerSamp, compand_a);
   }

   // TSIP 3 configuration
   if ((sysCfg.Port3SupSlots == 0) || ((tsipMask & 4) == 0)) {
      ADT_printf ("Port 3. 0 slots.\n");
   } else {
      PortConfig.TsipEnable3 = Enabled;
      if (slotCnt != 0) sysCfg.Port3SupSlots = slotCnt;
      if (1024 < (sysCfg.Port3SupSlots * slotsPerSample)) {
         slotsPerSamp = 1024 / sysCfg.Port3SupSlots;
      } else {
         slotsPerSamp = slotsPerSample;
      }
      ADT_printf ("Port 3. %u slots spaced %u slots apart.\n", sysCfg.Port3SupSlots, slotsPerSamp);

      PopulateSlotMask (PortConfig.slotMask3, sysCfg.Port3SupSlots, slotsPerSamp);
      PortConfig.singleClk3       = Enabled;
      PortConfig.txFsPolarity3    = fsActLow;
      PortConfig.txDataClkEdge3   = clkRising;
      PortConfig.txFsClkEdge3     = clkRising;
      PortConfig.txClkMode3       = clkSingleRate;
      PortConfig.txDataRate3      = rate_8Mbps;
      PortConfig.txClkFsSrc3      = clkFsA;
      PortConfig.txDatDly3        = 1023;
      PortConfig.txDriveState3    = highz;
      PortConfig.txOutputDly3     = Disabled;
      PortConfig.rxFsPolarity3    = fsActLow;
      PortConfig.rxDataClkEdge3   = clkFalling;
      PortConfig.rxFsClkEdge3     = clkRising;
      PortConfig.rxClkMode3       = clkSingleRate;
      PortConfig.rxDataRate3      = rate_8Mbps;
      PortConfig.rxClkFsSrc3      = clkFsA;
      PortConfig.rxDatDly3        = 1023;
      PortConfig.loopBack3        = loopback;
      PopulateCompandingMask (PortConfig.slotCompand3, sysCfg.Port3SupSlots, slotsPerSamp, compand_a);
   }

   attempts = 5;
   while (attempts) {
       PrtCfgStat = gpakConfigurePorts (DspId, &PortConfig, &PrtDspStat);
       attempts--;
       if (PrtCfgStat == GpakApiSuccess)   break;
       gpakHostDelay ();
   }
   if (PrtCfgStat == GpakApiParmError && PrtDspStat == Pc_ChannelsActive) {
      ADT_printf ("\n TSIP configuration failure- channels active\n");
      return;
   }
   if (PrtCfgStat != GpakApiSuccess || PrtDspStat != Pc_Success) {
      ADT_printf ("\n TSIP setup failure %d:%d:%d\n", PrtCfgStat, PrtDspStat, DSPError [DspId]);
      memset (cfgSlots, 0, sizeof (cfgSlots));
   } else {
      ADT_printf (" \n TSIP configuration success after %d attempts\n",(5-attempts));
      memcpy (cfgSlots[0], PortConfig.slotMask1, sizeof (PortConfig.slotMask1));
      memcpy (cfgSlots[1], PortConfig.slotMask2, sizeof (PortConfig.slotMask2));
      memcpy (cfgSlots[2], PortConfig.slotMask3, sizeof (PortConfig.slotMask3));
      freeAllSlots (0);
      freeAllSlots (1);
      freeAllSlots (2);
   }
   if (loopback != noLoopback) ADT_printf ("TDM loopback enabled\n");
}
#endif
// Assign slots from first to last.
int getSlot (GpakSerialPort_t port) {
   int i, j;
   ADT_UInt32 availSlots;

#ifdef ADT_DEVELOPMENT
   // NOTE: For testing with TI EVM and GL.COMM
   // Give priority to slots 8 and 16 on TSIP port connected to GL Comm box
   int  GLComm_port;

   if (sysCfg.Port3SupSlots != 0) GLComm_port = 2;
   else                           GLComm_port = 0;

   if (((cfgSlots[GLComm_port][0] & 0x100) == 0x100) && ((activeSlots[GLComm_port][0] & 0x100) == 0)) {
      activeSlots[GLComm_port][0] |= 0x100;  
      ADT_printf ("GL.COM 2, slot 8\n");
      return 8 | (GLComm_port << 16);
   }
   if (((cfgSlots[GLComm_port][0] & 0x10000) == 0x10000) && ((activeSlots[GLComm_port][0] & 0x10000) == 0)) {
      activeSlots[GLComm_port][0] |= 0x10000;  
      ADT_printf ("GL.COM 2, slot 16\n");
      return 0x10 | (GLComm_port << 16);
   }

   // jdc added - route second channel to gl.comm too
   // Give priority to slot 24 on TSIP port connected to GL Comm box
   if (((cfgSlots[GLComm_port][0] & 0x01000000) == 0x01000000) && ((activeSlots[GLComm_port][0] & 0x01000000) == 0)) {
      activeSlots[GLComm_port][0] |= 0x01000000;  
      ADT_printf ("GL.COM 2, slot 24\n");
      return 0x18 | (GLComm_port << 16);
   }
   // Give priority to slot 32 on TSIP port connected to GL Comm box
   if (((cfgSlots[GLComm_port][1] & 0x01) == 0x01) && ((activeSlots[GLComm_port][1] & 0x01) == 0)) {
      activeSlots[GLComm_port][1] |= 0x01;  
      ADT_printf ("GL.COM 2, slot 32\n");
      return 0x20 | (GLComm_port << 16);
   }
#endif

   for (i=0; i < (MAX_SLOTS/32); i++) {
      if (cfgSlots[port][i] == 0) continue;
      if (cfgSlots[port][i] == activeSlots[port][i]) continue;
      availSlots = ~activeSlots [port][i] & cfgSlots [port][i];
      j = 0;
      while ((availSlots & 1) == 0) {
         availSlots = availSlots >> 1;
         j++;
      }
      activeSlots[port][i] |= 1 << j;  
      return (port << 16) | ((i * 32) + j);
   }
   return -1;
}
void freeSlot (GpakSerialPort_t port, int slot) {
   int idx, slotMask;
   
   idx = slot >> 5;
   slot = slot & 0x1f;
   slotMask = 1 << slot;
   activeSlots[port][idx] &= ~slotMask;
}
void freeAllSlots (GpakSerialPort_t port) {
   int i;
   for (i = 0; i < MAX_SLOTS/32; i++) activeSlots[port][i] = 0;
}

//}

//=================================================
//{  User interface support
#define txLock()
#define txUnlock()
#define rxLock()
#define rxUnlock()

int ADT_printf (char *format, ...) {
   int count;
   va_list  params;

   if (format == NULL) return 0;

   va_start (params, format);
   count = vprintf (format, params);
   va_end (params);

   return count;
}

void ADT_putchar (char key) {
   txLock ();
   _putch (key);
   txUnlock ();
}

char ADT_getchar (void) {
   ADT_UInt8  data;

   txLock ();
   rxLock ();
   while ((data = getch ()) == 0) {
      txUnlock ();  // Allow higher priority tasks to gain access to transmit data
      ADT_sleep (1);
      txLock ();
   }
   rxUnlock ();
   txUnlock ();
   ADT_putchar (data); // Echo character back to console
   return data;
}

void ADT_gets (char *buffer, int bufI8) {
   ADT_UInt8 *data;
   int i;

   data = (ADT_UInt8 *) buffer;
   txLock ();
   rxLock ();
   for (i=0; i<bufI8; i++) {
      while ((*data = getch ()) == 0) {
         txUnlock ();
         ADT_sleep (2);
         txLock ();
      }
      ADT_putchar (*data); // Echo character back to console
      if (*data == '\r')  break;
      if (*data == '\n')  break;
      if (*data == 8) {   // Backspace
         if (i==0) continue;
         data--;
         i--;
         continue;
      }
      data++;
   }
   data++;
   *data = 0;
   rxUnlock ();
   txUnlock ();
}

int ADT_getint (char *prompt) {
   char inputBuffer[80];
   if (prompt) ADT_printf ("\n\r%s", prompt);
   ADT_gets (inputBuffer, sizeof (inputBuffer));
   return atoi (inputBuffer);
}

void ADT_sleep (int ms) {
   Sleep (ms);
}

//}

// ========================================
//{  Host threads
//
//  NOTE: User interface thread is handled by main.
//        Events are handled by evtThread.
//        No RTP thread is required since DSP places packets on network.
static unsigned int threadHandles [2];
static int exitFlag = FALSE;
static HANDLE evtSemaphore = NULL;

void (*rtpProcess)(ADT_UInt32 DSP) = NULL;

static void evtThread (void * args) {
   ADT_UInt32 Dsp = 0;
   ADT_Int16  chanId;
   GpakApiStatus_t      fifoStat;
   GpakAsyncEventCode_t eventCode;
   GpakAsyncEventData_t eventData;

   if (SetPriorityClass (GetCurrentProcess (), NORMAL_PRIORITY_CLASS) == 0) {
      ADT_printf ("Priority change failure %d\n", GetLastError ());
      appErr (__LINE__);
   }

   // Poll DSP 0 for events.
   while (!exitFlag) {

      ADT_sleep (20);

      // Transfer events from DSP to host.
      do {
         fifoStat = gpakReadEventFIFOMessage (Dsp, &chanId, &eventCode, &eventData);
         if (fifoStat == RefNoEventAvail) break;
         else if (fifoStat != RefEventAvail) {
            ADT_printf ("readEvent error %d\n", fifoStat);
            break;
         }
         processEvent (Dsp, chanId, eventCode, &eventData);
      } while (fifoStat == RefEventAvail);

      // Display periodic status
      periodicStatusDisplay (Dsp);
   }
   ADT_printf ("Polling thread ended\n");
}

static void threadInit () {

   // Create Semaphore to signal RTP thread on interrupt
   evtSemaphore = CreateSemaphore (NULL, 0, 1, (LPCWSTR) "Evt available");

   // Start up DSP transfer threads
   threadHandles[0] = _beginthread (evtThread, 0, NULL);
   if (threadHandles[0] != -1) return;

   ADT_printf ("Thread startup error %d\n", errno);
   appErr (__LINE__);
}

static void threadStop () {

   exitFlag = TRUE;
   if (evtSemaphore != NULL) ReleaseSemaphore (evtSemaphore, 1, NULL);

   WaitForMultipleObjects (1, (void *) threadHandles, TRUE, INFINITE);
   CloseHandle (evtSemaphore);
   evtSemaphore = NULL;
   printf ("Polling thread stopped\n");
}
//}

//-----------------------------------------------
//{ Main processing loop.
//
//  Setup channels.
//  Perform RTP packet transfers
//
int main (int argc, char* argv[]) {
   ADT_Int32  DspId = 0;
   ADT_UInt32 hostIP;

   atexit (appExit);

   // Initialize modules
   initGpakStructures ();
   gpakServerInit ();

   // Format name of G.PAK server from passed in argument
   strncpy  (GPAKSERVER, "Gpak.", sizeof (GPAKSERVER));
   if (1 < argc) {
      strncat (&GPAKSERVER[5], argv[1], 18);
   } else {
      strncat (&GPAKSERVER[5], DefaultServer, 18);
   }

   // Find hostIP on server's subnet
   hostIP = 0;
   pollForDSP (0, GPAKSERVER, &hostIP, &dspIP);

   if (hostIP == 0) {
      ADT_printf ("Failed to find DSP\n");
      ADT_printf ("\nEnter dsp's IP address: ");
      ADT_gets (&IPAddrBuff[0], sizeof (IPAddrBuff));

      dspIP = strToIP (IPAddrBuff);
   } else {
      ADT_printf ("DSPIP = %s\n", IPToStr(dspIP));
   }

   if (!gpakServerConnect (DspId, GPAKSERVER, dspIP)) {
      ADT_printf ("Server %s initialization failure %x\n", GPAKSERVER, DSPError [DspId]);
      appErr (__LINE__);
   }

   // Turn off fire hose packets to DSP
   DSPMac[0] = hextoc (&GPAKSERVER[5]);
   DSPMac[1] = hextoc (&GPAKSERVER[8]);
   DSPMac[2] = hextoc (&GPAKSERVER[11]);
   DSPMac[3] = hextoc (&GPAKSERVER[14]);
   DSPMac[4] = hextoc (&GPAKSERVER[17]);
   DSPMac[5] = hextoc (&GPAKSERVER[20]);

#ifdef FIRE_HOSE_TEST
do {
   ADT_UInt8 localMac[] = {  0x0, 0x18, 0xf8, 0x88, 0x5a, 0x86 };
   int dspIP2;
   dspIP2 = 0x0400000a;
   FirehoseIP = cfgFireHose (DspId, 0, 160, 160, 1, 1, 6334, dspIP,  DSPMac);
   FirehoseIP = cfgFireHose (DspId, 0, 160, 160, 1, 1, 6334, dspIP2, localMac);
ADT_getint ("Start");
   FirehoseIP = cfgFireHose (DspId, 2, 160, 160, 1, 1, 6334, dspIP,  DSPMac);
   FirehoseIP = cfgFireHose (DspId, 2, 160, 160, 1, 1, 6334, dspIP2, localMac);
ADT_getint ("Stop");
} while (ADT_TRUE);
#endif


   // Initialize thread for event transfers
   threadInit ();

   // Initialize DSP's TDM interface
   gpakDspStartup (DspId, NULL);


   userInterface ();
   
   // Teardown channels and stop all processing
   threadStop ();

   gpakServerDisconnect (DspId);
   ADT_printf ("\n\nProgram ended\n");
   return 0;
}

static void appErr (int exitCode) {
   ADT_printf ("Application error %d\n", exitCode);
   exit (exitCode);
}

static void appExit (void) {
   threadStop ();

   gpakServerDisconnect (0);
   ADT_getchar ();
}
//}

