//  sd_pci_lib.c
//
//  Interface to one of two variants (dm642 or evm6424) of spectrum digital's PCI dll
//

#include "windows.h"
#include "stdio.h"
#include "sd_pci_lib.h"

HMODULE PCILibOpen (int dspType) {
   HMODULE dll;

   if (dspType == 6424) 
      dll =  LoadLibrary ("sd_pci_6424.dll");
   else 
      dll =  LoadLibrary ("sd_pci_dm642.dll");
       
   if (dll == NULL) {
      printf ("PCI load library failed\n");
      return NULL;
   }


   PCI64_Open  = (PCIOpen)  GetProcAddress (dll, "PCI64_Open");
   PCI64_Close = (PCIClose) GetProcAddress (dll, "PCI64_Close");

   PCI64_MemFill32  = (PCIXfer) GetProcAddress (dll, "PCI64_MemFill32");
   PCI64_MemWrite32 = (PCIXfer) GetProcAddress (dll, "PCI64_MemWrite32");
   PCI64_MemRead32  = (PCIXfer) GetProcAddress (dll, "PCI64_MemRead32");
   PCI64_RegWrite32 = (PCIXfer) GetProcAddress (dll, "PCI64_RegWrite32");
   PCI64_RegRead32  = (PCIXfer) GetProcAddress (dll, "PCI64_RegRead32"); 

   PCI64_LoadCoff           = (PCILoadCoff) GetProcAddress (dll, "PCI64_LoadCoff"); 
   PCI64_GetCallbackEvent   = (PCICbHndl)   GetProcAddress (dll, "PCI64_GetCallbackEvent"); 
   PCI64_SetupCallbackEvent = (PCICbEvent)  GetProcAddress (dll, "PCI64_SetupCallbackEvent");
   return dll;
}

PCILibClose (HMODULE dll) {
   FreeLibrary (dll);
}

