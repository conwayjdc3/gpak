/*
 * Copyright (c) 2001-2010, Adaptive Digital Technologies, Inc.
 *
 * File Name: GpakCustWin32.h
 *
 * Description:
 *   This file contains host system dependent definitions and prototypes of
 *   functions to support generic G.PAK API functions. The file is used when
 *   integrating G.PAK API functions in a specific host processor environment.
 *
 *   Note: This file may need to be modified by the G.PAK system integrator.
 *
 * Version: 1.0
 *
 * Revision History:
 *   10/17/01 - Initial release.
 *
 */

#ifndef _GPAKCUSTWIN32_H  /* prevent multiple inclusion */
#define _GPAKCUSTWIN32_H

#include "adt_typedef_user.h"
#include <stdlib.h>
#include <stdio.h>

#define HOST_LITTLE_ENDIAN         /* un-comment this if little endian host */

#define MAX_NUM_DSPS            1       /* maximum number of DSPs */
#define MAX_CHANNELS           160       /* maximum number of channels */
#define DOWNLOAD_BLOCK_I8     512       /* download block size (bytes) */
#ifndef NETWORK_MESSAGING
   #define MAX_WAIT_LOOPS         50       /* max number of wait delay loops for API call */
#else
   #define MAX_WAIT_LOOPS        250       /* max number of wait delay loops for API call */
#endif
#define NUM_TDM_SLOTS 256  // number of tdm slots available on port (either: mcbsp, mcasp, or tsip)
#define MIN_CHAN_CORE 2    // lowest coreID that supports channels
#define MAX_CHAN_CORE 8    // max coreID that supports channels

#define UDP_MESSAGING     // un-comment this to enable UDP transport for control and event messages

typedef struct IP6N
{
    union
    {
        ADT_UInt8   addr8[16];
        ADT_UInt16  addr16[8];
        ADT_UInt32  addr32[4];
    }u;
}IP6N;

#ifndef HOST_LITTLE_ENDIAN
#define htons(a) (a)
#define ntohs(a) (a)
#else
#define htons(a) ( (((a)>>8)&0xffU) + (((a)<<8)&0xff00U) )
#define ntohs(a) htons(a)
#endif

#endif  /* prevent multiple inclusion */


