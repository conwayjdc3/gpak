/*
 * Copyright (c) 2012, Adaptive Digital Technologies, Inc.
 *
 * File Name: TrilogyApi.h
 *
 * Description:
 *   This file contains Trilogy specific API functions to communicate with DSPs executing
 *   G.PAK software. 
 *
 */
#ifndef Trilogy_h

#define Trilogy_h

#include "adt_typedef.h"
#include "GpakEnum.h"
#include "GpakErrs.h"

#define Frame_40ms ((GpakFrameHalfMS)  80)
#define Frame_60ms ((GpakFrameHalfMS) 120)
#define Frame_80ms ((GpakFrameHalfMS) 160)

typedef enum preHdrStat_t {
   preHdrSuccess,
   preHdrInvalidChannel,  // G.PAK Channel number is invalid
   preHdrLengthErr,       // HdrI8 is greater than allowed maximum
   preHdrRxDataErr,       // High-order 2 bits of first byte of rxHdr data are 10b. 
   preHdrTxDataErr        // High-order 2 bits of first byte of txHdr data are 10b. 
} preHdrStat_t;

#define MSG_SLOT_RELEASE               248  // Pre-RTP header
#define MSG_SLOT_RELEASE_REPLY         249
#define MSG_PRE_RTP_HDR                250  // Pre-RTP header
#define MSG_PRE_RTP_HDR_REPLY          251
#define MSG_CONFIGURE_MCBSPPORTS       252  // Custom McBsp Config
#define MSG_CONFIGURE_MCBSPPORTS_REPLY 253
#define MSG_G722_RTP_STATS             254  // Read G722 RTP Stats
#define MSG_G722_RTP_STATS_REPLY       255


//{
//   N = number of bytes in pre-header
//   n = number of 16-bit words in pre-header (one byte padding if necessary)
//
//   Word:Bits
//
//     0:ff00    -  Msg ID
//     0:00ff    -  Channel ID
//     1:00ff    -  Pre-header length (bytes)
//
//     2:ff00    -  First byte rx
//     2:00ff    -  Second byte rx
//             .   .   .
//   n+2:ff00    -  Nth odd byte rx
//   n+2:00ff    -  Nth even byte rx or padding
//
//   n+3:ff00    -  First byte tx
//   n+3:00ff    -  Second byte tx
//             .   .   .
//  2n+2:ff00    -  Nth odd byte tx
//} 2n+2:00ff    -  Nth even byte tx or padding

typedef struct GpakMcBspPortConfig_t {
   GpakSlotCfg_t SlotsSelect1;         /* port 1 Slot selection */
   GpakActivation Port1Enable;
   ADT_Int32 Port1SlotMask0_31;
   ADT_Int32 Port1SlotMask32_63;
   ADT_Int32 Port1SlotMask64_95;
   ADT_Int32 Port1SlotMask96_127;

   GpakSlotCfg_t SlotsSelect2;         /* port 2 Slot selection */
   GpakActivation Port2Enable;
   ADT_Int32 Port2SlotMask0_31;
   ADT_Int32 Port2SlotMask32_63;
   ADT_Int32 Port2SlotMask64_95;
   ADT_Int32 Port2SlotMask96_127;
   
   GpakSlotCfg_t SlotsSelect3;         /* port 3 Slot selection */
   GpakActivation Port3Enable;
   ADT_Int32 Port3SlotMask0_31;
   ADT_Int32 Port3SlotMask32_63;
   ADT_Int32 Port3SlotMask64_95;
   ADT_Int32 Port3SlotMask96_127;

   ADT_Int16 TxDataDelay1, TxDataDelay2, TxDataDelay3;
   ADT_Int16 RxDataDelay1, RxDataDelay2, RxDataDelay3;
   ADT_Int16 ClkDiv1,      ClkDiv2,      ClkDiv3;
   ADT_Int16 FramePeriod1, FramePeriod2, FramePeriod3;
   ADT_Int16 FrameWidth1,  FrameWidth2,  FrameWidth3;
   GpakCompandModes Compand1,     Compand2,  Compand3;
   GpakActivation   SampRateGen1, SampRateGen2, SampRateGen3; 

   ADT_UInt16 txFrameSyncPolarity1, txFrameSyncPolarity2, txFrameSyncPolarity3;
   ADT_UInt16 rxFrameSyncPolarity1, rxFrameSyncPolarity2, rxFrameSyncPolarity3;
   ADT_UInt16 txClockPolarity1, txClockPolarity2, txClockPolarity3;
   ADT_UInt16 rxClockPolarity1, rxClockPolarity2, rxClockPolarity3;

}GpakMcBspPortConfig_t;

typedef struct G722RTPSync_t {
    ADT_UInt32 rtp_sr;
    ADT_UInt32 sr1_count;
    ADT_UInt32 Sequence;
    ADT_UInt32 TimeStamp;
    ADT_UInt32 BitFlds;
    ADT_UInt32 hdrI8Len;
    ADT_UInt32 PktI8;
    ADT_UInt32 pyldI8Len;
} G722RTPSync_t;

typedef struct G722RtpDebugStats_t {
    ADT_UInt32  hdrDiscards; 
    ADT_UInt32  payDiscards;
    ADT_UInt32  folDiscards;
    ADT_UInt32  cidDiscards;
    ADT_UInt32  inaDiscards;
    ADT_UInt32  invDiscards;
    ADT_UInt32  srtpDiscards;
    ADT_UInt32  sr0_count;
    ADT_UInt32  sr1_count;
    ADT_UInt32  sr8_adj_count;
    ADT_UInt32  sr8_nadj_count;
    ADT_UInt32  sr16_adj_count;
    ADT_UInt32  sr16_nadj_count;
    ADT_UInt32  discard_count;
    G722RTPSync_t g722Sync[2];
} G722RtpDebugStats_t;

typedef enum g722RtpStat_t {
   g722RtpStatSuccess,
   g722RtpStatChannelInactive,
   g722RtpStatInvalidChannel  // G.PAK Channel number is invalid
} g722RtpStat_t;

#ifndef __BIOS__
GpakApiStatus_t gpakPreRTPHeader (ADT_UInt32 DspId,  ADT_UInt16 ChanId, 
                                 ADT_UInt16 HdrI8, ADT_UInt8 *rxHdr, ADT_UInt8 *txHdr, 
                                 preHdrStat_t *dspStatus);
GpakApiStatus_t gpakConfigureMcBspPorts (
    ADT_UInt32 DspId,         /* DSP Id (0 to MaxDSPCores-1) */
    GpakMcBspPortConfig_t *PortCfg,      /* pointer to Port Config info */
    GPAK_PortConfigStat_t *Status   /* pointer to Port Config Status */
    );
GpakApiStatus_t gpakReadG722RTPStats (ADT_UInt32 DspId,  ADT_UInt16 ChanId, G722RtpDebugStats_t *g722RTPStats 
                                 g722RtpStat_t *dspStatus);
GpakApiStatus_t gpakReleaseTDMPortSlot (ADT_UInt32 DspId, 
                                 GpakSerialPort_t InPortId,  ADT_UInt16 InSlotId,
                                 GpakSerialPort_t OutPortId, ADT_UInt16 OutSlotId,
                                 ADT_UInt16 *dspStatus);

#endif
#endif
