// GpakApiTestSupport.c
//
//  Generic API example co-de
#ifdef BIOS
   #include <xdc/std.h>
   #include <stdlib.h>
#endif

#if defined(WIN32)
   #include <direct.h>
#else
   #include <unistd.h>
#endif

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#ifdef CORNET
   // Special because of CORNET's one-off on G.PAK API.
   #include "G:\V6_0_1\Cornet\Host\GpakApi.h"
#endif
#include "GpakApiTestSupport.h"
#include "sysconfig.h"

#define RTPEnableBits  0x00000018
#define DTMFEnableBit  0x10000000
#define VadEnableBit   0x40000000
#define AGCEnableBit   0x80000000

#define catEnum(b,bI8,e) case e: strcat_s (b, bI8, #e); break;
#define CORE_ID(chan) ((chan >> 8) & 0xFF)

#define ESC 'x'

static char *CHN_STR = "channel";
static char *CNF_STR = "conference";
extern char *chanType[] = { "ina", "T2P", "T2T", 
     "P2P", "CD", "T2C", "P2C", "CMP", "TRC", "M2C"
};
static char *EnabledStr [] = { "off", "on" };
static char *type2CIDString []= { "type 1", "type1&2" };
const char *dtmfString[16] = {
    "DTMF 1",
    "DTMF 2",
    "DTMF 3",
    "DTMF A",
    "DTMF 4",
    "DTMF 5",
    "DTMF 6",
    "DTMF B",
    "DTMF 7",
    "DTMF 8",
    "DTMF 9",
    "DTMF C",
    "DTMF *",
    "DTMF 0",
    "DTMF #",
    "DTMF D"};

extern char *prompt = NULL;
extern int rxMcastChan;
extern ADT_Int32  CustomDestIP;
extern int UseCustomDestIP;
extern ADT_UInt32 CustomSSRC;
extern ADT_UInt32 CustomDestSSRC;
extern int UseCustomSSRC;
extern int halfDuplex;// for tone relay debugging
extern int fixedCore;
extern void toneAlgControl(ADT_UInt32 DspId);

GpakSystemConfig_t sysCfg;
GpakSystemParms_t  sysParams;
GpakSRTPCfg_t      SRTPCfg;    // SRTP configuration parameters

ADT_UInt8 masterKey[] = {
   0x2B, 0x7E, 0x15, 0x16, 0x28, 0xAE, 0xD2, 0xA6, 
   0xAB, 0xF7, 0x15, 0x88, 0x09, 0xCF, 0x4F, 0x3C
};
ADT_UInt8 masterSalt[] = {
   0xF0, 0xF1, 0xF2, 0xF3, 0xF4, 0xF5, 0xF6, 0xF7, 
   0xF8, 0xF9, 0xFA, 0xFB, 0xFC, 0xFD
};
ADT_UInt8 mki[] = { '1', '2', '3', '4' };


#ifdef ADT_DEVELOPMENT
extern void turnOnFireHose  (int DspId);
extern void turnOffFireHose (int DspId);
#endif

//=========================================
//  Host stats
// Host statistics
typedef struct hostStats_t {
   int toDSPCnt,    toDSPDropped;
   int toNetCnt,    toNetDropped;
   int loopBackCnt, crossOverCnt;
   int DSPInterruptCnt;
} hostStats_t;
hostStats_t hostStats;



static RTPPkt rtpPkt;       //   Temporary RTP packet buffer
static int iteration = 0;   // Used to distribute channels across cores
static int framingCores, framingCoreOffset;
static ADT_Bool RTP_dump_all = ADT_TRUE;

typedef enum { SYS_NO_CHANGE, SYS_MIN, SYS_MAX, SYS_DEFAULT, SYS_PROMPT } SYS_MODIFY;
SYS_MODIFY sysParamsModify = SYS_NO_CHANGE;

ADT_Bool autoSetup = ADT_FALSE, autoUsage = ADT_FALSE;
ADT_Bool autoRTP   = ADT_FALSE, autoStats = ADT_FALSE;
netEnables_t NetEnables;

// Auto display parameters
int cyclesPerSetup    = 2500;
int cyclesSinceSetup  = 0;
int cyclesPerDisplay  = 250;
int cyclesSinceDisplay = 0;

void (*customDiagnostics)() = NULL;
void (*dumpCustomEvent) (Dsp, chId, EvtCodem, evt) = NULL;

static resetSsMask  resetStats;

// Size and address of record/playback buffers
int playBufI8;
DSP_Address playRecBuffers[5];
// 0 - Single segment recording
// 1 - File recording


#define PLAYBACK_HOST_MEMORY_I8  (16000 * 2 * 32)   // 32 seconds of i16 data at 16 kHz.
typedef enum { PR_IDLE, PR_RECORDING, PR_PLAYING } playRecordState;
struct fileRecording {
   playRecordState state;
   int chId, dev;
   ADT_UInt32 recI8;
   ADT_UInt32 playI8;
   ADT_UInt8  *hostAddr;
   ADT_UInt8  hostMemory [PLAYBACK_HOST_MEMORY_I8];
} playRecordInfo = { PR_IDLE };

// Channel setup for recording tests
chanDef chanDefRecord[]= {
{   {0,  0,   pcmToPacket},   LFT_TDM, NULL_TDM, { L16, Frame_10ms, L16, Frame_10ms, L16  }, ALL_DISABLES }, 
{   {1,  0,   pcmToPacket},   RGT_TDM, NULL_TDM, { L16, Frame_10ms, L16, Frame_10ms, L16  }, ALL_DISABLES }, 
LIST_END
};

#include <Windows.h>
#include <stdio.h>

char *months[] = {"January","February","March","April","May","June","July","August","September","October","November","December"};
char *AMPM[] = {"AM","PM"};
void showTime() {
int amPm = 0;
int hour, day, month, year;
int tz;

	SYSTEMTIME str_t;
	GetSystemTime(&str_t);

    hour  = str_t.wHour;
    day   = str_t.wDay;
    month = str_t.wMonth;
    year  = str_t.wYear;

    // timezone based on daylight savings
    switch (month) {
        case 11: case 12: case 1: case 2: case 3: 
        tz = 5;
        break;
        default:
        tz = 4;
    };

    // adjust GMT and handle wrap
    hour -= tz;
    if (hour < 0) {
        hour += 24;
        day--;
        if (day == 0) {
            month--;
            if (month == 0) {
                year--;
                month = 12;
            }
            switch (month) {
                case 1: case 3: case 5: case 7: case 8: case 10: case 12:
                day = 31;
                break;
                case 2: 
                day = 28;
                break;
                default:
                day = 30;
            };
        }
    }

    if (hour >= 12) {
        amPm = 1;
        hour = hour - 12;
        if (hour == 0) hour = 12;
    }
    printf("\n%02d:%02d:%02d %s   %d %s, %d\n",hour, str_t.wMinute, str_t.wSecond, AMPM[amPm], day, months[month-1],year);
}
//=================================================
//{   General purpose routines.  Channel to  network address routines
//
static union {
   ADT_UInt32  I32;
   ADT_UInt8   I8[4];
} IPAdd;

static char IPAddrBuff [16];
char *IPToStr (ADT_Int32 IPAddr) {
   char numStr[40];
   int i;

   if (IPAddr == dspIP) return "DSP";

   IPAdd.I32 = IPAddr;
   memset (IPAddrBuff, 0, sizeof (IPAddrBuff));
   for (i=0; i<3; i++) {
      snprintf (numStr, sizeof (numStr), "%u", (unsigned int) IPAdd.I8[3-i]);
      strcat_s (IPAddrBuff, sizeof (IPAddrBuff), numStr);
      strcat_s (IPAddrBuff, sizeof (IPAddrBuff), ".");
   }
   snprintf (numStr, sizeof (numStr), "%u", (unsigned int) IPAdd.I8[0]);
   strcat_s (IPAddrBuff, sizeof (IPAddrBuff), numStr);
   return IPAddrBuff;
}

// SSRC for packets originating at DSP
#define ssrcFromChan(chID)    (BaseSSRC + (chID))

// SSRC for packets originating at remote soruce
ADT_UInt32 dstSSRCFromChan (ADT_UInt16 chID) {
   if (NetEnables.fireHoseEnable) return 0;
   if (NetEnables.PortCrossover)  return (BaseSSRC + (chID ^ 1));
   return (BaseSSRC + chID);
}

// Local device's UDP port
ADT_UInt16 srcPortFromChan (ADT_UInt16 chID) {
   if (NetEnables.fireHoseEnable) return 6334 + chID;
   return 6334 + (chID * 2);
}

// Remote device's UDP port
ADT_UInt16 dstPortFromChan (ADT_UInt16 chID) {
   if (NetEnables.fireHoseEnable) return 6334;
   if (NetEnables.PortCrossover)  return  srcPortFromChan (chID ^ 1);
   return 6334 + (chID * 2);
}

// Remote device's IP address on in-bound packets
ADT_Int32  dstIPFromChan (ADT_UInt16 chID) {
   if (NetEnables.fireHoseEnable) {
#ifdef ADT_DEVELOPMENT
      if (FirehoseIP == 0) turnOffFireHose (0);
#endif
      return FirehoseIP;
   }
   if (NetEnables.PortCrossover) return dspIP;
   if (UseCustomDestIP)           return  CustomDestIP;
   return BaseDestIP + chID;
}

// Remote device's IP address on out-bound packets (for multicast transmits)
ADT_Int32  dstTxIPFromChan (ADT_UInt16 chID) {
   if (NetEnables.multicastTx)    return BaseMCastIP + chID;
   if (NetEnables.PortCrossover)  return dspIP;
   if (UseCustomDestIP)           return  CustomDestIP;

   return BaseDestIP;
}

// Destination IP of in-bound packets (For multicast receives)
ADT_Int32 lclDstIPFromChan (ADT_UInt16 chID) {
   if (NetEnables.multicastRx) return (BaseMCastIP + rxMcastChan);
   return 0;
}

char *txIPFromChan (ADT_UInt16 chID) {
   ADT_Int32 IPAdd;

   IPAdd = dstTxIPFromChan (chID);
   return IPToStr (IPAdd);
}
char *rxIPFromChan (ADT_UInt16 chID) {
   ADT_Int32 IPAdd;
   IPAdd = lclDstIPFromChan (chID);
   if (IPAdd == 0) return "DSP";
   return IPToStr (IPAdd);
}

#define vlanFromChan(chID)    (NetEnables.vlan ? (chID%10) : 0xff)

int  ADT_UpdateInt16Value (char *name, ADT_Int16 *value) {
   char string [20];

   ADT_printf ("\n%s (%5d): ", name, *value);
   ADT_gets (string, sizeof (string));
   if (string[0] == 0) return 0;
   if (string[0] == '\n') return 0;
   if (string[0] == '\r') return 0;

   *value = atoi (string);
   return 1;
}

GpakFrameHalfMS getCodecHalfMS (GpakCodecs Coding, GpakFrameHalfMS FrameHalfMS) {

   switch (Coding) {
   case G729:
   case G729AB:   
      if ((FrameHalfMS % Frame_10ms) != 0) return Frame_10ms;
      break;

   case AMR_475:    case AMR_515:    case AMR_590:
   case AMR_670:    case AMR_740:    case AMR_795:
   case AMR_1020:   case AMR_1220:
   case Speex:      case SpeexWB:
      if (FrameHalfMS != Frame_20ms) ADT_printf ("\nChanging rate to 20 milliseconds\n");
      return Frame_20ms;

   case MELP:       case MELP_E:   return Frame_22_5ms;
   
   case G723_63:    case G723_63A:
   case G723_53:    case G723_53A:
   case ADT_4800: 
      if (FrameHalfMS != Frame_30ms) ADT_printf ("\nChanging rate to 30 milliseconds\n");
      return Frame_30ms;
   }
   return FrameHalfMS;
}

GpakCodecs getCodecType (char **C) {
   GpakCodecs codec;
   char *c = *C;

   switch (*c++) {
   case 's': codec = Speex;   break;
   case 'S': codec = SpeexWB; break;
   case 'm': codec = MELP;    break;
   case 'M': codec = MELP_E;  break;
   case 'u': codec = PCMU_64; break;
   case 'U': codec = PCMU_WB; break;
   case 'a': codec = PCMA_64; break;
   case 'A': codec = PCMA_WB; break;

   case 'l':
      if (*c++ == 'w')  codec = L8_WB;
      else              codec = L8;
      break;

   case 'L':
      if (*c++ == 'w')  codec = L16_WB;
      else              codec = L16;
      break;


   case '7':
      if (*c++ == 'a')  codec = G723_53A;
      else              codec = G723_63;
      break;

   case '9':
      if (*c++ == 'a')  codec = G729AB;
      else              codec = G729;
      break;


   default:
      codec = PCMA_64;
      break;
   }
   *C = c;
   return codec;
}
//}


//=================================================
//{   DSP-host Packet transfers
//
ADT_Bool DSPPerformsRTP (ADT_UInt32 Dsp) {
   return sysCfg.CfgEnableFlags & RTPEnableBits;
}

void loopbackRTPPackets (ADT_UInt32 Dsp) {
   ADT_UInt16 chanId, PktI8;
   GpakApiStatus_t pktRcvStat;
   GpakApiStatus_t pktSndStat;

   // Perform loopback or crossover on RTP packets
   do {
      PktI8 = sizeof (rtpPkt);
      pktRcvStat =  gpakGetPacketFromDsp (Dsp, &chanId, (ADT_UInt8 *) &rtpPkt, &PktI8);
      if (pktRcvStat == GpakApiNoPayload) break;
      else if (pktRcvStat != GpakApiSuccess) {
         ADT_printf ("GetPktFromDSP error %d\n", pktRcvStat);
      }
      hostStats.toNetCnt++;

      if (hostLoopBack==LOOPBACK) {
         pktSndStat = gpakSendPacketToDsp (Dsp, chanId, (ADT_UInt8 *) &rtpPkt, PktI8);
         hostStats.loopBackCnt++;
      } else if (hostLoopBack==CROSSOVER) {
         chanId ^= 1;
         pktSndStat = gpakSendPacketToDsp (Dsp, chanId, (ADT_UInt8 *) &rtpPkt, PktI8);
         hostStats.crossOverCnt++;
      } else {
         pktSndStat = GpakApiNotConfigured;
      }
      if (pktSndStat == SpsSuccess)      hostStats.toDSPCnt++;
      else if (pktSndStat != SpsSuccess) hostStats.toDSPDropped++;
   } while (pktRcvStat == GpakApiSuccess);

}

void loopbackRTPPayloads (ADT_UInt32 Dsp) {
   ADT_UInt16 chID, chanId, PktI8;

   GpakApiStatus_t  pktRcvStat;
   GpakApiStatus_t  pktSndStat;
   GpakPayloadClass PayloadClass;
   ADT_Int16        PayloadType;
   ADT_UInt32       TimeStamp;

   // Loop reading all channels
   for (chID=0; chID<sysCfg.MaxChannels; chID++) {
      // Perform loopback or crossover until all payloads for a channel are processed
      do {
         PktI8 = sizeof (rtpPkt);
         pktRcvStat =  gpakGetPayloadFromDsp (Dsp, chID, &PayloadClass, &PayloadType,  
                                              (ADT_UInt8 *) &rtpPkt, &PktI8, &TimeStamp);
         if (pktRcvStat == GpakApiNoPayload) break;
         else if (pktRcvStat != GpakApiSuccess) {
            ADT_printf ("GetPktFromDSP error %d\n", pktRcvStat);
            break;
         }
         hostStats.toNetCnt++;

         if (hostLoopBack==LOOPBACK) {
            chanId = chID;
            hostStats.loopBackCnt++;
         } else if (hostLoopBack==CROSSOVER) {
            chanId = chID ^ 1;
            hostStats.crossOverCnt++;
         } else {
            hostStats.toDSPDropped++;
            continue;
         }
         pktSndStat = gpakSendPayloadToDsp (Dsp, chanId, PayloadClass, PayloadType,
                                              (ADT_UInt8 *) &rtpPkt, PktI8);
         if (pktSndStat == SpsSuccess)      hostStats.toDSPCnt++;
         else if (pktSndStat != SpsSuccess) hostStats.toDSPDropped++;
      } while (pktRcvStat == GpakApiSuccess);
   }
}

void processDSPInterrupt () {
   hostStats.DSPInterruptCnt++;
}

void displayNetworkStats (void) {

   ADT_printf ("\n\nPackets sent/dropped to net = %5d/%5d\n", hostStats.toNetCnt, hostStats.toNetDropped);
   ADT_printf (    "Packets sent/dropped to DSP = %5d/%5d\n", hostStats.toDSPCnt, hostStats.toDSPDropped);
   ADT_printf (    "Packets loopback/crossover  = %5d/%5d\n", hostStats.loopBackCnt, hostStats.crossOverCnt);
   ADT_printf (    "Interrupts received         = %5d\n", hostStats.DSPInterruptCnt);
}


//}

//=================================================
//   G.PAK API support
//
//------------------------------------------------------------

//------------------------------------------------------------
//{ File record and playback APIs
static void fileRecord (ADT_UInt32 Dsp, GpakAsyncEventCode_t EvtCode, ADT_UInt16 chId, int dev, GpakAsyncEventData_t *evt) {
   GpakApiStatus_t          ApiStat;
   GPAK_PlayRecordStat_t    playRecDspStat;

   ADT_UInt32 RecordI8;   // Length of recording buffer or final
   ADT_UInt32 DspAddr;    // Address of recording buffer

   if (playRecordInfo.state == PR_PLAYING) return;

   if (playRecordInfo.state == PR_IDLE) {
      playRecordInfo.hostAddr =  playRecordInfo.hostMemory;
 
      playRecordInfo.recI8  = 0;
      playRecordInfo.playI8 = 0;
      playRecordInfo.state = PR_RECORDING;
      playRecordInfo.chId = chId;
      playRecordInfo.dev  = dev;
   } else if ((playRecordInfo.chId != chId) || (playRecordInfo.dev != dev))
      return;

   RecordI8 = evt->aux.recordEvent.RecordLength;
   DspAddr  = evt->aux.recordEvent.RecordAddr;
   switch (EvtCode) {
   case EventRecordingAtEnd:
      DspAddr += RecordI8/2;
      RecordI8 /= 2;
      break;

   case EventRecordingHalfWay:
      RecordI8 /= 2;
      break;

   case EventRecordingStopped:
      ADT_printf ("\n\rRecording stopped on channel %d.%c\t", chId, dev + 'A');
      playRecordInfo.state = PR_IDLE;
      break;
   }

   // Automatic stop on recording file overflow.
   if (PLAYBACK_HOST_MEMORY_I8 <= (playRecordInfo.recI8 + RecordI8)) {
      ADT_printf ("\n\rRecording file overflow %d + %d\t", playRecordInfo.recI8, RecordI8);
      ApiStat = gpakSendPlayRecordMsg (Dsp, chId, (GpakDeviceSide_t) dev, StopRecording, (DSP_Address) NULL, 0,
                                       RecordingL16, tdsNoToneDetected, &playRecDspStat);
      if (ApiStat != GpakApiSuccess)  {
         ADT_printf ("\n\rRecord autostop failure = %d.%d.%d\t", ApiStat, playRecDspStat, DSPError[Dsp]);
      }
      RecordI8 = PLAYBACK_HOST_MEMORY_I8 - playRecordInfo.recI8;
      if (RecordI8 == 0) return;
   }
   gpakReadDspMemory8 (Dsp, (DSP_Address) DspAddr, RecordI8, playRecordInfo.hostAddr);
   ADT_printf ("\n\rReading %x bytes from %p to %p\t", RecordI8, DspAddr, playRecordInfo.hostAddr);
   playRecordInfo.hostAddr += RecordI8;
   playRecordInfo.recI8    += RecordI8;
   return;  
}
static void filePlayback (ADT_UInt32 Dsp, GpakAsyncEventCode_t EvtCode, ADT_UInt16 chId, int dev, GpakAsyncEventData_t *evt) {
   GpakApiStatus_t          ApiStat;
   GPAK_PlayRecordStat_t    DspStat;
   ADT_UInt32 PlaybackI8; // Length of recording buffer or final
   ADT_UInt32 DspAddr;    // Address of recording buffer

   if ((playRecordInfo.chId != chId) || (playRecordInfo.dev != dev)) {
      if (EvtCode == EventPlaybackComplete)
         ADT_printf ("\n\rPlayback complete on channel %d.%c\t", chId, dev + 'A');
      return;
   }
   if (playRecordInfo.state != PR_PLAYING)
      return;

   PlaybackI8 = evt->aux.recordEvent.RecordLength;
   DspAddr    = evt->aux.recordEvent.RecordAddr;
   switch (EvtCode) {
   case EventPlaybackComplete:
      ADT_printf ("\n\rPlayback completion on channel %d.%d\t", chId, dev);
      playRecordInfo.state = PR_IDLE;
      return;

   case EventPlaybackAtEnd:
      DspAddr += PlaybackI8/2;
      PlaybackI8 /= 2;
      break;

   case EventPlaybackHalfWay:
      PlaybackI8 /= 2;
      break;

   }
   
   // At end of file. Issue stop playback.
   if (playRecordInfo.recI8 <= (playRecordInfo.playI8 + PlaybackI8)) {
      PlaybackI8 = playRecordInfo.recI8 - playRecordInfo.playI8;
      ApiStat = gpakSendPlayRecordMsg (Dsp, chId, (GpakDeviceSide_t) dev, StopPlayBack, (DSP_Address) NULL, 0,
                                       RecordingL16, tdsNoToneDetected, &DspStat);
      if (ApiStat != GpakApiSuccess)  {
         ADT_printf ("\n\rAutoPlayback stop failure %d.%d.%d\t", ApiStat, DspStat, DSPError[Dsp]);
      }
   }
   gpakWriteDspMemory8 (Dsp, (DSP_Address) DspAddr, PlaybackI8, playRecordInfo.hostAddr);
   ADT_printf ("\n\rWriting %x bytes from %p to %p\t", PlaybackI8, playRecordInfo.hostAddr, DspAddr);
   playRecordInfo.hostAddr += PlaybackI8;
   playRecordInfo.playI8   += PlaybackI8;
   return;  
}

void readPlaybackAddress (ADT_Int32 DspId) {
   GpakTestData_t  TestParm;
   GPAK_TestStat_t TestStatus;
   GpakApiStatus_t ApiStatus;

   ADT_UInt16 RespData[10];

   // Divide play record buffer segment into five recording buffers 
   ApiStatus = gpakTestMode (DspId, ReadPlayRecAddr, &TestParm, &RespData[0], &TestStatus);
   if (ApiStatus != GpakApiSuccess) {
      ADT_printf ("\n\rReadPlayRecAddr failure %d:%d:%d\t", ApiStatus, TestStatus, DSPError [DspId]);
      PlayRecAddress = (DSP_Address) -1;
   } else {
      PlayRecAddress = (RespData[0] << 16) | RespData[1];
      ADT_printf ("\n\rPlayRecAddr= %x\t", PlayRecAddress);
   }
}

// Prime DSP's file playback buffer.
void startFilePlayback (ADT_UInt32 Dsp, ADT_UInt16 chId, int dev, int bufrID, unsigned int buffI8) {
   DSP_Address DspAddr;

   if (playRecordInfo.state != PR_IDLE) {
      ADT_printf ("\n\rFile playback buffer in use\t");
      return;
   }

   if (playRecordInfo.recI8 == 0) return;

   if (playRecordInfo.recI8 < buffI8)
      buffI8 = playRecordInfo.recI8;

   // Skip past DSP's header.
   DspAddr = playRecBuffers[bufrID] + 8;
   playRecordInfo.hostAddr =  playRecordInfo.hostMemory;
   playRecordInfo.playI8 = buffI8;
   playRecordInfo.chId = chId;
   playRecordInfo.dev  = dev;
   playRecordInfo.state = PR_PLAYING;
   gpakWriteDspMemory8 (Dsp, DspAddr, buffI8, playRecordInfo.hostAddr);
   ADT_printf ("\n\rWriting %x bytes from %p to %p\t", buffI8, playRecordInfo.hostAddr, DspAddr);
   playRecordInfo.hostAddr += buffI8;
}

char recordVoice (ADT_UInt32 DspId, char *instructions, int bufrID, ADT_UInt16 chID, GpakDeviceSide_t device, 
                  GpakPlayRecordCmd_t mode, GpakToneCodes_t stopDigit) {
   GpakApiStatus_t          ApiStat;
   GPAK_PlayRecordStat_t    DspStat = PrSuccess;
   char rtn = 0;
   ADT_UInt16 chanID;

   // chID -1  -->  Setup PCM to PCM for recording
   if (chID == NULL_CHAN) {
      dspTearDown (DspId);
      setupChannelsViaTable (DspId, chanDefRecord, Null_tone);
      chanID = 0;
   } else {
      chanID = chID;
   }

   ApiStat = gpakSendPlayRecordMsg (DspId, chanID, device, mode,  playRecBuffers[bufrID], playBufI8, 
                                    RecordingL16, stopDigit, &DspStat);

   if ((ApiStat != GpakApiSuccess) || (DspStat != PrSuccess)) {
      ADT_printf ("\n\rStart record failure = %d.%d.%d\t", ApiStat, DspStat, DSPError[DspId]);
      rtn = ESC;
   }
   if (chID == NULL_CHAN) {
      ADT_printf ("\n\rRecord %s for up to %d seconds\nAny key when complete\n", instructions, playBufI8/16000);
      ADT_getchar ();
      dspTearDown (DspId);
   }
   return rtn;
}

void playbackVoice (ADT_UInt32 DspId, int bufrID, ADT_UInt16 chID, GpakDeviceSide_t device, GpakPlayRecordCmd_t mode) {
   GpakApiStatus_t          ApiStat;
   GPAK_PlayRecordStat_t    DspStat =  PrSuccess;

   ApiStat = gpakSendPlayRecordMsg (DspId, chID, device, mode, playRecBuffers[bufrID], playBufI8, 
                                    RecordingL16, tdsNoToneDetected, &DspStat);

   if ((ApiStat != GpakApiSuccess) || (DspStat != PrSuccess))  
      ADT_printf ("Start playback failure = %d.%d.%d\n", ApiStat, DspStat, DSPError[DspId]);
}

void playRecordTest (ADT_UInt32 DspId, char playRec) {
   char *oldPrompt;
   char inputBuffer [20];
   int  bufrID;
   ADT_UInt16 chanId;
   ADT_UInt32 seconds;
   GpakDeviceSide_t      Device;
   GpakPlayRecordCmd_t   Cmd;
   GPAK_PlayRecordStat_t dspStat;
   GpakApiStatus_t       apiStat;
   GpakToneCodes_t       tone;

   ADT_printf ("\n\nPlay/record test. Enter channel id: ");
   ADT_gets (inputBuffer, sizeof (inputBuffer));
   chanId = atoi (&inputBuffer[0]);

   ADT_printf ("\n\rDevice[a/b/d/n]: ");
   tone = tdsNoToneDetected;
   switch (ADT_getchar ()) {
   default:
      Device = ADevice;
      if (ActiveChan.TONEA) tone = tdsDtmfDigit1;
      break;

   case 'b':
      Device = BDevice;
      if (ActiveChan.TONEB) tone = tdsDtmfDigit1;
      break;

   case 'd': Device = DSPToNet;  break;
   case 'n': Device = NetToDSP;  break;
   }

   bufrID = 0;

   switch (playRec) {
   default:  Cmd = StartPlayBack;            break;
   case 'p':
      ADT_printf ("\nCmd [s=start e=end f=file l=loop]: ");
      switch (ADT_getchar ()) {
      default:  Cmd = StartPlayBack;            break;
      case 'l': Cmd = StartContinuousPlayBack;  break;
      case 'e': Cmd = StopPlayBack;             break;
      case 'f':
         bufrID = 1;
         Cmd = StartFilePlayBack;
         break;
      }
      break;

   case 'r':
      ADT_printf ("\nCmd [s=start e=end f=file]: ");
      switch (ADT_getchar ()) {
      default:  Cmd = StartRecording;      break;
      case 'e': Cmd = StopRecording;       break;
      case 'f':
         bufrID = 1;
         Cmd = StartFileRecording;
         break;
      }
   }

   seconds = ADT_getint ("Seconds: ");

   if (Cmd == StartFilePlayBack)
      startFilePlayback (DspId, chanId, Device, bufrID, seconds * 16000) ;

   apiStat = gpakSendPlayRecordMsg (DspId, chanId, Device, Cmd, playRecBuffers[bufrID],
                                    seconds * 16000, RecordingL16, tone, &dspStat);
   if (apiStat != GpakApiSuccess) {
      ADT_printf ("Play/record failure %d.%d.%d\n", apiStat, dspStat, DSPError[DspId]);
      return;
   }
   if ((Cmd != StartFileRecording) && (Cmd != StartFilePlayBack) && (Cmd != StartContinuousPlayBack)) return;

   
   ADT_printf ("\nCR to stop\n");
   oldPrompt = prompt;
   prompt = "";
   while (ADT_getchar () != '\r');

   if (Cmd == StartFileRecording) Cmd = StopRecording;
   else                           Cmd = StopPlayBack;
   apiStat = gpakSendPlayRecordMsg (DspId, chanId, Device, Cmd, playRecBuffers[bufrID],
                                    seconds * 16000, RecordingL16, tone, &dspStat);
   prompt = oldPrompt;
}

//}

//------------------------------------------------------------
//{ Channel algorithm tests
void toneGenRequest (ADT_UInt32 DspId) {
   char inputBuffer[80];
   
   ADT_UInt16 ChannelId;
   GpakToneGenParms_t   Tone;
   GPAK_ToneGenStat_t   DspStat;

   GpakChannelStatus_t      ChanStatus;
   GPAK_ChannelStatusStat_t DspStatus;

   GpakApiStatus_t  ApiStat;

   char *Destination;
   int i, CoreId;

   GpakApiStatus_t (*toneGen)(ADT_UInt32, ADT_UInt16, GpakToneGenParms_t *, GPAK_ToneGenStat_t *);
   
   ADT_printf ("\nTone test. Enter channel[+] or conference[-] id: ");
   ADT_gets (inputBuffer, sizeof (inputBuffer));
   if (inputBuffer[0] == '-') {
      ChannelId = atoi (&inputBuffer[1]);
      toneGen = gpakConferenceGenerateTone;
      ADT_printf ("\nEnter core id: ");
      ADT_gets (inputBuffer, sizeof (inputBuffer));
      CoreId = atoi (&inputBuffer[0]);
      ChannelId |= CoreId << 8;
      Destination = CNF_STR;
   } else {
      toneGen = gpakToneGenerate;
      ChannelId = atoi (&inputBuffer[0]);
      Destination = CHN_STR;
   }

   DspStatus = Cs_Success;
   ApiStat = gpakGetChannelStatus (DspId, ChannelId, &ChanStatus, &DspStatus);
   if ((ApiStat != GpakApiSuccess) || (DspStatus != Cc_Success)) {
      ADT_printf ("API ERROR %d.%d.%d\n", ApiStat, DspStatus, DSPError[DspId]);
      return;
   }
   if (ChanStatus.ChannelType == inactive) return;


   //  Tone testing side A.
   //{   burst tone of 100 milliseconds
   ADT_printf ("\n1100 millisecond burst (1 freq) A-side ... ");
   memset (&Tone, 0, sizeof (Tone));
   Tone.ToneCmd  = ToneGenStart;
   Tone.ToneType = TgBurst; 
   Tone.Device   = ADevice;

   Tone.Level[0] = -5;
   Tone.Frequency[0] = 500;
   Tone.OnDuration[0] = 1100;

   ApiStat = (toneGen) (DspId, ChannelId, &Tone, &DspStat);
   if ((ApiStat != GpakApiSuccess) || (DspStat != Tg_Success)) {
      ADT_printf ("\nDSP %d %s %3d tone generation failure %d:%d:%d\n\n", 
          DspId, Destination, ChannelId, ApiStat, DspStat, DSPError [DspId]);
   }
   // Wait until tone has a chance to end
   for (i=0; i < 250; i++)  gpakHostDelay();

   // Generate a cadence tone on side A
   ADT_printf ("\nCadence (A) start (4 freq) ... ");
   Tone.ToneCmd  = ToneGenStart;
   Tone.ToneType = TgCadence; 
   Tone.Device   = ADevice;

   Tone.Level[0] = -5;         Tone.Level[1] = -8;         Tone.Level[2] = -11;        Tone.Level[3] = -13;
   Tone.Frequency[0] = 400;    Tone.Frequency[1] = 800;    Tone.Frequency[2] = 1600;   Tone.Frequency[3] = 3200;
   Tone.OnDuration[0] = 1100;  Tone.OnDuration[1] = 1080;  Tone.OnDuration[2] = 1060;  Tone.OnDuration[3] = 1040;
   Tone.OffDuration[0] = 1040; Tone.OffDuration[1] = 1060; Tone.OffDuration[2] = 1080; Tone.OffDuration[3] = 1000;

   ApiStat = (toneGen) (DspId, ChannelId, &Tone, &DspStat);
   if ((ApiStat != GpakApiSuccess) || (DspStat != Tg_Success)) {
      ADT_printf ("\nDSP %d %s %3d tone generation failure %d:%d:%d\n\n",
                DspId, Destination, ChannelId, ApiStat, DspStat, DSPError [DspId]);
   }
   // Wait for a while
   for (i=0; i < 500; i++) gpakHostDelay();


   // Stop tone
   ADT_printf ("Cadence end\n");
   Tone.ToneCmd  = ToneGenStop;
   Tone.Device   = ADevice;

   ApiStat = (toneGen) (DspId, ChannelId, &Tone, &DspStat);
   if ((ApiStat != GpakApiSuccess) || (DspStat != Tg_Success)) {
      ADT_printf ("\nDSP %d %s %3d tone generation failure %d:%d:%d\n\n", 
           DspId, Destination, ChannelId, ApiStat, DspStat, DSPError [DspId]);
   }

   if (ChanStatus.ChannelType != pcmToPcm) return;

   //}-----------------------------------------------------------------
   //{  Tone testing side B
   memset (&Tone, 0, sizeof (Tone));

   ADT_printf ("\n1100 millisecond burst (1 freq) B-side ... ");
   Tone.ToneCmd  = ToneGenStart;
   Tone.ToneType = TgBurst; 
   Tone.Device = BDevice;

   Tone.Level[0]      = -5;
   Tone.OnDuration[0] = 1100;
   Tone.Frequency[0]  = 1000;

   ApiStat = (toneGen) (DspId, ChannelId, &Tone, &DspStat);
   if ((ApiStat != GpakApiSuccess) || (DspStat != Tg_Success)) {
      ADT_printf ("\nDSP %d %s %3d tone generation failure %d:%d:%d\n\n", 
          DspId, Destination, ChannelId, ApiStat, DspStat, DSPError [DspId]);
   }

   // Wait until tone has a chance to end
   for (i=0; i < 250; i++) gpakHostDelay();
   
   Tone.Level[0] = -5;         Tone.Level[1] = -8;         Tone.Level[2] = -11;        Tone.Level[3] = -13;
   Tone.Frequency[0] = 400;    Tone.Frequency[1] = 800;    Tone.Frequency[2] = 1600;   Tone.Frequency[3] = 3200;
   Tone.OnDuration[0] = 1100;  Tone.OnDuration[1] = 1080;  Tone.OnDuration[2] = 1060;  Tone.OnDuration[3] = 1040;
   Tone.OffDuration[0] = 1040; Tone.OffDuration[1] = 1060; Tone.OffDuration[2] = 1080; Tone.OffDuration[3] = 1000;

   // Generate a continuous tone on side B
   ADT_printf ("\nContinuous (B) start (4 freq) ... ");
   Tone.ToneCmd  = ToneGenStart;
   Tone.ToneType = TgContinuous;
   Tone.Device   = BDevice;

   ApiStat = (toneGen) (DspId, ChannelId, &Tone, &DspStat);
   if ((ApiStat != GpakApiSuccess) || (DspStat != Tg_Success)) {
      ADT_printf ("\nDSP %d %s %3d tone generation failure %d:%d:%d\n\n", 
            DspId, Destination, ChannelId, ApiStat, DspStat, DSPError [DspId]);
   }

   // Wait for a while
   for (i=0; i < 500; i++) gpakHostDelay();

   // Stop tone
   Tone.ToneCmd  = ToneGenStop;
   Tone.Device   = BDevice;
   ApiStat = (toneGen) (DspId, ChannelId, &Tone, &DspStat);
   if ((ApiStat != GpakApiSuccess) || (DspStat != Tg_Success)) {
      ADT_printf ("\nDSP %d %s %3d tone generation failure %d:%d:%d\n\n", 
           DspId, Destination, ChannelId, ApiStat, DspStat, DSPError [DspId]);
   }

   //}
}

void toneGenDtmfString (ADT_UInt32 DspId) {
   char inputBuffer[80];
   ADT_UInt16               ChannelId;
   GpakToneGenParms_t       Tone;
   GPAK_ToneGenStat_t       DspStat;
   GpakApiStatus_t          ApiStat;
   int i, dir;

   ADT_printf ("\nEnter channel id: ");
   ADT_gets (inputBuffer, sizeof (inputBuffer));
   ChannelId = atoi (&inputBuffer[0]);

   ADT_printf ("\nTDM-Side (0) or PktSide(1): ");
   ADT_gets (inputBuffer, sizeof (inputBuffer));
   dir = atoi (&inputBuffer[0]);
   

   ADT_printf ("\nGenerating DTMF string 1234 ");
   memset (&Tone, 0, sizeof (Tone));
   Tone.ToneCmd  = ToneGenStart;
   Tone.ToneType = TgBurst; 
   if (dir == 0)
        Tone.Device   = ADevice;
   else
        Tone.Device   = BDevice;

   Tone.Frequency[0]   = 697;      // 697,1209 is a DTMF '1'
   Tone.Level[0]       = -15;
   Tone.OnDuration[0]  = 100;
   Tone.OffDuration[0] = 100;
   Tone.Frequency[1]   = 1209;
   Tone.Level[1]       = -13;
//   Tone.OnDuration[1]  = 100;
//   Tone.OffDuration[1] = 100;
   ApiStat = gpakToneGenerate (DspId, ChannelId, &Tone, &DspStat);
   if ((ApiStat != GpakApiSuccess) || (DspStat != Tg_Success)) {
      ADT_printf ("\nDSP %d, Channel %3d tone generation failure %d:%d:%d\n\n", 
          DspId, ChannelId, ApiStat, DspStat, DSPError [DspId]);
   }

   // Wait until tone has a chance to end
   for (i=0; i < 250; i++)  gpakHostDelay();

   Tone.Frequency[0]   = 697;      // 697, 1336 is a DTMF '2'
   Tone.Level[0]       = -15;
   Tone.OnDuration[0]  = 100;
   Tone.OffDuration[0] = 100;
   Tone.Frequency[1]   = 1336;
   Tone.Level[1]       = -13;
//   Tone.OnDuration[1]  = 100;
//   Tone.OffDuration[1] = 100;
   ApiStat = gpakToneGenerate (DspId, ChannelId, &Tone, &DspStat);
   if ((ApiStat != GpakApiSuccess) || (DspStat != Tg_Success)) {
      ADT_printf ("\nDSP %d, Channel %3d tone generation failure %d:%d:%d\n\n", 
          DspId, ChannelId, ApiStat, DspStat, DSPError [DspId]);
   }
   
   // Wait until tone has a chance to end
   for (i=0; i < 250; i++)  gpakHostDelay();

   Tone.Frequency[0]   = 697;      // 697, 1477 is a DTMF '3'
   Tone.Level[0]       = -15;
   Tone.OnDuration[0]  = 100;
   Tone.OffDuration[0] = 100;
   Tone.Frequency[1]   = 1477;
   Tone.Level[1]       = -13;
//   Tone.OnDuration[1]  = 100;
//   Tone.OffDuration[1] = 100;
   ApiStat = gpakToneGenerate (DspId, ChannelId, &Tone, &DspStat);
   if ((ApiStat != GpakApiSuccess) || (DspStat != Tg_Success)) {
      ADT_printf ("\nDSP %d, Channel %3d tone generation failure %d:%d:%d\n\n", 
          DspId, ChannelId, ApiStat, DspStat, DSPError [DspId]);
   }
   
   // Wait until tone has a chance to end
   for (i=0; i < 250; i++)  gpakHostDelay();

   Tone.Frequency[0]   = 770;      // 770,1209 is a DTMF '4'
   Tone.Level[0]       = -15;
   Tone.OnDuration[0]  = 100;
   Tone.OffDuration[0] = 100;
   Tone.Frequency[1]   = 1209;
   Tone.Level[1]       = -13;
//   Tone.OnDuration[1]  = 100;
//   Tone.OffDuration[1] = 100;
   ApiStat = gpakToneGenerate (DspId, ChannelId, &Tone, &DspStat);
   if ((ApiStat != GpakApiSuccess) || (DspStat != Tg_Success)) {
      ADT_printf ("\nDSP %d, Channel %3d tone generation failure %d:%d:%d\n\n", 
          DspId, ChannelId, ApiStat, DspStat, DSPError [DspId]);
   }
}


void toneInsert (ADT_UInt32 DspId) {
   char inputBuffer [20];
   char tonePyld[4];
   ADT_UInt16 chanId;
   GpakApiStatus_t        apiStat;
   GPAK_InsertEventStat_t dspStat;
   int digit, volume, duration;
   
   ADT_printf ("\nTone insertion test. Enter channel id: ");
   ADT_gets (inputBuffer, sizeof (inputBuffer));
   chanId = atoi (&inputBuffer[0]);

   // Set defaults
   digit = 1;
   volume = 4;
   duration = 8000;

   ADT_printf ("\nDigit: ");
   ADT_gets (inputBuffer, sizeof (inputBuffer));
   if (strlen (inputBuffer)) digit = atoi (&inputBuffer[0]);

   ADT_printf ("\nVolume(-dBm): ");
   ADT_gets (inputBuffer, sizeof (inputBuffer));
   if (strlen (inputBuffer)) volume = atoi (&inputBuffer[0]);

   ADT_printf ("\nTone duration(ms): ");
   ADT_gets (inputBuffer, sizeof (inputBuffer));
   if (strlen (inputBuffer)) duration = atoi (&inputBuffer[0]) * 8;
   ADT_printf ("\n");

   tonePyld [0] = (char) digit;
   tonePyld [1] = (char) (0x80 | volume);
   tonePyld [2] = (char) (duration >> 8);
   tonePyld [3] = (char) (duration & 0xff);
   apiStat = gpakRTPInsertTone (DspId, chanId, sizeof (tonePyld), (ADT_UInt16 *) tonePyld, &dspStat);
   if (apiStat == GpakApiSuccess) return;

   ADT_printf ("Insert tone failure %d.%d.%d\n", apiStat, dspStat, DSPError[DspId]);

}
//}

//------------------------------------------------------------
//{ Channel control APIs (RTP, SRTP, Loopback modes, captures, etc.)
void sendTestMsg (ADT_Int32 DspId, char *ID, GpakTestMode Mode) {
   int side = 0;
   GpakTestData_t  Params;
   GpakApiStatus_t apiStat;
   GPAK_TestStat_t dspStat;

   char inputBuffer [20];
   ADT_UInt16 chanId;
   ADT_UInt16 Reply [5];

   ADT_printf ("\n%s channel id: ", ID);
   ADT_gets (inputBuffer, sizeof (inputBuffer));
   chanId = atoi (&inputBuffer[0]);

   if (Mode == StartEcCapt) {
      ADT_printf ("\n%s Side: ", ID);
      side = ADT_getchar () - 'a';
      chanId |= side << 8; 
   }

   Params.u.TestParm = chanId;
   apiStat = gpakTestMode (DspId, Mode, &Params, Reply, &dspStat);
   if (apiStat == GpakApiSuccess) return;

   ADT_printf ("Channel test failure %d.5%d.%d\n", apiStat, dspStat, DSPError[DspId]);
}
void startECCaptute (ADT_Int32 DspId) {
   GpakTestData_t  Params;
   GpakApiStatus_t apiStat;
   GPAK_TestStat_t dspStat;
   char inputBuffer [20];
   ADT_UInt16 chanId, side;
   ADT_UInt16 Reply [5];

   ADT_printf ("\nChannel id: ");
   ADT_gets (inputBuffer, sizeof (inputBuffer));
   chanId = atoi (&inputBuffer[0]);

   ADT_printf ("\nSide[ab]: ");
   if (inputBuffer[0] == 'b') side = 0x100;
   else                       side = 0;

   Params.u.TestParm = side | chanId;
   apiStat = gpakTestMode (DspId, StartEcCapt, &Params, Reply, &dspStat);
   if (apiStat == GpakApiSuccess) return;

   ADT_printf ("Channel test failure %d.5%d.%d\n", apiStat, dspStat, DSPError[DspId]);
}

void resetLoopBacks () {
   NetEnables.fireHoseEnable = ADT_FALSE;
   NetEnables.PortCrossover  = ADT_FALSE;
   NetEnables.multicastTx    = ADT_FALSE;
   NetEnables.multicastRx    = ADT_FALSE;
}

void setDSPCrossover (ADT_Int32 DspId) {
   GpakTestData_t  Params;
   GpakApiStatus_t apiStat;
   GPAK_TestStat_t dspStat;

   ADT_UInt16 Reply [5];

   ADT_printf ("\nDSP Crossover set\n");

   resetLoopBacks ();
   NetEnables.PortCrossover = ADT_TRUE;

   // Disable host loopback and let 
   hostLoopBack = DSP_LOOP;

   Params.u.TestParm = 0;
   apiStat = gpakTestMode (DspId, PacketCrossOver, &Params, Reply, &dspStat);
   if (apiStat == GpakApiSuccess) return;

   ADT_printf ("Channel test failure %d.5%d.%d\n", apiStat, dspStat, DSPError[DspId]);
}
void setDSPDaisyChain (ADT_Int32 DspId) {
   GpakTestData_t  Params;
   GpakApiStatus_t apiStat;
   GPAK_TestStat_t dspStat;

   ADT_UInt16 Reply [5];
 
   resetLoopBacks ();

   // Disable host loopback and let 
   hostLoopBack = DSP_LOOP;
   ADT_printf ("DSP daisy chain set\n");

   Params.u.TestParm = 0;
   apiStat = gpakTestMode (DspId, PacketDaisyChain, &Params, Reply, &dspStat);
   gpakHostDelay ();
   if (apiStat == GpakApiSuccess) return;
   ADT_printf ("Channel test failure %d.5%d.%d\n", apiStat, dspStat, DSPError[DspId]);
}
void setDSPLoopBack   (ADT_Int32 DspId) {
   GpakTestData_t  Params;
   GpakApiStatus_t apiStat;
   GPAK_TestStat_t dspStat;

   ADT_UInt16 Reply [5];
   
   // Disable host loopback and let 
   hostLoopBack = DSP_LOOP;
   resetLoopBacks ();

   ADT_printf ("DSP loopback set\n");

   Params.u.TestParm = 0;
   apiStat = gpakTestMode (DspId, PacketLoopBack, &Params, Reply, &dspStat);
   gpakHostDelay ();
   if (apiStat == GpakApiSuccess) return;
   ADT_printf ("Channel test failure %d.5%d.%d\n", apiStat, dspStat, DSPError[DspId]);
}
void setDSPNoLoopBack (ADT_Int32 DspId) {
   GpakTestData_t  Params;
   GpakApiStatus_t apiStat;
   GPAK_TestStat_t dspStat;

   ADT_UInt16 Reply [5];
   
   // Disable host loopback and let 
   hostLoopBack = NO_LOOP;
   resetLoopBacks ();

   Params.u.TestParm = 0;
   apiStat = gpakTestMode (DspId, DisableTest, &Params, Reply, &dspStat);
   gpakHostDelay ();
   if (apiStat == GpakApiSuccess) return;
   ADT_printf ("Channel test failure %d.5%d.%d\n", apiStat, dspStat, DSPError[DspId]);
}
void setCaptureSize  (ADT_Int32 DspId, ADT_UInt16 secs) {
   GpakTestData_t  Params;
   GpakApiStatus_t apiStat;
   GPAK_TestStat_t dspStat;

   ADT_UInt16 Reply [5];

   ADT_printf ("Capture buffer set to: %d seconds\n", secs);

   Params.u.TestParm = secs;
   apiStat = gpakTestMode (DspId, (GpakTestMode) 2, &Params, Reply, &dspStat);
   if (apiStat == GpakApiSuccess) return;

   ADT_printf ("Capture buffer set length failure %d.5%d.%d\n", apiStat, dspStat, DSPError[DspId]);
}

void setToneMode (ADT_UInt32 DspId, ADT_UInt16 chID, GpakDeviceSide_t device, GpakToneTypes toneFlags) {
   GpakApiStatus_t  ApiStat;
   GPAK_AlgControlStat_t dspStat;

   ApiStat = gpakAlgControl (DspId, chID, ModifyToneDetector, ToneDetectAll, toneFlags, device, 0, 0, &dspStat);
   if (ApiStat !=  GpakApiSuccess || dspStat != Cc_Success) {
      ADT_printf ("\nModify channel %d failure %d:%d:%d\n\n", chID, ApiStat, dspStat, DSPError [DspId]);
   }
   return;
}
void setRTPTxMode (ADT_Int32 DspId, GpakAlgCtrl_t rtpState) {
   GpakApiStatus_t       apiStat;
   GPAK_AlgControlStat_t dspStat;
   char inputBuffer[80];

   ADT_UInt16 chanId;

   ADT_printf ("\nRTP transmit mode test. Enter channel id: ");
   ADT_gets (inputBuffer, sizeof (inputBuffer));
   chanId = atoi (&inputBuffer[0]);

   apiStat = gpakAlgControl (DspId, chanId, rtpState, Null_tone, Null_tone, ADevice, 0, 0, &dspStat);
   if (apiStat == GpakApiSuccess) return;

   ADT_printf ("AlgCtrl RTP xmit failure %d:%d:%d\n", apiStat, dspStat, DSPError [DspId]);
}

void setSRTPCfgDefaults () {
   SRTPCfg.Encode.EncryptionScheme     = GPAK_ENCRYPT_NONE;
   SRTPCfg.Encode.AuthenticationScheme = GPAK_AUTH_NONE;
   SRTPCfg.Encode.AuthenticationI8     = 4;
   memcpy (SRTPCfg.Encode.MasterKey,  masterKey,  sizeof (SRTPCfg.Encode.MasterKey));
   memcpy (SRTPCfg.Encode.MasterSalt, masterSalt, sizeof (SRTPCfg.Encode.MasterSalt));
   memcpy (SRTPCfg.Encode.MKI, mki, 4);
   SRTPCfg.Encode.MKI_I8 = 0;
   SRTPCfg.Encode.KDR = 24;

   SRTPCfg.Decode.EncryptionScheme     = GPAK_ENCRYPT_NONE;
   SRTPCfg.Decode.AuthenticationScheme = GPAK_AUTH_NONE;
   SRTPCfg.Decode.AuthenticationI8     = 4;
   memcpy (SRTPCfg.Decode.MasterKey,  masterKey,  sizeof (SRTPCfg.Decode.MasterKey));
   memcpy (SRTPCfg.Decode.MasterSalt, masterSalt, sizeof (SRTPCfg.Decode.MasterSalt));
   memcpy (SRTPCfg.Decode.MKI, mki, 4);
   SRTPCfg.Decode.MKI_I8 = 0;
   SRTPCfg.Decode.KDR = 24;
   SRTPCfg.Decode.ROC = 0;

}

GpakApiStatus_t sendRTPConfigMsg (ADT_UInt32 DspId, ADT_UInt16 chID, GpakCodecs codec, GpakFrameHalfMS frameHalfMS) {
   // SRTP configuration variables
   GpakApiStatus_t  ApiStat;
   GpakSrtpStatus_t DspStat;

   // RTP configuration variables
   GpakRTPCfg_t             rtpCfg;
   GpakApiStatus_t          rtpCfgStat = GpakApiSuccess;
   GPAK_RTPConfigStat_t     rtpDspStat = (GPAK_RTPConfigStat_t) 0;

   if (!(sysCfg.CfgEnableFlags & RTPEnableBits)) return GpakApiSuccess;

   if (WB_START <= codec && codec <= WB_END) {
      rtpCfg.SamplingHz = 16000;
      codec -= 97;                   // Shift wide band codec codes into range of RTP payloads
   } else
      rtpCfg.SamplingHz = 8000;

#if 1 // jdc tone relay debug code: was this
   rtpCfg.DelayTargetMinMS = frameHalfMS / 2;
   rtpCfg.DelayTargetMS    = frameHalfMS;
   rtpCfg.DelayTargetMaxMS = frameHalfMS * 2;

   // Initialize RTP processing for channels
   rtpCfg.JitterMode       = 6;
#else
   rtpCfg.DelayTargetMinMS = frameHalfMS / 2;
   rtpCfg.DelayTargetMS    = (frameHalfMS/2) + frameHalfMS;
   rtpCfg.DelayTargetMaxMS = (frameHalfMS/2)  + frameHalfMS * 2;
   rtpCfg.JitterMode       = 0;
#endif
   rtpCfg.TonePyldType     = 97;
   rtpCfg.T38PyldType      = 98;
   rtpCfg.CNGPyldType      = 99;
   if (codec == G729 || codec == G729AB)
      rtpCfg.VoicePyldType    = codec;
   else
      rtpCfg.VoicePyldType    = 100;

   rtpCfg.StartSequence    =  1;
   rtpCfg.SrcPort          = srcPortFromChan (chID);
   rtpCfg.DestPort         = dstPortFromChan (chID);
   rtpCfg.DestIP           = dstIPFromChan (chID);
   rtpCfg.DestTxIP         = dstTxIPFromChan (chID);  // Multicast
   rtpCfg.InDestIP         = lclDstIPFromChan (chID); // Multicast
   rtpCfg.DSCP             = chID & 0x3f;
 
   rtpCfg.VlanIdx          = vlanFromChan (chID);

   memset (rtpCfg.DestMAC, 0, sizeof (rtpCfg.DestMAC));

   if (UseCustomSSRC) { 
        rtpCfg.SSRC     = CustomSSRC;
        rtpCfg.DestSSRC = CustomDestSSRC;
   } else {
      // Dummy remote MAC address to avoid ARP.
      rtpCfg.DestMAC[0] = 2;
      rtpCfg.DestMAC[1] = 2;
      rtpCfg.DestMAC[2] = 3;
      rtpCfg.DestMAC[3] = 3;
      rtpCfg.DestMAC[4] = 4;
      rtpCfg.DestMAC[5] = 4;
   }

   rtpCfg.SSRC     = ssrcFromChan(chID);
   rtpCfg.DestSSRC = dstSSRCFromChan(chID);
   rtpCfgStat = GpakApiSuccess;
   rtpDspStat = (GPAK_RTPConfigStat_t) 0;

   rtpCfgStat = gpakSendRTPMsg (DspId, chID, &rtpCfg, &rtpDspStat);
   if (rtpCfgStat != GpakApiSuccess || rtpDspStat != RTPSuccess) {
      ADT_printf ("RTP channel %d setup failure %d:%d:%d\n", chID, rtpCfgStat, rtpDspStat, DSPError [DspId]);
      return rtpCfgStat;
   }
   gpakHostDelay ();

   if (SRTPCfg.Encode.EncryptionScheme     == GPAK_ENCRYPT_NONE  &&
       SRTPCfg.Decode.EncryptionScheme     == GPAK_ENCRYPT_NONE  &&
       SRTPCfg.Encode.AuthenticationScheme == GPAK_AUTH_NONE     &&
       SRTPCfg.Decode.AuthenticationScheme == GPAK_AUTH_NONE) return rtpCfgStat;

   SRTPCfg.FrameHalfMS = frameHalfMS;
   ApiStat = gpakConfigSRTP (DspId, chID, &SRTPCfg, &DspStat);
   if (ApiStat != GpakApiSuccess || DspStat != GPAK_SRTP_SUCCESS) {
      ADT_printf ("SRTP channel %d setup failure %d:%d:%d\n", chID, ApiStat, DspStat, DSPError [DspId]);
   }

   return ApiStat;
}

void setCIDParams (ADT_UInt32 DspId, GpakCIDTypes CIDType) {
   GpakSystemParms_t  parms;
   GpakApiStatus_t    ApiStat;
   GPAK_SysParmsStat_t dspStatus;

   memset (&parms, 0, sizeof (parms));
   if (CIDType == CID_OFF_HOOK)
      parms.Cid.numSeizureBytes = 0;
   else
      parms.Cid.numSeizureBytes = 19;
   parms.Cid.numMarkBytes       = 10;
   parms.Cid.numPostambleBytes  = 5;
   parms.Cid.fskType            = fskBell202;
   parms.Cid.fskLevel           = -4;
                                              //  AGC   VAD       PCM-EC     PKT-EC     Cnfr      CID
   ApiStat = gpakWriteSystemParms (DspId, &parms, 0, ADT_FALSE, ADT_FALSE, ADT_FALSE,  ADT_FALSE, ADT_TRUE, &dspStatus);
   if (ApiStat!= GpakApiSuccess) {
      ADT_printf ("Write system configuration failure %d:%d:%d\n", ApiStat, dspStatus, DSPError [DspId]);
   }
}
void sendCIDMsg (ADT_UInt32 DspId) {
   GpakApiStatus_t ApiStat;
   GPAK_TxCIDPayloadStat_t DspStat;
   GPAK_AlgControlStat_t   AlgStat;
   ADT_UInt16 chanId;
   char  cidMsg[50];

   // Caller ID type 1 tests
   setCIDParams (DspId, CID_ON_HOOK);

   if (TDM_loopback != CROSSOVER) {
      ADT_printf ("TDM loopback not enabled\n");
      return;
   }

   chanId = ADT_getint ("\nCaller ID channel id: ");
   ADT_printf ("\n\r------------------------------------------------\r\nType 1. Single field test.\r\n");
   // Single message format  mmddhhmmtttttttttt  (month day hours minutes and telephone)
   sprintf (cidMsg, "%s", "032114051115551234"); 

   AlgStat = Ac_Success;
   ApiStat = gpakAlgControl (DspId, (ADT_UInt16) (chanId ^ 1), CID1RXRestart, Null_tone, Null_tone, ADevice, 0, 0, &AlgStat);

   DspStat =  Scp_Success;
   ApiStat = gpakSendCIDPayloadToDsp (DspId, chanId, CID_ON_HOOK, CID_SDMF, (ADT_UInt8 *) cidMsg, (ADT_UInt16) strlen (cidMsg), ADevice, &DspStat);
   if (ApiStat != GpakApiSuccess || DspStat != Scp_Success) {
      ADT_printf ("CIDTx channel %d setup failure %d:%d:%d\n", chanId, ApiStat, DspStat, DSPError [DspId]);
   }
   if (processTestKeys (0, NULL) == ESC) return;

   ADT_printf ("\n\r------------------------------------------------\r\nType 1. Multiple field test.\r\n");
   // Multiple message format   _01_mmddhhmm_02_tttttttttt_07_name
   sprintf (cidMsg, "%c%c%s%c%c%s%c%c%s", 1, 8,"03211405", 2, 10, "1115551234", 7, 9, "DOE JOE 1");
   DspStat =  Scp_Success;
   ApiStat = gpakSendCIDPayloadToDsp (DspId, chanId, CID_ON_HOOK, CID_MDMF, (ADT_UInt8 *) cidMsg, (ADT_UInt16) strlen (cidMsg), ADevice, &DspStat);
   if (ApiStat != GpakApiSuccess || DspStat != Scp_Success) {
      ADT_printf ("CIDTx channel %d setup failure %d:%d:%d\n", chanId, ApiStat, DspStat, DSPError [DspId]);
   }
   if (processTestKeys (0, NULL) == ESC) return;

   //------------------------------------------------------------------------------------
   // Caller ID type 2 tests
   ADT_printf ("\n\r------------------------------------------------\r\nType 2. Single field test.\r\n");
   setCIDParams (DspId, CID_OFF_HOOK);

   AlgStat = Ac_Success;
   ApiStat = gpakAlgControl (DspId, (ADT_UInt16) (chanId ^ 1), CID2RXRestart, Null_tone, Null_tone, ADevice, 0, 0, &AlgStat);
   if (ApiStat != GpakApiSuccess || AlgStat != Ac_Success) {
      ADT_printf ("AlgCtrl channel %d setup failure %d:%d:%d\n", chanId, ApiStat, AlgStat, DSPError [DspId]);
   }
   
   // Single message format  mmddhhmmtttttttttt  (month day hours minutes and telephone)
   sprintf (cidMsg, "%s", "032114052225551234"); 
   DspStat =  Scp_Success;
   ApiStat = gpakSendCIDPayloadToDsp (DspId, chanId, CID_OFF_HOOK, CID_SDMF, (ADT_UInt8 *) cidMsg, (ADT_UInt16) strlen (cidMsg), ADevice, &DspStat);
   if (ApiStat != GpakApiSuccess || DspStat != Scp_Success) {
      ADT_printf ("CIDTx channel %d setup failure %d:%d:%d\n", chanId, ApiStat, DspStat, DSPError [DspId]);
   }

   if (processTestKeys (0, NULL) == ESC) return;

   // Multiple message format   _01_mmddhhmm_02_tttttttttt_07_name
   ADT_printf ("\n\r------------------------------------------------\r\nType 2. Multiple field test\r\n");
   sprintf (cidMsg, "%c%c%s%c%c%s%c%c%s", 1, 8,"03211405", 2, 10, "2225551234", 7, 9, "DOE JOE 2");
   DspStat =  Scp_Success;
   ApiStat = gpakSendCIDPayloadToDsp (DspId, chanId, CID_OFF_HOOK, CID_MDMF, (ADT_UInt8 *) cidMsg, (ADT_UInt16) strlen (cidMsg), ADevice, &DspStat);
   if (ApiStat != GpakApiSuccess || DspStat != Scp_Success) {
      ADT_printf ("CIDTx channel %d setup failure %d:%d:%d\n", chanId, ApiStat, DspStat, DSPError [DspId]);
   }

   processTestKeys (0, NULL);

   
}
//}

//-----------------------------------------------
//{ Interactive channel setup support routines
static char InCodec[20];
static char OutCodec[20];
int getSlotAndPort () {
   int port, slot;

   for (port = 0; port < 3; port++) {
      slot = getSlot (port);
      if (slot != -1) return slot;
   }
   return -1;
}
void setTDMTDMDefaults (GpakChannelConfig_t *chCfgData, ADT_UInt16 chID) {
   int aSlotPort, bSlotPort;
   char *spacing;
   LoopbackMode loopback;

   PcmPcmCfg_t *PcmPcm = &chCfgData->PcmPcm;

   memset (PcmPcm, 0, sizeof (PcmPcmCfg_t));

   loopback = TDM_loopback;

   aSlotPort = getSlotAndPort ();
   if (ActiveChan.TDM_PORT != SerialPortNull)
      bSlotPort = getSlotAndPort ();
   else {
      bSlotPort = SerialPortNull << 16;
      loopback = LOOPBACK;
   }

   PcmPcm->InPortA = aSlotPort >> 16;
   PcmPcm->InSlotA = aSlotPort & 0xffff;
   if (loopback == LOOPBACK) {
      PcmPcm->OutPortB = aSlotPort >> 16;
      PcmPcm->OutSlotB = aSlotPort & 0xffff;
   } else {
      PcmPcm->OutPortB = bSlotPort >> 16;
      PcmPcm->OutSlotB = bSlotPort & 0xffff;
   }

   PcmPcm->InPortB = bSlotPort >> 16;
   PcmPcm->InSlotB = bSlotPort & 0xffff;
   if (loopback == LOOPBACK) {
      PcmPcm->OutPortA = bSlotPort >> 16;
      PcmPcm->OutSlotA = bSlotPort & 0xffff;
   } else {
      PcmPcm->OutPortA = aSlotPort >> 16;
      PcmPcm->OutSlotA = aSlotPort & 0xffff;
   }

   PcmPcm->EcanEnableA    = ActiveChan.PCM_EC;
   PcmPcm->AECEcanEnableA = ActiveChan.AEC;
   PcmPcm->AgcEnableA     = ActiveChan.AGC;
   PcmPcm->NoiseSuppressA = ActiveChan.NOISE;
   PcmPcm->CIdModeA       = ActiveChan.CID;
   PcmPcm->ToneTypesA     = ActiveChan.TONEA;
   PcmPcm->ToneGenGainG1A = ActiveChan.TG_GAIN;
   PcmPcm->OutputGainG2A  = ActiveChan.OUT_GAIN;
   PcmPcm->InputGainG3A   = ActiveChan.IN_GAIN;

   PcmPcm->EcanEnableB    = ActiveChan.PCM_EC;
   PcmPcm->AECEcanEnableB = Disabled;
   PcmPcm->AgcEnableB     = ActiveChan.AGCB;
   PcmPcm->NoiseSuppressB = ActiveChan.NOISEB;
   PcmPcm->CIdModeB       = ActiveChan.CID;
   PcmPcm->ToneTypesB     = ActiveChan.TONEB;
   PcmPcm->ToneGenGainG1B = ActiveChan.TG_GAIN;
   PcmPcm->OutputGainG2B  = ActiveChan.OUT_GAIN;
   PcmPcm->InputGainG3B   = ActiveChan.IN_GAIN;

   PcmPcm->FrameHalfMS   = ActiveChan.ENCSZ;
   PcmPcm->Coding        = GpakVoiceChannel;
#ifdef CORNET
   // Special because of CORNET's one-off on G.PAK API.
   if (PcmPcm->ToneTypesA & FAX_tone) {
      PcmPcm->ToneTypesA ^= FAX_tone;
      PcmPcm->SF26RxA = Enabled;
      PcmPcm->SF26Tx  = Enabled;
   }
   if (PcmPcm->ToneTypesB & FAX_tone) {
      PcmPcm->ToneTypesB ^= FAX_tone;
      PcmPcm->SF26RxB = Enabled;
   }
#endif
   ADT_printf ("\n\rChan: %3d  ", chID);
   if ((PcmPcm->InPortA != SerialPortNull) || (PcmPcm->OutPortA != SerialPortNull)) {
      ADT_printf ("TDM[%d.%d.%dms]_TDM[%d.%d.%dms]\t", 
               PcmPcm->InPortA,  PcmPcm->InSlotA,  PcmPcm->FrameHalfMS/2,
               PcmPcm->OutPortB, PcmPcm->OutSlotB, PcmPcm->FrameHalfMS/2);
      spacing = "\n\r           ";
   } else {
      spacing = "";
   }
   if ((PcmPcm->InPortB != SerialPortNull) || (PcmPcm->OutPortB != SerialPortNull)) {
      ADT_printf ("%sTDM[%d.%d.%dms]_TDM[%d.%d.%dms]\t", spacing,
               PcmPcm->InPortB,  PcmPcm->InSlotB,  PcmPcm->FrameHalfMS/2,
               PcmPcm->OutPortA, PcmPcm->OutSlotA, PcmPcm->FrameHalfMS/2);
   }
}
void setTDMPktDefaults (GpakChannelConfig_t *chCfgData, ADT_UInt16 chID, ADT_UInt32 DspId) {
   int slotPort;
   int slot, port;
   GpakCodecs tempCodec;

   PcmPktCfg_t *PcmPkt = &chCfgData->PcmPkt;

   slotPort = getSlotAndPort ();
   slot = slotPort & 0xffff;
   port = slotPort >> 16;
   memset (PcmPkt, 0, sizeof (PcmPktCfg_t));
   PcmPkt->PcmInPort    = port;
   PcmPkt->PcmOutPort   = port;
   PcmPkt->PcmInSlot    = slot;
   if (TDM_loopback == CROSSOVER) PcmPkt->PcmOutSlot = slot ^ TDM_crossoverbit;
   else                           PcmPkt->PcmOutSlot = slot;

   if (halfDuplex) {
        if (chID == 0) {
            tempCodec = ActiveChan.DECODE;
            ActiveChan.DECODE = NullCodec;
        }
        if (chID == 1) {
            tempCodec = ActiveChan.ENCODE;
            ActiveChan.ENCODE = NullCodec;
        }
   }

   PcmPkt->PktInCoding       = ActiveChan.DECODE;
   PcmPkt->PktInFrameHalfMS  = getCodecHalfMS (ActiveChan.DECODE, ActiveChan.DECSZ);
   PcmPkt->PktOutCoding      = ActiveChan.ENCODE;
   PcmPkt->PktOutFrameHalfMS = getCodecHalfMS (ActiveChan.ENCODE, ActiveChan.ENCSZ);

   PcmPkt->PcmEcanEnable = ActiveChan.PCM_EC;
   PcmPkt->PktEcanEnable = ActiveChan.PKT_EC;
   PcmPkt->AECEcanEnable = ActiveChan.AEC;
   PcmPkt->VadEnable     = ActiveChan.VAD;
   PcmPkt->AgcEnable     = ActiveChan.AGC;
   PcmPkt->ToneTypes     = ActiveChan.TONEA;
   PcmPkt->CIdMode       = ActiveChan.CID;
   PcmPkt->FaxMode       = ActiveChan.FAX;
   PcmPkt->FaxTransport  = faxUdptl;
   PcmPkt->Coding        = GpakVoiceChannel;

   PcmPkt->ToneGenGainG1 = ActiveChan.TG_GAIN;
   PcmPkt->OutputGainG2  = ActiveChan.OUT_GAIN;
   PcmPkt->InputGainG3   = ActiveChan.IN_GAIN;


   // Setup RTP stream
   sendRTPConfigMsg (DspId, chID, ActiveChan.DECODE, ActiveChan.DECSZ);

   memset (InCodec, 0, sizeof (InCodec));
   memset (OutCodec, 0, sizeof (OutCodec));
   appendChannelCodecs (PcmPkt->PktInCoding,  PcmPkt->PktInFrameHalfMS,  InCodec,  sizeof (InCodec));
   appendChannelCodecs (PcmPkt->PktOutCoding, PcmPkt->PktOutFrameHalfMS, OutCodec, sizeof (OutCodec));

   ADT_printf ("\n\rChan: %3d  TDM[%d.%d.%dms]_PKT[%s:%s:%d]\t", chID,
            PcmPkt->PcmInPort,    PcmPkt->PcmInSlot,       PcmPkt->PktOutFrameHalfMS/2, 
            OutCodec, txIPFromChan(chID), dstPortFromChan(chID));
   ADT_printf ("\n\r           PKT[%s:%s:%d]_TDM[%d.%d.%dms]\t",  
            InCodec, rxIPFromChan((ADT_UInt16) (chID)), srcPortFromChan(chID),
            PcmPkt->PcmOutPort,  PcmPkt->PcmOutSlot, PcmPkt->PktInFrameHalfMS/2);

   if (halfDuplex) {
        if (chID == 0)
            ActiveChan.DECODE = tempCodec;
        if (chID == 1)
            ActiveChan.ENCODE = tempCodec;
   }
}
void setPktPktDefaults (GpakChannelConfig_t *chCfgData, ADT_UInt16 chID, ADT_UInt32 DspId) {

   PktPktCfg_t *PktPkt = &chCfgData->PktPkt;

   memset (PktPkt, 0, sizeof (PktPktCfg_t));

   PktPkt->PktInCodingA       = ActiveChan.DECODE;
   PktPkt->PktInFrameHalfMSA  = getCodecHalfMS (ActiveChan.DECODE, ActiveChan.DECSZ);
   PktPkt->PktOutCodingA      = ActiveChan.ENCODE;
   PktPkt->PktOutFrameHalfMSA = getCodecHalfMS (ActiveChan.ENCODE, ActiveChan.ENCSZ);

   PktPkt->EcanEnableA      = ActiveChan.PKT_EC;
   PktPkt->VadEnableA       = ActiveChan.VAD;
   PktPkt->ToneTypesA       = ActiveChan.TONEA;
   PktPkt->AgcEnableA       = ActiveChan.AGC;

   PktPkt->ChannelIdB       = chID + 1;
   PktPkt->PktInCodingB       = ActiveChan.ENCODE;
   PktPkt->PktInFrameHalfMSB  = getCodecHalfMS (ActiveChan.ENCODE, ActiveChan.ENCSZ);
   PktPkt->PktOutCodingB      = ActiveChan.DECODE;
   PktPkt->PktOutFrameHalfMSB = getCodecHalfMS (ActiveChan.DECODE, ActiveChan.DECSZ);

   PktPkt->EcanEnableB      = ActiveChan.PKT_EC;
   PktPkt->VadEnableB       = ActiveChan.VAD;
   PktPkt->ToneTypesB       = ActiveChan.TONEB;
   PktPkt->AgcEnableB       = ActiveChan.AGCB;

   sendRTPConfigMsg (DspId, chID,   ActiveChan.DECODE, ActiveChan.DECSZ);
   sendRTPConfigMsg (DspId, (ADT_UInt16) (chID+1), ActiveChan.ENCODE, ActiveChan.ENCSZ);

   memset (InCodec, 0, sizeof (InCodec));
   memset (OutCodec, 0, sizeof (OutCodec));
   appendChannelCodecs (PktPkt->PktInCodingA,  PktPkt->PktInFrameHalfMSA,  InCodec,  sizeof (InCodec));
   appendChannelCodecs (PktPkt->PktOutCodingA, PktPkt->PktOutFrameHalfMSA, OutCodec, sizeof (OutCodec));

   ADT_printf ("\n\rChan: %3d  PKT[%s:%s]_PKT[%s:%s]\t", chID,
          InCodec, rxIPFromChan (chID),  OutCodec, txIPFromChan (chID));

   memset (InCodec, 0, sizeof (InCodec));
   memset (OutCodec, 0, sizeof (OutCodec));
   appendChannelCodecs (PktPkt->PktInCodingB,  PktPkt->PktInFrameHalfMSB,  InCodec,  sizeof (InCodec));
   appendChannelCodecs (PktPkt->PktOutCodingB, PktPkt->PktOutFrameHalfMSB, OutCodec, sizeof (OutCodec));

  ADT_printf ("\n\r      %3d  PKT[%s:%s]_PKT[%s:%s]\t", PktPkt->ChannelIdB,
          InCodec, rxIPFromChan((ADT_UInt16) (chID + 1)),  OutCodec, txIPFromChan((ADT_UInt16) (chID + 1)));
}
void setTDMCnfrDefaults (GpakChannelConfig_t *chCfgData, ADT_UInt16 cfrId, ADT_UInt16 chID) {
   int slotPort, slot, port;
   CnfPcmCfg_t *PcmCnf = &chCfgData->ConferPcm;
   int cnfrMs;

   cnfrMs = ActiveCnfr[cfrId].cnfrHalfMS/2;

   memset (PcmCnf, 0, sizeof (CnfPcmCfg_t));

   slotPort = getSlotAndPort ();
   slot = slotPort & 0xffff;
   port = slotPort >> 16;
   PcmCnf->ConferenceId = cfrId;

   PcmCnf->PcmInPort    = port;
   PcmCnf->PcmOutPort   = port;
   PcmCnf->PcmInSlot    = slot;
   PcmCnf->PcmOutSlot   = slot;

   PcmCnf->EcanEnable    = ActiveChan.PCM_EC;
   PcmCnf->AECEcanEnable = ActiveChan.AEC;
   PcmCnf->AgcInEnable   = ActiveChan.AGC;
   PcmCnf->AgcOutEnable  = ActiveChan.AGC;
   PcmCnf->ToneTypes     = ActiveChan.TONEA;

   PcmCnf->ToneGenGainG1 = ActiveChan.TG_GAIN;
   PcmCnf->OutputGainG2  = ActiveChan.OUT_GAIN;
   PcmCnf->InputGainG3   = ActiveChan.IN_GAIN;

   ADT_printf ("\n\rChan: %3d  TDM[%d.%d]_CNF[%d.%dms]\t", chID,
            PcmCnf->PcmInPort,  PcmCnf->PcmInSlot, 
            PcmCnf->ConferenceId, cnfrMs);
   ADT_printf ("\n\r           CNF[%d.%dms]_TDM[%d.%d]\t", 
            PcmCnf->ConferenceId, cnfrMs,
            PcmCnf->PcmOutPort, PcmCnf->PcmOutSlot);

}
void setPktCnfrDefaults (GpakChannelConfig_t *chCfgData, ADT_UInt16 cfrId, ADT_UInt16 chID, GpakFrameHalfMS FrameHalfMS, ADT_UInt32 DspId) {

   CnfPktCfg_t *CnfPkt = &chCfgData->ConferPkt;
   int cnfrMs;

   cnfrMs = ActiveCnfr[cfrId].cnfrHalfMS/2;
   memset (CnfPkt, 0, sizeof (CnfPktCfg_t));

   CnfPkt->ConferenceId = cfrId;
   CnfPkt->PktInCoding  = ActiveChan.DECODE;
   CnfPkt->PktOutCoding = ActiveChan.ENCODE;
   CnfPkt->EcanEnable   = ActiveChan.PKT_EC;
   CnfPkt->VadEnable    = ActiveChan.VAD;
   CnfPkt->AgcInEnable  = ActiveChan.AGC;
   CnfPkt->AgcOutEnable = ActiveChan.AGC;
   CnfPkt->ToneTypes    = ActiveChan.TONEA;
   CnfPkt->PktFrameHalfMS = getCodecHalfMS (ActiveChan.DECODE, FrameHalfMS);

   CnfPkt->ToneGenGainG1 = ActiveChan.TG_GAIN;
   CnfPkt->OutputGainG2  = ActiveChan.OUT_GAIN;
   CnfPkt->InputGainG3   = ActiveChan.IN_GAIN;

   
   sendRTPConfigMsg (DspId, chID, ActiveChan.DECODE, FrameHalfMS);

   memset (InCodec, 0, sizeof (InCodec));
   memset (OutCodec, 0, sizeof (OutCodec));
   appendChannelCodecs (CnfPkt->PktInCoding,  CnfPkt->PktFrameHalfMS,  InCodec,  sizeof (InCodec));
   appendChannelCodecs (CnfPkt->PktOutCoding, CnfPkt->PktFrameHalfMS, OutCodec, sizeof (OutCodec));

   ADT_printf ("\n\rChan: %3d  PKT[%s:%s]_CNF[%d.%dms]\t", chID,
            InCodec, rxIPFromChan (chID), CnfPkt->ConferenceId, cnfrMs);

   ADT_printf ("\n\r           CNF[%d.%dms]_PKT[%s:%s]\t", 
            CnfPkt->ConferenceId, cnfrMs,      OutCodec, txIPFromChan (chID));
}
void setCmpConfDefaults (GpakChannelConfig_t *chCfgData, ADT_UInt16 cfrId, ADT_UInt16 chID, ADT_UInt32 DspId) {

   CnfCmpCfg_t *CnfCmp = &chCfgData->ConferComp;
   int cnfrMs;

   cnfrMs = ActiveCnfr[cfrId].cnfrHalfMS/2;

   memset (CnfCmp, 0, sizeof (CnfCmpCfg_t));

   CnfCmp->ConferenceId     = cfrId;
   CnfCmp->PcmOutPort       = SerialPortNull;
   CnfCmp->PcmOutSlot       = 0;
   CnfCmp->PcmOutPin        = 0;
   CnfCmp->PktOutCoding     = ActiveChan.ENCODE;
   CnfCmp->VadEnable        = ActiveChan.VAD;

   sendRTPConfigMsg (DspId, chID, ActiveChan.ENCODE, ActiveChan.ENCSZ);

   ADT_printf ("\n\r           CNF[%d.%dms]_CMP[%2d:%s]\t", 
            CnfCmp->ConferenceId, cnfrMs,
            CnfCmp->PktOutCoding, txIPFromChan (chID));

}

void getChannelSlots (ADT_UInt32 DspId, ADT_UInt16 chanID, GpakSerialPort_t *port, int *slotA, int *slotB) {
   GpakChannelStatus_t      ChanStatus;
   GpakChannelConfig_t     *chanCfg;
   gpakGetChanStatStatus_t  ApiStatus;
   GPAK_ChannelStatusStat_t DspStatus;

   *slotA = -1;
   *slotB = -1;

   DspStatus = Cs_Success;
   ApiStatus = gpakGetChannelStatus (DspId, chanID, &ChanStatus, &DspStatus);
   if (ApiStatus != GpakApiSuccess) return;

   chanCfg = &ChanStatus.ChannelConfig;

   switch (ChanStatus.ChannelType) {
   case pcmToPcm: {
      PcmPcmCfg_t *PcmPcm = &chanCfg->PcmPcm;
      *port  = PcmPcm->OutPortA;
      *slotA = PcmPcm->OutSlotA;
      *slotB = PcmPcm->OutSlotB;
      }
      break;
 
   case pcmToPacket: {
      PcmPktCfg_t *PcmPkt = &chanCfg->PcmPkt;
      *port  = PcmPkt->PcmOutPort;
      *slotA = PcmPkt->PcmOutSlot;
      }
      break;

   case conferencePcm: {
      CnfPcmCfg_t *CfrPcm = &chanCfg->ConferPcm;
      *port  = CfrPcm->PcmOutPort;
      *slotA = CfrPcm->PcmOutSlot;
      }
      break;
   }
}
void initTdmTdmChannels (ADT_UInt32 DspId, int count) {

   // Channel configuration variables
   GpakApiStatus_t          ApiStat;
   GpakChannelConfig_t      chCfgData;
   GPAK_ChannelConfigStat_t chDspStat = Cc_Success;

   ADT_UInt16 j;
   ADT_UInt16 chanID, chanId, coreId;

   iteration++;
   chanID = sysCfg.ActiveChannels;   // Current channel count

   // Open TDM-TDM channels
   for (j=0; chanID<sysCfg.MaxChannels && j<count; chanID++, j++) {
      if ((sysCfg.CfgFramesChans & PCM2PCM_CFG_BIT) == 0) {
         ADT_printf ("TDM-TDM channels are not configured\n");
         break;
      }

      setTDMTDMDefaults (&chCfgData, chanID);

      coreId = ((chanID + iteration)%framingCores) + framingCoreOffset;
      chanId = (coreId << 8) + chanID;  // Distribute channel processing across 5 cores
      ApiStat = gpakConfigureChannel (DspId, chanId, pcmToPcm, &chCfgData, &chDspStat);
      if (ApiStat !=  GpakApiSuccess || chDspStat != Cc_Success) {
         ADT_printf ("Core %d Channel %3d setup failure %d:%d:%d\n\r", coreId, chanID, ApiStat, chDspStat, DSPError [DspId]);
         freeSlot (chCfgData.PcmPcm.OutPortA, chCfgData.PcmPcm.OutSlotA);
         freeSlot (chCfgData.PcmPcm.OutPortB, chCfgData.PcmPcm.OutSlotB);
      } else {
         ADT_printf ("Core %d Channel %3d setup success\n\r", coreId, chanID);
         sysCfg.ActiveChannels++;
      }
      gpakHostDelay ();      gpakHostDelay ();
      gpakHostDelay ();      gpakHostDelay ();
      gpakHostDelay ();      gpakHostDelay ();
   }

}
void initTdmPktChannels (ADT_UInt32 DspId, int count) {

   // Channel configuration variables
   GpakApiStatus_t          ApiStat;
   GpakChannelConfig_t      chCfgData;
   GPAK_ChannelConfigStat_t chDspStat  = Cc_Success;
   GPAK_AlgControlStat_t    algDspStat = Ac_Success;
   ADT_UInt16 j;
   ADT_UInt16 chanID, chanId, coreId;

   iteration++;
   chanID = sysCfg.ActiveChannels;   // Current channel count

   // Open TDM-Packet channels
   for (j=0; chanID<sysCfg.MaxChannels && j<count; chanID++, j++) {
      if ((sysCfg.CfgFramesChans & PCM2PKT_CFG_BIT) == 0) {
         ADT_printf ("TDM-Pkt channels are not configured\n");
         break;
      }

      setTDMPktDefaults (&chCfgData, chanID, DspId);

// jdc: -------------------------
      if (fixedCore) {
            if (chanID & 1) coreId = 2;  // odd channels on core 2
            else            coreId = 2;  // even channels on core 3
      } else {
            coreId = ((chanID + iteration)%framingCores) + framingCoreOffset;
      }
// ------------------------------
      chanId = (coreId << 8) + chanID;  // Distribute channel processing across 5 cores
      ApiStat = gpakConfigureChannel (DspId, chanId, pcmToPacket, &chCfgData, &chDspStat);
      if (ApiStat !=  GpakApiSuccess || chDspStat != Cc_Success) {
         ADT_printf ("Core %d Channel %3d setup failure %d:%d:%d\n\r", coreId, chanID, ApiStat, chDspStat, DSPError [DspId]);
         freeSlot (chCfgData.PcmPkt.PcmOutPort, chCfgData.PcmPkt.PcmOutSlot);
      } else {
         ADT_printf ("Core %d Channel %3d setup success\n\r", coreId, chanID);
         sysCfg.ActiveChannels++;
      }
      gpakHostDelay ();
      gpakHostDelay ();

      // Disable RTPTx via AlgControl
      if (!NetEnables.disableRTPTx) continue;
      ApiStat = gpakAlgControl (DspId, chanId, RTPTransmitDisable, Null_tone, Null_tone, ADevice, 0, 0, &algDspStat);
      if (ApiStat !=  GpakApiSuccess || algDspStat != Ac_Success) {
         ADT_printf ("Core %d Channel %3d disable RTPTx failure %d:%d:%d\n\n", coreId, chanID, ApiStat, algDspStat, DSPError [DspId]);
      }
   }
#ifdef ADT_DEVELOPMENT
   if (NetEnables.fireHoseEnable) turnOnFireHose (DspId);
#endif

}
void initPktPktChannels (ADT_UInt32 DspId, int count) {

   // Channel configuration variables
   GpakApiStatus_t          ApiStat;
   GpakChannelConfig_t      chCfgData;
   GPAK_ChannelConfigStat_t chDspStat = Cc_Success;

   ADT_UInt16 j;
   ADT_UInt16 chanID, chanId, coreId;
   
   iteration++;
   chanID = sysCfg.ActiveChannels;   // Current channel count

   // Open Packet-Packet channels
   for (j=0; chanID<sysCfg.MaxChannels && j<count; chanID+=2, j++) {
      if ((sysCfg.CfgFramesChans & PKT2PKT_CFG_BIT) == 0) {
         ADT_printf ("Pkt-Pkt channels are not configured\n");
         break;
      }
      setPktPktDefaults (&chCfgData, chanID, DspId);

      coreId = ((j + iteration)%framingCores) + framingCoreOffset;
      chanId = (coreId << 8) + chanID;  // Distribute channel processing across 5 cores
      ApiStat = gpakConfigureChannel (DspId, chanId, packetToPacket, &chCfgData, &chDspStat);
      if (ApiStat !=  GpakApiSuccess || chDspStat != Cc_Success) {
         ADT_printf ("Core %d Channel %3d setup failure %d:%d:%d\n\r", coreId, chanID, ApiStat, chDspStat, DSPError [DspId]);
      } else {
         ADT_printf ("Core %d Channel %3d setup success\n\r", coreId, chanID);
         sysCfg.ActiveChannels += 2;
      }
      gpakHostDelay ();
   }

}
void initTdmCnfrChannels (ADT_UInt32 DspId, int count) {

   // Channel configuration variables
   gpakConfigChanStatus_t   ApiStat;
   GpakChannelConfig_t      chCfgData;
   GPAK_ChannelConfigStat_t chDspStat = Cc_Success;
   int chansPerConf;

   ADT_UInt16 j;
   ADT_UInt16 chanID, chanId, cfrId, coreId;

   chanID = sysCfg.ActiveChannels;   // Current channel count
   chansPerConf = sysCfg.MaxNumConfChans;

   // Open Pcm-Conference channels
   for (j=0; chanID<sysCfg.MaxChannels && j<count; chanID++, j++) {

      if ((sysCfg.CfgFramesChans & CONFPCM_CFG_BIT) == 0) {
         ADT_printf ("TDM conference channels are not configured\n\r");
         break;
      }

      cfrId = j / (chansPerConf * framingCores);
      setTDMCnfrDefaults (&chCfgData, cfrId, chanID);

      coreId = (((j/chansPerConf) + iteration) %framingCores) + framingCoreOffset;
      chanId = (coreId << 8) + chanID;  // Distribute channel processing across 5 cores
      ApiStat = gpakConfigureChannel (DspId, chanId, conferencePcm, &chCfgData, &chDspStat);
      if (ApiStat !=  GpakApiSuccess || chDspStat != Cc_Success) {
         ADT_printf ("Core %d Channel %3d setup failure %d:%d:%d\n\r", coreId, chanID, ApiStat, chDspStat, DSPError [DspId]);
         freeSlot (chCfgData.ConferPcm.PcmOutPort, chCfgData.ConferPcm.PcmOutSlot);
      } else {
         ADT_printf ("Core %d Channel %3d setup success\n\r", coreId, chanID);
         sysCfg.ActiveChannels++;
      }
      gpakHostDelay ();
   }
}
void initPktCnfrChannels (ADT_UInt32 DspId, int count) {

   // Channel configuration variables
   gpakConfigChanStatus_t   ApiStat;
   GpakChannelConfig_t      chCfgData;
   GPAK_ChannelConfigStat_t chDspStat = Cc_Success;
   int chansPerConf;

   ADT_UInt16 j;
   ADT_UInt16 chanID, chanId, cfrId, coreId;

   chanID = sysCfg.ActiveChannels;   // Current channel count
   chansPerConf = sysCfg.MaxNumConfChans;

   // Open Packet-Conference channels
   for (j=0; chanID<sysCfg.MaxChannels && j<count; chanID++, j++) {

      if ((sysCfg.CfgFramesChans & CONFPKT_CFG_BIT) == 0) {
         ADT_printf ("Packet conference channels are not configured\n\r");
         break;
      }

      cfrId = j / (chansPerConf * framingCores);
      setPktCnfrDefaults (&chCfgData, cfrId, chanID, ActiveCnfr[cfrId].cnfrHalfMS, DspId);

      coreId = (((j/chansPerConf) + iteration) %framingCores) + framingCoreOffset;
      chanId = (coreId << 8) + chanID;  // Distribute channel processing across 5 cores
      ApiStat = gpakConfigureChannel (DspId, chanId, conferencePacket, &chCfgData, &chDspStat);
      if (ApiStat !=  GpakApiSuccess || chDspStat != Cc_Success) {
         ADT_printf ("Core %d Channel %3d setup failure %d:%d:%d\n\r", coreId, chanID, ApiStat, chDspStat, DSPError [DspId]);
      } else {
         ADT_printf ("Core %d Channel %3d setup success\n\r", coreId, chanID);
         sysCfg.ActiveChannels++;
      }
      gpakHostDelay ();
   }
}
void initCmpCnfrChannels (ADT_UInt32 DspId, int count) {

   // Channel configuration variables
   gpakConfigChanStatus_t   ApiStat;
   GpakChannelConfig_t      chCfgData;
   GPAK_ChannelConfigStat_t chDspStat = Cc_Success;
   int chansPerConf;

   ADT_UInt16 j;
   ADT_UInt16 chanID, chanId, cfrId, coreId;

   chanID = sysCfg.ActiveChannels;   // Current channel count
   chansPerConf = sysCfg.MaxNumConfChans;

   // Open Conference-Composite channels
   for (j=0; chanID<sysCfg.MaxChannels && j<count; chanID++, j++) {

      if ((sysCfg.CfgFramesChans & CONFCOMP_CFG_BIT) == 0) {
         ADT_printf ("Conference composite channels are not configured\n\r");
         break;
      }

      cfrId = j / (chansPerConf * framingCores);
      setCmpConfDefaults (&chCfgData, (ADT_UInt16) (cfrId%sysCfg.MaxConferences), chanID, DspId);

      coreId = (((j/chansPerConf) + iteration)%framingCores) + framingCoreOffset;
      chanId = (coreId << 8) + chanID;  // Distribute channel processing across 5 cores
      ApiStat = gpakConfigureChannel (DspId, chanId, conferenceComposite, &chCfgData, &chDspStat);
      if (ApiStat !=  GpakApiSuccess || chDspStat != Cc_Success) {
         ADT_printf ("Core %d Channel %3d setup failure %d:%d:%d\n\r", coreId, chanID, ApiStat, chDspStat, DSPError [DspId]);
      } else {
         ADT_printf ("Core %d Channel %3d setup success\n\r", coreId, chanID);
         sysCfg.ActiveChannels++;
      }
      gpakHostDelay ();
   }

   ADT_printf ("Channels started\n");
}

void initChannels (ADT_UInt32 DspId) {
   ADT_printf ("\nStarting channels...");

   if (NetEnables.fireHoseEnable) ADT_printf (" fire hose enabled");
   if (NetEnables.multicastTx)    ADT_printf (" tx multicast enabled");
   if (NetEnables.multicastRx)    ADT_printf (" rx multicast enabled");
   if (NetEnables.PortCrossover)  ADT_printf (" DSP cross over enabled");
   ADT_printf ("\n");

   initTdmTdmChannels (DspId, chns.tdmTdm);     // Open TDM-TDM channels
   initPktPktChannels (DspId, chns.pktPkt);     // Open Packet-Packet channels
   initTdmPktChannels (DspId, chns.tdmPkt);     // Open TDM-Packet channels

   if (sysCfg.MaxConferences <= 0) return;

   initTdmPktChannels (DspId, chns.tdmCnfr);    // Open Pcm-Conference channels
   initPktCnfrChannels (DspId, chns.pktCnfr);   // Open Packet-Conference channels
   initCmpCnfrChannels (DspId, chns.cmpCnfr);   // Open Conference-Composite channels

   ADT_printf ("Channels started\n");
}
void restoreChannelDefaults (void) {
   ActiveChan = DefaultChan; 
   memcpy (&ActiveCnfr, &DefaultCnfr, sizeof (ActiveCnfr));
}

static void getChannelTypeCounts (void) {
   char inputBuffer[80];

   ADT_printf ("\nEnter tdm to tdm channel count: ");
   ADT_gets (inputBuffer, sizeof (inputBuffer));
   chns.tdmTdm = atoi (inputBuffer);

   ADT_printf ("\nEnter tdm to pkt channel count: ");
   ADT_gets (inputBuffer, sizeof (inputBuffer));
   chns.tdmPkt = atoi (inputBuffer);

   ADT_printf ("\nEnter pkt to pkt channel count: ");
   ADT_gets (inputBuffer, sizeof (inputBuffer));
   chns.pktPkt = atoi (inputBuffer);

   ADT_printf ("\nEnter tdm to cnfr channel count: ");
   ADT_gets (inputBuffer, sizeof (inputBuffer));
   chns.tdmCnfr = atoi (inputBuffer);

   ADT_printf ("\nEnter pkt to cnfr channel count: ");
   ADT_gets (inputBuffer, sizeof (inputBuffer));
   chns.pktCnfr = atoi (inputBuffer);

   ADT_printf ("\nEnter cnfr composite channel count: ");
   ADT_gets (inputBuffer, sizeof (inputBuffer));
   chns.cmpCnfr = atoi (inputBuffer);

   ADT_printf ("\n\n");
}
//}

void tearDownChannels (ADT_UInt32 DspId, int chanCnt, int endChannel) {
   GPAK_TearDownChanStat_t  tdStat = (GPAK_TearDownChanStat_t) 0;
   GpakSerialPort_t port;
   int slotA, slotB, i;
   ADT_UInt16 chanID;

#ifdef ADT_DEVELOPMENT
   if (NetEnables.fireHoseEnable) turnOffFireHose (DspId);
#endif

   ADT_printf ("\nChannels freed: ");
   for (i=0; i<chanCnt; i++) {
      chanID = endChannel--;
      getChannelSlots (DspId, chanID, &port, &slotA, &slotB);
      if (GpakApiSuccess != gpakTearDownChannel (DspId, chanID, &tdStat))
         continue;
      ADT_printf ("%d ", chanID);
      --sysCfg.ActiveChannels;
      if (slotA != -1) freeSlot (port, slotA);
      if (slotB != -1) freeSlot (port, slotB);
   }
   ADT_printf (".\n");
}

void dspTearDown (ADT_UInt32 DspId) {
   
   GPAK_TearDownChanStat_t  tdStat = (GPAK_TearDownChanStat_t) 0;
   GpakApiStatus_t          ApiStat;
   GpakSerialPort_t port;
   ADT_Int16 chanID;
   int slotA, slotB;

#ifdef ADT_DEVELOPMENT
   if (NetEnables.fireHoseEnable) turnOffFireHose (DspId);
#endif
   gpakGetSystemConfig (DspId, &sysCfg);

   ADT_printf ("\nChannels freed: ");
   // Tear down active channels 
   for (chanID = sysCfg.ActiveChannels - 1; 0 <= chanID; chanID--) {
      getChannelSlots (DspId, chanID, &port, &slotA, &slotB);
      ApiStat = gpakTearDownChannel (DspId, chanID, &tdStat);
      if (GpakApiSuccess != ApiStat) {
         ADT_printf ("Channel %3d teardown failure %d:%d:%d\n\n", chanID, ApiStat, tdStat, DSPError [DspId]);
         break;
      }
      ADT_printf ("%d ", chanID);
      if (slotA != -1) freeSlot (port, slotA);
      if (slotB != -1) freeSlot (port, slotB);
   }
   ADT_printf (".\n");

   gpakResetSystemStats (DspId, resetStats);

   // Read current number of active channels
   gpakGetSystemConfig (DspId, &sysCfg);
   if (sysCfg.ActiveChannels == 0) {
      freeAllSlots (0);
      freeAllSlots (1);
      freeAllSlots (2);
      return;
   }

   ADT_printf ("\n%d Channels still active\n", sysCfg.ActiveChannels);
   ADT_getchar ();
   // Channels still active.  Try tearing down all channels
   for (chanID = 0; chanID < sysCfg.MaxChannels; chanID++) {
      getChannelSlots (DspId, chanID, &port, &slotA, &slotB);
      if (GpakApiSuccess != gpakTearDownChannel (DspId, chanID, &tdStat))
         continue;
      ADT_printf ("%d ", chanID);
      if (slotA != -1) freeSlot (port, slotA);
      if (slotB != -1) freeSlot (port, slotB);
   }

   gpakGetSystemConfig (DspId, &sysCfg);
   if (sysCfg.ActiveChannels == 0) {
      freeAllSlots (0);
      freeAllSlots (1);
      freeAllSlots (2);
      return;
   }
   ADT_printf ("\nChannels still active\n");

}


void dspTearDownAll (ADT_UInt32 DspId) {
   
   GPAK_TearDownChanStat_t  tdStat = (GPAK_TearDownChanStat_t) 0;
   GpakApiStatus_t          ApiStat;
   GpakSerialPort_t port;
   ADT_Int16 chanID;
   int slotA, slotB;

#ifdef ADT_DEVELOPMENT
   if (NetEnables.fireHoseEnable) turnOffFireHose (DspId);
#endif
   ADT_printf ("\nChannels freed: ");
   // Tear down active channels 
   for (chanID = sysCfg.MaxChannels - 1; 0 <= chanID; chanID--) {
      getChannelSlots (DspId, chanID, &port, &slotA, &slotB);
      ApiStat = gpakTearDownChannel (DspId, chanID, &tdStat);
      if (GpakApiSuccess != ApiStat) {
         ADT_printf ("Channel %3d teardown failure %d:%d:%d\n\n", chanID, ApiStat, tdStat, DSPError [DspId]);
      }
      ADT_printf ("%d ", chanID);
      if (slotA != -1) freeSlot (port, slotA);
      if (slotB != -1) freeSlot (port, slotB);
   }
   ADT_printf (".\n");

}

//----------------------------------------------------
//{  Conference initialization
static void initConferences (ADT_UInt32 DspId) {
   // Conference configuration variables
   ADT_UInt16 CfrId, CoreId;
   ADT_UInt16 cfrId;
   GpakToneTypes tones;
   GpakApiStatus_t          cfrCfgStat = GpakApiSuccess;
   GPAK_ConferConfigStat_t  cfrDspStat = (GPAK_ConferConfigStat_t) 0;
   ADT_printf ("\n\nConference setup\n");
   ADT_printf ("\nCore  Cnfr  Rate\n");

   tones =  Null_tone;
   if (sysCfg.CfgEnableFlags & DTMFEnableBit) tones = DTMF_tone | Notify_Host;

   for (CoreId = 0; CoreId < MaxCoresPerDSP; CoreId++)
   for (CfrId = 0; (ActiveCnfr[CfrId].cnfrHalfMS != 0) && (CfrId < sysCfg.MaxConferences); CfrId++) {
      cfrId = (CoreId << 8) + CfrId;  // Distribute channel processing across 5 cores
      cfrCfgStat = gpakConfigureConference (DspId, cfrId, ActiveCnfr[CfrId].cnfrHalfMS, 
                                            tones, &cfrDspStat);
      ADT_printf ("%3d %5d  [%3dms]", CoreId, CfrId, ActiveCnfr[CfrId].cnfrHalfMS/2);
      if (cfrCfgStat != CpsSuccess || cfrDspStat != Pc_Success)
         ADT_printf (" setup failure %d:%d\n", cfrCfgStat, cfrDspStat);
      else
         ADT_printf ("\n");
   }
   ADT_printf ("\n");
}

char setupConference (ADT_UInt32 DspId, ADT_UInt16 CnfrID, GpakFrameHalfMS HalfMS, ADT_Bool DTMFEnabled) {
   GPAK_ConferConfigStat_t  cfrDspStat = (GPAK_ConferConfigStat_t) 0;
   GpakApiStatus_t          ApiStat = GpakApiSuccess;
   GpakToneTypes            tones;

   if (HalfMS == 0) return 0;

   if (DTMFEnabled) tones = DTMF_tone;
   else             tones = Null_tone;
   ApiStat = gpakConfigureConference (DspId, CnfrID, HalfMS, tones, &cfrDspStat);
   if (ApiStat != CpsSuccess || cfrDspStat != Pc_Success) {
      ADT_printf ("%d.%dms setup failure %d:%d\n", CnfrID, HalfMS/2, ApiStat, cfrDspStat);
      return ESC; 
   }
   ActiveCnfr[CnfrID].cnfrHalfMS = HalfMS;
   return 0;
}
//}

//---------------------------------------------------------
//{ System parameter display
extern void setMaxDominant (ADT_UInt32 DspId, int dominant) {
   GpakApiStatus_t     ApiStat;
   GpakSystemParms_t   Params;
   GPAK_SysParmsStat_t DspStat = Sp_Success;

   ApiStat = gpakReadSystemParms (DspId, &Params);
   if (ApiStat != GpakApiSuccess) {
      ADT_printf ("Get system configuration failure %d:%d\n", ApiStat, DSPError [DspId]);
      return;
   }
   Params.ConfNumDominant = dominant;
   ApiStat = gpakWriteSystemParms (DspId, &Params, 3, ADT_FALSE, ADT_FALSE, ADT_FALSE, ADT_TRUE, ADT_FALSE, &DspStat);
   if ((ApiStat != GpakApiSuccess) || (DspStat != Sp_Success)) {
      ADT_printf ("Write system configuration failure %d:%d:%d\n", ApiStat, DspStat, DSPError [DspId]);
   }

}

static void setSysMin (GpakSystemParms_t *params) {
   GpakAGCParms_t *AGC_A, *AGC_B;
   GpakECParams_t *Pcm, *Pkt;

   Pcm   = &params->PcmEc;
   Pkt   = &params->PktEc;
   AGC_A = &params->AGC_A;
   AGC_B = &params->AGC_B;

   AGC_A->TargetPower = AGC_B->TargetPower = -30;
   AGC_A->LossLimit   = AGC_B->LossLimit   = -23;
   AGC_A->GainLimit   = AGC_B->GainLimit   =   0;
   AGC_A->LowSignal   = AGC_B->LowSignal   = -65;

   params->VadNoiseFloor   = -96;
   params->VadHangTime     =  0;
   params->VadWindowSize   =  2;
   params->VadReport       =  0;

   params->ConfNumDominant = 1;
   params->MaxConfNoiseSuppress = 0;

   Pcm->TailLength        = Pkt->TailLength      = 8;
   Pcm->NumFirSegments    = Pkt->NumFirSegments  = 1;
   Pcm->FirSegmentLen     = Pkt->FirSegmentLen   = 32;
   Pcm->FirTapCheckPeriod = Pkt->FirTapCheckPeriod = 10;

   Pcm->NlpType                    = Pkt->NlpType = 0;
   Pcm->NlpThreshold               = Pkt->NlpThreshold = 0;
   Pcm->NlpUpperLimitThreshConv    = Pkt->NlpUpperLimitThreshConv = 0;
   Pcm->NlpUpperLimitThreshPreconv = Pkt->NlpUpperLimitThreshPreconv = 0;
   Pcm->NlpMaxSupp                 = Pkt->NlpMaxSupp   = 0;
   Pcm->CngThreshold               = Pkt->CngThreshold = 0;

   Pcm->AdaptLimit         = Pkt->AdaptLimit         = 1;
   Pcm->CrossCorrLimit     = Pkt->CrossCorrLimit     = 1;
   Pcm->DblTalkThresh      = Pkt->DblTalkThresh      = 0;
   Pcm->MaxDoubleTalkThres = Pkt->MaxDoubleTalkThres = 0;

   Pcm->AdaptEnable        = Pkt->AdaptEnable = 0;
   Pcm->G165DetEnable      = Pkt->G165DetEnable = 0;
   Pcm->MixedFourWireMode  = Pkt->MixedFourWireMode = 0;
   Pcm->ReconvergenceCheckEnable = Pkt->ReconvergenceCheckEnable = 0;
}
static void setSysMax (GpakSystemParms_t *params) {
   GpakAGCParms_t *AGC_A, *AGC_B;
   GpakECParams_t *Pcm, *Pkt;

   Pcm   = &params->PcmEc;
   Pkt   = &params->PktEc;
   AGC_A = &params->AGC_A;
   AGC_B = &params->AGC_B;

   AGC_A->TargetPower = AGC_B->TargetPower =   0;
   AGC_A->LossLimit   = AGC_B->LossLimit   =   0;
   AGC_A->GainLimit   = AGC_B->GainLimit   =  23;
   AGC_A->LowSignal   = AGC_B->LowSignal   = -20;

   params->VadNoiseFloor  = 0;
   params->VadHangTime    = 4000;
   params->VadWindowSize  = 10;
   params->VadReport      = 1;

   params->ConfNumDominant = sysCfg.MaxNumConfChans;
   params->MaxConfNoiseSuppress = 95;

   Pcm->TailLength = sysCfg.PcmMaxTailLen;
   Pkt->TailLength = sysCfg.PktMaxTailLen;

   Pcm->NumFirSegments = sysCfg.PcmMaxFirSegs;
   Pkt->NumFirSegments = sysCfg.PktMaxFirSegs;
   Pcm->FirSegmentLen  = sysCfg.PcmMaxFirSegLen * 8;
   Pkt->FirSegmentLen  = sysCfg.PktMaxFirSegLen * 8;

   Pcm->FirTapCheckPeriod = Pkt->FirTapCheckPeriod = 8000;

   Pcm->NlpType                    = Pkt->NlpType = 6;
   Pcm->NlpThreshold               = Pkt->NlpThreshold = 40;
   Pcm->NlpUpperLimitThreshConv    = Pkt->NlpUpperLimitThreshConv = 90;
   Pcm->NlpUpperLimitThreshPreconv = Pkt->NlpUpperLimitThreshPreconv = 90;
   Pcm->NlpMaxSupp                 = Pkt->NlpMaxSupp   = 90;
   Pcm->CngThreshold               = Pkt->CngThreshold = 96;

   Pcm->AdaptLimit         = Pkt->AdaptLimit         = 100;
   Pcm->CrossCorrLimit     = Pkt->CrossCorrLimit     = 100;
   Pcm->DblTalkThresh      = Pkt->DblTalkThresh      = 18;
   Pcm->MaxDoubleTalkThres = Pkt->MaxDoubleTalkThres = 50;

   Pcm->AdaptEnable = Pkt->AdaptEnable = 1;
   Pcm->G165DetEnable = Pkt->G165DetEnable = 1;
   Pcm->MixedFourWireMode = Pkt->MixedFourWireMode = 1;
   Pcm->ReconvergenceCheckEnable = Pkt->ReconvergenceCheckEnable = 1;

}
static void setSysDflt(GpakSystemParms_t *params) {
   *params = sysParams;
}

static int dumpAGCParms (GpakAGCParms_t *AGC_A, GpakAGCParms_t *AGC_B) {
   int mods;
   if (!(sysCfg.CfgEnableFlags & (AGCEnableBit))) {
      ADT_printf ("AGC not configured\n");
      return 0;
   }
   ADT_printf ("\n---- AGC ------------    A     B\n");
   ADT_printf ("  Target Power [dBm]:  %3d   %3d\n", AGC_A->TargetPower, AGC_B->TargetPower);
   ADT_printf ("Max attenuation [dB]:  %3d   %3d\n", AGC_A->LossLimit,   AGC_B->LossLimit);
   ADT_printf ("       Max gain [dB]:  %3d   %3d\n", AGC_A->GainLimit,   AGC_B->GainLimit);
   ADT_printf ("    Low signal [dBm]:  %3d   %3d\n", AGC_A->LowSignal,   AGC_B->LowSignal);

   if (sysParamsModify != SYS_PROMPT) return 0;

   mods = 0;
   mods |= ADT_UpdateInt16Value ("   A Power[dBm]", &AGC_A->TargetPower);
   mods |= ADT_UpdateInt16Value ("     A Loss[dB]", &AGC_A->LossLimit);
   mods |= ADT_UpdateInt16Value ("     A Gain[dB]", &AGC_A->GainLimit);
   mods |= ADT_UpdateInt16Value ("A LowPower[dBm]", &AGC_A->LowSignal);

   mods |= ADT_UpdateInt16Value ("   B Power[dBm]", &AGC_B->TargetPower) << 1;
   mods |= ADT_UpdateInt16Value ("     B Loss[dB]", &AGC_B->LossLimit) << 1;
   mods |= ADT_UpdateInt16Value ("     B Gain[dB]", &AGC_B->GainLimit) << 1;
   mods |= ADT_UpdateInt16Value ("B LowPower[dBm]", &AGC_B->LowSignal) << 1;
   return mods;
}
static void dumpECParms  (GpakECParams_t *Pcm, GpakECParams_t *Pkt) {
   if ((sysCfg.NumPcmEcans + sysCfg.NumPktEcans) == 0)
      return;

   ADT_printf ("\n----- EC ------------   PCM   PKT\n");
   ADT_printf ("               Count:   %3u   %3u\n", sysCfg.NumPcmEcans, sysCfg.NumPktEcans);
   ADT_printf ("           Tail [ms]:   %3u   %3u\n", Pcm->TailLength, Pkt->TailLength);
   ADT_printf ("            Segments:   %3u   %3u\n", Pcm->NumFirSegments, Pkt->NumFirSegments);
   ADT_printf ("    Segment size[ms]:   %3u   %3u\n", Pcm->FirSegmentLen, Pkt->FirSegmentLen);
   ADT_printf ("      Tap check [ms]:   %3u   %3u\n", Pcm->FirTapCheckPeriod, Pkt->FirTapCheckPeriod);
   
   ADT_printf ("\n--- Adapt                %3s   %3s\n", EnabledStr [Pcm->AdaptEnable & 1], EnabledStr [Pkt->AdaptEnable & 1]);
   ADT_printf ("     Limit [%% frame]:   %3u   %3u\n", Pcm->AdaptLimit, Pkt->AdaptLimit);
   ADT_printf (" Correlate [%% frame]:   %3u   %3u\n", Pcm->CrossCorrLimit, Pkt->CrossCorrLimit);

   ADT_printf ("\n--- NLP\n");
   ADT_printf ("                Type:   %3u   %3u\n", Pcm->NlpType, Pkt->NlpType);
   ADT_printf ("           level[dB]:   %3d   %3d\n", Pcm->NlpThreshold, Pkt->NlpThreshold);
   ADT_printf ("     cnvrg level[dB]:   %3d   %3d\n", Pcm->NlpUpperLimitThreshConv, Pkt->NlpUpperLimitThreshConv);
   ADT_printf ("  precnvrg level[dB]:   %3d   %3d\n", Pcm->NlpUpperLimitThreshPreconv, Pkt->NlpUpperLimitThreshPreconv);
   ADT_printf (" max suppression[dB]:   %3d   %3d\n", Pcm->NlpMaxSupp, Pkt->NlpMaxSupp);
   ADT_printf ("       CNG level[dB]:   %3d   %3d\n", Pcm->CngThreshold, Pkt->CngThreshold);
   
   ADT_printf ("\n--- Double Talk\n");
   ADT_printf ("       Threshold[dB]:   %3d   %3d\n", Pcm->DblTalkThresh, Pkt->DblTalkThresh);
   ADT_printf ("   Max Threshold[dB]:   %3d   %3d\n", Pcm->MaxDoubleTalkThres, Pkt->MaxDoubleTalkThres);

   ADT_printf ("\n--- Features\n");
   ADT_printf ("            G165 Det:   %3s   %3s\n", EnabledStr [Pcm->G165DetEnable & 1], EnabledStr [Pkt->G165DetEnable & 1]);
   ADT_printf ("   Mixed 4-wire mode:   %3s   %3s\n", EnabledStr [Pcm->MixedFourWireMode & 1], EnabledStr [Pkt->MixedFourWireMode & 1]);
   ADT_printf ("    Reconverge check:   %3s   %3s\n", EnabledStr [Pcm->ReconvergenceCheckEnable & 1], EnabledStr [Pkt->ReconvergenceCheckEnable & 1]);
}
static void dumpCIDParms (GpakCIDParms_t *Cid) {

   if (Cid->fskType == fskNone) return;

   ADT_printf ("\n---- CID ------------\n");
   ADT_printf ("        fsk type: %d\n", Cid->fskType);
   ADT_printf ("        msg type: %d\n", Cid->msgType);
   ADT_printf ("  fsk Level[dBm]: %d\n", Cid->fskLevel);
   ADT_printf ("type 2 Caller ID: %s\n", type2CIDString[Cid->type2CID & 1]);
   ADT_printf ("  Seizure[bytes]: %d\n", Cid->numSeizureBytes);
   ADT_printf ("  Marking[bytes]: %d\n", Cid->numMarkBytes);
   ADT_printf ("Postamble[bytes]: %d\n", Cid->numPostambleBytes);
   
}
static void dumpAECParms (GpakAECParms_t *AEC, ADT_UInt32 DspId) {
   if (sysCfg.AECInstances != 0)
      return;
#if 0 // jdc must update AEC
   ADT_printf ("\n------- Configurable AEC parameters DSP %d --------\n", DspId);
   ADT_printf ("\n----- AEC ------------\n");
   ADT_printf ("      Active Tail [ms]: %3u\n", AEC->activeTailLengthMSec);
   ADT_printf ("       Total Tail [ms]: %3u\n", AEC->totalTailLengthMSec);
   ADT_printf ("  rx staturation [dBm]: %3d\n", AEC->rxSaturateLeveldBm);
   ADT_printf ("     fixedGain [.1 dB]: %3d\n", AEC->fixedGaindB10);
   ADT_printf ("max expected lows [dB]: %3d\n", AEC->worstExpectedERLdB);
   
//   ADT_printf ("\n--- Noise reduction  %3s\n", EnabledStr [AEC->noiseReductionEnable & 1]);
   ADT_printf ("\n--- Noise reduction  %3s\n", EnabledStr [AEC->noiseReduction1Setting & 1]);

   ADT_printf ("     max rx level [dBm]: %3d\n", AEC->maxRxNoiseLeveldBm);
   
   ADT_printf ("\n--- NLP\n");
   ADT_printf ("      Max threshold[dB]: %3d\n", AEC->maxTxNLPThresholddB);
   ADT_printf ("Max tx attenuation [dB]: %3d\n", AEC->maxTxLossdB);
   ADT_printf ("Max rx attenuation [dB]: %3d\n", AEC->maxRxLossdB);
   ADT_printf (" Target threshold [dBm]: %3d\n", AEC->targetResidualLeveldBm);
   
   ADT_printf ("\n--- AGC %3s\n", EnabledStr [AEC->agcEnable & 1]);
   ADT_printf ("         max gain[dB]: %3d\n", AEC->agcMaxGaindB);
   ADT_printf ("  max attenuation[dB]: %3d\n", AEC->agcMaxLossdB);
   ADT_printf ("    target level[dBm]: %3d\n", AEC->agcTargetLeveldBm);
   ADT_printf ("signal threshold[dBm]: %3d\n", AEC->agcLowSigThreshdBm);

   ADT_printf ("\n--- Training\n");
   ADT_printf ("Duration[ms]: %u\n", AEC->maxTrainingTimeMSec);
   ADT_printf ("RxNoise[dBm]: %d\n", AEC->trainingRxNoiseLeveldBm);
#endif

}
static int  dumpVADParms (GpakSystemParms_t *params) {
   int mods;
   if (!(sysCfg.CfgEnableFlags & VadEnableBit)) {
      ADT_printf ("VAD not configured\n");
      return 0;
   }

   ADT_printf ("\n---- VAD ------------\n");
   ADT_printf ("Noise floor %d dBM. Hangtime: %d msec  Window: %d msec  Reporting %s\n\n",
           params->VadNoiseFloor, params->VadHangTime, params->VadWindowSize, 
           EnabledStr [params->VadReport & 1]);

   if (sysParamsModify != SYS_PROMPT) return 0;

   mods = 0;
   mods |= ADT_UpdateInt16Value ("       Floor [dBm]", &params->VadNoiseFloor);
   mods |= ADT_UpdateInt16Value ("   HangTime [msec]", &params->VadHangTime);
   mods |= ADT_UpdateInt16Value ("Anal window [msec]", &params->VadWindowSize);
   mods |= ADT_UpdateInt16Value ("        VAD report", (ADT_Int16 *) &params->VadReport);
   return mods;
}
static int dumpCnfrParms (GpakSystemParms_t *params) {
   int mods;
   if (sysCfg.MaxConferences == 0) return 0;

   ADT_printf ("\n---- Conferencing ---\n");
   ADT_printf ("Max dominant %u speakers.  Noise suppress[dB] %u\n",
       params->ConfNumDominant, params->MaxConfNoiseSuppress);

   if (sysParamsModify != SYS_PROMPT) return 0;

   mods = 0;
   mods |= ADT_UpdateInt16Value ("    Dominant spkrs", &params->ConfNumDominant);
   mods |= ADT_UpdateInt16Value ("Noise suppress[dB]", &params->MaxConfNoiseSuppress);
   return mods;
}
void displaySystemData (ADT_UInt32 DspId) {

   GpakApiStatus_t status, parmStatus, aecStatus, cfgStatus;
   ADT_UInt16 agcMods, vadMods, pcmECMods, pktECMods, cnfrMods, cidMods;
   GpakSystemParms_t  parms;
   GpakAECParms_t     aecParms;
   GPAK_SysParmsStat_t dspStatus;

   memset (&parms, 0, sizeof (parms));

   agcMods = 3;
   vadMods   = cnfrMods =  ADT_TRUE;
   pcmECMods = pktECMods =  cidMods = ADT_FALSE;

   cfgStatus     = gpakGetSystemConfig (DspId, &sysCfg);
   if (cfgStatus != GpakApiSuccess) {
      ADT_printf ("Get system configuration failure %d:%d\n", cfgStatus, DSPError [DspId]);
   }

   // Run-time configurable system parameters
   if (sysCfg.AECInstances) {
      aecStatus = gpakReadAECParms (DspId, &aecParms);
      if (aecStatus != AECSuccess) {
         ADT_printf ("Get AEC configuration failure %d:%d\n", aecStatus, DSPError [DspId]);
      } else {
         dumpAECParms (&aecParms, DspId);
      }
   }
   //--------------------------------------------------------------------------
   // Dump conferencing, EC, CID, AGC and VAD
   switch (sysParamsModify) {
   case SYS_MIN:       setSysMin  (&parms); break;
   case SYS_MAX:       setSysMax  (&parms); break;
   case SYS_DEFAULT:   setSysDflt (&parms); break;
   case SYS_PROMPT:
      ADT_printf ("\n------- Configurable system parameters DSP %d --------\n", DspId);
      // dumpECParms  (&parms.PcmEc, &parms.PktEc);
      // dumpCIDParms (&parms.Cid);
      gpakReadSystemParms (DspId, &parms);
      agcMods  = dumpAGCParms (&parms.AGC_A, &parms.AGC_B);
      vadMods  = dumpVADParms (&parms);
      cnfrMods = dumpCnfrParms (&parms);
      break;
   }
   if (sysParamsModify != SYS_NO_CHANGE) {   
      status = gpakWriteSystemParms (DspId, &parms, agcMods, vadMods, pcmECMods, pktECMods, cnfrMods, cidMods, &dspStatus);
      if (status != GpakApiSuccess) {
         ADT_printf ("Write system configuration failure %d:%d:%d\n", status, dspStatus, DSPError [DspId]);
         ADT_getchar ();
      }
      sysParamsModify = SYS_NO_CHANGE;
   }

   parmStatus = gpakReadSystemParms (DspId, &parms);
   if (parmStatus != GpakApiSuccess) {
      ADT_printf ("Get system configuration failure %d:%d\n", parmStatus, DSPError [DspId]);
   } else {
      ADT_printf ("\n------- Configurable system parameters DSP %d --------\n", DspId);
      dumpECParms  (&parms.PcmEc, &parms.PktEc);
      
      dumpCIDParms (&parms.Cid);
      dumpAGCParms (&parms.AGC_A, &parms.AGC_B);
      dumpVADParms (&parms);
      dumpCnfrParms (&parms);
   }


   // Static system configuration
   if (cfgStatus == GpakApiSuccess) {
      ADT_printf ("\n------- Static system configuration DSP %d --------\n", DspId);
      ADT_printf ("Version: %x\n", sysCfg.GpakVersionId);
      ADT_printf ("Channels.  Max: %8u      Active: %8u\n",   sysCfg.MaxChannels,    sysCfg.ActiveChannels);
      ADT_printf ("Confs.     Max: %8u   Attendees: %8u\n",   sysCfg.MaxConferences, sysCfg.MaxNumConfChans);
      ADT_printf ("Config.   Algs: %08x      Frames: %08x\n", sysCfg.CfgEnableFlags, sysCfg.CfgFramesChans);
      ADT_printf ("RTP.   Profile: %8u        Tone: %8u\n",   sysCfg.PacketProfile,  sysCfg.RtpTonePktType);
      if (sysCfg.Port1SupSlots != 0)
         ADT_printf ("TDM1. Slot Cnt: %8u   Supported: %8u\n",   sysCfg.Port1NumSlots,  sysCfg.Port1SupSlots);
      if (sysCfg.Port2SupSlots != 0)
         ADT_printf ("TDM2. Slot Cnt: %8u   Supported: %8u\n",   sysCfg.Port2NumSlots,  sysCfg.Port2SupSlots);
      if (sysCfg.Port3SupSlots != 0)
         ADT_printf ("TDM3. Slot Cnt: %8u   Supported: %8u\n",   sysCfg.Port3NumSlots,  sysCfg.Port3SupSlots);

      if ((sysCfg.NumPcmEcans + sysCfg.NumPktEcans + sysCfg.AECInstances) != 0) {
         ADT_printf ("\nEcho cancellers\n");
         ADT_printf ("            PCM     PKT     AEC\n");
         ADT_printf ("Num.     %5u   %5u   %5u\n", sysCfg.NumPcmEcans,     sysCfg.NumPktEcans, sysCfg.AECInstances);
         ADT_printf ("Tail(ms) %5u   %5u   %5u\n", sysCfg.PcmMaxTailLen,   sysCfg.PktMaxTailLen, sysCfg.AECTailLen);
         ADT_printf ("Seg(ms)  %5u   %5u\n", sysCfg.PcmMaxFirSegLen, sysCfg.PktMaxFirSegLen);
         ADT_printf ("Seg(cnt) %5u   %5u\n", sysCfg.PcmMaxFirSegs,   sysCfg.PktMaxFirSegs);
      }

   }
   return;
}
//}
const char *lbString[] = {
"NULL STRING",
"NO_LOOP",
"LOOPBACK", 
"CROSSOVER", 
"DSP_LOOP"};
const char *enabString[] =  {"Disabled","Enabled"};
const char *faxString[] =  {"Disabled","FaxOnly","FaxVoice"};
const char *cidString[] =  {"Disabled","Rx","Tx","Rx+Tx"};

void displayToneString(int tone) {
    if (tone == Null_tone) {
        ADT_printf("DISABLED\n"); 
        return;
    }
    if (tone & DTMF_tone)
        ADT_printf("DTMF");
    if (tone &  Tone_Squelch)
        ADT_printf("+SUPP");
    if (tone &  MFR1_tone)
        ADT_printf(" MFR1");
    if (tone &  MFR2Fwd_tone)
        ADT_printf(" MFR2F");
    if (tone &  MFR2Back_tone)
        ADT_printf(" MFR2R");
    if (tone &  CallProg_tone)
        ADT_printf(" CPRG");
    if (tone &  Tone_Relay)
        ADT_printf(" RELAY");
    if (tone &  Tone_Regen)
        ADT_printf(" REGEN");
    if (tone &  Tone_Generate)
        ADT_printf(" GEN");
    if (tone &  FAX_tone)
        ADT_printf(" FAX");
    if (tone &  Notify_Host)
        ADT_printf(" NOTIFY");
    if (tone &  Arbit_tone)
        ADT_printf(" ARB");

    ADT_printf("\n");
}
void displayCodecs(GpakCodecs codec) {
    switch (codec) {
    case     PCMU_64:   ADT_printf("PCMU_64\n");    break;
    case     PCMA_64:   ADT_printf("PCMA_64\n");    break;
    case     L8:        ADT_printf("L8\n");         break;
    case     L16:       ADT_printf("L16\n");        break;
    case     G722_64:   ADT_printf("G722\n");       break;
    case     G722_56:   ADT_printf("G722_56\n");    break;
    case     G722_48:   ADT_printf("G722_48\n");    break;
    case     G723_63:   ADT_printf("G723_63\n");    break;
    case     G723_53:   ADT_printf("G723_53\n");    break;
    case     G723_53A:  ADT_printf("G723_53A\n");   break;
    case     G726_40:   ADT_printf("G726_40\n");    break;
    case     G726_32:   ADT_printf("G726_32\n");    break;
    case     G726_24:   ADT_printf("G726_24\n");    break;
    case     G726_16:   ADT_printf("G726_16\n");    break;
    case     G728_16:   ADT_printf("G728_16\n");    break;
    case     G728_12:   ADT_printf("G728_12\n");    break;
    case     G728_9:    ADT_printf("G728_9\n");     break;
    case     G729:      ADT_printf("G729\n");       break;
    case     G729AB:    ADT_printf("G729AB\n");     break;
    case     G729D:     ADT_printf("G729D\n");      break;
    case     G729E:     ADT_printf("G729E\n");      break;
    case     CircuitPkt:   ADT_printf("Circuit\n"); break;
    case     PCMU_56:   ADT_printf("PCMU_56\n");    break;
    case     PCMU_48:   ADT_printf("PCMU_48\n");    break;
    case     PCMA_56:   ADT_printf("PCMA_56\n");    break;
    case     PCMA_48:   ADT_printf("PCMA_48\n");    break;
    case     MELP:      ADT_printf("MELP\n");       break;
    case     G723_63A:  ADT_printf("G723_63A\n");   break;
    case     Speex:     ADT_printf("Speex\n");      break;
    case     MELP_E:    ADT_printf("MELP_E\n");     break;
    case     ADT_4800:  ADT_printf("ADT_4800\n");   break;
    case     AMR_475:   ADT_printf("AMR_475\n");    break;
    case     AMR_515:   ADT_printf("AMR_515\n");    break;
    case     AMR_590:   ADT_printf("AMR_590\n");    break;
    case     AMR_670:   ADT_printf("AMR_670\n");    break;
    case     AMR_740:   ADT_printf("AMR_740\n");    break;
    case     AMR_795:   ADT_printf("AMR_795\n");    break;
    case     AMR_1020:  ADT_printf("AMR_1020\n");   break;
    case     AMR_1220:  ADT_printf("AMR_1220\n");   break;
    case     L8_WB:     ADT_printf("L8_WB\n");      break;
    case     L16_WB:    ADT_printf("L16_WB\n");     break;
    case     SpeexWB:   ADT_printf("SpeexWB\n");    break;
    case     PCMU_WB:   ADT_printf("PCMU_WB\n");    break;
    case     PCMA_WB:   ADT_printf("PCMA_WB\n");    break;
    case     CustomCodec_WB: ADT_printf("CustomWB\n"); break;
    case     DefaultCodec:   ADT_printf("Default\n");  break;
    case     CustomCodec:    ADT_printf("Custom\n");   break;
    case     NullCodec:      ADT_printf("NULL\n");     break;
    default: ADT_printf("UNRECOGNIZED CODEC\n");    break;
    };
}
void displayHostSettings(chanCfg *chan) {

    ADT_printf ("LoopbackMode = %s\n",lbString[chan->loopback]);
    ADT_printf ("PCM_EC = %s\n",enabString[chan->PCM_EC]);
    ADT_printf ("PKT_EC = %s\n",enabString[chan->PKT_EC]);
    ADT_printf ("AEC = %s\n",enabString[chan->AEC]);
    ADT_printf ("AGC = %s\n",enabString[chan->AGC]);
    ADT_printf ("VAD = %s\n",enabString[chan->VAD]);
    ADT_printf ("NOISE = %s\n",enabString[chan->NOISE]);
    ADT_printf ("FAX = %s\n",faxString[chan->FAX]);

    ADT_printf ("ENCSZ = %2.1f ms\n",(float)chan->ENCSZ/2.0);
    ADT_printf ("DECSZ = %2.1f ms\n",(float)chan->DECSZ/2.0);

    ADT_printf ("TONEA:");
    displayToneString(chan->TONEA);
    ADT_printf ("TONEB:");
    displayToneString(chan->TONEB);

    ADT_printf ("ENCODE = ");
    displayCodecs(chan->ENCODE);

    ADT_printf ("DECODE = ");
    displayCodecs(chan->DECODE);

    ADT_printf ("RTP_CODEC_PYLD = ");
    displayCodecs(chan->RTP_CODEC_PYLD);
                                                           
    ADT_printf ("CID = %s\n",cidString[chan->CID]);
    ADT_printf ("TG_GAIN = %d\n",chan->TG_GAIN);
    ADT_printf ("IN_GAIN = %d\n",chan->IN_GAIN); 
    ADT_printf ("OUT_GAIN = %d\n",chan->OUT_GAIN); 
   
    ADT_printf ("TDM_PORT = %d\n",chan->TDM_PORT);
    ADT_printf ("NOISEB = %s\n", enabString[chan->NOISEB]);
    ADT_printf ("AGCB = %s\n",enabString[chan->AGCB]);
    
}
#define bit_agcEnable           (1<<31)
#define bit_vadCngEnable        (1<<30)
#define bit_toneGenEnable       (1<<29)
#define bit_dtmfEnable          (1<<28)
#define bit_mfr1Enable          (1<<27)
#define bit_mfr2FwdEnable       (1<<26)
#define bit_mfr2BackEnable      (1<<25)
#define bit_cprgEnable          (1<<24)
#define bit_toneRlyGenEnable    (1<<23)
#define bit_toneRlyDetEnable    (1<<22)
#define bit_dtmfDialEnable      (1<<21)
#define bit_spares16            (5<<16) //16,17,18,19,20
#define bit_g168PcmEnable       (1<<15)
#define bit_g168PktEnable       (1<<14)
#define bit_g168VarAEnable      (1<<13)
#define bit_spares9             (4<<9) 
#define bit_noiseSuppressEnable (1<<8) 
#define bit_spares7             (1<<7) 
#define bit_toneDetEnable_B     (1<<6) 
#define bit_spares5             (1<<5)  
#define bit_rtpHostDelivery     (1<<4) 
#define bit_rtpNetDelivery      (1<<3) 
#define bit_networkStack        (1<<2) 
#define bit_apiCacheEnable      (1<<1) 
#define bit_wbEnable            (1<<0)

void displayBuildOpt() {
int onOff;

    onOff = ((sysCfg.CfgEnableFlags & bit_agcEnable)        != 0);
    ADT_printf ("\nAGC = %s\n",enabString[onOff]);
    onOff = ((sysCfg.CfgEnableFlags & bit_vadCngEnable)     != 0);
    ADT_printf ("VADCNG = %s\n",enabString[onOff]);
    onOff = ((sysCfg.CfgEnableFlags & bit_toneGenEnable)    != 0);       
    ADT_printf ("TG = %s\n",enabString[onOff]);
    onOff = ((sysCfg.CfgEnableFlags & bit_dtmfEnable)       != 0);       
    ADT_printf ("DTMF = %s\n",enabString[onOff]);
    onOff = ((sysCfg.CfgEnableFlags & bit_mfr1Enable)       != 0);       
    ADT_printf ("MFR1 = %s\n",enabString[onOff]);
    onOff = ((sysCfg.CfgEnableFlags & bit_mfr2FwdEnable)    != 0);       
    ADT_printf ("MFR2F = %s\n",enabString[onOff]);
    onOff = ((sysCfg.CfgEnableFlags & bit_mfr2BackEnable)   != 0);      
    ADT_printf ("MFR2R = %s\n",enabString[onOff]);
    onOff = ((sysCfg.CfgEnableFlags & bit_cprgEnable)       != 0);      
    ADT_printf ("CPRG = %s\n",enabString[onOff]);
    onOff = ((sysCfg.CfgEnableFlags & bit_toneRlyGenEnable) != 0);    
    ADT_printf ("TRGEN = %s\n",enabString[onOff]);
    onOff = ((sysCfg.CfgEnableFlags & bit_toneRlyDetEnable) != 0);    
    ADT_printf ("TRDET = %s\n",enabString[onOff]);
    onOff = ((sysCfg.CfgEnableFlags & bit_dtmfDialEnable)   != 0);    
    ADT_printf ("DTMFDIAL = %s\n",enabString[onOff]);
    onOff = ((sysCfg.CfgEnableFlags & bit_g168PcmEnable)    != 0);    
    ADT_printf ("PCM_EC = %s\n",enabString[onOff]);
    onOff = ((sysCfg.CfgEnableFlags & bit_g168PktEnable)    != 0);    
    ADT_printf ("PKT_EC = %s\n",enabString[onOff]);
    onOff = ((sysCfg.CfgEnableFlags & bit_g168VarAEnable)   != 0);    
    ADT_printf ("G168_VARA = %s\n",enabString[onOff]);
    onOff = ((sysCfg.CfgEnableFlags & bit_noiseSuppressEnable) != 0); 
    ADT_printf ("NOISE_SUPP = %s\n",enabString[onOff]);
    onOff = ((sysCfg.CfgEnableFlags & bit_toneDetEnable_B)  != 0);    
    ADT_printf ("TONEDET_B = %s\n",enabString[onOff]);
    onOff = ((sysCfg.CfgEnableFlags & bit_rtpHostDelivery)  != 0);    
    ADT_printf ("RTP_HOST = %s\n",enabString[onOff]);
    onOff = ((sysCfg.CfgEnableFlags & bit_rtpNetDelivery)   != 0);    
    ADT_printf ("RTP_NET = %s\n",enabString[onOff]);
    onOff = ((sysCfg.CfgEnableFlags & bit_networkStack)     != 0);    
    ADT_printf ("NET_STACK = %s\n",enabString[onOff]);
    onOff = ((sysCfg.CfgEnableFlags & bit_apiCacheEnable)   != 0);    
    ADT_printf ("API_CACHE = %s\n",enabString[onOff]);
    onOff = ((sysCfg.CfgEnableFlags & bit_wbEnable)         != 0);    
    ADT_printf ("WB = %s\n",enabString[onOff]);
}

//---------------------------------------------------------
//{ Channel parameter display
static void appendInt (int intVal, char *format, char *Buffer, size_t BufferI8) {
   char intStr[20];
   snprintf (intStr, sizeof (intStr), format, intVal);
   strcat_s (Buffer, BufferI8, intStr);
}
static void appendConferenceInfo (int CfrId, char *Buffer, size_t BufferI8) {
   appendInt (CfrId, "[%d.", Buffer, BufferI8);
   appendInt (ActiveCnfr[CfrId].cnfrHalfMS/2, "%dms]", Buffer, BufferI8);
}
extern void appendToneTypes (GpakToneTypes toneTypes, char *Buffer, size_t BufferI8) {
   if (toneTypes & DTMF_tone)     strcat_s (Buffer, BufferI8, " DTMF");
   if (toneTypes & MFR1_tone)     strcat_s (Buffer, BufferI8, " MFR1");
   if (toneTypes & MFR2Fwd_tone)  strcat_s (Buffer, BufferI8, " MFRF");
   if (toneTypes & MFR2Back_tone) strcat_s (Buffer, BufferI8, " MFRR");
   if (toneTypes & CallProg_tone) strcat_s (Buffer, BufferI8, " CP");
   if (toneTypes & FAX_tone)      strcat_s (Buffer, BufferI8, " FAXD");
   if (toneTypes & Arbit_tone)    strcat_s (Buffer, BufferI8, " ARB");
   if (toneTypes & Tone_Relay)    strcat_s (Buffer, BufferI8, " TR");
   if (toneTypes & Tone_Squelch)  strcat_s (Buffer, BufferI8, " TS");

   if (toneTypes & Tone_Regen)    strcat_s (Buffer, BufferI8, " TRG");
   if (toneTypes & Tone_Generate) strcat_s (Buffer, BufferI8, " TG");

   if (toneTypes & Notify_Host)   strcat_s (Buffer, BufferI8, " NTFY");
   strcat_s (Buffer, BufferI8, " ");
}
extern void appendTDMSlot (int Port, int Slot, char *Buffer, size_t BufferI8) {
   if (Port == SerialPortNull) {
      strcat_s (Buffer, BufferI8, "[NULL]");
      return;
   }
   appendInt (Port,   "[TDM.%u", Buffer, BufferI8);
   appendInt (Slot,   ".%u]",    Buffer, BufferI8);
}
extern void appendChannelCodecs (GpakCodecs Coding, GpakFrameHalfMS FrameHalfMS, char *Buffer, size_t BufferI8) {
   char size[10];
   strcat_s (Buffer, BufferI8, "[");
   switch (Coding) {
   catEnum (Buffer, BufferI8, PCMU_64);
   catEnum (Buffer, BufferI8, PCMU_WB);
   catEnum (Buffer, BufferI8, PCMA_64);
   catEnum (Buffer, BufferI8, PCMA_WB);
   catEnum (Buffer, BufferI8, L8);
   catEnum (Buffer, BufferI8, L8_WB);
   catEnum (Buffer, BufferI8, L16);
   catEnum (Buffer, BufferI8, L16_WB);
   catEnum (Buffer, BufferI8, G722_64);
   catEnum (Buffer, BufferI8, G722_56);
   catEnum (Buffer, BufferI8, G722_48);
   catEnum (Buffer, BufferI8, G723_63);
   catEnum (Buffer, BufferI8, G723_53);
   catEnum (Buffer, BufferI8, G723_53A);
   catEnum (Buffer, BufferI8, G726_40);
   catEnum (Buffer, BufferI8, G726_32);
   catEnum (Buffer, BufferI8, G726_24);
   catEnum (Buffer, BufferI8, G726_16);
   catEnum (Buffer, BufferI8, G728_16);
   catEnum (Buffer, BufferI8, G728_12);
   catEnum (Buffer, BufferI8, G728_9);
   catEnum (Buffer, BufferI8, G729);
   catEnum (Buffer, BufferI8, G729AB);
   catEnum (Buffer, BufferI8, G729D);
   catEnum (Buffer, BufferI8, G729E);
   catEnum (Buffer, BufferI8, PCMU_56);
   catEnum (Buffer, BufferI8, PCMU_48);
   catEnum (Buffer, BufferI8, PCMA_56);
   catEnum (Buffer, BufferI8, PCMA_48);
   catEnum (Buffer, BufferI8, MELP);
   catEnum (Buffer, BufferI8, MELP_E);
   catEnum (Buffer, BufferI8, G723_63A);
   catEnum (Buffer, BufferI8, AMR_475);
   catEnum (Buffer, BufferI8, AMR_515);
   catEnum (Buffer, BufferI8, AMR_590);
   catEnum (Buffer, BufferI8, AMR_670);
   catEnum (Buffer, BufferI8, AMR_740);
   catEnum (Buffer, BufferI8, AMR_795);
   catEnum (Buffer, BufferI8, AMR_1020);
   catEnum (Buffer, BufferI8, AMR_1220);
   catEnum (Buffer, BufferI8, ADT_4800);
   catEnum (Buffer, BufferI8, Speex);
   catEnum (Buffer, BufferI8, SpeexWB);
   catEnum (Buffer, BufferI8, CustomCodec);
   catEnum (Buffer, BufferI8, CustomCodec_WB);
   default:
      strncpy (Buffer, "?", BufferI8);
   }
   snprintf (size, sizeof (size), ".%dms", FrameHalfMS/2);
   strcat_s (Buffer, BufferI8 - strlen (Buffer), size);
   strcat_s (Buffer, BufferI8, "]");
}
static void appendChannelEndPoints (GpakChanType channelType,  GpakChannelConfig_t *chanCfg, char *Buffer, size_t BufferI8) {
   int CfrId;
   switch (channelType) {
   case pcmToPcm: {
      PcmPcmCfg_t *PcmPcm = &chanCfg->PcmPcm;
      appendTDMSlot (PcmPcm->InPortA,  PcmPcm->InSlotA, Buffer, BufferI8);
      strcat_s (Buffer, BufferI8, "-");
      appendTDMSlot (PcmPcm->OutPortB, PcmPcm->OutSlotB, Buffer, BufferI8);

      strcat_s (Buffer, BufferI8, "  ...  ");

      appendTDMSlot (PcmPcm->InPortB,   PcmPcm->InSlotB, Buffer, BufferI8);
      strcat_s (Buffer, BufferI8, "-");
      appendTDMSlot (PcmPcm->OutPortA,  PcmPcm->OutSlotA, Buffer, BufferI8);
      appendInt (PcmPcm->FrameHalfMS/2, "%u ms  ", Buffer, BufferI8);
      }
      break;

   case pcmToPacket: {
      PcmPktCfg_t *PcmPkt = &chanCfg->PcmPkt;

      appendTDMSlot (PcmPkt->PcmInPort, PcmPkt->PcmInSlot, Buffer, BufferI8);
      strcat_s (Buffer, BufferI8, "-");
      appendChannelCodecs (PcmPkt->PktOutCoding, PcmPkt->PktOutFrameHalfMS, Buffer, BufferI8);

      strcat_s (Buffer, BufferI8, "...");

      appendChannelCodecs (PcmPkt->PktInCoding, PcmPkt->PktInFrameHalfMS, Buffer, BufferI8);
      strcat_s (Buffer, BufferI8, "-");
      appendTDMSlot (PcmPkt->PcmOutPort, PcmPkt->PcmOutSlot, Buffer, BufferI8);
      }
      break;
      
   case packetToPacket:  {
      PktPktCfg_t *PktPkt = &chanCfg->PktPkt;
      appendChannelCodecs (PktPkt->PktInCodingA, PktPkt->PktInFrameHalfMSA, Buffer, BufferI8);
      strcat_s (Buffer, BufferI8, "<->");
      appendChannelCodecs (PktPkt->PktOutCodingA, PktPkt->PktOutFrameHalfMSA, Buffer, BufferI8);
      }
      break;

   case conferencePcm: {
      CnfPcmCfg_t *CfrPcm = &chanCfg->ConferPcm;
      CfrId = CfrPcm->ConferenceId;

      appendTDMSlot (CfrPcm->PcmInPort, CfrPcm->PcmInSlot, Buffer, BufferI8);
      strcat_s (Buffer, BufferI8, "-");
      appendConferenceInfo (CfrId, Buffer, BufferI8);
      strcat_s (Buffer, BufferI8, "-");
      appendTDMSlot (CfrPcm->PcmOutPort, CfrPcm->PcmOutSlot, Buffer, BufferI8);

      }
      break;

   case conferenceMultiRate:
   case conferencePacket: {
      CnfPktCfg_t *CfrPkt = &chanCfg->ConferPkt;
      CfrId = CfrPkt->ConferenceId;
      appendChannelCodecs (CfrPkt->PktInCoding, CfrPkt->PktFrameHalfMS, Buffer, BufferI8);
      strcat_s (Buffer, BufferI8, "-");
      appendConferenceInfo (CfrId, Buffer, BufferI8);
      strcat_s (Buffer, BufferI8, "-");
      appendChannelCodecs (CfrPkt->PktOutCoding, CfrPkt->PktFrameHalfMS, Buffer, BufferI8);
      }
      break;

   case conferenceComposite: {
      CnfCmpCfg_t *CfrLstn = &chanCfg->ConferComp;
      CfrId = CfrLstn->ConferenceId;
      appendInt (ActiveCnfr[CfrId].cnfrHalfMS/2, "%dms", Buffer, BufferI8);
      strcat_s (Buffer, BufferI8, "-");
      if (CfrLstn->PcmOutPort != SerialPortNull) {
         appendTDMSlot (CfrLstn->PcmOutPort, CfrLstn->PcmOutSlot, Buffer, BufferI8);
      }
      if (CfrLstn->PktOutCoding != NullCodec)
         appendChannelCodecs (CfrLstn->PktOutCoding, ActiveCnfr[CfrId].cnfrHalfMS, Buffer, BufferI8);
      
      }
      break;

   case backgroundTranscoder: {
      LbCdrCgf_t *TransCode = &chanCfg->LpbkCoder;

      appendChannelCodecs (TransCode->PktInCoding, TransCode->PktFrameHalfMS, Buffer, BufferI8);
      strcat_s (Buffer, BufferI8, "-");
      appendChannelCodecs (TransCode->PktOutCoding, TransCode->PktFrameHalfMS, Buffer, BufferI8);
      }
      break;
   }

}
static void appendChannelOpts   (GpakChanType channelType, GpakChannelConfig_t *chanCfg, char *Buffer, size_t BufferI8) {
   int CfrId, blanks;

   appendChannelEndPoints (channelType,  chanCfg, Buffer, BufferI8);
   blanks = 30 - strlen (Buffer);
   if (0 < blanks) memset (Buffer, ' ', blanks);

   switch (channelType) {
   case pcmToPcm: {
      PcmPcmCfg_t *PcmPcm = &chanCfg->PcmPcm;
      strcat_s (Buffer, BufferI8, "A:"); 
      if (PcmPcm->EcanEnableA)    strcat_s (Buffer, BufferI8, " EC");
      if (PcmPcm->AECEcanEnableA) strcat_s (Buffer, BufferI8, " AEC");
      if (PcmPcm->NoiseSuppressA) strcat_s (Buffer, BufferI8, " NS");
      if (PcmPcm->AgcEnableA)     strcat_s (Buffer, BufferI8, " AGC");
      if (PcmPcm->CIdModeA & CidReceive)  strcat_s (Buffer, BufferI8, " CIDRx");
      if (PcmPcm->CIdModeA & CidTransmit) strcat_s (Buffer, BufferI8, " CIDTx");
      appendToneTypes (PcmPcm->ToneTypesA, Buffer, BufferI8);

      strcat_s (Buffer, BufferI8, "    B:"); 
      if (PcmPcm->EcanEnableB)    strcat_s (Buffer, BufferI8, " EC");
      if (PcmPcm->AECEcanEnableB) strcat_s (Buffer, BufferI8, " AEC");
      if (PcmPcm->NoiseSuppressB) strcat_s (Buffer, BufferI8, " NS");
      if (PcmPcm->AgcEnableB)     strcat_s (Buffer, BufferI8, " AGC");
      if (PcmPcm->CIdModeB & CidReceive)  strcat_s (Buffer, BufferI8, " CIDRx");
      if (PcmPcm->CIdModeB & CidTransmit) strcat_s (Buffer, BufferI8, " CIDTx");
      appendToneTypes (PcmPcm->ToneTypesB, Buffer, BufferI8);

      }
      break;

   case pcmToPacket: {
      PcmPktCfg_t *PcmPkt = &chanCfg->PcmPkt;

      if (PcmPkt->PcmEcanEnable) strcat_s (Buffer, BufferI8, " LEC");
      if (PcmPkt->PktEcanEnable) strcat_s (Buffer, BufferI8, " PEC");
      if (PcmPkt->AECEcanEnable) strcat_s (Buffer, BufferI8, " AEC");
      if (PcmPkt->VadEnable)     strcat_s (Buffer, BufferI8, " VAD");
      if (PcmPkt->AgcEnable)     strcat_s (Buffer, BufferI8, " AGC");
      appendToneTypes (PcmPkt->ToneTypes, Buffer, BufferI8);
      if (PcmPkt->CIdMode & CidReceive)  strcat_s (Buffer, BufferI8, " CIDRx");
      if (PcmPkt->CIdMode & CidTransmit) strcat_s (Buffer, BufferI8, " CIDTx");
      if (PcmPkt->FaxMode == faxOnly)  strcat_s (Buffer, BufferI8, " T38");
      if (PcmPkt->FaxMode == faxVoice) strcat_s (Buffer, BufferI8, " T38+");
      }
      break;

   case packetToPacket:  {
      PktPktCfg_t *PktPkt = &chanCfg->PktPkt;
      if (PktPkt->EcanEnableA) strcat_s (Buffer, BufferI8, " PEC");
      if (PktPkt->VadEnableA)  strcat_s (Buffer, BufferI8, " VAD");
      if (PktPkt->AgcEnableA)  strcat_s (Buffer, BufferI8, " AGC");
      appendToneTypes (PktPkt->ToneTypesA, Buffer, BufferI8);
      }
      break;

   case conferencePcm: {
      CnfPcmCfg_t *CfrPcm = &chanCfg->ConferPcm;
      CfrId = CfrPcm->ConferenceId;

      if (CfrPcm->EcanEnable)    strcat_s (Buffer, BufferI8, " LEC");
      if (CfrPcm->AECEcanEnable) strcat_s (Buffer, BufferI8, " AEC");
      if (CfrPcm->AgcInEnable)   strcat_s (Buffer, BufferI8, " ACGIn");
      if (CfrPcm->AgcOutEnable)  strcat_s (Buffer, BufferI8, " ACGOut");
      appendToneTypes (CfrPcm->ToneTypes, Buffer, BufferI8);
      }
      break;

   case conferenceMultiRate:
   case conferencePacket: {
      CnfPktCfg_t *CfrPkt = &chanCfg->ConferPkt;
      CfrId = CfrPkt->ConferenceId;

      if (CfrPkt->EcanEnable)   strcat_s (Buffer, BufferI8, " LEC");
      if (CfrPkt->VadEnable)    strcat_s (Buffer, BufferI8, " VAD");
      if (CfrPkt->AgcInEnable)  strcat_s (Buffer, BufferI8, " AGCIn");
      if (CfrPkt->AgcOutEnable) strcat_s (Buffer, BufferI8, " AGCOut");
      appendToneTypes (CfrPkt->ToneTypes, Buffer, BufferI8);

      }
      break;

   case conferenceComposite: {
      CnfCmpCfg_t *CfrLstn = &chanCfg->ConferComp;
      CfrId = CfrLstn->ConferenceId;

      if (CfrLstn->VadEnable) strcpy_s (Buffer, BufferI8, " VAD");          /* VAD enable */
      }
      break;

   case backgroundTranscoder: {
      }
      break;
   }
}
static void appendChannelGains  (GpakChanType channelType, GpakChannelConfig_t *chanCfg, char *Buffer, size_t BufferI8) {
   switch (channelType) {
   case pcmToPcm: {
      PcmPcmCfg_t *PcmPcm = &chanCfg->PcmPcm;
      appendInt (PcmPcm->InputGainG3A,   "%9d", Buffer, BufferI8);
      appendInt (PcmPcm->OutputGainG2A,  "%7d", Buffer, BufferI8);
      appendInt (PcmPcm->ToneGenGainG1A, "%7d", Buffer, BufferI8);

      appendInt (PcmPcm->InputGainG3B,   "%9d", Buffer, BufferI8);
      appendInt (PcmPcm->OutputGainG2B,  "%7d", Buffer, BufferI8);
      appendInt (PcmPcm->ToneGenGainG1B, "%7d", Buffer, BufferI8);
      }
      break;
      
   case pcmToPacket: {
      PcmPktCfg_t *PcmPkt = &chanCfg->PcmPkt;
      appendInt (PcmPkt->InputGainG3,   "%9d", Buffer, BufferI8);
      appendInt (PcmPkt->OutputGainG2,  "%7d", Buffer, BufferI8);
      appendInt (PcmPkt->ToneGenGainG1, "%7d", Buffer, BufferI8);
      }
      break;
      
   case packetToPacket:  {
      PktPktCfg_t *PktPkt = &chanCfg->PktPkt;
      appendInt (PktPkt->InputGainG3A,   "%9d", Buffer, BufferI8);
      appendInt (PktPkt->OutputGainG2A,  "%7d", Buffer, BufferI8);
      appendInt (0, "    N/A", Buffer, BufferI8);

      appendInt (PktPkt->InputGainG3B,   "%9d", Buffer, BufferI8);
      appendInt (PktPkt->OutputGainG2B,  "%7d", Buffer, BufferI8);
      appendInt (0, "    N/A", Buffer, BufferI8);
      }
      break;

   case conferencePcm: {
      CnfPcmCfg_t *CfrPcm = &chanCfg->ConferPcm;
      appendInt (CfrPcm->InputGainG3,   "%9d", Buffer, BufferI8);
      appendInt (CfrPcm->OutputGainG2,  "%7d", Buffer, BufferI8);
      appendInt (CfrPcm->ToneGenGainG1, "%7d", Buffer, BufferI8);
      }
      break;

   case conferenceMultiRate:
   case conferencePacket: {
      CnfPktCfg_t *CfrPkt = &chanCfg->ConferPkt;
      appendInt (CfrPkt->InputGainG3,   "%9d", Buffer, BufferI8);
      appendInt (CfrPkt->OutputGainG2,  "%7d", Buffer, BufferI8);
      appendInt (CfrPkt->ToneGenGainG1, "%7d", Buffer, BufferI8);
      }
      break;

   }
}

static void displayChannelStats (ADT_UInt32 DspId) {
   GpakChannelStatus_t      ChanStatus;
   gpakGetChanStatStatus_t  ApiStatus;
   GPAK_ChannelStatusStat_t DspStatus;
   ADT_UInt16 i;

   ADT_printf ("\n          -- ASide --  -- BSide --   ---- Failures ----\n");
   ADT_printf (  "Chan Type ToDSP FmDSP  ToDSP FmDSP   BadHdr OvrFl UndFl\n");
   
   for (i=0; i< sysCfg.MaxChannels-1; i++) {
      DspStatus = Cs_Success;
      ApiStatus = gpakGetChannelStatus (DspId, i, &ChanStatus, &DspStatus);
      if (ApiStatus != GpakApiSuccess) {
         ADT_printf ("%3d ", i);
         ADT_printf ("API ERROR %d.%d.%d\n", ApiStatus, DspStatus, DSPError[DspId]);
         break;
      }
      if (ChanStatus.ChannelType == inactive) continue;
      ADT_printf ("%3d ", i);
      ADT_printf (" %4s", chanType [ChanStatus.ChannelType]);
      ADT_printf (" %5u", ChanStatus.NumPktsToDsp);
      ADT_printf (" %5u", ChanStatus.NumPktsFromDsp);
      ADT_printf (" %6u", ChanStatus.NumPktsToDspB);
      ADT_printf (" %5u", ChanStatus.NumPktsFromDspB);
      ADT_printf (" %8u", ChanStatus.BadPktHeaderCnt);
      ADT_printf (" %5u", ChanStatus.PktOverflowCnt);
      ADT_printf (" %5u", ChanStatus.PktUnderflowCnt);

      ADT_printf ("\n");
   }
}
extern void displayChannelOpts (ADT_UInt32 DspId) {
   GpakChannelStatus_t      ChanStatus;
   gpakGetChanStatStatus_t  ApiStatus;
   GPAK_ChannelStatusStat_t DspStatus;
   char Buffer [220];
   ADT_UInt16 i, chanCnt;

   ADT_printf ("\n          \n");
   ADT_printf ("\nCore Chan Type  Algorithms\n");
   chanCnt = 0;
   for (i=0; i< sysCfg.MaxChannels-1; i++) {
      DspStatus = Cs_Success;
      ApiStatus = gpakGetChannelStatus (DspId, i, &ChanStatus, &DspStatus);
      if (ApiStatus != GpakApiSuccess) {
         ADT_printf ("%3d ", i);
         ADT_printf ("API ERROR %d.%d.%d\n", ApiStatus, DspStatus, DSPError[DspId]);
         break;
      }
      if (ChanStatus.ChannelType == inactive) continue;

      ADT_printf ("%3d %4d ",  CORE_ID(ChanStatus.ChannelId), i);
      if (ChanStatus.ChannelType == customChannel)
         ADT_printf ("  CST");
      else
         ADT_printf (" %4s  ", chanType [ChanStatus.ChannelType]);
      memset (Buffer, 0, sizeof (Buffer));
      appendChannelOpts (ChanStatus.ChannelType, &ChanStatus.ChannelConfig, Buffer, sizeof (Buffer));
      ADT_printf ("%s\n", Buffer);
      chanCnt++;
   }
   if (chanCnt) ADT_printf ("%d channels active\n", chanCnt);
}
static void displayChannelGains (ADT_UInt32 DspId) {
   GpakChannelStatus_t      ChanStatus;
   gpakGetChanStatStatus_t  ApiStatus;
   GPAK_ChannelStatusStat_t DspStatus;
   char Buffer [120];
   ADT_UInt16 i;

   ADT_printf ("\n\n");
   ADT_printf ("            ----- A Gains ------   ----- B Gains ------\n");
   ADT_printf ("Chan Type   --In-- -Out-- -Tone-   --In-- -Out-- -Tone-\n");
   
   for (i=0; i< sysCfg.MaxChannels-1; i++) {
      DspStatus = Cs_Success;
      ApiStatus = gpakGetChannelStatus (DspId, i, &ChanStatus, &DspStatus);
      if (ApiStatus != GpakApiSuccess) {
         ADT_printf ("%3d ", i);
         ADT_printf ("API ERROR %d.%d.\n", ApiStatus, DspStatus, DSPError[DspId]);
         break;
      }
      if (ChanStatus.ChannelType == inactive) continue;

      ADT_printf ("%3d ", i);
      ADT_printf (" %4s", chanType [ChanStatus.ChannelType]);
      memset (Buffer, 0, sizeof (Buffer));
      appendChannelGains (ChanStatus.ChannelType, &ChanStatus.ChannelConfig, Buffer, sizeof (Buffer));
      ADT_printf (Buffer);
      ADT_printf ("\n");
   }
}
static void displayChannelRTPStats (ADT_UInt32 DspId, ADT_Bool sampling) {
   GPAK_RTP_STATS       Stats;
   GPAK_RTP_Stat_Rply_t dspStatus;
   GpakApiStatus_t      ApiStatus;

   ADT_Int16 i, j;
   ADT_Int32 integer;
   ADT_UInt32 decimal;

   if (sysCfg.ActiveChannels == 0) return;

   ADT_printf ("\n          \n");
   ADT_printf ("\nChan    Sent    Rcvd Loss%%    Plyd  Late   OOR  Ovfw  Unfw   Jitter (ms)      Delay (ms)\n");
   if (sampling) j = (sysCfg.ActiveChannels/10) + 1;
   else          j = 1;

   for (i=0; i< sysCfg.MaxChannels-1; ) {
      dspStatus = RTPStat_Success;
      ApiStatus = gpakGetRTPStatistics (DspId, i, &Stats, &dspStatus);

      if ((ApiStatus == GpakApiSuccess) && (dspStatus == RTPStat_Success)) {
         ADT_printf ("%3d ", Stats.ChannelId);
         Stats.LossPercentage += 0.05f;
         integer = (ADT_Int32) Stats.LossPercentage;
         decimal = (ADT_UInt32) ((Stats.LossPercentage - integer) * 10.0f);
         if (Stats.JitterMinMS == ((float) 0x4D000000)) Stats.JitterMinMS = 0;
         ADT_printf ("%8u %7u %3d.%1d %7lu %5lu %5lu %5lu %5lu", Stats.PktsSent, Stats.PktsRcvd,
               integer, decimal,
                Stats.PktsPlayed, Stats.PktsLate, Stats.PktsOutOfRange, Stats.BufferOverflowCnt, 
                Stats.BufferUnderflowCnt);

         ADT_printf ("  %3u/%3u/%3u", (ADT_UInt32) Stats.JitterMinMS, (ADT_UInt32) Stats.JitterCurrentMS,
                       (ADT_UInt32) Stats.JitterMaxMS);

         ADT_printf ("    %2u:%3u/%3u/%3u", Stats.JitterMode, Stats.MinDelayMS, Stats.CurrentDelayMS, 
                 Stats.MaxDelayMS);
         ADT_printf ("\n");
      } else  {
         if (dspStatus != RTPStat_ChannelInactive) { 
            ADT_printf ("%3d ", i);
            ADT_printf ("API ERROR %d.%d.%d\n", ApiStatus, dspStatus, DSPError[DspId]);
            break;
         }
      }
      if (sysCfg.MaxChannels-1 <= i) break;
      if (sysCfg.MaxChannels <= i+j) i=sysCfg.MaxChannels-1;
      else                          i+=j;
   }
}
//}

//-----------------------------------------------------
//{ Generic tone detection
GpakArbTdParams_t GenericToneParams = {
     4,   // num frequencies
     4,   // num of tones
     0,   // min dBm to detect tone
     0    // allowable frequency deviation (% times 10)
};

GpakArbTdToneIdx_t GenericTones[] = {
  { 350, 440, 101 },  // Dial tone
  { 440, 480, 102 },  // Ring back tone
  { 480, 620, 103 },  // Busy tone
  { 350, 620, 104 }   // Test tone
};

ADT_UInt16 GenericFrequencies[] = {
   350, 440, 480, 620
};

static void configGenericToneDetection (ADT_UInt32 DspId) {

   GpakApiStatus_t ApiStatus;
   GPAK_ConfigArbToneStat_t CfgStatus;
   GPAK_ActiveArbToneStat_t ActStatus;

   ApiStatus = gpakConfigArbToneDetect (DspId, 0, &GenericToneParams,
                                      GenericTones, GenericFrequencies, &CfgStatus);
   if ((ApiStatus != GpakApiSuccess) || (CfgStatus != Cat_Success)) {
      ADT_printf ("Generic tone cfg error %d.%d.%d\n", ApiStatus, CfgStatus, DSPError[DspId]);
      return;
   }

   ApiStatus = gpakActiveArbToneDetect (DspId, 0, &ActStatus);
   if ((ApiStatus != GpakApiSuccess) || (ActStatus != Aat_Success)) {
      ADT_printf ("Generic tone activate error %d.%d.%d\n", ApiStatus, ActStatus, DSPError[DspId]);
      return;
   }
}

void configCustomToneDetection (ADT_UInt32 DspId, ADT_UInt16 CfgId, GpakArbTdParams_t *pParams, GpakArbTdToneIdx_t *pTones, ADT_UInt16 *pFreqs) {

   GpakApiStatus_t ApiStatus;
   GPAK_ConfigArbToneStat_t CfgStatus;

   ApiStatus = gpakConfigArbToneDetect (DspId, CfgId, pParams,
                                      pTones, pFreqs, &CfgStatus);
   if ((ApiStatus != GpakApiSuccess) || (CfgStatus != Cat_Success)) {
      ADT_printf ("Custom tone cfg error %d.%d.%d\n", ApiStatus, CfgStatus, DSPError[DspId]);
      return;
   }

}
void activateCustomToneDetection (ADT_UInt32 DspId, ADT_UInt16 CfgId) {

   GpakApiStatus_t ApiStatus;
   GPAK_ActiveArbToneStat_t ActStatus;

   ApiStatus = gpakActiveArbToneDetect (DspId, CfgId, &ActStatus);
   if ((ApiStatus != GpakApiSuccess) || (ActStatus != Aat_Success)) {
      ADT_printf ("Custom tone activate error %d.%d.%d\n", ApiStatus, ActStatus, DSPError[DspId]);
      return;
   }
}
//}

//-----------------------------------------------------
//{ Speex configuration
void configureSpeex (ADT_UInt32 DspId) {
   int quality, complexity, vr, enhance;
   GpakApiStatus_t ApiStat;
   GPAK_SpeexParamsStat_t DspStat;

   ADT_printf ("\n\nSet speex parameters");
   quality    = ADT_getint ("Quality: ");
   complexity = ADT_getint ("Complexity: ");
   vr         = ADT_getint ("Variable rate: ");
   enhance    = ADT_getint ("Perceptual enhancement: ");
   ApiStat = gpakConfigSpeex (DspId, (ADT_UInt8) quality, (ADT_UInt8) complexity, (ADT_Bool) vr,
                                        (ADT_Bool) enhance, &DspStat);
   if (ApiStat == GpakApiSuccess) return;
   
   ADT_printf ("Speex config failure %d:%d:%d\n\n", ApiStat, DspStat, DSPError [DspId]);
}
//}

//-----------------------------------------------------
// Auto status display
void dspGetCoreUsage (ADT_UInt32 DspId) {
   GpakApiStatus_t usageStat;
   ADT_UInt32 AvgCpuUsage [6][8];
   ADT_UInt32 PeakCpuUsage[6][8];
   ADT_UInt32 avg, pk;
   unsigned int i, j;

   usageStat = gpakReadCoreUsage (DspId, MaxCoresPerDSP, (void *) AvgCpuUsage, (void *) PeakCpuUsage);

   if (usageStat != GcuSuccess) {
      ADT_printf ("Read Cpu Usage Failure %d:%d", usageStat, DSPError [DspId]);
      return;
   }

   ADT_printf ("\n   %% CPU Utilization\n");
   ADT_printf ("    CPU:     ");
   for (i=0; i<MaxCoresPerDSP; i++) ADT_printf ("%1d            ", i);
   ADT_printf ("\nFrame");
   for (i=0; i<MaxCoresPerDSP; i++) ADT_printf ("  --Avg/Peak-");
   ADT_printf ("\n");

   for (j=0; j<8; j++) {
      ADT_printf ("%4d ", j);
      for (i=0; i<MaxCoresPerDSP; i++) {
         avg = AvgCpuUsage[i][j];
         if (avg) ADT_printf ("  %3d.%1d/", avg / 10, avg % 10);
         else     ADT_printf ("       /");
         pk  = PeakCpuUsage[i][j];
         if (pk) ADT_printf ("%3d.%1d", pk / 10, pk % 10);
         else    ADT_printf ("     ");
      }
      ADT_printf ("\n");
   }
}
void periodicStatusDisplay (ADT_UInt32  DspId) {

   if (autoSetup) {
      dspGetCoreUsage (DspId);
      if (cyclesSinceSetup++ < cyclesPerSetup) {
         dspTearDown (DspId);
         initChannels (DspId);
         cyclesSinceSetup = 0;
      }
   }
   if (cyclesSinceDisplay++ < cyclesPerDisplay) return;
   cyclesSinceDisplay = 0;

   if (autoUsage) {
      dspGetCoreUsage (DspId);
   } 
   if (sysCfg.ActiveChannels == 0) return;

   if (autoRTP) {
      displayChannelRTPStats (DspId, RTP_dump_all);
   }
   if (autoStats) {
      displayChannelStats (DspId);
   }
}

//-----------------------------------------------------
//{ Event notifications
static void dumpMono (char *filename, ADT_UInt32 dsp, int addr, int lenI32) {
   FILE *outFile;
   ADT_UInt32 value;
   char cw_buff[150];
   if (lenI32 == 0) return;

   fopen_s (&outFile, filename, "wb");
   if (outFile == NULL) { 
      ADT_printf ("Unable to open file %s\n", filename);
      return;
   }
   ADT_printf ("Writing %d bytes from dsp %d at %x to %s\\%s\n", lenI32*4, dsp, addr, getcwd (cw_buff, sizeof (cw_buff)), filename);
   do {
   
      gpakLockAccess (dsp);
      gpakReadDspMemory32 (dsp, addr, 1, &value);
      gpakUnlockAccess (dsp);

      fwrite (&value, 1, 4, outFile);
      addr += 4;
      lenI32 -= 1;
   } while (0 < lenI32);

   fclose (outFile);
   return;
}
static void dumpStereo (char *filename, ADT_UInt32 dsp, int addr1, int len1I32, int addr2, int len2I32, int rate) {
   FILE *outFile;
   char cw_buff[150];
   ADT_UInt32 Mono1[64], Mono2[64];
   ADT_UInt16* mono1 = (ADT_UInt16*) Mono1;
   ADT_UInt16* mono2 = (ADT_UInt16*) Mono2;

   ADT_UInt32 stereo[128], temp;
   ADT_UInt16 sTemp;
   ADT_UInt16* buff;
   int buffI32, lenI32, i;

   if (len1I32 < len2I32) lenI32 = len1I32;
   else                   lenI32 = len2I32;

   if (lenI32 <= 0) return;

   fopen_s (&outFile, filename, "wb");
   if (outFile == NULL) { 
      ADT_printf ("Unable to open file %s\n", filename);
      return;
   }
   ADT_printf ("Writing %d bytes from dsp %d at %x and %x to %s\\%s\n", lenI32*8, dsp, addr1, addr2, getcwd (cw_buff, sizeof (cw_buff)), filename);
      fwrite ("RIFF", 4, 1, outFile);
      temp = (lenI32 * 8) + 36;     fwrite (&temp,  1, 4, outFile);  //  RIFF chunk size.
      fwrite ("WAVE", 4, 1, outFile);
      fwrite ("fmt ", 4, 1, outFile);
      temp = 16;         fwrite (&temp,  1, 4, outFile);  // fmt chunk size
      sTemp = 1;         fwrite (&sTemp, 1, 2, outFile);  // data type
      sTemp = 2;         fwrite (&sTemp, 1, 2, outFile);  // Channel count
      if (rate == 16) {
         temp = 16000;      fwrite (&temp,  1, 4, outFile);  // Samps / second
         temp = 64000;      fwrite (&temp,  1, 4, outFile);  // Bytes / seconds
      } else {
         temp = 8000;       fwrite (&temp,  1, 4, outFile);  // Samps / second
         temp = 32000;      fwrite (&temp,  1, 4, outFile);  // Bytes / seconds
      }
      sTemp = 2;         fwrite (&sTemp, 2, 1, outFile);  // Bytes / sample
      sTemp = 16;        fwrite (&sTemp, 2, 1, outFile);  // Bits / sample
      fwrite ("data", 4, 1, outFile);
      temp = lenI32 * 8; fwrite (&temp, 1, 4, outFile);  // data size I8

   while (1 < lenI32) {
      // Read in 128 16-bit elements from both channels
      gpakLockAccess (dsp);
      gpakReadDspMemory32 (dsp, addr1, 64, Mono1);
      gpakReadDspMemory32 (dsp, addr2, 64, Mono2);
      gpakUnlockAccess (dsp);

      // Multiplex both channels together to form stereo output
      buff = (ADT_UInt16*) stereo;
      for (i=0; i<128; i++) {
         *buff++ = mono1[i];
         *buff++ = mono2[i];
      }

      // Write stereo data to file
      buffI32 = lenI32;
      if (128 < buffI32) buffI32 = 128;
      fwrite (stereo, buffI32, 4, outFile);

      // Update pointers
      addr1 += 256;
      addr2 += 256;
      lenI32 -= (buffI32 / 2);
   }

   fclose (outFile);
   return;
}
static void dumpFaxCapture (ADT_UInt32 dsp, ADT_UInt16 chId, GpakAsyncEventData_t *eventData) {
   struct captureData *evt = (struct captureData *) &eventData->aux.captureData;

   
   if (chId==0) {
      dumpMono   ("T38Debug.txt",      dsp, evt->CAddr, evt->CLenI32);
      dumpStereo ("T38RawStereo.wav",  dsp, evt->AAddr, evt->ALenI32, evt->BAddr, evt->BLenI32, 8000);
   } else {
      dumpStereo ("T38DecodedStereo.wav",  dsp, evt->AAddr, evt->ALenI32, evt->BAddr, evt->BLenI32, 8000);
   }

   ADT_printf ("FAX capture complete on channel %d\n\n", chId);
   return;
}
static void dumpECCapture (ADT_UInt32 dsp, ADT_UInt16 chId, GpakAsyncEventData_t *eventData, int rate) {
   struct captureData *evt = (struct captureData *) &eventData->aux.captureData;

   dumpStereo ("ECInStereo.wav",  dsp, evt->AAddr, evt->ALenI32, evt->BAddr, evt->BLenI32, rate);
   dumpStereo ("ECOutStereo.wav", dsp, evt->CAddr, evt->CLenI32, evt->DAddr, evt->DLenI32, rate);

   ADT_printf ("EC capture complete on channel %d\n\n", chId);
   return;
}
static void dumpChannelCapture (ADT_UInt32 dsp, ADT_UInt16 chId, GpakAsyncEventData_t *eventData, int rate) {
   struct captureData *evt = (struct captureData *) &eventData->aux.captureData;
   char name[80];
   memset (name, 0, sizeof (name));

   snprintf (name, sizeof (name), "Chan%d_Decode_Stereo.wav", chId);
   dumpStereo (name,  dsp, evt->AAddr, evt->ALenI32, evt->BAddr, evt->BLenI32, rate);
   ADT_printf ("Channel capture completed to %s\n\n", name);

   snprintf (name, sizeof (name), "Chan%d_Encode_Stereo.wav", chId);
   dumpStereo (name, dsp, evt->CAddr, evt->CLenI32, evt->DAddr, evt->DLenI32, rate);
   ADT_printf ("Channel capture completed to %s\n\n", name);
   return;
}
static void dumpCallerID (ADT_UInt16 chId, GpakAsyncEventData_t *eventData, int dev) {
   ADT_UInt8 *data;
   int fieldI8, msgI8;
   unsigned int type;
   ADT_printf ("\n\rCIDRx. Message received on chan: %3d.%c.", chId, dev + 'A');
   data = eventData->aux.rxCidEvent.Payload;
   type  = *data++;
   msgI8 = *data++;
   ADT_printf ("\tType=%d. Length=%d  ", type, msgI8);
   if (type == CID_MDMF) {
      type    = *data++;
      fieldI8 = *data++;
   } else {
      fieldI8 = msgI8;
   }
   do {
      msgI8 -= (fieldI8 + 2);
      while (fieldI8--) {
         if (isprint (*data)) ADT_printf ("%c", *data++);
         else                 ADT_printf ("[%x]", *data++);
      }
      ADT_printf (" ");
      type    = *data++;
      fieldI8 = *data++;
   } while (0 < msgI8);
}

static void displayAppErr (GpakAsyncEventData_t *evt, int dspCore) {
   ADT_printf ("Error: %s on core %d\n",evt->aux.customPayload, dspCore);
}
static int printWarning (GpakAsyncEventData_t *evt, int warningValue) {
   static int TxSlipCnt = 0, RxSlipCnt = 0, FrameWarnCnt = 0;
   switch (evt->ToneDevice) {
   case WarnTxDmaSlip:       
      if ((TxSlipCnt++ % 50) != 0) return 0;
      ADT_printf ("\n\rWarning: TDM slips %d on TX Buffer. Port %d\t", TxSlipCnt, warningValue%100);  
      break;

   case WarnRxDmaSlip:
      if ((RxSlipCnt++ % 50) != 0) return 0;
      ADT_printf ("\n\rWarning: TDM slips %d on RX Buffer. Port %d\t", RxSlipCnt, warningValue%100);
      break;

   case WarnFrameSyncError:
      if ((FrameWarnCnt++ % 50) != 0) return 0;
      ADT_printf ("\n\rWarning: TDM port %d frame sync error\t", warningValue);  break;

   case WarnFrameTiming:     ADT_printf ("\n\rCore %d: %d ms framing task MIPS overload\t", (warningValue >> 8),
                                                                                        (warningValue & 0xff));  break;
   case WarnFrameBuffers:    ADT_printf ("\n\rCore %d: %d ms framing task circular buffer reset\t", (warningValue >> 8),
                                                                                           (warningValue & 0xff));  break;
   case WarnPortRestart:     ADT_printf ("\n\rWarning: TDM port restart\t");  break;
   case WarnTDMFailure:      ADT_printf ("\n\rWarning: TDM port %d failure\t", warningValue);  break;

   case WarnApplicationError:  displayAppErr (evt, warningValue);  break;
   case WarnTsip_1:  ADT_printf ("\n\rWarning: TDM port %d Rx overrun at start of frame\t", warningValue); break;
   case WarnTsip_2:  ADT_printf ("\n\rWarning: TDM port %d Rx overrun during frame\t", warningValue); break;
   case WarnTsip_3:  ADT_printf ("\n\rWarning: TDM port %d Rx channel bit map too large\t", warningValue); break;
   case WarnTsip_5:  ADT_printf ("\n\rWarning: TDM port %d Rx size too small\t", warningValue); break;
   
   case WarnTsip_16: ADT_printf ("\n\rWarning: TDM port %d Tx CID Error\t", warningValue); break;

   case WarnTsip_17: ADT_printf ("\n\rWarning: TDM port %d Tx underrun at start of frame\t", warningValue); break;
   case WarnTsip_18: ADT_printf ("\n\rWarning: TDM port %d Tx underrun during frame\t", warningValue); break;
   case WarnTsip_19: ADT_printf ("\n\rWarning: TDM port %d Tx channel bit map too large\t", warningValue); break;
   case WarnTsip_21: ADT_printf ("\n\rWarning: TDM port %d Tx size too small\t", warningValue); break;
   
   case WarnTsipRxFcntErr:  ADT_printf ("\n\rWarning: TDM port %d Rx frame marker error\t", warningValue); break;
   case WarnTsipTxFcntErr:  ADT_printf ("\n\rWarning: TDM port %d Tx frame marker error\t", warningValue); break;
   default:  ADT_printf ("\n\rUnknown warning %d\t", evt->ToneDevice); return 0;
   }
   showTime();
   return 1;
}


void processEvent (ADT_UInt32 Dsp, ADT_UInt16 chId, GpakAsyncEventCode_t EvtCode, GpakAsyncEventData_t *evt) {
   int dev;
   char Dev;

   dev = evt->ToneDevice;
   Dev = dev + 'A';
   switch (EvtCode) {
   case EventDspReady:             ADT_printf ("\n\rDsp %u, Core %u: ready        \t", Dsp, chId);   return;
   case EventWarning:              printWarning (evt, chId); return;

   case EventToneDetect: 
      if ((evt->aux.toneEvent.ToneCode >= 0) && (evt->aux.toneEvent.ToneCode <= 15)) {
            // dtmf tone
        ADT_printf ("\n\rChan: %3d.%c %s for %4d ms\t", chId, Dev,
                     dtmfString[evt->aux.toneEvent.ToneCode], evt->aux.toneEvent.ToneDuration); 
      } 
      else {
        ADT_printf ("\n\rChan: %3d.%c Tone code %3d for %4d ms\t", chId, Dev,
                     evt->aux.toneEvent.ToneCode, evt->aux.toneEvent.ToneDuration); 
        if (evt->aux.toneEvent.ToneCode == 100) ADT_printf ("\n");
      }
      return;

   case EventPlaybackComplete:
   case EventPlaybackHalfWay:
   case EventPlaybackAtEnd:        filePlayback (Dsp, EvtCode, chId, dev, evt); return;

   case EventRecordingBufferFull:  ADT_printf ("\n\rRecording buffer full on channel %3d.%c\t", chId, Dev);        break;
   case EventRecordingHalfWay:
   case EventRecordingAtEnd:
   case EventRecordingStopped:     fileRecord (Dsp, EvtCode, chId, dev, evt);  return;

   case EventFaxComplete:          dumpFaxCapture (Dsp, chId, evt);            break;
   case EventCaptureComplete:      dumpChannelCapture (Dsp, chId, evt, dev);   break; 
   case EventEcCaptureComplete:    dumpECCapture (Dsp, chId, evt, dev);        break; 
   
   case EventLoopbackTeardownComplete: ADT_printf ("\n\rTranscoding ended on channel %3d\t", chId);    break;
   case EventVadStateVoice:        ADT_printf ("\n\rVAD. Voice on channel   %3d.%c\t", chId, Dev);     return;
   case EventVadStateSilence:      ADT_printf ("\n\rVAD. Silence on channel %3d.%c\t", chId, Dev);     return;
   case EventNeedSRTPMasterKey:    ADT_printf ("\n\rSRTP master key required on channel %3d\t", chId); break;

   case EventTxCidMessageComplete: return; // ADT_printf ("\n\rCIDTx. Messsage sent on channel %d.%c", chId, Dev);        break;
   case EventCID2Tx:               ADT_printf ("\n\rCID2Tx chan: %d.%c. Status: %d Time: %d\t", chId, Dev,
                                       evt->aux.CID2StateCode[0], (evt->aux.CID2StateCode[2] << 16) | evt->aux.CID2StateCode[1]); break;
   case EventRxCidCarrierLock:     return; // ADT_printf ("\n\rCIDRx. Carrier detected on channel %d.%c", chId, Dev);    break;
   case EventCID2Rx:               ADT_printf ("\n\rCID2Rx chan: %d.%c. Status: %d Time: %d\t", chId, Dev,
                                       evt->aux.CID2StateCode[0], (evt->aux.CID2StateCode[2] << 16) | evt->aux.CID2StateCode[1]); break;
   case EventRxCidMessageComplete: dumpCallerID (chId, evt, dev);    break;
   default:                        
      if (EvtCode < CustomEventStart) {
         ADT_printf ("\n\rUnknown event %d received on channel %d.c\n", EvtCode, chId, Dev);
      } else {
         if (dumpCustomEvent == NULL) 
            ADT_printf ("\n\rCustom event %d received on channel %d.c\n", EvtCode, chId, Dev);
         else {
            dumpCustomEvent (Dsp, chId, EvtCode, evt);
            return;
         }
      }
      break;
         
   };
   if (prompt != NULL)
      ADT_printf (prompt);
}
//}

//------------------------------------------------------
//   Parameter modification
void modifyChannel (ADT_UInt32 DspId) {
   GpakApiStatus_t  ApiStat;
   GpakAlgCtrl_t    ControlCode;
   GpakDeviceSide_t AorBSide;
   GpakToneTypes    ActTonetype;
   GPAK_AlgControlStat_t dspStat;
   ADT_UInt16 NLPsetting = 0;
   ADT_UInt16 chanID;
   ADT_Int16 GaindB;
   char string [20];
   char *tone;

   chanID = ADT_getint ("Channel: ");
   ADT_printf ("\nSide[a|b]: ");
   AorBSide = (GpakDeviceSide_t) (ADT_getchar () - 'a');

   ADT_printf ("\n\ng=Gains \tt=Tones  \ts=Stats reset \tA[e|d]=AGC  \tv=VAD[e|d]:  \tr=RTP[e|d]\n");
   switch (toupper (ADT_getchar ())) {
   case 'A':
      if (ADT_getchar () == 'e') ControlCode = EnableVAGC;
      else                       ControlCode = BypassVAGC;
      break;

   case 'R':
      if (ADT_getchar () == 'e') ControlCode = RTPTransmitEnable;
      else                       ControlCode = RTPTransmitDisable;
      break;

   case 'S':
      ControlCode = ResetChanStats;
      break;

   case 'V':
      if (ADT_getchar () == 'e') ControlCode = EnableVad;
      else                       ControlCode = BypassVad;
      break;

   case 'T':
      ControlCode = ModifyToneDetector;
      ADT_printf ("\nD=DTMF\tC=Call prog\t1=MFR1\t2=MFR2\t3=MFR2Rev\tR=Relay\tG=Generation: ");
      ADT_gets (string, sizeof (string));
      tone = string;
      ActTonetype = Notify_Host;
      while (*tone != 0) {
         switch (toupper (*tone++)) {
         case 'D': ActTonetype |= DTMF_tone;     break;
         case '1': ActTonetype |= MFR1_tone;     break;
         case '2': ActTonetype |= MFR2Fwd_tone;  break;
         case '3': ActTonetype |= MFR2Back_tone; break;
         case 'C': ActTonetype |= CallProg_tone; break; 
         case 'R': ActTonetype |= Tone_Relay | Tone_Regen;   break;
         case 'G': ActTonetype |= Tone_Generate; break;
         }
      }
      break;

   case 'G':
      GaindB = ADT_getint ("Gain [dB/.1]: ");
      ControlCode = (GpakAlgCtrl_t) ADT_getint ("1=OutTDM  2=InTDM  3=OutNet   4=InNet: ");
      switch (ControlCode) {
      case 1:  ControlCode = UpdateTdmOutGain;  break;
      case 2:  ControlCode = UpdateTdmInGain;   break;
      case 3:  ControlCode = UpdateNetOutGain;  break;
      default: ControlCode = UpdateNetInGain;   break;
      }
      break;
   }

   ADT_printf ("\n");
   ApiStat = gpakAlgControl (DspId, chanID, ControlCode, ToneDetectAll, ActTonetype, 
                             AorBSide, NLPsetting, GaindB, &dspStat);
   if (ApiStat !=  GpakApiSuccess || dspStat != Cc_Success) {
      ADT_printf ("\nModify channel %d failure %d:%d:%d\n\n", chanID, ApiStat, dspStat, DSPError [DspId]);
   }
   return;
}


char getParams (void) {
   char *c;
   char cmd[80];

   prompt = "\nEnter channel parameters: ";
   ADT_printf ("\n\n\nDEFAULTS:\tr=reset");
   ADT_printf ("\nChannel counts: n\n");
   ADT_printf ("\nCODECS[d|e]:\tu=PCM_mu\ta=PCM_a\tl=l8\tlw=l8_WB\tL=L16\tLw=L16_WB");
   ADT_printf ("\n            \t7=723\t7a=723A\t9=729\t9a=729AB");
   ADT_printf ("\n            \tm=MELP\tM=MELPe\ts=Speex\tS=SpeexWB");
   ADT_printf ("\nFRAMES[d|e]:\t5=5ms\t1=10ms\t2=20ms\t3=30ms\n");
   ADT_printf ("\nALGS:\taa=AGC\tac=CID\taf=FAX+VOICE\taF=FAX ONLY\tav=VAD");
   ADT_printf ("\nECS:\tca=AEC\tcp=Pkt\tct=TDM");
   ADT_printf ("\nTONES[A|B]:\ta=Generic\td=DTMF\tp=PROG\tg=GEN\tr=RLY\th=HOST\tR=Regen");
   ADT_printf (prompt);
   ADT_gets (cmd, sizeof (cmd));
   c = cmd;

   while (*c != 0) {
      switch (*c++) {
      case 'x':  return 'x';
      case '\n': return 0;

      case 'r': restoreChannelDefaults(); break;

      case 'd':  // Decode
         switch (*c) {
         case '1': ActiveChan.DECSZ = Frame_10ms; break;
         case '2': ActiveChan.DECSZ = Frame_20ms; break;
         case '3': ActiveChan.DECSZ = Frame_30ms; break;
         case '5': ActiveChan.DECSZ = Frame_5ms;  break;
         default:  ActiveChan.DECODE = getCodecType (&c);
         }
         break;

      case 'e':  // Encode
         switch (*c) {
         case '1': ActiveChan.ENCSZ = Frame_10ms; break;
         case '2': ActiveChan.ENCSZ = Frame_20ms; break;
         case '3': ActiveChan.ENCSZ = Frame_30ms; break;
         case '5': ActiveChan.ENCSZ = Frame_5ms;  break;
         default:  ActiveChan.ENCODE = getCodecType (&c);
         }
         break;

      case 'a':  // Algorithms
         switch (*c++) {
         case 'a': ActiveChan.AGC = Enabled;    break;
         case 'c': ActiveChan.CID = CidReceive; break;
         case 'f': ActiveChan.FAX = faxVoice;   ActiveChan.TONEA |= Tone_Generate;   break;
         case 'F': ActiveChan.FAX = faxOnly;    ActiveChan.TONEA |= Tone_Generate;   break;
         case 'v': ActiveChan.VAD = Enabled;    break;
         }
         break;

      case 'c':  // Echo cancellers
         switch (*c++) {
         case 'a': ActiveChan.AEC    = Enabled; break;
         case 'p': ActiveChan.PKT_EC = Enabled; break;
         case 't': ActiveChan.PCM_EC = Enabled; break;
         }
         break;

      case 'n':  // Channel type set up
         getChannelTypeCounts ();
         break;

      case 'l':    // Loopback 
         switch (*c++) {
         case 'c': hostLoopBack = CROSSOVER; break;
         case 'l': hostLoopBack = LOOPBACK;  break;
         case 'n': hostLoopBack = NO_LOOP;   break;
         }
         break;

      case 'A':   // A- Tones
         switch (*c++) {
         case 'a': ActiveChan.TONEA |= Arbit_tone;    break;
         case 'd': ActiveChan.TONEA |= DTMF_tone;     break;
         case 'g': ActiveChan.TONEA |= Tone_Generate; break;
         case 'h': ActiveChan.TONEA |= Notify_Host;   break;
         case 'p': ActiveChan.TONEA |= CallProg_tone; break;
         case 'r': ActiveChan.TONEA |= Tone_Relay;    break;
         case 'R': ActiveChan.TONEA |= Tone_Regen;    break;
         }
         break;

      case 'B':   // B- Tones
         switch (*c++) {
         case 'a': ActiveChan.TONEB |= Arbit_tone;    break;
         case 'd': ActiveChan.TONEB |= DTMF_tone;     break;
         case 'g': ActiveChan.TONEB |= Tone_Generate; break;
         case 'h': ActiveChan.TONEB |= Notify_Host;   break;
         case 'p': ActiveChan.TONEB |= CallProg_tone; break;
         case 'r': ActiveChan.TONEB |= Tone_Relay;    break;
         case 'R': ActiveChan.TONEB |= Tone_Regen;    break;
         }
         break;
      }
   }
   ADT_printf ("\n");
   return 0;
}

void userInterface (void) {
   char key;
   int  secs, i;
   ADT_Int32 DspId = 0;
   DSP_Address playAddr;
   GpakApiStatus_t ApiStatus;

   gpakResetSystemStat_t resetStatus;

#ifdef DIAGNOSTICS
   extern void (*customDiagnostics)(void);
   extern void DIAGNOSTICS (void);
   customDiagnostics = DIAGNOSTICS;
#endif


   resetStats.FramingStats = 1;
   resetStats.CpuUsageStats = 1;

   memset (&hostStats, 0, sizeof (hostStats));
   memset (&sysCfg, 0, sizeof (sysCfg));
   memset (&NetEnables, 0, sizeof (NetEnables));

   ApiStatus = gpakReadSystemParms (DspId, &sysParams);
   if (ApiStatus != GpakApiSuccess)
      ADT_printf ("Get system configuration failure %d:%d\n", ApiStatus, DSPError [DspId]);

   sysParamsModify = SYS_NO_CHANGE;
   displaySystemData (DspId);

   dspTDMInit (DspId);   // Initialize DSP's TDM interface
   configGenericToneDetection (DspId);  // Initialize generic tone detector

   // Disable default channel parameters when not supported by DSP
   if (sysCfg.NumPcmEcans == 0)   DefaultChan.PCM_EC = Disabled;
   if (sysCfg.NumPktEcans == 0)   DefaultChan.PKT_EC = Disabled;
   if (sysCfg.AECInstances == 0)  DefaultChan.AEC    = Disabled;
   if (!(sysCfg.CfgEnableFlags & (AGCEnableBit)))  DefaultChan.AGC    = Disabled;
   if (!(sysCfg.CfgEnableFlags & (VadEnableBit)))  DefaultChan.VAD    = Disabled;


   memcpy (&ActiveChan, &DefaultChan, sizeof (ActiveChan));
   memcpy (&ActiveCnfr, &DefaultCnfr, sizeof (ActiveCnfr));
   setSRTPCfgDefaults ();

   readPlaybackAddress (DspId); 

   playAddr  = PlayRecAddress;
   playBufI8 = (((PlayRecI8/5) >> 2) << 2);
   for (i = 0; i < 5; i++, playAddr += playBufI8) 
      playRecBuffers[i] = playAddr;

   if (1 < MaxCoresPerDSP) {
      framingCores = 4;
      framingCoreOffset = 2;
   } else {
      framingCores = 1;
      framingCoreOffset = 0;
   }
   if (sysCfg.MaxConferences) initConferences (DspId);
   hostLoopBack = DSP_LOOP;
   NetEnables.fireHoseEnable = ADT_FALSE;

   // Wait until keyboard is pressed to exit
commandOptions:
   key = 0;
   ADT_printf ("\n\nC = canned scripts   \t D = diagnostic scripts \n");
   ADT_printf ("\nParms. pc=chan  \t pt= TMD  \t px=Speex  \t ps=system");
   ADT_printf ("\nInit.  ic=chans \t if=cnfr  \t i+=add    \t i-=teardown");
   ADT_printf ("\nCapt.  ce=echo  \t cc=chan");
   ADT_printf ("\nTest.  tg=tone gen   \t ti=toneinsert \t tp=play  \t tr=record \t td = dtmf generate\t tc = algcontrol");
   ADT_printf ("\nView.  vg=gains      \t vo=opts       \t vs=sys params \tvh=host settings");
   ADT_printf ("\n       vb=build opts");
   ADT_printf ("\nStats. sp=pkt        \t sr=RTP        \t ss=sys   \t sn=network");
   ADT_printf ("\n\nAuto.  au=cpu   \t ap=pkt   \t ar=RTP    \t ac=auto-setup/teardown");
   ADT_printf ("\nLoop.  hc=host cross \t hl=host loop  \t hn=none");
   ADT_printf ("\n       dc=dsp cross  \t dl=dsp loop");
   showTime();
   prompt = "\n\nCmd: ";
   ADT_printf (prompt);

   if (DSPPerformsRTP (DspId)) rtpProcess = loopbackRTPPackets;
   else                        rtpProcess = loopbackRTPPayloads;

   do {
      key = ADT_getchar ();
      switch (key) {
      // Canned tests
      case 'C':  cannedGpakTests (DspId);   break;

      case 'D':   // custom tests
         ADT_printf ("\n \n");
         if (customDiagnostics != NULL)
            (customDiagnostics)();
         break;

      default:
         goto commandOptions;

      case 'q':  break;   // quit

      case '2': NetEnables.stereo = !NetEnables.stereo;
         ADT_printf ("\nStereo enable = %d\n", NetEnables.stereo);
         break;

      // periodic status updates
      case 'a': // Auto 
         key = ADT_getchar ();
         switch (key) {
         case 'c':  autoSetup = !autoSetup;  break; // Auto setup/teardown
         case 'p':  autoStats = !autoStats;  break; // Packet counts
         case 'r':  autoRTP   = !autoRTP;  RTP_dump_all = ADT_FALSE; break; // RTP stats
         case 'R':  autoRTP   = !autoRTP;  RTP_dump_all = ADT_TRUE;  break; // RTP stats
         case 'u':  // CPU utilization
            autoUsage = !autoUsage;
            resetStatus = gpakResetSystemStats (DspId, resetStats);
            if (resetStatus != RssSuccess)
               ADT_printf ("Reset stats %d:%d", resetStatus, DSPError [DspId]);
            break;

         }
         cyclesSinceDisplay = cyclesPerDisplay - 4;
         break;

      case 'c':   // signal captures
         key = ADT_getchar ();
         secs = ADT_getint ("Capture size [secs]: ");
         setCaptureSize  (DspId, (ADT_UInt16) secs);

        switch (key) {
         case 'e': sendTestMsg (DspId, "EC Capture",   StartEcCapt);    break;
         case 'c': sendTestMsg (DspId, "Chan capture", StartChanCapt);  break;
        }
        break;

      case 'd':   // DSP loopback
         key = ADT_getchar ();
         switch (key) {
         case 'c': setDSPCrossover (DspId); break;
         case 'l': setDSPLoopBack  (DspId);  break;
         }
         break;

#ifdef ADT_DEVELOPMENT
      case 'f': NetEnables.fireHoseEnable = !NetEnables.fireHoseEnable;
         ADT_printf ("\nFire hose enable = %d\n", NetEnables.fireHoseEnable);
         break;
#endif

      case 'h':   // Host loopback
         key = ADT_getchar ();
         switch (key) {
         case 'c':  setDSPNoLoopBack (DspId); hostLoopBack = CROSSOVER;  break;
         case 'l':  setDSPNoLoopBack (DspId); hostLoopBack = LOOPBACK;   break;
         case 'n':  setDSPNoLoopBack (DspId); hostLoopBack = NO_LOOP;    break;
         }
         break;

      case 'i':   // Initialization
         key = ADT_getchar ();
         switch (key) {
         case 'f':  initConferences (DspId);  break;
         case '\r': dspTearDown (DspId);   // Intentional fall through
         case '+':  initChannels (DspId);     break;
         case '-':  dspTearDown (DspId);      break;        
         }
         break;

      case 'p':   // Param configurations
         key = ADT_getchar ();
         switch (key) {
         case 'c': getParams ();  goto commandOptions;
         case 't': dspTDMInit (DspId);  break;
         case 's': sysParamsModify = SYS_PROMPT;  displaySystemData (DspId);  break;
         case 'x': configureSpeex (DspId); break;
         case '-': sysParamsModify = SYS_MIN;     displaySystemData (DspId);  break;
         case '.': sysParamsModify = SYS_DEFAULT; displaySystemData (DspId);  break;
         case '+': sysParamsModify = SYS_MAX;     displaySystemData (DspId);  break;
         }
         break;

      case 'r':   // RTP  
         key = ADT_getchar ();
         switch (key) {
         case 'd': setRTPTxMode (DspId, RTPTransmitDisable); break;
         case 'e': setRTPTxMode (DspId, RTPTransmitEnable);  break;
         }
         break;

      case 's':   // Stats
         key = ADT_getchar ();
         switch (key) {
         case 'n': displayNetworkStats ();       break;
         case 'p': displayChannelStats (DspId);  break;
         case 'r': displayChannelRTPStats (DspId, ADT_FALSE); break;
         }
         break;

      case 't':   // Tests
         key = ADT_getchar ();
         switch (key) {
         case 'g': toneGenRequest (DspId);          break;
         case 'i': toneInsert (DspId);              break;
         case 'p': playRecordTest (DspId, 'p');     break;
         case 'r': playRecordTest (DspId, 'r');     break;
         case 'd': toneGenDtmfString (DspId);       break;
         case 'c': toneAlgControl(DspId);           break;
        }
         break;

      case 'v':   // View configuration
         key = ADT_getchar ();
         switch (key) {
         case 'g': displayChannelGains (DspId);  break;
         case 'o': displayChannelOpts  (DspId);  break;
         case 's': sysParamsModify = SYS_NO_CHANGE; displaySystemData (DspId); break;
         case 'h': ADT_printf ("\nHost Settings: ActiveChan:\n");
                   displayHostSettings(&ActiveChan); 
                   ADT_printf ("\nHost Settings: Default:\n");
                   displayHostSettings(&DefaultChan); 
                   break;
         case 'b': displayBuildOpt(); break;
         }
         break;

      case '-':  dspTearDownAll (DspId);      break;

      }
      ADT_printf (prompt);

   } while (key != 27);

   dspTearDown (DspId);
   ADT_printf ("\n\nG.PAK API session ended\n");
   return;
}
