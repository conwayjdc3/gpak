/*
 * Copyright (c) 2010, Adaptive Digital Technologies, Inc.
 *
 * File Name: GpakCustOmapL138.c
 *
 * Description:
 *   This file contains OMAPL138 functions to support generic
 *   G.PAK API functions. The file provides the drivers to interface the
 *   host to the DSP using shared memory.
 *
 *
 * Version: 1.0
 *
 * Revision History:
 *   10/17/11 - Initial release.
 *
 */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Semaphore.h>

#include <string.h>
#include "adt_typedef.h"
#include "GpakCustOmapL138.h"
#include "GpakApi.h"

extern void ADT_sleep (int ms);  // One second between keyboard polling

unsigned int MaxCoresPerDSP   = CORES_PER_DSP;
unsigned int MaxDsps          = MAX_DSP_CORES;   // Max number of messaging DSPs
unsigned int MaxChannelAlloc  = MAX_CHANNELS;
unsigned int MaxWaitLoops     = MAX_WAIT_LOOPS;
unsigned int DownloadI8       = DOWNLOAD_BLOCK_I8;

#if defined (HOST_BIG_ENDIAN)
unsigned int HostBigEndian   = 1;
#elif defined (HOST_LITTLE_ENDIAN)
unsigned int HostBigEndian   = 0;
#else
   #error HOST_ENDIAN_MODE must be defined as either BIG or LITTLE
#endif

/* DSP Related values */
DSP_Address  ifBlkAddress = DSP_IFBLK_ADDRESS;                // Interface block address

GPAK_DspCommStat_t DSPError[MAX_DSP_CORES] = { DSPSuccess };

DSP_Word    MaxChannels[MAX_DSP_CORES]  = { 0 };  /* max num channels */
DSP_Word    MaxCmdMsgLen[MAX_DSP_CORES] = { 0 };  /* max Cmd msg length (octets) */

DSP_Address pDspIfBlk[MAX_DSP_CORES] = { 0 };          /* DSP address of interface blocks */
DSP_Address pEventFifoAddress[MAX_DSP_CORES] = { 0 };  /* Event circular buffers */

DSP_Address pPktInBufr [MAX_DSP_CORES][MAX_CHANNELS];     /* Pkt In circular buffers */
DSP_Address pPktOutBufr[MAX_DSP_CORES][MAX_CHANNELS];     /* Pkt Out circular buffers */

/* Download buffers */
ADT_UInt32 DlByteBufr_[(DOWNLOAD_BLOCK_I8 + sizeof(int))/sizeof(int)];                /* Dowload byte buf */
DSP_Word   DlWordBufr [(DOWNLOAD_BLOCK_I8 + sizeof(DSP_Word))/sizeof(DSP_Word)];      /* Dowload word buffer */


Semaphore_Handle msgSem;

DSP_Address *getPktInBufrAddr  (int DspID, int channel) {
   return &pPktInBufr[DspID][channel];
}
DSP_Address *getPktOutBufrAddr (int DspID, int channel) {
   return &pPktOutBufr[DspID][channel];
}

void initGpakStructures () {

// Set DIAGNOSTICS to your diagnostics routine name to make use of the custom diagnostics feature
#ifdef DIAGNOSTICS
   extern void (*customDiagnostics)();
   extern void DIAGNOSTICS ();
   customDiagnostics = DIAGNOSTICS;
#endif

   memset (DSPError,     0, sizeof (DSPError));

   memset (MaxChannels,  0, sizeof (MaxChannels));
   memset (MaxCmdMsgLen, 0, sizeof (MaxCmdMsgLen));

   memset (pDspIfBlk,    0, sizeof (pDspIfBlk));
   memset (pEventFifoAddress, 0, sizeof (pEventFifoAddress));

   memset (pPktInBufr,   0, sizeof (pPktInBufr));
   memset (pPktOutBufr,  0, sizeof (pPktOutBufr));
}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadDspMemory8 - Read DSP memory.
 *
 * FUNCTION
 *  This function reads a contiguous block of bytes from DSP memory starting at
 *  the specified address.
 *
 * RETURNS
 *  nothing
 *
 */
void gpakReadDspMemory8  (ADT_UInt32 DspId, DSP_Address DspAddress, ADT_UInt32 I8Cnt, ADT_UInt8 *I8Array) {
   memcpy (I8Array, (void *) DspAddress, I8Cnt);
   return;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadDspMemory16 - Read DSP memory.
 *
 * FUNCTION
 *  This function reads a contiguous block of 16-bit word from DSP memory starting at
 *  the specified address.
 *
 * RETURNS
 *  nothing
 *
 */
void gpakReadDspMemory16 (ADT_UInt32 DspId, DSP_Address DspAddress, ADT_UInt32 I16Cnt, ADT_UInt16 *I16Array) {
   memcpy (I16Array, (void *) DspAddress, I16Cnt * 2);
   return;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadDspNoSwap16 - Read DSP packet memory without byte swapping.
 *
 * FUNCTION
 *  This function reads a contiguous block of 16-bit values from DSP memory 
 *  starting at the specified address.  DSP packet memory is in big-endian order
 *
 * RETURNS
 *  nothing
 *
 */
void gpakReadDspNoSwap16 (ADT_UInt32 DspId, DSP_Address DspAddress, ADT_UInt32 I16Cnt, ADT_UInt16 *I16Array) {
   ADT_UInt32 i;            /* loop index / counter */
   memcpy (I16Array, (void *) DspAddress, I16Cnt * 2);

   for (i = 0; i < I16Cnt; i++) {
      I16Array[i] = (I16Array[i] << 8) | (I16Array[i] >> 8);
   }
   return;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadDspMemory32 - Read DSP memory.
 *
 * FUNCTION
 *  This function reads a contiguous block of 32-bit values from DSP memory 
 *  starting at the specified address. 
 *
 * RETURNS
 *  nothing
 *
 */
void gpakReadDspMemory32 (ADT_UInt32 DspId, DSP_Address DspAddress, ADT_UInt32 I32Cnt, ADT_UInt32 *I32Array) {
   memcpy (I32Array, (void *) DspAddress, I32Cnt * 4);
   return;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadDspNoSwap32 - Read DSP packet memory without byte swapping.
 *
 * FUNCTION
 *  This function reads a contiguous block of 32-bit values from DSP memory 
 *  starting at the specified address.  DSP packet memory is in big-endian order
 *
 * RETURNS
 *  nothing
 *
 */
void gpakReadDspNoSwap32 (ADT_UInt32 DspId, DSP_Address DspAddress, ADT_UInt32 I32Cnt, ADT_UInt32 *I32Array) {
   memcpy (I32Array, (void *) DspAddress, I32Cnt * 4);
   return;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakWriteDspMemory8 - Write DSP memory.
 *
 * FUNCTION
 *  This function writes a contiguous block of bytes to DSP memory starting at
 *  the specified address.
 *
 * RETURNS
 *  nothing
 *
 */
void gpakWriteDspMemory8  (ADT_UInt32 DspId, DSP_Address DspAddress, ADT_UInt32 I8Cnt, ADT_UInt8 *I8Array) {

   memcpy ((void *) DspAddress, I8Array, I8Cnt);
   return;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakWriteDspMemory16 - Write DSP memory.
 *
 * FUNCTION
 *  This function writes a contiguous block of words to DSP memory starting at
 *  the specified address.
 *
 * RETURNS
 *  nothing
 *
 */
void gpakWriteDspMemory16 (ADT_UInt32 DspId, DSP_Address DspAddress, ADT_UInt32 I16Cnt, ADT_UInt16 *I16Array) {
   memcpy ((void *) DspAddress, I16Array, I16Cnt * 2);
   return;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakWriteDspNoSwap16 - Write DSP packet memory without byte swapping.
 *
 * FUNCTION
 *  This function writes a contiguous block of words to DSP memory starting at
 *  the specified address.
 *
 * RETURNS
 *  nothing
 *
 */
void gpakWriteDspNoSwap16 (ADT_UInt32 DspId, DSP_Address DspAddress, ADT_UInt32 I16Cnt, ADT_UInt16 *I16Array) {
   ADT_UInt32 i;            /* loop index / counter */

   for (i = 0; i < I16Cnt; i++) {
      I16Array[i] = (I16Array[i] << 8) | (I16Array[i] >> 8);
   }
   memcpy ((void *) DspAddress, I16Array, I16Cnt * 2);
   return;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakWriteDspMemory32 - Write DSP memory.
 *
 * FUNCTION
 *  This function writes a contiguous block of 32-bit values to DSP memory 
 *  starting at the specified address.
 *
 * RETURNS
 *  nothing
 *
 */
void gpakWriteDspMemory32 (ADT_UInt32 DspId, DSP_Address DspAddress, ADT_UInt32 I32Cnt, ADT_UInt32 *I32Array) {
   memcpy ((void *) DspAddress, I32Array, I32Cnt * 4);
   return;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakWriteDspNoSwap32 - Write DSP packet memory without byte swapping.
 *
 * FUNCTION
 *  This function writes a contiguous block of words to DSP memory starting at
 *  the specified address.
 *
 * RETURNS
 *  nothing
 *
 */
void gpakWriteDspNoSwap32 (ADT_UInt32 DspId, DSP_Address DspAddress, ADT_UInt32 I32Cnt, ADT_UInt32 *I32Array) {
   memcpy ((void *) DspAddress, I32Array, I32Cnt * 4);
   return;
}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakHostDelay - Delay for a fixed time interval.
 *
 * FUNCTION
 *  This function delays for a fixed time interval before returning. The time
 *  interval is the Host Port Interface sampling period when polling a DSP for
 *  replies to command messages.
 *
 * RETURNS
 *  nothing
 *
 */
void gpakHostDelay (void) {

   ADT_sleep (100); // 100 milliseconds
   return;
}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakLockAccess - Lock access to the specified DSP.
 *
 * FUNCTION
 *  This function aquires exclusive access to the specified DSP.
 *
 * RETURNS
 *  nothing
 *
 */
void gpakLockAccess (ADT_UInt32 DspId) {

   if (msgSem != NULL) Semaphore_pend (msgSem, BIOS_WAIT_FOREVER);
   return;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakUnlockAccess - Unlock access to the specified DSP.
 *
 * FUNCTION
 *  This function releases exclusive access to the specified DSP.
 *
 * RETURNS
 *  nothing
 *
 */
void gpakUnlockAccess (ADT_UInt32 DspId) {
   if (msgSem != NULL) Semaphore_post (msgSem); 
   return;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakReadFile - Read a block of bytes from a G.PAK Download file.
 *
 * FUNCTION
 *  This function reads a contiguous block of bytes from a G.PAK Download file
 *  starting at the current file position.
 *
 * RETURNS
 *  The number of bytes read from the file.
 *   -1 indicates an error occurred.
 *    0 indicates all bytes have been read (end of file)
 *
 */
int gpakReadFile (GPAK_FILE_ID FileId,  ADT_UInt8 *pBuffer, ADT_UInt32 I8Cnt) {

    return -1;
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakDspStartup - Startup the G.PAK DSP.
 *
 * FUNCTION
 *  This function loads and starts execution of the G.PAK DSP and prepares for
 *  Host-DSP communication.
 *
 * RETURNS
 *  An indication of success or failure.
 *   -1 indicates an error occurred
 *    0 indicates success
 *
 */
int gpakDspStartup (ADT_UInt32 DspId, char *pCoffFileName) {

   msgSem = Semaphore_create (1, NULL, NULL);
   return 0;
}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * gpakDspShutdown - Shutdown the G.PAK DSP.
 *
 * FUNCTION
 *  This function stops execution of the G.PAK DSP and terminates Host-DSP
 *  communication.
 *
 * RETURNS
 *  An indication of success or failure.
 *   -1 indicates an error occurred
 *    0 indicates success
 *
 */
int gpakDspShutdown (ADT_UInt32 DspId) {

   if (msgSem != NULL) Semaphore_delete (&msgSem);
   msgSem = NULL;
   return 0;
}
