


/*======================================= */
#define GPAK_SWVER 0x0607U
#define DSP_Type Ti667


/*================================== */
/*           System Sizing           */

//{
#define MAX_NUM_CHANNELS      160
#define MAX_NUM_LB_CHANNELS     0
#define MAX_FRAME_SIZE        480
#define CORE_CNT                6
#define NUM_TRGEN_EVENTS       10

// Conferencing sizes
#define MAX_TOTAL_CONFERENCES        0
#define MAX_GPAK_CONFERENCES         0
#define MAX_CUSTOM_CONFERENCES       0
#define MAX_NUM_CONF_CHANS           0
#define NUM_CONF_SCRATCH_MEM         0

// Algorithm pool sizes
#define MAX_NUM_AGC_CHANNELS         0  // AGC
#define MAX_NUM_RXCID_CHANNELS       0  // Caller ID
#define MAX_NUM_TXCID_CHANNELS       0  // Caller ID
#define MAX_NUM_TONEGEN_CHANNELS   320  // Tone generate
#define MAX_NUM_TONEDET_CHANNELS   160  // Tone detect
#define MAX_TONE_DET_TYPES           1  // Tone detect
#define CED_DETECT_INSTANCES       160  // CED detect
#define CNG_DETECT_INSTANCES       160  // CNG detect
#define ARB_DETECT_INSTANCES         0  // Generic tone detect
#define NUM_ARB_TONE_CONFIGS         0  // Generic tone detect
#define MAX_NUM_FAX_CHANS          160  // T.38
#define MAX_NUM_SRC_CHANNELS         0  // Rate conversion
#define SRCUPBY2_BUFFER_I16       1080  // Rate conversion
#define SRCDOWNBY2_BUFFER_I16     1084  // Rate conversion
#define SRTPRX_SIZE_I4 ((1184+3)/4)
#define SRTPTX_SIZE_I4 ((1184+3)/4)

// Channel buffer pool sizes
#define MAX_DELAY_MS_PKT_BUFS      0
#define MAX_NUM_PCM_BUFFS        MAX_NUM_CHANNELS
#define MAX_NUM_PKT_BUFFS        MAX_NUM_CHANNELS
#define MAX_NUM_BULKDELAY_BUFFS  MAX_NUM_CHANNELS
#define PCMIN_BUFFER_I16           976
#define PCMIN_ALIGN_I16           1024
#define PCMOUT_BUFFER_I16         1456
#define PCMOUT_ALIGN_I16          1472
#define PKT_BUFFER_I16            1008
#define PKT_ALIGN_I16             1024
#define BULKDELAY_BUFFER_I16         0

#define BULKDELAY_ALIGN_I16          1

#define ENCODER_INST_I16    1074
#define DECODER_INST_I16    1100
#define ENCODER_BUFFER_I16  1088
#define DECODER_BUFFER_I16  1152
#define BG_SCRATCH_I16      1458
#define SARAM_BLOCK_I16     2180
#define DARAM_BLOCK_I16        1


// DMA buffer sizes
#define TSIP_MAX_ACTIVE_SLOTS_0      256
#define TSIP_FALLOC_I8_0       (TSIP_MAX_ACTIVE_SLOTS_0 + 8)     // 8-bytes for pre/post frame count headers
#define TSIP_BUFLEN_I8_0       (TSIP_FALLOC_I8_0 * 8 * 2)        // 8 samps/interrupt, 1 bytes/sample, double buffered
#define BSP0_DMA_BUFFER_I16    (TSIP_BUFLEN_I8_0/2)
#define TDM0_DMA_BUFFER_I8     (TSIP_MAX_ACTIVE_SLOTS_0 * 64)    // 64 = 4-bytes (16-bit WB) * 8 (frames/msec) * 2 (ping+pong) 

#define TSIP_MAX_ACTIVE_SLOTS_1      128
#define TSIP_FALLOC_I8_1       (TSIP_MAX_ACTIVE_SLOTS_1 + 8)     // 8-bytes for pre/post frame count headers
#define TSIP_BUFLEN_I8_1       (TSIP_FALLOC_I8_1 * 8 * 2)        // 8 samps/interrupt, 1 bytes/sample, double buffered
#define BSP1_DMA_BUFFER_I16    (TSIP_BUFLEN_I8_1/2)
#define TDM1_DMA_BUFFER_I8     (TSIP_MAX_ACTIVE_SLOTS_1 * 64)    // 64 = 4-bytes (16-bit WB) * 8 (frames/msec) * 2 (ping+pong) 

#define TSIP_MAX_ACTIVE_SLOTS_2        0
#define TSIP_FALLOC_I8_2       1
#define TSIP_BUFLEN_I8_2       1
#define BSP2_DMA_BUFFER_I16    1
#define TDM2_DMA_BUFFER_I8     2



/*  Echo canceller config-time settings. 
   Run time over-rides must not exceed any of these. */
#define MAX_PCM_ECANS         160
#define CFG_PCM_TAP_LEN      1024
#define CFG_PCM_NFIR_SEG        3
#define CFG_PCM_FIR_SEG_LEN    48

#define MAX_PKT_ECANS           0
#define CFG_PKT_TAP_LEN       128
#define CFG_PKT_NFIR_SEG        3
#define CFG_PKT_FIR_SEG_LEN    48

#define CFG_PCM_ECAN_SAVELEN (6 + CFG_PCM_NFIR_SEG * (2 + CFG_PCM_FIR_SEG_LEN) + CFG_PCM_TAP_LEN/4)
#define CFG_PKT_ECAN_SAVELEN (6 + CFG_PKT_NFIR_SEG * (2 + CFG_PKT_FIR_SEG_LEN) + CFG_PKT_TAP_LEN/4)

#define MAX_AEC_ECANS           0
#define MAX_AEC_TAIL            0

#define MIC_COEF_I16            0
#define SPKR_COEF_I16           0
#define MIC_EQ_STATE_I16        1
#define SPKR_EQ_STATE_I16       1

// RTP jitter buffer sizes
#define MAX_JITTER_MS         640
#define SMALL_JB_BLK_I32       16
#define LARGE_JB_BLK_I32       88
#define HUGE_JB_BLK_I32      188
#define MAX_RTP_BUFFS_PER_CHAN    20
#define SMALL_JB_POOL_I32  (SMALL_JB_BLK_I32 * MAX_NUM_CHANNELS * MAX_JITTER_MS)
#define LARGE_JB_POOL_I32  (LARGE_JB_BLK_I32 * MAX_NUM_CHANNELS * ((MAX_JITTER_MS / 10) + 1))
#define HUGE_JB_POOL_I32   (HUGE_JB_BLK_I32 * MAX_NUM_CHANNELS * ((MAX_JITTER_MS / 30) + 1))
#define RTP_BUFFERS_I16    (MAX_RTP_BUFFS_PER_CHAN * HUGE_JB_BLK_I32 * MAX_NUM_CHANNELS*2)

//}



/*======================================= */
//           Include files           

#define _CSL_H_         // Avoid csl definitions
#include <std.h>
#include "sysconfig.h"
#include "sysmem.h"
#include "GpakDefs.h"
#include "gpakpcmglob.h"
#include "GpakDma.h"

#define RX_CID_MSG_I8  0
#define TX_CID_MSG_I8  0
#define TX_CID_BRST_I8 0

#define EVENT_FIFO_BUFLEN_I16 ((sizeof(EventFifoCIDMsg_t) + (MAX_NUM_CHANNELS * sizeof(EventFifoMsg_t)) + sizeof(ADT_UInt32) + 1) / (sizeof(ADT_Int16)))

