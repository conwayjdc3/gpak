/* G.PAK Config Tool generated command file. */

SECTIONS
{


/* ---------------------------------------- */
/* -------  Algorithm fast access  ------- */

  vtext : {} ALIGN (128) > DDR3

/* ADT_G729AB fast memory requirements. */
  .G729AB   : RUN_START(_Start$G729Prog), RUN_END(_End$G729Prog) {}   ALIGN (128) > DDR3


/* T38 Fax Relay Memory Requirements */
  CHAN_INST_DATA:FAX   : {} ALIGN (128) >> DDR3
  T38PACKET : {} ALIGN (128) >> DDR3

/*  G168 memory requirements. */
  
/*  G168 memory requirements. */
     LEC_ADT_Text  : {}  ALIGN (128) > DDR3

/*  G168 memory requirements. */
     LEC_ADT_Const : {}  ALIGN (128) > DDR3

/*  Network Stack memory requirements. */
  
/*  Network Stack memory requirements. */
     
    IP_STACK_DATA_SECT:  RUN_START(_Start$IPData), RUN_END(_End$IPData) {
      *(IPStack)            /* IPStack data memory */
      *(.far:NDK_MMBUFFER)  /* mmAlloc heap */
      *(STATS:IPStack)
    } ALIGN (128)   > L2SRAM

/*  Network Stack memory requirements. */
     
    IPStackPktHeap: RUN_START(_Start$IPHeap), RUN_END(_End$IPHeap)  {
      *(.far:NDK_PACKETMEM)
    }  ALIGN (128) >> DDR3

/*  Voice Detect memory requirements. */
  
/*  Voice Detect memory requirements. */
     .VADCNG_ADT_CODE : {}  ALIGN (128) > DDR3

/* Pcm Echo Canceller fast memory requirements. */
  PcmEcDaState :  {} > DDR3
  PcmEcEchoPath : {} > DDR3
  PcmEcBackEp :   {} > DDR3


/* Uninitialized fast memory */
  /* Cacheable instance memory */
  FAST_INSTANCE_DATA: {
		*(CHAN_INST_DATA:VAD)
		*(CHAN_INST_DATA:SRTP)
		*(CHAN_INST_DATA:ToneDetect)
		*(CHAN_INST_DATA:ToneGen)
		*(CHAN_INST_DATA:DtmfDial)
		*(CHAN_INST_DATA:RTP)
		*(CHAN_INST_DATA:RTCP)
  } ALIGN (128) >> DDR3

  SWITCH_AND_DATA_TABLES: RUN_START(_Start$SWITCH_AND_DATA_TABLES), RUN_END (_End$SWITCH_AND_DATA_TABLES) {
		*(.switch)
		*(.data)
  } ALIGN (128) > DDR3


/* ----------------------- */
  RECOMMENDED_FAST_DATA_SECT: {
		*(blkDly_POOL)
  } ALIGN (128) >> DDR3




/* ---------------------------------------- */
/* -------  Program medium access  ------- */

  GROUP (MEDIUM_PROG) : RUN_START(_Start$MEDIUM_PROG), RUN_END (_End$MEDIUM_PROG) {
		MEDIUM_PROG_SECT
  } ALIGN (128) > DDR3

  GROUP (PKT_PROG) : RUN_START(_Start$PKT_PROG), RUN_END (_End$PKT_PROG) {
		PKT_PROG_SECT
  } ALIGN (128) > DDR3

  GROUP (TONE_PROG) : RUN_START(_Start$TONE_PROG), RUN_END (_End$TONE_PROG) {
		TONE_PROG_SECT
  } ALIGN (128) > DDR3

  GROUP (FAX_PROG) : RUN_START(_Start$FAX_PROG), RUN_END (_End$FAX_PROG) {
		FAX_PROG_SECT
  } ALIGN (128) > DDR3



/* ---------------------------------------- */
/* -------  Algorithm slow access  ------- */
  ENCODER_INST_DATA : {} > DDR3
  DECODER_INST_DATA : {} > DDR3

/*  ADT_G729AB slower memory recommended. */
  .G729AB_INIT           : RUN_START(_Start$G729SlowProg), RUN_END(_End$G729SlowProg) {}
   ALIGN (128) > DDR3
  GROUP (G729AB_TABLES) : RUN_START(_Start$G729Data), RUN_END(_End$G729Data)  {
		G729_ADT_TABLE1 
		G729_ADT_TABLE2 
		G729_ADT_TABLE3 
		G729_ADT_TABLE4 
  }     ALIGN (128) > DDR3

/* Pcm Echo Canceller slow memory requirements. */
  PcmEcChan :     {} ALIGN (8) >> L2SRAM
  PcmEcSaState :  {} ALIGN (8) >> L2SRAM



/* ----------------------- */
/* Uninitialized slow memory */
	.cio :            {
		*(.cio)
		. += 128;
	} ALIGN (128) > L2SRAM

  SLOW_DATA_SECTIONS: RUN_START(_Start$SLOW_DATA_SECTIONS), RUN_END (_End$SLOW_DATA_SECTIONS) {
		*(STATS)
		*(SLOW_DATA_SECT)
  } ALIGN (128) > DDR3


  JB_POOL: {
		*(JB_POOL)
  } ALIGN (128) >> DDR3


  GROUP (CONSTANTS) : RUN_START(_Start$CONSTANTS), RUN_END (_End$CONSTANTS) {
		.pinit:  {}
		.const:  {}
		.printf: {}
  } ALIGN (128) > DDR3


/* ----------------------- */
/* Slow access program sections*/

  GROUP (SLOW_PROG) : RUN_START(_Start$SLOW_PROG), RUN_END (_End$SLOW_PROG) {
		SLOW_PROG_SECT
		.text
  } ALIGN (128) > DDR3


/* Playback record buffer's start address */

  PlayRecBuff: RUN_START(_Start$PlayRecBuff), RUN_END (_End$PlayRecBuff) {
		*(PLAYRECSTARTADDR)
		. += 0x80000;
  } ALIGN (0x1000) > DDR3

  platform_lib : {} ALIGN (128) > DDR3
  waveHeapSect : {} ALIGN (128) > DDR3
}
