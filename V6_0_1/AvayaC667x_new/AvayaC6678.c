/*-------------------------------------------------------------------
   This memory allocation supports the following configuration:

	G722                           [   0]			=              DISABLED
	G723                           [   0]			=              DISABLED
	TI_G7231A                      [   0]			=              DISABLED
	G726_LowMips                   [ 101]			= ENABLED
	G726_LowMem                    [   0]			=              DISABLED
	G728                           [ 102]			=              DISABLED
	ADT_G729AB                     [ 303]			= ENABLED
	TI_G729AB                      [   0]			=              DISABLED
	ADT_4800                       [   0]			=              DISABLED
	AMR                            [ 205]			=              DISABLED
	MELP                           [ 401]			=              DISABLED
	MELPE                          [ 101]			=              DISABLED
	SpeexWB                        [ 102]			=              DISABLED
	Speex                          [ 102]			=              DISABLED

	Acoustic Echo Canceller        [ 151]			=              DISABLED
	AGC                            [ 406]			=              DISABLED
	Generic Tone Detection         [ 721]			=              DISABLED
	CED                            [ 100]			= ENABLED
	Conferencing                   [ 406]			=              DISABLED
	CNG                            [ 100]			= ENABLED
	CPRG                           [ 721]			=              DISABLED
	DTMF                           [ 721]			= ENABLED
	Fixed Gain                     [ 100]			= ENABLED
	MFR1                           [ 721]			=              DISABLED
	MFR2F                          [ 721]			=              DISABLED
	MFR2R                          [ 721]			=              DISABLED
	G168                           [111901]			= ENABLED
	Noise Suppression              [ 102]			=              DISABLED
	Rx Caller Id                   [ 406]			=              DISABLED
	Relay Detection                [ 721]			= ENABLED
	Relay Generation               [ 721]			= ENABLED
	Rate conversion (conf)         [ 406]			=              DISABLED
	RTP                            [ 503]			= ENABLED
	Sampling Rate Converter        [ 104]			=              DISABLED
	Secure RTP                     [ 104]			= ENABLED
	Network Stack                  [3610101]			= ENABLED
	T38FaxRelay                    [ 100]			= ENABLED
	T38FaxRelay[TI]                [ 102]			=              DISABLED
	Tx Caller Id                   [ 406]			=              DISABLED
	Tone Generation                [ 721]			= ENABLED
	Voice Detect                   [ 406]			= ENABLED
	TAS tone Detect                [ 600]			=              DISABLED



Frame sizes (ms): 	 10.0	 20.0	 30.0
Channel types: 	PCM2PKT
External memory is used with cache

-------------------------------------------------------------------*/

#include "AvayaC6678.h"



/*======================================= */
/*  SW Versions of installed components */
const unsigned int G726_LowMips_SWVER=    0x0101;
const unsigned int ADT_G729AB_SWVER=    0x0303;
const unsigned int AEC_SWVER=    0x0151;
const unsigned int AGC_SWVER=    0x0406;
const unsigned int ARB_SWVER=    0x0721;
const unsigned int CED_SWVER=    0x0100;
const unsigned int CNFR_SWVER=    0x0406;
const unsigned int CNG_SWVER=    0x0100;
const unsigned int CPRG_SWVER=    0x0721;
const unsigned int DTMF_SWVER=    0x0721;
const unsigned int GAIN_SWVER=    0x0100;
const unsigned int MFR1_SWVER=    0x0721;
const unsigned int MF2F_SWVER=    0x0721;
const unsigned int MF2R_SWVER=    0x0721;
const unsigned int NEC_SWVER=    0x111901;
const unsigned int NSPR_SWVER=    0x0102;
const unsigned int RCID_SWVER=    0x0406;
const unsigned int RDET_SWVER=    0x0721;
const unsigned int RGEN_SWVER=    0x0721;
const unsigned int RTCV_SWVER=    0x0406;
const unsigned int RTP_SWVER=    0x0503;
const unsigned int SRC_SWVER=    0x0104;
const unsigned int SRTP_SWVER=    0x0104;
const unsigned int STK_SWVER=    0x3610101;
const unsigned int T38_SWVER=    0x0100;
const unsigned int T38t_SWVER=    0x0102;
const unsigned int TCID_SWVER=    0x0406;
const unsigned int TGEN_SWVER=    0x0721;
const unsigned int VAD_SWVER=    0x0406;
const unsigned int TAS_SWVER=    0x0600;

typedef TDScratch_t GPAK_TD_Scratch;
typedef int           GPAK_Cnfr_Scratch;



/*=============================== */
// System configuration structure 

sysConfig_t sysConfig = {
0x70E0C00C,		// enab.Bits
0x0003E080,		// Codecs.Bits
GPAK_SWVER,		// gpakVersion
Ti667,		// DspType
(void *) 0x80000000,		// hpiBlockStartAddress
  1000,		// DspMips
{  1024,  1024,     0},		// numSlotsOnStream[ ]
{   256,   128,     0},		// maxSlotsSupported[ ]
{     8,     8,    16},		// serialWordSize[ ]
{	cmpNone8,		// compandingMode[0]
	cmpNone8,		// compandingMode[1]
	cmpNone16,		// compandingMode[2]
},
{  4096,  2048,     1},		// dmaBufferSize[ ]
{     0,     0,     0},		// txFrameSyncPolarity[ ]
{     0,     0,     0},		// rxFrameSyncPolarity[ ]
{     0,     0,     0},		// txClockPolarity[ ]
{     0,     0,     0},		// rxClockPolarity[ ]
{     1,     1,     1},		// txDataDelay[ ]
{     1,     1,     1},		// rxDataDelay[ ]
{     0,     0,     0},		// dxDelay[ ]
	RTPAVP,		// packetProfile
(GpakRTPPkts) 128,		// rtpPktType
0x8058,		// cfgFramesChans
   -18,		// agcTargetPower
   -10,		// agcMinGain
    10,		// agcMaxGain
   -47,		// agcLowSigThreshDBM
   -18,		// agcTargetPowerB
   -10,		// agcMinGainB
    10,		// agcMaxGainB
   -47,		// agcLowSigThreshDBMB
   -50,		// vadNoiseFloorZThresh
   500,		// vadHangOverTime
     5,		// vadWindowMs
MAX_NUM_CHANNELS,       // maxNumChannels
MAX_GPAK_CONFERENCES,   // maxNumConferences
MAX_CUSTOM_CONFERENCES, // customCnfrCnt
MAX_NUM_CONF_CHANS,     // maxNumConfChans
CFG_PCM_TAP_LEN,        // maxG168PcmTapLength
CFG_PKT_TAP_LEN,        // maxG168PktTapLength
CFG_PCM_NFIR_SEG,       // maxG168PcmNFIRSegments
CFG_PKT_NFIR_SEG,       // maxG168PktNFIRSegments
CFG_PCM_FIR_SEG_LEN,    // maxG168PcmFIRSegmentLength
CFG_PKT_FIR_SEG_LEN,    // maxG168PktFIRSegmentLength
SARAM_BLOCK_I16,        // saramBlockSize
DARAM_BLOCK_I16,        // daramBlockSize
PCMIN_BUFFER_I16,       // pcmInBufferSize
PCMOUT_BUFFER_I16,      // pcmOutBufferSize
PKT_BUFFER_I16,         // pktInOutBufferSize
BULKDELAY_BUFFER_I16,   // bulkDelayBufferSize
MAX_DOMINANT_N,         // numConfDominant
     0,		// maxConfNoiseSuppress
MAX_PCM_ECANS,          // numPcmEcans
MAX_PKT_ECANS,          // numPktEcans
MAX_TONE_DET_TYPES,     // maxToneDetTypes
MAX_AEC_ECANS,            // AECInstances
MAX_AEC_TAIL,             // AECTailLen
MAX_NUM_FAX_CHANS,        // numFaxChans
MAX_NUM_TONEGEN_CHANNELS, // numTGChans
MAX_NUM_AGC_CHANNELS,     // numAGCChans
     6,		// HostIF
     8,		// samplesPerMs
MAX_JITTER_MS,            // maxJitterms
SMALL_JB_BLK_I32,         // smallJBBlockI32
LARGE_JB_BLK_I32,         // largeJBBlockI32
HUGE_JB_BLK_I32,          // hugeJBBlockI32
RTP_BUFFERS_I16,          // RTPBuffersI16
MAX_NUM_TXCID_CHANNELS,   // numTxCidChans,
MAX_NUM_RXCID_CHANNELS,   // numRxCidChans,
     0,		// type2CID
    38,		// numSeizureBytes
    23,		// numMarkBytes
    10,		// numPostambleBytes
(GpakFskType)   2,          // fskType
   -10,		// fskLevel
     0,		// msgType
EVENT_FIFO_BUFLEN_I16,    // evtFifoLenI16
MAX_NUM_PCM_BUFFS,        // maxNumPcmBuffs
MAX_NUM_PKT_BUFFS,        // maxNumPktBuffs
MAX_NUM_BULKDELAY_BUFFS,  // maxNumBulkDelayBuffs
(GpakActivation)   0,     // VadReport
CFG_PCM_ECAN_SAVELEN,     // EcanPcmSaveLenI16
CFG_PKT_ECAN_SAVELEN,     // EcanPktSaveLenI16
MAX_NUM_LB_CHANNELS,      // maxNumLbCoders
MAX_RTP_BUFFS_PER_CHAN,   // rtpMaxBuffsPerChan
MIC_COEF_I16,             // Aec Microphone equalizer length
SPKR_COEF_I16,            // Aec Speaker equalizer length
0,                        // g168MipsConserveLev;
MAX_NUM_SRC_CHANNELS, // numSrcChans
     1,		// SRTP_Enable
};
#pragma DATA_SECTION (sysConfig, "NON_CACHED_DATA:sysConfig")



/*=============================== */
// Echo canceller configuration structures 



/* PCM ECHO CANCELLER DECLARATION */
G168Params_t g168_PCM_EcParams = {
ADT_EC_API_VERSION,    // APIVersion
    0,                 // ChannelSizeI8    Channel (Instance) structure size, in bytes
    0,                 // EchoPathSizeI8   Echo path array size, in bytes
    0,                 // DAStateSizeI8    DAState structure size in bytes
    0,                 // SAStateSizeI8    SAState structure size in bytes
    0,                 // SAScratchSizeI8  SAScratch structure size in bytes
    0,                 // BGEchoPathSizeI8 BGEchoPath array size in bytes
    0,                 // DAScratchSizeI8  DAScratch structure size in bytes
    0,		// FrameSize
CFG_PCM_TAP_LEN,		// TapLength
    3,		// NLPType
    1,		// AdaptEnable
    0,		// BypassEnable
    1,		// G165DetectEnable
    6,		// DoubleTalkThres
    6,		// MaxDoubleTalkThres
    3,		// SaturationLevel
   -1,      // DynamicNLPAggressiveness
   24,		// NLPThres
   18,		// NLPUpperLimitThresConv
   12,		// NLPUpperLimitThresUnConv
    6,		// NLPSaturationThreshold
   10,		// NLPMaxSupp
   43,		// CNGNoiseThreshold
   75,		// AdaptLimit
   25,		// CrossCorrLimit
   80,		// FIRTapCheckPeriod
    3,		// NFIRSegments
   48,		// FIRSegmentLength
    0,		// TandemOperation
    0,		// MixedFourWireMode
    2,		// ReconvergenceChk
    0,      // pMIPSConserve
    0,      // ChannelNumber
    0,		// SmartPacketModeSelect
   50,		// SmartPacketBypassERLThresholddB
   25,		// SmartPacketSuppressERLThresholddB
    6,		// SmartPacketNLPThresholddB
};


/* PKT ECHO CANCELLER DECLARATION */
G168Params_t g168_PKT_EcParams = {
ADT_EC_API_VERSION,    // APIVersion
    0,                 // ChannelSizeI8    Channel (Instance) structure size, in bytes
    0,                 // EchoPathSizeI8   Echo path array size, in bytes
    0,                 // DAStateSizeI8    DAState structure size in bytes
    0,                 // SAStateSizeI8    SAState structure size in bytes
    0,                 // SAScratchSizeI8  SAScratch structure size in bytes
    0,                 // BGEchoPathSizeI8 BGEchoPath array size in bytes
    0,                 // DAScratchSizeI8  DAScratch structure size in bytes
    0,		// FrameSize
CFG_PKT_TAP_LEN,		// TapLength
    3,		// NLPType
    1,		// AdaptEnable
    0,		// BypassEnable
    1,		// G165DetectEnable
    6,		// DoubleTalkThres
    6,		// MaxDoubleTalkThres
    3,		// SaturationLevel
   -1,      // DynamicNLPAggressiveness
   24,		// NLPThres
   18,		// NLPUpperLimitThresConv
   12,		// NLPUpperLimitThresUnConv
    6,		// NLPSaturationThreshold
   10,		// NLPMaxSupp
   43,		// CNGNoiseThreshold
   75,		// AdaptLimit
   25,		// CrossCorrLimit
   80,		// FIRTapCheckPeriod
    3,		// NFIRSegments
   48,		// FIRSegmentLength
    0,		// TandemOperation
    0,		// MixedFourWireMode
    2,		// ReconvergenceChk
    0,      // pMIPSConserve
    0,      // ChannelNumber
    0,		// SmartPacketModeSelect
   50,		// SmartPacketBypassERLThresholddB
   25,		// SmartPacketSuppressERLThresholddB
    6,		// SmartPacketNLPThresholddB
};



/*=============================== */
// Framing task buffers. 

//{
FrameTaskData_t FrameTaskData[48];
#pragma DATA_SECTION (FrameTaskData,  "SHARED_DATA_SECT:FrameTaskData")

chanInfo_t *PendingEncodeQueue [48];
chanInfo_t *PendingDecodeQueue [48];
#pragma DATA_SECTION (PendingEncodeQueue,  "SHARED_DATA_SECT:EncodeQueue")
#pragma DATA_SECTION (PendingDecodeQueue,  "SHARED_DATA_SECT:DecodeQueue")

chanInfo_t *PPendingEncodeQueue [48];
chanInfo_t *PPendingDecodeQueue [48];
#pragma DATA_SECTION (PPendingEncodeQueue,  "SHARED_DATA_SECT:EncodeQueue")
#pragma DATA_SECTION (PPendingDecodeQueue,  "SHARED_DATA_SECT:DecodeQueue")



ADT_UInt16 inWork_1msec[  1], 	outWork_1msec[  1], 	ECFarWork_1msec[  1];
#pragma DATA_ALIGN (inWork_1msec,   4)
#pragma DATA_ALIGN (outWork_1msec,   4)
#pragma DATA_ALIGN (ECFarWork_1msec, 4)

#pragma DATA_SECTION (inWork_1msec,    "IN_WORK")
#pragma DATA_SECTION (outWork_1msec,   "OUT_WORK")
#pragma DATA_SECTION (ECFarWork_1msec, "ECFAR_WORK")


ADT_UInt16 inWork_2_5msec[  1], 	outWork_2_5msec[  1], 	ECFarWork_2_5msec[  1];
#pragma DATA_ALIGN (inWork_2_5msec,   4)
#pragma DATA_ALIGN (outWork_2_5msec,   4)
#pragma DATA_ALIGN (ECFarWork_2_5msec, 4)

#pragma DATA_SECTION (inWork_2_5msec,    "IN_WORK")
#pragma DATA_SECTION (outWork_2_5msec,   "OUT_WORK")
#pragma DATA_SECTION (ECFarWork_2_5msec, "ECFAR_WORK")


ADT_UInt16 inWork_5msec[  1], 	outWork_5msec[  1], 	ECFarWork_5msec[  1];
#pragma DATA_ALIGN (inWork_5msec,   4)
#pragma DATA_ALIGN (outWork_5msec,   4)
#pragma DATA_ALIGN (ECFarWork_5msec, 4)

#pragma DATA_SECTION (inWork_5msec,    "IN_WORK")
#pragma DATA_SECTION (outWork_5msec,   "OUT_WORK")
#pragma DATA_SECTION (ECFarWork_5msec, "ECFAR_WORK")


ADT_UInt16 inWork_10msec[ 80], 	outWork_10msec[ 80], 	ECFarWork_10msec[ 80];
#pragma DATA_ALIGN (inWork_10msec,   4)
#pragma DATA_ALIGN (outWork_10msec,   4)
#pragma DATA_ALIGN (ECFarWork_10msec, 4)

#pragma DATA_SECTION (inWork_10msec,    "IN_WORK")
#pragma DATA_SECTION (outWork_10msec,   "OUT_WORK")
#pragma DATA_SECTION (ECFarWork_10msec, "ECFAR_WORK")


ADT_UInt16 inWork_20msec[160], 	outWork_20msec[160], 	ECFarWork_20msec[160];
#pragma DATA_ALIGN (inWork_20msec,   4)
#pragma DATA_ALIGN (outWork_20msec,   4)
#pragma DATA_ALIGN (ECFarWork_20msec, 4)

#pragma DATA_SECTION (inWork_20msec,    "IN_WORK")
#pragma DATA_SECTION (outWork_20msec,   "OUT_WORK")
#pragma DATA_SECTION (ECFarWork_20msec, "ECFAR_WORK")


ADT_UInt16 inWork_22_5msec[  1], 	outWork_22_5msec[  1], 	ECFarWork_22_5msec[  1];
#pragma DATA_ALIGN (inWork_22_5msec,   4)
#pragma DATA_ALIGN (outWork_22_5msec,   4)
#pragma DATA_ALIGN (ECFarWork_22_5msec, 4)

#pragma DATA_SECTION (inWork_22_5msec,    "IN_WORK")
#pragma DATA_SECTION (outWork_22_5msec,   "OUT_WORK")
#pragma DATA_SECTION (ECFarWork_22_5msec, "ECFAR_WORK")


ADT_UInt16 inWork_30msec[240], 	outWork_30msec[240], 	ECFarWork_30msec[240];
#pragma DATA_ALIGN (inWork_30msec,   4)
#pragma DATA_ALIGN (outWork_30msec,   4)
#pragma DATA_ALIGN (ECFarWork_30msec, 4)

#pragma DATA_SECTION (inWork_30msec,    "IN_WORK")
#pragma DATA_SECTION (outWork_30msec,   "OUT_WORK")
#pragma DATA_SECTION (ECFarWork_30msec, "ECFAR_WORK")



/*=============================== */
/* Host Port Interrupt flags. */

const ADT_UInt16 Hint_1ms    = 0;
const ADT_UInt16 Hint_2_5ms  = 0;
const ADT_UInt16 Hint_5ms    = 0;
const ADT_UInt16 Hint_10ms   = 0;
const ADT_UInt16 Hint_20ms   = 0;
const ADT_UInt16 Hint_22_5ms = 0;
const ADT_UInt16 Hint_30ms   = 0;

//}



/*=============================== */
// Conference configuration data. 

ConferenceInfo_t *ConferenceInfo[1] = {0};
ADT_UInt16 ConferenceInst;
ADT_UInt16 ConfScratch;
ADT_UInt16 ConfMemberData;
ADT_UInt16 ConfVadData;
ADT_UInt16 pConfScratch;
ADT_UInt16 ConfIndexType;
ADT_UInt16 ConfAgcInstance;
ADT_UInt16 ConfToneGenInstance;
ADT_UInt16 ConfToneGenParams;
ADT_UInt16 PendingMemberQueue;
ADT_UInt16 PendingCompositeQueue;
ADT_UInt16 PPendingMemberQueue;
ADT_UInt16 PPendingCompositeQueue;



/*=============================== */
// Background transcoder structures


// Loopback Coder is disabled... allocate to satisfy linker
int lbScratch;
ADT_UInt16 lbScratchI16 = 0;
#pragma DATA_SECTION (lbScratch,    "STUB_DATA_SECT")
#pragma DATA_SECTION (lbScratchI16, "STUB_DATA_SECT")
const int numLbsInUse = 0;
int teardownLBPending;
int teardownLBCoder;
int LoopbackList;
#pragma DATA_SECTION (teardownLBPending, "STUB_DATA_SECT")
#pragma DATA_SECTION (teardownLBCoder,   "STUB_DATA_SECT")
#pragma DATA_SECTION (LoopbackList,      "STUB_DATA_SECT")




/*======================================= */
//      Event/DMA/Pkt buffer/Channel/En-DeCoder allocation         

/* Host event notification.   */
ADT_UInt32 eventFIFOBuff[EVENT_FIFO_BUFLEN_I16/2];
CircBufInfo_t  eventFIFOInfo;
#pragma DATA_SECTION (eventFIFOBuff,"evt_POOL")
#pragma DATA_SECTION (eventFIFOInfo,"evt_CIRC")


/* DMA BLOCK DECLARATION */

//{
ADT_UInt16 BSP0DMA_TxBuffer[BSP0_DMA_BUFFER_I16];
ADT_UInt16 BSP0DMA_RxBuffer[BSP0_DMA_BUFFER_I16];
#pragma DATA_SECTION (BSP0DMA_TxBuffer,"DMABUFF0T")
#pragma DATA_SECTION (BSP0DMA_RxBuffer,"DMABUFF0R")
#pragma DATA_ALIGN   (BSP0DMA_TxBuffer, 128)
#pragma DATA_ALIGN   (BSP0DMA_RxBuffer, 128)

ADT_UInt16 BSP1DMA_TxBuffer[BSP1_DMA_BUFFER_I16];
ADT_UInt16 BSP1DMA_RxBuffer[BSP1_DMA_BUFFER_I16];
#pragma DATA_SECTION (BSP1DMA_TxBuffer,"DMABUFF1T")
#pragma DATA_SECTION (BSP1DMA_RxBuffer,"DMABUFF1R")
#pragma DATA_ALIGN   (BSP1DMA_TxBuffer, 128)
#pragma DATA_ALIGN   (BSP1DMA_RxBuffer, 128)

ADT_UInt16 BSP2DMA_TxBuffer[BSP2_DMA_BUFFER_I16];
ADT_UInt16 BSP2DMA_RxBuffer[BSP2_DMA_BUFFER_I16];
#pragma DATA_SECTION (BSP2DMA_TxBuffer,"DMABUFF2T")
#pragma DATA_SECTION (BSP2DMA_RxBuffer,"DMABUFF2R")
#pragma DATA_ALIGN   (BSP2DMA_TxBuffer, 128)
#pragma DATA_ALIGN   (BSP2DMA_RxBuffer, 128)

ADT_UInt16 const TsipBuflenI8[3] = {
    TSIP_BUFLEN_I8_0,
    TSIP_BUFLEN_I8_1,
    TSIP_BUFLEN_I8_2};
ADT_UInt16 const TsipFallocI8[3] = {
    TSIP_FALLOC_I8_0,
    TSIP_FALLOC_I8_1,
    TSIP_FALLOC_I8_2};

ADT_PCM16 RxTransBuff0[(TDM0_DMA_BUFFER_I8)/2], TxTransBuff0[(TDM0_DMA_BUFFER_I8)/2];
#pragma DATA_ALIGN (RxTransBuff0, 128)
#pragma DATA_ALIGN (TxTransBuff0, 128)
#pragma DATA_SECTION (RxTransBuff0, "RxTransBuff")
#pragma DATA_SECTION (TxTransBuff0, "TxTransBuff")

static ADT_PCM16 RxTransBuff1[(TDM1_DMA_BUFFER_I8)/2], TxTransBuff1[(TDM1_DMA_BUFFER_I8)/2];
#pragma DATA_ALIGN (RxTransBuff1, 128)
#pragma DATA_ALIGN (TxTransBuff1, 128)
#pragma DATA_SECTION (RxTransBuff1, "RxTransBuff")
#pragma DATA_SECTION (TxTransBuff1, "TxTransBuff")

static ADT_PCM16 RxTransBuff2[(TDM2_DMA_BUFFER_I8)/2], TxTransBuff2[(TDM2_DMA_BUFFER_I8)/2];
#pragma DATA_ALIGN (RxTransBuff2, 128)
#pragma DATA_ALIGN (TxTransBuff2, 128)
#pragma DATA_SECTION (RxTransBuff2, "RxTransBuff")
#pragma DATA_SECTION (TxTransBuff2, "TxTransBuff")

const ADT_PCM16* RxTransBuffers[] = {
 RxTransBuff0,  RxTransBuff1,  RxTransBuff2, };
const ADT_PCM16* TxTransBuffers[] = {
 TxTransBuff0,  TxTransBuff1,  TxTransBuff2, };

ADT_UInt16 SlotMap0[1024];
#pragma DATA_SECTION (SlotMap0, "FAST_DATA_SECT:TDM")
ADT_UInt16 SlotMap1[1024];
#pragma DATA_SECTION (SlotMap1, "FAST_DATA_SECT:TDM")
ADT_UInt16 SlotMap2[1];
#pragma DATA_SECTION (SlotMap2, "STUB_DATA_SECT")
ADT_UInt16* const pSlotMap[3] = {SlotMap0, SlotMap1, SlotMap2};


ADT_UInt16 TxSlotMap0[1024];
#pragma DATA_SECTION (TxSlotMap0, "FAST_DATA_SECT:TDM")
ADT_UInt16 TxSlotMap1[1024];
#pragma DATA_SECTION (TxSlotMap1, "FAST_DATA_SECT:TDM")
ADT_UInt16 TxSlotMap2[1];
#pragma DATA_SECTION (TxSlotMap2, "STUB_DATA_SECT")
ADT_UInt16* const pTxSlotMap[3] = {TxSlotMap0, TxSlotMap1, TxSlotMap2};


CircBufInfo_t *CrcRxBuff0[256];
CircBufInfo_t *CrcTxBuff0[256];
#pragma DATA_SECTION (CrcRxBuff0,  "FAST_DATA_SECT:TDM")
#pragma DATA_SECTION (CrcTxBuff0,  "FAST_DATA_SECT:TDM")
CircBufInfo_t *CrcRxBuff1[128];
CircBufInfo_t *CrcTxBuff1[128];
#pragma DATA_SECTION (CrcRxBuff1,  "FAST_DATA_SECT:TDM")
#pragma DATA_SECTION (CrcTxBuff1,  "FAST_DATA_SECT:TDM")
CircBufInfo_t *CrcRxBuff2[1];
CircBufInfo_t *CrcTxBuff2[1];
#pragma DATA_SECTION (CrcRxBuff2,  "STUB_DATA_SECT")
#pragma DATA_SECTION (CrcTxBuff2,  "STUB_DATA_SECT")
CircBufInfo_t** const pCrcRxBuff[3] = {CrcRxBuff0, CrcRxBuff1, CrcRxBuff2};
CircBufInfo_t** const pCrcTxBuff[3] = {CrcTxBuff0, CrcTxBuff1, CrcTxBuff2};

//}


/* PCM BUFFER POOL DECLARATIONS */
//{
const ADT_UInt16 pcmInAlignI16     = PCMIN_ALIGN_I16;
const ADT_UInt16 pcmOutAlignI16    = PCMOUT_ALIGN_I16;
const ADT_UInt16 bulkDelayAlignI16 = BULKDELAY_ALIGN_I16;
ADT_UInt16 PcmInBufferPool  [MAX_NUM_PCM_BUFFS][PCMIN_ALIGN_I16];
ADT_UInt16 PcmOutBufferPool [MAX_NUM_PCM_BUFFS][PCMOUT_ALIGN_I16];
ADT_UInt16 BulkDelayBufferPool;  // Stub for linker
#pragma DATA_SECTION (PcmInBufferPool, "pcmIn_POOL")
#pragma DATA_ALIGN   (PcmInBufferPool, 128)
#pragma DATA_SECTION (PcmOutBufferPool, "pcmOut_POOL")
#pragma DATA_ALIGN   (PcmOutBufferPool, 128)
#pragma DATA_SECTION (BulkDelayBufferPool, "blkDly_POOL")
#pragma DATA_ALIGN   (BulkDelayBufferPool, 128)
ADT_UInt16 PcmBuffAvail = MAX_NUM_PCM_BUFFS;
ADT_UInt16 PcmBuffInUse[MAX_NUM_PCM_BUFFS];
#pragma DATA_SECTION (PcmBuffAvail, "POOL_ALLOC")
#pragma DATA_SECTION (PcmBuffInUse, "POOL_ALLOC")
ADT_UInt16 BulkDelayBuffAvail = MAX_NUM_BULKDELAY_BUFFS;
ADT_UInt16 BulkDelayBuffInUse[MAX_NUM_BULKDELAY_BUFFS];
#pragma DATA_SECTION (BulkDelayBuffAvail, "POOL_ALLOC")
#pragma DATA_SECTION (BulkDelayBuffInUse, "POOL_ALLOC")


/*  Packet Buffer Info Structure  */
/*    2 buffers (in and out) per channel */

const ADT_UInt16 MaxDelayMs          = MAX_DELAY_MS_PKT_BUFS;
const ADT_UInt16 pktBuffAlignI16     = PKT_ALIGN_I16;
CircBufInfo_t pktBuffStructTable [2][MAX_NUM_CHANNELS];
ADT_UInt16 PktInBufferPool    [MAX_NUM_PKT_BUFFS][PKT_ALIGN_I16];
ADT_UInt16 PktOutBufferPool   [MAX_NUM_PKT_BUFFS][PKT_ALIGN_I16];
#pragma DATA_SECTION (pktBuffStructTable, "pkt_CIRC")
#pragma DATA_ALIGN   (pktBuffStructTable, 4)
#pragma DATA_SECTION (PktInBufferPool, "pktIn_POOL")
#pragma DATA_ALIGN   (PktInBufferPool, 128)
#pragma DATA_SECTION (PktOutBufferPool, "pktOut_POOL")
#pragma DATA_ALIGN   (PktOutBufferPool, 128)
ADT_UInt16 PktBuffAvail = MAX_NUM_PKT_BUFFS;
ADT_UInt16 PktBuffInUse[MAX_NUM_PKT_BUFFS];
#pragma DATA_SECTION (PktBuffAvail, "POOL_ALLOC")
#pragma DATA_SECTION (PktBuffInUse, "POOL_ALLOC")

//}



/*======================================= */
//      G.PAK channel data.
struct chanInfo* chanTable[MAX_NUM_CHANNELS];
chanInfo_t GpakChanInstance[MAX_NUM_CHANNELS];
#pragma DATA_SECTION (chanTable, "CHAN_INST_DATA:Chan")
#pragma DATA_ALIGN   (chanTable, 128)
#pragma DATA_SECTION (GpakChanInstance, "CHAN_INST_DATA:Chan")



//{ Per Channel ENCODE/DECODE BUFFERS
const ADT_Word encoderInstI16 = ENCODER_INST_I16;
const ADT_Word decoderInstI16 = DECODER_INST_I16;

/* Channel 0 */
ADT_UInt16 encoderInstance0 [ENCODER_BUFFER_I16];
ADT_UInt16 decoderInstance0 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance0, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance0, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance0, 128);
#pragma DATA_ALIGN   (decoderInstance0, 128);
/* Channel 1 */
static ADT_UInt16 encoderInstance1 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance1 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance1, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance1, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance1, 128);
#pragma DATA_ALIGN   (decoderInstance1, 128);
/* Channel 2 */
static ADT_UInt16 encoderInstance2 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance2 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance2, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance2, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance2, 128);
#pragma DATA_ALIGN   (decoderInstance2, 128);
/* Channel 3 */
static ADT_UInt16 encoderInstance3 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance3 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance3, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance3, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance3, 128);
#pragma DATA_ALIGN   (decoderInstance3, 128);
/* Channel 4 */
static ADT_UInt16 encoderInstance4 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance4 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance4, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance4, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance4, 128);
#pragma DATA_ALIGN   (decoderInstance4, 128);
/* Channel 5 */
static ADT_UInt16 encoderInstance5 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance5 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance5, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance5, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance5, 128);
#pragma DATA_ALIGN   (decoderInstance5, 128);
/* Channel 6 */
static ADT_UInt16 encoderInstance6 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance6 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance6, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance6, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance6, 128);
#pragma DATA_ALIGN   (decoderInstance6, 128);
/* Channel 7 */
static ADT_UInt16 encoderInstance7 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance7 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance7, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance7, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance7, 128);
#pragma DATA_ALIGN   (decoderInstance7, 128);
/* Channel 8 */
static ADT_UInt16 encoderInstance8 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance8 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance8, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance8, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance8, 128);
#pragma DATA_ALIGN   (decoderInstance8, 128);
/* Channel 9 */
static ADT_UInt16 encoderInstance9 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance9 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance9, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance9, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance9, 128);
#pragma DATA_ALIGN   (decoderInstance9, 128);
/* Channel 10 */
static ADT_UInt16 encoderInstance10 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance10 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance10, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance10, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance10, 128);
#pragma DATA_ALIGN   (decoderInstance10, 128);
/* Channel 11 */
static ADT_UInt16 encoderInstance11 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance11 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance11, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance11, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance11, 128);
#pragma DATA_ALIGN   (decoderInstance11, 128);
/* Channel 12 */
static ADT_UInt16 encoderInstance12 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance12 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance12, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance12, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance12, 128);
#pragma DATA_ALIGN   (decoderInstance12, 128);
/* Channel 13 */
static ADT_UInt16 encoderInstance13 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance13 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance13, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance13, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance13, 128);
#pragma DATA_ALIGN   (decoderInstance13, 128);
/* Channel 14 */
static ADT_UInt16 encoderInstance14 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance14 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance14, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance14, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance14, 128);
#pragma DATA_ALIGN   (decoderInstance14, 128);
/* Channel 15 */
static ADT_UInt16 encoderInstance15 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance15 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance15, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance15, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance15, 128);
#pragma DATA_ALIGN   (decoderInstance15, 128);
/* Channel 16 */
static ADT_UInt16 encoderInstance16 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance16 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance16, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance16, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance16, 128);
#pragma DATA_ALIGN   (decoderInstance16, 128);
/* Channel 17 */
static ADT_UInt16 encoderInstance17 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance17 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance17, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance17, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance17, 128);
#pragma DATA_ALIGN   (decoderInstance17, 128);
/* Channel 18 */
static ADT_UInt16 encoderInstance18 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance18 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance18, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance18, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance18, 128);
#pragma DATA_ALIGN   (decoderInstance18, 128);
/* Channel 19 */
static ADT_UInt16 encoderInstance19 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance19 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance19, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance19, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance19, 128);
#pragma DATA_ALIGN   (decoderInstance19, 128);
/* Channel 20 */
static ADT_UInt16 encoderInstance20 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance20 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance20, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance20, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance20, 128);
#pragma DATA_ALIGN   (decoderInstance20, 128);
/* Channel 21 */
static ADT_UInt16 encoderInstance21 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance21 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance21, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance21, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance21, 128);
#pragma DATA_ALIGN   (decoderInstance21, 128);
/* Channel 22 */
static ADT_UInt16 encoderInstance22 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance22 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance22, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance22, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance22, 128);
#pragma DATA_ALIGN   (decoderInstance22, 128);
/* Channel 23 */
static ADT_UInt16 encoderInstance23 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance23 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance23, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance23, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance23, 128);
#pragma DATA_ALIGN   (decoderInstance23, 128);
/* Channel 24 */
static ADT_UInt16 encoderInstance24 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance24 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance24, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance24, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance24, 128);
#pragma DATA_ALIGN   (decoderInstance24, 128);
/* Channel 25 */
static ADT_UInt16 encoderInstance25 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance25 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance25, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance25, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance25, 128);
#pragma DATA_ALIGN   (decoderInstance25, 128);
/* Channel 26 */
static ADT_UInt16 encoderInstance26 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance26 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance26, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance26, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance26, 128);
#pragma DATA_ALIGN   (decoderInstance26, 128);
/* Channel 27 */
static ADT_UInt16 encoderInstance27 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance27 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance27, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance27, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance27, 128);
#pragma DATA_ALIGN   (decoderInstance27, 128);
/* Channel 28 */
static ADT_UInt16 encoderInstance28 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance28 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance28, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance28, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance28, 128);
#pragma DATA_ALIGN   (decoderInstance28, 128);
/* Channel 29 */
static ADT_UInt16 encoderInstance29 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance29 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance29, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance29, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance29, 128);
#pragma DATA_ALIGN   (decoderInstance29, 128);
/* Channel 30 */
static ADT_UInt16 encoderInstance30 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance30 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance30, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance30, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance30, 128);
#pragma DATA_ALIGN   (decoderInstance30, 128);
/* Channel 31 */
static ADT_UInt16 encoderInstance31 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance31 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance31, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance31, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance31, 128);
#pragma DATA_ALIGN   (decoderInstance31, 128);
/* Channel 32 */
static ADT_UInt16 encoderInstance32 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance32 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance32, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance32, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance32, 128);
#pragma DATA_ALIGN   (decoderInstance32, 128);
/* Channel 33 */
static ADT_UInt16 encoderInstance33 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance33 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance33, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance33, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance33, 128);
#pragma DATA_ALIGN   (decoderInstance33, 128);
/* Channel 34 */
static ADT_UInt16 encoderInstance34 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance34 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance34, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance34, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance34, 128);
#pragma DATA_ALIGN   (decoderInstance34, 128);
/* Channel 35 */
static ADT_UInt16 encoderInstance35 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance35 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance35, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance35, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance35, 128);
#pragma DATA_ALIGN   (decoderInstance35, 128);
/* Channel 36 */
static ADT_UInt16 encoderInstance36 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance36 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance36, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance36, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance36, 128);
#pragma DATA_ALIGN   (decoderInstance36, 128);
/* Channel 37 */
static ADT_UInt16 encoderInstance37 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance37 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance37, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance37, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance37, 128);
#pragma DATA_ALIGN   (decoderInstance37, 128);
/* Channel 38 */
static ADT_UInt16 encoderInstance38 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance38 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance38, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance38, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance38, 128);
#pragma DATA_ALIGN   (decoderInstance38, 128);
/* Channel 39 */
static ADT_UInt16 encoderInstance39 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance39 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance39, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance39, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance39, 128);
#pragma DATA_ALIGN   (decoderInstance39, 128);
/* Channel 40 */
static ADT_UInt16 encoderInstance40 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance40 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance40, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance40, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance40, 128);
#pragma DATA_ALIGN   (decoderInstance40, 128);
/* Channel 41 */
static ADT_UInt16 encoderInstance41 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance41 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance41, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance41, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance41, 128);
#pragma DATA_ALIGN   (decoderInstance41, 128);
/* Channel 42 */
static ADT_UInt16 encoderInstance42 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance42 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance42, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance42, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance42, 128);
#pragma DATA_ALIGN   (decoderInstance42, 128);
/* Channel 43 */
static ADT_UInt16 encoderInstance43 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance43 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance43, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance43, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance43, 128);
#pragma DATA_ALIGN   (decoderInstance43, 128);
/* Channel 44 */
static ADT_UInt16 encoderInstance44 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance44 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance44, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance44, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance44, 128);
#pragma DATA_ALIGN   (decoderInstance44, 128);
/* Channel 45 */
static ADT_UInt16 encoderInstance45 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance45 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance45, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance45, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance45, 128);
#pragma DATA_ALIGN   (decoderInstance45, 128);
/* Channel 46 */
static ADT_UInt16 encoderInstance46 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance46 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance46, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance46, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance46, 128);
#pragma DATA_ALIGN   (decoderInstance46, 128);
/* Channel 47 */
static ADT_UInt16 encoderInstance47 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance47 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance47, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance47, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance47, 128);
#pragma DATA_ALIGN   (decoderInstance47, 128);
/* Channel 48 */
static ADT_UInt16 encoderInstance48 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance48 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance48, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance48, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance48, 128);
#pragma DATA_ALIGN   (decoderInstance48, 128);
/* Channel 49 */
static ADT_UInt16 encoderInstance49 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance49 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance49, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance49, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance49, 128);
#pragma DATA_ALIGN   (decoderInstance49, 128);
/* Channel 50 */
static ADT_UInt16 encoderInstance50 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance50 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance50, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance50, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance50, 128);
#pragma DATA_ALIGN   (decoderInstance50, 128);
/* Channel 51 */
static ADT_UInt16 encoderInstance51 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance51 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance51, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance51, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance51, 128);
#pragma DATA_ALIGN   (decoderInstance51, 128);
/* Channel 52 */
static ADT_UInt16 encoderInstance52 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance52 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance52, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance52, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance52, 128);
#pragma DATA_ALIGN   (decoderInstance52, 128);
/* Channel 53 */
static ADT_UInt16 encoderInstance53 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance53 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance53, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance53, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance53, 128);
#pragma DATA_ALIGN   (decoderInstance53, 128);
/* Channel 54 */
static ADT_UInt16 encoderInstance54 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance54 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance54, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance54, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance54, 128);
#pragma DATA_ALIGN   (decoderInstance54, 128);
/* Channel 55 */
static ADT_UInt16 encoderInstance55 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance55 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance55, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance55, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance55, 128);
#pragma DATA_ALIGN   (decoderInstance55, 128);
/* Channel 56 */
static ADT_UInt16 encoderInstance56 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance56 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance56, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance56, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance56, 128);
#pragma DATA_ALIGN   (decoderInstance56, 128);
/* Channel 57 */
static ADT_UInt16 encoderInstance57 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance57 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance57, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance57, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance57, 128);
#pragma DATA_ALIGN   (decoderInstance57, 128);
/* Channel 58 */
static ADT_UInt16 encoderInstance58 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance58 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance58, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance58, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance58, 128);
#pragma DATA_ALIGN   (decoderInstance58, 128);
/* Channel 59 */
static ADT_UInt16 encoderInstance59 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance59 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance59, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance59, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance59, 128);
#pragma DATA_ALIGN   (decoderInstance59, 128);
/* Channel 60 */
static ADT_UInt16 encoderInstance60 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance60 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance60, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance60, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance60, 128);
#pragma DATA_ALIGN   (decoderInstance60, 128);
/* Channel 61 */
static ADT_UInt16 encoderInstance61 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance61 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance61, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance61, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance61, 128);
#pragma DATA_ALIGN   (decoderInstance61, 128);
/* Channel 62 */
static ADT_UInt16 encoderInstance62 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance62 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance62, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance62, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance62, 128);
#pragma DATA_ALIGN   (decoderInstance62, 128);
/* Channel 63 */
static ADT_UInt16 encoderInstance63 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance63 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance63, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance63, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance63, 128);
#pragma DATA_ALIGN   (decoderInstance63, 128);
/* Channel 64 */
static ADT_UInt16 encoderInstance64 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance64 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance64, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance64, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance64, 128);
#pragma DATA_ALIGN   (decoderInstance64, 128);
/* Channel 65 */
static ADT_UInt16 encoderInstance65 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance65 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance65, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance65, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance65, 128);
#pragma DATA_ALIGN   (decoderInstance65, 128);
/* Channel 66 */
static ADT_UInt16 encoderInstance66 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance66 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance66, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance66, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance66, 128);
#pragma DATA_ALIGN   (decoderInstance66, 128);
/* Channel 67 */
static ADT_UInt16 encoderInstance67 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance67 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance67, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance67, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance67, 128);
#pragma DATA_ALIGN   (decoderInstance67, 128);
/* Channel 68 */
static ADT_UInt16 encoderInstance68 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance68 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance68, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance68, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance68, 128);
#pragma DATA_ALIGN   (decoderInstance68, 128);
/* Channel 69 */
static ADT_UInt16 encoderInstance69 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance69 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance69, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance69, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance69, 128);
#pragma DATA_ALIGN   (decoderInstance69, 128);
/* Channel 70 */
static ADT_UInt16 encoderInstance70 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance70 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance70, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance70, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance70, 128);
#pragma DATA_ALIGN   (decoderInstance70, 128);
/* Channel 71 */
static ADT_UInt16 encoderInstance71 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance71 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance71, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance71, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance71, 128);
#pragma DATA_ALIGN   (decoderInstance71, 128);
/* Channel 72 */
static ADT_UInt16 encoderInstance72 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance72 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance72, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance72, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance72, 128);
#pragma DATA_ALIGN   (decoderInstance72, 128);
/* Channel 73 */
static ADT_UInt16 encoderInstance73 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance73 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance73, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance73, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance73, 128);
#pragma DATA_ALIGN   (decoderInstance73, 128);
/* Channel 74 */
static ADT_UInt16 encoderInstance74 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance74 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance74, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance74, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance74, 128);
#pragma DATA_ALIGN   (decoderInstance74, 128);
/* Channel 75 */
static ADT_UInt16 encoderInstance75 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance75 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance75, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance75, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance75, 128);
#pragma DATA_ALIGN   (decoderInstance75, 128);
/* Channel 76 */
static ADT_UInt16 encoderInstance76 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance76 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance76, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance76, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance76, 128);
#pragma DATA_ALIGN   (decoderInstance76, 128);
/* Channel 77 */
static ADT_UInt16 encoderInstance77 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance77 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance77, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance77, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance77, 128);
#pragma DATA_ALIGN   (decoderInstance77, 128);
/* Channel 78 */
static ADT_UInt16 encoderInstance78 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance78 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance78, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance78, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance78, 128);
#pragma DATA_ALIGN   (decoderInstance78, 128);
/* Channel 79 */
static ADT_UInt16 encoderInstance79 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance79 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance79, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance79, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance79, 128);
#pragma DATA_ALIGN   (decoderInstance79, 128);
/* Channel 80 */
static ADT_UInt16 encoderInstance80 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance80 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance80, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance80, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance80, 128);
#pragma DATA_ALIGN   (decoderInstance80, 128);
/* Channel 81 */
static ADT_UInt16 encoderInstance81 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance81 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance81, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance81, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance81, 128);
#pragma DATA_ALIGN   (decoderInstance81, 128);
/* Channel 82 */
static ADT_UInt16 encoderInstance82 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance82 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance82, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance82, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance82, 128);
#pragma DATA_ALIGN   (decoderInstance82, 128);
/* Channel 83 */
static ADT_UInt16 encoderInstance83 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance83 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance83, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance83, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance83, 128);
#pragma DATA_ALIGN   (decoderInstance83, 128);
/* Channel 84 */
static ADT_UInt16 encoderInstance84 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance84 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance84, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance84, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance84, 128);
#pragma DATA_ALIGN   (decoderInstance84, 128);
/* Channel 85 */
static ADT_UInt16 encoderInstance85 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance85 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance85, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance85, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance85, 128);
#pragma DATA_ALIGN   (decoderInstance85, 128);
/* Channel 86 */
static ADT_UInt16 encoderInstance86 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance86 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance86, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance86, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance86, 128);
#pragma DATA_ALIGN   (decoderInstance86, 128);
/* Channel 87 */
static ADT_UInt16 encoderInstance87 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance87 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance87, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance87, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance87, 128);
#pragma DATA_ALIGN   (decoderInstance87, 128);
/* Channel 88 */
static ADT_UInt16 encoderInstance88 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance88 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance88, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance88, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance88, 128);
#pragma DATA_ALIGN   (decoderInstance88, 128);
/* Channel 89 */
static ADT_UInt16 encoderInstance89 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance89 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance89, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance89, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance89, 128);
#pragma DATA_ALIGN   (decoderInstance89, 128);
/* Channel 90 */
static ADT_UInt16 encoderInstance90 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance90 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance90, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance90, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance90, 128);
#pragma DATA_ALIGN   (decoderInstance90, 128);
/* Channel 91 */
static ADT_UInt16 encoderInstance91 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance91 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance91, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance91, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance91, 128);
#pragma DATA_ALIGN   (decoderInstance91, 128);
/* Channel 92 */
static ADT_UInt16 encoderInstance92 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance92 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance92, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance92, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance92, 128);
#pragma DATA_ALIGN   (decoderInstance92, 128);
/* Channel 93 */
static ADT_UInt16 encoderInstance93 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance93 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance93, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance93, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance93, 128);
#pragma DATA_ALIGN   (decoderInstance93, 128);
/* Channel 94 */
static ADT_UInt16 encoderInstance94 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance94 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance94, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance94, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance94, 128);
#pragma DATA_ALIGN   (decoderInstance94, 128);
/* Channel 95 */
static ADT_UInt16 encoderInstance95 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance95 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance95, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance95, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance95, 128);
#pragma DATA_ALIGN   (decoderInstance95, 128);
/* Channel 96 */
static ADT_UInt16 encoderInstance96 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance96 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance96, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance96, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance96, 128);
#pragma DATA_ALIGN   (decoderInstance96, 128);
/* Channel 97 */
static ADT_UInt16 encoderInstance97 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance97 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance97, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance97, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance97, 128);
#pragma DATA_ALIGN   (decoderInstance97, 128);
/* Channel 98 */
static ADT_UInt16 encoderInstance98 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance98 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance98, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance98, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance98, 128);
#pragma DATA_ALIGN   (decoderInstance98, 128);
/* Channel 99 */
static ADT_UInt16 encoderInstance99 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance99 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance99, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance99, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance99, 128);
#pragma DATA_ALIGN   (decoderInstance99, 128);
/* Channel 100 */
static ADT_UInt16 encoderInstance100 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance100 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance100, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance100, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance100, 128);
#pragma DATA_ALIGN   (decoderInstance100, 128);
/* Channel 101 */
static ADT_UInt16 encoderInstance101 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance101 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance101, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance101, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance101, 128);
#pragma DATA_ALIGN   (decoderInstance101, 128);
/* Channel 102 */
static ADT_UInt16 encoderInstance102 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance102 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance102, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance102, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance102, 128);
#pragma DATA_ALIGN   (decoderInstance102, 128);
/* Channel 103 */
static ADT_UInt16 encoderInstance103 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance103 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance103, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance103, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance103, 128);
#pragma DATA_ALIGN   (decoderInstance103, 128);
/* Channel 104 */
static ADT_UInt16 encoderInstance104 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance104 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance104, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance104, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance104, 128);
#pragma DATA_ALIGN   (decoderInstance104, 128);
/* Channel 105 */
static ADT_UInt16 encoderInstance105 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance105 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance105, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance105, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance105, 128);
#pragma DATA_ALIGN   (decoderInstance105, 128);
/* Channel 106 */
static ADT_UInt16 encoderInstance106 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance106 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance106, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance106, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance106, 128);
#pragma DATA_ALIGN   (decoderInstance106, 128);
/* Channel 107 */
static ADT_UInt16 encoderInstance107 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance107 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance107, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance107, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance107, 128);
#pragma DATA_ALIGN   (decoderInstance107, 128);
/* Channel 108 */
static ADT_UInt16 encoderInstance108 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance108 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance108, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance108, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance108, 128);
#pragma DATA_ALIGN   (decoderInstance108, 128);
/* Channel 109 */
static ADT_UInt16 encoderInstance109 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance109 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance109, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance109, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance109, 128);
#pragma DATA_ALIGN   (decoderInstance109, 128);
/* Channel 110 */
static ADT_UInt16 encoderInstance110 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance110 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance110, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance110, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance110, 128);
#pragma DATA_ALIGN   (decoderInstance110, 128);
/* Channel 111 */
static ADT_UInt16 encoderInstance111 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance111 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance111, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance111, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance111, 128);
#pragma DATA_ALIGN   (decoderInstance111, 128);
/* Channel 112 */
static ADT_UInt16 encoderInstance112 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance112 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance112, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance112, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance112, 128);
#pragma DATA_ALIGN   (decoderInstance112, 128);
/* Channel 113 */
static ADT_UInt16 encoderInstance113 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance113 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance113, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance113, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance113, 128);
#pragma DATA_ALIGN   (decoderInstance113, 128);
/* Channel 114 */
static ADT_UInt16 encoderInstance114 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance114 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance114, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance114, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance114, 128);
#pragma DATA_ALIGN   (decoderInstance114, 128);
/* Channel 115 */
static ADT_UInt16 encoderInstance115 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance115 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance115, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance115, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance115, 128);
#pragma DATA_ALIGN   (decoderInstance115, 128);
/* Channel 116 */
static ADT_UInt16 encoderInstance116 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance116 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance116, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance116, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance116, 128);
#pragma DATA_ALIGN   (decoderInstance116, 128);
/* Channel 117 */
static ADT_UInt16 encoderInstance117 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance117 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance117, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance117, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance117, 128);
#pragma DATA_ALIGN   (decoderInstance117, 128);
/* Channel 118 */
static ADT_UInt16 encoderInstance118 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance118 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance118, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance118, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance118, 128);
#pragma DATA_ALIGN   (decoderInstance118, 128);
/* Channel 119 */
static ADT_UInt16 encoderInstance119 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance119 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance119, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance119, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance119, 128);
#pragma DATA_ALIGN   (decoderInstance119, 128);
/* Channel 120 */
static ADT_UInt16 encoderInstance120 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance120 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance120, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance120, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance120, 128);
#pragma DATA_ALIGN   (decoderInstance120, 128);
/* Channel 121 */
static ADT_UInt16 encoderInstance121 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance121 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance121, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance121, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance121, 128);
#pragma DATA_ALIGN   (decoderInstance121, 128);
/* Channel 122 */
static ADT_UInt16 encoderInstance122 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance122 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance122, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance122, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance122, 128);
#pragma DATA_ALIGN   (decoderInstance122, 128);
/* Channel 123 */
static ADT_UInt16 encoderInstance123 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance123 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance123, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance123, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance123, 128);
#pragma DATA_ALIGN   (decoderInstance123, 128);
/* Channel 124 */
static ADT_UInt16 encoderInstance124 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance124 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance124, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance124, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance124, 128);
#pragma DATA_ALIGN   (decoderInstance124, 128);
/* Channel 125 */
static ADT_UInt16 encoderInstance125 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance125 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance125, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance125, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance125, 128);
#pragma DATA_ALIGN   (decoderInstance125, 128);
/* Channel 126 */
static ADT_UInt16 encoderInstance126 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance126 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance126, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance126, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance126, 128);
#pragma DATA_ALIGN   (decoderInstance126, 128);
/* Channel 127 */
static ADT_UInt16 encoderInstance127 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance127 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance127, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance127, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance127, 128);
#pragma DATA_ALIGN   (decoderInstance127, 128);
/* Channel 128 */
static ADT_UInt16 encoderInstance128 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance128 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance128, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance128, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance128, 128);
#pragma DATA_ALIGN   (decoderInstance128, 128);
/* Channel 129 */
static ADT_UInt16 encoderInstance129 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance129 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance129, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance129, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance129, 128);
#pragma DATA_ALIGN   (decoderInstance129, 128);
/* Channel 130 */
static ADT_UInt16 encoderInstance130 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance130 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance130, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance130, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance130, 128);
#pragma DATA_ALIGN   (decoderInstance130, 128);
/* Channel 131 */
static ADT_UInt16 encoderInstance131 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance131 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance131, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance131, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance131, 128);
#pragma DATA_ALIGN   (decoderInstance131, 128);
/* Channel 132 */
static ADT_UInt16 encoderInstance132 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance132 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance132, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance132, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance132, 128);
#pragma DATA_ALIGN   (decoderInstance132, 128);
/* Channel 133 */
static ADT_UInt16 encoderInstance133 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance133 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance133, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance133, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance133, 128);
#pragma DATA_ALIGN   (decoderInstance133, 128);
/* Channel 134 */
static ADT_UInt16 encoderInstance134 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance134 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance134, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance134, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance134, 128);
#pragma DATA_ALIGN   (decoderInstance134, 128);
/* Channel 135 */
static ADT_UInt16 encoderInstance135 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance135 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance135, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance135, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance135, 128);
#pragma DATA_ALIGN   (decoderInstance135, 128);
/* Channel 136 */
static ADT_UInt16 encoderInstance136 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance136 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance136, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance136, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance136, 128);
#pragma DATA_ALIGN   (decoderInstance136, 128);
/* Channel 137 */
static ADT_UInt16 encoderInstance137 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance137 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance137, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance137, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance137, 128);
#pragma DATA_ALIGN   (decoderInstance137, 128);
/* Channel 138 */
static ADT_UInt16 encoderInstance138 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance138 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance138, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance138, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance138, 128);
#pragma DATA_ALIGN   (decoderInstance138, 128);
/* Channel 139 */
static ADT_UInt16 encoderInstance139 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance139 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance139, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance139, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance139, 128);
#pragma DATA_ALIGN   (decoderInstance139, 128);
/* Channel 140 */
static ADT_UInt16 encoderInstance140 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance140 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance140, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance140, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance140, 128);
#pragma DATA_ALIGN   (decoderInstance140, 128);
/* Channel 141 */
static ADT_UInt16 encoderInstance141 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance141 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance141, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance141, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance141, 128);
#pragma DATA_ALIGN   (decoderInstance141, 128);
/* Channel 142 */
static ADT_UInt16 encoderInstance142 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance142 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance142, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance142, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance142, 128);
#pragma DATA_ALIGN   (decoderInstance142, 128);
/* Channel 143 */
static ADT_UInt16 encoderInstance143 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance143 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance143, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance143, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance143, 128);
#pragma DATA_ALIGN   (decoderInstance143, 128);
/* Channel 144 */
static ADT_UInt16 encoderInstance144 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance144 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance144, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance144, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance144, 128);
#pragma DATA_ALIGN   (decoderInstance144, 128);
/* Channel 145 */
static ADT_UInt16 encoderInstance145 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance145 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance145, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance145, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance145, 128);
#pragma DATA_ALIGN   (decoderInstance145, 128);
/* Channel 146 */
static ADT_UInt16 encoderInstance146 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance146 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance146, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance146, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance146, 128);
#pragma DATA_ALIGN   (decoderInstance146, 128);
/* Channel 147 */
static ADT_UInt16 encoderInstance147 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance147 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance147, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance147, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance147, 128);
#pragma DATA_ALIGN   (decoderInstance147, 128);
/* Channel 148 */
static ADT_UInt16 encoderInstance148 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance148 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance148, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance148, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance148, 128);
#pragma DATA_ALIGN   (decoderInstance148, 128);
/* Channel 149 */
static ADT_UInt16 encoderInstance149 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance149 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance149, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance149, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance149, 128);
#pragma DATA_ALIGN   (decoderInstance149, 128);
/* Channel 150 */
static ADT_UInt16 encoderInstance150 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance150 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance150, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance150, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance150, 128);
#pragma DATA_ALIGN   (decoderInstance150, 128);
/* Channel 151 */
static ADT_UInt16 encoderInstance151 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance151 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance151, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance151, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance151, 128);
#pragma DATA_ALIGN   (decoderInstance151, 128);
/* Channel 152 */
static ADT_UInt16 encoderInstance152 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance152 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance152, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance152, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance152, 128);
#pragma DATA_ALIGN   (decoderInstance152, 128);
/* Channel 153 */
static ADT_UInt16 encoderInstance153 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance153 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance153, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance153, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance153, 128);
#pragma DATA_ALIGN   (decoderInstance153, 128);
/* Channel 154 */
static ADT_UInt16 encoderInstance154 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance154 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance154, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance154, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance154, 128);
#pragma DATA_ALIGN   (decoderInstance154, 128);
/* Channel 155 */
static ADT_UInt16 encoderInstance155 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance155 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance155, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance155, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance155, 128);
#pragma DATA_ALIGN   (decoderInstance155, 128);
/* Channel 156 */
static ADT_UInt16 encoderInstance156 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance156 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance156, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance156, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance156, 128);
#pragma DATA_ALIGN   (decoderInstance156, 128);
/* Channel 157 */
static ADT_UInt16 encoderInstance157 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance157 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance157, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance157, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance157, 128);
#pragma DATA_ALIGN   (decoderInstance157, 128);
/* Channel 158 */
static ADT_UInt16 encoderInstance158 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance158 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance158, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance158, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance158, 128);
#pragma DATA_ALIGN   (decoderInstance158, 128);
/* Channel 159 */
static ADT_UInt16 encoderInstance159 [ENCODER_BUFFER_I16];
static ADT_UInt16 decoderInstance159 [DECODER_BUFFER_I16];
#pragma DATA_SECTION (encoderInstance159, "ENCODER_INST_DATA")
#pragma DATA_SECTION (decoderInstance159, "DECODER_INST_DATA")
#pragma DATA_ALIGN   (encoderInstance159, 128);
#pragma DATA_ALIGN   (decoderInstance159, 128);
ADT_UInt16 * const encoderPool[MAX_NUM_CHANNELS] = {
	encoderInstance0,
	encoderInstance1,
	encoderInstance2,
	encoderInstance3,
	encoderInstance4,
	encoderInstance5,
	encoderInstance6,
	encoderInstance7,
	encoderInstance8,
	encoderInstance9,
	encoderInstance10,
	encoderInstance11,
	encoderInstance12,
	encoderInstance13,
	encoderInstance14,
	encoderInstance15,
	encoderInstance16,
	encoderInstance17,
	encoderInstance18,
	encoderInstance19,
	encoderInstance20,
	encoderInstance21,
	encoderInstance22,
	encoderInstance23,
	encoderInstance24,
	encoderInstance25,
	encoderInstance26,
	encoderInstance27,
	encoderInstance28,
	encoderInstance29,
	encoderInstance30,
	encoderInstance31,
	encoderInstance32,
	encoderInstance33,
	encoderInstance34,
	encoderInstance35,
	encoderInstance36,
	encoderInstance37,
	encoderInstance38,
	encoderInstance39,
	encoderInstance40,
	encoderInstance41,
	encoderInstance42,
	encoderInstance43,
	encoderInstance44,
	encoderInstance45,
	encoderInstance46,
	encoderInstance47,
	encoderInstance48,
	encoderInstance49,
	encoderInstance50,
	encoderInstance51,
	encoderInstance52,
	encoderInstance53,
	encoderInstance54,
	encoderInstance55,
	encoderInstance56,
	encoderInstance57,
	encoderInstance58,
	encoderInstance59,
	encoderInstance60,
	encoderInstance61,
	encoderInstance62,
	encoderInstance63,
	encoderInstance64,
	encoderInstance65,
	encoderInstance66,
	encoderInstance67,
	encoderInstance68,
	encoderInstance69,
	encoderInstance70,
	encoderInstance71,
	encoderInstance72,
	encoderInstance73,
	encoderInstance74,
	encoderInstance75,
	encoderInstance76,
	encoderInstance77,
	encoderInstance78,
	encoderInstance79,
	encoderInstance80,
	encoderInstance81,
	encoderInstance82,
	encoderInstance83,
	encoderInstance84,
	encoderInstance85,
	encoderInstance86,
	encoderInstance87,
	encoderInstance88,
	encoderInstance89,
	encoderInstance90,
	encoderInstance91,
	encoderInstance92,
	encoderInstance93,
	encoderInstance94,
	encoderInstance95,
	encoderInstance96,
	encoderInstance97,
	encoderInstance98,
	encoderInstance99,
	encoderInstance100,
	encoderInstance101,
	encoderInstance102,
	encoderInstance103,
	encoderInstance104,
	encoderInstance105,
	encoderInstance106,
	encoderInstance107,
	encoderInstance108,
	encoderInstance109,
	encoderInstance110,
	encoderInstance111,
	encoderInstance112,
	encoderInstance113,
	encoderInstance114,
	encoderInstance115,
	encoderInstance116,
	encoderInstance117,
	encoderInstance118,
	encoderInstance119,
	encoderInstance120,
	encoderInstance121,
	encoderInstance122,
	encoderInstance123,
	encoderInstance124,
	encoderInstance125,
	encoderInstance126,
	encoderInstance127,
	encoderInstance128,
	encoderInstance129,
	encoderInstance130,
	encoderInstance131,
	encoderInstance132,
	encoderInstance133,
	encoderInstance134,
	encoderInstance135,
	encoderInstance136,
	encoderInstance137,
	encoderInstance138,
	encoderInstance139,
	encoderInstance140,
	encoderInstance141,
	encoderInstance142,
	encoderInstance143,
	encoderInstance144,
	encoderInstance145,
	encoderInstance146,
	encoderInstance147,
	encoderInstance148,
	encoderInstance149,
	encoderInstance150,
	encoderInstance151,
	encoderInstance152,
	encoderInstance153,
	encoderInstance154,
	encoderInstance155,
	encoderInstance156,
	encoderInstance157,
	encoderInstance158,
	encoderInstance159
};

ADT_UInt16 * const decoderPool[MAX_NUM_CHANNELS] = {
	decoderInstance0,
	decoderInstance1,
	decoderInstance2,
	decoderInstance3,
	decoderInstance4,
	decoderInstance5,
	decoderInstance6,
	decoderInstance7,
	decoderInstance8,
	decoderInstance9,
	decoderInstance10,
	decoderInstance11,
	decoderInstance12,
	decoderInstance13,
	decoderInstance14,
	decoderInstance15,
	decoderInstance16,
	decoderInstance17,
	decoderInstance18,
	decoderInstance19,
	decoderInstance20,
	decoderInstance21,
	decoderInstance22,
	decoderInstance23,
	decoderInstance24,
	decoderInstance25,
	decoderInstance26,
	decoderInstance27,
	decoderInstance28,
	decoderInstance29,
	decoderInstance30,
	decoderInstance31,
	decoderInstance32,
	decoderInstance33,
	decoderInstance34,
	decoderInstance35,
	decoderInstance36,
	decoderInstance37,
	decoderInstance38,
	decoderInstance39,
	decoderInstance40,
	decoderInstance41,
	decoderInstance42,
	decoderInstance43,
	decoderInstance44,
	decoderInstance45,
	decoderInstance46,
	decoderInstance47,
	decoderInstance48,
	decoderInstance49,
	decoderInstance50,
	decoderInstance51,
	decoderInstance52,
	decoderInstance53,
	decoderInstance54,
	decoderInstance55,
	decoderInstance56,
	decoderInstance57,
	decoderInstance58,
	decoderInstance59,
	decoderInstance60,
	decoderInstance61,
	decoderInstance62,
	decoderInstance63,
	decoderInstance64,
	decoderInstance65,
	decoderInstance66,
	decoderInstance67,
	decoderInstance68,
	decoderInstance69,
	decoderInstance70,
	decoderInstance71,
	decoderInstance72,
	decoderInstance73,
	decoderInstance74,
	decoderInstance75,
	decoderInstance76,
	decoderInstance77,
	decoderInstance78,
	decoderInstance79,
	decoderInstance80,
	decoderInstance81,
	decoderInstance82,
	decoderInstance83,
	decoderInstance84,
	decoderInstance85,
	decoderInstance86,
	decoderInstance87,
	decoderInstance88,
	decoderInstance89,
	decoderInstance90,
	decoderInstance91,
	decoderInstance92,
	decoderInstance93,
	decoderInstance94,
	decoderInstance95,
	decoderInstance96,
	decoderInstance97,
	decoderInstance98,
	decoderInstance99,
	decoderInstance100,
	decoderInstance101,
	decoderInstance102,
	decoderInstance103,
	decoderInstance104,
	decoderInstance105,
	decoderInstance106,
	decoderInstance107,
	decoderInstance108,
	decoderInstance109,
	decoderInstance110,
	decoderInstance111,
	decoderInstance112,
	decoderInstance113,
	decoderInstance114,
	decoderInstance115,
	decoderInstance116,
	decoderInstance117,
	decoderInstance118,
	decoderInstance119,
	decoderInstance120,
	decoderInstance121,
	decoderInstance122,
	decoderInstance123,
	decoderInstance124,
	decoderInstance125,
	decoderInstance126,
	decoderInstance127,
	decoderInstance128,
	decoderInstance129,
	decoderInstance130,
	decoderInstance131,
	decoderInstance132,
	decoderInstance133,
	decoderInstance134,
	decoderInstance135,
	decoderInstance136,
	decoderInstance137,
	decoderInstance138,
	decoderInstance139,
	decoderInstance140,
	decoderInstance141,
	decoderInstance142,
	decoderInstance143,
	decoderInstance144,
	decoderInstance145,
	decoderInstance146,
	decoderInstance147,
	decoderInstance148,
	decoderInstance149,
	decoderInstance150,
	decoderInstance151,
	decoderInstance152,
	decoderInstance153,
	decoderInstance154,
	decoderInstance155,
	decoderInstance156,
	decoderInstance157,
	decoderInstance158,
	decoderInstance159
};

//}



/*=============================== */
// Echo cancellation structures. 


// ------ Framing task scratch memory.

//{

ADT_UInt16 G168DAscratch_1ms;
union {
   ADT_UInt16 TDScratch;
   ADT_UInt16 CnfrScratch;
   ADT_UInt16 SAScratch;
   ADT_UInt16 SRTPScratch;
} G168SAscratch_1ms;
#pragma DATA_SECTION (G168DAscratch_1ms, "SLOW_SCRATCH")
#pragma DATA_SECTION (G168SAscratch_1ms, "SLOW_SCRATCH")


ADT_UInt16 G168DAscratch_2_5ms;
union {
   ADT_UInt16 TDScratch;
   ADT_UInt16 CnfrScratch;
   ADT_UInt16 SAScratch;
   ADT_UInt16 SRTPScratch;
} G168SAscratch_2_5ms;
#pragma DATA_SECTION (G168DAscratch_2_5ms, "SLOW_SCRATCH")
#pragma DATA_SECTION (G168SAscratch_2_5ms, "SLOW_SCRATCH")


ADT_UInt16 G168DAscratch_5ms;
union {
   ADT_UInt16 TDScratch;
   ADT_UInt16 CnfrScratch;
   ADT_UInt16 SAScratch;
   ADT_UInt16 SRTPScratch;
} G168SAscratch_5ms;
#pragma DATA_SECTION (G168DAscratch_5ms, "SLOW_SCRATCH")
#pragma DATA_SECTION (G168SAscratch_5ms, "SLOW_SCRATCH")


ADT_UInt16 G168DAscratch_10ms [1040];
union {
   GPAK_TD_Scratch   TDScratch;
   GPAK_Cnfr_Scratch CnfrScratch;
   ADT_UInt16        SAScratch [1200];
   ADT_UInt32        SRTPScratch [ 80];
} G168SAscratch_10ms;
#pragma DATA_SECTION (G168DAscratch_10ms, "SLOW_SCRATCH")
#pragma DATA_SECTION (G168SAscratch_10ms, "FAST_SCRATCH")


ADT_UInt16 G168DAscratch_20ms [1040];
union {
   GPAK_TD_Scratch   TDScratch;
   GPAK_Cnfr_Scratch CnfrScratch;
   ADT_UInt16        SAScratch [1200];
   ADT_UInt32        SRTPScratch [ 80];
} G168SAscratch_20ms;
#pragma DATA_SECTION (G168DAscratch_20ms, "SLOW_SCRATCH")
#pragma DATA_SECTION (G168SAscratch_20ms, "FAST_SCRATCH")


ADT_UInt16 G168DAscratch_22_5ms;
union {
   ADT_UInt16 TDScratch;
   ADT_UInt16 CnfrScratch;
   ADT_UInt16 SAScratch;
   ADT_UInt16 SRTPScratch;
} G168SAscratch_22_5ms;
#pragma DATA_SECTION (G168DAscratch_22_5ms, "SLOW_SCRATCH")
#pragma DATA_SECTION (G168SAscratch_22_5ms, "SLOW_SCRATCH")


ADT_UInt16 G168DAscratch_30ms [1040];
union {
   GPAK_TD_Scratch   TDScratch;
   GPAK_Cnfr_Scratch CnfrScratch;
   ADT_UInt16        SAScratch [1458];
   ADT_UInt32        SRTPScratch [ 80];
} G168SAscratch_30ms;
#pragma DATA_SECTION (G168DAscratch_30ms, "SLOW_SCRATCH")
#pragma DATA_SECTION (G168SAscratch_30ms, "FAST_SCRATCH")

MipsConserve_t G168MipsConserve_1ms;
const ADT_UInt16 G168SAscratch_1msLenI16 = sizeof(G168SAscratch_1ms)/sizeof(ADT_UInt16);

const ADT_UInt16 G168DAscratch_1msLenI16 = sizeof(G168DAscratch_1ms)/sizeof(ADT_UInt16);

MipsConserve_t G168MipsConserve_2_5ms;
const ADT_UInt16 G168SAscratch_2_5msLenI16 = sizeof(G168SAscratch_2_5ms)/sizeof(ADT_UInt16);

const ADT_UInt16 G168DAscratch_2_5msLenI16 = sizeof(G168DAscratch_2_5ms)/sizeof(ADT_UInt16);

MipsConserve_t G168MipsConserve_5ms;
const ADT_UInt16 G168SAscratch_5msLenI16 = sizeof(G168SAscratch_5ms)/sizeof(ADT_UInt16);

const ADT_UInt16 G168DAscratch_5msLenI16 = sizeof(G168DAscratch_5ms)/sizeof(ADT_UInt16);

MipsConserve_t G168MipsConserve_10ms;
const ADT_UInt16 G168SAscratch_10msLenI16 = sizeof(G168SAscratch_10ms)/sizeof(ADT_UInt16);

const ADT_UInt16 G168DAscratch_10msLenI16 = sizeof(G168DAscratch_10ms)/sizeof(ADT_UInt16);

MipsConserve_t G168MipsConserve_20ms;
const ADT_UInt16 G168SAscratch_20msLenI16 = sizeof(G168SAscratch_20ms)/sizeof(ADT_UInt16);

const ADT_UInt16 G168DAscratch_20msLenI16 = sizeof(G168DAscratch_20ms)/sizeof(ADT_UInt16);

MipsConserve_t G168MipsConserve_22_5ms;
const ADT_UInt16 G168SAscratch_22_5msLenI16 = sizeof(G168SAscratch_22_5ms)/sizeof(ADT_UInt16);

const ADT_UInt16 G168DAscratch_22_5msLenI16 = sizeof(G168DAscratch_22_5ms)/sizeof(ADT_UInt16);

MipsConserve_t G168MipsConserve_30ms;
const ADT_UInt16 G168SAscratch_30msLenI16 = sizeof(G168SAscratch_30ms)/sizeof(ADT_UInt16);

const ADT_UInt16 G168DAscratch_30msLenI16 = sizeof(G168DAscratch_30ms)/sizeof(ADT_UInt16);

#pragma DATA_ALIGN (G168DAscratch_1ms, 8)
#pragma DATA_ALIGN (G168DAscratch_2_5ms, 8)
#pragma DATA_ALIGN (G168DAscratch_5ms, 8)
#pragma DATA_ALIGN (G168DAscratch_10ms, 8)
#pragma DATA_ALIGN (G168DAscratch_20ms, 8)
#pragma DATA_ALIGN (G168DAscratch_22_5ms, 8)
#pragma DATA_ALIGN (G168DAscratch_30ms, 8)
#pragma DATA_ALIGN (G168SAscratch_1ms, 8)
#pragma DATA_ALIGN (G168SAscratch_2_5ms, 8)
#pragma DATA_ALIGN (G168SAscratch_5ms, 8)
#pragma DATA_ALIGN (G168SAscratch_10ms, 8)
#pragma DATA_ALIGN (G168SAscratch_20ms, 8)
#pragma DATA_ALIGN (G168SAscratch_22_5ms, 8)
#pragma DATA_ALIGN (G168SAscratch_30ms, 8)

//}

//{ ------ Pcm Echo canceller buffer allocation

#define PcmECChanI16        368
#define PcmECChanI16_EXT    384

#define PcmECDAStateI16        272
#define PcmECDAStateI16_EXT    320

#define PcmECSAStateI16        982
#define PcmECSAStateI16_EXT   1024

#define PcmEchoPathI16       1512
#define PcmEchoPathI16_EXT   1536

#define PcmBgEchoPathI16       737
#define PcmBgEchoPathI16_EXT   768


ADT_UInt16 PcmEcChan0[PcmECChanI16_EXT], PcmEcDaState0[PcmECDAStateI16_EXT], PcmEcSaState0[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan0,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState0,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState0,"PcmEcSaState")

ADT_UInt16 PcmEcChan1[PcmECChanI16_EXT], PcmEcDaState1[PcmECDAStateI16_EXT], PcmEcSaState1[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan1,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState1,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState1,"PcmEcSaState")

ADT_UInt16 PcmEcChan2[PcmECChanI16_EXT], PcmEcDaState2[PcmECDAStateI16_EXT], PcmEcSaState2[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan2,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState2,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState2,"PcmEcSaState")

ADT_UInt16 PcmEcChan3[PcmECChanI16_EXT], PcmEcDaState3[PcmECDAStateI16_EXT], PcmEcSaState3[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan3,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState3,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState3,"PcmEcSaState")

ADT_UInt16 PcmEcChan4[PcmECChanI16_EXT], PcmEcDaState4[PcmECDAStateI16_EXT], PcmEcSaState4[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan4,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState4,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState4,"PcmEcSaState")

ADT_UInt16 PcmEcChan5[PcmECChanI16_EXT], PcmEcDaState5[PcmECDAStateI16_EXT], PcmEcSaState5[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan5,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState5,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState5,"PcmEcSaState")

ADT_UInt16 PcmEcChan6[PcmECChanI16_EXT], PcmEcDaState6[PcmECDAStateI16_EXT], PcmEcSaState6[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan6,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState6,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState6,"PcmEcSaState")

ADT_UInt16 PcmEcChan7[PcmECChanI16_EXT], PcmEcDaState7[PcmECDAStateI16_EXT], PcmEcSaState7[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan7,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState7,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState7,"PcmEcSaState")

ADT_UInt16 PcmEcChan8[PcmECChanI16_EXT], PcmEcDaState8[PcmECDAStateI16_EXT], PcmEcSaState8[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan8,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState8,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState8,"PcmEcSaState")

ADT_UInt16 PcmEcChan9[PcmECChanI16_EXT], PcmEcDaState9[PcmECDAStateI16_EXT], PcmEcSaState9[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan9,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState9,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState9,"PcmEcSaState")

ADT_UInt16 PcmEcChan10[PcmECChanI16_EXT], PcmEcDaState10[PcmECDAStateI16_EXT], PcmEcSaState10[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan10,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState10,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState10,"PcmEcSaState")

ADT_UInt16 PcmEcChan11[PcmECChanI16_EXT], PcmEcDaState11[PcmECDAStateI16_EXT], PcmEcSaState11[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan11,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState11,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState11,"PcmEcSaState")

ADT_UInt16 PcmEcChan12[PcmECChanI16_EXT], PcmEcDaState12[PcmECDAStateI16_EXT], PcmEcSaState12[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan12,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState12,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState12,"PcmEcSaState")

ADT_UInt16 PcmEcChan13[PcmECChanI16_EXT], PcmEcDaState13[PcmECDAStateI16_EXT], PcmEcSaState13[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan13,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState13,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState13,"PcmEcSaState")

ADT_UInt16 PcmEcChan14[PcmECChanI16_EXT], PcmEcDaState14[PcmECDAStateI16_EXT], PcmEcSaState14[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan14,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState14,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState14,"PcmEcSaState")

ADT_UInt16 PcmEcChan15[PcmECChanI16_EXT], PcmEcDaState15[PcmECDAStateI16_EXT], PcmEcSaState15[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan15,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState15,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState15,"PcmEcSaState")

ADT_UInt16 PcmEcChan16[PcmECChanI16_EXT], PcmEcDaState16[PcmECDAStateI16_EXT], PcmEcSaState16[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan16,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState16,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState16,"PcmEcSaState")

ADT_UInt16 PcmEcChan17[PcmECChanI16_EXT], PcmEcDaState17[PcmECDAStateI16_EXT], PcmEcSaState17[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan17,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState17,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState17,"PcmEcSaState")

ADT_UInt16 PcmEcChan18[PcmECChanI16_EXT], PcmEcDaState18[PcmECDAStateI16_EXT], PcmEcSaState18[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan18,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState18,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState18,"PcmEcSaState")

ADT_UInt16 PcmEcChan19[PcmECChanI16_EXT], PcmEcDaState19[PcmECDAStateI16_EXT], PcmEcSaState19[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan19,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState19,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState19,"PcmEcSaState")

ADT_UInt16 PcmEcChan20[PcmECChanI16_EXT], PcmEcDaState20[PcmECDAStateI16_EXT], PcmEcSaState20[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan20,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState20,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState20,"PcmEcSaState")

ADT_UInt16 PcmEcChan21[PcmECChanI16_EXT], PcmEcDaState21[PcmECDAStateI16_EXT], PcmEcSaState21[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan21,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState21,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState21,"PcmEcSaState")

ADT_UInt16 PcmEcChan22[PcmECChanI16_EXT], PcmEcDaState22[PcmECDAStateI16_EXT], PcmEcSaState22[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan22,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState22,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState22,"PcmEcSaState")

ADT_UInt16 PcmEcChan23[PcmECChanI16_EXT], PcmEcDaState23[PcmECDAStateI16_EXT], PcmEcSaState23[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan23,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState23,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState23,"PcmEcSaState")

ADT_UInt16 PcmEcChan24[PcmECChanI16_EXT], PcmEcDaState24[PcmECDAStateI16_EXT], PcmEcSaState24[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan24,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState24,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState24,"PcmEcSaState")

ADT_UInt16 PcmEcChan25[PcmECChanI16_EXT], PcmEcDaState25[PcmECDAStateI16_EXT], PcmEcSaState25[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan25,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState25,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState25,"PcmEcSaState")

ADT_UInt16 PcmEcChan26[PcmECChanI16_EXT], PcmEcDaState26[PcmECDAStateI16_EXT], PcmEcSaState26[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan26,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState26,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState26,"PcmEcSaState")

ADT_UInt16 PcmEcChan27[PcmECChanI16_EXT], PcmEcDaState27[PcmECDAStateI16_EXT], PcmEcSaState27[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan27,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState27,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState27,"PcmEcSaState")

ADT_UInt16 PcmEcChan28[PcmECChanI16_EXT], PcmEcDaState28[PcmECDAStateI16_EXT], PcmEcSaState28[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan28,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState28,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState28,"PcmEcSaState")

ADT_UInt16 PcmEcChan29[PcmECChanI16_EXT], PcmEcDaState29[PcmECDAStateI16_EXT], PcmEcSaState29[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan29,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState29,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState29,"PcmEcSaState")

ADT_UInt16 PcmEcChan30[PcmECChanI16_EXT], PcmEcDaState30[PcmECDAStateI16_EXT], PcmEcSaState30[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan30,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState30,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState30,"PcmEcSaState")

ADT_UInt16 PcmEcChan31[PcmECChanI16_EXT], PcmEcDaState31[PcmECDAStateI16_EXT], PcmEcSaState31[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan31,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState31,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState31,"PcmEcSaState")

ADT_UInt16 PcmEcChan32[PcmECChanI16_EXT], PcmEcDaState32[PcmECDAStateI16_EXT], PcmEcSaState32[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan32,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState32,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState32,"PcmEcSaState")

ADT_UInt16 PcmEcChan33[PcmECChanI16_EXT], PcmEcDaState33[PcmECDAStateI16_EXT], PcmEcSaState33[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan33,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState33,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState33,"PcmEcSaState")

ADT_UInt16 PcmEcChan34[PcmECChanI16_EXT], PcmEcDaState34[PcmECDAStateI16_EXT], PcmEcSaState34[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan34,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState34,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState34,"PcmEcSaState")

ADT_UInt16 PcmEcChan35[PcmECChanI16_EXT], PcmEcDaState35[PcmECDAStateI16_EXT], PcmEcSaState35[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan35,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState35,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState35,"PcmEcSaState")

ADT_UInt16 PcmEcChan36[PcmECChanI16_EXT], PcmEcDaState36[PcmECDAStateI16_EXT], PcmEcSaState36[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan36,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState36,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState36,"PcmEcSaState")

ADT_UInt16 PcmEcChan37[PcmECChanI16_EXT], PcmEcDaState37[PcmECDAStateI16_EXT], PcmEcSaState37[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan37,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState37,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState37,"PcmEcSaState")

ADT_UInt16 PcmEcChan38[PcmECChanI16_EXT], PcmEcDaState38[PcmECDAStateI16_EXT], PcmEcSaState38[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan38,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState38,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState38,"PcmEcSaState")

ADT_UInt16 PcmEcChan39[PcmECChanI16_EXT], PcmEcDaState39[PcmECDAStateI16_EXT], PcmEcSaState39[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan39,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState39,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState39,"PcmEcSaState")

ADT_UInt16 PcmEcChan40[PcmECChanI16_EXT], PcmEcDaState40[PcmECDAStateI16_EXT], PcmEcSaState40[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan40,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState40,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState40,"PcmEcSaState")

ADT_UInt16 PcmEcChan41[PcmECChanI16_EXT], PcmEcDaState41[PcmECDAStateI16_EXT], PcmEcSaState41[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan41,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState41,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState41,"PcmEcSaState")

ADT_UInt16 PcmEcChan42[PcmECChanI16_EXT], PcmEcDaState42[PcmECDAStateI16_EXT], PcmEcSaState42[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan42,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState42,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState42,"PcmEcSaState")

ADT_UInt16 PcmEcChan43[PcmECChanI16_EXT], PcmEcDaState43[PcmECDAStateI16_EXT], PcmEcSaState43[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan43,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState43,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState43,"PcmEcSaState")

ADT_UInt16 PcmEcChan44[PcmECChanI16_EXT], PcmEcDaState44[PcmECDAStateI16_EXT], PcmEcSaState44[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan44,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState44,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState44,"PcmEcSaState")

ADT_UInt16 PcmEcChan45[PcmECChanI16_EXT], PcmEcDaState45[PcmECDAStateI16_EXT], PcmEcSaState45[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan45,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState45,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState45,"PcmEcSaState")

ADT_UInt16 PcmEcChan46[PcmECChanI16_EXT], PcmEcDaState46[PcmECDAStateI16_EXT], PcmEcSaState46[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan46,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState46,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState46,"PcmEcSaState")

ADT_UInt16 PcmEcChan47[PcmECChanI16_EXT], PcmEcDaState47[PcmECDAStateI16_EXT], PcmEcSaState47[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan47,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState47,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState47,"PcmEcSaState")

ADT_UInt16 PcmEcChan48[PcmECChanI16_EXT], PcmEcDaState48[PcmECDAStateI16_EXT], PcmEcSaState48[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan48,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState48,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState48,"PcmEcSaState")

ADT_UInt16 PcmEcChan49[PcmECChanI16_EXT], PcmEcDaState49[PcmECDAStateI16_EXT], PcmEcSaState49[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan49,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState49,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState49,"PcmEcSaState")

ADT_UInt16 PcmEcChan50[PcmECChanI16_EXT], PcmEcDaState50[PcmECDAStateI16_EXT], PcmEcSaState50[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan50,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState50,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState50,"PcmEcSaState")

ADT_UInt16 PcmEcChan51[PcmECChanI16_EXT], PcmEcDaState51[PcmECDAStateI16_EXT], PcmEcSaState51[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan51,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState51,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState51,"PcmEcSaState")

ADT_UInt16 PcmEcChan52[PcmECChanI16_EXT], PcmEcDaState52[PcmECDAStateI16_EXT], PcmEcSaState52[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan52,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState52,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState52,"PcmEcSaState")

ADT_UInt16 PcmEcChan53[PcmECChanI16_EXT], PcmEcDaState53[PcmECDAStateI16_EXT], PcmEcSaState53[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan53,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState53,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState53,"PcmEcSaState")

ADT_UInt16 PcmEcChan54[PcmECChanI16_EXT], PcmEcDaState54[PcmECDAStateI16_EXT], PcmEcSaState54[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan54,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState54,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState54,"PcmEcSaState")

ADT_UInt16 PcmEcChan55[PcmECChanI16_EXT], PcmEcDaState55[PcmECDAStateI16_EXT], PcmEcSaState55[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan55,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState55,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState55,"PcmEcSaState")

ADT_UInt16 PcmEcChan56[PcmECChanI16_EXT], PcmEcDaState56[PcmECDAStateI16_EXT], PcmEcSaState56[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan56,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState56,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState56,"PcmEcSaState")

ADT_UInt16 PcmEcChan57[PcmECChanI16_EXT], PcmEcDaState57[PcmECDAStateI16_EXT], PcmEcSaState57[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan57,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState57,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState57,"PcmEcSaState")

ADT_UInt16 PcmEcChan58[PcmECChanI16_EXT], PcmEcDaState58[PcmECDAStateI16_EXT], PcmEcSaState58[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan58,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState58,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState58,"PcmEcSaState")

ADT_UInt16 PcmEcChan59[PcmECChanI16_EXT], PcmEcDaState59[PcmECDAStateI16_EXT], PcmEcSaState59[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan59,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState59,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState59,"PcmEcSaState")

ADT_UInt16 PcmEcChan60[PcmECChanI16_EXT], PcmEcDaState60[PcmECDAStateI16_EXT], PcmEcSaState60[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan60,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState60,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState60,"PcmEcSaState")

ADT_UInt16 PcmEcChan61[PcmECChanI16_EXT], PcmEcDaState61[PcmECDAStateI16_EXT], PcmEcSaState61[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan61,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState61,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState61,"PcmEcSaState")

ADT_UInt16 PcmEcChan62[PcmECChanI16_EXT], PcmEcDaState62[PcmECDAStateI16_EXT], PcmEcSaState62[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan62,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState62,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState62,"PcmEcSaState")

ADT_UInt16 PcmEcChan63[PcmECChanI16_EXT], PcmEcDaState63[PcmECDAStateI16_EXT], PcmEcSaState63[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan63,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState63,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState63,"PcmEcSaState")

ADT_UInt16 PcmEcChan64[PcmECChanI16_EXT], PcmEcDaState64[PcmECDAStateI16_EXT], PcmEcSaState64[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan64,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState64,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState64,"PcmEcSaState")

ADT_UInt16 PcmEcChan65[PcmECChanI16_EXT], PcmEcDaState65[PcmECDAStateI16_EXT], PcmEcSaState65[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan65,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState65,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState65,"PcmEcSaState")

ADT_UInt16 PcmEcChan66[PcmECChanI16_EXT], PcmEcDaState66[PcmECDAStateI16_EXT], PcmEcSaState66[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan66,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState66,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState66,"PcmEcSaState")

ADT_UInt16 PcmEcChan67[PcmECChanI16_EXT], PcmEcDaState67[PcmECDAStateI16_EXT], PcmEcSaState67[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan67,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState67,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState67,"PcmEcSaState")

ADT_UInt16 PcmEcChan68[PcmECChanI16_EXT], PcmEcDaState68[PcmECDAStateI16_EXT], PcmEcSaState68[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan68,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState68,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState68,"PcmEcSaState")

ADT_UInt16 PcmEcChan69[PcmECChanI16_EXT], PcmEcDaState69[PcmECDAStateI16_EXT], PcmEcSaState69[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan69,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState69,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState69,"PcmEcSaState")

ADT_UInt16 PcmEcChan70[PcmECChanI16_EXT], PcmEcDaState70[PcmECDAStateI16_EXT], PcmEcSaState70[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan70,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState70,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState70,"PcmEcSaState")

ADT_UInt16 PcmEcChan71[PcmECChanI16_EXT], PcmEcDaState71[PcmECDAStateI16_EXT], PcmEcSaState71[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan71,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState71,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState71,"PcmEcSaState")

ADT_UInt16 PcmEcChan72[PcmECChanI16_EXT], PcmEcDaState72[PcmECDAStateI16_EXT], PcmEcSaState72[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan72,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState72,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState72,"PcmEcSaState")

ADT_UInt16 PcmEcChan73[PcmECChanI16_EXT], PcmEcDaState73[PcmECDAStateI16_EXT], PcmEcSaState73[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan73,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState73,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState73,"PcmEcSaState")

ADT_UInt16 PcmEcChan74[PcmECChanI16_EXT], PcmEcDaState74[PcmECDAStateI16_EXT], PcmEcSaState74[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan74,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState74,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState74,"PcmEcSaState")

ADT_UInt16 PcmEcChan75[PcmECChanI16_EXT], PcmEcDaState75[PcmECDAStateI16_EXT], PcmEcSaState75[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan75,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState75,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState75,"PcmEcSaState")

ADT_UInt16 PcmEcChan76[PcmECChanI16_EXT], PcmEcDaState76[PcmECDAStateI16_EXT], PcmEcSaState76[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan76,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState76,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState76,"PcmEcSaState")

ADT_UInt16 PcmEcChan77[PcmECChanI16_EXT], PcmEcDaState77[PcmECDAStateI16_EXT], PcmEcSaState77[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan77,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState77,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState77,"PcmEcSaState")

ADT_UInt16 PcmEcChan78[PcmECChanI16_EXT], PcmEcDaState78[PcmECDAStateI16_EXT], PcmEcSaState78[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan78,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState78,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState78,"PcmEcSaState")

ADT_UInt16 PcmEcChan79[PcmECChanI16_EXT], PcmEcDaState79[PcmECDAStateI16_EXT], PcmEcSaState79[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan79,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState79,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState79,"PcmEcSaState")

ADT_UInt16 PcmEcChan80[PcmECChanI16_EXT], PcmEcDaState80[PcmECDAStateI16_EXT], PcmEcSaState80[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan80,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState80,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState80,"PcmEcSaState")

ADT_UInt16 PcmEcChan81[PcmECChanI16_EXT], PcmEcDaState81[PcmECDAStateI16_EXT], PcmEcSaState81[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan81,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState81,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState81,"PcmEcSaState")

ADT_UInt16 PcmEcChan82[PcmECChanI16_EXT], PcmEcDaState82[PcmECDAStateI16_EXT], PcmEcSaState82[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan82,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState82,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState82,"PcmEcSaState")

ADT_UInt16 PcmEcChan83[PcmECChanI16_EXT], PcmEcDaState83[PcmECDAStateI16_EXT], PcmEcSaState83[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan83,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState83,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState83,"PcmEcSaState")

ADT_UInt16 PcmEcChan84[PcmECChanI16_EXT], PcmEcDaState84[PcmECDAStateI16_EXT], PcmEcSaState84[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan84,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState84,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState84,"PcmEcSaState")

ADT_UInt16 PcmEcChan85[PcmECChanI16_EXT], PcmEcDaState85[PcmECDAStateI16_EXT], PcmEcSaState85[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan85,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState85,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState85,"PcmEcSaState")

ADT_UInt16 PcmEcChan86[PcmECChanI16_EXT], PcmEcDaState86[PcmECDAStateI16_EXT], PcmEcSaState86[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan86,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState86,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState86,"PcmEcSaState")

ADT_UInt16 PcmEcChan87[PcmECChanI16_EXT], PcmEcDaState87[PcmECDAStateI16_EXT], PcmEcSaState87[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan87,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState87,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState87,"PcmEcSaState")

ADT_UInt16 PcmEcChan88[PcmECChanI16_EXT], PcmEcDaState88[PcmECDAStateI16_EXT], PcmEcSaState88[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan88,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState88,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState88,"PcmEcSaState")

ADT_UInt16 PcmEcChan89[PcmECChanI16_EXT], PcmEcDaState89[PcmECDAStateI16_EXT], PcmEcSaState89[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan89,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState89,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState89,"PcmEcSaState")

ADT_UInt16 PcmEcChan90[PcmECChanI16_EXT], PcmEcDaState90[PcmECDAStateI16_EXT], PcmEcSaState90[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan90,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState90,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState90,"PcmEcSaState")

ADT_UInt16 PcmEcChan91[PcmECChanI16_EXT], PcmEcDaState91[PcmECDAStateI16_EXT], PcmEcSaState91[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan91,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState91,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState91,"PcmEcSaState")

ADT_UInt16 PcmEcChan92[PcmECChanI16_EXT], PcmEcDaState92[PcmECDAStateI16_EXT], PcmEcSaState92[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan92,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState92,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState92,"PcmEcSaState")

ADT_UInt16 PcmEcChan93[PcmECChanI16_EXT], PcmEcDaState93[PcmECDAStateI16_EXT], PcmEcSaState93[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan93,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState93,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState93,"PcmEcSaState")

ADT_UInt16 PcmEcChan94[PcmECChanI16_EXT], PcmEcDaState94[PcmECDAStateI16_EXT], PcmEcSaState94[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan94,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState94,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState94,"PcmEcSaState")

ADT_UInt16 PcmEcChan95[PcmECChanI16_EXT], PcmEcDaState95[PcmECDAStateI16_EXT], PcmEcSaState95[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan95,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState95,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState95,"PcmEcSaState")

ADT_UInt16 PcmEcChan96[PcmECChanI16_EXT], PcmEcDaState96[PcmECDAStateI16_EXT], PcmEcSaState96[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan96,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState96,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState96,"PcmEcSaState")

ADT_UInt16 PcmEcChan97[PcmECChanI16_EXT], PcmEcDaState97[PcmECDAStateI16_EXT], PcmEcSaState97[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan97,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState97,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState97,"PcmEcSaState")

ADT_UInt16 PcmEcChan98[PcmECChanI16_EXT], PcmEcDaState98[PcmECDAStateI16_EXT], PcmEcSaState98[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan98,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState98,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState98,"PcmEcSaState")

ADT_UInt16 PcmEcChan99[PcmECChanI16_EXT], PcmEcDaState99[PcmECDAStateI16_EXT], PcmEcSaState99[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan99,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState99,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState99,"PcmEcSaState")

ADT_UInt16 PcmEcChan100[PcmECChanI16_EXT], PcmEcDaState100[PcmECDAStateI16_EXT], PcmEcSaState100[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan100,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState100,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState100,"PcmEcSaState")

ADT_UInt16 PcmEcChan101[PcmECChanI16_EXT], PcmEcDaState101[PcmECDAStateI16_EXT], PcmEcSaState101[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan101,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState101,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState101,"PcmEcSaState")

ADT_UInt16 PcmEcChan102[PcmECChanI16_EXT], PcmEcDaState102[PcmECDAStateI16_EXT], PcmEcSaState102[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan102,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState102,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState102,"PcmEcSaState")

ADT_UInt16 PcmEcChan103[PcmECChanI16_EXT], PcmEcDaState103[PcmECDAStateI16_EXT], PcmEcSaState103[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan103,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState103,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState103,"PcmEcSaState")

ADT_UInt16 PcmEcChan104[PcmECChanI16_EXT], PcmEcDaState104[PcmECDAStateI16_EXT], PcmEcSaState104[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan104,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState104,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState104,"PcmEcSaState")

ADT_UInt16 PcmEcChan105[PcmECChanI16_EXT], PcmEcDaState105[PcmECDAStateI16_EXT], PcmEcSaState105[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan105,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState105,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState105,"PcmEcSaState")

ADT_UInt16 PcmEcChan106[PcmECChanI16_EXT], PcmEcDaState106[PcmECDAStateI16_EXT], PcmEcSaState106[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan106,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState106,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState106,"PcmEcSaState")

ADT_UInt16 PcmEcChan107[PcmECChanI16_EXT], PcmEcDaState107[PcmECDAStateI16_EXT], PcmEcSaState107[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan107,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState107,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState107,"PcmEcSaState")

ADT_UInt16 PcmEcChan108[PcmECChanI16_EXT], PcmEcDaState108[PcmECDAStateI16_EXT], PcmEcSaState108[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan108,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState108,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState108,"PcmEcSaState")

ADT_UInt16 PcmEcChan109[PcmECChanI16_EXT], PcmEcDaState109[PcmECDAStateI16_EXT], PcmEcSaState109[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan109,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState109,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState109,"PcmEcSaState")

ADT_UInt16 PcmEcChan110[PcmECChanI16_EXT], PcmEcDaState110[PcmECDAStateI16_EXT], PcmEcSaState110[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan110,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState110,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState110,"PcmEcSaState")

ADT_UInt16 PcmEcChan111[PcmECChanI16_EXT], PcmEcDaState111[PcmECDAStateI16_EXT], PcmEcSaState111[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan111,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState111,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState111,"PcmEcSaState")

ADT_UInt16 PcmEcChan112[PcmECChanI16_EXT], PcmEcDaState112[PcmECDAStateI16_EXT], PcmEcSaState112[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan112,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState112,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState112,"PcmEcSaState")

ADT_UInt16 PcmEcChan113[PcmECChanI16_EXT], PcmEcDaState113[PcmECDAStateI16_EXT], PcmEcSaState113[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan113,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState113,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState113,"PcmEcSaState")

ADT_UInt16 PcmEcChan114[PcmECChanI16_EXT], PcmEcDaState114[PcmECDAStateI16_EXT], PcmEcSaState114[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan114,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState114,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState114,"PcmEcSaState")

ADT_UInt16 PcmEcChan115[PcmECChanI16_EXT], PcmEcDaState115[PcmECDAStateI16_EXT], PcmEcSaState115[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan115,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState115,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState115,"PcmEcSaState")

ADT_UInt16 PcmEcChan116[PcmECChanI16_EXT], PcmEcDaState116[PcmECDAStateI16_EXT], PcmEcSaState116[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan116,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState116,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState116,"PcmEcSaState")

ADT_UInt16 PcmEcChan117[PcmECChanI16_EXT], PcmEcDaState117[PcmECDAStateI16_EXT], PcmEcSaState117[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan117,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState117,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState117,"PcmEcSaState")

ADT_UInt16 PcmEcChan118[PcmECChanI16_EXT], PcmEcDaState118[PcmECDAStateI16_EXT], PcmEcSaState118[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan118,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState118,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState118,"PcmEcSaState")

ADT_UInt16 PcmEcChan119[PcmECChanI16_EXT], PcmEcDaState119[PcmECDAStateI16_EXT], PcmEcSaState119[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan119,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState119,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState119,"PcmEcSaState")

ADT_UInt16 PcmEcChan120[PcmECChanI16_EXT], PcmEcDaState120[PcmECDAStateI16_EXT], PcmEcSaState120[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan120,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState120,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState120,"PcmEcSaState")

ADT_UInt16 PcmEcChan121[PcmECChanI16_EXT], PcmEcDaState121[PcmECDAStateI16_EXT], PcmEcSaState121[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan121,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState121,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState121,"PcmEcSaState")

ADT_UInt16 PcmEcChan122[PcmECChanI16_EXT], PcmEcDaState122[PcmECDAStateI16_EXT], PcmEcSaState122[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan122,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState122,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState122,"PcmEcSaState")

ADT_UInt16 PcmEcChan123[PcmECChanI16_EXT], PcmEcDaState123[PcmECDAStateI16_EXT], PcmEcSaState123[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan123,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState123,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState123,"PcmEcSaState")

ADT_UInt16 PcmEcChan124[PcmECChanI16_EXT], PcmEcDaState124[PcmECDAStateI16_EXT], PcmEcSaState124[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan124,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState124,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState124,"PcmEcSaState")

ADT_UInt16 PcmEcChan125[PcmECChanI16_EXT], PcmEcDaState125[PcmECDAStateI16_EXT], PcmEcSaState125[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan125,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState125,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState125,"PcmEcSaState")

ADT_UInt16 PcmEcChan126[PcmECChanI16_EXT], PcmEcDaState126[PcmECDAStateI16_EXT], PcmEcSaState126[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan126,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState126,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState126,"PcmEcSaState")

ADT_UInt16 PcmEcChan127[PcmECChanI16_EXT], PcmEcDaState127[PcmECDAStateI16_EXT], PcmEcSaState127[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan127,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState127,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState127,"PcmEcSaState")

ADT_UInt16 PcmEcChan128[PcmECChanI16_EXT], PcmEcDaState128[PcmECDAStateI16_EXT], PcmEcSaState128[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan128,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState128,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState128,"PcmEcSaState")

ADT_UInt16 PcmEcChan129[PcmECChanI16_EXT], PcmEcDaState129[PcmECDAStateI16_EXT], PcmEcSaState129[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan129,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState129,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState129,"PcmEcSaState")

ADT_UInt16 PcmEcChan130[PcmECChanI16_EXT], PcmEcDaState130[PcmECDAStateI16_EXT], PcmEcSaState130[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan130,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState130,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState130,"PcmEcSaState")

ADT_UInt16 PcmEcChan131[PcmECChanI16_EXT], PcmEcDaState131[PcmECDAStateI16_EXT], PcmEcSaState131[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan131,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState131,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState131,"PcmEcSaState")

ADT_UInt16 PcmEcChan132[PcmECChanI16_EXT], PcmEcDaState132[PcmECDAStateI16_EXT], PcmEcSaState132[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan132,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState132,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState132,"PcmEcSaState")

ADT_UInt16 PcmEcChan133[PcmECChanI16_EXT], PcmEcDaState133[PcmECDAStateI16_EXT], PcmEcSaState133[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan133,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState133,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState133,"PcmEcSaState")

ADT_UInt16 PcmEcChan134[PcmECChanI16_EXT], PcmEcDaState134[PcmECDAStateI16_EXT], PcmEcSaState134[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan134,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState134,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState134,"PcmEcSaState")

ADT_UInt16 PcmEcChan135[PcmECChanI16_EXT], PcmEcDaState135[PcmECDAStateI16_EXT], PcmEcSaState135[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan135,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState135,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState135,"PcmEcSaState")

ADT_UInt16 PcmEcChan136[PcmECChanI16_EXT], PcmEcDaState136[PcmECDAStateI16_EXT], PcmEcSaState136[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan136,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState136,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState136,"PcmEcSaState")

ADT_UInt16 PcmEcChan137[PcmECChanI16_EXT], PcmEcDaState137[PcmECDAStateI16_EXT], PcmEcSaState137[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan137,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState137,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState137,"PcmEcSaState")

ADT_UInt16 PcmEcChan138[PcmECChanI16_EXT], PcmEcDaState138[PcmECDAStateI16_EXT], PcmEcSaState138[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan138,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState138,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState138,"PcmEcSaState")

ADT_UInt16 PcmEcChan139[PcmECChanI16_EXT], PcmEcDaState139[PcmECDAStateI16_EXT], PcmEcSaState139[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan139,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState139,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState139,"PcmEcSaState")

ADT_UInt16 PcmEcChan140[PcmECChanI16_EXT], PcmEcDaState140[PcmECDAStateI16_EXT], PcmEcSaState140[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan140,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState140,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState140,"PcmEcSaState")

ADT_UInt16 PcmEcChan141[PcmECChanI16_EXT], PcmEcDaState141[PcmECDAStateI16_EXT], PcmEcSaState141[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan141,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState141,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState141,"PcmEcSaState")

ADT_UInt16 PcmEcChan142[PcmECChanI16_EXT], PcmEcDaState142[PcmECDAStateI16_EXT], PcmEcSaState142[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan142,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState142,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState142,"PcmEcSaState")

ADT_UInt16 PcmEcChan143[PcmECChanI16_EXT], PcmEcDaState143[PcmECDAStateI16_EXT], PcmEcSaState143[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan143,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState143,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState143,"PcmEcSaState")

ADT_UInt16 PcmEcChan144[PcmECChanI16_EXT], PcmEcDaState144[PcmECDAStateI16_EXT], PcmEcSaState144[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan144,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState144,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState144,"PcmEcSaState")

ADT_UInt16 PcmEcChan145[PcmECChanI16_EXT], PcmEcDaState145[PcmECDAStateI16_EXT], PcmEcSaState145[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan145,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState145,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState145,"PcmEcSaState")

ADT_UInt16 PcmEcChan146[PcmECChanI16_EXT], PcmEcDaState146[PcmECDAStateI16_EXT], PcmEcSaState146[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan146,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState146,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState146,"PcmEcSaState")

ADT_UInt16 PcmEcChan147[PcmECChanI16_EXT], PcmEcDaState147[PcmECDAStateI16_EXT], PcmEcSaState147[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan147,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState147,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState147,"PcmEcSaState")

ADT_UInt16 PcmEcChan148[PcmECChanI16_EXT], PcmEcDaState148[PcmECDAStateI16_EXT], PcmEcSaState148[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan148,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState148,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState148,"PcmEcSaState")

ADT_UInt16 PcmEcChan149[PcmECChanI16_EXT], PcmEcDaState149[PcmECDAStateI16_EXT], PcmEcSaState149[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan149,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState149,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState149,"PcmEcSaState")

ADT_UInt16 PcmEcChan150[PcmECChanI16_EXT], PcmEcDaState150[PcmECDAStateI16_EXT], PcmEcSaState150[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan150,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState150,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState150,"PcmEcSaState")

ADT_UInt16 PcmEcChan151[PcmECChanI16_EXT], PcmEcDaState151[PcmECDAStateI16_EXT], PcmEcSaState151[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan151,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState151,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState151,"PcmEcSaState")

ADT_UInt16 PcmEcChan152[PcmECChanI16_EXT], PcmEcDaState152[PcmECDAStateI16_EXT], PcmEcSaState152[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan152,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState152,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState152,"PcmEcSaState")

ADT_UInt16 PcmEcChan153[PcmECChanI16_EXT], PcmEcDaState153[PcmECDAStateI16_EXT], PcmEcSaState153[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan153,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState153,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState153,"PcmEcSaState")

ADT_UInt16 PcmEcChan154[PcmECChanI16_EXT], PcmEcDaState154[PcmECDAStateI16_EXT], PcmEcSaState154[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan154,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState154,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState154,"PcmEcSaState")

ADT_UInt16 PcmEcChan155[PcmECChanI16_EXT], PcmEcDaState155[PcmECDAStateI16_EXT], PcmEcSaState155[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan155,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState155,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState155,"PcmEcSaState")

ADT_UInt16 PcmEcChan156[PcmECChanI16_EXT], PcmEcDaState156[PcmECDAStateI16_EXT], PcmEcSaState156[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan156,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState156,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState156,"PcmEcSaState")

ADT_UInt16 PcmEcChan157[PcmECChanI16_EXT], PcmEcDaState157[PcmECDAStateI16_EXT], PcmEcSaState157[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan157,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState157,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState157,"PcmEcSaState")

ADT_UInt16 PcmEcChan158[PcmECChanI16_EXT], PcmEcDaState158[PcmECDAStateI16_EXT], PcmEcSaState158[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan158,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState158,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState158,"PcmEcSaState")

ADT_UInt16 PcmEcChan159[PcmECChanI16_EXT], PcmEcDaState159[PcmECDAStateI16_EXT], PcmEcSaState159[PcmECSAStateI16_EXT];
#pragma DATA_SECTION (PcmEcChan159,"PcmEcChan")
#pragma DATA_SECTION (PcmEcDaState159,"PcmEcDaState")
#pragma DATA_SECTION (PcmEcSaState159,"PcmEcSaState")

//}
//{

ADT_UInt16 PcmEcEchoPath0[PcmEchoPathI16_EXT];
ADT_UInt16 PcmEcBackEp0  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath0,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp0,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath1[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp1  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath1,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp1,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath2[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp2  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath2,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp2,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath3[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp3  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath3,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp3,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath4[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp4  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath4,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp4,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath5[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp5  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath5,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp5,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath6[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp6  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath6,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp6,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath7[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp7  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath7,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp7,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath8[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp8  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath8,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp8,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath9[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp9  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath9,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp9,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath10[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp10  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath10,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp10,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath11[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp11  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath11,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp11,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath12[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp12  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath12,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp12,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath13[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp13  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath13,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp13,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath14[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp14  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath14,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp14,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath15[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp15  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath15,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp15,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath16[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp16  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath16,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp16,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath17[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp17  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath17,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp17,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath18[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp18  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath18,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp18,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath19[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp19  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath19,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp19,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath20[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp20  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath20,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp20,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath21[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp21  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath21,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp21,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath22[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp22  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath22,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp22,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath23[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp23  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath23,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp23,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath24[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp24  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath24,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp24,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath25[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp25  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath25,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp25,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath26[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp26  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath26,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp26,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath27[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp27  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath27,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp27,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath28[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp28  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath28,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp28,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath29[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp29  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath29,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp29,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath30[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp30  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath30,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp30,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath31[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp31  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath31,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp31,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath32[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp32  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath32,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp32,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath33[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp33  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath33,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp33,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath34[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp34  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath34,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp34,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath35[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp35  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath35,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp35,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath36[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp36  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath36,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp36,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath37[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp37  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath37,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp37,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath38[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp38  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath38,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp38,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath39[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp39  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath39,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp39,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath40[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp40  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath40,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp40,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath41[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp41  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath41,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp41,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath42[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp42  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath42,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp42,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath43[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp43  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath43,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp43,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath44[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp44  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath44,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp44,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath45[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp45  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath45,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp45,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath46[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp46  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath46,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp46,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath47[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp47  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath47,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp47,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath48[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp48  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath48,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp48,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath49[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp49  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath49,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp49,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath50[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp50  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath50,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp50,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath51[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp51  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath51,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp51,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath52[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp52  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath52,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp52,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath53[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp53  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath53,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp53,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath54[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp54  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath54,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp54,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath55[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp55  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath55,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp55,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath56[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp56  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath56,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp56,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath57[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp57  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath57,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp57,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath58[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp58  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath58,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp58,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath59[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp59  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath59,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp59,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath60[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp60  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath60,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp60,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath61[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp61  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath61,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp61,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath62[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp62  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath62,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp62,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath63[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp63  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath63,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp63,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath64[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp64  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath64,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp64,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath65[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp65  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath65,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp65,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath66[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp66  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath66,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp66,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath67[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp67  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath67,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp67,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath68[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp68  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath68,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp68,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath69[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp69  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath69,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp69,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath70[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp70  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath70,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp70,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath71[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp71  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath71,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp71,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath72[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp72  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath72,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp72,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath73[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp73  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath73,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp73,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath74[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp74  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath74,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp74,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath75[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp75  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath75,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp75,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath76[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp76  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath76,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp76,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath77[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp77  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath77,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp77,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath78[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp78  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath78,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp78,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath79[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp79  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath79,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp79,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath80[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp80  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath80,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp80,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath81[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp81  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath81,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp81,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath82[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp82  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath82,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp82,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath83[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp83  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath83,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp83,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath84[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp84  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath84,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp84,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath85[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp85  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath85,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp85,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath86[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp86  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath86,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp86,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath87[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp87  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath87,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp87,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath88[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp88  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath88,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp88,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath89[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp89  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath89,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp89,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath90[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp90  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath90,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp90,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath91[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp91  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath91,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp91,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath92[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp92  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath92,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp92,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath93[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp93  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath93,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp93,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath94[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp94  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath94,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp94,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath95[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp95  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath95,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp95,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath96[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp96  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath96,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp96,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath97[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp97  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath97,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp97,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath98[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp98  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath98,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp98,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath99[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp99  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath99,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp99,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath100[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp100  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath100,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp100,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath101[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp101  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath101,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp101,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath102[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp102  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath102,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp102,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath103[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp103  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath103,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp103,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath104[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp104  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath104,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp104,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath105[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp105  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath105,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp105,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath106[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp106  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath106,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp106,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath107[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp107  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath107,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp107,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath108[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp108  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath108,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp108,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath109[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp109  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath109,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp109,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath110[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp110  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath110,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp110,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath111[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp111  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath111,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp111,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath112[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp112  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath112,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp112,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath113[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp113  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath113,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp113,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath114[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp114  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath114,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp114,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath115[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp115  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath115,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp115,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath116[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp116  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath116,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp116,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath117[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp117  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath117,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp117,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath118[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp118  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath118,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp118,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath119[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp119  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath119,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp119,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath120[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp120  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath120,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp120,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath121[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp121  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath121,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp121,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath122[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp122  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath122,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp122,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath123[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp123  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath123,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp123,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath124[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp124  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath124,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp124,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath125[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp125  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath125,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp125,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath126[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp126  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath126,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp126,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath127[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp127  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath127,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp127,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath128[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp128  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath128,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp128,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath129[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp129  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath129,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp129,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath130[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp130  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath130,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp130,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath131[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp131  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath131,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp131,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath132[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp132  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath132,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp132,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath133[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp133  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath133,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp133,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath134[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp134  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath134,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp134,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath135[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp135  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath135,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp135,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath136[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp136  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath136,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp136,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath137[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp137  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath137,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp137,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath138[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp138  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath138,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp138,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath139[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp139  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath139,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp139,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath140[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp140  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath140,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp140,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath141[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp141  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath141,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp141,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath142[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp142  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath142,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp142,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath143[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp143  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath143,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp143,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath144[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp144  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath144,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp144,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath145[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp145  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath145,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp145,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath146[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp146  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath146,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp146,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath147[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp147  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath147,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp147,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath148[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp148  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath148,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp148,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath149[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp149  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath149,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp149,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath150[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp150  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath150,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp150,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath151[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp151  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath151,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp151,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath152[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp152  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath152,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp152,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath153[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp153  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath153,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp153,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath154[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp154  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath154,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp154,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath155[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp155  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath155,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp155,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath156[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp156  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath156,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp156,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath157[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp157  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath157,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp157,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath158[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp158  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath158,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp158,"PcmEcBackEp")

static ADT_UInt16 PcmEcEchoPath159[PcmEchoPathI16_EXT];
static ADT_UInt16 PcmEcBackEp159  [PcmBgEchoPathI16_EXT];
#pragma DATA_SECTION (PcmEcEchoPath159,"PcmEcEchoPath")
#pragma DATA_SECTION (PcmEcBackEp159,"PcmEcBackEp")

#pragma DATA_ALIGN (PcmEcChan0,     128)
#pragma DATA_ALIGN (PcmEcDaState0,  128)
#pragma DATA_ALIGN (PcmEcSaState0,  128)
#pragma DATA_ALIGN (PcmEcEchoPath0, 128)
#pragma DATA_ALIGN (PcmEcBackEp0,   128)
#pragma DATA_ALIGN (PcmEcChan1,     128)
#pragma DATA_ALIGN (PcmEcDaState1,  128)
#pragma DATA_ALIGN (PcmEcSaState1,  128)
#pragma DATA_ALIGN (PcmEcEchoPath1, 128)
#pragma DATA_ALIGN (PcmEcBackEp1,   128)
#pragma DATA_ALIGN (PcmEcChan2,     128)
#pragma DATA_ALIGN (PcmEcDaState2,  128)
#pragma DATA_ALIGN (PcmEcSaState2,  128)
#pragma DATA_ALIGN (PcmEcEchoPath2, 128)
#pragma DATA_ALIGN (PcmEcBackEp2,   128)
#pragma DATA_ALIGN (PcmEcChan3,     128)
#pragma DATA_ALIGN (PcmEcDaState3,  128)
#pragma DATA_ALIGN (PcmEcSaState3,  128)
#pragma DATA_ALIGN (PcmEcEchoPath3, 128)
#pragma DATA_ALIGN (PcmEcBackEp3,   128)
#pragma DATA_ALIGN (PcmEcChan4,     128)
#pragma DATA_ALIGN (PcmEcDaState4,  128)
#pragma DATA_ALIGN (PcmEcSaState4,  128)
#pragma DATA_ALIGN (PcmEcEchoPath4, 128)
#pragma DATA_ALIGN (PcmEcBackEp4,   128)
#pragma DATA_ALIGN (PcmEcChan5,     128)
#pragma DATA_ALIGN (PcmEcDaState5,  128)
#pragma DATA_ALIGN (PcmEcSaState5,  128)
#pragma DATA_ALIGN (PcmEcEchoPath5, 128)
#pragma DATA_ALIGN (PcmEcBackEp5,   128)
#pragma DATA_ALIGN (PcmEcChan6,     128)
#pragma DATA_ALIGN (PcmEcDaState6,  128)
#pragma DATA_ALIGN (PcmEcSaState6,  128)
#pragma DATA_ALIGN (PcmEcEchoPath6, 128)
#pragma DATA_ALIGN (PcmEcBackEp6,   128)
#pragma DATA_ALIGN (PcmEcChan7,     128)
#pragma DATA_ALIGN (PcmEcDaState7,  128)
#pragma DATA_ALIGN (PcmEcSaState7,  128)
#pragma DATA_ALIGN (PcmEcEchoPath7, 128)
#pragma DATA_ALIGN (PcmEcBackEp7,   128)
#pragma DATA_ALIGN (PcmEcChan8,     128)
#pragma DATA_ALIGN (PcmEcDaState8,  128)
#pragma DATA_ALIGN (PcmEcSaState8,  128)
#pragma DATA_ALIGN (PcmEcEchoPath8, 128)
#pragma DATA_ALIGN (PcmEcBackEp8,   128)
#pragma DATA_ALIGN (PcmEcChan9,     128)
#pragma DATA_ALIGN (PcmEcDaState9,  128)
#pragma DATA_ALIGN (PcmEcSaState9,  128)
#pragma DATA_ALIGN (PcmEcEchoPath9, 128)
#pragma DATA_ALIGN (PcmEcBackEp9,   128)
#pragma DATA_ALIGN (PcmEcChan10,     128)
#pragma DATA_ALIGN (PcmEcDaState10,  128)
#pragma DATA_ALIGN (PcmEcSaState10,  128)
#pragma DATA_ALIGN (PcmEcEchoPath10, 128)
#pragma DATA_ALIGN (PcmEcBackEp10,   128)
#pragma DATA_ALIGN (PcmEcChan11,     128)
#pragma DATA_ALIGN (PcmEcDaState11,  128)
#pragma DATA_ALIGN (PcmEcSaState11,  128)
#pragma DATA_ALIGN (PcmEcEchoPath11, 128)
#pragma DATA_ALIGN (PcmEcBackEp11,   128)
#pragma DATA_ALIGN (PcmEcChan12,     128)
#pragma DATA_ALIGN (PcmEcDaState12,  128)
#pragma DATA_ALIGN (PcmEcSaState12,  128)
#pragma DATA_ALIGN (PcmEcEchoPath12, 128)
#pragma DATA_ALIGN (PcmEcBackEp12,   128)
#pragma DATA_ALIGN (PcmEcChan13,     128)
#pragma DATA_ALIGN (PcmEcDaState13,  128)
#pragma DATA_ALIGN (PcmEcSaState13,  128)
#pragma DATA_ALIGN (PcmEcEchoPath13, 128)
#pragma DATA_ALIGN (PcmEcBackEp13,   128)
#pragma DATA_ALIGN (PcmEcChan14,     128)
#pragma DATA_ALIGN (PcmEcDaState14,  128)
#pragma DATA_ALIGN (PcmEcSaState14,  128)
#pragma DATA_ALIGN (PcmEcEchoPath14, 128)
#pragma DATA_ALIGN (PcmEcBackEp14,   128)
#pragma DATA_ALIGN (PcmEcChan15,     128)
#pragma DATA_ALIGN (PcmEcDaState15,  128)
#pragma DATA_ALIGN (PcmEcSaState15,  128)
#pragma DATA_ALIGN (PcmEcEchoPath15, 128)
#pragma DATA_ALIGN (PcmEcBackEp15,   128)
#pragma DATA_ALIGN (PcmEcChan16,     128)
#pragma DATA_ALIGN (PcmEcDaState16,  128)
#pragma DATA_ALIGN (PcmEcSaState16,  128)
#pragma DATA_ALIGN (PcmEcEchoPath16, 128)
#pragma DATA_ALIGN (PcmEcBackEp16,   128)
#pragma DATA_ALIGN (PcmEcChan17,     128)
#pragma DATA_ALIGN (PcmEcDaState17,  128)
#pragma DATA_ALIGN (PcmEcSaState17,  128)
#pragma DATA_ALIGN (PcmEcEchoPath17, 128)
#pragma DATA_ALIGN (PcmEcBackEp17,   128)
#pragma DATA_ALIGN (PcmEcChan18,     128)
#pragma DATA_ALIGN (PcmEcDaState18,  128)
#pragma DATA_ALIGN (PcmEcSaState18,  128)
#pragma DATA_ALIGN (PcmEcEchoPath18, 128)
#pragma DATA_ALIGN (PcmEcBackEp18,   128)
#pragma DATA_ALIGN (PcmEcChan19,     128)
#pragma DATA_ALIGN (PcmEcDaState19,  128)
#pragma DATA_ALIGN (PcmEcSaState19,  128)
#pragma DATA_ALIGN (PcmEcEchoPath19, 128)
#pragma DATA_ALIGN (PcmEcBackEp19,   128)
#pragma DATA_ALIGN (PcmEcChan20,     128)
#pragma DATA_ALIGN (PcmEcDaState20,  128)
#pragma DATA_ALIGN (PcmEcSaState20,  128)
#pragma DATA_ALIGN (PcmEcEchoPath20, 128)
#pragma DATA_ALIGN (PcmEcBackEp20,   128)
#pragma DATA_ALIGN (PcmEcChan21,     128)
#pragma DATA_ALIGN (PcmEcDaState21,  128)
#pragma DATA_ALIGN (PcmEcSaState21,  128)
#pragma DATA_ALIGN (PcmEcEchoPath21, 128)
#pragma DATA_ALIGN (PcmEcBackEp21,   128)
#pragma DATA_ALIGN (PcmEcChan22,     128)
#pragma DATA_ALIGN (PcmEcDaState22,  128)
#pragma DATA_ALIGN (PcmEcSaState22,  128)
#pragma DATA_ALIGN (PcmEcEchoPath22, 128)
#pragma DATA_ALIGN (PcmEcBackEp22,   128)
#pragma DATA_ALIGN (PcmEcChan23,     128)
#pragma DATA_ALIGN (PcmEcDaState23,  128)
#pragma DATA_ALIGN (PcmEcSaState23,  128)
#pragma DATA_ALIGN (PcmEcEchoPath23, 128)
#pragma DATA_ALIGN (PcmEcBackEp23,   128)
#pragma DATA_ALIGN (PcmEcChan24,     128)
#pragma DATA_ALIGN (PcmEcDaState24,  128)
#pragma DATA_ALIGN (PcmEcSaState24,  128)
#pragma DATA_ALIGN (PcmEcEchoPath24, 128)
#pragma DATA_ALIGN (PcmEcBackEp24,   128)
#pragma DATA_ALIGN (PcmEcChan25,     128)
#pragma DATA_ALIGN (PcmEcDaState25,  128)
#pragma DATA_ALIGN (PcmEcSaState25,  128)
#pragma DATA_ALIGN (PcmEcEchoPath25, 128)
#pragma DATA_ALIGN (PcmEcBackEp25,   128)
#pragma DATA_ALIGN (PcmEcChan26,     128)
#pragma DATA_ALIGN (PcmEcDaState26,  128)
#pragma DATA_ALIGN (PcmEcSaState26,  128)
#pragma DATA_ALIGN (PcmEcEchoPath26, 128)
#pragma DATA_ALIGN (PcmEcBackEp26,   128)
#pragma DATA_ALIGN (PcmEcChan27,     128)
#pragma DATA_ALIGN (PcmEcDaState27,  128)
#pragma DATA_ALIGN (PcmEcSaState27,  128)
#pragma DATA_ALIGN (PcmEcEchoPath27, 128)
#pragma DATA_ALIGN (PcmEcBackEp27,   128)
#pragma DATA_ALIGN (PcmEcChan28,     128)
#pragma DATA_ALIGN (PcmEcDaState28,  128)
#pragma DATA_ALIGN (PcmEcSaState28,  128)
#pragma DATA_ALIGN (PcmEcEchoPath28, 128)
#pragma DATA_ALIGN (PcmEcBackEp28,   128)
#pragma DATA_ALIGN (PcmEcChan29,     128)
#pragma DATA_ALIGN (PcmEcDaState29,  128)
#pragma DATA_ALIGN (PcmEcSaState29,  128)
#pragma DATA_ALIGN (PcmEcEchoPath29, 128)
#pragma DATA_ALIGN (PcmEcBackEp29,   128)
#pragma DATA_ALIGN (PcmEcChan30,     128)
#pragma DATA_ALIGN (PcmEcDaState30,  128)
#pragma DATA_ALIGN (PcmEcSaState30,  128)
#pragma DATA_ALIGN (PcmEcEchoPath30, 128)
#pragma DATA_ALIGN (PcmEcBackEp30,   128)
#pragma DATA_ALIGN (PcmEcChan31,     128)
#pragma DATA_ALIGN (PcmEcDaState31,  128)
#pragma DATA_ALIGN (PcmEcSaState31,  128)
#pragma DATA_ALIGN (PcmEcEchoPath31, 128)
#pragma DATA_ALIGN (PcmEcBackEp31,   128)
#pragma DATA_ALIGN (PcmEcChan32,     128)
#pragma DATA_ALIGN (PcmEcDaState32,  128)
#pragma DATA_ALIGN (PcmEcSaState32,  128)
#pragma DATA_ALIGN (PcmEcEchoPath32, 128)
#pragma DATA_ALIGN (PcmEcBackEp32,   128)
#pragma DATA_ALIGN (PcmEcChan33,     128)
#pragma DATA_ALIGN (PcmEcDaState33,  128)
#pragma DATA_ALIGN (PcmEcSaState33,  128)
#pragma DATA_ALIGN (PcmEcEchoPath33, 128)
#pragma DATA_ALIGN (PcmEcBackEp33,   128)
#pragma DATA_ALIGN (PcmEcChan34,     128)
#pragma DATA_ALIGN (PcmEcDaState34,  128)
#pragma DATA_ALIGN (PcmEcSaState34,  128)
#pragma DATA_ALIGN (PcmEcEchoPath34, 128)
#pragma DATA_ALIGN (PcmEcBackEp34,   128)
#pragma DATA_ALIGN (PcmEcChan35,     128)
#pragma DATA_ALIGN (PcmEcDaState35,  128)
#pragma DATA_ALIGN (PcmEcSaState35,  128)
#pragma DATA_ALIGN (PcmEcEchoPath35, 128)
#pragma DATA_ALIGN (PcmEcBackEp35,   128)
#pragma DATA_ALIGN (PcmEcChan36,     128)
#pragma DATA_ALIGN (PcmEcDaState36,  128)
#pragma DATA_ALIGN (PcmEcSaState36,  128)
#pragma DATA_ALIGN (PcmEcEchoPath36, 128)
#pragma DATA_ALIGN (PcmEcBackEp36,   128)
#pragma DATA_ALIGN (PcmEcChan37,     128)
#pragma DATA_ALIGN (PcmEcDaState37,  128)
#pragma DATA_ALIGN (PcmEcSaState37,  128)
#pragma DATA_ALIGN (PcmEcEchoPath37, 128)
#pragma DATA_ALIGN (PcmEcBackEp37,   128)
#pragma DATA_ALIGN (PcmEcChan38,     128)
#pragma DATA_ALIGN (PcmEcDaState38,  128)
#pragma DATA_ALIGN (PcmEcSaState38,  128)
#pragma DATA_ALIGN (PcmEcEchoPath38, 128)
#pragma DATA_ALIGN (PcmEcBackEp38,   128)
#pragma DATA_ALIGN (PcmEcChan39,     128)
#pragma DATA_ALIGN (PcmEcDaState39,  128)
#pragma DATA_ALIGN (PcmEcSaState39,  128)
#pragma DATA_ALIGN (PcmEcEchoPath39, 128)
#pragma DATA_ALIGN (PcmEcBackEp39,   128)
#pragma DATA_ALIGN (PcmEcChan40,     128)
#pragma DATA_ALIGN (PcmEcDaState40,  128)
#pragma DATA_ALIGN (PcmEcSaState40,  128)
#pragma DATA_ALIGN (PcmEcEchoPath40, 128)
#pragma DATA_ALIGN (PcmEcBackEp40,   128)
#pragma DATA_ALIGN (PcmEcChan41,     128)
#pragma DATA_ALIGN (PcmEcDaState41,  128)
#pragma DATA_ALIGN (PcmEcSaState41,  128)
#pragma DATA_ALIGN (PcmEcEchoPath41, 128)
#pragma DATA_ALIGN (PcmEcBackEp41,   128)
#pragma DATA_ALIGN (PcmEcChan42,     128)
#pragma DATA_ALIGN (PcmEcDaState42,  128)
#pragma DATA_ALIGN (PcmEcSaState42,  128)
#pragma DATA_ALIGN (PcmEcEchoPath42, 128)
#pragma DATA_ALIGN (PcmEcBackEp42,   128)
#pragma DATA_ALIGN (PcmEcChan43,     128)
#pragma DATA_ALIGN (PcmEcDaState43,  128)
#pragma DATA_ALIGN (PcmEcSaState43,  128)
#pragma DATA_ALIGN (PcmEcEchoPath43, 128)
#pragma DATA_ALIGN (PcmEcBackEp43,   128)
#pragma DATA_ALIGN (PcmEcChan44,     128)
#pragma DATA_ALIGN (PcmEcDaState44,  128)
#pragma DATA_ALIGN (PcmEcSaState44,  128)
#pragma DATA_ALIGN (PcmEcEchoPath44, 128)
#pragma DATA_ALIGN (PcmEcBackEp44,   128)
#pragma DATA_ALIGN (PcmEcChan45,     128)
#pragma DATA_ALIGN (PcmEcDaState45,  128)
#pragma DATA_ALIGN (PcmEcSaState45,  128)
#pragma DATA_ALIGN (PcmEcEchoPath45, 128)
#pragma DATA_ALIGN (PcmEcBackEp45,   128)
#pragma DATA_ALIGN (PcmEcChan46,     128)
#pragma DATA_ALIGN (PcmEcDaState46,  128)
#pragma DATA_ALIGN (PcmEcSaState46,  128)
#pragma DATA_ALIGN (PcmEcEchoPath46, 128)
#pragma DATA_ALIGN (PcmEcBackEp46,   128)
#pragma DATA_ALIGN (PcmEcChan47,     128)
#pragma DATA_ALIGN (PcmEcDaState47,  128)
#pragma DATA_ALIGN (PcmEcSaState47,  128)
#pragma DATA_ALIGN (PcmEcEchoPath47, 128)
#pragma DATA_ALIGN (PcmEcBackEp47,   128)
#pragma DATA_ALIGN (PcmEcChan48,     128)
#pragma DATA_ALIGN (PcmEcDaState48,  128)
#pragma DATA_ALIGN (PcmEcSaState48,  128)
#pragma DATA_ALIGN (PcmEcEchoPath48, 128)
#pragma DATA_ALIGN (PcmEcBackEp48,   128)
#pragma DATA_ALIGN (PcmEcChan49,     128)
#pragma DATA_ALIGN (PcmEcDaState49,  128)
#pragma DATA_ALIGN (PcmEcSaState49,  128)
#pragma DATA_ALIGN (PcmEcEchoPath49, 128)
#pragma DATA_ALIGN (PcmEcBackEp49,   128)
#pragma DATA_ALIGN (PcmEcChan50,     128)
#pragma DATA_ALIGN (PcmEcDaState50,  128)
#pragma DATA_ALIGN (PcmEcSaState50,  128)
#pragma DATA_ALIGN (PcmEcEchoPath50, 128)
#pragma DATA_ALIGN (PcmEcBackEp50,   128)
#pragma DATA_ALIGN (PcmEcChan51,     128)
#pragma DATA_ALIGN (PcmEcDaState51,  128)
#pragma DATA_ALIGN (PcmEcSaState51,  128)
#pragma DATA_ALIGN (PcmEcEchoPath51, 128)
#pragma DATA_ALIGN (PcmEcBackEp51,   128)
#pragma DATA_ALIGN (PcmEcChan52,     128)
#pragma DATA_ALIGN (PcmEcDaState52,  128)
#pragma DATA_ALIGN (PcmEcSaState52,  128)
#pragma DATA_ALIGN (PcmEcEchoPath52, 128)
#pragma DATA_ALIGN (PcmEcBackEp52,   128)
#pragma DATA_ALIGN (PcmEcChan53,     128)
#pragma DATA_ALIGN (PcmEcDaState53,  128)
#pragma DATA_ALIGN (PcmEcSaState53,  128)
#pragma DATA_ALIGN (PcmEcEchoPath53, 128)
#pragma DATA_ALIGN (PcmEcBackEp53,   128)
#pragma DATA_ALIGN (PcmEcChan54,     128)
#pragma DATA_ALIGN (PcmEcDaState54,  128)
#pragma DATA_ALIGN (PcmEcSaState54,  128)
#pragma DATA_ALIGN (PcmEcEchoPath54, 128)
#pragma DATA_ALIGN (PcmEcBackEp54,   128)
#pragma DATA_ALIGN (PcmEcChan55,     128)
#pragma DATA_ALIGN (PcmEcDaState55,  128)
#pragma DATA_ALIGN (PcmEcSaState55,  128)
#pragma DATA_ALIGN (PcmEcEchoPath55, 128)
#pragma DATA_ALIGN (PcmEcBackEp55,   128)
#pragma DATA_ALIGN (PcmEcChan56,     128)
#pragma DATA_ALIGN (PcmEcDaState56,  128)
#pragma DATA_ALIGN (PcmEcSaState56,  128)
#pragma DATA_ALIGN (PcmEcEchoPath56, 128)
#pragma DATA_ALIGN (PcmEcBackEp56,   128)
#pragma DATA_ALIGN (PcmEcChan57,     128)
#pragma DATA_ALIGN (PcmEcDaState57,  128)
#pragma DATA_ALIGN (PcmEcSaState57,  128)
#pragma DATA_ALIGN (PcmEcEchoPath57, 128)
#pragma DATA_ALIGN (PcmEcBackEp57,   128)
#pragma DATA_ALIGN (PcmEcChan58,     128)
#pragma DATA_ALIGN (PcmEcDaState58,  128)
#pragma DATA_ALIGN (PcmEcSaState58,  128)
#pragma DATA_ALIGN (PcmEcEchoPath58, 128)
#pragma DATA_ALIGN (PcmEcBackEp58,   128)
#pragma DATA_ALIGN (PcmEcChan59,     128)
#pragma DATA_ALIGN (PcmEcDaState59,  128)
#pragma DATA_ALIGN (PcmEcSaState59,  128)
#pragma DATA_ALIGN (PcmEcEchoPath59, 128)
#pragma DATA_ALIGN (PcmEcBackEp59,   128)
#pragma DATA_ALIGN (PcmEcChan60,     128)
#pragma DATA_ALIGN (PcmEcDaState60,  128)
#pragma DATA_ALIGN (PcmEcSaState60,  128)
#pragma DATA_ALIGN (PcmEcEchoPath60, 128)
#pragma DATA_ALIGN (PcmEcBackEp60,   128)
#pragma DATA_ALIGN (PcmEcChan61,     128)
#pragma DATA_ALIGN (PcmEcDaState61,  128)
#pragma DATA_ALIGN (PcmEcSaState61,  128)
#pragma DATA_ALIGN (PcmEcEchoPath61, 128)
#pragma DATA_ALIGN (PcmEcBackEp61,   128)
#pragma DATA_ALIGN (PcmEcChan62,     128)
#pragma DATA_ALIGN (PcmEcDaState62,  128)
#pragma DATA_ALIGN (PcmEcSaState62,  128)
#pragma DATA_ALIGN (PcmEcEchoPath62, 128)
#pragma DATA_ALIGN (PcmEcBackEp62,   128)
#pragma DATA_ALIGN (PcmEcChan63,     128)
#pragma DATA_ALIGN (PcmEcDaState63,  128)
#pragma DATA_ALIGN (PcmEcSaState63,  128)
#pragma DATA_ALIGN (PcmEcEchoPath63, 128)
#pragma DATA_ALIGN (PcmEcBackEp63,   128)
#pragma DATA_ALIGN (PcmEcChan64,     128)
#pragma DATA_ALIGN (PcmEcDaState64,  128)
#pragma DATA_ALIGN (PcmEcSaState64,  128)
#pragma DATA_ALIGN (PcmEcEchoPath64, 128)
#pragma DATA_ALIGN (PcmEcBackEp64,   128)
#pragma DATA_ALIGN (PcmEcChan65,     128)
#pragma DATA_ALIGN (PcmEcDaState65,  128)
#pragma DATA_ALIGN (PcmEcSaState65,  128)
#pragma DATA_ALIGN (PcmEcEchoPath65, 128)
#pragma DATA_ALIGN (PcmEcBackEp65,   128)
#pragma DATA_ALIGN (PcmEcChan66,     128)
#pragma DATA_ALIGN (PcmEcDaState66,  128)
#pragma DATA_ALIGN (PcmEcSaState66,  128)
#pragma DATA_ALIGN (PcmEcEchoPath66, 128)
#pragma DATA_ALIGN (PcmEcBackEp66,   128)
#pragma DATA_ALIGN (PcmEcChan67,     128)
#pragma DATA_ALIGN (PcmEcDaState67,  128)
#pragma DATA_ALIGN (PcmEcSaState67,  128)
#pragma DATA_ALIGN (PcmEcEchoPath67, 128)
#pragma DATA_ALIGN (PcmEcBackEp67,   128)
#pragma DATA_ALIGN (PcmEcChan68,     128)
#pragma DATA_ALIGN (PcmEcDaState68,  128)
#pragma DATA_ALIGN (PcmEcSaState68,  128)
#pragma DATA_ALIGN (PcmEcEchoPath68, 128)
#pragma DATA_ALIGN (PcmEcBackEp68,   128)
#pragma DATA_ALIGN (PcmEcChan69,     128)
#pragma DATA_ALIGN (PcmEcDaState69,  128)
#pragma DATA_ALIGN (PcmEcSaState69,  128)
#pragma DATA_ALIGN (PcmEcEchoPath69, 128)
#pragma DATA_ALIGN (PcmEcBackEp69,   128)
#pragma DATA_ALIGN (PcmEcChan70,     128)
#pragma DATA_ALIGN (PcmEcDaState70,  128)
#pragma DATA_ALIGN (PcmEcSaState70,  128)
#pragma DATA_ALIGN (PcmEcEchoPath70, 128)
#pragma DATA_ALIGN (PcmEcBackEp70,   128)
#pragma DATA_ALIGN (PcmEcChan71,     128)
#pragma DATA_ALIGN (PcmEcDaState71,  128)
#pragma DATA_ALIGN (PcmEcSaState71,  128)
#pragma DATA_ALIGN (PcmEcEchoPath71, 128)
#pragma DATA_ALIGN (PcmEcBackEp71,   128)
#pragma DATA_ALIGN (PcmEcChan72,     128)
#pragma DATA_ALIGN (PcmEcDaState72,  128)
#pragma DATA_ALIGN (PcmEcSaState72,  128)
#pragma DATA_ALIGN (PcmEcEchoPath72, 128)
#pragma DATA_ALIGN (PcmEcBackEp72,   128)
#pragma DATA_ALIGN (PcmEcChan73,     128)
#pragma DATA_ALIGN (PcmEcDaState73,  128)
#pragma DATA_ALIGN (PcmEcSaState73,  128)
#pragma DATA_ALIGN (PcmEcEchoPath73, 128)
#pragma DATA_ALIGN (PcmEcBackEp73,   128)
#pragma DATA_ALIGN (PcmEcChan74,     128)
#pragma DATA_ALIGN (PcmEcDaState74,  128)
#pragma DATA_ALIGN (PcmEcSaState74,  128)
#pragma DATA_ALIGN (PcmEcEchoPath74, 128)
#pragma DATA_ALIGN (PcmEcBackEp74,   128)
#pragma DATA_ALIGN (PcmEcChan75,     128)
#pragma DATA_ALIGN (PcmEcDaState75,  128)
#pragma DATA_ALIGN (PcmEcSaState75,  128)
#pragma DATA_ALIGN (PcmEcEchoPath75, 128)
#pragma DATA_ALIGN (PcmEcBackEp75,   128)
#pragma DATA_ALIGN (PcmEcChan76,     128)
#pragma DATA_ALIGN (PcmEcDaState76,  128)
#pragma DATA_ALIGN (PcmEcSaState76,  128)
#pragma DATA_ALIGN (PcmEcEchoPath76, 128)
#pragma DATA_ALIGN (PcmEcBackEp76,   128)
#pragma DATA_ALIGN (PcmEcChan77,     128)
#pragma DATA_ALIGN (PcmEcDaState77,  128)
#pragma DATA_ALIGN (PcmEcSaState77,  128)
#pragma DATA_ALIGN (PcmEcEchoPath77, 128)
#pragma DATA_ALIGN (PcmEcBackEp77,   128)
#pragma DATA_ALIGN (PcmEcChan78,     128)
#pragma DATA_ALIGN (PcmEcDaState78,  128)
#pragma DATA_ALIGN (PcmEcSaState78,  128)
#pragma DATA_ALIGN (PcmEcEchoPath78, 128)
#pragma DATA_ALIGN (PcmEcBackEp78,   128)
#pragma DATA_ALIGN (PcmEcChan79,     128)
#pragma DATA_ALIGN (PcmEcDaState79,  128)
#pragma DATA_ALIGN (PcmEcSaState79,  128)
#pragma DATA_ALIGN (PcmEcEchoPath79, 128)
#pragma DATA_ALIGN (PcmEcBackEp79,   128)
#pragma DATA_ALIGN (PcmEcChan80,     128)
#pragma DATA_ALIGN (PcmEcDaState80,  128)
#pragma DATA_ALIGN (PcmEcSaState80,  128)
#pragma DATA_ALIGN (PcmEcEchoPath80, 128)
#pragma DATA_ALIGN (PcmEcBackEp80,   128)
#pragma DATA_ALIGN (PcmEcChan81,     128)
#pragma DATA_ALIGN (PcmEcDaState81,  128)
#pragma DATA_ALIGN (PcmEcSaState81,  128)
#pragma DATA_ALIGN (PcmEcEchoPath81, 128)
#pragma DATA_ALIGN (PcmEcBackEp81,   128)
#pragma DATA_ALIGN (PcmEcChan82,     128)
#pragma DATA_ALIGN (PcmEcDaState82,  128)
#pragma DATA_ALIGN (PcmEcSaState82,  128)
#pragma DATA_ALIGN (PcmEcEchoPath82, 128)
#pragma DATA_ALIGN (PcmEcBackEp82,   128)
#pragma DATA_ALIGN (PcmEcChan83,     128)
#pragma DATA_ALIGN (PcmEcDaState83,  128)
#pragma DATA_ALIGN (PcmEcSaState83,  128)
#pragma DATA_ALIGN (PcmEcEchoPath83, 128)
#pragma DATA_ALIGN (PcmEcBackEp83,   128)
#pragma DATA_ALIGN (PcmEcChan84,     128)
#pragma DATA_ALIGN (PcmEcDaState84,  128)
#pragma DATA_ALIGN (PcmEcSaState84,  128)
#pragma DATA_ALIGN (PcmEcEchoPath84, 128)
#pragma DATA_ALIGN (PcmEcBackEp84,   128)
#pragma DATA_ALIGN (PcmEcChan85,     128)
#pragma DATA_ALIGN (PcmEcDaState85,  128)
#pragma DATA_ALIGN (PcmEcSaState85,  128)
#pragma DATA_ALIGN (PcmEcEchoPath85, 128)
#pragma DATA_ALIGN (PcmEcBackEp85,   128)
#pragma DATA_ALIGN (PcmEcChan86,     128)
#pragma DATA_ALIGN (PcmEcDaState86,  128)
#pragma DATA_ALIGN (PcmEcSaState86,  128)
#pragma DATA_ALIGN (PcmEcEchoPath86, 128)
#pragma DATA_ALIGN (PcmEcBackEp86,   128)
#pragma DATA_ALIGN (PcmEcChan87,     128)
#pragma DATA_ALIGN (PcmEcDaState87,  128)
#pragma DATA_ALIGN (PcmEcSaState87,  128)
#pragma DATA_ALIGN (PcmEcEchoPath87, 128)
#pragma DATA_ALIGN (PcmEcBackEp87,   128)
#pragma DATA_ALIGN (PcmEcChan88,     128)
#pragma DATA_ALIGN (PcmEcDaState88,  128)
#pragma DATA_ALIGN (PcmEcSaState88,  128)
#pragma DATA_ALIGN (PcmEcEchoPath88, 128)
#pragma DATA_ALIGN (PcmEcBackEp88,   128)
#pragma DATA_ALIGN (PcmEcChan89,     128)
#pragma DATA_ALIGN (PcmEcDaState89,  128)
#pragma DATA_ALIGN (PcmEcSaState89,  128)
#pragma DATA_ALIGN (PcmEcEchoPath89, 128)
#pragma DATA_ALIGN (PcmEcBackEp89,   128)
#pragma DATA_ALIGN (PcmEcChan90,     128)
#pragma DATA_ALIGN (PcmEcDaState90,  128)
#pragma DATA_ALIGN (PcmEcSaState90,  128)
#pragma DATA_ALIGN (PcmEcEchoPath90, 128)
#pragma DATA_ALIGN (PcmEcBackEp90,   128)
#pragma DATA_ALIGN (PcmEcChan91,     128)
#pragma DATA_ALIGN (PcmEcDaState91,  128)
#pragma DATA_ALIGN (PcmEcSaState91,  128)
#pragma DATA_ALIGN (PcmEcEchoPath91, 128)
#pragma DATA_ALIGN (PcmEcBackEp91,   128)
#pragma DATA_ALIGN (PcmEcChan92,     128)
#pragma DATA_ALIGN (PcmEcDaState92,  128)
#pragma DATA_ALIGN (PcmEcSaState92,  128)
#pragma DATA_ALIGN (PcmEcEchoPath92, 128)
#pragma DATA_ALIGN (PcmEcBackEp92,   128)
#pragma DATA_ALIGN (PcmEcChan93,     128)
#pragma DATA_ALIGN (PcmEcDaState93,  128)
#pragma DATA_ALIGN (PcmEcSaState93,  128)
#pragma DATA_ALIGN (PcmEcEchoPath93, 128)
#pragma DATA_ALIGN (PcmEcBackEp93,   128)
#pragma DATA_ALIGN (PcmEcChan94,     128)
#pragma DATA_ALIGN (PcmEcDaState94,  128)
#pragma DATA_ALIGN (PcmEcSaState94,  128)
#pragma DATA_ALIGN (PcmEcEchoPath94, 128)
#pragma DATA_ALIGN (PcmEcBackEp94,   128)
#pragma DATA_ALIGN (PcmEcChan95,     128)
#pragma DATA_ALIGN (PcmEcDaState95,  128)
#pragma DATA_ALIGN (PcmEcSaState95,  128)
#pragma DATA_ALIGN (PcmEcEchoPath95, 128)
#pragma DATA_ALIGN (PcmEcBackEp95,   128)
#pragma DATA_ALIGN (PcmEcChan96,     128)
#pragma DATA_ALIGN (PcmEcDaState96,  128)
#pragma DATA_ALIGN (PcmEcSaState96,  128)
#pragma DATA_ALIGN (PcmEcEchoPath96, 128)
#pragma DATA_ALIGN (PcmEcBackEp96,   128)
#pragma DATA_ALIGN (PcmEcChan97,     128)
#pragma DATA_ALIGN (PcmEcDaState97,  128)
#pragma DATA_ALIGN (PcmEcSaState97,  128)
#pragma DATA_ALIGN (PcmEcEchoPath97, 128)
#pragma DATA_ALIGN (PcmEcBackEp97,   128)
#pragma DATA_ALIGN (PcmEcChan98,     128)
#pragma DATA_ALIGN (PcmEcDaState98,  128)
#pragma DATA_ALIGN (PcmEcSaState98,  128)
#pragma DATA_ALIGN (PcmEcEchoPath98, 128)
#pragma DATA_ALIGN (PcmEcBackEp98,   128)
#pragma DATA_ALIGN (PcmEcChan99,     128)
#pragma DATA_ALIGN (PcmEcDaState99,  128)
#pragma DATA_ALIGN (PcmEcSaState99,  128)
#pragma DATA_ALIGN (PcmEcEchoPath99, 128)
#pragma DATA_ALIGN (PcmEcBackEp99,   128)
#pragma DATA_ALIGN (PcmEcChan100,     128)
#pragma DATA_ALIGN (PcmEcDaState100,  128)
#pragma DATA_ALIGN (PcmEcSaState100,  128)
#pragma DATA_ALIGN (PcmEcEchoPath100, 128)
#pragma DATA_ALIGN (PcmEcBackEp100,   128)
#pragma DATA_ALIGN (PcmEcChan101,     128)
#pragma DATA_ALIGN (PcmEcDaState101,  128)
#pragma DATA_ALIGN (PcmEcSaState101,  128)
#pragma DATA_ALIGN (PcmEcEchoPath101, 128)
#pragma DATA_ALIGN (PcmEcBackEp101,   128)
#pragma DATA_ALIGN (PcmEcChan102,     128)
#pragma DATA_ALIGN (PcmEcDaState102,  128)
#pragma DATA_ALIGN (PcmEcSaState102,  128)
#pragma DATA_ALIGN (PcmEcEchoPath102, 128)
#pragma DATA_ALIGN (PcmEcBackEp102,   128)
#pragma DATA_ALIGN (PcmEcChan103,     128)
#pragma DATA_ALIGN (PcmEcDaState103,  128)
#pragma DATA_ALIGN (PcmEcSaState103,  128)
#pragma DATA_ALIGN (PcmEcEchoPath103, 128)
#pragma DATA_ALIGN (PcmEcBackEp103,   128)
#pragma DATA_ALIGN (PcmEcChan104,     128)
#pragma DATA_ALIGN (PcmEcDaState104,  128)
#pragma DATA_ALIGN (PcmEcSaState104,  128)
#pragma DATA_ALIGN (PcmEcEchoPath104, 128)
#pragma DATA_ALIGN (PcmEcBackEp104,   128)
#pragma DATA_ALIGN (PcmEcChan105,     128)
#pragma DATA_ALIGN (PcmEcDaState105,  128)
#pragma DATA_ALIGN (PcmEcSaState105,  128)
#pragma DATA_ALIGN (PcmEcEchoPath105, 128)
#pragma DATA_ALIGN (PcmEcBackEp105,   128)
#pragma DATA_ALIGN (PcmEcChan106,     128)
#pragma DATA_ALIGN (PcmEcDaState106,  128)
#pragma DATA_ALIGN (PcmEcSaState106,  128)
#pragma DATA_ALIGN (PcmEcEchoPath106, 128)
#pragma DATA_ALIGN (PcmEcBackEp106,   128)
#pragma DATA_ALIGN (PcmEcChan107,     128)
#pragma DATA_ALIGN (PcmEcDaState107,  128)
#pragma DATA_ALIGN (PcmEcSaState107,  128)
#pragma DATA_ALIGN (PcmEcEchoPath107, 128)
#pragma DATA_ALIGN (PcmEcBackEp107,   128)
#pragma DATA_ALIGN (PcmEcChan108,     128)
#pragma DATA_ALIGN (PcmEcDaState108,  128)
#pragma DATA_ALIGN (PcmEcSaState108,  128)
#pragma DATA_ALIGN (PcmEcEchoPath108, 128)
#pragma DATA_ALIGN (PcmEcBackEp108,   128)
#pragma DATA_ALIGN (PcmEcChan109,     128)
#pragma DATA_ALIGN (PcmEcDaState109,  128)
#pragma DATA_ALIGN (PcmEcSaState109,  128)
#pragma DATA_ALIGN (PcmEcEchoPath109, 128)
#pragma DATA_ALIGN (PcmEcBackEp109,   128)
#pragma DATA_ALIGN (PcmEcChan110,     128)
#pragma DATA_ALIGN (PcmEcDaState110,  128)
#pragma DATA_ALIGN (PcmEcSaState110,  128)
#pragma DATA_ALIGN (PcmEcEchoPath110, 128)
#pragma DATA_ALIGN (PcmEcBackEp110,   128)
#pragma DATA_ALIGN (PcmEcChan111,     128)
#pragma DATA_ALIGN (PcmEcDaState111,  128)
#pragma DATA_ALIGN (PcmEcSaState111,  128)
#pragma DATA_ALIGN (PcmEcEchoPath111, 128)
#pragma DATA_ALIGN (PcmEcBackEp111,   128)
#pragma DATA_ALIGN (PcmEcChan112,     128)
#pragma DATA_ALIGN (PcmEcDaState112,  128)
#pragma DATA_ALIGN (PcmEcSaState112,  128)
#pragma DATA_ALIGN (PcmEcEchoPath112, 128)
#pragma DATA_ALIGN (PcmEcBackEp112,   128)
#pragma DATA_ALIGN (PcmEcChan113,     128)
#pragma DATA_ALIGN (PcmEcDaState113,  128)
#pragma DATA_ALIGN (PcmEcSaState113,  128)
#pragma DATA_ALIGN (PcmEcEchoPath113, 128)
#pragma DATA_ALIGN (PcmEcBackEp113,   128)
#pragma DATA_ALIGN (PcmEcChan114,     128)
#pragma DATA_ALIGN (PcmEcDaState114,  128)
#pragma DATA_ALIGN (PcmEcSaState114,  128)
#pragma DATA_ALIGN (PcmEcEchoPath114, 128)
#pragma DATA_ALIGN (PcmEcBackEp114,   128)
#pragma DATA_ALIGN (PcmEcChan115,     128)
#pragma DATA_ALIGN (PcmEcDaState115,  128)
#pragma DATA_ALIGN (PcmEcSaState115,  128)
#pragma DATA_ALIGN (PcmEcEchoPath115, 128)
#pragma DATA_ALIGN (PcmEcBackEp115,   128)
#pragma DATA_ALIGN (PcmEcChan116,     128)
#pragma DATA_ALIGN (PcmEcDaState116,  128)
#pragma DATA_ALIGN (PcmEcSaState116,  128)
#pragma DATA_ALIGN (PcmEcEchoPath116, 128)
#pragma DATA_ALIGN (PcmEcBackEp116,   128)
#pragma DATA_ALIGN (PcmEcChan117,     128)
#pragma DATA_ALIGN (PcmEcDaState117,  128)
#pragma DATA_ALIGN (PcmEcSaState117,  128)
#pragma DATA_ALIGN (PcmEcEchoPath117, 128)
#pragma DATA_ALIGN (PcmEcBackEp117,   128)
#pragma DATA_ALIGN (PcmEcChan118,     128)
#pragma DATA_ALIGN (PcmEcDaState118,  128)
#pragma DATA_ALIGN (PcmEcSaState118,  128)
#pragma DATA_ALIGN (PcmEcEchoPath118, 128)
#pragma DATA_ALIGN (PcmEcBackEp118,   128)
#pragma DATA_ALIGN (PcmEcChan119,     128)
#pragma DATA_ALIGN (PcmEcDaState119,  128)
#pragma DATA_ALIGN (PcmEcSaState119,  128)
#pragma DATA_ALIGN (PcmEcEchoPath119, 128)
#pragma DATA_ALIGN (PcmEcBackEp119,   128)
#pragma DATA_ALIGN (PcmEcChan120,     128)
#pragma DATA_ALIGN (PcmEcDaState120,  128)
#pragma DATA_ALIGN (PcmEcSaState120,  128)
#pragma DATA_ALIGN (PcmEcEchoPath120, 128)
#pragma DATA_ALIGN (PcmEcBackEp120,   128)
#pragma DATA_ALIGN (PcmEcChan121,     128)
#pragma DATA_ALIGN (PcmEcDaState121,  128)
#pragma DATA_ALIGN (PcmEcSaState121,  128)
#pragma DATA_ALIGN (PcmEcEchoPath121, 128)
#pragma DATA_ALIGN (PcmEcBackEp121,   128)
#pragma DATA_ALIGN (PcmEcChan122,     128)
#pragma DATA_ALIGN (PcmEcDaState122,  128)
#pragma DATA_ALIGN (PcmEcSaState122,  128)
#pragma DATA_ALIGN (PcmEcEchoPath122, 128)
#pragma DATA_ALIGN (PcmEcBackEp122,   128)
#pragma DATA_ALIGN (PcmEcChan123,     128)
#pragma DATA_ALIGN (PcmEcDaState123,  128)
#pragma DATA_ALIGN (PcmEcSaState123,  128)
#pragma DATA_ALIGN (PcmEcEchoPath123, 128)
#pragma DATA_ALIGN (PcmEcBackEp123,   128)
#pragma DATA_ALIGN (PcmEcChan124,     128)
#pragma DATA_ALIGN (PcmEcDaState124,  128)
#pragma DATA_ALIGN (PcmEcSaState124,  128)
#pragma DATA_ALIGN (PcmEcEchoPath124, 128)
#pragma DATA_ALIGN (PcmEcBackEp124,   128)
#pragma DATA_ALIGN (PcmEcChan125,     128)
#pragma DATA_ALIGN (PcmEcDaState125,  128)
#pragma DATA_ALIGN (PcmEcSaState125,  128)
#pragma DATA_ALIGN (PcmEcEchoPath125, 128)
#pragma DATA_ALIGN (PcmEcBackEp125,   128)
#pragma DATA_ALIGN (PcmEcChan126,     128)
#pragma DATA_ALIGN (PcmEcDaState126,  128)
#pragma DATA_ALIGN (PcmEcSaState126,  128)
#pragma DATA_ALIGN (PcmEcEchoPath126, 128)
#pragma DATA_ALIGN (PcmEcBackEp126,   128)
#pragma DATA_ALIGN (PcmEcChan127,     128)
#pragma DATA_ALIGN (PcmEcDaState127,  128)
#pragma DATA_ALIGN (PcmEcSaState127,  128)
#pragma DATA_ALIGN (PcmEcEchoPath127, 128)
#pragma DATA_ALIGN (PcmEcBackEp127,   128)
#pragma DATA_ALIGN (PcmEcChan128,     128)
#pragma DATA_ALIGN (PcmEcDaState128,  128)
#pragma DATA_ALIGN (PcmEcSaState128,  128)
#pragma DATA_ALIGN (PcmEcEchoPath128, 128)
#pragma DATA_ALIGN (PcmEcBackEp128,   128)
#pragma DATA_ALIGN (PcmEcChan129,     128)
#pragma DATA_ALIGN (PcmEcDaState129,  128)
#pragma DATA_ALIGN (PcmEcSaState129,  128)
#pragma DATA_ALIGN (PcmEcEchoPath129, 128)
#pragma DATA_ALIGN (PcmEcBackEp129,   128)
#pragma DATA_ALIGN (PcmEcChan130,     128)
#pragma DATA_ALIGN (PcmEcDaState130,  128)
#pragma DATA_ALIGN (PcmEcSaState130,  128)
#pragma DATA_ALIGN (PcmEcEchoPath130, 128)
#pragma DATA_ALIGN (PcmEcBackEp130,   128)
#pragma DATA_ALIGN (PcmEcChan131,     128)
#pragma DATA_ALIGN (PcmEcDaState131,  128)
#pragma DATA_ALIGN (PcmEcSaState131,  128)
#pragma DATA_ALIGN (PcmEcEchoPath131, 128)
#pragma DATA_ALIGN (PcmEcBackEp131,   128)
#pragma DATA_ALIGN (PcmEcChan132,     128)
#pragma DATA_ALIGN (PcmEcDaState132,  128)
#pragma DATA_ALIGN (PcmEcSaState132,  128)
#pragma DATA_ALIGN (PcmEcEchoPath132, 128)
#pragma DATA_ALIGN (PcmEcBackEp132,   128)
#pragma DATA_ALIGN (PcmEcChan133,     128)
#pragma DATA_ALIGN (PcmEcDaState133,  128)
#pragma DATA_ALIGN (PcmEcSaState133,  128)
#pragma DATA_ALIGN (PcmEcEchoPath133, 128)
#pragma DATA_ALIGN (PcmEcBackEp133,   128)
#pragma DATA_ALIGN (PcmEcChan134,     128)
#pragma DATA_ALIGN (PcmEcDaState134,  128)
#pragma DATA_ALIGN (PcmEcSaState134,  128)
#pragma DATA_ALIGN (PcmEcEchoPath134, 128)
#pragma DATA_ALIGN (PcmEcBackEp134,   128)
#pragma DATA_ALIGN (PcmEcChan135,     128)
#pragma DATA_ALIGN (PcmEcDaState135,  128)
#pragma DATA_ALIGN (PcmEcSaState135,  128)
#pragma DATA_ALIGN (PcmEcEchoPath135, 128)
#pragma DATA_ALIGN (PcmEcBackEp135,   128)
#pragma DATA_ALIGN (PcmEcChan136,     128)
#pragma DATA_ALIGN (PcmEcDaState136,  128)
#pragma DATA_ALIGN (PcmEcSaState136,  128)
#pragma DATA_ALIGN (PcmEcEchoPath136, 128)
#pragma DATA_ALIGN (PcmEcBackEp136,   128)
#pragma DATA_ALIGN (PcmEcChan137,     128)
#pragma DATA_ALIGN (PcmEcDaState137,  128)
#pragma DATA_ALIGN (PcmEcSaState137,  128)
#pragma DATA_ALIGN (PcmEcEchoPath137, 128)
#pragma DATA_ALIGN (PcmEcBackEp137,   128)
#pragma DATA_ALIGN (PcmEcChan138,     128)
#pragma DATA_ALIGN (PcmEcDaState138,  128)
#pragma DATA_ALIGN (PcmEcSaState138,  128)
#pragma DATA_ALIGN (PcmEcEchoPath138, 128)
#pragma DATA_ALIGN (PcmEcBackEp138,   128)
#pragma DATA_ALIGN (PcmEcChan139,     128)
#pragma DATA_ALIGN (PcmEcDaState139,  128)
#pragma DATA_ALIGN (PcmEcSaState139,  128)
#pragma DATA_ALIGN (PcmEcEchoPath139, 128)
#pragma DATA_ALIGN (PcmEcBackEp139,   128)
#pragma DATA_ALIGN (PcmEcChan140,     128)
#pragma DATA_ALIGN (PcmEcDaState140,  128)
#pragma DATA_ALIGN (PcmEcSaState140,  128)
#pragma DATA_ALIGN (PcmEcEchoPath140, 128)
#pragma DATA_ALIGN (PcmEcBackEp140,   128)
#pragma DATA_ALIGN (PcmEcChan141,     128)
#pragma DATA_ALIGN (PcmEcDaState141,  128)
#pragma DATA_ALIGN (PcmEcSaState141,  128)
#pragma DATA_ALIGN (PcmEcEchoPath141, 128)
#pragma DATA_ALIGN (PcmEcBackEp141,   128)
#pragma DATA_ALIGN (PcmEcChan142,     128)
#pragma DATA_ALIGN (PcmEcDaState142,  128)
#pragma DATA_ALIGN (PcmEcSaState142,  128)
#pragma DATA_ALIGN (PcmEcEchoPath142, 128)
#pragma DATA_ALIGN (PcmEcBackEp142,   128)
#pragma DATA_ALIGN (PcmEcChan143,     128)
#pragma DATA_ALIGN (PcmEcDaState143,  128)
#pragma DATA_ALIGN (PcmEcSaState143,  128)
#pragma DATA_ALIGN (PcmEcEchoPath143, 128)
#pragma DATA_ALIGN (PcmEcBackEp143,   128)
#pragma DATA_ALIGN (PcmEcChan144,     128)
#pragma DATA_ALIGN (PcmEcDaState144,  128)
#pragma DATA_ALIGN (PcmEcSaState144,  128)
#pragma DATA_ALIGN (PcmEcEchoPath144, 128)
#pragma DATA_ALIGN (PcmEcBackEp144,   128)
#pragma DATA_ALIGN (PcmEcChan145,     128)
#pragma DATA_ALIGN (PcmEcDaState145,  128)
#pragma DATA_ALIGN (PcmEcSaState145,  128)
#pragma DATA_ALIGN (PcmEcEchoPath145, 128)
#pragma DATA_ALIGN (PcmEcBackEp145,   128)
#pragma DATA_ALIGN (PcmEcChan146,     128)
#pragma DATA_ALIGN (PcmEcDaState146,  128)
#pragma DATA_ALIGN (PcmEcSaState146,  128)
#pragma DATA_ALIGN (PcmEcEchoPath146, 128)
#pragma DATA_ALIGN (PcmEcBackEp146,   128)
#pragma DATA_ALIGN (PcmEcChan147,     128)
#pragma DATA_ALIGN (PcmEcDaState147,  128)
#pragma DATA_ALIGN (PcmEcSaState147,  128)
#pragma DATA_ALIGN (PcmEcEchoPath147, 128)
#pragma DATA_ALIGN (PcmEcBackEp147,   128)
#pragma DATA_ALIGN (PcmEcChan148,     128)
#pragma DATA_ALIGN (PcmEcDaState148,  128)
#pragma DATA_ALIGN (PcmEcSaState148,  128)
#pragma DATA_ALIGN (PcmEcEchoPath148, 128)
#pragma DATA_ALIGN (PcmEcBackEp148,   128)
#pragma DATA_ALIGN (PcmEcChan149,     128)
#pragma DATA_ALIGN (PcmEcDaState149,  128)
#pragma DATA_ALIGN (PcmEcSaState149,  128)
#pragma DATA_ALIGN (PcmEcEchoPath149, 128)
#pragma DATA_ALIGN (PcmEcBackEp149,   128)
#pragma DATA_ALIGN (PcmEcChan150,     128)
#pragma DATA_ALIGN (PcmEcDaState150,  128)
#pragma DATA_ALIGN (PcmEcSaState150,  128)
#pragma DATA_ALIGN (PcmEcEchoPath150, 128)
#pragma DATA_ALIGN (PcmEcBackEp150,   128)
#pragma DATA_ALIGN (PcmEcChan151,     128)
#pragma DATA_ALIGN (PcmEcDaState151,  128)
#pragma DATA_ALIGN (PcmEcSaState151,  128)
#pragma DATA_ALIGN (PcmEcEchoPath151, 128)
#pragma DATA_ALIGN (PcmEcBackEp151,   128)
#pragma DATA_ALIGN (PcmEcChan152,     128)
#pragma DATA_ALIGN (PcmEcDaState152,  128)
#pragma DATA_ALIGN (PcmEcSaState152,  128)
#pragma DATA_ALIGN (PcmEcEchoPath152, 128)
#pragma DATA_ALIGN (PcmEcBackEp152,   128)
#pragma DATA_ALIGN (PcmEcChan153,     128)
#pragma DATA_ALIGN (PcmEcDaState153,  128)
#pragma DATA_ALIGN (PcmEcSaState153,  128)
#pragma DATA_ALIGN (PcmEcEchoPath153, 128)
#pragma DATA_ALIGN (PcmEcBackEp153,   128)
#pragma DATA_ALIGN (PcmEcChan154,     128)
#pragma DATA_ALIGN (PcmEcDaState154,  128)
#pragma DATA_ALIGN (PcmEcSaState154,  128)
#pragma DATA_ALIGN (PcmEcEchoPath154, 128)
#pragma DATA_ALIGN (PcmEcBackEp154,   128)
#pragma DATA_ALIGN (PcmEcChan155,     128)
#pragma DATA_ALIGN (PcmEcDaState155,  128)
#pragma DATA_ALIGN (PcmEcSaState155,  128)
#pragma DATA_ALIGN (PcmEcEchoPath155, 128)
#pragma DATA_ALIGN (PcmEcBackEp155,   128)
#pragma DATA_ALIGN (PcmEcChan156,     128)
#pragma DATA_ALIGN (PcmEcDaState156,  128)
#pragma DATA_ALIGN (PcmEcSaState156,  128)
#pragma DATA_ALIGN (PcmEcEchoPath156, 128)
#pragma DATA_ALIGN (PcmEcBackEp156,   128)
#pragma DATA_ALIGN (PcmEcChan157,     128)
#pragma DATA_ALIGN (PcmEcDaState157,  128)
#pragma DATA_ALIGN (PcmEcSaState157,  128)
#pragma DATA_ALIGN (PcmEcEchoPath157, 128)
#pragma DATA_ALIGN (PcmEcBackEp157,   128)
#pragma DATA_ALIGN (PcmEcChan158,     128)
#pragma DATA_ALIGN (PcmEcDaState158,  128)
#pragma DATA_ALIGN (PcmEcSaState158,  128)
#pragma DATA_ALIGN (PcmEcEchoPath158, 128)
#pragma DATA_ALIGN (PcmEcBackEp158,   128)
#pragma DATA_ALIGN (PcmEcChan159,     128)
#pragma DATA_ALIGN (PcmEcDaState159,  128)
#pragma DATA_ALIGN (PcmEcSaState159,  128)
#pragma DATA_ALIGN (PcmEcEchoPath159, 128)
#pragma DATA_ALIGN (PcmEcBackEp159,   128)
//}
//{
ADT_UInt16 PcmEcInUse[160];
#pragma DATA_SECTION (PcmEcInUse, "POOL_ALLOC")
ADT_UInt16 * const pPcmEcChan[] = {
	PcmEcChan0,
	PcmEcChan1,
	PcmEcChan2,
	PcmEcChan3,
	PcmEcChan4,
	PcmEcChan5,
	PcmEcChan6,
	PcmEcChan7,
	PcmEcChan8,
	PcmEcChan9,
	PcmEcChan10,
	PcmEcChan11,
	PcmEcChan12,
	PcmEcChan13,
	PcmEcChan14,
	PcmEcChan15,
	PcmEcChan16,
	PcmEcChan17,
	PcmEcChan18,
	PcmEcChan19,
	PcmEcChan20,
	PcmEcChan21,
	PcmEcChan22,
	PcmEcChan23,
	PcmEcChan24,
	PcmEcChan25,
	PcmEcChan26,
	PcmEcChan27,
	PcmEcChan28,
	PcmEcChan29,
	PcmEcChan30,
	PcmEcChan31,
	PcmEcChan32,
	PcmEcChan33,
	PcmEcChan34,
	PcmEcChan35,
	PcmEcChan36,
	PcmEcChan37,
	PcmEcChan38,
	PcmEcChan39,
	PcmEcChan40,
	PcmEcChan41,
	PcmEcChan42,
	PcmEcChan43,
	PcmEcChan44,
	PcmEcChan45,
	PcmEcChan46,
	PcmEcChan47,
	PcmEcChan48,
	PcmEcChan49,
	PcmEcChan50,
	PcmEcChan51,
	PcmEcChan52,
	PcmEcChan53,
	PcmEcChan54,
	PcmEcChan55,
	PcmEcChan56,
	PcmEcChan57,
	PcmEcChan58,
	PcmEcChan59,
	PcmEcChan60,
	PcmEcChan61,
	PcmEcChan62,
	PcmEcChan63,
	PcmEcChan64,
	PcmEcChan65,
	PcmEcChan66,
	PcmEcChan67,
	PcmEcChan68,
	PcmEcChan69,
	PcmEcChan70,
	PcmEcChan71,
	PcmEcChan72,
	PcmEcChan73,
	PcmEcChan74,
	PcmEcChan75,
	PcmEcChan76,
	PcmEcChan77,
	PcmEcChan78,
	PcmEcChan79,
	PcmEcChan80,
	PcmEcChan81,
	PcmEcChan82,
	PcmEcChan83,
	PcmEcChan84,
	PcmEcChan85,
	PcmEcChan86,
	PcmEcChan87,
	PcmEcChan88,
	PcmEcChan89,
	PcmEcChan90,
	PcmEcChan91,
	PcmEcChan92,
	PcmEcChan93,
	PcmEcChan94,
	PcmEcChan95,
	PcmEcChan96,
	PcmEcChan97,
	PcmEcChan98,
	PcmEcChan99,
	PcmEcChan100,
	PcmEcChan101,
	PcmEcChan102,
	PcmEcChan103,
	PcmEcChan104,
	PcmEcChan105,
	PcmEcChan106,
	PcmEcChan107,
	PcmEcChan108,
	PcmEcChan109,
	PcmEcChan110,
	PcmEcChan111,
	PcmEcChan112,
	PcmEcChan113,
	PcmEcChan114,
	PcmEcChan115,
	PcmEcChan116,
	PcmEcChan117,
	PcmEcChan118,
	PcmEcChan119,
	PcmEcChan120,
	PcmEcChan121,
	PcmEcChan122,
	PcmEcChan123,
	PcmEcChan124,
	PcmEcChan125,
	PcmEcChan126,
	PcmEcChan127,
	PcmEcChan128,
	PcmEcChan129,
	PcmEcChan130,
	PcmEcChan131,
	PcmEcChan132,
	PcmEcChan133,
	PcmEcChan134,
	PcmEcChan135,
	PcmEcChan136,
	PcmEcChan137,
	PcmEcChan138,
	PcmEcChan139,
	PcmEcChan140,
	PcmEcChan141,
	PcmEcChan142,
	PcmEcChan143,
	PcmEcChan144,
	PcmEcChan145,
	PcmEcChan146,
	PcmEcChan147,
	PcmEcChan148,
	PcmEcChan149,
	PcmEcChan150,
	PcmEcChan151,
	PcmEcChan152,
	PcmEcChan153,
	PcmEcChan154,
	PcmEcChan155,
	PcmEcChan156,
	PcmEcChan157,
	PcmEcChan158,
	PcmEcChan159
};

ADT_UInt16 * const pPcmEcDaState[] = {
	PcmEcDaState0,
	PcmEcDaState1,
	PcmEcDaState2,
	PcmEcDaState3,
	PcmEcDaState4,
	PcmEcDaState5,
	PcmEcDaState6,
	PcmEcDaState7,
	PcmEcDaState8,
	PcmEcDaState9,
	PcmEcDaState10,
	PcmEcDaState11,
	PcmEcDaState12,
	PcmEcDaState13,
	PcmEcDaState14,
	PcmEcDaState15,
	PcmEcDaState16,
	PcmEcDaState17,
	PcmEcDaState18,
	PcmEcDaState19,
	PcmEcDaState20,
	PcmEcDaState21,
	PcmEcDaState22,
	PcmEcDaState23,
	PcmEcDaState24,
	PcmEcDaState25,
	PcmEcDaState26,
	PcmEcDaState27,
	PcmEcDaState28,
	PcmEcDaState29,
	PcmEcDaState30,
	PcmEcDaState31,
	PcmEcDaState32,
	PcmEcDaState33,
	PcmEcDaState34,
	PcmEcDaState35,
	PcmEcDaState36,
	PcmEcDaState37,
	PcmEcDaState38,
	PcmEcDaState39,
	PcmEcDaState40,
	PcmEcDaState41,
	PcmEcDaState42,
	PcmEcDaState43,
	PcmEcDaState44,
	PcmEcDaState45,
	PcmEcDaState46,
	PcmEcDaState47,
	PcmEcDaState48,
	PcmEcDaState49,
	PcmEcDaState50,
	PcmEcDaState51,
	PcmEcDaState52,
	PcmEcDaState53,
	PcmEcDaState54,
	PcmEcDaState55,
	PcmEcDaState56,
	PcmEcDaState57,
	PcmEcDaState58,
	PcmEcDaState59,
	PcmEcDaState60,
	PcmEcDaState61,
	PcmEcDaState62,
	PcmEcDaState63,
	PcmEcDaState64,
	PcmEcDaState65,
	PcmEcDaState66,
	PcmEcDaState67,
	PcmEcDaState68,
	PcmEcDaState69,
	PcmEcDaState70,
	PcmEcDaState71,
	PcmEcDaState72,
	PcmEcDaState73,
	PcmEcDaState74,
	PcmEcDaState75,
	PcmEcDaState76,
	PcmEcDaState77,
	PcmEcDaState78,
	PcmEcDaState79,
	PcmEcDaState80,
	PcmEcDaState81,
	PcmEcDaState82,
	PcmEcDaState83,
	PcmEcDaState84,
	PcmEcDaState85,
	PcmEcDaState86,
	PcmEcDaState87,
	PcmEcDaState88,
	PcmEcDaState89,
	PcmEcDaState90,
	PcmEcDaState91,
	PcmEcDaState92,
	PcmEcDaState93,
	PcmEcDaState94,
	PcmEcDaState95,
	PcmEcDaState96,
	PcmEcDaState97,
	PcmEcDaState98,
	PcmEcDaState99,
	PcmEcDaState100,
	PcmEcDaState101,
	PcmEcDaState102,
	PcmEcDaState103,
	PcmEcDaState104,
	PcmEcDaState105,
	PcmEcDaState106,
	PcmEcDaState107,
	PcmEcDaState108,
	PcmEcDaState109,
	PcmEcDaState110,
	PcmEcDaState111,
	PcmEcDaState112,
	PcmEcDaState113,
	PcmEcDaState114,
	PcmEcDaState115,
	PcmEcDaState116,
	PcmEcDaState117,
	PcmEcDaState118,
	PcmEcDaState119,
	PcmEcDaState120,
	PcmEcDaState121,
	PcmEcDaState122,
	PcmEcDaState123,
	PcmEcDaState124,
	PcmEcDaState125,
	PcmEcDaState126,
	PcmEcDaState127,
	PcmEcDaState128,
	PcmEcDaState129,
	PcmEcDaState130,
	PcmEcDaState131,
	PcmEcDaState132,
	PcmEcDaState133,
	PcmEcDaState134,
	PcmEcDaState135,
	PcmEcDaState136,
	PcmEcDaState137,
	PcmEcDaState138,
	PcmEcDaState139,
	PcmEcDaState140,
	PcmEcDaState141,
	PcmEcDaState142,
	PcmEcDaState143,
	PcmEcDaState144,
	PcmEcDaState145,
	PcmEcDaState146,
	PcmEcDaState147,
	PcmEcDaState148,
	PcmEcDaState149,
	PcmEcDaState150,
	PcmEcDaState151,
	PcmEcDaState152,
	PcmEcDaState153,
	PcmEcDaState154,
	PcmEcDaState155,
	PcmEcDaState156,
	PcmEcDaState157,
	PcmEcDaState158,
	PcmEcDaState159
};

ADT_UInt16 * const pPcmEcSaState[] = {
	PcmEcSaState0,
	PcmEcSaState1,
	PcmEcSaState2,
	PcmEcSaState3,
	PcmEcSaState4,
	PcmEcSaState5,
	PcmEcSaState6,
	PcmEcSaState7,
	PcmEcSaState8,
	PcmEcSaState9,
	PcmEcSaState10,
	PcmEcSaState11,
	PcmEcSaState12,
	PcmEcSaState13,
	PcmEcSaState14,
	PcmEcSaState15,
	PcmEcSaState16,
	PcmEcSaState17,
	PcmEcSaState18,
	PcmEcSaState19,
	PcmEcSaState20,
	PcmEcSaState21,
	PcmEcSaState22,
	PcmEcSaState23,
	PcmEcSaState24,
	PcmEcSaState25,
	PcmEcSaState26,
	PcmEcSaState27,
	PcmEcSaState28,
	PcmEcSaState29,
	PcmEcSaState30,
	PcmEcSaState31,
	PcmEcSaState32,
	PcmEcSaState33,
	PcmEcSaState34,
	PcmEcSaState35,
	PcmEcSaState36,
	PcmEcSaState37,
	PcmEcSaState38,
	PcmEcSaState39,
	PcmEcSaState40,
	PcmEcSaState41,
	PcmEcSaState42,
	PcmEcSaState43,
	PcmEcSaState44,
	PcmEcSaState45,
	PcmEcSaState46,
	PcmEcSaState47,
	PcmEcSaState48,
	PcmEcSaState49,
	PcmEcSaState50,
	PcmEcSaState51,
	PcmEcSaState52,
	PcmEcSaState53,
	PcmEcSaState54,
	PcmEcSaState55,
	PcmEcSaState56,
	PcmEcSaState57,
	PcmEcSaState58,
	PcmEcSaState59,
	PcmEcSaState60,
	PcmEcSaState61,
	PcmEcSaState62,
	PcmEcSaState63,
	PcmEcSaState64,
	PcmEcSaState65,
	PcmEcSaState66,
	PcmEcSaState67,
	PcmEcSaState68,
	PcmEcSaState69,
	PcmEcSaState70,
	PcmEcSaState71,
	PcmEcSaState72,
	PcmEcSaState73,
	PcmEcSaState74,
	PcmEcSaState75,
	PcmEcSaState76,
	PcmEcSaState77,
	PcmEcSaState78,
	PcmEcSaState79,
	PcmEcSaState80,
	PcmEcSaState81,
	PcmEcSaState82,
	PcmEcSaState83,
	PcmEcSaState84,
	PcmEcSaState85,
	PcmEcSaState86,
	PcmEcSaState87,
	PcmEcSaState88,
	PcmEcSaState89,
	PcmEcSaState90,
	PcmEcSaState91,
	PcmEcSaState92,
	PcmEcSaState93,
	PcmEcSaState94,
	PcmEcSaState95,
	PcmEcSaState96,
	PcmEcSaState97,
	PcmEcSaState98,
	PcmEcSaState99,
	PcmEcSaState100,
	PcmEcSaState101,
	PcmEcSaState102,
	PcmEcSaState103,
	PcmEcSaState104,
	PcmEcSaState105,
	PcmEcSaState106,
	PcmEcSaState107,
	PcmEcSaState108,
	PcmEcSaState109,
	PcmEcSaState110,
	PcmEcSaState111,
	PcmEcSaState112,
	PcmEcSaState113,
	PcmEcSaState114,
	PcmEcSaState115,
	PcmEcSaState116,
	PcmEcSaState117,
	PcmEcSaState118,
	PcmEcSaState119,
	PcmEcSaState120,
	PcmEcSaState121,
	PcmEcSaState122,
	PcmEcSaState123,
	PcmEcSaState124,
	PcmEcSaState125,
	PcmEcSaState126,
	PcmEcSaState127,
	PcmEcSaState128,
	PcmEcSaState129,
	PcmEcSaState130,
	PcmEcSaState131,
	PcmEcSaState132,
	PcmEcSaState133,
	PcmEcSaState134,
	PcmEcSaState135,
	PcmEcSaState136,
	PcmEcSaState137,
	PcmEcSaState138,
	PcmEcSaState139,
	PcmEcSaState140,
	PcmEcSaState141,
	PcmEcSaState142,
	PcmEcSaState143,
	PcmEcSaState144,
	PcmEcSaState145,
	PcmEcSaState146,
	PcmEcSaState147,
	PcmEcSaState148,
	PcmEcSaState149,
	PcmEcSaState150,
	PcmEcSaState151,
	PcmEcSaState152,
	PcmEcSaState153,
	PcmEcSaState154,
	PcmEcSaState155,
	PcmEcSaState156,
	PcmEcSaState157,
	PcmEcSaState158,
	PcmEcSaState159
};

ADT_UInt16 * const pPcmEcEchoPath[] = {
	PcmEcEchoPath0,
	PcmEcEchoPath1,
	PcmEcEchoPath2,
	PcmEcEchoPath3,
	PcmEcEchoPath4,
	PcmEcEchoPath5,
	PcmEcEchoPath6,
	PcmEcEchoPath7,
	PcmEcEchoPath8,
	PcmEcEchoPath9,
	PcmEcEchoPath10,
	PcmEcEchoPath11,
	PcmEcEchoPath12,
	PcmEcEchoPath13,
	PcmEcEchoPath14,
	PcmEcEchoPath15,
	PcmEcEchoPath16,
	PcmEcEchoPath17,
	PcmEcEchoPath18,
	PcmEcEchoPath19,
	PcmEcEchoPath20,
	PcmEcEchoPath21,
	PcmEcEchoPath22,
	PcmEcEchoPath23,
	PcmEcEchoPath24,
	PcmEcEchoPath25,
	PcmEcEchoPath26,
	PcmEcEchoPath27,
	PcmEcEchoPath28,
	PcmEcEchoPath29,
	PcmEcEchoPath30,
	PcmEcEchoPath31,
	PcmEcEchoPath32,
	PcmEcEchoPath33,
	PcmEcEchoPath34,
	PcmEcEchoPath35,
	PcmEcEchoPath36,
	PcmEcEchoPath37,
	PcmEcEchoPath38,
	PcmEcEchoPath39,
	PcmEcEchoPath40,
	PcmEcEchoPath41,
	PcmEcEchoPath42,
	PcmEcEchoPath43,
	PcmEcEchoPath44,
	PcmEcEchoPath45,
	PcmEcEchoPath46,
	PcmEcEchoPath47,
	PcmEcEchoPath48,
	PcmEcEchoPath49,
	PcmEcEchoPath50,
	PcmEcEchoPath51,
	PcmEcEchoPath52,
	PcmEcEchoPath53,
	PcmEcEchoPath54,
	PcmEcEchoPath55,
	PcmEcEchoPath56,
	PcmEcEchoPath57,
	PcmEcEchoPath58,
	PcmEcEchoPath59,
	PcmEcEchoPath60,
	PcmEcEchoPath61,
	PcmEcEchoPath62,
	PcmEcEchoPath63,
	PcmEcEchoPath64,
	PcmEcEchoPath65,
	PcmEcEchoPath66,
	PcmEcEchoPath67,
	PcmEcEchoPath68,
	PcmEcEchoPath69,
	PcmEcEchoPath70,
	PcmEcEchoPath71,
	PcmEcEchoPath72,
	PcmEcEchoPath73,
	PcmEcEchoPath74,
	PcmEcEchoPath75,
	PcmEcEchoPath76,
	PcmEcEchoPath77,
	PcmEcEchoPath78,
	PcmEcEchoPath79,
	PcmEcEchoPath80,
	PcmEcEchoPath81,
	PcmEcEchoPath82,
	PcmEcEchoPath83,
	PcmEcEchoPath84,
	PcmEcEchoPath85,
	PcmEcEchoPath86,
	PcmEcEchoPath87,
	PcmEcEchoPath88,
	PcmEcEchoPath89,
	PcmEcEchoPath90,
	PcmEcEchoPath91,
	PcmEcEchoPath92,
	PcmEcEchoPath93,
	PcmEcEchoPath94,
	PcmEcEchoPath95,
	PcmEcEchoPath96,
	PcmEcEchoPath97,
	PcmEcEchoPath98,
	PcmEcEchoPath99,
	PcmEcEchoPath100,
	PcmEcEchoPath101,
	PcmEcEchoPath102,
	PcmEcEchoPath103,
	PcmEcEchoPath104,
	PcmEcEchoPath105,
	PcmEcEchoPath106,
	PcmEcEchoPath107,
	PcmEcEchoPath108,
	PcmEcEchoPath109,
	PcmEcEchoPath110,
	PcmEcEchoPath111,
	PcmEcEchoPath112,
	PcmEcEchoPath113,
	PcmEcEchoPath114,
	PcmEcEchoPath115,
	PcmEcEchoPath116,
	PcmEcEchoPath117,
	PcmEcEchoPath118,
	PcmEcEchoPath119,
	PcmEcEchoPath120,
	PcmEcEchoPath121,
	PcmEcEchoPath122,
	PcmEcEchoPath123,
	PcmEcEchoPath124,
	PcmEcEchoPath125,
	PcmEcEchoPath126,
	PcmEcEchoPath127,
	PcmEcEchoPath128,
	PcmEcEchoPath129,
	PcmEcEchoPath130,
	PcmEcEchoPath131,
	PcmEcEchoPath132,
	PcmEcEchoPath133,
	PcmEcEchoPath134,
	PcmEcEchoPath135,
	PcmEcEchoPath136,
	PcmEcEchoPath137,
	PcmEcEchoPath138,
	PcmEcEchoPath139,
	PcmEcEchoPath140,
	PcmEcEchoPath141,
	PcmEcEchoPath142,
	PcmEcEchoPath143,
	PcmEcEchoPath144,
	PcmEcEchoPath145,
	PcmEcEchoPath146,
	PcmEcEchoPath147,
	PcmEcEchoPath148,
	PcmEcEchoPath149,
	PcmEcEchoPath150,
	PcmEcEchoPath151,
	PcmEcEchoPath152,
	PcmEcEchoPath153,
	PcmEcEchoPath154,
	PcmEcEchoPath155,
	PcmEcEchoPath156,
	PcmEcEchoPath157,
	PcmEcEchoPath158,
	PcmEcEchoPath159
};

ADT_UInt16 * const pPcmEcBackEp[] = {
	PcmEcBackEp0,
	PcmEcBackEp1,
	PcmEcBackEp2,
	PcmEcBackEp3,
	PcmEcBackEp4,
	PcmEcBackEp5,
	PcmEcBackEp6,
	PcmEcBackEp7,
	PcmEcBackEp8,
	PcmEcBackEp9,
	PcmEcBackEp10,
	PcmEcBackEp11,
	PcmEcBackEp12,
	PcmEcBackEp13,
	PcmEcBackEp14,
	PcmEcBackEp15,
	PcmEcBackEp16,
	PcmEcBackEp17,
	PcmEcBackEp18,
	PcmEcBackEp19,
	PcmEcBackEp20,
	PcmEcBackEp21,
	PcmEcBackEp22,
	PcmEcBackEp23,
	PcmEcBackEp24,
	PcmEcBackEp25,
	PcmEcBackEp26,
	PcmEcBackEp27,
	PcmEcBackEp28,
	PcmEcBackEp29,
	PcmEcBackEp30,
	PcmEcBackEp31,
	PcmEcBackEp32,
	PcmEcBackEp33,
	PcmEcBackEp34,
	PcmEcBackEp35,
	PcmEcBackEp36,
	PcmEcBackEp37,
	PcmEcBackEp38,
	PcmEcBackEp39,
	PcmEcBackEp40,
	PcmEcBackEp41,
	PcmEcBackEp42,
	PcmEcBackEp43,
	PcmEcBackEp44,
	PcmEcBackEp45,
	PcmEcBackEp46,
	PcmEcBackEp47,
	PcmEcBackEp48,
	PcmEcBackEp49,
	PcmEcBackEp50,
	PcmEcBackEp51,
	PcmEcBackEp52,
	PcmEcBackEp53,
	PcmEcBackEp54,
	PcmEcBackEp55,
	PcmEcBackEp56,
	PcmEcBackEp57,
	PcmEcBackEp58,
	PcmEcBackEp59,
	PcmEcBackEp60,
	PcmEcBackEp61,
	PcmEcBackEp62,
	PcmEcBackEp63,
	PcmEcBackEp64,
	PcmEcBackEp65,
	PcmEcBackEp66,
	PcmEcBackEp67,
	PcmEcBackEp68,
	PcmEcBackEp69,
	PcmEcBackEp70,
	PcmEcBackEp71,
	PcmEcBackEp72,
	PcmEcBackEp73,
	PcmEcBackEp74,
	PcmEcBackEp75,
	PcmEcBackEp76,
	PcmEcBackEp77,
	PcmEcBackEp78,
	PcmEcBackEp79,
	PcmEcBackEp80,
	PcmEcBackEp81,
	PcmEcBackEp82,
	PcmEcBackEp83,
	PcmEcBackEp84,
	PcmEcBackEp85,
	PcmEcBackEp86,
	PcmEcBackEp87,
	PcmEcBackEp88,
	PcmEcBackEp89,
	PcmEcBackEp90,
	PcmEcBackEp91,
	PcmEcBackEp92,
	PcmEcBackEp93,
	PcmEcBackEp94,
	PcmEcBackEp95,
	PcmEcBackEp96,
	PcmEcBackEp97,
	PcmEcBackEp98,
	PcmEcBackEp99,
	PcmEcBackEp100,
	PcmEcBackEp101,
	PcmEcBackEp102,
	PcmEcBackEp103,
	PcmEcBackEp104,
	PcmEcBackEp105,
	PcmEcBackEp106,
	PcmEcBackEp107,
	PcmEcBackEp108,
	PcmEcBackEp109,
	PcmEcBackEp110,
	PcmEcBackEp111,
	PcmEcBackEp112,
	PcmEcBackEp113,
	PcmEcBackEp114,
	PcmEcBackEp115,
	PcmEcBackEp116,
	PcmEcBackEp117,
	PcmEcBackEp118,
	PcmEcBackEp119,
	PcmEcBackEp120,
	PcmEcBackEp121,
	PcmEcBackEp122,
	PcmEcBackEp123,
	PcmEcBackEp124,
	PcmEcBackEp125,
	PcmEcBackEp126,
	PcmEcBackEp127,
	PcmEcBackEp128,
	PcmEcBackEp129,
	PcmEcBackEp130,
	PcmEcBackEp131,
	PcmEcBackEp132,
	PcmEcBackEp133,
	PcmEcBackEp134,
	PcmEcBackEp135,
	PcmEcBackEp136,
	PcmEcBackEp137,
	PcmEcBackEp138,
	PcmEcBackEp139,
	PcmEcBackEp140,
	PcmEcBackEp141,
	PcmEcBackEp142,
	PcmEcBackEp143,
	PcmEcBackEp144,
	PcmEcBackEp145,
	PcmEcBackEp146,
	PcmEcBackEp147,
	PcmEcBackEp148,
	PcmEcBackEp149,
	PcmEcBackEp150,
	PcmEcBackEp151,
	PcmEcBackEp152,
	PcmEcBackEp153,
	PcmEcBackEp154,
	PcmEcBackEp155,
	PcmEcBackEp156,
	PcmEcBackEp157,
	PcmEcBackEp158,
	PcmEcBackEp159
};


ecInstanceInfo_t const pcmEcInfo = {
    (void *)pPcmEcChan,  (void *) pPcmEcSaState, (void *)pPcmEcDaState, (void *) pPcmEcEchoPath, (void *) pPcmEcBackEp,
    PcmECChanI16,        PcmECSAStateI16,        PcmECDAStateI16,       PcmEchoPathI16,          PcmBgEchoPathI16
};

//}
//{ ------ Pkt Echo canceller buffer allocation
#define PktECChanI16   0
#define PktECDAStateI16   0
#define PktECSAStateI16   0
#define PktEchoPathI16   0
#define PktBgEchoPathI16  0

ADT_UInt16 PktEcInUse=0;
ADT_UInt16 * const pPktEcChan[] = {0};
ADT_UInt16 * const pPktEcDaState[] = {0};
ADT_UInt16 * const pPktEcSaState[] = {0};
ADT_UInt16 * const pPktEcEchoPath[] = {0};
ADT_UInt16 * const pPktEcBackEp[] = {0};
ADT_UInt16 * const pPktEcBackFar[] = {0};
ADT_UInt16 * const pPktEcBackNear[] = {0};

ADT_UInt16 const pktEcInfo;

//}



/*=================================*/
//{ Acoustic canceller declarations

ADT_Int16 AECInUse;
ADT_Int16 AECHandles;

ADT_Int16 AECConfig;
ADT_Int16 AECMemTab;
ADT_Int16 AECChan;
ADT_Int16 pAECFarPool;
const int AEC_Inst_I8 = 0;
#pragma DATA_SECTION (AECInUse,    "STUB_DATA_SECT")
#pragma DATA_SECTION (AECHandles,  "STUB_DATA_SECT")
#pragma DATA_SECTION (AECConfig,   "STUB_DATA_SECT")
#pragma DATA_SECTION (AECMemTab,   "STUB_DATA_SECT")
#pragma DATA_SECTION (AECChan,     "STUB_DATA_SECT")
#pragma DATA_SECTION (pAECFarPool, "STUB_DATA_SECT")

ADT_Int16 micEqChan;
ADT_Int16 micEqState;
#pragma DATA_SECTION (micEqChan,  "STUB_DATA_SECT")
#pragma DATA_SECTION (micEqState, "STUB_DATA_SECT")

ADT_Int16 spkrEqChan;
ADT_Int16 spkrEqState;
#pragma DATA_SECTION (spkrEqChan,  "STUB_DATA_SECT")
#pragma DATA_SECTION (spkrEqState, "STUB_DATA_SECT")
//}


/*=============================== */
// Tone detect, detect relay, generate and generate relay data structures.

//{
ADT_UInt16    ToneDetectorsAvail = MAX_NUM_TONEDET_CHANNELS;
ADT_UInt16    ToneDetectorInUse[MAX_NUM_TONEDET_CHANNELS];
#pragma DATA_SECTION (ToneDetectorsAvail, "POOL_ALLOC")
#pragma DATA_SECTION (ToneDetectorInUse,  "POOL_ALLOC")
//{           TDInstance_t      ToneDetInstance STRUCTURES to CHAN_INST_DATA:ToneDetect

TDInstance_t ToneDetInstance0;
#pragma DATA_SECTION (ToneDetInstance0, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance0, 128)

static TDInstance_t ToneDetInstance1;
#pragma DATA_SECTION (ToneDetInstance1, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance1, 128)

static TDInstance_t ToneDetInstance2;
#pragma DATA_SECTION (ToneDetInstance2, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance2, 128)

static TDInstance_t ToneDetInstance3;
#pragma DATA_SECTION (ToneDetInstance3, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance3, 128)

static TDInstance_t ToneDetInstance4;
#pragma DATA_SECTION (ToneDetInstance4, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance4, 128)

static TDInstance_t ToneDetInstance5;
#pragma DATA_SECTION (ToneDetInstance5, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance5, 128)

static TDInstance_t ToneDetInstance6;
#pragma DATA_SECTION (ToneDetInstance6, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance6, 128)

static TDInstance_t ToneDetInstance7;
#pragma DATA_SECTION (ToneDetInstance7, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance7, 128)

static TDInstance_t ToneDetInstance8;
#pragma DATA_SECTION (ToneDetInstance8, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance8, 128)

static TDInstance_t ToneDetInstance9;
#pragma DATA_SECTION (ToneDetInstance9, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance9, 128)

static TDInstance_t ToneDetInstance10;
#pragma DATA_SECTION (ToneDetInstance10, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance10, 128)

static TDInstance_t ToneDetInstance11;
#pragma DATA_SECTION (ToneDetInstance11, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance11, 128)

static TDInstance_t ToneDetInstance12;
#pragma DATA_SECTION (ToneDetInstance12, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance12, 128)

static TDInstance_t ToneDetInstance13;
#pragma DATA_SECTION (ToneDetInstance13, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance13, 128)

static TDInstance_t ToneDetInstance14;
#pragma DATA_SECTION (ToneDetInstance14, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance14, 128)

static TDInstance_t ToneDetInstance15;
#pragma DATA_SECTION (ToneDetInstance15, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance15, 128)

static TDInstance_t ToneDetInstance16;
#pragma DATA_SECTION (ToneDetInstance16, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance16, 128)

static TDInstance_t ToneDetInstance17;
#pragma DATA_SECTION (ToneDetInstance17, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance17, 128)

static TDInstance_t ToneDetInstance18;
#pragma DATA_SECTION (ToneDetInstance18, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance18, 128)

static TDInstance_t ToneDetInstance19;
#pragma DATA_SECTION (ToneDetInstance19, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance19, 128)

static TDInstance_t ToneDetInstance20;
#pragma DATA_SECTION (ToneDetInstance20, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance20, 128)

static TDInstance_t ToneDetInstance21;
#pragma DATA_SECTION (ToneDetInstance21, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance21, 128)

static TDInstance_t ToneDetInstance22;
#pragma DATA_SECTION (ToneDetInstance22, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance22, 128)

static TDInstance_t ToneDetInstance23;
#pragma DATA_SECTION (ToneDetInstance23, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance23, 128)

static TDInstance_t ToneDetInstance24;
#pragma DATA_SECTION (ToneDetInstance24, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance24, 128)

static TDInstance_t ToneDetInstance25;
#pragma DATA_SECTION (ToneDetInstance25, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance25, 128)

static TDInstance_t ToneDetInstance26;
#pragma DATA_SECTION (ToneDetInstance26, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance26, 128)

static TDInstance_t ToneDetInstance27;
#pragma DATA_SECTION (ToneDetInstance27, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance27, 128)

static TDInstance_t ToneDetInstance28;
#pragma DATA_SECTION (ToneDetInstance28, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance28, 128)

static TDInstance_t ToneDetInstance29;
#pragma DATA_SECTION (ToneDetInstance29, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance29, 128)

static TDInstance_t ToneDetInstance30;
#pragma DATA_SECTION (ToneDetInstance30, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance30, 128)

static TDInstance_t ToneDetInstance31;
#pragma DATA_SECTION (ToneDetInstance31, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance31, 128)

static TDInstance_t ToneDetInstance32;
#pragma DATA_SECTION (ToneDetInstance32, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance32, 128)

static TDInstance_t ToneDetInstance33;
#pragma DATA_SECTION (ToneDetInstance33, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance33, 128)

static TDInstance_t ToneDetInstance34;
#pragma DATA_SECTION (ToneDetInstance34, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance34, 128)

static TDInstance_t ToneDetInstance35;
#pragma DATA_SECTION (ToneDetInstance35, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance35, 128)

static TDInstance_t ToneDetInstance36;
#pragma DATA_SECTION (ToneDetInstance36, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance36, 128)

static TDInstance_t ToneDetInstance37;
#pragma DATA_SECTION (ToneDetInstance37, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance37, 128)

static TDInstance_t ToneDetInstance38;
#pragma DATA_SECTION (ToneDetInstance38, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance38, 128)

static TDInstance_t ToneDetInstance39;
#pragma DATA_SECTION (ToneDetInstance39, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance39, 128)

static TDInstance_t ToneDetInstance40;
#pragma DATA_SECTION (ToneDetInstance40, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance40, 128)

static TDInstance_t ToneDetInstance41;
#pragma DATA_SECTION (ToneDetInstance41, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance41, 128)

static TDInstance_t ToneDetInstance42;
#pragma DATA_SECTION (ToneDetInstance42, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance42, 128)

static TDInstance_t ToneDetInstance43;
#pragma DATA_SECTION (ToneDetInstance43, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance43, 128)

static TDInstance_t ToneDetInstance44;
#pragma DATA_SECTION (ToneDetInstance44, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance44, 128)

static TDInstance_t ToneDetInstance45;
#pragma DATA_SECTION (ToneDetInstance45, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance45, 128)

static TDInstance_t ToneDetInstance46;
#pragma DATA_SECTION (ToneDetInstance46, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance46, 128)

static TDInstance_t ToneDetInstance47;
#pragma DATA_SECTION (ToneDetInstance47, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance47, 128)

static TDInstance_t ToneDetInstance48;
#pragma DATA_SECTION (ToneDetInstance48, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance48, 128)

static TDInstance_t ToneDetInstance49;
#pragma DATA_SECTION (ToneDetInstance49, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance49, 128)

static TDInstance_t ToneDetInstance50;
#pragma DATA_SECTION (ToneDetInstance50, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance50, 128)

static TDInstance_t ToneDetInstance51;
#pragma DATA_SECTION (ToneDetInstance51, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance51, 128)

static TDInstance_t ToneDetInstance52;
#pragma DATA_SECTION (ToneDetInstance52, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance52, 128)

static TDInstance_t ToneDetInstance53;
#pragma DATA_SECTION (ToneDetInstance53, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance53, 128)

static TDInstance_t ToneDetInstance54;
#pragma DATA_SECTION (ToneDetInstance54, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance54, 128)

static TDInstance_t ToneDetInstance55;
#pragma DATA_SECTION (ToneDetInstance55, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance55, 128)

static TDInstance_t ToneDetInstance56;
#pragma DATA_SECTION (ToneDetInstance56, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance56, 128)

static TDInstance_t ToneDetInstance57;
#pragma DATA_SECTION (ToneDetInstance57, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance57, 128)

static TDInstance_t ToneDetInstance58;
#pragma DATA_SECTION (ToneDetInstance58, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance58, 128)

static TDInstance_t ToneDetInstance59;
#pragma DATA_SECTION (ToneDetInstance59, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance59, 128)

static TDInstance_t ToneDetInstance60;
#pragma DATA_SECTION (ToneDetInstance60, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance60, 128)

static TDInstance_t ToneDetInstance61;
#pragma DATA_SECTION (ToneDetInstance61, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance61, 128)

static TDInstance_t ToneDetInstance62;
#pragma DATA_SECTION (ToneDetInstance62, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance62, 128)

static TDInstance_t ToneDetInstance63;
#pragma DATA_SECTION (ToneDetInstance63, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance63, 128)

static TDInstance_t ToneDetInstance64;
#pragma DATA_SECTION (ToneDetInstance64, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance64, 128)

static TDInstance_t ToneDetInstance65;
#pragma DATA_SECTION (ToneDetInstance65, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance65, 128)

static TDInstance_t ToneDetInstance66;
#pragma DATA_SECTION (ToneDetInstance66, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance66, 128)

static TDInstance_t ToneDetInstance67;
#pragma DATA_SECTION (ToneDetInstance67, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance67, 128)

static TDInstance_t ToneDetInstance68;
#pragma DATA_SECTION (ToneDetInstance68, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance68, 128)

static TDInstance_t ToneDetInstance69;
#pragma DATA_SECTION (ToneDetInstance69, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance69, 128)

static TDInstance_t ToneDetInstance70;
#pragma DATA_SECTION (ToneDetInstance70, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance70, 128)

static TDInstance_t ToneDetInstance71;
#pragma DATA_SECTION (ToneDetInstance71, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance71, 128)

static TDInstance_t ToneDetInstance72;
#pragma DATA_SECTION (ToneDetInstance72, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance72, 128)

static TDInstance_t ToneDetInstance73;
#pragma DATA_SECTION (ToneDetInstance73, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance73, 128)

static TDInstance_t ToneDetInstance74;
#pragma DATA_SECTION (ToneDetInstance74, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance74, 128)

static TDInstance_t ToneDetInstance75;
#pragma DATA_SECTION (ToneDetInstance75, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance75, 128)

static TDInstance_t ToneDetInstance76;
#pragma DATA_SECTION (ToneDetInstance76, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance76, 128)

static TDInstance_t ToneDetInstance77;
#pragma DATA_SECTION (ToneDetInstance77, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance77, 128)

static TDInstance_t ToneDetInstance78;
#pragma DATA_SECTION (ToneDetInstance78, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance78, 128)

static TDInstance_t ToneDetInstance79;
#pragma DATA_SECTION (ToneDetInstance79, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance79, 128)

static TDInstance_t ToneDetInstance80;
#pragma DATA_SECTION (ToneDetInstance80, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance80, 128)

static TDInstance_t ToneDetInstance81;
#pragma DATA_SECTION (ToneDetInstance81, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance81, 128)

static TDInstance_t ToneDetInstance82;
#pragma DATA_SECTION (ToneDetInstance82, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance82, 128)

static TDInstance_t ToneDetInstance83;
#pragma DATA_SECTION (ToneDetInstance83, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance83, 128)

static TDInstance_t ToneDetInstance84;
#pragma DATA_SECTION (ToneDetInstance84, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance84, 128)

static TDInstance_t ToneDetInstance85;
#pragma DATA_SECTION (ToneDetInstance85, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance85, 128)

static TDInstance_t ToneDetInstance86;
#pragma DATA_SECTION (ToneDetInstance86, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance86, 128)

static TDInstance_t ToneDetInstance87;
#pragma DATA_SECTION (ToneDetInstance87, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance87, 128)

static TDInstance_t ToneDetInstance88;
#pragma DATA_SECTION (ToneDetInstance88, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance88, 128)

static TDInstance_t ToneDetInstance89;
#pragma DATA_SECTION (ToneDetInstance89, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance89, 128)

static TDInstance_t ToneDetInstance90;
#pragma DATA_SECTION (ToneDetInstance90, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance90, 128)

static TDInstance_t ToneDetInstance91;
#pragma DATA_SECTION (ToneDetInstance91, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance91, 128)

static TDInstance_t ToneDetInstance92;
#pragma DATA_SECTION (ToneDetInstance92, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance92, 128)

static TDInstance_t ToneDetInstance93;
#pragma DATA_SECTION (ToneDetInstance93, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance93, 128)

static TDInstance_t ToneDetInstance94;
#pragma DATA_SECTION (ToneDetInstance94, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance94, 128)

static TDInstance_t ToneDetInstance95;
#pragma DATA_SECTION (ToneDetInstance95, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance95, 128)

static TDInstance_t ToneDetInstance96;
#pragma DATA_SECTION (ToneDetInstance96, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance96, 128)

static TDInstance_t ToneDetInstance97;
#pragma DATA_SECTION (ToneDetInstance97, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance97, 128)

static TDInstance_t ToneDetInstance98;
#pragma DATA_SECTION (ToneDetInstance98, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance98, 128)

static TDInstance_t ToneDetInstance99;
#pragma DATA_SECTION (ToneDetInstance99, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance99, 128)

static TDInstance_t ToneDetInstance100;
#pragma DATA_SECTION (ToneDetInstance100, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance100, 128)

static TDInstance_t ToneDetInstance101;
#pragma DATA_SECTION (ToneDetInstance101, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance101, 128)

static TDInstance_t ToneDetInstance102;
#pragma DATA_SECTION (ToneDetInstance102, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance102, 128)

static TDInstance_t ToneDetInstance103;
#pragma DATA_SECTION (ToneDetInstance103, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance103, 128)

static TDInstance_t ToneDetInstance104;
#pragma DATA_SECTION (ToneDetInstance104, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance104, 128)

static TDInstance_t ToneDetInstance105;
#pragma DATA_SECTION (ToneDetInstance105, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance105, 128)

static TDInstance_t ToneDetInstance106;
#pragma DATA_SECTION (ToneDetInstance106, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance106, 128)

static TDInstance_t ToneDetInstance107;
#pragma DATA_SECTION (ToneDetInstance107, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance107, 128)

static TDInstance_t ToneDetInstance108;
#pragma DATA_SECTION (ToneDetInstance108, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance108, 128)

static TDInstance_t ToneDetInstance109;
#pragma DATA_SECTION (ToneDetInstance109, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance109, 128)

static TDInstance_t ToneDetInstance110;
#pragma DATA_SECTION (ToneDetInstance110, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance110, 128)

static TDInstance_t ToneDetInstance111;
#pragma DATA_SECTION (ToneDetInstance111, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance111, 128)

static TDInstance_t ToneDetInstance112;
#pragma DATA_SECTION (ToneDetInstance112, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance112, 128)

static TDInstance_t ToneDetInstance113;
#pragma DATA_SECTION (ToneDetInstance113, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance113, 128)

static TDInstance_t ToneDetInstance114;
#pragma DATA_SECTION (ToneDetInstance114, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance114, 128)

static TDInstance_t ToneDetInstance115;
#pragma DATA_SECTION (ToneDetInstance115, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance115, 128)

static TDInstance_t ToneDetInstance116;
#pragma DATA_SECTION (ToneDetInstance116, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance116, 128)

static TDInstance_t ToneDetInstance117;
#pragma DATA_SECTION (ToneDetInstance117, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance117, 128)

static TDInstance_t ToneDetInstance118;
#pragma DATA_SECTION (ToneDetInstance118, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance118, 128)

static TDInstance_t ToneDetInstance119;
#pragma DATA_SECTION (ToneDetInstance119, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance119, 128)

static TDInstance_t ToneDetInstance120;
#pragma DATA_SECTION (ToneDetInstance120, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance120, 128)

static TDInstance_t ToneDetInstance121;
#pragma DATA_SECTION (ToneDetInstance121, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance121, 128)

static TDInstance_t ToneDetInstance122;
#pragma DATA_SECTION (ToneDetInstance122, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance122, 128)

static TDInstance_t ToneDetInstance123;
#pragma DATA_SECTION (ToneDetInstance123, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance123, 128)

static TDInstance_t ToneDetInstance124;
#pragma DATA_SECTION (ToneDetInstance124, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance124, 128)

static TDInstance_t ToneDetInstance125;
#pragma DATA_SECTION (ToneDetInstance125, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance125, 128)

static TDInstance_t ToneDetInstance126;
#pragma DATA_SECTION (ToneDetInstance126, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance126, 128)

static TDInstance_t ToneDetInstance127;
#pragma DATA_SECTION (ToneDetInstance127, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance127, 128)

static TDInstance_t ToneDetInstance128;
#pragma DATA_SECTION (ToneDetInstance128, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance128, 128)

static TDInstance_t ToneDetInstance129;
#pragma DATA_SECTION (ToneDetInstance129, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance129, 128)

static TDInstance_t ToneDetInstance130;
#pragma DATA_SECTION (ToneDetInstance130, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance130, 128)

static TDInstance_t ToneDetInstance131;
#pragma DATA_SECTION (ToneDetInstance131, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance131, 128)

static TDInstance_t ToneDetInstance132;
#pragma DATA_SECTION (ToneDetInstance132, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance132, 128)

static TDInstance_t ToneDetInstance133;
#pragma DATA_SECTION (ToneDetInstance133, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance133, 128)

static TDInstance_t ToneDetInstance134;
#pragma DATA_SECTION (ToneDetInstance134, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance134, 128)

static TDInstance_t ToneDetInstance135;
#pragma DATA_SECTION (ToneDetInstance135, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance135, 128)

static TDInstance_t ToneDetInstance136;
#pragma DATA_SECTION (ToneDetInstance136, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance136, 128)

static TDInstance_t ToneDetInstance137;
#pragma DATA_SECTION (ToneDetInstance137, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance137, 128)

static TDInstance_t ToneDetInstance138;
#pragma DATA_SECTION (ToneDetInstance138, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance138, 128)

static TDInstance_t ToneDetInstance139;
#pragma DATA_SECTION (ToneDetInstance139, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance139, 128)

static TDInstance_t ToneDetInstance140;
#pragma DATA_SECTION (ToneDetInstance140, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance140, 128)

static TDInstance_t ToneDetInstance141;
#pragma DATA_SECTION (ToneDetInstance141, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance141, 128)

static TDInstance_t ToneDetInstance142;
#pragma DATA_SECTION (ToneDetInstance142, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance142, 128)

static TDInstance_t ToneDetInstance143;
#pragma DATA_SECTION (ToneDetInstance143, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance143, 128)

static TDInstance_t ToneDetInstance144;
#pragma DATA_SECTION (ToneDetInstance144, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance144, 128)

static TDInstance_t ToneDetInstance145;
#pragma DATA_SECTION (ToneDetInstance145, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance145, 128)

static TDInstance_t ToneDetInstance146;
#pragma DATA_SECTION (ToneDetInstance146, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance146, 128)

static TDInstance_t ToneDetInstance147;
#pragma DATA_SECTION (ToneDetInstance147, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance147, 128)

static TDInstance_t ToneDetInstance148;
#pragma DATA_SECTION (ToneDetInstance148, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance148, 128)

static TDInstance_t ToneDetInstance149;
#pragma DATA_SECTION (ToneDetInstance149, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance149, 128)

static TDInstance_t ToneDetInstance150;
#pragma DATA_SECTION (ToneDetInstance150, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance150, 128)

static TDInstance_t ToneDetInstance151;
#pragma DATA_SECTION (ToneDetInstance151, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance151, 128)

static TDInstance_t ToneDetInstance152;
#pragma DATA_SECTION (ToneDetInstance152, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance152, 128)

static TDInstance_t ToneDetInstance153;
#pragma DATA_SECTION (ToneDetInstance153, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance153, 128)

static TDInstance_t ToneDetInstance154;
#pragma DATA_SECTION (ToneDetInstance154, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance154, 128)

static TDInstance_t ToneDetInstance155;
#pragma DATA_SECTION (ToneDetInstance155, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance155, 128)

static TDInstance_t ToneDetInstance156;
#pragma DATA_SECTION (ToneDetInstance156, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance156, 128)

static TDInstance_t ToneDetInstance157;
#pragma DATA_SECTION (ToneDetInstance157, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance157, 128)

static TDInstance_t ToneDetInstance158;
#pragma DATA_SECTION (ToneDetInstance158, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance158, 128)

static TDInstance_t ToneDetInstance159;
#pragma DATA_SECTION (ToneDetInstance159, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneDetInstance159, 128)

TDInstance_t* const ToneDetInstance[] = {
	&ToneDetInstance0,
	&ToneDetInstance1,
	&ToneDetInstance2,
	&ToneDetInstance3,
	&ToneDetInstance4,
	&ToneDetInstance5,
	&ToneDetInstance6,
	&ToneDetInstance7,
	&ToneDetInstance8,
	&ToneDetInstance9,
	&ToneDetInstance10,
	&ToneDetInstance11,
	&ToneDetInstance12,
	&ToneDetInstance13,
	&ToneDetInstance14,
	&ToneDetInstance15,
	&ToneDetInstance16,
	&ToneDetInstance17,
	&ToneDetInstance18,
	&ToneDetInstance19,
	&ToneDetInstance20,
	&ToneDetInstance21,
	&ToneDetInstance22,
	&ToneDetInstance23,
	&ToneDetInstance24,
	&ToneDetInstance25,
	&ToneDetInstance26,
	&ToneDetInstance27,
	&ToneDetInstance28,
	&ToneDetInstance29,
	&ToneDetInstance30,
	&ToneDetInstance31,
	&ToneDetInstance32,
	&ToneDetInstance33,
	&ToneDetInstance34,
	&ToneDetInstance35,
	&ToneDetInstance36,
	&ToneDetInstance37,
	&ToneDetInstance38,
	&ToneDetInstance39,
	&ToneDetInstance40,
	&ToneDetInstance41,
	&ToneDetInstance42,
	&ToneDetInstance43,
	&ToneDetInstance44,
	&ToneDetInstance45,
	&ToneDetInstance46,
	&ToneDetInstance47,
	&ToneDetInstance48,
	&ToneDetInstance49,
	&ToneDetInstance50,
	&ToneDetInstance51,
	&ToneDetInstance52,
	&ToneDetInstance53,
	&ToneDetInstance54,
	&ToneDetInstance55,
	&ToneDetInstance56,
	&ToneDetInstance57,
	&ToneDetInstance58,
	&ToneDetInstance59,
	&ToneDetInstance60,
	&ToneDetInstance61,
	&ToneDetInstance62,
	&ToneDetInstance63,
	&ToneDetInstance64,
	&ToneDetInstance65,
	&ToneDetInstance66,
	&ToneDetInstance67,
	&ToneDetInstance68,
	&ToneDetInstance69,
	&ToneDetInstance70,
	&ToneDetInstance71,
	&ToneDetInstance72,
	&ToneDetInstance73,
	&ToneDetInstance74,
	&ToneDetInstance75,
	&ToneDetInstance76,
	&ToneDetInstance77,
	&ToneDetInstance78,
	&ToneDetInstance79,
	&ToneDetInstance80,
	&ToneDetInstance81,
	&ToneDetInstance82,
	&ToneDetInstance83,
	&ToneDetInstance84,
	&ToneDetInstance85,
	&ToneDetInstance86,
	&ToneDetInstance87,
	&ToneDetInstance88,
	&ToneDetInstance89,
	&ToneDetInstance90,
	&ToneDetInstance91,
	&ToneDetInstance92,
	&ToneDetInstance93,
	&ToneDetInstance94,
	&ToneDetInstance95,
	&ToneDetInstance96,
	&ToneDetInstance97,
	&ToneDetInstance98,
	&ToneDetInstance99,
	&ToneDetInstance100,
	&ToneDetInstance101,
	&ToneDetInstance102,
	&ToneDetInstance103,
	&ToneDetInstance104,
	&ToneDetInstance105,
	&ToneDetInstance106,
	&ToneDetInstance107,
	&ToneDetInstance108,
	&ToneDetInstance109,
	&ToneDetInstance110,
	&ToneDetInstance111,
	&ToneDetInstance112,
	&ToneDetInstance113,
	&ToneDetInstance114,
	&ToneDetInstance115,
	&ToneDetInstance116,
	&ToneDetInstance117,
	&ToneDetInstance118,
	&ToneDetInstance119,
	&ToneDetInstance120,
	&ToneDetInstance121,
	&ToneDetInstance122,
	&ToneDetInstance123,
	&ToneDetInstance124,
	&ToneDetInstance125,
	&ToneDetInstance126,
	&ToneDetInstance127,
	&ToneDetInstance128,
	&ToneDetInstance129,
	&ToneDetInstance130,
	&ToneDetInstance131,
	&ToneDetInstance132,
	&ToneDetInstance133,
	&ToneDetInstance134,
	&ToneDetInstance135,
	&ToneDetInstance136,
	&ToneDetInstance137,
	&ToneDetInstance138,
	&ToneDetInstance139,
	&ToneDetInstance140,
	&ToneDetInstance141,
	&ToneDetInstance142,
	&ToneDetInstance143,
	&ToneDetInstance144,
	&ToneDetInstance145,
	&ToneDetInstance146,
	&ToneDetInstance147,
	&ToneDetInstance148,
	&ToneDetInstance149,
	&ToneDetInstance150,
	&ToneDetInstance151,
	&ToneDetInstance152,
	&ToneDetInstance153,
	&ToneDetInstance154,
	&ToneDetInstance155,
	&ToneDetInstance156,
	&ToneDetInstance157,
	&ToneDetInstance158,
	&ToneDetInstance159
};
//}

ADT_UInt16    CedDetAvail = CED_DETECT_INSTANCES;
ADT_UInt16    CedDetInUse[CED_DETECT_INSTANCES];
#pragma DATA_SECTION (CedDetAvail, "POOL_ALLOC")
#pragma DATA_SECTION (CedDetInUse, "POOL_ALLOC")
//{        FaxCEDChannel_t       faxCedInstance STRUCTURES to CHAN_INST_DATA:ToneDetect

FaxCEDChannel_t faxCedInstance0;
#pragma DATA_SECTION (faxCedInstance0, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance0, 128)

static FaxCEDChannel_t faxCedInstance1;
#pragma DATA_SECTION (faxCedInstance1, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance1, 128)

static FaxCEDChannel_t faxCedInstance2;
#pragma DATA_SECTION (faxCedInstance2, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance2, 128)

static FaxCEDChannel_t faxCedInstance3;
#pragma DATA_SECTION (faxCedInstance3, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance3, 128)

static FaxCEDChannel_t faxCedInstance4;
#pragma DATA_SECTION (faxCedInstance4, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance4, 128)

static FaxCEDChannel_t faxCedInstance5;
#pragma DATA_SECTION (faxCedInstance5, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance5, 128)

static FaxCEDChannel_t faxCedInstance6;
#pragma DATA_SECTION (faxCedInstance6, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance6, 128)

static FaxCEDChannel_t faxCedInstance7;
#pragma DATA_SECTION (faxCedInstance7, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance7, 128)

static FaxCEDChannel_t faxCedInstance8;
#pragma DATA_SECTION (faxCedInstance8, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance8, 128)

static FaxCEDChannel_t faxCedInstance9;
#pragma DATA_SECTION (faxCedInstance9, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance9, 128)

static FaxCEDChannel_t faxCedInstance10;
#pragma DATA_SECTION (faxCedInstance10, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance10, 128)

static FaxCEDChannel_t faxCedInstance11;
#pragma DATA_SECTION (faxCedInstance11, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance11, 128)

static FaxCEDChannel_t faxCedInstance12;
#pragma DATA_SECTION (faxCedInstance12, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance12, 128)

static FaxCEDChannel_t faxCedInstance13;
#pragma DATA_SECTION (faxCedInstance13, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance13, 128)

static FaxCEDChannel_t faxCedInstance14;
#pragma DATA_SECTION (faxCedInstance14, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance14, 128)

static FaxCEDChannel_t faxCedInstance15;
#pragma DATA_SECTION (faxCedInstance15, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance15, 128)

static FaxCEDChannel_t faxCedInstance16;
#pragma DATA_SECTION (faxCedInstance16, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance16, 128)

static FaxCEDChannel_t faxCedInstance17;
#pragma DATA_SECTION (faxCedInstance17, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance17, 128)

static FaxCEDChannel_t faxCedInstance18;
#pragma DATA_SECTION (faxCedInstance18, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance18, 128)

static FaxCEDChannel_t faxCedInstance19;
#pragma DATA_SECTION (faxCedInstance19, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance19, 128)

static FaxCEDChannel_t faxCedInstance20;
#pragma DATA_SECTION (faxCedInstance20, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance20, 128)

static FaxCEDChannel_t faxCedInstance21;
#pragma DATA_SECTION (faxCedInstance21, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance21, 128)

static FaxCEDChannel_t faxCedInstance22;
#pragma DATA_SECTION (faxCedInstance22, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance22, 128)

static FaxCEDChannel_t faxCedInstance23;
#pragma DATA_SECTION (faxCedInstance23, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance23, 128)

static FaxCEDChannel_t faxCedInstance24;
#pragma DATA_SECTION (faxCedInstance24, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance24, 128)

static FaxCEDChannel_t faxCedInstance25;
#pragma DATA_SECTION (faxCedInstance25, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance25, 128)

static FaxCEDChannel_t faxCedInstance26;
#pragma DATA_SECTION (faxCedInstance26, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance26, 128)

static FaxCEDChannel_t faxCedInstance27;
#pragma DATA_SECTION (faxCedInstance27, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance27, 128)

static FaxCEDChannel_t faxCedInstance28;
#pragma DATA_SECTION (faxCedInstance28, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance28, 128)

static FaxCEDChannel_t faxCedInstance29;
#pragma DATA_SECTION (faxCedInstance29, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance29, 128)

static FaxCEDChannel_t faxCedInstance30;
#pragma DATA_SECTION (faxCedInstance30, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance30, 128)

static FaxCEDChannel_t faxCedInstance31;
#pragma DATA_SECTION (faxCedInstance31, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance31, 128)

static FaxCEDChannel_t faxCedInstance32;
#pragma DATA_SECTION (faxCedInstance32, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance32, 128)

static FaxCEDChannel_t faxCedInstance33;
#pragma DATA_SECTION (faxCedInstance33, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance33, 128)

static FaxCEDChannel_t faxCedInstance34;
#pragma DATA_SECTION (faxCedInstance34, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance34, 128)

static FaxCEDChannel_t faxCedInstance35;
#pragma DATA_SECTION (faxCedInstance35, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance35, 128)

static FaxCEDChannel_t faxCedInstance36;
#pragma DATA_SECTION (faxCedInstance36, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance36, 128)

static FaxCEDChannel_t faxCedInstance37;
#pragma DATA_SECTION (faxCedInstance37, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance37, 128)

static FaxCEDChannel_t faxCedInstance38;
#pragma DATA_SECTION (faxCedInstance38, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance38, 128)

static FaxCEDChannel_t faxCedInstance39;
#pragma DATA_SECTION (faxCedInstance39, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance39, 128)

static FaxCEDChannel_t faxCedInstance40;
#pragma DATA_SECTION (faxCedInstance40, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance40, 128)

static FaxCEDChannel_t faxCedInstance41;
#pragma DATA_SECTION (faxCedInstance41, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance41, 128)

static FaxCEDChannel_t faxCedInstance42;
#pragma DATA_SECTION (faxCedInstance42, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance42, 128)

static FaxCEDChannel_t faxCedInstance43;
#pragma DATA_SECTION (faxCedInstance43, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance43, 128)

static FaxCEDChannel_t faxCedInstance44;
#pragma DATA_SECTION (faxCedInstance44, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance44, 128)

static FaxCEDChannel_t faxCedInstance45;
#pragma DATA_SECTION (faxCedInstance45, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance45, 128)

static FaxCEDChannel_t faxCedInstance46;
#pragma DATA_SECTION (faxCedInstance46, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance46, 128)

static FaxCEDChannel_t faxCedInstance47;
#pragma DATA_SECTION (faxCedInstance47, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance47, 128)

static FaxCEDChannel_t faxCedInstance48;
#pragma DATA_SECTION (faxCedInstance48, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance48, 128)

static FaxCEDChannel_t faxCedInstance49;
#pragma DATA_SECTION (faxCedInstance49, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance49, 128)

static FaxCEDChannel_t faxCedInstance50;
#pragma DATA_SECTION (faxCedInstance50, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance50, 128)

static FaxCEDChannel_t faxCedInstance51;
#pragma DATA_SECTION (faxCedInstance51, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance51, 128)

static FaxCEDChannel_t faxCedInstance52;
#pragma DATA_SECTION (faxCedInstance52, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance52, 128)

static FaxCEDChannel_t faxCedInstance53;
#pragma DATA_SECTION (faxCedInstance53, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance53, 128)

static FaxCEDChannel_t faxCedInstance54;
#pragma DATA_SECTION (faxCedInstance54, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance54, 128)

static FaxCEDChannel_t faxCedInstance55;
#pragma DATA_SECTION (faxCedInstance55, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance55, 128)

static FaxCEDChannel_t faxCedInstance56;
#pragma DATA_SECTION (faxCedInstance56, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance56, 128)

static FaxCEDChannel_t faxCedInstance57;
#pragma DATA_SECTION (faxCedInstance57, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance57, 128)

static FaxCEDChannel_t faxCedInstance58;
#pragma DATA_SECTION (faxCedInstance58, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance58, 128)

static FaxCEDChannel_t faxCedInstance59;
#pragma DATA_SECTION (faxCedInstance59, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance59, 128)

static FaxCEDChannel_t faxCedInstance60;
#pragma DATA_SECTION (faxCedInstance60, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance60, 128)

static FaxCEDChannel_t faxCedInstance61;
#pragma DATA_SECTION (faxCedInstance61, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance61, 128)

static FaxCEDChannel_t faxCedInstance62;
#pragma DATA_SECTION (faxCedInstance62, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance62, 128)

static FaxCEDChannel_t faxCedInstance63;
#pragma DATA_SECTION (faxCedInstance63, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance63, 128)

static FaxCEDChannel_t faxCedInstance64;
#pragma DATA_SECTION (faxCedInstance64, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance64, 128)

static FaxCEDChannel_t faxCedInstance65;
#pragma DATA_SECTION (faxCedInstance65, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance65, 128)

static FaxCEDChannel_t faxCedInstance66;
#pragma DATA_SECTION (faxCedInstance66, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance66, 128)

static FaxCEDChannel_t faxCedInstance67;
#pragma DATA_SECTION (faxCedInstance67, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance67, 128)

static FaxCEDChannel_t faxCedInstance68;
#pragma DATA_SECTION (faxCedInstance68, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance68, 128)

static FaxCEDChannel_t faxCedInstance69;
#pragma DATA_SECTION (faxCedInstance69, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance69, 128)

static FaxCEDChannel_t faxCedInstance70;
#pragma DATA_SECTION (faxCedInstance70, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance70, 128)

static FaxCEDChannel_t faxCedInstance71;
#pragma DATA_SECTION (faxCedInstance71, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance71, 128)

static FaxCEDChannel_t faxCedInstance72;
#pragma DATA_SECTION (faxCedInstance72, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance72, 128)

static FaxCEDChannel_t faxCedInstance73;
#pragma DATA_SECTION (faxCedInstance73, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance73, 128)

static FaxCEDChannel_t faxCedInstance74;
#pragma DATA_SECTION (faxCedInstance74, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance74, 128)

static FaxCEDChannel_t faxCedInstance75;
#pragma DATA_SECTION (faxCedInstance75, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance75, 128)

static FaxCEDChannel_t faxCedInstance76;
#pragma DATA_SECTION (faxCedInstance76, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance76, 128)

static FaxCEDChannel_t faxCedInstance77;
#pragma DATA_SECTION (faxCedInstance77, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance77, 128)

static FaxCEDChannel_t faxCedInstance78;
#pragma DATA_SECTION (faxCedInstance78, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance78, 128)

static FaxCEDChannel_t faxCedInstance79;
#pragma DATA_SECTION (faxCedInstance79, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance79, 128)

static FaxCEDChannel_t faxCedInstance80;
#pragma DATA_SECTION (faxCedInstance80, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance80, 128)

static FaxCEDChannel_t faxCedInstance81;
#pragma DATA_SECTION (faxCedInstance81, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance81, 128)

static FaxCEDChannel_t faxCedInstance82;
#pragma DATA_SECTION (faxCedInstance82, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance82, 128)

static FaxCEDChannel_t faxCedInstance83;
#pragma DATA_SECTION (faxCedInstance83, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance83, 128)

static FaxCEDChannel_t faxCedInstance84;
#pragma DATA_SECTION (faxCedInstance84, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance84, 128)

static FaxCEDChannel_t faxCedInstance85;
#pragma DATA_SECTION (faxCedInstance85, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance85, 128)

static FaxCEDChannel_t faxCedInstance86;
#pragma DATA_SECTION (faxCedInstance86, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance86, 128)

static FaxCEDChannel_t faxCedInstance87;
#pragma DATA_SECTION (faxCedInstance87, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance87, 128)

static FaxCEDChannel_t faxCedInstance88;
#pragma DATA_SECTION (faxCedInstance88, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance88, 128)

static FaxCEDChannel_t faxCedInstance89;
#pragma DATA_SECTION (faxCedInstance89, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance89, 128)

static FaxCEDChannel_t faxCedInstance90;
#pragma DATA_SECTION (faxCedInstance90, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance90, 128)

static FaxCEDChannel_t faxCedInstance91;
#pragma DATA_SECTION (faxCedInstance91, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance91, 128)

static FaxCEDChannel_t faxCedInstance92;
#pragma DATA_SECTION (faxCedInstance92, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance92, 128)

static FaxCEDChannel_t faxCedInstance93;
#pragma DATA_SECTION (faxCedInstance93, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance93, 128)

static FaxCEDChannel_t faxCedInstance94;
#pragma DATA_SECTION (faxCedInstance94, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance94, 128)

static FaxCEDChannel_t faxCedInstance95;
#pragma DATA_SECTION (faxCedInstance95, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance95, 128)

static FaxCEDChannel_t faxCedInstance96;
#pragma DATA_SECTION (faxCedInstance96, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance96, 128)

static FaxCEDChannel_t faxCedInstance97;
#pragma DATA_SECTION (faxCedInstance97, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance97, 128)

static FaxCEDChannel_t faxCedInstance98;
#pragma DATA_SECTION (faxCedInstance98, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance98, 128)

static FaxCEDChannel_t faxCedInstance99;
#pragma DATA_SECTION (faxCedInstance99, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance99, 128)

static FaxCEDChannel_t faxCedInstance100;
#pragma DATA_SECTION (faxCedInstance100, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance100, 128)

static FaxCEDChannel_t faxCedInstance101;
#pragma DATA_SECTION (faxCedInstance101, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance101, 128)

static FaxCEDChannel_t faxCedInstance102;
#pragma DATA_SECTION (faxCedInstance102, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance102, 128)

static FaxCEDChannel_t faxCedInstance103;
#pragma DATA_SECTION (faxCedInstance103, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance103, 128)

static FaxCEDChannel_t faxCedInstance104;
#pragma DATA_SECTION (faxCedInstance104, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance104, 128)

static FaxCEDChannel_t faxCedInstance105;
#pragma DATA_SECTION (faxCedInstance105, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance105, 128)

static FaxCEDChannel_t faxCedInstance106;
#pragma DATA_SECTION (faxCedInstance106, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance106, 128)

static FaxCEDChannel_t faxCedInstance107;
#pragma DATA_SECTION (faxCedInstance107, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance107, 128)

static FaxCEDChannel_t faxCedInstance108;
#pragma DATA_SECTION (faxCedInstance108, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance108, 128)

static FaxCEDChannel_t faxCedInstance109;
#pragma DATA_SECTION (faxCedInstance109, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance109, 128)

static FaxCEDChannel_t faxCedInstance110;
#pragma DATA_SECTION (faxCedInstance110, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance110, 128)

static FaxCEDChannel_t faxCedInstance111;
#pragma DATA_SECTION (faxCedInstance111, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance111, 128)

static FaxCEDChannel_t faxCedInstance112;
#pragma DATA_SECTION (faxCedInstance112, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance112, 128)

static FaxCEDChannel_t faxCedInstance113;
#pragma DATA_SECTION (faxCedInstance113, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance113, 128)

static FaxCEDChannel_t faxCedInstance114;
#pragma DATA_SECTION (faxCedInstance114, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance114, 128)

static FaxCEDChannel_t faxCedInstance115;
#pragma DATA_SECTION (faxCedInstance115, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance115, 128)

static FaxCEDChannel_t faxCedInstance116;
#pragma DATA_SECTION (faxCedInstance116, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance116, 128)

static FaxCEDChannel_t faxCedInstance117;
#pragma DATA_SECTION (faxCedInstance117, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance117, 128)

static FaxCEDChannel_t faxCedInstance118;
#pragma DATA_SECTION (faxCedInstance118, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance118, 128)

static FaxCEDChannel_t faxCedInstance119;
#pragma DATA_SECTION (faxCedInstance119, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance119, 128)

static FaxCEDChannel_t faxCedInstance120;
#pragma DATA_SECTION (faxCedInstance120, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance120, 128)

static FaxCEDChannel_t faxCedInstance121;
#pragma DATA_SECTION (faxCedInstance121, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance121, 128)

static FaxCEDChannel_t faxCedInstance122;
#pragma DATA_SECTION (faxCedInstance122, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance122, 128)

static FaxCEDChannel_t faxCedInstance123;
#pragma DATA_SECTION (faxCedInstance123, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance123, 128)

static FaxCEDChannel_t faxCedInstance124;
#pragma DATA_SECTION (faxCedInstance124, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance124, 128)

static FaxCEDChannel_t faxCedInstance125;
#pragma DATA_SECTION (faxCedInstance125, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance125, 128)

static FaxCEDChannel_t faxCedInstance126;
#pragma DATA_SECTION (faxCedInstance126, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance126, 128)

static FaxCEDChannel_t faxCedInstance127;
#pragma DATA_SECTION (faxCedInstance127, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance127, 128)

static FaxCEDChannel_t faxCedInstance128;
#pragma DATA_SECTION (faxCedInstance128, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance128, 128)

static FaxCEDChannel_t faxCedInstance129;
#pragma DATA_SECTION (faxCedInstance129, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance129, 128)

static FaxCEDChannel_t faxCedInstance130;
#pragma DATA_SECTION (faxCedInstance130, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance130, 128)

static FaxCEDChannel_t faxCedInstance131;
#pragma DATA_SECTION (faxCedInstance131, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance131, 128)

static FaxCEDChannel_t faxCedInstance132;
#pragma DATA_SECTION (faxCedInstance132, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance132, 128)

static FaxCEDChannel_t faxCedInstance133;
#pragma DATA_SECTION (faxCedInstance133, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance133, 128)

static FaxCEDChannel_t faxCedInstance134;
#pragma DATA_SECTION (faxCedInstance134, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance134, 128)

static FaxCEDChannel_t faxCedInstance135;
#pragma DATA_SECTION (faxCedInstance135, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance135, 128)

static FaxCEDChannel_t faxCedInstance136;
#pragma DATA_SECTION (faxCedInstance136, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance136, 128)

static FaxCEDChannel_t faxCedInstance137;
#pragma DATA_SECTION (faxCedInstance137, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance137, 128)

static FaxCEDChannel_t faxCedInstance138;
#pragma DATA_SECTION (faxCedInstance138, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance138, 128)

static FaxCEDChannel_t faxCedInstance139;
#pragma DATA_SECTION (faxCedInstance139, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance139, 128)

static FaxCEDChannel_t faxCedInstance140;
#pragma DATA_SECTION (faxCedInstance140, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance140, 128)

static FaxCEDChannel_t faxCedInstance141;
#pragma DATA_SECTION (faxCedInstance141, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance141, 128)

static FaxCEDChannel_t faxCedInstance142;
#pragma DATA_SECTION (faxCedInstance142, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance142, 128)

static FaxCEDChannel_t faxCedInstance143;
#pragma DATA_SECTION (faxCedInstance143, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance143, 128)

static FaxCEDChannel_t faxCedInstance144;
#pragma DATA_SECTION (faxCedInstance144, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance144, 128)

static FaxCEDChannel_t faxCedInstance145;
#pragma DATA_SECTION (faxCedInstance145, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance145, 128)

static FaxCEDChannel_t faxCedInstance146;
#pragma DATA_SECTION (faxCedInstance146, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance146, 128)

static FaxCEDChannel_t faxCedInstance147;
#pragma DATA_SECTION (faxCedInstance147, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance147, 128)

static FaxCEDChannel_t faxCedInstance148;
#pragma DATA_SECTION (faxCedInstance148, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance148, 128)

static FaxCEDChannel_t faxCedInstance149;
#pragma DATA_SECTION (faxCedInstance149, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance149, 128)

static FaxCEDChannel_t faxCedInstance150;
#pragma DATA_SECTION (faxCedInstance150, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance150, 128)

static FaxCEDChannel_t faxCedInstance151;
#pragma DATA_SECTION (faxCedInstance151, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance151, 128)

static FaxCEDChannel_t faxCedInstance152;
#pragma DATA_SECTION (faxCedInstance152, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance152, 128)

static FaxCEDChannel_t faxCedInstance153;
#pragma DATA_SECTION (faxCedInstance153, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance153, 128)

static FaxCEDChannel_t faxCedInstance154;
#pragma DATA_SECTION (faxCedInstance154, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance154, 128)

static FaxCEDChannel_t faxCedInstance155;
#pragma DATA_SECTION (faxCedInstance155, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance155, 128)

static FaxCEDChannel_t faxCedInstance156;
#pragma DATA_SECTION (faxCedInstance156, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance156, 128)

static FaxCEDChannel_t faxCedInstance157;
#pragma DATA_SECTION (faxCedInstance157, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance157, 128)

static FaxCEDChannel_t faxCedInstance158;
#pragma DATA_SECTION (faxCedInstance158, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance158, 128)

static FaxCEDChannel_t faxCedInstance159;
#pragma DATA_SECTION (faxCedInstance159, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCedInstance159, 128)

FaxCEDChannel_t* const faxCedInstance[] = {
	&faxCedInstance0,
	&faxCedInstance1,
	&faxCedInstance2,
	&faxCedInstance3,
	&faxCedInstance4,
	&faxCedInstance5,
	&faxCedInstance6,
	&faxCedInstance7,
	&faxCedInstance8,
	&faxCedInstance9,
	&faxCedInstance10,
	&faxCedInstance11,
	&faxCedInstance12,
	&faxCedInstance13,
	&faxCedInstance14,
	&faxCedInstance15,
	&faxCedInstance16,
	&faxCedInstance17,
	&faxCedInstance18,
	&faxCedInstance19,
	&faxCedInstance20,
	&faxCedInstance21,
	&faxCedInstance22,
	&faxCedInstance23,
	&faxCedInstance24,
	&faxCedInstance25,
	&faxCedInstance26,
	&faxCedInstance27,
	&faxCedInstance28,
	&faxCedInstance29,
	&faxCedInstance30,
	&faxCedInstance31,
	&faxCedInstance32,
	&faxCedInstance33,
	&faxCedInstance34,
	&faxCedInstance35,
	&faxCedInstance36,
	&faxCedInstance37,
	&faxCedInstance38,
	&faxCedInstance39,
	&faxCedInstance40,
	&faxCedInstance41,
	&faxCedInstance42,
	&faxCedInstance43,
	&faxCedInstance44,
	&faxCedInstance45,
	&faxCedInstance46,
	&faxCedInstance47,
	&faxCedInstance48,
	&faxCedInstance49,
	&faxCedInstance50,
	&faxCedInstance51,
	&faxCedInstance52,
	&faxCedInstance53,
	&faxCedInstance54,
	&faxCedInstance55,
	&faxCedInstance56,
	&faxCedInstance57,
	&faxCedInstance58,
	&faxCedInstance59,
	&faxCedInstance60,
	&faxCedInstance61,
	&faxCedInstance62,
	&faxCedInstance63,
	&faxCedInstance64,
	&faxCedInstance65,
	&faxCedInstance66,
	&faxCedInstance67,
	&faxCedInstance68,
	&faxCedInstance69,
	&faxCedInstance70,
	&faxCedInstance71,
	&faxCedInstance72,
	&faxCedInstance73,
	&faxCedInstance74,
	&faxCedInstance75,
	&faxCedInstance76,
	&faxCedInstance77,
	&faxCedInstance78,
	&faxCedInstance79,
	&faxCedInstance80,
	&faxCedInstance81,
	&faxCedInstance82,
	&faxCedInstance83,
	&faxCedInstance84,
	&faxCedInstance85,
	&faxCedInstance86,
	&faxCedInstance87,
	&faxCedInstance88,
	&faxCedInstance89,
	&faxCedInstance90,
	&faxCedInstance91,
	&faxCedInstance92,
	&faxCedInstance93,
	&faxCedInstance94,
	&faxCedInstance95,
	&faxCedInstance96,
	&faxCedInstance97,
	&faxCedInstance98,
	&faxCedInstance99,
	&faxCedInstance100,
	&faxCedInstance101,
	&faxCedInstance102,
	&faxCedInstance103,
	&faxCedInstance104,
	&faxCedInstance105,
	&faxCedInstance106,
	&faxCedInstance107,
	&faxCedInstance108,
	&faxCedInstance109,
	&faxCedInstance110,
	&faxCedInstance111,
	&faxCedInstance112,
	&faxCedInstance113,
	&faxCedInstance114,
	&faxCedInstance115,
	&faxCedInstance116,
	&faxCedInstance117,
	&faxCedInstance118,
	&faxCedInstance119,
	&faxCedInstance120,
	&faxCedInstance121,
	&faxCedInstance122,
	&faxCedInstance123,
	&faxCedInstance124,
	&faxCedInstance125,
	&faxCedInstance126,
	&faxCedInstance127,
	&faxCedInstance128,
	&faxCedInstance129,
	&faxCedInstance130,
	&faxCedInstance131,
	&faxCedInstance132,
	&faxCedInstance133,
	&faxCedInstance134,
	&faxCedInstance135,
	&faxCedInstance136,
	&faxCedInstance137,
	&faxCedInstance138,
	&faxCedInstance139,
	&faxCedInstance140,
	&faxCedInstance141,
	&faxCedInstance142,
	&faxCedInstance143,
	&faxCedInstance144,
	&faxCedInstance145,
	&faxCedInstance146,
	&faxCedInstance147,
	&faxCedInstance148,
	&faxCedInstance149,
	&faxCedInstance150,
	&faxCedInstance151,
	&faxCedInstance152,
	&faxCedInstance153,
	&faxCedInstance154,
	&faxCedInstance155,
	&faxCedInstance156,
	&faxCedInstance157,
	&faxCedInstance158,
	&faxCedInstance159
};
//}

ADT_UInt16    CngDetAvail = CNG_DETECT_INSTANCES;
ADT_UInt16    CngDetInUse[CNG_DETECT_INSTANCES];
#pragma DATA_SECTION (CngDetAvail, "POOL_ALLOC")
#pragma DATA_SECTION (CngDetInUse, "POOL_ALLOC")
//{       FAXCNGInstance_t       faxCngInstance STRUCTURES to CHAN_INST_DATA:ToneDetect

FAXCNGInstance_t faxCngInstance0;
#pragma DATA_SECTION (faxCngInstance0, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance0, 128)

static FAXCNGInstance_t faxCngInstance1;
#pragma DATA_SECTION (faxCngInstance1, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance1, 128)

static FAXCNGInstance_t faxCngInstance2;
#pragma DATA_SECTION (faxCngInstance2, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance2, 128)

static FAXCNGInstance_t faxCngInstance3;
#pragma DATA_SECTION (faxCngInstance3, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance3, 128)

static FAXCNGInstance_t faxCngInstance4;
#pragma DATA_SECTION (faxCngInstance4, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance4, 128)

static FAXCNGInstance_t faxCngInstance5;
#pragma DATA_SECTION (faxCngInstance5, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance5, 128)

static FAXCNGInstance_t faxCngInstance6;
#pragma DATA_SECTION (faxCngInstance6, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance6, 128)

static FAXCNGInstance_t faxCngInstance7;
#pragma DATA_SECTION (faxCngInstance7, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance7, 128)

static FAXCNGInstance_t faxCngInstance8;
#pragma DATA_SECTION (faxCngInstance8, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance8, 128)

static FAXCNGInstance_t faxCngInstance9;
#pragma DATA_SECTION (faxCngInstance9, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance9, 128)

static FAXCNGInstance_t faxCngInstance10;
#pragma DATA_SECTION (faxCngInstance10, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance10, 128)

static FAXCNGInstance_t faxCngInstance11;
#pragma DATA_SECTION (faxCngInstance11, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance11, 128)

static FAXCNGInstance_t faxCngInstance12;
#pragma DATA_SECTION (faxCngInstance12, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance12, 128)

static FAXCNGInstance_t faxCngInstance13;
#pragma DATA_SECTION (faxCngInstance13, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance13, 128)

static FAXCNGInstance_t faxCngInstance14;
#pragma DATA_SECTION (faxCngInstance14, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance14, 128)

static FAXCNGInstance_t faxCngInstance15;
#pragma DATA_SECTION (faxCngInstance15, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance15, 128)

static FAXCNGInstance_t faxCngInstance16;
#pragma DATA_SECTION (faxCngInstance16, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance16, 128)

static FAXCNGInstance_t faxCngInstance17;
#pragma DATA_SECTION (faxCngInstance17, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance17, 128)

static FAXCNGInstance_t faxCngInstance18;
#pragma DATA_SECTION (faxCngInstance18, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance18, 128)

static FAXCNGInstance_t faxCngInstance19;
#pragma DATA_SECTION (faxCngInstance19, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance19, 128)

static FAXCNGInstance_t faxCngInstance20;
#pragma DATA_SECTION (faxCngInstance20, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance20, 128)

static FAXCNGInstance_t faxCngInstance21;
#pragma DATA_SECTION (faxCngInstance21, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance21, 128)

static FAXCNGInstance_t faxCngInstance22;
#pragma DATA_SECTION (faxCngInstance22, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance22, 128)

static FAXCNGInstance_t faxCngInstance23;
#pragma DATA_SECTION (faxCngInstance23, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance23, 128)

static FAXCNGInstance_t faxCngInstance24;
#pragma DATA_SECTION (faxCngInstance24, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance24, 128)

static FAXCNGInstance_t faxCngInstance25;
#pragma DATA_SECTION (faxCngInstance25, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance25, 128)

static FAXCNGInstance_t faxCngInstance26;
#pragma DATA_SECTION (faxCngInstance26, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance26, 128)

static FAXCNGInstance_t faxCngInstance27;
#pragma DATA_SECTION (faxCngInstance27, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance27, 128)

static FAXCNGInstance_t faxCngInstance28;
#pragma DATA_SECTION (faxCngInstance28, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance28, 128)

static FAXCNGInstance_t faxCngInstance29;
#pragma DATA_SECTION (faxCngInstance29, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance29, 128)

static FAXCNGInstance_t faxCngInstance30;
#pragma DATA_SECTION (faxCngInstance30, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance30, 128)

static FAXCNGInstance_t faxCngInstance31;
#pragma DATA_SECTION (faxCngInstance31, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance31, 128)

static FAXCNGInstance_t faxCngInstance32;
#pragma DATA_SECTION (faxCngInstance32, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance32, 128)

static FAXCNGInstance_t faxCngInstance33;
#pragma DATA_SECTION (faxCngInstance33, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance33, 128)

static FAXCNGInstance_t faxCngInstance34;
#pragma DATA_SECTION (faxCngInstance34, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance34, 128)

static FAXCNGInstance_t faxCngInstance35;
#pragma DATA_SECTION (faxCngInstance35, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance35, 128)

static FAXCNGInstance_t faxCngInstance36;
#pragma DATA_SECTION (faxCngInstance36, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance36, 128)

static FAXCNGInstance_t faxCngInstance37;
#pragma DATA_SECTION (faxCngInstance37, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance37, 128)

static FAXCNGInstance_t faxCngInstance38;
#pragma DATA_SECTION (faxCngInstance38, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance38, 128)

static FAXCNGInstance_t faxCngInstance39;
#pragma DATA_SECTION (faxCngInstance39, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance39, 128)

static FAXCNGInstance_t faxCngInstance40;
#pragma DATA_SECTION (faxCngInstance40, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance40, 128)

static FAXCNGInstance_t faxCngInstance41;
#pragma DATA_SECTION (faxCngInstance41, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance41, 128)

static FAXCNGInstance_t faxCngInstance42;
#pragma DATA_SECTION (faxCngInstance42, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance42, 128)

static FAXCNGInstance_t faxCngInstance43;
#pragma DATA_SECTION (faxCngInstance43, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance43, 128)

static FAXCNGInstance_t faxCngInstance44;
#pragma DATA_SECTION (faxCngInstance44, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance44, 128)

static FAXCNGInstance_t faxCngInstance45;
#pragma DATA_SECTION (faxCngInstance45, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance45, 128)

static FAXCNGInstance_t faxCngInstance46;
#pragma DATA_SECTION (faxCngInstance46, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance46, 128)

static FAXCNGInstance_t faxCngInstance47;
#pragma DATA_SECTION (faxCngInstance47, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance47, 128)

static FAXCNGInstance_t faxCngInstance48;
#pragma DATA_SECTION (faxCngInstance48, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance48, 128)

static FAXCNGInstance_t faxCngInstance49;
#pragma DATA_SECTION (faxCngInstance49, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance49, 128)

static FAXCNGInstance_t faxCngInstance50;
#pragma DATA_SECTION (faxCngInstance50, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance50, 128)

static FAXCNGInstance_t faxCngInstance51;
#pragma DATA_SECTION (faxCngInstance51, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance51, 128)

static FAXCNGInstance_t faxCngInstance52;
#pragma DATA_SECTION (faxCngInstance52, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance52, 128)

static FAXCNGInstance_t faxCngInstance53;
#pragma DATA_SECTION (faxCngInstance53, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance53, 128)

static FAXCNGInstance_t faxCngInstance54;
#pragma DATA_SECTION (faxCngInstance54, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance54, 128)

static FAXCNGInstance_t faxCngInstance55;
#pragma DATA_SECTION (faxCngInstance55, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance55, 128)

static FAXCNGInstance_t faxCngInstance56;
#pragma DATA_SECTION (faxCngInstance56, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance56, 128)

static FAXCNGInstance_t faxCngInstance57;
#pragma DATA_SECTION (faxCngInstance57, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance57, 128)

static FAXCNGInstance_t faxCngInstance58;
#pragma DATA_SECTION (faxCngInstance58, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance58, 128)

static FAXCNGInstance_t faxCngInstance59;
#pragma DATA_SECTION (faxCngInstance59, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance59, 128)

static FAXCNGInstance_t faxCngInstance60;
#pragma DATA_SECTION (faxCngInstance60, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance60, 128)

static FAXCNGInstance_t faxCngInstance61;
#pragma DATA_SECTION (faxCngInstance61, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance61, 128)

static FAXCNGInstance_t faxCngInstance62;
#pragma DATA_SECTION (faxCngInstance62, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance62, 128)

static FAXCNGInstance_t faxCngInstance63;
#pragma DATA_SECTION (faxCngInstance63, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance63, 128)

static FAXCNGInstance_t faxCngInstance64;
#pragma DATA_SECTION (faxCngInstance64, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance64, 128)

static FAXCNGInstance_t faxCngInstance65;
#pragma DATA_SECTION (faxCngInstance65, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance65, 128)

static FAXCNGInstance_t faxCngInstance66;
#pragma DATA_SECTION (faxCngInstance66, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance66, 128)

static FAXCNGInstance_t faxCngInstance67;
#pragma DATA_SECTION (faxCngInstance67, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance67, 128)

static FAXCNGInstance_t faxCngInstance68;
#pragma DATA_SECTION (faxCngInstance68, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance68, 128)

static FAXCNGInstance_t faxCngInstance69;
#pragma DATA_SECTION (faxCngInstance69, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance69, 128)

static FAXCNGInstance_t faxCngInstance70;
#pragma DATA_SECTION (faxCngInstance70, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance70, 128)

static FAXCNGInstance_t faxCngInstance71;
#pragma DATA_SECTION (faxCngInstance71, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance71, 128)

static FAXCNGInstance_t faxCngInstance72;
#pragma DATA_SECTION (faxCngInstance72, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance72, 128)

static FAXCNGInstance_t faxCngInstance73;
#pragma DATA_SECTION (faxCngInstance73, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance73, 128)

static FAXCNGInstance_t faxCngInstance74;
#pragma DATA_SECTION (faxCngInstance74, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance74, 128)

static FAXCNGInstance_t faxCngInstance75;
#pragma DATA_SECTION (faxCngInstance75, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance75, 128)

static FAXCNGInstance_t faxCngInstance76;
#pragma DATA_SECTION (faxCngInstance76, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance76, 128)

static FAXCNGInstance_t faxCngInstance77;
#pragma DATA_SECTION (faxCngInstance77, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance77, 128)

static FAXCNGInstance_t faxCngInstance78;
#pragma DATA_SECTION (faxCngInstance78, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance78, 128)

static FAXCNGInstance_t faxCngInstance79;
#pragma DATA_SECTION (faxCngInstance79, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance79, 128)

static FAXCNGInstance_t faxCngInstance80;
#pragma DATA_SECTION (faxCngInstance80, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance80, 128)

static FAXCNGInstance_t faxCngInstance81;
#pragma DATA_SECTION (faxCngInstance81, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance81, 128)

static FAXCNGInstance_t faxCngInstance82;
#pragma DATA_SECTION (faxCngInstance82, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance82, 128)

static FAXCNGInstance_t faxCngInstance83;
#pragma DATA_SECTION (faxCngInstance83, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance83, 128)

static FAXCNGInstance_t faxCngInstance84;
#pragma DATA_SECTION (faxCngInstance84, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance84, 128)

static FAXCNGInstance_t faxCngInstance85;
#pragma DATA_SECTION (faxCngInstance85, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance85, 128)

static FAXCNGInstance_t faxCngInstance86;
#pragma DATA_SECTION (faxCngInstance86, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance86, 128)

static FAXCNGInstance_t faxCngInstance87;
#pragma DATA_SECTION (faxCngInstance87, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance87, 128)

static FAXCNGInstance_t faxCngInstance88;
#pragma DATA_SECTION (faxCngInstance88, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance88, 128)

static FAXCNGInstance_t faxCngInstance89;
#pragma DATA_SECTION (faxCngInstance89, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance89, 128)

static FAXCNGInstance_t faxCngInstance90;
#pragma DATA_SECTION (faxCngInstance90, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance90, 128)

static FAXCNGInstance_t faxCngInstance91;
#pragma DATA_SECTION (faxCngInstance91, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance91, 128)

static FAXCNGInstance_t faxCngInstance92;
#pragma DATA_SECTION (faxCngInstance92, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance92, 128)

static FAXCNGInstance_t faxCngInstance93;
#pragma DATA_SECTION (faxCngInstance93, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance93, 128)

static FAXCNGInstance_t faxCngInstance94;
#pragma DATA_SECTION (faxCngInstance94, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance94, 128)

static FAXCNGInstance_t faxCngInstance95;
#pragma DATA_SECTION (faxCngInstance95, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance95, 128)

static FAXCNGInstance_t faxCngInstance96;
#pragma DATA_SECTION (faxCngInstance96, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance96, 128)

static FAXCNGInstance_t faxCngInstance97;
#pragma DATA_SECTION (faxCngInstance97, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance97, 128)

static FAXCNGInstance_t faxCngInstance98;
#pragma DATA_SECTION (faxCngInstance98, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance98, 128)

static FAXCNGInstance_t faxCngInstance99;
#pragma DATA_SECTION (faxCngInstance99, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance99, 128)

static FAXCNGInstance_t faxCngInstance100;
#pragma DATA_SECTION (faxCngInstance100, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance100, 128)

static FAXCNGInstance_t faxCngInstance101;
#pragma DATA_SECTION (faxCngInstance101, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance101, 128)

static FAXCNGInstance_t faxCngInstance102;
#pragma DATA_SECTION (faxCngInstance102, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance102, 128)

static FAXCNGInstance_t faxCngInstance103;
#pragma DATA_SECTION (faxCngInstance103, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance103, 128)

static FAXCNGInstance_t faxCngInstance104;
#pragma DATA_SECTION (faxCngInstance104, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance104, 128)

static FAXCNGInstance_t faxCngInstance105;
#pragma DATA_SECTION (faxCngInstance105, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance105, 128)

static FAXCNGInstance_t faxCngInstance106;
#pragma DATA_SECTION (faxCngInstance106, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance106, 128)

static FAXCNGInstance_t faxCngInstance107;
#pragma DATA_SECTION (faxCngInstance107, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance107, 128)

static FAXCNGInstance_t faxCngInstance108;
#pragma DATA_SECTION (faxCngInstance108, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance108, 128)

static FAXCNGInstance_t faxCngInstance109;
#pragma DATA_SECTION (faxCngInstance109, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance109, 128)

static FAXCNGInstance_t faxCngInstance110;
#pragma DATA_SECTION (faxCngInstance110, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance110, 128)

static FAXCNGInstance_t faxCngInstance111;
#pragma DATA_SECTION (faxCngInstance111, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance111, 128)

static FAXCNGInstance_t faxCngInstance112;
#pragma DATA_SECTION (faxCngInstance112, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance112, 128)

static FAXCNGInstance_t faxCngInstance113;
#pragma DATA_SECTION (faxCngInstance113, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance113, 128)

static FAXCNGInstance_t faxCngInstance114;
#pragma DATA_SECTION (faxCngInstance114, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance114, 128)

static FAXCNGInstance_t faxCngInstance115;
#pragma DATA_SECTION (faxCngInstance115, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance115, 128)

static FAXCNGInstance_t faxCngInstance116;
#pragma DATA_SECTION (faxCngInstance116, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance116, 128)

static FAXCNGInstance_t faxCngInstance117;
#pragma DATA_SECTION (faxCngInstance117, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance117, 128)

static FAXCNGInstance_t faxCngInstance118;
#pragma DATA_SECTION (faxCngInstance118, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance118, 128)

static FAXCNGInstance_t faxCngInstance119;
#pragma DATA_SECTION (faxCngInstance119, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance119, 128)

static FAXCNGInstance_t faxCngInstance120;
#pragma DATA_SECTION (faxCngInstance120, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance120, 128)

static FAXCNGInstance_t faxCngInstance121;
#pragma DATA_SECTION (faxCngInstance121, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance121, 128)

static FAXCNGInstance_t faxCngInstance122;
#pragma DATA_SECTION (faxCngInstance122, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance122, 128)

static FAXCNGInstance_t faxCngInstance123;
#pragma DATA_SECTION (faxCngInstance123, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance123, 128)

static FAXCNGInstance_t faxCngInstance124;
#pragma DATA_SECTION (faxCngInstance124, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance124, 128)

static FAXCNGInstance_t faxCngInstance125;
#pragma DATA_SECTION (faxCngInstance125, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance125, 128)

static FAXCNGInstance_t faxCngInstance126;
#pragma DATA_SECTION (faxCngInstance126, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance126, 128)

static FAXCNGInstance_t faxCngInstance127;
#pragma DATA_SECTION (faxCngInstance127, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance127, 128)

static FAXCNGInstance_t faxCngInstance128;
#pragma DATA_SECTION (faxCngInstance128, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance128, 128)

static FAXCNGInstance_t faxCngInstance129;
#pragma DATA_SECTION (faxCngInstance129, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance129, 128)

static FAXCNGInstance_t faxCngInstance130;
#pragma DATA_SECTION (faxCngInstance130, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance130, 128)

static FAXCNGInstance_t faxCngInstance131;
#pragma DATA_SECTION (faxCngInstance131, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance131, 128)

static FAXCNGInstance_t faxCngInstance132;
#pragma DATA_SECTION (faxCngInstance132, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance132, 128)

static FAXCNGInstance_t faxCngInstance133;
#pragma DATA_SECTION (faxCngInstance133, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance133, 128)

static FAXCNGInstance_t faxCngInstance134;
#pragma DATA_SECTION (faxCngInstance134, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance134, 128)

static FAXCNGInstance_t faxCngInstance135;
#pragma DATA_SECTION (faxCngInstance135, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance135, 128)

static FAXCNGInstance_t faxCngInstance136;
#pragma DATA_SECTION (faxCngInstance136, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance136, 128)

static FAXCNGInstance_t faxCngInstance137;
#pragma DATA_SECTION (faxCngInstance137, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance137, 128)

static FAXCNGInstance_t faxCngInstance138;
#pragma DATA_SECTION (faxCngInstance138, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance138, 128)

static FAXCNGInstance_t faxCngInstance139;
#pragma DATA_SECTION (faxCngInstance139, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance139, 128)

static FAXCNGInstance_t faxCngInstance140;
#pragma DATA_SECTION (faxCngInstance140, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance140, 128)

static FAXCNGInstance_t faxCngInstance141;
#pragma DATA_SECTION (faxCngInstance141, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance141, 128)

static FAXCNGInstance_t faxCngInstance142;
#pragma DATA_SECTION (faxCngInstance142, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance142, 128)

static FAXCNGInstance_t faxCngInstance143;
#pragma DATA_SECTION (faxCngInstance143, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance143, 128)

static FAXCNGInstance_t faxCngInstance144;
#pragma DATA_SECTION (faxCngInstance144, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance144, 128)

static FAXCNGInstance_t faxCngInstance145;
#pragma DATA_SECTION (faxCngInstance145, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance145, 128)

static FAXCNGInstance_t faxCngInstance146;
#pragma DATA_SECTION (faxCngInstance146, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance146, 128)

static FAXCNGInstance_t faxCngInstance147;
#pragma DATA_SECTION (faxCngInstance147, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance147, 128)

static FAXCNGInstance_t faxCngInstance148;
#pragma DATA_SECTION (faxCngInstance148, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance148, 128)

static FAXCNGInstance_t faxCngInstance149;
#pragma DATA_SECTION (faxCngInstance149, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance149, 128)

static FAXCNGInstance_t faxCngInstance150;
#pragma DATA_SECTION (faxCngInstance150, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance150, 128)

static FAXCNGInstance_t faxCngInstance151;
#pragma DATA_SECTION (faxCngInstance151, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance151, 128)

static FAXCNGInstance_t faxCngInstance152;
#pragma DATA_SECTION (faxCngInstance152, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance152, 128)

static FAXCNGInstance_t faxCngInstance153;
#pragma DATA_SECTION (faxCngInstance153, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance153, 128)

static FAXCNGInstance_t faxCngInstance154;
#pragma DATA_SECTION (faxCngInstance154, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance154, 128)

static FAXCNGInstance_t faxCngInstance155;
#pragma DATA_SECTION (faxCngInstance155, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance155, 128)

static FAXCNGInstance_t faxCngInstance156;
#pragma DATA_SECTION (faxCngInstance156, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance156, 128)

static FAXCNGInstance_t faxCngInstance157;
#pragma DATA_SECTION (faxCngInstance157, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance157, 128)

static FAXCNGInstance_t faxCngInstance158;
#pragma DATA_SECTION (faxCngInstance158, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance158, 128)

static FAXCNGInstance_t faxCngInstance159;
#pragma DATA_SECTION (faxCngInstance159, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (faxCngInstance159, 128)

FAXCNGInstance_t* const faxCngInstance[] = {
	&faxCngInstance0,
	&faxCngInstance1,
	&faxCngInstance2,
	&faxCngInstance3,
	&faxCngInstance4,
	&faxCngInstance5,
	&faxCngInstance6,
	&faxCngInstance7,
	&faxCngInstance8,
	&faxCngInstance9,
	&faxCngInstance10,
	&faxCngInstance11,
	&faxCngInstance12,
	&faxCngInstance13,
	&faxCngInstance14,
	&faxCngInstance15,
	&faxCngInstance16,
	&faxCngInstance17,
	&faxCngInstance18,
	&faxCngInstance19,
	&faxCngInstance20,
	&faxCngInstance21,
	&faxCngInstance22,
	&faxCngInstance23,
	&faxCngInstance24,
	&faxCngInstance25,
	&faxCngInstance26,
	&faxCngInstance27,
	&faxCngInstance28,
	&faxCngInstance29,
	&faxCngInstance30,
	&faxCngInstance31,
	&faxCngInstance32,
	&faxCngInstance33,
	&faxCngInstance34,
	&faxCngInstance35,
	&faxCngInstance36,
	&faxCngInstance37,
	&faxCngInstance38,
	&faxCngInstance39,
	&faxCngInstance40,
	&faxCngInstance41,
	&faxCngInstance42,
	&faxCngInstance43,
	&faxCngInstance44,
	&faxCngInstance45,
	&faxCngInstance46,
	&faxCngInstance47,
	&faxCngInstance48,
	&faxCngInstance49,
	&faxCngInstance50,
	&faxCngInstance51,
	&faxCngInstance52,
	&faxCngInstance53,
	&faxCngInstance54,
	&faxCngInstance55,
	&faxCngInstance56,
	&faxCngInstance57,
	&faxCngInstance58,
	&faxCngInstance59,
	&faxCngInstance60,
	&faxCngInstance61,
	&faxCngInstance62,
	&faxCngInstance63,
	&faxCngInstance64,
	&faxCngInstance65,
	&faxCngInstance66,
	&faxCngInstance67,
	&faxCngInstance68,
	&faxCngInstance69,
	&faxCngInstance70,
	&faxCngInstance71,
	&faxCngInstance72,
	&faxCngInstance73,
	&faxCngInstance74,
	&faxCngInstance75,
	&faxCngInstance76,
	&faxCngInstance77,
	&faxCngInstance78,
	&faxCngInstance79,
	&faxCngInstance80,
	&faxCngInstance81,
	&faxCngInstance82,
	&faxCngInstance83,
	&faxCngInstance84,
	&faxCngInstance85,
	&faxCngInstance86,
	&faxCngInstance87,
	&faxCngInstance88,
	&faxCngInstance89,
	&faxCngInstance90,
	&faxCngInstance91,
	&faxCngInstance92,
	&faxCngInstance93,
	&faxCngInstance94,
	&faxCngInstance95,
	&faxCngInstance96,
	&faxCngInstance97,
	&faxCngInstance98,
	&faxCngInstance99,
	&faxCngInstance100,
	&faxCngInstance101,
	&faxCngInstance102,
	&faxCngInstance103,
	&faxCngInstance104,
	&faxCngInstance105,
	&faxCngInstance106,
	&faxCngInstance107,
	&faxCngInstance108,
	&faxCngInstance109,
	&faxCngInstance110,
	&faxCngInstance111,
	&faxCngInstance112,
	&faxCngInstance113,
	&faxCngInstance114,
	&faxCngInstance115,
	&faxCngInstance116,
	&faxCngInstance117,
	&faxCngInstance118,
	&faxCngInstance119,
	&faxCngInstance120,
	&faxCngInstance121,
	&faxCngInstance122,
	&faxCngInstance123,
	&faxCngInstance124,
	&faxCngInstance125,
	&faxCngInstance126,
	&faxCngInstance127,
	&faxCngInstance128,
	&faxCngInstance129,
	&faxCngInstance130,
	&faxCngInstance131,
	&faxCngInstance132,
	&faxCngInstance133,
	&faxCngInstance134,
	&faxCngInstance135,
	&faxCngInstance136,
	&faxCngInstance137,
	&faxCngInstance138,
	&faxCngInstance139,
	&faxCngInstance140,
	&faxCngInstance141,
	&faxCngInstance142,
	&faxCngInstance143,
	&faxCngInstance144,
	&faxCngInstance145,
	&faxCngInstance146,
	&faxCngInstance147,
	&faxCngInstance148,
	&faxCngInstance149,
	&faxCngInstance150,
	&faxCngInstance151,
	&faxCngInstance152,
	&faxCngInstance153,
	&faxCngInstance154,
	&faxCngInstance155,
	&faxCngInstance156,
	&faxCngInstance157,
	&faxCngInstance158,
	&faxCngInstance159
};
//}

ADT_UInt16 ArbToneDetectorsAvail = ARB_DETECT_INSTANCES;
ADT_UInt16 ArbToneDetectorInUse;
#pragma DATA_SECTION (ArbToneDetectorsAvail, "STUB_DATA_SECT")
#pragma DATA_SECTION (ArbToneDetectorInUse,  "STUB_DATA_SECT")

ADT_UInt16 activeArbToneCfg = VOID_INDEX;
ADT_UInt16 numArbToneCfgs = NUM_ARB_TONE_CONFIGS;
ADT_UInt16 ArbToneConfigured;
#pragma DATA_SECTION (activeArbToneCfg,  "STUB_DATA_SECT")
#pragma DATA_SECTION (numArbToneCfgs,    "STUB_DATA_SECT")
#pragma DATA_SECTION (ArbToneConfigured, "STUB_DATA_SECT")

//{     TRDetectInstance_t   ToneRlyDetInstance STRUCTURES to CHAN_INST_DATA:ToneDetect

TRDetectInstance_t ToneRlyDetInstance0;
#pragma DATA_SECTION (ToneRlyDetInstance0, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance0, 128)

static TRDetectInstance_t ToneRlyDetInstance1;
#pragma DATA_SECTION (ToneRlyDetInstance1, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance1, 128)

static TRDetectInstance_t ToneRlyDetInstance2;
#pragma DATA_SECTION (ToneRlyDetInstance2, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance2, 128)

static TRDetectInstance_t ToneRlyDetInstance3;
#pragma DATA_SECTION (ToneRlyDetInstance3, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance3, 128)

static TRDetectInstance_t ToneRlyDetInstance4;
#pragma DATA_SECTION (ToneRlyDetInstance4, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance4, 128)

static TRDetectInstance_t ToneRlyDetInstance5;
#pragma DATA_SECTION (ToneRlyDetInstance5, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance5, 128)

static TRDetectInstance_t ToneRlyDetInstance6;
#pragma DATA_SECTION (ToneRlyDetInstance6, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance6, 128)

static TRDetectInstance_t ToneRlyDetInstance7;
#pragma DATA_SECTION (ToneRlyDetInstance7, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance7, 128)

static TRDetectInstance_t ToneRlyDetInstance8;
#pragma DATA_SECTION (ToneRlyDetInstance8, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance8, 128)

static TRDetectInstance_t ToneRlyDetInstance9;
#pragma DATA_SECTION (ToneRlyDetInstance9, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance9, 128)

static TRDetectInstance_t ToneRlyDetInstance10;
#pragma DATA_SECTION (ToneRlyDetInstance10, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance10, 128)

static TRDetectInstance_t ToneRlyDetInstance11;
#pragma DATA_SECTION (ToneRlyDetInstance11, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance11, 128)

static TRDetectInstance_t ToneRlyDetInstance12;
#pragma DATA_SECTION (ToneRlyDetInstance12, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance12, 128)

static TRDetectInstance_t ToneRlyDetInstance13;
#pragma DATA_SECTION (ToneRlyDetInstance13, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance13, 128)

static TRDetectInstance_t ToneRlyDetInstance14;
#pragma DATA_SECTION (ToneRlyDetInstance14, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance14, 128)

static TRDetectInstance_t ToneRlyDetInstance15;
#pragma DATA_SECTION (ToneRlyDetInstance15, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance15, 128)

static TRDetectInstance_t ToneRlyDetInstance16;
#pragma DATA_SECTION (ToneRlyDetInstance16, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance16, 128)

static TRDetectInstance_t ToneRlyDetInstance17;
#pragma DATA_SECTION (ToneRlyDetInstance17, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance17, 128)

static TRDetectInstance_t ToneRlyDetInstance18;
#pragma DATA_SECTION (ToneRlyDetInstance18, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance18, 128)

static TRDetectInstance_t ToneRlyDetInstance19;
#pragma DATA_SECTION (ToneRlyDetInstance19, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance19, 128)

static TRDetectInstance_t ToneRlyDetInstance20;
#pragma DATA_SECTION (ToneRlyDetInstance20, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance20, 128)

static TRDetectInstance_t ToneRlyDetInstance21;
#pragma DATA_SECTION (ToneRlyDetInstance21, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance21, 128)

static TRDetectInstance_t ToneRlyDetInstance22;
#pragma DATA_SECTION (ToneRlyDetInstance22, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance22, 128)

static TRDetectInstance_t ToneRlyDetInstance23;
#pragma DATA_SECTION (ToneRlyDetInstance23, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance23, 128)

static TRDetectInstance_t ToneRlyDetInstance24;
#pragma DATA_SECTION (ToneRlyDetInstance24, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance24, 128)

static TRDetectInstance_t ToneRlyDetInstance25;
#pragma DATA_SECTION (ToneRlyDetInstance25, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance25, 128)

static TRDetectInstance_t ToneRlyDetInstance26;
#pragma DATA_SECTION (ToneRlyDetInstance26, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance26, 128)

static TRDetectInstance_t ToneRlyDetInstance27;
#pragma DATA_SECTION (ToneRlyDetInstance27, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance27, 128)

static TRDetectInstance_t ToneRlyDetInstance28;
#pragma DATA_SECTION (ToneRlyDetInstance28, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance28, 128)

static TRDetectInstance_t ToneRlyDetInstance29;
#pragma DATA_SECTION (ToneRlyDetInstance29, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance29, 128)

static TRDetectInstance_t ToneRlyDetInstance30;
#pragma DATA_SECTION (ToneRlyDetInstance30, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance30, 128)

static TRDetectInstance_t ToneRlyDetInstance31;
#pragma DATA_SECTION (ToneRlyDetInstance31, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance31, 128)

static TRDetectInstance_t ToneRlyDetInstance32;
#pragma DATA_SECTION (ToneRlyDetInstance32, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance32, 128)

static TRDetectInstance_t ToneRlyDetInstance33;
#pragma DATA_SECTION (ToneRlyDetInstance33, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance33, 128)

static TRDetectInstance_t ToneRlyDetInstance34;
#pragma DATA_SECTION (ToneRlyDetInstance34, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance34, 128)

static TRDetectInstance_t ToneRlyDetInstance35;
#pragma DATA_SECTION (ToneRlyDetInstance35, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance35, 128)

static TRDetectInstance_t ToneRlyDetInstance36;
#pragma DATA_SECTION (ToneRlyDetInstance36, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance36, 128)

static TRDetectInstance_t ToneRlyDetInstance37;
#pragma DATA_SECTION (ToneRlyDetInstance37, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance37, 128)

static TRDetectInstance_t ToneRlyDetInstance38;
#pragma DATA_SECTION (ToneRlyDetInstance38, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance38, 128)

static TRDetectInstance_t ToneRlyDetInstance39;
#pragma DATA_SECTION (ToneRlyDetInstance39, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance39, 128)

static TRDetectInstance_t ToneRlyDetInstance40;
#pragma DATA_SECTION (ToneRlyDetInstance40, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance40, 128)

static TRDetectInstance_t ToneRlyDetInstance41;
#pragma DATA_SECTION (ToneRlyDetInstance41, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance41, 128)

static TRDetectInstance_t ToneRlyDetInstance42;
#pragma DATA_SECTION (ToneRlyDetInstance42, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance42, 128)

static TRDetectInstance_t ToneRlyDetInstance43;
#pragma DATA_SECTION (ToneRlyDetInstance43, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance43, 128)

static TRDetectInstance_t ToneRlyDetInstance44;
#pragma DATA_SECTION (ToneRlyDetInstance44, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance44, 128)

static TRDetectInstance_t ToneRlyDetInstance45;
#pragma DATA_SECTION (ToneRlyDetInstance45, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance45, 128)

static TRDetectInstance_t ToneRlyDetInstance46;
#pragma DATA_SECTION (ToneRlyDetInstance46, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance46, 128)

static TRDetectInstance_t ToneRlyDetInstance47;
#pragma DATA_SECTION (ToneRlyDetInstance47, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance47, 128)

static TRDetectInstance_t ToneRlyDetInstance48;
#pragma DATA_SECTION (ToneRlyDetInstance48, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance48, 128)

static TRDetectInstance_t ToneRlyDetInstance49;
#pragma DATA_SECTION (ToneRlyDetInstance49, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance49, 128)

static TRDetectInstance_t ToneRlyDetInstance50;
#pragma DATA_SECTION (ToneRlyDetInstance50, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance50, 128)

static TRDetectInstance_t ToneRlyDetInstance51;
#pragma DATA_SECTION (ToneRlyDetInstance51, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance51, 128)

static TRDetectInstance_t ToneRlyDetInstance52;
#pragma DATA_SECTION (ToneRlyDetInstance52, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance52, 128)

static TRDetectInstance_t ToneRlyDetInstance53;
#pragma DATA_SECTION (ToneRlyDetInstance53, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance53, 128)

static TRDetectInstance_t ToneRlyDetInstance54;
#pragma DATA_SECTION (ToneRlyDetInstance54, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance54, 128)

static TRDetectInstance_t ToneRlyDetInstance55;
#pragma DATA_SECTION (ToneRlyDetInstance55, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance55, 128)

static TRDetectInstance_t ToneRlyDetInstance56;
#pragma DATA_SECTION (ToneRlyDetInstance56, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance56, 128)

static TRDetectInstance_t ToneRlyDetInstance57;
#pragma DATA_SECTION (ToneRlyDetInstance57, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance57, 128)

static TRDetectInstance_t ToneRlyDetInstance58;
#pragma DATA_SECTION (ToneRlyDetInstance58, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance58, 128)

static TRDetectInstance_t ToneRlyDetInstance59;
#pragma DATA_SECTION (ToneRlyDetInstance59, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance59, 128)

static TRDetectInstance_t ToneRlyDetInstance60;
#pragma DATA_SECTION (ToneRlyDetInstance60, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance60, 128)

static TRDetectInstance_t ToneRlyDetInstance61;
#pragma DATA_SECTION (ToneRlyDetInstance61, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance61, 128)

static TRDetectInstance_t ToneRlyDetInstance62;
#pragma DATA_SECTION (ToneRlyDetInstance62, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance62, 128)

static TRDetectInstance_t ToneRlyDetInstance63;
#pragma DATA_SECTION (ToneRlyDetInstance63, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance63, 128)

static TRDetectInstance_t ToneRlyDetInstance64;
#pragma DATA_SECTION (ToneRlyDetInstance64, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance64, 128)

static TRDetectInstance_t ToneRlyDetInstance65;
#pragma DATA_SECTION (ToneRlyDetInstance65, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance65, 128)

static TRDetectInstance_t ToneRlyDetInstance66;
#pragma DATA_SECTION (ToneRlyDetInstance66, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance66, 128)

static TRDetectInstance_t ToneRlyDetInstance67;
#pragma DATA_SECTION (ToneRlyDetInstance67, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance67, 128)

static TRDetectInstance_t ToneRlyDetInstance68;
#pragma DATA_SECTION (ToneRlyDetInstance68, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance68, 128)

static TRDetectInstance_t ToneRlyDetInstance69;
#pragma DATA_SECTION (ToneRlyDetInstance69, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance69, 128)

static TRDetectInstance_t ToneRlyDetInstance70;
#pragma DATA_SECTION (ToneRlyDetInstance70, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance70, 128)

static TRDetectInstance_t ToneRlyDetInstance71;
#pragma DATA_SECTION (ToneRlyDetInstance71, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance71, 128)

static TRDetectInstance_t ToneRlyDetInstance72;
#pragma DATA_SECTION (ToneRlyDetInstance72, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance72, 128)

static TRDetectInstance_t ToneRlyDetInstance73;
#pragma DATA_SECTION (ToneRlyDetInstance73, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance73, 128)

static TRDetectInstance_t ToneRlyDetInstance74;
#pragma DATA_SECTION (ToneRlyDetInstance74, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance74, 128)

static TRDetectInstance_t ToneRlyDetInstance75;
#pragma DATA_SECTION (ToneRlyDetInstance75, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance75, 128)

static TRDetectInstance_t ToneRlyDetInstance76;
#pragma DATA_SECTION (ToneRlyDetInstance76, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance76, 128)

static TRDetectInstance_t ToneRlyDetInstance77;
#pragma DATA_SECTION (ToneRlyDetInstance77, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance77, 128)

static TRDetectInstance_t ToneRlyDetInstance78;
#pragma DATA_SECTION (ToneRlyDetInstance78, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance78, 128)

static TRDetectInstance_t ToneRlyDetInstance79;
#pragma DATA_SECTION (ToneRlyDetInstance79, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance79, 128)

static TRDetectInstance_t ToneRlyDetInstance80;
#pragma DATA_SECTION (ToneRlyDetInstance80, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance80, 128)

static TRDetectInstance_t ToneRlyDetInstance81;
#pragma DATA_SECTION (ToneRlyDetInstance81, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance81, 128)

static TRDetectInstance_t ToneRlyDetInstance82;
#pragma DATA_SECTION (ToneRlyDetInstance82, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance82, 128)

static TRDetectInstance_t ToneRlyDetInstance83;
#pragma DATA_SECTION (ToneRlyDetInstance83, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance83, 128)

static TRDetectInstance_t ToneRlyDetInstance84;
#pragma DATA_SECTION (ToneRlyDetInstance84, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance84, 128)

static TRDetectInstance_t ToneRlyDetInstance85;
#pragma DATA_SECTION (ToneRlyDetInstance85, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance85, 128)

static TRDetectInstance_t ToneRlyDetInstance86;
#pragma DATA_SECTION (ToneRlyDetInstance86, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance86, 128)

static TRDetectInstance_t ToneRlyDetInstance87;
#pragma DATA_SECTION (ToneRlyDetInstance87, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance87, 128)

static TRDetectInstance_t ToneRlyDetInstance88;
#pragma DATA_SECTION (ToneRlyDetInstance88, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance88, 128)

static TRDetectInstance_t ToneRlyDetInstance89;
#pragma DATA_SECTION (ToneRlyDetInstance89, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance89, 128)

static TRDetectInstance_t ToneRlyDetInstance90;
#pragma DATA_SECTION (ToneRlyDetInstance90, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance90, 128)

static TRDetectInstance_t ToneRlyDetInstance91;
#pragma DATA_SECTION (ToneRlyDetInstance91, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance91, 128)

static TRDetectInstance_t ToneRlyDetInstance92;
#pragma DATA_SECTION (ToneRlyDetInstance92, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance92, 128)

static TRDetectInstance_t ToneRlyDetInstance93;
#pragma DATA_SECTION (ToneRlyDetInstance93, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance93, 128)

static TRDetectInstance_t ToneRlyDetInstance94;
#pragma DATA_SECTION (ToneRlyDetInstance94, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance94, 128)

static TRDetectInstance_t ToneRlyDetInstance95;
#pragma DATA_SECTION (ToneRlyDetInstance95, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance95, 128)

static TRDetectInstance_t ToneRlyDetInstance96;
#pragma DATA_SECTION (ToneRlyDetInstance96, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance96, 128)

static TRDetectInstance_t ToneRlyDetInstance97;
#pragma DATA_SECTION (ToneRlyDetInstance97, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance97, 128)

static TRDetectInstance_t ToneRlyDetInstance98;
#pragma DATA_SECTION (ToneRlyDetInstance98, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance98, 128)

static TRDetectInstance_t ToneRlyDetInstance99;
#pragma DATA_SECTION (ToneRlyDetInstance99, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance99, 128)

static TRDetectInstance_t ToneRlyDetInstance100;
#pragma DATA_SECTION (ToneRlyDetInstance100, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance100, 128)

static TRDetectInstance_t ToneRlyDetInstance101;
#pragma DATA_SECTION (ToneRlyDetInstance101, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance101, 128)

static TRDetectInstance_t ToneRlyDetInstance102;
#pragma DATA_SECTION (ToneRlyDetInstance102, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance102, 128)

static TRDetectInstance_t ToneRlyDetInstance103;
#pragma DATA_SECTION (ToneRlyDetInstance103, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance103, 128)

static TRDetectInstance_t ToneRlyDetInstance104;
#pragma DATA_SECTION (ToneRlyDetInstance104, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance104, 128)

static TRDetectInstance_t ToneRlyDetInstance105;
#pragma DATA_SECTION (ToneRlyDetInstance105, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance105, 128)

static TRDetectInstance_t ToneRlyDetInstance106;
#pragma DATA_SECTION (ToneRlyDetInstance106, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance106, 128)

static TRDetectInstance_t ToneRlyDetInstance107;
#pragma DATA_SECTION (ToneRlyDetInstance107, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance107, 128)

static TRDetectInstance_t ToneRlyDetInstance108;
#pragma DATA_SECTION (ToneRlyDetInstance108, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance108, 128)

static TRDetectInstance_t ToneRlyDetInstance109;
#pragma DATA_SECTION (ToneRlyDetInstance109, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance109, 128)

static TRDetectInstance_t ToneRlyDetInstance110;
#pragma DATA_SECTION (ToneRlyDetInstance110, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance110, 128)

static TRDetectInstance_t ToneRlyDetInstance111;
#pragma DATA_SECTION (ToneRlyDetInstance111, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance111, 128)

static TRDetectInstance_t ToneRlyDetInstance112;
#pragma DATA_SECTION (ToneRlyDetInstance112, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance112, 128)

static TRDetectInstance_t ToneRlyDetInstance113;
#pragma DATA_SECTION (ToneRlyDetInstance113, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance113, 128)

static TRDetectInstance_t ToneRlyDetInstance114;
#pragma DATA_SECTION (ToneRlyDetInstance114, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance114, 128)

static TRDetectInstance_t ToneRlyDetInstance115;
#pragma DATA_SECTION (ToneRlyDetInstance115, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance115, 128)

static TRDetectInstance_t ToneRlyDetInstance116;
#pragma DATA_SECTION (ToneRlyDetInstance116, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance116, 128)

static TRDetectInstance_t ToneRlyDetInstance117;
#pragma DATA_SECTION (ToneRlyDetInstance117, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance117, 128)

static TRDetectInstance_t ToneRlyDetInstance118;
#pragma DATA_SECTION (ToneRlyDetInstance118, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance118, 128)

static TRDetectInstance_t ToneRlyDetInstance119;
#pragma DATA_SECTION (ToneRlyDetInstance119, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance119, 128)

static TRDetectInstance_t ToneRlyDetInstance120;
#pragma DATA_SECTION (ToneRlyDetInstance120, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance120, 128)

static TRDetectInstance_t ToneRlyDetInstance121;
#pragma DATA_SECTION (ToneRlyDetInstance121, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance121, 128)

static TRDetectInstance_t ToneRlyDetInstance122;
#pragma DATA_SECTION (ToneRlyDetInstance122, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance122, 128)

static TRDetectInstance_t ToneRlyDetInstance123;
#pragma DATA_SECTION (ToneRlyDetInstance123, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance123, 128)

static TRDetectInstance_t ToneRlyDetInstance124;
#pragma DATA_SECTION (ToneRlyDetInstance124, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance124, 128)

static TRDetectInstance_t ToneRlyDetInstance125;
#pragma DATA_SECTION (ToneRlyDetInstance125, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance125, 128)

static TRDetectInstance_t ToneRlyDetInstance126;
#pragma DATA_SECTION (ToneRlyDetInstance126, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance126, 128)

static TRDetectInstance_t ToneRlyDetInstance127;
#pragma DATA_SECTION (ToneRlyDetInstance127, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance127, 128)

static TRDetectInstance_t ToneRlyDetInstance128;
#pragma DATA_SECTION (ToneRlyDetInstance128, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance128, 128)

static TRDetectInstance_t ToneRlyDetInstance129;
#pragma DATA_SECTION (ToneRlyDetInstance129, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance129, 128)

static TRDetectInstance_t ToneRlyDetInstance130;
#pragma DATA_SECTION (ToneRlyDetInstance130, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance130, 128)

static TRDetectInstance_t ToneRlyDetInstance131;
#pragma DATA_SECTION (ToneRlyDetInstance131, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance131, 128)

static TRDetectInstance_t ToneRlyDetInstance132;
#pragma DATA_SECTION (ToneRlyDetInstance132, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance132, 128)

static TRDetectInstance_t ToneRlyDetInstance133;
#pragma DATA_SECTION (ToneRlyDetInstance133, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance133, 128)

static TRDetectInstance_t ToneRlyDetInstance134;
#pragma DATA_SECTION (ToneRlyDetInstance134, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance134, 128)

static TRDetectInstance_t ToneRlyDetInstance135;
#pragma DATA_SECTION (ToneRlyDetInstance135, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance135, 128)

static TRDetectInstance_t ToneRlyDetInstance136;
#pragma DATA_SECTION (ToneRlyDetInstance136, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance136, 128)

static TRDetectInstance_t ToneRlyDetInstance137;
#pragma DATA_SECTION (ToneRlyDetInstance137, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance137, 128)

static TRDetectInstance_t ToneRlyDetInstance138;
#pragma DATA_SECTION (ToneRlyDetInstance138, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance138, 128)

static TRDetectInstance_t ToneRlyDetInstance139;
#pragma DATA_SECTION (ToneRlyDetInstance139, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance139, 128)

static TRDetectInstance_t ToneRlyDetInstance140;
#pragma DATA_SECTION (ToneRlyDetInstance140, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance140, 128)

static TRDetectInstance_t ToneRlyDetInstance141;
#pragma DATA_SECTION (ToneRlyDetInstance141, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance141, 128)

static TRDetectInstance_t ToneRlyDetInstance142;
#pragma DATA_SECTION (ToneRlyDetInstance142, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance142, 128)

static TRDetectInstance_t ToneRlyDetInstance143;
#pragma DATA_SECTION (ToneRlyDetInstance143, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance143, 128)

static TRDetectInstance_t ToneRlyDetInstance144;
#pragma DATA_SECTION (ToneRlyDetInstance144, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance144, 128)

static TRDetectInstance_t ToneRlyDetInstance145;
#pragma DATA_SECTION (ToneRlyDetInstance145, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance145, 128)

static TRDetectInstance_t ToneRlyDetInstance146;
#pragma DATA_SECTION (ToneRlyDetInstance146, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance146, 128)

static TRDetectInstance_t ToneRlyDetInstance147;
#pragma DATA_SECTION (ToneRlyDetInstance147, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance147, 128)

static TRDetectInstance_t ToneRlyDetInstance148;
#pragma DATA_SECTION (ToneRlyDetInstance148, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance148, 128)

static TRDetectInstance_t ToneRlyDetInstance149;
#pragma DATA_SECTION (ToneRlyDetInstance149, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance149, 128)

static TRDetectInstance_t ToneRlyDetInstance150;
#pragma DATA_SECTION (ToneRlyDetInstance150, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance150, 128)

static TRDetectInstance_t ToneRlyDetInstance151;
#pragma DATA_SECTION (ToneRlyDetInstance151, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance151, 128)

static TRDetectInstance_t ToneRlyDetInstance152;
#pragma DATA_SECTION (ToneRlyDetInstance152, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance152, 128)

static TRDetectInstance_t ToneRlyDetInstance153;
#pragma DATA_SECTION (ToneRlyDetInstance153, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance153, 128)

static TRDetectInstance_t ToneRlyDetInstance154;
#pragma DATA_SECTION (ToneRlyDetInstance154, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance154, 128)

static TRDetectInstance_t ToneRlyDetInstance155;
#pragma DATA_SECTION (ToneRlyDetInstance155, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance155, 128)

static TRDetectInstance_t ToneRlyDetInstance156;
#pragma DATA_SECTION (ToneRlyDetInstance156, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance156, 128)

static TRDetectInstance_t ToneRlyDetInstance157;
#pragma DATA_SECTION (ToneRlyDetInstance157, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance157, 128)

static TRDetectInstance_t ToneRlyDetInstance158;
#pragma DATA_SECTION (ToneRlyDetInstance158, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance158, 128)

static TRDetectInstance_t ToneRlyDetInstance159;
#pragma DATA_SECTION (ToneRlyDetInstance159, "CHAN_INST_DATA:ToneDetect")
#pragma DATA_ALIGN   (ToneRlyDetInstance159, 128)

TRDetectInstance_t* const ToneRlyDetInstance[] = {
	&ToneRlyDetInstance0,
	&ToneRlyDetInstance1,
	&ToneRlyDetInstance2,
	&ToneRlyDetInstance3,
	&ToneRlyDetInstance4,
	&ToneRlyDetInstance5,
	&ToneRlyDetInstance6,
	&ToneRlyDetInstance7,
	&ToneRlyDetInstance8,
	&ToneRlyDetInstance9,
	&ToneRlyDetInstance10,
	&ToneRlyDetInstance11,
	&ToneRlyDetInstance12,
	&ToneRlyDetInstance13,
	&ToneRlyDetInstance14,
	&ToneRlyDetInstance15,
	&ToneRlyDetInstance16,
	&ToneRlyDetInstance17,
	&ToneRlyDetInstance18,
	&ToneRlyDetInstance19,
	&ToneRlyDetInstance20,
	&ToneRlyDetInstance21,
	&ToneRlyDetInstance22,
	&ToneRlyDetInstance23,
	&ToneRlyDetInstance24,
	&ToneRlyDetInstance25,
	&ToneRlyDetInstance26,
	&ToneRlyDetInstance27,
	&ToneRlyDetInstance28,
	&ToneRlyDetInstance29,
	&ToneRlyDetInstance30,
	&ToneRlyDetInstance31,
	&ToneRlyDetInstance32,
	&ToneRlyDetInstance33,
	&ToneRlyDetInstance34,
	&ToneRlyDetInstance35,
	&ToneRlyDetInstance36,
	&ToneRlyDetInstance37,
	&ToneRlyDetInstance38,
	&ToneRlyDetInstance39,
	&ToneRlyDetInstance40,
	&ToneRlyDetInstance41,
	&ToneRlyDetInstance42,
	&ToneRlyDetInstance43,
	&ToneRlyDetInstance44,
	&ToneRlyDetInstance45,
	&ToneRlyDetInstance46,
	&ToneRlyDetInstance47,
	&ToneRlyDetInstance48,
	&ToneRlyDetInstance49,
	&ToneRlyDetInstance50,
	&ToneRlyDetInstance51,
	&ToneRlyDetInstance52,
	&ToneRlyDetInstance53,
	&ToneRlyDetInstance54,
	&ToneRlyDetInstance55,
	&ToneRlyDetInstance56,
	&ToneRlyDetInstance57,
	&ToneRlyDetInstance58,
	&ToneRlyDetInstance59,
	&ToneRlyDetInstance60,
	&ToneRlyDetInstance61,
	&ToneRlyDetInstance62,
	&ToneRlyDetInstance63,
	&ToneRlyDetInstance64,
	&ToneRlyDetInstance65,
	&ToneRlyDetInstance66,
	&ToneRlyDetInstance67,
	&ToneRlyDetInstance68,
	&ToneRlyDetInstance69,
	&ToneRlyDetInstance70,
	&ToneRlyDetInstance71,
	&ToneRlyDetInstance72,
	&ToneRlyDetInstance73,
	&ToneRlyDetInstance74,
	&ToneRlyDetInstance75,
	&ToneRlyDetInstance76,
	&ToneRlyDetInstance77,
	&ToneRlyDetInstance78,
	&ToneRlyDetInstance79,
	&ToneRlyDetInstance80,
	&ToneRlyDetInstance81,
	&ToneRlyDetInstance82,
	&ToneRlyDetInstance83,
	&ToneRlyDetInstance84,
	&ToneRlyDetInstance85,
	&ToneRlyDetInstance86,
	&ToneRlyDetInstance87,
	&ToneRlyDetInstance88,
	&ToneRlyDetInstance89,
	&ToneRlyDetInstance90,
	&ToneRlyDetInstance91,
	&ToneRlyDetInstance92,
	&ToneRlyDetInstance93,
	&ToneRlyDetInstance94,
	&ToneRlyDetInstance95,
	&ToneRlyDetInstance96,
	&ToneRlyDetInstance97,
	&ToneRlyDetInstance98,
	&ToneRlyDetInstance99,
	&ToneRlyDetInstance100,
	&ToneRlyDetInstance101,
	&ToneRlyDetInstance102,
	&ToneRlyDetInstance103,
	&ToneRlyDetInstance104,
	&ToneRlyDetInstance105,
	&ToneRlyDetInstance106,
	&ToneRlyDetInstance107,
	&ToneRlyDetInstance108,
	&ToneRlyDetInstance109,
	&ToneRlyDetInstance110,
	&ToneRlyDetInstance111,
	&ToneRlyDetInstance112,
	&ToneRlyDetInstance113,
	&ToneRlyDetInstance114,
	&ToneRlyDetInstance115,
	&ToneRlyDetInstance116,
	&ToneRlyDetInstance117,
	&ToneRlyDetInstance118,
	&ToneRlyDetInstance119,
	&ToneRlyDetInstance120,
	&ToneRlyDetInstance121,
	&ToneRlyDetInstance122,
	&ToneRlyDetInstance123,
	&ToneRlyDetInstance124,
	&ToneRlyDetInstance125,
	&ToneRlyDetInstance126,
	&ToneRlyDetInstance127,
	&ToneRlyDetInstance128,
	&ToneRlyDetInstance129,
	&ToneRlyDetInstance130,
	&ToneRlyDetInstance131,
	&ToneRlyDetInstance132,
	&ToneRlyDetInstance133,
	&ToneRlyDetInstance134,
	&ToneRlyDetInstance135,
	&ToneRlyDetInstance136,
	&ToneRlyDetInstance137,
	&ToneRlyDetInstance138,
	&ToneRlyDetInstance139,
	&ToneRlyDetInstance140,
	&ToneRlyDetInstance141,
	&ToneRlyDetInstance142,
	&ToneRlyDetInstance143,
	&ToneRlyDetInstance144,
	&ToneRlyDetInstance145,
	&ToneRlyDetInstance146,
	&ToneRlyDetInstance147,
	&ToneRlyDetInstance148,
	&ToneRlyDetInstance149,
	&ToneRlyDetInstance150,
	&ToneRlyDetInstance151,
	&ToneRlyDetInstance152,
	&ToneRlyDetInstance153,
	&ToneRlyDetInstance154,
	&ToneRlyDetInstance155,
	&ToneRlyDetInstance156,
	&ToneRlyDetInstance157,
	&ToneRlyDetInstance158,
	&ToneRlyDetInstance159
};
//}

ADT_UInt16   numToneGenChansAvail = MAX_NUM_TONEGEN_CHANNELS;
ADT_UInt16   toneGenChanInUse[MAX_NUM_TONEGEN_CHANNELS];
#pragma DATA_SECTION (numToneGenChansAvail, "POOL_ALLOC")
#pragma DATA_SECTION (toneGenChanInUse,     "POOL_ALLOC")
//{           TGInstance_t      ToneGenInstance STRUCTURES to CHAN_INST_DATA:ToneGen

TGInstance_t ToneGenInstance0;
#pragma DATA_SECTION (ToneGenInstance0, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance0, 128)

static TGInstance_t ToneGenInstance1;
#pragma DATA_SECTION (ToneGenInstance1, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance1, 128)

static TGInstance_t ToneGenInstance2;
#pragma DATA_SECTION (ToneGenInstance2, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance2, 128)

static TGInstance_t ToneGenInstance3;
#pragma DATA_SECTION (ToneGenInstance3, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance3, 128)

static TGInstance_t ToneGenInstance4;
#pragma DATA_SECTION (ToneGenInstance4, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance4, 128)

static TGInstance_t ToneGenInstance5;
#pragma DATA_SECTION (ToneGenInstance5, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance5, 128)

static TGInstance_t ToneGenInstance6;
#pragma DATA_SECTION (ToneGenInstance6, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance6, 128)

static TGInstance_t ToneGenInstance7;
#pragma DATA_SECTION (ToneGenInstance7, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance7, 128)

static TGInstance_t ToneGenInstance8;
#pragma DATA_SECTION (ToneGenInstance8, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance8, 128)

static TGInstance_t ToneGenInstance9;
#pragma DATA_SECTION (ToneGenInstance9, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance9, 128)

static TGInstance_t ToneGenInstance10;
#pragma DATA_SECTION (ToneGenInstance10, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance10, 128)

static TGInstance_t ToneGenInstance11;
#pragma DATA_SECTION (ToneGenInstance11, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance11, 128)

static TGInstance_t ToneGenInstance12;
#pragma DATA_SECTION (ToneGenInstance12, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance12, 128)

static TGInstance_t ToneGenInstance13;
#pragma DATA_SECTION (ToneGenInstance13, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance13, 128)

static TGInstance_t ToneGenInstance14;
#pragma DATA_SECTION (ToneGenInstance14, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance14, 128)

static TGInstance_t ToneGenInstance15;
#pragma DATA_SECTION (ToneGenInstance15, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance15, 128)

static TGInstance_t ToneGenInstance16;
#pragma DATA_SECTION (ToneGenInstance16, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance16, 128)

static TGInstance_t ToneGenInstance17;
#pragma DATA_SECTION (ToneGenInstance17, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance17, 128)

static TGInstance_t ToneGenInstance18;
#pragma DATA_SECTION (ToneGenInstance18, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance18, 128)

static TGInstance_t ToneGenInstance19;
#pragma DATA_SECTION (ToneGenInstance19, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance19, 128)

static TGInstance_t ToneGenInstance20;
#pragma DATA_SECTION (ToneGenInstance20, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance20, 128)

static TGInstance_t ToneGenInstance21;
#pragma DATA_SECTION (ToneGenInstance21, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance21, 128)

static TGInstance_t ToneGenInstance22;
#pragma DATA_SECTION (ToneGenInstance22, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance22, 128)

static TGInstance_t ToneGenInstance23;
#pragma DATA_SECTION (ToneGenInstance23, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance23, 128)

static TGInstance_t ToneGenInstance24;
#pragma DATA_SECTION (ToneGenInstance24, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance24, 128)

static TGInstance_t ToneGenInstance25;
#pragma DATA_SECTION (ToneGenInstance25, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance25, 128)

static TGInstance_t ToneGenInstance26;
#pragma DATA_SECTION (ToneGenInstance26, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance26, 128)

static TGInstance_t ToneGenInstance27;
#pragma DATA_SECTION (ToneGenInstance27, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance27, 128)

static TGInstance_t ToneGenInstance28;
#pragma DATA_SECTION (ToneGenInstance28, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance28, 128)

static TGInstance_t ToneGenInstance29;
#pragma DATA_SECTION (ToneGenInstance29, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance29, 128)

static TGInstance_t ToneGenInstance30;
#pragma DATA_SECTION (ToneGenInstance30, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance30, 128)

static TGInstance_t ToneGenInstance31;
#pragma DATA_SECTION (ToneGenInstance31, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance31, 128)

static TGInstance_t ToneGenInstance32;
#pragma DATA_SECTION (ToneGenInstance32, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance32, 128)

static TGInstance_t ToneGenInstance33;
#pragma DATA_SECTION (ToneGenInstance33, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance33, 128)

static TGInstance_t ToneGenInstance34;
#pragma DATA_SECTION (ToneGenInstance34, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance34, 128)

static TGInstance_t ToneGenInstance35;
#pragma DATA_SECTION (ToneGenInstance35, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance35, 128)

static TGInstance_t ToneGenInstance36;
#pragma DATA_SECTION (ToneGenInstance36, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance36, 128)

static TGInstance_t ToneGenInstance37;
#pragma DATA_SECTION (ToneGenInstance37, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance37, 128)

static TGInstance_t ToneGenInstance38;
#pragma DATA_SECTION (ToneGenInstance38, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance38, 128)

static TGInstance_t ToneGenInstance39;
#pragma DATA_SECTION (ToneGenInstance39, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance39, 128)

static TGInstance_t ToneGenInstance40;
#pragma DATA_SECTION (ToneGenInstance40, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance40, 128)

static TGInstance_t ToneGenInstance41;
#pragma DATA_SECTION (ToneGenInstance41, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance41, 128)

static TGInstance_t ToneGenInstance42;
#pragma DATA_SECTION (ToneGenInstance42, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance42, 128)

static TGInstance_t ToneGenInstance43;
#pragma DATA_SECTION (ToneGenInstance43, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance43, 128)

static TGInstance_t ToneGenInstance44;
#pragma DATA_SECTION (ToneGenInstance44, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance44, 128)

static TGInstance_t ToneGenInstance45;
#pragma DATA_SECTION (ToneGenInstance45, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance45, 128)

static TGInstance_t ToneGenInstance46;
#pragma DATA_SECTION (ToneGenInstance46, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance46, 128)

static TGInstance_t ToneGenInstance47;
#pragma DATA_SECTION (ToneGenInstance47, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance47, 128)

static TGInstance_t ToneGenInstance48;
#pragma DATA_SECTION (ToneGenInstance48, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance48, 128)

static TGInstance_t ToneGenInstance49;
#pragma DATA_SECTION (ToneGenInstance49, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance49, 128)

static TGInstance_t ToneGenInstance50;
#pragma DATA_SECTION (ToneGenInstance50, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance50, 128)

static TGInstance_t ToneGenInstance51;
#pragma DATA_SECTION (ToneGenInstance51, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance51, 128)

static TGInstance_t ToneGenInstance52;
#pragma DATA_SECTION (ToneGenInstance52, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance52, 128)

static TGInstance_t ToneGenInstance53;
#pragma DATA_SECTION (ToneGenInstance53, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance53, 128)

static TGInstance_t ToneGenInstance54;
#pragma DATA_SECTION (ToneGenInstance54, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance54, 128)

static TGInstance_t ToneGenInstance55;
#pragma DATA_SECTION (ToneGenInstance55, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance55, 128)

static TGInstance_t ToneGenInstance56;
#pragma DATA_SECTION (ToneGenInstance56, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance56, 128)

static TGInstance_t ToneGenInstance57;
#pragma DATA_SECTION (ToneGenInstance57, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance57, 128)

static TGInstance_t ToneGenInstance58;
#pragma DATA_SECTION (ToneGenInstance58, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance58, 128)

static TGInstance_t ToneGenInstance59;
#pragma DATA_SECTION (ToneGenInstance59, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance59, 128)

static TGInstance_t ToneGenInstance60;
#pragma DATA_SECTION (ToneGenInstance60, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance60, 128)

static TGInstance_t ToneGenInstance61;
#pragma DATA_SECTION (ToneGenInstance61, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance61, 128)

static TGInstance_t ToneGenInstance62;
#pragma DATA_SECTION (ToneGenInstance62, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance62, 128)

static TGInstance_t ToneGenInstance63;
#pragma DATA_SECTION (ToneGenInstance63, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance63, 128)

static TGInstance_t ToneGenInstance64;
#pragma DATA_SECTION (ToneGenInstance64, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance64, 128)

static TGInstance_t ToneGenInstance65;
#pragma DATA_SECTION (ToneGenInstance65, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance65, 128)

static TGInstance_t ToneGenInstance66;
#pragma DATA_SECTION (ToneGenInstance66, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance66, 128)

static TGInstance_t ToneGenInstance67;
#pragma DATA_SECTION (ToneGenInstance67, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance67, 128)

static TGInstance_t ToneGenInstance68;
#pragma DATA_SECTION (ToneGenInstance68, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance68, 128)

static TGInstance_t ToneGenInstance69;
#pragma DATA_SECTION (ToneGenInstance69, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance69, 128)

static TGInstance_t ToneGenInstance70;
#pragma DATA_SECTION (ToneGenInstance70, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance70, 128)

static TGInstance_t ToneGenInstance71;
#pragma DATA_SECTION (ToneGenInstance71, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance71, 128)

static TGInstance_t ToneGenInstance72;
#pragma DATA_SECTION (ToneGenInstance72, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance72, 128)

static TGInstance_t ToneGenInstance73;
#pragma DATA_SECTION (ToneGenInstance73, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance73, 128)

static TGInstance_t ToneGenInstance74;
#pragma DATA_SECTION (ToneGenInstance74, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance74, 128)

static TGInstance_t ToneGenInstance75;
#pragma DATA_SECTION (ToneGenInstance75, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance75, 128)

static TGInstance_t ToneGenInstance76;
#pragma DATA_SECTION (ToneGenInstance76, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance76, 128)

static TGInstance_t ToneGenInstance77;
#pragma DATA_SECTION (ToneGenInstance77, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance77, 128)

static TGInstance_t ToneGenInstance78;
#pragma DATA_SECTION (ToneGenInstance78, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance78, 128)

static TGInstance_t ToneGenInstance79;
#pragma DATA_SECTION (ToneGenInstance79, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance79, 128)

static TGInstance_t ToneGenInstance80;
#pragma DATA_SECTION (ToneGenInstance80, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance80, 128)

static TGInstance_t ToneGenInstance81;
#pragma DATA_SECTION (ToneGenInstance81, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance81, 128)

static TGInstance_t ToneGenInstance82;
#pragma DATA_SECTION (ToneGenInstance82, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance82, 128)

static TGInstance_t ToneGenInstance83;
#pragma DATA_SECTION (ToneGenInstance83, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance83, 128)

static TGInstance_t ToneGenInstance84;
#pragma DATA_SECTION (ToneGenInstance84, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance84, 128)

static TGInstance_t ToneGenInstance85;
#pragma DATA_SECTION (ToneGenInstance85, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance85, 128)

static TGInstance_t ToneGenInstance86;
#pragma DATA_SECTION (ToneGenInstance86, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance86, 128)

static TGInstance_t ToneGenInstance87;
#pragma DATA_SECTION (ToneGenInstance87, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance87, 128)

static TGInstance_t ToneGenInstance88;
#pragma DATA_SECTION (ToneGenInstance88, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance88, 128)

static TGInstance_t ToneGenInstance89;
#pragma DATA_SECTION (ToneGenInstance89, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance89, 128)

static TGInstance_t ToneGenInstance90;
#pragma DATA_SECTION (ToneGenInstance90, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance90, 128)

static TGInstance_t ToneGenInstance91;
#pragma DATA_SECTION (ToneGenInstance91, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance91, 128)

static TGInstance_t ToneGenInstance92;
#pragma DATA_SECTION (ToneGenInstance92, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance92, 128)

static TGInstance_t ToneGenInstance93;
#pragma DATA_SECTION (ToneGenInstance93, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance93, 128)

static TGInstance_t ToneGenInstance94;
#pragma DATA_SECTION (ToneGenInstance94, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance94, 128)

static TGInstance_t ToneGenInstance95;
#pragma DATA_SECTION (ToneGenInstance95, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance95, 128)

static TGInstance_t ToneGenInstance96;
#pragma DATA_SECTION (ToneGenInstance96, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance96, 128)

static TGInstance_t ToneGenInstance97;
#pragma DATA_SECTION (ToneGenInstance97, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance97, 128)

static TGInstance_t ToneGenInstance98;
#pragma DATA_SECTION (ToneGenInstance98, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance98, 128)

static TGInstance_t ToneGenInstance99;
#pragma DATA_SECTION (ToneGenInstance99, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance99, 128)

static TGInstance_t ToneGenInstance100;
#pragma DATA_SECTION (ToneGenInstance100, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance100, 128)

static TGInstance_t ToneGenInstance101;
#pragma DATA_SECTION (ToneGenInstance101, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance101, 128)

static TGInstance_t ToneGenInstance102;
#pragma DATA_SECTION (ToneGenInstance102, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance102, 128)

static TGInstance_t ToneGenInstance103;
#pragma DATA_SECTION (ToneGenInstance103, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance103, 128)

static TGInstance_t ToneGenInstance104;
#pragma DATA_SECTION (ToneGenInstance104, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance104, 128)

static TGInstance_t ToneGenInstance105;
#pragma DATA_SECTION (ToneGenInstance105, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance105, 128)

static TGInstance_t ToneGenInstance106;
#pragma DATA_SECTION (ToneGenInstance106, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance106, 128)

static TGInstance_t ToneGenInstance107;
#pragma DATA_SECTION (ToneGenInstance107, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance107, 128)

static TGInstance_t ToneGenInstance108;
#pragma DATA_SECTION (ToneGenInstance108, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance108, 128)

static TGInstance_t ToneGenInstance109;
#pragma DATA_SECTION (ToneGenInstance109, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance109, 128)

static TGInstance_t ToneGenInstance110;
#pragma DATA_SECTION (ToneGenInstance110, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance110, 128)

static TGInstance_t ToneGenInstance111;
#pragma DATA_SECTION (ToneGenInstance111, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance111, 128)

static TGInstance_t ToneGenInstance112;
#pragma DATA_SECTION (ToneGenInstance112, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance112, 128)

static TGInstance_t ToneGenInstance113;
#pragma DATA_SECTION (ToneGenInstance113, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance113, 128)

static TGInstance_t ToneGenInstance114;
#pragma DATA_SECTION (ToneGenInstance114, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance114, 128)

static TGInstance_t ToneGenInstance115;
#pragma DATA_SECTION (ToneGenInstance115, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance115, 128)

static TGInstance_t ToneGenInstance116;
#pragma DATA_SECTION (ToneGenInstance116, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance116, 128)

static TGInstance_t ToneGenInstance117;
#pragma DATA_SECTION (ToneGenInstance117, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance117, 128)

static TGInstance_t ToneGenInstance118;
#pragma DATA_SECTION (ToneGenInstance118, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance118, 128)

static TGInstance_t ToneGenInstance119;
#pragma DATA_SECTION (ToneGenInstance119, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance119, 128)

static TGInstance_t ToneGenInstance120;
#pragma DATA_SECTION (ToneGenInstance120, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance120, 128)

static TGInstance_t ToneGenInstance121;
#pragma DATA_SECTION (ToneGenInstance121, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance121, 128)

static TGInstance_t ToneGenInstance122;
#pragma DATA_SECTION (ToneGenInstance122, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance122, 128)

static TGInstance_t ToneGenInstance123;
#pragma DATA_SECTION (ToneGenInstance123, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance123, 128)

static TGInstance_t ToneGenInstance124;
#pragma DATA_SECTION (ToneGenInstance124, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance124, 128)

static TGInstance_t ToneGenInstance125;
#pragma DATA_SECTION (ToneGenInstance125, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance125, 128)

static TGInstance_t ToneGenInstance126;
#pragma DATA_SECTION (ToneGenInstance126, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance126, 128)

static TGInstance_t ToneGenInstance127;
#pragma DATA_SECTION (ToneGenInstance127, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance127, 128)

static TGInstance_t ToneGenInstance128;
#pragma DATA_SECTION (ToneGenInstance128, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance128, 128)

static TGInstance_t ToneGenInstance129;
#pragma DATA_SECTION (ToneGenInstance129, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance129, 128)

static TGInstance_t ToneGenInstance130;
#pragma DATA_SECTION (ToneGenInstance130, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance130, 128)

static TGInstance_t ToneGenInstance131;
#pragma DATA_SECTION (ToneGenInstance131, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance131, 128)

static TGInstance_t ToneGenInstance132;
#pragma DATA_SECTION (ToneGenInstance132, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance132, 128)

static TGInstance_t ToneGenInstance133;
#pragma DATA_SECTION (ToneGenInstance133, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance133, 128)

static TGInstance_t ToneGenInstance134;
#pragma DATA_SECTION (ToneGenInstance134, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance134, 128)

static TGInstance_t ToneGenInstance135;
#pragma DATA_SECTION (ToneGenInstance135, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance135, 128)

static TGInstance_t ToneGenInstance136;
#pragma DATA_SECTION (ToneGenInstance136, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance136, 128)

static TGInstance_t ToneGenInstance137;
#pragma DATA_SECTION (ToneGenInstance137, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance137, 128)

static TGInstance_t ToneGenInstance138;
#pragma DATA_SECTION (ToneGenInstance138, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance138, 128)

static TGInstance_t ToneGenInstance139;
#pragma DATA_SECTION (ToneGenInstance139, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance139, 128)

static TGInstance_t ToneGenInstance140;
#pragma DATA_SECTION (ToneGenInstance140, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance140, 128)

static TGInstance_t ToneGenInstance141;
#pragma DATA_SECTION (ToneGenInstance141, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance141, 128)

static TGInstance_t ToneGenInstance142;
#pragma DATA_SECTION (ToneGenInstance142, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance142, 128)

static TGInstance_t ToneGenInstance143;
#pragma DATA_SECTION (ToneGenInstance143, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance143, 128)

static TGInstance_t ToneGenInstance144;
#pragma DATA_SECTION (ToneGenInstance144, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance144, 128)

static TGInstance_t ToneGenInstance145;
#pragma DATA_SECTION (ToneGenInstance145, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance145, 128)

static TGInstance_t ToneGenInstance146;
#pragma DATA_SECTION (ToneGenInstance146, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance146, 128)

static TGInstance_t ToneGenInstance147;
#pragma DATA_SECTION (ToneGenInstance147, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance147, 128)

static TGInstance_t ToneGenInstance148;
#pragma DATA_SECTION (ToneGenInstance148, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance148, 128)

static TGInstance_t ToneGenInstance149;
#pragma DATA_SECTION (ToneGenInstance149, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance149, 128)

static TGInstance_t ToneGenInstance150;
#pragma DATA_SECTION (ToneGenInstance150, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance150, 128)

static TGInstance_t ToneGenInstance151;
#pragma DATA_SECTION (ToneGenInstance151, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance151, 128)

static TGInstance_t ToneGenInstance152;
#pragma DATA_SECTION (ToneGenInstance152, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance152, 128)

static TGInstance_t ToneGenInstance153;
#pragma DATA_SECTION (ToneGenInstance153, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance153, 128)

static TGInstance_t ToneGenInstance154;
#pragma DATA_SECTION (ToneGenInstance154, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance154, 128)

static TGInstance_t ToneGenInstance155;
#pragma DATA_SECTION (ToneGenInstance155, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance155, 128)

static TGInstance_t ToneGenInstance156;
#pragma DATA_SECTION (ToneGenInstance156, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance156, 128)

static TGInstance_t ToneGenInstance157;
#pragma DATA_SECTION (ToneGenInstance157, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance157, 128)

static TGInstance_t ToneGenInstance158;
#pragma DATA_SECTION (ToneGenInstance158, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance158, 128)

static TGInstance_t ToneGenInstance159;
#pragma DATA_SECTION (ToneGenInstance159, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance159, 128)

static TGInstance_t ToneGenInstance160;
#pragma DATA_SECTION (ToneGenInstance160, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance160, 128)

static TGInstance_t ToneGenInstance161;
#pragma DATA_SECTION (ToneGenInstance161, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance161, 128)

static TGInstance_t ToneGenInstance162;
#pragma DATA_SECTION (ToneGenInstance162, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance162, 128)

static TGInstance_t ToneGenInstance163;
#pragma DATA_SECTION (ToneGenInstance163, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance163, 128)

static TGInstance_t ToneGenInstance164;
#pragma DATA_SECTION (ToneGenInstance164, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance164, 128)

static TGInstance_t ToneGenInstance165;
#pragma DATA_SECTION (ToneGenInstance165, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance165, 128)

static TGInstance_t ToneGenInstance166;
#pragma DATA_SECTION (ToneGenInstance166, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance166, 128)

static TGInstance_t ToneGenInstance167;
#pragma DATA_SECTION (ToneGenInstance167, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance167, 128)

static TGInstance_t ToneGenInstance168;
#pragma DATA_SECTION (ToneGenInstance168, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance168, 128)

static TGInstance_t ToneGenInstance169;
#pragma DATA_SECTION (ToneGenInstance169, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance169, 128)

static TGInstance_t ToneGenInstance170;
#pragma DATA_SECTION (ToneGenInstance170, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance170, 128)

static TGInstance_t ToneGenInstance171;
#pragma DATA_SECTION (ToneGenInstance171, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance171, 128)

static TGInstance_t ToneGenInstance172;
#pragma DATA_SECTION (ToneGenInstance172, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance172, 128)

static TGInstance_t ToneGenInstance173;
#pragma DATA_SECTION (ToneGenInstance173, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance173, 128)

static TGInstance_t ToneGenInstance174;
#pragma DATA_SECTION (ToneGenInstance174, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance174, 128)

static TGInstance_t ToneGenInstance175;
#pragma DATA_SECTION (ToneGenInstance175, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance175, 128)

static TGInstance_t ToneGenInstance176;
#pragma DATA_SECTION (ToneGenInstance176, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance176, 128)

static TGInstance_t ToneGenInstance177;
#pragma DATA_SECTION (ToneGenInstance177, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance177, 128)

static TGInstance_t ToneGenInstance178;
#pragma DATA_SECTION (ToneGenInstance178, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance178, 128)

static TGInstance_t ToneGenInstance179;
#pragma DATA_SECTION (ToneGenInstance179, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance179, 128)

static TGInstance_t ToneGenInstance180;
#pragma DATA_SECTION (ToneGenInstance180, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance180, 128)

static TGInstance_t ToneGenInstance181;
#pragma DATA_SECTION (ToneGenInstance181, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance181, 128)

static TGInstance_t ToneGenInstance182;
#pragma DATA_SECTION (ToneGenInstance182, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance182, 128)

static TGInstance_t ToneGenInstance183;
#pragma DATA_SECTION (ToneGenInstance183, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance183, 128)

static TGInstance_t ToneGenInstance184;
#pragma DATA_SECTION (ToneGenInstance184, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance184, 128)

static TGInstance_t ToneGenInstance185;
#pragma DATA_SECTION (ToneGenInstance185, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance185, 128)

static TGInstance_t ToneGenInstance186;
#pragma DATA_SECTION (ToneGenInstance186, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance186, 128)

static TGInstance_t ToneGenInstance187;
#pragma DATA_SECTION (ToneGenInstance187, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance187, 128)

static TGInstance_t ToneGenInstance188;
#pragma DATA_SECTION (ToneGenInstance188, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance188, 128)

static TGInstance_t ToneGenInstance189;
#pragma DATA_SECTION (ToneGenInstance189, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance189, 128)

static TGInstance_t ToneGenInstance190;
#pragma DATA_SECTION (ToneGenInstance190, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance190, 128)

static TGInstance_t ToneGenInstance191;
#pragma DATA_SECTION (ToneGenInstance191, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance191, 128)

static TGInstance_t ToneGenInstance192;
#pragma DATA_SECTION (ToneGenInstance192, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance192, 128)

static TGInstance_t ToneGenInstance193;
#pragma DATA_SECTION (ToneGenInstance193, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance193, 128)

static TGInstance_t ToneGenInstance194;
#pragma DATA_SECTION (ToneGenInstance194, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance194, 128)

static TGInstance_t ToneGenInstance195;
#pragma DATA_SECTION (ToneGenInstance195, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance195, 128)

static TGInstance_t ToneGenInstance196;
#pragma DATA_SECTION (ToneGenInstance196, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance196, 128)

static TGInstance_t ToneGenInstance197;
#pragma DATA_SECTION (ToneGenInstance197, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance197, 128)

static TGInstance_t ToneGenInstance198;
#pragma DATA_SECTION (ToneGenInstance198, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance198, 128)

static TGInstance_t ToneGenInstance199;
#pragma DATA_SECTION (ToneGenInstance199, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance199, 128)

static TGInstance_t ToneGenInstance200;
#pragma DATA_SECTION (ToneGenInstance200, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance200, 128)

static TGInstance_t ToneGenInstance201;
#pragma DATA_SECTION (ToneGenInstance201, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance201, 128)

static TGInstance_t ToneGenInstance202;
#pragma DATA_SECTION (ToneGenInstance202, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance202, 128)

static TGInstance_t ToneGenInstance203;
#pragma DATA_SECTION (ToneGenInstance203, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance203, 128)

static TGInstance_t ToneGenInstance204;
#pragma DATA_SECTION (ToneGenInstance204, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance204, 128)

static TGInstance_t ToneGenInstance205;
#pragma DATA_SECTION (ToneGenInstance205, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance205, 128)

static TGInstance_t ToneGenInstance206;
#pragma DATA_SECTION (ToneGenInstance206, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance206, 128)

static TGInstance_t ToneGenInstance207;
#pragma DATA_SECTION (ToneGenInstance207, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance207, 128)

static TGInstance_t ToneGenInstance208;
#pragma DATA_SECTION (ToneGenInstance208, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance208, 128)

static TGInstance_t ToneGenInstance209;
#pragma DATA_SECTION (ToneGenInstance209, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance209, 128)

static TGInstance_t ToneGenInstance210;
#pragma DATA_SECTION (ToneGenInstance210, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance210, 128)

static TGInstance_t ToneGenInstance211;
#pragma DATA_SECTION (ToneGenInstance211, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance211, 128)

static TGInstance_t ToneGenInstance212;
#pragma DATA_SECTION (ToneGenInstance212, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance212, 128)

static TGInstance_t ToneGenInstance213;
#pragma DATA_SECTION (ToneGenInstance213, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance213, 128)

static TGInstance_t ToneGenInstance214;
#pragma DATA_SECTION (ToneGenInstance214, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance214, 128)

static TGInstance_t ToneGenInstance215;
#pragma DATA_SECTION (ToneGenInstance215, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance215, 128)

static TGInstance_t ToneGenInstance216;
#pragma DATA_SECTION (ToneGenInstance216, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance216, 128)

static TGInstance_t ToneGenInstance217;
#pragma DATA_SECTION (ToneGenInstance217, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance217, 128)

static TGInstance_t ToneGenInstance218;
#pragma DATA_SECTION (ToneGenInstance218, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance218, 128)

static TGInstance_t ToneGenInstance219;
#pragma DATA_SECTION (ToneGenInstance219, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance219, 128)

static TGInstance_t ToneGenInstance220;
#pragma DATA_SECTION (ToneGenInstance220, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance220, 128)

static TGInstance_t ToneGenInstance221;
#pragma DATA_SECTION (ToneGenInstance221, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance221, 128)

static TGInstance_t ToneGenInstance222;
#pragma DATA_SECTION (ToneGenInstance222, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance222, 128)

static TGInstance_t ToneGenInstance223;
#pragma DATA_SECTION (ToneGenInstance223, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance223, 128)

static TGInstance_t ToneGenInstance224;
#pragma DATA_SECTION (ToneGenInstance224, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance224, 128)

static TGInstance_t ToneGenInstance225;
#pragma DATA_SECTION (ToneGenInstance225, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance225, 128)

static TGInstance_t ToneGenInstance226;
#pragma DATA_SECTION (ToneGenInstance226, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance226, 128)

static TGInstance_t ToneGenInstance227;
#pragma DATA_SECTION (ToneGenInstance227, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance227, 128)

static TGInstance_t ToneGenInstance228;
#pragma DATA_SECTION (ToneGenInstance228, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance228, 128)

static TGInstance_t ToneGenInstance229;
#pragma DATA_SECTION (ToneGenInstance229, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance229, 128)

static TGInstance_t ToneGenInstance230;
#pragma DATA_SECTION (ToneGenInstance230, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance230, 128)

static TGInstance_t ToneGenInstance231;
#pragma DATA_SECTION (ToneGenInstance231, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance231, 128)

static TGInstance_t ToneGenInstance232;
#pragma DATA_SECTION (ToneGenInstance232, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance232, 128)

static TGInstance_t ToneGenInstance233;
#pragma DATA_SECTION (ToneGenInstance233, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance233, 128)

static TGInstance_t ToneGenInstance234;
#pragma DATA_SECTION (ToneGenInstance234, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance234, 128)

static TGInstance_t ToneGenInstance235;
#pragma DATA_SECTION (ToneGenInstance235, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance235, 128)

static TGInstance_t ToneGenInstance236;
#pragma DATA_SECTION (ToneGenInstance236, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance236, 128)

static TGInstance_t ToneGenInstance237;
#pragma DATA_SECTION (ToneGenInstance237, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance237, 128)

static TGInstance_t ToneGenInstance238;
#pragma DATA_SECTION (ToneGenInstance238, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance238, 128)

static TGInstance_t ToneGenInstance239;
#pragma DATA_SECTION (ToneGenInstance239, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance239, 128)

static TGInstance_t ToneGenInstance240;
#pragma DATA_SECTION (ToneGenInstance240, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance240, 128)

static TGInstance_t ToneGenInstance241;
#pragma DATA_SECTION (ToneGenInstance241, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance241, 128)

static TGInstance_t ToneGenInstance242;
#pragma DATA_SECTION (ToneGenInstance242, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance242, 128)

static TGInstance_t ToneGenInstance243;
#pragma DATA_SECTION (ToneGenInstance243, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance243, 128)

static TGInstance_t ToneGenInstance244;
#pragma DATA_SECTION (ToneGenInstance244, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance244, 128)

static TGInstance_t ToneGenInstance245;
#pragma DATA_SECTION (ToneGenInstance245, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance245, 128)

static TGInstance_t ToneGenInstance246;
#pragma DATA_SECTION (ToneGenInstance246, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance246, 128)

static TGInstance_t ToneGenInstance247;
#pragma DATA_SECTION (ToneGenInstance247, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance247, 128)

static TGInstance_t ToneGenInstance248;
#pragma DATA_SECTION (ToneGenInstance248, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance248, 128)

static TGInstance_t ToneGenInstance249;
#pragma DATA_SECTION (ToneGenInstance249, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance249, 128)

static TGInstance_t ToneGenInstance250;
#pragma DATA_SECTION (ToneGenInstance250, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance250, 128)

static TGInstance_t ToneGenInstance251;
#pragma DATA_SECTION (ToneGenInstance251, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance251, 128)

static TGInstance_t ToneGenInstance252;
#pragma DATA_SECTION (ToneGenInstance252, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance252, 128)

static TGInstance_t ToneGenInstance253;
#pragma DATA_SECTION (ToneGenInstance253, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance253, 128)

static TGInstance_t ToneGenInstance254;
#pragma DATA_SECTION (ToneGenInstance254, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance254, 128)

static TGInstance_t ToneGenInstance255;
#pragma DATA_SECTION (ToneGenInstance255, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance255, 128)

static TGInstance_t ToneGenInstance256;
#pragma DATA_SECTION (ToneGenInstance256, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance256, 128)

static TGInstance_t ToneGenInstance257;
#pragma DATA_SECTION (ToneGenInstance257, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance257, 128)

static TGInstance_t ToneGenInstance258;
#pragma DATA_SECTION (ToneGenInstance258, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance258, 128)

static TGInstance_t ToneGenInstance259;
#pragma DATA_SECTION (ToneGenInstance259, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance259, 128)

static TGInstance_t ToneGenInstance260;
#pragma DATA_SECTION (ToneGenInstance260, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance260, 128)

static TGInstance_t ToneGenInstance261;
#pragma DATA_SECTION (ToneGenInstance261, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance261, 128)

static TGInstance_t ToneGenInstance262;
#pragma DATA_SECTION (ToneGenInstance262, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance262, 128)

static TGInstance_t ToneGenInstance263;
#pragma DATA_SECTION (ToneGenInstance263, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance263, 128)

static TGInstance_t ToneGenInstance264;
#pragma DATA_SECTION (ToneGenInstance264, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance264, 128)

static TGInstance_t ToneGenInstance265;
#pragma DATA_SECTION (ToneGenInstance265, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance265, 128)

static TGInstance_t ToneGenInstance266;
#pragma DATA_SECTION (ToneGenInstance266, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance266, 128)

static TGInstance_t ToneGenInstance267;
#pragma DATA_SECTION (ToneGenInstance267, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance267, 128)

static TGInstance_t ToneGenInstance268;
#pragma DATA_SECTION (ToneGenInstance268, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance268, 128)

static TGInstance_t ToneGenInstance269;
#pragma DATA_SECTION (ToneGenInstance269, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance269, 128)

static TGInstance_t ToneGenInstance270;
#pragma DATA_SECTION (ToneGenInstance270, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance270, 128)

static TGInstance_t ToneGenInstance271;
#pragma DATA_SECTION (ToneGenInstance271, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance271, 128)

static TGInstance_t ToneGenInstance272;
#pragma DATA_SECTION (ToneGenInstance272, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance272, 128)

static TGInstance_t ToneGenInstance273;
#pragma DATA_SECTION (ToneGenInstance273, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance273, 128)

static TGInstance_t ToneGenInstance274;
#pragma DATA_SECTION (ToneGenInstance274, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance274, 128)

static TGInstance_t ToneGenInstance275;
#pragma DATA_SECTION (ToneGenInstance275, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance275, 128)

static TGInstance_t ToneGenInstance276;
#pragma DATA_SECTION (ToneGenInstance276, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance276, 128)

static TGInstance_t ToneGenInstance277;
#pragma DATA_SECTION (ToneGenInstance277, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance277, 128)

static TGInstance_t ToneGenInstance278;
#pragma DATA_SECTION (ToneGenInstance278, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance278, 128)

static TGInstance_t ToneGenInstance279;
#pragma DATA_SECTION (ToneGenInstance279, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance279, 128)

static TGInstance_t ToneGenInstance280;
#pragma DATA_SECTION (ToneGenInstance280, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance280, 128)

static TGInstance_t ToneGenInstance281;
#pragma DATA_SECTION (ToneGenInstance281, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance281, 128)

static TGInstance_t ToneGenInstance282;
#pragma DATA_SECTION (ToneGenInstance282, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance282, 128)

static TGInstance_t ToneGenInstance283;
#pragma DATA_SECTION (ToneGenInstance283, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance283, 128)

static TGInstance_t ToneGenInstance284;
#pragma DATA_SECTION (ToneGenInstance284, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance284, 128)

static TGInstance_t ToneGenInstance285;
#pragma DATA_SECTION (ToneGenInstance285, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance285, 128)

static TGInstance_t ToneGenInstance286;
#pragma DATA_SECTION (ToneGenInstance286, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance286, 128)

static TGInstance_t ToneGenInstance287;
#pragma DATA_SECTION (ToneGenInstance287, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance287, 128)

static TGInstance_t ToneGenInstance288;
#pragma DATA_SECTION (ToneGenInstance288, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance288, 128)

static TGInstance_t ToneGenInstance289;
#pragma DATA_SECTION (ToneGenInstance289, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance289, 128)

static TGInstance_t ToneGenInstance290;
#pragma DATA_SECTION (ToneGenInstance290, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance290, 128)

static TGInstance_t ToneGenInstance291;
#pragma DATA_SECTION (ToneGenInstance291, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance291, 128)

static TGInstance_t ToneGenInstance292;
#pragma DATA_SECTION (ToneGenInstance292, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance292, 128)

static TGInstance_t ToneGenInstance293;
#pragma DATA_SECTION (ToneGenInstance293, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance293, 128)

static TGInstance_t ToneGenInstance294;
#pragma DATA_SECTION (ToneGenInstance294, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance294, 128)

static TGInstance_t ToneGenInstance295;
#pragma DATA_SECTION (ToneGenInstance295, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance295, 128)

static TGInstance_t ToneGenInstance296;
#pragma DATA_SECTION (ToneGenInstance296, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance296, 128)

static TGInstance_t ToneGenInstance297;
#pragma DATA_SECTION (ToneGenInstance297, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance297, 128)

static TGInstance_t ToneGenInstance298;
#pragma DATA_SECTION (ToneGenInstance298, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance298, 128)

static TGInstance_t ToneGenInstance299;
#pragma DATA_SECTION (ToneGenInstance299, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance299, 128)

static TGInstance_t ToneGenInstance300;
#pragma DATA_SECTION (ToneGenInstance300, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance300, 128)

static TGInstance_t ToneGenInstance301;
#pragma DATA_SECTION (ToneGenInstance301, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance301, 128)

static TGInstance_t ToneGenInstance302;
#pragma DATA_SECTION (ToneGenInstance302, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance302, 128)

static TGInstance_t ToneGenInstance303;
#pragma DATA_SECTION (ToneGenInstance303, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance303, 128)

static TGInstance_t ToneGenInstance304;
#pragma DATA_SECTION (ToneGenInstance304, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance304, 128)

static TGInstance_t ToneGenInstance305;
#pragma DATA_SECTION (ToneGenInstance305, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance305, 128)

static TGInstance_t ToneGenInstance306;
#pragma DATA_SECTION (ToneGenInstance306, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance306, 128)

static TGInstance_t ToneGenInstance307;
#pragma DATA_SECTION (ToneGenInstance307, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance307, 128)

static TGInstance_t ToneGenInstance308;
#pragma DATA_SECTION (ToneGenInstance308, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance308, 128)

static TGInstance_t ToneGenInstance309;
#pragma DATA_SECTION (ToneGenInstance309, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance309, 128)

static TGInstance_t ToneGenInstance310;
#pragma DATA_SECTION (ToneGenInstance310, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance310, 128)

static TGInstance_t ToneGenInstance311;
#pragma DATA_SECTION (ToneGenInstance311, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance311, 128)

static TGInstance_t ToneGenInstance312;
#pragma DATA_SECTION (ToneGenInstance312, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance312, 128)

static TGInstance_t ToneGenInstance313;
#pragma DATA_SECTION (ToneGenInstance313, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance313, 128)

static TGInstance_t ToneGenInstance314;
#pragma DATA_SECTION (ToneGenInstance314, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance314, 128)

static TGInstance_t ToneGenInstance315;
#pragma DATA_SECTION (ToneGenInstance315, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance315, 128)

static TGInstance_t ToneGenInstance316;
#pragma DATA_SECTION (ToneGenInstance316, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance316, 128)

static TGInstance_t ToneGenInstance317;
#pragma DATA_SECTION (ToneGenInstance317, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance317, 128)

static TGInstance_t ToneGenInstance318;
#pragma DATA_SECTION (ToneGenInstance318, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance318, 128)

static TGInstance_t ToneGenInstance319;
#pragma DATA_SECTION (ToneGenInstance319, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenInstance319, 128)

TGInstance_t* const ToneGenInstance[] = {
	&ToneGenInstance0,
	&ToneGenInstance1,
	&ToneGenInstance2,
	&ToneGenInstance3,
	&ToneGenInstance4,
	&ToneGenInstance5,
	&ToneGenInstance6,
	&ToneGenInstance7,
	&ToneGenInstance8,
	&ToneGenInstance9,
	&ToneGenInstance10,
	&ToneGenInstance11,
	&ToneGenInstance12,
	&ToneGenInstance13,
	&ToneGenInstance14,
	&ToneGenInstance15,
	&ToneGenInstance16,
	&ToneGenInstance17,
	&ToneGenInstance18,
	&ToneGenInstance19,
	&ToneGenInstance20,
	&ToneGenInstance21,
	&ToneGenInstance22,
	&ToneGenInstance23,
	&ToneGenInstance24,
	&ToneGenInstance25,
	&ToneGenInstance26,
	&ToneGenInstance27,
	&ToneGenInstance28,
	&ToneGenInstance29,
	&ToneGenInstance30,
	&ToneGenInstance31,
	&ToneGenInstance32,
	&ToneGenInstance33,
	&ToneGenInstance34,
	&ToneGenInstance35,
	&ToneGenInstance36,
	&ToneGenInstance37,
	&ToneGenInstance38,
	&ToneGenInstance39,
	&ToneGenInstance40,
	&ToneGenInstance41,
	&ToneGenInstance42,
	&ToneGenInstance43,
	&ToneGenInstance44,
	&ToneGenInstance45,
	&ToneGenInstance46,
	&ToneGenInstance47,
	&ToneGenInstance48,
	&ToneGenInstance49,
	&ToneGenInstance50,
	&ToneGenInstance51,
	&ToneGenInstance52,
	&ToneGenInstance53,
	&ToneGenInstance54,
	&ToneGenInstance55,
	&ToneGenInstance56,
	&ToneGenInstance57,
	&ToneGenInstance58,
	&ToneGenInstance59,
	&ToneGenInstance60,
	&ToneGenInstance61,
	&ToneGenInstance62,
	&ToneGenInstance63,
	&ToneGenInstance64,
	&ToneGenInstance65,
	&ToneGenInstance66,
	&ToneGenInstance67,
	&ToneGenInstance68,
	&ToneGenInstance69,
	&ToneGenInstance70,
	&ToneGenInstance71,
	&ToneGenInstance72,
	&ToneGenInstance73,
	&ToneGenInstance74,
	&ToneGenInstance75,
	&ToneGenInstance76,
	&ToneGenInstance77,
	&ToneGenInstance78,
	&ToneGenInstance79,
	&ToneGenInstance80,
	&ToneGenInstance81,
	&ToneGenInstance82,
	&ToneGenInstance83,
	&ToneGenInstance84,
	&ToneGenInstance85,
	&ToneGenInstance86,
	&ToneGenInstance87,
	&ToneGenInstance88,
	&ToneGenInstance89,
	&ToneGenInstance90,
	&ToneGenInstance91,
	&ToneGenInstance92,
	&ToneGenInstance93,
	&ToneGenInstance94,
	&ToneGenInstance95,
	&ToneGenInstance96,
	&ToneGenInstance97,
	&ToneGenInstance98,
	&ToneGenInstance99,
	&ToneGenInstance100,
	&ToneGenInstance101,
	&ToneGenInstance102,
	&ToneGenInstance103,
	&ToneGenInstance104,
	&ToneGenInstance105,
	&ToneGenInstance106,
	&ToneGenInstance107,
	&ToneGenInstance108,
	&ToneGenInstance109,
	&ToneGenInstance110,
	&ToneGenInstance111,
	&ToneGenInstance112,
	&ToneGenInstance113,
	&ToneGenInstance114,
	&ToneGenInstance115,
	&ToneGenInstance116,
	&ToneGenInstance117,
	&ToneGenInstance118,
	&ToneGenInstance119,
	&ToneGenInstance120,
	&ToneGenInstance121,
	&ToneGenInstance122,
	&ToneGenInstance123,
	&ToneGenInstance124,
	&ToneGenInstance125,
	&ToneGenInstance126,
	&ToneGenInstance127,
	&ToneGenInstance128,
	&ToneGenInstance129,
	&ToneGenInstance130,
	&ToneGenInstance131,
	&ToneGenInstance132,
	&ToneGenInstance133,
	&ToneGenInstance134,
	&ToneGenInstance135,
	&ToneGenInstance136,
	&ToneGenInstance137,
	&ToneGenInstance138,
	&ToneGenInstance139,
	&ToneGenInstance140,
	&ToneGenInstance141,
	&ToneGenInstance142,
	&ToneGenInstance143,
	&ToneGenInstance144,
	&ToneGenInstance145,
	&ToneGenInstance146,
	&ToneGenInstance147,
	&ToneGenInstance148,
	&ToneGenInstance149,
	&ToneGenInstance150,
	&ToneGenInstance151,
	&ToneGenInstance152,
	&ToneGenInstance153,
	&ToneGenInstance154,
	&ToneGenInstance155,
	&ToneGenInstance156,
	&ToneGenInstance157,
	&ToneGenInstance158,
	&ToneGenInstance159,
	&ToneGenInstance160,
	&ToneGenInstance161,
	&ToneGenInstance162,
	&ToneGenInstance163,
	&ToneGenInstance164,
	&ToneGenInstance165,
	&ToneGenInstance166,
	&ToneGenInstance167,
	&ToneGenInstance168,
	&ToneGenInstance169,
	&ToneGenInstance170,
	&ToneGenInstance171,
	&ToneGenInstance172,
	&ToneGenInstance173,
	&ToneGenInstance174,
	&ToneGenInstance175,
	&ToneGenInstance176,
	&ToneGenInstance177,
	&ToneGenInstance178,
	&ToneGenInstance179,
	&ToneGenInstance180,
	&ToneGenInstance181,
	&ToneGenInstance182,
	&ToneGenInstance183,
	&ToneGenInstance184,
	&ToneGenInstance185,
	&ToneGenInstance186,
	&ToneGenInstance187,
	&ToneGenInstance188,
	&ToneGenInstance189,
	&ToneGenInstance190,
	&ToneGenInstance191,
	&ToneGenInstance192,
	&ToneGenInstance193,
	&ToneGenInstance194,
	&ToneGenInstance195,
	&ToneGenInstance196,
	&ToneGenInstance197,
	&ToneGenInstance198,
	&ToneGenInstance199,
	&ToneGenInstance200,
	&ToneGenInstance201,
	&ToneGenInstance202,
	&ToneGenInstance203,
	&ToneGenInstance204,
	&ToneGenInstance205,
	&ToneGenInstance206,
	&ToneGenInstance207,
	&ToneGenInstance208,
	&ToneGenInstance209,
	&ToneGenInstance210,
	&ToneGenInstance211,
	&ToneGenInstance212,
	&ToneGenInstance213,
	&ToneGenInstance214,
	&ToneGenInstance215,
	&ToneGenInstance216,
	&ToneGenInstance217,
	&ToneGenInstance218,
	&ToneGenInstance219,
	&ToneGenInstance220,
	&ToneGenInstance221,
	&ToneGenInstance222,
	&ToneGenInstance223,
	&ToneGenInstance224,
	&ToneGenInstance225,
	&ToneGenInstance226,
	&ToneGenInstance227,
	&ToneGenInstance228,
	&ToneGenInstance229,
	&ToneGenInstance230,
	&ToneGenInstance231,
	&ToneGenInstance232,
	&ToneGenInstance233,
	&ToneGenInstance234,
	&ToneGenInstance235,
	&ToneGenInstance236,
	&ToneGenInstance237,
	&ToneGenInstance238,
	&ToneGenInstance239,
	&ToneGenInstance240,
	&ToneGenInstance241,
	&ToneGenInstance242,
	&ToneGenInstance243,
	&ToneGenInstance244,
	&ToneGenInstance245,
	&ToneGenInstance246,
	&ToneGenInstance247,
	&ToneGenInstance248,
	&ToneGenInstance249,
	&ToneGenInstance250,
	&ToneGenInstance251,
	&ToneGenInstance252,
	&ToneGenInstance253,
	&ToneGenInstance254,
	&ToneGenInstance255,
	&ToneGenInstance256,
	&ToneGenInstance257,
	&ToneGenInstance258,
	&ToneGenInstance259,
	&ToneGenInstance260,
	&ToneGenInstance261,
	&ToneGenInstance262,
	&ToneGenInstance263,
	&ToneGenInstance264,
	&ToneGenInstance265,
	&ToneGenInstance266,
	&ToneGenInstance267,
	&ToneGenInstance268,
	&ToneGenInstance269,
	&ToneGenInstance270,
	&ToneGenInstance271,
	&ToneGenInstance272,
	&ToneGenInstance273,
	&ToneGenInstance274,
	&ToneGenInstance275,
	&ToneGenInstance276,
	&ToneGenInstance277,
	&ToneGenInstance278,
	&ToneGenInstance279,
	&ToneGenInstance280,
	&ToneGenInstance281,
	&ToneGenInstance282,
	&ToneGenInstance283,
	&ToneGenInstance284,
	&ToneGenInstance285,
	&ToneGenInstance286,
	&ToneGenInstance287,
	&ToneGenInstance288,
	&ToneGenInstance289,
	&ToneGenInstance290,
	&ToneGenInstance291,
	&ToneGenInstance292,
	&ToneGenInstance293,
	&ToneGenInstance294,
	&ToneGenInstance295,
	&ToneGenInstance296,
	&ToneGenInstance297,
	&ToneGenInstance298,
	&ToneGenInstance299,
	&ToneGenInstance300,
	&ToneGenInstance301,
	&ToneGenInstance302,
	&ToneGenInstance303,
	&ToneGenInstance304,
	&ToneGenInstance305,
	&ToneGenInstance306,
	&ToneGenInstance307,
	&ToneGenInstance308,
	&ToneGenInstance309,
	&ToneGenInstance310,
	&ToneGenInstance311,
	&ToneGenInstance312,
	&ToneGenInstance313,
	&ToneGenInstance314,
	&ToneGenInstance315,
	&ToneGenInstance316,
	&ToneGenInstance317,
	&ToneGenInstance318,
	&ToneGenInstance319
};
//}

//{           TGParams_1_t        ToneGenParams STRUCTURES to CHAN_INST_DATA:ToneGen

TGParams_1_t ToneGenParams0;
#pragma DATA_SECTION (ToneGenParams0, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams0, 128)

static TGParams_1_t ToneGenParams1;
#pragma DATA_SECTION (ToneGenParams1, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams1, 128)

static TGParams_1_t ToneGenParams2;
#pragma DATA_SECTION (ToneGenParams2, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams2, 128)

static TGParams_1_t ToneGenParams3;
#pragma DATA_SECTION (ToneGenParams3, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams3, 128)

static TGParams_1_t ToneGenParams4;
#pragma DATA_SECTION (ToneGenParams4, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams4, 128)

static TGParams_1_t ToneGenParams5;
#pragma DATA_SECTION (ToneGenParams5, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams5, 128)

static TGParams_1_t ToneGenParams6;
#pragma DATA_SECTION (ToneGenParams6, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams6, 128)

static TGParams_1_t ToneGenParams7;
#pragma DATA_SECTION (ToneGenParams7, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams7, 128)

static TGParams_1_t ToneGenParams8;
#pragma DATA_SECTION (ToneGenParams8, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams8, 128)

static TGParams_1_t ToneGenParams9;
#pragma DATA_SECTION (ToneGenParams9, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams9, 128)

static TGParams_1_t ToneGenParams10;
#pragma DATA_SECTION (ToneGenParams10, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams10, 128)

static TGParams_1_t ToneGenParams11;
#pragma DATA_SECTION (ToneGenParams11, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams11, 128)

static TGParams_1_t ToneGenParams12;
#pragma DATA_SECTION (ToneGenParams12, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams12, 128)

static TGParams_1_t ToneGenParams13;
#pragma DATA_SECTION (ToneGenParams13, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams13, 128)

static TGParams_1_t ToneGenParams14;
#pragma DATA_SECTION (ToneGenParams14, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams14, 128)

static TGParams_1_t ToneGenParams15;
#pragma DATA_SECTION (ToneGenParams15, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams15, 128)

static TGParams_1_t ToneGenParams16;
#pragma DATA_SECTION (ToneGenParams16, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams16, 128)

static TGParams_1_t ToneGenParams17;
#pragma DATA_SECTION (ToneGenParams17, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams17, 128)

static TGParams_1_t ToneGenParams18;
#pragma DATA_SECTION (ToneGenParams18, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams18, 128)

static TGParams_1_t ToneGenParams19;
#pragma DATA_SECTION (ToneGenParams19, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams19, 128)

static TGParams_1_t ToneGenParams20;
#pragma DATA_SECTION (ToneGenParams20, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams20, 128)

static TGParams_1_t ToneGenParams21;
#pragma DATA_SECTION (ToneGenParams21, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams21, 128)

static TGParams_1_t ToneGenParams22;
#pragma DATA_SECTION (ToneGenParams22, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams22, 128)

static TGParams_1_t ToneGenParams23;
#pragma DATA_SECTION (ToneGenParams23, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams23, 128)

static TGParams_1_t ToneGenParams24;
#pragma DATA_SECTION (ToneGenParams24, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams24, 128)

static TGParams_1_t ToneGenParams25;
#pragma DATA_SECTION (ToneGenParams25, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams25, 128)

static TGParams_1_t ToneGenParams26;
#pragma DATA_SECTION (ToneGenParams26, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams26, 128)

static TGParams_1_t ToneGenParams27;
#pragma DATA_SECTION (ToneGenParams27, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams27, 128)

static TGParams_1_t ToneGenParams28;
#pragma DATA_SECTION (ToneGenParams28, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams28, 128)

static TGParams_1_t ToneGenParams29;
#pragma DATA_SECTION (ToneGenParams29, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams29, 128)

static TGParams_1_t ToneGenParams30;
#pragma DATA_SECTION (ToneGenParams30, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams30, 128)

static TGParams_1_t ToneGenParams31;
#pragma DATA_SECTION (ToneGenParams31, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams31, 128)

static TGParams_1_t ToneGenParams32;
#pragma DATA_SECTION (ToneGenParams32, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams32, 128)

static TGParams_1_t ToneGenParams33;
#pragma DATA_SECTION (ToneGenParams33, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams33, 128)

static TGParams_1_t ToneGenParams34;
#pragma DATA_SECTION (ToneGenParams34, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams34, 128)

static TGParams_1_t ToneGenParams35;
#pragma DATA_SECTION (ToneGenParams35, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams35, 128)

static TGParams_1_t ToneGenParams36;
#pragma DATA_SECTION (ToneGenParams36, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams36, 128)

static TGParams_1_t ToneGenParams37;
#pragma DATA_SECTION (ToneGenParams37, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams37, 128)

static TGParams_1_t ToneGenParams38;
#pragma DATA_SECTION (ToneGenParams38, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams38, 128)

static TGParams_1_t ToneGenParams39;
#pragma DATA_SECTION (ToneGenParams39, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams39, 128)

static TGParams_1_t ToneGenParams40;
#pragma DATA_SECTION (ToneGenParams40, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams40, 128)

static TGParams_1_t ToneGenParams41;
#pragma DATA_SECTION (ToneGenParams41, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams41, 128)

static TGParams_1_t ToneGenParams42;
#pragma DATA_SECTION (ToneGenParams42, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams42, 128)

static TGParams_1_t ToneGenParams43;
#pragma DATA_SECTION (ToneGenParams43, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams43, 128)

static TGParams_1_t ToneGenParams44;
#pragma DATA_SECTION (ToneGenParams44, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams44, 128)

static TGParams_1_t ToneGenParams45;
#pragma DATA_SECTION (ToneGenParams45, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams45, 128)

static TGParams_1_t ToneGenParams46;
#pragma DATA_SECTION (ToneGenParams46, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams46, 128)

static TGParams_1_t ToneGenParams47;
#pragma DATA_SECTION (ToneGenParams47, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams47, 128)

static TGParams_1_t ToneGenParams48;
#pragma DATA_SECTION (ToneGenParams48, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams48, 128)

static TGParams_1_t ToneGenParams49;
#pragma DATA_SECTION (ToneGenParams49, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams49, 128)

static TGParams_1_t ToneGenParams50;
#pragma DATA_SECTION (ToneGenParams50, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams50, 128)

static TGParams_1_t ToneGenParams51;
#pragma DATA_SECTION (ToneGenParams51, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams51, 128)

static TGParams_1_t ToneGenParams52;
#pragma DATA_SECTION (ToneGenParams52, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams52, 128)

static TGParams_1_t ToneGenParams53;
#pragma DATA_SECTION (ToneGenParams53, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams53, 128)

static TGParams_1_t ToneGenParams54;
#pragma DATA_SECTION (ToneGenParams54, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams54, 128)

static TGParams_1_t ToneGenParams55;
#pragma DATA_SECTION (ToneGenParams55, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams55, 128)

static TGParams_1_t ToneGenParams56;
#pragma DATA_SECTION (ToneGenParams56, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams56, 128)

static TGParams_1_t ToneGenParams57;
#pragma DATA_SECTION (ToneGenParams57, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams57, 128)

static TGParams_1_t ToneGenParams58;
#pragma DATA_SECTION (ToneGenParams58, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams58, 128)

static TGParams_1_t ToneGenParams59;
#pragma DATA_SECTION (ToneGenParams59, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams59, 128)

static TGParams_1_t ToneGenParams60;
#pragma DATA_SECTION (ToneGenParams60, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams60, 128)

static TGParams_1_t ToneGenParams61;
#pragma DATA_SECTION (ToneGenParams61, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams61, 128)

static TGParams_1_t ToneGenParams62;
#pragma DATA_SECTION (ToneGenParams62, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams62, 128)

static TGParams_1_t ToneGenParams63;
#pragma DATA_SECTION (ToneGenParams63, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams63, 128)

static TGParams_1_t ToneGenParams64;
#pragma DATA_SECTION (ToneGenParams64, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams64, 128)

static TGParams_1_t ToneGenParams65;
#pragma DATA_SECTION (ToneGenParams65, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams65, 128)

static TGParams_1_t ToneGenParams66;
#pragma DATA_SECTION (ToneGenParams66, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams66, 128)

static TGParams_1_t ToneGenParams67;
#pragma DATA_SECTION (ToneGenParams67, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams67, 128)

static TGParams_1_t ToneGenParams68;
#pragma DATA_SECTION (ToneGenParams68, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams68, 128)

static TGParams_1_t ToneGenParams69;
#pragma DATA_SECTION (ToneGenParams69, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams69, 128)

static TGParams_1_t ToneGenParams70;
#pragma DATA_SECTION (ToneGenParams70, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams70, 128)

static TGParams_1_t ToneGenParams71;
#pragma DATA_SECTION (ToneGenParams71, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams71, 128)

static TGParams_1_t ToneGenParams72;
#pragma DATA_SECTION (ToneGenParams72, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams72, 128)

static TGParams_1_t ToneGenParams73;
#pragma DATA_SECTION (ToneGenParams73, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams73, 128)

static TGParams_1_t ToneGenParams74;
#pragma DATA_SECTION (ToneGenParams74, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams74, 128)

static TGParams_1_t ToneGenParams75;
#pragma DATA_SECTION (ToneGenParams75, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams75, 128)

static TGParams_1_t ToneGenParams76;
#pragma DATA_SECTION (ToneGenParams76, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams76, 128)

static TGParams_1_t ToneGenParams77;
#pragma DATA_SECTION (ToneGenParams77, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams77, 128)

static TGParams_1_t ToneGenParams78;
#pragma DATA_SECTION (ToneGenParams78, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams78, 128)

static TGParams_1_t ToneGenParams79;
#pragma DATA_SECTION (ToneGenParams79, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams79, 128)

static TGParams_1_t ToneGenParams80;
#pragma DATA_SECTION (ToneGenParams80, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams80, 128)

static TGParams_1_t ToneGenParams81;
#pragma DATA_SECTION (ToneGenParams81, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams81, 128)

static TGParams_1_t ToneGenParams82;
#pragma DATA_SECTION (ToneGenParams82, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams82, 128)

static TGParams_1_t ToneGenParams83;
#pragma DATA_SECTION (ToneGenParams83, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams83, 128)

static TGParams_1_t ToneGenParams84;
#pragma DATA_SECTION (ToneGenParams84, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams84, 128)

static TGParams_1_t ToneGenParams85;
#pragma DATA_SECTION (ToneGenParams85, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams85, 128)

static TGParams_1_t ToneGenParams86;
#pragma DATA_SECTION (ToneGenParams86, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams86, 128)

static TGParams_1_t ToneGenParams87;
#pragma DATA_SECTION (ToneGenParams87, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams87, 128)

static TGParams_1_t ToneGenParams88;
#pragma DATA_SECTION (ToneGenParams88, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams88, 128)

static TGParams_1_t ToneGenParams89;
#pragma DATA_SECTION (ToneGenParams89, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams89, 128)

static TGParams_1_t ToneGenParams90;
#pragma DATA_SECTION (ToneGenParams90, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams90, 128)

static TGParams_1_t ToneGenParams91;
#pragma DATA_SECTION (ToneGenParams91, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams91, 128)

static TGParams_1_t ToneGenParams92;
#pragma DATA_SECTION (ToneGenParams92, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams92, 128)

static TGParams_1_t ToneGenParams93;
#pragma DATA_SECTION (ToneGenParams93, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams93, 128)

static TGParams_1_t ToneGenParams94;
#pragma DATA_SECTION (ToneGenParams94, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams94, 128)

static TGParams_1_t ToneGenParams95;
#pragma DATA_SECTION (ToneGenParams95, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams95, 128)

static TGParams_1_t ToneGenParams96;
#pragma DATA_SECTION (ToneGenParams96, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams96, 128)

static TGParams_1_t ToneGenParams97;
#pragma DATA_SECTION (ToneGenParams97, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams97, 128)

static TGParams_1_t ToneGenParams98;
#pragma DATA_SECTION (ToneGenParams98, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams98, 128)

static TGParams_1_t ToneGenParams99;
#pragma DATA_SECTION (ToneGenParams99, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams99, 128)

static TGParams_1_t ToneGenParams100;
#pragma DATA_SECTION (ToneGenParams100, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams100, 128)

static TGParams_1_t ToneGenParams101;
#pragma DATA_SECTION (ToneGenParams101, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams101, 128)

static TGParams_1_t ToneGenParams102;
#pragma DATA_SECTION (ToneGenParams102, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams102, 128)

static TGParams_1_t ToneGenParams103;
#pragma DATA_SECTION (ToneGenParams103, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams103, 128)

static TGParams_1_t ToneGenParams104;
#pragma DATA_SECTION (ToneGenParams104, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams104, 128)

static TGParams_1_t ToneGenParams105;
#pragma DATA_SECTION (ToneGenParams105, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams105, 128)

static TGParams_1_t ToneGenParams106;
#pragma DATA_SECTION (ToneGenParams106, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams106, 128)

static TGParams_1_t ToneGenParams107;
#pragma DATA_SECTION (ToneGenParams107, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams107, 128)

static TGParams_1_t ToneGenParams108;
#pragma DATA_SECTION (ToneGenParams108, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams108, 128)

static TGParams_1_t ToneGenParams109;
#pragma DATA_SECTION (ToneGenParams109, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams109, 128)

static TGParams_1_t ToneGenParams110;
#pragma DATA_SECTION (ToneGenParams110, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams110, 128)

static TGParams_1_t ToneGenParams111;
#pragma DATA_SECTION (ToneGenParams111, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams111, 128)

static TGParams_1_t ToneGenParams112;
#pragma DATA_SECTION (ToneGenParams112, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams112, 128)

static TGParams_1_t ToneGenParams113;
#pragma DATA_SECTION (ToneGenParams113, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams113, 128)

static TGParams_1_t ToneGenParams114;
#pragma DATA_SECTION (ToneGenParams114, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams114, 128)

static TGParams_1_t ToneGenParams115;
#pragma DATA_SECTION (ToneGenParams115, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams115, 128)

static TGParams_1_t ToneGenParams116;
#pragma DATA_SECTION (ToneGenParams116, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams116, 128)

static TGParams_1_t ToneGenParams117;
#pragma DATA_SECTION (ToneGenParams117, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams117, 128)

static TGParams_1_t ToneGenParams118;
#pragma DATA_SECTION (ToneGenParams118, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams118, 128)

static TGParams_1_t ToneGenParams119;
#pragma DATA_SECTION (ToneGenParams119, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams119, 128)

static TGParams_1_t ToneGenParams120;
#pragma DATA_SECTION (ToneGenParams120, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams120, 128)

static TGParams_1_t ToneGenParams121;
#pragma DATA_SECTION (ToneGenParams121, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams121, 128)

static TGParams_1_t ToneGenParams122;
#pragma DATA_SECTION (ToneGenParams122, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams122, 128)

static TGParams_1_t ToneGenParams123;
#pragma DATA_SECTION (ToneGenParams123, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams123, 128)

static TGParams_1_t ToneGenParams124;
#pragma DATA_SECTION (ToneGenParams124, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams124, 128)

static TGParams_1_t ToneGenParams125;
#pragma DATA_SECTION (ToneGenParams125, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams125, 128)

static TGParams_1_t ToneGenParams126;
#pragma DATA_SECTION (ToneGenParams126, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams126, 128)

static TGParams_1_t ToneGenParams127;
#pragma DATA_SECTION (ToneGenParams127, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams127, 128)

static TGParams_1_t ToneGenParams128;
#pragma DATA_SECTION (ToneGenParams128, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams128, 128)

static TGParams_1_t ToneGenParams129;
#pragma DATA_SECTION (ToneGenParams129, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams129, 128)

static TGParams_1_t ToneGenParams130;
#pragma DATA_SECTION (ToneGenParams130, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams130, 128)

static TGParams_1_t ToneGenParams131;
#pragma DATA_SECTION (ToneGenParams131, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams131, 128)

static TGParams_1_t ToneGenParams132;
#pragma DATA_SECTION (ToneGenParams132, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams132, 128)

static TGParams_1_t ToneGenParams133;
#pragma DATA_SECTION (ToneGenParams133, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams133, 128)

static TGParams_1_t ToneGenParams134;
#pragma DATA_SECTION (ToneGenParams134, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams134, 128)

static TGParams_1_t ToneGenParams135;
#pragma DATA_SECTION (ToneGenParams135, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams135, 128)

static TGParams_1_t ToneGenParams136;
#pragma DATA_SECTION (ToneGenParams136, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams136, 128)

static TGParams_1_t ToneGenParams137;
#pragma DATA_SECTION (ToneGenParams137, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams137, 128)

static TGParams_1_t ToneGenParams138;
#pragma DATA_SECTION (ToneGenParams138, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams138, 128)

static TGParams_1_t ToneGenParams139;
#pragma DATA_SECTION (ToneGenParams139, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams139, 128)

static TGParams_1_t ToneGenParams140;
#pragma DATA_SECTION (ToneGenParams140, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams140, 128)

static TGParams_1_t ToneGenParams141;
#pragma DATA_SECTION (ToneGenParams141, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams141, 128)

static TGParams_1_t ToneGenParams142;
#pragma DATA_SECTION (ToneGenParams142, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams142, 128)

static TGParams_1_t ToneGenParams143;
#pragma DATA_SECTION (ToneGenParams143, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams143, 128)

static TGParams_1_t ToneGenParams144;
#pragma DATA_SECTION (ToneGenParams144, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams144, 128)

static TGParams_1_t ToneGenParams145;
#pragma DATA_SECTION (ToneGenParams145, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams145, 128)

static TGParams_1_t ToneGenParams146;
#pragma DATA_SECTION (ToneGenParams146, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams146, 128)

static TGParams_1_t ToneGenParams147;
#pragma DATA_SECTION (ToneGenParams147, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams147, 128)

static TGParams_1_t ToneGenParams148;
#pragma DATA_SECTION (ToneGenParams148, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams148, 128)

static TGParams_1_t ToneGenParams149;
#pragma DATA_SECTION (ToneGenParams149, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams149, 128)

static TGParams_1_t ToneGenParams150;
#pragma DATA_SECTION (ToneGenParams150, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams150, 128)

static TGParams_1_t ToneGenParams151;
#pragma DATA_SECTION (ToneGenParams151, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams151, 128)

static TGParams_1_t ToneGenParams152;
#pragma DATA_SECTION (ToneGenParams152, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams152, 128)

static TGParams_1_t ToneGenParams153;
#pragma DATA_SECTION (ToneGenParams153, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams153, 128)

static TGParams_1_t ToneGenParams154;
#pragma DATA_SECTION (ToneGenParams154, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams154, 128)

static TGParams_1_t ToneGenParams155;
#pragma DATA_SECTION (ToneGenParams155, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams155, 128)

static TGParams_1_t ToneGenParams156;
#pragma DATA_SECTION (ToneGenParams156, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams156, 128)

static TGParams_1_t ToneGenParams157;
#pragma DATA_SECTION (ToneGenParams157, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams157, 128)

static TGParams_1_t ToneGenParams158;
#pragma DATA_SECTION (ToneGenParams158, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams158, 128)

static TGParams_1_t ToneGenParams159;
#pragma DATA_SECTION (ToneGenParams159, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams159, 128)

static TGParams_1_t ToneGenParams160;
#pragma DATA_SECTION (ToneGenParams160, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams160, 128)

static TGParams_1_t ToneGenParams161;
#pragma DATA_SECTION (ToneGenParams161, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams161, 128)

static TGParams_1_t ToneGenParams162;
#pragma DATA_SECTION (ToneGenParams162, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams162, 128)

static TGParams_1_t ToneGenParams163;
#pragma DATA_SECTION (ToneGenParams163, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams163, 128)

static TGParams_1_t ToneGenParams164;
#pragma DATA_SECTION (ToneGenParams164, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams164, 128)

static TGParams_1_t ToneGenParams165;
#pragma DATA_SECTION (ToneGenParams165, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams165, 128)

static TGParams_1_t ToneGenParams166;
#pragma DATA_SECTION (ToneGenParams166, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams166, 128)

static TGParams_1_t ToneGenParams167;
#pragma DATA_SECTION (ToneGenParams167, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams167, 128)

static TGParams_1_t ToneGenParams168;
#pragma DATA_SECTION (ToneGenParams168, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams168, 128)

static TGParams_1_t ToneGenParams169;
#pragma DATA_SECTION (ToneGenParams169, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams169, 128)

static TGParams_1_t ToneGenParams170;
#pragma DATA_SECTION (ToneGenParams170, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams170, 128)

static TGParams_1_t ToneGenParams171;
#pragma DATA_SECTION (ToneGenParams171, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams171, 128)

static TGParams_1_t ToneGenParams172;
#pragma DATA_SECTION (ToneGenParams172, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams172, 128)

static TGParams_1_t ToneGenParams173;
#pragma DATA_SECTION (ToneGenParams173, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams173, 128)

static TGParams_1_t ToneGenParams174;
#pragma DATA_SECTION (ToneGenParams174, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams174, 128)

static TGParams_1_t ToneGenParams175;
#pragma DATA_SECTION (ToneGenParams175, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams175, 128)

static TGParams_1_t ToneGenParams176;
#pragma DATA_SECTION (ToneGenParams176, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams176, 128)

static TGParams_1_t ToneGenParams177;
#pragma DATA_SECTION (ToneGenParams177, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams177, 128)

static TGParams_1_t ToneGenParams178;
#pragma DATA_SECTION (ToneGenParams178, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams178, 128)

static TGParams_1_t ToneGenParams179;
#pragma DATA_SECTION (ToneGenParams179, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams179, 128)

static TGParams_1_t ToneGenParams180;
#pragma DATA_SECTION (ToneGenParams180, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams180, 128)

static TGParams_1_t ToneGenParams181;
#pragma DATA_SECTION (ToneGenParams181, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams181, 128)

static TGParams_1_t ToneGenParams182;
#pragma DATA_SECTION (ToneGenParams182, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams182, 128)

static TGParams_1_t ToneGenParams183;
#pragma DATA_SECTION (ToneGenParams183, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams183, 128)

static TGParams_1_t ToneGenParams184;
#pragma DATA_SECTION (ToneGenParams184, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams184, 128)

static TGParams_1_t ToneGenParams185;
#pragma DATA_SECTION (ToneGenParams185, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams185, 128)

static TGParams_1_t ToneGenParams186;
#pragma DATA_SECTION (ToneGenParams186, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams186, 128)

static TGParams_1_t ToneGenParams187;
#pragma DATA_SECTION (ToneGenParams187, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams187, 128)

static TGParams_1_t ToneGenParams188;
#pragma DATA_SECTION (ToneGenParams188, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams188, 128)

static TGParams_1_t ToneGenParams189;
#pragma DATA_SECTION (ToneGenParams189, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams189, 128)

static TGParams_1_t ToneGenParams190;
#pragma DATA_SECTION (ToneGenParams190, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams190, 128)

static TGParams_1_t ToneGenParams191;
#pragma DATA_SECTION (ToneGenParams191, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams191, 128)

static TGParams_1_t ToneGenParams192;
#pragma DATA_SECTION (ToneGenParams192, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams192, 128)

static TGParams_1_t ToneGenParams193;
#pragma DATA_SECTION (ToneGenParams193, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams193, 128)

static TGParams_1_t ToneGenParams194;
#pragma DATA_SECTION (ToneGenParams194, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams194, 128)

static TGParams_1_t ToneGenParams195;
#pragma DATA_SECTION (ToneGenParams195, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams195, 128)

static TGParams_1_t ToneGenParams196;
#pragma DATA_SECTION (ToneGenParams196, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams196, 128)

static TGParams_1_t ToneGenParams197;
#pragma DATA_SECTION (ToneGenParams197, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams197, 128)

static TGParams_1_t ToneGenParams198;
#pragma DATA_SECTION (ToneGenParams198, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams198, 128)

static TGParams_1_t ToneGenParams199;
#pragma DATA_SECTION (ToneGenParams199, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams199, 128)

static TGParams_1_t ToneGenParams200;
#pragma DATA_SECTION (ToneGenParams200, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams200, 128)

static TGParams_1_t ToneGenParams201;
#pragma DATA_SECTION (ToneGenParams201, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams201, 128)

static TGParams_1_t ToneGenParams202;
#pragma DATA_SECTION (ToneGenParams202, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams202, 128)

static TGParams_1_t ToneGenParams203;
#pragma DATA_SECTION (ToneGenParams203, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams203, 128)

static TGParams_1_t ToneGenParams204;
#pragma DATA_SECTION (ToneGenParams204, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams204, 128)

static TGParams_1_t ToneGenParams205;
#pragma DATA_SECTION (ToneGenParams205, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams205, 128)

static TGParams_1_t ToneGenParams206;
#pragma DATA_SECTION (ToneGenParams206, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams206, 128)

static TGParams_1_t ToneGenParams207;
#pragma DATA_SECTION (ToneGenParams207, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams207, 128)

static TGParams_1_t ToneGenParams208;
#pragma DATA_SECTION (ToneGenParams208, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams208, 128)

static TGParams_1_t ToneGenParams209;
#pragma DATA_SECTION (ToneGenParams209, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams209, 128)

static TGParams_1_t ToneGenParams210;
#pragma DATA_SECTION (ToneGenParams210, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams210, 128)

static TGParams_1_t ToneGenParams211;
#pragma DATA_SECTION (ToneGenParams211, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams211, 128)

static TGParams_1_t ToneGenParams212;
#pragma DATA_SECTION (ToneGenParams212, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams212, 128)

static TGParams_1_t ToneGenParams213;
#pragma DATA_SECTION (ToneGenParams213, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams213, 128)

static TGParams_1_t ToneGenParams214;
#pragma DATA_SECTION (ToneGenParams214, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams214, 128)

static TGParams_1_t ToneGenParams215;
#pragma DATA_SECTION (ToneGenParams215, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams215, 128)

static TGParams_1_t ToneGenParams216;
#pragma DATA_SECTION (ToneGenParams216, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams216, 128)

static TGParams_1_t ToneGenParams217;
#pragma DATA_SECTION (ToneGenParams217, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams217, 128)

static TGParams_1_t ToneGenParams218;
#pragma DATA_SECTION (ToneGenParams218, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams218, 128)

static TGParams_1_t ToneGenParams219;
#pragma DATA_SECTION (ToneGenParams219, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams219, 128)

static TGParams_1_t ToneGenParams220;
#pragma DATA_SECTION (ToneGenParams220, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams220, 128)

static TGParams_1_t ToneGenParams221;
#pragma DATA_SECTION (ToneGenParams221, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams221, 128)

static TGParams_1_t ToneGenParams222;
#pragma DATA_SECTION (ToneGenParams222, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams222, 128)

static TGParams_1_t ToneGenParams223;
#pragma DATA_SECTION (ToneGenParams223, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams223, 128)

static TGParams_1_t ToneGenParams224;
#pragma DATA_SECTION (ToneGenParams224, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams224, 128)

static TGParams_1_t ToneGenParams225;
#pragma DATA_SECTION (ToneGenParams225, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams225, 128)

static TGParams_1_t ToneGenParams226;
#pragma DATA_SECTION (ToneGenParams226, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams226, 128)

static TGParams_1_t ToneGenParams227;
#pragma DATA_SECTION (ToneGenParams227, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams227, 128)

static TGParams_1_t ToneGenParams228;
#pragma DATA_SECTION (ToneGenParams228, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams228, 128)

static TGParams_1_t ToneGenParams229;
#pragma DATA_SECTION (ToneGenParams229, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams229, 128)

static TGParams_1_t ToneGenParams230;
#pragma DATA_SECTION (ToneGenParams230, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams230, 128)

static TGParams_1_t ToneGenParams231;
#pragma DATA_SECTION (ToneGenParams231, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams231, 128)

static TGParams_1_t ToneGenParams232;
#pragma DATA_SECTION (ToneGenParams232, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams232, 128)

static TGParams_1_t ToneGenParams233;
#pragma DATA_SECTION (ToneGenParams233, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams233, 128)

static TGParams_1_t ToneGenParams234;
#pragma DATA_SECTION (ToneGenParams234, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams234, 128)

static TGParams_1_t ToneGenParams235;
#pragma DATA_SECTION (ToneGenParams235, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams235, 128)

static TGParams_1_t ToneGenParams236;
#pragma DATA_SECTION (ToneGenParams236, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams236, 128)

static TGParams_1_t ToneGenParams237;
#pragma DATA_SECTION (ToneGenParams237, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams237, 128)

static TGParams_1_t ToneGenParams238;
#pragma DATA_SECTION (ToneGenParams238, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams238, 128)

static TGParams_1_t ToneGenParams239;
#pragma DATA_SECTION (ToneGenParams239, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams239, 128)

static TGParams_1_t ToneGenParams240;
#pragma DATA_SECTION (ToneGenParams240, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams240, 128)

static TGParams_1_t ToneGenParams241;
#pragma DATA_SECTION (ToneGenParams241, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams241, 128)

static TGParams_1_t ToneGenParams242;
#pragma DATA_SECTION (ToneGenParams242, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams242, 128)

static TGParams_1_t ToneGenParams243;
#pragma DATA_SECTION (ToneGenParams243, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams243, 128)

static TGParams_1_t ToneGenParams244;
#pragma DATA_SECTION (ToneGenParams244, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams244, 128)

static TGParams_1_t ToneGenParams245;
#pragma DATA_SECTION (ToneGenParams245, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams245, 128)

static TGParams_1_t ToneGenParams246;
#pragma DATA_SECTION (ToneGenParams246, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams246, 128)

static TGParams_1_t ToneGenParams247;
#pragma DATA_SECTION (ToneGenParams247, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams247, 128)

static TGParams_1_t ToneGenParams248;
#pragma DATA_SECTION (ToneGenParams248, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams248, 128)

static TGParams_1_t ToneGenParams249;
#pragma DATA_SECTION (ToneGenParams249, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams249, 128)

static TGParams_1_t ToneGenParams250;
#pragma DATA_SECTION (ToneGenParams250, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams250, 128)

static TGParams_1_t ToneGenParams251;
#pragma DATA_SECTION (ToneGenParams251, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams251, 128)

static TGParams_1_t ToneGenParams252;
#pragma DATA_SECTION (ToneGenParams252, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams252, 128)

static TGParams_1_t ToneGenParams253;
#pragma DATA_SECTION (ToneGenParams253, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams253, 128)

static TGParams_1_t ToneGenParams254;
#pragma DATA_SECTION (ToneGenParams254, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams254, 128)

static TGParams_1_t ToneGenParams255;
#pragma DATA_SECTION (ToneGenParams255, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams255, 128)

static TGParams_1_t ToneGenParams256;
#pragma DATA_SECTION (ToneGenParams256, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams256, 128)

static TGParams_1_t ToneGenParams257;
#pragma DATA_SECTION (ToneGenParams257, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams257, 128)

static TGParams_1_t ToneGenParams258;
#pragma DATA_SECTION (ToneGenParams258, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams258, 128)

static TGParams_1_t ToneGenParams259;
#pragma DATA_SECTION (ToneGenParams259, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams259, 128)

static TGParams_1_t ToneGenParams260;
#pragma DATA_SECTION (ToneGenParams260, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams260, 128)

static TGParams_1_t ToneGenParams261;
#pragma DATA_SECTION (ToneGenParams261, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams261, 128)

static TGParams_1_t ToneGenParams262;
#pragma DATA_SECTION (ToneGenParams262, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams262, 128)

static TGParams_1_t ToneGenParams263;
#pragma DATA_SECTION (ToneGenParams263, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams263, 128)

static TGParams_1_t ToneGenParams264;
#pragma DATA_SECTION (ToneGenParams264, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams264, 128)

static TGParams_1_t ToneGenParams265;
#pragma DATA_SECTION (ToneGenParams265, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams265, 128)

static TGParams_1_t ToneGenParams266;
#pragma DATA_SECTION (ToneGenParams266, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams266, 128)

static TGParams_1_t ToneGenParams267;
#pragma DATA_SECTION (ToneGenParams267, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams267, 128)

static TGParams_1_t ToneGenParams268;
#pragma DATA_SECTION (ToneGenParams268, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams268, 128)

static TGParams_1_t ToneGenParams269;
#pragma DATA_SECTION (ToneGenParams269, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams269, 128)

static TGParams_1_t ToneGenParams270;
#pragma DATA_SECTION (ToneGenParams270, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams270, 128)

static TGParams_1_t ToneGenParams271;
#pragma DATA_SECTION (ToneGenParams271, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams271, 128)

static TGParams_1_t ToneGenParams272;
#pragma DATA_SECTION (ToneGenParams272, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams272, 128)

static TGParams_1_t ToneGenParams273;
#pragma DATA_SECTION (ToneGenParams273, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams273, 128)

static TGParams_1_t ToneGenParams274;
#pragma DATA_SECTION (ToneGenParams274, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams274, 128)

static TGParams_1_t ToneGenParams275;
#pragma DATA_SECTION (ToneGenParams275, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams275, 128)

static TGParams_1_t ToneGenParams276;
#pragma DATA_SECTION (ToneGenParams276, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams276, 128)

static TGParams_1_t ToneGenParams277;
#pragma DATA_SECTION (ToneGenParams277, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams277, 128)

static TGParams_1_t ToneGenParams278;
#pragma DATA_SECTION (ToneGenParams278, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams278, 128)

static TGParams_1_t ToneGenParams279;
#pragma DATA_SECTION (ToneGenParams279, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams279, 128)

static TGParams_1_t ToneGenParams280;
#pragma DATA_SECTION (ToneGenParams280, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams280, 128)

static TGParams_1_t ToneGenParams281;
#pragma DATA_SECTION (ToneGenParams281, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams281, 128)

static TGParams_1_t ToneGenParams282;
#pragma DATA_SECTION (ToneGenParams282, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams282, 128)

static TGParams_1_t ToneGenParams283;
#pragma DATA_SECTION (ToneGenParams283, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams283, 128)

static TGParams_1_t ToneGenParams284;
#pragma DATA_SECTION (ToneGenParams284, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams284, 128)

static TGParams_1_t ToneGenParams285;
#pragma DATA_SECTION (ToneGenParams285, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams285, 128)

static TGParams_1_t ToneGenParams286;
#pragma DATA_SECTION (ToneGenParams286, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams286, 128)

static TGParams_1_t ToneGenParams287;
#pragma DATA_SECTION (ToneGenParams287, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams287, 128)

static TGParams_1_t ToneGenParams288;
#pragma DATA_SECTION (ToneGenParams288, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams288, 128)

static TGParams_1_t ToneGenParams289;
#pragma DATA_SECTION (ToneGenParams289, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams289, 128)

static TGParams_1_t ToneGenParams290;
#pragma DATA_SECTION (ToneGenParams290, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams290, 128)

static TGParams_1_t ToneGenParams291;
#pragma DATA_SECTION (ToneGenParams291, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams291, 128)

static TGParams_1_t ToneGenParams292;
#pragma DATA_SECTION (ToneGenParams292, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams292, 128)

static TGParams_1_t ToneGenParams293;
#pragma DATA_SECTION (ToneGenParams293, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams293, 128)

static TGParams_1_t ToneGenParams294;
#pragma DATA_SECTION (ToneGenParams294, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams294, 128)

static TGParams_1_t ToneGenParams295;
#pragma DATA_SECTION (ToneGenParams295, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams295, 128)

static TGParams_1_t ToneGenParams296;
#pragma DATA_SECTION (ToneGenParams296, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams296, 128)

static TGParams_1_t ToneGenParams297;
#pragma DATA_SECTION (ToneGenParams297, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams297, 128)

static TGParams_1_t ToneGenParams298;
#pragma DATA_SECTION (ToneGenParams298, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams298, 128)

static TGParams_1_t ToneGenParams299;
#pragma DATA_SECTION (ToneGenParams299, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams299, 128)

static TGParams_1_t ToneGenParams300;
#pragma DATA_SECTION (ToneGenParams300, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams300, 128)

static TGParams_1_t ToneGenParams301;
#pragma DATA_SECTION (ToneGenParams301, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams301, 128)

static TGParams_1_t ToneGenParams302;
#pragma DATA_SECTION (ToneGenParams302, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams302, 128)

static TGParams_1_t ToneGenParams303;
#pragma DATA_SECTION (ToneGenParams303, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams303, 128)

static TGParams_1_t ToneGenParams304;
#pragma DATA_SECTION (ToneGenParams304, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams304, 128)

static TGParams_1_t ToneGenParams305;
#pragma DATA_SECTION (ToneGenParams305, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams305, 128)

static TGParams_1_t ToneGenParams306;
#pragma DATA_SECTION (ToneGenParams306, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams306, 128)

static TGParams_1_t ToneGenParams307;
#pragma DATA_SECTION (ToneGenParams307, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams307, 128)

static TGParams_1_t ToneGenParams308;
#pragma DATA_SECTION (ToneGenParams308, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams308, 128)

static TGParams_1_t ToneGenParams309;
#pragma DATA_SECTION (ToneGenParams309, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams309, 128)

static TGParams_1_t ToneGenParams310;
#pragma DATA_SECTION (ToneGenParams310, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams310, 128)

static TGParams_1_t ToneGenParams311;
#pragma DATA_SECTION (ToneGenParams311, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams311, 128)

static TGParams_1_t ToneGenParams312;
#pragma DATA_SECTION (ToneGenParams312, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams312, 128)

static TGParams_1_t ToneGenParams313;
#pragma DATA_SECTION (ToneGenParams313, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams313, 128)

static TGParams_1_t ToneGenParams314;
#pragma DATA_SECTION (ToneGenParams314, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams314, 128)

static TGParams_1_t ToneGenParams315;
#pragma DATA_SECTION (ToneGenParams315, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams315, 128)

static TGParams_1_t ToneGenParams316;
#pragma DATA_SECTION (ToneGenParams316, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams316, 128)

static TGParams_1_t ToneGenParams317;
#pragma DATA_SECTION (ToneGenParams317, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams317, 128)

static TGParams_1_t ToneGenParams318;
#pragma DATA_SECTION (ToneGenParams318, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams318, 128)

static TGParams_1_t ToneGenParams319;
#pragma DATA_SECTION (ToneGenParams319, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneGenParams319, 128)

TGParams_1_t* const ToneGenParams[] = {
	&ToneGenParams0,
	&ToneGenParams1,
	&ToneGenParams2,
	&ToneGenParams3,
	&ToneGenParams4,
	&ToneGenParams5,
	&ToneGenParams6,
	&ToneGenParams7,
	&ToneGenParams8,
	&ToneGenParams9,
	&ToneGenParams10,
	&ToneGenParams11,
	&ToneGenParams12,
	&ToneGenParams13,
	&ToneGenParams14,
	&ToneGenParams15,
	&ToneGenParams16,
	&ToneGenParams17,
	&ToneGenParams18,
	&ToneGenParams19,
	&ToneGenParams20,
	&ToneGenParams21,
	&ToneGenParams22,
	&ToneGenParams23,
	&ToneGenParams24,
	&ToneGenParams25,
	&ToneGenParams26,
	&ToneGenParams27,
	&ToneGenParams28,
	&ToneGenParams29,
	&ToneGenParams30,
	&ToneGenParams31,
	&ToneGenParams32,
	&ToneGenParams33,
	&ToneGenParams34,
	&ToneGenParams35,
	&ToneGenParams36,
	&ToneGenParams37,
	&ToneGenParams38,
	&ToneGenParams39,
	&ToneGenParams40,
	&ToneGenParams41,
	&ToneGenParams42,
	&ToneGenParams43,
	&ToneGenParams44,
	&ToneGenParams45,
	&ToneGenParams46,
	&ToneGenParams47,
	&ToneGenParams48,
	&ToneGenParams49,
	&ToneGenParams50,
	&ToneGenParams51,
	&ToneGenParams52,
	&ToneGenParams53,
	&ToneGenParams54,
	&ToneGenParams55,
	&ToneGenParams56,
	&ToneGenParams57,
	&ToneGenParams58,
	&ToneGenParams59,
	&ToneGenParams60,
	&ToneGenParams61,
	&ToneGenParams62,
	&ToneGenParams63,
	&ToneGenParams64,
	&ToneGenParams65,
	&ToneGenParams66,
	&ToneGenParams67,
	&ToneGenParams68,
	&ToneGenParams69,
	&ToneGenParams70,
	&ToneGenParams71,
	&ToneGenParams72,
	&ToneGenParams73,
	&ToneGenParams74,
	&ToneGenParams75,
	&ToneGenParams76,
	&ToneGenParams77,
	&ToneGenParams78,
	&ToneGenParams79,
	&ToneGenParams80,
	&ToneGenParams81,
	&ToneGenParams82,
	&ToneGenParams83,
	&ToneGenParams84,
	&ToneGenParams85,
	&ToneGenParams86,
	&ToneGenParams87,
	&ToneGenParams88,
	&ToneGenParams89,
	&ToneGenParams90,
	&ToneGenParams91,
	&ToneGenParams92,
	&ToneGenParams93,
	&ToneGenParams94,
	&ToneGenParams95,
	&ToneGenParams96,
	&ToneGenParams97,
	&ToneGenParams98,
	&ToneGenParams99,
	&ToneGenParams100,
	&ToneGenParams101,
	&ToneGenParams102,
	&ToneGenParams103,
	&ToneGenParams104,
	&ToneGenParams105,
	&ToneGenParams106,
	&ToneGenParams107,
	&ToneGenParams108,
	&ToneGenParams109,
	&ToneGenParams110,
	&ToneGenParams111,
	&ToneGenParams112,
	&ToneGenParams113,
	&ToneGenParams114,
	&ToneGenParams115,
	&ToneGenParams116,
	&ToneGenParams117,
	&ToneGenParams118,
	&ToneGenParams119,
	&ToneGenParams120,
	&ToneGenParams121,
	&ToneGenParams122,
	&ToneGenParams123,
	&ToneGenParams124,
	&ToneGenParams125,
	&ToneGenParams126,
	&ToneGenParams127,
	&ToneGenParams128,
	&ToneGenParams129,
	&ToneGenParams130,
	&ToneGenParams131,
	&ToneGenParams132,
	&ToneGenParams133,
	&ToneGenParams134,
	&ToneGenParams135,
	&ToneGenParams136,
	&ToneGenParams137,
	&ToneGenParams138,
	&ToneGenParams139,
	&ToneGenParams140,
	&ToneGenParams141,
	&ToneGenParams142,
	&ToneGenParams143,
	&ToneGenParams144,
	&ToneGenParams145,
	&ToneGenParams146,
	&ToneGenParams147,
	&ToneGenParams148,
	&ToneGenParams149,
	&ToneGenParams150,
	&ToneGenParams151,
	&ToneGenParams152,
	&ToneGenParams153,
	&ToneGenParams154,
	&ToneGenParams155,
	&ToneGenParams156,
	&ToneGenParams157,
	&ToneGenParams158,
	&ToneGenParams159,
	&ToneGenParams160,
	&ToneGenParams161,
	&ToneGenParams162,
	&ToneGenParams163,
	&ToneGenParams164,
	&ToneGenParams165,
	&ToneGenParams166,
	&ToneGenParams167,
	&ToneGenParams168,
	&ToneGenParams169,
	&ToneGenParams170,
	&ToneGenParams171,
	&ToneGenParams172,
	&ToneGenParams173,
	&ToneGenParams174,
	&ToneGenParams175,
	&ToneGenParams176,
	&ToneGenParams177,
	&ToneGenParams178,
	&ToneGenParams179,
	&ToneGenParams180,
	&ToneGenParams181,
	&ToneGenParams182,
	&ToneGenParams183,
	&ToneGenParams184,
	&ToneGenParams185,
	&ToneGenParams186,
	&ToneGenParams187,
	&ToneGenParams188,
	&ToneGenParams189,
	&ToneGenParams190,
	&ToneGenParams191,
	&ToneGenParams192,
	&ToneGenParams193,
	&ToneGenParams194,
	&ToneGenParams195,
	&ToneGenParams196,
	&ToneGenParams197,
	&ToneGenParams198,
	&ToneGenParams199,
	&ToneGenParams200,
	&ToneGenParams201,
	&ToneGenParams202,
	&ToneGenParams203,
	&ToneGenParams204,
	&ToneGenParams205,
	&ToneGenParams206,
	&ToneGenParams207,
	&ToneGenParams208,
	&ToneGenParams209,
	&ToneGenParams210,
	&ToneGenParams211,
	&ToneGenParams212,
	&ToneGenParams213,
	&ToneGenParams214,
	&ToneGenParams215,
	&ToneGenParams216,
	&ToneGenParams217,
	&ToneGenParams218,
	&ToneGenParams219,
	&ToneGenParams220,
	&ToneGenParams221,
	&ToneGenParams222,
	&ToneGenParams223,
	&ToneGenParams224,
	&ToneGenParams225,
	&ToneGenParams226,
	&ToneGenParams227,
	&ToneGenParams228,
	&ToneGenParams229,
	&ToneGenParams230,
	&ToneGenParams231,
	&ToneGenParams232,
	&ToneGenParams233,
	&ToneGenParams234,
	&ToneGenParams235,
	&ToneGenParams236,
	&ToneGenParams237,
	&ToneGenParams238,
	&ToneGenParams239,
	&ToneGenParams240,
	&ToneGenParams241,
	&ToneGenParams242,
	&ToneGenParams243,
	&ToneGenParams244,
	&ToneGenParams245,
	&ToneGenParams246,
	&ToneGenParams247,
	&ToneGenParams248,
	&ToneGenParams249,
	&ToneGenParams250,
	&ToneGenParams251,
	&ToneGenParams252,
	&ToneGenParams253,
	&ToneGenParams254,
	&ToneGenParams255,
	&ToneGenParams256,
	&ToneGenParams257,
	&ToneGenParams258,
	&ToneGenParams259,
	&ToneGenParams260,
	&ToneGenParams261,
	&ToneGenParams262,
	&ToneGenParams263,
	&ToneGenParams264,
	&ToneGenParams265,
	&ToneGenParams266,
	&ToneGenParams267,
	&ToneGenParams268,
	&ToneGenParams269,
	&ToneGenParams270,
	&ToneGenParams271,
	&ToneGenParams272,
	&ToneGenParams273,
	&ToneGenParams274,
	&ToneGenParams275,
	&ToneGenParams276,
	&ToneGenParams277,
	&ToneGenParams278,
	&ToneGenParams279,
	&ToneGenParams280,
	&ToneGenParams281,
	&ToneGenParams282,
	&ToneGenParams283,
	&ToneGenParams284,
	&ToneGenParams285,
	&ToneGenParams286,
	&ToneGenParams287,
	&ToneGenParams288,
	&ToneGenParams289,
	&ToneGenParams290,
	&ToneGenParams291,
	&ToneGenParams292,
	&ToneGenParams293,
	&ToneGenParams294,
	&ToneGenParams295,
	&ToneGenParams296,
	&ToneGenParams297,
	&ToneGenParams298,
	&ToneGenParams299,
	&ToneGenParams300,
	&ToneGenParams301,
	&ToneGenParams302,
	&ToneGenParams303,
	&ToneGenParams304,
	&ToneGenParams305,
	&ToneGenParams306,
	&ToneGenParams307,
	&ToneGenParams308,
	&ToneGenParams309,
	&ToneGenParams310,
	&ToneGenParams311,
	&ToneGenParams312,
	&ToneGenParams313,
	&ToneGenParams314,
	&ToneGenParams315,
	&ToneGenParams316,
	&ToneGenParams317,
	&ToneGenParams318,
	&ToneGenParams319
};
//}

//{         DtmfDialInfo_t     DtmfDialInstance STRUCTURES to CHAN_INST_DATA:DtmfDial

DtmfDialInfo_t DtmfDialInstance0;
#pragma DATA_SECTION (DtmfDialInstance0, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance0, 128)

static DtmfDialInfo_t DtmfDialInstance1;
#pragma DATA_SECTION (DtmfDialInstance1, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance1, 128)

static DtmfDialInfo_t DtmfDialInstance2;
#pragma DATA_SECTION (DtmfDialInstance2, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance2, 128)

static DtmfDialInfo_t DtmfDialInstance3;
#pragma DATA_SECTION (DtmfDialInstance3, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance3, 128)

static DtmfDialInfo_t DtmfDialInstance4;
#pragma DATA_SECTION (DtmfDialInstance4, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance4, 128)

static DtmfDialInfo_t DtmfDialInstance5;
#pragma DATA_SECTION (DtmfDialInstance5, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance5, 128)

static DtmfDialInfo_t DtmfDialInstance6;
#pragma DATA_SECTION (DtmfDialInstance6, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance6, 128)

static DtmfDialInfo_t DtmfDialInstance7;
#pragma DATA_SECTION (DtmfDialInstance7, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance7, 128)

static DtmfDialInfo_t DtmfDialInstance8;
#pragma DATA_SECTION (DtmfDialInstance8, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance8, 128)

static DtmfDialInfo_t DtmfDialInstance9;
#pragma DATA_SECTION (DtmfDialInstance9, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance9, 128)

static DtmfDialInfo_t DtmfDialInstance10;
#pragma DATA_SECTION (DtmfDialInstance10, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance10, 128)

static DtmfDialInfo_t DtmfDialInstance11;
#pragma DATA_SECTION (DtmfDialInstance11, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance11, 128)

static DtmfDialInfo_t DtmfDialInstance12;
#pragma DATA_SECTION (DtmfDialInstance12, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance12, 128)

static DtmfDialInfo_t DtmfDialInstance13;
#pragma DATA_SECTION (DtmfDialInstance13, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance13, 128)

static DtmfDialInfo_t DtmfDialInstance14;
#pragma DATA_SECTION (DtmfDialInstance14, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance14, 128)

static DtmfDialInfo_t DtmfDialInstance15;
#pragma DATA_SECTION (DtmfDialInstance15, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance15, 128)

static DtmfDialInfo_t DtmfDialInstance16;
#pragma DATA_SECTION (DtmfDialInstance16, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance16, 128)

static DtmfDialInfo_t DtmfDialInstance17;
#pragma DATA_SECTION (DtmfDialInstance17, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance17, 128)

static DtmfDialInfo_t DtmfDialInstance18;
#pragma DATA_SECTION (DtmfDialInstance18, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance18, 128)

static DtmfDialInfo_t DtmfDialInstance19;
#pragma DATA_SECTION (DtmfDialInstance19, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance19, 128)

static DtmfDialInfo_t DtmfDialInstance20;
#pragma DATA_SECTION (DtmfDialInstance20, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance20, 128)

static DtmfDialInfo_t DtmfDialInstance21;
#pragma DATA_SECTION (DtmfDialInstance21, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance21, 128)

static DtmfDialInfo_t DtmfDialInstance22;
#pragma DATA_SECTION (DtmfDialInstance22, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance22, 128)

static DtmfDialInfo_t DtmfDialInstance23;
#pragma DATA_SECTION (DtmfDialInstance23, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance23, 128)

static DtmfDialInfo_t DtmfDialInstance24;
#pragma DATA_SECTION (DtmfDialInstance24, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance24, 128)

static DtmfDialInfo_t DtmfDialInstance25;
#pragma DATA_SECTION (DtmfDialInstance25, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance25, 128)

static DtmfDialInfo_t DtmfDialInstance26;
#pragma DATA_SECTION (DtmfDialInstance26, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance26, 128)

static DtmfDialInfo_t DtmfDialInstance27;
#pragma DATA_SECTION (DtmfDialInstance27, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance27, 128)

static DtmfDialInfo_t DtmfDialInstance28;
#pragma DATA_SECTION (DtmfDialInstance28, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance28, 128)

static DtmfDialInfo_t DtmfDialInstance29;
#pragma DATA_SECTION (DtmfDialInstance29, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance29, 128)

static DtmfDialInfo_t DtmfDialInstance30;
#pragma DATA_SECTION (DtmfDialInstance30, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance30, 128)

static DtmfDialInfo_t DtmfDialInstance31;
#pragma DATA_SECTION (DtmfDialInstance31, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance31, 128)

static DtmfDialInfo_t DtmfDialInstance32;
#pragma DATA_SECTION (DtmfDialInstance32, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance32, 128)

static DtmfDialInfo_t DtmfDialInstance33;
#pragma DATA_SECTION (DtmfDialInstance33, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance33, 128)

static DtmfDialInfo_t DtmfDialInstance34;
#pragma DATA_SECTION (DtmfDialInstance34, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance34, 128)

static DtmfDialInfo_t DtmfDialInstance35;
#pragma DATA_SECTION (DtmfDialInstance35, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance35, 128)

static DtmfDialInfo_t DtmfDialInstance36;
#pragma DATA_SECTION (DtmfDialInstance36, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance36, 128)

static DtmfDialInfo_t DtmfDialInstance37;
#pragma DATA_SECTION (DtmfDialInstance37, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance37, 128)

static DtmfDialInfo_t DtmfDialInstance38;
#pragma DATA_SECTION (DtmfDialInstance38, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance38, 128)

static DtmfDialInfo_t DtmfDialInstance39;
#pragma DATA_SECTION (DtmfDialInstance39, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance39, 128)

static DtmfDialInfo_t DtmfDialInstance40;
#pragma DATA_SECTION (DtmfDialInstance40, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance40, 128)

static DtmfDialInfo_t DtmfDialInstance41;
#pragma DATA_SECTION (DtmfDialInstance41, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance41, 128)

static DtmfDialInfo_t DtmfDialInstance42;
#pragma DATA_SECTION (DtmfDialInstance42, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance42, 128)

static DtmfDialInfo_t DtmfDialInstance43;
#pragma DATA_SECTION (DtmfDialInstance43, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance43, 128)

static DtmfDialInfo_t DtmfDialInstance44;
#pragma DATA_SECTION (DtmfDialInstance44, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance44, 128)

static DtmfDialInfo_t DtmfDialInstance45;
#pragma DATA_SECTION (DtmfDialInstance45, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance45, 128)

static DtmfDialInfo_t DtmfDialInstance46;
#pragma DATA_SECTION (DtmfDialInstance46, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance46, 128)

static DtmfDialInfo_t DtmfDialInstance47;
#pragma DATA_SECTION (DtmfDialInstance47, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance47, 128)

static DtmfDialInfo_t DtmfDialInstance48;
#pragma DATA_SECTION (DtmfDialInstance48, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance48, 128)

static DtmfDialInfo_t DtmfDialInstance49;
#pragma DATA_SECTION (DtmfDialInstance49, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance49, 128)

static DtmfDialInfo_t DtmfDialInstance50;
#pragma DATA_SECTION (DtmfDialInstance50, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance50, 128)

static DtmfDialInfo_t DtmfDialInstance51;
#pragma DATA_SECTION (DtmfDialInstance51, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance51, 128)

static DtmfDialInfo_t DtmfDialInstance52;
#pragma DATA_SECTION (DtmfDialInstance52, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance52, 128)

static DtmfDialInfo_t DtmfDialInstance53;
#pragma DATA_SECTION (DtmfDialInstance53, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance53, 128)

static DtmfDialInfo_t DtmfDialInstance54;
#pragma DATA_SECTION (DtmfDialInstance54, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance54, 128)

static DtmfDialInfo_t DtmfDialInstance55;
#pragma DATA_SECTION (DtmfDialInstance55, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance55, 128)

static DtmfDialInfo_t DtmfDialInstance56;
#pragma DATA_SECTION (DtmfDialInstance56, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance56, 128)

static DtmfDialInfo_t DtmfDialInstance57;
#pragma DATA_SECTION (DtmfDialInstance57, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance57, 128)

static DtmfDialInfo_t DtmfDialInstance58;
#pragma DATA_SECTION (DtmfDialInstance58, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance58, 128)

static DtmfDialInfo_t DtmfDialInstance59;
#pragma DATA_SECTION (DtmfDialInstance59, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance59, 128)

static DtmfDialInfo_t DtmfDialInstance60;
#pragma DATA_SECTION (DtmfDialInstance60, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance60, 128)

static DtmfDialInfo_t DtmfDialInstance61;
#pragma DATA_SECTION (DtmfDialInstance61, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance61, 128)

static DtmfDialInfo_t DtmfDialInstance62;
#pragma DATA_SECTION (DtmfDialInstance62, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance62, 128)

static DtmfDialInfo_t DtmfDialInstance63;
#pragma DATA_SECTION (DtmfDialInstance63, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance63, 128)

static DtmfDialInfo_t DtmfDialInstance64;
#pragma DATA_SECTION (DtmfDialInstance64, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance64, 128)

static DtmfDialInfo_t DtmfDialInstance65;
#pragma DATA_SECTION (DtmfDialInstance65, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance65, 128)

static DtmfDialInfo_t DtmfDialInstance66;
#pragma DATA_SECTION (DtmfDialInstance66, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance66, 128)

static DtmfDialInfo_t DtmfDialInstance67;
#pragma DATA_SECTION (DtmfDialInstance67, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance67, 128)

static DtmfDialInfo_t DtmfDialInstance68;
#pragma DATA_SECTION (DtmfDialInstance68, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance68, 128)

static DtmfDialInfo_t DtmfDialInstance69;
#pragma DATA_SECTION (DtmfDialInstance69, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance69, 128)

static DtmfDialInfo_t DtmfDialInstance70;
#pragma DATA_SECTION (DtmfDialInstance70, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance70, 128)

static DtmfDialInfo_t DtmfDialInstance71;
#pragma DATA_SECTION (DtmfDialInstance71, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance71, 128)

static DtmfDialInfo_t DtmfDialInstance72;
#pragma DATA_SECTION (DtmfDialInstance72, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance72, 128)

static DtmfDialInfo_t DtmfDialInstance73;
#pragma DATA_SECTION (DtmfDialInstance73, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance73, 128)

static DtmfDialInfo_t DtmfDialInstance74;
#pragma DATA_SECTION (DtmfDialInstance74, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance74, 128)

static DtmfDialInfo_t DtmfDialInstance75;
#pragma DATA_SECTION (DtmfDialInstance75, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance75, 128)

static DtmfDialInfo_t DtmfDialInstance76;
#pragma DATA_SECTION (DtmfDialInstance76, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance76, 128)

static DtmfDialInfo_t DtmfDialInstance77;
#pragma DATA_SECTION (DtmfDialInstance77, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance77, 128)

static DtmfDialInfo_t DtmfDialInstance78;
#pragma DATA_SECTION (DtmfDialInstance78, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance78, 128)

static DtmfDialInfo_t DtmfDialInstance79;
#pragma DATA_SECTION (DtmfDialInstance79, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance79, 128)

static DtmfDialInfo_t DtmfDialInstance80;
#pragma DATA_SECTION (DtmfDialInstance80, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance80, 128)

static DtmfDialInfo_t DtmfDialInstance81;
#pragma DATA_SECTION (DtmfDialInstance81, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance81, 128)

static DtmfDialInfo_t DtmfDialInstance82;
#pragma DATA_SECTION (DtmfDialInstance82, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance82, 128)

static DtmfDialInfo_t DtmfDialInstance83;
#pragma DATA_SECTION (DtmfDialInstance83, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance83, 128)

static DtmfDialInfo_t DtmfDialInstance84;
#pragma DATA_SECTION (DtmfDialInstance84, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance84, 128)

static DtmfDialInfo_t DtmfDialInstance85;
#pragma DATA_SECTION (DtmfDialInstance85, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance85, 128)

static DtmfDialInfo_t DtmfDialInstance86;
#pragma DATA_SECTION (DtmfDialInstance86, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance86, 128)

static DtmfDialInfo_t DtmfDialInstance87;
#pragma DATA_SECTION (DtmfDialInstance87, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance87, 128)

static DtmfDialInfo_t DtmfDialInstance88;
#pragma DATA_SECTION (DtmfDialInstance88, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance88, 128)

static DtmfDialInfo_t DtmfDialInstance89;
#pragma DATA_SECTION (DtmfDialInstance89, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance89, 128)

static DtmfDialInfo_t DtmfDialInstance90;
#pragma DATA_SECTION (DtmfDialInstance90, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance90, 128)

static DtmfDialInfo_t DtmfDialInstance91;
#pragma DATA_SECTION (DtmfDialInstance91, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance91, 128)

static DtmfDialInfo_t DtmfDialInstance92;
#pragma DATA_SECTION (DtmfDialInstance92, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance92, 128)

static DtmfDialInfo_t DtmfDialInstance93;
#pragma DATA_SECTION (DtmfDialInstance93, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance93, 128)

static DtmfDialInfo_t DtmfDialInstance94;
#pragma DATA_SECTION (DtmfDialInstance94, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance94, 128)

static DtmfDialInfo_t DtmfDialInstance95;
#pragma DATA_SECTION (DtmfDialInstance95, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance95, 128)

static DtmfDialInfo_t DtmfDialInstance96;
#pragma DATA_SECTION (DtmfDialInstance96, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance96, 128)

static DtmfDialInfo_t DtmfDialInstance97;
#pragma DATA_SECTION (DtmfDialInstance97, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance97, 128)

static DtmfDialInfo_t DtmfDialInstance98;
#pragma DATA_SECTION (DtmfDialInstance98, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance98, 128)

static DtmfDialInfo_t DtmfDialInstance99;
#pragma DATA_SECTION (DtmfDialInstance99, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance99, 128)

static DtmfDialInfo_t DtmfDialInstance100;
#pragma DATA_SECTION (DtmfDialInstance100, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance100, 128)

static DtmfDialInfo_t DtmfDialInstance101;
#pragma DATA_SECTION (DtmfDialInstance101, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance101, 128)

static DtmfDialInfo_t DtmfDialInstance102;
#pragma DATA_SECTION (DtmfDialInstance102, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance102, 128)

static DtmfDialInfo_t DtmfDialInstance103;
#pragma DATA_SECTION (DtmfDialInstance103, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance103, 128)

static DtmfDialInfo_t DtmfDialInstance104;
#pragma DATA_SECTION (DtmfDialInstance104, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance104, 128)

static DtmfDialInfo_t DtmfDialInstance105;
#pragma DATA_SECTION (DtmfDialInstance105, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance105, 128)

static DtmfDialInfo_t DtmfDialInstance106;
#pragma DATA_SECTION (DtmfDialInstance106, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance106, 128)

static DtmfDialInfo_t DtmfDialInstance107;
#pragma DATA_SECTION (DtmfDialInstance107, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance107, 128)

static DtmfDialInfo_t DtmfDialInstance108;
#pragma DATA_SECTION (DtmfDialInstance108, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance108, 128)

static DtmfDialInfo_t DtmfDialInstance109;
#pragma DATA_SECTION (DtmfDialInstance109, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance109, 128)

static DtmfDialInfo_t DtmfDialInstance110;
#pragma DATA_SECTION (DtmfDialInstance110, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance110, 128)

static DtmfDialInfo_t DtmfDialInstance111;
#pragma DATA_SECTION (DtmfDialInstance111, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance111, 128)

static DtmfDialInfo_t DtmfDialInstance112;
#pragma DATA_SECTION (DtmfDialInstance112, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance112, 128)

static DtmfDialInfo_t DtmfDialInstance113;
#pragma DATA_SECTION (DtmfDialInstance113, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance113, 128)

static DtmfDialInfo_t DtmfDialInstance114;
#pragma DATA_SECTION (DtmfDialInstance114, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance114, 128)

static DtmfDialInfo_t DtmfDialInstance115;
#pragma DATA_SECTION (DtmfDialInstance115, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance115, 128)

static DtmfDialInfo_t DtmfDialInstance116;
#pragma DATA_SECTION (DtmfDialInstance116, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance116, 128)

static DtmfDialInfo_t DtmfDialInstance117;
#pragma DATA_SECTION (DtmfDialInstance117, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance117, 128)

static DtmfDialInfo_t DtmfDialInstance118;
#pragma DATA_SECTION (DtmfDialInstance118, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance118, 128)

static DtmfDialInfo_t DtmfDialInstance119;
#pragma DATA_SECTION (DtmfDialInstance119, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance119, 128)

static DtmfDialInfo_t DtmfDialInstance120;
#pragma DATA_SECTION (DtmfDialInstance120, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance120, 128)

static DtmfDialInfo_t DtmfDialInstance121;
#pragma DATA_SECTION (DtmfDialInstance121, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance121, 128)

static DtmfDialInfo_t DtmfDialInstance122;
#pragma DATA_SECTION (DtmfDialInstance122, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance122, 128)

static DtmfDialInfo_t DtmfDialInstance123;
#pragma DATA_SECTION (DtmfDialInstance123, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance123, 128)

static DtmfDialInfo_t DtmfDialInstance124;
#pragma DATA_SECTION (DtmfDialInstance124, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance124, 128)

static DtmfDialInfo_t DtmfDialInstance125;
#pragma DATA_SECTION (DtmfDialInstance125, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance125, 128)

static DtmfDialInfo_t DtmfDialInstance126;
#pragma DATA_SECTION (DtmfDialInstance126, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance126, 128)

static DtmfDialInfo_t DtmfDialInstance127;
#pragma DATA_SECTION (DtmfDialInstance127, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance127, 128)

static DtmfDialInfo_t DtmfDialInstance128;
#pragma DATA_SECTION (DtmfDialInstance128, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance128, 128)

static DtmfDialInfo_t DtmfDialInstance129;
#pragma DATA_SECTION (DtmfDialInstance129, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance129, 128)

static DtmfDialInfo_t DtmfDialInstance130;
#pragma DATA_SECTION (DtmfDialInstance130, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance130, 128)

static DtmfDialInfo_t DtmfDialInstance131;
#pragma DATA_SECTION (DtmfDialInstance131, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance131, 128)

static DtmfDialInfo_t DtmfDialInstance132;
#pragma DATA_SECTION (DtmfDialInstance132, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance132, 128)

static DtmfDialInfo_t DtmfDialInstance133;
#pragma DATA_SECTION (DtmfDialInstance133, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance133, 128)

static DtmfDialInfo_t DtmfDialInstance134;
#pragma DATA_SECTION (DtmfDialInstance134, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance134, 128)

static DtmfDialInfo_t DtmfDialInstance135;
#pragma DATA_SECTION (DtmfDialInstance135, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance135, 128)

static DtmfDialInfo_t DtmfDialInstance136;
#pragma DATA_SECTION (DtmfDialInstance136, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance136, 128)

static DtmfDialInfo_t DtmfDialInstance137;
#pragma DATA_SECTION (DtmfDialInstance137, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance137, 128)

static DtmfDialInfo_t DtmfDialInstance138;
#pragma DATA_SECTION (DtmfDialInstance138, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance138, 128)

static DtmfDialInfo_t DtmfDialInstance139;
#pragma DATA_SECTION (DtmfDialInstance139, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance139, 128)

static DtmfDialInfo_t DtmfDialInstance140;
#pragma DATA_SECTION (DtmfDialInstance140, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance140, 128)

static DtmfDialInfo_t DtmfDialInstance141;
#pragma DATA_SECTION (DtmfDialInstance141, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance141, 128)

static DtmfDialInfo_t DtmfDialInstance142;
#pragma DATA_SECTION (DtmfDialInstance142, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance142, 128)

static DtmfDialInfo_t DtmfDialInstance143;
#pragma DATA_SECTION (DtmfDialInstance143, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance143, 128)

static DtmfDialInfo_t DtmfDialInstance144;
#pragma DATA_SECTION (DtmfDialInstance144, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance144, 128)

static DtmfDialInfo_t DtmfDialInstance145;
#pragma DATA_SECTION (DtmfDialInstance145, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance145, 128)

static DtmfDialInfo_t DtmfDialInstance146;
#pragma DATA_SECTION (DtmfDialInstance146, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance146, 128)

static DtmfDialInfo_t DtmfDialInstance147;
#pragma DATA_SECTION (DtmfDialInstance147, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance147, 128)

static DtmfDialInfo_t DtmfDialInstance148;
#pragma DATA_SECTION (DtmfDialInstance148, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance148, 128)

static DtmfDialInfo_t DtmfDialInstance149;
#pragma DATA_SECTION (DtmfDialInstance149, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance149, 128)

static DtmfDialInfo_t DtmfDialInstance150;
#pragma DATA_SECTION (DtmfDialInstance150, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance150, 128)

static DtmfDialInfo_t DtmfDialInstance151;
#pragma DATA_SECTION (DtmfDialInstance151, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance151, 128)

static DtmfDialInfo_t DtmfDialInstance152;
#pragma DATA_SECTION (DtmfDialInstance152, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance152, 128)

static DtmfDialInfo_t DtmfDialInstance153;
#pragma DATA_SECTION (DtmfDialInstance153, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance153, 128)

static DtmfDialInfo_t DtmfDialInstance154;
#pragma DATA_SECTION (DtmfDialInstance154, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance154, 128)

static DtmfDialInfo_t DtmfDialInstance155;
#pragma DATA_SECTION (DtmfDialInstance155, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance155, 128)

static DtmfDialInfo_t DtmfDialInstance156;
#pragma DATA_SECTION (DtmfDialInstance156, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance156, 128)

static DtmfDialInfo_t DtmfDialInstance157;
#pragma DATA_SECTION (DtmfDialInstance157, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance157, 128)

static DtmfDialInfo_t DtmfDialInstance158;
#pragma DATA_SECTION (DtmfDialInstance158, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance158, 128)

static DtmfDialInfo_t DtmfDialInstance159;
#pragma DATA_SECTION (DtmfDialInstance159, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance159, 128)

static DtmfDialInfo_t DtmfDialInstance160;
#pragma DATA_SECTION (DtmfDialInstance160, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance160, 128)

static DtmfDialInfo_t DtmfDialInstance161;
#pragma DATA_SECTION (DtmfDialInstance161, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance161, 128)

static DtmfDialInfo_t DtmfDialInstance162;
#pragma DATA_SECTION (DtmfDialInstance162, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance162, 128)

static DtmfDialInfo_t DtmfDialInstance163;
#pragma DATA_SECTION (DtmfDialInstance163, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance163, 128)

static DtmfDialInfo_t DtmfDialInstance164;
#pragma DATA_SECTION (DtmfDialInstance164, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance164, 128)

static DtmfDialInfo_t DtmfDialInstance165;
#pragma DATA_SECTION (DtmfDialInstance165, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance165, 128)

static DtmfDialInfo_t DtmfDialInstance166;
#pragma DATA_SECTION (DtmfDialInstance166, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance166, 128)

static DtmfDialInfo_t DtmfDialInstance167;
#pragma DATA_SECTION (DtmfDialInstance167, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance167, 128)

static DtmfDialInfo_t DtmfDialInstance168;
#pragma DATA_SECTION (DtmfDialInstance168, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance168, 128)

static DtmfDialInfo_t DtmfDialInstance169;
#pragma DATA_SECTION (DtmfDialInstance169, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance169, 128)

static DtmfDialInfo_t DtmfDialInstance170;
#pragma DATA_SECTION (DtmfDialInstance170, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance170, 128)

static DtmfDialInfo_t DtmfDialInstance171;
#pragma DATA_SECTION (DtmfDialInstance171, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance171, 128)

static DtmfDialInfo_t DtmfDialInstance172;
#pragma DATA_SECTION (DtmfDialInstance172, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance172, 128)

static DtmfDialInfo_t DtmfDialInstance173;
#pragma DATA_SECTION (DtmfDialInstance173, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance173, 128)

static DtmfDialInfo_t DtmfDialInstance174;
#pragma DATA_SECTION (DtmfDialInstance174, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance174, 128)

static DtmfDialInfo_t DtmfDialInstance175;
#pragma DATA_SECTION (DtmfDialInstance175, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance175, 128)

static DtmfDialInfo_t DtmfDialInstance176;
#pragma DATA_SECTION (DtmfDialInstance176, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance176, 128)

static DtmfDialInfo_t DtmfDialInstance177;
#pragma DATA_SECTION (DtmfDialInstance177, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance177, 128)

static DtmfDialInfo_t DtmfDialInstance178;
#pragma DATA_SECTION (DtmfDialInstance178, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance178, 128)

static DtmfDialInfo_t DtmfDialInstance179;
#pragma DATA_SECTION (DtmfDialInstance179, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance179, 128)

static DtmfDialInfo_t DtmfDialInstance180;
#pragma DATA_SECTION (DtmfDialInstance180, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance180, 128)

static DtmfDialInfo_t DtmfDialInstance181;
#pragma DATA_SECTION (DtmfDialInstance181, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance181, 128)

static DtmfDialInfo_t DtmfDialInstance182;
#pragma DATA_SECTION (DtmfDialInstance182, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance182, 128)

static DtmfDialInfo_t DtmfDialInstance183;
#pragma DATA_SECTION (DtmfDialInstance183, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance183, 128)

static DtmfDialInfo_t DtmfDialInstance184;
#pragma DATA_SECTION (DtmfDialInstance184, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance184, 128)

static DtmfDialInfo_t DtmfDialInstance185;
#pragma DATA_SECTION (DtmfDialInstance185, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance185, 128)

static DtmfDialInfo_t DtmfDialInstance186;
#pragma DATA_SECTION (DtmfDialInstance186, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance186, 128)

static DtmfDialInfo_t DtmfDialInstance187;
#pragma DATA_SECTION (DtmfDialInstance187, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance187, 128)

static DtmfDialInfo_t DtmfDialInstance188;
#pragma DATA_SECTION (DtmfDialInstance188, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance188, 128)

static DtmfDialInfo_t DtmfDialInstance189;
#pragma DATA_SECTION (DtmfDialInstance189, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance189, 128)

static DtmfDialInfo_t DtmfDialInstance190;
#pragma DATA_SECTION (DtmfDialInstance190, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance190, 128)

static DtmfDialInfo_t DtmfDialInstance191;
#pragma DATA_SECTION (DtmfDialInstance191, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance191, 128)

static DtmfDialInfo_t DtmfDialInstance192;
#pragma DATA_SECTION (DtmfDialInstance192, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance192, 128)

static DtmfDialInfo_t DtmfDialInstance193;
#pragma DATA_SECTION (DtmfDialInstance193, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance193, 128)

static DtmfDialInfo_t DtmfDialInstance194;
#pragma DATA_SECTION (DtmfDialInstance194, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance194, 128)

static DtmfDialInfo_t DtmfDialInstance195;
#pragma DATA_SECTION (DtmfDialInstance195, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance195, 128)

static DtmfDialInfo_t DtmfDialInstance196;
#pragma DATA_SECTION (DtmfDialInstance196, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance196, 128)

static DtmfDialInfo_t DtmfDialInstance197;
#pragma DATA_SECTION (DtmfDialInstance197, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance197, 128)

static DtmfDialInfo_t DtmfDialInstance198;
#pragma DATA_SECTION (DtmfDialInstance198, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance198, 128)

static DtmfDialInfo_t DtmfDialInstance199;
#pragma DATA_SECTION (DtmfDialInstance199, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance199, 128)

static DtmfDialInfo_t DtmfDialInstance200;
#pragma DATA_SECTION (DtmfDialInstance200, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance200, 128)

static DtmfDialInfo_t DtmfDialInstance201;
#pragma DATA_SECTION (DtmfDialInstance201, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance201, 128)

static DtmfDialInfo_t DtmfDialInstance202;
#pragma DATA_SECTION (DtmfDialInstance202, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance202, 128)

static DtmfDialInfo_t DtmfDialInstance203;
#pragma DATA_SECTION (DtmfDialInstance203, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance203, 128)

static DtmfDialInfo_t DtmfDialInstance204;
#pragma DATA_SECTION (DtmfDialInstance204, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance204, 128)

static DtmfDialInfo_t DtmfDialInstance205;
#pragma DATA_SECTION (DtmfDialInstance205, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance205, 128)

static DtmfDialInfo_t DtmfDialInstance206;
#pragma DATA_SECTION (DtmfDialInstance206, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance206, 128)

static DtmfDialInfo_t DtmfDialInstance207;
#pragma DATA_SECTION (DtmfDialInstance207, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance207, 128)

static DtmfDialInfo_t DtmfDialInstance208;
#pragma DATA_SECTION (DtmfDialInstance208, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance208, 128)

static DtmfDialInfo_t DtmfDialInstance209;
#pragma DATA_SECTION (DtmfDialInstance209, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance209, 128)

static DtmfDialInfo_t DtmfDialInstance210;
#pragma DATA_SECTION (DtmfDialInstance210, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance210, 128)

static DtmfDialInfo_t DtmfDialInstance211;
#pragma DATA_SECTION (DtmfDialInstance211, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance211, 128)

static DtmfDialInfo_t DtmfDialInstance212;
#pragma DATA_SECTION (DtmfDialInstance212, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance212, 128)

static DtmfDialInfo_t DtmfDialInstance213;
#pragma DATA_SECTION (DtmfDialInstance213, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance213, 128)

static DtmfDialInfo_t DtmfDialInstance214;
#pragma DATA_SECTION (DtmfDialInstance214, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance214, 128)

static DtmfDialInfo_t DtmfDialInstance215;
#pragma DATA_SECTION (DtmfDialInstance215, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance215, 128)

static DtmfDialInfo_t DtmfDialInstance216;
#pragma DATA_SECTION (DtmfDialInstance216, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance216, 128)

static DtmfDialInfo_t DtmfDialInstance217;
#pragma DATA_SECTION (DtmfDialInstance217, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance217, 128)

static DtmfDialInfo_t DtmfDialInstance218;
#pragma DATA_SECTION (DtmfDialInstance218, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance218, 128)

static DtmfDialInfo_t DtmfDialInstance219;
#pragma DATA_SECTION (DtmfDialInstance219, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance219, 128)

static DtmfDialInfo_t DtmfDialInstance220;
#pragma DATA_SECTION (DtmfDialInstance220, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance220, 128)

static DtmfDialInfo_t DtmfDialInstance221;
#pragma DATA_SECTION (DtmfDialInstance221, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance221, 128)

static DtmfDialInfo_t DtmfDialInstance222;
#pragma DATA_SECTION (DtmfDialInstance222, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance222, 128)

static DtmfDialInfo_t DtmfDialInstance223;
#pragma DATA_SECTION (DtmfDialInstance223, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance223, 128)

static DtmfDialInfo_t DtmfDialInstance224;
#pragma DATA_SECTION (DtmfDialInstance224, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance224, 128)

static DtmfDialInfo_t DtmfDialInstance225;
#pragma DATA_SECTION (DtmfDialInstance225, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance225, 128)

static DtmfDialInfo_t DtmfDialInstance226;
#pragma DATA_SECTION (DtmfDialInstance226, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance226, 128)

static DtmfDialInfo_t DtmfDialInstance227;
#pragma DATA_SECTION (DtmfDialInstance227, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance227, 128)

static DtmfDialInfo_t DtmfDialInstance228;
#pragma DATA_SECTION (DtmfDialInstance228, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance228, 128)

static DtmfDialInfo_t DtmfDialInstance229;
#pragma DATA_SECTION (DtmfDialInstance229, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance229, 128)

static DtmfDialInfo_t DtmfDialInstance230;
#pragma DATA_SECTION (DtmfDialInstance230, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance230, 128)

static DtmfDialInfo_t DtmfDialInstance231;
#pragma DATA_SECTION (DtmfDialInstance231, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance231, 128)

static DtmfDialInfo_t DtmfDialInstance232;
#pragma DATA_SECTION (DtmfDialInstance232, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance232, 128)

static DtmfDialInfo_t DtmfDialInstance233;
#pragma DATA_SECTION (DtmfDialInstance233, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance233, 128)

static DtmfDialInfo_t DtmfDialInstance234;
#pragma DATA_SECTION (DtmfDialInstance234, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance234, 128)

static DtmfDialInfo_t DtmfDialInstance235;
#pragma DATA_SECTION (DtmfDialInstance235, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance235, 128)

static DtmfDialInfo_t DtmfDialInstance236;
#pragma DATA_SECTION (DtmfDialInstance236, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance236, 128)

static DtmfDialInfo_t DtmfDialInstance237;
#pragma DATA_SECTION (DtmfDialInstance237, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance237, 128)

static DtmfDialInfo_t DtmfDialInstance238;
#pragma DATA_SECTION (DtmfDialInstance238, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance238, 128)

static DtmfDialInfo_t DtmfDialInstance239;
#pragma DATA_SECTION (DtmfDialInstance239, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance239, 128)

static DtmfDialInfo_t DtmfDialInstance240;
#pragma DATA_SECTION (DtmfDialInstance240, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance240, 128)

static DtmfDialInfo_t DtmfDialInstance241;
#pragma DATA_SECTION (DtmfDialInstance241, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance241, 128)

static DtmfDialInfo_t DtmfDialInstance242;
#pragma DATA_SECTION (DtmfDialInstance242, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance242, 128)

static DtmfDialInfo_t DtmfDialInstance243;
#pragma DATA_SECTION (DtmfDialInstance243, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance243, 128)

static DtmfDialInfo_t DtmfDialInstance244;
#pragma DATA_SECTION (DtmfDialInstance244, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance244, 128)

static DtmfDialInfo_t DtmfDialInstance245;
#pragma DATA_SECTION (DtmfDialInstance245, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance245, 128)

static DtmfDialInfo_t DtmfDialInstance246;
#pragma DATA_SECTION (DtmfDialInstance246, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance246, 128)

static DtmfDialInfo_t DtmfDialInstance247;
#pragma DATA_SECTION (DtmfDialInstance247, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance247, 128)

static DtmfDialInfo_t DtmfDialInstance248;
#pragma DATA_SECTION (DtmfDialInstance248, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance248, 128)

static DtmfDialInfo_t DtmfDialInstance249;
#pragma DATA_SECTION (DtmfDialInstance249, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance249, 128)

static DtmfDialInfo_t DtmfDialInstance250;
#pragma DATA_SECTION (DtmfDialInstance250, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance250, 128)

static DtmfDialInfo_t DtmfDialInstance251;
#pragma DATA_SECTION (DtmfDialInstance251, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance251, 128)

static DtmfDialInfo_t DtmfDialInstance252;
#pragma DATA_SECTION (DtmfDialInstance252, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance252, 128)

static DtmfDialInfo_t DtmfDialInstance253;
#pragma DATA_SECTION (DtmfDialInstance253, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance253, 128)

static DtmfDialInfo_t DtmfDialInstance254;
#pragma DATA_SECTION (DtmfDialInstance254, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance254, 128)

static DtmfDialInfo_t DtmfDialInstance255;
#pragma DATA_SECTION (DtmfDialInstance255, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance255, 128)

static DtmfDialInfo_t DtmfDialInstance256;
#pragma DATA_SECTION (DtmfDialInstance256, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance256, 128)

static DtmfDialInfo_t DtmfDialInstance257;
#pragma DATA_SECTION (DtmfDialInstance257, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance257, 128)

static DtmfDialInfo_t DtmfDialInstance258;
#pragma DATA_SECTION (DtmfDialInstance258, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance258, 128)

static DtmfDialInfo_t DtmfDialInstance259;
#pragma DATA_SECTION (DtmfDialInstance259, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance259, 128)

static DtmfDialInfo_t DtmfDialInstance260;
#pragma DATA_SECTION (DtmfDialInstance260, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance260, 128)

static DtmfDialInfo_t DtmfDialInstance261;
#pragma DATA_SECTION (DtmfDialInstance261, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance261, 128)

static DtmfDialInfo_t DtmfDialInstance262;
#pragma DATA_SECTION (DtmfDialInstance262, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance262, 128)

static DtmfDialInfo_t DtmfDialInstance263;
#pragma DATA_SECTION (DtmfDialInstance263, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance263, 128)

static DtmfDialInfo_t DtmfDialInstance264;
#pragma DATA_SECTION (DtmfDialInstance264, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance264, 128)

static DtmfDialInfo_t DtmfDialInstance265;
#pragma DATA_SECTION (DtmfDialInstance265, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance265, 128)

static DtmfDialInfo_t DtmfDialInstance266;
#pragma DATA_SECTION (DtmfDialInstance266, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance266, 128)

static DtmfDialInfo_t DtmfDialInstance267;
#pragma DATA_SECTION (DtmfDialInstance267, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance267, 128)

static DtmfDialInfo_t DtmfDialInstance268;
#pragma DATA_SECTION (DtmfDialInstance268, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance268, 128)

static DtmfDialInfo_t DtmfDialInstance269;
#pragma DATA_SECTION (DtmfDialInstance269, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance269, 128)

static DtmfDialInfo_t DtmfDialInstance270;
#pragma DATA_SECTION (DtmfDialInstance270, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance270, 128)

static DtmfDialInfo_t DtmfDialInstance271;
#pragma DATA_SECTION (DtmfDialInstance271, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance271, 128)

static DtmfDialInfo_t DtmfDialInstance272;
#pragma DATA_SECTION (DtmfDialInstance272, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance272, 128)

static DtmfDialInfo_t DtmfDialInstance273;
#pragma DATA_SECTION (DtmfDialInstance273, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance273, 128)

static DtmfDialInfo_t DtmfDialInstance274;
#pragma DATA_SECTION (DtmfDialInstance274, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance274, 128)

static DtmfDialInfo_t DtmfDialInstance275;
#pragma DATA_SECTION (DtmfDialInstance275, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance275, 128)

static DtmfDialInfo_t DtmfDialInstance276;
#pragma DATA_SECTION (DtmfDialInstance276, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance276, 128)

static DtmfDialInfo_t DtmfDialInstance277;
#pragma DATA_SECTION (DtmfDialInstance277, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance277, 128)

static DtmfDialInfo_t DtmfDialInstance278;
#pragma DATA_SECTION (DtmfDialInstance278, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance278, 128)

static DtmfDialInfo_t DtmfDialInstance279;
#pragma DATA_SECTION (DtmfDialInstance279, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance279, 128)

static DtmfDialInfo_t DtmfDialInstance280;
#pragma DATA_SECTION (DtmfDialInstance280, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance280, 128)

static DtmfDialInfo_t DtmfDialInstance281;
#pragma DATA_SECTION (DtmfDialInstance281, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance281, 128)

static DtmfDialInfo_t DtmfDialInstance282;
#pragma DATA_SECTION (DtmfDialInstance282, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance282, 128)

static DtmfDialInfo_t DtmfDialInstance283;
#pragma DATA_SECTION (DtmfDialInstance283, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance283, 128)

static DtmfDialInfo_t DtmfDialInstance284;
#pragma DATA_SECTION (DtmfDialInstance284, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance284, 128)

static DtmfDialInfo_t DtmfDialInstance285;
#pragma DATA_SECTION (DtmfDialInstance285, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance285, 128)

static DtmfDialInfo_t DtmfDialInstance286;
#pragma DATA_SECTION (DtmfDialInstance286, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance286, 128)

static DtmfDialInfo_t DtmfDialInstance287;
#pragma DATA_SECTION (DtmfDialInstance287, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance287, 128)

static DtmfDialInfo_t DtmfDialInstance288;
#pragma DATA_SECTION (DtmfDialInstance288, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance288, 128)

static DtmfDialInfo_t DtmfDialInstance289;
#pragma DATA_SECTION (DtmfDialInstance289, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance289, 128)

static DtmfDialInfo_t DtmfDialInstance290;
#pragma DATA_SECTION (DtmfDialInstance290, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance290, 128)

static DtmfDialInfo_t DtmfDialInstance291;
#pragma DATA_SECTION (DtmfDialInstance291, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance291, 128)

static DtmfDialInfo_t DtmfDialInstance292;
#pragma DATA_SECTION (DtmfDialInstance292, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance292, 128)

static DtmfDialInfo_t DtmfDialInstance293;
#pragma DATA_SECTION (DtmfDialInstance293, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance293, 128)

static DtmfDialInfo_t DtmfDialInstance294;
#pragma DATA_SECTION (DtmfDialInstance294, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance294, 128)

static DtmfDialInfo_t DtmfDialInstance295;
#pragma DATA_SECTION (DtmfDialInstance295, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance295, 128)

static DtmfDialInfo_t DtmfDialInstance296;
#pragma DATA_SECTION (DtmfDialInstance296, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance296, 128)

static DtmfDialInfo_t DtmfDialInstance297;
#pragma DATA_SECTION (DtmfDialInstance297, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance297, 128)

static DtmfDialInfo_t DtmfDialInstance298;
#pragma DATA_SECTION (DtmfDialInstance298, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance298, 128)

static DtmfDialInfo_t DtmfDialInstance299;
#pragma DATA_SECTION (DtmfDialInstance299, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance299, 128)

static DtmfDialInfo_t DtmfDialInstance300;
#pragma DATA_SECTION (DtmfDialInstance300, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance300, 128)

static DtmfDialInfo_t DtmfDialInstance301;
#pragma DATA_SECTION (DtmfDialInstance301, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance301, 128)

static DtmfDialInfo_t DtmfDialInstance302;
#pragma DATA_SECTION (DtmfDialInstance302, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance302, 128)

static DtmfDialInfo_t DtmfDialInstance303;
#pragma DATA_SECTION (DtmfDialInstance303, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance303, 128)

static DtmfDialInfo_t DtmfDialInstance304;
#pragma DATA_SECTION (DtmfDialInstance304, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance304, 128)

static DtmfDialInfo_t DtmfDialInstance305;
#pragma DATA_SECTION (DtmfDialInstance305, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance305, 128)

static DtmfDialInfo_t DtmfDialInstance306;
#pragma DATA_SECTION (DtmfDialInstance306, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance306, 128)

static DtmfDialInfo_t DtmfDialInstance307;
#pragma DATA_SECTION (DtmfDialInstance307, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance307, 128)

static DtmfDialInfo_t DtmfDialInstance308;
#pragma DATA_SECTION (DtmfDialInstance308, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance308, 128)

static DtmfDialInfo_t DtmfDialInstance309;
#pragma DATA_SECTION (DtmfDialInstance309, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance309, 128)

static DtmfDialInfo_t DtmfDialInstance310;
#pragma DATA_SECTION (DtmfDialInstance310, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance310, 128)

static DtmfDialInfo_t DtmfDialInstance311;
#pragma DATA_SECTION (DtmfDialInstance311, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance311, 128)

static DtmfDialInfo_t DtmfDialInstance312;
#pragma DATA_SECTION (DtmfDialInstance312, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance312, 128)

static DtmfDialInfo_t DtmfDialInstance313;
#pragma DATA_SECTION (DtmfDialInstance313, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance313, 128)

static DtmfDialInfo_t DtmfDialInstance314;
#pragma DATA_SECTION (DtmfDialInstance314, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance314, 128)

static DtmfDialInfo_t DtmfDialInstance315;
#pragma DATA_SECTION (DtmfDialInstance315, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance315, 128)

static DtmfDialInfo_t DtmfDialInstance316;
#pragma DATA_SECTION (DtmfDialInstance316, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance316, 128)

static DtmfDialInfo_t DtmfDialInstance317;
#pragma DATA_SECTION (DtmfDialInstance317, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance317, 128)

static DtmfDialInfo_t DtmfDialInstance318;
#pragma DATA_SECTION (DtmfDialInstance318, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance318, 128)

static DtmfDialInfo_t DtmfDialInstance319;
#pragma DATA_SECTION (DtmfDialInstance319, "CHAN_INST_DATA:DtmfDial")
#pragma DATA_ALIGN   (DtmfDialInstance319, 128)

DtmfDialInfo_t* const DtmfDialInstance[] = {
	&DtmfDialInstance0,
	&DtmfDialInstance1,
	&DtmfDialInstance2,
	&DtmfDialInstance3,
	&DtmfDialInstance4,
	&DtmfDialInstance5,
	&DtmfDialInstance6,
	&DtmfDialInstance7,
	&DtmfDialInstance8,
	&DtmfDialInstance9,
	&DtmfDialInstance10,
	&DtmfDialInstance11,
	&DtmfDialInstance12,
	&DtmfDialInstance13,
	&DtmfDialInstance14,
	&DtmfDialInstance15,
	&DtmfDialInstance16,
	&DtmfDialInstance17,
	&DtmfDialInstance18,
	&DtmfDialInstance19,
	&DtmfDialInstance20,
	&DtmfDialInstance21,
	&DtmfDialInstance22,
	&DtmfDialInstance23,
	&DtmfDialInstance24,
	&DtmfDialInstance25,
	&DtmfDialInstance26,
	&DtmfDialInstance27,
	&DtmfDialInstance28,
	&DtmfDialInstance29,
	&DtmfDialInstance30,
	&DtmfDialInstance31,
	&DtmfDialInstance32,
	&DtmfDialInstance33,
	&DtmfDialInstance34,
	&DtmfDialInstance35,
	&DtmfDialInstance36,
	&DtmfDialInstance37,
	&DtmfDialInstance38,
	&DtmfDialInstance39,
	&DtmfDialInstance40,
	&DtmfDialInstance41,
	&DtmfDialInstance42,
	&DtmfDialInstance43,
	&DtmfDialInstance44,
	&DtmfDialInstance45,
	&DtmfDialInstance46,
	&DtmfDialInstance47,
	&DtmfDialInstance48,
	&DtmfDialInstance49,
	&DtmfDialInstance50,
	&DtmfDialInstance51,
	&DtmfDialInstance52,
	&DtmfDialInstance53,
	&DtmfDialInstance54,
	&DtmfDialInstance55,
	&DtmfDialInstance56,
	&DtmfDialInstance57,
	&DtmfDialInstance58,
	&DtmfDialInstance59,
	&DtmfDialInstance60,
	&DtmfDialInstance61,
	&DtmfDialInstance62,
	&DtmfDialInstance63,
	&DtmfDialInstance64,
	&DtmfDialInstance65,
	&DtmfDialInstance66,
	&DtmfDialInstance67,
	&DtmfDialInstance68,
	&DtmfDialInstance69,
	&DtmfDialInstance70,
	&DtmfDialInstance71,
	&DtmfDialInstance72,
	&DtmfDialInstance73,
	&DtmfDialInstance74,
	&DtmfDialInstance75,
	&DtmfDialInstance76,
	&DtmfDialInstance77,
	&DtmfDialInstance78,
	&DtmfDialInstance79,
	&DtmfDialInstance80,
	&DtmfDialInstance81,
	&DtmfDialInstance82,
	&DtmfDialInstance83,
	&DtmfDialInstance84,
	&DtmfDialInstance85,
	&DtmfDialInstance86,
	&DtmfDialInstance87,
	&DtmfDialInstance88,
	&DtmfDialInstance89,
	&DtmfDialInstance90,
	&DtmfDialInstance91,
	&DtmfDialInstance92,
	&DtmfDialInstance93,
	&DtmfDialInstance94,
	&DtmfDialInstance95,
	&DtmfDialInstance96,
	&DtmfDialInstance97,
	&DtmfDialInstance98,
	&DtmfDialInstance99,
	&DtmfDialInstance100,
	&DtmfDialInstance101,
	&DtmfDialInstance102,
	&DtmfDialInstance103,
	&DtmfDialInstance104,
	&DtmfDialInstance105,
	&DtmfDialInstance106,
	&DtmfDialInstance107,
	&DtmfDialInstance108,
	&DtmfDialInstance109,
	&DtmfDialInstance110,
	&DtmfDialInstance111,
	&DtmfDialInstance112,
	&DtmfDialInstance113,
	&DtmfDialInstance114,
	&DtmfDialInstance115,
	&DtmfDialInstance116,
	&DtmfDialInstance117,
	&DtmfDialInstance118,
	&DtmfDialInstance119,
	&DtmfDialInstance120,
	&DtmfDialInstance121,
	&DtmfDialInstance122,
	&DtmfDialInstance123,
	&DtmfDialInstance124,
	&DtmfDialInstance125,
	&DtmfDialInstance126,
	&DtmfDialInstance127,
	&DtmfDialInstance128,
	&DtmfDialInstance129,
	&DtmfDialInstance130,
	&DtmfDialInstance131,
	&DtmfDialInstance132,
	&DtmfDialInstance133,
	&DtmfDialInstance134,
	&DtmfDialInstance135,
	&DtmfDialInstance136,
	&DtmfDialInstance137,
	&DtmfDialInstance138,
	&DtmfDialInstance139,
	&DtmfDialInstance140,
	&DtmfDialInstance141,
	&DtmfDialInstance142,
	&DtmfDialInstance143,
	&DtmfDialInstance144,
	&DtmfDialInstance145,
	&DtmfDialInstance146,
	&DtmfDialInstance147,
	&DtmfDialInstance148,
	&DtmfDialInstance149,
	&DtmfDialInstance150,
	&DtmfDialInstance151,
	&DtmfDialInstance152,
	&DtmfDialInstance153,
	&DtmfDialInstance154,
	&DtmfDialInstance155,
	&DtmfDialInstance156,
	&DtmfDialInstance157,
	&DtmfDialInstance158,
	&DtmfDialInstance159,
	&DtmfDialInstance160,
	&DtmfDialInstance161,
	&DtmfDialInstance162,
	&DtmfDialInstance163,
	&DtmfDialInstance164,
	&DtmfDialInstance165,
	&DtmfDialInstance166,
	&DtmfDialInstance167,
	&DtmfDialInstance168,
	&DtmfDialInstance169,
	&DtmfDialInstance170,
	&DtmfDialInstance171,
	&DtmfDialInstance172,
	&DtmfDialInstance173,
	&DtmfDialInstance174,
	&DtmfDialInstance175,
	&DtmfDialInstance176,
	&DtmfDialInstance177,
	&DtmfDialInstance178,
	&DtmfDialInstance179,
	&DtmfDialInstance180,
	&DtmfDialInstance181,
	&DtmfDialInstance182,
	&DtmfDialInstance183,
	&DtmfDialInstance184,
	&DtmfDialInstance185,
	&DtmfDialInstance186,
	&DtmfDialInstance187,
	&DtmfDialInstance188,
	&DtmfDialInstance189,
	&DtmfDialInstance190,
	&DtmfDialInstance191,
	&DtmfDialInstance192,
	&DtmfDialInstance193,
	&DtmfDialInstance194,
	&DtmfDialInstance195,
	&DtmfDialInstance196,
	&DtmfDialInstance197,
	&DtmfDialInstance198,
	&DtmfDialInstance199,
	&DtmfDialInstance200,
	&DtmfDialInstance201,
	&DtmfDialInstance202,
	&DtmfDialInstance203,
	&DtmfDialInstance204,
	&DtmfDialInstance205,
	&DtmfDialInstance206,
	&DtmfDialInstance207,
	&DtmfDialInstance208,
	&DtmfDialInstance209,
	&DtmfDialInstance210,
	&DtmfDialInstance211,
	&DtmfDialInstance212,
	&DtmfDialInstance213,
	&DtmfDialInstance214,
	&DtmfDialInstance215,
	&DtmfDialInstance216,
	&DtmfDialInstance217,
	&DtmfDialInstance218,
	&DtmfDialInstance219,
	&DtmfDialInstance220,
	&DtmfDialInstance221,
	&DtmfDialInstance222,
	&DtmfDialInstance223,
	&DtmfDialInstance224,
	&DtmfDialInstance225,
	&DtmfDialInstance226,
	&DtmfDialInstance227,
	&DtmfDialInstance228,
	&DtmfDialInstance229,
	&DtmfDialInstance230,
	&DtmfDialInstance231,
	&DtmfDialInstance232,
	&DtmfDialInstance233,
	&DtmfDialInstance234,
	&DtmfDialInstance235,
	&DtmfDialInstance236,
	&DtmfDialInstance237,
	&DtmfDialInstance238,
	&DtmfDialInstance239,
	&DtmfDialInstance240,
	&DtmfDialInstance241,
	&DtmfDialInstance242,
	&DtmfDialInstance243,
	&DtmfDialInstance244,
	&DtmfDialInstance245,
	&DtmfDialInstance246,
	&DtmfDialInstance247,
	&DtmfDialInstance248,
	&DtmfDialInstance249,
	&DtmfDialInstance250,
	&DtmfDialInstance251,
	&DtmfDialInstance252,
	&DtmfDialInstance253,
	&DtmfDialInstance254,
	&DtmfDialInstance255,
	&DtmfDialInstance256,
	&DtmfDialInstance257,
	&DtmfDialInstance258,
	&DtmfDialInstance259,
	&DtmfDialInstance260,
	&DtmfDialInstance261,
	&DtmfDialInstance262,
	&DtmfDialInstance263,
	&DtmfDialInstance264,
	&DtmfDialInstance265,
	&DtmfDialInstance266,
	&DtmfDialInstance267,
	&DtmfDialInstance268,
	&DtmfDialInstance269,
	&DtmfDialInstance270,
	&DtmfDialInstance271,
	&DtmfDialInstance272,
	&DtmfDialInstance273,
	&DtmfDialInstance274,
	&DtmfDialInstance275,
	&DtmfDialInstance276,
	&DtmfDialInstance277,
	&DtmfDialInstance278,
	&DtmfDialInstance279,
	&DtmfDialInstance280,
	&DtmfDialInstance281,
	&DtmfDialInstance282,
	&DtmfDialInstance283,
	&DtmfDialInstance284,
	&DtmfDialInstance285,
	&DtmfDialInstance286,
	&DtmfDialInstance287,
	&DtmfDialInstance288,
	&DtmfDialInstance289,
	&DtmfDialInstance290,
	&DtmfDialInstance291,
	&DtmfDialInstance292,
	&DtmfDialInstance293,
	&DtmfDialInstance294,
	&DtmfDialInstance295,
	&DtmfDialInstance296,
	&DtmfDialInstance297,
	&DtmfDialInstance298,
	&DtmfDialInstance299,
	&DtmfDialInstance300,
	&DtmfDialInstance301,
	&DtmfDialInstance302,
	&DtmfDialInstance303,
	&DtmfDialInstance304,
	&DtmfDialInstance305,
	&DtmfDialInstance306,
	&DtmfDialInstance307,
	&DtmfDialInstance308,
	&DtmfDialInstance309,
	&DtmfDialInstance310,
	&DtmfDialInstance311,
	&DtmfDialInstance312,
	&DtmfDialInstance313,
	&DtmfDialInstance314,
	&DtmfDialInstance315,
	&DtmfDialInstance316,
	&DtmfDialInstance317,
	&DtmfDialInstance318,
	&DtmfDialInstance319
};
//}

int numPastToneRlyGenEvents = NUM_TRGEN_EVENTS;
//{   TRGenerateInstance_t   ToneRlyGenInstance STRUCTURES to CHAN_INST_DATA:ToneGen

TRGenerateInstance_t ToneRlyGenInstance0;
#pragma DATA_SECTION (ToneRlyGenInstance0, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance0, 128)

static TRGenerateInstance_t ToneRlyGenInstance1;
#pragma DATA_SECTION (ToneRlyGenInstance1, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance1, 128)

static TRGenerateInstance_t ToneRlyGenInstance2;
#pragma DATA_SECTION (ToneRlyGenInstance2, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance2, 128)

static TRGenerateInstance_t ToneRlyGenInstance3;
#pragma DATA_SECTION (ToneRlyGenInstance3, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance3, 128)

static TRGenerateInstance_t ToneRlyGenInstance4;
#pragma DATA_SECTION (ToneRlyGenInstance4, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance4, 128)

static TRGenerateInstance_t ToneRlyGenInstance5;
#pragma DATA_SECTION (ToneRlyGenInstance5, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance5, 128)

static TRGenerateInstance_t ToneRlyGenInstance6;
#pragma DATA_SECTION (ToneRlyGenInstance6, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance6, 128)

static TRGenerateInstance_t ToneRlyGenInstance7;
#pragma DATA_SECTION (ToneRlyGenInstance7, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance7, 128)

static TRGenerateInstance_t ToneRlyGenInstance8;
#pragma DATA_SECTION (ToneRlyGenInstance8, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance8, 128)

static TRGenerateInstance_t ToneRlyGenInstance9;
#pragma DATA_SECTION (ToneRlyGenInstance9, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance9, 128)

static TRGenerateInstance_t ToneRlyGenInstance10;
#pragma DATA_SECTION (ToneRlyGenInstance10, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance10, 128)

static TRGenerateInstance_t ToneRlyGenInstance11;
#pragma DATA_SECTION (ToneRlyGenInstance11, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance11, 128)

static TRGenerateInstance_t ToneRlyGenInstance12;
#pragma DATA_SECTION (ToneRlyGenInstance12, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance12, 128)

static TRGenerateInstance_t ToneRlyGenInstance13;
#pragma DATA_SECTION (ToneRlyGenInstance13, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance13, 128)

static TRGenerateInstance_t ToneRlyGenInstance14;
#pragma DATA_SECTION (ToneRlyGenInstance14, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance14, 128)

static TRGenerateInstance_t ToneRlyGenInstance15;
#pragma DATA_SECTION (ToneRlyGenInstance15, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance15, 128)

static TRGenerateInstance_t ToneRlyGenInstance16;
#pragma DATA_SECTION (ToneRlyGenInstance16, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance16, 128)

static TRGenerateInstance_t ToneRlyGenInstance17;
#pragma DATA_SECTION (ToneRlyGenInstance17, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance17, 128)

static TRGenerateInstance_t ToneRlyGenInstance18;
#pragma DATA_SECTION (ToneRlyGenInstance18, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance18, 128)

static TRGenerateInstance_t ToneRlyGenInstance19;
#pragma DATA_SECTION (ToneRlyGenInstance19, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance19, 128)

static TRGenerateInstance_t ToneRlyGenInstance20;
#pragma DATA_SECTION (ToneRlyGenInstance20, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance20, 128)

static TRGenerateInstance_t ToneRlyGenInstance21;
#pragma DATA_SECTION (ToneRlyGenInstance21, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance21, 128)

static TRGenerateInstance_t ToneRlyGenInstance22;
#pragma DATA_SECTION (ToneRlyGenInstance22, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance22, 128)

static TRGenerateInstance_t ToneRlyGenInstance23;
#pragma DATA_SECTION (ToneRlyGenInstance23, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance23, 128)

static TRGenerateInstance_t ToneRlyGenInstance24;
#pragma DATA_SECTION (ToneRlyGenInstance24, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance24, 128)

static TRGenerateInstance_t ToneRlyGenInstance25;
#pragma DATA_SECTION (ToneRlyGenInstance25, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance25, 128)

static TRGenerateInstance_t ToneRlyGenInstance26;
#pragma DATA_SECTION (ToneRlyGenInstance26, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance26, 128)

static TRGenerateInstance_t ToneRlyGenInstance27;
#pragma DATA_SECTION (ToneRlyGenInstance27, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance27, 128)

static TRGenerateInstance_t ToneRlyGenInstance28;
#pragma DATA_SECTION (ToneRlyGenInstance28, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance28, 128)

static TRGenerateInstance_t ToneRlyGenInstance29;
#pragma DATA_SECTION (ToneRlyGenInstance29, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance29, 128)

static TRGenerateInstance_t ToneRlyGenInstance30;
#pragma DATA_SECTION (ToneRlyGenInstance30, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance30, 128)

static TRGenerateInstance_t ToneRlyGenInstance31;
#pragma DATA_SECTION (ToneRlyGenInstance31, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance31, 128)

static TRGenerateInstance_t ToneRlyGenInstance32;
#pragma DATA_SECTION (ToneRlyGenInstance32, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance32, 128)

static TRGenerateInstance_t ToneRlyGenInstance33;
#pragma DATA_SECTION (ToneRlyGenInstance33, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance33, 128)

static TRGenerateInstance_t ToneRlyGenInstance34;
#pragma DATA_SECTION (ToneRlyGenInstance34, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance34, 128)

static TRGenerateInstance_t ToneRlyGenInstance35;
#pragma DATA_SECTION (ToneRlyGenInstance35, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance35, 128)

static TRGenerateInstance_t ToneRlyGenInstance36;
#pragma DATA_SECTION (ToneRlyGenInstance36, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance36, 128)

static TRGenerateInstance_t ToneRlyGenInstance37;
#pragma DATA_SECTION (ToneRlyGenInstance37, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance37, 128)

static TRGenerateInstance_t ToneRlyGenInstance38;
#pragma DATA_SECTION (ToneRlyGenInstance38, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance38, 128)

static TRGenerateInstance_t ToneRlyGenInstance39;
#pragma DATA_SECTION (ToneRlyGenInstance39, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance39, 128)

static TRGenerateInstance_t ToneRlyGenInstance40;
#pragma DATA_SECTION (ToneRlyGenInstance40, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance40, 128)

static TRGenerateInstance_t ToneRlyGenInstance41;
#pragma DATA_SECTION (ToneRlyGenInstance41, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance41, 128)

static TRGenerateInstance_t ToneRlyGenInstance42;
#pragma DATA_SECTION (ToneRlyGenInstance42, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance42, 128)

static TRGenerateInstance_t ToneRlyGenInstance43;
#pragma DATA_SECTION (ToneRlyGenInstance43, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance43, 128)

static TRGenerateInstance_t ToneRlyGenInstance44;
#pragma DATA_SECTION (ToneRlyGenInstance44, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance44, 128)

static TRGenerateInstance_t ToneRlyGenInstance45;
#pragma DATA_SECTION (ToneRlyGenInstance45, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance45, 128)

static TRGenerateInstance_t ToneRlyGenInstance46;
#pragma DATA_SECTION (ToneRlyGenInstance46, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance46, 128)

static TRGenerateInstance_t ToneRlyGenInstance47;
#pragma DATA_SECTION (ToneRlyGenInstance47, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance47, 128)

static TRGenerateInstance_t ToneRlyGenInstance48;
#pragma DATA_SECTION (ToneRlyGenInstance48, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance48, 128)

static TRGenerateInstance_t ToneRlyGenInstance49;
#pragma DATA_SECTION (ToneRlyGenInstance49, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance49, 128)

static TRGenerateInstance_t ToneRlyGenInstance50;
#pragma DATA_SECTION (ToneRlyGenInstance50, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance50, 128)

static TRGenerateInstance_t ToneRlyGenInstance51;
#pragma DATA_SECTION (ToneRlyGenInstance51, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance51, 128)

static TRGenerateInstance_t ToneRlyGenInstance52;
#pragma DATA_SECTION (ToneRlyGenInstance52, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance52, 128)

static TRGenerateInstance_t ToneRlyGenInstance53;
#pragma DATA_SECTION (ToneRlyGenInstance53, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance53, 128)

static TRGenerateInstance_t ToneRlyGenInstance54;
#pragma DATA_SECTION (ToneRlyGenInstance54, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance54, 128)

static TRGenerateInstance_t ToneRlyGenInstance55;
#pragma DATA_SECTION (ToneRlyGenInstance55, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance55, 128)

static TRGenerateInstance_t ToneRlyGenInstance56;
#pragma DATA_SECTION (ToneRlyGenInstance56, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance56, 128)

static TRGenerateInstance_t ToneRlyGenInstance57;
#pragma DATA_SECTION (ToneRlyGenInstance57, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance57, 128)

static TRGenerateInstance_t ToneRlyGenInstance58;
#pragma DATA_SECTION (ToneRlyGenInstance58, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance58, 128)

static TRGenerateInstance_t ToneRlyGenInstance59;
#pragma DATA_SECTION (ToneRlyGenInstance59, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance59, 128)

static TRGenerateInstance_t ToneRlyGenInstance60;
#pragma DATA_SECTION (ToneRlyGenInstance60, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance60, 128)

static TRGenerateInstance_t ToneRlyGenInstance61;
#pragma DATA_SECTION (ToneRlyGenInstance61, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance61, 128)

static TRGenerateInstance_t ToneRlyGenInstance62;
#pragma DATA_SECTION (ToneRlyGenInstance62, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance62, 128)

static TRGenerateInstance_t ToneRlyGenInstance63;
#pragma DATA_SECTION (ToneRlyGenInstance63, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance63, 128)

static TRGenerateInstance_t ToneRlyGenInstance64;
#pragma DATA_SECTION (ToneRlyGenInstance64, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance64, 128)

static TRGenerateInstance_t ToneRlyGenInstance65;
#pragma DATA_SECTION (ToneRlyGenInstance65, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance65, 128)

static TRGenerateInstance_t ToneRlyGenInstance66;
#pragma DATA_SECTION (ToneRlyGenInstance66, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance66, 128)

static TRGenerateInstance_t ToneRlyGenInstance67;
#pragma DATA_SECTION (ToneRlyGenInstance67, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance67, 128)

static TRGenerateInstance_t ToneRlyGenInstance68;
#pragma DATA_SECTION (ToneRlyGenInstance68, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance68, 128)

static TRGenerateInstance_t ToneRlyGenInstance69;
#pragma DATA_SECTION (ToneRlyGenInstance69, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance69, 128)

static TRGenerateInstance_t ToneRlyGenInstance70;
#pragma DATA_SECTION (ToneRlyGenInstance70, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance70, 128)

static TRGenerateInstance_t ToneRlyGenInstance71;
#pragma DATA_SECTION (ToneRlyGenInstance71, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance71, 128)

static TRGenerateInstance_t ToneRlyGenInstance72;
#pragma DATA_SECTION (ToneRlyGenInstance72, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance72, 128)

static TRGenerateInstance_t ToneRlyGenInstance73;
#pragma DATA_SECTION (ToneRlyGenInstance73, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance73, 128)

static TRGenerateInstance_t ToneRlyGenInstance74;
#pragma DATA_SECTION (ToneRlyGenInstance74, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance74, 128)

static TRGenerateInstance_t ToneRlyGenInstance75;
#pragma DATA_SECTION (ToneRlyGenInstance75, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance75, 128)

static TRGenerateInstance_t ToneRlyGenInstance76;
#pragma DATA_SECTION (ToneRlyGenInstance76, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance76, 128)

static TRGenerateInstance_t ToneRlyGenInstance77;
#pragma DATA_SECTION (ToneRlyGenInstance77, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance77, 128)

static TRGenerateInstance_t ToneRlyGenInstance78;
#pragma DATA_SECTION (ToneRlyGenInstance78, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance78, 128)

static TRGenerateInstance_t ToneRlyGenInstance79;
#pragma DATA_SECTION (ToneRlyGenInstance79, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance79, 128)

static TRGenerateInstance_t ToneRlyGenInstance80;
#pragma DATA_SECTION (ToneRlyGenInstance80, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance80, 128)

static TRGenerateInstance_t ToneRlyGenInstance81;
#pragma DATA_SECTION (ToneRlyGenInstance81, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance81, 128)

static TRGenerateInstance_t ToneRlyGenInstance82;
#pragma DATA_SECTION (ToneRlyGenInstance82, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance82, 128)

static TRGenerateInstance_t ToneRlyGenInstance83;
#pragma DATA_SECTION (ToneRlyGenInstance83, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance83, 128)

static TRGenerateInstance_t ToneRlyGenInstance84;
#pragma DATA_SECTION (ToneRlyGenInstance84, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance84, 128)

static TRGenerateInstance_t ToneRlyGenInstance85;
#pragma DATA_SECTION (ToneRlyGenInstance85, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance85, 128)

static TRGenerateInstance_t ToneRlyGenInstance86;
#pragma DATA_SECTION (ToneRlyGenInstance86, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance86, 128)

static TRGenerateInstance_t ToneRlyGenInstance87;
#pragma DATA_SECTION (ToneRlyGenInstance87, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance87, 128)

static TRGenerateInstance_t ToneRlyGenInstance88;
#pragma DATA_SECTION (ToneRlyGenInstance88, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance88, 128)

static TRGenerateInstance_t ToneRlyGenInstance89;
#pragma DATA_SECTION (ToneRlyGenInstance89, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance89, 128)

static TRGenerateInstance_t ToneRlyGenInstance90;
#pragma DATA_SECTION (ToneRlyGenInstance90, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance90, 128)

static TRGenerateInstance_t ToneRlyGenInstance91;
#pragma DATA_SECTION (ToneRlyGenInstance91, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance91, 128)

static TRGenerateInstance_t ToneRlyGenInstance92;
#pragma DATA_SECTION (ToneRlyGenInstance92, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance92, 128)

static TRGenerateInstance_t ToneRlyGenInstance93;
#pragma DATA_SECTION (ToneRlyGenInstance93, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance93, 128)

static TRGenerateInstance_t ToneRlyGenInstance94;
#pragma DATA_SECTION (ToneRlyGenInstance94, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance94, 128)

static TRGenerateInstance_t ToneRlyGenInstance95;
#pragma DATA_SECTION (ToneRlyGenInstance95, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance95, 128)

static TRGenerateInstance_t ToneRlyGenInstance96;
#pragma DATA_SECTION (ToneRlyGenInstance96, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance96, 128)

static TRGenerateInstance_t ToneRlyGenInstance97;
#pragma DATA_SECTION (ToneRlyGenInstance97, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance97, 128)

static TRGenerateInstance_t ToneRlyGenInstance98;
#pragma DATA_SECTION (ToneRlyGenInstance98, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance98, 128)

static TRGenerateInstance_t ToneRlyGenInstance99;
#pragma DATA_SECTION (ToneRlyGenInstance99, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance99, 128)

static TRGenerateInstance_t ToneRlyGenInstance100;
#pragma DATA_SECTION (ToneRlyGenInstance100, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance100, 128)

static TRGenerateInstance_t ToneRlyGenInstance101;
#pragma DATA_SECTION (ToneRlyGenInstance101, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance101, 128)

static TRGenerateInstance_t ToneRlyGenInstance102;
#pragma DATA_SECTION (ToneRlyGenInstance102, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance102, 128)

static TRGenerateInstance_t ToneRlyGenInstance103;
#pragma DATA_SECTION (ToneRlyGenInstance103, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance103, 128)

static TRGenerateInstance_t ToneRlyGenInstance104;
#pragma DATA_SECTION (ToneRlyGenInstance104, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance104, 128)

static TRGenerateInstance_t ToneRlyGenInstance105;
#pragma DATA_SECTION (ToneRlyGenInstance105, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance105, 128)

static TRGenerateInstance_t ToneRlyGenInstance106;
#pragma DATA_SECTION (ToneRlyGenInstance106, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance106, 128)

static TRGenerateInstance_t ToneRlyGenInstance107;
#pragma DATA_SECTION (ToneRlyGenInstance107, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance107, 128)

static TRGenerateInstance_t ToneRlyGenInstance108;
#pragma DATA_SECTION (ToneRlyGenInstance108, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance108, 128)

static TRGenerateInstance_t ToneRlyGenInstance109;
#pragma DATA_SECTION (ToneRlyGenInstance109, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance109, 128)

static TRGenerateInstance_t ToneRlyGenInstance110;
#pragma DATA_SECTION (ToneRlyGenInstance110, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance110, 128)

static TRGenerateInstance_t ToneRlyGenInstance111;
#pragma DATA_SECTION (ToneRlyGenInstance111, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance111, 128)

static TRGenerateInstance_t ToneRlyGenInstance112;
#pragma DATA_SECTION (ToneRlyGenInstance112, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance112, 128)

static TRGenerateInstance_t ToneRlyGenInstance113;
#pragma DATA_SECTION (ToneRlyGenInstance113, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance113, 128)

static TRGenerateInstance_t ToneRlyGenInstance114;
#pragma DATA_SECTION (ToneRlyGenInstance114, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance114, 128)

static TRGenerateInstance_t ToneRlyGenInstance115;
#pragma DATA_SECTION (ToneRlyGenInstance115, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance115, 128)

static TRGenerateInstance_t ToneRlyGenInstance116;
#pragma DATA_SECTION (ToneRlyGenInstance116, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance116, 128)

static TRGenerateInstance_t ToneRlyGenInstance117;
#pragma DATA_SECTION (ToneRlyGenInstance117, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance117, 128)

static TRGenerateInstance_t ToneRlyGenInstance118;
#pragma DATA_SECTION (ToneRlyGenInstance118, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance118, 128)

static TRGenerateInstance_t ToneRlyGenInstance119;
#pragma DATA_SECTION (ToneRlyGenInstance119, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance119, 128)

static TRGenerateInstance_t ToneRlyGenInstance120;
#pragma DATA_SECTION (ToneRlyGenInstance120, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance120, 128)

static TRGenerateInstance_t ToneRlyGenInstance121;
#pragma DATA_SECTION (ToneRlyGenInstance121, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance121, 128)

static TRGenerateInstance_t ToneRlyGenInstance122;
#pragma DATA_SECTION (ToneRlyGenInstance122, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance122, 128)

static TRGenerateInstance_t ToneRlyGenInstance123;
#pragma DATA_SECTION (ToneRlyGenInstance123, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance123, 128)

static TRGenerateInstance_t ToneRlyGenInstance124;
#pragma DATA_SECTION (ToneRlyGenInstance124, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance124, 128)

static TRGenerateInstance_t ToneRlyGenInstance125;
#pragma DATA_SECTION (ToneRlyGenInstance125, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance125, 128)

static TRGenerateInstance_t ToneRlyGenInstance126;
#pragma DATA_SECTION (ToneRlyGenInstance126, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance126, 128)

static TRGenerateInstance_t ToneRlyGenInstance127;
#pragma DATA_SECTION (ToneRlyGenInstance127, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance127, 128)

static TRGenerateInstance_t ToneRlyGenInstance128;
#pragma DATA_SECTION (ToneRlyGenInstance128, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance128, 128)

static TRGenerateInstance_t ToneRlyGenInstance129;
#pragma DATA_SECTION (ToneRlyGenInstance129, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance129, 128)

static TRGenerateInstance_t ToneRlyGenInstance130;
#pragma DATA_SECTION (ToneRlyGenInstance130, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance130, 128)

static TRGenerateInstance_t ToneRlyGenInstance131;
#pragma DATA_SECTION (ToneRlyGenInstance131, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance131, 128)

static TRGenerateInstance_t ToneRlyGenInstance132;
#pragma DATA_SECTION (ToneRlyGenInstance132, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance132, 128)

static TRGenerateInstance_t ToneRlyGenInstance133;
#pragma DATA_SECTION (ToneRlyGenInstance133, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance133, 128)

static TRGenerateInstance_t ToneRlyGenInstance134;
#pragma DATA_SECTION (ToneRlyGenInstance134, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance134, 128)

static TRGenerateInstance_t ToneRlyGenInstance135;
#pragma DATA_SECTION (ToneRlyGenInstance135, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance135, 128)

static TRGenerateInstance_t ToneRlyGenInstance136;
#pragma DATA_SECTION (ToneRlyGenInstance136, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance136, 128)

static TRGenerateInstance_t ToneRlyGenInstance137;
#pragma DATA_SECTION (ToneRlyGenInstance137, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance137, 128)

static TRGenerateInstance_t ToneRlyGenInstance138;
#pragma DATA_SECTION (ToneRlyGenInstance138, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance138, 128)

static TRGenerateInstance_t ToneRlyGenInstance139;
#pragma DATA_SECTION (ToneRlyGenInstance139, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance139, 128)

static TRGenerateInstance_t ToneRlyGenInstance140;
#pragma DATA_SECTION (ToneRlyGenInstance140, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance140, 128)

static TRGenerateInstance_t ToneRlyGenInstance141;
#pragma DATA_SECTION (ToneRlyGenInstance141, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance141, 128)

static TRGenerateInstance_t ToneRlyGenInstance142;
#pragma DATA_SECTION (ToneRlyGenInstance142, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance142, 128)

static TRGenerateInstance_t ToneRlyGenInstance143;
#pragma DATA_SECTION (ToneRlyGenInstance143, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance143, 128)

static TRGenerateInstance_t ToneRlyGenInstance144;
#pragma DATA_SECTION (ToneRlyGenInstance144, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance144, 128)

static TRGenerateInstance_t ToneRlyGenInstance145;
#pragma DATA_SECTION (ToneRlyGenInstance145, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance145, 128)

static TRGenerateInstance_t ToneRlyGenInstance146;
#pragma DATA_SECTION (ToneRlyGenInstance146, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance146, 128)

static TRGenerateInstance_t ToneRlyGenInstance147;
#pragma DATA_SECTION (ToneRlyGenInstance147, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance147, 128)

static TRGenerateInstance_t ToneRlyGenInstance148;
#pragma DATA_SECTION (ToneRlyGenInstance148, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance148, 128)

static TRGenerateInstance_t ToneRlyGenInstance149;
#pragma DATA_SECTION (ToneRlyGenInstance149, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance149, 128)

static TRGenerateInstance_t ToneRlyGenInstance150;
#pragma DATA_SECTION (ToneRlyGenInstance150, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance150, 128)

static TRGenerateInstance_t ToneRlyGenInstance151;
#pragma DATA_SECTION (ToneRlyGenInstance151, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance151, 128)

static TRGenerateInstance_t ToneRlyGenInstance152;
#pragma DATA_SECTION (ToneRlyGenInstance152, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance152, 128)

static TRGenerateInstance_t ToneRlyGenInstance153;
#pragma DATA_SECTION (ToneRlyGenInstance153, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance153, 128)

static TRGenerateInstance_t ToneRlyGenInstance154;
#pragma DATA_SECTION (ToneRlyGenInstance154, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance154, 128)

static TRGenerateInstance_t ToneRlyGenInstance155;
#pragma DATA_SECTION (ToneRlyGenInstance155, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance155, 128)

static TRGenerateInstance_t ToneRlyGenInstance156;
#pragma DATA_SECTION (ToneRlyGenInstance156, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance156, 128)

static TRGenerateInstance_t ToneRlyGenInstance157;
#pragma DATA_SECTION (ToneRlyGenInstance157, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance157, 128)

static TRGenerateInstance_t ToneRlyGenInstance158;
#pragma DATA_SECTION (ToneRlyGenInstance158, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance158, 128)

static TRGenerateInstance_t ToneRlyGenInstance159;
#pragma DATA_SECTION (ToneRlyGenInstance159, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenInstance159, 128)

TRGenerateInstance_t* const ToneRlyGenInstance[] = {
	&ToneRlyGenInstance0,
	&ToneRlyGenInstance1,
	&ToneRlyGenInstance2,
	&ToneRlyGenInstance3,
	&ToneRlyGenInstance4,
	&ToneRlyGenInstance5,
	&ToneRlyGenInstance6,
	&ToneRlyGenInstance7,
	&ToneRlyGenInstance8,
	&ToneRlyGenInstance9,
	&ToneRlyGenInstance10,
	&ToneRlyGenInstance11,
	&ToneRlyGenInstance12,
	&ToneRlyGenInstance13,
	&ToneRlyGenInstance14,
	&ToneRlyGenInstance15,
	&ToneRlyGenInstance16,
	&ToneRlyGenInstance17,
	&ToneRlyGenInstance18,
	&ToneRlyGenInstance19,
	&ToneRlyGenInstance20,
	&ToneRlyGenInstance21,
	&ToneRlyGenInstance22,
	&ToneRlyGenInstance23,
	&ToneRlyGenInstance24,
	&ToneRlyGenInstance25,
	&ToneRlyGenInstance26,
	&ToneRlyGenInstance27,
	&ToneRlyGenInstance28,
	&ToneRlyGenInstance29,
	&ToneRlyGenInstance30,
	&ToneRlyGenInstance31,
	&ToneRlyGenInstance32,
	&ToneRlyGenInstance33,
	&ToneRlyGenInstance34,
	&ToneRlyGenInstance35,
	&ToneRlyGenInstance36,
	&ToneRlyGenInstance37,
	&ToneRlyGenInstance38,
	&ToneRlyGenInstance39,
	&ToneRlyGenInstance40,
	&ToneRlyGenInstance41,
	&ToneRlyGenInstance42,
	&ToneRlyGenInstance43,
	&ToneRlyGenInstance44,
	&ToneRlyGenInstance45,
	&ToneRlyGenInstance46,
	&ToneRlyGenInstance47,
	&ToneRlyGenInstance48,
	&ToneRlyGenInstance49,
	&ToneRlyGenInstance50,
	&ToneRlyGenInstance51,
	&ToneRlyGenInstance52,
	&ToneRlyGenInstance53,
	&ToneRlyGenInstance54,
	&ToneRlyGenInstance55,
	&ToneRlyGenInstance56,
	&ToneRlyGenInstance57,
	&ToneRlyGenInstance58,
	&ToneRlyGenInstance59,
	&ToneRlyGenInstance60,
	&ToneRlyGenInstance61,
	&ToneRlyGenInstance62,
	&ToneRlyGenInstance63,
	&ToneRlyGenInstance64,
	&ToneRlyGenInstance65,
	&ToneRlyGenInstance66,
	&ToneRlyGenInstance67,
	&ToneRlyGenInstance68,
	&ToneRlyGenInstance69,
	&ToneRlyGenInstance70,
	&ToneRlyGenInstance71,
	&ToneRlyGenInstance72,
	&ToneRlyGenInstance73,
	&ToneRlyGenInstance74,
	&ToneRlyGenInstance75,
	&ToneRlyGenInstance76,
	&ToneRlyGenInstance77,
	&ToneRlyGenInstance78,
	&ToneRlyGenInstance79,
	&ToneRlyGenInstance80,
	&ToneRlyGenInstance81,
	&ToneRlyGenInstance82,
	&ToneRlyGenInstance83,
	&ToneRlyGenInstance84,
	&ToneRlyGenInstance85,
	&ToneRlyGenInstance86,
	&ToneRlyGenInstance87,
	&ToneRlyGenInstance88,
	&ToneRlyGenInstance89,
	&ToneRlyGenInstance90,
	&ToneRlyGenInstance91,
	&ToneRlyGenInstance92,
	&ToneRlyGenInstance93,
	&ToneRlyGenInstance94,
	&ToneRlyGenInstance95,
	&ToneRlyGenInstance96,
	&ToneRlyGenInstance97,
	&ToneRlyGenInstance98,
	&ToneRlyGenInstance99,
	&ToneRlyGenInstance100,
	&ToneRlyGenInstance101,
	&ToneRlyGenInstance102,
	&ToneRlyGenInstance103,
	&ToneRlyGenInstance104,
	&ToneRlyGenInstance105,
	&ToneRlyGenInstance106,
	&ToneRlyGenInstance107,
	&ToneRlyGenInstance108,
	&ToneRlyGenInstance109,
	&ToneRlyGenInstance110,
	&ToneRlyGenInstance111,
	&ToneRlyGenInstance112,
	&ToneRlyGenInstance113,
	&ToneRlyGenInstance114,
	&ToneRlyGenInstance115,
	&ToneRlyGenInstance116,
	&ToneRlyGenInstance117,
	&ToneRlyGenInstance118,
	&ToneRlyGenInstance119,
	&ToneRlyGenInstance120,
	&ToneRlyGenInstance121,
	&ToneRlyGenInstance122,
	&ToneRlyGenInstance123,
	&ToneRlyGenInstance124,
	&ToneRlyGenInstance125,
	&ToneRlyGenInstance126,
	&ToneRlyGenInstance127,
	&ToneRlyGenInstance128,
	&ToneRlyGenInstance129,
	&ToneRlyGenInstance130,
	&ToneRlyGenInstance131,
	&ToneRlyGenInstance132,
	&ToneRlyGenInstance133,
	&ToneRlyGenInstance134,
	&ToneRlyGenInstance135,
	&ToneRlyGenInstance136,
	&ToneRlyGenInstance137,
	&ToneRlyGenInstance138,
	&ToneRlyGenInstance139,
	&ToneRlyGenInstance140,
	&ToneRlyGenInstance141,
	&ToneRlyGenInstance142,
	&ToneRlyGenInstance143,
	&ToneRlyGenInstance144,
	&ToneRlyGenInstance145,
	&ToneRlyGenInstance146,
	&ToneRlyGenInstance147,
	&ToneRlyGenInstance148,
	&ToneRlyGenInstance149,
	&ToneRlyGenInstance150,
	&ToneRlyGenInstance151,
	&ToneRlyGenInstance152,
	&ToneRlyGenInstance153,
	&ToneRlyGenInstance154,
	&ToneRlyGenInstance155,
	&ToneRlyGenInstance156,
	&ToneRlyGenInstance157,
	&ToneRlyGenInstance158,
	&ToneRlyGenInstance159
};
//}

//{   TRGenerateEvent_t ToneRlyGenEvent STRUCTURES to CHAN_INST_DATA:ToneGen

static TRGenerateEvent_t ToneRlyGenEvent0[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent0, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent0, 128)

static TRGenerateEvent_t ToneRlyGenEvent1[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent1, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent1, 128)

static TRGenerateEvent_t ToneRlyGenEvent2[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent2, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent2, 128)

static TRGenerateEvent_t ToneRlyGenEvent3[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent3, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent3, 128)

static TRGenerateEvent_t ToneRlyGenEvent4[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent4, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent4, 128)

static TRGenerateEvent_t ToneRlyGenEvent5[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent5, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent5, 128)

static TRGenerateEvent_t ToneRlyGenEvent6[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent6, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent6, 128)

static TRGenerateEvent_t ToneRlyGenEvent7[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent7, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent7, 128)

static TRGenerateEvent_t ToneRlyGenEvent8[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent8, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent8, 128)

static TRGenerateEvent_t ToneRlyGenEvent9[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent9, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent9, 128)

static TRGenerateEvent_t ToneRlyGenEvent10[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent10, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent10, 128)

static TRGenerateEvent_t ToneRlyGenEvent11[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent11, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent11, 128)

static TRGenerateEvent_t ToneRlyGenEvent12[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent12, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent12, 128)

static TRGenerateEvent_t ToneRlyGenEvent13[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent13, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent13, 128)

static TRGenerateEvent_t ToneRlyGenEvent14[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent14, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent14, 128)

static TRGenerateEvent_t ToneRlyGenEvent15[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent15, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent15, 128)

static TRGenerateEvent_t ToneRlyGenEvent16[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent16, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent16, 128)

static TRGenerateEvent_t ToneRlyGenEvent17[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent17, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent17, 128)

static TRGenerateEvent_t ToneRlyGenEvent18[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent18, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent18, 128)

static TRGenerateEvent_t ToneRlyGenEvent19[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent19, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent19, 128)

static TRGenerateEvent_t ToneRlyGenEvent20[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent20, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent20, 128)

static TRGenerateEvent_t ToneRlyGenEvent21[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent21, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent21, 128)

static TRGenerateEvent_t ToneRlyGenEvent22[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent22, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent22, 128)

static TRGenerateEvent_t ToneRlyGenEvent23[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent23, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent23, 128)

static TRGenerateEvent_t ToneRlyGenEvent24[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent24, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent24, 128)

static TRGenerateEvent_t ToneRlyGenEvent25[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent25, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent25, 128)

static TRGenerateEvent_t ToneRlyGenEvent26[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent26, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent26, 128)

static TRGenerateEvent_t ToneRlyGenEvent27[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent27, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent27, 128)

static TRGenerateEvent_t ToneRlyGenEvent28[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent28, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent28, 128)

static TRGenerateEvent_t ToneRlyGenEvent29[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent29, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent29, 128)

static TRGenerateEvent_t ToneRlyGenEvent30[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent30, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent30, 128)

static TRGenerateEvent_t ToneRlyGenEvent31[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent31, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent31, 128)

static TRGenerateEvent_t ToneRlyGenEvent32[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent32, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent32, 128)

static TRGenerateEvent_t ToneRlyGenEvent33[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent33, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent33, 128)

static TRGenerateEvent_t ToneRlyGenEvent34[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent34, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent34, 128)

static TRGenerateEvent_t ToneRlyGenEvent35[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent35, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent35, 128)

static TRGenerateEvent_t ToneRlyGenEvent36[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent36, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent36, 128)

static TRGenerateEvent_t ToneRlyGenEvent37[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent37, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent37, 128)

static TRGenerateEvent_t ToneRlyGenEvent38[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent38, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent38, 128)

static TRGenerateEvent_t ToneRlyGenEvent39[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent39, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent39, 128)

static TRGenerateEvent_t ToneRlyGenEvent40[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent40, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent40, 128)

static TRGenerateEvent_t ToneRlyGenEvent41[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent41, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent41, 128)

static TRGenerateEvent_t ToneRlyGenEvent42[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent42, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent42, 128)

static TRGenerateEvent_t ToneRlyGenEvent43[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent43, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent43, 128)

static TRGenerateEvent_t ToneRlyGenEvent44[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent44, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent44, 128)

static TRGenerateEvent_t ToneRlyGenEvent45[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent45, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent45, 128)

static TRGenerateEvent_t ToneRlyGenEvent46[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent46, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent46, 128)

static TRGenerateEvent_t ToneRlyGenEvent47[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent47, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent47, 128)

static TRGenerateEvent_t ToneRlyGenEvent48[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent48, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent48, 128)

static TRGenerateEvent_t ToneRlyGenEvent49[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent49, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent49, 128)

static TRGenerateEvent_t ToneRlyGenEvent50[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent50, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent50, 128)

static TRGenerateEvent_t ToneRlyGenEvent51[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent51, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent51, 128)

static TRGenerateEvent_t ToneRlyGenEvent52[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent52, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent52, 128)

static TRGenerateEvent_t ToneRlyGenEvent53[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent53, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent53, 128)

static TRGenerateEvent_t ToneRlyGenEvent54[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent54, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent54, 128)

static TRGenerateEvent_t ToneRlyGenEvent55[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent55, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent55, 128)

static TRGenerateEvent_t ToneRlyGenEvent56[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent56, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent56, 128)

static TRGenerateEvent_t ToneRlyGenEvent57[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent57, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent57, 128)

static TRGenerateEvent_t ToneRlyGenEvent58[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent58, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent58, 128)

static TRGenerateEvent_t ToneRlyGenEvent59[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent59, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent59, 128)

static TRGenerateEvent_t ToneRlyGenEvent60[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent60, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent60, 128)

static TRGenerateEvent_t ToneRlyGenEvent61[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent61, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent61, 128)

static TRGenerateEvent_t ToneRlyGenEvent62[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent62, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent62, 128)

static TRGenerateEvent_t ToneRlyGenEvent63[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent63, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent63, 128)

static TRGenerateEvent_t ToneRlyGenEvent64[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent64, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent64, 128)

static TRGenerateEvent_t ToneRlyGenEvent65[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent65, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent65, 128)

static TRGenerateEvent_t ToneRlyGenEvent66[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent66, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent66, 128)

static TRGenerateEvent_t ToneRlyGenEvent67[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent67, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent67, 128)

static TRGenerateEvent_t ToneRlyGenEvent68[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent68, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent68, 128)

static TRGenerateEvent_t ToneRlyGenEvent69[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent69, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent69, 128)

static TRGenerateEvent_t ToneRlyGenEvent70[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent70, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent70, 128)

static TRGenerateEvent_t ToneRlyGenEvent71[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent71, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent71, 128)

static TRGenerateEvent_t ToneRlyGenEvent72[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent72, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent72, 128)

static TRGenerateEvent_t ToneRlyGenEvent73[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent73, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent73, 128)

static TRGenerateEvent_t ToneRlyGenEvent74[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent74, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent74, 128)

static TRGenerateEvent_t ToneRlyGenEvent75[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent75, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent75, 128)

static TRGenerateEvent_t ToneRlyGenEvent76[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent76, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent76, 128)

static TRGenerateEvent_t ToneRlyGenEvent77[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent77, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent77, 128)

static TRGenerateEvent_t ToneRlyGenEvent78[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent78, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent78, 128)

static TRGenerateEvent_t ToneRlyGenEvent79[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent79, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent79, 128)

static TRGenerateEvent_t ToneRlyGenEvent80[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent80, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent80, 128)

static TRGenerateEvent_t ToneRlyGenEvent81[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent81, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent81, 128)

static TRGenerateEvent_t ToneRlyGenEvent82[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent82, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent82, 128)

static TRGenerateEvent_t ToneRlyGenEvent83[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent83, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent83, 128)

static TRGenerateEvent_t ToneRlyGenEvent84[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent84, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent84, 128)

static TRGenerateEvent_t ToneRlyGenEvent85[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent85, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent85, 128)

static TRGenerateEvent_t ToneRlyGenEvent86[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent86, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent86, 128)

static TRGenerateEvent_t ToneRlyGenEvent87[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent87, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent87, 128)

static TRGenerateEvent_t ToneRlyGenEvent88[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent88, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent88, 128)

static TRGenerateEvent_t ToneRlyGenEvent89[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent89, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent89, 128)

static TRGenerateEvent_t ToneRlyGenEvent90[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent90, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent90, 128)

static TRGenerateEvent_t ToneRlyGenEvent91[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent91, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent91, 128)

static TRGenerateEvent_t ToneRlyGenEvent92[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent92, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent92, 128)

static TRGenerateEvent_t ToneRlyGenEvent93[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent93, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent93, 128)

static TRGenerateEvent_t ToneRlyGenEvent94[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent94, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent94, 128)

static TRGenerateEvent_t ToneRlyGenEvent95[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent95, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent95, 128)

static TRGenerateEvent_t ToneRlyGenEvent96[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent96, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent96, 128)

static TRGenerateEvent_t ToneRlyGenEvent97[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent97, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent97, 128)

static TRGenerateEvent_t ToneRlyGenEvent98[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent98, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent98, 128)

static TRGenerateEvent_t ToneRlyGenEvent99[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent99, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent99, 128)

static TRGenerateEvent_t ToneRlyGenEvent100[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent100, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent100, 128)

static TRGenerateEvent_t ToneRlyGenEvent101[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent101, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent101, 128)

static TRGenerateEvent_t ToneRlyGenEvent102[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent102, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent102, 128)

static TRGenerateEvent_t ToneRlyGenEvent103[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent103, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent103, 128)

static TRGenerateEvent_t ToneRlyGenEvent104[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent104, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent104, 128)

static TRGenerateEvent_t ToneRlyGenEvent105[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent105, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent105, 128)

static TRGenerateEvent_t ToneRlyGenEvent106[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent106, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent106, 128)

static TRGenerateEvent_t ToneRlyGenEvent107[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent107, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent107, 128)

static TRGenerateEvent_t ToneRlyGenEvent108[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent108, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent108, 128)

static TRGenerateEvent_t ToneRlyGenEvent109[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent109, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent109, 128)

static TRGenerateEvent_t ToneRlyGenEvent110[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent110, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent110, 128)

static TRGenerateEvent_t ToneRlyGenEvent111[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent111, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent111, 128)

static TRGenerateEvent_t ToneRlyGenEvent112[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent112, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent112, 128)

static TRGenerateEvent_t ToneRlyGenEvent113[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent113, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent113, 128)

static TRGenerateEvent_t ToneRlyGenEvent114[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent114, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent114, 128)

static TRGenerateEvent_t ToneRlyGenEvent115[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent115, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent115, 128)

static TRGenerateEvent_t ToneRlyGenEvent116[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent116, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent116, 128)

static TRGenerateEvent_t ToneRlyGenEvent117[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent117, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent117, 128)

static TRGenerateEvent_t ToneRlyGenEvent118[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent118, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent118, 128)

static TRGenerateEvent_t ToneRlyGenEvent119[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent119, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent119, 128)

static TRGenerateEvent_t ToneRlyGenEvent120[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent120, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent120, 128)

static TRGenerateEvent_t ToneRlyGenEvent121[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent121, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent121, 128)

static TRGenerateEvent_t ToneRlyGenEvent122[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent122, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent122, 128)

static TRGenerateEvent_t ToneRlyGenEvent123[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent123, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent123, 128)

static TRGenerateEvent_t ToneRlyGenEvent124[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent124, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent124, 128)

static TRGenerateEvent_t ToneRlyGenEvent125[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent125, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent125, 128)

static TRGenerateEvent_t ToneRlyGenEvent126[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent126, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent126, 128)

static TRGenerateEvent_t ToneRlyGenEvent127[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent127, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent127, 128)

static TRGenerateEvent_t ToneRlyGenEvent128[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent128, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent128, 128)

static TRGenerateEvent_t ToneRlyGenEvent129[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent129, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent129, 128)

static TRGenerateEvent_t ToneRlyGenEvent130[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent130, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent130, 128)

static TRGenerateEvent_t ToneRlyGenEvent131[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent131, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent131, 128)

static TRGenerateEvent_t ToneRlyGenEvent132[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent132, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent132, 128)

static TRGenerateEvent_t ToneRlyGenEvent133[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent133, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent133, 128)

static TRGenerateEvent_t ToneRlyGenEvent134[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent134, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent134, 128)

static TRGenerateEvent_t ToneRlyGenEvent135[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent135, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent135, 128)

static TRGenerateEvent_t ToneRlyGenEvent136[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent136, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent136, 128)

static TRGenerateEvent_t ToneRlyGenEvent137[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent137, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent137, 128)

static TRGenerateEvent_t ToneRlyGenEvent138[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent138, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent138, 128)

static TRGenerateEvent_t ToneRlyGenEvent139[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent139, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent139, 128)

static TRGenerateEvent_t ToneRlyGenEvent140[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent140, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent140, 128)

static TRGenerateEvent_t ToneRlyGenEvent141[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent141, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent141, 128)

static TRGenerateEvent_t ToneRlyGenEvent142[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent142, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent142, 128)

static TRGenerateEvent_t ToneRlyGenEvent143[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent143, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent143, 128)

static TRGenerateEvent_t ToneRlyGenEvent144[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent144, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent144, 128)

static TRGenerateEvent_t ToneRlyGenEvent145[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent145, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent145, 128)

static TRGenerateEvent_t ToneRlyGenEvent146[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent146, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent146, 128)

static TRGenerateEvent_t ToneRlyGenEvent147[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent147, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent147, 128)

static TRGenerateEvent_t ToneRlyGenEvent148[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent148, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent148, 128)

static TRGenerateEvent_t ToneRlyGenEvent149[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent149, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent149, 128)

static TRGenerateEvent_t ToneRlyGenEvent150[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent150, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent150, 128)

static TRGenerateEvent_t ToneRlyGenEvent151[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent151, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent151, 128)

static TRGenerateEvent_t ToneRlyGenEvent152[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent152, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent152, 128)

static TRGenerateEvent_t ToneRlyGenEvent153[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent153, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent153, 128)

static TRGenerateEvent_t ToneRlyGenEvent154[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent154, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent154, 128)

static TRGenerateEvent_t ToneRlyGenEvent155[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent155, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent155, 128)

static TRGenerateEvent_t ToneRlyGenEvent156[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent156, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent156, 128)

static TRGenerateEvent_t ToneRlyGenEvent157[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent157, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent157, 128)

static TRGenerateEvent_t ToneRlyGenEvent158[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent158, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent158, 128)

static TRGenerateEvent_t ToneRlyGenEvent159[NUM_TRGEN_EVENTS];
#pragma DATA_SECTION (ToneRlyGenEvent159, "CHAN_INST_DATA:ToneGen")
#pragma DATA_ALIGN   (ToneRlyGenEvent159, 128)

TRGenerateEvent_t* const ToneRlyGenEvent[] = {
	&ToneRlyGenEvent0[0],
	&ToneRlyGenEvent1[0],
	&ToneRlyGenEvent2[0],
	&ToneRlyGenEvent3[0],
	&ToneRlyGenEvent4[0],
	&ToneRlyGenEvent5[0],
	&ToneRlyGenEvent6[0],
	&ToneRlyGenEvent7[0],
	&ToneRlyGenEvent8[0],
	&ToneRlyGenEvent9[0],
	&ToneRlyGenEvent10[0],
	&ToneRlyGenEvent11[0],
	&ToneRlyGenEvent12[0],
	&ToneRlyGenEvent13[0],
	&ToneRlyGenEvent14[0],
	&ToneRlyGenEvent15[0],
	&ToneRlyGenEvent16[0],
	&ToneRlyGenEvent17[0],
	&ToneRlyGenEvent18[0],
	&ToneRlyGenEvent19[0],
	&ToneRlyGenEvent20[0],
	&ToneRlyGenEvent21[0],
	&ToneRlyGenEvent22[0],
	&ToneRlyGenEvent23[0],
	&ToneRlyGenEvent24[0],
	&ToneRlyGenEvent25[0],
	&ToneRlyGenEvent26[0],
	&ToneRlyGenEvent27[0],
	&ToneRlyGenEvent28[0],
	&ToneRlyGenEvent29[0],
	&ToneRlyGenEvent30[0],
	&ToneRlyGenEvent31[0],
	&ToneRlyGenEvent32[0],
	&ToneRlyGenEvent33[0],
	&ToneRlyGenEvent34[0],
	&ToneRlyGenEvent35[0],
	&ToneRlyGenEvent36[0],
	&ToneRlyGenEvent37[0],
	&ToneRlyGenEvent38[0],
	&ToneRlyGenEvent39[0],
	&ToneRlyGenEvent40[0],
	&ToneRlyGenEvent41[0],
	&ToneRlyGenEvent42[0],
	&ToneRlyGenEvent43[0],
	&ToneRlyGenEvent44[0],
	&ToneRlyGenEvent45[0],
	&ToneRlyGenEvent46[0],
	&ToneRlyGenEvent47[0],
	&ToneRlyGenEvent48[0],
	&ToneRlyGenEvent49[0],
	&ToneRlyGenEvent50[0],
	&ToneRlyGenEvent51[0],
	&ToneRlyGenEvent52[0],
	&ToneRlyGenEvent53[0],
	&ToneRlyGenEvent54[0],
	&ToneRlyGenEvent55[0],
	&ToneRlyGenEvent56[0],
	&ToneRlyGenEvent57[0],
	&ToneRlyGenEvent58[0],
	&ToneRlyGenEvent59[0],
	&ToneRlyGenEvent60[0],
	&ToneRlyGenEvent61[0],
	&ToneRlyGenEvent62[0],
	&ToneRlyGenEvent63[0],
	&ToneRlyGenEvent64[0],
	&ToneRlyGenEvent65[0],
	&ToneRlyGenEvent66[0],
	&ToneRlyGenEvent67[0],
	&ToneRlyGenEvent68[0],
	&ToneRlyGenEvent69[0],
	&ToneRlyGenEvent70[0],
	&ToneRlyGenEvent71[0],
	&ToneRlyGenEvent72[0],
	&ToneRlyGenEvent73[0],
	&ToneRlyGenEvent74[0],
	&ToneRlyGenEvent75[0],
	&ToneRlyGenEvent76[0],
	&ToneRlyGenEvent77[0],
	&ToneRlyGenEvent78[0],
	&ToneRlyGenEvent79[0],
	&ToneRlyGenEvent80[0],
	&ToneRlyGenEvent81[0],
	&ToneRlyGenEvent82[0],
	&ToneRlyGenEvent83[0],
	&ToneRlyGenEvent84[0],
	&ToneRlyGenEvent85[0],
	&ToneRlyGenEvent86[0],
	&ToneRlyGenEvent87[0],
	&ToneRlyGenEvent88[0],
	&ToneRlyGenEvent89[0],
	&ToneRlyGenEvent90[0],
	&ToneRlyGenEvent91[0],
	&ToneRlyGenEvent92[0],
	&ToneRlyGenEvent93[0],
	&ToneRlyGenEvent94[0],
	&ToneRlyGenEvent95[0],
	&ToneRlyGenEvent96[0],
	&ToneRlyGenEvent97[0],
	&ToneRlyGenEvent98[0],
	&ToneRlyGenEvent99[0],
	&ToneRlyGenEvent100[0],
	&ToneRlyGenEvent101[0],
	&ToneRlyGenEvent102[0],
	&ToneRlyGenEvent103[0],
	&ToneRlyGenEvent104[0],
	&ToneRlyGenEvent105[0],
	&ToneRlyGenEvent106[0],
	&ToneRlyGenEvent107[0],
	&ToneRlyGenEvent108[0],
	&ToneRlyGenEvent109[0],
	&ToneRlyGenEvent110[0],
	&ToneRlyGenEvent111[0],
	&ToneRlyGenEvent112[0],
	&ToneRlyGenEvent113[0],
	&ToneRlyGenEvent114[0],
	&ToneRlyGenEvent115[0],
	&ToneRlyGenEvent116[0],
	&ToneRlyGenEvent117[0],
	&ToneRlyGenEvent118[0],
	&ToneRlyGenEvent119[0],
	&ToneRlyGenEvent120[0],
	&ToneRlyGenEvent121[0],
	&ToneRlyGenEvent122[0],
	&ToneRlyGenEvent123[0],
	&ToneRlyGenEvent124[0],
	&ToneRlyGenEvent125[0],
	&ToneRlyGenEvent126[0],
	&ToneRlyGenEvent127[0],
	&ToneRlyGenEvent128[0],
	&ToneRlyGenEvent129[0],
	&ToneRlyGenEvent130[0],
	&ToneRlyGenEvent131[0],
	&ToneRlyGenEvent132[0],
	&ToneRlyGenEvent133[0],
	&ToneRlyGenEvent134[0],
	&ToneRlyGenEvent135[0],
	&ToneRlyGenEvent136[0],
	&ToneRlyGenEvent137[0],
	&ToneRlyGenEvent138[0],
	&ToneRlyGenEvent139[0],
	&ToneRlyGenEvent140[0],
	&ToneRlyGenEvent141[0],
	&ToneRlyGenEvent142[0],
	&ToneRlyGenEvent143[0],
	&ToneRlyGenEvent144[0],
	&ToneRlyGenEvent145[0],
	&ToneRlyGenEvent146[0],
	&ToneRlyGenEvent147[0],
	&ToneRlyGenEvent148[0],
	&ToneRlyGenEvent149[0],
	&ToneRlyGenEvent150[0],
	&ToneRlyGenEvent151[0],
	&ToneRlyGenEvent152[0],
	&ToneRlyGenEvent153[0],
	&ToneRlyGenEvent154[0],
	&ToneRlyGenEvent155[0],
	&ToneRlyGenEvent156[0],
	&ToneRlyGenEvent157[0],
	&ToneRlyGenEvent158[0],
	&ToneRlyGenEvent159[0]
};
//}

//}




/*=============================== */
// VAD/CNG and AGC data structures.
//{      VADCNG_Instance_t       VadCngInstance STRUCTURES to CHAN_INST_DATA:VAD

VADCNG_Instance_t VadCngInstance0;
#pragma DATA_SECTION (VadCngInstance0, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance0, 128)

static VADCNG_Instance_t VadCngInstance1;
#pragma DATA_SECTION (VadCngInstance1, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance1, 128)

static VADCNG_Instance_t VadCngInstance2;
#pragma DATA_SECTION (VadCngInstance2, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance2, 128)

static VADCNG_Instance_t VadCngInstance3;
#pragma DATA_SECTION (VadCngInstance3, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance3, 128)

static VADCNG_Instance_t VadCngInstance4;
#pragma DATA_SECTION (VadCngInstance4, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance4, 128)

static VADCNG_Instance_t VadCngInstance5;
#pragma DATA_SECTION (VadCngInstance5, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance5, 128)

static VADCNG_Instance_t VadCngInstance6;
#pragma DATA_SECTION (VadCngInstance6, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance6, 128)

static VADCNG_Instance_t VadCngInstance7;
#pragma DATA_SECTION (VadCngInstance7, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance7, 128)

static VADCNG_Instance_t VadCngInstance8;
#pragma DATA_SECTION (VadCngInstance8, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance8, 128)

static VADCNG_Instance_t VadCngInstance9;
#pragma DATA_SECTION (VadCngInstance9, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance9, 128)

static VADCNG_Instance_t VadCngInstance10;
#pragma DATA_SECTION (VadCngInstance10, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance10, 128)

static VADCNG_Instance_t VadCngInstance11;
#pragma DATA_SECTION (VadCngInstance11, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance11, 128)

static VADCNG_Instance_t VadCngInstance12;
#pragma DATA_SECTION (VadCngInstance12, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance12, 128)

static VADCNG_Instance_t VadCngInstance13;
#pragma DATA_SECTION (VadCngInstance13, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance13, 128)

static VADCNG_Instance_t VadCngInstance14;
#pragma DATA_SECTION (VadCngInstance14, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance14, 128)

static VADCNG_Instance_t VadCngInstance15;
#pragma DATA_SECTION (VadCngInstance15, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance15, 128)

static VADCNG_Instance_t VadCngInstance16;
#pragma DATA_SECTION (VadCngInstance16, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance16, 128)

static VADCNG_Instance_t VadCngInstance17;
#pragma DATA_SECTION (VadCngInstance17, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance17, 128)

static VADCNG_Instance_t VadCngInstance18;
#pragma DATA_SECTION (VadCngInstance18, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance18, 128)

static VADCNG_Instance_t VadCngInstance19;
#pragma DATA_SECTION (VadCngInstance19, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance19, 128)

static VADCNG_Instance_t VadCngInstance20;
#pragma DATA_SECTION (VadCngInstance20, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance20, 128)

static VADCNG_Instance_t VadCngInstance21;
#pragma DATA_SECTION (VadCngInstance21, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance21, 128)

static VADCNG_Instance_t VadCngInstance22;
#pragma DATA_SECTION (VadCngInstance22, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance22, 128)

static VADCNG_Instance_t VadCngInstance23;
#pragma DATA_SECTION (VadCngInstance23, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance23, 128)

static VADCNG_Instance_t VadCngInstance24;
#pragma DATA_SECTION (VadCngInstance24, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance24, 128)

static VADCNG_Instance_t VadCngInstance25;
#pragma DATA_SECTION (VadCngInstance25, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance25, 128)

static VADCNG_Instance_t VadCngInstance26;
#pragma DATA_SECTION (VadCngInstance26, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance26, 128)

static VADCNG_Instance_t VadCngInstance27;
#pragma DATA_SECTION (VadCngInstance27, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance27, 128)

static VADCNG_Instance_t VadCngInstance28;
#pragma DATA_SECTION (VadCngInstance28, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance28, 128)

static VADCNG_Instance_t VadCngInstance29;
#pragma DATA_SECTION (VadCngInstance29, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance29, 128)

static VADCNG_Instance_t VadCngInstance30;
#pragma DATA_SECTION (VadCngInstance30, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance30, 128)

static VADCNG_Instance_t VadCngInstance31;
#pragma DATA_SECTION (VadCngInstance31, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance31, 128)

static VADCNG_Instance_t VadCngInstance32;
#pragma DATA_SECTION (VadCngInstance32, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance32, 128)

static VADCNG_Instance_t VadCngInstance33;
#pragma DATA_SECTION (VadCngInstance33, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance33, 128)

static VADCNG_Instance_t VadCngInstance34;
#pragma DATA_SECTION (VadCngInstance34, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance34, 128)

static VADCNG_Instance_t VadCngInstance35;
#pragma DATA_SECTION (VadCngInstance35, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance35, 128)

static VADCNG_Instance_t VadCngInstance36;
#pragma DATA_SECTION (VadCngInstance36, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance36, 128)

static VADCNG_Instance_t VadCngInstance37;
#pragma DATA_SECTION (VadCngInstance37, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance37, 128)

static VADCNG_Instance_t VadCngInstance38;
#pragma DATA_SECTION (VadCngInstance38, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance38, 128)

static VADCNG_Instance_t VadCngInstance39;
#pragma DATA_SECTION (VadCngInstance39, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance39, 128)

static VADCNG_Instance_t VadCngInstance40;
#pragma DATA_SECTION (VadCngInstance40, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance40, 128)

static VADCNG_Instance_t VadCngInstance41;
#pragma DATA_SECTION (VadCngInstance41, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance41, 128)

static VADCNG_Instance_t VadCngInstance42;
#pragma DATA_SECTION (VadCngInstance42, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance42, 128)

static VADCNG_Instance_t VadCngInstance43;
#pragma DATA_SECTION (VadCngInstance43, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance43, 128)

static VADCNG_Instance_t VadCngInstance44;
#pragma DATA_SECTION (VadCngInstance44, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance44, 128)

static VADCNG_Instance_t VadCngInstance45;
#pragma DATA_SECTION (VadCngInstance45, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance45, 128)

static VADCNG_Instance_t VadCngInstance46;
#pragma DATA_SECTION (VadCngInstance46, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance46, 128)

static VADCNG_Instance_t VadCngInstance47;
#pragma DATA_SECTION (VadCngInstance47, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance47, 128)

static VADCNG_Instance_t VadCngInstance48;
#pragma DATA_SECTION (VadCngInstance48, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance48, 128)

static VADCNG_Instance_t VadCngInstance49;
#pragma DATA_SECTION (VadCngInstance49, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance49, 128)

static VADCNG_Instance_t VadCngInstance50;
#pragma DATA_SECTION (VadCngInstance50, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance50, 128)

static VADCNG_Instance_t VadCngInstance51;
#pragma DATA_SECTION (VadCngInstance51, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance51, 128)

static VADCNG_Instance_t VadCngInstance52;
#pragma DATA_SECTION (VadCngInstance52, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance52, 128)

static VADCNG_Instance_t VadCngInstance53;
#pragma DATA_SECTION (VadCngInstance53, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance53, 128)

static VADCNG_Instance_t VadCngInstance54;
#pragma DATA_SECTION (VadCngInstance54, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance54, 128)

static VADCNG_Instance_t VadCngInstance55;
#pragma DATA_SECTION (VadCngInstance55, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance55, 128)

static VADCNG_Instance_t VadCngInstance56;
#pragma DATA_SECTION (VadCngInstance56, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance56, 128)

static VADCNG_Instance_t VadCngInstance57;
#pragma DATA_SECTION (VadCngInstance57, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance57, 128)

static VADCNG_Instance_t VadCngInstance58;
#pragma DATA_SECTION (VadCngInstance58, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance58, 128)

static VADCNG_Instance_t VadCngInstance59;
#pragma DATA_SECTION (VadCngInstance59, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance59, 128)

static VADCNG_Instance_t VadCngInstance60;
#pragma DATA_SECTION (VadCngInstance60, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance60, 128)

static VADCNG_Instance_t VadCngInstance61;
#pragma DATA_SECTION (VadCngInstance61, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance61, 128)

static VADCNG_Instance_t VadCngInstance62;
#pragma DATA_SECTION (VadCngInstance62, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance62, 128)

static VADCNG_Instance_t VadCngInstance63;
#pragma DATA_SECTION (VadCngInstance63, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance63, 128)

static VADCNG_Instance_t VadCngInstance64;
#pragma DATA_SECTION (VadCngInstance64, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance64, 128)

static VADCNG_Instance_t VadCngInstance65;
#pragma DATA_SECTION (VadCngInstance65, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance65, 128)

static VADCNG_Instance_t VadCngInstance66;
#pragma DATA_SECTION (VadCngInstance66, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance66, 128)

static VADCNG_Instance_t VadCngInstance67;
#pragma DATA_SECTION (VadCngInstance67, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance67, 128)

static VADCNG_Instance_t VadCngInstance68;
#pragma DATA_SECTION (VadCngInstance68, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance68, 128)

static VADCNG_Instance_t VadCngInstance69;
#pragma DATA_SECTION (VadCngInstance69, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance69, 128)

static VADCNG_Instance_t VadCngInstance70;
#pragma DATA_SECTION (VadCngInstance70, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance70, 128)

static VADCNG_Instance_t VadCngInstance71;
#pragma DATA_SECTION (VadCngInstance71, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance71, 128)

static VADCNG_Instance_t VadCngInstance72;
#pragma DATA_SECTION (VadCngInstance72, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance72, 128)

static VADCNG_Instance_t VadCngInstance73;
#pragma DATA_SECTION (VadCngInstance73, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance73, 128)

static VADCNG_Instance_t VadCngInstance74;
#pragma DATA_SECTION (VadCngInstance74, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance74, 128)

static VADCNG_Instance_t VadCngInstance75;
#pragma DATA_SECTION (VadCngInstance75, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance75, 128)

static VADCNG_Instance_t VadCngInstance76;
#pragma DATA_SECTION (VadCngInstance76, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance76, 128)

static VADCNG_Instance_t VadCngInstance77;
#pragma DATA_SECTION (VadCngInstance77, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance77, 128)

static VADCNG_Instance_t VadCngInstance78;
#pragma DATA_SECTION (VadCngInstance78, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance78, 128)

static VADCNG_Instance_t VadCngInstance79;
#pragma DATA_SECTION (VadCngInstance79, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance79, 128)

static VADCNG_Instance_t VadCngInstance80;
#pragma DATA_SECTION (VadCngInstance80, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance80, 128)

static VADCNG_Instance_t VadCngInstance81;
#pragma DATA_SECTION (VadCngInstance81, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance81, 128)

static VADCNG_Instance_t VadCngInstance82;
#pragma DATA_SECTION (VadCngInstance82, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance82, 128)

static VADCNG_Instance_t VadCngInstance83;
#pragma DATA_SECTION (VadCngInstance83, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance83, 128)

static VADCNG_Instance_t VadCngInstance84;
#pragma DATA_SECTION (VadCngInstance84, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance84, 128)

static VADCNG_Instance_t VadCngInstance85;
#pragma DATA_SECTION (VadCngInstance85, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance85, 128)

static VADCNG_Instance_t VadCngInstance86;
#pragma DATA_SECTION (VadCngInstance86, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance86, 128)

static VADCNG_Instance_t VadCngInstance87;
#pragma DATA_SECTION (VadCngInstance87, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance87, 128)

static VADCNG_Instance_t VadCngInstance88;
#pragma DATA_SECTION (VadCngInstance88, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance88, 128)

static VADCNG_Instance_t VadCngInstance89;
#pragma DATA_SECTION (VadCngInstance89, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance89, 128)

static VADCNG_Instance_t VadCngInstance90;
#pragma DATA_SECTION (VadCngInstance90, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance90, 128)

static VADCNG_Instance_t VadCngInstance91;
#pragma DATA_SECTION (VadCngInstance91, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance91, 128)

static VADCNG_Instance_t VadCngInstance92;
#pragma DATA_SECTION (VadCngInstance92, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance92, 128)

static VADCNG_Instance_t VadCngInstance93;
#pragma DATA_SECTION (VadCngInstance93, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance93, 128)

static VADCNG_Instance_t VadCngInstance94;
#pragma DATA_SECTION (VadCngInstance94, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance94, 128)

static VADCNG_Instance_t VadCngInstance95;
#pragma DATA_SECTION (VadCngInstance95, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance95, 128)

static VADCNG_Instance_t VadCngInstance96;
#pragma DATA_SECTION (VadCngInstance96, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance96, 128)

static VADCNG_Instance_t VadCngInstance97;
#pragma DATA_SECTION (VadCngInstance97, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance97, 128)

static VADCNG_Instance_t VadCngInstance98;
#pragma DATA_SECTION (VadCngInstance98, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance98, 128)

static VADCNG_Instance_t VadCngInstance99;
#pragma DATA_SECTION (VadCngInstance99, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance99, 128)

static VADCNG_Instance_t VadCngInstance100;
#pragma DATA_SECTION (VadCngInstance100, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance100, 128)

static VADCNG_Instance_t VadCngInstance101;
#pragma DATA_SECTION (VadCngInstance101, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance101, 128)

static VADCNG_Instance_t VadCngInstance102;
#pragma DATA_SECTION (VadCngInstance102, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance102, 128)

static VADCNG_Instance_t VadCngInstance103;
#pragma DATA_SECTION (VadCngInstance103, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance103, 128)

static VADCNG_Instance_t VadCngInstance104;
#pragma DATA_SECTION (VadCngInstance104, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance104, 128)

static VADCNG_Instance_t VadCngInstance105;
#pragma DATA_SECTION (VadCngInstance105, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance105, 128)

static VADCNG_Instance_t VadCngInstance106;
#pragma DATA_SECTION (VadCngInstance106, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance106, 128)

static VADCNG_Instance_t VadCngInstance107;
#pragma DATA_SECTION (VadCngInstance107, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance107, 128)

static VADCNG_Instance_t VadCngInstance108;
#pragma DATA_SECTION (VadCngInstance108, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance108, 128)

static VADCNG_Instance_t VadCngInstance109;
#pragma DATA_SECTION (VadCngInstance109, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance109, 128)

static VADCNG_Instance_t VadCngInstance110;
#pragma DATA_SECTION (VadCngInstance110, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance110, 128)

static VADCNG_Instance_t VadCngInstance111;
#pragma DATA_SECTION (VadCngInstance111, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance111, 128)

static VADCNG_Instance_t VadCngInstance112;
#pragma DATA_SECTION (VadCngInstance112, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance112, 128)

static VADCNG_Instance_t VadCngInstance113;
#pragma DATA_SECTION (VadCngInstance113, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance113, 128)

static VADCNG_Instance_t VadCngInstance114;
#pragma DATA_SECTION (VadCngInstance114, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance114, 128)

static VADCNG_Instance_t VadCngInstance115;
#pragma DATA_SECTION (VadCngInstance115, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance115, 128)

static VADCNG_Instance_t VadCngInstance116;
#pragma DATA_SECTION (VadCngInstance116, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance116, 128)

static VADCNG_Instance_t VadCngInstance117;
#pragma DATA_SECTION (VadCngInstance117, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance117, 128)

static VADCNG_Instance_t VadCngInstance118;
#pragma DATA_SECTION (VadCngInstance118, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance118, 128)

static VADCNG_Instance_t VadCngInstance119;
#pragma DATA_SECTION (VadCngInstance119, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance119, 128)

static VADCNG_Instance_t VadCngInstance120;
#pragma DATA_SECTION (VadCngInstance120, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance120, 128)

static VADCNG_Instance_t VadCngInstance121;
#pragma DATA_SECTION (VadCngInstance121, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance121, 128)

static VADCNG_Instance_t VadCngInstance122;
#pragma DATA_SECTION (VadCngInstance122, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance122, 128)

static VADCNG_Instance_t VadCngInstance123;
#pragma DATA_SECTION (VadCngInstance123, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance123, 128)

static VADCNG_Instance_t VadCngInstance124;
#pragma DATA_SECTION (VadCngInstance124, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance124, 128)

static VADCNG_Instance_t VadCngInstance125;
#pragma DATA_SECTION (VadCngInstance125, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance125, 128)

static VADCNG_Instance_t VadCngInstance126;
#pragma DATA_SECTION (VadCngInstance126, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance126, 128)

static VADCNG_Instance_t VadCngInstance127;
#pragma DATA_SECTION (VadCngInstance127, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance127, 128)

static VADCNG_Instance_t VadCngInstance128;
#pragma DATA_SECTION (VadCngInstance128, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance128, 128)

static VADCNG_Instance_t VadCngInstance129;
#pragma DATA_SECTION (VadCngInstance129, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance129, 128)

static VADCNG_Instance_t VadCngInstance130;
#pragma DATA_SECTION (VadCngInstance130, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance130, 128)

static VADCNG_Instance_t VadCngInstance131;
#pragma DATA_SECTION (VadCngInstance131, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance131, 128)

static VADCNG_Instance_t VadCngInstance132;
#pragma DATA_SECTION (VadCngInstance132, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance132, 128)

static VADCNG_Instance_t VadCngInstance133;
#pragma DATA_SECTION (VadCngInstance133, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance133, 128)

static VADCNG_Instance_t VadCngInstance134;
#pragma DATA_SECTION (VadCngInstance134, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance134, 128)

static VADCNG_Instance_t VadCngInstance135;
#pragma DATA_SECTION (VadCngInstance135, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance135, 128)

static VADCNG_Instance_t VadCngInstance136;
#pragma DATA_SECTION (VadCngInstance136, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance136, 128)

static VADCNG_Instance_t VadCngInstance137;
#pragma DATA_SECTION (VadCngInstance137, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance137, 128)

static VADCNG_Instance_t VadCngInstance138;
#pragma DATA_SECTION (VadCngInstance138, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance138, 128)

static VADCNG_Instance_t VadCngInstance139;
#pragma DATA_SECTION (VadCngInstance139, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance139, 128)

static VADCNG_Instance_t VadCngInstance140;
#pragma DATA_SECTION (VadCngInstance140, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance140, 128)

static VADCNG_Instance_t VadCngInstance141;
#pragma DATA_SECTION (VadCngInstance141, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance141, 128)

static VADCNG_Instance_t VadCngInstance142;
#pragma DATA_SECTION (VadCngInstance142, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance142, 128)

static VADCNG_Instance_t VadCngInstance143;
#pragma DATA_SECTION (VadCngInstance143, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance143, 128)

static VADCNG_Instance_t VadCngInstance144;
#pragma DATA_SECTION (VadCngInstance144, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance144, 128)

static VADCNG_Instance_t VadCngInstance145;
#pragma DATA_SECTION (VadCngInstance145, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance145, 128)

static VADCNG_Instance_t VadCngInstance146;
#pragma DATA_SECTION (VadCngInstance146, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance146, 128)

static VADCNG_Instance_t VadCngInstance147;
#pragma DATA_SECTION (VadCngInstance147, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance147, 128)

static VADCNG_Instance_t VadCngInstance148;
#pragma DATA_SECTION (VadCngInstance148, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance148, 128)

static VADCNG_Instance_t VadCngInstance149;
#pragma DATA_SECTION (VadCngInstance149, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance149, 128)

static VADCNG_Instance_t VadCngInstance150;
#pragma DATA_SECTION (VadCngInstance150, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance150, 128)

static VADCNG_Instance_t VadCngInstance151;
#pragma DATA_SECTION (VadCngInstance151, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance151, 128)

static VADCNG_Instance_t VadCngInstance152;
#pragma DATA_SECTION (VadCngInstance152, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance152, 128)

static VADCNG_Instance_t VadCngInstance153;
#pragma DATA_SECTION (VadCngInstance153, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance153, 128)

static VADCNG_Instance_t VadCngInstance154;
#pragma DATA_SECTION (VadCngInstance154, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance154, 128)

static VADCNG_Instance_t VadCngInstance155;
#pragma DATA_SECTION (VadCngInstance155, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance155, 128)

static VADCNG_Instance_t VadCngInstance156;
#pragma DATA_SECTION (VadCngInstance156, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance156, 128)

static VADCNG_Instance_t VadCngInstance157;
#pragma DATA_SECTION (VadCngInstance157, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance157, 128)

static VADCNG_Instance_t VadCngInstance158;
#pragma DATA_SECTION (VadCngInstance158, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance158, 128)

static VADCNG_Instance_t VadCngInstance159;
#pragma DATA_SECTION (VadCngInstance159, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance159, 128)

static VADCNG_Instance_t VadCngInstance160;
#pragma DATA_SECTION (VadCngInstance160, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance160, 128)

static VADCNG_Instance_t VadCngInstance161;
#pragma DATA_SECTION (VadCngInstance161, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance161, 128)

static VADCNG_Instance_t VadCngInstance162;
#pragma DATA_SECTION (VadCngInstance162, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance162, 128)

static VADCNG_Instance_t VadCngInstance163;
#pragma DATA_SECTION (VadCngInstance163, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance163, 128)

static VADCNG_Instance_t VadCngInstance164;
#pragma DATA_SECTION (VadCngInstance164, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance164, 128)

static VADCNG_Instance_t VadCngInstance165;
#pragma DATA_SECTION (VadCngInstance165, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance165, 128)

static VADCNG_Instance_t VadCngInstance166;
#pragma DATA_SECTION (VadCngInstance166, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance166, 128)

static VADCNG_Instance_t VadCngInstance167;
#pragma DATA_SECTION (VadCngInstance167, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance167, 128)

static VADCNG_Instance_t VadCngInstance168;
#pragma DATA_SECTION (VadCngInstance168, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance168, 128)

static VADCNG_Instance_t VadCngInstance169;
#pragma DATA_SECTION (VadCngInstance169, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance169, 128)

static VADCNG_Instance_t VadCngInstance170;
#pragma DATA_SECTION (VadCngInstance170, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance170, 128)

static VADCNG_Instance_t VadCngInstance171;
#pragma DATA_SECTION (VadCngInstance171, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance171, 128)

static VADCNG_Instance_t VadCngInstance172;
#pragma DATA_SECTION (VadCngInstance172, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance172, 128)

static VADCNG_Instance_t VadCngInstance173;
#pragma DATA_SECTION (VadCngInstance173, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance173, 128)

static VADCNG_Instance_t VadCngInstance174;
#pragma DATA_SECTION (VadCngInstance174, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance174, 128)

static VADCNG_Instance_t VadCngInstance175;
#pragma DATA_SECTION (VadCngInstance175, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance175, 128)

static VADCNG_Instance_t VadCngInstance176;
#pragma DATA_SECTION (VadCngInstance176, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance176, 128)

static VADCNG_Instance_t VadCngInstance177;
#pragma DATA_SECTION (VadCngInstance177, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance177, 128)

static VADCNG_Instance_t VadCngInstance178;
#pragma DATA_SECTION (VadCngInstance178, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance178, 128)

static VADCNG_Instance_t VadCngInstance179;
#pragma DATA_SECTION (VadCngInstance179, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance179, 128)

static VADCNG_Instance_t VadCngInstance180;
#pragma DATA_SECTION (VadCngInstance180, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance180, 128)

static VADCNG_Instance_t VadCngInstance181;
#pragma DATA_SECTION (VadCngInstance181, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance181, 128)

static VADCNG_Instance_t VadCngInstance182;
#pragma DATA_SECTION (VadCngInstance182, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance182, 128)

static VADCNG_Instance_t VadCngInstance183;
#pragma DATA_SECTION (VadCngInstance183, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance183, 128)

static VADCNG_Instance_t VadCngInstance184;
#pragma DATA_SECTION (VadCngInstance184, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance184, 128)

static VADCNG_Instance_t VadCngInstance185;
#pragma DATA_SECTION (VadCngInstance185, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance185, 128)

static VADCNG_Instance_t VadCngInstance186;
#pragma DATA_SECTION (VadCngInstance186, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance186, 128)

static VADCNG_Instance_t VadCngInstance187;
#pragma DATA_SECTION (VadCngInstance187, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance187, 128)

static VADCNG_Instance_t VadCngInstance188;
#pragma DATA_SECTION (VadCngInstance188, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance188, 128)

static VADCNG_Instance_t VadCngInstance189;
#pragma DATA_SECTION (VadCngInstance189, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance189, 128)

static VADCNG_Instance_t VadCngInstance190;
#pragma DATA_SECTION (VadCngInstance190, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance190, 128)

static VADCNG_Instance_t VadCngInstance191;
#pragma DATA_SECTION (VadCngInstance191, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance191, 128)

static VADCNG_Instance_t VadCngInstance192;
#pragma DATA_SECTION (VadCngInstance192, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance192, 128)

static VADCNG_Instance_t VadCngInstance193;
#pragma DATA_SECTION (VadCngInstance193, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance193, 128)

static VADCNG_Instance_t VadCngInstance194;
#pragma DATA_SECTION (VadCngInstance194, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance194, 128)

static VADCNG_Instance_t VadCngInstance195;
#pragma DATA_SECTION (VadCngInstance195, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance195, 128)

static VADCNG_Instance_t VadCngInstance196;
#pragma DATA_SECTION (VadCngInstance196, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance196, 128)

static VADCNG_Instance_t VadCngInstance197;
#pragma DATA_SECTION (VadCngInstance197, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance197, 128)

static VADCNG_Instance_t VadCngInstance198;
#pragma DATA_SECTION (VadCngInstance198, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance198, 128)

static VADCNG_Instance_t VadCngInstance199;
#pragma DATA_SECTION (VadCngInstance199, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance199, 128)

static VADCNG_Instance_t VadCngInstance200;
#pragma DATA_SECTION (VadCngInstance200, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance200, 128)

static VADCNG_Instance_t VadCngInstance201;
#pragma DATA_SECTION (VadCngInstance201, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance201, 128)

static VADCNG_Instance_t VadCngInstance202;
#pragma DATA_SECTION (VadCngInstance202, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance202, 128)

static VADCNG_Instance_t VadCngInstance203;
#pragma DATA_SECTION (VadCngInstance203, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance203, 128)

static VADCNG_Instance_t VadCngInstance204;
#pragma DATA_SECTION (VadCngInstance204, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance204, 128)

static VADCNG_Instance_t VadCngInstance205;
#pragma DATA_SECTION (VadCngInstance205, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance205, 128)

static VADCNG_Instance_t VadCngInstance206;
#pragma DATA_SECTION (VadCngInstance206, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance206, 128)

static VADCNG_Instance_t VadCngInstance207;
#pragma DATA_SECTION (VadCngInstance207, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance207, 128)

static VADCNG_Instance_t VadCngInstance208;
#pragma DATA_SECTION (VadCngInstance208, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance208, 128)

static VADCNG_Instance_t VadCngInstance209;
#pragma DATA_SECTION (VadCngInstance209, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance209, 128)

static VADCNG_Instance_t VadCngInstance210;
#pragma DATA_SECTION (VadCngInstance210, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance210, 128)

static VADCNG_Instance_t VadCngInstance211;
#pragma DATA_SECTION (VadCngInstance211, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance211, 128)

static VADCNG_Instance_t VadCngInstance212;
#pragma DATA_SECTION (VadCngInstance212, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance212, 128)

static VADCNG_Instance_t VadCngInstance213;
#pragma DATA_SECTION (VadCngInstance213, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance213, 128)

static VADCNG_Instance_t VadCngInstance214;
#pragma DATA_SECTION (VadCngInstance214, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance214, 128)

static VADCNG_Instance_t VadCngInstance215;
#pragma DATA_SECTION (VadCngInstance215, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance215, 128)

static VADCNG_Instance_t VadCngInstance216;
#pragma DATA_SECTION (VadCngInstance216, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance216, 128)

static VADCNG_Instance_t VadCngInstance217;
#pragma DATA_SECTION (VadCngInstance217, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance217, 128)

static VADCNG_Instance_t VadCngInstance218;
#pragma DATA_SECTION (VadCngInstance218, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance218, 128)

static VADCNG_Instance_t VadCngInstance219;
#pragma DATA_SECTION (VadCngInstance219, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance219, 128)

static VADCNG_Instance_t VadCngInstance220;
#pragma DATA_SECTION (VadCngInstance220, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance220, 128)

static VADCNG_Instance_t VadCngInstance221;
#pragma DATA_SECTION (VadCngInstance221, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance221, 128)

static VADCNG_Instance_t VadCngInstance222;
#pragma DATA_SECTION (VadCngInstance222, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance222, 128)

static VADCNG_Instance_t VadCngInstance223;
#pragma DATA_SECTION (VadCngInstance223, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance223, 128)

static VADCNG_Instance_t VadCngInstance224;
#pragma DATA_SECTION (VadCngInstance224, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance224, 128)

static VADCNG_Instance_t VadCngInstance225;
#pragma DATA_SECTION (VadCngInstance225, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance225, 128)

static VADCNG_Instance_t VadCngInstance226;
#pragma DATA_SECTION (VadCngInstance226, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance226, 128)

static VADCNG_Instance_t VadCngInstance227;
#pragma DATA_SECTION (VadCngInstance227, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance227, 128)

static VADCNG_Instance_t VadCngInstance228;
#pragma DATA_SECTION (VadCngInstance228, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance228, 128)

static VADCNG_Instance_t VadCngInstance229;
#pragma DATA_SECTION (VadCngInstance229, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance229, 128)

static VADCNG_Instance_t VadCngInstance230;
#pragma DATA_SECTION (VadCngInstance230, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance230, 128)

static VADCNG_Instance_t VadCngInstance231;
#pragma DATA_SECTION (VadCngInstance231, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance231, 128)

static VADCNG_Instance_t VadCngInstance232;
#pragma DATA_SECTION (VadCngInstance232, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance232, 128)

static VADCNG_Instance_t VadCngInstance233;
#pragma DATA_SECTION (VadCngInstance233, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance233, 128)

static VADCNG_Instance_t VadCngInstance234;
#pragma DATA_SECTION (VadCngInstance234, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance234, 128)

static VADCNG_Instance_t VadCngInstance235;
#pragma DATA_SECTION (VadCngInstance235, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance235, 128)

static VADCNG_Instance_t VadCngInstance236;
#pragma DATA_SECTION (VadCngInstance236, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance236, 128)

static VADCNG_Instance_t VadCngInstance237;
#pragma DATA_SECTION (VadCngInstance237, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance237, 128)

static VADCNG_Instance_t VadCngInstance238;
#pragma DATA_SECTION (VadCngInstance238, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance238, 128)

static VADCNG_Instance_t VadCngInstance239;
#pragma DATA_SECTION (VadCngInstance239, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance239, 128)

static VADCNG_Instance_t VadCngInstance240;
#pragma DATA_SECTION (VadCngInstance240, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance240, 128)

static VADCNG_Instance_t VadCngInstance241;
#pragma DATA_SECTION (VadCngInstance241, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance241, 128)

static VADCNG_Instance_t VadCngInstance242;
#pragma DATA_SECTION (VadCngInstance242, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance242, 128)

static VADCNG_Instance_t VadCngInstance243;
#pragma DATA_SECTION (VadCngInstance243, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance243, 128)

static VADCNG_Instance_t VadCngInstance244;
#pragma DATA_SECTION (VadCngInstance244, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance244, 128)

static VADCNG_Instance_t VadCngInstance245;
#pragma DATA_SECTION (VadCngInstance245, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance245, 128)

static VADCNG_Instance_t VadCngInstance246;
#pragma DATA_SECTION (VadCngInstance246, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance246, 128)

static VADCNG_Instance_t VadCngInstance247;
#pragma DATA_SECTION (VadCngInstance247, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance247, 128)

static VADCNG_Instance_t VadCngInstance248;
#pragma DATA_SECTION (VadCngInstance248, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance248, 128)

static VADCNG_Instance_t VadCngInstance249;
#pragma DATA_SECTION (VadCngInstance249, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance249, 128)

static VADCNG_Instance_t VadCngInstance250;
#pragma DATA_SECTION (VadCngInstance250, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance250, 128)

static VADCNG_Instance_t VadCngInstance251;
#pragma DATA_SECTION (VadCngInstance251, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance251, 128)

static VADCNG_Instance_t VadCngInstance252;
#pragma DATA_SECTION (VadCngInstance252, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance252, 128)

static VADCNG_Instance_t VadCngInstance253;
#pragma DATA_SECTION (VadCngInstance253, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance253, 128)

static VADCNG_Instance_t VadCngInstance254;
#pragma DATA_SECTION (VadCngInstance254, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance254, 128)

static VADCNG_Instance_t VadCngInstance255;
#pragma DATA_SECTION (VadCngInstance255, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance255, 128)

static VADCNG_Instance_t VadCngInstance256;
#pragma DATA_SECTION (VadCngInstance256, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance256, 128)

static VADCNG_Instance_t VadCngInstance257;
#pragma DATA_SECTION (VadCngInstance257, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance257, 128)

static VADCNG_Instance_t VadCngInstance258;
#pragma DATA_SECTION (VadCngInstance258, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance258, 128)

static VADCNG_Instance_t VadCngInstance259;
#pragma DATA_SECTION (VadCngInstance259, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance259, 128)

static VADCNG_Instance_t VadCngInstance260;
#pragma DATA_SECTION (VadCngInstance260, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance260, 128)

static VADCNG_Instance_t VadCngInstance261;
#pragma DATA_SECTION (VadCngInstance261, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance261, 128)

static VADCNG_Instance_t VadCngInstance262;
#pragma DATA_SECTION (VadCngInstance262, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance262, 128)

static VADCNG_Instance_t VadCngInstance263;
#pragma DATA_SECTION (VadCngInstance263, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance263, 128)

static VADCNG_Instance_t VadCngInstance264;
#pragma DATA_SECTION (VadCngInstance264, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance264, 128)

static VADCNG_Instance_t VadCngInstance265;
#pragma DATA_SECTION (VadCngInstance265, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance265, 128)

static VADCNG_Instance_t VadCngInstance266;
#pragma DATA_SECTION (VadCngInstance266, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance266, 128)

static VADCNG_Instance_t VadCngInstance267;
#pragma DATA_SECTION (VadCngInstance267, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance267, 128)

static VADCNG_Instance_t VadCngInstance268;
#pragma DATA_SECTION (VadCngInstance268, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance268, 128)

static VADCNG_Instance_t VadCngInstance269;
#pragma DATA_SECTION (VadCngInstance269, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance269, 128)

static VADCNG_Instance_t VadCngInstance270;
#pragma DATA_SECTION (VadCngInstance270, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance270, 128)

static VADCNG_Instance_t VadCngInstance271;
#pragma DATA_SECTION (VadCngInstance271, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance271, 128)

static VADCNG_Instance_t VadCngInstance272;
#pragma DATA_SECTION (VadCngInstance272, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance272, 128)

static VADCNG_Instance_t VadCngInstance273;
#pragma DATA_SECTION (VadCngInstance273, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance273, 128)

static VADCNG_Instance_t VadCngInstance274;
#pragma DATA_SECTION (VadCngInstance274, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance274, 128)

static VADCNG_Instance_t VadCngInstance275;
#pragma DATA_SECTION (VadCngInstance275, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance275, 128)

static VADCNG_Instance_t VadCngInstance276;
#pragma DATA_SECTION (VadCngInstance276, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance276, 128)

static VADCNG_Instance_t VadCngInstance277;
#pragma DATA_SECTION (VadCngInstance277, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance277, 128)

static VADCNG_Instance_t VadCngInstance278;
#pragma DATA_SECTION (VadCngInstance278, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance278, 128)

static VADCNG_Instance_t VadCngInstance279;
#pragma DATA_SECTION (VadCngInstance279, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance279, 128)

static VADCNG_Instance_t VadCngInstance280;
#pragma DATA_SECTION (VadCngInstance280, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance280, 128)

static VADCNG_Instance_t VadCngInstance281;
#pragma DATA_SECTION (VadCngInstance281, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance281, 128)

static VADCNG_Instance_t VadCngInstance282;
#pragma DATA_SECTION (VadCngInstance282, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance282, 128)

static VADCNG_Instance_t VadCngInstance283;
#pragma DATA_SECTION (VadCngInstance283, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance283, 128)

static VADCNG_Instance_t VadCngInstance284;
#pragma DATA_SECTION (VadCngInstance284, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance284, 128)

static VADCNG_Instance_t VadCngInstance285;
#pragma DATA_SECTION (VadCngInstance285, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance285, 128)

static VADCNG_Instance_t VadCngInstance286;
#pragma DATA_SECTION (VadCngInstance286, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance286, 128)

static VADCNG_Instance_t VadCngInstance287;
#pragma DATA_SECTION (VadCngInstance287, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance287, 128)

static VADCNG_Instance_t VadCngInstance288;
#pragma DATA_SECTION (VadCngInstance288, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance288, 128)

static VADCNG_Instance_t VadCngInstance289;
#pragma DATA_SECTION (VadCngInstance289, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance289, 128)

static VADCNG_Instance_t VadCngInstance290;
#pragma DATA_SECTION (VadCngInstance290, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance290, 128)

static VADCNG_Instance_t VadCngInstance291;
#pragma DATA_SECTION (VadCngInstance291, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance291, 128)

static VADCNG_Instance_t VadCngInstance292;
#pragma DATA_SECTION (VadCngInstance292, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance292, 128)

static VADCNG_Instance_t VadCngInstance293;
#pragma DATA_SECTION (VadCngInstance293, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance293, 128)

static VADCNG_Instance_t VadCngInstance294;
#pragma DATA_SECTION (VadCngInstance294, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance294, 128)

static VADCNG_Instance_t VadCngInstance295;
#pragma DATA_SECTION (VadCngInstance295, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance295, 128)

static VADCNG_Instance_t VadCngInstance296;
#pragma DATA_SECTION (VadCngInstance296, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance296, 128)

static VADCNG_Instance_t VadCngInstance297;
#pragma DATA_SECTION (VadCngInstance297, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance297, 128)

static VADCNG_Instance_t VadCngInstance298;
#pragma DATA_SECTION (VadCngInstance298, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance298, 128)

static VADCNG_Instance_t VadCngInstance299;
#pragma DATA_SECTION (VadCngInstance299, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance299, 128)

static VADCNG_Instance_t VadCngInstance300;
#pragma DATA_SECTION (VadCngInstance300, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance300, 128)

static VADCNG_Instance_t VadCngInstance301;
#pragma DATA_SECTION (VadCngInstance301, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance301, 128)

static VADCNG_Instance_t VadCngInstance302;
#pragma DATA_SECTION (VadCngInstance302, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance302, 128)

static VADCNG_Instance_t VadCngInstance303;
#pragma DATA_SECTION (VadCngInstance303, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance303, 128)

static VADCNG_Instance_t VadCngInstance304;
#pragma DATA_SECTION (VadCngInstance304, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance304, 128)

static VADCNG_Instance_t VadCngInstance305;
#pragma DATA_SECTION (VadCngInstance305, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance305, 128)

static VADCNG_Instance_t VadCngInstance306;
#pragma DATA_SECTION (VadCngInstance306, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance306, 128)

static VADCNG_Instance_t VadCngInstance307;
#pragma DATA_SECTION (VadCngInstance307, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance307, 128)

static VADCNG_Instance_t VadCngInstance308;
#pragma DATA_SECTION (VadCngInstance308, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance308, 128)

static VADCNG_Instance_t VadCngInstance309;
#pragma DATA_SECTION (VadCngInstance309, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance309, 128)

static VADCNG_Instance_t VadCngInstance310;
#pragma DATA_SECTION (VadCngInstance310, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance310, 128)

static VADCNG_Instance_t VadCngInstance311;
#pragma DATA_SECTION (VadCngInstance311, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance311, 128)

static VADCNG_Instance_t VadCngInstance312;
#pragma DATA_SECTION (VadCngInstance312, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance312, 128)

static VADCNG_Instance_t VadCngInstance313;
#pragma DATA_SECTION (VadCngInstance313, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance313, 128)

static VADCNG_Instance_t VadCngInstance314;
#pragma DATA_SECTION (VadCngInstance314, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance314, 128)

static VADCNG_Instance_t VadCngInstance315;
#pragma DATA_SECTION (VadCngInstance315, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance315, 128)

static VADCNG_Instance_t VadCngInstance316;
#pragma DATA_SECTION (VadCngInstance316, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance316, 128)

static VADCNG_Instance_t VadCngInstance317;
#pragma DATA_SECTION (VadCngInstance317, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance317, 128)

static VADCNG_Instance_t VadCngInstance318;
#pragma DATA_SECTION (VadCngInstance318, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance318, 128)

static VADCNG_Instance_t VadCngInstance319;
#pragma DATA_SECTION (VadCngInstance319, "CHAN_INST_DATA:VAD")
#pragma DATA_ALIGN   (VadCngInstance319, 128)

VADCNG_Instance_t* const VadCngInstance[] = {
	&VadCngInstance0,
	&VadCngInstance1,
	&VadCngInstance2,
	&VadCngInstance3,
	&VadCngInstance4,
	&VadCngInstance5,
	&VadCngInstance6,
	&VadCngInstance7,
	&VadCngInstance8,
	&VadCngInstance9,
	&VadCngInstance10,
	&VadCngInstance11,
	&VadCngInstance12,
	&VadCngInstance13,
	&VadCngInstance14,
	&VadCngInstance15,
	&VadCngInstance16,
	&VadCngInstance17,
	&VadCngInstance18,
	&VadCngInstance19,
	&VadCngInstance20,
	&VadCngInstance21,
	&VadCngInstance22,
	&VadCngInstance23,
	&VadCngInstance24,
	&VadCngInstance25,
	&VadCngInstance26,
	&VadCngInstance27,
	&VadCngInstance28,
	&VadCngInstance29,
	&VadCngInstance30,
	&VadCngInstance31,
	&VadCngInstance32,
	&VadCngInstance33,
	&VadCngInstance34,
	&VadCngInstance35,
	&VadCngInstance36,
	&VadCngInstance37,
	&VadCngInstance38,
	&VadCngInstance39,
	&VadCngInstance40,
	&VadCngInstance41,
	&VadCngInstance42,
	&VadCngInstance43,
	&VadCngInstance44,
	&VadCngInstance45,
	&VadCngInstance46,
	&VadCngInstance47,
	&VadCngInstance48,
	&VadCngInstance49,
	&VadCngInstance50,
	&VadCngInstance51,
	&VadCngInstance52,
	&VadCngInstance53,
	&VadCngInstance54,
	&VadCngInstance55,
	&VadCngInstance56,
	&VadCngInstance57,
	&VadCngInstance58,
	&VadCngInstance59,
	&VadCngInstance60,
	&VadCngInstance61,
	&VadCngInstance62,
	&VadCngInstance63,
	&VadCngInstance64,
	&VadCngInstance65,
	&VadCngInstance66,
	&VadCngInstance67,
	&VadCngInstance68,
	&VadCngInstance69,
	&VadCngInstance70,
	&VadCngInstance71,
	&VadCngInstance72,
	&VadCngInstance73,
	&VadCngInstance74,
	&VadCngInstance75,
	&VadCngInstance76,
	&VadCngInstance77,
	&VadCngInstance78,
	&VadCngInstance79,
	&VadCngInstance80,
	&VadCngInstance81,
	&VadCngInstance82,
	&VadCngInstance83,
	&VadCngInstance84,
	&VadCngInstance85,
	&VadCngInstance86,
	&VadCngInstance87,
	&VadCngInstance88,
	&VadCngInstance89,
	&VadCngInstance90,
	&VadCngInstance91,
	&VadCngInstance92,
	&VadCngInstance93,
	&VadCngInstance94,
	&VadCngInstance95,
	&VadCngInstance96,
	&VadCngInstance97,
	&VadCngInstance98,
	&VadCngInstance99,
	&VadCngInstance100,
	&VadCngInstance101,
	&VadCngInstance102,
	&VadCngInstance103,
	&VadCngInstance104,
	&VadCngInstance105,
	&VadCngInstance106,
	&VadCngInstance107,
	&VadCngInstance108,
	&VadCngInstance109,
	&VadCngInstance110,
	&VadCngInstance111,
	&VadCngInstance112,
	&VadCngInstance113,
	&VadCngInstance114,
	&VadCngInstance115,
	&VadCngInstance116,
	&VadCngInstance117,
	&VadCngInstance118,
	&VadCngInstance119,
	&VadCngInstance120,
	&VadCngInstance121,
	&VadCngInstance122,
	&VadCngInstance123,
	&VadCngInstance124,
	&VadCngInstance125,
	&VadCngInstance126,
	&VadCngInstance127,
	&VadCngInstance128,
	&VadCngInstance129,
	&VadCngInstance130,
	&VadCngInstance131,
	&VadCngInstance132,
	&VadCngInstance133,
	&VadCngInstance134,
	&VadCngInstance135,
	&VadCngInstance136,
	&VadCngInstance137,
	&VadCngInstance138,
	&VadCngInstance139,
	&VadCngInstance140,
	&VadCngInstance141,
	&VadCngInstance142,
	&VadCngInstance143,
	&VadCngInstance144,
	&VadCngInstance145,
	&VadCngInstance146,
	&VadCngInstance147,
	&VadCngInstance148,
	&VadCngInstance149,
	&VadCngInstance150,
	&VadCngInstance151,
	&VadCngInstance152,
	&VadCngInstance153,
	&VadCngInstance154,
	&VadCngInstance155,
	&VadCngInstance156,
	&VadCngInstance157,
	&VadCngInstance158,
	&VadCngInstance159,
	&VadCngInstance160,
	&VadCngInstance161,
	&VadCngInstance162,
	&VadCngInstance163,
	&VadCngInstance164,
	&VadCngInstance165,
	&VadCngInstance166,
	&VadCngInstance167,
	&VadCngInstance168,
	&VadCngInstance169,
	&VadCngInstance170,
	&VadCngInstance171,
	&VadCngInstance172,
	&VadCngInstance173,
	&VadCngInstance174,
	&VadCngInstance175,
	&VadCngInstance176,
	&VadCngInstance177,
	&VadCngInstance178,
	&VadCngInstance179,
	&VadCngInstance180,
	&VadCngInstance181,
	&VadCngInstance182,
	&VadCngInstance183,
	&VadCngInstance184,
	&VadCngInstance185,
	&VadCngInstance186,
	&VadCngInstance187,
	&VadCngInstance188,
	&VadCngInstance189,
	&VadCngInstance190,
	&VadCngInstance191,
	&VadCngInstance192,
	&VadCngInstance193,
	&VadCngInstance194,
	&VadCngInstance195,
	&VadCngInstance196,
	&VadCngInstance197,
	&VadCngInstance198,
	&VadCngInstance199,
	&VadCngInstance200,
	&VadCngInstance201,
	&VadCngInstance202,
	&VadCngInstance203,
	&VadCngInstance204,
	&VadCngInstance205,
	&VadCngInstance206,
	&VadCngInstance207,
	&VadCngInstance208,
	&VadCngInstance209,
	&VadCngInstance210,
	&VadCngInstance211,
	&VadCngInstance212,
	&VadCngInstance213,
	&VadCngInstance214,
	&VadCngInstance215,
	&VadCngInstance216,
	&VadCngInstance217,
	&VadCngInstance218,
	&VadCngInstance219,
	&VadCngInstance220,
	&VadCngInstance221,
	&VadCngInstance222,
	&VadCngInstance223,
	&VadCngInstance224,
	&VadCngInstance225,
	&VadCngInstance226,
	&VadCngInstance227,
	&VadCngInstance228,
	&VadCngInstance229,
	&VadCngInstance230,
	&VadCngInstance231,
	&VadCngInstance232,
	&VadCngInstance233,
	&VadCngInstance234,
	&VadCngInstance235,
	&VadCngInstance236,
	&VadCngInstance237,
	&VadCngInstance238,
	&VadCngInstance239,
	&VadCngInstance240,
	&VadCngInstance241,
	&VadCngInstance242,
	&VadCngInstance243,
	&VadCngInstance244,
	&VadCngInstance245,
	&VadCngInstance246,
	&VadCngInstance247,
	&VadCngInstance248,
	&VadCngInstance249,
	&VadCngInstance250,
	&VadCngInstance251,
	&VadCngInstance252,
	&VadCngInstance253,
	&VadCngInstance254,
	&VadCngInstance255,
	&VadCngInstance256,
	&VadCngInstance257,
	&VadCngInstance258,
	&VadCngInstance259,
	&VadCngInstance260,
	&VadCngInstance261,
	&VadCngInstance262,
	&VadCngInstance263,
	&VadCngInstance264,
	&VadCngInstance265,
	&VadCngInstance266,
	&VadCngInstance267,
	&VadCngInstance268,
	&VadCngInstance269,
	&VadCngInstance270,
	&VadCngInstance271,
	&VadCngInstance272,
	&VadCngInstance273,
	&VadCngInstance274,
	&VadCngInstance275,
	&VadCngInstance276,
	&VadCngInstance277,
	&VadCngInstance278,
	&VadCngInstance279,
	&VadCngInstance280,
	&VadCngInstance281,
	&VadCngInstance282,
	&VadCngInstance283,
	&VadCngInstance284,
	&VadCngInstance285,
	&VadCngInstance286,
	&VadCngInstance287,
	&VadCngInstance288,
	&VadCngInstance289,
	&VadCngInstance290,
	&VadCngInstance291,
	&VadCngInstance292,
	&VadCngInstance293,
	&VadCngInstance294,
	&VadCngInstance295,
	&VadCngInstance296,
	&VadCngInstance297,
	&VadCngInstance298,
	&VadCngInstance299,
	&VadCngInstance300,
	&VadCngInstance301,
	&VadCngInstance302,
	&VadCngInstance303,
	&VadCngInstance304,
	&VadCngInstance305,
	&VadCngInstance306,
	&VadCngInstance307,
	&VadCngInstance308,
	&VadCngInstance309,
	&VadCngInstance310,
	&VadCngInstance311,
	&VadCngInstance312,
	&VadCngInstance313,
	&VadCngInstance314,
	&VadCngInstance315,
	&VadCngInstance316,
	&VadCngInstance317,
	&VadCngInstance318,
	&VadCngInstance319
};
//}

const ADT_UInt16 numAGCChansAvail = 0;
ADT_UInt16 agcChanInUse;
ADT_UInt16 AgcInstance;
#pragma DATA_SECTION (agcChanInUse,     "STUB_DATA_SECT")
#pragma DATA_SECTION (AgcInstance,      "STUB_DATA_SECT")




/*=============================== */
// Sampling rate converter data structures.
const ADT_UInt16 numSrcUpBy2ChansAvail = 0;
ADT_UInt16 srcUpBy2ChanInUse;
ADT_UInt16 SrcUpBy2Instance;
#pragma DATA_SECTION (srcUpBy2ChanInUse,     "STUB_DATA_SECT")
#pragma DATA_SECTION (SrcUpBy2Instance,      "STUB_DATA_SECT")

const ADT_UInt16 numSrcDownBy2ChansAvail = 0;
ADT_UInt16 srcDownBy2ChanInUse;
ADT_UInt16 SrcDownBy2Instance;
#pragma DATA_SECTION (srcDownBy2ChanInUse,     "STUB_DATA_SECT")
#pragma DATA_SECTION (SrcDownBy2Instance,      "STUB_DATA_SECT")




/*=============================== */
// Noise Suppression Structures. 

// Noise cancellation not supported at this time
ADT_UInt16*  const NoiseSuppressInstance = 0;  // Stub for linker
ADT_Int16  NoiseRemnantPool;        // Stub for linker
ADT_UInt16 NCAN_Params;             // Stub for linker
#pragma DATA_SECTION (NoiseRemnantPool, "STUB_DATA_SECT")
#pragma DATA_SECTION (NCAN_Params,      "STUB_DATA_SECT")




/*=============================== */
// T.38 Fax relay Structures. 

ADT_UInt16 numFaxChansUsed = 0;
ADT_UInt16 faxChanInUse[MAX_NUM_FAX_CHANS];
#pragma DATA_SECTION (faxChanInUse,    "POOL_ALLOC")

ADT_UInt16 faxChan0[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan1[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan2[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan3[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan4[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan5[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan6[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan7[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan8[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan9[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan10[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan11[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan12[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan13[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan14[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan15[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan16[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan17[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan18[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan19[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan20[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan21[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan22[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan23[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan24[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan25[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan26[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan27[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan28[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan29[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan30[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan31[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan32[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan33[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan34[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan35[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan36[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan37[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan38[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan39[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan40[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan41[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan42[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan43[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan44[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan45[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan46[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan47[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan48[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan49[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan50[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan51[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan52[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan53[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan54[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan55[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan56[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan57[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan58[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan59[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan60[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan61[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan62[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan63[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan64[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan65[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan66[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan67[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan68[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan69[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan70[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan71[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan72[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan73[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan74[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan75[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan76[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan77[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan78[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan79[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan80[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan81[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan82[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan83[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan84[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan85[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan86[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan87[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan88[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan89[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan90[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan91[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan92[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan93[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan94[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan95[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan96[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan97[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan98[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan99[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan100[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan101[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan102[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan103[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan104[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan105[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan106[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan107[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan108[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan109[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan110[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan111[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan112[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan113[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan114[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan115[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan116[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan117[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan118[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan119[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan120[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan121[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan122[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan123[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan124[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan125[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan126[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan127[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan128[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan129[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan130[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan131[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan132[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan133[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan134[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan135[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan136[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan137[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan138[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan139[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan140[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan141[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan142[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan143[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan144[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan145[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan146[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan147[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan148[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan149[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan150[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan151[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan152[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan153[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan154[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan155[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan156[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan157[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan158[RLY_CHANNEL_SIZE];
ADT_UInt16 faxChan159[RLY_CHANNEL_SIZE];
ADT_UInt16 * const pFaxChan[] = {
	faxChan0,
	faxChan1,
	faxChan2,
	faxChan3,
	faxChan4,
	faxChan5,
	faxChan6,
	faxChan7,
	faxChan8,
	faxChan9,
	faxChan10,
	faxChan11,
	faxChan12,
	faxChan13,
	faxChan14,
	faxChan15,
	faxChan16,
	faxChan17,
	faxChan18,
	faxChan19,
	faxChan20,
	faxChan21,
	faxChan22,
	faxChan23,
	faxChan24,
	faxChan25,
	faxChan26,
	faxChan27,
	faxChan28,
	faxChan29,
	faxChan30,
	faxChan31,
	faxChan32,
	faxChan33,
	faxChan34,
	faxChan35,
	faxChan36,
	faxChan37,
	faxChan38,
	faxChan39,
	faxChan40,
	faxChan41,
	faxChan42,
	faxChan43,
	faxChan44,
	faxChan45,
	faxChan46,
	faxChan47,
	faxChan48,
	faxChan49,
	faxChan50,
	faxChan51,
	faxChan52,
	faxChan53,
	faxChan54,
	faxChan55,
	faxChan56,
	faxChan57,
	faxChan58,
	faxChan59,
	faxChan60,
	faxChan61,
	faxChan62,
	faxChan63,
	faxChan64,
	faxChan65,
	faxChan66,
	faxChan67,
	faxChan68,
	faxChan69,
	faxChan70,
	faxChan71,
	faxChan72,
	faxChan73,
	faxChan74,
	faxChan75,
	faxChan76,
	faxChan77,
	faxChan78,
	faxChan79,
	faxChan80,
	faxChan81,
	faxChan82,
	faxChan83,
	faxChan84,
	faxChan85,
	faxChan86,
	faxChan87,
	faxChan88,
	faxChan89,
	faxChan90,
	faxChan91,
	faxChan92,
	faxChan93,
	faxChan94,
	faxChan95,
	faxChan96,
	faxChan97,
	faxChan98,
	faxChan99,
	faxChan100,
	faxChan101,
	faxChan102,
	faxChan103,
	faxChan104,
	faxChan105,
	faxChan106,
	faxChan107,
	faxChan108,
	faxChan109,
	faxChan110,
	faxChan111,
	faxChan112,
	faxChan113,
	faxChan114,
	faxChan115,
	faxChan116,
	faxChan117,
	faxChan118,
	faxChan119,
	faxChan120,
	faxChan121,
	faxChan122,
	faxChan123,
	faxChan124,
	faxChan125,
	faxChan126,
	faxChan127,
	faxChan128,
	faxChan129,
	faxChan130,
	faxChan131,
	faxChan132,
	faxChan133,
	faxChan134,
	faxChan135,
	faxChan136,
	faxChan137,
	faxChan138,
	faxChan139,
	faxChan140,
	faxChan141,
	faxChan142,
	faxChan143,
	faxChan144,
	faxChan145,
	faxChan146,
	faxChan147,
	faxChan148,
	faxChan149,
	faxChan150,
	faxChan151,
	faxChan152,
	faxChan153,
	faxChan154,
	faxChan155,
	faxChan156,
	faxChan157,
	faxChan158,
	faxChan159
};
#pragma DATA_SECTION (faxChan0, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan0, 8)
#pragma DATA_SECTION (faxChan1, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan1, 8)
#pragma DATA_SECTION (faxChan2, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan2, 8)
#pragma DATA_SECTION (faxChan3, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan3, 8)
#pragma DATA_SECTION (faxChan4, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan4, 8)
#pragma DATA_SECTION (faxChan5, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan5, 8)
#pragma DATA_SECTION (faxChan6, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan6, 8)
#pragma DATA_SECTION (faxChan7, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan7, 8)
#pragma DATA_SECTION (faxChan8, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan8, 8)
#pragma DATA_SECTION (faxChan9, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan9, 8)
#pragma DATA_SECTION (faxChan10, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan10, 8)
#pragma DATA_SECTION (faxChan11, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan11, 8)
#pragma DATA_SECTION (faxChan12, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan12, 8)
#pragma DATA_SECTION (faxChan13, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan13, 8)
#pragma DATA_SECTION (faxChan14, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan14, 8)
#pragma DATA_SECTION (faxChan15, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan15, 8)
#pragma DATA_SECTION (faxChan16, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan16, 8)
#pragma DATA_SECTION (faxChan17, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan17, 8)
#pragma DATA_SECTION (faxChan18, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan18, 8)
#pragma DATA_SECTION (faxChan19, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan19, 8)
#pragma DATA_SECTION (faxChan20, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan20, 8)
#pragma DATA_SECTION (faxChan21, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan21, 8)
#pragma DATA_SECTION (faxChan22, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan22, 8)
#pragma DATA_SECTION (faxChan23, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan23, 8)
#pragma DATA_SECTION (faxChan24, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan24, 8)
#pragma DATA_SECTION (faxChan25, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan25, 8)
#pragma DATA_SECTION (faxChan26, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan26, 8)
#pragma DATA_SECTION (faxChan27, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan27, 8)
#pragma DATA_SECTION (faxChan28, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan28, 8)
#pragma DATA_SECTION (faxChan29, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan29, 8)
#pragma DATA_SECTION (faxChan30, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan30, 8)
#pragma DATA_SECTION (faxChan31, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan31, 8)
#pragma DATA_SECTION (faxChan32, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan32, 8)
#pragma DATA_SECTION (faxChan33, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan33, 8)
#pragma DATA_SECTION (faxChan34, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan34, 8)
#pragma DATA_SECTION (faxChan35, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan35, 8)
#pragma DATA_SECTION (faxChan36, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan36, 8)
#pragma DATA_SECTION (faxChan37, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan37, 8)
#pragma DATA_SECTION (faxChan38, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan38, 8)
#pragma DATA_SECTION (faxChan39, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan39, 8)
#pragma DATA_SECTION (faxChan40, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan40, 8)
#pragma DATA_SECTION (faxChan41, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan41, 8)
#pragma DATA_SECTION (faxChan42, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan42, 8)
#pragma DATA_SECTION (faxChan43, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan43, 8)
#pragma DATA_SECTION (faxChan44, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan44, 8)
#pragma DATA_SECTION (faxChan45, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan45, 8)
#pragma DATA_SECTION (faxChan46, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan46, 8)
#pragma DATA_SECTION (faxChan47, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan47, 8)
#pragma DATA_SECTION (faxChan48, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan48, 8)
#pragma DATA_SECTION (faxChan49, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan49, 8)
#pragma DATA_SECTION (faxChan50, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan50, 8)
#pragma DATA_SECTION (faxChan51, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan51, 8)
#pragma DATA_SECTION (faxChan52, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan52, 8)
#pragma DATA_SECTION (faxChan53, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan53, 8)
#pragma DATA_SECTION (faxChan54, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan54, 8)
#pragma DATA_SECTION (faxChan55, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan55, 8)
#pragma DATA_SECTION (faxChan56, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan56, 8)
#pragma DATA_SECTION (faxChan57, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan57, 8)
#pragma DATA_SECTION (faxChan58, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan58, 8)
#pragma DATA_SECTION (faxChan59, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan59, 8)
#pragma DATA_SECTION (faxChan60, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan60, 8)
#pragma DATA_SECTION (faxChan61, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan61, 8)
#pragma DATA_SECTION (faxChan62, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan62, 8)
#pragma DATA_SECTION (faxChan63, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan63, 8)
#pragma DATA_SECTION (faxChan64, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan64, 8)
#pragma DATA_SECTION (faxChan65, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan65, 8)
#pragma DATA_SECTION (faxChan66, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan66, 8)
#pragma DATA_SECTION (faxChan67, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan67, 8)
#pragma DATA_SECTION (faxChan68, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan68, 8)
#pragma DATA_SECTION (faxChan69, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan69, 8)
#pragma DATA_SECTION (faxChan70, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan70, 8)
#pragma DATA_SECTION (faxChan71, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan71, 8)
#pragma DATA_SECTION (faxChan72, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan72, 8)
#pragma DATA_SECTION (faxChan73, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan73, 8)
#pragma DATA_SECTION (faxChan74, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan74, 8)
#pragma DATA_SECTION (faxChan75, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan75, 8)
#pragma DATA_SECTION (faxChan76, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan76, 8)
#pragma DATA_SECTION (faxChan77, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan77, 8)
#pragma DATA_SECTION (faxChan78, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan78, 8)
#pragma DATA_SECTION (faxChan79, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan79, 8)
#pragma DATA_SECTION (faxChan80, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan80, 8)
#pragma DATA_SECTION (faxChan81, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan81, 8)
#pragma DATA_SECTION (faxChan82, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan82, 8)
#pragma DATA_SECTION (faxChan83, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan83, 8)
#pragma DATA_SECTION (faxChan84, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan84, 8)
#pragma DATA_SECTION (faxChan85, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan85, 8)
#pragma DATA_SECTION (faxChan86, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan86, 8)
#pragma DATA_SECTION (faxChan87, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan87, 8)
#pragma DATA_SECTION (faxChan88, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan88, 8)
#pragma DATA_SECTION (faxChan89, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan89, 8)
#pragma DATA_SECTION (faxChan90, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan90, 8)
#pragma DATA_SECTION (faxChan91, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan91, 8)
#pragma DATA_SECTION (faxChan92, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan92, 8)
#pragma DATA_SECTION (faxChan93, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan93, 8)
#pragma DATA_SECTION (faxChan94, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan94, 8)
#pragma DATA_SECTION (faxChan95, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan95, 8)
#pragma DATA_SECTION (faxChan96, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan96, 8)
#pragma DATA_SECTION (faxChan97, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan97, 8)
#pragma DATA_SECTION (faxChan98, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan98, 8)
#pragma DATA_SECTION (faxChan99, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan99, 8)
#pragma DATA_SECTION (faxChan100, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan100, 8)
#pragma DATA_SECTION (faxChan101, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan101, 8)
#pragma DATA_SECTION (faxChan102, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan102, 8)
#pragma DATA_SECTION (faxChan103, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan103, 8)
#pragma DATA_SECTION (faxChan104, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan104, 8)
#pragma DATA_SECTION (faxChan105, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan105, 8)
#pragma DATA_SECTION (faxChan106, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan106, 8)
#pragma DATA_SECTION (faxChan107, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan107, 8)
#pragma DATA_SECTION (faxChan108, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan108, 8)
#pragma DATA_SECTION (faxChan109, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan109, 8)
#pragma DATA_SECTION (faxChan110, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan110, 8)
#pragma DATA_SECTION (faxChan111, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan111, 8)
#pragma DATA_SECTION (faxChan112, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan112, 8)
#pragma DATA_SECTION (faxChan113, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan113, 8)
#pragma DATA_SECTION (faxChan114, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan114, 8)
#pragma DATA_SECTION (faxChan115, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan115, 8)
#pragma DATA_SECTION (faxChan116, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan116, 8)
#pragma DATA_SECTION (faxChan117, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan117, 8)
#pragma DATA_SECTION (faxChan118, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan118, 8)
#pragma DATA_SECTION (faxChan119, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan119, 8)
#pragma DATA_SECTION (faxChan120, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan120, 8)
#pragma DATA_SECTION (faxChan121, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan121, 8)
#pragma DATA_SECTION (faxChan122, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan122, 8)
#pragma DATA_SECTION (faxChan123, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan123, 8)
#pragma DATA_SECTION (faxChan124, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan124, 8)
#pragma DATA_SECTION (faxChan125, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan125, 8)
#pragma DATA_SECTION (faxChan126, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan126, 8)
#pragma DATA_SECTION (faxChan127, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan127, 8)
#pragma DATA_SECTION (faxChan128, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan128, 8)
#pragma DATA_SECTION (faxChan129, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan129, 8)
#pragma DATA_SECTION (faxChan130, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan130, 8)
#pragma DATA_SECTION (faxChan131, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan131, 8)
#pragma DATA_SECTION (faxChan132, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan132, 8)
#pragma DATA_SECTION (faxChan133, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan133, 8)
#pragma DATA_SECTION (faxChan134, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan134, 8)
#pragma DATA_SECTION (faxChan135, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan135, 8)
#pragma DATA_SECTION (faxChan136, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan136, 8)
#pragma DATA_SECTION (faxChan137, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan137, 8)
#pragma DATA_SECTION (faxChan138, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan138, 8)
#pragma DATA_SECTION (faxChan139, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan139, 8)
#pragma DATA_SECTION (faxChan140, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan140, 8)
#pragma DATA_SECTION (faxChan141, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan141, 8)
#pragma DATA_SECTION (faxChan142, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan142, 8)
#pragma DATA_SECTION (faxChan143, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan143, 8)
#pragma DATA_SECTION (faxChan144, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan144, 8)
#pragma DATA_SECTION (faxChan145, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan145, 8)
#pragma DATA_SECTION (faxChan146, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan146, 8)
#pragma DATA_SECTION (faxChan147, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan147, 8)
#pragma DATA_SECTION (faxChan148, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan148, 8)
#pragma DATA_SECTION (faxChan149, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan149, 8)
#pragma DATA_SECTION (faxChan150, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan150, 8)
#pragma DATA_SECTION (faxChan151, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan151, 8)
#pragma DATA_SECTION (faxChan152, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan152, 8)
#pragma DATA_SECTION (faxChan153, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan153, 8)
#pragma DATA_SECTION (faxChan154, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan154, 8)
#pragma DATA_SECTION (faxChan155, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan155, 8)
#pragma DATA_SECTION (faxChan156, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan156, 8)
#pragma DATA_SECTION (faxChan157, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan157, 8)
#pragma DATA_SECTION (faxChan158, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan158, 8)
#pragma DATA_SECTION (faxChan159, "CHAN_INST_DATA:FAX")
#pragma DATA_ALIGN   (faxChan159, 8)

t38Packet_t t38Packet0;
#pragma DATA_SECTION (t38Packet0, "T38PACKET")
t38Packet_t t38Packet1;
#pragma DATA_SECTION (t38Packet1, "T38PACKET")
t38Packet_t t38Packet2;
#pragma DATA_SECTION (t38Packet2, "T38PACKET")
t38Packet_t t38Packet3;
#pragma DATA_SECTION (t38Packet3, "T38PACKET")
t38Packet_t t38Packet4;
#pragma DATA_SECTION (t38Packet4, "T38PACKET")
t38Packet_t t38Packet5;
#pragma DATA_SECTION (t38Packet5, "T38PACKET")
t38Packet_t t38Packet6;
#pragma DATA_SECTION (t38Packet6, "T38PACKET")
t38Packet_t t38Packet7;
#pragma DATA_SECTION (t38Packet7, "T38PACKET")
t38Packet_t t38Packet8;
#pragma DATA_SECTION (t38Packet8, "T38PACKET")
t38Packet_t t38Packet9;
#pragma DATA_SECTION (t38Packet9, "T38PACKET")
t38Packet_t t38Packet10;
#pragma DATA_SECTION (t38Packet10, "T38PACKET")
t38Packet_t t38Packet11;
#pragma DATA_SECTION (t38Packet11, "T38PACKET")
t38Packet_t t38Packet12;
#pragma DATA_SECTION (t38Packet12, "T38PACKET")
t38Packet_t t38Packet13;
#pragma DATA_SECTION (t38Packet13, "T38PACKET")
t38Packet_t t38Packet14;
#pragma DATA_SECTION (t38Packet14, "T38PACKET")
t38Packet_t t38Packet15;
#pragma DATA_SECTION (t38Packet15, "T38PACKET")
t38Packet_t t38Packet16;
#pragma DATA_SECTION (t38Packet16, "T38PACKET")
t38Packet_t t38Packet17;
#pragma DATA_SECTION (t38Packet17, "T38PACKET")
t38Packet_t t38Packet18;
#pragma DATA_SECTION (t38Packet18, "T38PACKET")
t38Packet_t t38Packet19;
#pragma DATA_SECTION (t38Packet19, "T38PACKET")
t38Packet_t t38Packet20;
#pragma DATA_SECTION (t38Packet20, "T38PACKET")
t38Packet_t t38Packet21;
#pragma DATA_SECTION (t38Packet21, "T38PACKET")
t38Packet_t t38Packet22;
#pragma DATA_SECTION (t38Packet22, "T38PACKET")
t38Packet_t t38Packet23;
#pragma DATA_SECTION (t38Packet23, "T38PACKET")
t38Packet_t t38Packet24;
#pragma DATA_SECTION (t38Packet24, "T38PACKET")
t38Packet_t t38Packet25;
#pragma DATA_SECTION (t38Packet25, "T38PACKET")
t38Packet_t t38Packet26;
#pragma DATA_SECTION (t38Packet26, "T38PACKET")
t38Packet_t t38Packet27;
#pragma DATA_SECTION (t38Packet27, "T38PACKET")
t38Packet_t t38Packet28;
#pragma DATA_SECTION (t38Packet28, "T38PACKET")
t38Packet_t t38Packet29;
#pragma DATA_SECTION (t38Packet29, "T38PACKET")
t38Packet_t t38Packet30;
#pragma DATA_SECTION (t38Packet30, "T38PACKET")
t38Packet_t t38Packet31;
#pragma DATA_SECTION (t38Packet31, "T38PACKET")
t38Packet_t t38Packet32;
#pragma DATA_SECTION (t38Packet32, "T38PACKET")
t38Packet_t t38Packet33;
#pragma DATA_SECTION (t38Packet33, "T38PACKET")
t38Packet_t t38Packet34;
#pragma DATA_SECTION (t38Packet34, "T38PACKET")
t38Packet_t t38Packet35;
#pragma DATA_SECTION (t38Packet35, "T38PACKET")
t38Packet_t t38Packet36;
#pragma DATA_SECTION (t38Packet36, "T38PACKET")
t38Packet_t t38Packet37;
#pragma DATA_SECTION (t38Packet37, "T38PACKET")
t38Packet_t t38Packet38;
#pragma DATA_SECTION (t38Packet38, "T38PACKET")
t38Packet_t t38Packet39;
#pragma DATA_SECTION (t38Packet39, "T38PACKET")
t38Packet_t t38Packet40;
#pragma DATA_SECTION (t38Packet40, "T38PACKET")
t38Packet_t t38Packet41;
#pragma DATA_SECTION (t38Packet41, "T38PACKET")
t38Packet_t t38Packet42;
#pragma DATA_SECTION (t38Packet42, "T38PACKET")
t38Packet_t t38Packet43;
#pragma DATA_SECTION (t38Packet43, "T38PACKET")
t38Packet_t t38Packet44;
#pragma DATA_SECTION (t38Packet44, "T38PACKET")
t38Packet_t t38Packet45;
#pragma DATA_SECTION (t38Packet45, "T38PACKET")
t38Packet_t t38Packet46;
#pragma DATA_SECTION (t38Packet46, "T38PACKET")
t38Packet_t t38Packet47;
#pragma DATA_SECTION (t38Packet47, "T38PACKET")
t38Packet_t t38Packet48;
#pragma DATA_SECTION (t38Packet48, "T38PACKET")
t38Packet_t t38Packet49;
#pragma DATA_SECTION (t38Packet49, "T38PACKET")
t38Packet_t t38Packet50;
#pragma DATA_SECTION (t38Packet50, "T38PACKET")
t38Packet_t t38Packet51;
#pragma DATA_SECTION (t38Packet51, "T38PACKET")
t38Packet_t t38Packet52;
#pragma DATA_SECTION (t38Packet52, "T38PACKET")
t38Packet_t t38Packet53;
#pragma DATA_SECTION (t38Packet53, "T38PACKET")
t38Packet_t t38Packet54;
#pragma DATA_SECTION (t38Packet54, "T38PACKET")
t38Packet_t t38Packet55;
#pragma DATA_SECTION (t38Packet55, "T38PACKET")
t38Packet_t t38Packet56;
#pragma DATA_SECTION (t38Packet56, "T38PACKET")
t38Packet_t t38Packet57;
#pragma DATA_SECTION (t38Packet57, "T38PACKET")
t38Packet_t t38Packet58;
#pragma DATA_SECTION (t38Packet58, "T38PACKET")
t38Packet_t t38Packet59;
#pragma DATA_SECTION (t38Packet59, "T38PACKET")
t38Packet_t t38Packet60;
#pragma DATA_SECTION (t38Packet60, "T38PACKET")
t38Packet_t t38Packet61;
#pragma DATA_SECTION (t38Packet61, "T38PACKET")
t38Packet_t t38Packet62;
#pragma DATA_SECTION (t38Packet62, "T38PACKET")
t38Packet_t t38Packet63;
#pragma DATA_SECTION (t38Packet63, "T38PACKET")
t38Packet_t t38Packet64;
#pragma DATA_SECTION (t38Packet64, "T38PACKET")
t38Packet_t t38Packet65;
#pragma DATA_SECTION (t38Packet65, "T38PACKET")
t38Packet_t t38Packet66;
#pragma DATA_SECTION (t38Packet66, "T38PACKET")
t38Packet_t t38Packet67;
#pragma DATA_SECTION (t38Packet67, "T38PACKET")
t38Packet_t t38Packet68;
#pragma DATA_SECTION (t38Packet68, "T38PACKET")
t38Packet_t t38Packet69;
#pragma DATA_SECTION (t38Packet69, "T38PACKET")
t38Packet_t t38Packet70;
#pragma DATA_SECTION (t38Packet70, "T38PACKET")
t38Packet_t t38Packet71;
#pragma DATA_SECTION (t38Packet71, "T38PACKET")
t38Packet_t t38Packet72;
#pragma DATA_SECTION (t38Packet72, "T38PACKET")
t38Packet_t t38Packet73;
#pragma DATA_SECTION (t38Packet73, "T38PACKET")
t38Packet_t t38Packet74;
#pragma DATA_SECTION (t38Packet74, "T38PACKET")
t38Packet_t t38Packet75;
#pragma DATA_SECTION (t38Packet75, "T38PACKET")
t38Packet_t t38Packet76;
#pragma DATA_SECTION (t38Packet76, "T38PACKET")
t38Packet_t t38Packet77;
#pragma DATA_SECTION (t38Packet77, "T38PACKET")
t38Packet_t t38Packet78;
#pragma DATA_SECTION (t38Packet78, "T38PACKET")
t38Packet_t t38Packet79;
#pragma DATA_SECTION (t38Packet79, "T38PACKET")
t38Packet_t t38Packet80;
#pragma DATA_SECTION (t38Packet80, "T38PACKET")
t38Packet_t t38Packet81;
#pragma DATA_SECTION (t38Packet81, "T38PACKET")
t38Packet_t t38Packet82;
#pragma DATA_SECTION (t38Packet82, "T38PACKET")
t38Packet_t t38Packet83;
#pragma DATA_SECTION (t38Packet83, "T38PACKET")
t38Packet_t t38Packet84;
#pragma DATA_SECTION (t38Packet84, "T38PACKET")
t38Packet_t t38Packet85;
#pragma DATA_SECTION (t38Packet85, "T38PACKET")
t38Packet_t t38Packet86;
#pragma DATA_SECTION (t38Packet86, "T38PACKET")
t38Packet_t t38Packet87;
#pragma DATA_SECTION (t38Packet87, "T38PACKET")
t38Packet_t t38Packet88;
#pragma DATA_SECTION (t38Packet88, "T38PACKET")
t38Packet_t t38Packet89;
#pragma DATA_SECTION (t38Packet89, "T38PACKET")
t38Packet_t t38Packet90;
#pragma DATA_SECTION (t38Packet90, "T38PACKET")
t38Packet_t t38Packet91;
#pragma DATA_SECTION (t38Packet91, "T38PACKET")
t38Packet_t t38Packet92;
#pragma DATA_SECTION (t38Packet92, "T38PACKET")
t38Packet_t t38Packet93;
#pragma DATA_SECTION (t38Packet93, "T38PACKET")
t38Packet_t t38Packet94;
#pragma DATA_SECTION (t38Packet94, "T38PACKET")
t38Packet_t t38Packet95;
#pragma DATA_SECTION (t38Packet95, "T38PACKET")
t38Packet_t t38Packet96;
#pragma DATA_SECTION (t38Packet96, "T38PACKET")
t38Packet_t t38Packet97;
#pragma DATA_SECTION (t38Packet97, "T38PACKET")
t38Packet_t t38Packet98;
#pragma DATA_SECTION (t38Packet98, "T38PACKET")
t38Packet_t t38Packet99;
#pragma DATA_SECTION (t38Packet99, "T38PACKET")
t38Packet_t t38Packet100;
#pragma DATA_SECTION (t38Packet100, "T38PACKET")
t38Packet_t t38Packet101;
#pragma DATA_SECTION (t38Packet101, "T38PACKET")
t38Packet_t t38Packet102;
#pragma DATA_SECTION (t38Packet102, "T38PACKET")
t38Packet_t t38Packet103;
#pragma DATA_SECTION (t38Packet103, "T38PACKET")
t38Packet_t t38Packet104;
#pragma DATA_SECTION (t38Packet104, "T38PACKET")
t38Packet_t t38Packet105;
#pragma DATA_SECTION (t38Packet105, "T38PACKET")
t38Packet_t t38Packet106;
#pragma DATA_SECTION (t38Packet106, "T38PACKET")
t38Packet_t t38Packet107;
#pragma DATA_SECTION (t38Packet107, "T38PACKET")
t38Packet_t t38Packet108;
#pragma DATA_SECTION (t38Packet108, "T38PACKET")
t38Packet_t t38Packet109;
#pragma DATA_SECTION (t38Packet109, "T38PACKET")
t38Packet_t t38Packet110;
#pragma DATA_SECTION (t38Packet110, "T38PACKET")
t38Packet_t t38Packet111;
#pragma DATA_SECTION (t38Packet111, "T38PACKET")
t38Packet_t t38Packet112;
#pragma DATA_SECTION (t38Packet112, "T38PACKET")
t38Packet_t t38Packet113;
#pragma DATA_SECTION (t38Packet113, "T38PACKET")
t38Packet_t t38Packet114;
#pragma DATA_SECTION (t38Packet114, "T38PACKET")
t38Packet_t t38Packet115;
#pragma DATA_SECTION (t38Packet115, "T38PACKET")
t38Packet_t t38Packet116;
#pragma DATA_SECTION (t38Packet116, "T38PACKET")
t38Packet_t t38Packet117;
#pragma DATA_SECTION (t38Packet117, "T38PACKET")
t38Packet_t t38Packet118;
#pragma DATA_SECTION (t38Packet118, "T38PACKET")
t38Packet_t t38Packet119;
#pragma DATA_SECTION (t38Packet119, "T38PACKET")
t38Packet_t t38Packet120;
#pragma DATA_SECTION (t38Packet120, "T38PACKET")
t38Packet_t t38Packet121;
#pragma DATA_SECTION (t38Packet121, "T38PACKET")
t38Packet_t t38Packet122;
#pragma DATA_SECTION (t38Packet122, "T38PACKET")
t38Packet_t t38Packet123;
#pragma DATA_SECTION (t38Packet123, "T38PACKET")
t38Packet_t t38Packet124;
#pragma DATA_SECTION (t38Packet124, "T38PACKET")
t38Packet_t t38Packet125;
#pragma DATA_SECTION (t38Packet125, "T38PACKET")
t38Packet_t t38Packet126;
#pragma DATA_SECTION (t38Packet126, "T38PACKET")
t38Packet_t t38Packet127;
#pragma DATA_SECTION (t38Packet127, "T38PACKET")
t38Packet_t t38Packet128;
#pragma DATA_SECTION (t38Packet128, "T38PACKET")
t38Packet_t t38Packet129;
#pragma DATA_SECTION (t38Packet129, "T38PACKET")
t38Packet_t t38Packet130;
#pragma DATA_SECTION (t38Packet130, "T38PACKET")
t38Packet_t t38Packet131;
#pragma DATA_SECTION (t38Packet131, "T38PACKET")
t38Packet_t t38Packet132;
#pragma DATA_SECTION (t38Packet132, "T38PACKET")
t38Packet_t t38Packet133;
#pragma DATA_SECTION (t38Packet133, "T38PACKET")
t38Packet_t t38Packet134;
#pragma DATA_SECTION (t38Packet134, "T38PACKET")
t38Packet_t t38Packet135;
#pragma DATA_SECTION (t38Packet135, "T38PACKET")
t38Packet_t t38Packet136;
#pragma DATA_SECTION (t38Packet136, "T38PACKET")
t38Packet_t t38Packet137;
#pragma DATA_SECTION (t38Packet137, "T38PACKET")
t38Packet_t t38Packet138;
#pragma DATA_SECTION (t38Packet138, "T38PACKET")
t38Packet_t t38Packet139;
#pragma DATA_SECTION (t38Packet139, "T38PACKET")
t38Packet_t t38Packet140;
#pragma DATA_SECTION (t38Packet140, "T38PACKET")
t38Packet_t t38Packet141;
#pragma DATA_SECTION (t38Packet141, "T38PACKET")
t38Packet_t t38Packet142;
#pragma DATA_SECTION (t38Packet142, "T38PACKET")
t38Packet_t t38Packet143;
#pragma DATA_SECTION (t38Packet143, "T38PACKET")
t38Packet_t t38Packet144;
#pragma DATA_SECTION (t38Packet144, "T38PACKET")
t38Packet_t t38Packet145;
#pragma DATA_SECTION (t38Packet145, "T38PACKET")
t38Packet_t t38Packet146;
#pragma DATA_SECTION (t38Packet146, "T38PACKET")
t38Packet_t t38Packet147;
#pragma DATA_SECTION (t38Packet147, "T38PACKET")
t38Packet_t t38Packet148;
#pragma DATA_SECTION (t38Packet148, "T38PACKET")
t38Packet_t t38Packet149;
#pragma DATA_SECTION (t38Packet149, "T38PACKET")
t38Packet_t t38Packet150;
#pragma DATA_SECTION (t38Packet150, "T38PACKET")
t38Packet_t t38Packet151;
#pragma DATA_SECTION (t38Packet151, "T38PACKET")
t38Packet_t t38Packet152;
#pragma DATA_SECTION (t38Packet152, "T38PACKET")
t38Packet_t t38Packet153;
#pragma DATA_SECTION (t38Packet153, "T38PACKET")
t38Packet_t t38Packet154;
#pragma DATA_SECTION (t38Packet154, "T38PACKET")
t38Packet_t t38Packet155;
#pragma DATA_SECTION (t38Packet155, "T38PACKET")
t38Packet_t t38Packet156;
#pragma DATA_SECTION (t38Packet156, "T38PACKET")
t38Packet_t t38Packet157;
#pragma DATA_SECTION (t38Packet157, "T38PACKET")
t38Packet_t t38Packet158;
#pragma DATA_SECTION (t38Packet158, "T38PACKET")
t38Packet_t t38Packet159;
#pragma DATA_SECTION (t38Packet159, "T38PACKET")
t38Packet_t *pT38Packets[] = {
	&t38Packet0,
	&t38Packet1,
	&t38Packet2,
	&t38Packet3,
	&t38Packet4,
	&t38Packet5,
	&t38Packet6,
	&t38Packet7,
	&t38Packet8,
	&t38Packet9,
	&t38Packet10,
	&t38Packet11,
	&t38Packet12,
	&t38Packet13,
	&t38Packet14,
	&t38Packet15,
	&t38Packet16,
	&t38Packet17,
	&t38Packet18,
	&t38Packet19,
	&t38Packet20,
	&t38Packet21,
	&t38Packet22,
	&t38Packet23,
	&t38Packet24,
	&t38Packet25,
	&t38Packet26,
	&t38Packet27,
	&t38Packet28,
	&t38Packet29,
	&t38Packet30,
	&t38Packet31,
	&t38Packet32,
	&t38Packet33,
	&t38Packet34,
	&t38Packet35,
	&t38Packet36,
	&t38Packet37,
	&t38Packet38,
	&t38Packet39,
	&t38Packet40,
	&t38Packet41,
	&t38Packet42,
	&t38Packet43,
	&t38Packet44,
	&t38Packet45,
	&t38Packet46,
	&t38Packet47,
	&t38Packet48,
	&t38Packet49,
	&t38Packet50,
	&t38Packet51,
	&t38Packet52,
	&t38Packet53,
	&t38Packet54,
	&t38Packet55,
	&t38Packet56,
	&t38Packet57,
	&t38Packet58,
	&t38Packet59,
	&t38Packet60,
	&t38Packet61,
	&t38Packet62,
	&t38Packet63,
	&t38Packet64,
	&t38Packet65,
	&t38Packet66,
	&t38Packet67,
	&t38Packet68,
	&t38Packet69,
	&t38Packet70,
	&t38Packet71,
	&t38Packet72,
	&t38Packet73,
	&t38Packet74,
	&t38Packet75,
	&t38Packet76,
	&t38Packet77,
	&t38Packet78,
	&t38Packet79,
	&t38Packet80,
	&t38Packet81,
	&t38Packet82,
	&t38Packet83,
	&t38Packet84,
	&t38Packet85,
	&t38Packet86,
	&t38Packet87,
	&t38Packet88,
	&t38Packet89,
	&t38Packet90,
	&t38Packet91,
	&t38Packet92,
	&t38Packet93,
	&t38Packet94,
	&t38Packet95,
	&t38Packet96,
	&t38Packet97,
	&t38Packet98,
	&t38Packet99,
	&t38Packet100,
	&t38Packet101,
	&t38Packet102,
	&t38Packet103,
	&t38Packet104,
	&t38Packet105,
	&t38Packet106,
	&t38Packet107,
	&t38Packet108,
	&t38Packet109,
	&t38Packet110,
	&t38Packet111,
	&t38Packet112,
	&t38Packet113,
	&t38Packet114,
	&t38Packet115,
	&t38Packet116,
	&t38Packet117,
	&t38Packet118,
	&t38Packet119,
	&t38Packet120,
	&t38Packet121,
	&t38Packet122,
	&t38Packet123,
	&t38Packet124,
	&t38Packet125,
	&t38Packet126,
	&t38Packet127,
	&t38Packet128,
	&t38Packet129,
	&t38Packet130,
	&t38Packet131,
	&t38Packet132,
	&t38Packet133,
	&t38Packet134,
	&t38Packet135,
	&t38Packet136,
	&t38Packet137,
	&t38Packet138,
	&t38Packet139,
	&t38Packet140,
	&t38Packet141,
	&t38Packet142,
	&t38Packet143,
	&t38Packet144,
	&t38Packet145,
	&t38Packet146,
	&t38Packet147,
	&t38Packet148,
	&t38Packet149,
	&t38Packet150,
	&t38Packet151,
	&t38Packet152,
	&t38Packet153,
	&t38Packet154,
	&t38Packet155,
	&t38Packet156,
	&t38Packet157,
	&t38Packet158,
	&t38Packet159
};




/*=============================== */
// Caller Id data structures.

const ADT_UInt16 rxCidBufferI8 = RX_CID_MSG_I8;
ADT_UInt16 numRxCidChansAvail=0;
ADT_UInt16 rxCidInUse;
ADT_UInt16 rxCidPool;
ADT_UInt8  rxCidBuffer;
#pragma DATA_SECTION (numRxCidChansAvail,  "POOL_ALLOC")
#pragma DATA_SECTION (rxCidInUse,  "STUB_DATA_SECT")
#pragma DATA_SECTION (rxCidPool,   "STUB_DATA_SECT")
#pragma DATA_SECTION (rxCidBuffer, "STUB_DATA_SECT")


const ADT_UInt16 txCidBufferI8 = TX_CID_MSG_I8;
const ADT_UInt16 txCidBurstI8  = TX_CID_BRST_I8;
ADT_UInt16 numTxCidChansAvail=0;
ADT_UInt16 txCidInUse;
ADT_UInt16 txCidPool;
ADT_UInt8  txCidMsgBuffer;
ADT_UInt8  txCidBrstBuffer;
#pragma DATA_SECTION (numTxCidChansAvail,  "POOL_ALLOC")
#pragma DATA_SECTION (txCidInUse,      "STUB_DATA_SECT")
#pragma DATA_SECTION (txCidPool,       "STUB_DATA_SECT")
#pragma DATA_SECTION (txCidMsgBuffer,  "STUB_DATA_SECT")
#pragma DATA_SECTION (txCidBrstBuffer, "STUB_DATA_SECT")




/*=============================== */
// RTP and jitter buffer data structures.
//{          RtpChanData_t          RtpChanData STRUCTURES to CHAN_INST_DATA:RTP

RtpChanData_t RtpChanData0;
#pragma DATA_SECTION (RtpChanData0, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData0, 128)

static RtpChanData_t RtpChanData1;
#pragma DATA_SECTION (RtpChanData1, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData1, 128)

static RtpChanData_t RtpChanData2;
#pragma DATA_SECTION (RtpChanData2, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData2, 128)

static RtpChanData_t RtpChanData3;
#pragma DATA_SECTION (RtpChanData3, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData3, 128)

static RtpChanData_t RtpChanData4;
#pragma DATA_SECTION (RtpChanData4, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData4, 128)

static RtpChanData_t RtpChanData5;
#pragma DATA_SECTION (RtpChanData5, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData5, 128)

static RtpChanData_t RtpChanData6;
#pragma DATA_SECTION (RtpChanData6, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData6, 128)

static RtpChanData_t RtpChanData7;
#pragma DATA_SECTION (RtpChanData7, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData7, 128)

static RtpChanData_t RtpChanData8;
#pragma DATA_SECTION (RtpChanData8, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData8, 128)

static RtpChanData_t RtpChanData9;
#pragma DATA_SECTION (RtpChanData9, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData9, 128)

static RtpChanData_t RtpChanData10;
#pragma DATA_SECTION (RtpChanData10, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData10, 128)

static RtpChanData_t RtpChanData11;
#pragma DATA_SECTION (RtpChanData11, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData11, 128)

static RtpChanData_t RtpChanData12;
#pragma DATA_SECTION (RtpChanData12, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData12, 128)

static RtpChanData_t RtpChanData13;
#pragma DATA_SECTION (RtpChanData13, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData13, 128)

static RtpChanData_t RtpChanData14;
#pragma DATA_SECTION (RtpChanData14, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData14, 128)

static RtpChanData_t RtpChanData15;
#pragma DATA_SECTION (RtpChanData15, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData15, 128)

static RtpChanData_t RtpChanData16;
#pragma DATA_SECTION (RtpChanData16, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData16, 128)

static RtpChanData_t RtpChanData17;
#pragma DATA_SECTION (RtpChanData17, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData17, 128)

static RtpChanData_t RtpChanData18;
#pragma DATA_SECTION (RtpChanData18, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData18, 128)

static RtpChanData_t RtpChanData19;
#pragma DATA_SECTION (RtpChanData19, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData19, 128)

static RtpChanData_t RtpChanData20;
#pragma DATA_SECTION (RtpChanData20, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData20, 128)

static RtpChanData_t RtpChanData21;
#pragma DATA_SECTION (RtpChanData21, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData21, 128)

static RtpChanData_t RtpChanData22;
#pragma DATA_SECTION (RtpChanData22, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData22, 128)

static RtpChanData_t RtpChanData23;
#pragma DATA_SECTION (RtpChanData23, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData23, 128)

static RtpChanData_t RtpChanData24;
#pragma DATA_SECTION (RtpChanData24, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData24, 128)

static RtpChanData_t RtpChanData25;
#pragma DATA_SECTION (RtpChanData25, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData25, 128)

static RtpChanData_t RtpChanData26;
#pragma DATA_SECTION (RtpChanData26, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData26, 128)

static RtpChanData_t RtpChanData27;
#pragma DATA_SECTION (RtpChanData27, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData27, 128)

static RtpChanData_t RtpChanData28;
#pragma DATA_SECTION (RtpChanData28, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData28, 128)

static RtpChanData_t RtpChanData29;
#pragma DATA_SECTION (RtpChanData29, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData29, 128)

static RtpChanData_t RtpChanData30;
#pragma DATA_SECTION (RtpChanData30, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData30, 128)

static RtpChanData_t RtpChanData31;
#pragma DATA_SECTION (RtpChanData31, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData31, 128)

static RtpChanData_t RtpChanData32;
#pragma DATA_SECTION (RtpChanData32, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData32, 128)

static RtpChanData_t RtpChanData33;
#pragma DATA_SECTION (RtpChanData33, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData33, 128)

static RtpChanData_t RtpChanData34;
#pragma DATA_SECTION (RtpChanData34, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData34, 128)

static RtpChanData_t RtpChanData35;
#pragma DATA_SECTION (RtpChanData35, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData35, 128)

static RtpChanData_t RtpChanData36;
#pragma DATA_SECTION (RtpChanData36, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData36, 128)

static RtpChanData_t RtpChanData37;
#pragma DATA_SECTION (RtpChanData37, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData37, 128)

static RtpChanData_t RtpChanData38;
#pragma DATA_SECTION (RtpChanData38, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData38, 128)

static RtpChanData_t RtpChanData39;
#pragma DATA_SECTION (RtpChanData39, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData39, 128)

static RtpChanData_t RtpChanData40;
#pragma DATA_SECTION (RtpChanData40, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData40, 128)

static RtpChanData_t RtpChanData41;
#pragma DATA_SECTION (RtpChanData41, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData41, 128)

static RtpChanData_t RtpChanData42;
#pragma DATA_SECTION (RtpChanData42, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData42, 128)

static RtpChanData_t RtpChanData43;
#pragma DATA_SECTION (RtpChanData43, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData43, 128)

static RtpChanData_t RtpChanData44;
#pragma DATA_SECTION (RtpChanData44, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData44, 128)

static RtpChanData_t RtpChanData45;
#pragma DATA_SECTION (RtpChanData45, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData45, 128)

static RtpChanData_t RtpChanData46;
#pragma DATA_SECTION (RtpChanData46, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData46, 128)

static RtpChanData_t RtpChanData47;
#pragma DATA_SECTION (RtpChanData47, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData47, 128)

static RtpChanData_t RtpChanData48;
#pragma DATA_SECTION (RtpChanData48, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData48, 128)

static RtpChanData_t RtpChanData49;
#pragma DATA_SECTION (RtpChanData49, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData49, 128)

static RtpChanData_t RtpChanData50;
#pragma DATA_SECTION (RtpChanData50, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData50, 128)

static RtpChanData_t RtpChanData51;
#pragma DATA_SECTION (RtpChanData51, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData51, 128)

static RtpChanData_t RtpChanData52;
#pragma DATA_SECTION (RtpChanData52, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData52, 128)

static RtpChanData_t RtpChanData53;
#pragma DATA_SECTION (RtpChanData53, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData53, 128)

static RtpChanData_t RtpChanData54;
#pragma DATA_SECTION (RtpChanData54, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData54, 128)

static RtpChanData_t RtpChanData55;
#pragma DATA_SECTION (RtpChanData55, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData55, 128)

static RtpChanData_t RtpChanData56;
#pragma DATA_SECTION (RtpChanData56, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData56, 128)

static RtpChanData_t RtpChanData57;
#pragma DATA_SECTION (RtpChanData57, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData57, 128)

static RtpChanData_t RtpChanData58;
#pragma DATA_SECTION (RtpChanData58, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData58, 128)

static RtpChanData_t RtpChanData59;
#pragma DATA_SECTION (RtpChanData59, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData59, 128)

static RtpChanData_t RtpChanData60;
#pragma DATA_SECTION (RtpChanData60, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData60, 128)

static RtpChanData_t RtpChanData61;
#pragma DATA_SECTION (RtpChanData61, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData61, 128)

static RtpChanData_t RtpChanData62;
#pragma DATA_SECTION (RtpChanData62, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData62, 128)

static RtpChanData_t RtpChanData63;
#pragma DATA_SECTION (RtpChanData63, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData63, 128)

static RtpChanData_t RtpChanData64;
#pragma DATA_SECTION (RtpChanData64, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData64, 128)

static RtpChanData_t RtpChanData65;
#pragma DATA_SECTION (RtpChanData65, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData65, 128)

static RtpChanData_t RtpChanData66;
#pragma DATA_SECTION (RtpChanData66, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData66, 128)

static RtpChanData_t RtpChanData67;
#pragma DATA_SECTION (RtpChanData67, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData67, 128)

static RtpChanData_t RtpChanData68;
#pragma DATA_SECTION (RtpChanData68, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData68, 128)

static RtpChanData_t RtpChanData69;
#pragma DATA_SECTION (RtpChanData69, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData69, 128)

static RtpChanData_t RtpChanData70;
#pragma DATA_SECTION (RtpChanData70, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData70, 128)

static RtpChanData_t RtpChanData71;
#pragma DATA_SECTION (RtpChanData71, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData71, 128)

static RtpChanData_t RtpChanData72;
#pragma DATA_SECTION (RtpChanData72, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData72, 128)

static RtpChanData_t RtpChanData73;
#pragma DATA_SECTION (RtpChanData73, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData73, 128)

static RtpChanData_t RtpChanData74;
#pragma DATA_SECTION (RtpChanData74, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData74, 128)

static RtpChanData_t RtpChanData75;
#pragma DATA_SECTION (RtpChanData75, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData75, 128)

static RtpChanData_t RtpChanData76;
#pragma DATA_SECTION (RtpChanData76, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData76, 128)

static RtpChanData_t RtpChanData77;
#pragma DATA_SECTION (RtpChanData77, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData77, 128)

static RtpChanData_t RtpChanData78;
#pragma DATA_SECTION (RtpChanData78, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData78, 128)

static RtpChanData_t RtpChanData79;
#pragma DATA_SECTION (RtpChanData79, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData79, 128)

static RtpChanData_t RtpChanData80;
#pragma DATA_SECTION (RtpChanData80, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData80, 128)

static RtpChanData_t RtpChanData81;
#pragma DATA_SECTION (RtpChanData81, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData81, 128)

static RtpChanData_t RtpChanData82;
#pragma DATA_SECTION (RtpChanData82, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData82, 128)

static RtpChanData_t RtpChanData83;
#pragma DATA_SECTION (RtpChanData83, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData83, 128)

static RtpChanData_t RtpChanData84;
#pragma DATA_SECTION (RtpChanData84, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData84, 128)

static RtpChanData_t RtpChanData85;
#pragma DATA_SECTION (RtpChanData85, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData85, 128)

static RtpChanData_t RtpChanData86;
#pragma DATA_SECTION (RtpChanData86, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData86, 128)

static RtpChanData_t RtpChanData87;
#pragma DATA_SECTION (RtpChanData87, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData87, 128)

static RtpChanData_t RtpChanData88;
#pragma DATA_SECTION (RtpChanData88, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData88, 128)

static RtpChanData_t RtpChanData89;
#pragma DATA_SECTION (RtpChanData89, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData89, 128)

static RtpChanData_t RtpChanData90;
#pragma DATA_SECTION (RtpChanData90, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData90, 128)

static RtpChanData_t RtpChanData91;
#pragma DATA_SECTION (RtpChanData91, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData91, 128)

static RtpChanData_t RtpChanData92;
#pragma DATA_SECTION (RtpChanData92, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData92, 128)

static RtpChanData_t RtpChanData93;
#pragma DATA_SECTION (RtpChanData93, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData93, 128)

static RtpChanData_t RtpChanData94;
#pragma DATA_SECTION (RtpChanData94, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData94, 128)

static RtpChanData_t RtpChanData95;
#pragma DATA_SECTION (RtpChanData95, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData95, 128)

static RtpChanData_t RtpChanData96;
#pragma DATA_SECTION (RtpChanData96, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData96, 128)

static RtpChanData_t RtpChanData97;
#pragma DATA_SECTION (RtpChanData97, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData97, 128)

static RtpChanData_t RtpChanData98;
#pragma DATA_SECTION (RtpChanData98, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData98, 128)

static RtpChanData_t RtpChanData99;
#pragma DATA_SECTION (RtpChanData99, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData99, 128)

static RtpChanData_t RtpChanData100;
#pragma DATA_SECTION (RtpChanData100, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData100, 128)

static RtpChanData_t RtpChanData101;
#pragma DATA_SECTION (RtpChanData101, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData101, 128)

static RtpChanData_t RtpChanData102;
#pragma DATA_SECTION (RtpChanData102, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData102, 128)

static RtpChanData_t RtpChanData103;
#pragma DATA_SECTION (RtpChanData103, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData103, 128)

static RtpChanData_t RtpChanData104;
#pragma DATA_SECTION (RtpChanData104, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData104, 128)

static RtpChanData_t RtpChanData105;
#pragma DATA_SECTION (RtpChanData105, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData105, 128)

static RtpChanData_t RtpChanData106;
#pragma DATA_SECTION (RtpChanData106, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData106, 128)

static RtpChanData_t RtpChanData107;
#pragma DATA_SECTION (RtpChanData107, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData107, 128)

static RtpChanData_t RtpChanData108;
#pragma DATA_SECTION (RtpChanData108, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData108, 128)

static RtpChanData_t RtpChanData109;
#pragma DATA_SECTION (RtpChanData109, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData109, 128)

static RtpChanData_t RtpChanData110;
#pragma DATA_SECTION (RtpChanData110, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData110, 128)

static RtpChanData_t RtpChanData111;
#pragma DATA_SECTION (RtpChanData111, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData111, 128)

static RtpChanData_t RtpChanData112;
#pragma DATA_SECTION (RtpChanData112, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData112, 128)

static RtpChanData_t RtpChanData113;
#pragma DATA_SECTION (RtpChanData113, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData113, 128)

static RtpChanData_t RtpChanData114;
#pragma DATA_SECTION (RtpChanData114, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData114, 128)

static RtpChanData_t RtpChanData115;
#pragma DATA_SECTION (RtpChanData115, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData115, 128)

static RtpChanData_t RtpChanData116;
#pragma DATA_SECTION (RtpChanData116, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData116, 128)

static RtpChanData_t RtpChanData117;
#pragma DATA_SECTION (RtpChanData117, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData117, 128)

static RtpChanData_t RtpChanData118;
#pragma DATA_SECTION (RtpChanData118, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData118, 128)

static RtpChanData_t RtpChanData119;
#pragma DATA_SECTION (RtpChanData119, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData119, 128)

static RtpChanData_t RtpChanData120;
#pragma DATA_SECTION (RtpChanData120, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData120, 128)

static RtpChanData_t RtpChanData121;
#pragma DATA_SECTION (RtpChanData121, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData121, 128)

static RtpChanData_t RtpChanData122;
#pragma DATA_SECTION (RtpChanData122, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData122, 128)

static RtpChanData_t RtpChanData123;
#pragma DATA_SECTION (RtpChanData123, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData123, 128)

static RtpChanData_t RtpChanData124;
#pragma DATA_SECTION (RtpChanData124, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData124, 128)

static RtpChanData_t RtpChanData125;
#pragma DATA_SECTION (RtpChanData125, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData125, 128)

static RtpChanData_t RtpChanData126;
#pragma DATA_SECTION (RtpChanData126, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData126, 128)

static RtpChanData_t RtpChanData127;
#pragma DATA_SECTION (RtpChanData127, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData127, 128)

static RtpChanData_t RtpChanData128;
#pragma DATA_SECTION (RtpChanData128, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData128, 128)

static RtpChanData_t RtpChanData129;
#pragma DATA_SECTION (RtpChanData129, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData129, 128)

static RtpChanData_t RtpChanData130;
#pragma DATA_SECTION (RtpChanData130, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData130, 128)

static RtpChanData_t RtpChanData131;
#pragma DATA_SECTION (RtpChanData131, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData131, 128)

static RtpChanData_t RtpChanData132;
#pragma DATA_SECTION (RtpChanData132, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData132, 128)

static RtpChanData_t RtpChanData133;
#pragma DATA_SECTION (RtpChanData133, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData133, 128)

static RtpChanData_t RtpChanData134;
#pragma DATA_SECTION (RtpChanData134, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData134, 128)

static RtpChanData_t RtpChanData135;
#pragma DATA_SECTION (RtpChanData135, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData135, 128)

static RtpChanData_t RtpChanData136;
#pragma DATA_SECTION (RtpChanData136, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData136, 128)

static RtpChanData_t RtpChanData137;
#pragma DATA_SECTION (RtpChanData137, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData137, 128)

static RtpChanData_t RtpChanData138;
#pragma DATA_SECTION (RtpChanData138, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData138, 128)

static RtpChanData_t RtpChanData139;
#pragma DATA_SECTION (RtpChanData139, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData139, 128)

static RtpChanData_t RtpChanData140;
#pragma DATA_SECTION (RtpChanData140, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData140, 128)

static RtpChanData_t RtpChanData141;
#pragma DATA_SECTION (RtpChanData141, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData141, 128)

static RtpChanData_t RtpChanData142;
#pragma DATA_SECTION (RtpChanData142, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData142, 128)

static RtpChanData_t RtpChanData143;
#pragma DATA_SECTION (RtpChanData143, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData143, 128)

static RtpChanData_t RtpChanData144;
#pragma DATA_SECTION (RtpChanData144, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData144, 128)

static RtpChanData_t RtpChanData145;
#pragma DATA_SECTION (RtpChanData145, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData145, 128)

static RtpChanData_t RtpChanData146;
#pragma DATA_SECTION (RtpChanData146, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData146, 128)

static RtpChanData_t RtpChanData147;
#pragma DATA_SECTION (RtpChanData147, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData147, 128)

static RtpChanData_t RtpChanData148;
#pragma DATA_SECTION (RtpChanData148, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData148, 128)

static RtpChanData_t RtpChanData149;
#pragma DATA_SECTION (RtpChanData149, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData149, 128)

static RtpChanData_t RtpChanData150;
#pragma DATA_SECTION (RtpChanData150, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData150, 128)

static RtpChanData_t RtpChanData151;
#pragma DATA_SECTION (RtpChanData151, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData151, 128)

static RtpChanData_t RtpChanData152;
#pragma DATA_SECTION (RtpChanData152, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData152, 128)

static RtpChanData_t RtpChanData153;
#pragma DATA_SECTION (RtpChanData153, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData153, 128)

static RtpChanData_t RtpChanData154;
#pragma DATA_SECTION (RtpChanData154, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData154, 128)

static RtpChanData_t RtpChanData155;
#pragma DATA_SECTION (RtpChanData155, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData155, 128)

static RtpChanData_t RtpChanData156;
#pragma DATA_SECTION (RtpChanData156, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData156, 128)

static RtpChanData_t RtpChanData157;
#pragma DATA_SECTION (RtpChanData157, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData157, 128)

static RtpChanData_t RtpChanData158;
#pragma DATA_SECTION (RtpChanData158, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData158, 128)

static RtpChanData_t RtpChanData159;
#pragma DATA_SECTION (RtpChanData159, "CHAN_INST_DATA:RTP")
#pragma DATA_ALIGN   (RtpChanData159, 128)

RtpChanData_t* const RtpChanData[] = {
	&RtpChanData0,
	&RtpChanData1,
	&RtpChanData2,
	&RtpChanData3,
	&RtpChanData4,
	&RtpChanData5,
	&RtpChanData6,
	&RtpChanData7,
	&RtpChanData8,
	&RtpChanData9,
	&RtpChanData10,
	&RtpChanData11,
	&RtpChanData12,
	&RtpChanData13,
	&RtpChanData14,
	&RtpChanData15,
	&RtpChanData16,
	&RtpChanData17,
	&RtpChanData18,
	&RtpChanData19,
	&RtpChanData20,
	&RtpChanData21,
	&RtpChanData22,
	&RtpChanData23,
	&RtpChanData24,
	&RtpChanData25,
	&RtpChanData26,
	&RtpChanData27,
	&RtpChanData28,
	&RtpChanData29,
	&RtpChanData30,
	&RtpChanData31,
	&RtpChanData32,
	&RtpChanData33,
	&RtpChanData34,
	&RtpChanData35,
	&RtpChanData36,
	&RtpChanData37,
	&RtpChanData38,
	&RtpChanData39,
	&RtpChanData40,
	&RtpChanData41,
	&RtpChanData42,
	&RtpChanData43,
	&RtpChanData44,
	&RtpChanData45,
	&RtpChanData46,
	&RtpChanData47,
	&RtpChanData48,
	&RtpChanData49,
	&RtpChanData50,
	&RtpChanData51,
	&RtpChanData52,
	&RtpChanData53,
	&RtpChanData54,
	&RtpChanData55,
	&RtpChanData56,
	&RtpChanData57,
	&RtpChanData58,
	&RtpChanData59,
	&RtpChanData60,
	&RtpChanData61,
	&RtpChanData62,
	&RtpChanData63,
	&RtpChanData64,
	&RtpChanData65,
	&RtpChanData66,
	&RtpChanData67,
	&RtpChanData68,
	&RtpChanData69,
	&RtpChanData70,
	&RtpChanData71,
	&RtpChanData72,
	&RtpChanData73,
	&RtpChanData74,
	&RtpChanData75,
	&RtpChanData76,
	&RtpChanData77,
	&RtpChanData78,
	&RtpChanData79,
	&RtpChanData80,
	&RtpChanData81,
	&RtpChanData82,
	&RtpChanData83,
	&RtpChanData84,
	&RtpChanData85,
	&RtpChanData86,
	&RtpChanData87,
	&RtpChanData88,
	&RtpChanData89,
	&RtpChanData90,
	&RtpChanData91,
	&RtpChanData92,
	&RtpChanData93,
	&RtpChanData94,
	&RtpChanData95,
	&RtpChanData96,
	&RtpChanData97,
	&RtpChanData98,
	&RtpChanData99,
	&RtpChanData100,
	&RtpChanData101,
	&RtpChanData102,
	&RtpChanData103,
	&RtpChanData104,
	&RtpChanData105,
	&RtpChanData106,
	&RtpChanData107,
	&RtpChanData108,
	&RtpChanData109,
	&RtpChanData110,
	&RtpChanData111,
	&RtpChanData112,
	&RtpChanData113,
	&RtpChanData114,
	&RtpChanData115,
	&RtpChanData116,
	&RtpChanData117,
	&RtpChanData118,
	&RtpChanData119,
	&RtpChanData120,
	&RtpChanData121,
	&RtpChanData122,
	&RtpChanData123,
	&RtpChanData124,
	&RtpChanData125,
	&RtpChanData126,
	&RtpChanData127,
	&RtpChanData128,
	&RtpChanData129,
	&RtpChanData130,
	&RtpChanData131,
	&RtpChanData132,
	&RtpChanData133,
	&RtpChanData134,
	&RtpChanData135,
	&RtpChanData136,
	&RtpChanData137,
	&RtpChanData138,
	&RtpChanData139,
	&RtpChanData140,
	&RtpChanData141,
	&RtpChanData142,
	&RtpChanData143,
	&RtpChanData144,
	&RtpChanData145,
	&RtpChanData146,
	&RtpChanData147,
	&RtpChanData148,
	&RtpChanData149,
	&RtpChanData150,
	&RtpChanData151,
	&RtpChanData152,
	&RtpChanData153,
	&RtpChanData154,
	&RtpChanData155,
	&RtpChanData156,
	&RtpChanData157,
	&RtpChanData158,
	&RtpChanData159
};
//}



//{
ADT_UInt32 SmallJBPool[SMALL_JB_POOL_I32];     // Memory pool for 1, 2.5 and 5 millisecond frame rates
ADT_UInt32 LargeJBPool[LARGE_JB_POOL_I32];     // Memory pool for 10, 20, 22.5 and 30 millisecond frame rates
ADT_UInt32 HugeJBPool[HUGE_JB_POOL_I32];       // Memory pool for custom frame rates greater than 30 milliseconds
#pragma DATA_SECTION (SmallJBPool,  "JB_POOL")
#pragma DATA_SECTION (LargeJBPool,  "JB_POOL")
#pragma DATA_SECTION (HugeJBPool,  "JB_POOL")
#pragma DATA_ALIGN   (SmallJBPool, 128)
#pragma DATA_ALIGN   (LargeJBPool, 128)

#pragma DATA_ALIGN   (HugeJBPool, 128)

CircBufInfo_t RTPFreeBuffers[CORE_CNT];
#pragma DATA_SECTION (RTPFreeBuffers,  "RTP_CIRC")
CircBufInfo_t RTPCircBuffers[CORE_CNT * 2];              // 1 = To Net, 0 = To DSP
#pragma DATA_SECTION (RTPCircBuffers,  "RTP_CIRC")

ADT_UInt16    ToNetRTPBuffer[RTP_BUFFERS_I16];
ADT_UInt16    ToDSPRTPBuffer[RTP_BUFFERS_I16];
#pragma DATA_SECTION (ToNetRTPBuffer, "RTP_POOL:ToNet")
#pragma DATA_SECTION (ToDSPRTPBuffer, "RTP_POOL:ToDSP")
//}

// SRTP data structures.
#pragma DATA_SECTION (SrtpScratchPtr, "PER_CORE_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpScratchPtr, 128)
ADT_UInt32 SrtpScratchPtr[340/4];

const int srtpTxI8 = (SRTPTX_SIZE_I4 * 4);
//{       SrtpTxInstance_t       SrtpTxChanData STRUCTURES to CHAN_INST_DATA:SRTP

SrtpTxInstance_t SrtpTxChanData0;
#pragma DATA_SECTION (SrtpTxChanData0, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData0, 128)

static SrtpTxInstance_t SrtpTxChanData1;
#pragma DATA_SECTION (SrtpTxChanData1, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData1, 128)

static SrtpTxInstance_t SrtpTxChanData2;
#pragma DATA_SECTION (SrtpTxChanData2, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData2, 128)

static SrtpTxInstance_t SrtpTxChanData3;
#pragma DATA_SECTION (SrtpTxChanData3, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData3, 128)

static SrtpTxInstance_t SrtpTxChanData4;
#pragma DATA_SECTION (SrtpTxChanData4, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData4, 128)

static SrtpTxInstance_t SrtpTxChanData5;
#pragma DATA_SECTION (SrtpTxChanData5, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData5, 128)

static SrtpTxInstance_t SrtpTxChanData6;
#pragma DATA_SECTION (SrtpTxChanData6, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData6, 128)

static SrtpTxInstance_t SrtpTxChanData7;
#pragma DATA_SECTION (SrtpTxChanData7, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData7, 128)

static SrtpTxInstance_t SrtpTxChanData8;
#pragma DATA_SECTION (SrtpTxChanData8, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData8, 128)

static SrtpTxInstance_t SrtpTxChanData9;
#pragma DATA_SECTION (SrtpTxChanData9, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData9, 128)

static SrtpTxInstance_t SrtpTxChanData10;
#pragma DATA_SECTION (SrtpTxChanData10, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData10, 128)

static SrtpTxInstance_t SrtpTxChanData11;
#pragma DATA_SECTION (SrtpTxChanData11, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData11, 128)

static SrtpTxInstance_t SrtpTxChanData12;
#pragma DATA_SECTION (SrtpTxChanData12, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData12, 128)

static SrtpTxInstance_t SrtpTxChanData13;
#pragma DATA_SECTION (SrtpTxChanData13, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData13, 128)

static SrtpTxInstance_t SrtpTxChanData14;
#pragma DATA_SECTION (SrtpTxChanData14, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData14, 128)

static SrtpTxInstance_t SrtpTxChanData15;
#pragma DATA_SECTION (SrtpTxChanData15, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData15, 128)

static SrtpTxInstance_t SrtpTxChanData16;
#pragma DATA_SECTION (SrtpTxChanData16, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData16, 128)

static SrtpTxInstance_t SrtpTxChanData17;
#pragma DATA_SECTION (SrtpTxChanData17, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData17, 128)

static SrtpTxInstance_t SrtpTxChanData18;
#pragma DATA_SECTION (SrtpTxChanData18, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData18, 128)

static SrtpTxInstance_t SrtpTxChanData19;
#pragma DATA_SECTION (SrtpTxChanData19, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData19, 128)

static SrtpTxInstance_t SrtpTxChanData20;
#pragma DATA_SECTION (SrtpTxChanData20, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData20, 128)

static SrtpTxInstance_t SrtpTxChanData21;
#pragma DATA_SECTION (SrtpTxChanData21, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData21, 128)

static SrtpTxInstance_t SrtpTxChanData22;
#pragma DATA_SECTION (SrtpTxChanData22, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData22, 128)

static SrtpTxInstance_t SrtpTxChanData23;
#pragma DATA_SECTION (SrtpTxChanData23, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData23, 128)

static SrtpTxInstance_t SrtpTxChanData24;
#pragma DATA_SECTION (SrtpTxChanData24, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData24, 128)

static SrtpTxInstance_t SrtpTxChanData25;
#pragma DATA_SECTION (SrtpTxChanData25, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData25, 128)

static SrtpTxInstance_t SrtpTxChanData26;
#pragma DATA_SECTION (SrtpTxChanData26, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData26, 128)

static SrtpTxInstance_t SrtpTxChanData27;
#pragma DATA_SECTION (SrtpTxChanData27, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData27, 128)

static SrtpTxInstance_t SrtpTxChanData28;
#pragma DATA_SECTION (SrtpTxChanData28, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData28, 128)

static SrtpTxInstance_t SrtpTxChanData29;
#pragma DATA_SECTION (SrtpTxChanData29, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData29, 128)

static SrtpTxInstance_t SrtpTxChanData30;
#pragma DATA_SECTION (SrtpTxChanData30, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData30, 128)

static SrtpTxInstance_t SrtpTxChanData31;
#pragma DATA_SECTION (SrtpTxChanData31, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData31, 128)

static SrtpTxInstance_t SrtpTxChanData32;
#pragma DATA_SECTION (SrtpTxChanData32, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData32, 128)

static SrtpTxInstance_t SrtpTxChanData33;
#pragma DATA_SECTION (SrtpTxChanData33, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData33, 128)

static SrtpTxInstance_t SrtpTxChanData34;
#pragma DATA_SECTION (SrtpTxChanData34, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData34, 128)

static SrtpTxInstance_t SrtpTxChanData35;
#pragma DATA_SECTION (SrtpTxChanData35, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData35, 128)

static SrtpTxInstance_t SrtpTxChanData36;
#pragma DATA_SECTION (SrtpTxChanData36, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData36, 128)

static SrtpTxInstance_t SrtpTxChanData37;
#pragma DATA_SECTION (SrtpTxChanData37, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData37, 128)

static SrtpTxInstance_t SrtpTxChanData38;
#pragma DATA_SECTION (SrtpTxChanData38, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData38, 128)

static SrtpTxInstance_t SrtpTxChanData39;
#pragma DATA_SECTION (SrtpTxChanData39, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData39, 128)

static SrtpTxInstance_t SrtpTxChanData40;
#pragma DATA_SECTION (SrtpTxChanData40, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData40, 128)

static SrtpTxInstance_t SrtpTxChanData41;
#pragma DATA_SECTION (SrtpTxChanData41, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData41, 128)

static SrtpTxInstance_t SrtpTxChanData42;
#pragma DATA_SECTION (SrtpTxChanData42, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData42, 128)

static SrtpTxInstance_t SrtpTxChanData43;
#pragma DATA_SECTION (SrtpTxChanData43, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData43, 128)

static SrtpTxInstance_t SrtpTxChanData44;
#pragma DATA_SECTION (SrtpTxChanData44, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData44, 128)

static SrtpTxInstance_t SrtpTxChanData45;
#pragma DATA_SECTION (SrtpTxChanData45, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData45, 128)

static SrtpTxInstance_t SrtpTxChanData46;
#pragma DATA_SECTION (SrtpTxChanData46, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData46, 128)

static SrtpTxInstance_t SrtpTxChanData47;
#pragma DATA_SECTION (SrtpTxChanData47, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData47, 128)

static SrtpTxInstance_t SrtpTxChanData48;
#pragma DATA_SECTION (SrtpTxChanData48, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData48, 128)

static SrtpTxInstance_t SrtpTxChanData49;
#pragma DATA_SECTION (SrtpTxChanData49, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData49, 128)

static SrtpTxInstance_t SrtpTxChanData50;
#pragma DATA_SECTION (SrtpTxChanData50, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData50, 128)

static SrtpTxInstance_t SrtpTxChanData51;
#pragma DATA_SECTION (SrtpTxChanData51, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData51, 128)

static SrtpTxInstance_t SrtpTxChanData52;
#pragma DATA_SECTION (SrtpTxChanData52, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData52, 128)

static SrtpTxInstance_t SrtpTxChanData53;
#pragma DATA_SECTION (SrtpTxChanData53, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData53, 128)

static SrtpTxInstance_t SrtpTxChanData54;
#pragma DATA_SECTION (SrtpTxChanData54, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData54, 128)

static SrtpTxInstance_t SrtpTxChanData55;
#pragma DATA_SECTION (SrtpTxChanData55, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData55, 128)

static SrtpTxInstance_t SrtpTxChanData56;
#pragma DATA_SECTION (SrtpTxChanData56, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData56, 128)

static SrtpTxInstance_t SrtpTxChanData57;
#pragma DATA_SECTION (SrtpTxChanData57, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData57, 128)

static SrtpTxInstance_t SrtpTxChanData58;
#pragma DATA_SECTION (SrtpTxChanData58, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData58, 128)

static SrtpTxInstance_t SrtpTxChanData59;
#pragma DATA_SECTION (SrtpTxChanData59, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData59, 128)

static SrtpTxInstance_t SrtpTxChanData60;
#pragma DATA_SECTION (SrtpTxChanData60, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData60, 128)

static SrtpTxInstance_t SrtpTxChanData61;
#pragma DATA_SECTION (SrtpTxChanData61, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData61, 128)

static SrtpTxInstance_t SrtpTxChanData62;
#pragma DATA_SECTION (SrtpTxChanData62, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData62, 128)

static SrtpTxInstance_t SrtpTxChanData63;
#pragma DATA_SECTION (SrtpTxChanData63, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData63, 128)

static SrtpTxInstance_t SrtpTxChanData64;
#pragma DATA_SECTION (SrtpTxChanData64, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData64, 128)

static SrtpTxInstance_t SrtpTxChanData65;
#pragma DATA_SECTION (SrtpTxChanData65, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData65, 128)

static SrtpTxInstance_t SrtpTxChanData66;
#pragma DATA_SECTION (SrtpTxChanData66, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData66, 128)

static SrtpTxInstance_t SrtpTxChanData67;
#pragma DATA_SECTION (SrtpTxChanData67, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData67, 128)

static SrtpTxInstance_t SrtpTxChanData68;
#pragma DATA_SECTION (SrtpTxChanData68, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData68, 128)

static SrtpTxInstance_t SrtpTxChanData69;
#pragma DATA_SECTION (SrtpTxChanData69, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData69, 128)

static SrtpTxInstance_t SrtpTxChanData70;
#pragma DATA_SECTION (SrtpTxChanData70, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData70, 128)

static SrtpTxInstance_t SrtpTxChanData71;
#pragma DATA_SECTION (SrtpTxChanData71, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData71, 128)

static SrtpTxInstance_t SrtpTxChanData72;
#pragma DATA_SECTION (SrtpTxChanData72, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData72, 128)

static SrtpTxInstance_t SrtpTxChanData73;
#pragma DATA_SECTION (SrtpTxChanData73, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData73, 128)

static SrtpTxInstance_t SrtpTxChanData74;
#pragma DATA_SECTION (SrtpTxChanData74, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData74, 128)

static SrtpTxInstance_t SrtpTxChanData75;
#pragma DATA_SECTION (SrtpTxChanData75, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData75, 128)

static SrtpTxInstance_t SrtpTxChanData76;
#pragma DATA_SECTION (SrtpTxChanData76, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData76, 128)

static SrtpTxInstance_t SrtpTxChanData77;
#pragma DATA_SECTION (SrtpTxChanData77, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData77, 128)

static SrtpTxInstance_t SrtpTxChanData78;
#pragma DATA_SECTION (SrtpTxChanData78, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData78, 128)

static SrtpTxInstance_t SrtpTxChanData79;
#pragma DATA_SECTION (SrtpTxChanData79, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData79, 128)

static SrtpTxInstance_t SrtpTxChanData80;
#pragma DATA_SECTION (SrtpTxChanData80, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData80, 128)

static SrtpTxInstance_t SrtpTxChanData81;
#pragma DATA_SECTION (SrtpTxChanData81, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData81, 128)

static SrtpTxInstance_t SrtpTxChanData82;
#pragma DATA_SECTION (SrtpTxChanData82, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData82, 128)

static SrtpTxInstance_t SrtpTxChanData83;
#pragma DATA_SECTION (SrtpTxChanData83, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData83, 128)

static SrtpTxInstance_t SrtpTxChanData84;
#pragma DATA_SECTION (SrtpTxChanData84, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData84, 128)

static SrtpTxInstance_t SrtpTxChanData85;
#pragma DATA_SECTION (SrtpTxChanData85, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData85, 128)

static SrtpTxInstance_t SrtpTxChanData86;
#pragma DATA_SECTION (SrtpTxChanData86, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData86, 128)

static SrtpTxInstance_t SrtpTxChanData87;
#pragma DATA_SECTION (SrtpTxChanData87, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData87, 128)

static SrtpTxInstance_t SrtpTxChanData88;
#pragma DATA_SECTION (SrtpTxChanData88, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData88, 128)

static SrtpTxInstance_t SrtpTxChanData89;
#pragma DATA_SECTION (SrtpTxChanData89, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData89, 128)

static SrtpTxInstance_t SrtpTxChanData90;
#pragma DATA_SECTION (SrtpTxChanData90, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData90, 128)

static SrtpTxInstance_t SrtpTxChanData91;
#pragma DATA_SECTION (SrtpTxChanData91, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData91, 128)

static SrtpTxInstance_t SrtpTxChanData92;
#pragma DATA_SECTION (SrtpTxChanData92, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData92, 128)

static SrtpTxInstance_t SrtpTxChanData93;
#pragma DATA_SECTION (SrtpTxChanData93, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData93, 128)

static SrtpTxInstance_t SrtpTxChanData94;
#pragma DATA_SECTION (SrtpTxChanData94, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData94, 128)

static SrtpTxInstance_t SrtpTxChanData95;
#pragma DATA_SECTION (SrtpTxChanData95, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData95, 128)

static SrtpTxInstance_t SrtpTxChanData96;
#pragma DATA_SECTION (SrtpTxChanData96, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData96, 128)

static SrtpTxInstance_t SrtpTxChanData97;
#pragma DATA_SECTION (SrtpTxChanData97, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData97, 128)

static SrtpTxInstance_t SrtpTxChanData98;
#pragma DATA_SECTION (SrtpTxChanData98, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData98, 128)

static SrtpTxInstance_t SrtpTxChanData99;
#pragma DATA_SECTION (SrtpTxChanData99, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData99, 128)

static SrtpTxInstance_t SrtpTxChanData100;
#pragma DATA_SECTION (SrtpTxChanData100, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData100, 128)

static SrtpTxInstance_t SrtpTxChanData101;
#pragma DATA_SECTION (SrtpTxChanData101, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData101, 128)

static SrtpTxInstance_t SrtpTxChanData102;
#pragma DATA_SECTION (SrtpTxChanData102, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData102, 128)

static SrtpTxInstance_t SrtpTxChanData103;
#pragma DATA_SECTION (SrtpTxChanData103, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData103, 128)

static SrtpTxInstance_t SrtpTxChanData104;
#pragma DATA_SECTION (SrtpTxChanData104, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData104, 128)

static SrtpTxInstance_t SrtpTxChanData105;
#pragma DATA_SECTION (SrtpTxChanData105, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData105, 128)

static SrtpTxInstance_t SrtpTxChanData106;
#pragma DATA_SECTION (SrtpTxChanData106, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData106, 128)

static SrtpTxInstance_t SrtpTxChanData107;
#pragma DATA_SECTION (SrtpTxChanData107, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData107, 128)

static SrtpTxInstance_t SrtpTxChanData108;
#pragma DATA_SECTION (SrtpTxChanData108, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData108, 128)

static SrtpTxInstance_t SrtpTxChanData109;
#pragma DATA_SECTION (SrtpTxChanData109, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData109, 128)

static SrtpTxInstance_t SrtpTxChanData110;
#pragma DATA_SECTION (SrtpTxChanData110, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData110, 128)

static SrtpTxInstance_t SrtpTxChanData111;
#pragma DATA_SECTION (SrtpTxChanData111, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData111, 128)

static SrtpTxInstance_t SrtpTxChanData112;
#pragma DATA_SECTION (SrtpTxChanData112, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData112, 128)

static SrtpTxInstance_t SrtpTxChanData113;
#pragma DATA_SECTION (SrtpTxChanData113, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData113, 128)

static SrtpTxInstance_t SrtpTxChanData114;
#pragma DATA_SECTION (SrtpTxChanData114, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData114, 128)

static SrtpTxInstance_t SrtpTxChanData115;
#pragma DATA_SECTION (SrtpTxChanData115, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData115, 128)

static SrtpTxInstance_t SrtpTxChanData116;
#pragma DATA_SECTION (SrtpTxChanData116, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData116, 128)

static SrtpTxInstance_t SrtpTxChanData117;
#pragma DATA_SECTION (SrtpTxChanData117, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData117, 128)

static SrtpTxInstance_t SrtpTxChanData118;
#pragma DATA_SECTION (SrtpTxChanData118, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData118, 128)

static SrtpTxInstance_t SrtpTxChanData119;
#pragma DATA_SECTION (SrtpTxChanData119, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData119, 128)

static SrtpTxInstance_t SrtpTxChanData120;
#pragma DATA_SECTION (SrtpTxChanData120, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData120, 128)

static SrtpTxInstance_t SrtpTxChanData121;
#pragma DATA_SECTION (SrtpTxChanData121, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData121, 128)

static SrtpTxInstance_t SrtpTxChanData122;
#pragma DATA_SECTION (SrtpTxChanData122, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData122, 128)

static SrtpTxInstance_t SrtpTxChanData123;
#pragma DATA_SECTION (SrtpTxChanData123, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData123, 128)

static SrtpTxInstance_t SrtpTxChanData124;
#pragma DATA_SECTION (SrtpTxChanData124, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData124, 128)

static SrtpTxInstance_t SrtpTxChanData125;
#pragma DATA_SECTION (SrtpTxChanData125, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData125, 128)

static SrtpTxInstance_t SrtpTxChanData126;
#pragma DATA_SECTION (SrtpTxChanData126, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData126, 128)

static SrtpTxInstance_t SrtpTxChanData127;
#pragma DATA_SECTION (SrtpTxChanData127, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData127, 128)

static SrtpTxInstance_t SrtpTxChanData128;
#pragma DATA_SECTION (SrtpTxChanData128, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData128, 128)

static SrtpTxInstance_t SrtpTxChanData129;
#pragma DATA_SECTION (SrtpTxChanData129, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData129, 128)

static SrtpTxInstance_t SrtpTxChanData130;
#pragma DATA_SECTION (SrtpTxChanData130, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData130, 128)

static SrtpTxInstance_t SrtpTxChanData131;
#pragma DATA_SECTION (SrtpTxChanData131, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData131, 128)

static SrtpTxInstance_t SrtpTxChanData132;
#pragma DATA_SECTION (SrtpTxChanData132, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData132, 128)

static SrtpTxInstance_t SrtpTxChanData133;
#pragma DATA_SECTION (SrtpTxChanData133, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData133, 128)

static SrtpTxInstance_t SrtpTxChanData134;
#pragma DATA_SECTION (SrtpTxChanData134, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData134, 128)

static SrtpTxInstance_t SrtpTxChanData135;
#pragma DATA_SECTION (SrtpTxChanData135, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData135, 128)

static SrtpTxInstance_t SrtpTxChanData136;
#pragma DATA_SECTION (SrtpTxChanData136, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData136, 128)

static SrtpTxInstance_t SrtpTxChanData137;
#pragma DATA_SECTION (SrtpTxChanData137, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData137, 128)

static SrtpTxInstance_t SrtpTxChanData138;
#pragma DATA_SECTION (SrtpTxChanData138, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData138, 128)

static SrtpTxInstance_t SrtpTxChanData139;
#pragma DATA_SECTION (SrtpTxChanData139, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData139, 128)

static SrtpTxInstance_t SrtpTxChanData140;
#pragma DATA_SECTION (SrtpTxChanData140, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData140, 128)

static SrtpTxInstance_t SrtpTxChanData141;
#pragma DATA_SECTION (SrtpTxChanData141, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData141, 128)

static SrtpTxInstance_t SrtpTxChanData142;
#pragma DATA_SECTION (SrtpTxChanData142, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData142, 128)

static SrtpTxInstance_t SrtpTxChanData143;
#pragma DATA_SECTION (SrtpTxChanData143, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData143, 128)

static SrtpTxInstance_t SrtpTxChanData144;
#pragma DATA_SECTION (SrtpTxChanData144, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData144, 128)

static SrtpTxInstance_t SrtpTxChanData145;
#pragma DATA_SECTION (SrtpTxChanData145, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData145, 128)

static SrtpTxInstance_t SrtpTxChanData146;
#pragma DATA_SECTION (SrtpTxChanData146, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData146, 128)

static SrtpTxInstance_t SrtpTxChanData147;
#pragma DATA_SECTION (SrtpTxChanData147, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData147, 128)

static SrtpTxInstance_t SrtpTxChanData148;
#pragma DATA_SECTION (SrtpTxChanData148, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData148, 128)

static SrtpTxInstance_t SrtpTxChanData149;
#pragma DATA_SECTION (SrtpTxChanData149, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData149, 128)

static SrtpTxInstance_t SrtpTxChanData150;
#pragma DATA_SECTION (SrtpTxChanData150, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData150, 128)

static SrtpTxInstance_t SrtpTxChanData151;
#pragma DATA_SECTION (SrtpTxChanData151, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData151, 128)

static SrtpTxInstance_t SrtpTxChanData152;
#pragma DATA_SECTION (SrtpTxChanData152, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData152, 128)

static SrtpTxInstance_t SrtpTxChanData153;
#pragma DATA_SECTION (SrtpTxChanData153, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData153, 128)

static SrtpTxInstance_t SrtpTxChanData154;
#pragma DATA_SECTION (SrtpTxChanData154, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData154, 128)

static SrtpTxInstance_t SrtpTxChanData155;
#pragma DATA_SECTION (SrtpTxChanData155, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData155, 128)

static SrtpTxInstance_t SrtpTxChanData156;
#pragma DATA_SECTION (SrtpTxChanData156, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData156, 128)

static SrtpTxInstance_t SrtpTxChanData157;
#pragma DATA_SECTION (SrtpTxChanData157, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData157, 128)

static SrtpTxInstance_t SrtpTxChanData158;
#pragma DATA_SECTION (SrtpTxChanData158, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData158, 128)

static SrtpTxInstance_t SrtpTxChanData159;
#pragma DATA_SECTION (SrtpTxChanData159, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpTxChanData159, 128)

SrtpTxInstance_t* const SrtpTxChanData[] = {
	&SrtpTxChanData0,
	&SrtpTxChanData1,
	&SrtpTxChanData2,
	&SrtpTxChanData3,
	&SrtpTxChanData4,
	&SrtpTxChanData5,
	&SrtpTxChanData6,
	&SrtpTxChanData7,
	&SrtpTxChanData8,
	&SrtpTxChanData9,
	&SrtpTxChanData10,
	&SrtpTxChanData11,
	&SrtpTxChanData12,
	&SrtpTxChanData13,
	&SrtpTxChanData14,
	&SrtpTxChanData15,
	&SrtpTxChanData16,
	&SrtpTxChanData17,
	&SrtpTxChanData18,
	&SrtpTxChanData19,
	&SrtpTxChanData20,
	&SrtpTxChanData21,
	&SrtpTxChanData22,
	&SrtpTxChanData23,
	&SrtpTxChanData24,
	&SrtpTxChanData25,
	&SrtpTxChanData26,
	&SrtpTxChanData27,
	&SrtpTxChanData28,
	&SrtpTxChanData29,
	&SrtpTxChanData30,
	&SrtpTxChanData31,
	&SrtpTxChanData32,
	&SrtpTxChanData33,
	&SrtpTxChanData34,
	&SrtpTxChanData35,
	&SrtpTxChanData36,
	&SrtpTxChanData37,
	&SrtpTxChanData38,
	&SrtpTxChanData39,
	&SrtpTxChanData40,
	&SrtpTxChanData41,
	&SrtpTxChanData42,
	&SrtpTxChanData43,
	&SrtpTxChanData44,
	&SrtpTxChanData45,
	&SrtpTxChanData46,
	&SrtpTxChanData47,
	&SrtpTxChanData48,
	&SrtpTxChanData49,
	&SrtpTxChanData50,
	&SrtpTxChanData51,
	&SrtpTxChanData52,
	&SrtpTxChanData53,
	&SrtpTxChanData54,
	&SrtpTxChanData55,
	&SrtpTxChanData56,
	&SrtpTxChanData57,
	&SrtpTxChanData58,
	&SrtpTxChanData59,
	&SrtpTxChanData60,
	&SrtpTxChanData61,
	&SrtpTxChanData62,
	&SrtpTxChanData63,
	&SrtpTxChanData64,
	&SrtpTxChanData65,
	&SrtpTxChanData66,
	&SrtpTxChanData67,
	&SrtpTxChanData68,
	&SrtpTxChanData69,
	&SrtpTxChanData70,
	&SrtpTxChanData71,
	&SrtpTxChanData72,
	&SrtpTxChanData73,
	&SrtpTxChanData74,
	&SrtpTxChanData75,
	&SrtpTxChanData76,
	&SrtpTxChanData77,
	&SrtpTxChanData78,
	&SrtpTxChanData79,
	&SrtpTxChanData80,
	&SrtpTxChanData81,
	&SrtpTxChanData82,
	&SrtpTxChanData83,
	&SrtpTxChanData84,
	&SrtpTxChanData85,
	&SrtpTxChanData86,
	&SrtpTxChanData87,
	&SrtpTxChanData88,
	&SrtpTxChanData89,
	&SrtpTxChanData90,
	&SrtpTxChanData91,
	&SrtpTxChanData92,
	&SrtpTxChanData93,
	&SrtpTxChanData94,
	&SrtpTxChanData95,
	&SrtpTxChanData96,
	&SrtpTxChanData97,
	&SrtpTxChanData98,
	&SrtpTxChanData99,
	&SrtpTxChanData100,
	&SrtpTxChanData101,
	&SrtpTxChanData102,
	&SrtpTxChanData103,
	&SrtpTxChanData104,
	&SrtpTxChanData105,
	&SrtpTxChanData106,
	&SrtpTxChanData107,
	&SrtpTxChanData108,
	&SrtpTxChanData109,
	&SrtpTxChanData110,
	&SrtpTxChanData111,
	&SrtpTxChanData112,
	&SrtpTxChanData113,
	&SrtpTxChanData114,
	&SrtpTxChanData115,
	&SrtpTxChanData116,
	&SrtpTxChanData117,
	&SrtpTxChanData118,
	&SrtpTxChanData119,
	&SrtpTxChanData120,
	&SrtpTxChanData121,
	&SrtpTxChanData122,
	&SrtpTxChanData123,
	&SrtpTxChanData124,
	&SrtpTxChanData125,
	&SrtpTxChanData126,
	&SrtpTxChanData127,
	&SrtpTxChanData128,
	&SrtpTxChanData129,
	&SrtpTxChanData130,
	&SrtpTxChanData131,
	&SrtpTxChanData132,
	&SrtpTxChanData133,
	&SrtpTxChanData134,
	&SrtpTxChanData135,
	&SrtpTxChanData136,
	&SrtpTxChanData137,
	&SrtpTxChanData138,
	&SrtpTxChanData139,
	&SrtpTxChanData140,
	&SrtpTxChanData141,
	&SrtpTxChanData142,
	&SrtpTxChanData143,
	&SrtpTxChanData144,
	&SrtpTxChanData145,
	&SrtpTxChanData146,
	&SrtpTxChanData147,
	&SrtpTxChanData148,
	&SrtpTxChanData149,
	&SrtpTxChanData150,
	&SrtpTxChanData151,
	&SrtpTxChanData152,
	&SrtpTxChanData153,
	&SrtpTxChanData154,
	&SrtpTxChanData155,
	&SrtpTxChanData156,
	&SrtpTxChanData157,
	&SrtpTxChanData158,
	&SrtpTxChanData159
};
//}

//{      GpakSrtpKeyInfo_t        SrtpTxKeyData STRUCTURES to CHAN_INST_DATA:SRTP_KEY

GpakSrtpKeyInfo_t SrtpTxKeyData0;
#pragma DATA_SECTION (SrtpTxKeyData0, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData0, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData1;
#pragma DATA_SECTION (SrtpTxKeyData1, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData1, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData2;
#pragma DATA_SECTION (SrtpTxKeyData2, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData2, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData3;
#pragma DATA_SECTION (SrtpTxKeyData3, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData3, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData4;
#pragma DATA_SECTION (SrtpTxKeyData4, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData4, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData5;
#pragma DATA_SECTION (SrtpTxKeyData5, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData5, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData6;
#pragma DATA_SECTION (SrtpTxKeyData6, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData6, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData7;
#pragma DATA_SECTION (SrtpTxKeyData7, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData7, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData8;
#pragma DATA_SECTION (SrtpTxKeyData8, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData8, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData9;
#pragma DATA_SECTION (SrtpTxKeyData9, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData9, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData10;
#pragma DATA_SECTION (SrtpTxKeyData10, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData10, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData11;
#pragma DATA_SECTION (SrtpTxKeyData11, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData11, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData12;
#pragma DATA_SECTION (SrtpTxKeyData12, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData12, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData13;
#pragma DATA_SECTION (SrtpTxKeyData13, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData13, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData14;
#pragma DATA_SECTION (SrtpTxKeyData14, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData14, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData15;
#pragma DATA_SECTION (SrtpTxKeyData15, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData15, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData16;
#pragma DATA_SECTION (SrtpTxKeyData16, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData16, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData17;
#pragma DATA_SECTION (SrtpTxKeyData17, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData17, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData18;
#pragma DATA_SECTION (SrtpTxKeyData18, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData18, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData19;
#pragma DATA_SECTION (SrtpTxKeyData19, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData19, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData20;
#pragma DATA_SECTION (SrtpTxKeyData20, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData20, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData21;
#pragma DATA_SECTION (SrtpTxKeyData21, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData21, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData22;
#pragma DATA_SECTION (SrtpTxKeyData22, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData22, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData23;
#pragma DATA_SECTION (SrtpTxKeyData23, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData23, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData24;
#pragma DATA_SECTION (SrtpTxKeyData24, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData24, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData25;
#pragma DATA_SECTION (SrtpTxKeyData25, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData25, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData26;
#pragma DATA_SECTION (SrtpTxKeyData26, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData26, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData27;
#pragma DATA_SECTION (SrtpTxKeyData27, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData27, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData28;
#pragma DATA_SECTION (SrtpTxKeyData28, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData28, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData29;
#pragma DATA_SECTION (SrtpTxKeyData29, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData29, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData30;
#pragma DATA_SECTION (SrtpTxKeyData30, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData30, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData31;
#pragma DATA_SECTION (SrtpTxKeyData31, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData31, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData32;
#pragma DATA_SECTION (SrtpTxKeyData32, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData32, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData33;
#pragma DATA_SECTION (SrtpTxKeyData33, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData33, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData34;
#pragma DATA_SECTION (SrtpTxKeyData34, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData34, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData35;
#pragma DATA_SECTION (SrtpTxKeyData35, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData35, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData36;
#pragma DATA_SECTION (SrtpTxKeyData36, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData36, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData37;
#pragma DATA_SECTION (SrtpTxKeyData37, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData37, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData38;
#pragma DATA_SECTION (SrtpTxKeyData38, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData38, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData39;
#pragma DATA_SECTION (SrtpTxKeyData39, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData39, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData40;
#pragma DATA_SECTION (SrtpTxKeyData40, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData40, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData41;
#pragma DATA_SECTION (SrtpTxKeyData41, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData41, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData42;
#pragma DATA_SECTION (SrtpTxKeyData42, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData42, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData43;
#pragma DATA_SECTION (SrtpTxKeyData43, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData43, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData44;
#pragma DATA_SECTION (SrtpTxKeyData44, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData44, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData45;
#pragma DATA_SECTION (SrtpTxKeyData45, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData45, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData46;
#pragma DATA_SECTION (SrtpTxKeyData46, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData46, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData47;
#pragma DATA_SECTION (SrtpTxKeyData47, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData47, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData48;
#pragma DATA_SECTION (SrtpTxKeyData48, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData48, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData49;
#pragma DATA_SECTION (SrtpTxKeyData49, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData49, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData50;
#pragma DATA_SECTION (SrtpTxKeyData50, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData50, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData51;
#pragma DATA_SECTION (SrtpTxKeyData51, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData51, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData52;
#pragma DATA_SECTION (SrtpTxKeyData52, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData52, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData53;
#pragma DATA_SECTION (SrtpTxKeyData53, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData53, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData54;
#pragma DATA_SECTION (SrtpTxKeyData54, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData54, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData55;
#pragma DATA_SECTION (SrtpTxKeyData55, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData55, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData56;
#pragma DATA_SECTION (SrtpTxKeyData56, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData56, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData57;
#pragma DATA_SECTION (SrtpTxKeyData57, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData57, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData58;
#pragma DATA_SECTION (SrtpTxKeyData58, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData58, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData59;
#pragma DATA_SECTION (SrtpTxKeyData59, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData59, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData60;
#pragma DATA_SECTION (SrtpTxKeyData60, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData60, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData61;
#pragma DATA_SECTION (SrtpTxKeyData61, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData61, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData62;
#pragma DATA_SECTION (SrtpTxKeyData62, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData62, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData63;
#pragma DATA_SECTION (SrtpTxKeyData63, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData63, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData64;
#pragma DATA_SECTION (SrtpTxKeyData64, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData64, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData65;
#pragma DATA_SECTION (SrtpTxKeyData65, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData65, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData66;
#pragma DATA_SECTION (SrtpTxKeyData66, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData66, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData67;
#pragma DATA_SECTION (SrtpTxKeyData67, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData67, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData68;
#pragma DATA_SECTION (SrtpTxKeyData68, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData68, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData69;
#pragma DATA_SECTION (SrtpTxKeyData69, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData69, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData70;
#pragma DATA_SECTION (SrtpTxKeyData70, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData70, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData71;
#pragma DATA_SECTION (SrtpTxKeyData71, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData71, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData72;
#pragma DATA_SECTION (SrtpTxKeyData72, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData72, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData73;
#pragma DATA_SECTION (SrtpTxKeyData73, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData73, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData74;
#pragma DATA_SECTION (SrtpTxKeyData74, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData74, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData75;
#pragma DATA_SECTION (SrtpTxKeyData75, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData75, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData76;
#pragma DATA_SECTION (SrtpTxKeyData76, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData76, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData77;
#pragma DATA_SECTION (SrtpTxKeyData77, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData77, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData78;
#pragma DATA_SECTION (SrtpTxKeyData78, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData78, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData79;
#pragma DATA_SECTION (SrtpTxKeyData79, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData79, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData80;
#pragma DATA_SECTION (SrtpTxKeyData80, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData80, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData81;
#pragma DATA_SECTION (SrtpTxKeyData81, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData81, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData82;
#pragma DATA_SECTION (SrtpTxKeyData82, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData82, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData83;
#pragma DATA_SECTION (SrtpTxKeyData83, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData83, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData84;
#pragma DATA_SECTION (SrtpTxKeyData84, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData84, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData85;
#pragma DATA_SECTION (SrtpTxKeyData85, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData85, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData86;
#pragma DATA_SECTION (SrtpTxKeyData86, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData86, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData87;
#pragma DATA_SECTION (SrtpTxKeyData87, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData87, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData88;
#pragma DATA_SECTION (SrtpTxKeyData88, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData88, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData89;
#pragma DATA_SECTION (SrtpTxKeyData89, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData89, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData90;
#pragma DATA_SECTION (SrtpTxKeyData90, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData90, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData91;
#pragma DATA_SECTION (SrtpTxKeyData91, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData91, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData92;
#pragma DATA_SECTION (SrtpTxKeyData92, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData92, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData93;
#pragma DATA_SECTION (SrtpTxKeyData93, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData93, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData94;
#pragma DATA_SECTION (SrtpTxKeyData94, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData94, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData95;
#pragma DATA_SECTION (SrtpTxKeyData95, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData95, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData96;
#pragma DATA_SECTION (SrtpTxKeyData96, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData96, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData97;
#pragma DATA_SECTION (SrtpTxKeyData97, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData97, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData98;
#pragma DATA_SECTION (SrtpTxKeyData98, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData98, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData99;
#pragma DATA_SECTION (SrtpTxKeyData99, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData99, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData100;
#pragma DATA_SECTION (SrtpTxKeyData100, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData100, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData101;
#pragma DATA_SECTION (SrtpTxKeyData101, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData101, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData102;
#pragma DATA_SECTION (SrtpTxKeyData102, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData102, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData103;
#pragma DATA_SECTION (SrtpTxKeyData103, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData103, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData104;
#pragma DATA_SECTION (SrtpTxKeyData104, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData104, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData105;
#pragma DATA_SECTION (SrtpTxKeyData105, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData105, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData106;
#pragma DATA_SECTION (SrtpTxKeyData106, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData106, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData107;
#pragma DATA_SECTION (SrtpTxKeyData107, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData107, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData108;
#pragma DATA_SECTION (SrtpTxKeyData108, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData108, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData109;
#pragma DATA_SECTION (SrtpTxKeyData109, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData109, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData110;
#pragma DATA_SECTION (SrtpTxKeyData110, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData110, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData111;
#pragma DATA_SECTION (SrtpTxKeyData111, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData111, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData112;
#pragma DATA_SECTION (SrtpTxKeyData112, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData112, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData113;
#pragma DATA_SECTION (SrtpTxKeyData113, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData113, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData114;
#pragma DATA_SECTION (SrtpTxKeyData114, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData114, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData115;
#pragma DATA_SECTION (SrtpTxKeyData115, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData115, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData116;
#pragma DATA_SECTION (SrtpTxKeyData116, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData116, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData117;
#pragma DATA_SECTION (SrtpTxKeyData117, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData117, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData118;
#pragma DATA_SECTION (SrtpTxKeyData118, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData118, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData119;
#pragma DATA_SECTION (SrtpTxKeyData119, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData119, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData120;
#pragma DATA_SECTION (SrtpTxKeyData120, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData120, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData121;
#pragma DATA_SECTION (SrtpTxKeyData121, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData121, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData122;
#pragma DATA_SECTION (SrtpTxKeyData122, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData122, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData123;
#pragma DATA_SECTION (SrtpTxKeyData123, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData123, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData124;
#pragma DATA_SECTION (SrtpTxKeyData124, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData124, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData125;
#pragma DATA_SECTION (SrtpTxKeyData125, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData125, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData126;
#pragma DATA_SECTION (SrtpTxKeyData126, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData126, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData127;
#pragma DATA_SECTION (SrtpTxKeyData127, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData127, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData128;
#pragma DATA_SECTION (SrtpTxKeyData128, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData128, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData129;
#pragma DATA_SECTION (SrtpTxKeyData129, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData129, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData130;
#pragma DATA_SECTION (SrtpTxKeyData130, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData130, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData131;
#pragma DATA_SECTION (SrtpTxKeyData131, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData131, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData132;
#pragma DATA_SECTION (SrtpTxKeyData132, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData132, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData133;
#pragma DATA_SECTION (SrtpTxKeyData133, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData133, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData134;
#pragma DATA_SECTION (SrtpTxKeyData134, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData134, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData135;
#pragma DATA_SECTION (SrtpTxKeyData135, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData135, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData136;
#pragma DATA_SECTION (SrtpTxKeyData136, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData136, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData137;
#pragma DATA_SECTION (SrtpTxKeyData137, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData137, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData138;
#pragma DATA_SECTION (SrtpTxKeyData138, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData138, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData139;
#pragma DATA_SECTION (SrtpTxKeyData139, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData139, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData140;
#pragma DATA_SECTION (SrtpTxKeyData140, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData140, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData141;
#pragma DATA_SECTION (SrtpTxKeyData141, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData141, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData142;
#pragma DATA_SECTION (SrtpTxKeyData142, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData142, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData143;
#pragma DATA_SECTION (SrtpTxKeyData143, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData143, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData144;
#pragma DATA_SECTION (SrtpTxKeyData144, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData144, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData145;
#pragma DATA_SECTION (SrtpTxKeyData145, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData145, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData146;
#pragma DATA_SECTION (SrtpTxKeyData146, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData146, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData147;
#pragma DATA_SECTION (SrtpTxKeyData147, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData147, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData148;
#pragma DATA_SECTION (SrtpTxKeyData148, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData148, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData149;
#pragma DATA_SECTION (SrtpTxKeyData149, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData149, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData150;
#pragma DATA_SECTION (SrtpTxKeyData150, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData150, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData151;
#pragma DATA_SECTION (SrtpTxKeyData151, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData151, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData152;
#pragma DATA_SECTION (SrtpTxKeyData152, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData152, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData153;
#pragma DATA_SECTION (SrtpTxKeyData153, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData153, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData154;
#pragma DATA_SECTION (SrtpTxKeyData154, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData154, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData155;
#pragma DATA_SECTION (SrtpTxKeyData155, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData155, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData156;
#pragma DATA_SECTION (SrtpTxKeyData156, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData156, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData157;
#pragma DATA_SECTION (SrtpTxKeyData157, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData157, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData158;
#pragma DATA_SECTION (SrtpTxKeyData158, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData158, 128)

static GpakSrtpKeyInfo_t SrtpTxKeyData159;
#pragma DATA_SECTION (SrtpTxKeyData159, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpTxKeyData159, 128)

GpakSrtpKeyInfo_t* const SrtpTxKeyData[] = {
	&SrtpTxKeyData0,
	&SrtpTxKeyData1,
	&SrtpTxKeyData2,
	&SrtpTxKeyData3,
	&SrtpTxKeyData4,
	&SrtpTxKeyData5,
	&SrtpTxKeyData6,
	&SrtpTxKeyData7,
	&SrtpTxKeyData8,
	&SrtpTxKeyData9,
	&SrtpTxKeyData10,
	&SrtpTxKeyData11,
	&SrtpTxKeyData12,
	&SrtpTxKeyData13,
	&SrtpTxKeyData14,
	&SrtpTxKeyData15,
	&SrtpTxKeyData16,
	&SrtpTxKeyData17,
	&SrtpTxKeyData18,
	&SrtpTxKeyData19,
	&SrtpTxKeyData20,
	&SrtpTxKeyData21,
	&SrtpTxKeyData22,
	&SrtpTxKeyData23,
	&SrtpTxKeyData24,
	&SrtpTxKeyData25,
	&SrtpTxKeyData26,
	&SrtpTxKeyData27,
	&SrtpTxKeyData28,
	&SrtpTxKeyData29,
	&SrtpTxKeyData30,
	&SrtpTxKeyData31,
	&SrtpTxKeyData32,
	&SrtpTxKeyData33,
	&SrtpTxKeyData34,
	&SrtpTxKeyData35,
	&SrtpTxKeyData36,
	&SrtpTxKeyData37,
	&SrtpTxKeyData38,
	&SrtpTxKeyData39,
	&SrtpTxKeyData40,
	&SrtpTxKeyData41,
	&SrtpTxKeyData42,
	&SrtpTxKeyData43,
	&SrtpTxKeyData44,
	&SrtpTxKeyData45,
	&SrtpTxKeyData46,
	&SrtpTxKeyData47,
	&SrtpTxKeyData48,
	&SrtpTxKeyData49,
	&SrtpTxKeyData50,
	&SrtpTxKeyData51,
	&SrtpTxKeyData52,
	&SrtpTxKeyData53,
	&SrtpTxKeyData54,
	&SrtpTxKeyData55,
	&SrtpTxKeyData56,
	&SrtpTxKeyData57,
	&SrtpTxKeyData58,
	&SrtpTxKeyData59,
	&SrtpTxKeyData60,
	&SrtpTxKeyData61,
	&SrtpTxKeyData62,
	&SrtpTxKeyData63,
	&SrtpTxKeyData64,
	&SrtpTxKeyData65,
	&SrtpTxKeyData66,
	&SrtpTxKeyData67,
	&SrtpTxKeyData68,
	&SrtpTxKeyData69,
	&SrtpTxKeyData70,
	&SrtpTxKeyData71,
	&SrtpTxKeyData72,
	&SrtpTxKeyData73,
	&SrtpTxKeyData74,
	&SrtpTxKeyData75,
	&SrtpTxKeyData76,
	&SrtpTxKeyData77,
	&SrtpTxKeyData78,
	&SrtpTxKeyData79,
	&SrtpTxKeyData80,
	&SrtpTxKeyData81,
	&SrtpTxKeyData82,
	&SrtpTxKeyData83,
	&SrtpTxKeyData84,
	&SrtpTxKeyData85,
	&SrtpTxKeyData86,
	&SrtpTxKeyData87,
	&SrtpTxKeyData88,
	&SrtpTxKeyData89,
	&SrtpTxKeyData90,
	&SrtpTxKeyData91,
	&SrtpTxKeyData92,
	&SrtpTxKeyData93,
	&SrtpTxKeyData94,
	&SrtpTxKeyData95,
	&SrtpTxKeyData96,
	&SrtpTxKeyData97,
	&SrtpTxKeyData98,
	&SrtpTxKeyData99,
	&SrtpTxKeyData100,
	&SrtpTxKeyData101,
	&SrtpTxKeyData102,
	&SrtpTxKeyData103,
	&SrtpTxKeyData104,
	&SrtpTxKeyData105,
	&SrtpTxKeyData106,
	&SrtpTxKeyData107,
	&SrtpTxKeyData108,
	&SrtpTxKeyData109,
	&SrtpTxKeyData110,
	&SrtpTxKeyData111,
	&SrtpTxKeyData112,
	&SrtpTxKeyData113,
	&SrtpTxKeyData114,
	&SrtpTxKeyData115,
	&SrtpTxKeyData116,
	&SrtpTxKeyData117,
	&SrtpTxKeyData118,
	&SrtpTxKeyData119,
	&SrtpTxKeyData120,
	&SrtpTxKeyData121,
	&SrtpTxKeyData122,
	&SrtpTxKeyData123,
	&SrtpTxKeyData124,
	&SrtpTxKeyData125,
	&SrtpTxKeyData126,
	&SrtpTxKeyData127,
	&SrtpTxKeyData128,
	&SrtpTxKeyData129,
	&SrtpTxKeyData130,
	&SrtpTxKeyData131,
	&SrtpTxKeyData132,
	&SrtpTxKeyData133,
	&SrtpTxKeyData134,
	&SrtpTxKeyData135,
	&SrtpTxKeyData136,
	&SrtpTxKeyData137,
	&SrtpTxKeyData138,
	&SrtpTxKeyData139,
	&SrtpTxKeyData140,
	&SrtpTxKeyData141,
	&SrtpTxKeyData142,
	&SrtpTxKeyData143,
	&SrtpTxKeyData144,
	&SrtpTxKeyData145,
	&SrtpTxKeyData146,
	&SrtpTxKeyData147,
	&SrtpTxKeyData148,
	&SrtpTxKeyData149,
	&SrtpTxKeyData150,
	&SrtpTxKeyData151,
	&SrtpTxKeyData152,
	&SrtpTxKeyData153,
	&SrtpTxKeyData154,
	&SrtpTxKeyData155,
	&SrtpTxKeyData156,
	&SrtpTxKeyData157,
	&SrtpTxKeyData158,
	&SrtpTxKeyData159
};
//}

const int srtpRxI8 = (SRTPRX_SIZE_I4 * 4);
//{       SrtpRxInstance_t       SrtpRxChanData STRUCTURES to CHAN_INST_DATA:SRTP

SrtpRxInstance_t SrtpRxChanData0;
#pragma DATA_SECTION (SrtpRxChanData0, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData0, 128)

static SrtpRxInstance_t SrtpRxChanData1;
#pragma DATA_SECTION (SrtpRxChanData1, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData1, 128)

static SrtpRxInstance_t SrtpRxChanData2;
#pragma DATA_SECTION (SrtpRxChanData2, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData2, 128)

static SrtpRxInstance_t SrtpRxChanData3;
#pragma DATA_SECTION (SrtpRxChanData3, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData3, 128)

static SrtpRxInstance_t SrtpRxChanData4;
#pragma DATA_SECTION (SrtpRxChanData4, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData4, 128)

static SrtpRxInstance_t SrtpRxChanData5;
#pragma DATA_SECTION (SrtpRxChanData5, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData5, 128)

static SrtpRxInstance_t SrtpRxChanData6;
#pragma DATA_SECTION (SrtpRxChanData6, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData6, 128)

static SrtpRxInstance_t SrtpRxChanData7;
#pragma DATA_SECTION (SrtpRxChanData7, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData7, 128)

static SrtpRxInstance_t SrtpRxChanData8;
#pragma DATA_SECTION (SrtpRxChanData8, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData8, 128)

static SrtpRxInstance_t SrtpRxChanData9;
#pragma DATA_SECTION (SrtpRxChanData9, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData9, 128)

static SrtpRxInstance_t SrtpRxChanData10;
#pragma DATA_SECTION (SrtpRxChanData10, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData10, 128)

static SrtpRxInstance_t SrtpRxChanData11;
#pragma DATA_SECTION (SrtpRxChanData11, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData11, 128)

static SrtpRxInstance_t SrtpRxChanData12;
#pragma DATA_SECTION (SrtpRxChanData12, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData12, 128)

static SrtpRxInstance_t SrtpRxChanData13;
#pragma DATA_SECTION (SrtpRxChanData13, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData13, 128)

static SrtpRxInstance_t SrtpRxChanData14;
#pragma DATA_SECTION (SrtpRxChanData14, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData14, 128)

static SrtpRxInstance_t SrtpRxChanData15;
#pragma DATA_SECTION (SrtpRxChanData15, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData15, 128)

static SrtpRxInstance_t SrtpRxChanData16;
#pragma DATA_SECTION (SrtpRxChanData16, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData16, 128)

static SrtpRxInstance_t SrtpRxChanData17;
#pragma DATA_SECTION (SrtpRxChanData17, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData17, 128)

static SrtpRxInstance_t SrtpRxChanData18;
#pragma DATA_SECTION (SrtpRxChanData18, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData18, 128)

static SrtpRxInstance_t SrtpRxChanData19;
#pragma DATA_SECTION (SrtpRxChanData19, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData19, 128)

static SrtpRxInstance_t SrtpRxChanData20;
#pragma DATA_SECTION (SrtpRxChanData20, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData20, 128)

static SrtpRxInstance_t SrtpRxChanData21;
#pragma DATA_SECTION (SrtpRxChanData21, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData21, 128)

static SrtpRxInstance_t SrtpRxChanData22;
#pragma DATA_SECTION (SrtpRxChanData22, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData22, 128)

static SrtpRxInstance_t SrtpRxChanData23;
#pragma DATA_SECTION (SrtpRxChanData23, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData23, 128)

static SrtpRxInstance_t SrtpRxChanData24;
#pragma DATA_SECTION (SrtpRxChanData24, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData24, 128)

static SrtpRxInstance_t SrtpRxChanData25;
#pragma DATA_SECTION (SrtpRxChanData25, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData25, 128)

static SrtpRxInstance_t SrtpRxChanData26;
#pragma DATA_SECTION (SrtpRxChanData26, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData26, 128)

static SrtpRxInstance_t SrtpRxChanData27;
#pragma DATA_SECTION (SrtpRxChanData27, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData27, 128)

static SrtpRxInstance_t SrtpRxChanData28;
#pragma DATA_SECTION (SrtpRxChanData28, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData28, 128)

static SrtpRxInstance_t SrtpRxChanData29;
#pragma DATA_SECTION (SrtpRxChanData29, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData29, 128)

static SrtpRxInstance_t SrtpRxChanData30;
#pragma DATA_SECTION (SrtpRxChanData30, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData30, 128)

static SrtpRxInstance_t SrtpRxChanData31;
#pragma DATA_SECTION (SrtpRxChanData31, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData31, 128)

static SrtpRxInstance_t SrtpRxChanData32;
#pragma DATA_SECTION (SrtpRxChanData32, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData32, 128)

static SrtpRxInstance_t SrtpRxChanData33;
#pragma DATA_SECTION (SrtpRxChanData33, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData33, 128)

static SrtpRxInstance_t SrtpRxChanData34;
#pragma DATA_SECTION (SrtpRxChanData34, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData34, 128)

static SrtpRxInstance_t SrtpRxChanData35;
#pragma DATA_SECTION (SrtpRxChanData35, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData35, 128)

static SrtpRxInstance_t SrtpRxChanData36;
#pragma DATA_SECTION (SrtpRxChanData36, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData36, 128)

static SrtpRxInstance_t SrtpRxChanData37;
#pragma DATA_SECTION (SrtpRxChanData37, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData37, 128)

static SrtpRxInstance_t SrtpRxChanData38;
#pragma DATA_SECTION (SrtpRxChanData38, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData38, 128)

static SrtpRxInstance_t SrtpRxChanData39;
#pragma DATA_SECTION (SrtpRxChanData39, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData39, 128)

static SrtpRxInstance_t SrtpRxChanData40;
#pragma DATA_SECTION (SrtpRxChanData40, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData40, 128)

static SrtpRxInstance_t SrtpRxChanData41;
#pragma DATA_SECTION (SrtpRxChanData41, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData41, 128)

static SrtpRxInstance_t SrtpRxChanData42;
#pragma DATA_SECTION (SrtpRxChanData42, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData42, 128)

static SrtpRxInstance_t SrtpRxChanData43;
#pragma DATA_SECTION (SrtpRxChanData43, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData43, 128)

static SrtpRxInstance_t SrtpRxChanData44;
#pragma DATA_SECTION (SrtpRxChanData44, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData44, 128)

static SrtpRxInstance_t SrtpRxChanData45;
#pragma DATA_SECTION (SrtpRxChanData45, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData45, 128)

static SrtpRxInstance_t SrtpRxChanData46;
#pragma DATA_SECTION (SrtpRxChanData46, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData46, 128)

static SrtpRxInstance_t SrtpRxChanData47;
#pragma DATA_SECTION (SrtpRxChanData47, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData47, 128)

static SrtpRxInstance_t SrtpRxChanData48;
#pragma DATA_SECTION (SrtpRxChanData48, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData48, 128)

static SrtpRxInstance_t SrtpRxChanData49;
#pragma DATA_SECTION (SrtpRxChanData49, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData49, 128)

static SrtpRxInstance_t SrtpRxChanData50;
#pragma DATA_SECTION (SrtpRxChanData50, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData50, 128)

static SrtpRxInstance_t SrtpRxChanData51;
#pragma DATA_SECTION (SrtpRxChanData51, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData51, 128)

static SrtpRxInstance_t SrtpRxChanData52;
#pragma DATA_SECTION (SrtpRxChanData52, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData52, 128)

static SrtpRxInstance_t SrtpRxChanData53;
#pragma DATA_SECTION (SrtpRxChanData53, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData53, 128)

static SrtpRxInstance_t SrtpRxChanData54;
#pragma DATA_SECTION (SrtpRxChanData54, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData54, 128)

static SrtpRxInstance_t SrtpRxChanData55;
#pragma DATA_SECTION (SrtpRxChanData55, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData55, 128)

static SrtpRxInstance_t SrtpRxChanData56;
#pragma DATA_SECTION (SrtpRxChanData56, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData56, 128)

static SrtpRxInstance_t SrtpRxChanData57;
#pragma DATA_SECTION (SrtpRxChanData57, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData57, 128)

static SrtpRxInstance_t SrtpRxChanData58;
#pragma DATA_SECTION (SrtpRxChanData58, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData58, 128)

static SrtpRxInstance_t SrtpRxChanData59;
#pragma DATA_SECTION (SrtpRxChanData59, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData59, 128)

static SrtpRxInstance_t SrtpRxChanData60;
#pragma DATA_SECTION (SrtpRxChanData60, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData60, 128)

static SrtpRxInstance_t SrtpRxChanData61;
#pragma DATA_SECTION (SrtpRxChanData61, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData61, 128)

static SrtpRxInstance_t SrtpRxChanData62;
#pragma DATA_SECTION (SrtpRxChanData62, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData62, 128)

static SrtpRxInstance_t SrtpRxChanData63;
#pragma DATA_SECTION (SrtpRxChanData63, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData63, 128)

static SrtpRxInstance_t SrtpRxChanData64;
#pragma DATA_SECTION (SrtpRxChanData64, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData64, 128)

static SrtpRxInstance_t SrtpRxChanData65;
#pragma DATA_SECTION (SrtpRxChanData65, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData65, 128)

static SrtpRxInstance_t SrtpRxChanData66;
#pragma DATA_SECTION (SrtpRxChanData66, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData66, 128)

static SrtpRxInstance_t SrtpRxChanData67;
#pragma DATA_SECTION (SrtpRxChanData67, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData67, 128)

static SrtpRxInstance_t SrtpRxChanData68;
#pragma DATA_SECTION (SrtpRxChanData68, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData68, 128)

static SrtpRxInstance_t SrtpRxChanData69;
#pragma DATA_SECTION (SrtpRxChanData69, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData69, 128)

static SrtpRxInstance_t SrtpRxChanData70;
#pragma DATA_SECTION (SrtpRxChanData70, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData70, 128)

static SrtpRxInstance_t SrtpRxChanData71;
#pragma DATA_SECTION (SrtpRxChanData71, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData71, 128)

static SrtpRxInstance_t SrtpRxChanData72;
#pragma DATA_SECTION (SrtpRxChanData72, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData72, 128)

static SrtpRxInstance_t SrtpRxChanData73;
#pragma DATA_SECTION (SrtpRxChanData73, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData73, 128)

static SrtpRxInstance_t SrtpRxChanData74;
#pragma DATA_SECTION (SrtpRxChanData74, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData74, 128)

static SrtpRxInstance_t SrtpRxChanData75;
#pragma DATA_SECTION (SrtpRxChanData75, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData75, 128)

static SrtpRxInstance_t SrtpRxChanData76;
#pragma DATA_SECTION (SrtpRxChanData76, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData76, 128)

static SrtpRxInstance_t SrtpRxChanData77;
#pragma DATA_SECTION (SrtpRxChanData77, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData77, 128)

static SrtpRxInstance_t SrtpRxChanData78;
#pragma DATA_SECTION (SrtpRxChanData78, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData78, 128)

static SrtpRxInstance_t SrtpRxChanData79;
#pragma DATA_SECTION (SrtpRxChanData79, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData79, 128)

static SrtpRxInstance_t SrtpRxChanData80;
#pragma DATA_SECTION (SrtpRxChanData80, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData80, 128)

static SrtpRxInstance_t SrtpRxChanData81;
#pragma DATA_SECTION (SrtpRxChanData81, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData81, 128)

static SrtpRxInstance_t SrtpRxChanData82;
#pragma DATA_SECTION (SrtpRxChanData82, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData82, 128)

static SrtpRxInstance_t SrtpRxChanData83;
#pragma DATA_SECTION (SrtpRxChanData83, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData83, 128)

static SrtpRxInstance_t SrtpRxChanData84;
#pragma DATA_SECTION (SrtpRxChanData84, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData84, 128)

static SrtpRxInstance_t SrtpRxChanData85;
#pragma DATA_SECTION (SrtpRxChanData85, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData85, 128)

static SrtpRxInstance_t SrtpRxChanData86;
#pragma DATA_SECTION (SrtpRxChanData86, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData86, 128)

static SrtpRxInstance_t SrtpRxChanData87;
#pragma DATA_SECTION (SrtpRxChanData87, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData87, 128)

static SrtpRxInstance_t SrtpRxChanData88;
#pragma DATA_SECTION (SrtpRxChanData88, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData88, 128)

static SrtpRxInstance_t SrtpRxChanData89;
#pragma DATA_SECTION (SrtpRxChanData89, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData89, 128)

static SrtpRxInstance_t SrtpRxChanData90;
#pragma DATA_SECTION (SrtpRxChanData90, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData90, 128)

static SrtpRxInstance_t SrtpRxChanData91;
#pragma DATA_SECTION (SrtpRxChanData91, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData91, 128)

static SrtpRxInstance_t SrtpRxChanData92;
#pragma DATA_SECTION (SrtpRxChanData92, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData92, 128)

static SrtpRxInstance_t SrtpRxChanData93;
#pragma DATA_SECTION (SrtpRxChanData93, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData93, 128)

static SrtpRxInstance_t SrtpRxChanData94;
#pragma DATA_SECTION (SrtpRxChanData94, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData94, 128)

static SrtpRxInstance_t SrtpRxChanData95;
#pragma DATA_SECTION (SrtpRxChanData95, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData95, 128)

static SrtpRxInstance_t SrtpRxChanData96;
#pragma DATA_SECTION (SrtpRxChanData96, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData96, 128)

static SrtpRxInstance_t SrtpRxChanData97;
#pragma DATA_SECTION (SrtpRxChanData97, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData97, 128)

static SrtpRxInstance_t SrtpRxChanData98;
#pragma DATA_SECTION (SrtpRxChanData98, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData98, 128)

static SrtpRxInstance_t SrtpRxChanData99;
#pragma DATA_SECTION (SrtpRxChanData99, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData99, 128)

static SrtpRxInstance_t SrtpRxChanData100;
#pragma DATA_SECTION (SrtpRxChanData100, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData100, 128)

static SrtpRxInstance_t SrtpRxChanData101;
#pragma DATA_SECTION (SrtpRxChanData101, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData101, 128)

static SrtpRxInstance_t SrtpRxChanData102;
#pragma DATA_SECTION (SrtpRxChanData102, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData102, 128)

static SrtpRxInstance_t SrtpRxChanData103;
#pragma DATA_SECTION (SrtpRxChanData103, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData103, 128)

static SrtpRxInstance_t SrtpRxChanData104;
#pragma DATA_SECTION (SrtpRxChanData104, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData104, 128)

static SrtpRxInstance_t SrtpRxChanData105;
#pragma DATA_SECTION (SrtpRxChanData105, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData105, 128)

static SrtpRxInstance_t SrtpRxChanData106;
#pragma DATA_SECTION (SrtpRxChanData106, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData106, 128)

static SrtpRxInstance_t SrtpRxChanData107;
#pragma DATA_SECTION (SrtpRxChanData107, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData107, 128)

static SrtpRxInstance_t SrtpRxChanData108;
#pragma DATA_SECTION (SrtpRxChanData108, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData108, 128)

static SrtpRxInstance_t SrtpRxChanData109;
#pragma DATA_SECTION (SrtpRxChanData109, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData109, 128)

static SrtpRxInstance_t SrtpRxChanData110;
#pragma DATA_SECTION (SrtpRxChanData110, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData110, 128)

static SrtpRxInstance_t SrtpRxChanData111;
#pragma DATA_SECTION (SrtpRxChanData111, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData111, 128)

static SrtpRxInstance_t SrtpRxChanData112;
#pragma DATA_SECTION (SrtpRxChanData112, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData112, 128)

static SrtpRxInstance_t SrtpRxChanData113;
#pragma DATA_SECTION (SrtpRxChanData113, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData113, 128)

static SrtpRxInstance_t SrtpRxChanData114;
#pragma DATA_SECTION (SrtpRxChanData114, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData114, 128)

static SrtpRxInstance_t SrtpRxChanData115;
#pragma DATA_SECTION (SrtpRxChanData115, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData115, 128)

static SrtpRxInstance_t SrtpRxChanData116;
#pragma DATA_SECTION (SrtpRxChanData116, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData116, 128)

static SrtpRxInstance_t SrtpRxChanData117;
#pragma DATA_SECTION (SrtpRxChanData117, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData117, 128)

static SrtpRxInstance_t SrtpRxChanData118;
#pragma DATA_SECTION (SrtpRxChanData118, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData118, 128)

static SrtpRxInstance_t SrtpRxChanData119;
#pragma DATA_SECTION (SrtpRxChanData119, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData119, 128)

static SrtpRxInstance_t SrtpRxChanData120;
#pragma DATA_SECTION (SrtpRxChanData120, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData120, 128)

static SrtpRxInstance_t SrtpRxChanData121;
#pragma DATA_SECTION (SrtpRxChanData121, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData121, 128)

static SrtpRxInstance_t SrtpRxChanData122;
#pragma DATA_SECTION (SrtpRxChanData122, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData122, 128)

static SrtpRxInstance_t SrtpRxChanData123;
#pragma DATA_SECTION (SrtpRxChanData123, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData123, 128)

static SrtpRxInstance_t SrtpRxChanData124;
#pragma DATA_SECTION (SrtpRxChanData124, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData124, 128)

static SrtpRxInstance_t SrtpRxChanData125;
#pragma DATA_SECTION (SrtpRxChanData125, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData125, 128)

static SrtpRxInstance_t SrtpRxChanData126;
#pragma DATA_SECTION (SrtpRxChanData126, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData126, 128)

static SrtpRxInstance_t SrtpRxChanData127;
#pragma DATA_SECTION (SrtpRxChanData127, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData127, 128)

static SrtpRxInstance_t SrtpRxChanData128;
#pragma DATA_SECTION (SrtpRxChanData128, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData128, 128)

static SrtpRxInstance_t SrtpRxChanData129;
#pragma DATA_SECTION (SrtpRxChanData129, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData129, 128)

static SrtpRxInstance_t SrtpRxChanData130;
#pragma DATA_SECTION (SrtpRxChanData130, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData130, 128)

static SrtpRxInstance_t SrtpRxChanData131;
#pragma DATA_SECTION (SrtpRxChanData131, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData131, 128)

static SrtpRxInstance_t SrtpRxChanData132;
#pragma DATA_SECTION (SrtpRxChanData132, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData132, 128)

static SrtpRxInstance_t SrtpRxChanData133;
#pragma DATA_SECTION (SrtpRxChanData133, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData133, 128)

static SrtpRxInstance_t SrtpRxChanData134;
#pragma DATA_SECTION (SrtpRxChanData134, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData134, 128)

static SrtpRxInstance_t SrtpRxChanData135;
#pragma DATA_SECTION (SrtpRxChanData135, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData135, 128)

static SrtpRxInstance_t SrtpRxChanData136;
#pragma DATA_SECTION (SrtpRxChanData136, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData136, 128)

static SrtpRxInstance_t SrtpRxChanData137;
#pragma DATA_SECTION (SrtpRxChanData137, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData137, 128)

static SrtpRxInstance_t SrtpRxChanData138;
#pragma DATA_SECTION (SrtpRxChanData138, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData138, 128)

static SrtpRxInstance_t SrtpRxChanData139;
#pragma DATA_SECTION (SrtpRxChanData139, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData139, 128)

static SrtpRxInstance_t SrtpRxChanData140;
#pragma DATA_SECTION (SrtpRxChanData140, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData140, 128)

static SrtpRxInstance_t SrtpRxChanData141;
#pragma DATA_SECTION (SrtpRxChanData141, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData141, 128)

static SrtpRxInstance_t SrtpRxChanData142;
#pragma DATA_SECTION (SrtpRxChanData142, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData142, 128)

static SrtpRxInstance_t SrtpRxChanData143;
#pragma DATA_SECTION (SrtpRxChanData143, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData143, 128)

static SrtpRxInstance_t SrtpRxChanData144;
#pragma DATA_SECTION (SrtpRxChanData144, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData144, 128)

static SrtpRxInstance_t SrtpRxChanData145;
#pragma DATA_SECTION (SrtpRxChanData145, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData145, 128)

static SrtpRxInstance_t SrtpRxChanData146;
#pragma DATA_SECTION (SrtpRxChanData146, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData146, 128)

static SrtpRxInstance_t SrtpRxChanData147;
#pragma DATA_SECTION (SrtpRxChanData147, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData147, 128)

static SrtpRxInstance_t SrtpRxChanData148;
#pragma DATA_SECTION (SrtpRxChanData148, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData148, 128)

static SrtpRxInstance_t SrtpRxChanData149;
#pragma DATA_SECTION (SrtpRxChanData149, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData149, 128)

static SrtpRxInstance_t SrtpRxChanData150;
#pragma DATA_SECTION (SrtpRxChanData150, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData150, 128)

static SrtpRxInstance_t SrtpRxChanData151;
#pragma DATA_SECTION (SrtpRxChanData151, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData151, 128)

static SrtpRxInstance_t SrtpRxChanData152;
#pragma DATA_SECTION (SrtpRxChanData152, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData152, 128)

static SrtpRxInstance_t SrtpRxChanData153;
#pragma DATA_SECTION (SrtpRxChanData153, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData153, 128)

static SrtpRxInstance_t SrtpRxChanData154;
#pragma DATA_SECTION (SrtpRxChanData154, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData154, 128)

static SrtpRxInstance_t SrtpRxChanData155;
#pragma DATA_SECTION (SrtpRxChanData155, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData155, 128)

static SrtpRxInstance_t SrtpRxChanData156;
#pragma DATA_SECTION (SrtpRxChanData156, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData156, 128)

static SrtpRxInstance_t SrtpRxChanData157;
#pragma DATA_SECTION (SrtpRxChanData157, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData157, 128)

static SrtpRxInstance_t SrtpRxChanData158;
#pragma DATA_SECTION (SrtpRxChanData158, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData158, 128)

static SrtpRxInstance_t SrtpRxChanData159;
#pragma DATA_SECTION (SrtpRxChanData159, "CHAN_INST_DATA:SRTP")
#pragma DATA_ALIGN   (SrtpRxChanData159, 128)

SrtpRxInstance_t* const SrtpRxChanData[] = {
	&SrtpRxChanData0,
	&SrtpRxChanData1,
	&SrtpRxChanData2,
	&SrtpRxChanData3,
	&SrtpRxChanData4,
	&SrtpRxChanData5,
	&SrtpRxChanData6,
	&SrtpRxChanData7,
	&SrtpRxChanData8,
	&SrtpRxChanData9,
	&SrtpRxChanData10,
	&SrtpRxChanData11,
	&SrtpRxChanData12,
	&SrtpRxChanData13,
	&SrtpRxChanData14,
	&SrtpRxChanData15,
	&SrtpRxChanData16,
	&SrtpRxChanData17,
	&SrtpRxChanData18,
	&SrtpRxChanData19,
	&SrtpRxChanData20,
	&SrtpRxChanData21,
	&SrtpRxChanData22,
	&SrtpRxChanData23,
	&SrtpRxChanData24,
	&SrtpRxChanData25,
	&SrtpRxChanData26,
	&SrtpRxChanData27,
	&SrtpRxChanData28,
	&SrtpRxChanData29,
	&SrtpRxChanData30,
	&SrtpRxChanData31,
	&SrtpRxChanData32,
	&SrtpRxChanData33,
	&SrtpRxChanData34,
	&SrtpRxChanData35,
	&SrtpRxChanData36,
	&SrtpRxChanData37,
	&SrtpRxChanData38,
	&SrtpRxChanData39,
	&SrtpRxChanData40,
	&SrtpRxChanData41,
	&SrtpRxChanData42,
	&SrtpRxChanData43,
	&SrtpRxChanData44,
	&SrtpRxChanData45,
	&SrtpRxChanData46,
	&SrtpRxChanData47,
	&SrtpRxChanData48,
	&SrtpRxChanData49,
	&SrtpRxChanData50,
	&SrtpRxChanData51,
	&SrtpRxChanData52,
	&SrtpRxChanData53,
	&SrtpRxChanData54,
	&SrtpRxChanData55,
	&SrtpRxChanData56,
	&SrtpRxChanData57,
	&SrtpRxChanData58,
	&SrtpRxChanData59,
	&SrtpRxChanData60,
	&SrtpRxChanData61,
	&SrtpRxChanData62,
	&SrtpRxChanData63,
	&SrtpRxChanData64,
	&SrtpRxChanData65,
	&SrtpRxChanData66,
	&SrtpRxChanData67,
	&SrtpRxChanData68,
	&SrtpRxChanData69,
	&SrtpRxChanData70,
	&SrtpRxChanData71,
	&SrtpRxChanData72,
	&SrtpRxChanData73,
	&SrtpRxChanData74,
	&SrtpRxChanData75,
	&SrtpRxChanData76,
	&SrtpRxChanData77,
	&SrtpRxChanData78,
	&SrtpRxChanData79,
	&SrtpRxChanData80,
	&SrtpRxChanData81,
	&SrtpRxChanData82,
	&SrtpRxChanData83,
	&SrtpRxChanData84,
	&SrtpRxChanData85,
	&SrtpRxChanData86,
	&SrtpRxChanData87,
	&SrtpRxChanData88,
	&SrtpRxChanData89,
	&SrtpRxChanData90,
	&SrtpRxChanData91,
	&SrtpRxChanData92,
	&SrtpRxChanData93,
	&SrtpRxChanData94,
	&SrtpRxChanData95,
	&SrtpRxChanData96,
	&SrtpRxChanData97,
	&SrtpRxChanData98,
	&SrtpRxChanData99,
	&SrtpRxChanData100,
	&SrtpRxChanData101,
	&SrtpRxChanData102,
	&SrtpRxChanData103,
	&SrtpRxChanData104,
	&SrtpRxChanData105,
	&SrtpRxChanData106,
	&SrtpRxChanData107,
	&SrtpRxChanData108,
	&SrtpRxChanData109,
	&SrtpRxChanData110,
	&SrtpRxChanData111,
	&SrtpRxChanData112,
	&SrtpRxChanData113,
	&SrtpRxChanData114,
	&SrtpRxChanData115,
	&SrtpRxChanData116,
	&SrtpRxChanData117,
	&SrtpRxChanData118,
	&SrtpRxChanData119,
	&SrtpRxChanData120,
	&SrtpRxChanData121,
	&SrtpRxChanData122,
	&SrtpRxChanData123,
	&SrtpRxChanData124,
	&SrtpRxChanData125,
	&SrtpRxChanData126,
	&SrtpRxChanData127,
	&SrtpRxChanData128,
	&SrtpRxChanData129,
	&SrtpRxChanData130,
	&SrtpRxChanData131,
	&SrtpRxChanData132,
	&SrtpRxChanData133,
	&SrtpRxChanData134,
	&SrtpRxChanData135,
	&SrtpRxChanData136,
	&SrtpRxChanData137,
	&SrtpRxChanData138,
	&SrtpRxChanData139,
	&SrtpRxChanData140,
	&SrtpRxChanData141,
	&SrtpRxChanData142,
	&SrtpRxChanData143,
	&SrtpRxChanData144,
	&SrtpRxChanData145,
	&SrtpRxChanData146,
	&SrtpRxChanData147,
	&SrtpRxChanData148,
	&SrtpRxChanData149,
	&SrtpRxChanData150,
	&SrtpRxChanData151,
	&SrtpRxChanData152,
	&SrtpRxChanData153,
	&SrtpRxChanData154,
	&SrtpRxChanData155,
	&SrtpRxChanData156,
	&SrtpRxChanData157,
	&SrtpRxChanData158,
	&SrtpRxChanData159
};
//}

//{      GpakSrtpKeyInfo_t        SrtpRxKeyData STRUCTURES to CHAN_INST_DATA:SRTP_KEY

GpakSrtpKeyInfo_t SrtpRxKeyData0;
#pragma DATA_SECTION (SrtpRxKeyData0, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData0, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData1;
#pragma DATA_SECTION (SrtpRxKeyData1, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData1, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData2;
#pragma DATA_SECTION (SrtpRxKeyData2, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData2, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData3;
#pragma DATA_SECTION (SrtpRxKeyData3, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData3, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData4;
#pragma DATA_SECTION (SrtpRxKeyData4, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData4, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData5;
#pragma DATA_SECTION (SrtpRxKeyData5, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData5, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData6;
#pragma DATA_SECTION (SrtpRxKeyData6, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData6, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData7;
#pragma DATA_SECTION (SrtpRxKeyData7, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData7, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData8;
#pragma DATA_SECTION (SrtpRxKeyData8, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData8, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData9;
#pragma DATA_SECTION (SrtpRxKeyData9, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData9, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData10;
#pragma DATA_SECTION (SrtpRxKeyData10, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData10, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData11;
#pragma DATA_SECTION (SrtpRxKeyData11, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData11, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData12;
#pragma DATA_SECTION (SrtpRxKeyData12, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData12, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData13;
#pragma DATA_SECTION (SrtpRxKeyData13, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData13, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData14;
#pragma DATA_SECTION (SrtpRxKeyData14, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData14, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData15;
#pragma DATA_SECTION (SrtpRxKeyData15, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData15, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData16;
#pragma DATA_SECTION (SrtpRxKeyData16, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData16, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData17;
#pragma DATA_SECTION (SrtpRxKeyData17, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData17, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData18;
#pragma DATA_SECTION (SrtpRxKeyData18, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData18, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData19;
#pragma DATA_SECTION (SrtpRxKeyData19, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData19, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData20;
#pragma DATA_SECTION (SrtpRxKeyData20, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData20, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData21;
#pragma DATA_SECTION (SrtpRxKeyData21, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData21, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData22;
#pragma DATA_SECTION (SrtpRxKeyData22, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData22, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData23;
#pragma DATA_SECTION (SrtpRxKeyData23, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData23, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData24;
#pragma DATA_SECTION (SrtpRxKeyData24, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData24, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData25;
#pragma DATA_SECTION (SrtpRxKeyData25, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData25, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData26;
#pragma DATA_SECTION (SrtpRxKeyData26, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData26, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData27;
#pragma DATA_SECTION (SrtpRxKeyData27, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData27, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData28;
#pragma DATA_SECTION (SrtpRxKeyData28, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData28, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData29;
#pragma DATA_SECTION (SrtpRxKeyData29, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData29, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData30;
#pragma DATA_SECTION (SrtpRxKeyData30, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData30, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData31;
#pragma DATA_SECTION (SrtpRxKeyData31, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData31, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData32;
#pragma DATA_SECTION (SrtpRxKeyData32, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData32, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData33;
#pragma DATA_SECTION (SrtpRxKeyData33, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData33, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData34;
#pragma DATA_SECTION (SrtpRxKeyData34, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData34, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData35;
#pragma DATA_SECTION (SrtpRxKeyData35, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData35, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData36;
#pragma DATA_SECTION (SrtpRxKeyData36, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData36, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData37;
#pragma DATA_SECTION (SrtpRxKeyData37, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData37, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData38;
#pragma DATA_SECTION (SrtpRxKeyData38, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData38, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData39;
#pragma DATA_SECTION (SrtpRxKeyData39, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData39, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData40;
#pragma DATA_SECTION (SrtpRxKeyData40, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData40, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData41;
#pragma DATA_SECTION (SrtpRxKeyData41, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData41, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData42;
#pragma DATA_SECTION (SrtpRxKeyData42, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData42, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData43;
#pragma DATA_SECTION (SrtpRxKeyData43, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData43, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData44;
#pragma DATA_SECTION (SrtpRxKeyData44, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData44, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData45;
#pragma DATA_SECTION (SrtpRxKeyData45, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData45, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData46;
#pragma DATA_SECTION (SrtpRxKeyData46, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData46, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData47;
#pragma DATA_SECTION (SrtpRxKeyData47, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData47, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData48;
#pragma DATA_SECTION (SrtpRxKeyData48, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData48, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData49;
#pragma DATA_SECTION (SrtpRxKeyData49, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData49, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData50;
#pragma DATA_SECTION (SrtpRxKeyData50, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData50, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData51;
#pragma DATA_SECTION (SrtpRxKeyData51, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData51, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData52;
#pragma DATA_SECTION (SrtpRxKeyData52, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData52, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData53;
#pragma DATA_SECTION (SrtpRxKeyData53, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData53, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData54;
#pragma DATA_SECTION (SrtpRxKeyData54, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData54, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData55;
#pragma DATA_SECTION (SrtpRxKeyData55, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData55, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData56;
#pragma DATA_SECTION (SrtpRxKeyData56, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData56, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData57;
#pragma DATA_SECTION (SrtpRxKeyData57, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData57, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData58;
#pragma DATA_SECTION (SrtpRxKeyData58, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData58, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData59;
#pragma DATA_SECTION (SrtpRxKeyData59, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData59, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData60;
#pragma DATA_SECTION (SrtpRxKeyData60, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData60, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData61;
#pragma DATA_SECTION (SrtpRxKeyData61, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData61, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData62;
#pragma DATA_SECTION (SrtpRxKeyData62, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData62, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData63;
#pragma DATA_SECTION (SrtpRxKeyData63, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData63, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData64;
#pragma DATA_SECTION (SrtpRxKeyData64, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData64, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData65;
#pragma DATA_SECTION (SrtpRxKeyData65, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData65, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData66;
#pragma DATA_SECTION (SrtpRxKeyData66, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData66, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData67;
#pragma DATA_SECTION (SrtpRxKeyData67, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData67, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData68;
#pragma DATA_SECTION (SrtpRxKeyData68, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData68, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData69;
#pragma DATA_SECTION (SrtpRxKeyData69, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData69, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData70;
#pragma DATA_SECTION (SrtpRxKeyData70, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData70, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData71;
#pragma DATA_SECTION (SrtpRxKeyData71, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData71, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData72;
#pragma DATA_SECTION (SrtpRxKeyData72, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData72, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData73;
#pragma DATA_SECTION (SrtpRxKeyData73, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData73, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData74;
#pragma DATA_SECTION (SrtpRxKeyData74, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData74, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData75;
#pragma DATA_SECTION (SrtpRxKeyData75, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData75, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData76;
#pragma DATA_SECTION (SrtpRxKeyData76, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData76, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData77;
#pragma DATA_SECTION (SrtpRxKeyData77, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData77, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData78;
#pragma DATA_SECTION (SrtpRxKeyData78, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData78, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData79;
#pragma DATA_SECTION (SrtpRxKeyData79, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData79, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData80;
#pragma DATA_SECTION (SrtpRxKeyData80, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData80, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData81;
#pragma DATA_SECTION (SrtpRxKeyData81, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData81, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData82;
#pragma DATA_SECTION (SrtpRxKeyData82, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData82, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData83;
#pragma DATA_SECTION (SrtpRxKeyData83, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData83, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData84;
#pragma DATA_SECTION (SrtpRxKeyData84, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData84, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData85;
#pragma DATA_SECTION (SrtpRxKeyData85, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData85, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData86;
#pragma DATA_SECTION (SrtpRxKeyData86, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData86, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData87;
#pragma DATA_SECTION (SrtpRxKeyData87, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData87, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData88;
#pragma DATA_SECTION (SrtpRxKeyData88, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData88, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData89;
#pragma DATA_SECTION (SrtpRxKeyData89, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData89, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData90;
#pragma DATA_SECTION (SrtpRxKeyData90, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData90, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData91;
#pragma DATA_SECTION (SrtpRxKeyData91, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData91, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData92;
#pragma DATA_SECTION (SrtpRxKeyData92, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData92, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData93;
#pragma DATA_SECTION (SrtpRxKeyData93, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData93, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData94;
#pragma DATA_SECTION (SrtpRxKeyData94, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData94, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData95;
#pragma DATA_SECTION (SrtpRxKeyData95, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData95, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData96;
#pragma DATA_SECTION (SrtpRxKeyData96, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData96, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData97;
#pragma DATA_SECTION (SrtpRxKeyData97, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData97, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData98;
#pragma DATA_SECTION (SrtpRxKeyData98, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData98, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData99;
#pragma DATA_SECTION (SrtpRxKeyData99, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData99, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData100;
#pragma DATA_SECTION (SrtpRxKeyData100, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData100, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData101;
#pragma DATA_SECTION (SrtpRxKeyData101, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData101, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData102;
#pragma DATA_SECTION (SrtpRxKeyData102, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData102, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData103;
#pragma DATA_SECTION (SrtpRxKeyData103, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData103, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData104;
#pragma DATA_SECTION (SrtpRxKeyData104, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData104, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData105;
#pragma DATA_SECTION (SrtpRxKeyData105, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData105, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData106;
#pragma DATA_SECTION (SrtpRxKeyData106, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData106, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData107;
#pragma DATA_SECTION (SrtpRxKeyData107, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData107, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData108;
#pragma DATA_SECTION (SrtpRxKeyData108, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData108, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData109;
#pragma DATA_SECTION (SrtpRxKeyData109, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData109, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData110;
#pragma DATA_SECTION (SrtpRxKeyData110, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData110, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData111;
#pragma DATA_SECTION (SrtpRxKeyData111, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData111, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData112;
#pragma DATA_SECTION (SrtpRxKeyData112, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData112, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData113;
#pragma DATA_SECTION (SrtpRxKeyData113, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData113, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData114;
#pragma DATA_SECTION (SrtpRxKeyData114, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData114, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData115;
#pragma DATA_SECTION (SrtpRxKeyData115, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData115, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData116;
#pragma DATA_SECTION (SrtpRxKeyData116, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData116, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData117;
#pragma DATA_SECTION (SrtpRxKeyData117, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData117, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData118;
#pragma DATA_SECTION (SrtpRxKeyData118, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData118, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData119;
#pragma DATA_SECTION (SrtpRxKeyData119, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData119, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData120;
#pragma DATA_SECTION (SrtpRxKeyData120, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData120, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData121;
#pragma DATA_SECTION (SrtpRxKeyData121, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData121, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData122;
#pragma DATA_SECTION (SrtpRxKeyData122, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData122, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData123;
#pragma DATA_SECTION (SrtpRxKeyData123, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData123, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData124;
#pragma DATA_SECTION (SrtpRxKeyData124, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData124, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData125;
#pragma DATA_SECTION (SrtpRxKeyData125, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData125, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData126;
#pragma DATA_SECTION (SrtpRxKeyData126, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData126, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData127;
#pragma DATA_SECTION (SrtpRxKeyData127, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData127, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData128;
#pragma DATA_SECTION (SrtpRxKeyData128, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData128, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData129;
#pragma DATA_SECTION (SrtpRxKeyData129, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData129, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData130;
#pragma DATA_SECTION (SrtpRxKeyData130, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData130, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData131;
#pragma DATA_SECTION (SrtpRxKeyData131, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData131, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData132;
#pragma DATA_SECTION (SrtpRxKeyData132, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData132, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData133;
#pragma DATA_SECTION (SrtpRxKeyData133, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData133, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData134;
#pragma DATA_SECTION (SrtpRxKeyData134, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData134, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData135;
#pragma DATA_SECTION (SrtpRxKeyData135, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData135, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData136;
#pragma DATA_SECTION (SrtpRxKeyData136, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData136, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData137;
#pragma DATA_SECTION (SrtpRxKeyData137, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData137, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData138;
#pragma DATA_SECTION (SrtpRxKeyData138, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData138, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData139;
#pragma DATA_SECTION (SrtpRxKeyData139, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData139, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData140;
#pragma DATA_SECTION (SrtpRxKeyData140, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData140, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData141;
#pragma DATA_SECTION (SrtpRxKeyData141, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData141, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData142;
#pragma DATA_SECTION (SrtpRxKeyData142, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData142, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData143;
#pragma DATA_SECTION (SrtpRxKeyData143, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData143, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData144;
#pragma DATA_SECTION (SrtpRxKeyData144, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData144, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData145;
#pragma DATA_SECTION (SrtpRxKeyData145, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData145, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData146;
#pragma DATA_SECTION (SrtpRxKeyData146, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData146, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData147;
#pragma DATA_SECTION (SrtpRxKeyData147, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData147, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData148;
#pragma DATA_SECTION (SrtpRxKeyData148, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData148, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData149;
#pragma DATA_SECTION (SrtpRxKeyData149, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData149, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData150;
#pragma DATA_SECTION (SrtpRxKeyData150, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData150, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData151;
#pragma DATA_SECTION (SrtpRxKeyData151, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData151, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData152;
#pragma DATA_SECTION (SrtpRxKeyData152, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData152, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData153;
#pragma DATA_SECTION (SrtpRxKeyData153, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData153, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData154;
#pragma DATA_SECTION (SrtpRxKeyData154, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData154, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData155;
#pragma DATA_SECTION (SrtpRxKeyData155, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData155, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData156;
#pragma DATA_SECTION (SrtpRxKeyData156, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData156, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData157;
#pragma DATA_SECTION (SrtpRxKeyData157, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData157, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData158;
#pragma DATA_SECTION (SrtpRxKeyData158, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData158, 128)

static GpakSrtpKeyInfo_t SrtpRxKeyData159;
#pragma DATA_SECTION (SrtpRxKeyData159, "CHAN_INST_DATA:SRTP_KEY")
#pragma DATA_ALIGN   (SrtpRxKeyData159, 128)

GpakSrtpKeyInfo_t* const SrtpRxKeyData[] = {
	&SrtpRxKeyData0,
	&SrtpRxKeyData1,
	&SrtpRxKeyData2,
	&SrtpRxKeyData3,
	&SrtpRxKeyData4,
	&SrtpRxKeyData5,
	&SrtpRxKeyData6,
	&SrtpRxKeyData7,
	&SrtpRxKeyData8,
	&SrtpRxKeyData9,
	&SrtpRxKeyData10,
	&SrtpRxKeyData11,
	&SrtpRxKeyData12,
	&SrtpRxKeyData13,
	&SrtpRxKeyData14,
	&SrtpRxKeyData15,
	&SrtpRxKeyData16,
	&SrtpRxKeyData17,
	&SrtpRxKeyData18,
	&SrtpRxKeyData19,
	&SrtpRxKeyData20,
	&SrtpRxKeyData21,
	&SrtpRxKeyData22,
	&SrtpRxKeyData23,
	&SrtpRxKeyData24,
	&SrtpRxKeyData25,
	&SrtpRxKeyData26,
	&SrtpRxKeyData27,
	&SrtpRxKeyData28,
	&SrtpRxKeyData29,
	&SrtpRxKeyData30,
	&SrtpRxKeyData31,
	&SrtpRxKeyData32,
	&SrtpRxKeyData33,
	&SrtpRxKeyData34,
	&SrtpRxKeyData35,
	&SrtpRxKeyData36,
	&SrtpRxKeyData37,
	&SrtpRxKeyData38,
	&SrtpRxKeyData39,
	&SrtpRxKeyData40,
	&SrtpRxKeyData41,
	&SrtpRxKeyData42,
	&SrtpRxKeyData43,
	&SrtpRxKeyData44,
	&SrtpRxKeyData45,
	&SrtpRxKeyData46,
	&SrtpRxKeyData47,
	&SrtpRxKeyData48,
	&SrtpRxKeyData49,
	&SrtpRxKeyData50,
	&SrtpRxKeyData51,
	&SrtpRxKeyData52,
	&SrtpRxKeyData53,
	&SrtpRxKeyData54,
	&SrtpRxKeyData55,
	&SrtpRxKeyData56,
	&SrtpRxKeyData57,
	&SrtpRxKeyData58,
	&SrtpRxKeyData59,
	&SrtpRxKeyData60,
	&SrtpRxKeyData61,
	&SrtpRxKeyData62,
	&SrtpRxKeyData63,
	&SrtpRxKeyData64,
	&SrtpRxKeyData65,
	&SrtpRxKeyData66,
	&SrtpRxKeyData67,
	&SrtpRxKeyData68,
	&SrtpRxKeyData69,
	&SrtpRxKeyData70,
	&SrtpRxKeyData71,
	&SrtpRxKeyData72,
	&SrtpRxKeyData73,
	&SrtpRxKeyData74,
	&SrtpRxKeyData75,
	&SrtpRxKeyData76,
	&SrtpRxKeyData77,
	&SrtpRxKeyData78,
	&SrtpRxKeyData79,
	&SrtpRxKeyData80,
	&SrtpRxKeyData81,
	&SrtpRxKeyData82,
	&SrtpRxKeyData83,
	&SrtpRxKeyData84,
	&SrtpRxKeyData85,
	&SrtpRxKeyData86,
	&SrtpRxKeyData87,
	&SrtpRxKeyData88,
	&SrtpRxKeyData89,
	&SrtpRxKeyData90,
	&SrtpRxKeyData91,
	&SrtpRxKeyData92,
	&SrtpRxKeyData93,
	&SrtpRxKeyData94,
	&SrtpRxKeyData95,
	&SrtpRxKeyData96,
	&SrtpRxKeyData97,
	&SrtpRxKeyData98,
	&SrtpRxKeyData99,
	&SrtpRxKeyData100,
	&SrtpRxKeyData101,
	&SrtpRxKeyData102,
	&SrtpRxKeyData103,
	&SrtpRxKeyData104,
	&SrtpRxKeyData105,
	&SrtpRxKeyData106,
	&SrtpRxKeyData107,
	&SrtpRxKeyData108,
	&SrtpRxKeyData109,
	&SrtpRxKeyData110,
	&SrtpRxKeyData111,
	&SrtpRxKeyData112,
	&SrtpRxKeyData113,
	&SrtpRxKeyData114,
	&SrtpRxKeyData115,
	&SrtpRxKeyData116,
	&SrtpRxKeyData117,
	&SrtpRxKeyData118,
	&SrtpRxKeyData119,
	&SrtpRxKeyData120,
	&SrtpRxKeyData121,
	&SrtpRxKeyData122,
	&SrtpRxKeyData123,
	&SrtpRxKeyData124,
	&SrtpRxKeyData125,
	&SrtpRxKeyData126,
	&SrtpRxKeyData127,
	&SrtpRxKeyData128,
	&SrtpRxKeyData129,
	&SrtpRxKeyData130,
	&SrtpRxKeyData131,
	&SrtpRxKeyData132,
	&SrtpRxKeyData133,
	&SrtpRxKeyData134,
	&SrtpRxKeyData135,
	&SrtpRxKeyData136,
	&SrtpRxKeyData137,
	&SrtpRxKeyData138,
	&SrtpRxKeyData139,
	&SrtpRxKeyData140,
	&SrtpRxKeyData141,
	&SrtpRxKeyData142,
	&SrtpRxKeyData143,
	&SrtpRxKeyData144,
	&SrtpRxKeyData145,
	&SrtpRxKeyData146,
	&SrtpRxKeyData147,
	&SrtpRxKeyData148,
	&SrtpRxKeyData149,
	&SrtpRxKeyData150,
	&SrtpRxKeyData151,
	&SrtpRxKeyData152,
	&SrtpRxKeyData153,
	&SrtpRxKeyData154,
	&SrtpRxKeyData155,
	&SrtpRxKeyData156,
	&SrtpRxKeyData157,
	&SrtpRxKeyData158,
	&SrtpRxKeyData159
};
//}

// RTCP data structures.
//{             rtcpdata_t             rtcpData STRUCTURES to CHAN_INST_DATA:RTCP

rtcpdata_t rtcpData0;
#pragma DATA_SECTION (rtcpData0, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData0, 128)

static rtcpdata_t rtcpData1;
#pragma DATA_SECTION (rtcpData1, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData1, 128)

static rtcpdata_t rtcpData2;
#pragma DATA_SECTION (rtcpData2, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData2, 128)

static rtcpdata_t rtcpData3;
#pragma DATA_SECTION (rtcpData3, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData3, 128)

static rtcpdata_t rtcpData4;
#pragma DATA_SECTION (rtcpData4, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData4, 128)

static rtcpdata_t rtcpData5;
#pragma DATA_SECTION (rtcpData5, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData5, 128)

static rtcpdata_t rtcpData6;
#pragma DATA_SECTION (rtcpData6, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData6, 128)

static rtcpdata_t rtcpData7;
#pragma DATA_SECTION (rtcpData7, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData7, 128)

static rtcpdata_t rtcpData8;
#pragma DATA_SECTION (rtcpData8, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData8, 128)

static rtcpdata_t rtcpData9;
#pragma DATA_SECTION (rtcpData9, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData9, 128)

static rtcpdata_t rtcpData10;
#pragma DATA_SECTION (rtcpData10, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData10, 128)

static rtcpdata_t rtcpData11;
#pragma DATA_SECTION (rtcpData11, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData11, 128)

static rtcpdata_t rtcpData12;
#pragma DATA_SECTION (rtcpData12, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData12, 128)

static rtcpdata_t rtcpData13;
#pragma DATA_SECTION (rtcpData13, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData13, 128)

static rtcpdata_t rtcpData14;
#pragma DATA_SECTION (rtcpData14, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData14, 128)

static rtcpdata_t rtcpData15;
#pragma DATA_SECTION (rtcpData15, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData15, 128)

static rtcpdata_t rtcpData16;
#pragma DATA_SECTION (rtcpData16, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData16, 128)

static rtcpdata_t rtcpData17;
#pragma DATA_SECTION (rtcpData17, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData17, 128)

static rtcpdata_t rtcpData18;
#pragma DATA_SECTION (rtcpData18, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData18, 128)

static rtcpdata_t rtcpData19;
#pragma DATA_SECTION (rtcpData19, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData19, 128)

static rtcpdata_t rtcpData20;
#pragma DATA_SECTION (rtcpData20, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData20, 128)

static rtcpdata_t rtcpData21;
#pragma DATA_SECTION (rtcpData21, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData21, 128)

static rtcpdata_t rtcpData22;
#pragma DATA_SECTION (rtcpData22, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData22, 128)

static rtcpdata_t rtcpData23;
#pragma DATA_SECTION (rtcpData23, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData23, 128)

static rtcpdata_t rtcpData24;
#pragma DATA_SECTION (rtcpData24, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData24, 128)

static rtcpdata_t rtcpData25;
#pragma DATA_SECTION (rtcpData25, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData25, 128)

static rtcpdata_t rtcpData26;
#pragma DATA_SECTION (rtcpData26, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData26, 128)

static rtcpdata_t rtcpData27;
#pragma DATA_SECTION (rtcpData27, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData27, 128)

static rtcpdata_t rtcpData28;
#pragma DATA_SECTION (rtcpData28, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData28, 128)

static rtcpdata_t rtcpData29;
#pragma DATA_SECTION (rtcpData29, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData29, 128)

static rtcpdata_t rtcpData30;
#pragma DATA_SECTION (rtcpData30, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData30, 128)

static rtcpdata_t rtcpData31;
#pragma DATA_SECTION (rtcpData31, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData31, 128)

static rtcpdata_t rtcpData32;
#pragma DATA_SECTION (rtcpData32, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData32, 128)

static rtcpdata_t rtcpData33;
#pragma DATA_SECTION (rtcpData33, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData33, 128)

static rtcpdata_t rtcpData34;
#pragma DATA_SECTION (rtcpData34, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData34, 128)

static rtcpdata_t rtcpData35;
#pragma DATA_SECTION (rtcpData35, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData35, 128)

static rtcpdata_t rtcpData36;
#pragma DATA_SECTION (rtcpData36, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData36, 128)

static rtcpdata_t rtcpData37;
#pragma DATA_SECTION (rtcpData37, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData37, 128)

static rtcpdata_t rtcpData38;
#pragma DATA_SECTION (rtcpData38, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData38, 128)

static rtcpdata_t rtcpData39;
#pragma DATA_SECTION (rtcpData39, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData39, 128)

static rtcpdata_t rtcpData40;
#pragma DATA_SECTION (rtcpData40, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData40, 128)

static rtcpdata_t rtcpData41;
#pragma DATA_SECTION (rtcpData41, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData41, 128)

static rtcpdata_t rtcpData42;
#pragma DATA_SECTION (rtcpData42, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData42, 128)

static rtcpdata_t rtcpData43;
#pragma DATA_SECTION (rtcpData43, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData43, 128)

static rtcpdata_t rtcpData44;
#pragma DATA_SECTION (rtcpData44, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData44, 128)

static rtcpdata_t rtcpData45;
#pragma DATA_SECTION (rtcpData45, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData45, 128)

static rtcpdata_t rtcpData46;
#pragma DATA_SECTION (rtcpData46, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData46, 128)

static rtcpdata_t rtcpData47;
#pragma DATA_SECTION (rtcpData47, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData47, 128)

static rtcpdata_t rtcpData48;
#pragma DATA_SECTION (rtcpData48, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData48, 128)

static rtcpdata_t rtcpData49;
#pragma DATA_SECTION (rtcpData49, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData49, 128)

static rtcpdata_t rtcpData50;
#pragma DATA_SECTION (rtcpData50, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData50, 128)

static rtcpdata_t rtcpData51;
#pragma DATA_SECTION (rtcpData51, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData51, 128)

static rtcpdata_t rtcpData52;
#pragma DATA_SECTION (rtcpData52, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData52, 128)

static rtcpdata_t rtcpData53;
#pragma DATA_SECTION (rtcpData53, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData53, 128)

static rtcpdata_t rtcpData54;
#pragma DATA_SECTION (rtcpData54, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData54, 128)

static rtcpdata_t rtcpData55;
#pragma DATA_SECTION (rtcpData55, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData55, 128)

static rtcpdata_t rtcpData56;
#pragma DATA_SECTION (rtcpData56, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData56, 128)

static rtcpdata_t rtcpData57;
#pragma DATA_SECTION (rtcpData57, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData57, 128)

static rtcpdata_t rtcpData58;
#pragma DATA_SECTION (rtcpData58, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData58, 128)

static rtcpdata_t rtcpData59;
#pragma DATA_SECTION (rtcpData59, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData59, 128)

static rtcpdata_t rtcpData60;
#pragma DATA_SECTION (rtcpData60, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData60, 128)

static rtcpdata_t rtcpData61;
#pragma DATA_SECTION (rtcpData61, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData61, 128)

static rtcpdata_t rtcpData62;
#pragma DATA_SECTION (rtcpData62, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData62, 128)

static rtcpdata_t rtcpData63;
#pragma DATA_SECTION (rtcpData63, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData63, 128)

static rtcpdata_t rtcpData64;
#pragma DATA_SECTION (rtcpData64, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData64, 128)

static rtcpdata_t rtcpData65;
#pragma DATA_SECTION (rtcpData65, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData65, 128)

static rtcpdata_t rtcpData66;
#pragma DATA_SECTION (rtcpData66, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData66, 128)

static rtcpdata_t rtcpData67;
#pragma DATA_SECTION (rtcpData67, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData67, 128)

static rtcpdata_t rtcpData68;
#pragma DATA_SECTION (rtcpData68, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData68, 128)

static rtcpdata_t rtcpData69;
#pragma DATA_SECTION (rtcpData69, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData69, 128)

static rtcpdata_t rtcpData70;
#pragma DATA_SECTION (rtcpData70, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData70, 128)

static rtcpdata_t rtcpData71;
#pragma DATA_SECTION (rtcpData71, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData71, 128)

static rtcpdata_t rtcpData72;
#pragma DATA_SECTION (rtcpData72, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData72, 128)

static rtcpdata_t rtcpData73;
#pragma DATA_SECTION (rtcpData73, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData73, 128)

static rtcpdata_t rtcpData74;
#pragma DATA_SECTION (rtcpData74, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData74, 128)

static rtcpdata_t rtcpData75;
#pragma DATA_SECTION (rtcpData75, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData75, 128)

static rtcpdata_t rtcpData76;
#pragma DATA_SECTION (rtcpData76, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData76, 128)

static rtcpdata_t rtcpData77;
#pragma DATA_SECTION (rtcpData77, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData77, 128)

static rtcpdata_t rtcpData78;
#pragma DATA_SECTION (rtcpData78, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData78, 128)

static rtcpdata_t rtcpData79;
#pragma DATA_SECTION (rtcpData79, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData79, 128)

static rtcpdata_t rtcpData80;
#pragma DATA_SECTION (rtcpData80, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData80, 128)

static rtcpdata_t rtcpData81;
#pragma DATA_SECTION (rtcpData81, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData81, 128)

static rtcpdata_t rtcpData82;
#pragma DATA_SECTION (rtcpData82, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData82, 128)

static rtcpdata_t rtcpData83;
#pragma DATA_SECTION (rtcpData83, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData83, 128)

static rtcpdata_t rtcpData84;
#pragma DATA_SECTION (rtcpData84, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData84, 128)

static rtcpdata_t rtcpData85;
#pragma DATA_SECTION (rtcpData85, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData85, 128)

static rtcpdata_t rtcpData86;
#pragma DATA_SECTION (rtcpData86, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData86, 128)

static rtcpdata_t rtcpData87;
#pragma DATA_SECTION (rtcpData87, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData87, 128)

static rtcpdata_t rtcpData88;
#pragma DATA_SECTION (rtcpData88, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData88, 128)

static rtcpdata_t rtcpData89;
#pragma DATA_SECTION (rtcpData89, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData89, 128)

static rtcpdata_t rtcpData90;
#pragma DATA_SECTION (rtcpData90, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData90, 128)

static rtcpdata_t rtcpData91;
#pragma DATA_SECTION (rtcpData91, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData91, 128)

static rtcpdata_t rtcpData92;
#pragma DATA_SECTION (rtcpData92, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData92, 128)

static rtcpdata_t rtcpData93;
#pragma DATA_SECTION (rtcpData93, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData93, 128)

static rtcpdata_t rtcpData94;
#pragma DATA_SECTION (rtcpData94, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData94, 128)

static rtcpdata_t rtcpData95;
#pragma DATA_SECTION (rtcpData95, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData95, 128)

static rtcpdata_t rtcpData96;
#pragma DATA_SECTION (rtcpData96, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData96, 128)

static rtcpdata_t rtcpData97;
#pragma DATA_SECTION (rtcpData97, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData97, 128)

static rtcpdata_t rtcpData98;
#pragma DATA_SECTION (rtcpData98, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData98, 128)

static rtcpdata_t rtcpData99;
#pragma DATA_SECTION (rtcpData99, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData99, 128)

static rtcpdata_t rtcpData100;
#pragma DATA_SECTION (rtcpData100, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData100, 128)

static rtcpdata_t rtcpData101;
#pragma DATA_SECTION (rtcpData101, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData101, 128)

static rtcpdata_t rtcpData102;
#pragma DATA_SECTION (rtcpData102, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData102, 128)

static rtcpdata_t rtcpData103;
#pragma DATA_SECTION (rtcpData103, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData103, 128)

static rtcpdata_t rtcpData104;
#pragma DATA_SECTION (rtcpData104, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData104, 128)

static rtcpdata_t rtcpData105;
#pragma DATA_SECTION (rtcpData105, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData105, 128)

static rtcpdata_t rtcpData106;
#pragma DATA_SECTION (rtcpData106, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData106, 128)

static rtcpdata_t rtcpData107;
#pragma DATA_SECTION (rtcpData107, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData107, 128)

static rtcpdata_t rtcpData108;
#pragma DATA_SECTION (rtcpData108, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData108, 128)

static rtcpdata_t rtcpData109;
#pragma DATA_SECTION (rtcpData109, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData109, 128)

static rtcpdata_t rtcpData110;
#pragma DATA_SECTION (rtcpData110, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData110, 128)

static rtcpdata_t rtcpData111;
#pragma DATA_SECTION (rtcpData111, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData111, 128)

static rtcpdata_t rtcpData112;
#pragma DATA_SECTION (rtcpData112, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData112, 128)

static rtcpdata_t rtcpData113;
#pragma DATA_SECTION (rtcpData113, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData113, 128)

static rtcpdata_t rtcpData114;
#pragma DATA_SECTION (rtcpData114, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData114, 128)

static rtcpdata_t rtcpData115;
#pragma DATA_SECTION (rtcpData115, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData115, 128)

static rtcpdata_t rtcpData116;
#pragma DATA_SECTION (rtcpData116, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData116, 128)

static rtcpdata_t rtcpData117;
#pragma DATA_SECTION (rtcpData117, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData117, 128)

static rtcpdata_t rtcpData118;
#pragma DATA_SECTION (rtcpData118, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData118, 128)

static rtcpdata_t rtcpData119;
#pragma DATA_SECTION (rtcpData119, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData119, 128)

static rtcpdata_t rtcpData120;
#pragma DATA_SECTION (rtcpData120, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData120, 128)

static rtcpdata_t rtcpData121;
#pragma DATA_SECTION (rtcpData121, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData121, 128)

static rtcpdata_t rtcpData122;
#pragma DATA_SECTION (rtcpData122, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData122, 128)

static rtcpdata_t rtcpData123;
#pragma DATA_SECTION (rtcpData123, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData123, 128)

static rtcpdata_t rtcpData124;
#pragma DATA_SECTION (rtcpData124, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData124, 128)

static rtcpdata_t rtcpData125;
#pragma DATA_SECTION (rtcpData125, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData125, 128)

static rtcpdata_t rtcpData126;
#pragma DATA_SECTION (rtcpData126, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData126, 128)

static rtcpdata_t rtcpData127;
#pragma DATA_SECTION (rtcpData127, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData127, 128)

static rtcpdata_t rtcpData128;
#pragma DATA_SECTION (rtcpData128, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData128, 128)

static rtcpdata_t rtcpData129;
#pragma DATA_SECTION (rtcpData129, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData129, 128)

static rtcpdata_t rtcpData130;
#pragma DATA_SECTION (rtcpData130, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData130, 128)

static rtcpdata_t rtcpData131;
#pragma DATA_SECTION (rtcpData131, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData131, 128)

static rtcpdata_t rtcpData132;
#pragma DATA_SECTION (rtcpData132, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData132, 128)

static rtcpdata_t rtcpData133;
#pragma DATA_SECTION (rtcpData133, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData133, 128)

static rtcpdata_t rtcpData134;
#pragma DATA_SECTION (rtcpData134, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData134, 128)

static rtcpdata_t rtcpData135;
#pragma DATA_SECTION (rtcpData135, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData135, 128)

static rtcpdata_t rtcpData136;
#pragma DATA_SECTION (rtcpData136, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData136, 128)

static rtcpdata_t rtcpData137;
#pragma DATA_SECTION (rtcpData137, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData137, 128)

static rtcpdata_t rtcpData138;
#pragma DATA_SECTION (rtcpData138, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData138, 128)

static rtcpdata_t rtcpData139;
#pragma DATA_SECTION (rtcpData139, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData139, 128)

static rtcpdata_t rtcpData140;
#pragma DATA_SECTION (rtcpData140, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData140, 128)

static rtcpdata_t rtcpData141;
#pragma DATA_SECTION (rtcpData141, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData141, 128)

static rtcpdata_t rtcpData142;
#pragma DATA_SECTION (rtcpData142, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData142, 128)

static rtcpdata_t rtcpData143;
#pragma DATA_SECTION (rtcpData143, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData143, 128)

static rtcpdata_t rtcpData144;
#pragma DATA_SECTION (rtcpData144, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData144, 128)

static rtcpdata_t rtcpData145;
#pragma DATA_SECTION (rtcpData145, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData145, 128)

static rtcpdata_t rtcpData146;
#pragma DATA_SECTION (rtcpData146, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData146, 128)

static rtcpdata_t rtcpData147;
#pragma DATA_SECTION (rtcpData147, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData147, 128)

static rtcpdata_t rtcpData148;
#pragma DATA_SECTION (rtcpData148, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData148, 128)

static rtcpdata_t rtcpData149;
#pragma DATA_SECTION (rtcpData149, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData149, 128)

static rtcpdata_t rtcpData150;
#pragma DATA_SECTION (rtcpData150, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData150, 128)

static rtcpdata_t rtcpData151;
#pragma DATA_SECTION (rtcpData151, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData151, 128)

static rtcpdata_t rtcpData152;
#pragma DATA_SECTION (rtcpData152, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData152, 128)

static rtcpdata_t rtcpData153;
#pragma DATA_SECTION (rtcpData153, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData153, 128)

static rtcpdata_t rtcpData154;
#pragma DATA_SECTION (rtcpData154, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData154, 128)

static rtcpdata_t rtcpData155;
#pragma DATA_SECTION (rtcpData155, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData155, 128)

static rtcpdata_t rtcpData156;
#pragma DATA_SECTION (rtcpData156, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData156, 128)

static rtcpdata_t rtcpData157;
#pragma DATA_SECTION (rtcpData157, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData157, 128)

static rtcpdata_t rtcpData158;
#pragma DATA_SECTION (rtcpData158, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData158, 128)

static rtcpdata_t rtcpData159;
#pragma DATA_SECTION (rtcpData159, "CHAN_INST_DATA:RTCP")
#pragma DATA_ALIGN   (rtcpData159, 128)

rtcpdata_t* const rtcpData[] = {
	&rtcpData0,
	&rtcpData1,
	&rtcpData2,
	&rtcpData3,
	&rtcpData4,
	&rtcpData5,
	&rtcpData6,
	&rtcpData7,
	&rtcpData8,
	&rtcpData9,
	&rtcpData10,
	&rtcpData11,
	&rtcpData12,
	&rtcpData13,
	&rtcpData14,
	&rtcpData15,
	&rtcpData16,
	&rtcpData17,
	&rtcpData18,
	&rtcpData19,
	&rtcpData20,
	&rtcpData21,
	&rtcpData22,
	&rtcpData23,
	&rtcpData24,
	&rtcpData25,
	&rtcpData26,
	&rtcpData27,
	&rtcpData28,
	&rtcpData29,
	&rtcpData30,
	&rtcpData31,
	&rtcpData32,
	&rtcpData33,
	&rtcpData34,
	&rtcpData35,
	&rtcpData36,
	&rtcpData37,
	&rtcpData38,
	&rtcpData39,
	&rtcpData40,
	&rtcpData41,
	&rtcpData42,
	&rtcpData43,
	&rtcpData44,
	&rtcpData45,
	&rtcpData46,
	&rtcpData47,
	&rtcpData48,
	&rtcpData49,
	&rtcpData50,
	&rtcpData51,
	&rtcpData52,
	&rtcpData53,
	&rtcpData54,
	&rtcpData55,
	&rtcpData56,
	&rtcpData57,
	&rtcpData58,
	&rtcpData59,
	&rtcpData60,
	&rtcpData61,
	&rtcpData62,
	&rtcpData63,
	&rtcpData64,
	&rtcpData65,
	&rtcpData66,
	&rtcpData67,
	&rtcpData68,
	&rtcpData69,
	&rtcpData70,
	&rtcpData71,
	&rtcpData72,
	&rtcpData73,
	&rtcpData74,
	&rtcpData75,
	&rtcpData76,
	&rtcpData77,
	&rtcpData78,
	&rtcpData79,
	&rtcpData80,
	&rtcpData81,
	&rtcpData82,
	&rtcpData83,
	&rtcpData84,
	&rtcpData85,
	&rtcpData86,
	&rtcpData87,
	&rtcpData88,
	&rtcpData89,
	&rtcpData90,
	&rtcpData91,
	&rtcpData92,
	&rtcpData93,
	&rtcpData94,
	&rtcpData95,
	&rtcpData96,
	&rtcpData97,
	&rtcpData98,
	&rtcpData99,
	&rtcpData100,
	&rtcpData101,
	&rtcpData102,
	&rtcpData103,
	&rtcpData104,
	&rtcpData105,
	&rtcpData106,
	&rtcpData107,
	&rtcpData108,
	&rtcpData109,
	&rtcpData110,
	&rtcpData111,
	&rtcpData112,
	&rtcpData113,
	&rtcpData114,
	&rtcpData115,
	&rtcpData116,
	&rtcpData117,
	&rtcpData118,
	&rtcpData119,
	&rtcpData120,
	&rtcpData121,
	&rtcpData122,
	&rtcpData123,
	&rtcpData124,
	&rtcpData125,
	&rtcpData126,
	&rtcpData127,
	&rtcpData128,
	&rtcpData129,
	&rtcpData130,
	&rtcpData131,
	&rtcpData132,
	&rtcpData133,
	&rtcpData134,
	&rtcpData135,
	&rtcpData136,
	&rtcpData137,
	&rtcpData138,
	&rtcpData139,
	&rtcpData140,
	&rtcpData141,
	&rtcpData142,
	&rtcpData143,
	&rtcpData144,
	&rtcpData145,
	&rtcpData146,
	&rtcpData147,
	&rtcpData148,
	&rtcpData149,
	&rtcpData150,
	&rtcpData151,
	&rtcpData152,
	&rtcpData153,
	&rtcpData154,
	&rtcpData155,
	&rtcpData156,
	&rtcpData157,
	&rtcpData158,
	&rtcpData159
};
//}

ADT_UInt16 numRtcpChannels = 160;
#pragma DATA_SECTION (numRtcpChannels, "POOL_ALLOC")


//{



/*=============================== */
//    Component dependent structures 



/*=============================== */
// Playback/record memory location. 

#pragma DATA_SECTION (playRec, "PLAYREC")
ADT_UInt16 playRec;




/*=============================== */
// Multi-core structures.
ADT_Word MaxFrameCnt   = 48;
ADT_Word MultiCoreCpuUsage[48];
ADT_Word MultiCoreCpuPeakUsage[48];
#pragma DATA_SECTION (MultiCoreCpuUsage, "SHARED_DATA_SECT:CpuUsage")
#pragma DATA_SECTION (MultiCoreCpuPeakUsage, "SHARED_DATA_SECT:CpuUsage")

far int DSPCore = 0;
#pragma DATA_SECTION (DSPCore, "PER_CORE_DATA:CoreID")
int DSPCoreLocks;
#pragma DATA_SECTION (DSPCoreLocks, "PER_CORE_DATA:Locks")
const int DSPTotalCores = CORE_CNT;
const ADT_UInt32 CoreOffsets[] = {
   0x10000000,
   0x11000000,
   0x12000000,
   0x13000000,
   0x14000000,
   0x15000000,
};

