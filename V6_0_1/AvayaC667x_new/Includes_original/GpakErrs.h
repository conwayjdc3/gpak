/*
 * Copyright (c) 2002 - 2007, Adaptive Digital Technologies, Inc.
 *
 * File Name: GpakErrs.h
 *
 * Description:
 *   This file contains DSP reply status codes used by G.PAK API functions to
 *   indicate specific errors. This file is common to both G.PAK API and
 *   G.PAK DSP code.
 *
 * Version: 1.0
 *
 * Revision History:
 *   10/17/01 - Initial release.
 *   07/03/02 - Updates for conferencing.
 *   06/15/04 - Tone type updates.
 *   1/2007   - Combined C54 and C64
 */

#ifndef _GPAKERRS_H  /* prevent multiple inclusion */
#define _GPAKERRS_H

// General DSP API interface API error codes
typedef enum GPAK_DspCommStat_t {
    DSPSuccess = 0,    DSPNullIFAddr,  DSPHostStatusError,  
    DSPCmdLengthError, DSPCmdTimeout,  DSPRplyLengthError, DSPRplyTimeout,
    DSPRplyValue,      DSPNoConnection
} GPAK_DspCommStat_t;


//{ System configuration and status
// Write System Parameters reply status codes.
typedef enum GPAK_SysParmsStat_t {
    Sp_Success = 0,             /* System Parameters written successfully */

    /* AGC parameters errors. */
    Sp_AgcNotConfigured = 1,    /* AGC is not configured in system */
    Sp_BadAgcTargetPower,   /* invalid AGC Target Power value */
    Sp_BadAgcLossLimit,     /* invalid AGC Maximum Attenuation value */
    Sp_BadAgcGainLimit,     /* invalid AGC Max Positive Gain value */
    Sp_BadAgcLowSignal,

    /* VAD parameters errors. */
    Sp_VadNotConfigured = 10,    /* VAD is not configured in system */
    Sp_BadVadNoiseFloor,    /* invalid VAD Noise Floor value */
    Sp_BadVadHangTime,      /* invalid VAD Report Delay Time value */
    Sp_BadVadWindowSize,    /* invalid VAD Analysis Window Size value */

    /* PCM Echo Canceller parameters errors. */
    Sp_PcmEcNotConfigured = 20,     /* PCM Ecan is not configured in system */
    Sp_BadPcmEcTapLength,     /* invalid PCM Ecan Num Taps */
    Sp_BadPcmEcNlpType,       /* invalid PCM Ecan NLP Type */
    Sp_BadPcmEcDblTalkThresh, /* invalid PCM Ecan Double Talk threshold */
    Sp_BadPcmEcNlpThreshold,  /* invalid PCM Ecan NLP threshold */
    Sp_BadPcmEcCngThreshold,  /* invalid PCM Ecan CNG Noise */
    Sp_BadPcmEcAdaptLimit,    /* invalid PCM Ecan Adapt Limit */
    Sp_BadPcmEcCrossCorrLim,  /* invalid PCM Ecan Cross Corr Limit */
    Sp_BadPcmEcNumFirSegs,    /* invalid PCM Ecan Num FIR Segments */
    Sp_BadPcmEcFirSegLen,     /* invalid PCM Ecan FIR Segment Length */
    Sp_BadPcmEcNLPUpperConv,
    Sp_BadPcmEcNLPUpperUnConv,
    Sp_BadPcmEcMaxSupp,
    Sp_BadPcmEcFIRTapCheck,
    Sp_BadPcmEcMaxDblTalkThresh,

    /* Packet Echo Canceller parameters errors. */
    Sp_PktEcNotConfigured = 40,    /* Pkt Ecan is not configured in system */
    Sp_BadPktEcTapLength,     /* invalid Pkt Ecan Num Taps */
    Sp_BadPktEcNlpType,       /* invalid Pkt Ecan NLP Type */
    Sp_BadPktEcDblTalkThresh, /* invalid Pkt Ecan Double Talk threshold */
    Sp_BadPktEcNlpThreshold,  /* invalid Pkt Ecan NLP threshold */
    Sp_BadPktEcCngThreshold,  /* invalid Pkt Ecan CNG Noise */
    Sp_BadPktEcAdaptLimit,    /* invalid Pkt Ecan Adapt Limit */
    Sp_BadPktEcCrossCorrLim,  /* invalid Pkt Ecan Cross Corr Limit */
    Sp_BadPktEcNumFirSegs,    /* invalid Pkt Ecan Num FIR Segments */
    Sp_BadPktEcFirSegLen,     /* invalid Pkt Ecan FIR Segment Length */
    Sp_BadPktEcNLPUpperConv,
    Sp_BadPktEcNLPUpperUnConv,
    Sp_BadPktEcMaxSupp,
    Sp_BadPktEcFIRTapCheck,
    Sp_BadPktEcMaxDblTalkThresh,

    /* Conference parameters errors. */
    Sp_BadConfDominant = 60,       /* invalid Num Dominant Conference Members */
    Sp_BadMaxConfNoiseSup,    /* invalid Max Conference Noise Suppress */

    //  Noise Suppression errors 
    Sp_NoiseSuppressionNotConfigured = 70,
    Sp_BadVadNoiseCeiling,
    Sp_BadNoiseAttenuation,

    Sp_CidNotConfigured = 80,
    Sp_BadCidNumSeizureBytes,
    Sp_BadCidNumMarkBytes,
    Sp_BadCidNumPostBytes,
    Sp_BadCidFskType,
    Sp_BadCidFskLevel

} GPAK_SysParmsStat_t;

// Write AEC Parameters reply status codes.
typedef enum GPAK_AECParmsStat_t {
    AEC_Success = 0,             // AEC Parameters written successfully

    /* AEC parameters errors. */
    AEC_NotConfigured,           // AEC not configured
    AEC_BadTailLength,
    AEC_BadTxNLPThreshold,
    AEC_BadTxLoss,
    AEC_BadRxLoss,
    AEC_BadTargetResidualLevel,
    AEC_BadMaxRxNoiseLevel,
    AEC_BadWorstExpectedERL,
    AEC_BadAgcMaxGain,
    AEC_BadAgcMaxLoss,
    AEC_BadAgcTargetLevel,
    AEC_BadAgcLowSigThresh,
    AEC_BadMaxTrainingTime,
    AEC_BadRxSaturateLevel,
    AEC_BadTrainingRxNoiseLevel,
    AEC_BadFixedGain,
    AEC_BadNoiseReduction
} GPAK_AECParmsStat_t;

// Arbitrary tone detector status codes
typedef enum GPAK_ConfigArbToneStat_t {
    Cat_Success = 0,           /* Arb Detector Configuration successful  */
    Cat_InvalidID = 1,         /* Invalid Arb Detector Configuration Id  */
    Cat_InvalidNumFreqs = 2,   /* Invalid number of Frequencies */
    Cat_InvalidNumTones = 3,   /* Invalid number of tones */
    Cat_InvalidFreqs = 4,      /* Invalid Frequency specification */
    Cat_InvalidTones = 5,      /* Invalid tone specification */
    Cat_InvalidIndex = 6       /* Invalid return tone index */
} GPAK_ConfigArbToneStat_t;

// Arbitrary tone detector status codes
typedef enum GPAK_ActiveArbToneStat_t {
    Aat_Success = 0,           /* Arb Detector Configuration successful  */
    Aat_InvalidID = 1,         /* Invalid Arb Detector Configuration Id  */
    Aat_NotConfigured = 2      /* The detector has not been configured  */
} GPAK_ActiveArbToneStat_t;

// DTMF parameter configuration status codes
typedef enum GPAK_DtmfDetConfigStat_t {
   Dc_Success = 0,                 // success
   Dc_ChanActive = 1,              // A channel was active during configuration
   Dc_InvalidSigLev = 2,           // invalid signal level
   Dc_InvalidFreqDev = 3,          // invalid frequency deviation
   Dc_invalidTwist = 4,            // invalid twist
   Dc_DtmfDetNotConfigured = 5,    // dtmf detector not configured in build
   Dc_invalidMaxWaivers   = 6,     // too many waivers
   Dc_invalidToneQuality  = 7      // Tone quality value out of range
} GPAK_DtmfDetConfigStat_t;

// Speex configuration message status codes
typedef enum GPAK_SpeexParamsStat_t  {
   SpxSuccess = 0,
   SpxInvalidQuality = 1,
   SpxInvalidComplexity = 2
} GPAK_SpeexParamsStat_t;

// VLAN configuration message status codes
typedef enum GPAK_VlanStat_t {
   vlanSuccess = 0,
   vlanIdxOutOfRange = 1,
   vlanActive = 2,
   vlanDuplicate = 3,
   vlanPriorityOutOfRange = 4,
   vlanIDOutOfRange = 5,
   vlanUnAvailable = 6
} GPAK_VlanStat_t;

// Configure Serial Ports reply status codes.
typedef enum GPAK_PortConfigStat_t {
    Pc_Success = 0,             /* serial ports configured successfully */
    Pc_ChannelsActive = 1,      /* unable to configure while channels active  */
    Pc_TooManySlots0,
    Pc_TooManySlots1,
    Pc_TooManySlots2,

    Pc_NoSlots0,
    Pc_NoSlots1,
    Pc_NoSlots2,
    Pc_InvalidSlots0,
    Pc_InvalidSlots1,

    Pc_InvalidSlots2=10,
    Pc_UnconfiguredAPort1,    /* unconfigured mcAsp */
    Pc_UnconfiguredAPort2, 
    Pc_UnconfiguredASerPort1, /* unconfigured serializer */
    Pc_UnconfiguredASerPort2,

    Pc_UnequalTxRxASerPort1,  /* unequal # tx-rx serializers*/
    Pc_UnequalTxRxASerPort2,
    Pc_TooManyASlots1,        /* too many slots selected*/
    Pc_TooManyASlots2,     
    Pc_NoASlots1,             /* no audio slots selected*/

    Pc_NoASlots2=20,          
    Pc_InvalidCompandMode0,   /* invalid companding mode */
    Pc_InvalidCompandMode1,
    Pc_InvalidCompandMode2,
    Pc_InvalidDataDelay0,     /* invalid data delay for ports */

    Pc_InvalidDataDelay1,
    Pc_InvalidDataDelay2,
    Pc_InvalidBlockCombo0,         /* invalid combination of channel blocks */
    Pc_InvalidBlockCombo1,
    Pc_InvalidBlockCombo2,

    Pc_NoInterrupts=30,   
    Pc_ClockDivideErr,      // HiRes or Normal clock divisor value too large
    Pc_SlotSizeErr,         // TDM Slot size is too large
    Pc_SampleSizeErr,       // Sample size is too large
    Pc_SizeInconsistency    // Sample size is larger than slot size

} GPAK_PortConfigStat_t;

// Configure Conference reply status codes.
typedef enum GPAK_ConferConfigStat_t {
    Cf_Success = 0,                 /* conference configured successfully */
    Cf_InvalidConference = 1,       /* invalid Conference Id */
    Cf_ConferenceActive = 2,        /* conference is currently active */
    Cf_InvalidFrameSize = 3,        /* invalid Frame Size */
    Cf_InvalidToneTypes = 4,        /* invalid Tone Detect Types */
    Cf_InvalidCore      = 5         /* invalid DSP core */
} GPAK_ConferConfigStat_t;
//}

//{ Channel configuration and status
/* Configure Channel reply status codes. */
typedef enum GPAK_ChannelConfigStat_t {
    Cc_Success = 0,             /* channel configured successfully */
    Cc_InvalidChannelType,      /* invalid Channel Type */
    Cc_InvalidChannelA,         /* invalid Channel A Id */
    Cc_ChannelActiveA,          /* Channel A is currently active */
    Cc_InvalidChannelB,         /* invalid Channel B Id */
    
    Cc_ChannelActiveB,          /* (5) Channel B is currently active */
    Cc_InvalidInputPortA,       /* invalid Input A Port */
    Cc_InvalidInputSlotA,       /* invalid Input A Slot */
    Cc_BusyInputSlotA,          /* busy Input A Slot */
    Cc_InvalidOutputPortA,      /* invalid Output A Port */
    
    Cc_InvalidOutputSlotA,     /* (10) invalid Output A Slot */
    Cc_BusyOutputSlotA,        /* busy Output A Slot */
    Cc_InvalidInputPortB,      /* invalid Input B Port */
    Cc_InvalidInputSlotB,      /* invalid Input B Slot */
    Cc_BusyInputSlotB,         /* busy Input B Slot */
    
    Cc_InvalidOutputPortB,     /* (15) invalid Output B Port */
    Cc_InvalidOutputSlotB,     /* invalid Output B Slot */
    Cc_BusyOutputSlotB,        /* busy Output B Slot */
    Cc_InvalidInputCktSlots,   /* invalid Input Circuit Slots */
    Cc_InvalidInCktPortSize,   /* invalid Input Circuit port word size */
    
    Cc_InvalidOutputCktSlots,  /* (20) invalid Output Circuit Slots */
    Cc_InvalidOutCktPortSize,  /* invalid Output Circuit port word size */
    Cc_InvalidFrameSize,       /* invalid Frame Size */
    Cc_InvalidPktInCodingA,    /* invalid Packet In A Coding */
    Cc_InvalidPktOutCodingA,   /* invalid Packet Out A Coding */
    
    Cc_InvalidPktInSizeA,      /* (25) invalid Packet In A Frame Size */
    Cc_InvalidPktOutSizeA,     /* invalid Packet Out A Frame Size */
    Cc_InvalidPktInCodingB,    /* invalid Packet In B Coding */
    Cc_InvalidPktOutCodingB,   /* invalid Packet Out B Coding */
    Cc_InvalidPktInSizeB,      /* invalid Packet In B Frame Size */
    
    Cc_InvalidPktOutSizeB,     /* (30) invalid Packet Out B Frame Size */
    Cc_PcmEchoCancelNotCfg,    /* PCM Echo Canceller not available */
    Cc_PktEchoCancelNotCfg,    /* Packet Echo Canceller not available */
    Cc_InvalidToneTypesA,      /* invalid Channel A Tone Types */
    Cc_InvalidToneTypesB,      /* invalid Channel B Tone Types */
    
    Cc_VadNotConfigured,       /* (35) VAD not configured */
    Cc_InsuffAgcResourcesAvail,  /* Insufficient AGC resources available */
    Cc_InvalidCktMuxFactor,    /* invalid Circuit Mux Factor */
    Cc_InvalidConference,      /* invalid Conference Id */
    Cc_ConfNotConfigured,      /* conference was not configured (setup) */
    
    Cc_ConferenceFull,         /* (40) conference is full */
    Cc_ChanTypeNotConfigured,  /* channel type was not configured */
    Cc_NoiseSuppressionNotConfigured,
    Cc_InvalidInputPinA,
    Cc_InvalidInputPinB,
    
    Cc_InvalidOutputPinA,      /* (45) */
    Cc_InvalidOutputPinB,
    Cc_AECEchoCancelNotCfg,    
    Cc_FaxRelayNotConfigured,
    Cc_FaxRelayInvalidMode,

    Cc_FaxRelayInvalidTransport, /* (50) */
    Cc_InsuffTGResourcesAvail,    /* not enough tone gen resources */   
    Cc_AgcNotConfigured,
    Cc_InsuffTDResourcesAvail,
    Cc_TxCIDNotAvail,

    Cc_RxCIDNotAvail,             /* (55) */
    Cc_LbcNotCfg,
    Cc_InsuffLbResourcesAvail,
    Cc_InsuffBuffsAvail,
    Cc_InvalidCore,

    Cc_InsuffRateConverters       //  60

} GPAK_ChannelConfigStat_t;

// Memory Test and status
/* Memory Test reply status codes. */
typedef enum GPAK_MemoryTestResults_t {
   MT_Success,             /* Memory Test successful */
   MT_DataBusFail,
   MT_AddressBusFail,
   MT_DeviceFail,
   MT_Undefined
} GPAK_MemoryTestResults_t;

/* Tear Down Channel reply status codes. */
typedef enum GPAK_TearDownChanStat_t {
    Td_Success = 0,                 /* channel torn down successfully */
    Td_InvalidChannel = 1,          /* invalid Channel Id */
    Td_ChannelNotActive = 2,        /* channel is not active */
    Td_TimeOut = 3                  /* channel teardown timed out */
} GPAK_TearDownChanStat_t;

/* Channel Status reply status codes. */
typedef enum GPAK_ChannelStatusStat_t {
    Cs_Success = 0,                 /* channel status obtained successfully */
    Cs_InvalidChannel = 1           /* invalid Channel Id */
} GPAK_ChannelStatusStat_t;

// RTP configuration reply codes
typedef enum GPAK_RTPConfigStat_t {
  RTPSuccess,
  RTPNotConfigured,
  RTPJitterModeError,
  RTPTargetError,
  RTPChannelError,
  RTPChannelActive,
  RTPDestinationError,
  RTPSrcPortError,
  RTPVlanError
} GPAK_RTPConfigStat_t;

// RTP status replay codes
typedef enum GPAK_RTP_Stat_Rply_t {
  RTPStat_Success,
  RTPStat_NotConfigured,
  RTPStat_ChannelError,
  RTPStat_ChannelInactive
} GPAK_RTP_Stat_Rply_t;

typedef enum GPAK_RTCPBye_t {
  RTCPBye_Success,
  RTCPBye_InvalidChannel,
  RTCPBye_NotConfigured,
  RTCPBye_ChannelError,
  RTCPBye_ChannelInactive
} GPAK_RTCPBye_t;
// Definition of SRTP/SRTCP status codes.
typedef enum GpakSrtpStatus_t {
   GPAK_SRTP_SUCCESS = 0,              // success
   GPAK_SRTP_INVALID_ENCRYPT_TYPE = 3, // invalid Encryption type
   GPAK_SRTP_INVALID_AUTH_TYPE = 4,    // invalid Authentication type
   GPAK_SRTP_UNSUPPORTED_CFG = 7,      // unsupported Encryption/Authentication
   GPAK_SRTP_UNCONFIGURED_STREAM = 8,  // SRTP stream not configured
   GPAK_SRTP_INVALID_DIRECTION = 10,   // invalid direction field
   GPAK_SRTPNotConfigured,             // SRTP not configed yet
   GPAK_SRTPNotSupported,             // SRTP not supported in the build 
   GPAK_SRTPChannelError,             // chan id out of range
   GPAK_SRTPChannelActive,            // Gpak Channel already acvtived, SRTP config must be called before channelConfig
   GPAK_SRTPChannelInactive,           // SRTP Channel not acvtived, gpakSRTPNewKey must be called after channelConfig
   GPAK_SRTP_INVALID_MKI_LENGTH  // MKI length too large
} GpakSrtpStatus_t;

//}

//{ Channel control
// Insert RTP tone pkt replay status codes
typedef enum  GPAK_InsertEventStat_t {
	Ti_Success =0,
	Ti_InvalidChannel,
	Ti_InactiveChannel,
	Ti_NotConfigured,
	Ti_PyldTooLarge,
	Ti_ToneGenPktBusy,
   Ti_ForwardingNotEnabled
} GPAK_InsertEventStat_t;

// tone generation replay status codes
typedef enum  GPAK_ToneGenStat_t {
    Tg_Success = 0,                 // success
    Tg_InvalidChannel = 1,          // invalid channel Id
    Tg_toneGenNotEnabledA = 2,      // A device tone gen not enabled
    Tg_toneGenNotEnabledB = 3,      // B device tone gen not enabled
    Tg_InvalidToneGenType = 4,      // invalid tonegen type
    Tg_InvalidFrequency = 5,        // invalid tone gen frequency
    Tg_InvalidOnTime = 6,           // invalid on-time
    Tg_InvalidLevel = 7,            // invalid level
    Tg_InactiveChannel = 8,         // inactive channel
    Tg_InvalidCore = 9,
	Tg_ToneGenPktBusy
} GPAK_ToneGenStat_t;

// playback and record reply codes
typedef enum GPAK_PlayRecordStat_t {
   PrSuccess = 0,
   PrInvalidChannel,
   PrInactiveChannel,
   PrUnsupportedCommand,
   PrUnsupportedRecordMode,
   PrToneDetectionUnsupported,
   PrInvalidDevice,
   PrInvalidStopTone,
   PrStartAddrOutOfRange,
   PrInvalidAddressAlignment,
   PrRateConvertersUnavailable
} GPAK_PlayRecordStat_t;

// Algorithm control return status codes
typedef enum  GPAK_AlgControlStat_t {
    Ac_Success = 0,           /* algorithm control is successfull */
    Ac_InvalidTone = 1,       /* invalid tone type specified */
    Ac_InvalidChannel = 2,    /* invalid channel identifier */
    Ac_InactiveChannel = 3,   /* channel is inactive */
    Ac_InvalidCode = 4,       /* invalid algorithm control code */
    Ac_ECNotEnabled = 5,      /* echo canceller was not allocated */
    Ac_ECInvalidNLP = 6,      /* Invalid NLP Update setting */
    Ac_PrevCmdPending = 7,    /* The previous control hasn't completed yet */
    Ac_TooManyActiveDetectorsA = 8, /* too many detectors activated on A-side */
    Ac_TooManyActiveDetectorsB = 9, /* too many detectors activated on B-side */
    Ac_InsuffDetectorsAvail = 10,   /* Not enough tone detector instances available */
    Ac_DetectorNotCfgrd = 11  /* selected detector is not configured */
} GPAK_AlgControlStat_t;

// Read AGC Power return status codes
typedef enum GPAK_AGCReadStat_t {
    Ar_Success = 0,           /* AGC read is successfull on A and/or B side */
    Ar_InvalidChannel = 1,    /* invalid channel identifier */
    Ar_InactiveChannel = 2,   /* inactive channel identifier */
    Ar_AGCNotEnabled = 3      /* AGC disabled on both A and B sides */
} GPAK_AGCReadStat_t;

// Tx CID Payload status codes
typedef enum GPAK_TxCIDPayloadStat_t {
    Scp_Success = 0,           /* Cid Tx Payload Transfer successful  */
    Scp_InvalidChannel = 1,    /* invalid channel identifier */
    Scp_InactiveChannel = 2,   /* inactive channel identifier */
    Scp_TxCidNotEnabled = 3,   /* TxCID disabled on selected Device side */
    Scp_TxPayloadTooBig = 4,    /* TxCID payload too large for DSP Msg Buffer */
    Scp_Cid2NotEnabled =  5,   /* CID type 2 not supported in the Gpak build */
    Scp_TxCid2SeizureNotZero =  6,   /* CID type 2 Tx Seizure bit error (should be zero) */
    Scp_TxCid2MarkInvalid =  7    /* CID type 2 Tx Mark bit error (not in range) */
} GPAK_TxCIDPayloadStat_t;

// Dtmf Dialing replay status codes
typedef enum  GPAK_DtmfDialStat_t {
    Dd_Success = 0,                 // success
    Dd_InvalidChannel = 1,          // invalid channel Id
    Dd_ToneGenNotEnabledA = 2,      // A device tone gen not enabled
    Dd_ToneGenNotEnabledB = 3,      // B device tone gen not enabled
    Dd_InvalidDtmfDigit = 4,        // invalid dtmf digit
    Dd_InvalidOnTime = 5,           // invalid on-time
    Dd_InvalidInterdigitTime = 6,   // invalid interdigit time
    Dd_InvalidLevel = 7,            // invalid level
    Dd_InactiveChannel = 8,         // inactive channel
    Dd_InvalidCore = 9,             // Invalid Core ID
    Dd_DialInProgress = 10,         // Dtmf Dial already in progress
    Dd_InvalidNumDigits = 11,       // Invalid number of digits
    Dd_DtmfDialNotEnabledA = 12,    // A device dtmf dial not enabled
    Dd_DtmfDialNotEnabledB = 13     // B device dtmf dial not enabled
} GPAK_DtmfDialStat_t;
//}

/* Test Mode reply status codes. */
typedef enum GPAK_TestStat_t {
    Tm_Success = 0,                 /* test mode successfull */
    Tm_InvalidTest = 1,             /* invalid Test Mode Id */
    Tm_InvalidParm = 2,             /* invalid Test Parameter */
    Tm_CustomError = 3              /* custom error code */
} GPAK_TestStat_t;

// Read AEC Channel Status return status codes.
typedef enum
{
    aecChanRead_Success = 0,            /* success, AEC status obtained */
    aecChanRead_InvalidChannel = 1,     /* invalid channel identifier */
    aecChanRead_InactiveChannel = 2,    /* inactive channel identifier */
    aecChanRead_NotEnabled = 3          /* AEC not enabled */
} GPAK_AecChanReadStat_t;

// Read LEC Channel Status return status codes.
typedef enum
{
    lecChanRead_Success = 0,            /* success, LEC status obtained */
    lecChanRead_InvalidChannel = 1,     /* invalid channel identifier */
    lecChanRead_InactiveChannel = 2,    /* inactive channel identifier */
    lecChanRead_NotEnabled = 3          /* LEC not enabled */
} GPAK_LecChanReadStat_t;

typedef enum
{ 
   Lw_Success = 0,               /* wavefile loaded success */
   Lw_NotEnoughStorage = 1,      /* not enough memory to store the file */
   Lw_InvalidSampleRate = 2,     /* sample rate not supported */
   Lw_DuplicateId = 3,           /* Playback ID already in list */
   Lw_IdNotFound = 4,            /* playback Id not found in list */
   Lw_OutOfSequence = 5,         /* download block arrived out of sequence */
   Lw_AlreadyLoaded = 6,         /* download already done */
   Lw_InvalidLength = 7,         /* download length exceeds expected value */
   Lw_LastBlockMissing = 8       /* last block marker not set */
} GPAK_LoadWavefileParmsStat_t;

typedef enum
{ 
   Pw_Success = 0,               /* wavefile playback success */
   Pw_InvalidChannel = 1,        /* invalid channel ID */
   Pw_ChannelInactive = 2,       /* channel not active */
   Pw_IdNotFound = 3,            /* playback Id not found in list */
   Pw_FileNotDownloaded = 4,     /* file was not downloaded */
   Pw_InvalidCommand = 5,        /* invalid playback command */
   Pw_InvalidDirection = 6,      /* invalid playback direction */
   Pw_InvalidSampleRate = 7,     /* channel sample rate different than file's */
   Pw_PlaybackActive = 8         /* playback already in progress */
} GPAK_PlayWavefileStat_t;

// Configure ED137B return status codes.
typedef enum
{
    ED137B_Success = 0,            /* success, ED137B status obtained */
    ED137B_InvalidChannel = 1,     /* invalid channel identifier */
    ED137B_InactiveChannel = 2,    /* inactive channel identifier */
    ED137B_NotEnabled = 3,         /* ED137B not enabled */
    ED137B_PendingCommand = 4,     /* ED137B Previous Command not Processed */
    ED137B_Invalid_Param = 5       /* ED137B Invalid Parameter */
} GPAK_ED137B_Stat_t;

#endif  /* prevent multiple inclusion */
