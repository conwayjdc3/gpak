/*
 * Copyright (c) 2001-2007, Adaptive Digital Technologies, Inc.
 *
 * File Name: GpakDefs.h
 *
 * Description:
 *   This file contains common definitions related to G.PAK application
 *   software.
 *
 * Version: 1.0
 *
 * Revision History:
 *   11/07/01 - Initial release.
 *   05/2005  - Added GSM_EFR
 *    2/2007  - Converged C54 and C64
 *
 */

#ifndef _GPAKDEF_H  /* prevent multiple inclusion */
#define _GPAKDEF_H

#ifndef NO_DAIS
   #define NO_DAIS
#endif
#include <std.h>
#include "adt_typedef.h"
#include "GpakEnum.h"
#include "GpakErrs.h"
#include "CircBuff.h"
#include "sysmem.h"
#include "sysconfig.h"
#include "GpakIf.h"
#include "GpakBios.h"
#include "ED137Defs.h"

#include GPAKVERSION_H

//#define STATIC_BUFF_ALLOC
#if (DSP_TYPE != 64)
#define far
#endif

#define PACK16TO32(high, low) ((ADT_UInt32)(((ADT_UInt32)(high) << 16) | (ADT_UInt32)(low)))


//==========================================================================

//  Debug logging

//{==========================================================================
#ifdef _DEBUG
int logTime (ADT_UInt32 type);
void ptrTrace (CircBufInfo_t *pTdmOut, CircBufInfo_t *pEcFar, int chan);
void ecSigTrace (int port, int slot, int code, ADT_Int16 *pBuf1, ADT_Int16 *pBuf2, int len);

#define LogTransfer(type,srcC,src,destC,dest,len)  \
  logTransfer (type, (void *) (srcC), (void *) (src), (void *) (destC), (void *) (dest), len);

int logTransfer (ADT_Int32 type, void *srcC, void *src, void *destC, void *dest, int len);
#else
#define LogTransfer(type,srcC,src,destC,dest,length)
#define logTime(type)
#define ptrTrace(pTdmOut, pEcFar, chan)
#define ecSigTrace(port, slot, code, pBuf1, pBuf2, len)
#endif

//}==========================================================================

//  Voice algorithms

//{==========================================================================
//-----------  Voice activity detection
#include inc_vadcng_user
#if (DSP_TYPE == 54)
   typedef  VadChannelInst_t VADCNG_Instance_t; 
   #define  VADCNG_ADT_init  VadInitialize 
   #define  VADCNG_ADT_vad   Vad
   #define  VADCNG_ADT_cng   CNG 
#endif
#define VAD_INIT_RELEASE 0x4000
//==================================================
//-----------  Automatic gain control

#include inc_agc_user

//==================================================
//-----------  Noise cancellation

#include inc_NCAN_user
typedef struct NSuppress_t {
    ADT_UInt16      Offset;    // Current offset into noiseRemnant buffer
    short          *Remnant;   // pointer to noise remnant buffer for unaligned frames
    NCAN_Channel_t *Bufr;      // Noise suppression channel instance buffer
} NSuppress_t;



//==================================================
//-------  Playback/record
#define PlayRecordUpConvertBit 0x8000
typedef struct playRecInfo_t {
   CircBufInfo_t         Circ;         // pointer to circ info buffer

   GpakPlayRecordMode_t  mode;         // data format mode
   GpakPlayRecordState_t state;        // playback/recorder state
   GpakToneCodes_t       stopTone;     // tone index used to stop record
   void                  *blockStart;  // pointer to start of memory block

   // Rate conversion instance.
   void       *CvtInst; 
   ADT_UInt16 CvtIndex;                // High bit indicates up conversion.
   ADT_UInt16 CvtFrame;                // Samples at faster rate for play/record conversions
} playRecInfo_t;
#define PLAY_REC_HEADER_LEN_WORDS 9 // circbuffer struct size + 1 short word

//==================================================
//-----------  Tone generation

/*
   typedef  int TGParams_t; 
   typedef  int TGInstance_t; 
   typedef  int TGParams_1_t; 
   #define  TG_ADT_init(a, b)
   #define TG_ADT_init_1(a, b)
   #define TG_ADT_generate(a,b,c)
*/
// note: C55X does not support the latest Gpak TG APIs
// To add the TG modules, we must update the C55X TG algorithm
#include inc_TG_USER
#include inc_tonerelayGen

typedef struct DtmfDialInfo_t {
    ADT_UInt16 Digits[16];   // array of digits
    ADT_UInt16 NumDigits;    // number of digits to dial
    ADT_UInt16 CurrentDigit; // current digit to dial
    ADT_UInt16 NSampsOn;     // number of samples in tone-on portion
    ADT_UInt16 NSampsOff;    // number of samples in tone-off portion
    ADT_UInt16 NSampsGen;    // number of samples generated thus far in active portion
    ADT_Int16  Level[2];     // level in dBm of each tone
    ADT_UInt16 Rate;         // 16000 or 8000
    DtmfDialState State;     // dialing state
} DtmfDialInfo_t;

// tone generation info structure
typedef struct TGInfo_t {
   ADT_UInt16       index;             // index to tonegen instance
   GpakToneGenCmd_t cmd;               // tone command
   GpakActivation   active;            // generator is active
   GpakActivation   regenActive;       // relay generator is active
   GpakActivation   update;            // update the tone generator
   GpakActivation   dtmfDial;          // dtmfDial is active
   TGInstance_t         *toneGenPtr;     // Tone Generate instance structure
   TGParams_1_t         *toneGenParmPtr; // Tone Generate param structure
   TRGenerateInstance_t *toneRlyGenPtr;  // Relay generation instance
   TRGenerateEvent_t    *toneRlyEvt;     // Relay generation events
   DtmfDialInfo_t  *dtmfDialParmPtr;     // pointer to dtmf Dial param structure
} TGInfo_t;
#define TGNULL  { VOID_INDEX, ToneGenStop, Disabled, Disabled, Disabled, Disabled, NULL, NULL, NULL, NULL, NULL }

//==================================================
//-----------  Tone detection
#include inc_tonedet_user

// Arbitrary Tone detector
// Gpak C55X use the same arb ToneDet header as C64X, to satisfy the compiler
// But there is no actual DSP module support this yet
#include inc_arbTd_user

#define ARBIT_ADT_toneDetect TDLOWMEM_ADT_toneDetect



#if (DSP_TYPE == 54)
  typedef int TDScratch_t;
  typedef int FAXCNGInstance_t;
  typedef int FaxCEDChannel_t;
#elif (DSP_TYPE == 55)
  typedef int TDScratch_t;
  typedef int FAXCNGInstance_t;
  typedef int FaxCEDChannel_t;
  typedef int  FAXCNGScratch_t;
#elif (DSP_TYPE == 64)
  #include inc_FAXCNG_user
  #include inc_FAXCNG
  #include inc_FAXCEDdet
#endif

typedef struct TDInst_t {
    TDInstance_t       *ToneDetInst;   // tone detection instance
    FAXCNGInstance_t   *CngDetInst;   // FAX detection
    FaxCEDChannel_t    *CedDetInst;
    ARBIT_TDInstance_t *ArbToneInst;      // Generic tone detection
    ADT_UInt32         ArbToneCfgId;      
    ADT_UInt40         ToneTimeStamp;
    ADT_UInt40         ToneTimeStampToneDet;
} TDInst_t;

// tone detection info structure
typedef struct toneDetInfo_t {
    int                 ChannelId;   // channel id
    GpakDeviceSide_t    deviceSide;  // device A/B
    GpakToneTypes       toneTypes;   // Types of tone: dtmf, mfr1, mfr2f, mfr2r, cprg
    int                 FrameSize8k; // frame size
    TDInst_t            *TDInstances;
    ADT_PCM16           *DTMFBuff;   // pre NLP buffer
    ADT_PCM16           *pcmBuff;    // output buffer for DTMF suppression
    playRecInfo_t       *Rec;        // record info struct
} toneDetInfo_t;



//==================================================
//---------- Tone relay
//  NOTE: In order of priority
#define FWD_PACKET_NONE     0
#define FWD_PACKET_CNG      1  // CNG packet from network
#define FWD_PACKET_TONE_MSG 2  // Tone packet from messaging
#define FWD_PACKET_TONE     3  // Tone packet from network

#define REMOTE_PACKET_NOT_DETECTED FWD_PACKET_NONE


#include inc_tonerelayDet


//==================================================
//---------  Echo cancellation

#define G168_PCM 0  // PCM echo canceller
#define G168_PKT 1  // Packet echo canceller 
#define ACOUSTIC 2  // Acoustic echo canceller

#if (DSP_TYPE == 55)
#define EC_VARIANT EC_VARIANT_NEC
#endif

#ifndef MAX_FRAME_SIZE
   #define MAX_FRAME_SIZE 8
#endif
#ifndef MAX_FIR_SEGMENTS
   #define MAX_FIR_SEGMENTS 1
#endif
#ifndef MAX_FIR_SEGMENT_LENGTH
   #define MAX_FIR_SEGMENT_LENGTH 128
#endif
#ifndef MAX_TAP_LENGTH
   #define MAX_TAP_LENGTH 128
#endif

#define G168_OPTS_H 1   // Disable echo canceller defaults

#ifdef AEC_SUPPORTED
#include inc_aec_user
#else
typedef int *AECG4_Handle;
typedef int *IAECG4_Handle;
typedef struct IAECG4_Params {
	ADT_UInt16 	frameSize;
	ADT_Int32	 	samplingRate;
	ADT_Int32		maxAudioFreq;
	ADT_Int16   bulkDelaySamples;
} IAECG4_Params;

#endif

#include inc_aeq_user

#include inc_g168_user
#if (DSP_TYPE == 55)
   #define NLP_CNG1 5
   typedef G168ChannelInst_t G168ChannelInstance_t;
   typedef int G168_BG_SA_State_t;
#endif
#if (DSP_TYPE == 64)
   typedef int G168_BG_SA_State_t;
#endif

//==================================================
//--------- Conferencing

#ifndef MAX_CHANNELS_PER_CONFERENCE
   #define MAX_CHANNELS_PER_CONFERENCE 1
#endif
#ifndef MAX_CONF_FRAME_SIZE
   #define MAX_CONF_FRAME_SIZE 240
#endif
#ifndef MAX_DOMINANT_N
   #define MAX_DOMINANT_N 3
#endif

#define CONF_BUILD_TYPE 3       // Mixed build
#include inc_conf_ADT_user


//==================================================
//------- T.38 FAX Relay
#define MAX_FAX_PKT 80
#define END_OF_FAX_TIMEOUT_MS 7000	// 7 seconds of bi-directional silence ends a fax

#if (DSP_TYPE == 54)
   typedef int  t38Packet_t;
   #define RLY_CHANNEL_SIZE 1
#elif (DSP_TYPE == 55)
   typedef int  t38Packet_t;
   #define RLY_CHANNEL_SIZE 1
#elif (DSP_TYPE == 64)
#include inc_ti_t38_user

   #define NSECTIONS 250
   #define FAX_RELAY 1
   #include inc_faxrelay

   #ifdef _DEBUG
   typedef struct FAXStats {
       ADT_UInt16 FromRelay;    // count of packets taken from the relay for delivery to host
       ADT_UInt16 NotInProgress;// count of packets dropped prior to V.21 
       ADT_UInt16 ToNetwork;    // count of packets delivered to host
       ADT_UInt16 HostFull;     // count of packets held waiting for host to free buffer
       ADT_UInt16 MaxPerFrame;  // maximum count of packets removed from relay during one frame

       ADT_UInt16 FromNetwork;   // count of packets received from the network
       ADT_UInt16 ToRelay;       // count of packets delivered to relay
       ADT_UInt16 RelayFull;	   // count of undeliverable packets because relay full
       ADT_UInt16 TooLarge;      // count of undeliverable packets because packet too large

       ADT_UInt16 healthErrors;  // count  of health errors reported by relay code
       ADT_UInt32 health;        // relay health bit field

   } FAXStats_t;
   #else
   typedef struct FAXStats {
       ADT_UInt16 NotInProgress;// count of packets dropped prior to V.21 
   } FAXStats_t;
   #endif

   typedef struct t38Packet_t {
      int pktLen;
      unsigned char  t38Packet[MAX_FAX_PKT];
      FAXStats_t pktStats;
   } t38Packet_t;

#endif



//}==========================================================================

// Vocoders

//{==========================================================================

//------- G.711
#include "G711.H"
extern void G711_muLawDecode(void *pyld, void *pcm,  short frmSize);
extern void G711_muLawEncode(void *pcm,  void *pyld, short frmSize);
extern void G711_aLawDecode (void *pyld, void *pcm,  short frmSize);
extern void G711_aLawEncode (void *pcm,  void *pyld, short frmSize);

//------- G.723
#if (DSP_TYPE == 54)
   #include "g723user.h"
   typedef struct G723EncChannel G723ENC_ChannelInst_t;
   typedef struct G723DecChannel G723DEC_ChannelInst_t;

   #define Rate53  53
   #define Rate63  63
   #define G723_Encode_Init(inst, scratch)           codInitializeG723Channel (inst)
   #define G723_Decode_Init(inst, scratch)           decInitializeG723Channel (inst);
   #define G723_Encode(inst, aux, pcm, pyld)         EncodeG723 (inst, pcm, pyld, 1, \
                                                               (aux)->WrkRate, (aux)->UseVx);
   #define G723_Decode(inst, aux, pyld, pcm, erase)  DecodeG723 (inst, pyld, pcm,  1, erase)

#elif (DSP_TYPE == 55)
   #include "g723dec_adt_user.h"
   #include "g723enc_adt_user.h"
   #define G723_Encode_Init(inst, scratch)            G723_ADT_initEnc(inst, scratch) 
   #define G723_Decode_Init(inst, scratch)            G723_ADT_initDec(inst, scratch)
   #define G723_Encode(inst, aux, pcm, param)         G723_ADT_encode(inst, aux, pcm, param)
   #define G723_Decode(inst, aux, pyld, pcm, erase)   G723_ADT_decode(inst, aux, pyld, pcm, erase)

#elif (DSP_TYPE == 64)
   #include "g723_adt_user.h"
   #define G723_Encode_Init(inst, scratch)            G723_ADT_initEnc(inst, scratch) 
   #define G723_Decode_Init(inst, scratch)            G723_ADT_initDec(inst, scratch)
   #define G723_Encode(inst, aux, pcm, param)         G723_ADT_encode(inst, aux, pcm, param)
   #define G723_Decode(inst, aux, pyld, pcm, erase)   G723_ADT_decode(inst, aux, pyld, pcm, erase)
#endif

//------- G.726
#if (DSP_TYPE == 54)
   #include "g726user.h"
   #define G726_Encode_Init(inst, size, compand)    \
      if (sysConfig.enab.Bits.g726LowMipsEnable)    \
         G726ENC_ADT_initialize (inst, size);       \
      else                                          \
         G726ENC_ADT_initializeLomem (inst, size, compand)

   #define  G726_Decode_Init(inst, size, compand)   \
      if (sysConfig.enab.Bits.g726LowMipsEnable)    \
        G726DEC_ADT_initialize (inst, size);        \
      else                                          \
        G726DEC_ADT_initializeLomem (inst, size, compand)

   #define G726_Encode(inst, pcm, pyld, frmSz)      \
      if (sysConfig.enab.Bits.g726LowMipsEnable)    \
          G726ENC_ADT_encode (inst, pcm, pyld, frmSz);  \
      else                                              \
          G726ENC_ADT_encodeLomem (inst, pcm, pyld, frmSz);

   #define G726_Decode(inst, pcm, pyld, frmSz)     \
      if (sysConfig.enab.Bits.g726LowMipsEnable)   \
          G726DEC_ADT_decode (inst, pyld, pcm, frmSz);  \
      else                                              \
          G726DEC_ADT_decodeLomem (inst, pyld, pcm, frmSz);

#elif (DSP_TYPE == 55)
   #include "g726user.h"
   #define G726_Encode_Init(inst, size, compand)    \
      if (sysConfig.enab.Bits.g726LowMipsEnable)    \
         G726ENC_ADT_initialize (inst, size);       \
      else                                          \
         G726ENC_ADT_initializeLomem (inst, size, compand)

   #define  G726_Decode_Init(inst, size, compand)   \
      if (sysConfig.enab.Bits.g726LowMipsEnable)    \
        G726DEC_ADT_initialize (inst, size);        \
      else                                          \
        G726DEC_ADT_initializeLomem (inst, size, compand)

   #define G726_Encode(inst, pcm, pyld, frmSz)      \
      if (sysConfig.enab.Bits.g726LowMipsEnable)    \
          G726ENC_ADT_encode (inst, pcm, pyld, frmSz);  \
      else                                              \
          G726ENC_ADT_encodeLomem (inst, pcm, pyld, frmSz);

   #define G726_Decode(inst, pcm, pyld, frmSz)     \
      if (sysConfig.enab.Bits.g726LowMipsEnable)   \
          G726DEC_ADT_decode (inst, pyld, pcm, frmSz);  \
      else                                              \
          G726DEC_ADT_decodeLomem (inst, pyld, pcm, frmSz);

#elif (DSP_TYPE == 64)
   #include "g726_user.h"
   typedef G726ChannelInstance_t G726DecInstance_t;
   typedef G726ChannelInstance_t G726EncInstance_t;

   #define G726_ADT_configure(tables, law) ;
   #define G726_Encode_Init(inst, size, compand)   G726_ADT_Init (inst, size, compand)
   #define G726_Decode_Init(inst, size, compand)   G726_ADT_Init (inst, size, compand)
#endif





//------- G.728
#if (DSP_TYPE == 54)
   #include "G728USER.H"
   typedef struct G728ChannelEncode G728EncodeChannelInstance_t;
   typedef struct G728ChannelDecode G728DecodeChannelInstance_t;
   #define G728_Encode_Init(inst, scratch, rate) G728_ADT_initEnc (inst)
   #define G728_Decode_Init(inst, scratch, rate) G728_ADT_initDec (inst)
   #define G728_Encode(inst, pcm, payld, rate)   G728_ADT_encode  (inst, pcm, payld, rate)
   #define G728_Decode(inst, pcm, payld, rate)   G728_ADT_decode  (inst, payld, pcm, 0, rate)
#elif (DSP_TYPE == 55)
   #include "G728_user.h"
   #define G728_Encode_Init(inst, scratch, rate) G728_ADT_encInit (inst, scratch, (ADT_Int16) rate)
   #define G728_Decode_Init(inst, scratch, rate) G728_ADT_decInit (inst, scratch, (ADT_Int16) 0, (ADT_Int16) rate)
   #define G728_Encode(inst, pcm, payld, rate)   G728_ADT_encode  (inst, pcm, payld)
   #define G728_Decode(inst, pcm, payld, rate)   G728_ADT_decode  (inst, pcm, payld)

#elif (DSP_TYPE == 64)
   #include "G728_user.h"
   #define G728_Encode_Init(inst, scratch, rate) G728_ADT_encInit (inst, scratch, (ADT_Int16) rate)
   #define G728_Decode_Init(inst, scratch, rate) G728_ADT_decInit (inst, scratch, (ADT_Int16) 0, (ADT_Int16) rate)
   #define G728_Encode(inst, pcm, payld, rate)   G728_ADT_encode  (inst, pcm, payld)
   #define G728_Decode(inst, pcm, payld, rate)   G728_ADT_decode  (inst, pcm, payld)
#endif

//------- G.729
#if (DSP_TYPE == 54)
   #define G729_ADT_MODE  G729_ADT_AB
   #include "g729user.h"

   #define G729AB_Encode_Init(inst, scratch)  G729_Encoder_Init (inst)
   #define G729AB_Decode_Init(inst, scratch)  G729_Decoder_Init (inst)
   #define G729AB_Encode(inst, pcm, pyld, vad, pktType)    \
                EncodeG729 (inst, pcm, pyld, vad, pktType)
   #define G729AB_Decode(inst, pyld, pcm, frmErs, PktType) \
             DecodeG729 (inst, pyld, pcm, frmErs, PktType) 
#elif (DSP_TYPE == 55)
   #include "G729ABUser_v0211.h"
   #define G729AB_Encode_Init(inst, scratch)  G729AB_ADT_EncodeInit(inst, scratch)
   #define G729AB_Decode_Init(inst, scratch)  G729AB_ADT_DecodeInit(inst, scratch)
   #define G729AB_Encode(inst, pcm, pyld, vad, pktType)    \
      G729AB_ADT_Encode (inst, pcm, pyld, vad, pktType)
   #define G729AB_Decode(inst, pyld, pcm, frmErs, PktType) \
      G729AB_ADT_Decode (inst, pyld, pcm, frmErs, PktType) 
#elif (DSP_TYPE == 64)
   #include "g729ab_user.h"
   #define G729AB_Encode_Init(inst, scratch)  G729AB_ADT_EncodeInit(inst, scratch)
   #define G729AB_Decode_Init(inst, scratch)  G729AB_ADT_DecodeInit(inst, scratch)
   #define G729AB_Encode(inst, pcm, pyld, vad, pktType)    \
      G729AB_ADT_Encode (inst, pcm, pyld, vad, pktType)
   #define G729AB_Decode(inst, pyld, pcm, frmErs, PktType) \
      G729AB_ADT_Decode (inst, pyld, pcm, frmErs, PktType) 

#endif

//-------MELP
#if (DSP_TYPE == 64)
   #include "MELP_user.h"
#else
   #include "melp_user.h"
   typedef MELPENC_ChannelInst_t MELP_AnaChannel_t;
   typedef MELPDEC_ChannelInst_t MELP_SynChannel_t;
   typedef MELPENC_Scratch_t MELP_Scratch_t;
   //#define MELP_ADT_initEnc(Inst, Scratch)
   //#define MELP_ADT_initDec(Inst, Scratch)
   //#define MELP_ADT_encode(Inst, pcm, param)
   //#define MELP_ADT_decode(Inst, pyld, pcm)

#endif
#include inc_melpe_user

//------- AMR
#if (DSP_TYPE == 54)
   #include "ADT_AMR_user.h"
   #define AMR_TX_NO_DATA TX_NO_DATA
   #define AMR_Encode_Init(inst,dx,scratch) AMR_ADT_encInit (inst, dx)
   #define AMR_Decode_Init(inst,scratch)    AMR_ADT_decInit (inst)
   #define AMR_Encode(inst,frRate,vad,pcm,pkt,mode,actRate,outType) \
       AMR_ADT_encode(inst,frRate,vad,pcm,pkt,mode,actRate,outType)
   #define AMR_Decode(inst,frRate,pkt,frType,pcm,flag_13bit,inType) \
       AMR_ADT_decode(inst,frRate,pkt,frType,pcm,flag_13bit,inType)

#elif (DSP_TYPE == 55)
   #include "amr_adt_user.h"
   typedef AMREncodeChannelInstance_t  AMREncInstance_t;
   typedef AMRDecodeChannelInstance_t  AMRDecInstance_t;
   #define AMR_Encode_Init(inst,dx,scratch) AMR_ADT_encInit (inst, dx, scratch)
   #define AMR_Decode_Init(inst,scratch)    AMR_ADT_decInit (inst, scratch)
   #define AMR_Encode(inst,frRate,vad,pcm,pkt,mode,actRate,outType) \
       AMR_ADT_encode(inst,frRate,vad,pcm,pkt,mode,actRate,outType)
   #define AMR_Decode(inst,frRate,pkt,frType,pcm,flag_13bit,inType) \
       AMR_ADT_decode(inst,frRate,pkt,frType,pcm,inType)

#elif (DSP_TYPE == 64)
   // jdc .. now included via components header file .... #include "amr_adt_user.h"
#include inc_amr_user
   typedef AMREncodeChannelInstance_t  AMREncInstance_t;
   typedef AMRDecodeChannelInstance_t  AMRDecInstance_t;
   #define AMR_Encode_Init(inst,dx,scratch) AMR_ADT_encInit (inst, dx, scratch)
   #define AMR_Decode_Init(inst,scratch)    AMR_ADT_decInit (inst, scratch)
   #define AMR_Encode(inst,frRate,vad,pcm,pkt,mode,actRate,outType) \
       AMR_ADT_encode(inst,frRate,vad,pcm,pkt,mode,actRate,outType)
   #define AMR_Decode(inst,frRate,pkt,frType,pcm,flag_13bit,inType) \
       AMR_ADT_decode(inst,frRate,pkt,frType,pcm,inType)
#endif

//------- ADT_4800
#include "adt4800user.h"
#include inc_g722_ADT_user
#include inc_src2_ADT_user
#include inc_srtp_ADT_user

//------- Speex
#include inc_speex_ADT_user
typedef struct Gpak_SpeexCfg {
   ADT_UInt32 Quality;
   ADT_UInt32 Complexity;
   ADT_UInt32 DecodeEnhancementEnable;

   ADT_UInt32 VBREnable;
   float      VBRQuality;

} Gpak_SpeexCfg;

extern Gpak_SpeexCfg SpeexDflt;

//}==========================================================================

// Algorithm Control

//{==========================================================================
typedef struct algCtrl_t {
    GpakAlgCtrl_t       ControlCode;    // The control code
    GpakDeviceSide_t    AorB;           // The device side
    GpakToneTypes       ActTonetype;    // The tone detector type to activate
    GpakToneTypes       DeactTonetype;  // The tone detector type to deactivate
    ADT_UInt16          NLP;
    ADT_Int16           Gain;           // dynamic gain adjust
	GpakCodecs          CodecType;      // dynamic Codec 
} algCtrl_t;

//}==========================================================================

// Gain Block

//{==========================================================================
typedef struct gains_t {
   ADT_Int16    ToneGenGainG1;      /* Gain Control Block 1 */
   ADT_Int16    OutputGainG2;       /* Gain Control Block 2 */
   ADT_Int16    InputGainG3;        /* Gain Control Block 3 */
} gains_t;

// Gain and Summer Blocks
#include inc_AdtGainSum

typedef struct tdmPattern_t {
   ADT_UInt16       value;
   GpakDeviceSide_t deviceSide;
} tdmPattern_t;

// Caller ID
// Gpak C55X use the same cid header as C64X, to satisfy the compiler
// But there is no actual DSP module support this yet
#include inc_adtCidUser


typedef struct cidInfo_t {
    ADT_UInt16          RxCIDIndex;      // pool index of Rx CID instance/buffer
    ADT_UInt16          TxCIDIndex;      // pool index of Tx CID instance/buffer
    CIDRX_Inst_t        *pRxCIDInst;     // pointer to Rx CID instance
    CIDTX_Inst_t        *pTxCIDInst;     // pointer to Tx CID instance
    ADT_UInt8           *pRxCidBuff;     // pointer to start of Rx Message buffer
    ADT_UInt8           *pTxCidMsgBuff;  // pointer to start of Tx Msg buffer
    ADT_UInt8           *pTxCidBrstBuff; // pointer to start of Tx Burst buffer
    ADT_UInt16          numTxBurstBytes; // number of bytes to transmit in burst
    ADT_UInt16          txBurstState;    // Tx cid state: 0 == inactive, 1 == active
    ADT_UInt16          rxCidState;      // CID_RX_MESSAGE_AVAILABLE_CODE == Rx CID msg ready to send to host
    ADT_UInt16          rxMsgLenI8;      // num bytes in decoded rx msg
    ADT_UInt16          type2CID;        // type2 CID
} cidInfo_t;


// NOTE:  SRC_ADT_...
//      Input sample count is number of samples at the higher rate. 
//      Output sample count is number of samples at the converted rate.

//      xxxConvert (below)
//      Input sample count is number of samples at the original rate. 
//      Output sample count is number of samples at the converted rate.
inline int downConvert (void *inst, void *inBuff, short samples, void *outBuff) {
   if (inst == NULL) return 0;
   SRC_ADT_decimateBy2  (inst, inBuff, samples, outBuff, SRC_ADT_dec_coeff, DEC_COEF_SIZE);
   return samples / 2;
}
inline int upConvert (void *inst, void *inBuff, short samples, void *outBuff) {
   if (inst == NULL) return 0;
   samples *= 2;
   SRC_ADT_interpolateBy2 (inst, inBuff, samples, outBuff, SRC_ADT_int_coeff_by_2, INT_COEF_SIZE);
   return samples;
}
//}==========================================================================


// Wavefile information structure
//}==========================================================================
#define WAVEFILE_STATE_OPEN      0       // open
#define WAVEFILE_STATE_LOADING   1       // file download in progress
#define WAVEFILE_STATE_READY     2       // file loaded
#define WAVEFILE_STATE_PLAY_ONCE 3       // playback active: 1-time only
#define WAVEFILE_STATE_PLAY_LOOP 4       // playback active: loop

typedef struct wavefileInfo_t {
    void       *startAddress;          // Start address of audio samples
    ADT_UInt32 dataLengthI8;           // number of bytes of audio samples
    ADT_UInt32 dataLoadCountI8;        // number of bytes of audio sample loaded
    ADT_UInt32 prevLoadBlock;          // previous block num loaded
    ADT_UInt32 playbackIndex;          // current sample playback index
    ADT_UInt16 coding;                 // L16, U-law, or A-law
    ADT_UInt16 sampleRate;             // 8K or 16K
    ADT_UInt16 playbackID;             // ID assigned by host to this file
    ADT_UInt16 state;                  // open, loaded, playOnce, playLoop
    ADT_UInt32 originalHeapFree;       // start value of HeapFree before malloc
} wavefileInfo_t;

// Wavefile List structure
typedef struct wavefileList_t {
   struct wavefileInfo_t  info;
   struct wavefileList_t  *next; 
} wavefileList_t;
//}==========================================================================



// Channel instance structures

//{==========================================================================

typedef ADT_UInt16 GpakEncodeInst;
typedef ADT_UInt16 GpakDecodeInst;

typedef struct GpakRateCvt {
   RateUpBy2_Channel_t   *UpInst; 
   RateDownBy2_Channel_t *DwnInst; 
   ADT_UInt16 UpIndex;
   ADT_UInt16 DwnIndex;
   ADT_UInt16 UpFrame;   // Frame size in samples (at faster rate)
   ADT_UInt16 DwnFrame;  // Frame size in samples (at faster rate)
} GpakRateCvt;

typedef struct pcm2pkt {
    // TDM Input specification
    GpakSerialPort_t InSerialPortId;    
    ADT_UInt16       InSlotId;          
    GpakCompandModes InCompandingMode;

    // TDM Output specification
    GpakSerialPort_t OutSerialPortId;   
    ADT_UInt16       OutSlotId_MuxFact; 
    GpakCompandModes OutCompandingMode;

    // Framing Encoding parameters
    GpakCodecs      Coding;     
    GpakCodecs      PreferredCoding;     
    GpakEncodeInst *EncodePtr;           // Instance data

    int             SamplesPerFrame;     // At TDM sampling rate
    int             OctetsPerPacket;
    ADT_UInt40      FrameTimeStamp;

    CircBufInfo_t   inbuffer;
    CircBufInfo_t  *outbuffer; 
    CircBufInfo_t  *activeInBuffer;
	CircBufInfo_t	savedPktPcmInBuffer;

    // Echo cancellation parameters
    GpakActivation  EchoCancel;
    GpakActivation  AECEchoCancel;      
    G168ChannelInstance_t   *pG168Chan;
    ADT_UInt16      EcIndex;
    CircBufInfo_t   ecFarPtr;
    CircBufInfo_t   ecBulkDelay;
    ADT_UInt16      bulkDelayIndex;
    ADT_UInt16      slipReset;
    
    // Voice activity parameters
    GpakVADMode     VAD;
    VADCNG_Instance_t *vadPtr;
    int             SIDPower;
    VADCNG_ADT_Param_t VadParms;         // saved copy of init of parameters
    ADT_Int16       PrevVadState;
    GpakActivation  DtxEnable;

    // AGC Parameters
    GpakActivation  AGC; 
    AGCInstance_t  *agcPtr;            
    ADT_UInt16      AGCIndex;           
    ADT_Int16       AGCPower;
    AGC_ADT_Param_t AGCParms;           // saved copy of init parameters
    
    // Tone parameters
    GpakToneTypes   toneTypes;          // Types of tone: dtmf, mfr1, mfr2f, mfr2r, cprg
    TGInfo_t        TGInfo;             // Generation
    TDInst_t        TDInstances;

    TRDetectInstance_t *toneRelayPtr;   // Relay detection
    ADT_UInt16      *tonePktGet;        // Forwarding / host insertion

    // Noise suppression parameters
    GpakActivation  NoiseSuppress;
    NSuppress_t     Noise;

    // Play/record parameters
    playRecInfo_t   recA;          
    playRecInfo_t   playB;

    // Gain settings
    gains_t          Gain;

    // Caller ID
    GpakCidMode_t    CIDMode;
    cidInfo_t        cidInfo;

    GpakRateCvt      rateCvt;         // Rate conversion Instances

	GpakActivation	SideTone;
	ADT_Int16		SideToneDB;

   wavefileInfo_t  waveInfo;
} pcm2pkt_t;

typedef struct pkt2pcm {
    // TDM Input specification
    GpakSerialPort_t InSerialPortId;    
    ADT_UInt16       InSlotId_MuxFact;
    GpakCompandModes InCompandingMode;

    // TDM Output specification 
    GpakSerialPort_t OutSerialPortId;   
    ADT_UInt16       OutSlotId; 
    GpakCompandModes OutCompandingMode;

    // Framing Encoding parameters
    GpakCodecs      Coding;     
    GpakCodecs      PreferredCoding;     
    GpakDecodeInst *DecodePtr;           // Instance data

    int             SamplesPerFrame;
    int             OctetsPerPacket;
    ADT_UInt40      FrameTimeStamp;

    CircBufInfo_t  *inbuffer;
    CircBufInfo_t   outbuffer; 


    // Echo cancellation parameters
    GpakActivation  EchoCancel;
    GpakActivation  AECEchoCancel;      
    G168ChannelInstance_t   *pG168Chan;
    ADT_UInt16      EcIndex;
    CircBufInfo_t   ecFarPtr;
    ADT_UInt16      slipReset;
    
    // Voice activity parameters CNG part
    GpakVADMode     CNG;
    GpakActivation  ForwardCNGPkts; 
    int             SIDPower;
    VADCNG_Instance_t *cngPtr;
	
    // Voice activity parameters VAD part
    GpakVADMode     VAD;
    VADCNG_Instance_t *vadPtr; // packet rx direction VAD
    VADCNG_ADT_Param_t VadParms;         // saved copy of init of parameters
    ADT_Int16       PrevVadState;
    // AGC Parameters
    GpakActivation  AGC; 
    AGCInstance_t  *agcPtr;            
    ADT_UInt16      AGCIndex;           
    ADT_Int16       AGCPower;
    AGC_ADT_Param_t AGCParms;           // saved copy of init parameters

    
    // Tone parameters
    GpakToneTypes     toneTypes;          // Types of tone: dtmf, mfr1, mfr2f, mfr2r, cprg
    TGInfo_t          TGInfo;             // Generation
    TDInst_t          TDInstances;

    GpakActivation   ForwardTonePkts;    // Forwarding
    ADT_UInt16      *tonePktPut;

    // Noise suppression parameters
    GpakActivation  NoiseSuppress;
    NSuppress_t     Noise;

    // Play/record parameters
    playRecInfo_t   playA;
    playRecInfo_t   recB;

    // Gain settings
    gains_t          Gain;

    // Caller ID
    GpakCidMode_t       CIDMode;
    cidInfo_t           cidInfo;

    // Rate conversion instances
    GpakRateCvt     rateCvt;

	GpakActivation	SideTone;
	ADT_Int16		SideToneDB;

   wavefileInfo_t  waveInfo;

} pkt2pcm_t;

typedef struct GpakSrtpKeyInfo_t {
   ADT_Bool   Configured; // Flag set when SRTP chan configed, teardown clears it
   ADT_Bool   NewKeyFlag; // Set after new key arrived
   ADT_UInt16 channelId;
   GpakDeviceSide_t Direction; // either DSPToNet(TX), or NetToDSP(RX)
   ADT_UInt8  MasterKey[32];    // Initial master keys
   ADT_UInt8  MasterSalt[14];   // Initial master salts
   ADT_UInt16 MasterKeyLife[3]; // 48-bit lifetime of master key
   ADT_UInt8  MKI[4];           // Master key identifier
   ADT_UInt8  MKI_I8;           // Number of bytes for MKI.
   ADT_UInt8  KDR;              // Key derivation rate (See RFC3711) 
   ADT_UInt16 ROC;              // Initial rollover count
   ADT_UInt16 KeySizeU8;
} GpakSrtpKeyInfo_t;

// Use the max instance size for the static instance allocation
#ifndef SRTPRX_SIZE_I4
   #define SRTPRX_SIZE_I4 1
#endif
typedef struct SrtpRxInstance_t {
   ADT_UInt32 Instance[SRTPRX_SIZE_I4];
} SrtpRxInstance_t;
extern const int srtpRxI8;

#ifndef SRTPTX_SIZE_I4
   #define SRTPTX_SIZE_I4 1
#endif
typedef struct SrtpTxInstance_t {
   ADT_UInt32 Instance[SRTPTX_SIZE_I4];
} SrtpTxInstance_t;
extern const int srtpTxI8;

typedef union status_t {
    unsigned int Word;
    struct {
        unsigned packetOverflow:1;       //  0
        unsigned packetUnderflow:1;      //  1
        unsigned invalidToneDetected:1;  //  2
        unsigned invalidPacketHeader:1;  //  3
        unsigned unused:12;              //  4-15 unused
    } Bits;
} status_t;

typedef struct faxInfo_t {
    struct FaxStruct *pFax;             // pointer to fax structure
    ADT_UInt16 faxIndex;                // index of the instance that was allocated
    ADT_UInt16 t38InProgress;           // flag indicating whether a t38 transfer is in progress, 1==true
    ADT_UInt16 faxPktRcvd;              // flag indicating a fax packet has been received
    GpakFaxMode_t  faxMode;             // fax mode of operation: disabled, fax only, fax plus voice
    GpakFaxTransport_t faxTransport;    // fax transport type

    ADT_UInt16 msBeforeTimeout;          // elapsed time in milliseconds before declaring a timeout

} faxInfo_t;   

typedef struct chanInfo {
   ADT_UInt16       CoreID;
   ADT_UInt16       ChannelId;       // This Channel's Id
   int              PairedChannelId; // Channel Id of paired channel in pkt-pkt mode 
   GpakChanType     channelType;     // type of channel

   struct chanInfo  *nextPktToPcm;   // Linked list of channels for framing taskst
   struct chanInfo  *prevPktToPcm;
   struct chanInfo  *nextPcmToPkt;
   struct chanInfo  *prevPcmToPkt;

   pcm2pkt_t        pcmToPkt;        // PCM -> PKT direction
   pkt2pcm_t        pktToPcm;        // PKT -> PCM direction

   status_t         status;          // error status flags
   unsigned int     invalidHdrCount;
   unsigned int     overflowCount;
   unsigned int     underflowCount;
   unsigned int     fromHostPktCount;
   unsigned int     toHostPktCount;
   unsigned int     Flags;           // flag bits

   faxInfo_t        fax;             // fax info structure
   algCtrl_t        algCtrl;         // Algorithm Control
   tdmPattern_t     tdmPattern;      // tdm fixed pattern
   ADT_UInt32       LastRTPToneStamp; // RTP time stamp of last delivered tone.

   ADT_Word         FrameCount;
   GpakActivation   VoiceChannel;    // True= voice channel.  False= data channel
   ADT_UInt16       pcmPoolIndex;    // pcm buffer pool index
   ADT_UInt16       pktPoolIndex;    // pkt buffer pool index

   ADT_UInt16       EncoderRate;     // sampling rate
   ADT_UInt16       DecoderRate;     // sampling rate
   ADT_UInt16       ProcessRate;     // sampling rate
   ADT_UInt16       G722RtpSamplingRate;  // help income wrong G722 sampling rate
   ADT_UInt16       G722RtpSequence;      // help income wrong G722 sampling rate
   ADT_UInt16       G722Marker;           // help income wrong G722 sampling rate
   ADT_UInt32       G722RtpTimeStamp;     // help income wrong G722 sampling rate
   ADT_UInt16       RtpRxTimeoutValue;
   ADT_UInt16       RtpRxTimeoutCount;

   GpakED137BcfgParms ED137BcfgParms;

   GpakED137B_PTTupdateParms ED137BPTT_Parms;

   GpakED137B_PersistentFeatureParms ED137B_PersistentFeature;

   GpakED137B_NPF ED137B_NPF;


   GpakRTPRxState_t ED137RxState;

   GpakRTPTxState_t ED137TxState;

   ED137Pkt_t ED137TxExT;

   ED137RxPkt_t R2SRxExT;
   void             *customData;     // Pointer to custom data structure
} chanInfo_t;



// Definition of channel structure flag bits (Flags field).
#define CF_REINIT_ENCODE         0x0001   // Reinitialize encoder
#define CF_REINIT_DECODE         0x0002   // Reinitialize decoder
#define CF_MULTI_CORE_LOCK_FLAG  0x0004   // Multi-core locking flag used to lock channel related operations
#define CF_VECTOR_TEST_MODE      0x0008   // Vector test mode active
#define CF_VECTOR_SYNC_CHECK     0x0010   // Vector test mode sync check
#define CF_VECTOR_SYNC_OK        0x0020   // Vector test mode sync'd
#define CF_ACTIVE                0x0040   // Channel is active and should be processed by framing task
#define CF_CNFR_ADD              0x0080   // Channel is to be added to it's conference on next conference frame
#define CF_CNFR_DELETE           0x0100   // Channel is to be removed from it's conference on next conference frame
#define CF_ENCODE_PRIORITY       0x0200   // Channel is to be added with priority on encode queue
#define CF_DECODE_PRIORITY       0x0400   // Channel is to be added with priority on decode queue

//  Channel flags for FAX signaling
#define CF_FAX_RESERVED         0x1F800   // Set when fax relay wants notification of FAX tones (CED/CNG)
#define CF_RTP_TX_DISABLED      0x20000   // Set to disable RTP transmissions
#define CF_RTP_RX_DISABLED      0x40000   // Set to disable RTP transmissions

//}==========================================================================

// Conferencing instance structures

//{==========================================================================
typedef struct ConferenceInfo_t {
   int             FrameSize;       // frame size
   GpakToneTypes   InputToneTypes;  // Input Tone Detection types
   ConfInstance_t *Inst;            // Conference instance data

   ADT_Word       NumMembers;      // current number of member channels
   chanInfo_t    *MemberList;      // linked list of member channels
   chanInfo_t    *CompositeList;   // linked list of composite output channels

   TGInfo_t       TG_NB;           // dedicated Tonegen instances
   TGInfo_t       TG_WB;           // dedicated Tonegen instances
   AGCInstance_t *AgcInstance;     // dedicated AGC instance

} ConferenceInfo_t;



//}==========================================================================

// Host interface block structure

//{==========================================================================
//  NOTE: Offsets into this structure are defined by GpakApi.h
#define WdSz  sizeof(ADT_Word)
#if (DSP_TYPE == 54) || (DSP_TYPE == 55)
#define SzInt16ToHost(a)   (a)  // Convert Int16 units to DSP Word units
#define SzHostToInt16(a)   (a)  // Convert DSP Word units to Int16
#define Int16PadForHost(a) (a)  // Calculate number of Int16 units needed for host transfer
#define SzHostToInt8(a)    (a)       // DSP transfer units (2bytes) to Int8
#define SzHostToBytes(a)  ((a) * 2)  // DSP transfer units (2bytes) to bytes
#elif (DSP_TYPE == 64)
#define SzInt16ToHost(a)   ((((a)+1) * 2) / 4)  // Convert Int16 units to DSP Word units (4 bytes)
#define SzHostToInt16(a)   ((a) * 2)            // Convert DSP Word units (4bytes) to Int16
#define Int16PadForHost(a) ((((a)+1)>>1)<<1)    // Calculate number of Int16 units needed for host transfer
#define SzHostToInt8(a)    ((a) * 4)            // DSP transfer units  (4bytes) to Int8
#define SzHostToBytes(a)   ((a) * 4)            // DSP transfer units  (4bytes) to bytes
#endif

// messaging interface to pcm port stats:
typedef struct PortStat_t {
    GpakActivation  Status;           /* port status */
    ADT_UInt32      RxIntCount;       /* Rx Interrupt count */
    ADT_UInt16      RxSlips;          /* Rx Slip count */
    ADT_UInt16      RxDmaErrors;      /* Rx Dma error count */
    ADT_UInt32      TxIntCount;       /* Tx interrupt count */
    ADT_UInt16      TxSlips;          /* Tx Slip count */
    ADT_UInt16      TxDmaErrors;      /* Tx Dma error count */
    ADT_UInt16      FrameSyncErrors;  /* Frame Sync error count */
    ADT_UInt16      RestartCount;     /* McBsp Restart count */
} PortStat_t;


//}==========================================================================

// Host packet interfaces

//{==========================================================================
//
//  PktHdr_t        -  Host responsible for RTP and IP stack
//  RTPToHostHdr_t  -  DSP responsible for RTP, host responsible for IP stack
//  NCHdr_t         -  DSP responsible for RTP and IP stack
//
// Payload Lengths are defined in octets
// Packet lengths are defined in 16-bit words
//
//  Fields common to all interfaces
//  Word 0:     Payload Size N (in octets)
//  Word 1:     Channel ID = {0...127}
//
//  PktHdr_t
//  Word 2:     Payload Class = {Audio, silence, tone, Gpak}
//  Word 3:     Payload Type = {GpakCodecs, AAL2Pkts, RTPPkts}
//  Words 4-5:  RTP Time stamp
//  Words6-End: N octets of payload data - payload data is packed into 16-bit
//              words as follows. If N is odd, then the final octet is packed
//              into the low byte.
//  RTPToHostHdr_t
//  Words3-End: N octets of payload data - payload data is packed into 16-bit
//              words as follows. If N is odd, then the final octet is packed
//              into the low byte.
//
//  NCHdr_t
//  Words 2-3:  RTPBuff = Pointer to location of RTP data
//  Words 4-5:  PktHndl = Pointer to base memory location to free
//
//  >>>>>>> NOTE: These structures should be multiples of 4 bytes for RTP header alignment


#include inc_rtpapi_user
#include inc_rtpapi
#include "GpakRtcp.h"
typedef struct PktHdr {
   ADT_UInt16 PayldBytes;    // MUST be first element
   ADT_UInt16 ChanId;
   ADT_UInt16 PayldClass;
   ADT_UInt16 PayldType;
   ADT_UInt32 TimeStamp;   // RTP TimeStamp
} PktHdr_t;

typedef struct PktExtHdr {
    ADT_UInt16 Marker;
    ADT_UInt32 SSCR;
    ADT_UInt32 CSRC[15];
    ADT_UInt32 CSRCCnt;
    ADT_UInt8  ExtData[256];
    ADT_UInt32 ExtDataI8Len;
} PktExtHdr_t;

typedef struct RTPToHostHdr_t {
   ADT_UInt16 PktI8;    // MUST be first element
   ADT_UInt16 ChanId;
} RTPToHostHdr_t;

#define RTP_TO_HOST_I16  (sizeof (RTPToHostHdr_t) / sizeof (ADT_Int16)) 

typedef struct _NCHdr {
   ADT_UInt16 PktI8;
   ADT_UInt16 ChanId;
   void *RTPBuff;
   void *PktHndl;
} NCHdr_t;

#define NCHdrI16 (sizeof (NCHdr_t) / sizeof (ADT_UInt16))



typedef enum packetStatus_t {
    noPacketAvailable  = 0,
    invalidPktHeader   = 1,
    nonT38PktAvailable = 2,
    relayFull          = 3
} packetStatus_t;

#define PKT_HDR_I16LEN  (sizeof (PktHdr_t) / sizeof (ADT_Int16)) 


//                          High Byte       Low Byte
//                      +---------------+------------------+
//           Word 6:    |    Octet 2    |     Octet 1      |
//                      +---------------+------------------+
//           Word 7:    |    Octet 4    |     Octet 3      |
//                   -   +---------------+------------------+
//           Word 8:    |    Octet 6    |     Octet 5      |
//                      +---------------+------------------+
//                ...   |     ...       |    ...           |
//                      +---------------+------------------+
//           Word End:  |    0          |     Octet N      |
//                      +---------------+------------------+

#define CHAN_ID_INDX        0
#define PAYLOAD_CLASS_INDX  1
#define PAYLOAD_TYPE_INDX   2
#define PAYLOAD_LEN_INDX    3
#define PAYLOAD_START   PKT_HDR_I16LEN

#define SID_PAYLOAD_LEN    1
#define SID_PAYLOAD_WORDS  1
#define SID_PKT_I16         (PKT_HDR_I16LEN + 1)

#define RTP_EVT_PAYLOAD_LEN 4
#define RTP_EVT_PAYLOAD_WORDS 2

#define RTP_TONE_PAYLOAD_LEN 8
#define RTP_TONE_PAYLOAD_WORDS 4
#define RTP_TONE_PKT_LEN    (PKT_HDR_LEN+4) // support Only 2 tones

#define AAL2_DIGITS_PAYLOAD_LEN 4
#define AAL2_DIGITS_PAYLOAD_WORDS 2

#define MAX_GPAK_PAYLOAD_WORDS 1

typedef struct ProfileTypes {
   ADT_UInt8  Voice, CNG, Tone, T38;
} ProfileTypes;

typedef struct RtpChanData_t {
    RTPDATA      Instance;
    ProfileTypes Profile;
    ADT_UInt16   ChannelId;
} RtpChanData_t;

// Interfaces to RTP processing
void RTP_Init ();
GPAK_RTPConfigStat_t ProcConfigRTPMsg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply);
GPAK_RTPConfigStat_t ProcConfigRTPv6Msg (ADT_UInt16 *pCmd, ADT_UInt16 *pReply);
int sendPacketToRTP (chanInfo_t *chan, PktHdr_t *Hdr, ADT_UInt16 *pyld);
packetStatus_t getPacketFromRTP (chanInfo_t *chan, PktHdr_t *Hdr, ADT_UInt16 *pyld, ADT_UInt16 maxPayI8);
//}==========================================================================

//  Frame task data

//{==========================================================================
typedef struct MipsConserve_t {
	ADT_UInt8 FrameCount;
	ADT_UInt8 ReductionAmount;
} MipsConserve_t;


typedef struct FrameTaskData_t {
   chanInfo_t *PcmToPktHead;    // Channel queues
   chanInfo_t *PktToPcmHead;
   ADT_UInt32  TicksPerFrame; 
   ADT_UInt16  SampsPerFrame;   // TDM samples per frame

   ADT_UInt16  WorkBytes;       // Task work buffers
   ADT_PCM16   *pInWork;     
   ADT_PCM16   *pOutWork;
   ADT_PCM16   *pFarWork; 
   ADT_UInt16  *SAScratch;      // Fast access algortihm scratch memory
   ADT_UInt16  SAScratchI16;
   ADT_UInt16  *DAScratch;      // Slow access algorithm scratch memory
   ADT_UInt16  DAScratchI16;
   ADT_Word    Hint;            // Host interrupt flag
   SWI_Handle  SWI;             // Software interrupt structure

   ADT_UInt16  SamplesPending;
   ADT_UInt16  TimeAvgCnt; 
   ADT_Word   *pCpuUsage;       // CPU Usage
   ADT_UInt32  ProcTimeSum; 
   ADT_UInt32  ScheduledStart; 
   ADT_UInt32  pkUsageTicks;
   ADT_Word    *pCpuPkUsage;
   ADT_UInt32  Flags;

   ADT_UInt32 FramesScheduled;
   ADT_UInt32 FramesProcessed;

   MipsConserve_t *pG168MipsConserve; // Ec Mips Management
   ADT_Int16  PcmEcInUseCnt;
   ADT_Int16  PktEcInUseCnt;
} FrameTaskData_t;

extern FrameTaskData_t FrameTaskData[];

extern ADT_Word MultiCoreCpuUsage[];
extern ADT_Word MultiCoreCpuPeakUsage[];

inline ADT_UInt16 halfMSToSamps (ADT_UInt16 halfMS) {
   ADT_UInt16 samps;
   samps = halfMS * sysConfig.samplesPerMs / 2;
   return samps;
}
#define sampsToHalfMS(samps) ((samps * 2)/sysConfig.samplesPerMs)


#define _sampsToMS(samps) (samps/sysConfig.samplesPerMs)   // Deprecated
#define _msToSamps(MS)    (MS * sysConfig.samplesPerMs)    // Deprecated
//}==========================================================================

// Async Event Fifo information (lengths are in octets)

//{==========================================================================
typedef struct EventFifoHdr_t {
    ADT_UInt16 channelId;       // channel id
    ADT_UInt16 eventCode;       // event code (see GpakAsyncEventCode_t)
    ADT_UInt16 eventLength;     // length in bytes of payload
    ADT_UInt16 deviceSide;      // device Side
} EventFifoHdr_t;

#define EVENT_FIFO_MSGLEN_ZERO               0
#define EVENT_FIFO_TONE_MSGLEN_BYTES         4    // tone event msg length
#define EVENT_MAX_ED137_MSGLEN_WORDS         264  // Allow for 16, 16-bit Non-Persistent messages plus 8 byte extensiion header
#define EVENT_FIFO_RXCID_MSGLEN_BYTES        260  // Rx Caller ID event msg length (16-bit length + 256 msg bytes + 2 bytes padding to make even number 32-bit words)
#define EVENT_FIFO_RECORD_STOPPED_MSGLEN     4    // voice record stopped event msg length
#define EVENT_FIFO_RECORD_BUFFER_FULL_MSGLEN 4    // voice record buffer full event msg length
#define EVENT_FIFO_SRTPNEWKEY_MSGLEN_BYTES   12   // SRTP request new key event msg length
#define EVENT_FIFO_CIDTYPE2_MSGLEN_BYTES     2    // CID type2 event msg length

#define EVENT_MAX_MSGLEN_I16              4      // max async event message length in words
#define EVENT_MAX_CID_MSGLEN_WORDS      130
#define EVENT_HDR_I16LEN    (sizeof (EventFifoHdr_t) / sizeof (ADT_Int16)) 
#define EVENT_HDR_HOSTLEN   (sizeof (EventFifoHdr_t) / WdSz)
#define MAX_ASYNC_EVENTS_PER_CHANNEL     1  // one event per channel per frame


#define PLAY_REC_PLAYLEN_OFFSET_I16  0      // offset to where length of recorded msg is stored
#define PLAY_REC_MODE_OFFSET_I16     2      // offset to where coding mode is stored
#define PLAY_REC_PAYLOAD_OFFSET_I16  4      // offset to where record/playback data begins

typedef struct EventFifoMsg_t {
    EventFifoHdr_t  header;    
    ADT_UInt16      payload[EVENT_MAX_MSGLEN_I16];
} EventFifoMsg_t;

typedef struct EventFifoCIDMsg_t {
    EventFifoHdr_t  header;    
    ADT_UInt16      payload[EVENT_MAX_CID_MSGLEN_WORDS];
} EventFifoCIDMsg_t;

typedef struct EventFifoED137Msg_t {
    EventFifoHdr_t  header;
    ADT_UInt16      payload[EVENT_MAX_ED137_MSGLEN_WORDS];
} EventFifoED137Msg_t;

//}==========================================================================

// Miscellaneous definitions.

//==========================================================================
extern ADT_Word MaxFrameCnt;
#define FRAME_TASK_CNT (MaxFrameCnt/DSPTotalCores)

#define SYS_TICKS_PER_MSEC 1  // System Clock ticks per msec
#define VOID_INDEX  0xFFFF     // instance index indicating not-allocated
#define PKT_LOOP_BACK   0x7FFF
#define NULL_CHANNEL    0xFFFF

#ifndef TRUE
#define TRUE (1==1)
#endif

#ifndef FALSE
#define FALSE 0
#endif

enum resources { 
   TONE_DETECT_LOCK, 
   ARB_DETECT_LOCK, 
   FAX_DETECT_LOCK,
   RESOURCE_CNT     // MUST BE LAST
};

typedef volatile struct lock {
   int Inst;
   char dummy[60];  // Force lock to be alone in L1D cache line.
} coreLock;


extern coreLock ResourceLocks [RESOURCE_CNT];

// Packet forwarding buffer.
typedef struct FwdBuf_t {
   ADT_Int16  fwdType;
   ADT_Int16  nullPktCnt;
   PktHdr_t   pktHdr;
   ADT_UInt16 pktData[1];
} FwdBuf_t;


// Echo canceller instance information structure
typedef struct ecInstanceInfo_t {
    ADT_UInt16 **EcChan;           // array of ec channel instances
    ADT_UInt16 **EcSaState;        // array of ec saState instances
    ADT_UInt16 **EcDaState;        // array of ec daState instances
    ADT_UInt16 **EcEchoPath;       // array of ec echo path instances
    ADT_UInt16 **EcBgEchoPath;     // array of ec background echo path instances
    ADT_UInt16 EcChanMemSize;       // ec chan instance size
    ADT_UInt16 EcSAStateMemSize;    // ec sa state instance size
    ADT_UInt16 EcDAStateMemSize;    // ec da state instance size
    ADT_UInt16 EcEchoPathMemSize;   // ec echo path instance size
    ADT_UInt16 EcBgEchoPathMemSize; // ec background echo path instance size
} ecInstanceInfo_t;

// support Type II Caller ID
typedef struct CIDTX2_Inst_t {
   CIDTX_Inst_t        cidTxInstance;
   CIDTX_TASGEN_Inst_t tasGenInstance;
   CIDTX_ACKDET_Inst_t ackDetInstance;
} CIDTX2_Inst_t;

typedef struct CIDRX2_Inst_t {
   CIDRX_Inst_t        cidRxInstance;
   CIDRX_ACKGEN_Inst_t ackGenInstance;
   CIDRX_TASDET_Inst_t tasDetInstance;
} CIDRX2_Inst_t;


/* ======================== G722 RTP DEBUG Channels ========================= */
typedef struct g722RTPSync_t {
    ADT_UInt32 rtp_sr;
    ADT_UInt32 sr1_count;
    ADT_UInt32 Sequence;
    ADT_UInt32 TimeStamp;
    ADT_UInt32 BitFlds;
    ADT_UInt32 hdrI8Len;
    ADT_UInt32 PktI8;
    ADT_UInt32 pyldI8Len;
} g722RTPSync_t;

typedef struct g722RtpDebugStats_t {
    unsigned int sr0_count;
    unsigned int sr1_count;
    unsigned int sr8_adj_count;
    unsigned int sr8_nadj_count;
    unsigned int sr16_adj_count;
    unsigned int sr16_nadj_count;
    unsigned int discard_count;
    g722RTPSync_t g722Sync[2];
} g722RtpDebugStats_t;

#endif  /* prevent multiple inclusion */
