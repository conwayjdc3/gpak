//*******************************************************************
//
// Copyright(c) 2001-2002 Adaptive Digital Technologies Inc. 
//              All Rights Reserved
//
//
// File Name:   sysmem.h
// 
//
// Description:   header file for memory block/channel structure creation
//
//
// Initial Version: 11/15/2001
//
//
// Modified:
//
//
//******************************************************************
#ifndef _SYSMEM_H
#define _SYSMEM_H

enum memID
 {
    ENCODER_ID,             // 0 
    DECODER_ID,             // 1
    NUM_IDS                 // 2
};

typedef struct
{
    enum memID      id;
    unsigned int    start;
} memAlloc_t;
    
typedef struct
{
    int daramCount;                 // number of channels in a block
    int daramblockSize;             // length of block in words
    int daramBaseAlignment;         // start alignment of block in words
    memAlloc_t daramAllocTable[NUM_IDS];   // id and start address for channels
    int saramCount;
    int saramblockSize;
    int saramBaseAlignment;
    memAlloc_t saramAllocTable[NUM_IDS];
}
blockStatus_t;

#define NUMFRAMES_PER_DMA_BUFFER     16 // number of frames per dma circ buffer

#define NUMFRAMES_PER_PCM_BUFF       2  // number of frames in PCM and 
                                        // packet in/out buffers

#define NUMFRAMES_OF_PCM_BULKDELAY   1  // additional number of frames to add
                                        // to pcm outbuffer for ec bulk delay

#define NUMFRAMES_OF_PKT_BULKDELAY   3  // number of frames in packet bulk 
                                        // delay buffer

#define DARAM 0
#define SARAM 1

#endif // conditional include
