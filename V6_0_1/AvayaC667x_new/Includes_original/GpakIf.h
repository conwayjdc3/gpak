#ifndef _GPAKIF_H
#define _GPAKIF_H

#include "GpakBios.h"
#include "sysconfig.h"
#include "adt_typedef.h"

#define CACHE_L2_LINE_SIZE 128
#define CACHE_L2_LINE_SIZE_I16 (CACHE_L2_LINE_SIZE/2)

extern int checkApiVer();

// Read-Area Gpak If block (Host writes, Dsp Reads)
typedef struct RdIfBlock_t {
   ADT_Word    ApiVersionId;            // API version ID
   ADT_Word    EventFIFOTakeIndex;      // Event FIFO Take Index
   ADT_Word    NetToDspPktPutIndex;     // Net To Dsp RTP packet buffer Put Index
   ADT_Word    DspToNetPktTakeIndex;    // Dsp To Net RTP packet buffer Take Index
} RdIfBlock_t;

// Read-Write-Area Gpak If block (Host and/or Dsp reads and writes these members)
typedef struct RdWrIfBlock_t {
   ADT_Word    DspStatus;            // DSP Status
   ADT_Word    CmdMsgLength;         // Command message's length (dwords)
   ADT_Word    ReplyMsgLength;       // Reply message's length (dwords)
} RdWrIfBlock_t;

// Write-Area Gpak If block (Host reads, Dsp Writes)
typedef struct WrIfBlock_t {
   ADT_Word       EventFIFOPutIndex;          // Event FIFO Put Index
   ADT_Word       NetToDspPktTakeIndex;       // Net To Dsp RTP packet buffer Take Index
   ADT_Word       DspToNetPktPutIndex;        // Dsp To Net RTP packet buffer Put Index
   ADT_Word       CpuUsage1;            // 1 ms frame's CPU usage estimate (.1 %)
   ADT_Word       CpuUsage2_5;          // 2.5 ms frame's CPU usage estimate (.1 %)
   ADT_Word       CpuUsage5;            // 5 ms frame's CPU usage estimate (.1 %)
   ADT_Word       CpuUsage10;           // 10 ms frame's CPU usage estimate (.1 %)
   ADT_Word       CpuUsage20;           // 20 ms frame's CPU usage estimate (.1 %)
   ADT_Word       CpuUsage22_5;         // 22.5 ms frame's CPU usage estimate (.1 %)
   ADT_Word       CpuUsage30;           // 30 ms frame's CPU usage estimate (.1 %)
   ADT_UInt32     DmaSwiCnt;            // Count of DMA software interrupts (timer ticks)
   ADT_Word       CpuPkUsage1;          // 1 ms frame's CPU peak usage estimate (.1 %)
   ADT_Word       CpuPkUsage2_5;        // 2.5 ms frame's CPU peak usage estimate (.1 %)
   ADT_Word       CpuPkUsage5;          // 5 ms frame's CPU peak usage estimate (.1 %)
   ADT_Word       CpuPkUsage10;         // 10 ms frame's CPU peak usage estimate (.1 %)
   ADT_Word       CpuPkUsage20;         // 20 ms frame's CPU peak usage estimate (.1 %)
   ADT_Word       CpuPkUsage22_5;       // 22.5 ms frame's CPU peak usage estimate (.1 %)
   ADT_Word       CpuPkUsage30;         // 30 ms frame's CPU peak usage estimate (.1 %)
} WrIfBlock_t;

typedef struct GpakIfBlock_t {
   ADT_UInt16    *CmdMsgPointer;     // Command message's buffer pointer
   ADT_UInt16    *ReplyMsgPointer;   // Reply message's buffer pointer
   CircBufInfo_t *EventMsgPointer;   // Event message's circular buffer pointer
   CircBufInfo_t *PktBufrMem;        // Packet Buffer circular buffer
   ADT_Word    DspStatus;            // DSP Status
   ADT_Word    VersionId;            // G.PAK Dsp Version Id
   ADT_Word    MaxCmdMsgLen;         // max Command message length (dwords)
   ADT_Word    CmdMsgLength;         // Command message's length (dwords)
   ADT_Word    ReplyMsgLength;       // Reply message's length (dwords)
   ADT_Word    EventMsgLength;       // Event Report message's length (octets)
   ADT_Word    NumBuiltChannels;     // number of channels in DSP's build
   union {
#ifdef __TMS320C55X__
      ADT_Word   CpuUsage;
#else
      ADT_Word   *CpuUsage;
#endif
      ADT_Word    CpuUsage1;            // 1 ms frame's CPU usage estimate (.1 %)
   } u1;
   ADT_Word    CpuUsage2_5;          // 2.5 ms frame's CPU usage estimate (.1 %)
   ADT_Word    CpuUsage5;            // 5 ms frame's CPU usage estimate (.1 %)
   ADT_Word    CpuUsage10;           // 10 ms frame's CPU usage estimate (.1 %)
   ADT_Word    CpuUsage20;           // 20 ms frame's CPU usage estimate (.1 %)
   ADT_Word    CpuUsage22_5;         // 22.5 ms frame's CPU usage estimate (.1 %)
   ADT_Word    CpuUsage30;           // 30 ms frame's CPU usage estimate (.1 %)
   ADT_UInt32  DmaSwiCnt;            // Count of DMA software interrupts (timer ticks)
   ADT_Word    ApiVersionId;         // API version ID
   union {
#ifdef __TMS320C55X__
      ADT_Word CpuPkUsage;
#else
      ADT_Word *CpuPkUsage;
#endif
      ADT_Word CpuPkUsage1;
   } u2;
   ADT_Word    CpuPkUsage1;          // 1 ms frame's CPU peak usage estimate (.1 %)
   ADT_Word    CpuPkUsage2_5;        // 2.5 ms frame's CPU peak usage estimate (.1 %)
   ADT_Word    CpuPkUsage5;          // 5 ms frame's CPU peak usage estimate (.1 %)
   ADT_Word    CpuPkUsage10;         // 10 ms frame's CPU peak usage estimate (.1 %)
   ADT_Word    CpuPkUsage20;         // 20 ms frame's CPU peak usage estimate (.1 %)
   ADT_Word    CpuPkUsage22_5;       // 22.5 ms frame's CPU peak usage estimate (.1 %)
   ADT_Word    CpuPkUsage30;         // 30 ms frame's CPU peak usage estimate (.1 %)
   WrIfBlock_t   *pWrBlock;           // Dsp's Write-Area Gpak IF-block pointer
   RdIfBlock_t   *pRdBlock;           // Dsp's Read-Area Gpak IF-block pointer
   RdWrIfBlock_t *pRdWrBlock;         // Read-Write area Gpak IF-block pointer
} GpakIfBlock_t;
extern GpakIfBlock_t ApiBlock;      // G.PAK API memory block used by host


#endif


