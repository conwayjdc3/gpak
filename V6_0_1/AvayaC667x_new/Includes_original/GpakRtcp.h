#ifndef _GPAK_RTCP_H
#define _GPAK_RTCP_H

// GPAK interface to RTCP functionality

// GPAK RTP/RTCP channel instance memory.
typedef struct RtcpStats_t { 
   ADT_UInt32 RTT;
   receiverReport_t rcvrRpt;
   senderReport_t   sndrRpt;
   sdesReport_t     sdesRpt;
   byeReport_t      byeRpt;
   appReport_t      appRpt;
   ADT_UInt8        sdesData[MAX_SDES_ITEMS][256];
   ADT_UInt8        byeData[256];
   ADT_UInt8        appData[256];
   ADT_UInt32       rcvrRptSSRC;   
   ADT_UInt32       sndrRptSSRC;
   ADT_UInt32       rcvRRCount; 
   ADT_UInt32       rcvSRCount; 
   ADT_UInt32       rcvSDESCount; 
   ADT_UInt32       rcvAPPCount; 
   ADT_UInt32       rcvEXTCount; 
   ADT_UInt32       rcvBYECount; 
   ADT_UInt32       rcvERRCount; 
   ADT_UInt32       rttNegCount; 
   ADT_UInt16       valid;
} RtcpStats_t;

typedef struct rtcpdata_t {
   RTPCONNECT          *sess;
   rtcpConfig_APIV5_t  cfg;
   ADT_UInt8           sdesText[MAX_SDES_ITEMS][256];
   int                 enabled;  
   ADT_UInt16          localRtcpPort;
   ADT_UInt16          remoteRtcpPort;
   ADT_UInt64          arrivalTime;

   // Scheduler callback fields.
   ADT_UInt32 NextTime;
   void (*Fxn) (void *);
   void *Sess;
   RtcpStats_t  RtcpStats;
} rtcpdata_t;
extern rtcpdata_t* const rtcpData[];
extern int numRtcpChannels;


// Callback pointers rtcpNetIn:
// callback from UDP rx packet processing in network stack. Used to determine whether
// received packet is an RTCP packet, and if so, determine the associated RTP Session
extern void* (*rtcpNetIn) (ADT_UInt32 ip, ADT_UInt16 port, ADT_UInt8 *payload);

// system level RTCP initialization. Must be called during startup.
extern void rtcp_system_init();

// process an IPv4 RTCP channel configuration message
extern int rtcp_process_cfg_msg(ADT_UInt16 *pCmd, ADT_UInt16 *pReply);

// process an IPv6 RTCP channel configuration message
extern int rtcp_process_cfg_msg_v6(ADT_UInt16 *pCmd, ADT_UInt16 *pReply);

// process an RTCP channel bye message
extern int rtcp_process_bye_msg(ADT_UInt16 *pCmd, ADT_UInt16 *pReply);

// Called from Gpak HOSTSWI to add RTCP packets into RTCP algorithm
extern int rtcp_rx_packet(ADT_UInt16 chanId, ADT_UInt8 *pktBuffer, ADT_UInt16 pktLenI8);

// indicate whether RTCP is enabled on this channel
extern int rtcp_channel_is_enabled(ADT_UInt16 ChanId);

// This function is called by gpak framework at the framerate. It executes the
// current RTCP-internal callback function that is scheduled to execute when the 
// callback's execution-timer has expired.  
extern void rtcp_execute_scheduled_callbacks(ADT_UInt16 chanId);

// clear out the gpak rtcp channel
extern void rtcp_channel_null (ADT_UInt16 ChanId);

// test function to validate ntp timing
extern void rtcp_debug_ntptime();

// read the rtcp stats and format into gpak message reply buffer. Return the 
// number of I16 words in the buffer.
extern int rtcp_read_stats (ADT_UInt16 chanId,  ADT_UInt16 *pReply);

#endif
