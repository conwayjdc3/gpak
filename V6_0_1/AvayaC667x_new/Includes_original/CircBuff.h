/*
 * Copyright (c) 2001-2007, Adaptive Digital Technologies, Inc.
 *
 * File Name: GpakDefs.h
 *
 * Description:
 *   This file contains common definitions related to G.PAK application
 *   software.
 *
 * Version: 1.0
 *
 * Revision History:
 *   11/07/01 - Initial release.
 *   05/2005  - Added GSM_EFR
 *    2/2007  - Converged C54 and C64
 *
 */

#ifndef _CIRCBUFF_H  /* prevent multiple inclusion */
#define _CIRCBUFF_H

#include "adt_typedef.h"

//==========================================================================

// Circular buffer information structure.

//==========================================================================
typedef struct {
   ADT_UInt16  *pBufrBase;    // pointer to base of circular buffer
   ADT_Word     BufrSize;     // size of buffer (words)
   ADT_Word     PutIndex;     // offset in buffer for next write
   ADT_Word     TakeIndex;    // offset in buffer for next read
   ADT_Word     SlipSamps;    // number of slip samples to compensate for
} CircBufInfo_t;

// Circular buffer routines
extern int  copyLinearToCirc (void *src, CircBufInfo_t *dest, ADT_Word len);
extern int copyCircToLinear (CircBufInfo_t *src, void *dest, ADT_Word len);
extern ADT_Word getFreeSpace (CircBufInfo_t *buf);
extern ADT_Word getAvailable (CircBufInfo_t *buf);
extern void addSlipToCirc (CircBufInfo_t *dest, ADT_Word slipCnt);

#endif  /* prevent multiple inclusion */
