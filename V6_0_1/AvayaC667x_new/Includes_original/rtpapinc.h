#ifndef  __RTPAPINC_H

#define __RTPAPINC_H
//==========================================================================

//         File:    rtpapinc.h
//  Description:    RTP/RTCP Application Program Interface header file.

//==========================================================================

#ifdef __cplusplus
extern "C" {
#endif



#ifndef _TI_
#define PAYLOAD_ALLOCATE
#else
#include <std.h>
#endif


#include "adt_typedef.h"

//==========================================================================
//   typedef and structure definitions
//==========================================================================
typedef enum { RTPRECEIVE, RTCPRECEIVE, RTPSEND, RTCPSEND} RTCPEVENTS;
typedef enum { PKT_ON_PROBATION, PKT_PURGE_EXISTING, PKT_VALID, PKT_OUT_OF_RANGE,
               PKT_OUT_OF_WINDOW, PKT_SEQ_DUPLICATE, PKT_OUT_OF_MEMORY,
               PKT_SESSION_NOT_ACTIVE, PKT_VERSION, PKT_PAYLOAD_SIZE,
               PKT_FORMAT,
               PKT_INVALID = 0x1000 } PKTSTATUS;

typedef void NETHANDLE;

typedef void NETFUNCTION (NETHANDLE*, ADT_UInt8*, ADT_UInt16*);
typedef PKTSTATUS RTPVALIDATION (ADT_UInt8*, ADT_UInt16, ADT_UInt16);


//  RTP Header format
typedef struct {
   ADT_UInt8 Data1[2];   // Bit fields = Vers:2, pad:1 xind:1 Cc:4 marker:1 pyldtype:7
   ADT_UInt8 Sequence[2];
   ADT_UInt8 TimeStamp[4];
   ADT_UInt8 SSRC[4];
   ADT_UInt8 Remainder[1];
} RTPHeader;

// Extension Header format;
typedef struct {
  ADT_UInt8 ProfileVars[2];
  ADT_UInt8 Length[2];
  ADT_UInt8 Remainder [1];
} RTPExtension;

typedef struct {
#ifndef PAYLOAD_ALLOCATE
   void      *Hndl;
#endif
   ADT_Int16  Size;
   ADT_UInt16 Type;
   ADT_UInt8 *Addr;
} PktDesc;

#define RTPHEADERSIZE 12
//==========================================================================
//   External interfaces
//==========================================================================
extern ADT_UInt32 RTP_DROPOUT;   // maximum delta allowed for sequence # gap.
extern ADT_UInt32 RTP_MISORDER;  // maximum delta allowed for sequence # out of order.

//  Application supplied RTP support functions
void *RTPAlloc(ADT_UInt32 size);
void RTPFree(void *);
void RTPLock();
void RTPUnlock();
void RTPWait();
ADT_UInt32 RTPTime ();

void htons_ (ADT_UInt8 *dst, ADT_UInt16 value);
void htonl_ (ADT_UInt8 *dst, ADT_UInt32 value);
ADT_UInt16 ntohs_ (ADT_UInt8 *src);
ADT_UInt32 ntohl_ (ADT_UInt8 *src);


//  RTP API calls
#define RTPCONNECTSIZE 23
#ifndef RTPREDEF
typedef struct rtpData {
    ADT_UInt32 structure_data [RTPCONNECTSIZE];
} RTPDATA;  


void RTPOpen (RTPDATA* RTPData, RTPVALIDATION* ProfileValidation,
              NETHANDLE*  RTPHandle,  NETFUNCTION* NetTransmit,
              ADT_UInt32 SSRC,            ADT_UInt32 RandomSeed, 
              ADT_UInt16 ProbationCount,  ADT_UInt16 JitterBufferDelay);

void RTPSend (RTPDATA* RTPData, PktDesc* Pyld, ADT_UInt32 TimeStamp, ADT_UInt16 Marker);

PKTSTATUS RTPBufferPacket (RTPDATA* RTPData, PktDesc *Pkt);

ADT_UInt16 RTPReceive (RTPDATA* RTPData, ADT_UInt32* TimeStart, ADT_UInt32 TimeEnd, PktDesc* Pyld);

void RTPClose (RTPDATA* RTPData);

//  RTCP calls
void RTCPStats (RTCPEVENTS event, RTPDATA* RTPData, ADT_UInt8* PktAddr, ADT_UInt16 PktSize);
#endif

#ifdef __cplusplus
}
#endif



#endif
