#ifndef _TONELOG_H
#define _TONELOG_H

//#define TONE_LOGGING // Tone logging
//#define JDC_TR

#define TONELOG_LEN 64

#define TONET  1
#define FRMNET 2
#define TOFWD  4
#define FRMFWD 8
#define TONEDETECT 0x10

typedef struct toneData_t {
   ADT_UInt16 ChanId;
   ADT_UInt16 Type;
   ADT_UInt16 PayldClass;
   ADT_UInt16 ToneIdx;
   ADT_UInt32 ToneDur;
   ADT_UInt32 PktTimeStamp;
   ADT_UInt32 ToneTimeStamp;
   ADT_UInt32 DmaCnt;
} toneData_t;

typedef struct toneLog_t {
   int idx;
   int dataEntries;
   int chan1;
   int chan2;
   int filter;
   toneData_t *data;
} toneLog_t;

extern toneLog_t toneLog;
extern void setToneLogMemory (void *memory, int memoryI8);


#endif
