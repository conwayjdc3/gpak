#ifndef _GPAK_BIOS_H
#define _GPAK_BIOS_H

#if defined(_TMS320C6600)
#include <xdc/std.h>
//#include <xdc/runtime/HeapStd.h>
#include <xdc/runtime/IHeap.h>
#include <xdc/runtime/Memory.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Types.h>
#include <xdc/runtime/Timestamp.h>
#include <xdc/runtime/Error.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Swi.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/knl/Task.h>
//#include <ti/sysbios/hal/Cache.h>
#include <ti/sysbios/family/c66/Cache.h>  // jdc try this
//#include <ti/sysbios/hal/Hwi.h>
#include <ti/sysbios/family/c64p/Hwi.h>
#include <ti/sysbios/family/c64p/Exception.h>
#include <ti/sysbios/family/c64p/EventCombiner.h>
#include <ti/sysbios/family/c66/tci66xx/CpIntc.h>
#include <ti/sysbios/heaps/HeapBuf.h>
#include <ti/sysbios/heaps/HeapMem.h>
// ---------------------------- TSK --------------------------------------
//------------------------------------------------------------------------
typedef struct TSK_Attrs {
    Int         priority;       /* task priority */
    Ptr         stack;          /* stack supplied  */
    size_t      stacksize;      /* size of stack */
#ifdef _55_
    size_t      sysstacksize;  /* size of system stack */
#endif
    Int         stackseg;       /* segment to allocate stack from */
    Ptr         environ;        /* environment pointer */
    String      name;           /* printable name */
    Bool        exitflag;       /* FALSE for server tasks */
    Bool        initstackflag;  /* FALSE disables stack initialization */
} TSK_Attrs;
static  TSK_Attrs TSK_ATTRS;
static  void * TSK_create(Fxn fxn, TSK_Attrs *attrs, ...) { 
Task_Params taskParams;
Task_Handle taskHandle;

    Task_Params_init(&taskParams);
    taskParams.priority = attrs->priority;
    taskParams.stack = attrs->stack;
    taskParams.stackSize = attrs->stacksize;
    taskHandle = Task_create ((Task_FuncPtr)fxn, &taskParams, NULL);
    return taskHandle;
}
#define TSK_exit Task_exit
#define TSK_sleep Task_sleep
#define TSK_Handle  Task_Handle
#define TSK_delete Task_delete
#define TSK_self Task_self
// ---------------------------- SWI --------------------------------------
//------------------------------------------------------------------------
#define SWI_Fxn Fxn
typedef struct SWI_Attrs {
    Fxn         fxn;
    int         arg0;
    int         arg1;
    int         priority;
    int         mailbox;
} SWI_Attrs;
#define SWI_Handle Swi_Handle
#define SWI_enable Swi_enable
#define SWI_disable Swi_disable
#define SWI_post Swi_post
#define SWI_or Swi_or
#define SWI_getmbox Swi_getTrigger
static  void * SWI_create (SWI_Attrs *pAtt) { 
Swi_Handle swi_handle;
Swi_Params swiParams;

    Swi_Params_init(&swiParams);
    swiParams.arg0 = pAtt->arg0;
    swiParams.arg1 = pAtt->arg1;
    swiParams.priority = pAtt->priority;
    swi_handle = Swi_create((Swi_FuncPtr)pAtt->fxn, &swiParams, NULL);
    return(swi_handle);
}

// ---------------------------- HWI --------------------------------------
//------------------------------------------------------------------------
#define HWI_disable Hwi_disable
#define HWI_restore Hwi_restore
extern Int setup_edma_interrupts(Hwi_FuncPtr edmaIsr);
extern Int setup_edmaErr_interrupts(Hwi_FuncPtr errIsr);
#define HWI_STACKCHECK_DISABLE


// ---------------------------- C64 --------------------------------------
//------------------------------------------------------------------------
#define C64_disableIER Hwi_disableIER
#define C64_enableIER Hwi_enableIER 
#define C64_clearIFR ;


// ---------------------------- SEM --------------------------------------
//------------------------------------------------------------------------
#define SYS_FOREVER BIOS_WAIT_FOREVER
typedef struct SEM_Attrs {
    Uns         type;
    String      name;       /* printable name */
} SEM_Attrs;
#define SEM_Handle Semaphore_Handle
#define SEM_pendBinary Semaphore_pend
#define SEM_postBinary Semaphore_post
static  inline void * SEM_create(Int count, SEM_Attrs *attrs) { 
Semaphore_Handle sem_handle;
Semaphore_Params semParams;

   Semaphore_Params_init(&semParams);
   semParams.mode = Semaphore_Mode_BINARY;
   sem_handle = Semaphore_create(0, &semParams, NULL);

   return sem_handle;
}

// ---------------------------- CLK --------------------------------------
//------------------------------------------------------------------------
static  inline Uint32 CLK_countspms(Void) {
Types_FreqHz freq;
Uint32 freq32;

   Timestamp_getFreq(&freq);
   freq32 = freq.lo;
   return freq32/1000;  
}
static  inline Uint32 CLK_gethtime(Void) {
Types_Timestamp64 ts64; 
   Timestamp_get64(&ts64);
   return ts64.lo;
}
static  inline Uint32 CLK_getltime(Void) {
Types_Timestamp64 ts64; 
   Timestamp_get64(&ts64);
   return ts64.hi;
}

// ---------------------------- EXC --------------------------------------
//------------------------------------------------------------------------
#define EXC_Status Exception_Status 
static  inline EXC_Status EXC_getLastStatus() { 
EXC_Status xstat; 
   Exception_getLastStatus(&xstat);
   return (xstat); 
}
#define EXC_clearLastStatus Exception_clearLastStatus


// ---------------------------- BCACHE -------------------------------------
//--------------------------------------------------------------------------
#define BCACHE_L1D  Cache_Type_L1D
#define BCACHE_L2   Cache_Type_L2D
#define BCACHE_FREEZE 0
#define BCACHE_NORMAL 1
static  inline void BCACHE_inv(void *a, int len, int flag) {
   Cache_inv((Ptr)a, (SizeT)len, Cache_Type_ALLD, (Bool)flag);
}
static  inline void BCACHE_wbInv(void *a, int len, int flag) {
   Cache_wbInv((Ptr)a, (SizeT)len, Cache_Type_ALLD, (Bool)flag);
}
static  inline void BCACHE_wb(void *a, int len, int flag) {
   Cache_wb((Ptr)a, (SizeT)len, Cache_Type_ALLD, (Bool)flag);
}
#define BCACHE_wbAll Cache_wbAll 
#define BCACHE_wait Cache_wait
#define BCACHE_wbInvAll Cache_wbInvAll
static  inline void BCACHE_setMode (int a, int b) {
   if (b == BCACHE_FREEZE)
       Cache_disable((Bits16)a);       
   else if (b == BCACHE_NORMAL)
       Cache_enable((Bits16)a);       
}


#else
//------------------------ BIOS 5 below --------------------------------------
//----------------------------------------------------------------------------
#include <bcache.h>
#include <swi.h>
#include <clk.h>
#include <hwi.h>
#include <swi.h>
#include <tsk.h>
#include <sem.h>
#include <c64.h>
#include <atm.h>

#ifndef BIOS_V_6
#include <exc.h>
#endif
#endif

#endif
