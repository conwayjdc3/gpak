/* G.PAK Config Tool generated command file. */

-priority

-I "..\..\..\ProjectLibs\c667x\Debug"
-I "..\..\..\ComponentObjs\c667x\Debug"
-I "..\..\..\V6_0_1\AvayaC667x"
-I "."
-I "..\..\..\V6_0_1\GpakDsp\Components64p"

-l "ADT_g711a1a2_EABI_c66x_V5_04_01_EL_Mml3_CG8_2_2.l66"
-l "ADT_G726_c66x_V1_01_EL_Mml3_CG8_2_2.l66"
-l "ADT_g729ab_EABI_c66x_V3_3_2_EL_Mml3_CG8_2_2.l66"

-l "FaxCEDDet_v0100.l64"
-l "FaxCNGDet_v0100.l64"
-l "ADT_DTMFDet_sd_c66x_V7_21_EL_Mml3_CG8_2_2.l66"
-l "ADT_gainsum_EABI_c66x_V0100_EL_Mml3_CG8_2_2.l66"
-l "ADT_TR_c66x_V7_21_EL_Mml3_CG8_2_2.l66"
-l "ADT_rtp_rtcp_nocpy_EABI_c66x_V5_03_EL_Mml3_CG8_2_2.l66"
-l "64xRtpSupport.obj"
-l "ADT_srtp_EABI_c66x_V1_4_EL_Mml3_CG8_2_2.l66"
-l "libmesiT38_v0100.a64"
-l "FaxRelay.obj"
-l "ADT_TG_sd_c66x_V7_21_EL_Mml3_CG8_2_2.l66"
-l "ADT_vadcng_c64xp_V4_06_EL_Mml3_CG8_2_2.l64p"
-l "ADT_g168_NEC_EABI_c64x_V11_19_01_EL_Mml3_CG7_3_2.l64"

-l "Pcm2Pkt.obj"
-l "GpakLSB.obj"
-l "GpakLSB2.obj"
-l "GpakLSB3.obj"
-l "GpakLSB4.obj"
-l "GpakLSB5.obj"
-l "G729_ADTWrapper.obj"
-l "64xRtpNetDelivery.obj"
-l "ipStackSupport.obj"
-l "gpakRtcp.obj"

-l "GpakDriverPjt.lib"
-l "GpakStdPjt.lib"
-l "GpakStubsPjt.lib"

_Start$Macros = 0;
_End$Macros = 0x001FFFFF;


SECTIONS
{

/* Per Port DARAM */
  HOST_API : > 0x80000000    /* Address of G.PAK interface structure */
  IPCFG :    > 0x80000004    /* Address of IP Configuration structure */
  TSIPCFG :  > 0x80000100    /* Address of TSIP Configuration structure */

  .core0init :  > 0x10800000
  .core1init :  > 0x11800000
  .core2init :  > 0x12800000
  .core3init :  > 0x13800000
  .core4init :  > 0x14800000
  .core5init :  > 0x15800000

/* Pre-Cache initialization */

  INIT_PROG_SECT: RUN_START(_Start$INIT_PROG_SECT), RUN_END (_End$INIT_PROG_SECT) {  } ALIGN (128) > DDR3

  .cinit   : {}          ALIGN (128) > DDR3 | L2SRAM | MSMCSRAM

  .bss      : RUN_START(_Start$bss), RUN_END (_End$bss) {}          > L2SRAM

  .far_bss  : RUN_START(_Start$far_bss), RUN_END (_End$far_bss) { *(.far) } > L2SRAM

/* Fast access memory sections */

  GROUP (FAST_PROG) : RUN_START(_Start$FAST_PROG), RUN_END (_End$FAST_PROG) {
		FAST_PROG_SECT
  } ALIGN (128) > DDR3

  DMABUFF0T : {} ALIGN(128) > L2SRAM
  DMABUFF0R : {} ALIGN(128) > L2SRAM

  DMABUFF1T : {} ALIGN(128) > L2SRAM
  DMABUFF1R : {} ALIGN(128) > L2SRAM

  DMABUFF2T : {} ALIGN (128) >> DDR3
  DMABUFF2R : {} ALIGN (128) >> DDR3

  RxTransBuff : {} ALIGN(128) > L2SRAM
  TxTransBuff : {} ALIGN(128) > L2SRAM


  FAST_DATA_SECT: RUN_START(_Start$FAST_DATA_SECT), RUN_END (_End$FAST_DATA_SECT) {} ALIGN (128) > L2SRAM

  SHARED_LOCK_SECT: {} > L2SRAM fill=0xffffffff  /* Lock values initialized to -1 to force waits */

  SHARED_DATA_SECT: RUN_START(_Start$SHARED_DATA_SECT), RUN_END (_End$SHARED_DATA_SECT) {
  } ALIGN (128) > DDR3   /* Data shared across CPUs */

  PER_CORE_DATA: RUN_START(_Start$PER_CORE_DATA), RUN_END (_End$PER_CORE_DATA) {
		*(FAST_SCRATCH)
		*(SLOW_SCRATCH)
		*(IN_WORK)
		*(OUT_WORK)
		*(ECFAR_WORK)
  } > L2SRAM


  NETWORK_TASK_STACK_SECT: RUN_START(_Start$NETWORK_TASK_STACK_SECT), RUN_END (_End$NETWORK_TASK_STACK_SECT) {
		*(STACK_ALLOCATION:MainIPTask)
		*(STACK_ALLOCATION:RTPTask)
  } ALIGN (128) > L2SRAM


  MSG_TASK_STACK_SECT: RUN_START(_Start$MSG_TASK_STACK_SECT), RUN_END (_End$MSG_TASK_STACK_SECT) {
		*(STACK_ALLOCATION:MsgTask)
  } ALIGN (128) >> DDR3


  HOST_DATA_SECT: RUN_START(_Start$HOST_DATA_SECT), RUN_END (_End$HOST_DATA_SECT) {
		*(GPAK_IF)
		*(CMDMSGBUF)
		*(RPLYMSGBUF)
		*(evt_CIRC)
		*(evt_POOL)

		*(RTP_CIRC)        /* RTP host packet interface */

		*(pkt_CIRC)
  } ALIGN (8) > MSMCSRAM

  RTP_DATA_SECT: {
		*(RTP_POOL:ToNet)   /* RTP host packet interface */
		*(RTP_POOL:ToDSP)   /* RTP host packet interface */
  } ALIGN (128) >> DDR3


  NON_CACHED_DATA: RUN_START(_Start$NON_CACHED_DATA), RUN_END (_End$NON_CACHED_DATA) {
		*(CHAN_INST_DATA:Chan)
		*(POOL_ALLOC)
		*(SHARED_DATA_SECT:logging)
		*(STUB_DATA_SECT)
  } ALIGN (8) > MSMCSRAM

  PCM_DATA: {
		*(pktIn_POOL)
		*(pktOut_POOL)
		*(pcmIn_POOL)
		*(pcmOut_POOL)
  } ALIGN (128) > DDR3

}
-l "AvayaC6678Cust.cmd"
