#include <std.h>
#include <strings.h>
#include <GpakBios.h>

#include <ti/ndk/inc/netmain.h>
#include <ti/ndk/inc/os/osif.h>
#include <ti/ndk/inc/stkmain.h>
//}-------------------------------------------------------------------------

#include <adt_typedef_user.h>
#include <ipStackUser.h>
#include <ipStackStats.h>
#include <netbuffer.h>
#include <gen_udp.h>
#include "ipdefs.h"

#include <ti/ndk/inc/stkmain.h>
#include <ti/ndk/inc/netmain.h>
int gpak_udp_echo( SOCKET s, uint32_t unused )
{
    struct sockaddr_in sin1;
    struct timeval     to;
    int                i,tmp;
    char               *pBuf;
    void*             hBuffer;

    (void)unused;

    // Configure our socket timeout to be 3 seconds
    to.tv_sec  = 3;
    to.tv_usec = 0;
    setsockopt( s, SOL_SOCKET, SO_SNDTIMEO, &to, sizeof( to ) );
    setsockopt( s, SOL_SOCKET, SO_RCVTIMEO, &to, sizeof( to ) );

    for(;;)
    {
        tmp = sizeof( sin1 );
        i = (int)recvncfrom( s, (void **)&pBuf, 0, (PSA) &sin1, &tmp, &hBuffer );

        // Spit any data back out
        if( i >= 0 )
        {
            sendto( s, pBuf, i, 0, (PSA) &sin1, sizeof(sin1) );
            recvncfree( hBuffer );
        }
        else
            break;
    }

    // Since the socket is still open, return "1"
    // (we need to leave UDP sockets open)
    return(1);    
}
