//-------------------------------------------------------------------------
//{  RTPTaskMultiCore.c
//
//  Performs socket operations (open/read/write/close) for G.PAK messages
//  and RTP packets over the IP stack
//
//  The IPToSess array links IP and port addresses to sockets and RTP sessions.
//  The sktList array identifies to the IP stack which sockets will be
//  read by the task.
//
//  G.PAK messages are stored in ApiBlock.CmdMsgPointer and replies found in
//  ApiBlock.ReplyMsgPointer are sent back to the originating socket.
//
//  Inbound RTP packets are stored in the corresponding jitter buffer and
//  outbound RTP packets are sent over the network to their corresponding
//  destination IP:port.
//
#include <std.h>
#include <strings.h>
#include <GpakBios.h>

#include <ti/ndk/inc/netmain.h>
#include <ti/ndk/inc/os/osif.h>
#include <ti/ndk/inc/stkmain.h>
#include <ti/ndk/inc/gpak_mods.h>
//}-------------------------------------------------------------------------

#include <adt_typedef_user.h>
#include <ipStackUser.h>
#include <ipStackStats.h>
#include <netbuffer.h>
#include <gen_udp.h>
#include "ipdefs.h"

#define UDP_MESSAGING       // un-comment this to enable UDP transport for control and event messages
#define UDP_TXRPLY_RETRY 5  // num resend attempts if UDP message reply send fails

//#define EMAC_STATS
#ifdef EMAC_STATS
// for emac stats
#include <ti/drv/emac/emac_drv.h>
#endif

extern HANDLE (*appUdpInput) (PBM_Pkt **ppPkt, UDPHDR *pUdpHdr);
extern int getMsgBuffers (void **msgBuff, void **rplyBuff, void **_evtBuff);
extern void closeMsgConnection ();
extern int msgBuffInUse ();
extern int msgReplyI8 ();
extern int evtReplyI8 ();
extern int acceptMsg (int msgI8);
extern void closeMsgConnection ();
extern int freePktHandles (int coreID);
extern int rtpGetPacket (void *IPBuff, int IPBuffI8, ADT_UInt16 *ChanID, int coreID);
extern int fastRTPsend (void *rtpBuf, int rtpI8, IPToSess_t *IPMapping);
extern int sendRTP (SOCKET s, void *pbuf, int size, int flags, void *ipToSess);
extern int sendRTP6 (SOCKET s, void *pbuf, int size, int flags, void *ipToSess);
extern void *RTPAlloc (ADT_UInt32 sizeI8);
extern void IGMPDeleteRTPMCastIP (IPN mcIpAddr);
extern uint32_t IGMPAddRTPMCastIP (IPN mcIpAddr);
extern void MulticastIPToMulticastMAC (IPN IpAddr, ADT_UInt8 *MacAddr);
extern int rtcp_is_valid_header(ADT_UInt8 *payload);
extern int storeInboundRTPPacket (ADT_UInt16 chanID, void *RTPBuff, int pktI8, void *PktHndl);
extern int areSameIp(ADT_UInt8 *ip1, ADT_UInt8 *ip2);
extern ADT_UInt8 isNonZeroIp(ADT_UInt8 *ip);

extern far NETIF_DEVICE *EMAC_NetIF;
extern volatile int network_up; // jdc ipv6 debug

#ifdef FASTRTPENAB
far int fast_rtp_enabled = 1;
far int fast_rtp_from_task = 1;
#else
far int fast_rtp_enabled = 0;
far int fast_rtp_from_task = 0;
#endif
#pragma DATA_SECTION (fast_rtp_enabled,  "NON_CACHED_DATA:IPStack")
#pragma DATA_SECTION (fast_rtp_from_task,  "NON_CACHED_DATA:IPStack")

#ifdef EMAC_STATS
EMAC_STATISTICS_T emac_stats;
int read_emac_stats = 0;
#endif


#ifndef _DEBUG
   #undef  AppErr
   #define AppErr(msg,cond)
   #define logTime(type)
   #define hwi_disable(a)
   #define hwi_enable(a)
   #define endTimerUs(timer, min, divisor)
   #define endMinTimerUs(timer, min, divisor)
   #define logIPSess(code,sess)
#else
   //static long loopTimer;
   static long timer1, timer2;
   int  maska;
   #define hwi_disable(a) a = HWI_disable (); 
   #define hwi_enable(a)  HWI_restore (a);
   static inline void endMinTimerUs (long timer, ADT_UInt32 *min, int divisor) {
      ADT_UInt32 duration;
      duration = CLK_gethtime () - timer;
      duration /= divisor;
      duration /= (CLK_countspms() / 1000);
      if (duration < *min) *min = duration;
   }
   int ipSesslastTime = 0;
   int ipSessIdx = 0;
   typedef struct IPSessLog_t {
      int code;
      int chan;
      int idx;
      int hashIdx;
      int delta;
   } IPSessLog_t;
   IPSessLog_t IPSessLog [0x100];

   void logIPSess (int code, IPToSess_t* ipSess) {
      IPSessLog_t* log;
      int time;

      ipSessIdx++;
      if (0xff < ipSessIdx) ipSessIdx = 0;
      log = &IPSessLog[ipSessIdx];

      log->code = code;
      log->chan = ipSess->ChanId;
      log->idx  = ipSess->idx;
      log->hashIdx = ipSess->hashIdx;
      time = CLK_gethtime ();
      log->delta = time - ipSesslastTime;
      ipSesslastTime = time;
   }
extern void AppError (char *FileName, int Line, char *Msg, int Error);
#define AppErr(msg,cond) AppError (__FILE__, __LINE__, msg, cond)
#endif

extern far int DSPTotalCores;
extern char HostName[30];
extern ADT_UInt16 RTPScratchI8;
extern far int useLclPortAsChannel;

#ifdef UDP_MESSAGING
// Socket list indices for UDP messaging
#define RTP_IDX      0
#define MSG_IDX      1
#define EVT_IDX      2
#define SOCK_COUNT   3

#ifdef _INCLUDE_IPv6_CODE
#define RTP6_IDX       3
#define MSG6_IDX       4
#define EVT6_IDX       5
#undef  SOCK_COUNT
#define SOCK_COUNT     6
#endif
#else
// Socket list indices for TCP messaging
#define RTP_IDX      0
#define MSG_LSTN_IDX 1
#define EVT_LSTN_IDX 2
#define MSG_IDX      3
#define SOCK_COUNT   4

#ifdef _INCLUDE_IPv6_CODE
#define RTP6_IDX       4
#define MSG6_LSTN_IDX  5
#define EVT6_LSTN_IDX  6
#define MSG6_IDX       7
#undef  SOCK_COUNT
#define SOCK_COUNT     8
#endif
#endif

unsigned short int RTP_port_range = (unsigned short int)DEFAULT_RTP_PORT_RANGE_DEFAULT;

extern int  IPMap_lock ();
extern void IPMap_unlock (int mask);
_IPStats IPStats;
#pragma DATA_SECTION (IPStats, "STATS:IPStack")

static far int RTPTaskInit = FALSE;
#pragma DATA_SECTION (RTPTaskInit,  "NON_CACHED_DATA:IPStack")

far int fireHoseTest = ADT_FALSE;
#pragma DATA_SECTION (fireHoseTest,  "NON_CACHED_DATA:IPStack")

static ADT_UInt32 *RTPBuff;     // Pointer to allocated RTP payload transfer buffer
static ADT_UInt8  *InBuff;      // Pointer to udp payload within the internal packet buffer
static HANDLE      InBuffHndl;  // Pointer to stack's internal packet buffer

#ifdef _INCLUDE_IPv6_CODE
extern void addGlobalIpv6();
extern void initIp6CallbackPtrs();
extern void gpakIPv6DisplayRouteTable (void);

// test code -----------------------------------------
#ifdef IPV6_TEST_UDP
int sendUdpPktToIPv6(
	ADT_UInt8 ipAddress[],		// destination IP Address
	ADT_UInt16 portNum,			// destination UDP Port Number
	ADT_UInt8 *pPayload,		// pointer to UDP payload
	ADT_UInt16 paylenI8			// length of UDP payload (bytes)
	);

#define MAX_BUFF6_LENI8 512
static ADT_UInt8 InBuff6[MAX_BUFF6_LENI8];
int udp6_echo = 0;
int udp6_txerr = 0;
int udp6_tx = 0;
char *remote_ip6 = "fe80::b55c:ef7e:cb55:4630";
IP6N  txudp6_ip = {0};
ADT_UInt16 txudp6_port = 6334;
ADT_UInt8  txudp6_buff[256];
ADT_UInt16 txudp6_lenI8 = 0;
#endif
// ---------------------------------------------------

extern far int gpak_ipv6_device_id;
struct sockaddr_in6   sktAddr6;
SOCKET                rtpSkt6;
#pragma DATA_SECTION (rtpSkt6,  "NON_CACHED_DATA:IPStack")
#pragma DATA_SECTION (sktAddr6,  "NON_CACHED_DATA:IPStack")

static IP6N msgIP6;
static IP6N evtIP6;
#pragma DATA_SECTION (msgIP6,  "NON_CACHED_DATA:IPStack")
#pragma DATA_SECTION (evtIP6,  "NON_CACHED_DATA:IPStack")
#endif

static FDPOLLITEM sktList [SOCK_COUNT];
static IPN msgIP;
static IPN evtIP;
static int sktCnt;
#pragma DATA_SECTION (msgIP,    "NON_CACHED_DATA:IPStack")
#pragma DATA_SECTION (evtIP,    "NON_CACHED_DATA:IPStack")

VLAN_TAG VLANTags[10];
#pragma DATA_SECTION (VLANTags, "NON_CACHED_DATA")

IPToSess_t IPToSess [0x100];
#pragma DATA_SECTION (IPToSess, "NON_CACHED_DATA")
#pragma DATA_ALIGN   (IPToSess, 128)

IPToSess_t nullIPSess;
#pragma DATA_SECTION (nullIPSess, "NON_CACHED_DATA")
#pragma DATA_ALIGN   (nullIPSess, 128)

IPToSess_t *chnToIPSess [0x100];  // Indexed by channel ID to point to IP session lookup table
#pragma DATA_SECTION (chnToIPSess,  "NON_CACHED_DATA")
#pragma DATA_ALIGN   (chnToIPSess, 128)


#ifdef RTCP_ENABLED
//#include "GpakRtcp.h"
#include "GpakDefs.h"

IPToSess_t RTCPIPToSess [0x100];
#pragma DATA_SECTION (RTCPIPToSess, "NON_CACHED_DATA")
#pragma DATA_ALIGN   (RTCPIPToSess, 128)

IPToSess_t *RTCPchnToIPSess [0x100];  // Indexed by channel ID to point to IP session lookup table
#pragma DATA_SECTION (RTCPchnToIPSess,  "NON_CACHED_DATA")
#pragma DATA_ALIGN   (RTCPchnToIPSess, 128)
#endif

extern far void (*MACUpdate) (IPN rmtIP, ADT_UInt8 *MAC);

void* (*customNetIn) (ADT_UInt32 ip, ADT_UInt16 port, ADT_UInt8 *payload) = NULL;


void queueMultiCastRequest (IPN ip, int msgType);


// - - - - - - - - - - - - - -  moved from udp.c - - - - - - - - - - - - - - - -

far UINT16  evtPort = 0;
far UINT16  msgPort = 0;
far UINT16 lowRTPPort = 6334, highRTPPort = 6334;
far HANDLE rtpSocket = NULL;
#pragma DATA_SECTION (lowRTPPort,  "NON_CACHED_DATA:IPStack")
#pragma DATA_SECTION (highRTPPort, "NON_CACHED_DATA:IPStack")
#pragma DATA_SECTION (rtpSocket,   "NON_CACHED_DATA:IPStack")

#ifdef UDP_MESSAGING
far HANDLE  msgSocket = NULL;
far UINT16  msgSrcPort = 0;
far HANDLE  evtSocket = NULL;
far UINT16  evtSrcPort = 0;
#pragma DATA_SECTION (msgPort,     "NON_CACHED_DATA:IPStack")
#pragma DATA_SECTION (msgSrcPort,  "NON_CACHED_DATA:IPStack")
#pragma DATA_SECTION (msgSocket,   "NON_CACHED_DATA:IPStack")
#pragma DATA_SECTION (evtPort,     "NON_CACHED_DATA:IPStack")
#pragma DATA_SECTION (evtSrcPort,  "NON_CACHED_DATA:IPStack")
#pragma DATA_SECTION (evtSocket,   "NON_CACHED_DATA:IPStack")

#pragma CODE_SECTION (verifyDisconnectTag, "SLOW_PROG_SECT")
const char *GpakDisconnect="disconnect";
int verifyDisconnectTag (void *MsgPtr, int msgI8) {
   char *msgPtr;   
   int i;

   msgPtr =  (char *) MsgPtr;

   for (i=0; i<22;i++) {
        if (*msgPtr++ != HostName[i])
            return 0;
   }
   for (i=0; i<10;i++) {
        if (*msgPtr++ != GpakDisconnect[i])
            return 0;
   }
   return 1;    
}

#endif

far int useLclPortAsChannel = ADT_TRUE;
#pragma DATA_SECTION (useLclPortAsChannel, "NON_CACHED_DATA:IPStack")

extern far int fireHoseTest;

void RTPPorts (HANDLE rtpSock, UINT16 lowRTP, UINT16 RTPPortRange) {
   lowRTPPort  = (UINT16) lowRTP;
   highRTPPort = (UINT16) (lowRTP + RTPPortRange);
   rtpSocket = rtpSock;
}

int rtpPortValid (UINT16 rtpPort) {
   return (lowRTPPort <= rtpPort) && (rtpPort <= highRTPPort);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#ifdef FASTRTPENAB
SOCKET hRtpRawEthSock = INVALID_SOCKET;		// RTP Tx raw socket
int rtpRawEthSockStatus = -1;				// RTP Tx raw socket status (0 = OK)
#pragma DATA_SECTION (hRtpRawEthSock, "NON_CACHED_DATA:IPStack")
#pragma DATA_SECTION (rtpRawEthSockStatus, "NON_CACHED_DATA:IPStack")
#endif

PBMQ rtpRxPbmQueue;                         // RTP Rx PBM queue
#ifdef USE_GENERAL_TX_SOCKET
void sendUdpPktTo(
	ADT_UInt32 ipAddress,		// destination IP Address
	ADT_UInt16 portNum,			// destination UDP Port Number
	ADT_UInt8 *pPayload,		// pointer to UDP payload
	ADT_UInt16 length			// length of UDP payload (bytes)
	);
SOCKET hGenUdpTxSock = INVALID_SOCKET;	// General UDP Send socket
PBMQ genUdpTxPbmQueue;
#define GEN_UDP_MAX_LEN 160					// General UDP Send PBM queue
ADT_UInt8 txGenUdpBuf[GEN_UDP_MAX_LEN];  
gen_udp_t gen_udp = {0,0,160,20,0,1, 6334, 0xC0A8001E, 0};

// Called from gpak 1ms timer function to schedule gen udp transmit
void gen_udp_timer() {
   if (gen_udp.enabled) {
        if (gen_udp.countdown) {
            gen_udp.countdown--;
        }
        if (gen_udp.countdown == 0) {
            gen_udp.send = 1;
            gen_udp.countdown = gen_udp.periodMs;

            // if enabled flag LT0, transmit forever, else tx fixed number of packets       
            if (gen_udp.enabled > 0)
                gen_udp.enabled--;
        }
   }
}
#endif

// Application handler for received UDP packets for multicore based G.PAK.
//
//  Return:
//    Handle of destination socket (NULL = unknown)
//    *ppPkt - set to NULL if packet directly consumed
static HANDLE appUdpInputMulticore(PBM_Pkt **ppPkt, UDPHDR *pUdpHdr)
{
	UINT32 chID;
	UINT16 dstPort;
    PBM_Pkt *pPkt = *ppPkt;

	dstPort = HNC16(pUdpHdr->DstPort);

	if (fireHoseTest)
	{
		chID = (dstPort - lowRTPPort) & 0xff;
		pPkt->Aux1 |= (chID << 24);
	}
	else if (useLclPortAsChannel)
	{
		chID = ((dstPort - lowRTPPort) / 2) & 0xff;
		pPkt->Aux1 |= (chID << 24);
	}

#ifdef UDP_MESSAGING
    if (dstPort == msgPort) {
        if (msgSrcPort == 0)
	        msgSrcPort = HNC16(pUdpHdr->SrcPort);
        return  msgSocket;
    }

    if (dstPort == evtPort) {
          if (evtSrcPort == 0)
              evtSrcPort = HNC16(pUdpHdr->SrcPort);
          return  evtSocket;
      }
#endif

	// Replace any destination UDP ports within the RTP port range for delivery
	// to the RTP default port number (lowRTPPort)
	if ((dstPort >= lowRTPPort) && (dstPort <= highRTPPort))
	{
		pUdpHdr->DstPort = HNC16(lowRTPPort);
		return rtpSocket;
	}

	return NULL;
}




#pragma CODE_SECTION (fastRTPMap, "FAST_PROG_SECT")
ADT_Bool fastRTPMap (int chanID, IPToSess_t **ipSess) {
   ADT_Bool valid;
   int mask;

   *ipSess = chnToIPSess[chanID & 0xff];
   mask = IPMap_lock ();
   valid = ((*ipSess)->ChanId == chanID) && ((*ipSess)->lcl.SSRC) && ((*ipSess)->lcl.IP != (*ipSess)->rmt.IP);
   IPMap_unlock (mask);

   if (fast_rtp_enabled && (!fast_rtp_from_task))
   		return valid;
   else
        return 0;
}

#ifdef RTCP_ENABLED
#pragma CODE_SECTION (fastRTCPMap, "FAST_PROG_SECT")
ADT_Bool fastRTCPMap (int chanID, IPToSess_t **ipSess) {
   ADT_Bool valid, validRtp;
   IPToSess_t *rtpIpSess;
   int sameRmtIp;
   int mask;

   *ipSess = RTCPchnToIPSess[chanID & 0xff];
   rtpIpSess = chnToIPSess[chanID & 0xff];
   mask = IPMap_lock ();
   sameRmtIp = (rtpIpSess->rmt.IP == (*ipSess)->rmt.IP);
   valid = ((*ipSess)->ChanId == chanID) && ((*ipSess)->lcl.SSRC) && ((*ipSess)->lcl.IP != (*ipSess)->rmt.IP) && sameRmtIp;
   if (!valid) {
        validRtp = (rtpIpSess->ChanId == chanID) && (rtpIpSess->lcl.SSRC) && (rtpIpSess->lcl.IP != rtpIpSess->rmt.IP);
        // RTP session has mac resolved, but not RTCP. Update RTCP session with MAC address.
        if (validRtp && sameRmtIp) {
            memcpy((*ipSess)->rmt.MAC, rtpIpSess->rmt.MAC,6);
            (*ipSess)->lcl.SSRC = 1;
            valid = 1;
        }
   }
   IPMap_unlock (mask);
   return valid;
}
#endif

// Called when ARP determines IP to MAC mapping.
#pragma CODE_SECTION (IPSessionUpdate, "PKT_PROG_SECT")
static void IPSessionUpdate (IPN rmt_dstIP, ADT_UInt8 *MAC) {
   IPToSess_t *ipSess;
   int i;

   for (i=0, ipSess=IPToSess; i < 0x100; i++, ipSess++) {
      if (ipSess->rmt.dstIP != 0 ) {
         if(ipSess->rmt.dstIP != rmt_dstIP)
		    continue;
	  } else {
         if(ipSess->rmt.IP == 0) {
		    continue;
		 } else {
		    if (ipSess->rmt.IP != rmt_dstIP) 
		       continue;  
		 }
      }
	  //if(IN_MULTICAST (rmt->dstIP)) // don't update MAC due to possible ARP problem
	  //    continue;
      memcpy (ipSess->rmt.MAC, MAC, 6);
      ipSess->lcl.SSRC = 1;
      logIPSess (0, ipSess);
   }
   return;
}

// Called when findMACForIP needs to search IP to MAC mapping.
#pragma CODE_SECTION (findMAC_IPSessionUpdate, "PKT_PROG_SECT")
static void findMAC_IPSessionUpdate (IPN rmt_dstIP, ADT_UInt8 *MAC) {
   IPToSess_t *ipSess;
   int i;

   for (i=0, ipSess=IPToSess; i < 0x100; i++, ipSess++) {
      if (ipSess->rmt.dstIP != 0 ) {
         if(ipSess->rmt.dstIP != rmt_dstIP)
		    continue;
	  } else if(ipSess->rmt.IP != 0) {
		   if (ipSess->rmt.IP != rmt_dstIP) 
		       continue;  
      } else if(ipSess->rmt.IP == 0) {
    	  continue;
      }
      if(ipSess->lcl.SSRC == 1)
    	  continue;
      memcpy (ipSess->rmt.MAC, MAC, 6);
      ipSess->lcl.SSRC = 1;
      logIPSess (0, ipSess);
   }
   return;
}


#pragma CODE_SECTION (findMACForIP, "PKT_PROG_SECT")
static void findMACForIP (IPN rmt_dstIP, ADT_UInt8 *MAC) {
   IPToSess_t *ipSess;
   int i;

   for (i=0, ipSess=IPToSess; i < 0x100; i++, ipSess++) {
      if (ipSess->lcl.SSRC == 0)   continue;
      if (ipSess->rmt.dstIP != 0 ) {
    	  if (ipSess->rmt.dstIP != rmt_dstIP) continue;
      } else { // rmt.IP
    	  if (ipSess->rmt.IP != rmt_dstIP) continue;
      }
      findMAC_IPSessionUpdate (rmt_dstIP, ipSess->rmt.MAC);
      return;
   }
   return;
}


#ifdef RTCP_ENABLED
// Search the IPSession table for an entry with a matching IP address that has 
// resolved the MAC address.
#pragma CODE_SECTION (findMACForIP_RTCP, "PKT_PROG_SECT")
static int findMACForIP_RTCP (IPN rmt_dstIP, ADT_UInt8 *MAC) {
   IPToSess_t *ipSess;
   int i;

   for (i=0, ipSess=IPToSess; i < 0x100; i++, ipSess++) {
      if (ipSess->lcl.SSRC == 0)   continue;
      if (ipSess->rmt.dstIP != 0 ) {
    	  if (ipSess->rmt.dstIP != rmt_dstIP) continue;
      } else { // rmt.IP
    	  if (ipSess->rmt.IP != rmt_dstIP) continue;
      }
      memcpy (MAC, ipSess->rmt.MAC, 6);
      return 1;
   }
   return 0;
}

#ifdef _INCLUDE_IPv6_CODE
#pragma CODE_SECTION (findMACForIP_RTCP6, "PKT_PROG_SECT")
int findMACForIP_RTCP6 (ADT_UInt8 *rmt_dstIP, ADT_UInt8 *MAC) {
   IPToSess_t *ipSess;
   int i;

   for (i=0, ipSess=IPToSess; i < 0x100; i++, ipSess++) {
      if (ipSess->lcl6.SSRC == 0)   continue;
      if (isNonZeroIp(ipSess->rmt6.dstIP)) {
    	  if (areSameIp(ipSess->rmt6.dstIP, rmt_dstIP)==0) continue;
      } else { // rmt.IP
    	  if (areSameIp(ipSess->rmt6.IP, rmt_dstIP)==0) continue;
      }
      memcpy (MAC, ipSess->rmt6.MAC, 6);
      return 1;
   }
   return 0;
}
#endif
#endif


#pragma CODE_SECTION (ipToChanLookup, "PKT_PROG_SECT")
IPToSess_t *ipToChanLookup (IPN rmtIP, ADT_UInt16 rmtPort, int *chanId) {
   int hashIPAddr, hashPort;
   int storeIdx, hashIdx;
   IPToSess_t *openIPSess;
   register IPToSess_t *ipSess;

   hashIPAddr = (int) rmtIP;
   hashPort   = (int) rmtPort;
   hashIdx = (hashIPAddr ^ hashIPAddr >> 8 ^ hashIPAddr >> 16 ^ hashIPAddr >> 24 ^ hashPort ^ hashPort >> 8) & 0xff;

   storeIdx = hashIdx;
   ipSess = &IPToSess[storeIdx];
   openIPSess = NULL;
   do {
      if (ipSess->RTPSession != NULL && ipSess->rmt.IP == rmtIP && ipSess->rmt.Port == rmtPort) {
        openIPSess = ipSess;
        break;
      }

      // Move to next ipSess entry, checking for wrap
      storeIdx = (storeIdx + 1) & 0xff;
      ipSess++;
      if (storeIdx == 0) ipSess = IPToSess;                   // Index wrap

   } while (storeIdx != hashIdx);

   if (openIPSess) {
        *chanId = openIPSess->ChanId;
        return (openIPSess);
   } else {
        *chanId = -1;
        return (IPToSess_t *)0;
   }
        
} 

//{
// Hash search of IPToSess table for matching remote IPAddr and port or next available entry
// A hashing algorithm is used to generate an 'best' index into the lookup table.  The lookup
// table is then searched forward (incrementing the index by one each iteration) until a matching
// or available entry is found
//
// Reomte IP and port may occur multiple times with different SSRC values.
//}

#pragma CODE_SECTION (IPSessionLookup, "PKT_PROG_SECT")
IPToSess_t *IPSessionLookup (IPN rmtIP, ADT_UInt16 rmtPort, ADT_UInt32 rmtSSRC, ADT_Bool new) {
   int hashIPAddr, hashPort;
   int storeIdx, hashIdx;
   IPToSess_t *openIPSess;
   register IPToSess_t *ipSess;

   hashIPAddr = (int) rmtIP;
   hashPort   = (int) rmtPort;
   hashIdx = (hashIPAddr ^ hashIPAddr >> 8 ^ hashIPAddr >> 16 ^ hashIPAddr >> 24 ^ hashPort ^ hashPort >> 8) & 0xff;

   storeIdx = hashIdx;
   ipSess = &IPToSess[storeIdx];
   openIPSess = NULL;

   if (!new) {
      // When searching for existing entries must find entry with:
      //    RTPSession defined && matching rmtIP && matching rmtPort
      //    if (entries rmtSSRC != 0) rmtSSRC must match
      do {
         if (ipSess->RTPSession != NULL && (ipSess->ipver != 6) && ipSess->rmt.IP == rmtIP && ipSess->rmt.Port == rmtPort) {
            if (ipSess->rmt.SSRC == rmtSSRC) return ipSess;   // Full match
            
            // Store first entry of unassigned matching IP/port for match if no others are found (first time for SSRC).
            if (openIPSess == NULL && ipSess->rmt.SSRC == 0) openIPSess = ipSess;
         }

         // Move to next ipSess entry, checking for wrap
         storeIdx = (storeIdx + 1) & 0xff;
         ipSess++;
         if (storeIdx == 0) ipSess = IPToSess;                   // Index wrap

      } while (storeIdx != hashIdx);

      // First packet for IP/port with unassigned SSRC.  Assign SSRC.
      if (openIPSess) openIPSess->rmt.SSRC = rmtSSRC;
      return openIPSess;
   }

   // For new entries, must find:
   //    first unused entry
   while (ipSess->RTPSession != 0) {

      // Check for wrap
      storeIdx = (storeIdx + 1) & 0xff;
      if (storeIdx == hashIdx) return NULL;

      ipSess++;
      if (storeIdx == 0) ipSess = IPToSess;   // Index wrap
   }

   // Channel setup.  Assign hash index to entry
   ipSess->idx = storeIdx;
   ipSess->hashIdx = hashIdx;
   ipSess->ipver = 4;
   return ipSess;
}

#pragma CODE_SECTION (IPSessionLookupRxMcast, "PKT_PROG_SECT")
IPToSess_t *IPSessionLookupRxMcast (IPN mcastIP, ADT_UInt16 mcastPort, ADT_UInt8 *matchList, ADT_UInt8 *listLen) {
   int hashIPAddr, hashPort;
   int storeIdx, hashIdx;
   IPToSess_t *openIPSess;
   register IPToSess_t *ipSess;
   ADT_UInt8 ListLen = 0;

   hashIPAddr = (int) mcastIP;
   hashPort   = (int) mcastPort;
   hashIdx = (hashIPAddr ^ hashIPAddr >> 8 ^ hashIPAddr >> 16 ^ hashIPAddr >> 24 ^ hashPort ^ hashPort >> 8) & 0xff;

   storeIdx = hashIdx;
   ipSess = &IPToSess[storeIdx];
   openIPSess = NULL;

    // When searching for existing entries must find entry with:
    //    RTPSession defined && matching rmtIP && matching rmtPort
    do {
       if (ipSess->RTPSession != NULL && ipSess->lcl.dstIP == mcastIP && ipSess->lcl.Port == mcastPort) 
       {
           matchList[ListLen++] = (ADT_UInt8)ipSess->ChanId;
           if (openIPSess == 0) openIPSess = ipSess;
       }

       // Move to next ipSess entry, checking for wrap
       storeIdx = (storeIdx + 1) & 0xff;
       ipSess++;
       if (storeIdx == 0) ipSess = IPToSess;                   // Index wrap

    } while (storeIdx != hashIdx);
    *listLen = ListLen;

    return openIPSess;
}

//
//{  Remove entry from IPSession lookup
//
//   Entry in RTPSession replaced with last entry in block with
//   its hashidx equal to its own hashidx
//
//
//   0 - failure (IPAddr:port already exists or table full)
//   1 - success (empty entry found)
//}
#pragma CODE_SECTION (removeIPSessionEntry, "SLOW_PROG_SECT")
static void removeIPSessionEntry (IPToSess_t * volatile deletedEntry) {
   IPToSess_t *nxtEntry, *matchedEntry;
   int idx, hashIdx, mask;
   ADT_UInt16 ChanId;
   IPN rxIP;
#ifdef IPV6_MCAST // jdc todo
   IP6N rxIP6;
#endif
   ADT_UInt8 ipver;

   if (!RTPTaskInit) return;

   // Remove RTP socket from RTP Session lookup table
   idx = (deletedEntry->idx + 1) & 0xff;
   hashIdx = deletedEntry->hashIdx;
   ChanId  = deletedEntry->ChanId;
   rxIP    = deletedEntry->lcl.dstIP;
   ipver = deletedEntry->ipver;

   #ifdef IPV6_MCAST // jdc todo
   if (ipver == 6) {
        memcpy(rxIP6.u.addr8, deletedEntry->lcl6.dstIP, 16);
   }
   #endif
   mask = IPMap_lock ();

   // Remove channel's link to session
   chnToIPSess [ChanId] = &nullIPSess;
   nxtEntry = &IPToSess[idx];
   matchedEntry = NULL;

   // Find last entry (within block of active entries) with matching hash index
   while (nxtEntry->RTPSession != 0) {
      if (hashIdx == nxtEntry->hashIdx) matchedEntry = nxtEntry;

      idx = (idx + 1) & 0xff;
      nxtEntry++;
      if (idx == 0) nxtEntry = IPToSess;
      if (idx == hashIdx) break;
   }

   if (matchedEntry != NULL) {
      // Copy matched entry to deleted entry's slot, restore index and
      // update chnToIPSess table for moved entry
      idx    = deletedEntry->idx;
      ChanId = matchedEntry->ChanId;

      *deletedEntry = *matchedEntry;
      chnToIPSess[ChanId] = deletedEntry;
      deletedEntry->idx = idx;
      DbgPrintf (DBG_INFO, "IPToSess moved %d..%d", ChanId, idx);

      logIPSess (0xc, matchedEntry);

      // Mark matched entry for deletion instead of original entry
      deletedEntry = matchedEntry;

   }

   if (ipver != 6) {
        // Remove IP/port/SSRC from entry
        // Clear out entry marked for deletion
        if (IN_MULTICAST (rxIP))
            queueMultiCastRequest (rxIP, DELETE_MULTICAST_IP);
   } 
#if 0 // def IPV6_MCAST // jdc todo
   else {
        if (IN_MULTICAST (rxIP6))
            queueMultiCastRequest (rxIP6, DELETE_MULTICAST_IP);
   }
#endif
   logIPSess (0xd, deletedEntry);

   idx = deletedEntry->idx;
   memset (deletedEntry, 0, sizeof (IPToSess_t));
   deletedEntry->vlanIndex = (ADT_UInt8) -1;
   deletedEntry->hashIdx   = (ADT_UInt8)  -1;
   deletedEntry->ChanId    = (ADT_UInt16) -1;
   deletedEntry->idx = idx;
   IPMap_unlock (mask);

   return;
}

//-----------------------------------------------------------------------
//
//  External interfaces
//
//-----------------------------------------------------------------------

// updateVlanDevice - called by IPStack thread to update the VLAN devices
#pragma CODE_SECTION (updateVlanDevice, "SLOW_PROG_SECT")
void updateVlanDevice (ADT_UInt32 *Data) {

   ADT_UInt16 vlanIdx, vlanID, vlanTag;
   ADT_UInt8 priority;
   int ifIdx;

   vlanIdx  = (ADT_UInt16) Data[1];
   vlanID   = (ADT_UInt16) Data[2];
   priority = (ADT_UInt8)  Data[3];
   vlanTag = 0;

   ifIdx = VLANTags[vlanIdx].ifIdx;
   if (ifIdx != -1)
      VLANDelDevice (ifIdx);   // Delete old reference

   if (vlanID == 0) ifIdx = -1;
   else             ifIdx = VLANAddDevice (1, vlanID, priority, NULL);

   if (ifIdx < 0) {
      ifIdx = -1;
      VLANTags[vlanIdx].vlanDeviceHndl = NULL;
   } else {
      vlanTag  = (priority << 13) | vlanID;
      VLANTags[vlanIdx].vlanDeviceHndl = (void *) NIMUFindByIndex (ifIdx);
   }
   VLANTags[vlanIdx].ifIdx = ifIdx;
   VLANTags[vlanIdx].Tag = vlanTag;
}

// processHostCoreMsg - process messages related to IP stack configuration.
//                These message require running IP stack APIs on IP stack core.
#pragma CODE_SECTION (processHostCoreMsg, "FAST_PROG_SECT")
void processHostCoreMsg (ADT_UInt32 *Data, int dataI8) {
   switch (Data[0]) {
   case DELETE_MULTICAST_IP:   IGMPDeleteRTPMCastIP (Data[1]);   break;
   case ADD_MULTICAST_IP:      IGMPAddRTPMCastIP (Data[1]);      break;
   case UPDATE_VLAN_TAG:       updateVlanDevice (Data);         break;
   default:
      AppErr ("Unknown host message", Data[0]);
   }
   return;  
}

//  Send multicast IP address change requests via host core messaging
#pragma CODE_SECTION (queueMultiCastRequest, "SLOW_PROG_SECT")
void queueMultiCastRequest (IPN ip, int msgType) {
   ADT_UInt32 Data[2];
   Data[0] = msgType;
   Data[1] = ip;
   sendHostCoreMsg (Data, sizeof(Data));
   return;
}


//{  Add entry to IPSession lookup table.
//
//  Called by messaging thread to link a remote socket/SSRC to an RTPSession.
//
//   non-negative - index to IPToSess structure
//   -1           - failure (table full)
//   -2           - failure (IPAddr:port:SSRC already exists)
//   -3           - failure RTPTask not initialized
//   -4           - failure (Channel ID not in range from 0-255)
//
//}
#pragma CODE_SECTION (addToIPSession, "SLOW_PROG_SECT")
int addToIPSession (IPToSess_t *new, void *RTPSession, ADT_UInt16 ChanId) {
   rtpAddr    *rmt, *lcl;
   IPToSess_t *entry;
   ADT_UInt8  nonZeroMac;
   int        i, mask;

   if (!RTPTaskInit)  return -3;
   if (0xff < ChanId) return -4;

   rmt = &new->rmt;
   lcl = &new->lcl;

   if (rmt->dstIP == 0) { // Rcvd only channel ? Huafeng/06/21/2016
        rmt->dstIP = rmt->IP;
   }

   // Convert addresses to network byte order.
   rmt->IP    = NDK_htonl (rmt->IP);
   rmt->dstIP = NDK_htonl (rmt->dstIP);
   rmt->Port  = NDK_htons (rmt->Port);
   rmt->SSRC  = NDK_htonl (rmt->SSRC);

   // Establish multicast MAC address from multicast IP address.
   if (IN_MULTICAST (rmt->dstIP)) {
      MulticastIPToMulticastMAC (rmt->dstIP, rmt->MAC);
   }

   lcl->Port  = NDK_htons (lcl->Port);
   lcl->dstIP = NDK_htonl (lcl->dstIP);

   //  Find open entry in IPToSess table
   mask = IPMap_lock ();

   if (lcl->dstIP != 0) {
        // lcl-> dstIP (i.e. API InDestIP) can be mcast rx listening IP address
        //  OR possibly unicast IP rx address
	   if(IN_MULTICAST(lcl->dstIP))
          entry = IPSessionLookup (lcl->dstIP, lcl->Port, 0, TRUE);
	   else  {
          if (rmt->IP == 0) { // Transmit only ? Huafeng/06/21/2016
                // API param destIP is zero, therefore use InDestIP(lcl.dstIP) for Rx
                rmt->IP = lcl->dstIP;
          } 
		  //else  {
                // API params destIP and InDestIP are both non-zero, therefore:
                // If the API parameter DestTxIP is 0: 
                //      use destIP for the Tx, InDestIP for Rx 
                // Else if DestTxIP is non-zero:
                //      use DestTxIP for Tx and destIP for Rx
                //if (rmtDstNull) {
                //     rmt->IP = lcl->dstIP;
                //}
          //}
          entry = IPSessionLookup (rmt->IP, rmt->Port, rmt->SSRC, TRUE);
        }
   } 
   else {
        entry = IPSessionLookup (rmt->IP, rmt->Port, rmt->SSRC, TRUE);
   }

   if (entry == NULL) {
      IPMap_unlock (mask);
      return -1;                                // Table full
   }
   if (entry->RTPSession != NULL) {
      IPMap_unlock (mask);
      return -2;                                // Duplicate IP:port:SSRC
   }

   logTime (0x100B1000 | ChanId);


   // Identify MAC:IP:Port:SSRC addresses of remote device
   mmCopy (&entry->rmt, rmt, sizeof (entry->rmt));


   // Create a static binding between remote IP and remote Mac address when a non-zero Mac Address is supplied
   // NOTE: When lcl.SSRC is zero, the remote MAC address is zero and the IP stack must perform ARP to
   //       resolve the MAC address.  When lcl.SSRC is non-zero, the remote MAC address in non-zero and 
   //       fastRTPSend can be used since the MAC address has been resolved.
   entry->lcl.SSRC = 0;
   if ((IPConfig.DHCPIP == rmt->IP) || (IPConfig.IPAddr == rmt->IP)) {
      // Force locally addressed packets to bypass fastRTPsend.
      memset (&entry->rmt.MAC, 0, 6);
   } else {
      for (i=0, nonZeroMac=0; i<6; i++) nonZeroMac |= rmt->MAC[i];
      
      if (nonZeroMac) {
        entry->lcl.SSRC = 1;
      } else {
   	      if(entry->rmt.dstIP != 0)
    		  findMACForIP (entry->rmt.dstIP, entry->rmt.MAC);
    	  else if(entry->rmt.IP != 0)
    		  findMACForIP (entry->rmt.IP, entry->rmt.MAC);
      }
   }

   // Save current local configuration of MAC:IP:Port addresses for use as src addresses in transmissions
   mmCopy (entry->lcl.MAC, (void *) IPConfig.TxMac, 6);
   if (IPConfig.RTPSrcIP)      entry->lcl.IP = IPConfig.RTPSrcIP;   // User config'd RTP IP address
   else if (IPConfig.IPAddr)   entry->lcl.IP = IPConfig.IPAddr;     // User config'd IP address
   else                        entry->lcl.IP = IPConfig.DHCPIP;     // DHCP IP address

   if (lcl->dstIP == 0) entry->lcl.dstIP = entry->lcl.IP;
   else                 entry->lcl.dstIP = lcl->dstIP;

   if (lcl->Port)
      entry->lcl.Port   = lcl->Port;
   else if (IPConfig.rtpPort)
      entry->lcl.Port   = IPConfig.rtpPort;
   else
      entry->lcl.Port   = NDK_htons (DEFAULT_RTP_PORT);

   entry->RTPSession = RTPSession;
   entry->ChanId     = ChanId;
   entry->vlanIndex  = new->vlanIndex;
   entry->DSCP       = new->DSCP;

   // Add local multicast IP to list of multicast IP receive filters
   if (IN_MULTICAST (entry->lcl.dstIP)) {
      queueMultiCastRequest  (entry->lcl.dstIP, ADD_MULTICAST_IP);
   }

   chnToIPSess [ChanId] = entry;
   logIPSess (0xa, entry);

   IPMap_unlock (mask);
   return entry->idx;
}


#ifdef RTCP_ENABLED
//
//{  Remove RTCP entry from IPSession lookup
//
//   Entry in RTPSession replaced with last entry in block with
//   its hashidx equal to its own hashidx
//
//
//   0 - failure (IPAddr:port already exists or table full)
//   1 - success (empty entry found)
//}
#pragma CODE_SECTION (removeIPSessionEntryRTCP, "SLOW_PROG_SECT")
static void removeIPSessionEntryRTCP (IPToSess_t * volatile deletedEntry) {
   IPToSess_t *nxtEntry, *matchedEntry;
   int idx, hashIdx, mask;
   ADT_UInt16 ChanId;
#ifdef MCAST_RTCP
   IPN rxIP;
#endif
   if (!RTPTaskInit) return;

   // Remove RTP socket from RTP Session lookup table
   idx = (deletedEntry->idx + 1) & 0xff;
   hashIdx = deletedEntry->hashIdx;
   ChanId  = deletedEntry->ChanId;
#ifdef MCAST_RTCP
   rxIP    = deletedEntry->lcl.dstIP;
#endif
   mask = IPMap_lock ();

   // Remove channel's link to session
   RTCPchnToIPSess [ChanId] = &nullIPSess;
   nxtEntry = &RTCPIPToSess[idx];
   matchedEntry = NULL;

   // Find last entry (within block of active entries) with matching hash index
   while (nxtEntry->RTPSession != 0) {
      if (hashIdx == nxtEntry->hashIdx) matchedEntry = nxtEntry;

      idx = (idx + 1) & 0xff;
      nxtEntry++;
      if (idx == 0) nxtEntry = IPToSess;
      if (idx == hashIdx) break;
   }

   if (matchedEntry != NULL) {
      // Copy matched entry to deleted entry's slot, restore index and
      // update chnToIPSess table for moved entry
      idx    = deletedEntry->idx;
      ChanId = matchedEntry->ChanId;

      *deletedEntry = *matchedEntry;
      RTCPchnToIPSess[ChanId] = deletedEntry;
      deletedEntry->idx = idx;
      DbgPrintf (DBG_INFO, "IPToSess moved %d..%d", ChanId, idx);

      logIPSess (0xc, matchedEntry);

      // Mark matched entry for deletion instead of original entry
      deletedEntry = matchedEntry;

   }

#ifdef MCAST_RTCP
   // Remove IP/port/SSRC from entry
   // Clear out entry marked for deletion
   if (IN_MULTICAST (rxIP))
      queueMultiCastRequest (rxIP, DELETE_MULTICAST_IP);

   logIPSess (0xd, deletedEntry);

   idx = deletedEntry->idx;
   memset (deletedEntry, 0, sizeof (IPToSess_t));
   deletedEntry->vlanIndex = (ADT_UInt8) -1;
   deletedEntry->hashIdx   = (ADT_UInt8)  -1;
   deletedEntry->ChanId    = (ADT_UInt16) -1;
   deletedEntry->idx = idx;
#endif

   IPMap_unlock (mask);

   return;
}



#pragma CODE_SECTION (IPSessionLookupRTCP, "PKT_PROG_SECT")
IPToSess_t *IPSessionLookupRTCP (IPN rmtIP, ADT_UInt16 rmtPort, ADT_UInt32 rmtSSRC, ADT_Bool new) {
   int hashIPAddr, hashPort;
   int storeIdx, hashIdx;
   IPToSess_t *openIPSess;
   register IPToSess_t *ipSess;

   hashIPAddr = (int) rmtIP;
   hashPort   = (int) rmtPort;
   hashIdx = (hashIPAddr ^ hashIPAddr >> 8 ^ hashIPAddr >> 16 ^ hashIPAddr >> 24 ^ hashPort ^ hashPort >> 8) & 0xff;

   storeIdx = hashIdx;
   ipSess = &RTCPIPToSess[storeIdx];
   openIPSess = NULL;

   if (!new) {
      // When searching for existing entries must find entry with:
      //    RTPSession defined && matching rmtIP && matching rmtPort
      //    if (entries rmtSSRC != 0) rmtSSRC must match
      do {
         if (ipSess->RTPSession != NULL && ipSess->rmt.IP == rmtIP && ipSess->rmt.Port == rmtPort) {
            if (ipSess->rmt.SSRC == rmtSSRC) return ipSess;   // Full match
            
            // Store first entry of unassigned matching IP/port for match if no others are found (first time for SSRC).
            if (openIPSess == NULL && ipSess->rmt.SSRC == 0) openIPSess = ipSess;
         }

         // Move to next ipSess entry, checking for wrap
         storeIdx = (storeIdx + 1) & 0xff;
         ipSess++;
         if (storeIdx == 0) ipSess = IPToSess;                   // Index wrap

      } while (storeIdx != hashIdx);

      // First packet for IP/port with unassigned SSRC.  Assign SSRC.
      if (openIPSess) openIPSess->rmt.SSRC = rmtSSRC;
      return openIPSess;
   }

   // For new entries, must find:
   //    first unused entry
   while (ipSess->RTPSession != 0) {

      // Check for wrap
      storeIdx = (storeIdx + 1) & 0xff;
      if (storeIdx == hashIdx) return NULL;

      ipSess++;
      if (storeIdx == 0) ipSess = IPToSess;   // Index wrap
   }

   // Channel setup.  Assign hash index to entry
   ipSess->idx = storeIdx;
   ipSess->hashIdx = hashIdx;
   return ipSess;
}


#ifdef _INCLUDE_IPv6_CODE
#pragma CODE_SECTION (IPSessionLookupRTCP6, "PKT_PROG_SECT")
IPToSess_t *IPSessionLookupRTCP6 (ADT_UInt8 *rmtIP, ADT_UInt16 rmtPort, ADT_UInt32 rmtSSRC, ADT_Bool new) {
   int hashPort;
   ADT_UInt8 storeIdx, hashIdx;
   IPToSess_t *openIPSess;
   register IPToSess_t *ipSess;
   int i;

   hashPort   = (int) rmtPort;
   hashIdx = (ADT_UInt8)(hashPort ^ hashPort >> 8);
   for (i=0; i<16; i++) hashIdx ^= rmtIP[i];   

   storeIdx = hashIdx;
   ipSess = &RTCPIPToSess[storeIdx];
   openIPSess = NULL;

   if (!new) {
      // When searching for existing entries must find entry with:
      //    RTPSession defined && matching rmtIP && matching rmtPort
      //    if (entries rmtSSRC != 0) rmtSSRC must match
      do {
         if (ipSess->RTPSession != NULL && (ipSess->ipver == 6) && areSameIp(ipSess->rmt6.IP, rmtIP) && ipSess->rmt6.Port == rmtPort) {
            if (ipSess->rmt6.SSRC == rmtSSRC) return ipSess;   // Full match
            
            // Store first entry of unassigned matching IP/port for match if no others are found (first time for SSRC).
            if (openIPSess == NULL && ipSess->rmt6.SSRC == 0) openIPSess = ipSess;
         }

         // Move to next ipSess entry, checking for wrap
         storeIdx = (storeIdx + 1) & 0xff;
         ipSess++;
         if (storeIdx == 0) ipSess = IPToSess;                   // Index wrap

      } while (storeIdx != hashIdx);

      // First packet for IP/port with unassigned SSRC.  Assign SSRC.
      if (openIPSess) openIPSess->rmt6.SSRC = rmtSSRC;
      return openIPSess;
   }

   // For new entries, must find:
   //    first unused entry
   while (ipSess->RTPSession != 0) {

      // Check for wrap
      storeIdx = (storeIdx + 1) & 0xff;
      if (storeIdx == hashIdx) return NULL;

      ipSess++;
      if (storeIdx == 0) ipSess = IPToSess;   // Index wrap
   }

   // Channel setup.  Assign hash index to entry
   ipSess->idx = storeIdx;
   ipSess->hashIdx = hashIdx;
   ipSess->ipver = 6;
   return ipSess;
}
#endif

#pragma CODE_SECTION (addToIPSession, "SLOW_PROG_SECT")
int addToIPSessionRTCP (IPToSess_t *new, void *RTPSession, ADT_UInt16 ChanId) {
   rtpAddr    *rmt, *lcl;
   IPToSess_t *entry;
   ADT_UInt8  nonZeroMac;
   int        i, mask;

   if (!RTPTaskInit)  return -3;
   if (0xff < ChanId) return -4;

   rmt = &new->rmt;
   lcl = &new->lcl;

   // Convert addresses to network byte order.
   rmt->IP    = NDK_htonl (rmt->IP);
   rmt->dstIP = NDK_htonl (rmt->dstIP);
   rmt->Port  = NDK_htons (rmt->Port);
   rmt->SSRC  = NDK_htonl (rmt->SSRC);

   lcl->Port  = NDK_htons (lcl->Port);
   lcl->dstIP = NDK_htonl (lcl->dstIP);

   //  Find open entry in IPToSess table
   mask = IPMap_lock ();

   entry = IPSessionLookupRTCP (rmt->IP, rmt->Port, rmt->SSRC, TRUE);

   if (entry == NULL) {
      IPMap_unlock (mask);
      return -1;                                // Table full
   }
   if (entry->RTPSession != NULL) {
      IPMap_unlock (mask);
      return -2;                                // Duplicate IP:port:SSRC
   }

   logTime (0x100B1000 | ChanId);


   // Identify MAC:IP:Port:SSRC addresses of remote device
   mmCopy (&entry->rmt, rmt, sizeof (entry->rmt));


   // Create a static binding between remote IP and remote Mac address when a non-zero Mac Address is supplied
   // NOTE: When lcl.SSRC is zero, the remote MAC address is zero and the IP stack must perform ARP to
   //       resolve the MAC address.  When lcl.SSRC is non-zero, the remote MAC address in non-zero and 
   //       fastRTPSend can be used since the MAC address has been resolved.
   entry->lcl.SSRC = 0;
   if ((IPConfig.DHCPIP == rmt->IP) || (IPConfig.IPAddr == rmt->IP)) {
      // Force locally addressed packets to bypass fastRTPsend.
      memset (&entry->rmt.MAC, 0, 6);
   } else {
      for (i=0, nonZeroMac=0; i<6; i++) nonZeroMac |= rmt->MAC[i];
      
      if (nonZeroMac) {
        entry->lcl.SSRC = 1;
      }
      else {
        if (findMACForIP_RTCP (entry->rmt.IP, entry->rmt.MAC) == 1) {
            // The MAC was found from an entry in the IPSess table
            entry->lcl.SSRC = 1;
        }
      }
   }

   // Save current local configuration of MAC:IP:Port addresses for use as src addresses in transmissions
   mmCopy (entry->lcl.MAC, (void *) IPConfig.TxMac, 6);
   if (IPConfig.RTPSrcIP)      entry->lcl.IP = IPConfig.RTPSrcIP;   // User config'd RTP IP address
   else if (IPConfig.IPAddr)   entry->lcl.IP = IPConfig.IPAddr;     // User config'd IP address
   else                        entry->lcl.IP = IPConfig.DHCPIP;     // DHCP IP address

   if (lcl->dstIP == 0) entry->lcl.dstIP = entry->lcl.IP;
   else                 entry->lcl.dstIP = lcl->dstIP;

   if (lcl->Port)
      entry->lcl.Port   = lcl->Port;

   entry->RTPSession = RTPSession;
   entry->ChanId     = ChanId;
   entry->vlanIndex  = new->vlanIndex;
   entry->DSCP       = new->DSCP;

#ifdef MCAST_RTCP
   // Add local multicast IP to list of multicast IP receive filters
   if (IN_MULTICAST (entry->lcl.dstIP)) {
      queueMultiCastRequest  (entry->lcl.dstIP, ADD_MULTICAST_IP);
   }
#endif

   RTCPchnToIPSess [ChanId] = entry;
   logIPSess (0xa, entry);

   IPMap_unlock (mask);
   return entry->idx;
}


#ifdef _INCLUDE_IPv6_CODE
#pragma CODE_SECTION (addToIPSessionRTCP6, "SLOW_PROG_SECT")
int addToIPSessionRTCP6 (IPToSess_t *new, void *RTPSession, ADT_UInt16 ChanId) {
   rtpAddr6    *rmt, *lcl;
   IPToSess_t *entry;
   ADT_UInt8  nonZeroMac, nonZeroRmtDstIP, nonZeroRmtIP, nonZeroLclDstIP;
   int        i, mask;

//   if (!RTPTaskInit)  return -3;
   if (0xff < ChanId) return -4;

   rmt = &new->rmt6;
   lcl = &new->lcl6;
   nonZeroRmtIP = isNonZeroIp(rmt->IP);
   nonZeroRmtDstIP = isNonZeroIp(rmt->dstIP);
   nonZeroLclDstIP = isNonZeroIp(lcl->dstIP);

   if (nonZeroRmtDstIP == 0)
        memcpy(rmt->dstIP, rmt->IP, 16);

   rmt->Port  = NDK_htons (rmt->Port);
   rmt->SSRC  = NDK_htonl (rmt->SSRC);

#ifdef MCAST_RTCP
#ifdef IPV6_MCAST
   // Establish multicast MAC address from multicast IP address.
   if (IN_MULTICAST6 (rmt->dstIP)) {
      MulticastIPToMulticastMAC6 (rmt->dstIP, rmt->MAC);
   }
#endif
#endif

   lcl->Port  = NDK_htons (lcl->Port);

   //  Find open entry in IPToSess table
   mask = IPMap_lock ();

   if (nonZeroLclDstIP != 0) {
        // lcl-> dstIP (i.e. API InDestIP) can be mcast rx listening IP address
        //  OR possibly unicast IP rx address
#ifdef MCAST_RTCP
#ifdef IPV6_MCAST
	   if(IN_MULTICAST6(lcl->dstIP))
          entry = IPSessionLookupRTCP6 (lcl->dstIP, 0, 0, TRUE);
	   else 
#endif 
#endif
       {
          if (nonZeroRmtIP == 0) { // Transmit only ? Huafeng/06/21/2016
                // API param destIP is zero, therefore use InDestIP(lcl.dstIP) for Rx
            for(i=0; i<16; i++) rmt->IP[i] = lcl->dstIP[i];
          } 
          entry = IPSessionLookupRTCP6 (rmt->IP, rmt->Port, rmt->SSRC, TRUE);
        }
   } 
   else 
   {
        entry = IPSessionLookupRTCP6 (rmt->IP, rmt->Port, rmt->SSRC, TRUE);
   }
   if (entry == NULL) {
      IPMap_unlock (mask);
      return -1;                                // Table full
   }
   if (entry->RTPSession != NULL) {
      IPMap_unlock (mask);
      return -2;                                // Duplicate IP:port:SSRC
   }

   //logTime (0x100B1000 | ChanId);


   // Identify MAC:IP:Port:SSRC addresses of remote device
   mmCopy (&entry->rmt6, rmt, sizeof (rtpAddr6));


   // Create a static binding between remote IP and remote Mac address when a non-zero Mac Address is supplied
   // NOTE: When lcl.SSRC is zero, the remote MAC address is zero and the IP stack must perform ARP to
   //       resolve the MAC address.  When lcl.SSRC is non-zero, the remote MAC address in non-zero and 
   //       fastRTPSend can be used since the MAC address has been resolved.
   entry->lcl6.SSRC = 0;
   if (areSameIp(IPv6Info.linkLocalAddress.u.addr8, rmt->IP) ||
       areSameIp(IPv6Info.globalAddress.u.addr8, rmt->IP))
   {
        // Force locally addressed packets to bypass fastRTPsend.
        memset (&entry->rmt.MAC, 0, 6);
   } else 
   {
      for (i=0, nonZeroMac=0; i<6; i++) nonZeroMac |= rmt->MAC[i];
      
      if (nonZeroMac) {
        entry->lcl6.SSRC = 1;
      }
      else {
        if (findMACForIP_RTCP6 (entry->rmt6.IP, entry->rmt6.MAC) == 1) {
            // The MAC was found from an entry in the IPSess table
            entry->lcl6.SSRC = 1;
        }
      }
   }

   // Save current local configuration of MAC:IP:Port addresses for use as src addresses in transmissions
   mmCopy (entry->lcl6.MAC, (void *) IPConfig.TxMac, 6);
   if (isNonZeroIp(IPv6Info.globalAddress.u.addr8))
        memcpy(&entry->lcl6.IP, &IPv6Info.globalAddress.u.addr8, 16);
   else
        memcpy(&entry->lcl6.IP, &IPv6Info.linkLocalAddress, 16);

   if (nonZeroLclDstIP == 0)
        memcpy(&entry->lcl6.dstIP, &entry->lcl6.IP, 16);
   else                 
        memcpy(&entry->lcl6.dstIP, &lcl->dstIP, 16);

   if (lcl->Port)
      entry->lcl6.Port   = lcl->Port;
   else if (IPConfig.rtpPort)
      entry->lcl6.Port   = IPConfig.rtpPort;
   else
      entry->lcl6.Port   = NDK_htons (DEFAULT_RTP_PORT);

   entry->RTPSession = RTPSession;
   entry->ChanId     = ChanId;
   entry->vlanIndex  = new->vlanIndex;
   entry->DSCP       = new->DSCP;

#ifdef MCAST_RTCP
#ifdef IPV6_MCAST
   // Add local multicast IP to list of multicast IP receive filters
   if (IN_MULTICAST6 (entry->lcl6.dstIP)) {
      queueMultiCastRequest6  (entry->lcl6.dstIP), ADD_MULTICAST_IP6);
   }
#endif
#endif

   RTCPchnToIPSess [ChanId] = entry;
   //logIPSess (0xa, entry);

   IPMap_unlock (mask);
   return entry->idx;
}
#endif
#endif

//{  Mark entry for removal from IPSession lookup table.
//
//  Called by messaging thread on channel teardown
//
//  0 - entry not found
//} 1 - entry removed
#pragma CODE_SECTION (removeFromIPSession, "SLOW_PROG_SECT")
int removeFromIPSession (ADT_UInt16 ChanId) {
   IPToSess_t *entry;

   if (!RTPTaskInit)  return 0;
   if (0xff < ChanId) return 0;

#ifdef RTCP_ENABLED
    if (rtcp_channel_is_enabled(ChanId)) {
        entry = RTCPchnToIPSess [ChanId];
        if (entry != &nullIPSess) {
            removeIPSessionEntryRTCP (entry);
        }
    }
#endif   

   entry = chnToIPSess [ChanId];
   if (entry == &nullIPSess) return 0;  // entry not found
   removeIPSessionEntry (entry);
   logTime (0x100B1300 | ChanId);

   return 1;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// RTPTask - G.PAK network RTP task.
//
// FUNCTION
//  This function is the G.PAK network RTP task.
//
// RETURNS
//  nothing
//
//  Process socekts
//  0 - RTP socket
//  1 - TCP message listening socket
//  2 - TCP event listening socket
//  3 - TCP Gpak messaging socket
#pragma CODE_SECTION (RTPTask, "PKT_PROG_SECT")
void RTPTask (RTPTaskParams_t *pParams) {
   SOCKET rtpSkt = INVALID_SOCKET;
   SOCKET msgSkt = INVALID_SOCKET;
   SOCKET evtSkt = INVALID_SOCKET;

#ifdef _INCLUDE_IPv6_CODE
   SOCKET msgSkt6 = INVALID_SOCKET;
   SOCKET evtSkt6 = INVALID_SOCKET;
#endif

#ifdef UDP_MESSAGING
int rplyRetry = 0;
#else
   SOCKET skt;
   SOCKET msgLstn = INVALID_SOCKET;
   SOCKET evtLstn = INVALID_SOCKET;
#ifdef _INCLUDE_IPv6_CODE
   SOCKET msgLstn6 = INVALID_SOCKET;
   SOCKET evtLstn6 = INVALID_SOCKET;
#endif

#endif

   SA_IN sktAddr;
   IPToSess_t *RtpMapping;
   UINT32 socketOption;
   int i, RTPBuffI8;
   int pktI8, msgI8, param, core, successive, mask;
   int cmdMsgI8, rplyMsgI8, evtMsgI8;
   ADT_UInt16 priority;

   void *cmdBuff, *rplyBuff, *evtBuff;
   UINT16 rtpPort;
   ADT_UInt16 ChanId;
   volatile int rtcp_packet;

#ifdef USE_GENERAL_TX_SOCKET
	PBM_Pkt *pPbmPkt;			// pointer to network packet buffer
	UINT8 *pByte;				// pointer to a packet byte
	ADT_UInt32 ipAddress;		// destination IP Address
	ADT_UInt16 portNum;			// destination UDP Port Number
	ADT_UInt8 *pPayload;		// pointer to UDP payload
	ADT_UInt16 length;			// length of UDP payload (bytes)
#endif

    ADT_UInt8 mcastList[0x100], numMcast = 0;
    PBM_Pkt *pPkt;
	IPHDR *pIpHdr;				// pointer to packet's IP header
    UDPHDR *pUdpHdr;            // pointer to packet's UDP header
	ADT_UInt32 dstAddr;		    // Dst IP address of receive packet
    ADT_UInt8 *ipHdr;

   rtpPort = pParams->rtpPort;
   msgPort = pParams->msgPort;
   evtPort = pParams->evtPort;

   MACUpdate= &IPSessionUpdate;

// JDC DEBUG: disable all gpak netstack mods except udp_in until stack bugs fixed
   memset(&gpak_mods, 0, sizeof(gpak_mods));
   gpak_mods.udp_in=1;
   gpak_mods.igmp=1;
   gpak_mods.lliout=1;
   gpak_mods.ipout=1;
   gpak_mods.nimu=1;
   gpak_mods.udp_out=1;


#ifdef _INCLUDE_IPv6_CODE
#if 0 // jdc debug for c66x ipv6
   while (network_up == 0) {
      TaskSleep (500);
   }
#endif
#endif

   DbgPrintf (DBG_INFO, "RTPTask starting\n");



#ifdef _INCLUDE_IPv6_CODE
   initIp6CallbackPtrs();
   memset(&msgIP6, 0, sizeof(IP6N)); 
   memset(&evtIP6, 0, sizeof(IP6N)); 
#endif

   RTPBuff = (ADT_UInt32 *) ((ADT_UInt32)RTPAlloc (RTPScratchI8) & ~3);
   if (RTPBuff == NULL)
      goto IPExit;
   RTPBuffI8 = RTPScratchI8 - 3;

   fdOpenSession ((HANDLE) TSK_self ());

   // Initialize global structures
   memset (&sktList,     0, sizeof (sktList));
   memset (&IPStats,     0, sizeof (IPStats));
   memset (&IPToSess,    0, sizeof (IPToSess));
   memset (&nullIPSess,  0, sizeof (nullIPSess));
//   memset (&RTCPIPToSess,    0, sizeof (RTCPIPToSess));
   for (i = 0; i < 0x100; i++) {
      chnToIPSess[i] = &nullIPSess;
      IPToSess[i].vlanIndex = (ADT_UInt8) -1;
#ifdef RTCP_ENABLED
      RTCPchnToIPSess[i] = &nullIPSess;
      RTCPIPToSess[i].vlanIndex = (ADT_UInt8) -1;
#endif	  
   }


#ifdef EMAC_STATS
   memset(&emac_stats, 0, sizeof(EMAC_STATISTICS_T));
#endif

   msgIP = 0;
   evtIP = 0;

   if (rtpPort == 0) {
      rtpPort = DEFAULT_RTP_PORT;
   }
   if (msgPort == 0) {
      msgPort = DEFAULT_MSG_PORT;
   }
   if (evtPort == 0) {
      evtPort = DEFAULT_EVT_PORT;
   }

   // --------------------------------------------------
   // --------------------------------------------------
   //  Open IPv4 rtp socket for G.PAK channels
   rtpSkt = socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP);
   if (rtpSkt == INVALID_SOCKET) {
      goto IPExit;
   }

   bzero (&sktAddr, sizeof(sktAddr));
   sktAddr.sin_family = AF_INET;
   sktAddr.sin_port = NDK_htons (rtpPort);

   if (bind (rtpSkt, (PSA) &sktAddr, sizeof(sktAddr)) != 0) {
      goto IPExit;
   }
   //  Create list of sockets for fdPoll
   sktList[RTP_IDX].fd = rtpSkt;
   sktList[RTP_IDX].eventsRequested = POLLIN;
   sktList[RTP_IDX].eventsDetected = 0;

   priority = 6;    // RTP packets set to second highest prioritiy;
   setsockopt (rtpSkt, SOL_SOCKET, SO_PRIORITY, &priority, sizeof(priority));

   param = 0x7fffffff;   // Do not limit unprocessed inbound RTP packets
   setsockopt (rtpSkt, SOL_SOCKET, SO_RCVBUF, &param, sizeof (param));


   // Set the application's UDP input packet handler.
   // Port range changed to global variable that can be modified by host
   if(IPConfig.portRange > 0)
      RTP_port_range = NDK_htons(IPConfig.portRange);
   RTPPorts ((HANDLE) rtpSkt, rtpPort, RTP_port_range); // 4000);

   DbgPrintf (DBG_INFO, "IPV4 RTP socket %x opened on port %d", rtpSkt, rtpPort);

#ifdef _INCLUDE_IPv6_CODE
   // --------------------------------------------------
   // --------------------------------------------------
   //  Open IPv6 rtp socket for G.PAK channels
    rtpSkt6 = socket(AF_INET6, SOCK_DGRAM, IPPROTO_UDP);
    if (rtpSkt6 == INVALID_SOCKET) {
      goto IPExit;
    }
    /* Bind to the specified Server port */
    memset( &sktAddr6, 0, sizeof(struct sockaddr_in) );
    sktAddr6.sin6_family = AF_INET6;
    memcpy((void *)&sktAddr6.sin6_addr,(void *)&IPV6_UNSPECIFIED_ADDRESS, sizeof(struct in6_addr));
    sktAddr6.sin6_port = NDK_htons(rtpPort);

    if( bind( rtpSkt6,(struct sockaddr *)&sktAddr6, sizeof(sktAddr6) ) < 0 )
    {
      goto IPExit;
    }

   sktList[RTP6_IDX].fd = rtpSkt6;
   sktList[RTP6_IDX].eventsRequested = POLLIN;
   sktList[RTP6_IDX].eventsDetected = 0;

   priority = 6;    // RTP packets set to second highest prioritiy;
   setsockopt (rtpSkt6, SOL_SOCKET, SO_PRIORITY, &priority, sizeof(priority));

   param = 0x7fffffff;   // Do not limit unprocessed inbound RTP packets
   setsockopt (rtpSkt6, SOL_SOCKET, SO_RCVBUF, &param, sizeof (param));

   DbgPrintf (DBG_INFO, "IPV6 RTP socket %x opened on port %d", rtpSkt6, rtpPort);
#endif

#ifdef FASTRTPENAB
   // --------------------------------------------------
   // --------------------------------------------------
   // create Raw ethernet socket used by fast RTP send
   memset(&fastRTPStats, 0, sizeof(fastRTPStats));
   hRtpRawEthSock = socket(AF_RAWETH, SOCK_RAWETH, IPPROTO_UDP);
   if ((hRtpRawEthSock != INVALID_SOCKET) && (EMAC_NetIF != NULL))
   {
      socketOption = EMAC_NetIF->index;
      rtpRawEthSockStatus =
         setsockopt(hRtpRawEthSock, SOL_SOCKET, SO_IFDEVICE, &socketOption,
                    sizeof(socketOption));
   }
#endif

   PBMQ_init(&rtpRxPbmQueue);
   appUdpInput = appUdpInputMulticore;

#ifdef USE_GENERAL_TX_SOCKET
	hGenUdpTxSock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (hGenUdpTxSock != INVALID_SOCKET)
	{
		bzero(&sktAddr, sizeof(sktAddr));
		sktAddr.sin_family = AF_INET;
		sktAddr.sin_port = NDK_htons(highRTPPort + 1);
		if (bind(hGenUdpTxSock, (PSA) &sktAddr, sizeof(sktAddr)) != 0)
		{
			shutdown(hGenUdpTxSock, SHUT_RDWR);
			hGenUdpTxSock = INVALID_SOCKET;
		}
	}
	PBMQ_init(&genUdpTxPbmQueue);
#endif


#ifdef UDP_MESSAGING
   // --------------------------------------------------
   // ---------------- UDP MESSAGING -------------------
   // --------------------------------------------------
   // --------------------------------------------------
   //  Open IPv4 UDP socket for G.PAK messaging
   msgSkt = socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP);
   if (msgSkt == INVALID_SOCKET) {
      goto IPExit;
   }

   bzero (&sktAddr, sizeof(sktAddr));
   sktAddr.sin_family = AF_INET;
   sktAddr.sin_port = NDK_htons (msgPort);

   if (bind (msgSkt, (PSA) &sktAddr, sizeof(sktAddr)) != 0) {
      goto IPExit;
   }
   msgSocket = msgSkt;

   sktList[MSG_IDX].fd = msgSkt;
   sktList[MSG_IDX].eventsRequested = POLLIN;
   sktList[MSG_IDX].eventsDetected = 0;

   priority = 7;    // Messaging packets set to highest prioritiy;
   setsockopt (msgSkt, SOL_SOCKET, SO_PRIORITY, &priority, sizeof(priority));
   DbgPrintf (DBG_INFO, "IPv4 Message UDP socket %x opened on port %d", msgSkt, msgPort);

   // --------------------------------------------------
   // --------------------------------------------------
   //  Open IPv4 UDP socket for G.PAK events
   evtSkt = socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP);
   if (evtSkt == INVALID_SOCKET) {
      goto IPExit;
   }

   bzero (&sktAddr, sizeof(sktAddr));
   sktAddr.sin_family = AF_INET;
   sktAddr.sin_port = NDK_htons (evtPort);

   if (bind (evtSkt, (PSA) &sktAddr, sizeof(sktAddr)) != 0) {
      goto IPExit;
   }

   evtSocket = evtSkt;

   sktList[EVT_IDX].fd = evtSkt;
   sktList[EVT_IDX].eventsRequested = POLLIN;
   sktList[EVT_IDX].eventsDetected = 0;

   priority = 0;    // Event packets set to lowest prioritiy;
   setsockopt (evtSkt, SOL_SOCKET, SO_PRIORITY, &priority, sizeof(priority));
   DbgPrintf (DBG_INFO, "IPv4 Event UDP socket %x opened on port %d", evtSkt, evtPort);

#ifdef _INCLUDE_IPv6_CODE
   // --------------------------------------------------
   // --------------------------------------------------
   //  Open IPv6 UDP socket for G.PAK messaging
   msgSkt6 = socket (AF_INET6, SOCK_DGRAM, IPPROTO_UDP);
   if (msgSkt6 == INVALID_SOCKET) {
      goto IPExit;
   }

   sktAddr6.sin6_port = NDK_htons (msgPort);
   if (bind (msgSkt6, (PSA) &sktAddr6, sizeof(sktAddr6)) != 0) {
      goto IPExit;
   }
   sktList[MSG6_IDX].fd = msgSkt6;
   sktList[MSG6_IDX].eventsRequested = POLLIN;
   sktList[MSG6_IDX].eventsDetected = 0;

   priority = 7;    // Messaging packets set to highest prioritiy;
   setsockopt (msgSkt6, SOL_SOCKET, SO_PRIORITY, &priority, sizeof(priority));
   DbgPrintf (DBG_INFO, "IPv6 Message UDP socket %x opened on port %d", msgSkt6, msgPort);

   // --------------------------------------------------
   // --------------------------------------------------
   //  Open IPv6 UDP socket for G.PAK events
   evtSkt6 = socket (AF_INET6, SOCK_DGRAM, IPPROTO_UDP);
   if (evtSkt6 == INVALID_SOCKET) {
      goto IPExit;
   }

   sktAddr6.sin6_port = NDK_htons (evtPort);
   if (bind (evtSkt6, (PSA) &sktAddr6, sizeof(sktAddr6)) != 0) {
      goto IPExit;
   }
   DbgPrintf (DBG_INFO, "IPv6 Event UDP socket %x opened on port %d", evtSkt6, evtPort);

   priority = 0;    // Event packets set to lowest prioritiy;
   setsockopt (evtSkt6, SOL_SOCKET, SO_PRIORITY, &priority, sizeof(priority));
#endif

#else 
   // --------------------------------------------------
   // ---------------- TCP MESSAGING -------------------
   // --------------------------------------------------
   // --------------------------------------------------
   //  Open IPv4 listening socket for G.PAK messaging
   msgLstn = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
   if (msgLstn == INVALID_SOCKET) {
      goto IPExit;
   }

   sktAddr.sin_port = NDK_htons (msgPort);
   if (bind (msgLstn, (PSA) &sktAddr, sizeof(sktAddr)) != 0) {
      goto IPExit;
   }

   if (listen (msgLstn, 1) != 0) {
      goto IPExit;
   }

   sktList[MSG_LSTN_IDX].fd = msgLstn;
   sktList[MSG_LSTN_IDX].eventsRequested = POLLIN;
   sktList[MSG_LSTN_IDX].eventsDetected = 0;

   sktList[MSG_IDX].fd = msgSkt;
   sktList[MSG_IDX].eventsRequested = 0;
   sktList[MSG_IDX].eventsDetected = 0;

   priority = 7;    // Messaging packets set to highest prioritiy;
   setsockopt (msgLstn, SOL_SOCKET, SO_PRIORITY, &priority, sizeof(priority));

   DbgPrintf (DBG_INFO, "Message listening socket %x opened on port %d", msgLstn, msgPort);

   // --------------------------------------------------
   // --------------------------------------------------
   //  Open IPv4 listening socket for G.PAK events
   evtLstn = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
   if (evtLstn == INVALID_SOCKET) {
      goto IPExit;
   }

   sktAddr.sin_port = NDK_htons (evtPort);
   if (bind (evtLstn, (PSA) &sktAddr, sizeof(sktAddr)) != 0) {
      goto IPExit;
   }

   if (listen (evtLstn, 1) != 0) {
      goto IPExit;
   }

   sktList[EVT_LSTN_IDX].fd = evtLstn;
   sktList[EVT_LSTN_IDX].eventsRequested = POLLIN;
   sktList[EVT_LSTN_IDX].eventsDetected = 0;

   priority = 0;    // Event packets set to lowest prioritiy;
   setsockopt (evtLstn, SOL_SOCKET, SO_PRIORITY, &priority, sizeof(priority));

   DbgPrintf (DBG_INFO, "Event listening socket %x opened on port %d", evtLstn, evtPort);


#ifdef _INCLUDE_IPv6_CODE
   // --------------------------------------------------
   // --------------------------------------------------
   //  Open IPv6 listening socket for G.PAK messaging
   msgLstn6 = socket (AF_INET6, SOCK_STREAM, IPPROTO_TCP);
   if (msgLstn6 == INVALID_SOCKET) {
      goto IPExit;
   }

   sktAddr6.sin6_port = NDK_htons (msgPort);
   if (bind (msgLstn6, (PSA) &sktAddr6, sizeof(sktAddr6)) != 0) {
      goto IPExit;
   }

   if (listen (msgLstn6, 1) != 0) {
      goto IPExit;
   }

   sktList[MSG6_LSTN_IDX].fd = msgLstn6;
   sktList[MSG6_LSTN_IDX].eventsRequested = POLLIN;
   sktList[MSG6_LSTN_IDX].eventsDetected = 0;

   sktList[MSG6_IDX].fd = msgSkt6;
   sktList[MSG6_IDX].eventsRequested = 0;
   sktList[MSG6_IDX].eventsDetected = 0;

   priority = 7;    // Messaging packets set to highest prioritiy;
   setsockopt (msgLstn6, SOL_SOCKET, SO_PRIORITY, &priority, sizeof(priority));

   DbgPrintf (DBG_INFO, "IPv6 Message listening socket %x opened on port %d", msgLstn6, msgPort);

   // --------------------------------------------------
   // --------------------------------------------------
   //  Open IPv6 listening socket for G.PAK events
   evtLstn6 = socket (AF_INET6, SOCK_STREAM, IPPROTO_TCP);
   if (evtLstn6 == INVALID_SOCKET) {
      goto IPExit;
   }

   sktAddr6.sin6_port = NDK_htons (evtPort);
   if (bind (evtLstn6, (PSA) &sktAddr6, sizeof(sktAddr6)) != 0) {
      goto IPExit;
   }

   if (listen (evtLstn6, 1) != 0) {
      goto IPExit;
   }

   sktList[EVT6_LSTN_IDX].fd = evtLstn6;
   sktList[EVT6_LSTN_IDX].eventsRequested = POLLIN;
   sktList[EVT6_LSTN_IDX].eventsDetected = 0;

   priority = 0;    // Event packets set to lowest prioritiy;
   setsockopt (evtLstn6, SOL_SOCKET, SO_PRIORITY, &priority, sizeof(priority));
   DbgPrintf (DBG_INFO, "IPv6 Event listening socket %x opened on port %d", evtLstn6, evtPort);
#endif
#endif

   sktCnt = SOCK_COUNT;
   RTPTaskInit = TRUE;
   cmdMsgI8 = getMsgBuffers (&cmdBuff, &rplyBuff, &evtBuff);

   //startTimer (loopTimer);

   //  Process all messaging, events and RTP packets
   for (;;) {
      logTime (0x80010010);   // End of RTP thread

      STAT_INC (IPStats.totalLoops);

      fdPoll (&sktList[0], sktCnt, 1);  // Poll for RTP activity. Wake up every 5 milliseconds
      //startTimer (loopTimer);

      logTime (0x80000010);   // Start of RTP thread

      //  Message replies -> Send to messaging clients
#ifdef UDP_MESSAGING
      if ((msgSkt != INVALID_SOCKET) && (msgIP != 0)) {
         rplyRetry = UDP_TXRPLY_RETRY;
         rplyMsgI8 = msgReplyI8 ();
         if (rplyMsgI8 != 0) {
            logTime (0x100B0500);
            while (rplyRetry) {
			    bzero(&sktAddr, sizeof(sktAddr));
			    sktAddr.sin_family = AF_INET;
			    sktAddr.sin_addr.s_addr = msgIP;
			    sktAddr.sin_port = NDK_htons(msgSrcPort);
                if (NDK_sendto(msgSkt, rplyBuff, rplyMsgI8, 0, (struct sockaddr *)&sktAddr, sizeof(sktAddr)) < 0) {
                   IPStats.tcp.Tx.ErrCnt++;
                   DbgPrintf (DBG_WARN, "Msg Reply send failure. %d, retry count %d", fdError (),  rplyRetry);
                   rplyRetry--; 
                } else {
                    IPStats.tcp.Tx.Cnt++;
                    break;
                }
            }
            logTime (0x100B0600);
         }
      }
#else
      if (msgSkt != INVALID_SOCKET) {
         rplyMsgI8 = msgReplyI8 ();
         if (rplyMsgI8 != 0) {
            logTime (0x100B0500);
            if (send (msgSkt, rplyBuff, rplyMsgI8, 0) < 0) {
               IPStats.tcp.Tx.ErrCnt++;
               DbgPrintf (DBG_WARN, "Msg Reply send failure. %d", fdError ());
            } else {
               IPStats.tcp.Tx.Cnt++;
            }
            logTime (0x100B0600);
         }
      }
#endif

#ifdef _INCLUDE_IPv6_CODE
      //  Message replies -> Send to messaging clients
      if (msgSkt6 != INVALID_SOCKET) {
         rplyMsgI8 = msgReplyI8 ();
         if (rplyMsgI8 != 0) {
            logTime (0x100B0500);
            if (send (msgSkt6, rplyBuff, rplyMsgI8, 0) < 0) {
               IPStats.tcp.Tx.ErrCnt++;
               DbgPrintf (DBG_WARN, "Msg Reply send failure. %d", fdError ());
            } else {
               IPStats.tcp.Tx.Cnt++;
            }
            logTime (0x100B0600);
         }
      }

#ifdef IPV6_TEST_UDP
    if (txudp6_lenI8) {
        // debug only:  ipv6 UDP transmit
        if (txudp6_ip.u.addr8[0] == 0) {
            IPv6StringToIPAddress (remote_ip6, &txudp6_ip);
        }
        sendUdpPktToIPv6((ADT_UInt8 *)&txudp6_ip, txudp6_port, txudp6_buff, txudp6_lenI8);
        txudp6_lenI8 = 0;
    }
#endif
#endif

#ifdef USE_GENERAL_TX_SOCKET
	if (hGenUdpTxSock != INVALID_SOCKET)
	{
        if (gen_udp.send && gen_udp.enabled) {
            memset(txGenUdpBuf, gen_udp.data++, gen_udp.pktlenI8);
            for (i=0; i<gen_udp.numPorts; i++) {
                sendUdpPktTo(gen_udp.dest_ip, gen_udp.dest_port+(i<<1), txGenUdpBuf, gen_udp.pktlenI8);
            }
            gen_udp.send = 0;
        }
		while (1)
		{
			pPbmPkt = PBMQ_deq(&genUdpTxPbmQueue);
			if (pPbmPkt == NULL)
			{
				break;
			}
			pByte = pPbmPkt->pDataBuffer + pPbmPkt->DataOffset;
			ipAddress = (((ADT_UInt32) pByte[0]) << 24) |
						(((ADT_UInt32) pByte[1]) << 16) |
						(((ADT_UInt32) pByte[2]) << 8) |
						((ADT_UInt32) pByte[3]);
			portNum = (((ADT_UInt16) pByte[4]) << 8) | ((ADT_UInt16) pByte[5]);
			pPayload = pByte + 6;
			length = (ADT_UInt16) pPbmPkt->ValidLen - 6;
			bzero(&sktAddr, sizeof(sktAddr));
			sktAddr.sin_family = AF_INET;
			sktAddr.sin_addr.s_addr = NDK_htonl(ipAddress);
			sktAddr.sin_port = NDK_htons(portNum);
			sendto(hGenUdpTxSock, pPayload, length, 0, (PSA) &sktAddr,
				   sizeof(sktAddr));
			PBM_free(pPbmPkt);
		}
	}
#endif
      // Outbound RTP packets -> Send to network
      logTime (0x100B0300);
      successive = 0;

      startTimer (timer1);
      for (core = 0; core < DSPTotalCores; core++) {
         IPStats.rtp.Rx.FrameReleaseCnt += freePktHandles (core);
         do {

            startTimer (timer2);
            pktI8 = rtpGetPacket (RTPBuff, RTPBuffI8, &ChanId, core);

            endTimerUs (timer2, &IPStats.maxRTPGetUs, 1);
            if (pktI8 <= 0) break;            // End of data
            if (ChanId == 0xffff) continue;   // Inactive channel
            if (ChanId == 0xfffe) {           // Host core messaging channel
               processHostCoreMsg (RTPBuff, pktI8);
               continue;
            }

            successive++;
            mask = IPMap_lock ();
            rtcp_packet = 0;
#ifdef RTCP_ENABLED
            rtcp_packet = rtcp_is_valid_header((ADT_UInt8 *)RTPBuff);
            if (rtcp_packet)
                RtpMapping = RTCPchnToIPSess[ChanId];
            else
#endif
            {
                RtpMapping = chnToIPSess [ChanId];
            }
            IPMap_unlock (mask);
            if (RtpMapping->RTPSession != NULL) {
                 startTimer (timer2);

#ifdef _INCLUDE_IPv6_CODE
                if (RtpMapping->ipver == 6) {
         		    if ((fast_rtp_from_task) && (RtpMapping->lcl.SSRC == 1)) {
                        fastRTPsend(RTPBuff, pktI8, RtpMapping);
            	    } else {
                        msgI8 = sendRTP6 (rtpSkt6,(void *)RTPBuff, pktI8, 0, RtpMapping);
                    }
                } 
                else
#endif
                {
         		    if ((fast_rtp_from_task) && (RtpMapping->lcl.SSRC == 1)) {
                        fastRTPsend(RTPBuff, pktI8, RtpMapping);
            	    } else {
                        msgI8 = sendRTP (rtpSkt, RTPBuff, pktI8, 0, RtpMapping);
                    }
                }
                endTimerUs (timer2, &IPStats.maxTxUs, 1);
                if (msgI8 < 0) {
                    IPStats.rtp.Tx.ErrCnt++;
                    if (IPStats.rtp.Tx.LastErr != fdError()) {
                        DbgPrintf (DBG_WARN, "Send error %d on channel %d", fdError (), ChanId);
                        IPStats.rtp.Tx.LastErr = fdError();
                    }
                    //  If stack returns indication that targetted address is unavailble
                    //  force transmission from framing tasks to reduce overhead
                    if (fdError() == NDK_EHOSTUNREACH) RtpMapping->lcl.SSRC = 2;
                } else {
                    if (rtcp_packet == 0)
                        IPStats.rtp.Tx.Cnt++;
                    else
                        IPStats.rtp.Tx.Rtcp++;
                }
             } else {
                if (IPStats.rtp.Tx.LastErr != NDK_EADDRNOTAVAIL) {
                    DbgPrintf (DBG_WARN, "Unassigned RTP channel");
                    IPStats.rtp.Tx.LastErr = NDK_EADDRNOTAVAIL;
                }
            }
         } while (TRUE);
      }
      if (IPStats.rtp.Tx.SuccessiveCnt < successive)
          IPStats.rtp.Tx.SuccessiveCnt = successive;
      endTimerUs (timer1, &IPStats.maxFrameTxUs, 1);
      endTimerUs (timer1, &IPStats.avgPktTxUs, successive);
      logTime (0x100B0400 | successive);


      //  Event notifications -> Send to event clients
#ifdef UDP_MESSAGING
      if ((evtSkt != INVALID_SOCKET) && (evtIP != 0)) {
         evtMsgI8 = evtReplyI8 ();
         if (evtMsgI8 != 0) {
            logTime (0x100B0700);
			bzero(&sktAddr, sizeof(sktAddr));
			sktAddr.sin_family = AF_INET;
			sktAddr.sin_addr.s_addr = evtIP;
			sktAddr.sin_port = NDK_htons(evtSrcPort);
            if (NDK_sendto(evtSkt, evtBuff, evtMsgI8, 0, (struct sockaddr *)&sktAddr, sizeof(sktAddr)) < 0) {
               IPStats.tcp.Tx.ErrCnt++;
               DbgPrintf (DBG_WARN, "Evt send failure. %d", fdError ());
            } else {
               IPStats.tcp.Tx.Cnt++;
            }
#else
      if (evtSkt != INVALID_SOCKET) {
         evtMsgI8 = evtReplyI8 ();
         if (evtMsgI8 != 0) {
            logTime (0x100B0700);
            pktI8 = send (evtSkt, evtBuff, evtMsgI8, 0);
#endif
            if (pktI8 < 0) {
               shutdown (evtSkt, SHUT_RDWR);
               evtSkt = INVALID_SOCKET;
               DbgPrintf (DBG_WARN, "IPv4 Event send failure. %d", fdError ());
               IPStats.tcp.Tx.ErrCnt++;
            } else {
               IPStats.tcp.Tx.Cnt++;
            }
            logTime (0x100B0800);
         }
      }

#ifdef _INCLUDE_IPv6_CODE
      //  Event notifications -> Send to event clients
      if (evtSkt6 != INVALID_SOCKET) {
         evtMsgI8 = evtReplyI8 ();
         if (evtMsgI8 != 0) {
            logTime (0x100B0700);
            pktI8 = send (evtSkt6, evtBuff, evtMsgI8, 0);
            if (pktI8 < 0) {
               shutdown (evtSkt6, SHUT_RDWR);
               evtSkt6 = INVALID_SOCKET;
               DbgPrintf (DBG_WARN, "IPv6 Event send failure. %d", fdError ());
               IPStats.tcp.Tx.ErrCnt++;
            } else {
               IPStats.tcp.Tx.Cnt++;
            }
            logTime (0x100B0800);
         }
      }
#endif

     // Inbound RTP packets -> Send to core's RTP buffer
     if (sktList[RTP_IDX].eventsDetected != 0) {
         logTime (0x100B0100);
         successive = 0;

         startTimer (timer1);
         do {
            hwi_disable (maska); 
            startTimer (timer2);
            param = sizeof(sktAddr);
            pktI8 = recvncfrom (rtpSkt, (void **) &InBuff, MSG_DONTWAIT, (PSA) &sktAddr, &param, &InBuffHndl);
            endTimerUs (timer2, &IPStats.maxRxUs, 1); 
            hwi_enable (maska);
            if (pktI8 == 0) break;
            if (pktI8 < 0) {
               if (fdError() == NDK_EWOULDBLOCK) break;
               if (IPStats.rtp.Rx.LastErr != fdError()) {
                  DbgPrintf (DBG_WARN, "Receive error %d", fdError ());
                  IPStats.rtp.Rx.LastErr = fdError();
               }
               IPStats.rtp.Rx.ErrCnt++;
               break;
            }
            IPStats.rtp.Rx.TotalCnt++;

            if ((pktI8 == 23) && (NDK_htons (sktAddr.sin_port) == (rtpPort + 1))) {
               // Respond to requests to IP address from host
               DbgPrintf (DBG_INFO, "Host IP request");
               if (strncmp (HostName, (const char *) InBuff, 22) == 0) {
                  recvncfree (InBuffHndl);
                  IPStats.rtp.Rx.HostCnt++;
                  IPStats.rtp.Rx.ReleaseCnt++;
                  msgI8 = sendto (rtpSkt, HostName, 28, 0, (PSA) &sktAddr, param);
                  if (msgI8 <= 0) 
                      DbgPrintf (DBG_WARN, "Host IP request failure %d", fdError ());
                  continue;
               }
            }

            successive++;
            hwi_disable (maska); 
            startTimer (timer2);
            RtpMapping = &nullIPSess;

            // Find RTP instance that matches the packet's remote address
            //    Order of precedence:
            //      1) customNetIn mapping (enabled in <proj>Init.c)
            //      2) sin_zero channel mapping.
            //         2.a) fireHose mapping (disabled by default, enabled by custom messaging)
            //         2.b) RTP port mapping (enabled by default, disabled by custom messaging)
            //         2.c) IP checksum mapping (disabled by default, enabled by custom messaging)
            //      3) table lookup (always enabled).
            if (customNetIn != NULL) {
               RtpMapping = (*customNetIn) (sktAddr.sin_addr.s_addr, sktAddr.sin_port, InBuff);
		       if (RtpMapping == NULL)
		            RtpMapping = &nullIPSess;
            }

            // run rtcp callback to determine if packet is RTCP
            rtcp_packet = 0;
#ifdef RTCP_ENABLED
	        if ((rtcpNetIn != NULL) && (RtpMapping->RTPSession == NULL))
	        {
		        RtpMapping = (*rtcpNetIn) (sktAddr.sin_addr.s_addr, sktAddr.sin_port, InBuff);
		        if (RtpMapping == NULL)
		            RtpMapping = &nullIPSess;
		        else
		            rtcp_packet = 1;
	        }
#endif

            if (RtpMapping->RTPSession == NULL) {
               ADT_UInt32 rmtSSRC;
               rmtSSRC = _mem4(&InBuff[8]);

               mask = IPMap_lock ();
               RtpMapping = chnToIPSess [(ADT_UInt8) sktAddr.sin_zero[0]];
               IPMap_unlock (mask);
               if ((RtpMapping->RTPSession != NULL) &&
                   (RtpMapping->ChanId   == sktAddr.sin_zero[0]) &&
                   (RtpMapping->rmt.IP   == sktAddr.sin_addr.s_addr) &&
                   (RtpMapping->rmt.Port == sktAddr.sin_port)) {
                      
                  // Found entry with matching: channel, IP, and port.
                  // --> Update SSRC if SSRC not initialized.  Ignore if SSRC initialized and SSRC does not match.
                  if (RtpMapping->rmt.SSRC == 0)            
                        RtpMapping->rmt.SSRC = rmtSSRC;
                  else if (RtpMapping->rmt.SSRC != rmtSSRC) 
                        RtpMapping = &nullIPSess;
               } else
                  RtpMapping = &nullIPSess;
            }

            if (RtpMapping->RTPSession == NULL) {
               pPkt = (PBM_Pkt *)InBuffHndl;
               ipHdr = pPkt->pDataBuffer + pPkt->DataOffset - UDPHDR_SIZE - pPkt->IpHdrLen;
               pIpHdr = (IPHDR *)ipHdr;
               pUdpHdr = (UDPHDR *)(ipHdr + pPkt->IpHdrLen);
	           dstAddr  = _mem4(&(pIpHdr->IPDst));
               numMcast = 0;
               mask = IPMap_lock ();
               if (IN_MULTICAST (dstAddr)) {
			        RtpMapping = IPSessionLookupRxMcast(dstAddr, pUdpHdr->DstPort, mcastList, &numMcast);
               } else {
               RtpMapping = IPSessionLookup (sktAddr.sin_addr.s_addr, sktAddr.sin_port, _mem4(&InBuff[8]), FALSE);
               }
               IPMap_unlock (mask);
               if (RtpMapping == NULL) 
                    RtpMapping = &nullIPSess;
            }
            endMinTimerUs (timer2, &IPStats.minLookupUs, 1);
            endTimerUs    (timer2, &IPStats.maxLookupUs, 1);
            hwi_enable (maska);

            hwi_disable (maska); 
            startTimer (timer2);
            if (RtpMapping->RTPSession != NULL) {
               if (storeInboundRTPPacket (RtpMapping->ChanId, InBuff, pktI8, (void *) InBuffHndl)) {
                  if (rtcp_packet == 0)
                        IPStats.rtp.Rx.ActiveCnt++;
                  else
                        IPStats.rtp.Rx.Rtcp++;
                
               } else {
                  recvncfree (InBuffHndl);
                  IPStats.rtp.Rx.OverflowCnt++;
                  IPStats.rtp.Rx.ReleaseCnt++;
               }
            } else {
               recvncfree (InBuffHndl);
               IPStats.rtp.Rx.InactiveCnt++;
               IPStats.rtp.Rx.ReleaseCnt++;
            }

            endTimerUs (timer2, &IPStats.maxRTPStoreUs, 1);  
            hwi_enable (maska);
            if (IPStats.rtp.Rx.SuccessiveCnt < successive)
               IPStats.rtp.Rx.SuccessiveCnt = successive;
         } while (ADT_TRUE);

         endTimerUs (timer1, &IPStats.maxFrameRxUs, 1);
         endTimerUs (timer1, &IPStats.avgPktRxUs, successive);
         logTime (0x100B0200 | successive);
     } // end RTP IPV4 socket event detected

#ifdef _INCLUDE_IPv6_CODE
      // Inbound RTP IPv6 packets
      if (sktList[RTP6_IDX].eventsDetected != 0) {
         successive = 0;
         do {
            param = sizeof(sktAddr6);
            pktI8 = recvncfrom (rtpSkt6, (void **) &InBuff, MSG_DONTWAIT, (PSA) &sktAddr6, &param, &InBuffHndl);
            //pktI8 = recvfrom (rtpSkt6, (void *)InBuff6, sizeof(InBuff6), MSG_DONTWAIT, (PSA) &sktAddr6, &param);
            if (pktI8 == 0) break;
            if (pktI8 < 0) {
               if (fdError() == NDK_EWOULDBLOCK) break;
               if (IPStats.rtp.Rx.LastErr != fdError()) {
                  DbgPrintf (DBG_WARN, "Receive error %d", fdError ());
                  IPStats.rtp.Rx.LastErr = fdError();
               }
               IPStats.rtp.Rx.ErrCnt++;
               break;
            }
            IPStats.rtp.Rx.TotalCnt6++;

            if ((pktI8 == 23) && (NDK_htons (sktAddr.sin_port) == (rtpPort + 1))) {
               // Respond to requests to IP address from host
               DbgPrintf (DBG_INFO, "Host IP request");
               if (strncmp (HostName, (const char *) InBuff, 22) == 0) {
                  recvncfree (InBuffHndl);
                  IPStats.rtp.Rx.HostCnt++;
                  IPStats.rtp.Rx.ReleaseCnt++;
                  msgI8 = sendto (rtpSkt, HostName, 28, 0, (PSA) &sktAddr, param);
                  if (msgI8 <= 0) 
                      DbgPrintf (DBG_WARN, "Host IP request failure %d", fdError ());
                  continue;
               }
            }
#ifdef IPV6_TEST_UDP
            if (udp6_echo) {
                int status;
                param = sizeof(sktAddr6);
                status = sendto (rtpSkt6,(void *)&InBuff6[0], pktI8, 0, (PSA)&sktAddr6, param);
                if (status < 0)
                {
                    udp6_txerr++;        
                } else {
                    udp6_tx++;        
                }
            }
#endif
            // JDC... TODO: add code to distribute IPv6 RTP rx packets to the cores
            // release packet for now!
            recvncfree(InBuffHndl);
         } while (ADT_TRUE);
     }
#endif
      // Inbound message requests -> Skip while waiting for a reply
      if (msgBuffInUse ()) {
         STAT_INC (IPStats.msgWaits);
         continue;
      }

#ifdef UDP_MESSAGING
     // check for inbound UDP message packet
     if (sktList[MSG_IDX].eventsDetected != 0) {
         do {
            hwi_disable (maska); 
            param = sizeof(sktAddr);
            msgI8 = recvncfrom (msgSkt, (void **) &InBuff, MSG_DONTWAIT, (PSA) &sktAddr, &param, &InBuffHndl);
            hwi_enable (maska);
            if (msgI8 == 0) break;
            if (msgI8 < 0) {
               if (fdError() == NDK_EWOULDBLOCK) break;
               if (IPStats.tcp.Rx.LastErr != fdError()) {
                  DbgPrintf (DBG_WARN, "Msg Receive error %d", fdError ());
                  IPStats.tcp.Rx.LastErr = fdError();
                  IPStats.tcp.Rx.ErrCnt++;
               }
               break;
            }
            memcpy(cmdBuff, InBuff, msgI8);
            recvncfree (InBuffHndl);
            if (msgIP == 0) {
                msgIP = sktAddr.sin_addr.s_addr;
                DbgPrintf (DBG_INFO, "IPv4 UDP message pseudo-connect ");
            }

            if (msgIP != 0) {
                if (msgIP == sktAddr.sin_addr.s_addr) {
                    if (verifyDisconnectTag (cmdBuff, msgI8)) {
                        msgIP = 0;
                        msgSrcPort=0;
                        DbgPrintf (DBG_INFO, "IPv4 UDP message pseudo-disconnect ");
                    } else {
                        if (acceptMsg (msgI8)) {
                            IPStats.tcp.Rx.Cnt++;
                        }
                    }
                } else {
                    msgIP = sktAddr.sin_addr.s_addr;
                    DbgPrintf (DBG_INFO, "New Message UDP IPv4 address");
                }
            }
         } while (ADT_TRUE);
     }

     // check for inbound UDP event pseudo-connect packet (sent once to establish connection)
     if (sktList[EVT_IDX].eventsDetected != 0) {
         do {
            hwi_disable (maska); 
            param = sizeof(sktAddr);
            msgI8 = recvncfrom (evtSkt, (void **) &InBuff, MSG_DONTWAIT, (PSA) &sktAddr, &param, &InBuffHndl);
            hwi_enable (maska);
            if (msgI8 == 0) break;
            if (msgI8 < 0) {
               if (fdError() == NDK_EWOULDBLOCK) break;
               if (IPStats.tcp.Rx.LastErr != fdError()) {
                  DbgPrintf (DBG_WARN, "Evt Receive error %d", fdError ());
                  IPStats.tcp.Rx.LastErr = fdError();
                  IPStats.tcp.Rx.ErrCnt++;
               }
               break;
            }
            if (evtIP == 0) {
                evtIP = sktAddr.sin_addr.s_addr;
                DbgPrintf (DBG_INFO, "IPv4 UDP event pseudo-connect ");
            } else {
                if (verifyDisconnectTag (InBuff, msgI8)) {
                    evtIP = 0;
                    evtSrcPort=0;
                    DbgPrintf (DBG_INFO, "IPv4 UDP event pseudo-disconnect ");
                } else {
                    if (evtIP != sktAddr.sin_addr.s_addr) {
                        evtIP = sktAddr.sin_addr.s_addr;
                        DbgPrintf (DBG_INFO, "New IPv4 UDP event IP address");
                    }
                }
            }
            recvncfree (InBuffHndl);
         } while (ADT_TRUE);
     }
#else
      // Inbound message requests -> Place in messaging task buffer
      if ((sktList[MSG_IDX].eventsDetected & POLLIN) != 0) {
         logTime (0x100B0900);
         startTimer (timer1);
         msgI8 = recv (msgSkt, cmdBuff, cmdMsgI8, MSG_DONTWAIT);
         if (msgI8 <= 0) {
            if ((fdError () != NDK_ECONNRESET) && (fdError() != NDK_EWOULDBLOCK))
               DbgPrintf (DBG_WARN, "Message socket rcv error %d. Closing", fdError ());
            if (msgSkt != INVALID_SOCKET) {
               shutdown (msgSkt, SHUT_RDWR);
            }
            closeMsgConnection ();
            sktList[MSG_IDX].fd = msgSkt = INVALID_SOCKET;
            sktList[MSG_IDX].eventsRequested = 0;
            sktList[MSG_IDX].eventsDetected = 0;
            msgIP = 0;
            IPStats.tcp.Rx.ErrCnt++;
         } else {
            if (acceptMsg (msgI8)) {
               IPStats.tcp.Rx.Cnt++;
               continue;
            }

            // Host handshake failure.
            DbgPrintf (DBG_WARN, "Message request rejected.");
            closeMsgConnection ();
            shutdown (msgSkt, SHUT_RDWR);
            sktList[MSG_IDX].fd = msgSkt = INVALID_SOCKET;
            sktList[MSG_IDX].eventsRequested = 0;
            sktList[MSG_IDX].eventsDetected = 0;
            msgIP = 0;
            IPStats.tcp.Rx.ErrCnt++;
         }
         logTime (0x100B0A00);
         endTimer (timer1, &IPStats.maxMsgMs, 1);

      } else if ((sktList[MSG_IDX].eventsDetected & POLLNVAL) != 0) {
         DbgPrintf (DBG_INFO, "Messaging socket closed. Invalid");
         closeMsgConnection ();
         shutdown (msgSkt, SHUT_RDWR);
         sktList[MSG_IDX].fd = msgSkt = INVALID_SOCKET;
         sktList[MSG_IDX].eventsRequested = 0;
         sktList[MSG_IDX].eventsDetected = 0;
         msgIP = 0;
      } else if (sktList[MSG_IDX].eventsDetected != 0) {
         DbgPrintf (DBG_WARN, "Unknown G.PAK message event %x", sktList[MSG_IDX].eventsDetected);
      }

      // Open new messaging session requests
      if ((sktList[MSG_LSTN_IDX].eventsDetected != 0)) {
         logTime (0x100B0B00);
         DbgPrintf (DBG_INFO, "Messaging socket open request");
         param = sizeof(sktAddr);
         skt = accept (msgLstn, (PSA) &sktAddr, &param);
         if (msgSkt == INVALID_SOCKET) {
            // Accept host messaging connection
            if (skt == INVALID_SOCKET) {
               DbgPrintf (DBG_ERROR, "Messaging open request failure %d", fdError ());
               sktList[MSG_IDX].eventsRequested = 0;
               msgIP = 0;
               IPStats.tcp.ConnectErrCnt++;
            } else {
               priority = 7;    // Messaging packets set to highest prioritiy;
               setsockopt (skt, SOL_SOCKET, SO_PRIORITY, &priority, sizeof(priority));

               DbgPrintf (DBG_INFO, "Message socket connected");
               sktList[MSG_IDX].eventsRequested = POLLIN;
               sktList[MSG_IDX].fd = msgSkt = skt;
               msgIP = sktAddr.sin_addr.s_addr;
               IPStats.tcp.ConnectCnt++;
            }
         } else {
            if (msgIP == sktAddr.sin_addr.s_addr) {
               DbgPrintf (DBG_INFO, "Messaging socket refreshed");
               closeMsgConnection ();
               shutdown (msgSkt, SHUT_RDWR);
               sktList[MSG_IDX].fd = msgSkt = skt;
               IPStats.tcp.ConnectCnt++;
            } else {
               // Disallow multiple host connections
               shutdown (skt, SHUT_RDWR);
               DbgPrintf (DBG_WARN, "Message socket already open. New socket rejected");
               IPStats.tcp.ConnectErrCnt++;
            }
         }
         logTime (0x100B0C00);
      }

      // Open new event session requests
      if ((sktList[EVT_LSTN_IDX].eventsDetected != 0)) {
         logTime (0x100B0D00);
         DbgPrintf (DBG_INFO, "Event socket open request");
         param = sizeof(sktAddr);
         skt = accept (evtLstn, (PSA) &sktAddr, &param);
         if (evtSkt == INVALID_SOCKET) {
            // Accept host event connection
            if (skt == INVALID_SOCKET) {
               DbgPrintf (DBG_ERROR, "Event open request failure %d", fdError ());
               IPStats.tcp.ConnectErrCnt++;
            } else {
               evtSkt = skt;
               DbgPrintf (DBG_INFO, "Event socket connected");
               evtIP = sktAddr.sin_addr.s_addr;
               IPStats.tcp.ConnectCnt++;
            }
         } else {
            if (evtIP == sktAddr.sin_addr.s_addr) {
               DbgPrintf (DBG_INFO, "Evt socket refreshed");
               shutdown (evtSkt, SHUT_RDWR);
               evtSkt = skt;
               IPStats.tcp.ConnectCnt++;
            } else {
               // Disallow multiple host connections
               shutdown (skt, SHUT_RDWR);
               DbgPrintf (DBG_WARN, "Event socket already open. New socket rejected");
               IPStats.tcp.ConnectErrCnt++;
            }
         }
         logTime (0x100B0E00);
		}

#ifdef _INCLUDE_IPv6_CODE
    // IPv6 message and event TCP sockets
      // Inbound message requests -> Place in messaging task buffer
      if ((sktList[MSG6_IDX].eventsDetected & POLLIN) != 0) {
         logTime (0x100B0900);
         startTimer (timer1);
         msgI8 = recv (msgSkt6, cmdBuff, cmdMsgI8, MSG_DONTWAIT);
         if (msgI8 <= 0) {
            if ((fdError () != NDK_ECONNRESET) && (fdError() != NDK_EWOULDBLOCK))
               DbgPrintf (DBG_WARN, "IPv6 Message socket rcv error %d. Closing", fdError ());
            if (msgSkt6 != INVALID_SOCKET) {
               shutdown (msgSkt6, SHUT_RDWR);
            }
            closeMsgConnection ();
            sktList[MSG6_IDX].fd = msgSkt6 = INVALID_SOCKET;
            sktList[MSG6_IDX].eventsRequested = 0;
            sktList[MSG6_IDX].eventsDetected = 0;
            memset(&msgIP6, 0, sizeof(IP6N)); 
            IPStats.tcp.Rx.ErrCnt++;
         } else {
            if (acceptMsg (msgI8)) {
               IPStats.tcp.Rx.Cnt++;
               continue;
            }

            // Host handshake failure.
            DbgPrintf (DBG_WARN, "IPv6 Message request rejected.");
            closeMsgConnection ();
            shutdown (msgSkt6, SHUT_RDWR);
            sktList[MSG6_IDX].fd = msgSkt6 = INVALID_SOCKET;
            sktList[MSG6_IDX].eventsRequested = 0;
            sktList[MSG6_IDX].eventsDetected = 0;
            memset(&msgIP6, 0, sizeof(IP6N)); 
            IPStats.tcp.Rx.ErrCnt++;
         }
         logTime (0x100B0A00);
         endTimer (timer1, &IPStats.maxMsgMs, 1);

      } else if ((sktList[MSG6_IDX].eventsDetected & POLLNVAL) != 0) {
         DbgPrintf (DBG_INFO, "IPv6 Messaging socket closed. Invalid");
         closeMsgConnection ();
         shutdown (msgSkt6, SHUT_RDWR);
         sktList[MSG6_IDX].fd = msgSkt6 = INVALID_SOCKET;
         sktList[MSG6_IDX].eventsRequested = 0;
         sktList[MSG6_IDX].eventsDetected = 0;
         memset(&msgIP6, 0, sizeof(IP6N)); 
      } else if (sktList[MSG6_IDX].eventsDetected != 0) {
         DbgPrintf (DBG_WARN, "Unknown G.PAK IPv6 message event %x", sktList[MSG6_IDX].eventsDetected);
      }

      // Open new messaging session requests
      if ((sktList[MSG6_LSTN_IDX].eventsDetected != 0)) {
         logTime (0x100B0B00);
         DbgPrintf (DBG_INFO, "IPv6 Messaging socket open request");
         param = sizeof(sktAddr6);
         skt = accept (msgLstn6, (PSA) &sktAddr6, &param);
         if (msgSkt6 == INVALID_SOCKET) {
            // Accept host messaging connection
            if (skt == INVALID_SOCKET) {
               DbgPrintf (DBG_ERROR, "IPv6 Messaging open request failure %d", fdError ());
               sktList[MSG6_IDX].eventsRequested = 0;
               memset(&msgIP6, 0, sizeof(IP6N));
               IPStats.tcp.ConnectErrCnt++;
            } else {
               priority = 7;    // Messaging packets set to highest prioritiy;
               setsockopt (skt, SOL_SOCKET, SO_PRIORITY, &priority, sizeof(priority));

               DbgPrintf (DBG_INFO, "IPv6 Message socket connected");
               sktList[MSG6_IDX].eventsRequested = POLLIN;
               sktList[MSG6_IDX].fd = msgSkt6 = skt;
               memcpy(&msgIP6, (void *)&sktAddr6.sin6_addr, sizeof(struct in6_addr));
               IPStats.tcp.ConnectCnt++;
            }
         } else {
            if (areSameIp((ADT_UInt8 *)&msgIP6, (ADT_UInt8 *)&sktAddr6.sin6_addr)) {
               DbgPrintf (DBG_INFO, "IPv6 Messaging socket refreshed");
               closeMsgConnection ();
               shutdown (msgSkt6, SHUT_RDWR);
               sktList[MSG6_IDX].fd = msgSkt6 = skt;
               IPStats.tcp.ConnectCnt++;
            } else {
               // Disallow multiple host connections
               shutdown (skt, SHUT_RDWR);
               DbgPrintf (DBG_WARN, "IPv6 Message socket already open. New socket rejected");
               IPStats.tcp.ConnectErrCnt++;
            }
         }
         logTime (0x100B0C00);
      }

      // Open new event session requests
      if ((sktList[EVT6_LSTN_IDX].eventsDetected != 0)) {
         logTime (0x100B0D00);
         DbgPrintf (DBG_INFO, "IPv6 Event socket open request");
         param = sizeof(sktAddr6);
         skt = accept (evtLstn6, (PSA) &sktAddr6, &param);
         if (evtSkt6 == INVALID_SOCKET) {
            // Accept host event connection
            if (skt == INVALID_SOCKET) {
               DbgPrintf (DBG_ERROR, "IPv6 Event open request failure %d", fdError ());
               IPStats.tcp.ConnectErrCnt++;
            } else {
               evtSkt6 = skt;
               DbgPrintf (DBG_INFO, "IPv6 Event socket connected");
               memcpy(&evtIP6, (void *)&sktAddr6.sin6_addr, sizeof(struct in6_addr));
               IPStats.tcp.ConnectCnt++;
            }
         } else {
            if (areSameIp((ADT_UInt8 *)&evtIP6, (ADT_UInt8 *)&sktAddr6.sin6_addr)) {
               DbgPrintf (DBG_INFO, "IPv6 Evt socket refreshed");
               shutdown (evtSkt6, SHUT_RDWR);
               evtSkt6 = skt;
               IPStats.tcp.ConnectCnt++;
            } else {
               // Disallow multiple host connections
               shutdown (skt, SHUT_RDWR);
               DbgPrintf (DBG_WARN, "IPv6 Event socket already open. New socket rejected");
               IPStats.tcp.ConnectErrCnt++;
            }
         }
         logTime (0x100B0E00);
      // update emac stats:

     }

    if (IPv6Info.add_address == 1) {
        IPv6Info.add_address = 0;
        addGlobalIpv6();
    }
    if (IPv6Info.add_address == 3) {
        IPv6Info.add_address = 0;
        gpakIPv6DisplayRouteTable(); 
    }
#endif // ipv6 message and event TCP
#endif

#ifdef EMAC_STATS
        if (read_emac_stats)
        {
            /* Read EMAC statistics */
            emac_get_statistics(0, &emac_stats);
        }
#endif

      }



 IPExit:
   DbgPrintf (DBG_WARN, "IP Recv Exit %x", fdError ());
#ifdef UDP_MESSAGING
   if (evtSkt != INVALID_SOCKET) {
      shutdown (msgSkt, SHUT_RDWR);
   }
#else
   if (msgLstn != INVALID_SOCKET) {
      shutdown (msgLstn, SHUT_RDWR);
   }
#endif

   if (msgSkt != INVALID_SOCKET) {
      shutdown (msgSkt, SHUT_RDWR);
   }
   if (rtpSkt != INVALID_SOCKET) {
      shutdown (rtpSkt, SHUT_RDWR);
   }

#ifdef _INCLUDE_IPv6_CODE
   if (msgLstn6 != INVALID_SOCKET) {
      shutdown (msgLstn6, SHUT_RDWR);
   }
   if (msgSkt6 != INVALID_SOCKET) {
      shutdown (msgSkt6, SHUT_RDWR);
   }
   if (rtpSkt6 != INVALID_SOCKET) {
      shutdown (rtpSkt6, SHUT_RDWR);
   }
#endif
   fdCloseSession ((HANDLE) TSK_self ());
}



#ifdef USE_GENERAL_TX_SOCKET
void sendUdpPktTo(
	ADT_UInt32 ipAddress,		// destination IP Address
	ADT_UInt16 portNum,			// destination UDP Port Number
	ADT_UInt8 *pPayload,		// pointer to UDP payload
	ADT_UInt16 length			// length of UDP payload (bytes)
	)
{
	PBM_Pkt *pPbmPkt;			// pointer to network packet buffer
	UINT8 *pByte;				// pointer to a packet byte
	if (hGenUdpTxSock != INVALID_SOCKET)
	{
		pPbmPkt = PBM_alloc(6 + length);
		if (pPbmPkt != NULL)
		{
			pByte = pPbmPkt->pDataBuffer + pPbmPkt->DataOffset;
			pByte[0] = (UINT8) ((ipAddress >> 24) & 0xFF);
			pByte[1] = (UINT8) ((ipAddress >> 16) & 0xFF);
			pByte[2] = (UINT8) ((ipAddress >> 8) & 0xFF);
			pByte[3] = (UINT8) (ipAddress & 0xFF);
			pByte[4] = (UINT8) ((portNum >> 8) & 0xFF);
			pByte[5] = (UINT8) (portNum & 0xFF);
			mmCopy(pByte + 6, pPayload, length);
			pPbmPkt->ValidLen = 6 + length;
			PBMQ_enq(&genUdpTxPbmQueue, pPbmPkt);
		}
	}
	return;
}
#endif
#ifdef _INCLUDE_IPv6_CODE
#ifdef IPV6_TEST_UDP

int sendUdpPktToIPv6(
	ADT_UInt8 ipAddress[],		// destination IP Address
	ADT_UInt16 portNum,			// destination UDP Port Number
	ADT_UInt8 *pPayload,		// pointer to UDP payload
	ADT_UInt16 paylenI8			// length of UDP payload (bytes)
	)
{
int param, status = -1;
struct sockaddr_in6   sktAddr;

	if (rtpSkt6 != INVALID_SOCKET)
	{
        param = sizeof(sktAddr6);
        memset( &sktAddr, 0, sizeof(struct sockaddr_in) );
        sktAddr.sin6_family = AF_INET6;
        memcpy((void *)&sktAddr.sin6_addr,(void *)ipAddress, sizeof(struct in6_addr));
        sktAddr.sin6_port = NDK_htons(portNum);
        sktAddr.sin6_scope_id = gpak_ipv6_device_id;
        status = sendto (rtpSkt6,(void *)pPayload, paylenI8, 0, (PSA)&sktAddr, param);
        if (status < 0)
        {
            udp6_txerr++;        
        } else {
            udp6_tx++;        
        }
	}

	return status;
}
#endif
#endif

#if 0 // jdc avaya integration stubs
void IGMPDeleteRTPMCastIP (IPN mcIpAddr) { }
uint32_t IGMPAddRTPMCastIP (IPN mcIpAddr) {return 0;}
void (*MACUpdate) (IPN rmtIP, ADT_UInt8 *MAC) = 0;
void (*MACUpdate6) (IP6N rmtIP, ADT_UInt8 *MAC) = 0;
void queueMultiCastRequest6 (ADT_UInt8 *ip, int msgType) { }
HANDLE (*appUdpInput6) (PBM_Pkt **ppPkt, UDPHDR *pUdpHdr) = 0;
void MulticastIPToMulticastMAC (IPN IpAddr, ADT_UInt8 *MacAddr) { }
#endif


