#ifndef _IPDEFS_H
#define _IPDEFS_H
typedef struct _IPStats {
   struct rtp {
      struct {
         unsigned int ErrCnt;
         unsigned int LastErr;
         unsigned int SuccessiveCnt;
         unsigned int ActiveCnt;
         unsigned int HostCnt;
         unsigned int InactiveCnt;
         unsigned int OverflowCnt;
         unsigned int TotalCnt;
         unsigned int FrameReleaseCnt;
         unsigned int ReleaseCnt;
         unsigned int DiscardCnt;
         unsigned int Rtcp;
         unsigned int TotalCnt6;
      } Rx;
      struct {
         unsigned int Cnt;
         unsigned int ErrCnt;
         int LastErr;
         unsigned int SuccessiveCnt;
         unsigned int Rtcp;
      } Tx;
   } rtp;
   struct tcp {
      struct {
         unsigned int Cnt;
         unsigned int ErrCnt;
         unsigned int LastErr;
      } Rx;
      struct {
         unsigned int Cnt;
         unsigned int ErrCnt;
         unsigned int LastErr;
      } Tx;
      unsigned int ConnectCnt;
      unsigned int ConnectErrCnt;
   } tcp;
   unsigned int totalLoops;
   unsigned int msgWaits;
   ADT_UInt32 maxMs, maxMsgMs;

#ifdef _DEBUG
   ADT_UInt32 maxRTPGetUs, maxTxUs, maxFrameTxUs, avgPktTxUs;

   ADT_UInt32 minLookupUs, maxLookupUs, maxRTPStoreUs, maxRxUs;

   ADT_UInt32 maxFrameRxUs, avgPktRxUs;

#endif
} _IPStats;

#define DEFAULT_RTP_PORT (UINT16)6334
#define DEFAULT_MSG_PORT (UINT16)56765
#define DEFAULT_EVT_PORT (UINT16)56766
#define DEFAULT_RTP_PORT_RANGE_DEFAULT (UINT16)4000

#ifdef _DEBUG
   #include <std.h>
   #include "adt_typedef_user.h"
   #define STAT_INC(value) value++
   #define STAT_DEC(value) value--
   #define STAT_ADD(sum,value) sum+=value
   #define startTimer(timer) timer = CLK_gethtime ();
   static void endTimer (long timer, ADT_UInt32 *max, int divisor) {
      ADT_UInt32 duration;
      duration = CLK_gethtime () - timer;
      duration /= divisor;
      duration /= CLK_countspms();
      if (*max < duration) *max = duration;
   }
   static void endTimerUs (long timer, ADT_UInt32 *max, int divisor) {
      ADT_UInt32 duration;
      duration = CLK_gethtime () - timer;
      duration /= divisor;
      duration /= (CLK_countspms() / 1000);
      if (*max < duration) *max = duration;
   }
#else
   #define STAT_INC(value)
   #define STAT_DEC(value)
   #define STAT_ADD(sum,value)
   #define startTimer(timer)
   #define endTimer(timer,max,divisor)
#endif

#endif

