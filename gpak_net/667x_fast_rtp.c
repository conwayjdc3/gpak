/*
 *  19-Dec-2013 modified for C674x.
 */

#include <stkmain.h>
#include <ti\ndk\stack\fdt\fdt.h>
#include <ipStackUser.h>
#include <ipStackStats.h>
#ifdef FASTRTPENAB
#define USE_FAST_STATS
extern far SOCKET hRtpRawEthSock;	// RTP Tx raw socket
extern far int rtpRawEthSockStatus;	// RTP Tx raw socket status (0 = OK)
int fastRTPsend6 (void *rtpBuf, int rtpI8, IPToSess_t *IPMapping);

#ifdef USE_NET_STATS
#define USE_FAST_STATS
#endif

#ifdef DEBUGON
#define USE_FAST_STATS
#endif

#ifdef USE_FAST_STATS
  #define FAST_STAT_INC(value) value++
  struct _fastRTPStats fastRTPStats;
#else
  #define FAST_STAT_INC(value)
#endif

// Definition of Ethernet header field offsets.
#define EPO_DEST_MAC 0			// offset to Destination MAC Address
#define EPO_SOURCE_MAC 6		// offset to Source MAC Address
#define EPO_TYPE_FIELD 12		// offset to Type field
#define EPO_VLAN_ID_FIELD 14	// offset to VLAN ID field
#define EPO_VLAN_TYPE_FIELD 16	// offset to VLAN Type field

// Definition of Ethernet Type field values.
#define ETHER_TYPE_IP 0x0800	// Ethernet type value for IP4 Packet
#define ETHER_TYPE_IP6 0x86DD	// Ethernet type value for IP6 Packet
#define ETHER_TYPE_VLAN 0x8100	// Ethernet type value for VLAN Tag

// Miscellaneous Ethernet packet related definitions.
#define ETH_HEADER_SIZE 14		// size of Ethernet header (bytes)
#define VLAN_TAG_LEN 4			// length of VLAN tag (bytes)

// Definition of IP packet field offsets.
#define IPO_VERSION_IHL 0			// offset to Version and IHL fields
#define IPO_TOS 1					// offset to Type Of Service
#define IPO_TOTAL_LENGTH 2			// offset to Total Length
#define IPO_IDENTIFICATION 4		// offset to Identification
#define IPO_FLAGS_FRAG_OFF 6		// offset to Flags and Fragment fields
#define IPO_TTL 8					// offset to Time To Live
#define IPO_PROTOCOL 9				// offset to Protocol
#define IPO_HDR_CHECKSUM 10			// offset to Header Checksum
#define IPO_SOURCE_IP_ADDRESS 12	// offset to Source Address
#define IPO_DEST_IP_ADDRESS 16		// offset to Destination Address

// IP6 header fields
#define IP6_VERSION_TRAFFIC_CLASS 0 // offset to Version and traffic class fields B7-B4
#define IP6_TRAFFIC_CLASS_FLOW    1 // offset to traffic class B3-B0 and flow label B19-B16
#define IP6_FLOW_LABEL            2 // offset to flow label B15-B8
#define IP6_PAY_LENGTH            4	// offset to payload Length B15-B8
#define IP6_NEXT_HEADER           6	// offset to next header field
#define IP6_HOP_LIMIT             7 // offset to hop limit field
#define IP6_SOURCE_IP_ADDRESS     8	// offset to Source Address
#define IP6_DEST_IP_ADDRESS       24 // offset to Destination Address


// Miscellaneous IP packet related definitions.
#define IP_HEADER_SIZE 20			// size of IP header (bytes)
#define IP6_HEADER_SIZE 40			// size of IP header (bytes)
#define IHL_FIELD_MASK 0x0F			// IHL field mask (byte value)
#define MF_FRAG_FIELDS_MASK 0x3FFF	// More Flag and Frag mask (16 bit value)

// Definition of UDP packet field offsets.
#define UDPO_SOURCE_PORT 0		// offset to Source Port
#define UDPO_DEST_PORT 2		// offset to Destination Port
#define UDPO_LENGTH 4			// offset to Length
#define UDPO_CHECKSUM 6			// offset to Checksum

// Miscellaneous UDP packet related definitions.
#define UDP_HEADER_SIZE 8			// size of UDP header (bytes) */
#define CHECKSUM_ALL_ONES 0xFFFF	// checksum value of all 1's

// Convenience macros to translate packet bytes from variables.
#define GET_U16(bufr, offset) \
	((((UINT16) bufr[offset]) << 8) | ((UINT16) bufr[offset + 1]))
#define PUT_U16(value, bufr, offset) \
	bufr[offset] = (UINT8) ((value >> 8) & 0xFF); \
	bufr[offset + 1] = (UINT8) (value & 0xFF)
#define PUT_U32(value, bufr, offset) \
	bufr[offset] = (UINT8) ((value >> 24) & 0xFF); \
	bufr[offset + 1] = (UINT8) ((value >> 16) & 0xFF); \
	bufr[offset + 2] = (UINT8) ((value >> 8) & 0xFF); \
	bufr[offset + 3] = (UINT8) (value & 0xFF)


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// fastRTPsend - Build and send a raw RTP packet on the network.
//
// FUNCTION
//  This function builds a complete raw Ethernet packet containing an RTP packet
//  and sends it on the network.
//
// RETURNS
//  Non zero if the packet was sent or zero if not sent.
//
// jdc #pragma CODE_SECTION (fastRTPsend, "FAST_PROG_SECT:IPStack")
int fastRTPsend (void *rtpBuf, int rtpI8, IPToSess_t *IPMapping)
{
	UINT32 payloadLength;	// payload length (bytes)
	UINT32 vlanLength;		// length of VLAN field (bytes)
	UINT32 udpLength;		// UDP length (bytes)
	UINT32 ipLength;		// IP length (bytes)
	UINT32 pktLength;		// packet length (bytes)
	PBM_Pkt *pPbmPkt;		// pointer to network packet buffer
	UINT32 errCode;			// error code
	UINT8 *pRawPktBufr;		// pointer to raw packet buffer
	UINT8 *pEthHdr;			// pointer to Ethernet header
	UINT8 *pIpHdr;			// pointer to IP header
	UINT8 *pUdpHdr;			// pointer to UDP header
	UINT16 id;				// identification
	UINT32 ipAddress;		// IP address
	UINT16 portNum;			// port number
	UINT32 sum;				// summation
	UINT32 i;				// loop index / counter
	UINT16 bytePair;		// pair of bytes
	UINT16 checksum;		// checksum value
	UINT8 *pByte;			// pointer to a packet byte
	UINT32 sumLength;		// number of bytes in checksum
	INT32 bytesSent;		// number of bytes sent

   FILEDESC *fd = (FILEDESC *) hRtpRawEthSock;
   int error = 0;

    if (IPMapping->ipver == 6) {
        return (fastRTPsend6 (rtpBuf, rtpI8, IPMapping));
    }



	FAST_STAT_INC(fastRTPStats.requests);

	// Verify the raw socket was created.
	if (rtpRawEthSockStatus != 0)
	{
		FAST_STAT_INC(fastRTPStats.memoryDrops);
		return 0;
	}

   llEnter ();
   // Lock the file descriptor
   if (fdint_lockfd (fd, HTYPE_RAWETHSOCK) == SOCKET_ERROR) {
      llExit ();
      return 0;
   }

	// Verify the destination MAC address was resolved.
	if ((IPMapping->rmt.MAC[0] | IPMapping->rmt.MAC[1] |
		 IPMapping->rmt.MAC[2] | IPMapping->rmt.MAC[3] |
		 IPMapping->rmt.MAC[4] | IPMapping->rmt.MAC[5]) == 0)
	{
		FAST_STAT_INC(fastRTPStats.memoryDrops);
        goto fastsend_error;
	}

	// Determine the VLAN field length.
	if (IPMapping->vlanIndex != 0xFF)
	{
		vlanLength = VLAN_TAG_LEN;
	}
	else
	{
		vlanLength = 0;
	}

	// Determine the length of the UDP, IP, and Ethernet packets.
	payloadLength = (UINT32) rtpI8;
	udpLength = UDP_HEADER_SIZE + payloadLength;
	ipLength = IP_HEADER_SIZE + udpLength;
	pktLength = ETH_HEADER_SIZE + vlanLength + ipLength;

	// Allocate a packet buffer.
	pPbmPkt = RawEthSockCreatePacket(hRtpRawEthSock, pktLength, &errCode);
	if (pPbmPkt == NULL)
	{
		FAST_STAT_INC(fastRTPStats.memoryDrops);
        goto fastsend_error;
	}

	// Build the packet.
	pRawPktBufr = pPbmPkt->pDataBuffer + pPbmPkt->DataOffset;

	// Build the Ethernet header.
	pEthHdr = pRawPktBufr;
	pEthHdr[EPO_DEST_MAC] = IPMapping->rmt.MAC[0];
	pEthHdr[EPO_DEST_MAC + 1] = IPMapping->rmt.MAC[1];
	pEthHdr[EPO_DEST_MAC + 2] = IPMapping->rmt.MAC[2];
	pEthHdr[EPO_DEST_MAC + 3] = IPMapping->rmt.MAC[3];
	pEthHdr[EPO_DEST_MAC + 4] = IPMapping->rmt.MAC[4];
	pEthHdr[EPO_DEST_MAC + 5] = IPMapping->rmt.MAC[5];
	pEthHdr[EPO_SOURCE_MAC] = IPMapping->lcl.MAC[0];
	pEthHdr[EPO_SOURCE_MAC + 1] = IPMapping->lcl.MAC[1];
	pEthHdr[EPO_SOURCE_MAC + 2] = IPMapping->lcl.MAC[2];
	pEthHdr[EPO_SOURCE_MAC + 3] = IPMapping->lcl.MAC[3];
	pEthHdr[EPO_SOURCE_MAC + 4] = IPMapping->lcl.MAC[4];
	pEthHdr[EPO_SOURCE_MAC + 5] = IPMapping->lcl.MAC[5];
	if (vlanLength == 0)
	{
		PUT_U16(ETHER_TYPE_IP, pEthHdr, EPO_TYPE_FIELD);
	}
	else
	{
		PUT_U16(ETHER_TYPE_VLAN, pEthHdr, EPO_TYPE_FIELD);
		PUT_U16(VLANTags[IPMapping->vlanIndex].Tag, pEthHdr, EPO_VLAN_ID_FIELD);
		PUT_U16(ETHER_TYPE_IP, pEthHdr, EPO_VLAN_TYPE_FIELD);
	}

	// Build the IP header.
	pIpHdr = &(pEthHdr[ETH_HEADER_SIZE + vlanLength]);
	pIpHdr[IPO_VERSION_IHL] = 0x45;
	pIpHdr[IPO_TOS] = IPMapping->DSCP;
	PUT_U16(ipLength, pIpHdr, IPO_TOTAL_LENGTH);
	id = IP_INDEX++;
	PUT_U16(id, pIpHdr, IPO_IDENTIFICATION);
	PUT_U16(0x0000, pIpHdr, IPO_FLAGS_FRAG_OFF);
	if (IN_MULTICAST(IPMapping->rmt.dstIP))
	{
		pIpHdr[IPO_TTL] = 1;
	}
	else
	{
		pIpHdr[IPO_TTL] = SOCK_TTL_DEFAULT;
	}
	pIpHdr[IPO_PROTOCOL] = IPPROTO_UDP;
	PUT_U16(0, pIpHdr, IPO_HDR_CHECKSUM);
	ipAddress = NDK_htonl(IPMapping->lcl.IP);
	PUT_U32(ipAddress, pIpHdr, IPO_SOURCE_IP_ADDRESS);
	ipAddress = NDK_htonl(IPMapping->rmt.dstIP);
	PUT_U32(ipAddress, pIpHdr, IPO_DEST_IP_ADDRESS);

	// Calculate the IP header checksum.
	sum = 0;
	for (i = 0; i < IP_HEADER_SIZE; i += 2)
	{
		bytePair = (((UINT16) pIpHdr[i]) << 8) | ((UINT16) pIpHdr[i + 1]);
		sum += (UINT32) bytePair;
	}
	while ((sum >> 16) != 0)
	{
		sum = (sum & 0xFFFF) + (sum >> 16);
	}
	checksum = (UINT16) (~sum & 0xFFFF);
	PUT_U16(checksum, pIpHdr, IPO_HDR_CHECKSUM);

	// Build the UDP header.
	pUdpHdr = &(pIpHdr[IP_HEADER_SIZE]);
	portNum = NDK_htons(IPMapping->lcl.Port);
	PUT_U16(portNum, pUdpHdr, UDPO_SOURCE_PORT);
	portNum = NDK_htons(IPMapping->rmt.Port);
	PUT_U16(portNum, pUdpHdr, UDPO_DEST_PORT);
	PUT_U16(udpLength, pUdpHdr, UDPO_LENGTH);
	PUT_U16(0, pUdpHdr, UDPO_CHECKSUM);

	// Append the UDP payload.
	mmCopy(&(pUdpHdr[UDP_HEADER_SIZE]), rtpBuf, payloadLength);

	// Calculate the UDP checksum.
	sum = IPPROTO_UDP + ((UINT32) udpLength);
	pByte = &(pIpHdr[IPO_SOURCE_IP_ADDRESS]);
	sumLength = ipLength - IPO_SOURCE_IP_ADDRESS;
	for (i = 0; i < (sumLength / 2); i++)
	{
		sum += (UINT32) GET_U16(pByte, (i * 2));
	}
	if ((sumLength & 1) != 0)
	{
		sum += ((UINT32) pByte[sumLength - 1]) << 8;
	}
	while ((sum >> 16) != 0)
	{
		sum = (sum & 0xFFFF) + (sum >> 16);
	}
	checksum = (UINT16) (~sum & 0xFFFF);
	if (checksum == 0)
	{
		checksum = CHECKSUM_ALL_ONES;
	}
	PUT_U16(checksum, pUdpHdr, UDPO_CHECKSUM);

	// Send the packet.
	error = RawEthSockSendNC((HANDLE) hRtpRawEthSock, (char *) pRawPktBufr,
						 (INT32) pktLength, (HANDLE) pPbmPkt, &bytesSent);
	if (error != 0)
	{
		PBM_free(pPbmPkt);
		FAST_STAT_INC(fastRTPStats.memoryDrops);
        goto fastsend_error;
	}

   fdint_unlockfd (fd, 0);
   llExit ();
   return 1;

fastsend_error:
   fdint_unlockfd (fd, error);
   llExit ();
   return 0;


}
int fastRTPsend6 (void *rtpBuf, int rtpI8, IPToSess_t *IPMapping)
{
	UINT32 payloadLength;	// payload length (bytes)
	UINT32 vlanLength;		// length of VLAN field (bytes)
	UINT32 udpLength;		// UDP length (bytes)
	UINT32 ipLength;		// IP length (bytes)
	UINT32 pktLength;		// packet length (bytes)
	PBM_Pkt *pPbmPkt;		// pointer to network packet buffer
	UINT32 errCode;			// error code
	UINT8 *pRawPktBufr;		// pointer to raw packet buffer
	UINT8 *pEthHdr;			// pointer to Ethernet header
	UINT8 *pIpHdr;			// pointer to IP header
	UINT8 *pUdpHdr;			// pointer to UDP header
	UINT16 portNum;			// port number
	UINT32 sum;				// summation
	UINT32 i;				// loop index / counter
	UINT16 checksum;		// checksum value
	UINT8 *pByte;			// pointer to a packet byte
	UINT32 sumLength;		// number of bytes in checksum
	INT32 bytesSent;		// number of bytes sent

	FAST_STAT_INC(fastRTPStats.requests);

	// Verify the raw socket was created.
	if (rtpRawEthSockStatus != 0)
	{
		FAST_STAT_INC(fastRTPStats.memoryDrops);
		return 0;
	}

	// Verify the destination MAC address was resolved.
	if ((IPMapping->rmt6.MAC[0] | IPMapping->rmt6.MAC[1] |
		 IPMapping->rmt6.MAC[2] | IPMapping->rmt6.MAC[3] |
		 IPMapping->rmt6.MAC[4] | IPMapping->rmt6.MAC[5]) == 0)
	{
		FAST_STAT_INC(fastRTPStats.memoryDrops);
		return 0;
	}

	// Determine the VLAN field length.
	if (IPMapping->vlanIndex != 0xFF)
	{
		vlanLength = VLAN_TAG_LEN;
	}
	else
	{
		vlanLength = 0;
	}

	// Determine the length of the UDP, IP, and Ethernet packets.
	payloadLength = (UINT32) rtpI8;
	udpLength = UDP_HEADER_SIZE + payloadLength;
	ipLength = IP6_HEADER_SIZE + udpLength;
	pktLength = ETH_HEADER_SIZE + vlanLength + ipLength;

	// Allocate a packet buffer.
	pPbmPkt = RawEthSockCreatePacket(hRtpRawEthSock, pktLength, &errCode);
	if (pPbmPkt == NULL)
	{
		FAST_STAT_INC(fastRTPStats.memoryDrops);
		return 0;
	}

	// Build the packet.
	pRawPktBufr = pPbmPkt->pDataBuffer + pPbmPkt->DataOffset;

	// Build the Ethernet header.
	pEthHdr = pRawPktBufr;
	pEthHdr[EPO_DEST_MAC] = IPMapping->rmt6.MAC[0];
	pEthHdr[EPO_DEST_MAC + 1] = IPMapping->rmt6.MAC[1];
	pEthHdr[EPO_DEST_MAC + 2] = IPMapping->rmt6.MAC[2];
	pEthHdr[EPO_DEST_MAC + 3] = IPMapping->rmt6.MAC[3];
	pEthHdr[EPO_DEST_MAC + 4] = IPMapping->rmt6.MAC[4];
	pEthHdr[EPO_DEST_MAC + 5] = IPMapping->rmt6.MAC[5];
	pEthHdr[EPO_SOURCE_MAC] = IPMapping->lcl6.MAC[0];
	pEthHdr[EPO_SOURCE_MAC + 1] = IPMapping->lcl6.MAC[1];
	pEthHdr[EPO_SOURCE_MAC + 2] = IPMapping->lcl6.MAC[2];
	pEthHdr[EPO_SOURCE_MAC + 3] = IPMapping->lcl6.MAC[3];
	pEthHdr[EPO_SOURCE_MAC + 4] = IPMapping->lcl6.MAC[4];
	pEthHdr[EPO_SOURCE_MAC + 5] = IPMapping->lcl6.MAC[5];
	if (vlanLength == 0)
	{
		PUT_U16(ETHER_TYPE_IP6, pEthHdr, EPO_TYPE_FIELD);
	}
	else
	{
		PUT_U16(ETHER_TYPE_VLAN, pEthHdr, EPO_TYPE_FIELD);
		PUT_U16(VLANTags[IPMapping->vlanIndex].Tag, pEthHdr, EPO_VLAN_ID_FIELD);
		PUT_U16(ETHER_TYPE_IP6, pEthHdr, EPO_VLAN_TYPE_FIELD);
	}

	// Build the IP header.
	pIpHdr = &(pEthHdr[ETH_HEADER_SIZE + vlanLength]);
	pIpHdr[IP6_VERSION_TRAFFIC_CLASS] = 0x60 | ((IPMapping->DSCP)>>4);
	pIpHdr[IP6_TRAFFIC_CLASS_FLOW] = ((IPMapping->DSCP) & 0xF)<<4;
	PUT_U16(0, pIpHdr, IP6_FLOW_LABEL);
	PUT_U16(udpLength, pIpHdr, IP6_PAY_LENGTH);
    pIpHdr[IP6_NEXT_HEADER] = IPPROTO_UDP;
	if (IN_MULTICAST6(IPMapping->rmt6.dstIP))
	{
		pIpHdr[IP6_HOP_LIMIT] = IPV6_MCAST_DEF_HOP_LIMIT;
	}
	else
	{
		pIpHdr[IP6_HOP_LIMIT] = IPV6_UCAST_DEF_HOP_LIMIT;
	}
    mmCopy(&(pIpHdr[IP6_SOURCE_IP_ADDRESS]), IPMapping->lcl6.IP, 16);
    mmCopy(&(pIpHdr[IP6_DEST_IP_ADDRESS]),   IPMapping->rmt6.dstIP, 16);

	// Build the UDP header.
	pUdpHdr = &(pIpHdr[IP6_HEADER_SIZE]);
	portNum = NDK_htons(IPMapping->lcl6.Port);
	PUT_U16(portNum, pUdpHdr, UDPO_SOURCE_PORT);
	portNum = NDK_htons(IPMapping->rmt6.Port);
	PUT_U16(portNum, pUdpHdr, UDPO_DEST_PORT);
	PUT_U16(udpLength, pUdpHdr, UDPO_LENGTH);
	PUT_U16(0, pUdpHdr, UDPO_CHECKSUM);

	// Append the UDP payload.
	mmCopy(&(pUdpHdr[UDP_HEADER_SIZE]), rtpBuf, payloadLength);

	// Calculate the UDP checksum.
	sum = IPPROTO_UDP + ((UINT32) udpLength);
	pByte = &(pIpHdr[IP6_SOURCE_IP_ADDRESS]);
	sumLength = ipLength - IP6_SOURCE_IP_ADDRESS;
	for (i = 0; i < (sumLength / 2); i++)
	{
		sum += (UINT32) GET_U16(pByte, (i * 2));
	}
	if ((sumLength & 1) != 0)
	{
		sum += ((UINT32) pByte[sumLength - 1]) << 8;
	}
	while ((sum >> 16) != 0)
	{
		sum = (sum & 0xFFFF) + (sum >> 16);
	}
	checksum = (UINT16) (~sum & 0xFFFF);
	if (checksum == 0)
	{
		checksum = CHECKSUM_ALL_ONES;
	}
	PUT_U16(checksum, pUdpHdr, UDPO_CHECKSUM);

	// Send the packet.
	if (RawEthSockSendNC((HANDLE) hRtpRawEthSock, (char *) pRawPktBufr,
						 (INT32) pktLength, (HANDLE) pPbmPkt, &bytesSent) != 0)
	{
		PBM_free(pPbmPkt);
		FAST_STAT_INC(fastRTPStats.memoryDrops);
		return 0;
	}

	return 1;
}
#else
int fastRTPsend (void *rtpBuf, int rtpI8, IPToSess_t *IPMapping) { return 0;}
int fastRTPsend6 (void *rtpBuf, int rtpI8, IPToSess_t *IPMapping) { return 0;}
#endif
