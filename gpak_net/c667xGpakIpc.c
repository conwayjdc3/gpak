#ifdef IPC_SUPPORT
#include <c6x.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <xdc/std.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/Memory.h>
#include <xdc/runtime/Types.h>
#include <xdc/runtime/Timestamp.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/IHeap.h>
#include <xdc/runtime/Log.h>
#include <ti/ipc/HeapBufMP.h>
#include <ti/ipc/MultiProc.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/hal/Cache.h>
#include <xdc/cfg/global.h>
#include "gpak_ipc.h"


extern far int DSPCore;
#define ROUNDUP(n,w) (((n) + (w) - 1) & ~((w) - 1))
#define MAX_CACHE_LINE (128)


static ipc_message_t ** p_queue_msg = 0;
static char slave_queue_name[MAX_NUM_CORES][16];

static MessageQ_Handle h_receive_queue = 0;
MessageQ_QueueId queue_id[MAX_NUM_CORES];

#define platform_write printf

int gpak_msgQ_init(int number_of_cores) {
    HeapBufMP_Handle heapHandle;
    HeapBufMP_Params heapBufParams;
    Int              status;
    int i, j;


    /* Create the heap that will be used to allocate messages. */
    HeapBufMP_Params_init(&heapBufParams);
    heapBufParams.regionId       = 0;
    heapBufParams.name           = GPAK_IPC_MSG_HEAP_NAME;
    heapBufParams.numBlocks      = number_of_cores;
    heapBufParams.blockSize      = sizeof(ipc_message_t);
    heapHandle = HeapBufMP_create(&heapBufParams);
    if (heapHandle == NULL) {
        platform_write(" gpak_msgQ_init: HeapBufMP_create failed\n" );
        return -1;
    }

    /* Register this heap with MessageQ */
    status = MessageQ_registerHeap((IHeap_Handle)heapHandle, GPAK_IPC_MSG_HEAPID);
    if(status != MessageQ_S_SUCCESS) {
        platform_write(" gpak_msgQ_init: MessageQ_registerHeap failed\n" );
        return -1;
    }

    p_queue_msg = (ipc_message_t **) calloc(number_of_cores, sizeof(ipc_message_t *));
    if (!p_queue_msg) {
        platform_write("alloc_queue_message: Can't allocate memory for queue message\n");
        return -1;
    }

    for (i = 0; i < number_of_cores; i++) {
        p_queue_msg[i] =  (ipc_message_t *) MessageQ_alloc(GPAK_IPC_MSG_HEAPID, sizeof(ipc_message_t));
        if (!p_queue_msg[i]) {
            platform_write("alloc_queue_message: Can't allocate memory for queue message %d\n", i);
            return -1;            
        }
    }

    memset(slave_queue_name, 0, MAX_NUM_CORES*16);
    for (i = 0; i < MAX_NUM_CORES; i++) {
        GET_SLAVE_QUEUE_NAME(slave_queue_name[i], i);
    }


    for (j = 0; j < number_of_cores; j++) {
        do {
            status = MessageQ_open(slave_queue_name[j], &queue_id[j]);
        } while (status < 0);
    }

    for (i = 0; i < number_of_cores; i++) {
        p_queue_msg[i]->info.msg = 
            (uint8_t *) Memory_alloc(0, ROUNDUP(GPAK_IPC_MAX_MSGLENI8, MAX_CACHE_LINE), MAX_CACHE_LINE, NULL);
        if(!p_queue_msg[i]->info.msg) {
            platform_write("gpak_msgQ_send: Memory_alloc failed for msgbuf\n");
    		return -1;
        }
        p_queue_msg[i]->info.msglenI8 = 0;
    }

    platform_write(" gpak_msgQ_init: Success\n" );

    return 0;
}

int gpak_msgQ_send (int code, uint8_t *msgBuf, int msglenI8, int number_of_cores, int ack_required) {
	int i;
    ipc_message_t * p_msg = 0;
    uint16_t msgId = 0;
    int msg_cache_size;


    if (msglenI8 > GPAK_IPC_MAX_MSGLENI8)
        return -1;
    
	for (i = number_of_cores-1; i >= 0; i-- ) {
        p_msg = p_queue_msg[i];
        p_msg->core_id       = i;
        p_msg->info.code     = code;
        p_msg->info.ack      = ack_required;
        p_msg->info.msglenI8 = msglenI8;
        memcpy(p_msg->info.msg, msgBuf, msglenI8);
        msg_cache_size = ROUNDUP(msglenI8, MAX_CACHE_LINE);
        Cache_wb(p_msg->info.msg, msg_cache_size, Cache_Type_ALL, FALSE);

        MessageQ_setMsgId(p_msg, ++msgId);
        MessageQ_setReplyQueue(h_receive_queue, (MessageQ_Msg)p_msg);

        /* send the message to the remote processor */
        if (MessageQ_put(queue_id[i], (MessageQ_Msg)p_msg) < 0) {
            platform_write("MessageQ_put had a failure error\n");
    		return -1;
        }
	}
    return 0;
}


// ipc message receive thread
void gpak_ipc_receive(void)
{
    ipc_message_t * p_msg = 0;    
//    MessageQ_Handle  h_receive_queue = 0;
    MessageQ_QueueId reply_queue_id = 0;
    HeapBufMP_Handle heapHandle;
    Int status;
    char receive_queue_name[16];
    int i;

    GET_SLAVE_QUEUE_NAME(receive_queue_name, DSPCore);

    /* Open the heap created by the master processor. Loop until opened. */    
    do {        
        status = HeapBufMP_open(GPAK_IPC_MSG_HEAP_NAME, &heapHandle);
        if (status < 0) { 
            Task_sleep(1);
        }
    } while (status < 0);

    /* Register this heap with MessageQ */    
    MessageQ_registerHeap((IHeap_Handle)heapHandle, GPAK_IPC_MSG_HEAPID);
    
    /* Create the local message queue */
    h_receive_queue = MessageQ_create(receive_queue_name, NULL);    
    if (h_receive_queue == NULL) {
        platform_write("MessageQ_create failed\n" );
		goto close_n_exit;
    }

	for (;;) {
 
		if (MessageQ_get(h_receive_queue, (MessageQ_Msg *)&p_msg, MessageQ_FOREVER) < 0) {
		    platform_write("%s: This should not happen since timeout is forever\n", receive_queue_name);
		    goto close_n_exit;
		}

        reply_queue_id = MessageQ_getReplyQueue(p_msg);
        if (reply_queue_id == MessageQ_INVALIDMESSAGEQ) {
            platform_write("receive_queue_name: (%s) Ignoring the message as reply queue is not set.\n", receive_queue_name);
            continue;
        }

        if (MultiProc_self() != 0) {
            Cache_inv(p_msg->info.msg, p_msg->info.msglenI8, Cache_Type_ALL, FALSE);
        }

        // placeholder to process the message here...
        platform_write("msg_id %d received on core%d, code %d, ack = %d, length %d from core_id%d\n", MessageQ_getMsgId(p_msg), DSPCore, p_msg->info.code,p_msg->info.ack, p_msg->info.msglenI8, p_msg->core_id);
        if (p_msg->info.msg) {
            for (i=0; i<p_msg->info.msglenI8; i++)
                platform_write("p_msg->info.msg[%d] = %d\n",i,p_msg->info.msg[i]);
        }

        /* send the reply message to the remote processor */
        if (p_msg->info.ack) {
            p_msg->info.ack = 0;
            if (MessageQ_put(reply_queue_id, (MessageQ_Msg)p_msg) < 0) {
                platform_write("%s: MessageQ_put had a failure error\n", receive_queue_name);
            }
        }
	}

close_n_exit:
    if(h_receive_queue) MessageQ_delete(&h_receive_queue);
}
#endif
