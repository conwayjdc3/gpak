/*
 * Copyright (c) 2012-2018, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * */
/*
 * ======== socket.c ========
 *
 * This file handles core BSD style socket calls and pipes.
 *
 */
#include <ti\ndk\inc\stkmain.h>
#include <ti\ndk\stack\fdt\fdt.h>
#include <ti\ndk\stack\sock\sock.h>
#include <ipStackUser.h>
//#include <ti\ndk\inc\ipStackUser.h>
#define SO_RTP          0x0800          // RTP special processing

// derived from NDK_sendto
int sendRTP6( SOCKET s, void *pbuf, int size, int flags, void *IpToSess)
{
#ifdef _INCLUDE_IPv6_CODE
    FILEDESC *pfd = (FILEDESC *)s;
    int  error = 0;
    int32_t  txsize;

    struct sockaddr_in6 sktAddr6;
    IPToSess_t *ipToSess = (IPToSess_t *)IpToSess;
    //SOCK *ps = (SOCK *)s;

    llEnter();

    /* Lock the fd - type must be SOCK / SOCK6 */
     if (fdint_lockfd(pfd, HTYPE_SOCK6) == SOCKET_ERROR)
    {
        llExit();
        return( SOCKET_ERROR );
    }

    // jdc custom code from sendRTP ---------------------------
    memset(&sktAddr6, 0, sizeof(struct sockaddr_in6) );
    sktAddr6.sin6_family = AF_INET6;
    mmCopy((void *)&sktAddr6.sin6_addr,(void *)ipToSess->rmt6.dstIP, sizeof(struct in6_addr));
    sktAddr6.sin6_port = ipToSess->rmt6.Port;
    sktAddr6.sin6_scope_id = IPv6Info.device_id;
    //mmCopy (&ps->LIP,   &ipToSess->lcl6.IP,   16); // Src IP
    //mmCopy (&ps->LPort, &ipToSess->lcl6.Port, 2);  // Src port
    // jdc todo ... update additional sock fields as done in sendRtp IPV4 

    /* Connect */
    if ((error = Sock6Connect(pfd, (struct sockaddr *)&sktAddr6)))
        goto sendto_error;

    /* Send */
    error = Sock6Send(pfd, pbuf, (int32_t)size, flags, &txsize);

    /* Disconnect */
    Sock6Disconnect(pfd);

    /* Record any error condition */
    if (error)
        goto sendto_error;

    /* Unlock the fd without error */
    fdint_unlockfd(pfd, 0);

    llExit();

    return ((int)txsize);

sendto_error:
    /* Unlock the fd with error */
    fdint_unlockfd(pfd, error);

    llExit();
#endif
    return (SOCKET_ERROR);
}


int sendRTP (SOCKET s, void *pbuf, int size, int flags, void *IpToSess) {

   FILEDESC *fd = (FILEDESC *) s;
   SOCK     *skt = (SOCK *) s;
   IPToSess_t *ipToSess = (IPToSess_t *)IpToSess;

   int error = 0;
   int32_t txsize;
   unsigned int txBufSize;
   uint8_t dstMAC[6], nullMAC;
   int i;

   llEnter ();

   // Lock the file descriptor
   if (fdint_lockfd (fd, HTYPE_SOCK) == SOCKET_ERROR) {
      llExit ();
      return SOCKET_ERROR;
   }

   if (skt->ErrorPending) {
      error = skt->ErrorPending;
      skt->ErrorPending = 0;
      goto sendto_error;
   }

   // Special RTP handling
   // Make use of IpOptions array to pass down SRC MAC Addr
   // DST MAC Addr is set via LLIAddStaticEntry
   skt->OptionFlags |= SO_RTP;
   mmCopy (&skt->LIP,        &ipToSess->lcl.IP,   4);  // Src IP
   mmCopy (&skt->LPort,      &ipToSess->lcl.Port, 2);  // Src port
   mmCopy (&skt->IpOptions,  &ipToSess->lcl.MAC,  6);  // Src Mac
   mmCopy (&skt->FIP,        &ipToSess->rmt.dstIP,4);  // Dst IP
   mmCopy (&skt->FPort,      &ipToSess->rmt.Port, 2);  // Dst port
   if (IN_MULTICAST (skt->FIP)) skt->IpTtl = 1;
   else                         skt->IpTtl = SOCK_TTL_DEFAULT;

   skt->IpTos = ipToSess->DSCP;
   skt->IpOptSize = 0;
   skt->StateFlags &= ~(SS_CANTRCVMORE | SS_CANTSENDMORE);
   skt->StateFlags |= SS_ISCONNECTED;
   if (ipToSess->vlanIndex < 10)
      skt->hIFTx = VLANTags[ipToSess->vlanIndex].vlanDeviceHndl;
   else
      skt->hIFTx = BindIPNet2IF (RdNet32 (&ipToSess->lcl.IP));

   txBufSize = skt->TxBufSize;
   skt->TxBufSize = 1500;
   mmCopy (dstMAC,  &ipToSess->rmt.MAC, 6);  // Dst Mac
   for (i=0, nullMAC=0; i<6; i++) nullMAC |= dstMAC[i];
   if (nullMAC) {
      mmCopy (&skt->IpOptions[8], &dstMAC, 6);
      flags |= MSG_DONTROUTE;
   }

   // updOutput --> IPTxPacket --> LLITxIpPacket --> NIMUAddHeader         --> NIMUSendPacket
   //                                                NIMUAddEthernetHeader --> EMACSend
   //
   error = SockSend (fd, pbuf, (INT32) size, flags, &txsize);
   SockDisconnect (fd);
   skt->TxBufSize = txBufSize;  // Restore original size

   if (error) {
      goto sendto_error;
   }
   fdint_unlockfd (fd, 0);
   llExit ();

   return (int) txsize;

 sendto_error:
   fdint_unlockfd (fd, error);
   llExit ();
   return SOCKET_ERROR;
}
