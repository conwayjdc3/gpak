/*
 * Copyright (c) 2014-2018, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * */
/*
 * ======== conipv6.c ========
 *
 * Console command processing which allows configuration and testing
 * of the various IPv6 API available to application developers.
 *
 */
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <GpakBios.h>

#include <ti/ndk/inc/netmain.h>
#include <ti/ndk/inc/_stack.h>
#include <ti/ndk/inc/_oskern.h>
//#include "console.h"
#include <adt_typedef_user.h>
#include <ipStackUser.h>
#include <ipStackStats.h>
//#include <ti/ndk/inc/ipStackUser.h>
//#include <ti/ndk/inc/ipStackStats.h>
#include "ipdefs.h"

extern _IPStats IPStats;
extern IPToSess_t *chnToIPSess [];  // Indexed by channel ID to point to IP session lookup table
extern void MulticastIPToMulticastMAC (IPN IpAddr, ADT_UInt8 *MacAddr);
extern int  IPMap_lock ();
extern void IPMap_unlock (int mask);
extern far INT16 lowRTPPort, highRTPPort;
extern IPToSess_t nullIPSess;
extern PBMQ rtpRxPbmQueue;							// RTP Rx PBM queue
#define TEST_IPV6_ADDR "2001:0:12:1234::2"
         
#ifdef _INCLUDE_IPv6_CODE
extern far void (*MACUpdate6) (IP6N rmtIP, ADT_UInt8 *MAC);
extern far HANDLE (*appUdpInput6) (PBM_Pkt **ppPkt, UDPHDR *pUdpHdr);
static HANDLE appUdpInputIPv6(PBM_Pkt **ppPkt, UDPHDR *pUdpHdr);
void* (*customNetIn6) (ADT_UInt8 *ip, ADT_UInt16 port, ADT_UInt8 *payload) = NULL;
extern void* (*rtcpNetIn6) (ADT_UInt8 *ip, ADT_UInt16 port, ADT_UInt8 *payload);
extern void queueMultiCastRequest6 (ADT_UInt8 *ip, int msgType);
extern void uart_printIPv6Address(char *addr);

#define MAXPACKET       1520        /* max packet size */
#define DATALEN         100         /* default data length */
#define ConPrintf printf

/**
 *  @b Description
 *  @n
 *      The function is registered as the call back function and
 *      indicates the status of IPv6 DAD State machine. System
 *      Developers can use this to ensure the uniqueness of the IPv6
 *      Address assignment.
 *
 *      This function is called OUTSIDE kernel mode.
 *
 *  @param[in]  Address
 *      The IPv6 address on which the DAD state machine has been executed
 *  @param[in]  dev_index
 *      Interface Index on which the IPv6 Address was assigned.
 *  @param[in]  Status
 *      This is the status of DAD state Machine.
 *          - Value of 1 indicates that the address was UNIQUE
 *          - Value of 0 indicates that the address was DUPLICATE
 *
 *  @retval
 *      Not Applicable.
 */
/* ARGSUSED */
static void IPv6DADStatus(IP6N Address, uint16_t dev_index, unsigned char Status)
{
    char strIPAddress[50];

    /* Convert the IP Address to String Format. */
    IPv6IPAddressToString (Address, strIPAddress);
    ConPrintf ("DAD: IPv6 Network  Address : %s\n", strIPAddress);
    if (Status) {
        if (Address.u.addr16[0] == 0xfe80) {
            IPv6Info.linkLocalAddress.u.addr32[0] = Address.u.addr32[0];
            IPv6Info.linkLocalAddress.u.addr32[1] = Address.u.addr32[1];
            IPv6Info.linkLocalAddress.u.addr32[2] = Address.u.addr32[2];
            IPv6Info.linkLocalAddress.u.addr32[3] = Address.u.addr32[3];
        }
        uart_printIPv6Address(strIPAddress);
    }

    return;
}
void gpakIPv6DisplayRouteTable (void)
{
    RT6_ENTRY*  ptr_RoutingTable = NULL;
    RT6_ENTRY*  ptr_rt6;
    char        strIPAddress[50];

    /* Get a copy of the routing table from the ROUTE6 Module. */
    ptr_RoutingTable = Rt6GetTable ();

    /* Search the routing table for a match? */
    ptr_rt6 = (RT6_ENTRY *)list_get_head ((NDK_LIST_NODE**)&ptr_RoutingTable);
    while (ptr_rt6 != NULL)
    {
        ConPrintf ("-------------------------------------------------\n");
        ConPrintf ("Interface Name   : %s\n", ptr_rt6->ptr_device->name);
        IPv6IPAddressToString (ptr_rt6->NetworkAddr, &strIPAddress[0]);
        ConPrintf ("Network  Address : %s\n", strIPAddress);
        IPv6IPAddressToString (ptr_rt6->NextHop, &strIPAddress[0]);
        ConPrintf ("Next Hop Address : %s\n", strIPAddress);

        /* Print the Route Type. */
        if (ptr_rt6->Flags & FLG_RTE_CLONING)
            ConPrintf ("Route Type       : CLONING\n");
        else if (ptr_rt6->Flags & FLG_RTE_IFLOCAL)
            ConPrintf ("Route Type       : LOCAL\n");
        else if (ptr_rt6->Flags & FLG_RTE_GATEWAY)
            ConPrintf ("Route Type       : GATEWAY\n");
        else if (ptr_rt6->Flags & FLG_RTE_HOST)
            ConPrintf ("Route Type       : HOST\n");

        /* Display the timeouts
         *  Note: Internally the timeouts are kept as absolute values */
        if (ptr_rt6->dwTimeout == INFINITE_LT)
            ConPrintf ("Timeout          : Infinite\n");
        else
            ConPrintf ("Timeout          : %d seconds\n", ptr_rt6->dwTimeout - llTimerGetTime(0));

        /* Get the next routing entry. */
        ptr_rt6 = (RT6_ENTRY *)list_get_next((NDK_LIST_NODE*)ptr_rt6);
    }
    ConPrintf ("-------------------------------------------------\n");

    /* Now cleanup the allocated list */
    Rt6CleanTable (ptr_RoutingTable);

    if (IPv6Info.add_address == 2)
        IPv6Info.add_address = 1;
    return;
}

/**
 *  @b Description
 *  @n
 *      The function is called to initialize the IPv6 stack over a
 *      specific Interface.
 *
 *  @param[in]  ifId
 *      Interface Identifier over which the stack it to be initialized
 *      This can either be the name or device index
 *
 *  @retval
 *      Not Applicable.
 */
void gpakIPv6InitializeStack (char* ifId)
{
    uint16_t    dev_index;
    NIMU_IF_REQ if_req;
    int         status;

    /* Get the device index; by first trying it to be an identifier. */
    dev_index = atoi (ifId);
    if (dev_index == 0)
    {
        if (strlen(ifId) < MAX_INTERFACE_NAME_LEN) {
            /* Error: Unable to convert to integer; maybe we were given a name. */
            strcpy (if_req.name, ifId);
        }
        else {
            ConPrintf(
                "gpakIPv6InitializeStack: error interface ID name too long\n");
        }

        if_req.index = 0;
        if (NIMUIoctl (NIMU_GET_DEVICE_INDEX, &if_req, &dev_index, sizeof(dev_index)) < 0)
        {
            ConPrintf ("Error: Device does not exist.\n");
            return;
        }
    }

    /* Enter the kernel Mode. */
    llEnter ();
    status = IPv6InterfaceInit (dev_index, IPv6DADStatus);
    llExit ();

    /* Were we able to initialize the IPv6 stack? */
    if (status < 0)
        ConPrintf ("Error: Unable to initialize the IPv6 stack on device %d\n", dev_index);
    else
        ConPrintf ("IPv6 stack has been initialized on %d\n", dev_index);

    /* Work has been done. */
    return;
}

/**
 *  @b Description
 *  @n
 *      The function is called to deinitialize the IPv6 stack over a
 *      specific Interface.
 *
 *  @param[in]  ifId
 *      Interface Identifier over which the stack it to be deinitialized
 *      This can either be the name or device index
 *
 *  @retval
 *      Not Applicable.
 */
void gpakIPv6DeinitializeStack (char* ifId)
{
    uint16_t    dev_index;
    NIMU_IF_REQ if_req;
    int         status;

    /* Get the device index; by first trying it to be an identifier. */
    dev_index = atoi (ifId);
    if (dev_index == 0)
    {
        if (strlen(ifId) < MAX_INTERFACE_NAME_LEN) {
            /* Error: Unable to convert to integer; maybe we were given a name. */
            strcpy (if_req.name, ifId);
        }
        else {
            ConPrintf(
                "gpakIPv6DeinitializeStack: error interface ID name too long\n");
        }

        if_req.index = 0;
        if (NIMUIoctl (NIMU_GET_DEVICE_INDEX, &if_req, &dev_index, sizeof(dev_index)) < 0)
        {
            ConPrintf ("Error: Device does not exist.\n");
            return;
        }
    }

    /* Enter the kernel Mode. */
    llEnter ();
    status = IPv6InterfaceDeInit (dev_index);
    llExit ();

    /* Were we able to deinitialize the stack? */
    if (status < 0)
        ConPrintf ("Error: Unable to de-initialize the IPv6 stack on device %d\n", dev_index);
    else
        ConPrintf ("IPv6 stack has been deinitialized on %d\n", dev_index);

    /* Work has been done. */
    return;
}

/**
 *  @b Description
 *  @n
 *      The function is called to add an IPv6 Address to the specific interface.
 *
 *  @param[in]  ifId
 *      Interface Identifier over which the address is to be added.
 *  @param[in]  Address
 *      IPv6 Address to be configured.
 *  @param[in]  PrefixBits
 *      The network mask on which this address resides.
 *  @param[in]  VLT
 *      Valid Lifetime of the address
 *  @param[in]  PLT
 *      Preferred Lifetime of the address
 *  @param[in]  Type
 *      Type of Address (UNICAST or ANYCAST).
 *
 *  @retval
 *      Not Applicable.
 */
void addGlobalIpv6() {

    if (IPv6Info.test_global_ipv6) {
        if (IPv6StringToIPAddress (TEST_IPV6_ADDR, &IPv6Info.globalAddress) < 0)
            return;
    }

    /* Set the IPv6 Address in the System */
    if (IPv6AddAddress ((uint16_t)IPv6Info.device_id, IPv6Info.globalAddress, IPv6Info.network_prefix, INFINITE_LT, INFINITE_LT, 0) < 0)
        ConPrintf ("Error: Unable to add the IPv6 Address\n");
    else {
        ConPrintf ("IPv6 Address has been configured.\n");
    }
}


void gpakIPv6AddAddress(char* Address, char* PrefixBits, char* VLT, char* PLT)
{
    uint16_t    dev_index;
    // NIMU_IF_REQ if_req;
    unsigned char     isAnycast = 0;
    uint16_t    NumBits;
    uint32_t    ValidLifetime;
    uint32_t    PrefLifetime;
    IP6N        IPAddress;

    /* Get the device index; by first trying it to be an identifier. */
    dev_index =  (uint16_t)IPv6Info.device_id;

    /* Convert the Address from CHAR to IP6N format. */
    IPv6StringToIPAddress (Address, &IPAddress);

    /* Make sure the number of prefix bits is correct. */
    NumBits = atoi (PrefixBits);
    if (NumBits == 0)
    {
        /* Error: Incorrect number of prefix bits. */
        ConPrintf ("Error: Incorrect Number of PREFIX Bits Specified %s\n", PrefixBits);
        return;
    }

    /* Configure the Valid Lifetime. Check if INFINITE was specified? */
    if (!strcmp (VLT, "INFINITE"))
    {
        /* YES. Set the Valid Lifetime. */
        ValidLifetime = INFINITE_LT;
    }
    else
    {
        /* NO; convert from string to integer format. */
        ValidLifetime = atoi (VLT);
    }


    /* Configure the Preferred Lifetime. Check if INFINITE was specified? */
    if (!strcmp (PLT, "INFINITE"))
    {
        /* YES. Set the Preferred Lifetime. */
        PrefLifetime = INFINITE_LT;
    }
    else
    {
        /* NO; convert from string to integer format. */
        PrefLifetime = atoi (PLT);
    }


    /* Set the IPv6 Address in the System */
    if (IPv6AddAddress (dev_index, IPAddress, NumBits, ValidLifetime, PrefLifetime, isAnycast) < 0)
        ConPrintf ("Error: Unable to add the IPv6 Address\n");
    else
        ConPrintf ("IPv6 Address has been configured.\n");

    /* Work has been done. */
    return;
}

/**
 *  @b Description
 *  @n
 *      The function is called to delete an IPv6 Address from the specific interface.
 *
 *  @param[in]  ifId
 *      Interface Identifier over which the address is to be deleted.
 *  @param[in]  Address
 *      IPv6 Address to be removed.
 *
 *  @retval
 *      Not Applicable.
 */
void gpakIPv6DelAddress (char* ifId, char* Address)
{
    uint16_t    dev_index;
    NIMU_IF_REQ if_req;
    IP6N        IPAddress;

    /* Get the device index; by first trying it to be an identifier. */
    dev_index = atoi (ifId);
    if (dev_index == 0)
    {
        if (strlen(ifId) < MAX_INTERFACE_NAME_LEN) {
            /* Error: Unable to convert to integer; maybe we were given a name. */
            strcpy (if_req.name, ifId);
        }
        else {
            ConPrintf("gpakIPv6DelAddress: error interface ID name too long\n");
        }

        if_req.index = 0;
        if (NIMUIoctl (NIMU_GET_DEVICE_INDEX, &if_req, &dev_index, sizeof(dev_index)) < 0)
        {
            ConPrintf ("Error: Device does not exist.\n");
            return;
        }
    }

    /* Convert the Address from CHAR to IP6N format. */
    IPv6StringToIPAddress (Address, &IPAddress);

    /* Delete the IPv6 Address from the */
    if (IPv6DelAddress (dev_index, IPAddress) < 0)
        ConPrintf ("Error: Unable to remove the address\n");
    else
        ConPrintf ("IPv6 address has been removed from the interface\n");

    return;
}

/**
 *  @b Description
 *  @n
 *      Utility Function which prints the IPv6 address on console. The IP address passed
 *      has to be specified in network order (IP6N).
 *
 *  @param[in]   address
 *      IPv6 Address to be displayed.
 *  @retval
 *   Not Applicable.
 */
void gpakIPv6DisplayIPAddress (IP6N address)
{
    char    strIPAddress[40];

    /* Convert to string format. */
    IPv6IPAddressToString (address, &strIPAddress[0]);

    /* Print out the address on the console. */
    ConPrintf ("%s\n", strIPAddress);

    return;
}
// return 1 if the same, 0 if different
#pragma CODE_SECTION (areSameIp, "PKT_PROG_SECT")
int areSameIp(ADT_UInt8 *ip1, ADT_UInt8 *ip2) {
int i;
    for (i=0; i<16; i++) {
        if (ip1[i] != ip2[i]) return 0;
    }
    return 1;
}

#pragma CODE_SECTION (isNonZeroIp, "PKT_PROG_SECT")
ADT_UInt8 isNonZeroIp(ADT_UInt8 *ip) {
int i;
ADT_UInt8 nullIP=0;
    for (i=0; i<16; i++) nullIP |= ip[i];

    return nullIP;
}

// Called when IPv6 adds new IP to route.
// neighbor advertisement determines IP to MAC mapping.
#pragma CODE_SECTION (IPSessionUpdate6, "PKT_PROG_SECT")
static void IPSessionUpdate6 (IP6N rmt_dstIP, ADT_UInt8 *MAC) {
   IPToSess_t *ipSess;
   int i;

   for (i=0, ipSess=IPToSess; i < 0x100; i++, ipSess++) {
      if (isNonZeroIp(ipSess->rmt6.dstIP)) {
         if (areSameIp(ipSess->rmt6.dstIP, rmt_dstIP.u.addr8) == 0)
		    continue;
	  } else {
         if (isNonZeroIp(ipSess->rmt6.IP) == 0) {
		    continue;
		 } else {
		    if (areSameIp(ipSess->rmt6.IP, rmt_dstIP.u.addr8) == 0)
		       continue;  
		 }
      }
      memcpy (ipSess->rmt6.MAC, MAC, 6);
      ipSess->lcl6.SSRC = 1;
      //logIPSess (0, ipSess);
   }
   return;
}

void initIp6CallbackPtrs() {
   MACUpdate6= &IPSessionUpdate6;
   appUdpInput6 = &appUdpInputIPv6;
}


// Called when findMACForIP6 needs to search IP to MAC mapping.
#pragma CODE_SECTION (findMAC_IPSessionUpdate6, "PKT_PROG_SECT")
static void findMAC_IPSessionUpdate6 (ADT_UInt8 *rmt_dstIP, ADT_UInt8 *MAC) {
   IPToSess_t *ipSess;
   int i;

   for (i=0, ipSess=IPToSess; i < 0x100; i++, ipSess++) {
      if (isNonZeroIp(ipSess->rmt6.dstIP)) {
         if(areSameIp(ipSess->rmt6.dstIP, rmt_dstIP)==0)
		    continue;
	  } else if(isNonZeroIp(ipSess->rmt6.IP)) {
		   if (areSameIp(ipSess->rmt6.IP, rmt_dstIP)==0) 
		       continue;  
      } else if(isNonZeroIp(ipSess->rmt6.IP) == 0) {
    	  continue;
      }
      if(ipSess->lcl6.SSRC == 1)
    	  continue;
      memcpy (ipSess->rmt6.MAC, MAC, 6);
      ipSess->lcl6.SSRC = 1;
      //logIPSess (0, ipSess);
   }
   return;
}


#pragma CODE_SECTION (findMACForIP6, "PKT_PROG_SECT")
static void findMACForIP6 (ADT_UInt8 *rmt_dstIP, ADT_UInt8 *MAC) {
   IPToSess_t *ipSess;
   int i;

   for (i=0, ipSess=IPToSess; i < 0x100; i++, ipSess++) {
      if (ipSess->lcl6.SSRC == 0)   continue;
      if (isNonZeroIp(ipSess->rmt6.dstIP)) {
    	  if (areSameIp(ipSess->rmt6.dstIP, rmt_dstIP)==0) continue;
      } else { // rmt.IP
    	  if (areSameIp(ipSess->rmt6.IP, rmt_dstIP)==0) continue;
      }
      findMAC_IPSessionUpdate6 (rmt_dstIP, ipSess->rmt6.MAC);
      return;
   }
   return;
}
#pragma CODE_SECTION (ipToChanLookup6, "PKT_PROG_SECT")
IPToSess_t *ipToChanLookup6 (ADT_UInt8 *rmtIP, ADT_UInt16 rmtPort, int *chanId) {
   int hashPort;
   int storeIdx, hashIdx;
   IPToSess_t *openIPSess;
   register IPToSess_t *ipSess;
   int i;

   hashPort   = (int) rmtPort;
   hashIdx = (ADT_UInt8)(hashPort ^ hashPort >> 8);
   for (i=0; i<16; i++) hashIdx ^= rmtIP[i];   



   storeIdx = hashIdx;
   ipSess = &IPToSess[storeIdx];
   openIPSess = NULL;
   do {
        if (ipSess->RTPSession != NULL && (ipSess->ipver == 6) && areSameIp(ipSess->rmt6.IP, rmtIP) && ipSess->rmt6.Port == rmtPort) {
            openIPSess = ipSess;
            break;
        }

      // Move to next ipSess entry, checking for wrap
      storeIdx = (storeIdx + 1) & 0xff;
      ipSess++;
      if (storeIdx == 0) ipSess = IPToSess;                   // Index wrap

   } while (storeIdx != hashIdx);

   if (openIPSess) {
        *chanId = openIPSess->ChanId;
        return (openIPSess);
   } else {
        *chanId = -1;
        return (IPToSess_t *)0;
   }
        
} 

#pragma CODE_SECTION (MulticastIPToMulticastMAC6, "PKT_PROG_SECT")
void MulticastIPToMulticastMAC6 (ADT_UInt8 *IpAddr, ADT_UInt8 *MacAddr) {

   // Form multicast MAC address
   MacAddr[0] = 0x33;
   MacAddr[1] = 0x33;
   MacAddr[2] = IpAddr[12];
   MacAddr[3] = IpAddr[13];
   MacAddr[4] = IpAddr[14];
   MacAddr[5] = IpAddr[15];
}

#pragma CODE_SECTION (IPSessionLookupRxMcast6, "PKT_PROG_SECT")
IPToSess_t *IPSessionLookupRxMcast6 (ADT_UInt8 *mcastIP, ADT_UInt16 mcastPort, ADT_UInt8 *matchList, ADT_UInt8 *listLen) {
   int hashPort, i;
   int storeIdx, hashIdx;
   IPToSess_t *openIPSess;
   register IPToSess_t *ipSess;
   ADT_UInt8 ListLen = 0;

   hashPort   = (int) mcastPort;
   hashIdx = (ADT_UInt8)(hashPort ^ hashPort >> 8);
   for (i=0; i<16; i++) hashIdx ^= mcastIP[i];   

   storeIdx = hashIdx;
   ipSess = &IPToSess[storeIdx];
   openIPSess = NULL;

    // When searching for existing entries must find entry with:
    //    RTPSession defined && matching rmtIP && matching rmtPort
    // There can be multiple receivers on the same IP:port, 
    // Search entire session table creating a list of all matches
    do {
        if (ipSess->RTPSession != NULL &&  areSameIp(ipSess->lcl6.dstIP, mcastIP)) 
        {
           // update list of mcast listener's channel ID
           matchList[ListLen++] = (ADT_UInt8)ipSess->ChanId;
           if (openIPSess == 0) openIPSess = ipSess;
           // return ipSess;   // Full match
        }

        // Move to next ipSess entry, checking for wrap
        storeIdx = (storeIdx + 1) & 0xff;
        ipSess++;
        if (storeIdx == 0) ipSess = IPToSess;                   // Index wrap

    } while (storeIdx != hashIdx);

    *listLen = ListLen;

    return openIPSess;
}

//{
// Hash search of IPToSess table for matching remote IPAddr and port or next available entry
// A hashing algorithm is used to generate an 'best' index into the lookup table.  The lookup
// table is then searched forward (incrementing the index by one each iteration) until a matching
// or available entry is found
//
// Reomte IP and port may occur multiple times with different SSRC values.
//}
 
#pragma CODE_SECTION (IPSessionLookup6, "PKT_PROG_SECT")
IPToSess_t *IPSessionLookup6 (ADT_UInt8 *rmtIP, ADT_UInt16 rmtPort, ADT_UInt32 rmtSSRC, ADT_Bool new) {
   int hashPort;
   ADT_UInt8 storeIdx, hashIdx;
   IPToSess_t *openIPSess;
   register IPToSess_t *ipSess;
   int i;

   hashPort   = (int) rmtPort;
   hashIdx = (ADT_UInt8)(hashPort ^ hashPort >> 8);
   for (i=0; i<16; i++) hashIdx ^= rmtIP[i];   

   storeIdx = hashIdx;
   ipSess = &IPToSess[storeIdx];
   openIPSess = NULL;

   if (!new) {
      // When searching for existing entries must find entry with:
      //    RTPSession defined && matching rmtIP && matching rmtPort
      //    if (entries rmtSSRC != 0) rmtSSRC must match
      do {
         if (ipSess->RTPSession != NULL && (ipSess->ipver == 6) && areSameIp(ipSess->rmt6.IP, rmtIP) && ipSess->rmt6.Port == rmtPort) {
            if (ipSess->rmt6.SSRC == rmtSSRC) return ipSess;   // Full match
            
            // Store first entry of unassigned matching IP/port for match if no others are found (first time for SSRC).
            if (openIPSess == NULL && ipSess->rmt6.SSRC == 0) openIPSess = ipSess;
         }

         // Move to next ipSess entry, checking for wrap
         storeIdx = (storeIdx + 1) & 0xff;
         ipSess++;
         if (storeIdx == 0) ipSess = IPToSess;                   // Index wrap

      } while (storeIdx != hashIdx);

      // First packet for IP/port with unassigned SSRC.  Assign SSRC.
      if (openIPSess) openIPSess->rmt6.SSRC = rmtSSRC;
      return openIPSess;
   }

   // For new entries, must find:
   //    first unused entry
   while (ipSess->RTPSession != 0) {

      // Check for wrap
      storeIdx = (storeIdx + 1) & 0xff;
      if (storeIdx == hashIdx) return NULL;

      ipSess++;
      if (storeIdx == 0) ipSess = IPToSess;   // Index wrap
   }

   // Channel setup.  Assign hash index to entry
   ipSess->idx = storeIdx;
   ipSess->hashIdx = hashIdx;
   ipSess->ipver = 6;
   return ipSess;
}
//{  Add entry to IPSession lookup table.
//
//  Called by messaging thread to link a remote socket/SSRC to an RTPSession.
//
//   non-negative - index to IPToSess structure
//   -1           - failure (table full)
//   -2           - failure (IPAddr:port:SSRC already exists)
//   -3           - failure RTPTask not initialized
//   -4           - failure (Channel ID not in range from 0-255)
//
//}
#pragma CODE_SECTION (addToIPSession6, "SLOW_PROG_SECT")
int addToIPSession6 (IPToSess_t *new, void *RTPSession, ADT_UInt16 ChanId) {
   rtpAddr6    *rmt, *lcl;
   IPToSess_t *entry;
   ADT_UInt8  nonZeroMac, nonZeroRmtDstIP, nonZeroRmtIP, nonZeroLclDstIP;
   int        i, mask;

//   if (!RTPTaskInit)  return -3;
   if (0xff < ChanId) return -4;

   rmt = &new->rmt6;
   lcl = &new->lcl6;
   nonZeroRmtIP = isNonZeroIp(rmt->IP);
   nonZeroRmtDstIP = isNonZeroIp(rmt->dstIP);
   nonZeroLclDstIP = isNonZeroIp(lcl->dstIP);

   if (nonZeroRmtDstIP == 0)
        memcpy(rmt->dstIP, rmt->IP, 16);

   // Convert addresses to network byte order.
   rmt->Port  = NDK_htons (rmt->Port);
   rmt->SSRC  = NDK_htonl (rmt->SSRC);

#ifdef IPV6_MCAST
   // Establish multicast MAC address from multicast IP address.
   if (IN_MULTICAST6 (rmt->dstIP)) {
      MulticastIPToMulticastMAC6 (rmt->dstIP, rmt->MAC);
   }
#endif

   lcl->Port  = NDK_htons (lcl->Port);

   //  Find open entry in IPToSess table
   mask = IPMap_lock ();

   if (nonZeroLclDstIP != 0) {
        // lcl-> dstIP (i.e. API InDestIP) can be mcast rx listening IP address
        //  OR possibly unicast IP rx address
#ifdef IPV6_MCAST
	   if(IN_MULTICAST6(lcl->dstIP))
          entry = IPSessionLookup6 (lcl->dstIP, 0, 0, TRUE);
	   else 
#endif 
       {
          if (nonZeroRmtIP == 0) {
                // API param destIP is zero, therefore use InDestIP(lcl.dstIP) for Rx
            for(i=0; i<16; i++) rmt->IP[i] = lcl->dstIP[i];
          } 
          entry = IPSessionLookup6 (rmt->IP, rmt->Port, rmt->SSRC, TRUE);
        }
   } 
   else 
   {
        entry = IPSessionLookup6 (rmt->IP, rmt->Port, rmt->SSRC, TRUE);
   }

   if (entry == NULL) {
      IPMap_unlock (mask);
      return -1;                                // Table full
   }
   if (entry->RTPSession != NULL) {
      IPMap_unlock (mask);
      return -2;                                // Duplicate IP:port:SSRC
   }

   //logTime (0x100B1000 | ChanId);


   // Identify MAC:IP:Port:SSRC addresses of remote device
   mmCopy (&entry->rmt6, rmt, sizeof (rtpAddr6));


   // Create a static binding between remote IP and remote Mac address when a non-zero Mac Address is supplied
   // NOTE: When lcl.SSRC is zero, the remote MAC address is zero and the IP stack must perform ARP to
   //       resolve the MAC address.  When lcl.SSRC is non-zero, the remote MAC address in non-zero and 
   //       fastRTPSend can be used since the MAC address has been resolved.
   entry->lcl6.SSRC = 0;
   if (areSameIp(IPv6Info.linkLocalAddress.u.addr8, rmt->IP) ||
        areSameIp(IPv6Info.globalAddress.u.addr8, rmt->IP))
   {
        // Force locally addressed packets to bypass fastRTPsend.
        memset (&entry->rmt.MAC, 0, 6);
   } else
   {
      for (i=0, nonZeroMac=0; i<6; i++) nonZeroMac |= rmt->MAC[i];
      
      if (nonZeroMac) {
        entry->lcl6.SSRC = 1;
      } else {
   	      if(isNonZeroIp(entry->rmt6.dstIP))
    		  findMACForIP6 (entry->rmt6.dstIP, entry->rmt6.MAC);
    	  else if(isNonZeroIp(entry->rmt6.IP))
    		  findMACForIP6 (entry->rmt6.IP, entry->rmt6.MAC);
      }
   }

   // Save current local configuration of MAC:IP:Port addresses for use as src addresses in transmissions
   mmCopy (entry->lcl6.MAC, (void *) IPConfig.TxMac, 6);
   if (isNonZeroIp(IPv6Info.globalAddress.u.addr8))
        memcpy(&entry->lcl6.IP, &IPv6Info.globalAddress.u.addr8, 16);
   else
        memcpy(&entry->lcl6.IP, &IPv6Info.linkLocalAddress, 16);

   if (nonZeroLclDstIP == 0)
        memcpy(&entry->lcl6.dstIP, &entry->lcl6.IP, 16);
   else                 
        memcpy(&entry->lcl6.dstIP, &lcl->dstIP, 16);

   if (lcl->Port)
      entry->lcl6.Port   = lcl->Port;
   else if (IPConfig.rtpPort)
      entry->lcl6.Port   = IPConfig.rtpPort;
   else
      entry->lcl6.Port   = NDK_htons (DEFAULT_RTP_PORT);

   entry->RTPSession = RTPSession;
   entry->ChanId     = ChanId;
   entry->vlanIndex  = new->vlanIndex;
   entry->DSCP       = new->DSCP;

#ifdef IPV6_MCAST 
   // Add local multicast IP to list of multicast IP receive filters
   if (IN_MULTICAST6 (entry->lcl6.dstIP)) {
      queueMultiCastRequest6  (entry->lcl6.dstIP, ADD_MULTICAST_IP6);
   }
#endif

   chnToIPSess [ChanId] = entry;
   //logIPSess (0xa, entry);

   IPMap_unlock (mask);
   return entry->idx;
}


static HANDLE appUdpInputIPv6(PBM_Pkt **ppPkt, UDPHDR *pUdpHdr)
{
	UINT16 dstPort;				// destination UDP port number
	//IPHDR *pIpHdr;		    // pointer to packet's IP header
	UINT8 *pUdpPayload;			// pointer to UDP payload
	ADT_UInt8 *remoteIp;		// remote's IP address
	ADT_UInt16 remotePort;		// remote's UDP port number
	IPToSess_t *pRtpMapping;	// pointer to RTP mapping info
	ADT_UInt32 remoteSSRC;		// remote's SSRC (endian swap ?)
	int mask;					// lock mask
	UINT16 channelId;			// channel Id
    int rtcp = 0;
    ADT_UInt8 mcastList[0x100], i, numMcast = 0;
    PBM_Pkt *pPktCopy;

	IP6N dstAddr;		    // Dst IP address of receive packet
    IPV6HDR* ipv6_hdr;

    ipv6_hdr = (IPV6HDR* )(*ppPkt)->pIpHdr;
    mmCopy((char *)&dstAddr, (char *)&ipv6_hdr->DstAddr, sizeof(IP6N));


	// Don't consume packets with a destination outside the RTP port range.
	dstPort = HNC16(pUdpHdr->DstPort);
	if ((dstPort < lowRTPPort) || (dstPort > highRTPPort))
	{
		return NULL;
	}

	// Get the remote transport address and pointer to payload.
	// pIpHdr = (IPHDR *) (((UINT8 *) pUdpHdr) - (*ppPkt)->IpHdrLen);
	remoteIp = (ADT_UInt8 *)&(*ppPkt)->SrcAddress;
	remoteIp = (ADT_UInt8 *)&(*ppPkt)->SrcAddress;
	remotePort = pUdpHdr->SrcPort;
	pUdpPayload = ((UINT8 *) pUdpHdr) + UDPHDR_SIZE;

	// If a custom packet input function is used, call it to make a channel
	// determination.
	pRtpMapping = &nullIPSess;
	
	// if Custom preheader used, return valid ipsess/modified payload with SSRC=rxpreheader
	// if standard RTP header used, return nullIPSess
	if (customNetIn6 != NULL) 
	{
		pRtpMapping = (*customNetIn6) (remoteIp, remotePort, pUdpPayload);
		if (pRtpMapping == NULL)
		    pRtpMapping = &nullIPSess;
	}

    // run rtcp callback to deterine if packet is RTCP
#ifdef RTCP_ENABLED
	if ((rtcpNetIn6 != NULL) && (pRtpMapping->RTPSession == NULL)) 
	{
		pRtpMapping = (*rtcpNetIn6) (remoteIp, remotePort, pUdpPayload);
		if (pRtpMapping == NULL)
		    pRtpMapping = &nullIPSess;
		else if (pRtpMapping->RTPSession != NULL) {
            IPStats.rtp.Rx.Rtcp++;
            rtcp = 1;
        }
	}
#endif

	// If the channel wasn't determined, check for a matching channel session.
	// For standard RTP header, lookup ipsess here and updated rmt.SSRC in lookup
	if (pRtpMapping->RTPSession == NULL)
	{
		if ((pUdpPayload[0] & 0xc0) == 0x80) { // if a standard RTP header

			remoteSSRC = _mem4(&(pUdpPayload[8]));
			mask = IPMap_lock();
            if (dstAddr.u.addr8[0] == 0xFF) {
                 // multicast
			    pRtpMapping = IPSessionLookupRxMcast6(dstAddr.u.addr8, 0, mcastList, &numMcast);
            } else 
			    pRtpMapping = IPSessionLookup6(remoteIp, remotePort, remoteSSRC, FALSE);
			IPMap_unlock(mask);
		}
		if (pRtpMapping == NULL)
		{
			pRtpMapping = &nullIPSess;
		}
	}

	// If the target channel was determined, put the packet on the received RTP
	// packet queue.
	if (pRtpMapping->RTPSession != NULL)
	{
        if (numMcast > 1) {
            // make a copy of the packet buffer for additional multicast listeners
            // and add to PBM queue
            for (i=1; i<numMcast; i++) {
                pPktCopy = PBM_copy(*ppPkt);
                if (pPktCopy != NULL) {
		            channelId = mcastList[i];
	        	    pPktCopy->Aux1 |= (channelId << 24);
		            PBMQ_enq(&rtpRxPbmQueue, pPktCopy);
                }
            }
        }
		channelId = pRtpMapping->ChanId;
		(*ppPkt)->Aux1 |= (channelId << 24);
		PBMQ_enq(&rtpRxPbmQueue, *ppPkt);
        if (rtcp == 0)
		    IPStats.rtp.Rx.TotalCnt6++;
	}

	// Discard the packet if it doesn't match an active session.
	else
	{
		PBM_free(*ppPkt);
		IPStats.rtp.Rx.DiscardCnt++;
	}

	// Indicate the packet was consumed.
	*ppPkt = NULL;
	return NULL;
}
#endif /* _INCLUDE_IPv6_CODE */

