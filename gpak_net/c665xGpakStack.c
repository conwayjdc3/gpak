/*
   c66xGpakStack.c

   G.Pak support for c66x IP stack

   Creates IP stack task and supplies callbacks and functions needed by the IP Stack software

   'ipStackTask' is the main IP stack task. When the stack is opened, the NetworkOpenCallBack routine
   is called which in turn starts the RTP and TCP messaging thread

 */
#if 0

#include <std.h>
#include <stdio.h>
#include <strings.h>
#include <ti/ndk/inc/netmain.h>
#include <ti/ndk/inc/stkmain.h>
#include <ti/ndk/inc/ipStackUser.h>
#include <GpakBios.h>

#define INCLUDE_TELNET
#ifdef INCLUDE_TELNET
#include <ti/ndk/inc/tools/console.h>
#include <ti/ndk/inc/tools/servers.h>

char *VerStr = "\nGpak IPStack Application\n\n";
#endif

// callback function from MDIO/EMAC driver to report changes in LInk status
extern void (*pGpakReportLink) (uint32_t LinkEvent, uint32_t LinkStatus);


#ifdef _INCLUDE_IPv6_CODE
extern void gpakIPv6InitializeStack (char* ifId);
extern void gpakIPv6DisplayRouteTable (void);
#endif
extern void uart_printIPv4Address(IPN LocalIPAddr);

//#include "adt_typedef_user.h"
//#include "ipStackStats.h"

#define _DEBUG
extern void AppError (char *FileName, int Line, char *Msg, int Error);

#define AppErr(msg,cond) AppError (__FILE__, __LINE__, msg, cond)

#ifndef _DEBUG
#undef  AppErr
#define AppErr(msg, cond)
#endif

extern unsigned int IPStackHeap;  // Assigned by BIOS
extern void _mmBulkAllocSeg (unsigned int segId);
extern StackTest();


// Define function callbacks to satisfy gpak64xNetDelivery.
// They don't appear to be used in IPStack v2.0 for C674x device
void* (*appAlloc) (ADT_UInt32 memI8) = 0;
void  (*appFree)  (void* mem) = 0;
int   appMemI8 = 0;



#ifdef EMAC_DEBUG 
#include <csl_emac.h>
// Allow structures to be seen by debugger.
extern EMAC_Device emacDev;
extern PKT_QUEUE   emacRxQueue, emacTxQueue;
extern EMAC_Pkt    emacHdrRx[], emacHdrTx[];
extern PBMQ        PBMQ_free;
extern PBM_Pkt     pbmHdrMem[];


struct ipDebug {
   EMAC_Device* _emacDev;

   EMAC_Pkt*    _emacPktRx;
   EMAC_Pkt*    _emacPktTx;
   PKT_QUEUE*   _pktQueueRx; 
   PKT_QUEUE*   _pktQueueTx;

   PBM_Pkt*     _pbmPktMem;
   PBMQ*        _PBMQ_free;

   PBMQ*        _pbmTx;
   PBMQ*        _rawPbmTx;
   PBMQ*        _pbmRx;
   PBMQ*        _rawPbmRx;
   
   SOCK*       _sock;
   SB*         _sockBuffer;
   FDPOLLITEM* sktList;
} ipDebug = {
   &emacDev,
   emacHdrRx,     emacHdrTx,
   &emacRxQueue, &emacTxQueue,
   pbmHdrMem,
   &PBMQ_free
};
#endif

// NOTE:  All values are expected in network byte order.
#pragma DATA_SECTION (IPConfig, "IPCFG")
const IPStackCfg IPConfig = {
   0,                                       // RTP Source IPAddr
   { 0, 0, 0, 0, 0, 0 },                    // Tx Mac

   { 0, 0, 0, 0, 0, 0 },                    // Rx Mac
   0,         // IPAddr
   0,         // Gateway IP
   0,         // IP Mask

   0,         // RTP port
   0,         // TCP Messaging port
   0,         // TCP Event port
   0,         // Gpak RTP range // Filler
   
   0,         // DHCP IP

   { 0 }        // Domain Name
};
// jdc tcp debug char HostName[30];
char HostName[30];
IPN *dhcpAddr = (IPN *) &IPConfig.DHCPIP;

// IPv6 information
IPv6Info_t IPv6Info;

// Initial debug warning level
#ifdef _DEBUG
#define WARNING_LEVEL DBG_WARN
#else
#define WARNING_LEVEL DBG_ERROR
#endif

//---------------------------------------------------------------------------

// Callback functions
static void NetworkOpenCallBack ();
static void NetworkCloseCallBack ();
static void NetworkIPAddrCallBack (IPN IPAddr, uint32_t IfIdx, uint32_t fAdd);
//static void NetworkShutDownCallBack ();

// DHCP status reporting function
static void ServiceReport (uint32_t Item, uint32_t Status, uint32_t Report, HANDLE hCfgEntry);

//---------------------------------------------------------------------------
//
//  IP task stack parameters.  DHCP, timeouts, general control
//
#pragma DATA_SECTION (IPTaskStack, "STACK_ALLOCATION:MainIPTask")
#pragma DATA_ALIGN   (IPTaskStack, 128)
static char IPTaskStack [0x1000];
#if 0
static const TSK_Attrs IPStackAttrs = {
   OS_TASKPRIHIGH_DEF,   // Priority
   IPTaskStack,
   sizeof(IPTaskStack),
   0,                 // Stack memory segment
   NULL,              // Environment
   "IPStack",        // Task name
   TRUE,              // Task terminates on program termination
   TRUE               // Initialize task stack
};
#endif

//
//  G.PAK task stack parameters.  RTP and TCP messaging task.
//
extern int RTPTask ();
static HANDLE IPRcvHndl = 0;
#pragma DATA_SECTION (RcvTaskStack, "STACK_ALLOCATION:RTPTask")
#pragma DATA_ALIGN   (RcvTaskStack, 128)
static char RcvTaskStack [0x1000];

#if 0
static const TSK_Attrs RcvStackAttrs = {
   OS_TASKPRIHIGH_DEF,   // Priority
   RcvTaskStack,
   sizeof(RcvTaskStack),
   0,                 // Stack memory segment
   NULL,              // Environment
   "GpakRTPTask",    // Task name
   TRUE,              // Task terminates on program termination
   TRUE               // Initialize task stack
};
#endif

RTPTaskParams_t RTPTaskParams;


far IPN LocalIPAddr;
#pragma DATA_SECTION (LocalIPAddr,  "NON_CACHED_DATA:IPStack")

//static void (*pFxn) () = &NetworkShutDownCallBack;

//  Service tasks and status info
static char * const TaskName[] = { "Telnet", "HTTP", "NAT", "DHCPS", "DHCPC", "DNS" };
static char * const StatusStr[] = { "Disabled", "Waiting", "IPTerm", "Failed", "Enabled" };
static char * const ReportStr[] = { "", "Running", "Updated", "Complete", "Fault" };

// IP stack initialization and main thread
static int ipStackTask (IPStackCfg *IPConfig) {
   int rc;
   HANDLE hCfg;

   LocalIPAddr = 0;

   memset (HostName, 0, sizeof(HostName));
   sprintf (HostName, "Gpak.%02x.%02x.%02x.%02x.%02x.%02x",
      IPConfig->RxMac[0], IPConfig->RxMac[1], IPConfig->RxMac[2],
      IPConfig->RxMac[3], IPConfig->RxMac[4], IPConfig->RxMac[5]);

   // Validate the length of the supplied names
   if ( CFG_DOMAIN_MAX <= strlen (IPConfig->DomainName) ||
        CFG_HOSTNAME_MAX <= strlen (HostName) ) {
      AppErr ("Network names too long", TRUE);
      return 1;
   }

#ifdef _DEBUG
   printf ("Host name= %s\n", HostName);
#endif

   // Initialize network system
   rc = NC_SystemOpen (NC_PRIORITY_HIGH, NC_OPMODE_INTERRUPT);
   if (rc) {
      AppErr ("NC_SystemOpen Failed", rc);
      return 1;
   }
   // jdc no longer needed ? _mmBulkAllocSeg (IPStackHeap);


   //============================================================
   // Create and build the system configuration from scratch.
   hCfg = CfgNew ();
   if (!hCfg) {
      AppErr ("Unable to create configuration", 1);
      goto main_exit;
   }

   // Format and add hostname (to be claimed in all connected domains)
   CfgAddEntry (hCfg, CFGTAG_SYSINFO, CFGITEM_DHCP_HOSTNAME, 0,
      strlen (HostName), (UINT8 *) HostName, 0);

   //rc = 512; jdc was this for all
   // UDP receive packet size limit
   rc = 8192;
   CfgAddEntry (hCfg, CFGTAG_IP, CFGITEM_IP_SOCKUDPRXLIMIT,
      CFG_ADDMODE_UNIQUE, sizeof(uint32_t), (UINT8 *) &rc, 0);

   // TCP Transmit buffer size
   rc = 8192;
   CfgAddEntry (hCfg, CFGTAG_IP, CFGITEM_IP_SOCKTCPTXBUF,
      CFG_ADDMODE_UNIQUE, sizeof(uint32_t), (UINT8 *) &rc, 0);

   // TCP Receive buffer size (copy mode)
   rc = 8192;
   CfgAddEntry (hCfg, CFGTAG_IP, CFGITEM_IP_SOCKTCPRXBUF,
      CFG_ADDMODE_UNIQUE, sizeof(uint32_t), (UINT8 *) &rc, 0);

   // TCP Receive limit (non-copy mode)
   rc = 8192;
   CfgAddEntry (hCfg, CFGTAG_IP, CFGITEM_IP_SOCKTCPRXLIMIT,
      CFG_ADDMODE_UNIQUE, sizeof(uint32_t), (UINT8 *) &rc, 0);

   if (IPConfig->IPAddr) {
      //  Setup static IP Address
      CI_IPNET NA;
      CI_ROUTE RT;

#if 0 //def _DEBUG
// JDC Why only when _DEBUG is defined ?
      if ((IPConfig->IPAddr & IPConfig->IPMask) != (IPConfig->GateWayIP & IPConfig->IPMask)) {
         // Gateway and IP on different networks
         // Add second network to allow access to gateway
         bzero (&NA, sizeof(NA));
         NA.NetType = 0;
         NA.IPAddr = (IPConfig->GateWayIP & IPConfig->IPMask) | 0xFE000000;  // JDC Why is IP address Or'd with FE ?
         NA.IPMask = IPConfig->IPMask;
         strcpy (NA.Domain, IPConfig->DomainName);
         CfgAddEntry (hCfg, CFGTAG_IPNET, 1, 0, sizeof(CI_IPNET), (UINT8 *) &NA, 0);
      }
#endif

      // Add the IP address to interface 1
      bzero (&NA, sizeof(NA));
      NA.NetType = 0;
      NA.IPAddr = IPConfig->IPAddr;
      NA.IPMask = IPConfig->IPMask;
      strcpy (NA.Domain, IPConfig->DomainName);
      CfgAddEntry (hCfg, CFGTAG_IPNET, 1, 0,  sizeof(CI_IPNET), (UINT8 *) &NA, 0);

      // Add the Gateway router
      bzero (&RT, sizeof(RT));
      RT.IPDestAddr = 0;
      RT.IPDestMask = 0;
      RT.IPGateAddr = IPConfig->GateWayIP;
      CfgAddEntry (hCfg, CFGTAG_ROUTE, 0, 0, sizeof(CI_ROUTE), (UINT8 *) &RT, 0);
   } else {
      // Setup DHCP client to obtain IP address
      CI_SERVICE_DHCPC dhcpc;

      // Specify DHCP Service on IF-1
      bzero (&dhcpc, sizeof(dhcpc));
      dhcpc.cisargs.Mode = CIS_FLG_IFIDXVALID;
      dhcpc.cisargs.IfIdx = 1;
      dhcpc.cisargs.pCbSrv = &ServiceReport;
      CfgAddEntry (hCfg, CFGTAG_SERVICE, CFGITEM_SERVICE_DHCPCLIENT, 0,
         sizeof(dhcpc), (UINT8 *) &dhcpc, 0);
   }
#ifdef INCLUDE_TELNET
   CI_SERVICE_TELNET telnet;

   /* Specify TELNET service for our Console example */
   bzero( &telnet, sizeof(telnet) );
   telnet.cisargs.IPAddr = INADDR_ANY;
   telnet.cisargs.pCbSrv = &ServiceReport;
   telnet.param.MaxCon   = 2;
   telnet.param.Callback = &ConsoleOpen;
   CfgAddEntry( hCfg, CFGTAG_SERVICE, CFGITEM_SERVICE_TELNET, 0,
                sizeof(telnet), (uint8_t *)&telnet, 0 );
#endif

   // Set debug messages level
//   rc = WARNING_LEVEL;
   rc = DBG_INFO;
   CfgAddEntry (hCfg, CFGTAG_OS, CFGITEM_OS_DBGPRINTLEVEL,
      CFG_ADDMODE_UNIQUE, sizeof(uint32_t), (UINT8 *) &rc, 0);


   //
   // Boot the system using this configuration
   //
   // Reboot when function returns > zero ("reboot" command).
   //
   do {
      rc = NC_NetStart (hCfg, NetworkOpenCallBack, NetworkCloseCallBack, NetworkIPAddrCallBack);
   } while (0 < rc);

   // Delete Configuration
   CfgFree (hCfg);

   // Close the OS
 main_exit:
   NC_SystemClose ();
   return 0;
}
#if 0 //tcp debug
int gpak_udp_hello( SOCKET s, uint32_t unused )
{
    struct sockaddr_in sin1;
    struct timeval     to;
    int                i,tmp;
    char               *pBuf;
    void*             hBuffer;

    (void)unused;

    // Configure our socket timeout to be 3 seconds
    to.tv_sec  = 3;
    to.tv_usec = 0;
    setsockopt( s, SOL_SOCKET, SO_SNDTIMEO, &to, sizeof( to ) );
    setsockopt( s, SOL_SOCKET, SO_RCVTIMEO, &to, sizeof( to ) );

    for(;;)
    {
        tmp = sizeof( sin1 );
        i = (int)recvncfrom( s, (void **)&pBuf, 0, &sin1, &tmp, &hBuffer );

        // Spit any data back out
        if( i >= 0 )
        {
            sendto( s, pBuf, i, 0, &sin1, sizeof(sin1) );
            recvncfree( hBuffer );
        }
        else
            break;
    }

    // Since the socket is still open, return "1"
    // (we need to leave UDP sockets open)
    return(1);    
}



//
// System Task Code [ Server Daemon Servers ]
//
static void* hHello=0;
#endif

//---------------------------------------------------------------------------
//{  IP stack call back routines
//
//
//{ NetworkOpenCallBack
//
// This function is called after the configuration has booted and creates the
// RTP task
//}
static void NetworkOpenCallBack () {
#if 0
    // Create our local server
    hHello = DaemonNew( SOCK_DGRAM, 0, 7, gpak_udp_hello,
                       OS_TASKPRINORM, OS_TASKSTKNORM, 0, 1 );
#else
Task_Params taskParams;
Error_Block eb;

#ifdef _INCLUDE_IPv6_CODE
    gpakIPv6InitializeStack ("1");
#endif

    // Create our local RTP, message, and event servers
    RTPTaskParams.rtpPort = NDK_ntohs (IPConfig.rtpPort);
    RTPTaskParams.msgPort = NDK_ntohs (IPConfig.msgPort);
    RTPTaskParams.evtPort = NDK_ntohs (IPConfig.evtPort);

    Task_Params_init(&taskParams);
    taskParams.priority = OS_TASKPRIHIGH_DEF;
    taskParams.stack = RcvTaskStack;
    taskParams.stackSize =   sizeof(RcvTaskStack);
    taskParams.arg0 =  (UArg)&RTPTaskParams;
    taskParams.arg1 = 0;
    Error_init(&eb);

    IPRcvHndl = Task_create ((Task_FuncPtr)RTPTask, &taskParams, &eb);
    if (IPRcvHndl == NULL) {
        AppErr ("RTPTask create Failed", -1);
    }

#ifdef EMAC_DEBUG 
    ipDebug._pbmRx    = &emacInst->PBMQ_rx; 
    ipDebug._rawPbmRx = &emacInst->PBMQ_rawrx;

    ipDebug._pbmTx    = &emacInst->PBMQ_tx; 
    ipDebug._rawPbmTx = &emacInst->PBMQ_rawtx;
#endif


#endif
}

//{ NetworkCloseCallBack
//
// This function is called when the network is shutting down,
// or when it no longer has any IP addresses assigned to it.
//}
static void NetworkCloseCallBack () {
   TSK_delete (IPRcvHndl);
}

//{ NetworkIPAddrCallBack
//
// This function is called whenever an IP address binding is
// added or removed from the system.
//}
static void NetworkIPAddrCallBack (IPN IPAddr, uint32_t IfIdx, uint32_t fAdd) {

   if (fAdd) {
      DbgPrintf (DBG_INFO, "Network Added: ");
      LocalIPAddr = NDK_ntohl (IPAddr);
      *dhcpAddr = IPAddr;
      uart_printIPv4Address(LocalIPAddr);
   } else {
      DbgPrintf (DBG_INFO, "Network Removed: ");
      LocalIPAddr = 0;
      *dhcpAddr = 0;
   }

   // Print a message
   DbgPrintf (DBG_INFO, "IF%d IP=%d.%d.%d.%d", IfIdx,
      (UINT8) (LocalIPAddr >> 24) & 0xFF, (UINT8) (LocalIPAddr >> 16) & 0xFF,
      (UINT8) (LocalIPAddr >> 8) & 0xFF, (UINT8) LocalIPAddr & 0xFF);
   memcpy (&HostName[24], &LocalIPAddr, 4);

#ifdef _INCLUDE_IPv6_CODE
   gpakIPv6DisplayRouteTable(); 
#endif

}
#if 0
// NetworkShutDownCallBack
static void NetworkShutDownCallBack () {
   DbgPrintf (DBG_INFO, "IP stack is evaluation version. Shutting down in 5 min.");

}
#endif

//{ DHCP_reset()
//
// Task to reset DHCP client by removing it from the active config,
// and then reinstalling it.
//
// Called with:
// IfIdx    set to the interface (1-n) that is using DHCP.
// fOwnTask set when called on a new task thread (via TaskCreate()).
//}
static void DHCP_reset (uint32_t IfIdx, uint32_t fOwnTask) {
   CI_SERVICE_DHCPC dhcpc;
   HANDLE h;
   int rc, tmp;
   uint32_t idx;

   // If we were called from a newly created task thread, allow
   // the entity that created us to complete
   if (fOwnTask) {
      TaskSleep (500);
   }

   // Find DHCP on the supplied interface
   for (idx = 1;; idx++) {
      // Find a DHCP entry
      rc = CfgGetEntry (0, CFGTAG_SERVICE, CFGITEM_SERVICE_DHCPCLIENT, idx, &h);
      if (rc != 1) {
         goto RESET_EXIT;
      }

      // Get DHCP entry data
      tmp = sizeof(dhcpc);
      rc = CfgEntryGetData (h, &tmp, (UINT8 *) &dhcpc);

      // If not the right entry, continue
      if ((rc <= 0) || dhcpc.cisargs.IfIdx != IfIdx) {
         CfgEntryDeRef (h);
         h = 0;
         continue;
      }

      // Remove the current DHCP service
      CfgRemoveEntry (0, h);

      // Specify DHCP Service on specified IF
      bzero (&dhcpc, sizeof(dhcpc));
      dhcpc.cisargs.Mode = CIS_FLG_IFIDXVALID;
      dhcpc.cisargs.IfIdx = IfIdx;
      dhcpc.cisargs.pCbSrv = &ServiceReport;
      CfgAddEntry (0, CFGTAG_SERVICE, CFGITEM_SERVICE_DHCPCLIENT, 0,
         sizeof(dhcpc), (UINT8 *) &dhcpc, 0);
      break;
   }

 RESET_EXIT:
   // If we are a function, return, otherwise, call TaskExit()
   if (fOwnTask) {
      TaskExit ();
   }
}

// Service Status Reports
static void ServiceReport (uint32_t Item, uint32_t Status, uint32_t Report, HANDLE h) {
   CI_SERVICE_DHCPC dhcpc;
   int tmp;

   DbgPrintf (DBG_INFO, "Service Status: %-9s: %-9s: %-9s: %03d",
      TaskName[Item - 1], StatusStr[Status], ReportStr[Report / 256], Report & 0xFF);

   // Reset DHCP client service on failure
   if (Item == CFGITEM_SERVICE_DHCPCLIENT && (Report & ~0xFF) == NETTOOLS_STAT_FAULT) {
      printf("DHCP client reset\n");

      // Get DHCP entry data (for index to pass to DHCP_reset).
      tmp = sizeof(dhcpc);
      CfgEntryGetData (h, &tmp, (UINT8 *) &dhcpc);

      // Create task to reset DHCP
      TaskCreate (DHCP_reset, "DHCPreset", OS_TASKPRINORM, 0x1000,
         dhcpc.cisargs.IfIdx, 1, 0);
   }
}

//{ EMAC_getConfig ()
//
// This is a callback from the Ethernet driver. This function
// is used by the driver to get its 6 byte MAC address.
//}
// TODO -  EMACInit_Core calls nimu_get_emac_info (0, &emac_info); to get mac address from chip registers. Override with user-defined?
#if 0 // not called by pdk 2.0.13 NIMU/CSL
void EMAC_getConfig (ADT_UInt8 *pMacAddr, ADT_UInt32 portNum) {
   mmCopy (pMacAddr, &IPConfig.RxMac, 6);

   DbgPrintf (DBG_INFO, "Using MAC Address: %02x.%02x.%02x.%02x.%02x.%02x\n",
      pMacAddr[0], pMacAddr[1], pMacAddr[2],
      pMacAddr[3], pMacAddr[4], pMacAddr[5]);
}
#endif

//}

// API to create Gpak's network stack thread. Network stack creation, initialization
// and startup occurs in the ipStackTask thread.
TSK_Handle  ipStkHndl;
void GpakStackInit () {
Task_Params taskParams;
Error_Block eb;

   if ((IPConfig.TxMac[0] | IPConfig.TxMac[1] | IPConfig.TxMac[2] |
        IPConfig.TxMac[3] | IPConfig.TxMac[4] | IPConfig.TxMac[5]) == 0) {
      memcpy ((void *) IPConfig.TxMac, IPConfig.RxMac, sizeof(IPConfig.TxMac));
   }
   *dhcpAddr = 0;

    Task_Params_init(&taskParams);
    taskParams.priority = OS_TASKPRIHIGH_DEF;
    taskParams.stack = IPTaskStack;
    taskParams.stackSize =   sizeof(IPTaskStack);
    taskParams.arg0 =  (UArg)&IPConfig;
    taskParams.arg1 = 0;
    Error_init(&eb);

    ipStkHndl = Task_create ((Task_FuncPtr)ipStackTask, &taskParams, &eb);
//    ipStkHndl = Task_create ((Task_FuncPtr)StackTest, &taskParams, &eb);
    if (ipStkHndl == NULL) {
        AppErr ("IPTask create Failed", -1);
    }
}

#ifdef CHECK_LINK_STATUS 
// jdc .. must re-enable for c6678 stack
typedef struct linkReport_t {
    uint32_t LinkEvent;
    uint32_t LinkStatus;
}  linkReport_t;

#define MAX_LINK_REPORTS 8 /// probably more than enough if link never changes 
typedef struct linkInfo_t {
    int wrIdx;
    int rdIdx;
    int count;
    linkReport_t previous; 
    linkReport_t report[MAX_LINK_REPORTS];
} linkInfo_t;

linkInfo_t linkInfo = {0,0,0,{0xffffffff,0xffffffff}}; 

// emac//mdio driver callback function to process changes in PHY link state
// adds the reports to a queue.
void GpakReportLink (uint32_t LinkEvent, uint32_t LinkStatus) {
linkReport_t *report;

    // only report changes from previous report
    if ((linkInfo.previous.LinkEvent == LinkEvent) &&
        (linkInfo.previous.LinkStatus == LinkStatus)) 
        return;

    SWI_disable();
    report = &linkInfo.report[linkInfo.wrIdx];
    report->LinkEvent = LinkEvent;
    report->LinkStatus = LinkStatus;
    linkInfo.previous = *report;    
    linkInfo.wrIdx++;
    linkInfo.wrIdx &= (MAX_LINK_REPORTS-1);
    linkInfo.count++;
    SWI_enable();
}

// read reports from the queue
int GpakReadLinkReports(uint32_t *LinkEvent, uint32_t *LinkStatus) {
linkReport_t *report;
int count;

    count = linkInfo.count;
    if (count == 0) return 0;

    SWI_disable();
    report = &linkInfo.report[linkInfo.rdIdx];
    *LinkEvent = report->LinkEvent;
    *LinkStatus =  report->LinkStatus;
    linkInfo.rdIdx++;
    linkInfo.rdIdx &= (MAX_LINK_REPORTS-1);
    linkInfo.count--;
    SWI_enable();

    return count; 
}
#else
int GpakReadLinkReports(uint32_t *LinkEvent, uint32_t *LinkStatus) {
    return 0;
}
#endif
   
void initIPv6Info() {
    memset (&IPv6Info, 0, sizeof(IPv6Info_t));
    IPv6Info.device_id = 1;
    IPv6Info.device_id = 1;
    IPv6Info.network_prefix = 64;
#ifdef CHECK_LINK_STATUS 
    pGpakReportLink = GpakReportLink;
#endif

}



#endif 
